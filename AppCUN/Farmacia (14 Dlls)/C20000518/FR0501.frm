VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form FrmGenPetOfe 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Generar Petici�n Oferta"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11895
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0501.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11895
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   37
      Top             =   0
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   10080
      Top             =   1920
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Material &Sanitario"
      Height          =   375
      Index           =   2
      Left            =   10080
      TabIndex        =   24
      Top             =   3120
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Medicamentos"
      Height          =   375
      Index           =   1
      Left            =   10080
      TabIndex        =   23
      Top             =   2640
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "En&viar"
      Height          =   375
      Index           =   0
      Left            =   10080
      TabIndex        =   25
      Top             =   960
      Width           =   1575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Enviar"
      Height          =   1095
      Left            =   9960
      TabIndex        =   43
      Top             =   600
      Width           =   1815
      Begin VB.OptionButton Option2 
         Caption         =   "E-mail"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   720
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Correo/Fax"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   240
         Value           =   -1  'True
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4065
      Index           =   1
      Left            =   0
      TabIndex        =   31
      Top             =   3720
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   3540
         Index           =   1
         Left            =   120
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   360
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   6244
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0501.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(11)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(12)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(13)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(14)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(15)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(3)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(9)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(11)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(12)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(13)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(4)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(10)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(14)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(15)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0501.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR67CODPETOFERT"
            Height          =   330
            HelpContextID   =   40101
            Index           =   15
            Left            =   1680
            TabIndex        =   19
            Tag             =   "C�d.Petici�n Oferta"
            Top             =   3000
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR29CODDETPETOFERT"
            Height          =   330
            HelpContextID   =   40101
            Index           =   14
            Left            =   120
            TabIndex        =   18
            Tag             =   "C�d.Det.Pet.Oferta"
            Top             =   3000
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   10
            Left            =   6240
            TabIndex        =   11
            Tag             =   "Forma Farmace�tica"
            Top             =   360
            Width           =   465
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR29OBSERV"
            Height          =   930
            HelpContextID   =   30104
            Index           =   4
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   17
            Tag             =   "Observaciones"
            Top             =   1800
            Width           =   10890
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   7680
            TabIndex        =   14
            Tag             =   "Referencia"
            Top             =   360
            Width           =   3330
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR93CODUNIMEDIDA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   12
            Left            =   7200
            TabIndex        =   13
            Tag             =   "Unidad Medida"
            Top             =   360
            Width           =   465
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   11
            Left            =   6720
            TabIndex        =   12
            Tag             =   "Dosis"
            Top             =   360
            Width           =   465
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   1560
            Locked          =   -1  'True
            TabIndex        =   10
            Tag             =   "Producto"
            Top             =   360
            Width           =   4650
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   120
            TabIndex        =   9
            Tag             =   "C�d.Producto"
            Top             =   360
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR29PRECIO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   1560
            TabIndex        =   16
            Tag             =   "Precio Producto"
            Top             =   1080
            Width           =   1260
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR29CANTPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   120
            TabIndex        =   15
            Tag             =   "Cantidad Producto"
            Top             =   1080
            Width           =   1410
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3120
            Index           =   1
            Left            =   -74880
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   240
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   5503
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR29PLAZOENTREGA"
            Height          =   330
            Index           =   0
            Left            =   3120
            TabIndex        =   20
            Tag             =   "Plazo Entrega"
            Top             =   3000
            Visible         =   0   'False
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   7680
            TabIndex        =   48
            Top             =   120
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   7200
            TabIndex        =   47
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   6720
            TabIndex        =   46
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   6240
            TabIndex        =   45
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   1560
            TabIndex        =   44
            Top             =   120
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   36
            Top             =   120
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   1560
            TabIndex        =   35
            Top             =   840
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   34
            Top             =   840
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   33
            Top             =   1560
            Width           =   1275
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n de Oferta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3165
      Index           =   0
      Left            =   0
      TabIndex        =   28
      Top             =   480
      Width           =   9825
      Begin TabDlg.SSTab tabTab1 
         Height          =   2535
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   360
         Width           =   9570
         _ExtentX        =   16880
         _ExtentY        =   4471
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0501.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(10)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(5)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(7)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(8)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(16)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(17)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(18)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0501.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR67PATH"
            Height          =   330
            HelpContextID   =   30101
            Index           =   18
            Left            =   7560
            TabIndex        =   7
            Tag             =   "Path"
            Top             =   840
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR67NUMREP"
            Height          =   330
            HelpContextID   =   30101
            Index           =   17
            Left            =   6840
            TabIndex        =   6
            Tag             =   "Representante 1,2 � 3"
            Top             =   840
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR99CODESTOFERTPROV"
            Height          =   330
            HelpContextID   =   30101
            Index           =   16
            Left            =   6120
            TabIndex        =   5
            Tag             =   "C�digo Estado Oferta"
            Top             =   840
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR67DESOFERTA"
            Height          =   930
            HelpContextID   =   30104
            Index           =   8
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   8
            Tag             =   "Descripci�n Oferta"
            Top             =   1440
            Width           =   8970
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   120
            TabIndex        =   4
            Tag             =   "Representante"
            Top             =   840
            Width           =   5610
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   4440
            Locked          =   -1  'True
            TabIndex        =   3
            Tag             =   "Proveedor"
            Top             =   300
            Width           =   4650
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   3360
            TabIndex        =   2
            Tag             =   "C�d.Proveedor"
            Top             =   300
            Width           =   945
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR67CODPETOFERT"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�d.Petici�n Oferta"
            Top             =   300
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2250
            Index           =   0
            Left            =   -74910
            TabIndex        =   27
            Top             =   90
            Width           =   9045
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15954
            _ExtentY        =   3969
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR67FECPETOFERT"
            Height          =   330
            Index           =   1
            Left            =   1440
            TabIndex        =   1
            Tag             =   "Fecha Petici�n Oferta"
            Top             =   300
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   42
            Top             =   1200
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Representante"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   41
            Top             =   600
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   40
            Top             =   75
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Petici�n Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1440
            TabIndex        =   39
            Top             =   75
            Width           =   1875
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   3360
            TabIndex        =   38
            Top             =   60
            Width           =   885
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   32
      Top             =   8055
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Buscadores"
      Height          =   1215
      Left            =   9960
      TabIndex        =   49
      Top             =   2400
      Width           =   1815
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmGenPetOfe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0501.FRM                                                   *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA:                                                               *
'* DESCRIPCION: Generar Petici�n Oferta                                 *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim blnTecl As Boolean
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  strWhere = "{FR7900.FR79CODPROVEEDOR} = {FR6700.FR79CODPROVEEDOR} AND " & _
              "{FR6700.FR67CODPETOFERT} = {FR2900.FR67CODPETOFERT} AND " & _
              "{FR2900.FR73CODPRODUCTO} = {FR7300.FR73CODPRODUCTO} AND " & _
              "{FR6700.FR67CODPETOFERT} = " & txtText1(6).Text
    
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp4
  CrystalReport1.Action = 1
Err_imp4:

End Sub

'
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm
Dim strKey As String
Dim StrSelect As String
  

  Set objWinInfo = New clsCWWin
  
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Petici�n de Oferta"
      
    .strTable = "FR6700"
    '.intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("FR67CODPETOFERT", cwAscending)
   
    strKey = .strDataBase & .strTable
    .blnAskPrimary = False
    .strWhere = " FR99CODESTOFERTPROV=0 "
    
    Call .FormCreateFilterWhere(strKey, "Petici�n de Oferta")
    Call .FormAddFilterWhere(strKey, "FR67CODPETOFERT", "C�digo Petici�n Oferta", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR67DESOFERTA", "Descripci�n Oferta", cwString)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR67NUMREP", "Representante 1,2 � 3", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR67FECPETOFERT", "Fecha Petici�n Oferta", cwDate)
    Call .FormAddFilterWhere(strKey, "FR99CODESTOFERTPROV", "C�digo Estado Oferta", cwNumeric)
        
    Call .FormAddFilterOrder(strKey, "FR67CODPETOFERT", "C�digo Petici�n Oferta")
    
  End With
  
  With objDetailInfo
    .strName = "Detalle Petici�n de Oferta"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FR2900"
    
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR67CODPETOFERT", cwAscending)
    Call .FormAddOrderField("FR29CODDETPETOFERT", cwAscending)
    
    Call .FormAddRelation("FR67CODPETOFERT", txtText1(6))

 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Petici�n de Oferta")
    Call .FormAddFilterWhere(strKey, "FR29CODDETPETOFERT", "C�digo Detalle Petici�n Oferta", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "FR29CANTPRODUCTO", "Cantidad Producto", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR29PRECIO", "Precio Producto", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR29PLAZOENTREGA", "Plazo Entrega Producto", cwDate)
    Call .FormAddFilterWhere(strKey, "FR29OBSERV", "Observaciones", cwString)
    'FR67CODPETOFERT:C�digo Petici�n Oferta
    
    Call .FormAddFilterOrder(strKey, "FR29CODDETPETOFERT", "C�digo Detalle Petici�n Oferta")
  End With

  With objWinInfo
   
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    '.CtrlGetInfo(txtText1(1)).blnInFind = True
    '.CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
  
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(5), "FR79PROVEEDOR")
      
'    StrSelect = "(SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'1' FR67NUMREP,FR79PROVEEDOR PROVEEDOR FROM FR7900 UNION"
'    StrSelect = StrSelect & " SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'2' FR67NUMREP,FR79REPRESENT2 PROVEEDOR FROM FR7900 WHERE FR79REPRESENT2 IS NOT NULL UNION"
'    StrSelect = StrSelect & " SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'3' FR67NUMREP,FR79REPRESENT3 PROVEEDOR FROM FR7900 WHERE FR79REPRESENT3 IS NOT NULL)"
'    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR79CODPROVEEDOR", "SELECT * FROM " & StrSelect & " WHERE FR79CODPROVEEDOR =? AND FR67NUMREP=? ")
'    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(7), "PROVEEDOR")
    StrSelect = "(SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'1' FR67NUMREP,FR79PROVEEDOR PROVEEDOR FROM FR7900 UNION"
    StrSelect = StrSelect & " SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'2' FR67NUMREP,FR79REPRESENT2 PROVEEDOR FROM FR7900 WHERE FR79REPRESENT2 IS NOT NULL UNION"
    StrSelect = StrSelect & " SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'3' FR67NUMREP,FR79REPRESENT3 PROVEEDOR FROM FR7900 WHERE FR79REPRESENT3 IS NOT NULL)"
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(17)), "FR67NUMREP", "SELECT * FROM " & StrSelect & " WHERE FR67NUMREP=? AND FR79CODPROVEEDOR =? ")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(17)), txtText1(7), "PROVEEDOR")
    
    .CtrlGetInfo(txtText1(0)).blnForeign = True
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(9), "FR73DESPRODUCTOPROV")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(10), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(12), "FR93CODUNIMEDIDA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(13), "FR73REFERENCIA")
    
    Call .WinRegister
    Call .WinStabilize
  End With
 
 
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

'Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'Dim strSQL As String
'Dim rstSQL As rdoResultset
'
'  If txtText1(1).Text <> "" Then
'    Select Case txtText1(17).Text
'    Case 1
'      'Call objWinInfo.CtrlSet(txtText1(17), 1)
'      strSQL = "SELECT FR79PROVEEDOR FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
'      Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
'      Call objWinInfo.CtrlSet(txtText1(7), rstSQL(0).Value)
'      rstSQL.Close
'      Set rstSQL = Nothing
'    Case 2
'      'Call objWinInfo.CtrlSet(txtText1(17), 1)
'      strSQL = "SELECT FR79REPRESENT2 FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
'      Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
'      Call objWinInfo.CtrlSet(txtText1(7), rstSQL(0).Value)
'      rstSQL.Close
'      Set rstSQL = Nothing
'    Case 3
'      'Call objWinInfo.CtrlSet(txtText1(17), 1)
'      strSQL = "SELECT FR79REPRESENT3 FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
'      Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
'      Call objWinInfo.CtrlSet(txtText1(7), rstSQL(0).Value)
'      rstSQL.Close
'      Set rstSQL = Nothing
'    End Select
'  Else
'    'Call objWinInfo.CtrlSet(txtText1(17), "")
'    Call objWinInfo.CtrlSet(txtText1(7), "")
'  End If
'
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Departamentos Realizadores" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim StrSelect As String

  If strFormName = "Petici�n de Oferta" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch

blnTecl = False
      
      StrSelect = "(SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'1' NUMREPR,FR79PROVEEDOR PROVEEDOR FROM FR7900 UNION"
      StrSelect = StrSelect & " SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'2' NUMREPR,FR79REPRESENT2 PROVEEDOR FROM FR7900 WHERE FR79REPRESENT2 IS NOT NULL UNION"
      StrSelect = StrSelect & " SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'3' NUMREPR,FR79REPRESENT3 PROVEEDOR FROM FR7900 WHERE FR79REPRESENT3 IS NOT NULL)"
     
     .strTable = StrSelect
     '.strWhere = "WHERE FR73FECFINVIG IS NULL" 'AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     .strOrder = "ORDER BY FR79CODPROVEEDOR ASC"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo del Proveedor"
     objField.blnInGrid = False

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Proveedor"

     Set objField = .AddField("PROVEEDOR")
     objField.strSmallDesc = "Representante"
     
     Set objField = .AddField("NUMREPR")
     objField.strSmallDesc = "Num.Representante"
     objField.blnInGrid = False

     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("ad02coddpto"))
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR79CODPROVEEDOR"))
      Call objWinInfo.CtrlSet(txtText1(17), .cllValues("NUMREPR"))
      Call objWinInfo.CtrlSet(txtText1(7), .cllValues("PROVEEDOR"))
     End If
   End With
  End If
  
    
    
    
  If strFormName = "Detalle Petici�n de Oferta" And strCtrl = "txtText1(0)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      
     .strTable = "FR7300"
     .strWhere = "WHERE FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"

     Set objField = .AddField("FR73DESPRODUCTOPROV")
     objField.strSmallDesc = "Producto"

     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad Medida"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(0), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
  End If
  
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
    
  'para comprobar que se trata del control en cuestion
  'If strFormName = "Petici�n de Oferta" And strCtrlName = "txtText1(1)" Then
  If strFormName = "Petici�n de Oferta" And strCtrlName = "txtText1(17)" Then
    'aValues(2) = txtText1(17)
    aValues(2) = txtText1(1)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim strSEQ As String
Dim rstSEQ As rdoResultset

  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index = 2 Then
    If objWinInfo.objWinActiveForm.strName = "Petici�n de Oferta" Then
      strSEQ = "SELECT FR67CODPETOFERT_SEQUENCE.nextval FROM dual"
      Set rstSEQ = objApp.rdoConnect.OpenResultset(strSEQ)
      Call objWinInfo.CtrlSet(txtText1(6), rstSEQ(0).Value)
      rstSEQ.Close
      Set rstSEQ = Nothing
      Call objWinInfo.CtrlSet(txtText1(16), 0) 'FR99CODESTOFERTPROV
    Else
      strSEQ = "SELECT FR29CODDETPETOFERT_SEQUENCE.nextval FROM dual"
      Set rstSEQ = objApp.rdoConnect.OpenResultset(strSEQ)
      Call objWinInfo.CtrlSet(txtText1(14), rstSEQ(0).Value)
      rstSEQ.Close
      Set rstSEQ = Nothing
    End If
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  If intIndex = 1 Then
    blnTecl = True
  End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim strSQL As String
  Dim rstSQL As rdoResultset
  Dim qryFR79 As rdoQuery
  
  Call objWinInfo.CtrlDataChange
  If intIndex = 1 Then
    If blnTecl = True Then
      If txtText1(1).Text <> "" Then
        Call objWinInfo.CtrlSet(txtText1(17), 1)
        strSQL = "SELECT FR79PROVEEDOR FROM FR7900 WHERE FR79CODPROVEEDOR = ? "
        Set qryFR79 = objApp.rdoConnect.CreateQuery("", strSQL)
        qryFR79(0) = txtText1(1).Text
        Set rstSQL = qryFR79.OpenResultset()
        If rstSQL.EOF = False Then
          Call objWinInfo.CtrlSet(txtText1(7), rstSQL(0).Value)
        End If
        qryFR79.Close
        Set qryFR79 = Nothing
        Set rstSQL = Nothing
      Else
        Call objWinInfo.CtrlSet(txtText1(17), "")
        Call objWinInfo.CtrlSet(txtText1(7), "")
      End If
    End If
  End If
End Sub

Private Sub Command1_Click(Index As Integer)
  Dim strSQL As String
  Dim rstSQL As rdoResultset
  Dim qryFR79 As rdoQuery
  
  Command1(Index).Enabled = False
  
  If txtText1(6).Text = "" Then
    Select Case Index
      Case 0 'Enviar
        MsgBox " Debe de tener una Petici�n de Oferta redactada y guardada para poder ENVIARLA ", vbInformation, "Aviso"
      Case 1, 2
        MsgBox " Debe de tener una Petici�n de Oferta redactada y guardada para poder A�ADIR PRODUCTOS ", vbInformation, "Aviso"
    End Select
    Command1(Index).Enabled = True
    Exit Sub
  End If
  If tlbToolbar1.Buttons(4).Enabled = True And objWinInfo.objWinActiveForm.strName = "Petici�n de Oferta" Then
    Select Case Index
      Case 0 'Enviar
        MsgBox " Debe de tener una Petici�n de Oferta redactada y guardada para poder ENVIARLA ", vbInformation, "Aviso"
      Case 1, 2
        MsgBox " Debe de tener una Petici�n de Oferta redactada y guardada para poder A�ADIR PRODUCTOS ", vbInformation, "Aviso"
    End Select
    Command1(Index).Enabled = True
    Exit Sub
  End If
  
  Select Case Index
    Case 0 'Enviar
      If Option1.Value Then
        'CORREO/FAX
        Call Imprimir("FR5011.RPT", 1)
      Else
        strSQL = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ? "
        Set qryFR79 = objApp.rdoConnect.CreateQuery("", strSQL)
        qryFR79(0) = txtText1(1).Text
        Set rstSQL = qryFR79.OpenResultset()
        Select Case txtText1(17).Text
        Case "1"
            If IsNull(rstSQL("FR79EMAIL1").Value) Then
                Call MsgBox("El proveedor no tiene Correo Electr�nico", vbInformation, "Aviso")
            Else
                frmMensajeCorreo.txtText1(0).Text = rstSQL("FR79EMAIL1").Value
                gstrLlamador = "FrmGenPetOfe"
                Call objsecurity.LaunchProcess("FR0533")
            End If
        Case "2"
            If IsNull(rstSQL("FR79EMAIL2").Value) Then
                Call MsgBox("El Delegado 1 no tiene Correo Electr�nico", vbInformation, "Aviso")
            Else
                frmMensajeCorreo.txtText1(0).Text = rstSQL("FR79EMAIL2").Value
                gstrLlamador = "FrmGenPetOfe"
                Call objsecurity.LaunchProcess("FR0533")
            End If
        Case "3"
            If IsNull(rstSQL("FR79EMAIL3").Value) Then
                Call MsgBox("El Delegado 2 no tiene Correo Electr�nico", vbInformation, "Aviso")
            Else
                frmMensajeCorreo.txtText1(0).Text = rstSQL("FR79EMAIL3").Value
                gstrLlamador = "FrmGenPetOfe"
                Call objsecurity.LaunchProcess("FR0533")
            End If
        End Select
        qryFR79.Close
        Set qryFR79 = Nothing
        Set rstSQL = Nothing
      End If
    
    Case 1 'Medicamentos
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      If tlbToolbar1.Buttons(4).Enabled = True Then
        If txtText1(0).Text & txtText1(2).Text & txtText1(3).Text & txtText1(4).Text = "" Then
'          objWinInfo.objWinActiveForm.blnChanged = False
        Else
          Select Case MsgBox(" �Desea salvar los cambios realizados? ", vbYesNoCancel + vbInformation, "Editar Solicitud Compra")
          Case vbYes
            If Not IsNumeric(txtText1(0).Text) Then
              If Not IsNumeric(txtText1(2).Text) Then
                MsgBox " El campo C�digo Producto es obligatorio " & Chr(13) & _
                       " El campo Cantidad Producto es obligatorio ", vbInformation, "Aviso"
                Call txtText1(0).SetFocus
                Command1(Index).Enabled = True
                Exit Sub
              Else
                MsgBox " El campo C�digo Producto es obligatorio ", vbInformation, "Aviso"
                Call txtText1(0).SetFocus
                Command1(Index).Enabled = True
                Exit Sub
              End If
            Else
              If Not IsNumeric(txtText1(2).Text) Then
                MsgBox " El campo Cantidad Producto es obligatorio ", vbInformation, "Aviso"
                Call txtText1(2).SetFocus
                Command1(Index).Enabled = True
                Exit Sub
              End If
            End If
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
          Case vbNo
            objWinInfo.objWinActiveForm.blnChanged = False
          Case vbCancel
            Command1(Index).Enabled = True
            Exit Sub
          End Select
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
        End If
      Else
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
      End If
      
      gstrLlamador = "FrmGenPetOfe"
      gintFR501Prov = txtText1(1).Text
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      Call objsecurity.LaunchProcess("FR0502")
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      If gintProdBuscado <> "" Then
        Call objWinInfo.CtrlSet(txtText1(0), gintProdBuscado)
        Call txtText1(2).SetFocus
      Else
        Call txtText1(0).SetFocus
      End If
      gstrLlamador = ""
      gintFR501Prov = ""
      gintProdBuscado = ""
      'Call FrmBusMed.Show(vbModal)
    
    Case 2 'Material Sanitario
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      If tlbToolbar1.Buttons(4).Enabled = True Then
        If txtText1(0).Text & txtText1(2).Text & txtText1(3).Text & txtText1(4).Text = "" Then
          'objWinInfo.objWinActiveForm.blnChanged = False
        Else
          Select Case MsgBox(" �Desea salvar los cambios realizados? ", vbYesNoCancel + vbInformation, "Editar Solicitud Compra")
          Case vbYes
            If txtText1(0).Text = "" Then
              If txtText1(2).Text = "" Then
                MsgBox " El campo C�digo Producto es obligatorio " & Chr(13) & _
                       " El campo Cantidad Producto es obligatorio ", vbInformation, "Aviso"
                Call txtText1(0).SetFocus
                Command1(Index).Enabled = True
                Exit Sub
              Else
                MsgBox " El campo C�digo Producto es obligatorio ", vbInformation, "Aviso"
                Call txtText1(0).SetFocus
                Command1(Index).Enabled = True
                Exit Sub
              End If
            Else
              If txtText1(2).Text = "" Then
                MsgBox " El campo Cantidad Producto es obligatorio ", vbInformation, "Aviso"
                Call txtText1(2).SetFocus
                Command1(Index).Enabled = True
                Exit Sub
              End If
            End If
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
          Case vbNo
            objWinInfo.objWinActiveForm.blnChanged = False
          Case vbCancel
            Command1(Index).Enabled = True
            Exit Sub
          End Select
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
        End If
      Else
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
      End If
      
      gstrLlamador = "FrmGenPetOfe"
      gintFR501Prov = txtText1(1).Text
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      Call objsecurity.LaunchProcess("FR0503")
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      'Call FrmBusMat.Show(vbModal)
      If gintProdBuscado <> "" Then
        Call objWinInfo.CtrlSet(txtText1(0), gintProdBuscado)
        Call txtText1(2).SetFocus
      Else
        Call txtText1(0).SetFocus
      End If
      gintFR501Prov = ""
      gstrLlamador = ""
      gintProdBuscado = ""
  
  End Select

  Command1(Index).Enabled = True

End Sub
