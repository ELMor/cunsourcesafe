VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmEstPedCom 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Estudiar Pedido de Compra"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0520.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   28
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4425
      Index           =   1
      Left            =   120
      TabIndex        =   26
      Top             =   3480
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   3900
         Index           =   1
         Left            =   120
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   360
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   6879
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0520.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(14)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(15)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(16)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(17)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(18)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(19)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(19)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(18)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(17)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(15)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(3)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(4)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(14)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(12)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(10)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(13)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(11)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "Frame3"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(0)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(21)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).ControlCount=   25
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0520.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   5520
            TabIndex        =   22
            Tag             =   "Moneda"
            Top             =   1080
            Width           =   705
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr73codproducto"
            Height          =   330
            HelpContextID   =   40102
            Index           =   0
            Left            =   120
            TabIndex        =   13
            Tag             =   "C�digo de Producto"
            Top             =   480
            Width           =   1455
         End
         Begin VB.Frame Frame3 
            BorderStyle     =   0  'None
            Height          =   615
            Left            =   120
            TabIndex        =   38
            Top             =   1680
            Width           =   2295
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8760
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Unidad de Medida"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Forma Farmaceutica"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   8160
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   9360
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Volumen"
            Top             =   480
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   1080
            Width           =   3225
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   1680
            TabIndex        =   14
            Tag             =   "Descripci�n Producto"
            Top             =   480
            Width           =   5850
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr25preciounidad"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   1440
            TabIndex        =   20
            Tag             =   "Precio Unidad"
            Top             =   1080
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25cantpedida"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   120
            TabIndex        =   19
            Tag             =   "Cantidad"
            Top             =   1080
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25imporlinea"
            Height          =   330
            HelpContextID   =   40102
            Index           =   15
            Left            =   3360
            TabIndex        =   21
            Tag             =   "Denominaci�n|Denominaci�n Departamento"
            Top             =   1080
            Width           =   2055
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "frh8moneda"
            Height          =   330
            HelpContextID   =   40102
            Index           =   17
            Left            =   240
            MaxLength       =   13
            TabIndex        =   41
            Tag             =   "Moneda"
            Top             =   1800
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62codpedcompra"
            Height          =   330
            HelpContextID   =   40102
            Index           =   18
            Left            =   960
            MaxLength       =   13
            TabIndex        =   40
            Tag             =   "C�dido Pedido"
            Top             =   1800
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25coddetpedcomp"
            Height          =   330
            HelpContextID   =   40102
            Index           =   19
            Left            =   1680
            MaxLength       =   13
            TabIndex        =   39
            Tag             =   "C�digo Detalle Pedido"
            Top             =   1800
            Width           =   495
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3480
            Index           =   1
            Left            =   -74880
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   240
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   6138
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   5520
            TabIndex        =   51
            Top             =   840
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   7560
            TabIndex        =   50
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   8160
            TabIndex        =   49
            Top             =   240
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   8760
            TabIndex        =   48
            Top             =   240
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   9360
            TabIndex        =   47
            Top             =   240
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   7560
            TabIndex        =   46
            Top             =   840
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   45
            Top             =   240
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Unidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1440
            TabIndex        =   44
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   43
            Top             =   840
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   3360
            TabIndex        =   42
            Top             =   840
            Width           =   645
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pedido de Compra"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   480
      Width           =   9465
      Begin TabDlg.SSTab tabTab1 
         Height          =   2415
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   360
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   4260
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0520.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(11)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(10)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(6)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(16)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(9)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(8)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(7)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(5)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(6)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(20)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "Frame2"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).ControlCount=   16
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0520.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Height          =   615
            Left            =   7320
            TabIndex        =   29
            Top             =   120
            Width           =   1455
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "sg02cod_pes"
            Height          =   330
            HelpContextID   =   40102
            Index           =   20
            Left            =   8040
            MaxLength       =   13
            TabIndex        =   31
            Tag             =   "C�digo Estudio"
            Top             =   240
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "fr62codpedcompra"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Pedido Compra"
            Top             =   360
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62portes"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   3480
            TabIndex        =   2
            Tag             =   "Portes"
            Top             =   360
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62recargo"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   4560
            TabIndex        =   3
            Tag             =   "Recargo Financiero"
            Top             =   360
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            DataField       =   "fr62descpersonalizada"
            Height          =   615
            Index           =   7
            Left            =   240
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   4
            Tag             =   "Descripci�n Personalizada"
            Top             =   960
            Width           =   8385
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr79codproveedor"
            Height          =   330
            HelpContextID   =   40102
            Index           =   8
            Left            =   240
            TabIndex        =   5
            Tag             =   "C�digo Proveedor"
            Top             =   1920
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   1320
            TabIndex        =   6
            Tag             =   "Descripci�n Proveedor"
            Top             =   1920
            Width           =   7290
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62indrepre"
            Height          =   330
            HelpContextID   =   40102
            Index           =   16
            Left            =   7440
            MaxLength       =   13
            TabIndex        =   30
            Tag             =   "Representante"
            Top             =   240
            Width           =   495
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2130
            Index           =   0
            Left            =   -74910
            TabIndex        =   7
            Top             =   90
            Width           =   8685
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15319
            _ExtentY        =   3757
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "fr62fecpedcompra"
            Height          =   330
            Index           =   1
            Left            =   1560
            TabIndex        =   1
            Tag             =   "Fecha Pedido"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   37
            Top             =   120
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Portes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   3480
            TabIndex        =   36
            Top             =   120
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Recargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   4560
            TabIndex        =   35
            Top             =   120
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1560
            TabIndex        =   34
            Top             =   120
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   240
            TabIndex        =   33
            Top             =   720
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   240
            TabIndex        =   32
            Top             =   1680
            Width           =   885
         End
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   9840
      TabIndex        =   12
      Top             =   1080
      Width           =   1815
      Begin VB.CommandButton cmdRechazar 
         Caption         =   "Rechazar"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   840
         Width           =   1575
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   1575
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   27
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmEstPedCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0520.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: ABRIL 1999                                                    *
'* DESCRIPCION: Estudiar Pedido de Compra                                *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1



Private Sub cmdAceptar_Click()
    'Dim strUpdate As String
    If txtText1(6).Text <> "" Then
        'strUpdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                    "FR95CODESTPEDCOMPRA=2 where fr62codpedcompra=" & txtText1(6).Text
        'objApp.rdoConnect.Execute strUpdate, 64
        If txtText1(8).Text <> "" Then
            gintProveedor(1) = txtText1(8).Text
            gintProveedor(2) = txtText1(6).Text
            gstrLlamador = "FrmEstPedCom"
            Call objsecurity.LaunchProcess("FR0524")
        Else
            Call MsgBox("No hay ning�n Proveedor", vbExclamation, "Aviso")
        End If
    End If
End Sub

Private Sub cmdRechazar_Click()
    Dim strupdate As String
    If txtText1(6).Text <> "" Then
        strupdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                    "FR95CODESTPEDCOMPRA=0 where fr62codpedcompra=" & txtText1(6).Text
        objApp.rdoConnect.Execute strupdate, 64
        
    End If
End Sub

'Private Sub cmdOfertas_Click()
'    Dim v As Integer
'
'    cmdOfertas.Enabled = False
'    If txtText1(19).Text <> "" Then
'      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'      Call objsecurity.LaunchProcess("FR0509")
'      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'      For v = 0 To gintprodtotal - 1
'        Call objWinInfo.CtrlSet(txtText1(0), gintprodbuscado(v, 0))
'      Next v
'      gintprodtotal = 0
'    End If
'    cmdOfertas.Enabled = True
'End Sub

'Private Sub cmdSolicitud_Click()
'    Dim v As Integer
'
'    cmdOfertas.Enabled = False
'    If txtText1(19).Text <> "" Then
'      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'      Call objsecurity.LaunchProcess("FR0519")
'      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'      For v = 0 To gintprodtotal - 1
'        Call objWinInfo.CtrlSet(txtText1(0), gintprodbuscado(v, 0))
'      Next v
'      gintprodtotal = 0
'    End If
'    cmdOfertas.Enabled = True
'End Sub

'Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
'Dim rsta As rdoResultset
'Dim sqlstr As String
'
'  If txtText1(16).Text <> "" Then
'    sqlstr = "SELECT * FROM AD0200 WHERE AD32CODTIPODPTO=3 AND AD02CODDPTO=" & txtText1(16).Text
'    sqlstr = sqlstr & " AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
'    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'    If rsta.EOF = True Then
'      MsgBox "El C�d.Servicio es incorrecto.", vbExclamation
'      blnCancel = True
'    End If
'    rsta.Close
'    Set rsta = Nothing
'  End If
'
'End Sub

'Private Sub cmdreceta_Click()
'    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'    objWinInfo.objWinActiveForm.intAllowance = cwAllowAdd
'    objWinInfo.DataNew
'    chkCheck1(1).Value = 1
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------


Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  

  Set objWinInfo = New clsCWWin
  
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Pedidos de Compra"
      
    .strTable = "FR6200"
    .strWhere = "fr95codestpedcompra=1"
    '.intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
   
    strKey = .strDataBase & .strTable
    .intAllowance = cwAllowModify
    
    Call .FormCreateFilterWhere(strKey, "Pedidos de Compra")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR62DESCPERSONALIZADA", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR62FECPEDCOMPRA", "Fecha Compra", cwDate)
    Call .FormAddFilterWhere(strKey, "FR62PORTES", "Portes", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR62RECARGO", "Recargo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "Proveedor", cwNumeric)
        
    Call .FormAddFilterOrder(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FR62DESCPERSONALIZADA", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "FR62FECPEDCOMPRA", "Fecha Compra")
    Call .FormAddFilterOrder(strKey, "FR62PORTES", "Portes")
    Call .FormAddFilterOrder(strKey, "FR62RECARGO", "Recargo")
    Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "Proveedor")
    
  End With
  With objDetailInfo
    .strName = "Detalle Petici�n"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FR2500"
    
    .intAllowance = cwAllowModify
    
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
    Call .FormAddOrderField("FR25CODDETPEDCOMP", cwAscending)
    
    Call .FormAddRelation("FR62CODPEDCOMPRA", txtText1(6))
    '.intAllowance = cwAllowModify
    'Call .objPrinter.Add("PR1281", "Listado por Departamentos con sus Actuaciones")
    
    '.blnHasMaint = True
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Pedido")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CODDETPEDCOMP", "C�digo Detalle Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CANTPEDIDA", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25IMPORLINEA", "Importe de Linea", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR50CODPRESENT", "C�digo Presentaci�n", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR65INDRECETA", "�Receta?", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR65CANTRECETA", "Cantidad Receta", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR34CODVIA", "C�digo V�a", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR65DOSIS", "D�sis", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FR25CODDETPEDCOMP", "C�digo Detalle Pedido")
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
End With
   
   
   With objWinInfo
   
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
'    Call .FormCreateInfo(objDetailInfo)
   
    '.CtrlGetInfo(txtText1(6)).blnInGrid = False
    '.CtrlGetInfo(txtText1(7)).blnInGrid = False
    
    '.CtrlGetInfo(chkCheck1(0)).blnInFind = False
    '.CtrlGetInfo(chkCheck1(1)).blnInFind = False
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    '.CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    '.CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
  
    .CtrlGetInfo(txtText1(8)).blnForeign = True
'    .CtrlGetInfo(txtText1(0)).blnForeign = True
    
    .CtrlGetInfo(txtText1(4)).blnReadOnly = True
    .CtrlGetInfo(txtText1(21)).blnReadOnly = True
    .CtrlGetInfo(txtText1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(2), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(13), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(10), "FR73DOSIS")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "FR93CODUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(12), "FR73VOLUMEN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(14), "FR73REFERENCIA")
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
'    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(4), "FR73PRECBASE")
    
    Call .WinRegister
    Call .WinStabilize
  End With
 
  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



'Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'    Dim strSelect As String
'    Dim rstSelect As rdoResultset
'    Dim strupdate As String
'
'    strSelect = "SELECT FR65INDCONSUMIDO WHERE FR65FECHA=" & dtcDateCombo1(0).Date & _
'                " AND PR65HORA=" & txtText1(5).Text & " AND FR73CODPRODUCTO=" & txtText1(0).Text & _
'                " AND CI21CODPERSONA=" & txtText1(6).Text
'    Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
'    If rstSelect(0).Value = 0 And chkCheck1(0).Value = True Then
'        strSelect = "SELECT FR04CODALMACEN,FR47ACUSALIDA FROM FR4700 WHERE FR73CODPRODUCTO=" & txtText1(0).Text & _
'                    " AND FR04CODALMACEN IN (SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=" & _
'                    txtText1(16).Text & ")"
'        Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
'        strupdate = "UPDATE FR4700 SET FR47ACUSALIDA=" & rstSelect(1).Value + txtText1(8).Text & _
'                    " WHERE FFR04CODALMACEN=" & rstSelect(0).Value & _
'                    " AND FR73CODPRODUCTO=" & txtText1(0).Text
'        objApp.rdoConnect.Execute strupdate, 64
'    Else
'        If rstSelect(0).Value = 1 And chkCheck1(0).Value = False Then
'            strSelect = "SELECT FR04CODALMACEN,FR47ACUSALIDA FROM FR4700 WHERE FR73CODPRODUCTO=" & txtText1(0).Text & _
'                        " AND FR04CODALMACEN IN (SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=" & _
'                        txtText1(16).Text & ")"
'            Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
'            strupdate = "UPDATE FR4700 SET FR47ACUSALIDA=" & rstSelect(1).Value - txtText1(8).Text & _
'                        " WHERE FFR04CODALMACEN=" & rstSelect(0).Value & _
'                        " AND FR73CODPRODUCTO=" & txtText1(0).Text
'            objApp.rdoConnect.Execute strupdate, 64
'        End If
'    End If
'    rstSelect.Close
'    Set rstSelect = Nothing
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Departamentos Realizadores" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

   Dim objField As clsCWFieldSearch
   
  ' Proveedor
  If strFormName = "Pedidos de Compra" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     .strOrder = "ORDER BY FR79CODPROVEEDOR ASC"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo del Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n del Proveedor "

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
  End If
  
'  ' Producto
'  If strFormName <> "Pedidos de Compra" And strCtrl = "txtText1(0)" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "FR7300"
'     .strOrder = "ORDER BY FR73DESPRODUCTO ASC"
'
'     Set objField = .AddField("FR73CODPRODUCTO")
'     objField.strSmallDesc = "C�digo del Producto"
'
'     Set objField = .AddField("FR73DESPRODUCTO")
'     objField.strSmallDesc = "Descripci�n del Producto"
'
'     If .Search Then
'      Call objWinInfo.CtrlSet(txtText1(0), .cllValues("FR73CODPRODUCTO"))
'     End If
'   End With
'  End If
   
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  '  Dim sqlstr As String
  '  Dim rsta As rdoResultset
  '  Dim linea As Integer
  '  Dim rstfec As rdoResultset
  '  Dim strfec As String
  '  Dim rstcontrolOP As rdoResultset
  '  Dim strcontrolOP As String
  'Select Case btnButton.Index
  '  Case 2 'Nuevo
  '      Select Case objWinInfo.objWinActiveForm.strName
  '          Case "Pedidos de Compra"
  '              Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  '              sqlstr = "SELECT FR62CODPEDCOMPRA_SEQUENCE.nextval FROM dual"
  '              Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  '              Call objWinInfo.CtrlSet(txtText1(6), rsta.rdoColumns(0).Value)
  '              rsta.Close
  '              Set rsta = Nothing
  '              strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
  '              Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
  '              Call objWinInfo.CtrlSet(dtcDateCombo1(1), rstfec.rdoColumns(0).Value)
  '              Call objWinInfo.CtrlSet(txtText1(16), 1)
  '              Call objWinInfo.CtrlSet(txtText1(20), objsecurity.strUser)
  '              rstfec.Close
  '              Set rstfec = Nothing
  '              Exit Sub
  '          Case "Detalle Petici�n"
  '              Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  '              sqlstr = "SELECT MAX(FR25CODDETPEDCOMP) FROM FR2500 WHERE FR62CODPEDCOMPRA=" & txtText1(6).Text
  '              Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  '              If IsNull(rsta.rdoColumns(0).Value) Then
  '                linea = 1
  '              Else
  '                linea = rsta.rdoColumns(0).Value + 1
  '              End If
  '              Call objWinInfo.CtrlSet(txtText1(19), linea)
  '              Call objWinInfo.CtrlSet(txtText1(17), 0)
  '              rsta.Close
  '              Set rsta = Nothing
  '      End Select
  '  Case Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  '  End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  '   Select Case intIndex
  '      'NUEVO
  '      Case 10:
  '          Call MsgBox("Para introducir una Receta pulse el bot�n Receta")
  '          Exit Sub
  '      'GUARDAR
  '      Case 40:
  '          objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
  '  End Select
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 4 Or intIndex = 3 Or intIndex = 15 Then
    If txtText1(4).Text <> "" And txtText1(3).Text <> "" Then
        txtText1(15).Text = CCur(txtText1(4).Text) * CCur(txtText1(3).Text)
    End If
  End If
End Sub






