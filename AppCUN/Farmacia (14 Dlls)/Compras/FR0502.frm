VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmBusMed 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Buscar Productos"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11685
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0502.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   50
      Top             =   0
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdtraer 
      Caption         =   "&Traer Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   9600
      TabIndex        =   162
      Top             =   6600
      Width           =   1935
   End
   Begin VB.Frame Frame4 
      Caption         =   "Productos Seleccionados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   2175
      Left            =   0
      TabIndex        =   160
      Top             =   5760
      Width           =   9375
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Height          =   1695
         Left            =   120
         TabIndex        =   161
         Top             =   360
         Width           =   9135
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   2
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4207
         Columns(1).Caption=   "Producto"
         Columns(1).Name =   "Medicamento"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   16113
         _ExtentY        =   2990
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3570
      Index           =   0
      Left            =   0
      TabIndex        =   58
      Top             =   2090
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   3105
         Index           =   0
         Left            =   120
         TabIndex        =   59
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   5477
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0502.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "SSTab3"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0502.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "cmda�adir"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).ControlCount=   2
         Begin VB.CommandButton cmda�adir 
            Caption         =   "A�adir"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   10080
            Style           =   1  'Graphical
            TabIndex        =   163
            Top             =   1200
            Width           =   855
         End
         Begin TabDlg.SSTab SSTab3 
            Height          =   2895
            Left            =   -74880
            TabIndex        =   62
            Top             =   120
            Width           =   10845
            _ExtentX        =   19129
            _ExtentY        =   5106
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Informaci�n General"
            TabPicture(0)   =   "FR0502.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(63)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(62)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(61)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(60)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(15)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(2)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(14)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(59)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(0)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(58)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(57)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(56)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(12)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "lblLabel1(11)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "lblLabel1(6)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "lblLabel1(5)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "lblLabel1(28)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "lblLabel1(27)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "lblLabel1(26)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "lblLabel1(25)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "lblLabel1(24)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "lblLabel1(23)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "lblLabel1(22)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "lblLabel1(21)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "lblLabel1(20)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "lblLabel1(18)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "lblLabel1(17)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "lblLabel1(16)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "lblLabel1(4)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).Control(29)=   "txtText1(35)"
            Tab(0).Control(29).Enabled=   0   'False
            Tab(0).Control(30)=   "txtText1(34)"
            Tab(0).Control(30).Enabled=   0   'False
            Tab(0).Control(31)=   "txtText1(33)"
            Tab(0).Control(31).Enabled=   0   'False
            Tab(0).Control(32)=   "txtText1(32)"
            Tab(0).Control(32).Enabled=   0   'False
            Tab(0).Control(33)=   "txtText1(7)"
            Tab(0).Control(33).Enabled=   0   'False
            Tab(0).Control(34)=   "txtText1(24)"
            Tab(0).Control(34).Enabled=   0   'False
            Tab(0).Control(35)=   "txtText1(4)"
            Tab(0).Control(35).Enabled=   0   'False
            Tab(0).Control(36)=   "txtText1(31)"
            Tab(0).Control(36).Enabled=   0   'False
            Tab(0).Control(37)=   "txtText1(5)"
            Tab(0).Control(37).Enabled=   0   'False
            Tab(0).Control(38)=   "txtText1(30)"
            Tab(0).Control(38).Enabled=   0   'False
            Tab(0).Control(39)=   "txtText1(17)"
            Tab(0).Control(39).Enabled=   0   'False
            Tab(0).Control(40)=   "txtText1(9)"
            Tab(0).Control(40).Enabled=   0   'False
            Tab(0).Control(41)=   "txtText1(8)"
            Tab(0).Control(41).Enabled=   0   'False
            Tab(0).Control(42)=   "txtText1(10)"
            Tab(0).Control(42).Enabled=   0   'False
            Tab(0).Control(43)=   "txtText1(6)"
            Tab(0).Control(43).Enabled=   0   'False
            Tab(0).Control(44)=   "txtText1(11)"
            Tab(0).Control(44).Enabled=   0   'False
            Tab(0).Control(45)=   "txtText1(0)"
            Tab(0).Control(45).Enabled=   0   'False
            Tab(0).Control(46)=   "txtText1(27)"
            Tab(0).Control(46).Enabled=   0   'False
            Tab(0).Control(47)=   "txtText1(26)"
            Tab(0).Control(47).Enabled=   0   'False
            Tab(0).Control(48)=   "txtText1(25)"
            Tab(0).Control(48).Enabled=   0   'False
            Tab(0).Control(49)=   "txtText1(23)"
            Tab(0).Control(49).Enabled=   0   'False
            Tab(0).Control(50)=   "txtText1(22)"
            Tab(0).Control(50).Enabled=   0   'False
            Tab(0).Control(51)=   "txtText1(21)"
            Tab(0).Control(51).Enabled=   0   'False
            Tab(0).Control(52)=   "txtText1(20)"
            Tab(0).Control(52).Enabled=   0   'False
            Tab(0).Control(53)=   "txtText1(19)"
            Tab(0).Control(53).Enabled=   0   'False
            Tab(0).Control(54)=   "txtText1(18)"
            Tab(0).Control(54).Enabled=   0   'False
            Tab(0).Control(55)=   "txtText1(16)"
            Tab(0).Control(55).Enabled=   0   'False
            Tab(0).Control(56)=   "txtText1(14)"
            Tab(0).Control(56).Enabled=   0   'False
            Tab(0).Control(57)=   "txtText1(13)"
            Tab(0).Control(57).Enabled=   0   'False
            Tab(0).Control(58)=   "txtText1(12)"
            Tab(0).Control(58).Enabled=   0   'False
            Tab(0).Control(59)=   "chkCheck1(0)"
            Tab(0).Control(59).Enabled=   0   'False
            Tab(0).Control(60)=   "Frame5"
            Tab(0).Control(60).Enabled=   0   'False
            Tab(0).ControlCount=   61
            TabCaption(1)   =   "Informaci�n Proveedor"
            TabPicture(1)   =   "FR0502.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "SSTab1"
            Tab(1).ControlCount=   1
            Begin VB.Frame Frame5 
               BorderStyle     =   0  'None
               Height          =   375
               Left            =   9240
               TabIndex        =   164
               Top             =   1200
               Width           =   975
            End
            Begin VB.CheckBox chkCheck1 
               BackColor       =   &H00C0C0C0&
               Caption         =   "Cons.Reg."
               DataField       =   "FR73INDCONSREGULAR"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   4440
               TabIndex        =   25
               TabStop         =   0   'False
               Tag             =   "Consumo Regular"
               Top             =   2400
               Width           =   1305
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73DESCUENTO"
               Height          =   330
               HelpContextID   =   30104
               Index           =   12
               Left            =   7560
               TabIndex        =   20
               TabStop         =   0   'False
               Tag             =   "%Desc|Tanto por ciento de Descuento"
               Top             =   1200
               Width           =   930
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CONSDIARIO"
               Height          =   330
               HelpContextID   =   30104
               Index           =   13
               Left            =   3000
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Cons.diario|Consumo Diario"
               Top             =   1200
               Width           =   1290
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CONSMES"
               Height          =   330
               HelpContextID   =   30104
               Index           =   14
               Left            =   3000
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "Cons.Mens|Consumo mensual"
               Top             =   1800
               Width           =   1290
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CONSANUAL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   16
               Left            =   3000
               TabIndex        =   24
               TabStop         =   0   'False
               Tag             =   "Cons.Anual|Consumo Anual"
               Top             =   2400
               Width           =   1290
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73STOCKMIN"
               Height          =   330
               HelpContextID   =   30104
               Index           =   18
               Left            =   120
               TabIndex        =   14
               TabStop         =   0   'False
               Tag             =   "Stock Min.|Stock m�nimo"
               Top             =   1800
               Width           =   1050
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73PTOPED"
               Height          =   330
               HelpContextID   =   30104
               Index           =   19
               Left            =   120
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "Pto.Pedido|Punto de Pedido"
               Top             =   2400
               Width           =   1050
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73EXISTENCIAS"
               Height          =   330
               HelpContextID   =   30104
               Index           =   20
               Left            =   120
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Exist.|Existencias"
               Top             =   1200
               Width           =   1050
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73RAPPEL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   21
               Left            =   7560
               TabIndex        =   27
               TabStop         =   0   'False
               Tag             =   "%Rappel|Tanto por ciento de Rappel"
               Top             =   2400
               Width           =   930
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73DIASSEG"
               Height          =   330
               HelpContextID   =   30104
               Index           =   22
               Left            =   4560
               TabIndex        =   19
               TabStop         =   0   'False
               Tag             =   "D�as Seg|D�as de Seguridad"
               Top             =   1200
               Width           =   705
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73TAMPEDI"
               Height          =   330
               HelpContextID   =   30104
               Index           =   23
               Left            =   4560
               TabIndex        =   21
               TabStop         =   0   'False
               Tag             =   "Tam.Pedido|Tama�o del pedido en d�as"
               Top             =   1800
               Width           =   945
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73PRECOFR"
               Height          =   330
               HelpContextID   =   30104
               Index           =   25
               Left            =   5880
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "Prec.Oferta|Precio Oferta de Compra"
               Top             =   1800
               Width           =   1530
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73BONIFICACION"
               Height          =   330
               HelpContextID   =   30104
               Index           =   26
               Left            =   7560
               TabIndex        =   23
               TabStop         =   0   'False
               Tag             =   "%Bonif|Tanto por ciento de Bonificaci�n"
               Top             =   1800
               Width           =   930
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FRH8MONEDA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   27
               Left            =   9840
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "Mon.|Moneda"
               Top             =   600
               Width           =   810
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H0000FFFF&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               HelpContextID   =   30104
               Index           =   0
               Left            =   9360
               TabIndex        =   0
               Tag             =   "C�d.Producto"
               Top             =   1200
               Width           =   570
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   11
               Left            =   7320
               Locked          =   -1  'True
               TabIndex        =   12
               TabStop         =   0   'False
               Tag             =   "U.M.|Unidad de Medida"
               Top             =   600
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FRH7CODFORMFAR"
               Height          =   330
               HelpContextID   =   30104
               Index           =   6
               Left            =   5760
               Locked          =   -1  'True
               TabIndex        =   10
               TabStop         =   0   'False
               Tag             =   "F.F.|Forma Farmac�utica"
               Top             =   600
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73DOSIS"
               Height          =   330
               HelpContextID   =   30104
               Index           =   10
               Left            =   6480
               Locked          =   -1  'True
               TabIndex        =   11
               TabStop         =   0   'False
               Tag             =   "Dosis"
               Top             =   600
               Width           =   720
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73VOLUMEN"
               Height          =   330
               HelpContextID   =   30104
               Index           =   8
               Left            =   8040
               Locked          =   -1  'True
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "Vol.|Volumen mL"
               Top             =   600
               Width           =   700
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CODINTFARSEG"
               Height          =   330
               HelpContextID   =   30104
               Index           =   9
               Left            =   840
               Locked          =   -1  'True
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "Seg"
               Top             =   600
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CODINTFAR"
               Height          =   330
               HelpContextID   =   30104
               Index           =   17
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   1
               TabStop         =   0   'False
               Tag             =   "C�digo"
               Top             =   600
               Width           =   720
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73PVL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   30
               Left            =   5880
               TabIndex        =   26
               TabStop         =   0   'False
               Tag             =   "PVL"
               Top             =   2400
               Width           =   1530
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73DESPRODUCTOPROV"
               Height          =   330
               HelpContextID   =   30104
               Index           =   5
               Left            =   1245
               Locked          =   -1  'True
               TabIndex        =   2
               TabStop         =   0   'False
               Tag             =   "Producto"
               Top             =   600
               Width           =   2940
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73TAMENVASE"
               Height          =   330
               HelpContextID   =   30104
               Index           =   31
               Left            =   8880
               Locked          =   -1  'True
               TabIndex        =   4
               TabStop         =   0   'False
               Tag             =   "U.E.|Unidades Envase"
               Top             =   600
               Width           =   900
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CANTPEND"
               Height          =   330
               HelpContextID   =   30104
               Index           =   4
               Left            =   1560
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Pdte.Rec|Cantidad pendiente de recibir"
               Top             =   1200
               Width           =   1170
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73PREULTENT"
               Height          =   330
               HelpContextID   =   30104
               Index           =   24
               Left            =   5880
               TabIndex        =   17
               TabStop         =   0   'False
               Tag             =   "Prec.Ult.Ent|Precio de la �ltima entrada"
               Top             =   1200
               Width           =   1530
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73PTEBONIF"
               Height          =   330
               HelpContextID   =   30104
               Index           =   7
               Left            =   1560
               TabIndex        =   16
               TabStop         =   0   'False
               Tag             =   "Pdte.Bonif|Pendiente Bonificar"
               Top             =   1800
               Width           =   1050
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73PRECIONETCOMPRA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   32
               Left            =   9120
               TabIndex        =   29
               TabStop         =   0   'False
               Tag             =   "Prec.Neto|Precio Neto"
               Top             =   2400
               Width           =   1410
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR88CODTIPIVA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   33
               Left            =   8640
               TabIndex        =   28
               TabStop         =   0   'False
               Tag             =   "IVA"
               Top             =   2400
               Width           =   330
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73REFERENCIA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   34
               Left            =   4320
               Locked          =   -1  'True
               TabIndex        =   3
               TabStop         =   0   'False
               Tag             =   "Referencia"
               Top             =   600
               Width           =   1380
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73DESVIACION"
               Height          =   330
               HelpContextID   =   30104
               Index           =   35
               Left            =   1560
               TabIndex        =   130
               TabStop         =   0   'False
               Tag             =   "Desviaci�n"
               Top             =   2400
               Width           =   1050
            End
            Begin TabDlg.SSTab SSTab1 
               Height          =   2175
               Left            =   -74880
               TabIndex        =   63
               Top             =   480
               Width           =   10575
               _ExtentX        =   18653
               _ExtentY        =   3836
               _Version        =   327681
               Style           =   1
               Tabs            =   2
               TabHeight       =   520
               TabCaption(0)   =   "Proveedores"
               TabPicture(0)   =   "FR0502.frx":007C
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "Frame3"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).ControlCount=   1
               TabCaption(1)   =   "Informaci�n Adicional"
               TabPicture(1)   =   "FR0502.frx":0098
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "SSTab2"
               Tab(1).ControlCount=   1
               Begin VB.Frame Frame3 
                  Caption         =   "Producto de"
                  ForeColor       =   &H00FF0000&
                  Height          =   1575
                  Left            =   120
                  TabIndex        =   64
                  Top             =   480
                  Width           =   9735
                  Begin VB.TextBox txtText1 
                     BackColor       =   &H00C0C0C0&
                     DataField       =   "FR79CODPROVEEDOR_B"
                     Height          =   330
                     HelpContextID   =   30104
                     Index           =   2
                     Left            =   360
                     TabIndex        =   34
                     TabStop         =   0   'False
                     Tag             =   "C�d.Prov.B"
                     Top             =   960
                     Width           =   930
                  End
                  Begin VB.TextBox txtText1 
                     BackColor       =   &H00C0C0C0&
                     DataField       =   "FR79CODPROVEEDOR_A"
                     Height          =   330
                     HelpContextID   =   30104
                     Index           =   3
                     Left            =   360
                     TabIndex        =   30
                     TabStop         =   0   'False
                     Tag             =   "C�d.Prov.A"
                     Top             =   240
                     Width           =   930
                  End
                  Begin VB.TextBox txtText1 
                     BackColor       =   &H00C0C0C0&
                     DataField       =   "FR79CODPROVEEDOR_C"
                     Height          =   330
                     HelpContextID   =   30104
                     Index           =   1
                     Left            =   360
                     TabIndex        =   32
                     TabStop         =   0   'False
                     Tag             =   "C�d.Prov.C"
                     Top             =   600
                     Visible         =   0   'False
                     Width           =   930
                  End
                  Begin VB.TextBox txtText1 
                     BackColor       =   &H00C0C0C0&
                     Height          =   330
                     HelpContextID   =   30104
                     Index           =   29
                     Left            =   1320
                     TabIndex        =   35
                     TabStop         =   0   'False
                     Tag             =   "Proveedor C"
                     Top             =   960
                     Visible         =   0   'False
                     Width           =   4770
                  End
                  Begin VB.TextBox txtText1 
                     BackColor       =   &H00C0C0C0&
                     Height          =   330
                     HelpContextID   =   30104
                     Index           =   28
                     Left            =   1320
                     TabIndex        =   33
                     TabStop         =   0   'False
                     Tag             =   "Proveedor B"
                     Top             =   600
                     Width           =   4770
                  End
                  Begin VB.TextBox txtText1 
                     BackColor       =   &H00C0C0C0&
                     Height          =   330
                     HelpContextID   =   30104
                     Index           =   15
                     Left            =   1320
                     TabIndex        =   31
                     TabStop         =   0   'False
                     Tag             =   "Proveedor A"
                     Top             =   240
                     Width           =   4770
                  End
                  Begin VB.OptionButton Option1 
                     Caption         =   "Option1"
                     Height          =   255
                     Index           =   2
                     Left            =   120
                     TabIndex        =   68
                     Top             =   960
                     Visible         =   0   'False
                     Width           =   255
                  End
                  Begin VB.OptionButton Option1 
                     Caption         =   "Option1"
                     Height          =   255
                     Index           =   1
                     Left            =   120
                     TabIndex        =   67
                     Top             =   600
                     Width           =   255
                  End
                  Begin VB.OptionButton Option1 
                     Caption         =   "Option1"
                     Height          =   255
                     Index           =   0
                     Left            =   120
                     TabIndex        =   66
                     Top             =   240
                     Value           =   -1  'True
                     Width           =   255
                  End
                  Begin VB.CommandButton Command2 
                     Caption         =   "&Traer Medicamento"
                     Height          =   375
                     Index           =   0
                     Left            =   6840
                     TabIndex        =   65
                     Top             =   720
                     Width           =   1695
                  End
                  Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
                     Bindings        =   "FR0502.frx":00B4
                     Height          =   330
                     Index           =   0
                     Left            =   8760
                     TabIndex        =   69
                     TabStop         =   0   'False
                     Top             =   360
                     Visible         =   0   'False
                     Width           =   810
                     DataFieldList   =   "Column 0"
                     _Version        =   131078
                     DataMode        =   2
                     BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Row.Count       =   3
                     Row(0)          =   "Directo a Proveedor"
                     Row(1)          =   "Delegado 1"
                     Row(2)          =   "Delegado 2"
                     ForeColorEven   =   0
                     BackColorOdd    =   16777215
                     RowHeight       =   423
                     Columns(0).Width=   3731
                     Columns(0).Caption=   "C�digo"
                     Columns(0).Name =   "C�digo"
                     Columns(0).DataField=   "Column 0"
                     Columns(0).DataType=   8
                     Columns(0).FieldLen=   256
                     _ExtentX        =   1429
                     _ExtentY        =   582
                     _StockProps     =   93
                     BackColor       =   -2147483643
                     DataFieldToDisplay=   "Column 0"
                  End
               End
               Begin TabDlg.SSTab SSTab2 
                  Height          =   1695
                  Left            =   -74880
                  TabIndex        =   70
                  Top             =   360
                  Width           =   10335
                  _ExtentX        =   18230
                  _ExtentY        =   2990
                  _Version        =   327681
                  Style           =   1
                  Tabs            =   4
                  TabsPerRow      =   4
                  TabHeight       =   520
                  TabCaption(0)   =   "Directo a Proveedor"
                  TabPicture(0)   =   "FR0502.frx":00C6
                  Tab(0).ControlEnabled=   -1  'True
                  Tab(0).Control(0)=   "lblLabel1(34)"
                  Tab(0).Control(0).Enabled=   0   'False
                  Tab(0).Control(1)=   "lblLabel1(33)"
                  Tab(0).Control(1).Enabled=   0   'False
                  Tab(0).Control(2)=   "lblLabel1(32)"
                  Tab(0).Control(2).Enabled=   0   'False
                  Tab(0).Control(3)=   "lblLabel1(31)"
                  Tab(0).Control(3).Enabled=   0   'False
                  Tab(0).Control(4)=   "lblLabel1(30)"
                  Tab(0).Control(4).Enabled=   0   'False
                  Tab(0).Control(5)=   "lblLabel1(29)"
                  Tab(0).Control(5).Enabled=   0   'False
                  Tab(0).Control(6)=   "lblLabel1(7)"
                  Tab(0).Control(6).Enabled=   0   'False
                  Tab(0).Control(7)=   "lblLabel1(3)"
                  Tab(0).Control(7).Enabled=   0   'False
                  Tab(0).Control(8)=   "lblLabel1(1)"
                  Tab(0).Control(8).Enabled=   0   'False
                  Tab(0).Control(9)=   "txtProv1(38)"
                  Tab(0).Control(9).Enabled=   0   'False
                  Tab(0).Control(10)=   "txtProv1(37)"
                  Tab(0).Control(10).Enabled=   0   'False
                  Tab(0).Control(11)=   "txtProv1(36)"
                  Tab(0).Control(11).Enabled=   0   'False
                  Tab(0).Control(12)=   "txtProv1(35)"
                  Tab(0).Control(12).Enabled=   0   'False
                  Tab(0).Control(13)=   "txtProv1(34)"
                  Tab(0).Control(13).Enabled=   0   'False
                  Tab(0).Control(14)=   "txtProv1(33)"
                  Tab(0).Control(14).Enabled=   0   'False
                  Tab(0).Control(15)=   "txtProv1(32)"
                  Tab(0).Control(15).Enabled=   0   'False
                  Tab(0).Control(16)=   "txtProv1(31)"
                  Tab(0).Control(16).Enabled=   0   'False
                  Tab(0).Control(17)=   "txtProv1(30)"
                  Tab(0).Control(17).Enabled=   0   'False
                  Tab(0).ControlCount=   18
                  TabCaption(1)   =   "Delegado 1"
                  TabPicture(1)   =   "FR0502.frx":00E2
                  Tab(1).ControlEnabled=   0   'False
                  Tab(1).Control(0)=   "lblLabel1(53)"
                  Tab(1).Control(1)=   "lblLabel1(43)"
                  Tab(1).Control(2)=   "lblLabel1(42)"
                  Tab(1).Control(3)=   "lblLabel1(41)"
                  Tab(1).Control(4)=   "lblLabel1(40)"
                  Tab(1).Control(5)=   "lblLabel1(39)"
                  Tab(1).Control(6)=   "lblLabel1(38)"
                  Tab(1).Control(7)=   "lblLabel1(37)"
                  Tab(1).Control(8)=   "lblLabel1(36)"
                  Tab(1).Control(9)=   "lblLabel1(35)"
                  Tab(1).Control(10)=   "txtProv1(57)"
                  Tab(1).Control(10).Enabled=   0   'False
                  Tab(1).Control(11)=   "txtProv1(47)"
                  Tab(1).Control(11).Enabled=   0   'False
                  Tab(1).Control(12)=   "txtProv1(46)"
                  Tab(1).Control(12).Enabled=   0   'False
                  Tab(1).Control(13)=   "txtProv1(45)"
                  Tab(1).Control(13).Enabled=   0   'False
                  Tab(1).Control(14)=   "txtProv1(44)"
                  Tab(1).Control(14).Enabled=   0   'False
                  Tab(1).Control(15)=   "txtProv1(43)"
                  Tab(1).Control(15).Enabled=   0   'False
                  Tab(1).Control(16)=   "txtProv1(42)"
                  Tab(1).Control(16).Enabled=   0   'False
                  Tab(1).Control(17)=   "txtProv1(41)"
                  Tab(1).Control(17).Enabled=   0   'False
                  Tab(1).Control(18)=   "txtProv1(40)"
                  Tab(1).Control(18).Enabled=   0   'False
                  Tab(1).Control(19)=   "txtProv1(39)"
                  Tab(1).Control(19).Enabled=   0   'False
                  Tab(1).ControlCount=   20
                  TabCaption(2)   =   "Delegado 2"
                  TabPicture(2)   =   "FR0502.frx":00FE
                  Tab(2).ControlEnabled=   0   'False
                  Tab(2).Control(0)=   "lblLabel1(44)"
                  Tab(2).Control(1)=   "lblLabel1(45)"
                  Tab(2).Control(2)=   "lblLabel1(46)"
                  Tab(2).Control(3)=   "lblLabel1(47)"
                  Tab(2).Control(4)=   "lblLabel1(48)"
                  Tab(2).Control(5)=   "lblLabel1(49)"
                  Tab(2).Control(6)=   "lblLabel1(50)"
                  Tab(2).Control(7)=   "lblLabel1(51)"
                  Tab(2).Control(8)=   "lblLabel1(52)"
                  Tab(2).Control(9)=   "lblLabel1(54)"
                  Tab(2).Control(10)=   "txtProv1(48)"
                  Tab(2).Control(10).Enabled=   0   'False
                  Tab(2).Control(11)=   "txtProv1(49)"
                  Tab(2).Control(11).Enabled=   0   'False
                  Tab(2).Control(12)=   "txtProv1(50)"
                  Tab(2).Control(12).Enabled=   0   'False
                  Tab(2).Control(13)=   "txtProv1(51)"
                  Tab(2).Control(13).Enabled=   0   'False
                  Tab(2).Control(14)=   "txtProv1(52)"
                  Tab(2).Control(14).Enabled=   0   'False
                  Tab(2).Control(15)=   "txtProv1(53)"
                  Tab(2).Control(15).Enabled=   0   'False
                  Tab(2).Control(16)=   "txtProv1(55)"
                  Tab(2).Control(16).Enabled=   0   'False
                  Tab(2).Control(17)=   "txtProv1(56)"
                  Tab(2).Control(17).Enabled=   0   'False
                  Tab(2).Control(18)=   "txtProv1(58)"
                  Tab(2).Control(18).Enabled=   0   'False
                  Tab(2).Control(19)=   "txtProv1(54)"
                  Tab(2).Control(19).Enabled=   0   'False
                  Tab(2).ControlCount=   20
                  TabCaption(3)   =   "Comentarios"
                  TabPicture(3)   =   "FR0502.frx":011A
                  Tab(3).ControlEnabled=   0   'False
                  Tab(3).Control(0)=   "txtProv1(59)"
                  Tab(3).Control(0).Enabled=   0   'False
                  Tab(3).ControlCount=   1
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   54
                     Left            =   -67320
                     TabIndex        =   100
                     TabStop         =   0   'False
                     Tag             =   "Provincia1"
                     Top             =   540
                     Width           =   1485
                  End
                  Begin VB.TextBox txtProv1 
                     Height          =   1170
                     Index           =   59
                     Left            =   -74880
                     MultiLine       =   -1  'True
                     ScrollBars      =   2  'Vertical
                     TabIndex        =   99
                     TabStop         =   0   'False
                     Tag             =   "Comentarios"
                     Top             =   480
                     Width           =   9960
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   58
                     Left            =   -70560
                     TabIndex        =   98
                     TabStop         =   0   'False
                     Tag             =   "Distrito1"
                     Top             =   540
                     Width           =   1440
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   56
                     Left            =   -72960
                     TabIndex        =   97
                     TabStop         =   0   'False
                     Tag             =   "Direcci�n1"
                     Top             =   540
                     Width           =   2295
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   55
                     Left            =   -69000
                     TabIndex        =   96
                     TabStop         =   0   'False
                     Tag             =   "Localidad1"
                     Top             =   540
                     Width           =   1605
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   53
                     Left            =   -65760
                     TabIndex        =   95
                     TabStop         =   0   'False
                     Tag             =   "Pa�s1"
                     Top             =   540
                     Width           =   975
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   52
                     Left            =   -67800
                     TabIndex        =   94
                     TabStop         =   0   'False
                     Tag             =   "Segundo Tel�fono1"
                     Top             =   1200
                     Width           =   2000
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   51
                     Left            =   -72720
                     TabIndex        =   93
                     TabStop         =   0   'False
                     Tag             =   "E-mail1"
                     Top             =   1200
                     Width           =   2640
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   50
                     Left            =   -69960
                     TabIndex        =   92
                     TabStop         =   0   'False
                     Tag             =   "Tel�fono1"
                     Top             =   1200
                     Width           =   2000
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   49
                     Left            =   -74880
                     TabIndex        =   91
                     TabStop         =   0   'False
                     Tag             =   "Fax1"
                     Top             =   1200
                     Width           =   2000
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   48
                     Left            =   -74880
                     TabIndex        =   90
                     TabStop         =   0   'False
                     Tag             =   "Nombre2"
                     Top             =   540
                     Width           =   1845
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   30
                     Left            =   3840
                     TabIndex        =   89
                     TabStop         =   0   'False
                     Tag             =   "Distrito1"
                     Top             =   540
                     Width           =   1320
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   31
                     Left            =   120
                     TabIndex        =   88
                     TabStop         =   0   'False
                     Tag             =   "Direcci�n1"
                     Top             =   540
                     Width           =   3615
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   32
                     Left            =   5280
                     TabIndex        =   87
                     TabStop         =   0   'False
                     Tag             =   "Localidad1"
                     Top             =   540
                     Width           =   1725
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   33
                     Left            =   7080
                     TabIndex        =   86
                     TabStop         =   0   'False
                     Tag             =   "Provincia1"
                     Top             =   540
                     Width           =   1725
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   34
                     Left            =   8880
                     TabIndex        =   85
                     TabStop         =   0   'False
                     Tag             =   "Pa�s1"
                     Top             =   540
                     Width           =   1335
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   35
                     Left            =   3960
                     TabIndex        =   84
                     TabStop         =   0   'False
                     Tag             =   "Segundo Tel�fono1"
                     Top             =   1200
                     Width           =   1875
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   36
                     Left            =   6000
                     TabIndex        =   83
                     TabStop         =   0   'False
                     Tag             =   "E-mail1"
                     Top             =   1200
                     Width           =   1800
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   37
                     Left            =   2040
                     TabIndex        =   82
                     TabStop         =   0   'False
                     Tag             =   "Tel�fono1"
                     Top             =   1200
                     Width           =   1755
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   38
                     Left            =   120
                     TabIndex        =   81
                     TabStop         =   0   'False
                     Tag             =   "Fax1"
                     Top             =   1200
                     Width           =   1755
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   39
                     Left            =   -70680
                     TabIndex        =   80
                     TabStop         =   0   'False
                     Tag             =   "Distrito1"
                     Top             =   600
                     Width           =   1440
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   40
                     Left            =   -73080
                     TabIndex        =   79
                     TabStop         =   0   'False
                     Tag             =   "Direcci�n1"
                     Top             =   600
                     Width           =   2295
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   41
                     Left            =   -69120
                     TabIndex        =   78
                     TabStop         =   0   'False
                     Tag             =   "Localidad1"
                     Top             =   600
                     Width           =   1605
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   42
                     Left            =   -67440
                     TabIndex        =   77
                     TabStop         =   0   'False
                     Tag             =   "Provincia1"
                     Top             =   600
                     Width           =   1485
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   43
                     Left            =   -65880
                     TabIndex        =   76
                     TabStop         =   0   'False
                     Tag             =   "Pa�s1"
                     Top             =   600
                     Width           =   1095
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   44
                     Left            =   -66840
                     TabIndex        =   75
                     TabStop         =   0   'False
                     Tag             =   "Segundo Tel�fono1"
                     Top             =   1200
                     Width           =   2000
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   45
                     Left            =   -72720
                     TabIndex        =   74
                     TabStop         =   0   'False
                     Tag             =   "E-mail1"
                     Top             =   1200
                     Width           =   3600
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   46
                     Left            =   -69000
                     TabIndex        =   73
                     TabStop         =   0   'False
                     Tag             =   "Tel�fono1"
                     Top             =   1200
                     Width           =   2000
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   47
                     Left            =   -74880
                     TabIndex        =   72
                     TabStop         =   0   'False
                     Tag             =   "Fax1"
                     Top             =   1200
                     Width           =   2000
                  End
                  Begin VB.TextBox txtProv1 
                     BackColor       =   &H8000000B&
                     Height          =   330
                     Index           =   57
                     Left            =   -74880
                     TabIndex        =   71
                     TabStop         =   0   'False
                     Tag             =   "Nombre2"
                     Top             =   600
                     Width           =   1725
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Direcci�n"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   54
                     Left            =   -72960
                     TabIndex        =   129
                     Top             =   360
                     Width           =   825
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Distrito"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   52
                     Left            =   -70560
                     TabIndex        =   128
                     Top             =   360
                     Width           =   615
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Localidad"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   51
                     Left            =   -69000
                     TabIndex        =   127
                     Top             =   360
                     Width           =   840
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Provincia"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   50
                     Left            =   -67320
                     TabIndex        =   126
                     Top             =   360
                     Width           =   810
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Pa�s"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   49
                     Left            =   -65760
                     TabIndex        =   125
                     Top             =   360
                     Width           =   405
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "2� Tel�fono"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   48
                     Left            =   -67800
                     TabIndex        =   124
                     Top             =   960
                     Width           =   1005
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "E-mail"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   47
                     Left            =   -72720
                     TabIndex        =   123
                     Top             =   960
                     Width           =   525
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Fax"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   46
                     Left            =   -74880
                     TabIndex        =   122
                     Top             =   960
                     Width           =   315
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Tel�fono"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   45
                     Left            =   -69960
                     TabIndex        =   121
                     Top             =   960
                     Width           =   765
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Nombre"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   44
                     Left            =   -74880
                     TabIndex        =   120
                     Top             =   360
                     Width           =   660
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Direcci�n"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   1
                     Left            =   120
                     TabIndex        =   119
                     Top             =   360
                     Width           =   825
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Distrito"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   3
                     Left            =   3840
                     TabIndex        =   118
                     Top             =   360
                     Width           =   615
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Localidad"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   7
                     Left            =   5280
                     TabIndex        =   117
                     Top             =   360
                     Width           =   840
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Provincia"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   29
                     Left            =   7080
                     TabIndex        =   116
                     Top             =   360
                     Width           =   810
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Pa�s"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   30
                     Left            =   8880
                     TabIndex        =   115
                     Top             =   360
                     Width           =   405
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "2� Tel�fono"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   31
                     Left            =   3960
                     TabIndex        =   114
                     Top             =   960
                     Width           =   1005
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "E-mail"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   32
                     Left            =   6000
                     TabIndex        =   113
                     Top             =   960
                     Width           =   525
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Fax"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   33
                     Left            =   120
                     TabIndex        =   112
                     Top             =   960
                     Width           =   315
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Tel�fono"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   34
                     Left            =   2040
                     TabIndex        =   111
                     Top             =   960
                     Width           =   765
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Direcci�n"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   35
                     Left            =   -73080
                     TabIndex        =   110
                     Top             =   360
                     Width           =   825
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Distrito"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   36
                     Left            =   -70680
                     TabIndex        =   109
                     Top             =   360
                     Width           =   615
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Localidad"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   37
                     Left            =   -69120
                     TabIndex        =   108
                     Top             =   360
                     Width           =   840
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Provincia"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   38
                     Left            =   -67440
                     TabIndex        =   107
                     Top             =   360
                     Width           =   810
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Pa�s"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   39
                     Left            =   -65880
                     TabIndex        =   106
                     Top             =   360
                     Width           =   405
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "2� Tel�fono"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   40
                     Left            =   -66840
                     TabIndex        =   105
                     Top             =   960
                     Width           =   1005
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "E-mail"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   41
                     Left            =   -72720
                     TabIndex        =   104
                     Top             =   960
                     Width           =   525
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Fax"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   42
                     Left            =   -74880
                     TabIndex        =   103
                     Top             =   960
                     Width           =   315
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Tel�fono"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   43
                     Left            =   -69000
                     TabIndex        =   102
                     Top             =   960
                     Width           =   765
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "Nombre"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   53
                     Left            =   -74880
                     TabIndex        =   101
                     Top             =   360
                     Width           =   660
                  End
               End
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Moneda"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   9840
               TabIndex        =   159
               Top             =   360
               Width           =   690
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Cons.Diario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   16
               Left            =   3000
               TabIndex        =   158
               Top             =   960
               Width           =   990
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Cons.Mensual"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   17
               Left            =   3000
               TabIndex        =   157
               Top             =   1560
               Width           =   1200
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Cons.Anual"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   18
               Left            =   3000
               TabIndex        =   156
               Top             =   2160
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Stock M�nimo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   20
               Left            =   120
               TabIndex        =   155
               Top             =   1560
               Width           =   1185
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Punto Pedido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   120
               TabIndex        =   154
               Top             =   2160
               Width           =   1155
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Existencias"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   120
               TabIndex        =   153
               Top             =   960
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "% Desc."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   23
               Left            =   7560
               TabIndex        =   152
               Top             =   960
               Width           =   705
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dias Seg"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   24
               Left            =   4560
               TabIndex        =   151
               Top             =   960
               Width           =   780
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tam.Pedido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   25
               Left            =   4560
               TabIndex        =   150
               Top             =   1560
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "% Rappel"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   26
               Left            =   7560
               TabIndex        =   149
               Top             =   2160
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Precio Oferta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   27
               Left            =   5880
               TabIndex        =   148
               Top             =   1560
               Width           =   1140
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "% Bonif."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   28
               Left            =   7560
               TabIndex        =   147
               Top             =   1560
               Width           =   705
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "F.F."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   5760
               TabIndex        =   146
               Top             =   360
               Width           =   345
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   6480
               TabIndex        =   145
               Top             =   360
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "U.M."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   7320
               TabIndex        =   144
               Top             =   360
               Width           =   420
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Vol.(mL)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   8040
               TabIndex        =   143
               Top             =   360
               Width           =   705
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Seg"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   56
               Left            =   840
               TabIndex        =   142
               Top             =   360
               Width           =   345
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   57
               Left            =   120
               TabIndex        =   141
               Top             =   360
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "PVL"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   58
               Left            =   5880
               TabIndex        =   140
               Top             =   2160
               Width           =   360
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   1245
               TabIndex        =   139
               Top             =   360
               Width           =   780
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "U.E."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   59
               Left            =   8880
               TabIndex        =   138
               Top             =   360
               Width           =   390
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pdte.Recibir"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   14
               Left            =   1560
               TabIndex        =   137
               Top             =   960
               Width           =   1065
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Prec.Ult.Entrada"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   5880
               TabIndex        =   136
               Top             =   960
               Width           =   1425
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pdte.Bonificar"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   15
               Left            =   1560
               TabIndex        =   135
               Top             =   1560
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Precio Neto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   60
               Left            =   9120
               TabIndex        =   134
               Top             =   2160
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "IVA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   61
               Left            =   8640
               TabIndex        =   133
               Top             =   2160
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Referencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   62
               Left            =   4320
               TabIndex        =   132
               Top             =   360
               Width           =   945
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Desviaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   63
               Left            =   1560
               TabIndex        =   131
               Top             =   2160
               Width           =   960
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   0
            Left            =   120
            TabIndex        =   60
            TabStop         =   0   'False
            Top             =   120
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   5054
            _StockProps     =   79
         End
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1575
      Left            =   0
      TabIndex        =   51
      Top             =   480
      Width           =   11655
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   0
         Left            =   6120
         TabIndex        =   45
         Tag             =   "Cod.Interno Farmacia"
         Top             =   1080
         Width           =   1185
      End
      Begin VB.TextBox txtBusq1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   1
         Left            =   7320
         TabIndex        =   46
         Tag             =   "Cod.Interno Farmacia"
         Top             =   1080
         Width           =   345
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   120
         TabIndex        =   36
         Tag             =   "Texto a buscar"
         Top             =   480
         Width           =   4890
      End
      Begin VB.Frame Frame2 
         Caption         =   "Buscarlo en"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   570
         Left            =   5160
         TabIndex        =   52
         Top             =   240
         Width           =   5175
         Begin VB.OptionButton Option2 
            Caption         =   "Proveedor"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   3
            Left            =   3840
            TabIndex        =   40
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Prin.Activo"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   1
            Left            =   1200
            TabIndex        =   38
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Producto"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   37
            Top             =   240
            Value           =   -1  'True
            Width           =   1095
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Grp.Terap."
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   2
            Left            =   2520
            TabIndex        =   39
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Buscar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   430
         Left            =   10440
         TabIndex        =   48
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   1800
         TabIndex        =   43
         Tag             =   "C�d. Grp. Terap�utico"
         Top             =   1080
         Width           =   1905
      End
      Begin VB.CheckBox chkBusq1 
         Caption         =   "Productos Bajo Stock"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008080&
         Height          =   240
         Index           =   4
         Left            =   8160
         TabIndex        =   47
         Top             =   1080
         Width           =   2265
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   17
         Left            =   3840
         TabIndex        =   44
         Tag             =   "Desviaci�n Mayor que"
         Top             =   1080
         Width           =   1905
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0502.frx":0136
         Height          =   330
         Index           =   1
         Left            =   120
         TabIndex        =   41
         Tag             =   "ABC(C)"
         Top             =   1080
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0502.frx":0148
         Height          =   330
         Index           =   2
         Left            =   960
         TabIndex        =   42
         Tag             =   "ABC(CE)"
         Top             =   1080
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Interno"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   6120
         TabIndex        =   61
         Top             =   840
         Width           =   1005
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Texto a buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   195
         Index           =   55
         Left            =   120
         TabIndex        =   57
         Top             =   240
         Width           =   1290
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(C)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   56
         Top             =   840
         Width           =   615
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(CE)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   960
         TabIndex        =   55
         Top             =   840
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Grp. Terap�utico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   1800
         TabIndex        =   54
         Top             =   840
         Width           =   1905
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desviaci�n Mayor que"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   3840
         TabIndex        =   53
         Top             =   840
         Width           =   1905
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   49
      Top             =   7935
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmBusMed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA, COMPRAS                                          *
'* NOMBRE: FrmBusMed (FR0502.FRM)                                       *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA:                                                               *
'* DESCRIPCION: Buscar Medicamentos                                     *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gintIndice As Integer


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cmda�adir_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
    
cmda�adir.Enabled = False
mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
For mintisel = 0 To mintNTotalSelRows - 1
  mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
  'c�d.producto y descripci�n
  SSDBGrid1.AddItem grdDBGrid1(0).Columns(1).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    grdDBGrid1(0).Columns("Producto").CellValue(mvarBkmrk)
Next mintisel

cmda�adir.Enabled = True
End Sub

Private Sub Command1_Click()
Dim strTextoABuscar As String
Dim strCodGrpTerap As String
Dim strDesvMayQue As String
Dim strProdBajoStock As String
Dim strABC_C As String
Dim strABC_CE As String
Dim strClausulaWhere As String
Dim strCodSeguridad As String
Dim strCodIntFar As String
  
  Screen.MousePointer = vbHourglass
  strTextoABuscar = txtBusq1(8).Text
  strCodGrpTerap = txtBusq1(9).Text
  strDesvMayQue = txtBusq1(17).Text
  strProdBajoStock = chkBusq1(4).Value
  strABC_C = cboBusq1(1).Value
  strABC_CE = cboBusq1(2).Value
  strCodIntFar = txtBusq1(0).Text
  strCodSeguridad = txtBusq1(1).Text
  
  
   If strCodGrpTerap <> "" Then
      'If Left(strCodGrpTerap, 1) = "Q" Then
         'MsgBox "El Grupo Terape�tico no ha de comenzar por Q (Medicamentos)", vbInformation, "Aviso"
         'Exit Sub
      'End If
   End If
  
   Command1.Enabled = False
   
   strClausulaWhere = " -1 = -1 "
   If strTextoABuscar <> "" Then
      If Option2(0).Value = True Then
         strClausulaWhere = strClausulaWhere & " AND (UPPER(FR73DESPRODUCTOPROV) LIKE UPPER('%" & strTextoABuscar & "%') OR UPPER(FR73DESPRODUCTO) LIKE UPPER('%" & strTextoABuscar & "%'))"
      ElseIf Option2(1).Value = True Then
         strClausulaWhere = strClausulaWhere & " AND (FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400,FR6800 WHERE FR6400.FR68CODPRINCACTIV=FR6800.FR68CODPRINCACTIV AND UPPER(FR68DESPRINCACTIV) LIKE UPPER('%" & strTextoABuscar & "%') )" & _
                 " OR " & _
                 "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400 WHERE FR68CODPRINCACTIV IN (SELECT FR68CODPRINCACTIV FROM FRH400 WHERE UPPER(FRH4DESSINONIMO) LIKE UPPER('%" & strTextoABuscar & "%')))" & _
                 ") "
      ElseIf Option2(2).Value = True Then
         strClausulaWhere = strClausulaWhere & " AND FR00CODGRPTERAP IN (SELECT FR00CODGRPTERAP FROM FR0000 WHERE UPPER(FR00DESGRPTERAP) LIKE UPPER('" & strTextoABuscar & "%')) "
      ElseIf Option2(3).Value = True Then
         ' Proveedor
        If gstrLlamador = "FrmGenPetOfe" Then
           strClausulaWhere = strClausulaWhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov
           strClausulaWhere = strClausulaWhere & " OR ( "
           strClausulaWhere = strClausulaWhere & " FR79CODPROVEEDOR_A IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
           strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_B IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
           strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_C IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
           strClausulaWhere = strClausulaWhere & " )) "
        Else
          strClausulaWhere = strClausulaWhere & " AND ( "
          strClausulaWhere = strClausulaWhere & " FR79CODPROVEEDOR_A IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_B IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_C IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " ) "
        End If
      End If
   End If
  
  If Option2(3).Value = False Then
     ' Proveedor
    If gstrLlamador = "FrmGenPetOfe" Then
       strClausulaWhere = strClausulaWhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov & ") "
    End If
  End If
  
   If strCodGrpTerap <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR00CODGRPTERAP LIKE '" & strCodGrpTerap & "%' "
   Else
      'strClausulaWhere = strClausulaWhere & " AND (FR00CODGRPTERAP NOT LIKE 'Q%' OR FR00CODGRPTERAP IS NULL) "
   End If
  
   If strProdBajoStock = 1 Then
      strClausulaWhere = strClausulaWhere & " AND FR73EXISTENCIAS<=FR73STOCKMIN "
   End If
  
   If strABC_C <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73ABCCONS='" & strABC_C & "' "
   End If

   If strABC_CE <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73ABCCONS='" & strABC_CE & "' "
   End If

   If strDesvMayQue <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73DESVIACION>" & strDesvMayQue & " "
   End If
   
  If strCodIntFar <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FR73CODINTFAR LIKE '%" & strCodIntFar & "%' "
  End If
  
  If strCodSeguridad <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FR73CODINTFARSEG=" & strCodSeguridad & " "
  End If

   objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
   Call objWinInfo.DataRefresh
   Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero

   Command1.Enabled = True
   Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Activate()
   gintprodbuscado = ""
   SSDBGrid1.Columns(1).Width = 6000
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  If gstrLlamador = "FrmGenPetOfe" Then
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  Else
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  End If
  With objDetailInfo
    .strName = "Medicamentos"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR7300"
    .intCursorSize = 225
    
    .blnAskPrimary = False
    .intAllowance = cwAllowReadOnly
    .strWhere = "FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
    If gstrLlamador = "FrmGenPetOfe" Then
      .strWhere = .strWhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov & ") "
    End If
    
    Call .FormAddOrderField("FR73DESPRODUCTOPROV", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno Medicamento", cwString)
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTOPROV", "Descripci�n Medicamento", cwString)
    Call .FormAddFilterOrder(strKey, "FR73CODINTFAR", "C�digo Interno Medicamento")
    Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTOPROV", "Descripci�n Medicamento")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)

    '.CtrlGetInfo(txtText1(0)).blnInGrid = False

    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(17)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    
    .CtrlGetInfo(txtText1(9)).blnInGrid = False  'c�d.seguridad
    .CtrlGetInfo(txtText1(6)).blnInGrid = False
    .CtrlGetInfo(txtText1(10)).blnInGrid = False
    .CtrlGetInfo(txtText1(11)).blnInGrid = False
    .CtrlGetInfo(txtText1(8)).blnInGrid = False
    
    .CtrlGetInfo(txtProv1(59)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(31)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(30)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(32)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(33)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(34)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(37)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(35)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(38)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(36)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(57)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(40)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(39)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(41)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(42)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(43)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(46)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(44)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(47)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(45)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(48)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(56)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(58)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(55)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(54)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(53)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(50)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(52)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(49)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(51)).blnNegotiated = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "FR79CODPROVEEDOR_A", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(15), "FR79PROVEEDOR")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR79CODPROVEEDOR_B", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(28), "FR79PROVEEDOR")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR79CODPROVEEDOR_C", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(29), "FR79PROVEEDOR")
    
    Call .WinRegister
    Call .WinStabilize
  End With
  If gstrLlamador = "FrmGenPetOfe" Then
    tabTab1(0).Tab = 1
'    grdDBGrid1(0).Columns(1).Caption = "C�digo" 'Cod.Int
'    grdDBGrid1(0).Columns(1).Width = 740 'Cod.Int
'    'grdDBGrid1(0).Columns(2).Visible = False 'C.S.
'    grdDBGrid1(0).Columns(3).Width = 3000 'Descripcion Mat.Sanitario
'    grdDBGrid1(0).Columns(5).Width = 650 'Dosis
'    grdDBGrid1(0).Columns(7).Width = 650 'Vol.
'
'    grdDBGrid1(0).Columns(8).Caption = "Exist." 'Existencias
'    grdDBGrid1(0).Columns(8).Width = 510 'Existencias
'    grdDBGrid1(0).Columns(9).Caption = "Stock Min" 'Stock Min.
'    grdDBGrid1(0).Columns(9).Width = 900 'Stock Min.
'    grdDBGrid1(0).Columns(10).Caption = "Pdte.Recibir" 'Pdte.Recibir
'    grdDBGrid1(0).Columns(10).Width = 1019 'Pdte.Recibir
'    grdDBGrid1(0).Columns(11).Caption = "Cons.Mens." 'Cons.Mensual
'    grdDBGrid1(0).Columns(11).Width = 976 'Cons.Mensual
'    grdDBGrid1(0).Columns(12).Caption = "Pdte.Bonif." 'Pdte.Bonificar
'    grdDBGrid1(0).Columns(12).Width = 900 'Pdte.Bonificar
'    grdDBGrid1(0).Columns(13).Caption = "P.Ult.Ent." 'P.Ult.Ent.
'    grdDBGrid1(0).Columns(13).Width = 1200 'P.Ult.Ent.
  End If
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
  If strFormName = "Medicamentos" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = "WHERE FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
     
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno Producto"
         
     Set objField = .AddField("FR73DESPRODUCTOPROV")
     objField.strSmallDesc = "Descripci�n Producto"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad Medida"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

If grdDBGrid1(0).Rows > 0 Then
  grdDBGrid1(0).Columns(1).Caption = "C�digo"
  grdDBGrid1(0).Columns(1).Width = 0
  grdDBGrid1(0).Columns("Producto").Width = 3500
  grdDBGrid1(0).Columns("Referencia").Width = 1000
  grdDBGrid1(0).Columns("U.E.").Width = 600
  grdDBGrid1(0).Columns("Exist.").Width = 800
  grdDBGrid1(0).Columns("Pdte.Rec").Width = 900
  grdDBGrid1(0).Columns("Cons.diario").Width = 950
  grdDBGrid1(0).Columns("Pto.Pedido").Width = 950
  grdDBGrid1(0).Columns("Stock Min.").Width = 950
  grdDBGrid1(0).Columns("Cons.Mens").Width = 950
  grdDBGrid1(0).Columns("Pdte.Bonif").Width = 950
  grdDBGrid1(0).Columns("Prec.Ult.Ent").Width = 950
  grdDBGrid1(0).Columns("Tam.Pedido").Width = 950
  grdDBGrid1(0).Columns("Prec.Oferta").Width = 950
  grdDBGrid1(0).Columns("Cons.Anual").Width = 950
  grdDBGrid1(0).Columns("PVL").Width = 950
  grdDBGrid1(0).Columns("Prec.Neto").Width = 950
End If

End Sub

'Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'If grdDBGrid1(0).Rows > 0 Then
'    grdDBGrid1(0).Columns(1).Visible = False
'    grdDBGrid1(0).Columns(4).Width = 500
'End If
'End Sub

Private Sub Option1_Click(Index As Integer)
  Dim rstp As rdoResultset
  Dim strP As String
  Dim qryFR79 As rdoQuery
  
  Select Case Index
  Case 0
    If txtText1(3).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(3).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  Case 1
    If txtText1(2).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(2).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  Case 2
    If txtText1(1).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(1).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  End Select

End Sub



Private Sub SSDBGrid1_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
  DispPromptMsg = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim rstp As rdoResultset
  Dim strP As String
  Dim qryFR79 As rdoQuery
  
  gintIndice = intIndex
  Call objWinInfo.CtrlDataChange


  Select Case intIndex
  Case 3
    If txtText1(3).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(3).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  Case 2
    If txtText1(2).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(2).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    End If
  Case 1
    If txtText1(1).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(1).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        If IsNull(rstp("FR79COMENTARIOS").Value) Then
          txtProv1(59).Text = ""
        Else
          txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
        End If
        'rstp("FR79REPRESENT1").Value
        If IsNull(rstp("FR79DIRECCION1").Value) Then
          txtProv1(31).Text = ""
        Else
          txtProv1(31).Text = rstp("FR79DIRECCION1").Value
        End If
        If IsNull(rstp("FR79DISTRITO1").Value) Then
          txtProv1(30).Text = ""
        Else
          txtProv1(30).Text = rstp("FR79DISTRITO1").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD1").Value) Then
          txtProv1(32).Text = ""
        Else
          txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
        End If
        If IsNull(rstp("FR79PROVINCIA1").Value) Then
          txtProv1(33).Text = ""
        Else
          txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
        End If
        If IsNull(rstp("FR79PAIS1").Value) Then
          txtProv1(34).Text = ""
        Else
          txtProv1(34).Text = rstp("FR79PAIS1").Value
        End If
        If IsNull(rstp("FR79TFNO1").Value) Then
          txtProv1(37).Text = ""
        Else
          txtProv1(37).Text = rstp("FR79TFNO1").Value
        End If
        If IsNull(rstp("FR79TFNO11").Value) Then
          txtProv1(35).Text = ""
        Else
          txtProv1(35).Text = rstp("FR79TFNO11").Value
        End If
        If IsNull(rstp("FR79FAX1").Value) Then
          txtProv1(38).Text = ""
        Else
          txtProv1(38).Text = rstp("FR79FAX1").Value
        End If
        If IsNull(rstp("FR79EMAIL1").Value) Then
          txtProv1(36).Text = ""
        Else
          txtProv1(36).Text = rstp("FR79EMAIL1").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT2").Value) Then
          txtProv1(57).Text = ""
        Else
          txtProv1(57).Text = rstp("FR79REPRESENT2").Value
        End If
        If IsNull(rstp("FR79DIRECCION2").Value) Then
          txtProv1(40).Text = ""
        Else
          txtProv1(40).Text = rstp("FR79DIRECCION2").Value
        End If
        If IsNull(rstp("FR79DISTRITO2").Value) Then
          txtProv1(39).Text = ""
        Else
          txtProv1(39).Text = rstp("FR79DISTRITO2").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD2").Value) Then
          txtProv1(41).Text = ""
        Else
          txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
        End If
        If IsNull(rstp("FR79PROVINCIA2").Value) Then
          txtProv1(42).Text = ""
        Else
          txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
        End If
        If IsNull(rstp("FR79PAIS2").Value) Then
          txtProv1(43).Text = ""
        Else
          txtProv1(43).Text = rstp("FR79PAIS2").Value
        End If
        If IsNull(rstp("FR79TFNO2").Value) Then
          txtProv1(46).Text = ""
        Else
          txtProv1(46).Text = rstp("FR79TFNO2").Value
        End If
        If IsNull(rstp("FR79TFNO22").Value) Then
          txtProv1(44).Text = ""
        Else
          txtProv1(44).Text = rstp("FR79TFNO22").Value
        End If
        If IsNull(rstp("FR79FAX2").Value) Then
          txtProv1(47).Text = ""
        Else
          txtProv1(47).Text = rstp("FR79FAX2").Value
        End If
        If IsNull(rstp("FR79EMAIL2").Value) Then
          txtProv1(45).Text = ""
        Else
          txtProv1(45).Text = rstp("FR79EMAIL2").Value
        End If
        '
        If IsNull(rstp("FR79REPRESENT3").Value) Then
          txtProv1(48).Text = ""
        Else
          txtProv1(48).Text = rstp("FR79REPRESENT3").Value
        End If
        If IsNull(rstp("FR79DIRECCION3").Value) Then
          txtProv1(56).Text = ""
        Else
          txtProv1(56).Text = rstp("FR79DIRECCION3").Value
        End If
        If IsNull(rstp("FR79DISTRITO3").Value) Then
          txtProv1(58).Text = ""
        Else
          txtProv1(58).Text = rstp("FR79DISTRITO3").Value
        End If
        If IsNull(rstp("FR79LOCALIDAD3").Value) Then
          txtProv1(55).Text = ""
        Else
          txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
        End If
        If IsNull(rstp("FR79PROVINCIA3").Value) Then
          txtProv1(54).Text = ""
        Else
          txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
        End If
        If IsNull(rstp("FR79PAIS3").Value) Then
          txtProv1(53).Text = ""
        Else
          txtProv1(53).Text = rstp("FR79PAIS3").Value
        End If
        If IsNull(rstp("FR79TFNO3").Value) Then
          txtProv1(50).Text = ""
        Else
          txtProv1(50).Text = rstp("FR79TFNO3").Value
        End If
        If IsNull(rstp("FR79TFNO33").Value) Then
          txtProv1(52).Text = ""
        Else
          txtProv1(52).Text = rstp("FR79TFNO33").Value
        End If
        If IsNull(rstp("FR79FAX3").Value) Then
          txtProv1(49).Text = ""
        Else
          txtProv1(49).Text = rstp("FR79FAX3").Value
        End If
        If IsNull(rstp("FR79EMAIL3").Value) Then
          txtProv1(51).Text = ""
        Else
          txtProv1(51).Text = rstp("FR79EMAIL3").Value
        End If
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
    Else
        'rstp("FR79CODPROVEEDOR").Value
        'rstp("FR79PROVEEDOR").Value
        txtProv1(59).Text = ""
        'rstp("FR79REPRESENT1").Value
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
        'rstp("FR79NIF").Value
        'rstp("FR79CODCONT").Value
        'rstp("FR79CANTABC").Value
        'rstp("FR79DINABC").Value
        'rstp("FR79CTRLFIAB").Value
        'rstp("FR79INDPRONTOPAG").Value
        'rstp("FR79INDPTEBONIF").Value
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    End If
  End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cmdTraer_Click(intIndex As Integer)
Dim intInd As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim intInd2 As Integer

mintNTotalSelRows = 0
mintNTotalSelRows = SSDBGrid1.Rows
SSDBGrid1.MoveFirst
For intInd2 = 0 To mintNTotalSelRows - 1
    gintCont = gintCont + 1
    gaListPrd(gintCont, 1) = SSDBGrid1.Columns(0).Value 'c�digo producto
SSDBGrid1.MoveNext
Next intInd2

Unload Me
  
End Sub
