VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form FrmConExi 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Gestionar Existencias"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11685
   HelpContextID   =   30001
   Icon            =   "FR0516.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   56
      Top             =   0
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1455
      Left            =   0
      TabIndex        =   89
      Top             =   480
      Width           =   11655
      Begin Crystal.CrystalReport CrystalReport1 
         Left            =   10920
         Top             =   240
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   2
         Left            =   120
         TabIndex        =   5
         ToolTipText     =   "Cod.Interno Farmacia"
         Top             =   1080
         Width           =   705
      End
      Begin VB.TextBox txtBusq1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   1
         Left            =   840
         TabIndex        =   6
         ToolTipText     =   "Cod.Interno Farmacia"
         Top             =   1080
         Width           =   225
      End
      Begin VB.CheckBox chkBusq1 
         Caption         =   "Propuesta de Pedidos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   240
         Index           =   0
         Left            =   9240
         TabIndex        =   13
         Top             =   1080
         Width           =   2265
      End
      Begin VB.TextBox txtBusq1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataSource      =   "c"
         Height          =   330
         HelpContextID   =   30104
         Index           =   0
         Left            =   6360
         TabIndex        =   12
         Tag             =   "% de proximidad al stock m�nimo"
         Top             =   440
         Width           =   825
      End
      Begin VB.Frame Frame2 
         ForeColor       =   &H00FF0000&
         Height          =   1240
         Left            =   4680
         TabIndex        =   148
         Top             =   145
         Width           =   1590
         Begin VB.OptionButton Option2 
            Caption         =   "C�d.Prov."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808000&
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   173
            Top             =   1020
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Ref."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808000&
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   2
            Top             =   330
            Width           =   855
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808000&
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   1
            Top             =   120
            Value           =   -1  'True
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Prin.Acti."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808000&
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   3
            Top             =   560
            Width           =   1215
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808000&
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   4
            Top             =   790
            Width           =   1215
         End
      End
      Begin VB.TextBox txtBusq1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataSource      =   "c"
         Height          =   330
         HelpContextID   =   30104
         Index           =   17
         Left            =   6360
         TabIndex        =   10
         Tag             =   "Desviaci�n mayor que"
         Top             =   1020
         Width           =   1425
      End
      Begin VB.CheckBox chkBusq1 
         Caption         =   "Prod.Bajo Stock"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   7320
         TabIndex        =   11
         Top             =   480
         Width           =   1785
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   2760
         TabIndex        =   9
         Tag             =   "C�digo grupo terap�utico"
         Top             =   1080
         Width           =   1785
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Buscar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   370
         Left            =   9600
         TabIndex        =   14
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   120
         TabIndex        =   0
         Top             =   480
         Width           =   4410
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0516.frx":000C
         Height          =   330
         Index           =   1
         Left            =   1200
         TabIndex        =   7
         Top             =   1080
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0516.frx":001E
         Height          =   330
         Index           =   2
         Left            =   1920
         TabIndex        =   8
         Top             =   1080
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Interno"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   65
         Left            =   120
         TabIndex        =   167
         Top             =   840
         Width           =   1005
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "% Margen"
         DataSource      =   "c"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   63
         Left            =   6360
         TabIndex        =   158
         Top             =   200
         Width           =   840
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desv Mayor que"
         DataSource      =   "c"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   6360
         TabIndex        =   94
         Top             =   790
         Width           =   1395
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d.Grp.Terape�tico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   2760
         TabIndex        =   93
         Top             =   840
         Width           =   1785
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(CE)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   1920
         TabIndex        =   92
         Top             =   840
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(C)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   1200
         TabIndex        =   91
         Top             =   840
         Width           =   615
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Texto a buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808000&
         Height          =   195
         Index           =   6
         Left            =   120
         TabIndex        =   90
         Top             =   240
         Width           =   1290
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Index           =   0
      Left            =   0
      TabIndex        =   54
      Top             =   1920
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   5580
         Index           =   0
         Left            =   240
         TabIndex        =   95
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   9843
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0516.frx":0030
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(22)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(28)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(27)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(26)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(25)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(24)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(23)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(21)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(20)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(18)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(17)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(16)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(15)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(13)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(12)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(11)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(5)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(0)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(2)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblLabel1(55)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "lblLabel1(56)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "lblLabel1(59)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "lblLabel1(58)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "lblLabel1(4)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "lblLabel1(60)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "lblLabel1(61)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "lblLabel1(62)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "lblLabel1(57)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "lblLabel1(64)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "lblLabel1(66)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "lblLabel1(68)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(4)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(8)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(20)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "SSTab1"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtText1(11)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(10)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(0)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(6)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "chkCheck1(5)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "txtText1(5)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "txtText1(60)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "txtText1(7)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "txtText1(22)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).Control(45)=   "txtText1(23)"
         Tab(0).Control(45).Enabled=   0   'False
         Tab(0).Control(46)=   "txtText1(18)"
         Tab(0).Control(46).Enabled=   0   'False
         Tab(0).Control(47)=   "txtText1(19)"
         Tab(0).Control(47).Enabled=   0   'False
         Tab(0).Control(48)=   "txtText1(13)"
         Tab(0).Control(48).Enabled=   0   'False
         Tab(0).Control(49)=   "txtText1(14)"
         Tab(0).Control(49).Enabled=   0   'False
         Tab(0).Control(50)=   "txtText1(16)"
         Tab(0).Control(50).Enabled=   0   'False
         Tab(0).Control(51)=   "txtText1(24)"
         Tab(0).Control(51).Enabled=   0   'False
         Tab(0).Control(52)=   "txtText1(25)"
         Tab(0).Control(52).Enabled=   0   'False
         Tab(0).Control(53)=   "txtText1(26)"
         Tab(0).Control(53).Enabled=   0   'False
         Tab(0).Control(54)=   "txtText1(21)"
         Tab(0).Control(54).Enabled=   0   'False
         Tab(0).Control(55)=   "txtText1(12)"
         Tab(0).Control(55).Enabled=   0   'False
         Tab(0).Control(56)=   "txtText1(17)"
         Tab(0).Control(56).Enabled=   0   'False
         Tab(0).Control(57)=   "txtText1(9)"
         Tab(0).Control(57).Enabled=   0   'False
         Tab(0).Control(58)=   "txtText1(31)"
         Tab(0).Control(58).Enabled=   0   'False
         Tab(0).Control(59)=   "txtText1(27)"
         Tab(0).Control(59).Enabled=   0   'False
         Tab(0).Control(60)=   "txtText1(30)"
         Tab(0).Control(60).Enabled=   0   'False
         Tab(0).Control(61)=   "txtText1(32)"
         Tab(0).Control(61).Enabled=   0   'False
         Tab(0).Control(62)=   "txtText1(33)"
         Tab(0).Control(62).Enabled=   0   'False
         Tab(0).Control(63)=   "txtText1(34)"
         Tab(0).Control(63).Enabled=   0   'False
         Tab(0).Control(64)=   "txtText1(35)"
         Tab(0).Control(64).Enabled=   0   'False
         Tab(0).Control(65)=   "txtText1(36)"
         Tab(0).Control(65).Enabled=   0   'False
         Tab(0).Control(66)=   "txtText1(37)"
         Tab(0).Control(66).Enabled=   0   'False
         Tab(0).Control(67)=   "chkCheck1(0)"
         Tab(0).Control(67).Enabled=   0   'False
         Tab(0).Control(68)=   "chkCheck1(1)"
         Tab(0).Control(68).Enabled=   0   'False
         Tab(0).Control(69)=   "chkCheck1(2)"
         Tab(0).Control(69).Enabled=   0   'False
         Tab(0).Control(70)=   "chkCheck1(3)"
         Tab(0).Control(70).Enabled=   0   'False
         Tab(0).Control(71)=   "txtText1(38)"
         Tab(0).Control(71).Enabled=   0   'False
         Tab(0).Control(72)=   "txtText1(39)"
         Tab(0).Control(72).Enabled=   0   'False
         Tab(0).Control(73)=   "txtText1(40)"
         Tab(0).Control(73).Enabled=   0   'False
         Tab(0).ControlCount=   74
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0516.frx":004C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(1)=   "cmdViewListGrid"
         Tab(1).Control(2)=   "cmdOutListGrid"
         Tab(1).Control(3)=   "cmdAddListGrid"
         Tab(1).Control(4)=   "cmdGenPedGrid"
         Tab(1).Control(5)=   "cmdDatGrid"
         Tab(1).ControlCount=   6
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONS30"
            Height          =   330
            HelpContextID   =   30104
            Index           =   40
            Left            =   2520
            TabIndex        =   184
            TabStop         =   0   'False
            Tag             =   "C.U.30|Consumo de los �ltimos 30 d�as"
            Top             =   2760
            Width           =   1290
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESPRODUCTOPROV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   39
            Left            =   2640
            Locked          =   -1  'True
            TabIndex        =   182
            TabStop         =   0   'False
            Tag             =   "Nom.Comercial|Nom.Comercial"
            Top             =   4200
            Visible         =   0   'False
            Width           =   4140
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PTOPED"
            Height          =   330
            HelpContextID   =   30104
            Index           =   38
            Left            =   3960
            TabIndex        =   180
            TabStop         =   0   'False
            Tag             =   "Pto.Ped"
            Top             =   2160
            Width           =   1140
         End
         Begin VB.CommandButton cmdDatGrid 
            Caption         =   "&M�s Datos"
            Height          =   375
            Left            =   -67560
            TabIndex        =   178
            Top             =   5040
            Width           =   1335
         End
         Begin VB.CommandButton cmdGenPedGrid 
            Caption         =   "&Generar Pedido"
            Height          =   375
            Left            =   -69000
            TabIndex        =   177
            Top             =   5040
            Width           =   1335
         End
         Begin VB.CommandButton cmdAddListGrid 
            Caption         =   "&A�adir Lista"
            Height          =   375
            Left            =   -73320
            TabIndex        =   176
            ToolTipText     =   "A�ade el producto a la lista con la que se genera el pedido"
            Top             =   5040
            Width           =   1335
         End
         Begin VB.CommandButton cmdOutListGrid 
            Caption         =   "&Quitar Lista"
            Height          =   375
            Left            =   -71880
            TabIndex        =   175
            Tag             =   "Quita el producto de la lista con la que se genera el pedido"
            ToolTipText     =   "Quita el producto de la lista con la que se genera el pedido"
            Top             =   5040
            Width           =   1335
         End
         Begin VB.CommandButton cmdViewListGrid 
            Caption         =   "&Ver Lista"
            Height          =   375
            Left            =   -70440
            TabIndex        =   174
            ToolTipText     =   "En la pantalla solo se ven los  productos de la lista con la que se genera el pedido"
            Top             =   5040
            Width           =   1335
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Gesti�n Fija"
            DataField       =   "FR73INDGESTFIJA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   3
            Left            =   8040
            TabIndex        =   166
            TabStop         =   0   'False
            Tag             =   "Gesti�n Fija"
            Top             =   2160
            Visible         =   0   'False
            Width           =   1785
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Programado"
            DataField       =   "FR73INDPROG"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   2
            Left            =   8040
            TabIndex        =   165
            TabStop         =   0   'False
            Tag             =   "Programado"
            Top             =   1920
            Visible         =   0   'False
            Width           =   1785
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Transito"
            DataField       =   "FR73INDTRANS"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   8040
            TabIndex        =   164
            TabStop         =   0   'False
            Tag             =   "Transito"
            Top             =   1680
            Visible         =   0   'False
            Width           =   1305
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Gest.Consumos"
            DataField       =   "FR73INDGESTCONS"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   8040
            TabIndex        =   163
            TabStop         =   0   'False
            Tag             =   "Gesti�n por consumos"
            Top             =   1440
            Visible         =   0   'False
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESVIACION"
            Height          =   330
            HelpContextID   =   30104
            Index           =   37
            Left            =   3960
            TabIndex        =   162
            TabStop         =   0   'False
            Tag             =   "Desv.|Desviaci�n"
            Top             =   2520
            Visible         =   0   'False
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "SUGPED"
            Height          =   330
            HelpContextID   =   30104
            Index           =   36
            Left            =   1320
            TabIndex        =   159
            TabStop         =   0   'False
            Tag             =   "SugPed|N� de envases que se recomienda pedir"
            Top             =   2160
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR00CODGRPTERAP"
            Height          =   330
            HelpContextID   =   30104
            Index           =   35
            Left            =   5040
            TabIndex        =   161
            TabStop         =   0   'False
            Tag             =   "Grupo Terap�utico"
            Top             =   2760
            Visible         =   0   'False
            Width           =   1500
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PVL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   34
            Left            =   8040
            TabIndex        =   38
            TabStop         =   0   'False
            Tag             =   "PVL"
            Top             =   960
            Width           =   1530
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PRECBASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   33
            Left            =   6600
            TabIndex        =   34
            TabStop         =   0   'False
            Tag             =   "Precio Base"
            Top             =   960
            Width           =   1410
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR88CODTIPIVA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   32
            Left            =   7680
            TabIndex        =   43
            TabStop         =   0   'False
            Tag             =   "IVA"
            Top             =   2760
            Width           =   330
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PRECIONETCOMPRA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   30
            Left            =   8160
            TabIndex        =   44
            TabStop         =   0   'False
            Tag             =   "Precio Neto"
            Top             =   2760
            Width           =   1530
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   27
            Left            =   10200
            TabIndex        =   30
            TabStop         =   0   'False
            Tag             =   "Mon.|Moneda"
            Top             =   360
            Visible         =   0   'False
            Width           =   570
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73TAMENVASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   31
            Left            =   9240
            Locked          =   -1  'True
            TabIndex        =   29
            TabStop         =   0   'False
            Tag             =   "U.E.|Unidades Envase"
            Top             =   360
            Width           =   900
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODINTFARSEG"
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   840
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Seg"
            Top             =   360
            Width           =   300
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODINTFAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   17
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "C�digo|C�d.Interno"
            Top             =   360
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESCUENTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   6600
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Descuento"
            Top             =   1560
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73RAPPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   6600
            TabIndex        =   42
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Rappel"
            Top             =   2760
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73BONIFICACION"
            Height          =   330
            HelpContextID   =   30104
            Index           =   26
            Left            =   6600
            TabIndex        =   37
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Bonificaci�n"
            Top             =   2160
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PRECOFR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   25
            Left            =   4920
            TabIndex        =   36
            TabStop         =   0   'False
            Tag             =   "Precio Oferta de Compra"
            Top             =   1560
            Width           =   1530
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PREULTENT"
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   4920
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "Precio de la �ltima entrada"
            Top             =   960
            Width           =   1530
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSANUAL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   16
            Left            =   2520
            TabIndex        =   40
            TabStop         =   0   'False
            Tag             =   "Consumo Anual"
            Top             =   2160
            Width           =   1290
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSMES"
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   2520
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "Consumo mensual"
            Top             =   1560
            Width           =   1290
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSDIARIO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   2520
            TabIndex        =   31
            TabStop         =   0   'False
            Tag             =   "Consumo Diario"
            Top             =   960
            Width           =   1290
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73STOCKMAX"
            Height          =   330
            HelpContextID   =   30104
            Index           =   19
            Left            =   120
            TabIndex        =   39
            TabStop         =   0   'False
            Tag             =   "Stock m�ximo"
            Top             =   2160
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73STOCKMIN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   18
            Left            =   120
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "Stock m�nimo"
            Top             =   1560
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73TAMPEDI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   3960
            TabIndex        =   35
            TabStop         =   0   'False
            Tag             =   "Tama�o del pedido en d�as"
            Top             =   1560
            Width           =   825
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DIASSEG"
            Height          =   330
            HelpContextID   =   30104
            Index           =   22
            Left            =   3960
            TabIndex        =   32
            TabStop         =   0   'False
            Tag             =   "D�as de Seguridad"
            Top             =   960
            Width           =   825
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PTEBONIF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   1320
            TabIndex        =   27
            TabStop         =   0   'False
            Tag             =   "Pendiente Bonificar"
            Top             =   1560
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73REFERENCIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   60
            Left            =   7800
            Locked          =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   360
            Width           =   1260
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESPRODUCTOPROV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   1320
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Producto|Descripci�n Producto"
            Top             =   360
            Width           =   3540
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Cons.Reg."
            DataField       =   "FR73INDCONSREGULAR"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   5
            Left            =   5160
            TabIndex        =   41
            TabStop         =   0   'False
            Tag             =   "Consumo Regular"
            Top             =   2160
            Width           =   1305
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH7CODFORMFAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   6
            Left            =   4920
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "F.F.|Forma Farmace�tica"
            Top             =   360
            Width           =   600
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DOSIS"
            Height          =   330
            HelpContextID   =   30104
            Index           =   0
            Left            =   5520
            Locked          =   -1  'True
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   960
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR93CODUNIMEDIDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   6480
            Locked          =   -1  'True
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "U.M.|Unidad de Medida"
            Top             =   360
            Width           =   600
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73VOLUMEN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   7080
            Locked          =   -1  'True
            TabIndex        =   21
            TabStop         =   0   'False
            Tag             =   "Vol.|Volumen (mL)"
            Top             =   360
            Width           =   700
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4785
            Index           =   0
            Left            =   -74880
            TabIndex        =   85
            TabStop         =   0   'False
            Top             =   90
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   8440
            _StockProps     =   79
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   2655
            Left            =   120
            TabIndex        =   116
            Top             =   2880
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   4683
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabHeight       =   520
            TabCaption(0)   =   "Proveedores"
            TabPicture(0)   =   "FR0516.frx":0068
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "txtText1(1)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "Frame3"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "txtText1(2)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "txtText1(3)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).ControlCount=   4
            TabCaption(1)   =   "Inf. Adicional"
            TabPicture(1)   =   "FR0516.frx":0084
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "SSTab2"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_A"
               Height          =   330
               HelpContextID   =   30104
               Index           =   3
               Left            =   840
               TabIndex        =   46
               TabStop         =   0   'False
               Tag             =   "C�d.Prov.A"
               Top             =   600
               Width           =   930
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_B"
               Height          =   330
               HelpContextID   =   30104
               Index           =   2
               Left            =   840
               TabIndex        =   49
               TabStop         =   0   'False
               Tag             =   "C�d.Prov.B"
               Top             =   960
               Width           =   930
            End
            Begin VB.Frame Frame3 
               Caption         =   "Producto de"
               ForeColor       =   &H00FF0000&
               Height          =   2055
               Left            =   240
               TabIndex        =   117
               Top             =   360
               Width           =   10215
               Begin VB.CommandButton cmdViewList 
                  Caption         =   "&Ver Lista"
                  Height          =   375
                  Left            =   4440
                  TabIndex        =   172
                  ToolTipText     =   "En la pantalla solo se ven los  productos de la lista con la que se genera el pedido"
                  Top             =   1560
                  Width           =   1335
               End
               Begin VB.CommandButton cmdOutList 
                  Caption         =   "&Quitar Lista"
                  Height          =   375
                  Left            =   3000
                  TabIndex        =   171
                  Tag             =   "Quita el producto de la lista con la que se genera el pedido"
                  ToolTipText     =   "Quita el producto de la lista con la que se genera el pedido"
                  Top             =   1560
                  Width           =   1335
               End
               Begin VB.CommandButton cmdInList 
                  Caption         =   "&A�adir Lista"
                  Height          =   375
                  Left            =   1560
                  TabIndex        =   170
                  ToolTipText     =   "A�ade el producto a la lista con la que se genera el pedido"
                  Top             =   1560
                  Width           =   1335
               End
               Begin VB.CommandButton cmdGenerar 
                  Caption         =   "&Generar Pedido"
                  Height          =   375
                  Left            =   5880
                  TabIndex        =   169
                  Top             =   1560
                  Width           =   1335
               End
               Begin VB.CommandButton cmdMasDatos 
                  Caption         =   "&M�s Datos"
                  Height          =   375
                  Left            =   7320
                  TabIndex        =   168
                  Top             =   1560
                  Width           =   1335
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   28
                  Left            =   1560
                  TabIndex        =   50
                  TabStop         =   0   'False
                  Tag             =   "Proveedor B"
                  Top             =   600
                  Width           =   8490
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   15
                  Left            =   1560
                  TabIndex        =   47
                  TabStop         =   0   'False
                  Tag             =   "Proveedor A"
                  Top             =   240
                  Width           =   8490
               End
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   2
                  Left            =   360
                  TabIndex        =   51
                  Top             =   960
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   1
                  Left            =   360
                  TabIndex        =   48
                  Top             =   600
                  Width           =   255
               End
               Begin VB.OptionButton Option1 
                  Caption         =   "Option1"
                  Height          =   255
                  Index           =   0
                  Left            =   360
                  TabIndex        =   45
                  Top             =   240
                  Value           =   -1  'True
                  Width           =   255
               End
               Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
                  Bindings        =   "FR0516.frx":00A0
                  Height          =   330
                  Index           =   0
                  Left            =   600
                  TabIndex        =   57
                  TabStop         =   0   'False
                  Top             =   1560
                  Visible         =   0   'False
                  Width           =   2130
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Row.Count       =   3
                  Row(0)          =   "Directo a Proveedor"
                  Row(1)          =   "Delegado 1"
                  Row(2)          =   "Delegado 2"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns(0).Width=   3731
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  _ExtentX        =   3757
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  DataFieldToDisplay=   "Column 0"
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   29
                  Left            =   3840
                  TabIndex        =   53
                  TabStop         =   0   'False
                  Tag             =   "Proveedor C"
                  Top             =   960
                  Visible         =   0   'False
                  Width           =   6330
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre Comercial:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   67
                  Left            =   600
                  TabIndex        =   183
                  Top             =   1080
                  Visible         =   0   'False
                  Width           =   1605
               End
            End
            Begin TabDlg.SSTab SSTab2 
               Height          =   2175
               Left            =   -74880
               TabIndex        =   118
               Top             =   360
               Width           =   10335
               _ExtentX        =   18230
               _ExtentY        =   3836
               _Version        =   327681
               Style           =   1
               Tabs            =   4
               TabsPerRow      =   4
               TabHeight       =   520
               TabCaption(0)   =   "Directo a Proveedor"
               TabPicture(0)   =   "FR0516.frx":00B2
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(34)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(33)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(32)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(31)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(30)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "lblLabel1(29)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "lblLabel1(7)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "lblLabel1(3)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "lblLabel1(1)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "txtProv1(38)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtProv1(37)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "txtProv1(36)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "txtProv1(35)"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).Control(13)=   "txtProv1(34)"
               Tab(0).Control(13).Enabled=   0   'False
               Tab(0).Control(14)=   "txtProv1(33)"
               Tab(0).Control(14).Enabled=   0   'False
               Tab(0).Control(15)=   "txtProv1(32)"
               Tab(0).Control(15).Enabled=   0   'False
               Tab(0).Control(16)=   "txtProv1(31)"
               Tab(0).Control(16).Enabled=   0   'False
               Tab(0).Control(17)=   "txtProv1(30)"
               Tab(0).Control(17).Enabled=   0   'False
               Tab(0).ControlCount=   18
               TabCaption(1)   =   "Delegado 1"
               TabPicture(1)   =   "FR0516.frx":00CE
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "lblLabel1(53)"
               Tab(1).Control(1)=   "lblLabel1(43)"
               Tab(1).Control(2)=   "lblLabel1(42)"
               Tab(1).Control(3)=   "lblLabel1(41)"
               Tab(1).Control(4)=   "lblLabel1(40)"
               Tab(1).Control(5)=   "lblLabel1(39)"
               Tab(1).Control(6)=   "lblLabel1(38)"
               Tab(1).Control(7)=   "lblLabel1(37)"
               Tab(1).Control(8)=   "lblLabel1(36)"
               Tab(1).Control(9)=   "lblLabel1(35)"
               Tab(1).Control(10)=   "txtProv1(57)"
               Tab(1).Control(10).Enabled=   0   'False
               Tab(1).Control(11)=   "txtProv1(47)"
               Tab(1).Control(11).Enabled=   0   'False
               Tab(1).Control(12)=   "txtProv1(46)"
               Tab(1).Control(12).Enabled=   0   'False
               Tab(1).Control(13)=   "txtProv1(45)"
               Tab(1).Control(13).Enabled=   0   'False
               Tab(1).Control(14)=   "txtProv1(44)"
               Tab(1).Control(14).Enabled=   0   'False
               Tab(1).Control(15)=   "txtProv1(43)"
               Tab(1).Control(15).Enabled=   0   'False
               Tab(1).Control(16)=   "txtProv1(42)"
               Tab(1).Control(16).Enabled=   0   'False
               Tab(1).Control(17)=   "txtProv1(41)"
               Tab(1).Control(17).Enabled=   0   'False
               Tab(1).Control(18)=   "txtProv1(40)"
               Tab(1).Control(18).Enabled=   0   'False
               Tab(1).Control(19)=   "txtProv1(39)"
               Tab(1).Control(19).Enabled=   0   'False
               Tab(1).ControlCount=   20
               TabCaption(2)   =   "Delegado 2"
               TabPicture(2)   =   "FR0516.frx":00EA
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "lblLabel1(44)"
               Tab(2).Control(1)=   "lblLabel1(45)"
               Tab(2).Control(2)=   "lblLabel1(46)"
               Tab(2).Control(3)=   "lblLabel1(47)"
               Tab(2).Control(4)=   "lblLabel1(48)"
               Tab(2).Control(5)=   "lblLabel1(49)"
               Tab(2).Control(6)=   "lblLabel1(50)"
               Tab(2).Control(7)=   "lblLabel1(51)"
               Tab(2).Control(8)=   "lblLabel1(52)"
               Tab(2).Control(9)=   "lblLabel1(54)"
               Tab(2).Control(10)=   "txtProv1(48)"
               Tab(2).Control(10).Enabled=   0   'False
               Tab(2).Control(11)=   "txtProv1(49)"
               Tab(2).Control(11).Enabled=   0   'False
               Tab(2).Control(12)=   "txtProv1(50)"
               Tab(2).Control(12).Enabled=   0   'False
               Tab(2).Control(13)=   "txtProv1(51)"
               Tab(2).Control(13).Enabled=   0   'False
               Tab(2).Control(14)=   "txtProv1(52)"
               Tab(2).Control(14).Enabled=   0   'False
               Tab(2).Control(15)=   "txtProv1(53)"
               Tab(2).Control(15).Enabled=   0   'False
               Tab(2).Control(16)=   "txtProv1(55)"
               Tab(2).Control(16).Enabled=   0   'False
               Tab(2).Control(17)=   "txtProv1(56)"
               Tab(2).Control(17).Enabled=   0   'False
               Tab(2).Control(18)=   "txtProv1(58)"
               Tab(2).Control(18).Enabled=   0   'False
               Tab(2).Control(19)=   "txtProv1(54)"
               Tab(2).Control(19).Enabled=   0   'False
               Tab(2).ControlCount=   20
               TabCaption(3)   =   "Comentarios"
               TabPicture(3)   =   "FR0516.frx":0106
               Tab(3).ControlEnabled=   0   'False
               Tab(3).Control(0)=   "txtProv1(59)"
               Tab(3).Control(0).Enabled=   0   'False
               Tab(3).ControlCount=   1
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   54
                  Left            =   -68280
                  TabIndex        =   80
                  TabStop         =   0   'False
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   1845
               End
               Begin VB.TextBox txtProv1 
                  Height          =   1530
                  Index           =   59
                  Left            =   -74880
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   84
                  TabStop         =   0   'False
                  Tag             =   "Comentarios"
                  Top             =   480
                  Width           =   10080
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   58
                  Left            =   -74880
                  TabIndex        =   78
                  TabStop         =   0   'False
                  Tag             =   "Distrito1"
                  Top             =   1140
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   56
                  Left            =   -70200
                  TabIndex        =   77
                  TabStop         =   0   'False
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   5295
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   55
                  Left            =   -72360
                  TabIndex        =   79
                  TabStop         =   0   'False
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   53
                  Left            =   -66360
                  TabIndex        =   81
                  TabStop         =   0   'False
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   1455
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   52
                  Left            =   -72360
                  TabIndex        =   86
                  TabStop         =   0   'False
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   51
                  Left            =   -68280
                  TabIndex        =   83
                  TabStop         =   0   'False
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3360
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   50
                  Left            =   -74880
                  TabIndex        =   87
                  TabStop         =   0   'False
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   49
                  Left            =   -70320
                  TabIndex        =   82
                  TabStop         =   0   'False
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   48
                  Left            =   -74880
                  TabIndex        =   76
                  TabStop         =   0   'False
                  Tag             =   "Nombre2"
                  Top             =   540
                  Width           =   4605
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   30
                  Left            =   6240
                  TabIndex        =   59
                  TabStop         =   0   'False
                  Tag             =   "Distrito1"
                  Top             =   540
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   31
                  Left            =   120
                  TabIndex        =   58
                  TabStop         =   0   'False
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   6015
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   32
                  Left            =   120
                  TabIndex        =   60
                  TabStop         =   0   'False
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   33
                  Left            =   4200
                  TabIndex        =   61
                  TabStop         =   0   'False
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   2800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   34
                  Left            =   7080
                  TabIndex        =   62
                  TabStop         =   0   'False
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   2780
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   35
                  Left            =   2280
                  TabIndex        =   64
                  TabStop         =   0   'False
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   36
                  Left            =   6600
                  TabIndex        =   66
                  TabStop         =   0   'False
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3240
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   37
                  Left            =   120
                  TabIndex        =   63
                  TabStop         =   0   'False
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   38
                  Left            =   4440
                  TabIndex        =   65
                  TabStop         =   0   'False
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   39
                  Left            =   -74880
                  TabIndex        =   69
                  TabStop         =   0   'False
                  Tag             =   "Distrito1"
                  Top             =   1140
                  Width           =   1800
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   40
                  Left            =   -70200
                  TabIndex        =   68
                  TabStop         =   0   'False
                  Tag             =   "Direcci�n1"
                  Top             =   540
                  Width           =   5295
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   41
                  Left            =   -72360
                  TabIndex        =   70
                  TabStop         =   0   'False
                  Tag             =   "Localidad1"
                  Top             =   1140
                  Width           =   4005
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   42
                  Left            =   -68280
                  TabIndex        =   71
                  TabStop         =   0   'False
                  Tag             =   "Provincia1"
                  Top             =   1140
                  Width           =   1845
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   43
                  Left            =   -66360
                  TabIndex        =   88
                  TabStop         =   0   'False
                  Tag             =   "Pa�s1"
                  Top             =   1140
                  Width           =   1455
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   44
                  Left            =   -72360
                  TabIndex        =   73
                  TabStop         =   0   'False
                  Tag             =   "Segundo Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   45
                  Left            =   -68280
                  TabIndex        =   75
                  TabStop         =   0   'False
                  Tag             =   "E-mail1"
                  Top             =   1740
                  Width           =   3360
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   46
                  Left            =   -74880
                  TabIndex        =   72
                  TabStop         =   0   'False
                  Tag             =   "Tel�fono1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   47
                  Left            =   -70320
                  TabIndex        =   74
                  TabStop         =   0   'False
                  Tag             =   "Fax1"
                  Top             =   1740
                  Width           =   2000
               End
               Begin VB.TextBox txtProv1 
                  BackColor       =   &H8000000B&
                  Height          =   330
                  Index           =   57
                  Left            =   -74880
                  TabIndex        =   67
                  TabStop         =   0   'False
                  Tag             =   "Nombre2"
                  Top             =   540
                  Width           =   4605
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   54
                  Left            =   -70200
                  TabIndex        =   147
                  Top             =   360
                  Width           =   825
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   52
                  Left            =   -74880
                  TabIndex        =   146
                  Top             =   960
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   51
                  Left            =   -72360
                  TabIndex        =   145
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   50
                  Left            =   -68280
                  TabIndex        =   144
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   49
                  Left            =   -66360
                  TabIndex        =   143
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   48
                  Left            =   -72360
                  TabIndex        =   142
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   47
                  Left            =   -68280
                  TabIndex        =   141
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   46
                  Left            =   -70320
                  TabIndex        =   140
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   45
                  Left            =   -74880
                  TabIndex        =   139
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   44
                  Left            =   -74880
                  TabIndex        =   138
                  Top             =   360
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   1
                  Left            =   120
                  TabIndex        =   137
                  Top             =   360
                  Width           =   825
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   3
                  Left            =   6240
                  TabIndex        =   136
                  Top             =   360
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   7
                  Left            =   120
                  TabIndex        =   135
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   29
                  Left            =   4200
                  TabIndex        =   134
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   30
                  Left            =   7080
                  TabIndex        =   133
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   31
                  Left            =   2280
                  TabIndex        =   132
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   32
                  Left            =   6600
                  TabIndex        =   131
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   33
                  Left            =   4440
                  TabIndex        =   130
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   34
                  Left            =   120
                  TabIndex        =   129
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Direcci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   35
                  Left            =   -70200
                  TabIndex        =   128
                  Top             =   360
                  Width           =   825
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Distrito"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   36
                  Left            =   -74880
                  TabIndex        =   127
                  Top             =   960
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Localidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   37
                  Left            =   -72360
                  TabIndex        =   126
                  Top             =   960
                  Width           =   840
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Provincia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   38
                  Left            =   -68280
                  TabIndex        =   125
                  Top             =   960
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pa�s"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   39
                  Left            =   -66360
                  TabIndex        =   124
                  Top             =   960
                  Width           =   405
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "2� Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   40
                  Left            =   -72360
                  TabIndex        =   123
                  Top             =   1560
                  Width           =   1005
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "E-mail"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   41
                  Left            =   -68280
                  TabIndex        =   122
                  Top             =   1560
                  Width           =   525
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Fax"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   42
                  Left            =   -70320
                  TabIndex        =   121
                  Top             =   1560
                  Width           =   315
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tel�fono"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   43
                  Left            =   -74880
                  TabIndex        =   120
                  Top             =   1560
                  Width           =   765
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   53
                  Left            =   -74880
                  TabIndex        =   119
                  Top             =   360
                  Width           =   660
               End
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR79CODPROVEEDOR_C"
               Height          =   330
               HelpContextID   =   30104
               Index           =   1
               Left            =   840
               TabIndex        =   52
               TabStop         =   0   'False
               Tag             =   "C�d.Prov.C"
               Top             =   1320
               Visible         =   0   'False
               Width           =   930
            End
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr73existencias"
            Height          =   330
            HelpContextID   =   30104
            Index           =   20
            Left            =   120
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "Existencias"
            Top             =   960
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   8
            Left            =   240
            TabIndex        =   179
            Tag             =   "CodPrd|CP"
            Top             =   120
            Visible         =   0   'False
            Width           =   690
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CANTPEND"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   1320
            TabIndex        =   25
            TabStop         =   0   'False
            Tag             =   "Cantidad pendiente de recibir"
            Top             =   960
            Width           =   1050
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cons.Ult.30d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   68
            Left            =   2520
            TabIndex        =   185
            Top             =   2520
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pto.Ped"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   66
            Left            =   3960
            TabIndex        =   181
            Top             =   1920
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Sug.Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   195
            Index           =   64
            Left            =   1320
            TabIndex        =   160
            Top             =   1920
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   57
            Left            =   120
            TabIndex        =   149
            Top             =   120
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "PVL"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   62
            Left            =   8040
            TabIndex        =   157
            Top             =   720
            Width           =   360
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Base"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   61
            Left            =   6600
            TabIndex        =   156
            Top             =   720
            Width           =   1035
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "IVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   60
            Left            =   7680
            TabIndex        =   155
            Top             =   2520
            Width           =   315
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Neto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   8160
            TabIndex        =   154
            Top             =   2520
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   58
            Left            =   10200
            TabIndex        =   152
            Top             =   120
            Visible         =   0   'False
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.E."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   59
            Left            =   9240
            TabIndex        =   151
            Top             =   120
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Seg"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   56
            Left            =   840
            TabIndex        =   150
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   55
            Left            =   7800
            TabIndex        =   115
            Top             =   120
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Prec.Ult.Entrada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   4920
            TabIndex        =   114
            Top             =   720
            Width           =   1425
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   1320
            TabIndex        =   113
            Top             =   120
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   4920
            TabIndex        =   112
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   5520
            TabIndex        =   111
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   6480
            TabIndex        =   110
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Vol.(mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   7080
            TabIndex        =   109
            Top             =   120
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pte.Bonifi."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   1320
            TabIndex        =   108
            Top             =   1320
            Width           =   900
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cons.Diario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   2520
            TabIndex        =   107
            Top             =   720
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cons.Mensual"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   2520
            TabIndex        =   106
            Top             =   1320
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cons.Anual"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   2520
            TabIndex        =   105
            Top             =   1920
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Stock M�n."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   120
            TabIndex        =   104
            Top             =   1320
            Width           =   960
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Stock M�x."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   120
            TabIndex        =   103
            Top             =   1920
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Desc."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   6600
            TabIndex        =   101
            Top             =   1320
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dias Seg."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   3960
            TabIndex        =   100
            Top             =   720
            Width           =   840
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tam. Ped."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   25
            Left            =   3960
            TabIndex        =   99
            Top             =   1320
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Rappel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   26
            Left            =   6600
            TabIndex        =   98
            Top             =   2520
            Width           =   810
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   27
            Left            =   4920
            TabIndex        =   97
            Top             =   1320
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Bonif."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   6600
            TabIndex        =   96
            Top             =   1920
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pte.Recibir"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   1320
            TabIndex        =   153
            Top             =   720
            Width           =   960
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Existencias"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   120
            TabIndex        =   102
            Top             =   720
            Width           =   975
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   55
      Top             =   7935
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmConExi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA, COMPRAS                                          *
'* NOMBRE: FrmBusMed (FR0502.FRM)                                       *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA:                                                               *
'* DESCRIPCION: Buscar Medicamentos                                     *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gintIndice As Integer
Dim mblnHayFiltro As Boolean
Dim mblnPas As Boolean
Dim mstrWhere As String
Private Function calcular_precio_neto(ByVal strPrecUni As Currency, _
                                      ByVal strTpDesc As Currency, _
                                      ByVal strRec As Currency, _
                                      ByVal strIVA As Currency) As Double
  Dim Precio_Neto As Currency
  Dim Precio_Base As Currency
  Dim Descuento As Currency
  Dim IVA As Currency
  Dim Recargo As Currency
  Dim PVL As Currency
  Dim PVP As Currency
  Dim Param_Gen As Currency
  Dim strPrecNetComp As String
  Dim strupdate As String
  Dim strPrecVenta As String

  'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
  If IsNumeric(strPrecUni) Then 'Precio_Base
    Precio_Base = strPrecUni
  Else
    Precio_Base = 0
  End If
  If IsNumeric(strTpDesc) Then 'Descuento
    Descuento = Precio_Base * strTpDesc / 100
  Else
    Descuento = 0
  End If
  If IsNumeric(strRec) Then   'Recargo
    Recargo = Precio_Base * strRec / 100
  Else
    Recargo = 0
  End If
  If IsNumeric(strIVA) Then 'IVA
    IVA = (Precio_Base + Recargo - Descuento) * strIVA / 100
  Else
    IVA = 0
  End If
  Precio_Neto = Precio_Base - Descuento + IVA + Recargo
  calcular_precio_neto = Precio_Neto
End Function



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub




Private Sub cmdAddListGrid_Click()
  'Se a�ade un producto a la lista
  Dim intResp As Integer
  Dim intInd As Integer
  Dim blnYaEsta As Boolean
  Dim intisel As Integer
  Dim varBkmrk As Variant
  Dim intNTotalSelRows As Integer
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rstFR73 As rdoResultset
  Dim blnEntra As Boolean
  
  'intNTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
  'For intisel = 0 To intNTotalSelRows - 1
  '  varBkmrk = grdDBGrid1(2).SelBookmarks(intisel)
    'SSDBGrid1.AddItem grdDBGrid1(2).Columns("C�digo Medicamento").CellValue(varBkmrk) & Chr(vbKeyTab) & _
                    grdDBGrid1(2).Columns("Descripci�n Medicamento").CellValue(varBkmrk)
  'Next intisel
  
  blnEntra = False
  strFR73 = "SELECT FR73CODPRODUCTO,FR00CODGRPTERAP,FR73PRECBASE,FR73PVL," & _
            "       FR73DESVIACION,FR73TAMENVASE,FR73DESCUENTO,FR73BONIFICACION,FR88CODTIPIVA,FR73PRECIONETCOMPRA " & _
            "  FROM FR7300 " & _
            " WHERE FR73CODINTFAR = ? "
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
            
  cmdAddListGrid.Enabled = False
  blnYaEsta = False
  intNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
  If intNTotalSelRows > 0 Then
    For intisel = 0 To intNTotalSelRows - 1
    varBkmrk = grdDBGrid1(0).SelBookmarks(intisel)
    qryFR73(0) = grdDBGrid1(0).Columns("C�digo").CellValue(varBkmrk)
    Set rstFR73 = qryFR73.OpenResultset()
    blnEntra = True
    If (gintCont < 250) And (Not rstFR73.EOF) Then
      For intInd = 1 To gintCont
        'Se comprueba que el producto no est� en la lista
        If (gaList(intInd, 1) = rstFR73.rdoColumns("FR73CODPRODUCTO").Value) And (gaList(intInd, 2) = -1) Then
          blnYaEsta = True
          Exit For
        End If
      Next intInd
      
      If blnYaEsta = False Then
        gintCont = gintCont + 1
        gaList(gintCont, 1) = rstFR73.rdoColumns("FR73CODPRODUCTO").Value
        gaList(gintCont, 2) = -1
       If IsNumeric(txtText1(30).Text) Then
         If Not IsNull(rstFR73.rdoColumns("FR73PRECIONETCOMPRA").Value) Then
          gaList(gintCont, 0) = Format(rstFR73.rdoColumns("FR73PRECIONETCOMPRA").Value, "0.000") 'Precio Neto
         Else
          gaList(gintCont, 0) = Format(0, "0.000") 'Precio Neto
         End If
       Else
         gaList(gintCont, 0) = 0
       End If
          
       If Not IsNull(rstFR73.rdoColumns("FR00CODGRPTERAP").Value) Then
        If UCase(Left(rstFR73.rdoColumns("FR00CODGRPTERAP").Value, 1)) = "Q" Then
          If Not IsNull(rstFR73.rdoColumns("FR73PRECBASE").Value) Then
            gaList(gintCont, 3) = Format(rstFR73.rdoColumns("FR73PRECBASE").Value, "0.000") 'Precio Base
          Else
            gaList(gintCont, 3) = Format(0, "0.000") 'Precio Base
          End If
        Else
          If Not IsNull(rstFR73.rdoColumns("FR73PVL").Value) Then
            gaList(gintCont, 3) = Format(rstFR73.rdoColumns("FR73PVL").Value, "0.000") 'PVL
          Else
            gaList(gintCont, 3) = Format(0, "0.000") 'PVL
          End If
        End If
       Else
        If Not IsNull(rstFR73.rdoColumns("FR73PVL").Value) Then
          gaList(gintCont, 3) = Format(rstFR73.rdoColumns("FR73PVL").Value, "0.000") 'PVL
        Else
          gaList(gintCont, 3) = Format(0, "0.000") 'PVL
        End If
       End If
       
      
       If Not IsNull(rstFR73.rdoColumns("FR73DESVIACION").Value) And _
          Not IsNull(rstFR73.rdoColumns("FR73TAMENVASE").Value) Then
        If (rstFR73.rdoColumns("FR73TAMENVASE").Value > 0) And _
          (rstFR73.rdoColumns("FR73DESVIACION").Value < 0) Then 'SugPed
          gaList(gintCont, 4) = (rstFR73.rdoColumns("FR73DESVIACION").Value \ rstFR73.rdoColumns("FR73TAMENVASE").Value) * (-1) 'Cantidad sugerida
        Else
          If (rstFR73.rdoColumns("FR73DESVIACION").Value > 0) Then
            gaList(gintCont, 4) = 0 '(-1) * rstFR73.rdoColumns("FR73DESVIACION").Value \ rstFR73.rdoColumns("FR73TAMENVASE").Value  'Cantidad sugerida
          Else
            gaList(gintCont, 4) = 1
          End If
        End If
       Else
        gaList(gintCont, 4) = 1
       End If
       
        If Not IsNull(rstFR73.rdoColumns("FR73DESCUENTO").Value) Then 'Tanto por ciento de Descuento
          gaList(gintCont, 5) = Format(rstFR73.rdoColumns("FR73DESCUENTO").Value, "0.00") '%Descuento
        Else
          gaList(gintCont, 5) = 0
        End If
        
        If Not IsNull(rstFR73.rdoColumns("FR73BONIFICACION").Value) Then
          gaList(gintCont, 6) = Format(rstFR73.rdoColumns("FR73BONIFICACION").Value, "0.00") '%Bonificaci�n
        Else
          gaList(gintCont, 6) = 0
        End If
        
        If Not IsNull(rstFR73.rdoColumns("FR88CODTIPIVA").Value) Then
          gaList(gintCont, 7) = rstFR73.rdoColumns("FR88CODTIPIVA").Value 'IVA
        Else
          gaList(gintCont, 7) = 0
        End If
        If Not IsNull(rstFR73.rdoColumns("FR73TAMENVASE").Value) Then 'U.E.
          If rstFR73.rdoColumns("FR73TAMENVASE").Value > 0 Then
            gaList(gintCont, 8) = rstFR73.rdoColumns("FR73TAMENVASE").Value 'U.E.
          Else
            gaList(gintCont, 8) = 1
          End If
        Else
          gaList(gintCont, 8) = 1
        End If
        If gaList(gintCont, 0) = 0 Then
          'Si no hay precio neto lo calculamos
          Dim curPB As Currency
          Dim curDes As Currency
          Dim curIVA As Currency
          curDes = Format((gaList(gintCont, 3) * gaList(gintCont, 5)) / 100, "0.00")
          curIVA = Format(((gaList(gintCont, 3) - curDes) * gaList(gintCont, 7)) / 100, "0.00")
          gaList(gintCont, 0) = Format(gaList(gintCont, 3) - curDes + curIVA, "0.000")
        End If
        gaList(gintCont, 9) = Fix((gaList(gintCont, 8) * gaList(gintCont, 4) * gaList(gintCont, 6)) / 100) 'Unidades por bonificar
        gaList(gintCont, 10) = Format(gaList(gintCont, 0) * gaList(gintCont, 4), "0.000") 'Importe de la l�nea
        'intResp = MsgBox("El producto ha sido a�adido a la lista", vbInformation, "Lista de productos")
      Else
        'intResp = MsgBox("El producto ha sido a�adido a la lista", vbInformation, "Lista de productos")
      End If
    Else
      'intResp = MsgBox("No se pueden seleccionar m�s de 250 productos a la vez", vbInformation, "A�adir a la lista")
    End If
    Next intisel
    intResp = MsgBox("Los Productos han sido a�adidos a la lista", vbInformation, "Lista de productos")
    If blnEntra = True Then
      qryFR73.Close
      Set qryFR73 = Nothing
      Set rstFR73 = Nothing
    End If
  End If
  mblnHayFiltro = False
  cmdViewListGrid.Caption = "&Ver Lista"
  cmdViewListGrid.ToolTipText = "En la pantalla solo se ven los  productos de la lista con la que se genera el pedido"
  cmdAddListGrid.Enabled = True
End Sub

Private Sub cmdDatGrid_Click()
  Dim intCont As Integer
    intCont = gintCont
    
    cmdDatGrid.Enabled = False
    tabTab1(0).Tab = 0
    If txtText1(8).Text = "" Then
      MsgBox "Debe seleccionar un producto apretando el bot�n 'Buscar'", vbInformation, "Aviso"
    Else
      gstrLlamador = "FrmConExi"
      gstrCP = txtText1(8).Text
      Call objsecurity.LaunchProcess("FR0535")
    End If
    cmdDatGrid.Enabled = True
    
     gintCont = intCont
End Sub

Private Sub cmdGenerar_Click()
  Dim intInd As Integer
  Dim blnListaVacia As Boolean
  Dim blnYaEsta As Boolean
  
   cmdGenerar.Enabled = False
   'Se comprueba si hay productos en la lista
   blnListaVacia = True
   For intInd = 1 To gintCont
    If gaList(intInd, 2) = -1 Then
      blnListaVacia = False
      Exit For
    End If
   Next intInd
   
   If blnListaVacia = True Then
     'Se mete el la lista el producto que se est� viendo en ese momento
     blnYaEsta = False
     If Len(Trim(txtText1(8).Text)) > 0 Then
      If gintCont < 250 Then
        For intInd = 1 To gintCont
          'Se comprueba que el producto no est� en la lista
          If (gaList(intInd, 1) = txtText1(8).Text) And (gaList(intInd, 2) = -1) Then
            blnYaEsta = True
            Exit For
          End If
        Next intInd
      
        If blnYaEsta = False Then
          gintCont = gintCont + 1
          gaList(gintCont, 1) = txtText1(8).Text
          gaList(gintCont, 2) = -1
          
          If IsNumeric(txtText1(30).Text) Then
            gaList(gintCont, 0) = Format(txtText1(30).Text, "0.000") 'Precio Neto
          Else
            gaList(gintCont, 0) = 0
          End If
          If UCase(Left(Me.txtText1(35), 1)) = "Q" Then
            gaList(gintCont, 3) = Format(txtText1(33).Text, "0.000") 'Precio Base
          Else
            gaList(gintCont, 3) = Format(txtText1(34).Text, "0.000") 'PVL
          End If
          If IsNumeric(txtText1(36).Text) Then
            If txtText1(36).Text >= 0 Then
              gaList(gintCont, 4) = txtText1(36).Text 'Cantidad sugerida
            Else
              gaList(gintCont, 4) = 0 'Cantidad sugerida
            End If
          Else
            gaList(gintCont, 4) = 0
          End If
          If IsNumeric(txtText1(12).Text) Then
            gaList(gintCont, 5) = Format(txtText1(12).Text, "0.00") '%Descuento
          Else
            gaList(gintCont, 5) = 0
          End If
          If IsNumeric(txtText1(26).Text) Then
            gaList(gintCont, 6) = Format(txtText1(26).Text, "0.00") '%Bonificaci�n
          Else
            gaList(gintCont, 6) = 0
          End If
          If IsNumeric(txtText1(32).Text) Then
            gaList(gintCont, 7) = txtText1(32).Text 'IVA
          Else
            gaList(gintCont, 7) = 0
          End If
          If IsNumeric(txtText1(31).Text) Then
            gaList(gintCont, 8) = txtText1(31).Text
          Else
            gaList(gintCont, 8) = 0
          End If
          If gaList(gintCont, 0) = 0 Then
          'Si no hay precio neto lo calculamos
          Dim curPB As Currency
          Dim curDes As Currency
          Dim curIVA As Currency
          curDes = Format((gaList(gintCont, 3) * gaList(gintCont, 5)) / 100, "0.00")
          curIVA = Format(((gaList(gintCont, 3) - curDes) * gaList(gintCont, 7)) / 100, "0.00")
          gaList(gintCont, 0) = Format(gaList(gintCont, 3) - curDes + curIVA, "0.000")
        End If
        gaList(gintCont, 9) = Fix((gaList(gintCont, 4) * gaList(gintCont, 6)) / 100) 'Unidades por bonificar
        gaList(gintCont, 10) = Format(gaList(gintCont, 0) * gaList(gintCont, 4), "0.000") 'Importe de la l�nea
        End If
      End If
     Else
      'MsgBox "No hay ning�n producto para generar el pedido", vbInformation, "Generar Pedido"
     End If
   
   End If
   
   If txtText1(8).Text <> "" Then
      'gintProdBuscado = txtText1(8).Text
      If Option1(0).Value = True Then
         gintCodProveedor = txtText1(3).Text
      ElseIf Option1(1).Value = True Then
         gintCodProveedor = txtText1(2).Text
      Else
         gintCodProveedor = txtText1(1).Text
      End If
      
      gstrMoneda = txtText1(27).Text
      'If UCase(Left(Me.txtText1(35), 1)) = "Q" Then
      '  gstrPrecioOfertado = txtText1(33).Text 'Precio Base
      'Else
      '  gstrPrecioOfertado = txtText1(34).Text 'PVL
      'End If
      'If IsNumeric(txtText1(36).Text) Then
      '  gdblCant = txtText1(36).Text
      'Else
      '  gdblCant = 0
      'End If
      
      gstrLlamador = "FrmConExi"
      Call objsecurity.LaunchProcess("FR0510")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      objWinInfo.DataRefresh
      gintCont = 0
      gstrLlamador = ""
      'Se vacia la lista
      For intInd = 1 To 250
        gaList(intInd, 2) = 0
      Next intInd
   Else
     'MsgBox "Debe seleccionar un producto apretando el bot�n 'Buscar'", vbInformation, "Lista de productos"
     'La lista est� vacia
     Call objsecurity.LaunchProcess("FR0517")
   End If
   mblnPas = True
   cmdGenerar.Enabled = True
End Sub

Private Sub cmdGenPedGrid_Click()
  Dim intInd As Integer
  Dim blnListaVacia As Boolean
  Dim blnYaEsta As Boolean
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rstFR73 As rdoResultset
  Dim blnEntra As Boolean
  
  blnEntra = False
  'strFR73 = "SELECT FR73CODPRODUCTO,FR00CODGRPTERAP,FR73PRECBASE,FR73PVL," & _
            "       FR73DESVIACION,FR73TAMENVASE,FR73DESCUENTO,FR73BONIFICACION,FR88CODTIPIVA " & _
            "  FROM FR7300 " & _
            " WHERE FR73CODINTFAR = ? "
  'Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
  
   cmdGenPedGrid.Enabled = False
   'Se comprueba si hay productos en la lista
   blnListaVacia = True
   For intInd = 1 To gintCont
    If gaList(intInd, 2) = -1 Then
      blnListaVacia = False
      Exit For
    End If
   Next intInd
   
   If blnListaVacia = True Then
     'Se mete el la lista el producto que se est� viendo en ese momento
     tabTab1(0).Tab = 0
     blnYaEsta = False
     
     If Len(Trim(txtText1(8).Text)) > 0 Then
      If gintCont < 250 Then
        For intInd = 1 To gintCont
          'Se comprueba que el producto no est� en la lista
          If (gaList(intInd, 1) = txtText1(8).Text) And (gaList(intInd, 2) = -1) Then
            blnYaEsta = True
            Exit For
          End If
        Next intInd
      
        If blnYaEsta = False Then
          gintCont = gintCont + 1
          gaList(gintCont, 1) = txtText1(8).Text
          gaList(gintCont, 2) = -1
          
          If IsNumeric(txtText1(30).Text) Then
            gaList(gintCont, 0) = Format(txtText1(30).Text, "0.000") 'Precio Neto
          Else
            gaList(gintCont, 0) = 0
          End If
          
          If UCase(Left(Me.txtText1(35), 1)) = "Q" Then
            gaList(gintCont, 3) = Format(txtText1(33).Text, "0.000") 'Precio Base
          Else
            gaList(gintCont, 3) = Format(txtText1(34).Text, "0.000") 'PVL
          End If
          'If IsNumeric(txtText1(37).Text) And IsNumeric(txtText1(31).Text) Then
          '  If txtText1(37).Text > 0 And txtText1(31).Text > 0 Then
          '    gaList(gintCont, 4) = txtText1(37).Text \ txtText1(31).Text
          '  Else
          '    gaList(gintCont, 4) = 1
          '  End If
          'Else
          '  gaList(gintCont, 4) = 1
          'End If
          If IsNumeric(txtText1(36).Text) Then
            If txtText1(36).Text > 0 Then
              gaList(gintCont, 4) = txtText1(36).Text 'Cantidad sugerida
            Else
              gaList(gintCont, 4) = 0
            End If
          Else
            gaList(gintCont, 4) = 1
          End If
          If IsNumeric(txtText1(12).Text) Then
            gaList(gintCont, 5) = Format(txtText1(12).Text, "0.00") '%Descuento
          Else
            gaList(gintCont, 5) = 0
          End If
          If IsNumeric(txtText1(26).Text) Then
            gaList(gintCont, 6) = Format(txtText1(26).Text, "0.00") '%Bonificaci�n
          Else
            gaList(gintCont, 6) = 0
          End If
          If IsNumeric(txtText1(32).Text) Then
            gaList(gintCont, 7) = txtText1(32).Text 'IVA
          Else
            gaList(gintCont, 7) = 0
          End If
          If IsNumeric(txtText1(31).Text) Then
            gaList(gintCont, 8) = txtText1(31).Text
          Else
            gaList(gintCont, 8) = 0
          End If
          If gaList(gintCont, 0) = 0 Then
            'Si no hay precio neto lo calculamos
            Dim curPB As Currency
            Dim curDes As Currency
            Dim curIVA As Currency
            curDes = Format((gaList(gintCont, 3) * gaList(gintCont, 5)) / 100, "0.00")
            curIVA = Format(((gaList(gintCont, 3) - curDes) * gaList(gintCont, 7)) / 100, "0.00")
            gaList(gintCont, 0) = Format(gaList(gintCont, 3) - curDes + curIVA, "0.000")
          End If
          gaList(gintCont, 9) = Fix((gaList(gintCont, 4) * gaList(gintCont, 6)) / 100) 'Unidades por bonificar
          gaList(gintCont, 10) = Format(gaList(gintCont, 0) * gaList(gintCont, 4), "0.000") 'Importe de la l�nea

        End If
      End If
     Else
      'MsgBox "No hay ning�n producto para generar el pedido", vbInformation, "Generar Pedido"
     End If
   
   End If
   
   If txtText1(8).Text <> "" Then
      'gintProdBuscado = txtText1(8).Text
      If Option1(0).Value = True Then
         gintCodProveedor = txtText1(3).Text
      ElseIf Option1(1).Value = True Then
         gintCodProveedor = txtText1(2).Text
      Else
         gintCodProveedor = txtText1(1).Text
      End If
      
      gstrMoneda = txtText1(27).Text
      
      gstrLlamador = "FrmConExi"
      Call objsecurity.LaunchProcess("FR0510")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      objWinInfo.DataRefresh
      gintCont = 0
      gstrLlamador = ""
      'Se vacia la lista
      For intInd = 1 To 250
        gaList(intInd, 2) = 0
      Next intInd
   Else
     'MsgBox "Debe seleccionar un producto apretando el bot�n 'Buscar'", vbInformation, "Lista de productos"
     'La lista est� vacia
     Call objsecurity.LaunchProcess("FR0517")
   End If
   cmdGenPedGrid.Enabled = True

End Sub


Private Sub cmdInList_Click()
  'Se a�ade un producto a la lista
  Dim intResp As Integer
  Dim intInd As Integer
  Dim blnYaEsta As Boolean
  
  blnYaEsta = False
  If Len(Trim(txtText1(8).Text)) > 0 Then
    If gintCont < 250 Then
      For intInd = 1 To gintCont
        'Se comprueba que el producto no est� en la lista
        If (gaList(intInd, 1) = txtText1(8).Text) And (gaList(intInd, 2) = -1) Then
          blnYaEsta = True
          Exit For
        End If
      Next intInd
      
      If blnYaEsta = False Then
        gintCont = gintCont + 1
        gaList(gintCont, 1) = txtText1(8).Text
        gaList(gintCont, 2) = -1
        
        If IsNumeric(txtText1(30).Text) Then
          gaList(gintCont, 0) = Format(txtText1(30).Text, "0.000") 'Precio Neto
        Else
          gaList(gintCont, 0) = 0
        End If
        If UCase(Left(Me.txtText1(35), 1)) = "Q" Then
          gaList(gintCont, 3) = Format(txtText1(33).Text, "0.000") 'Precio Base
        Else
          gaList(gintCont, 3) = Format(txtText1(34).Text, "0.000") 'PVL
        End If
        If IsNumeric(txtText1(36).Text) Then
          If txtText1(36).Text >= 0 Then
            gaList(gintCont, 4) = txtText1(36).Text 'Cantidad sugerida
          Else
            gaList(gintCont, 4) = 0 'Cantidad sugerida
          End If
        Else
          gaList(gintCont, 4) = 1
        End If
        If IsNumeric(txtText1(12).Text) Then
          gaList(gintCont, 5) = Format(txtText1(12).Text, , "0.00") '%Descuento
        Else
          gaList(gintCont, 5) = 0
        End If
        If IsNumeric(txtText1(26).Text) Then
          gaList(gintCont, 6) = Format(txtText1(26).Text, , "0.00") '%Bonificaci�n
        Else
          gaList(gintCont, 6) = 0
        End If
        If IsNumeric(txtText1(32).Text) Then
          gaList(gintCont, 7) = txtText1(32).Text 'IVA
        Else
          gaList(gintCont, 7) = 0
        End If
        If IsNumeric(txtText1(31).Text) Then
          gaList(gintCont, 8) = txtText1(31).Text 'U.E.
        Else
          gaList(gintCont, 8) = 1
        End If
        If gaList(gintCont, 0) = 0 Then
          'Si no hay precio neto lo calculamos
          Dim curPB As Currency
          Dim curDes As Currency
          Dim curIVA As Currency
          curDes = Format((gaList(gintCont, 3) * gaList(gintCont, 5)) / 100, "0.00")
          curIVA = Format(((gaList(gintCont, 3) - curDes) * gaList(gintCont, 7)) / 100, "0.00")
          gaList(gintCont, 0) = Format(gaList(gintCont, 3) - curDes + curIVA, "0.000")
        End If
        gaList(gintCont, 9) = Fix((gaList(gintCont, 8) * gaList(gintCont, 4) * gaList(gintCont, 6)) / 100) 'Unidades por bonificar
        gaList(gintCont, 10) = Format(gaList(gintCont, 0) * gaList(gintCont, 4), "0.000") 'Importe de la l�nea
        intResp = MsgBox("El producto ha sido a�adido a la lista", vbInformation, "Lista de productos")
      Else
        intResp = MsgBox("El producto ha sido a�adido a la lista", vbInformation, "Lista de productos")
      End If
    Else
      intResp = MsgBox("No se pueden seleccionar m�s de 250 productos a la vez", vbInformation, "A�adir a la lista")
    End If
  End If
  mblnHayFiltro = False
  cmdViewList.Caption = "&Ver Lista"
  cmdViewList.ToolTipText = "En la pantalla solo se ven los  productos de la lista con la que se genera el pedido"
End Sub

Private Sub cmdMasDatos_Click()
  Dim intCont As Integer
    intCont = gintCont
    cmdMasDatos.Enabled = False
    If txtText1(8).Text = "" Then
      MsgBox "Debe seleccionar un producto apretando el bot�n 'Buscar'", vbInformation, "Aviso"
    Else
      gstrLlamador = "FrmConExi"
      gstrCP = txtText1(8).Text
      Call objsecurity.LaunchProcess("FR0535")
    End If
    mblnPas = True
    cmdMasDatos.Enabled = True
    gintCont = intCont
End Sub

Private Sub cmdOutList_Click()
  'Se quita el elemento de la lista
  Dim intInd As Integer
  Dim intResp As Integer
  
  If Not IsNull(txtText1(8).Text) Then
    For intInd = 1 To gintCont
      If gaList(intInd, 1) = txtText1(8).Text Then
        'Se ha encontrado el elemento a eliminar de la lista
        gaList(intInd, 2) = 0
        intResp = MsgBox("El producto ha sido eliminado de la lista", vbInformation, "Lista de productos")
        Exit For
      End If
    Next intInd
  End If
End Sub

Private Sub cmdShowListGrid_Click()

End Sub

Private Sub cmdOutListGrid_Click()
 'Se quita el elemento de la lista
  Dim intInd As Integer
  Dim intResp As Integer
  Dim intisel As Integer
  Dim varBkmrk As Variant
  Dim intNTotalSelRows As Integer
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rstFR73 As rdoResultset
  Dim blnEntra As Boolean
 
  blnEntra = False
  strFR73 = "SELECT FR73CODPRODUCTO,FR00CODGRPTERAP,FR73PRECBASE,FR73PVL," & _
            "       FR73DESVIACION,FR73TAMENVASE,FR73DESCUENTO,FR73BONIFICACION,FR88CODTIPIVA " & _
            "  FROM FR7300 " & _
            " WHERE FR73CODINTFAR = ? "
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
  
  cmdOutListGrid.Enabled = False
  intNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
  If intNTotalSelRows > 0 Then
   For intisel = 0 To intNTotalSelRows - 1
    varBkmrk = grdDBGrid1(0).SelBookmarks(intisel)
    qryFR73(0) = grdDBGrid1(0).Columns("C�digo").CellValue(varBkmrk)
    Set rstFR73 = qryFR73.OpenResultset()
    blnEntra = True
    If Not rstFR73.EOF Then
     For intInd = 1 To gintCont
      If gaList(intInd, 1) = rstFR73.rdoColumns("FR73CODPRODUCTO").Value Then
        'Se ha encontrado el elemento a eliminar de la lista
        gaList(intInd, 2) = 0
        'intResp = MsgBox("El producto ha sido eliminado de la lista", vbInformation, "Lista de productos")
        Exit For
      End If
     Next intInd
    End If
   Next intisel
   intResp = MsgBox("El producto ha sido eliminado de la lista", vbInformation, "Lista de productos")
   If blnEntra = True Then
    qryFR73.Close
    Set qryFR73 = Nothing
    Set rstFR73 = Nothing
  End If
  End If
  cmdOutListGrid.Enabled = True
End Sub

Private Sub cmdViewList_Click()
  'Se modifica la clausula where para ver los productos seleccionado
  Dim strLista As String
  Dim intInd As Integer
  Dim blnPrim As Boolean
  Dim intResp As Integer
  
  Screen.MousePointer = vbHourglass
  If cmdViewListGrid.Caption = "&Ver Lista" Then
    blnPrim = True
    strLista = " ( "
    For intInd = 1 To gintCont
      If (gaList(intInd, 2) = -1) And (blnPrim = True) Then
        'El producto est� activamente seleccionado para hacer el pedido
        strLista = strLista & gaList(intInd, 1)
        blnPrim = False
      Else
        If (gaList(intInd, 2) = -1) And (blnPrim = False) Then
          strLista = strLista & "," & gaList(intInd, 1)
        End If
      End If
    Next intInd
    strLista = strLista & ") "
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    mstrWhere = objWinInfo.objWinActiveForm.strWhere
    cmdViewList.Caption = "&Ver Todos"
    cmdViewListGrid.Caption = "&Ver Todos"
    cmdViewList.ToolTipText = "En la pantalla se ven todos los  productos"
    mblnHayFiltro = True
    If blnPrim = False Then
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      objWinInfo.objWinActiveForm.strWhere = " FR73CODPRODUCTO IN " & strLista
      objWinInfo.DataRefresh
      tabTab1(0).Tab = 1
      objWinInfo.DataMoveFirst
    Else
      intResp = MsgBox("No hay productos en la lista", vbInformation, "Lista de productos")
    End If
  Else 'mblnHayFiltro = true
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.objWinActiveForm.strWhere = mstrWhere
    objWinInfo.DataRefresh
    cmdViewList.Caption = "&Ver Lista"
    cmdViewListGrid.Caption = "&Ver Lista"
    cmdViewList.ToolTipText = "En la pantalla solo se ven los  productos de la lista con la que se genera el pedido"
    Screen.MousePointer = vbDefault
  End If
   Screen.MousePointer = vbDefault
End Sub

Private Sub cmdViewListGrid_Click()
'Se modifica la clausula where para ver los productos seleccionado
  Dim strLista As String
  Dim intInd As Integer
  Dim blnPrim As Boolean
  Dim intResp As Integer
  
  Screen.MousePointer = vbHourglass
  cmdViewListGrid.Enabled = False
  If cmdViewListGrid.Caption = "&Ver Lista" Then
    blnPrim = True
    strLista = " ( "
    For intInd = 1 To gintCont
      If (gaList(intInd, 2) = -1) And (blnPrim = True) Then
        'El producto est� activamente seleccionado para hacer el pedido
        strLista = strLista & gaList(intInd, 1)
        blnPrim = False
      Else
        If (gaList(intInd, 2) = -1) And (blnPrim = False) Then
          strLista = strLista & "," & gaList(intInd, 1)
        End If
      End If
    Next intInd
    strLista = strLista & ") "
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    mstrWhere = objWinInfo.objWinActiveForm.strWhere
    cmdViewListGrid.Caption = "&Ver Todos"
    cmdViewList.Caption = "&Ver Todos"
    cmdViewListGrid.ToolTipText = "En la pantalla se todos los  productos"
    mblnHayFiltro = True
    If blnPrim = False Then
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      objWinInfo.objWinActiveForm.strWhere = " FR73CODPRODUCTO IN " & strLista
      objWinInfo.DataRefresh
      tabTab1(0).Tab = 1
      objWinInfo.DataMoveFirst
    Else
      intResp = MsgBox("No hay productos en la lista", vbInformation, "Lista de productos")
    End If
  Else 'mblnHayFiltro = true
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.objWinActiveForm.strWhere = mstrWhere
    objWinInfo.DataRefresh
    cmdViewListGrid.Caption = "&Ver Lista"
    cmdViewList.Caption = "&Ver Lista"
    cmdViewListGrid.ToolTipText = "En la pantalla solo se ven los  productos de la lista con la que se genera el pedido"
  End If
  cmdViewListGrid.Enabled = True
  Screen.MousePointer = vbDefault
End Sub


Private Sub Command1_Click()
  Dim strTextoABuscar As String
  Dim strCodGrpTerap As String
  Dim strDesvMayQue As String
  Dim strProdBajoStock As String
  Dim strABC_C As String
  Dim strABC_CE As String
  Dim strClausulaWhere As String
  Dim dblTP As Double
  Dim strCodIntFar As String
  Dim strCodSeguridad As String
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rstFR73 As rdoResultset
  Dim strCodProv As String
  Dim strInsFR62 As String
  Dim strDual As String
  Dim rstDual As rdoResultset
  Dim strCodPed As String
  Dim strInsFR25 As String
  Dim strCantPed As String
  Dim strPrecUni As String
  Dim strImpLin As String
  Dim strIVA As String
  Dim strPrecNet As String
  Dim dblAux1 As Double
  Dim dblAux2 As Double
  Dim strTpDesc, strRec As Currency
  Dim curPU, curTPDes, curRec, curIVA As Currency
  Dim blnSalir As Boolean
  Dim dblAux3 As Double
  Dim dblEnv As Double
  Dim dblExi As Currency
  Dim dblCantPte As Currency
  Dim dblPteBoni As Currency
  Dim dblPtoPed As Currency
  Dim dblTamPed As Currency
  Dim dblConsDia As Currency
  Dim curPrecNet As Currency
  Dim curCantPed As Currency
  Dim rsta As rdoResultset
  Dim stra As String
  Dim intnumregistros As Integer
  
  Screen.MousePointer = vbHourglass
  Command1.Enabled = False
  Me.Enabled = False
  strTextoABuscar = txtBusq1(8).Text
  strCodGrpTerap = txtBusq1(9).Text
  strDesvMayQue = txtBusq1(17).Text
  strProdBajoStock = chkBusq1(4).Value
  strABC_C = cboBusq1(1).Value
  strABC_CE = cboBusq1(2).Value
  strCodIntFar = txtBusq1(2).Text
  strCodSeguridad = txtBusq1(1).Text
  
  strClausulaWhere = " -1=-1 "
  
  If strTextoABuscar <> "" Then
      If Option2(0).Value = True Then
         ' Producto
         strClausulaWhere = strClausulaWhere & " AND (upper(FR73DESPRODUCTOPROV) LIKE upper('%" & strTextoABuscar & "%') OR upper(FR73DESPRODUCTO) LIKE upper('%" & strTextoABuscar & "%'))"
      ElseIf Option2(1).Value = True Then
         ' Referencia
         strClausulaWhere = strClausulaWhere & " AND upper(FR73REFERENCIA) LIKE upper('%" & strTextoABuscar & "%') "
      ElseIf Option2(2).Value = True Then
         'Principio Activo
         strClausulaWhere = strClausulaWhere & " AND (FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400,FR6800 WHERE FR6400.FR68CODPRINCACTIV=FR6800.FR68CODPRINCACTIV AND UPPER(FR68DESPRINCACTIV) LIKE UPPER('%" & strTextoABuscar & "%') )" & _
                 " OR " & _
                 "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400 WHERE FR68CODPRINCACTIV IN (SELECT FR68CODPRINCACTIV FROM FRH400 WHERE UPPER(FRH4DESSINONIMO) LIKE UPPER('%" & strTextoABuscar & "%')))" & _
                 ") "
      ElseIf Option2(4).Value = True Then
        'C�d. Proveedor
        If IsNumeric(strTextoABuscar) Then
          strClausulaWhere = strClausulaWhere & " AND FR79CODPROVEEDOR_A = " & strTextoABuscar
        Else
          strClausulaWhere = strClausulaWhere & " AND FR79CODPROVEEDOR_A IS NULL "
        End If
      ElseIf Option2(3).Value = True Then
          strClausulaWhere = strClausulaWhere & " AND ( "
          strClausulaWhere = strClausulaWhere & " FR79CODPROVEEDOR_A IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_B IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " OR FR79CODPROVEEDOR_C IN (SELECT FR79CODPROVEEDOR FROM FR7900 WHERE upper(FR79PROVEEDOR) LIKE upper('%" & strTextoABuscar & "%')) "
          strClausulaWhere = strClausulaWhere & " ) "
      End If
  End If
    
  If strCodGrpTerap <> "" Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND upper(FR00CODGRPTERAP) LIKE upper('%" & strCodGrpTerap & "%') "
    Else
      strClausulaWhere = " upper(FR00CODGRPTERAP) LIKE upper('%" & strCodGrpTerap & "%') "
    End If
  End If
  
  If strProdBajoStock = 1 Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73EXISTENCIAS<=FR73STOCKMIN "
    Else
      strClausulaWhere = " FR73EXISTENCIAS<=FR73STOCKMIN "
    End If
  End If
  
  If strABC_C <> "" Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73ABCCONS='" & strABC_C & "' "
    Else
      strClausulaWhere = " FR73ABCCONS='" & strABC_C & "' "
    End If
  End If

  If strABC_CE <> "" Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73ABCCONS='" & strABC_CE & "' "
    Else
      strClausulaWhere = " FR73ABCCONS='" & strABC_CE & "' "
    End If
  End If
  
  If strCodIntFar <> "" Then
   strClausulaWhere = strClausulaWhere & " AND FR73CODINTFAR LIKE '%" & strCodIntFar & "%' "
  End If
  
  If strCodSeguridad <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FR73CODINTFARSEG=" & strCodSeguridad & " "
  End If


  If strDesvMayQue <> "" Then
    If strClausulaWhere <> "" Then
      strClausulaWhere = strClausulaWhere & " AND FR73DESVIACION>" & strDesvMayQue & " "
    Else
      strClausulaWhere = " FR73DESVIACION>" & strDesvMayQue & " "
    End If
  End If
  '++++++++++++++++++++++++++++++++++++++++++++++++
  stra = "SELECT COUNT(*) FROM FR7316J WHERE "
  stra = stra & strClausulaWhere & " AND (FR73FECFINVIG IS NULL OR FR73FECFINVIG>(SELECT SYSDATE FROM DUAL)) AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL) "
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not IsNull(rsta.rdoColumns(0).Value) Then
    intnumregistros = rsta.rdoColumns(0).Value
    objWinInfo.objWinActiveForm.intCursorSize = intnumregistros
  End If
  rsta.Close
  Set rsta = Nothing
  '++++++++++++++++++++++++++++++++++++++++++++++
  objWinInfo.objWinActiveForm.strWhere = strClausulaWhere & " AND (FR73FECFINVIG IS NULL OR FR73FECFINVIG>(SELECT SYSDATE FROM DUAL)) AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL) "
  Call objWinInfo.DataRefresh
  If intnumregistros < 18 Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  End If
  
  If chkBusq1(0).Value = 1 Then
    'Se generan los pedidos
    If IsNumeric(Trim(txtBusq1(0).Text)) Then
      dblTP = Trim(Fix(txtBusq1(0).Text))
    Else
      dblTP = 0
    End If
        
    strFR73 = "SELECT FR73CODPRODUCTO,FR73TAMENVASE,FR73DESCUENTO,FR79CODPROVEEDOR_A, " & _
              "       FR73BONIFICACION,FR88CODTIPIVA,FR73PRECIONETCOMPRA,FR73DIASSEG,FR73TAMPEDI," & _
              "       FR73RECARGO,FR73DESCUENTO,FR73INDGESTFIJA,FR73TIEMPOREPOSI,FR73INDGESTCONS,FR73INDPROG,FR73INDGESTFIJA,FR73CONSDIARIO,FR73DESVIACION,FR73EXISTENCIAS,FR73CANTPEND,FR73PTEBONIF,FR73PTOPED,FR00CODGRPTERAP,FR73PRECBASE,FR73PVL " & _
              "  FROM FR7300 " & _
              " WHERE (FR73INDGESTFIJA = ? OR FR73INDGESTCONS = ?  OR FR73INDPROG = ?)" & _
              "   AND FR73INDTRANS = ? " & _
              "   AND FR73INDVISIBLE = ? " & _
              "   AND (NOT (FR79CODPROVEEDOR_A IS NULL)) " & _
              "   AND FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL) " & _
              "   AND ((FR73EXISTENCIAS+FR73CANTPEND)-FR73PTOPED < 0) AND " & strClausulaWhere & _
              "   AND FR7300.FR73CODPRODUCTO NOT IN (SELECT FR73CODPRODUCTO FROM FRN100,FRN200 WHERE FR95CODESTPEDCOMPRA = 9 AND FRN100.FRN1CODPEDCOMPRA = FRN200.FRN1CODPEDCOMPRA) " & _
              " ORDER BY FR79CODPROVEEDOR_A ASC "
    Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
    'qryFR73(0) = dblTP
    'qryFR73(1) = 100
    qryFR73(0) = -1
    'qryFR73(3) = dblTP
    'qryFR73(4) = 100
    qryFR73(1) = -1
    qryFR73(2) = -1
    qryFR73(3) = 0
    qryFR73(4) = -1
    Set rstFR73 = qryFR73.OpenResultset()
    While Not rstFR73.EOF
      strCodProv = rstFR73.rdoColumns("FR79CODPROVEEDOR_A").Value
      strDual = "SELECT FRN1CODPEDCOMPRA_SEQUENCE.NEXTVAL FROM DUAL"
      Set rstDual = objApp.rdoConnect.OpenResultset(strDual)
      strCodPed = rstDual.rdoColumns(0).Value
      rstDual.Close
      Set rstDual = Nothing
      strInsFR62 = "INSERT INTO FRN100 (FRN1CODPEDCOMPRA,FRN1FECPEDCOMPRA,FR95CODESTPEDCOMPRA," & _
                    "SG02COD_PID,FRN1INDREPRE,FR79CODPROVEEDOR) VALUES (" & _
                     strCodPed & ",SYSDATE,9,'" & _
                     objsecurity.strUser & "',1," & strCodProv & ")"
      objApp.rdoConnect.Execute strInsFR62, 64
      blnSalir = False
      While Not rstFR73.EOF And Not blnSalir
        If Not IsNull(rstFR73.rdoColumns("FR73TAMENVASE").Value) Then
          If rstFR73.rdoColumns("FR73TAMENVASE").Value > 0 Then
            dblEnv = rstFR73.rdoColumns("FR73TAMENVASE").Value
          Else
            dblEnv = 1
          End If
        Else
          dblEnv = 1
        End If
        If rstFR73.rdoColumns("FR73INDGESTFIJA").Value = -1 Then
          'Es de Gesti�n Fija
          If Not IsNull(rstFR73.rdoColumns("FR73TAMPEDI").Value) Then
            If rstFR73.rdoColumns("FR73TAMPEDI").Value > 0 Then
              dblAux1 = rstFR73.rdoColumns("FR73TAMPEDI").Value
            Else
              dblAux1 = 1
            End If
          Else
            dblAux1 = 1
          End If
          If Not IsNull(rstFR73.rdoColumns("FR73DIASSEG").Value) Then
            If rstFR73.rdoColumns("FR73DIASSEG").Value > 0 Then
              dblAux2 = rstFR73.rdoColumns("FR73DIASSEG").Value
            Else
              dblAux2 = 1
            End If
          Else
            dblAux2 = 1
          End If
          If Not IsNull(rstFR73.rdoColumns("FR73TIEMPOREPOSI").Value) Then
            If rstFR73.rdoColumns("FR73TIEMPOREPOSI").Value > 0 Then
              dblAux3 = rstFR73.rdoColumns("FR73TIEMPOREPOSI").Value
            Else
              dblAux3 = 1
            End If
          Else
            dblAux3 = 1
          End If
          dblAux2 = 1
          dblAux3 = 1
          
          strCantPed = objGen.ReplaceStr(Format(dblAux1, "0.00"), ",", ".", 1)
          'strCantPed = objGen.ReplaceStr(Format(dblAux1 * dblAux2 * dblAux3, "0.00"), ",", ".", 1)
        Else
          If rstFR73.rdoColumns("FR73INDGESTCONS").Value = -1 Then
            'Gesti�n por consumos
            If Not IsNull(rstFR73.rdoColumns("FR73CONSDIARIO").Value) Then
              If rstFR73.rdoColumns("FR73CONSDIARIO").Value > 0 Then
                dblAux1 = rstFR73.rdoColumns("FR73CONSDIARIO").Value
              Else
                dblAux1 = 1
              End If
            Else
              dblAux1 = 1
            End If
            
            If Not IsNull(rstFR73.rdoColumns("FR73DIASSEG").Value) Then
              If rstFR73.rdoColumns("FR73DIASSEG").Value > 0 Then
                dblAux2 = rstFR73.rdoColumns("FR73DIASSEG").Value
              Else
                dblAux2 = 1
              End If
            Else
              dblAux2 = 1
            End If
            If Not IsNull(rstFR73.rdoColumns("FR73TIEMPOREPOSI").Value) Then
              If rstFR73.rdoColumns("FR73TIEMPOREPOSI").Value > 0 Then
                dblAux3 = rstFR73.rdoColumns("FR73TIEMPOREPOSI").Value
              Else
                dblAux3 = 1
              End If
            Else
              dblAux3 = 1
            End If
            If Not IsNull(rstFR73.rdoColumns("FR73DESVIACION").Value) Then
              If rstFR73.rdoColumns("FR73DESVIACION").Value > 0 Then
                dblAux1 = rstFR73.rdoColumns("FR73DESVIACION").Value
              Else
                If rstFR73.rdoColumns("FR73DESVIACION").Value = 0 Then
                  dblAux1 = 1
                Else
                  dblAux1 = rstFR73.rdoColumns("FR73DESVIACION").Value * (-1)
                End If
              End If
            Else
              dblAux1 = 1
            End If
                        
            If Not IsNull(rstFR73.rdoColumns("FR73TAMPEDI").Value) Then
              If rstFR73.rdoColumns("FR73TAMPEDI").Value > 0 Then
                dblTamPed = rstFR73.rdoColumns("FR73TAMPEDI").Value
              Else
                If rstFR73.rdoColumns("FR73TAMPEDI").Value = 0 Then
                  dblTamPed = 1
                Else
                  dblTamPed = rstFR73.rdoColumns("FR73TAMPEDI").Value
                End If
              End If
            Else
              dblTamPed = 1
            End If
            
            If Not IsNull(rstFR73.rdoColumns("FR73CONSDIARIO").Value) Then
              If rstFR73.rdoColumns("FR73CONSDIARIO").Value > 0 Then
                dblConsDia = rstFR73.rdoColumns("FR73CONSDIARIO").Value
              Else
                If rstFR73.rdoColumns("FR73CONSDIARIO").Value = 0 Then
                  dblConsDia = 1
                Else
                  dblConsDia = rstFR73.rdoColumns("FR73CONSDIARIO").Value
                End If
              End If
            Else
              dblConsDia = 1
            End If
            
            If Not IsNull(rstFR73.rdoColumns("FR73EXISTENCIAS").Value) Then
              If rstFR73.rdoColumns("FR73EXISTENCIAS").Value > 0 Then
                dblExi = rstFR73.rdoColumns("FR73EXISTENCIAS").Value
              Else
                If rstFR73.rdoColumns("FR73EXISTENCIAS").Value = 0 Then
                  'dblExi = 1
                  dblExi = 0
                Else
                  dblExi = rstFR73.rdoColumns("FR73EXISTENCIAS").Value
                End If
              End If
            Else
              'dblExi = 1
              dblExi = 0
            End If
            
            If Not IsNull(rstFR73.rdoColumns("FR73CANTPEND").Value) Then
              If rstFR73.rdoColumns("FR73CANTPEND").Value > 0 Then
                dblCantPte = rstFR73.rdoColumns("FR73CANTPEND").Value
              Else
                If rstFR73.rdoColumns("FR73CANTPEND").Value = 0 Then
                  'dblCantPte = 1
                  dblCantPte = 0
                Else
                  dblCantPte = rstFR73.rdoColumns("FR73CANTPEND").Value
                End If
              End If
            Else
              'dblCantPte = 1
              dblCantPte = 0
            End If
            
            If Not IsNull(rstFR73.rdoColumns("FR73PTEBONIF").Value) Then
              If rstFR73.rdoColumns("FR73PTEBONIF").Value > 0 Then
                dblPteBoni = rstFR73.rdoColumns("FR73PTEBONIF").Value
              Else
                If rstFR73.rdoColumns("FR73PTEBONIF").Value = 0 Then
                  'dblPteBoni = 1
                  dblPteBoni = 0
                Else
                  dblPteBoni = rstFR73.rdoColumns("FR73PTEBONIF").Value
                End If
              End If
            Else
              'dblPteBoni = 1
              dblPteBoni = 0
            End If
            
            If Not IsNull(rstFR73.rdoColumns("FR73PTOPED").Value) Then
              If rstFR73.rdoColumns("FR73PTOPED").Value > 0 Then
                dblPtoPed = rstFR73.rdoColumns("FR73PTOPED").Value
              Else
                If rstFR73.rdoColumns("FR73PTOPED").Value = 0 Then
                  'dblPtoPed = 1
                  dblPtoPed = 0
                Else
                  dblPtoPed = rstFR73.rdoColumns("FR73PTOPED").Value
                End If
              End If
            Else
              'dblPtoPed = 1
              dblPtoPed = 0
            End If
  
            strCantPed = objGen.ReplaceStr(Format(((dblPtoPed + (dblConsDia * dblTamPed)) - (dblExi + dblCantPte + dblPteBoni)) / dblEnv, "0.00"), ",", ".", 1)
            'strCantPed = objGen.ReplaceStr(Format(((dblAux1 + dblAux2) * dblAux3) / dblEnv, "0.00"), ",", ".", 1)
          End If
        End If
        
        If Not IsNull(rstFR73.rdoColumns("FR00CODGRPTERAP").Value) Then
          If UCase(Left(rstFR73.rdoColumns("FR00CODGRPTERAP").Value, 1)) = "Q" Then
            If Not IsNull(rstFR73.rdoColumns("FR73PRECBASE").Value) Then
              strPrecUni = objGen.ReplaceStr(Format(rstFR73.rdoColumns("FR73PRECBASE").Value, "0.00"), ",", ".", 1)
            Else
              strPrecUni = 0
            End If
          Else
            If Not IsNull(rstFR73.rdoColumns("FR73PVL").Value) Then
              strPrecUni = objGen.ReplaceStr(Format(rstFR73.rdoColumns("FR73PVL").Value, "0.00"), ",", ".", 1)
            Else
              strPrecUni = 0
            End If
          End If
        End If
        
        If Not IsNull(rstFR73.rdoColumns("FR88CODTIPIVA").Value) Then
          strIVA = rstFR73.rdoColumns("FR88CODTIPIVA").Value
        Else
          strIVA = 0
        End If
         
        If Not IsNull(rstFR73.rdoColumns("FR73BONIFICACION").Value) Then
          strRec = rstFR73.rdoColumns("FR73BONIFICACION").Value
        Else
          strRec = 0
        End If
        If Not IsNull(rstFR73.rdoColumns("FR73DESCUENTO").Value) Then
          strTpDesc = rstFR73.rdoColumns("FR73DESCUENTO").Value
        Else
          strTpDesc = 0
        End If
        curPU = strPrecUni
        curTPDes = strTpDesc
        curRec = strRec
        curIVA = strIVA
        curPrecNet = calcular_precio_neto(objGen.ReplaceStr(curPU, ".", ",", 1), curTPDes, curRec, curIVA)
        strPrecNet = objGen.ReplaceStr(Format(curPrecNet, "0.00"), ",", ".", 1)
        'strImpLin = objGen.ReplaceStr(Format(fix, "0.00"), ",", ".", 1)
        'strImpLin = objGen.ReplaceStr(Format((calcular_precio_neto(Fix(objGen.ReplaceStr(curPU, ".", ",", 1)), curTPDes, curRec, curIVA)) * (Format(dblAux1 * dblAux2, "0.00")), "0.00"), ",", ".", 1)
        If strCantPed < 0 Then
          strCantPed = Fix(objGen.ReplaceStr(strCantPed, ".", ",", 1)) * -1
        Else
          strCantPed = Fix(objGen.ReplaceStr(strCantPed, ".", ",", 1))
        End If
        strImpLin = objGen.ReplaceStr(Format(strCantPed * objGen.ReplaceStr(strPrecNet, ".", ",", 1), "0.00"), ",", ".", 1)
        
        strInsFR25 = "INSERT INTO FRN200 (FRN1CODPEDCOMPRA,FRN2CODDETPEDCOMP,FR73CODPRODUCTO, " & _
                     "FRN2CANTPEDIDA,FRN2PRECIOUNIDAD,FRN2IMPORLINEA," & _
                     "FRN2TAMENVASE,FRN2DESCUENTO,FRN2BONIF," & _
                     "FR88CODTIPIVA,FRN2PRECNET) VALUES (" & _
                     strCodPed & ",FRN2CODDETPEDCOMP_SEQUENCE.NEXTVAL," & rstFR73.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                     strCantPed & "," & strPrecUni & "," & strImpLin & "," & _
                     objGen.ReplaceStr(rstFR73.rdoColumns("FR73TAMENVASE").Value, ",", ".", 1) & "," & objGen.ReplaceStr(strTpDesc, ",", ".", 1) & "," & objGen.ReplaceStr(strRec, ",", ".", 1) & "," & _
                     strIVA & "," & strPrecNet & ")"
        
        objApp.rdoConnect.Execute strInsFR25, 64
        rstFR73.MoveNext
        If Not rstFR73.EOF Then
          If strCodProv = rstFR73.rdoColumns("FR79CODPROVEEDOR_A").Value Then
            blnSalir = False
          Else
            blnSalir = True
          End If
        Else
          blnSalir = True
        End If
      Wend
    Wend
    qryFR73.Close
    Set qryFR73 = Nothing
    Set rstFR73 = Nothing
    'MsgBox "Se han generado los Pedidos", vbInformation, "Generar Pedidos"
    Screen.MousePointer = vbDefault
    Call objsecurity.LaunchProcess("FR0991")
    'Call objsecurity.LaunchProcess("FR0517")
  End If
  Command1.Enabled = True
  Screen.MousePointer = vbDefault
  
  Me.Enabled = True
  cmdGenerar.SetFocus
End Sub


Private Sub Form_Activate()
   gintprodbuscado = ""
   gintCont = 0
   mblnHayFiltro = False
   mblnPas = False
   
   'se limpian los buscadores
   txtBusq1(8).Text = ""
   txtBusq1(2).Text = ""
   txtBusq1(1).Text = ""
   txtBusq1(9).Text = ""
   txtBusq1(17).Text = ""
   txtBusq1(0).Text = ""
   
   cboBusq1(1).Text = ""
   cboBusq1(2).Text = ""
   
   chkBusq1(4).Value = 0
   chkBusq1(0).Value = 0
   
   Option2(0).Value = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
   
  With objDetailInfo
    .strName = "Medicamentos"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR7316J"
    
    .blnAskPrimary = False
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 225
    '.strWhere = " FR00CODGRPTERAP NOT LIKE 'Q%' OR FR00CODGRPTERAP IS NULL "
    .strWhere = " FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
    Call .FormAddOrderField("FR79CODPROVEEDOR_A", cwAscending)
    Call .FormAddOrderField("FR73DESPRODUCTOPROV", cwAscending)
    
    Call .objPrinter.Add("FR5161", "Productos Bajo Stock")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")

    Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno", cwString)
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTOPROV", "Descripci�n Producto", cwString)
    Call .FormAddFilterOrder(strKey, "FR73CODINTFAR", "C�digo Interno")
    Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTOPROV", "Descripci�n Producto")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)

    .CtrlGetInfo(txtText1(8)).blnInGrid = False
    
      
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    
    .CtrlGetInfo(txtProv1(59)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(31)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(30)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(32)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(33)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(34)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(37)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(35)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(38)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(36)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(57)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(40)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(39)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(41)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(42)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(43)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(46)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(44)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(47)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(45)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(48)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(56)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(58)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(55)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(54)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(53)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(50)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(52)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(49)).blnNegotiated = False
    .CtrlGetInfo(txtProv1(51)).blnNegotiated = False
    '*.CtrlGetInfo(txtText1(36)).blnNegotiated = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "FR79CODPROVEEDOR_A", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(15), "FR79PROVEEDOR")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR79CODPROVEEDOR_B", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(28), "FR79PROVEEDOR")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR79CODPROVEEDOR_C", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(29), "FR79PROVEEDOR")
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR73CODPRODUCTO", "SELECT * FROM FR7322J WHERE FR73CODPRODUCTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(40), "SUMA")
    
    Call .WinRegister
    Call .WinStabilize
  End With




End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
  If strFormName = "Medicamentos" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = "WHERE FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
     
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno Producto"
         
     Set objField = .AddField("FR73DESPRODUCTOPROV")
     objField.strSmallDesc = "Descripci�n Producto"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad Medida"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  Dim dblPedir As Double
  Dim blnTamDef As Boolean
  
If grdDBGrid1(0).Rows > 0 Then
  'If grdDBGrid1(0).Columns("CodPrd").Visible = True Then
  '  grdDBGrid1(0).Columns("CodPrd").Visible = False
  'End If
  'grdDBGrid1(0).Columns("CodPrd").Visible = False
  'grdDBGrid1(0).Columns("Grupo Terap�utico").Visible = False
 'If mblnPas = False Then
  mblnPas = True
  On Error GoTo err1
  grdDBGrid1(0).Columns("C�digo").Caption = "C�digo" 'Cod.Int
  grdDBGrid1(0).Columns("C�digo").Width = 740 'Cod.Int
  grdDBGrid1(0).Columns("Seg").Visible = False 'C.S.
  grdDBGrid1(0).Columns("Producto").Width = 3000 'Descripcion Mat.Sanitario
  'grdDBGrid1(0).Columns("F.F.").Width = 1095 'F.F.
  grdDBGrid1(0).Columns("Dosis").Width = 650 'Dosis
  'grdDBGrid1(0).Columns("U.M.").Width = 1095 'U.M.
  grdDBGrid1(0).Columns("Vol.").Width = 650 'Vol.
  grdDBGrid1(0).Columns("Referencia").Width = 1095 'Referencia
  grdDBGrid1(0).Columns("Existencias").Caption = "Exist." 'Existencias
  grdDBGrid1(0).Columns("Exist.").Width = 510 'Existencias
  grdDBGrid1(0).Columns("Stock m�nimo").Caption = "Stock Min" 'Stock Min.
  grdDBGrid1(0).Columns("Stock Min").Width = 900 'Stock Min.
  grdDBGrid1(0).Columns("Cantidad pendiente de recibir").Caption = "Pdte.Recibir" 'Pdte.Recibir
  grdDBGrid1(0).Columns("Pdte.Recibir").Width = 1019 'Pdte.Recibir
  grdDBGrid1(0).Columns("Consumo mensual").Caption = "Cons.Mens." 'Cons.Mensual
  grdDBGrid1(0).Columns("Cons.Mens.").Width = 976 'Cons.Mensual
  grdDBGrid1(0).Columns("Pto.Ped").Width = 900 'Pto de pedido
  grdDBGrid1(0).Columns("SugPed").Width = 900 'Cantidad Sugerida
  grdDBGrid1(0).Columns("Pendiente Bonificar").Caption = "Pdte.Bonif." 'Pdte.Bonificar
  grdDBGrid1(0).Columns("Pdte.Bonif.").Width = 900 'Pdte.Bonificar
  grdDBGrid1(0).Columns("Precio de la �ltima entrada").Caption = "P.Ult.Ent." 'P.Ult.Ent.
  grdDBGrid1(0).Columns("P.Ult.Ent.").Width = 1200 'P.Ult.Ent.
  grdDBGrid1(0).Columns("U.E.").Caption = "UE"
  grdDBGrid1(0).Columns("UE").Width = 400
  grdDBGrid1(0).Columns("Mon.").Visible = False
  grdDBGrid1(0).Columns("Consumo Diario").Caption = "Co/D�a"
  grdDBGrid1(0).Columns("Co/D�a").Width = 700
  grdDBGrid1(0).Columns("D�as de Seguridad").Caption = "D.Seg."
  grdDBGrid1(0).Columns("D.Seg.").Width = 700
  grdDBGrid1(0).Columns("Tanto por ciento de Descuento").Caption = "%Dto."
  grdDBGrid1(0).Columns("%Dto.").Width = 700
  grdDBGrid1(0).Columns("Precio Base").Caption = "P.Base"
  grdDBGrid1(0).Columns("P.Base").Width = 900
  grdDBGrid1(0).Columns("Tama�o del pedido en d�as").Caption = "Tam.Ped."
  grdDBGrid1(0).Columns("Tam.Ped.").Width = 900
  grdDBGrid1(0).Columns("Precio Oferta de Compra").Caption = "Prec.Ofr."
  grdDBGrid1(0).Columns("Prec.Ofr.").Width = 900
  grdDBGrid1(0).Columns("Tanto por ciento de Bonificaci�n").Caption = "%Bon."
  grdDBGrid1(0).Columns("%Bon.").Width = 700
  grdDBGrid1(0).Columns("PVL").Caption = "PVL"
  grdDBGrid1(0).Columns("PVL").Width = 700
  grdDBGrid1(0).Columns("Stock m�ximo").Caption = "Stock M�x."
  grdDBGrid1(0).Columns("Stock M�x.").Width = 900
  grdDBGrid1(0).Columns("Consumo Anual").Caption = "Co/A�o"
  grdDBGrid1(0).Columns("Co/A�o").Width = 800
  grdDBGrid1(0).Columns("Consumo Regular").Caption = "C.R."
  grdDBGrid1(0).Columns("C.R.").Width = 400
  grdDBGrid1(0).Columns("Tanto por ciento de Rappel").Caption = "%Rappel"
  grdDBGrid1(0).Columns("%Rappel").Width = 800
  'grdDBGrid1(0).Columns("IVA").Caption = "IVA"
  grdDBGrid1(0).Columns("Precio Neto").Caption = "Prec.Neto"
  grdDBGrid1(0).Columns("Prec.Neto").Width = 900
  grdDBGrid1(0).Columns("C�d.Prov.A").Caption = "Prov.A"
  grdDBGrid1(0).Columns("Prov.A").Width = 800
  grdDBGrid1(0).Columns("Proveedor A").Caption = "Nom.Prov.A"
  grdDBGrid1(0).Columns("Nom.Prov.A").Width = 1200
  grdDBGrid1(0).Columns("C�d.Prov.B").Caption = "Prov.b"
  grdDBGrid1(0).Columns("Prov.b").Width = 800
  grdDBGrid1(0).Columns("Proveedor B").Caption = "Nom.Prov.B"
  grdDBGrid1(0).Columns("Nom.Prov.B").Width = 1200
  grdDBGrid1(0).Columns("C�d.Prov.C").Caption = "Prov.C"
  grdDBGrid1(0).Columns("Prov.C").Width = 800
  grdDBGrid1(0).Columns("Proveedor C").Caption = "Nom.Prov.C"
  grdDBGrid1(0).Columns("Nom.Prov.C").Width = 1200
  grdDBGrid1(0).Columns("Grupo Terap�utico").Caption = "Grupo Terap."
  grdDBGrid1(0).Columns("Grupo Terap.").Width = 1800
  'grdDBGrid1(0).Columns("Desv.").Caption = "Ped.Seg."
  grdDBGrid1(0).Columns("Desv.").Caption = "Desv."
  grdDBGrid1(0).Columns("Desv.").Width = 800
  grdDBGrid1(0).Columns("Gesti�n por consumos").Caption = "GPC"
  grdDBGrid1(0).Columns("GPC").Width = 500
  grdDBGrid1(0).Columns("Transito").Caption = "Trans."
  grdDBGrid1(0).Columns("Trans.").Width = 500
  grdDBGrid1(0).Columns("Programado").Caption = "Prog."
  grdDBGrid1(0).Columns("Prog.").Width = 500
  grdDBGrid1(0).Columns("Gesti�n Fija").Caption = "GF"
  grdDBGrid1(0).Columns("GF").Width = 500
 'End If
err1:
  On Error GoTo error2
  grdDBGrid1(0).Columns("C�digo").Position = 1
  grdDBGrid1(0).Columns("Producto").Position = 2
  grdDBGrid1(0).Columns("Referencia").Position = 3
  grdDBGrid1(0).Columns("Exist.").Position = 4
  grdDBGrid1(0).Columns("Pdte.Recibir").Position = 5
  grdDBGrid1(0).Columns("Cons.Mens.").Position = 6
  grdDBGrid1(0).Columns("Pto.Ped").Position = 7
  grdDBGrid1(0).Columns("SugPed").Position = 8
  grdDBGrid1(0).Columns("UE").Position = 9
error2:
End If
  blnTamDef = False
  If chkCheck1(0).Value = Checked Then
    'Gesti�n por consumos
    If IsNumeric(txtText1(31).Text) And IsNumeric(txtText1(37).Text) Then
      If txtText1(31).Text > 0 Then
        dblPedir = (txtText1(37).Text \ txtText1(31).Text) 'desviaci�n/tama�o envase
        If dblPedir < 0 Then
          dblPedir = dblPedir * (-1)
        End If
        If IsNumeric(txtText1(20).Text) And IsNumeric(txtText1(18).Text) Then
          'Si las existencias y el stock m�nimo son n�meros
          If txtText1(18).Text = 0 Then
            '*txtText1(36).Text = 0
            blnTamDef = True
          Else
            '*txtText1(36).Text = dblPedir + 1
            blnTamDef = True
          End If
        Else
          '*txtText1(36).Text = dblPedir + 1
          blnTamDef = True
        End If
      End If
    End If
  Else
    If chkCheck1(1).Value = 1 Then
      'Transito
      '*txtText1(36).Text = 1
      blnTamDef = True
    Else
      If (chkCheck1(2).Value = 1) Or (chkCheck1(3).Value = 1) Then
        'Programado o de gesti�n fija
        If IsNumeric(txtText1(23).Text) Then
          If IsNumeric(txtText1(13).Text) Then
            'Tama�o del pedido por el consumo diario
            '*txtText1(36).Text = Fix(txtText1(23).Text * txtText1(13).Text)
            blnTamDef = True
          Else
            '*txtText1(36).Text = Fix(txtText1(23).Text)
            blnTamDef = True
          End If
        Else
          '*txtText1(36).Text = 0
          blnTamDef = True
        End If
      End If
    End If
  End If
  If blnTamDef = False Then
    '*txtText1(36).Text = 0
  End If
End Sub
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strProveedor As String
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  If UCase(strListado) = "FR5161.RPT" Then
      strWhere = "{FR7900.FR79CODPROVEEDOR} = {FR7316J.FR79CODPROVEEDOR_A} AND " & _
                 "{FR7316J.FR73CODPRODUCTO} = {FR7322J.FR73CODPRODUCTO} AND " & _
                 "{FR7316J.FR73EXISTENCIAS} <= {FR7316J.FR73STOCKMIN}"
      If Len(Trim(txtBusq1(9).Text)) > 0 Then
        If Option2(0).Value = True Then
          'Producto
          strWhere = strWhere & " AND ({FR7316J.FR73DESPRODUCTO} startswith " & Chr(34) & txtBusq1(9).Text & Chr(34) & " OR {FR7316J.FR73DESPRODUCTOPROV} startswith " & Chr(34) & txtBusq1(9).Text & Chr(34) & ")"
        ElseIf Option2(3).Value = True Then
          'Nombre Proveedor
          strWhere = strWhere & " AND {FR7900.FR79PROVEEDOR} startswith " & Chr(34) & txtBusq1(9).Text & Chr(34)
        ElseIf Option2(4).Value = True Then
          'C�digo Proveedor
          If IsNumeric(txtBusq1(9).Text) Then
            strWhere = strWhere & " AND {FR7900.FR79CODPROVEEDOR} = " & txtBusq1(9).Text
          End If
        End If
      End If
      CrystalReport1.SelectionFormula = strWhere
  End If
  
  On Error GoTo Err_imp4
  CrystalReport1.Action = 1
Err_imp4:

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strOrden As String
  Dim strDes As String
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  'strDes = objPrinter.Destination
  Select Case intReport
    Case 1
     Call Imprimir("FR5161.RPT", 0)
  End Select
  Set objPrinter = Nothing
End Sub

Private Sub Option1_Click(Index As Integer)
  Dim rstp As rdoResultset
  Dim strP As String
  Dim qryFR79 As rdoQuery

  Select Case Index
  Case 0
    If txtText1(3).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(3).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
       If Not IsNull(rstp("FR79COMENTARIOS").Value) Then
        txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION1").Value) Then
        txtProv1(31).Text = rstp("FR79DIRECCION1").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO1").Value) Then
        txtProv1(30).Text = rstp("FR79DISTRITO1").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD1").Value) Then
        txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA1").Value) Then
        txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
       End If
       If Not IsNull(rstp("FR79PAIS1").Value) Then
        txtProv1(34).Text = rstp("FR79PAIS1").Value
       End If
       If Not IsNull(rstp("FR79TFNO1").Value) Then
        txtProv1(37).Text = rstp("FR79TFNO1").Value
       End If
       If Not IsNull(rstp("FR79TFNO11").Value) Then
        txtProv1(35).Text = rstp("FR79TFNO11").Value
       End If
       If Not IsNull(rstp("FR79FAX1").Value) Then
        txtProv1(38).Text = rstp("FR79FAX1").Value
       End If
       If Not IsNull(rstp("FR79EMAIL1").Value) Then
        txtProv1(36).Text = rstp("FR79EMAIL1").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT2").Value) Then
        txtProv1(57).Text = rstp("FR79REPRESENT2").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION2").Value) Then
        txtProv1(40).Text = rstp("FR79DIRECCION2").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO2").Value) Then
        txtProv1(39).Text = rstp("FR79DISTRITO2").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD2").Value) Then
        txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA2").Value) Then
        txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
       End If
       If Not IsNull(rstp("FR79PAIS2").Value) Then
        txtProv1(43).Text = rstp("FR79PAIS2").Value
       End If
       If Not IsNull(rstp("FR79TFNO2").Value) Then
        txtProv1(46).Text = rstp("FR79TFNO2").Value
       End If
       If Not IsNull(rstp("FR79TFNO22").Value) Then
        txtProv1(44).Text = rstp("FR79TFNO22").Value
       End If
       If Not IsNull(rstp("FR79FAX2").Value) Then
        txtProv1(47).Text = rstp("FR79FAX2").Value
       End If
       If Not IsNull(rstp("FR79EMAIL2").Value) Then
        txtProv1(45).Text = rstp("FR79EMAIL2").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT3").Value) Then
        txtProv1(48).Text = rstp("FR79REPRESENT3").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION3").Value) Then
        txtProv1(56).Text = rstp("FR79DIRECCION3").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO3").Value) Then
        txtProv1(58).Text = rstp("FR79DISTRITO3").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD3").Value) Then
        txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA3").Value) Then
        txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
       End If
       If Not IsNull(rstp("FR79PAIS3").Value) Then
        txtProv1(53).Text = rstp("FR79PAIS3").Value
       End If
       If Not IsNull(rstp("FR79TFNO3").Value) Then
        txtProv1(50).Text = rstp("FR79TFNO3").Value
       End If
       If Not IsNull(rstp("FR79TFNO33").Value) Then
        txtProv1(52).Text = rstp("FR79TFNO33").Value
       End If
       If Not IsNull(rstp("FR79FAX3").Value) Then
        txtProv1(49).Text = rstp("FR79FAX3").Value
       End If
       If Not IsNull(rstp("FR79EMAIL3").Value) Then
        txtProv1(51).Text = rstp("FR79EMAIL3").Value
       End If
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        txtProv1(59).Text = ""
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
    End If
  Case 1
    If txtText1(2).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(2).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
       If Not IsNull(rstp("FR79COMENTARIOS").Value) Then
        txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION1").Value) Then
        txtProv1(31).Text = rstp("FR79DIRECCION1").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO1").Value) Then
        txtProv1(30).Text = rstp("FR79DISTRITO1").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD1").Value) Then
        txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA1").Value) Then
        txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
       End If
       If Not IsNull(rstp("FR79PAIS1").Value) Then
        txtProv1(34).Text = rstp("FR79PAIS1").Value
       End If
       If Not IsNull(rstp("FR79TFNO1").Value) Then
        txtProv1(37).Text = rstp("FR79TFNO1").Value
       End If
       If Not IsNull(rstp("FR79TFNO11").Value) Then
        txtProv1(35).Text = rstp("FR79TFNO11").Value
       End If
       If Not IsNull(rstp("FR79FAX1").Value) Then
        txtProv1(38).Text = rstp("FR79FAX1").Value
       End If
       If Not IsNull(rstp("FR79EMAIL1").Value) Then
        txtProv1(36).Text = rstp("FR79EMAIL1").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT2").Value) Then
        txtProv1(57).Text = rstp("FR79REPRESENT2").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION2").Value) Then
        txtProv1(40).Text = rstp("FR79DIRECCION2").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO2").Value) Then
        txtProv1(39).Text = rstp("FR79DISTRITO2").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD2").Value) Then
        txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA2").Value) Then
        txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
       End If
       If Not IsNull(rstp("FR79PAIS2").Value) Then
        txtProv1(43).Text = rstp("FR79PAIS2").Value
       End If
       If Not IsNull(rstp("FR79TFNO2").Value) Then
        txtProv1(46).Text = rstp("FR79TFNO2").Value
       End If
       If Not IsNull(rstp("FR79TFNO22").Value) Then
        txtProv1(44).Text = rstp("FR79TFNO22").Value
       End If
       If Not IsNull(rstp("FR79FAX2").Value) Then
        txtProv1(47).Text = rstp("FR79FAX2").Value
       End If
       If Not IsNull(rstp("FR79EMAIL2").Value) Then
        txtProv1(45).Text = rstp("FR79EMAIL2").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT3").Value) Then
        txtProv1(48).Text = rstp("FR79REPRESENT3").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION3").Value) Then
        txtProv1(56).Text = rstp("FR79DIRECCION3").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO3").Value) Then
        txtProv1(58).Text = rstp("FR79DISTRITO3").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD3").Value) Then
        txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA3").Value) Then
        txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
       End If
       If Not IsNull(rstp("FR79PAIS3").Value) Then
        txtProv1(53).Text = rstp("FR79PAIS3").Value
       End If
       If Not IsNull(rstp("FR79TFNO3").Value) Then
        txtProv1(50).Text = rstp("FR79TFNO3").Value
       End If
       If Not IsNull(rstp("FR79TFNO33").Value) Then
        txtProv1(52).Text = rstp("FR79TFNO33").Value
       End If
       If Not IsNull(rstp("FR79FAX3").Value) Then
        txtProv1(49).Text = rstp("FR79FAX3").Value
       End If
       If Not IsNull(rstp("FR79EMAIL3").Value) Then
        txtProv1(51).Text = rstp("FR79EMAIL3").Value
       End If
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        txtProv1(59).Text = ""
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
    End If
  Case 2
    If txtText1(1).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(1).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
       If Not IsNull(rstp("FR79COMENTARIOS").Value) Then
        txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION1").Value) Then
        txtProv1(31).Text = rstp("FR79DIRECCION1").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO1").Value) Then
        txtProv1(30).Text = rstp("FR79DISTRITO1").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD1").Value) Then
        txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA1").Value) Then
        txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
       End If
       If Not IsNull(rstp("FR79PAIS1").Value) Then
        txtProv1(34).Text = rstp("FR79PAIS1").Value
       End If
       If Not IsNull(rstp("FR79TFNO1").Value) Then
        txtProv1(37).Text = rstp("FR79TFNO1").Value
       End If
       If Not IsNull(rstp("FR79TFNO11").Value) Then
        txtProv1(35).Text = rstp("FR79TFNO11").Value
       End If
       If Not IsNull(rstp("FR79FAX1").Value) Then
        txtProv1(38).Text = rstp("FR79FAX1").Value
       End If
       If Not IsNull(rstp("FR79EMAIL1").Value) Then
        txtProv1(36).Text = rstp("FR79EMAIL1").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT2").Value) Then
        txtProv1(57).Text = rstp("FR79REPRESENT2").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION2").Value) Then
        txtProv1(40).Text = rstp("FR79DIRECCION2").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO2").Value) Then
        txtProv1(39).Text = rstp("FR79DISTRITO2").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD2").Value) Then
        txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA2").Value) Then
        txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
       End If
       If Not IsNull(rstp("FR79PAIS2").Value) Then
        txtProv1(43).Text = rstp("FR79PAIS2").Value
       End If
       If Not IsNull(rstp("FR79TFNO2").Value) Then
        txtProv1(46).Text = rstp("FR79TFNO2").Value
       End If
       If Not IsNull(rstp("FR79TFNO22").Value) Then
        txtProv1(44).Text = rstp("FR79TFNO22").Value
       End If
       If Not IsNull(rstp("FR79FAX2").Value) Then
        txtProv1(47).Text = rstp("FR79FAX2").Value
       End If
       If Not IsNull(rstp("FR79EMAIL2").Value) Then
        txtProv1(45).Text = rstp("FR79EMAIL2").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT3").Value) Then
        txtProv1(48).Text = rstp("FR79REPRESENT3").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION3").Value) Then
        txtProv1(56).Text = rstp("FR79DIRECCION3").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO3").Value) Then
        txtProv1(58).Text = rstp("FR79DISTRITO3").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD3").Value) Then
        txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA3").Value) Then
        txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
       End If
       If Not IsNull(rstp("FR79PAIS3").Value) Then
        txtProv1(53).Text = rstp("FR79PAIS3").Value
       End If
       If Not IsNull(rstp("FR79TFNO3").Value) Then
        txtProv1(50).Text = rstp("FR79TFNO3").Value
       End If
       If Not IsNull(rstp("FR79TFNO33").Value) Then
        txtProv1(52).Text = rstp("FR79TFNO33").Value
       End If
       If Not IsNull(rstp("FR79FAX3").Value) Then
        txtProv1(49).Text = rstp("FR79FAX3").Value
       End If
       If Not IsNull(rstp("FR79EMAIL3").Value) Then
        txtProv1(51).Text = rstp("FR79EMAIL3").Value
       End If
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        txtProv1(59).Text = ""
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
    End If
  End Select

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim rstp As rdoResultset
  Dim strP As String
  Dim qryFR79 As rdoQuery
  
  gintIndice = intIndex
  Call objWinInfo.CtrlDataChange


  Select Case intIndex
  Case 3
    If txtText1(3).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(3).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
       If Not IsNull(rstp("FR79COMENTARIOS").Value) Then
        txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION1").Value) Then
        txtProv1(31).Text = rstp("FR79DIRECCION1").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO1").Value) Then
        txtProv1(30).Text = rstp("FR79DISTRITO1").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD1").Value) Then
        txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA1").Value) Then
        txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
       End If
       If Not IsNull(rstp("FR79PAIS1").Value) Then
        txtProv1(34).Text = rstp("FR79PAIS1").Value
       End If
       If Not IsNull(rstp("FR79TFNO1").Value) Then
        txtProv1(37).Text = rstp("FR79TFNO1").Value
       End If
       If Not IsNull(rstp("FR79TFNO11").Value) Then
        txtProv1(35).Text = rstp("FR79TFNO11").Value
       End If
       If Not IsNull(rstp("FR79FAX1").Value) Then
        txtProv1(38).Text = rstp("FR79FAX1").Value
       End If
       If Not IsNull(rstp("FR79EMAIL1").Value) Then
        txtProv1(36).Text = rstp("FR79EMAIL1").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT2").Value) Then
        txtProv1(57).Text = rstp("FR79REPRESENT2").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION2").Value) Then
        txtProv1(40).Text = rstp("FR79DIRECCION2").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO2").Value) Then
        txtProv1(39).Text = rstp("FR79DISTRITO2").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD2").Value) Then
        txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA2").Value) Then
        txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
       End If
       If Not IsNull(rstp("FR79PAIS2").Value) Then
        txtProv1(43).Text = rstp("FR79PAIS2").Value
       End If
       If Not IsNull(rstp("FR79TFNO2").Value) Then
        txtProv1(46).Text = rstp("FR79TFNO2").Value
       End If
       If Not IsNull(rstp("FR79TFNO22").Value) Then
        txtProv1(44).Text = rstp("FR79TFNO22").Value
       End If
       If Not IsNull(rstp("FR79FAX2").Value) Then
        txtProv1(47).Text = rstp("FR79FAX2").Value
       End If
       If Not IsNull(rstp("FR79EMAIL2").Value) Then
        txtProv1(45).Text = rstp("FR79EMAIL2").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT3").Value) Then
        txtProv1(48).Text = rstp("FR79REPRESENT3").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION3").Value) Then
        txtProv1(56).Text = rstp("FR79DIRECCION3").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO3").Value) Then
        txtProv1(58).Text = rstp("FR79DISTRITO3").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD3").Value) Then
        txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA3").Value) Then
        txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
       End If
       If Not IsNull(rstp("FR79PAIS3").Value) Then
        txtProv1(53).Text = rstp("FR79PAIS3").Value
       End If
       If Not IsNull(rstp("FR79TFNO3").Value) Then
        txtProv1(50).Text = rstp("FR79TFNO3").Value
       End If
       If Not IsNull(rstp("FR79TFNO33").Value) Then
        txtProv1(52).Text = rstp("FR79TFNO33").Value
       End If
       If Not IsNull(rstp("FR79FAX3").Value) Then
        txtProv1(49).Text = rstp("FR79FAX3").Value
       End If
       If Not IsNull(rstp("FR79EMAIL3").Value) Then
        txtProv1(51).Text = rstp("FR79EMAIL3").Value
       End If
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        txtProv1(59).Text = ""
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
    End If
  Case 2
    If txtText1(2).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(2).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
       If Not IsNull(rstp("FR79COMENTARIOS").Value) Then
        txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION1").Value) Then
        txtProv1(31).Text = rstp("FR79DIRECCION1").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO1").Value) Then
        txtProv1(30).Text = rstp("FR79DISTRITO1").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD1").Value) Then
        txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA1").Value) Then
        txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
       End If
       If Not IsNull(rstp("FR79PAIS1").Value) Then
        txtProv1(34).Text = rstp("FR79PAIS1").Value
       End If
       If Not IsNull(rstp("FR79TFNO1").Value) Then
        txtProv1(37).Text = rstp("FR79TFNO1").Value
       End If
       If Not IsNull(rstp("FR79TFNO11").Value) Then
        txtProv1(35).Text = rstp("FR79TFNO11").Value
       End If
       If Not IsNull(rstp("FR79FAX1").Value) Then
        txtProv1(38).Text = rstp("FR79FAX1").Value
       End If
       If Not IsNull(rstp("FR79EMAIL1").Value) Then
        txtProv1(36).Text = rstp("FR79EMAIL1").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT2").Value) Then
        txtProv1(57).Text = rstp("FR79REPRESENT2").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION2").Value) Then
        txtProv1(40).Text = rstp("FR79DIRECCION2").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO2").Value) Then
        txtProv1(39).Text = rstp("FR79DISTRITO2").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD2").Value) Then
        txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA2").Value) Then
        txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
       End If
       If Not IsNull(rstp("FR79PAIS2").Value) Then
        txtProv1(43).Text = rstp("FR79PAIS2").Value
       End If
       If Not IsNull(rstp("FR79TFNO2").Value) Then
        txtProv1(46).Text = rstp("FR79TFNO2").Value
       End If
       If Not IsNull(rstp("FR79TFNO22").Value) Then
        txtProv1(44).Text = rstp("FR79TFNO22").Value
       End If
       If Not IsNull(rstp("FR79FAX2").Value) Then
        txtProv1(47).Text = rstp("FR79FAX2").Value
       End If
       If Not IsNull(rstp("FR79EMAIL2").Value) Then
        txtProv1(45).Text = rstp("FR79EMAIL2").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT3").Value) Then
        txtProv1(48).Text = rstp("FR79REPRESENT3").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION3").Value) Then
        txtProv1(56).Text = rstp("FR79DIRECCION3").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO3").Value) Then
        txtProv1(58).Text = rstp("FR79DISTRITO3").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD3").Value) Then
        txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA3").Value) Then
        txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
       End If
       If Not IsNull(rstp("FR79PAIS3").Value) Then
        txtProv1(53).Text = rstp("FR79PAIS3").Value
       End If
       If Not IsNull(rstp("FR79TFNO3").Value) Then
        txtProv1(50).Text = rstp("FR79TFNO3").Value
       End If
       If Not IsNull(rstp("FR79TFNO33").Value) Then
        txtProv1(52).Text = rstp("FR79TFNO33").Value
       End If
       If Not IsNull(rstp("FR79FAX3").Value) Then
        txtProv1(49).Text = rstp("FR79FAX3").Value
       End If
       If Not IsNull(rstp("FR79EMAIL3").Value) Then
        txtProv1(51).Text = rstp("FR79EMAIL3").Value
       End If
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    Else
        txtProv1(59).Text = ""
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
    End If
  Case 1
    If txtText1(1).Text <> "" Then
      strP = "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?"
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strP)
      qryFR79(0) = txtText1(1).Text
      Set rstp = qryFR79.OpenResultset()
      If rstp.EOF = False Then
       If Not IsNull(rstp("FR79COMENTARIOS").Value) Then
        txtProv1(59).Text = rstp("FR79COMENTARIOS").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION1").Value) Then
        txtProv1(31).Text = rstp("FR79DIRECCION1").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO1").Value) Then
        txtProv1(30).Text = rstp("FR79DISTRITO1").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD1").Value) Then
        txtProv1(32).Text = rstp("FR79LOCALIDAD1").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA1").Value) Then
        txtProv1(33).Text = rstp("FR79PROVINCIA1").Value
       End If
       If Not IsNull(rstp("FR79PAIS1").Value) Then
        txtProv1(34).Text = rstp("FR79PAIS1").Value
       End If
       If Not IsNull(rstp("FR79TFNO1").Value) Then
        txtProv1(37).Text = rstp("FR79TFNO1").Value
       End If
       If Not IsNull(rstp("FR79TFNO11").Value) Then
        txtProv1(35).Text = rstp("FR79TFNO11").Value
       End If
       If Not IsNull(rstp("FR79FAX1").Value) Then
        txtProv1(38).Text = rstp("FR79FAX1").Value
       End If
       If Not IsNull(rstp("FR79EMAIL1").Value) Then
        txtProv1(36).Text = rstp("FR79EMAIL1").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT2").Value) Then
        txtProv1(57).Text = rstp("FR79REPRESENT2").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION2").Value) Then
        txtProv1(40).Text = rstp("FR79DIRECCION2").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO2").Value) Then
        txtProv1(39).Text = rstp("FR79DISTRITO2").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD2").Value) Then
        txtProv1(41).Text = rstp("FR79LOCALIDAD2").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA2").Value) Then
        txtProv1(42).Text = rstp("FR79PROVINCIA2").Value
       End If
       If Not IsNull(rstp("FR79PAIS2").Value) Then
        txtProv1(43).Text = rstp("FR79PAIS2").Value
       End If
       If Not IsNull(rstp("FR79TFNO2").Value) Then
        txtProv1(46).Text = rstp("FR79TFNO2").Value
       End If
       If Not IsNull(rstp("FR79TFNO22").Value) Then
        txtProv1(44).Text = rstp("FR79TFNO22").Value
       End If
       If Not IsNull(rstp("FR79FAX2").Value) Then
        txtProv1(47).Text = rstp("FR79FAX2").Value
       End If
       If Not IsNull(rstp("FR79EMAIL2").Value) Then
        txtProv1(45).Text = rstp("FR79EMAIL2").Value
       End If
        '
       If Not IsNull(rstp("FR79REPRESENT3").Value) Then
        txtProv1(48).Text = rstp("FR79REPRESENT3").Value
       End If
       If Not IsNull(rstp("FR79DIRECCION3").Value) Then
        txtProv1(56).Text = rstp("FR79DIRECCION3").Value
       End If
       If Not IsNull(rstp("FR79DISTRITO3").Value) Then
        txtProv1(58).Text = rstp("FR79DISTRITO3").Value
       End If
       If Not IsNull(rstp("FR79LOCALIDAD3").Value) Then
        txtProv1(55).Text = rstp("FR79LOCALIDAD3").Value
       End If
       If Not IsNull(rstp("FR79PROVINCIA3").Value) Then
        txtProv1(54).Text = rstp("FR79PROVINCIA3").Value
       End If
       If Not IsNull(rstp("FR79PAIS3").Value) Then
        txtProv1(53).Text = rstp("FR79PAIS3").Value
       End If
       If Not IsNull(rstp("FR79TFNO3").Value) Then
        txtProv1(50).Text = rstp("FR79TFNO3").Value
       End If
       If Not IsNull(rstp("FR79TFNO33").Value) Then
        txtProv1(52).Text = rstp("FR79TFNO33").Value
       End If
       If Not IsNull(rstp("FR79FAX3").Value) Then
        txtProv1(49).Text = rstp("FR79FAX3").Value
       End If
       If Not IsNull(rstp("FR79EMAIL3").Value) Then
        txtProv1(51).Text = rstp("FR79EMAIL3").Value
       End If
    Else
        txtProv1(59).Text = ""
        txtProv1(31).Text = ""
        txtProv1(30).Text = ""
        txtProv1(32).Text = ""
        txtProv1(33).Text = ""
        txtProv1(34).Text = ""
        txtProv1(37).Text = ""
        txtProv1(35).Text = ""
        txtProv1(38).Text = ""
        txtProv1(36).Text = ""
        '
        txtProv1(57).Text = ""
        txtProv1(40).Text = ""
        txtProv1(39).Text = ""
        txtProv1(41).Text = ""
        txtProv1(42).Text = ""
        txtProv1(43).Text = ""
        txtProv1(46).Text = ""
        txtProv1(44).Text = ""
        txtProv1(47).Text = ""
        txtProv1(45).Text = ""
        '
        txtProv1(48).Text = ""
        txtProv1(56).Text = ""
        txtProv1(58).Text = ""
        txtProv1(55).Text = ""
        txtProv1(54).Text = ""
        txtProv1(53).Text = ""
        txtProv1(50).Text = ""
        txtProv1(52).Text = ""
        txtProv1(49).Text = ""
        txtProv1(51).Text = ""
      End If
      qryFR79.Close
      Set qryFR79 = Nothing
      Set rstp = Nothing
    End If
  End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



