VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form FrmRedPedCom3 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Propuestas de Pedidos de Compra"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0991.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdMasDatos 
      Caption         =   "M�&s Datos"
      Height          =   375
      Left            =   5280
      TabIndex        =   79
      Top             =   7680
      Width           =   1455
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Proveedor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2805
      Index           =   0
      Left            =   0
      TabIndex        =   26
      Top             =   480
      Width           =   11865
      Begin Crystal.CrystalReport CrystalReport1 
         Left            =   10800
         Top             =   360
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin VB.Frame Frame4 
         Height          =   1935
         Left            =   10080
         TabIndex        =   63
         Top             =   720
         Width           =   1575
         Begin VB.CommandButton Command1 
            Caption         =   "&Borrar Propuestas"
            Height          =   615
            Left            =   120
            TabIndex        =   78
            Top             =   1200
            Width           =   1335
         End
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Generar"
            Height          =   375
            Left            =   120
            TabIndex        =   65
            Top             =   240
            Width           =   1335
         End
         Begin VB.CommandButton cmdRechazar 
            Caption         =   "&Descartar"
            Height          =   375
            Left            =   120
            TabIndex        =   64
            Top             =   720
            Width           =   1335
         End
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   360
         Width           =   9690
         _ExtentX        =   17092
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0991.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(20)"
         Tab(0).Control(1)=   "txtText1(5)"
         Tab(0).Control(2)=   "txtText1(16)"
         Tab(0).Control(3)=   "txtText1(6)"
         Tab(0).Control(4)=   "txtText1(7)"
         Tab(0).Control(5)=   "txtText1(8)"
         Tab(0).Control(6)=   "txtText1(9)"
         Tab(0).Control(7)=   "dtcDateCombo1(1)"
         Tab(0).Control(8)=   "lblLabel1(24)"
         Tab(0).Control(9)=   "lblLabel1(23)"
         Tab(0).Control(10)=   "lblLabel1(21)"
         Tab(0).Control(11)=   "lblLabel1(6)"
         Tab(0).Control(12)=   "lblLabel1(8)"
         Tab(0).Control(13)=   "lblLabel1(10)"
         Tab(0).Control(14)=   "lblLabel1(11)"
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0991.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "SG02COD_PID"
            Height          =   330
            HelpContextID   =   40102
            Index           =   20
            Left            =   -68880
            MaxLength       =   13
            TabIndex        =   62
            Tag             =   "CPO|C�digo Peticionario"
            Top             =   1680
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR95CODESTPEDCOMPRA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   -71400
            MaxLength       =   13
            TabIndex        =   59
            Tag             =   "EP|Estado Pedido"
            Top             =   1440
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN1INDREPRE"
            Height          =   330
            HelpContextID   =   40102
            Index           =   16
            Left            =   -73080
            MaxLength       =   13
            TabIndex        =   41
            Tag             =   "R|Representante"
            Top             =   1320
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FRN1CODPEDCOMPRA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   -74640
            TabIndex        =   0
            Tag             =   "CP|C�digo Pedido Compra"
            Top             =   1920
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FRN1DESCPERSONALIZADA"
            Height          =   285
            Index           =   7
            Left            =   -74640
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Tag             =   "DP|Descripci�n Personalizada"
            Top             =   1560
            Visible         =   0   'False
            Width           =   8880
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   8
            Left            =   -74880
            TabIndex        =   1
            Tag             =   "Cod.Prov|C�digo Proveedor"
            Top             =   360
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   -73800
            TabIndex        =   23
            Tag             =   "Proveedor"
            Top             =   360
            Width           =   5970
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2085
            Index           =   0
            Left            =   120
            TabIndex        =   24
            Top             =   120
            Width           =   9165
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16166
            _ExtentY        =   3678
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRN1FECPEDCOMPRA"
            Height          =   330
            Index           =   1
            Left            =   -67680
            TabIndex        =   2
            Tag             =   "Fecha Pedido"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "sg02cod_pid"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   -68040
            TabIndex        =   61
            Top             =   960
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "fr95codestpedcompra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   -70080
            TabIndex        =   60
            Top             =   960
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "fr62indrepre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   -71040
            TabIndex        =   55
            Top             =   1080
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   -74040
            TabIndex        =   45
            Top             =   1440
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   -67680
            TabIndex        =   44
            Top             =   120
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   -74520
            TabIndex        =   43
            Top             =   1560
            Visible         =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   -74880
            TabIndex        =   42
            Top             =   120
            Width           =   885
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4185
      Index           =   1
      Left            =   0
      TabIndex        =   29
      Top             =   3360
      Width           =   11865
      Begin TabDlg.SSTab tabTab1 
         Height          =   3585
         Index           =   1
         Left            =   120
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   360
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   6324
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0991.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(14)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(15)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(16)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(17)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(18)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(19)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(9)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(57)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(56)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(7)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(12)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(13)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(20)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(22)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(5)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblLabel1(25)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "lblLabel1(26)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "lblLabel1(27)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "lblLabel1(28)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "lblLabel1(29)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "lblLabel1(30)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "lblLabel1(31)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "lblLabel1(32)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "lblLabel1(33)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "lblLabel1(34)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "lblLabel1(35)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "lblLabel1(36)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "dtcDateCombo1(0)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(18)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(17)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(15)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtText1(3)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(4)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(2)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(0)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(14)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "txtText1(12)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "txtText1(10)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "txtText1(13)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "txtText1(11)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).Control(45)=   "txtText1(21)"
         Tab(0).Control(45).Enabled=   0   'False
         Tab(0).Control(46)=   "txtText1(24)"
         Tab(0).Control(46).Enabled=   0   'False
         Tab(0).Control(47)=   "txtText1(23)"
         Tab(0).Control(47).Enabled=   0   'False
         Tab(0).Control(48)=   "txtbonificacion"
         Tab(0).Control(48).Enabled=   0   'False
         Tab(0).Control(49)=   "txtText1(19)"
         Tab(0).Control(49).Enabled=   0   'False
         Tab(0).Control(50)=   "txtText1(22)"
         Tab(0).Control(50).Enabled=   0   'False
         Tab(0).Control(51)=   "txtText1(1)"
         Tab(0).Control(51).Enabled=   0   'False
         Tab(0).Control(52)=   "txtText1(25)"
         Tab(0).Control(52).Enabled=   0   'False
         Tab(0).Control(53)=   "txtprecioneto"
         Tab(0).Control(53).Enabled=   0   'False
         Tab(0).Control(54)=   "txtText1(27)"
         Tab(0).Control(54).Enabled=   0   'False
         Tab(0).Control(55)=   "txtText1(26)"
         Tab(0).Control(55).Enabled=   0   'False
         Tab(0).Control(56)=   "txtText1(28)"
         Tab(0).Control(56).Enabled=   0   'False
         Tab(0).Control(57)=   "txtText1(29)"
         Tab(0).Control(57).Enabled=   0   'False
         Tab(0).Control(58)=   "txtText1(30)"
         Tab(0).Control(58).Enabled=   0   'False
         Tab(0).Control(59)=   "txtText1(31)"
         Tab(0).Control(59).Enabled=   0   'False
         Tab(0).Control(60)=   "txtText1(32)"
         Tab(0).Control(60).Enabled=   0   'False
         Tab(0).Control(61)=   "txtText1(33)"
         Tab(0).Control(61).Enabled=   0   'False
         Tab(0).Control(62)=   "txtText1(34)"
         Tab(0).Control(62).Enabled=   0   'False
         Tab(0).Control(63)=   "Frame1"
         Tab(0).Control(63).Enabled=   0   'False
         Tab(0).Control(64)=   "txtText1(35)"
         Tab(0).Control(64).Enabled=   0   'False
         Tab(0).Control(65)=   "txtText1(36)"
         Tab(0).Control(65).Enabled=   0   'False
         Tab(0).ControlCount=   66
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0991.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   36
            Left            =   2880
            TabIndex        =   91
            TabStop         =   0   'False
            Tag             =   "Pendiente Recibir"
            Top             =   1080
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   35
            Left            =   1560
            TabIndex        =   90
            TabStop         =   0   'False
            Tag             =   "Existencias"
            Top             =   1080
            Width           =   1050
         End
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Height          =   1215
            Left            =   3120
            TabIndex        =   89
            Top             =   2280
            Width           =   3495
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   34
            Left            =   4800
            TabIndex        =   87
            TabStop         =   0   'False
            Tag             =   "C�digo de Producto Volumen"
            Top             =   3120
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   33
            Left            =   5760
            TabIndex        =   85
            TabStop         =   0   'False
            Tag             =   "C�digo de Producto FF"
            Top             =   2520
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   32
            Left            =   4920
            TabIndex        =   83
            TabStop         =   0   'False
            Tag             =   "C�digo de Producto Referencia"
            Top             =   2520
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   31
            Left            =   4080
            TabIndex        =   81
            TabStop         =   0   'False
            Tag             =   "C�digo de Producto Interno"
            Top             =   2520
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Height          =   285
            Index           =   30
            Left            =   240
            TabIndex        =   80
            Top             =   2280
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   29
            Left            =   3240
            Locked          =   -1  'True
            TabIndex        =   77
            TabStop         =   0   'False
            Tag             =   "Recargo"
            Top             =   3120
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN2DESCUENTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   28
            Left            =   1560
            TabIndex        =   19
            Tag             =   "Descuento (%)"
            Top             =   1680
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN2BONIF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   26
            Left            =   1560
            TabIndex        =   20
            Tag             =   "Bonificaci�n (%)"
            Top             =   2280
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR88CODTIPIVA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   27
            Left            =   1560
            TabIndex        =   21
            Tag             =   "IVA(%)"
            Top             =   2880
            Width           =   700
         End
         Begin VB.TextBox txtprecioneto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN2PRECNET"
            Height          =   330
            HelpContextID   =   30104
            Left            =   6480
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Prec. Neto|Precio Neto"
            Top             =   1080
            Width           =   1185
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN2TAMENVASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   25
            Left            =   9840
            TabIndex        =   14
            Tag             =   "U.E"
            Top             =   480
            Width           =   1065
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   9720
            TabIndex        =   57
            TabStop         =   0   'False
            Tag             =   "Importe"
            Top             =   2400
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN2PRECIOUNIDAD"
            Height          =   330
            HelpContextID   =   30104
            Index           =   22
            Left            =   4320
            MaxLength       =   10
            TabIndex        =   15
            Tag             =   "Precio Unidad"
            Top             =   1080
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRN2CODDETPEDCOMP"
            Height          =   330
            HelpContextID   =   30101
            Index           =   19
            Left            =   6720
            TabIndex        =   74
            TabStop         =   0   'False
            Tag             =   "C�digo Detalle Pedido"
            Top             =   2760
            Visible         =   0   'False
            Width           =   825
         End
         Begin VB.TextBox txtbonificacion 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   40102
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Bonificaci�n|Unidades a Bonificar"
            Top             =   1680
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   960
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Seg"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "C�d.Interno"
            Top             =   480
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   10200
            TabIndex        =   18
            Tag             =   "Moneda"
            Top             =   1080
            Width           =   705
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   6120
            Locked          =   -1  'True
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "U.M"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   4920
            Locked          =   -1  'True
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "FF"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   5520
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   6720
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Volumen"
            Top             =   480
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   8160
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   480
            Width           =   1635
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   3240
            TabIndex        =   76
            TabStop         =   0   'False
            Tag             =   "C�digo de Producto"
            Top             =   2520
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   1560
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Producto"
            Top             =   480
            Width           =   3330
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   8400
            TabIndex        =   75
            TabStop         =   0   'False
            Tag             =   "Precio Unidad"
            Top             =   2880
            Visible         =   0   'False
            Width           =   1185
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN2CANTPEDIDA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   120
            MaxLength       =   10
            TabIndex        =   12
            Tag             =   "Cantidad|N�mero de envases"
            Top             =   1080
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN2IMPORLINEA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   15
            Left            =   8040
            TabIndex        =   17
            Tag             =   "Importe"
            Top             =   1080
            Width           =   1935
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   17
            Left            =   7560
            MaxLength       =   13
            TabIndex        =   72
            TabStop         =   0   'False
            Tag             =   "Moneda"
            Top             =   2160
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRN1CODPEDCOMPRA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   18
            Left            =   8880
            MaxLength       =   13
            TabIndex        =   73
            TabStop         =   0   'False
            Tag             =   "C�dido Pedido"
            Top             =   2160
            Visible         =   0   'False
            Width           =   495
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3120
            Index           =   1
            Left            =   -74880
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   240
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   5503
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRN2FECPLAZENTRE"
            Height          =   330
            Index           =   0
            Left            =   3120
            TabIndex        =   13
            Tag             =   "Plazo Entrega"
            Top             =   1680
            Visible         =   0   'False
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Existencias"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   36
            Left            =   1560
            TabIndex        =   93
            Top             =   840
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pdte.Recibir"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   35
            Left            =   2880
            TabIndex        =   92
            Top             =   840
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   34
            Left            =   4800
            TabIndex        =   88
            Top             =   2880
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   33
            Left            =   5760
            TabIndex        =   86
            Top             =   2280
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   32
            Left            =   4920
            TabIndex        =   84
            Top             =   2280
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   31
            Left            =   4080
            TabIndex        =   82
            Top             =   2280
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Recargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   30
            Left            =   3240
            TabIndex        =   71
            Top             =   2880
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descuento (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   29
            Left            =   1560
            TabIndex        =   70
            Top             =   1440
            Width           =   1365
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonificaci�n (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   1560
            TabIndex        =   69
            Top             =   2040
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "IVA (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   27
            Left            =   1560
            TabIndex        =   68
            Top             =   2640
            Width           =   630
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Neto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   26
            Left            =   6480
            TabIndex        =   67
            Top             =   840
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.E"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   25
            Left            =   9840
            TabIndex        =   66
            Top             =   240
            Width           =   330
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   9720
            TabIndex        =   58
            Top             =   2160
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Unidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   8400
            TabIndex        =   56
            Top             =   2640
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "fr62codpedcompra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   8880
            TabIndex        =   54
            Top             =   1920
            Visible         =   0   'False
            Width           =   1590
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "frh8moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   7560
            TabIndex        =   53
            Top             =   1920
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "FR25CODDETPEDCOMP"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   6720
            TabIndex        =   52
            Top             =   2520
            Visible         =   0   'False
            Width           =   2130
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonif. (Udes)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   120
            TabIndex        =   51
            Top             =   1440
            Width           =   1125
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1560
            TabIndex        =   50
            Top             =   240
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Seg"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   56
            Left            =   960
            TabIndex        =   49
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Int"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   57
            Left            =   120
            TabIndex        =   48
            Top             =   240
            Width           =   630
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Plazo Entrega"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   3120
            TabIndex        =   47
            Top             =   1440
            Visible         =   0   'False
            Width           =   1785
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   10200
            TabIndex        =   46
            Top             =   840
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   5040
            TabIndex        =   40
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   5520
            TabIndex        =   39
            Top             =   240
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   6120
            TabIndex        =   38
            Top             =   240
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   6720
            TabIndex        =   37
            Top             =   240
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   8160
            TabIndex        =   36
            Top             =   240
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   3240
            TabIndex        =   35
            Top             =   2280
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Base - PVL"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   4320
            TabIndex        =   34
            Top             =   840
            Width           =   1560
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   33
            Top             =   840
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   8040
            TabIndex        =   32
            Top             =   840
            Width           =   765
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   30
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRedPedCom3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0510.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: ABRIL 1999                                                    *
'* DESCRIPCION: Redactar Pedido de Compra                               *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mdblCodPedCom As Double
Dim mblnError As Boolean 'True si hay errores al guardar
Private Function Problems() As Boolean
  Dim blnPedido As Boolean
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rstFR73 As rdoResultset
  Dim strFRN2 As String
  Dim qryFRN2 As rdoQuery
  Dim rstFRN2 As rdoResultset
  Dim blnEntro1 As Boolean
  Dim strFR25 As String
  Dim qryFR25 As rdoQuery
  Dim rstFR25 As rdoResultset
  Dim blnEntro2 As Boolean
  Dim strMsg As String
  Dim intResp
  
  blnPedido = False
  blnEntro1 = False
  blnEntro2 = False
  
  strFR73 = "SELECT FR73CODINTFAR,FR73DESPRODUCTOPROV,FR73INDPROG " & _
            "  FROM FR7300 " & _
            " WHERE FR73CODPRODUCTO = ? "
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
  
  strFR25 = "SELECT FR6200.FR62CODPEDCOMPRA,FR6200.FR95CODESTPEDCOMPRA,FR9500.FR95DESESTPEDCOMPRA " & _
            "  FROM FR2500,FR6200, FR9500 " & _
            " WHERE FR2500.FR62CODPEDCOMPRA = FR6200.FR62CODPEDCOMPRA " & _
            "   AND FR6200.FR95CODESTPEDCOMPRA = FR9500.FR95CODESTPEDCOMPRA " & _
            "   AND FR73CODPRODUCTO = ? " & _
            "   AND FR6200.FR95CODESTPEDCOMPRA IN (1,2,3,4) "
  Set qryFR25 = objApp.rdoConnect.CreateQuery("", strFR25)
  
  strFRN2 = "SELECT * " & _
            "  FROM FRN200 " & _
            " WHERE FRN1CODPEDCOMPRA = ? "
  Set qryFRN2 = objApp.rdoConnect.CreateQuery("", strFRN2)
  qryFRN2(0) = txtText1(6).Text
  Set rstFRN2 = qryFRN2.OpenResultset()
    
  While Not rstFRN2.EOF
    blnEntro1 = True
    qryFR73(0) = rstFRN2.rdoColumns("FR73CODPRODUCTO").Value
    Set rstFR73 = qryFR73.OpenResultset()
    If Not rstFR73.EOF Then
      If Not IsNull(rstFR73.rdoColumns("FR73INDPROG").Value) Then
        If rstFR73.rdoColumns("FR73INDPROG").Value = -1 Then
          qryFR25(0) = rstFRN2.rdoColumns("FR73CODPRODUCTO").Value
          Set rstFR25 = qryFR25.OpenResultset()
          If Not rstFR25.EOF Then
            blnEntro2 = True
            'If Not rstFR25.EOF Then
              strMsg = "El producto: " & rstFR73.rdoColumns("FR73CODINTFAR").Value & "  " & _
                       rstFR73.rdoColumns("FR73DESPRODUCTOPROV").Value & _
                       " se encuentra en el Pedido en Estado : " & rstFR25.rdoColumns(2).Value & Chr(13) & Chr(13) & _
                       " N� Pedido: " & rstFR25.rdoColumns(0).Value & " " & Chr(13) & Chr(13) & _
                       "�Desea volver a pedir el producto?"
              intResp = MsgBox(strMsg, vbYesNo, "Pedidos Programados")
              If intResp = vbNo Then
                blnPedido = True
              End If
            'End If
          End If
        End If
      End If
    End If
    rstFRN2.MoveNext
  Wend
  If blnEntro1 Then
    qryFR73.Close
    Set qryFR73 = Nothing
    Set rstFR73 = Nothing
  End If
  If blnEntro2 Then
    qryFR25.Close
    Set qryFR25 = Nothing
    Set rstFR25 = Nothing
  End If
  qryFRN2.Close
  Set qryFRN2 = Nothing
  Set rstFRN2 = Nothing
  
  Problems = blnPedido
End Function



Private Sub cmdAceptar_Click()
  Dim strIns62 As String
  Dim strSQL As String
  Dim rstSQL As rdoResultset
  Dim strCod As String
  Dim strIns25 As String
  Dim strUpd As String
  Dim strObs As String
  Dim strPID As String
  Dim strProv As String
  Dim strOP As String
  Dim lista_productos As String
  Dim mintisel As Integer
  Dim mintNTotalSelRows As Integer
  Dim mvarBkmrk As Variant
    
  

  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataSave
  
  Me.Enabled = False
  Screen.MousePointer = vbHourglass
  
  lista_productos = "("
  mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  If mintNTotalSelRows > 0 Then
      mvarBkmrk = grdDBGrid1(1).SelBookmarks(0)
      lista_productos = lista_productos & grdDBGrid1(1).Columns("C�digo de Producto").CellValue(mvarBkmrk)
      If mintNTotalSelRows > 1 Then
        For mintisel = 1 To mintNTotalSelRows - 1
          mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintisel)
          lista_productos = lista_productos & "," & grdDBGrid1(1).Columns("C�digo de Producto").CellValue(mvarBkmrk)
        Next mintisel
      End If
  End If
  lista_productos = lista_productos & ")"
  If lista_productos = "()" Then
    Call MsgBox("No ha seleccionado ning�n producto para generar el pedido.", vbInformation, "Aviso")
    Me.Enabled = True
    Screen.MousePointer = vbDefault
    Exit Sub
  Else
  End If
  
  If Len(Trim(txtText1(6).Text)) > 0 Then
    strSQL = "SELECT FR62CODPEDCOMPRA_SEQUENCE.NEXTVAL FROM DUAL"
    Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
    strCod = rstSQL.rdoColumns(0).Value
    rstSQL.Close
    Set rstSQL = Nothing
    If Problems Then
      Screen.MousePointer = vbDefault
      Me.Enabled = True
      Exit Sub
    End If
    strIns62 = "INSERT INTO FR6200 (FR62CODPEDCOMPRA,FR62FECPEDCOMPRA,FR95CODESTPEDCOMPRA," & _
                                   "SG02COD_PID,FR62DESCPERSONALIZADA,FR79CODPROVEEDOR,FR62INDREPRE,FR62FECENTREG) " & _
                                   "  (SELECT " & strCod & ",SYSDATE,1," & _
                                   "  SG02COD_PID,FRN1DESCPERSONALIZADA,FR79CODPROVEEDOR," & _
                                   "        FRN1INDREPRE,FRN1FECENTREG " & _
                                   "   FROM FRN100 " & _
                                   "  WHERE FRN1CODPEDCOMPRA = " & txtText1(6).Text & ")"
    objApp.rdoConnect.Execute strIns62, 64
    
    strIns25 = "INSERT INTO FR2500 (FR62CODPEDCOMPRA,FR25CODDETPEDCOMP,FR73CODPRODUCTO," & _
                                  "FR25CANTPEDIDA,FR25PRECIOUNIDAD,FR25IMPORLINEA," & _
                                  "FR25FECPLAZENTRE,FR25INDENTRPROGR,FR25OBSERV," & _
                                  "FRH8MONEDA,FR25CANTENTREG,FR25INDCONT," & _
                                  "FR25CANTENT,FR25TAMENVASE,FR25DESCUENTO," & _
                                  "FR25BONIF,FR88CODTIPIVA,FR25PRECNET,FR25UDESBONIF) " & _
                                  "(SELECT " & strCod & ",FRN2CODDETPEDCOMP,FR73CODPRODUCTO," & _
                                  "FRN2CANTPEDIDA,FRN2PRECIOUNIDAD,FRN2IMPORLINEA," & _
                                  "SYSDATE+2,FRN2INDENTRPROGR,FRN2OBSERV," & _
                                  "FRH8MONEDA," & 0 & "," & 0 & "," & _
                                  0 & ",FRN2TAMENVASE,FRN2DESCUENTO," & _
                                  "FRN2BONIF,FR88CODTIPIVA,FRN2PRECNET," & _
                                  " CEIL(FRN2CANTPEDIDA*FRN2BONIF*FRN2TAMENVASE/100)" & _
                                  " FROM FRN200 WHERE FRN1CODPEDCOMPRA = " & _
                                  txtText1(6).Text & " AND FR73CODPRODUCTO IN " & lista_productos & ")"
    objApp.rdoConnect.Execute strIns25, 64
    strUpd = "UPDATE FRN100 SET FR95CODESTPEDCOMPRA = 1 WHERE FRN1CODPEDCOMPRA = " & txtText1(6).Text
    objApp.rdoConnect.Execute strUpd, 64
    gstrLlamadorFac = "PropPed"
    gstrCodPed = strCod
    Call objsecurity.LaunchProcess("FR0517")
    Screen.MousePointer = vbHourglass
  Else
    Screen.MousePointer = vbDefault
    MsgBox "No hay datos para generar pedidos", vbInformation, "Generar Pedidos"
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataRefresh
  Screen.MousePointer = vbDefault
  Me.Enabled = True
  
End Sub

Private Sub cmdMasDatos_Click()
    tabTab1(1).Tab = 0
    cmdMasDatos.Enabled = False
    If Not IsNumeric(txtText1(0).Text) Then
      MsgBox "Debe seleccionar un producto apretando el bot�n 'Buscar'", vbInformation, "M�s Datos"
    Else
      gstrLlamador = "FrmConExi"
      gstrCP = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0535")
    End If
    cmdMasDatos.Enabled = True
End Sub

Private Sub cmdRechazar_Click()
  Dim strUpd As String
  
  Me.Enabled = False
  Screen.MousePointer = vbHourglass
  If Len(Trim(txtText1(6).Text)) > 0 Then
    strUpd = "UPDATE FRN100 SET FR95CODESTPEDCOMPRA = 8 WHERE FRN1CODPEDCOMPRA = " & txtText1(6).Text
    objApp.rdoConnect.Execute strUpd, 64
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataRefresh
  Screen.MousePointer = vbDefault
  Me.Enabled = True
    
End Sub

Private Sub Command1_Click()
  Dim intResp
  Dim strUpd As String
  Dim qryUpd As rdoQuery
    
  Command1.Enabled = False
  Me.Enabled = False
  intResp = MsgBox("�Desea BORRAR TODAS las Propuestas de Pedido", vbYesNo, "Borrado Maxivo")
  If intResp = vbYes Then
    Screen.MousePointer = vbHourglass
    strUpd = "DELETE FRN200"
    'strUpd = "UPDATE FRN100 " & _
    '         "   SET FR95CODESTPEDCOMPRA = 8 " & _
    '         " WHERE FR95CODESTPEDCOMPRA = 9 "
    Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
    qryUpd.Execute
    strUpd = "DELETE FRN100"
    Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
    qryUpd.Execute
    Me.Enabled = True
    Command1.Enabled = True
    Screen.MousePointer = vbDefault
    Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
  Else
    Me.Enabled = True
    Command1.Enabled = True
    Screen.MousePointer = vbDefault
    'Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
  End If
End Sub

Private Sub Form_Activate()
txtText1(3).SelStart = 0
txtText1(3).SelLength = Len(txtText1(3).Text)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  gstrMoneda = strMonedaActual
  

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .blnAskPrimary = False
    .strName = "Pedidos de Compra"
    .strTable = "FRN100"
    .intAllowance = cwAllowDelete + cwAllowModify
    .strWhere = "FR95CODESTPEDCOMPRA=9 "
    
    
    Call .FormAddOrderField("FR79CODPROVEEDOR", cwAscending)
    Call .FormAddOrderField("FRN1FECPEDCOMPRA", cwDescending)
   
    Call .objPrinter.Add("FR9911", "Propuestas de Pedidos")
    
    strKey = .strDataBase & .strTable
    
    Call .FormCreateFilterWhere(strKey, "Pedidos de Compra")
    'Call .FormAddFilterWhere(strKey, "FRN1CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FRN1DESCPERSONALIZADA", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FRN1FECPEDCOMPRA", "Fecha Propuesta", cwDate)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�d. Proveedor", cwNumeric)
    'Call .FormAddFilterOrder(strKey, "FRN1CODPEDCOMPRA", "C�digo Pedido")
    'Call .FormAddFilterOrder(strKey, "FRN1DESCPERSONALIZADA", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "FRN1FECPEDCOMPRA", "Fecha Propuesta")
    Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "C�d. Proveedor")
    
  End With
  With objDetailInfo
    .strName = "Detalle Petici�n"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FRN200"
    
    .blnAskPrimary = False
    
    .intCursorSize = 100
    .intAllowance = cwAllowModify + cwAllowDelete
    
    Call .FormAddOrderField("FRN1CODPEDCOMPRA", cwAscending)
    Call .FormAddOrderField("FRN2CODDETPEDCOMP", cwAscending)
    
    Call .FormAddRelation("FRN1CODPEDCOMPRA", txtText1(6))
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Pedido")
    Call .FormAddFilterWhere(strKey, "FRN1CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRN2CODDETPEDCOMP", "C�digo Detalle Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRN2CANTPEDIDA", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRN2PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRN2IMPORLINEA", "Importe de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRN2FECPLAZENTRE", "Fecha Plazo Entrega", cwDate)
    
    Call .FormAddFilterOrder(strKey, "FRN2CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FRN2CODDETPEDCOMP", "C�digo Detalle Pedido")
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
End With
   
   
   With objWinInfo
   
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    '.CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    '.CtrlGetInfo(txtText1(4)).blnNegotiated = False
    .CtrlGetInfo(txtText1(15)).blnInFind = True
  
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    
    .CtrlGetInfo(txtText1(21)).blnReadOnly = True
    
    .CtrlGetInfo(txtbonificacion).blnNegotiated = False
    .CtrlGetInfo(txtbonificacion).blnReadOnly = True
    'JMRL.CtrlGetInfo(txtprecioneto).blnNegotiated = False
    .CtrlGetInfo(txtprecioneto).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(2), "FR73DESPRODUCTOPROV")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(10), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "FR93CODUNIMEDIDA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(35), "FR73EXISTENCIAS")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(36), "FR73CANTPEND")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(33)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(33)), txtText1(13), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(34)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(34)), txtText1(12), "FR73VOLUMEN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(32)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(32)), txtText1(14), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(31)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(31)), txtText1(24), "FR73CODINTFAR")
    
    ' Precio por defecto del producto
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(4), "FR73PRECIONETCOMPRA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(23), "FR73CODINTFARSEG")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(29), "FR73RECARGO")
    
    ' para que se pueda ver la descripci�n en modo tabla LUIS
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(30), "FR73DESPRODUCTOPROV")

    
    
    Call .WinRegister
    Call .WinStabilize
  End With
   
  gintCodProveedor = ""
  gintprodbuscado = ""
  
  'al entrar que refresque
  Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
  If (intKeyCode = 33 Or intKeyCode = 34) Then 'And objWinInfo.strName = "Detalle Petici�n" Then
    If intKeyCode = 34 Then
      Call objWinInfo.WinProcess(cwProcessToolBar, 23, 0)
    End If
    If intKeyCode = 33 Then
      Call objWinInfo.WinProcess(cwProcessToolBar, 22, 0)
    End If
    txtText1(3).SelStart = 0
    txtText1(3).SelLength = Len(txtText1(3).Text)
    txtText1(3).SetFocus
  End If
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)

   If strFormName = "Detalle Petici�n" Then
      tlbToolbar1.Buttons(2).Visible = False 'Nuevo
      tlbToolbar1.Buttons(3).Visible = False 'Abrir
      mnuDatosOpcion(10).Visible = False
      mnuDatosOpcion(20).Visible = False
   Else
      tlbToolbar1.Buttons(2).Visible = True
      tlbToolbar1.Buttons(3).Visible = True
      mnuDatosOpcion(10).Visible = True
      mnuDatosOpcion(20).Visible = True
   End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If (grdDBGrid1(1).Rows > 0) And (strFormName = "Detalle Petici�n") Then
    grdDBGrid1(1).Columns("C�d.Interno").Position = 1
    grdDBGrid1(1).Columns("C�d.Interno").Width = 1000
    grdDBGrid1(1).Columns("Producto").Position = 2
    grdDBGrid1(1).Columns("Producto").Width = 3500
    grdDBGrid1(1).Columns("Referencia").Position = 3
    grdDBGrid1(1).Columns("Referencia").Width = 1300
    grdDBGrid1(1).Columns("FF").Position = 4
    grdDBGrid1(1).Columns("FF").Width = 700
    grdDBGrid1(1).Columns("Volumen").Position = 5
    grdDBGrid1(1).Columns("Volumen").Width = 800
    grdDBGrid1(1).Columns("Cantidad").Position = 6
    grdDBGrid1(1).Columns("Cantidad").Width = 825
    grdDBGrid1(1).Columns("U.E").Position = 7
    grdDBGrid1(1).Columns("U.E").Width = 660
    grdDBGrid1(1).Columns("Precio Unidad").Position = 8
    grdDBGrid1(1).Columns("Precio Unidad").Width = 1215
    grdDBGrid1(1).Columns("Importe").Position = 9
    grdDBGrid1(1).Columns("Importe").Width = 916
    grdDBGrid1(1).Columns("Moneda").Position = 10
    grdDBGrid1(1).Columns("Moneda").Width = 735
    grdDBGrid1(1).Columns("Plazo Entrega").Position = 11
    grdDBGrid1(1).Columns("Plazo Entrega").Width = 1141
    grdDBGrid1(1).Columns("Descuento (%)").Position = 12
    grdDBGrid1(1).Columns("Descuento (%)").Width = 976
    grdDBGrid1(1).Columns("Bonificaci�n (%)").Position = 13
    grdDBGrid1(1).Columns("Bonificaci�n (%)").Width = 1020
    grdDBGrid1(1).Columns("IVA(%)").Position = 14
    grdDBGrid1(1).Columns("IVA(%)").Width = 391
    
    grdDBGrid1(1).Columns("C�dido Pedido").Visible = False
    grdDBGrid1(1).Columns("C�digo Detalle Pedido").Visible = False
    grdDBGrid1(1).Columns("C�digo de Producto").Position = 16
    grdDBGrid1(1).Columns("C�digo de Producto").Visible = False
    grdDBGrid1(1).Columns("C�digo de Producto Interno").Visible = False
    grdDBGrid1(1).Columns("C�digo de Producto Referencia").Visible = False
    grdDBGrid1(1).Columns("C�digo de Producto FF").Visible = False
    grdDBGrid1(1).Columns("C�digo de Producto Volumen").Visible = False
  End If
  If (grdDBGrid1(0).Rows > 0) And (strFormName = "Pedidos de Compra") Then
      grdDBGrid1(0).Columns("DP").Visible = False
      grdDBGrid1(0).Columns("R").Visible = False
      grdDBGrid1(0).Columns("EP").Visible = False
      grdDBGrid1(0).Columns("CP").Visible = False
      grdDBGrid1(0).Columns("CPO").Visible = False
      grdDBGrid1(0).Columns("Cod.Prov").Width = 1000
      grdDBGrid1(0).Columns("Proveedor").Width = 5500
  End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim strIVA As String
  Dim qryIVA As rdoQuery
  Dim rstIVA As rdoResultset
  
  If IsNumeric(txtText1(27).Text) And strFormName = "Detalle Petici�n" Then
    'Se comprueba que el IVA sea correcto
    strIVA = "SELECT * FROM FR8800 WHERE FR88CODTIPIVA = ?"
    Set qryIVA = objApp.rdoConnect.CreateQuery("", strIVA)
    qryIVA(0) = txtText1(27).Text
    Set rstIVA = qryIVA.OpenResultset()
    If rstIVA.EOF = True Then
      'El tipo de iva introducido por el usuario no es v�lido
      MsgBox "El IVA no es v�lido." & Chr(13) & Chr(13) & _
      "Dispone de una lista asociada para rellenar este valor", vbInformation, "Realizar Pedido"
      blnCancel = True
    End If
  End If
  mblnError = blnCancel
  If IsNumeric(txtText1(3).Text) And strFormName = "Detalle Petici�n" Then
    mblnError = False
  Else
    mblnError = True
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strProveedor As String
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  strWhere = "{FR7900.FR79CODPROVEEDOR} = {FRN100.FR79CODPROVEEDOR} AND " & _
             "{FRN100.FRN1CODPEDCOMPRA} = {FRN200.FRN1CODPEDCOMPRA} AND " & _
             "{FRN200.FR73CODPRODUCTO} = {FR7300.FR73CODPRODUCTO} AND " & _
             "{FRN100.FR95CODESTPEDCOMPRA} = 9"

    
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp4
  CrystalReport1.Action = 1
Err_imp4:

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strOrden As String
  Dim strDes As String
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  'strDes = objPrinter.Destination
  If intReport > 0 Then
     Call Imprimir("FR9911.RPT", 0)
  End If
  Set objPrinter = Nothing
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

  Dim objField As clsCWFieldSearch
  If strFormName = "Pedidos de Compra" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     .strOrder = "ORDER BY FR79CODPROVEEDOR ASC"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo del Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n del Proveedor "

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
If Index = 1 And tabTab1(1).Tab = 0 Then
  txtText1(3).SelStart = 0
  txtText1(3).SelLength = Len(txtText1(3).Text)
  txtText1(3).SetFocus
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim linea As Variant
   Dim strFec As String
   Dim rstFec As rdoResultset
   
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  
   If btnButton.Index = 30 Then
     Exit Sub
   End If
   
   Select Case btnButton.Index
   Case 2, 3, 16, 18, 19, 21, 22, 23, 24:
      If objWinInfo.objWinActiveForm.strName = "Pedidos de Compra" Then
         If Not IsNumeric(txtText1(6).Text) And Not IsNumeric(txtText1(19).Text) Then
            Call MsgBox("El Pedido no tiene ning�n producto, debe borrarlo o introducir productos", vbExclamation, "Aviso")
            Exit Sub
         Else
          If (btnButton.Index <> 2) And (btnButton.Index <> 4) And (btnButton.Index <> 6) Then
            objWinInfo.objWinActiveForm.strWhere = "FR95CODESTPEDCOMPRA=1 "
          End If
         End If
      End If
   Case 30:
      If Not IsNumeric(txtText1(6).Text) And Not IsNumeric(txtText1(19).Text) Then
         'Call MsgBox("El Pedido no tiene ning�n producto, debe borrarlo o introducir productos", vbExclamation, "Aviso")
         Exit Sub
      End If
   End Select
   
   If btnButton.Index = 2 Then
      If objWinInfo.objWinActiveForm.strName = "Pedidos de Compra" Then
         ' Nuevo Pedido
         sqlstr = "SELECT FRN1CODPEDCOMPRA_SEQUENCE.nextval FROM dual"
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         Call objWinInfo.CtrlSet(txtText1(6), rsta.rdoColumns(0).Value)
         mdblCodPedCom = rsta.rdoColumns(0).Value
         If IsNumeric(mdblCodPedCom) Then
           objWinInfo.objWinActiveForm.strWhere = "FR95CODESTPEDCOMPRA=1 AND FRN1CODPEDCOMPRA = " & mdblCodPedCom & " "
         Else
           objWinInfo.objWinActiveForm.strWhere = "FR95CODESTPEDCOMPRA=1 "
         End If
         rsta.Close
         Set rsta = Nothing
         strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
         Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
         Call objWinInfo.CtrlSet(dtcDateCombo1(1), rstFec.rdoColumns(0).Value)
         rstFec.Close
         Set rstFec = Nothing
         Call objWinInfo.CtrlSet(txtText1(16), 1) ' indrepre -> 1
         Call objWinInfo.CtrlSet(txtText1(5), 1) ' codestado pedido de compra -> 1
         Call objWinInfo.CtrlSet(txtText1(20), objsecurity.strUser) ' C�digo Peticionario -> sg02cod_pid
      Else
         ' Nuevo Producto
         sqlstr = "SELECT MAX(FRN2CODDETPEDCOMP) FROM FRN200 WHERE FRN1CODPEDCOMPRA=" & txtText1(6).Text
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         If IsNull(rsta.rdoColumns(0).Value) Then
            linea = 1
         Else
            linea = rsta.rdoColumns(0).Value + 1
         End If
         Call objWinInfo.CtrlSet(txtText1(19), linea)
         rsta.Close
         Set rsta = Nothing
         txtText1(21).Text = gstrMoneda
         strFec = "(SELECT TO_CHAR(SYSDATE+2,'DD/MM/YYYY') FROM DUAL)"
         Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
         Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
         rstFec.Close
         Set rstFec = Nothing
      End If
   End If
If btnButton.Index = 21 Or btnButton.Index = 22 Or btnButton.Index = 23 Or btnButton.Index = 24 Then
    txtText1(3).SelStart = 0
    txtText1(3).SelLength = Len(txtText1(3).Text)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
   Select Case intIndex
   Case 10
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   Case 20
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
   Case 40
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
   Case 60
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
   Case 80
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
   Case 100
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
   Case Else
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   If intIndex = 1 Then
    txtText1(3).SelStart = 0
    txtText1(3).SelLength = Len(txtText1(3).Text)
    txtText1(3).SetFocus
   End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtprecioneto_Change()
  'Se calcula el importe
  Dim dblImporte

    If IsNumeric(txtprecioneto) And IsNumeric(txtText1(3).Text) Then
      dblImporte = txtprecioneto * txtText1(3)
      If InStr(dblImporte, ",") = 0 Then
        txtText1(15).Text = dblImporte
      Else
        txtText1(15).Text = Format(dblImporte, "0.###")
      End If
    End If


  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  If intIndex = 7 Then
    txtText1(3).SetFocus
  End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim aux1 As Variant
   Dim intS As Integer

  Call objWinInfo.CtrlDataChange
  
  If intIndex = 0 Then
    'txtbonificacion.Text = ""
    txtprecioneto.Text = ""
    Call calcular_precio_neto
  End If
  
   If intIndex = 4 Then
      If Not IsNumeric(txtText1(22).Text) Then
         txtText1(22).Text = txtText1(4).Text
      End If
   End If
   If intIndex = 21 Then
      txtText1(17).Text = txtText1(21).Text
   End If
  
   If intIndex = 22 Or intIndex = 3 Or intIndex = 15 Or intIndex = 27 Or intIndex = 28 Then
      If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) And IsNumeric(txtprecioneto.Text) Then
         aux1 = txtprecioneto.Text * txtText1(3).Text
         If InStr(aux1, ",") = 0 Then
            txtText1(15).Text = aux1
         Else
            txtText1(15).Text = Format(aux1, "0.###")
         End If
      Else
         Call objWinInfo.CtrlSet(txtText1(15), 0)
      End If
   End If
  
  'cuando se introduce la cantidad de producto a pedir, se calcula la bonificaci�n
  If intIndex = 3 And IsNumeric(txtText1(0).Text) Then
    If IsNumeric(txtText1(3).Text) Then
      sqlstr = "SELECT FR73BONIFICACION FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns(0).Value) Then
           If IsNumeric(txtText1(3).Text) And IsNumeric(rsta.rdoColumns(0).Value) Then
            If IsNumeric(txtText1(25).Text) Then
              If txtText1(3).Text > 0 And rsta.rdoColumns(0).Value > 0 Then
                If ((((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100) * txtText1(25).Text) / (Fix(((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100) * txtText1(25).Text))) > 0 Then
                  intS = 1
                Else
                  intS = 0
                End If
                txtbonificacion.Text = Fix(((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100) * txtText1(25).Text) + intS
              Else
                txtbonificacion.Text = 0
              End If
            Else
              If txtText1(3).Text > 0 And rsta.rdoColumns(0).Value > 0 Then
                If ((((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100)) / (Fix(((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100)))) > 0 Then
                  intS = 1
                Else
                  intS = 0
                End If
                txtbonificacion.Text = Fix((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100) + intS
              Else
                txtbonificacion.Text = 0
              End If
            End If
           End If
        Else
            txtbonificacion.Text = 0
        End If
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If
  
  'si cambia el descuento, el IVA o el precio_unidad hay que actualizar el precio neto
  If intIndex = 28 Or intIndex = 27 Or intIndex = 22 Then
        Call calcular_precio_neto
  End If
  
  'si cambia el %bonificaci�n o el tama�o del envase que cambie la bonificaci�n
  If intIndex = 26 Or intIndex = 25 Then
    If IsNumeric(txtText1(3).Text) And IsNumeric(txtText1(26).Text) Then
        If IsNumeric(txtText1(25).Text) Then
          If txtText1(26).Text > 0 And txtText1(3).Text > 0 Then
            If ((((txtText1(3).Text * txtText1(26).Text) / 100) * txtText1(25).Text) / (Fix(((txtText1(3).Text * txtText1(26).Text) / 100) * txtText1(25).Text))) > 0 Then
              intS = 1
            Else
              intS = 0
            End If
            txtbonificacion.Text = Fix(((txtText1(3).Text * txtText1(26).Text) / 100) * txtText1(25).Text) + intS
          Else
            txtbonificacion.Text = 0
          End If
        Else
          If txtText1(26).Text > 0 And txtText1(3).Text > 0 Then
            If ((((txtText1(3).Text * txtText1(26).Text) / 100)) / (Fix(((txtText1(3).Text * txtText1(26).Text) / 100)))) > 0 Then
              intS = 1
            Else
              intS = 0
            End If
            txtbonificacion.Text = Fix((txtText1(3).Text * txtText1(26).Text) / 100) + intS
          Else
            txtbonificacion.Text = 0
          End If
        End If
    Else
        txtbonificacion.Text = 0
    End If
  End If
    
   
End Sub

Private Sub calcular_precio_neto()
Dim Precio_Neto As Currency
Dim Precio_Base As Currency
Dim Descuento As Currency
Dim IVA As Currency
Dim Recargo As Currency
Dim PVL As Currency
Dim PVP As Currency
Dim Param_Gen As Currency
Dim stra As String
Dim rsta As rdoResultset
Dim strPrecNetComp As String
Dim strupdate As String
Dim strPrecVenta As String

'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
If IsNumeric(txtText1(22).Text) Then 'Precio_Base
  Precio_Base = txtText1(22).Text
Else
  Precio_Base = 0
End If
If IsNumeric(txtText1(28).Text) Then 'Descuento
  Descuento = Precio_Base * txtText1(28).Text / 100
Else
  Descuento = 0
End If
If IsNumeric(txtText1(29).Text) Then 'Recargo
  Recargo = Precio_Base * txtText1(29).Text / 100
Else
  Recargo = 0
End If
If IsNumeric(txtText1(27).Text) Then 'IVA
  IVA = (Precio_Base + Recargo - Descuento) * txtText1(27).Text / 100
Else
  IVA = 0
End If
'If IsNumeric(txtText1(29).Text) Then 'Recargo
'  Recargo = Precio_Base * txtText1(29).Text / 100
'Else
'  Recargo = 0
'End If
Precio_Neto = Precio_Base - Descuento + IVA + Recargo
If txtprecioneto.Text <> Format(Precio_Neto, "0.00") Then
    txtprecioneto.Text = Format(Precio_Neto, "0.00")
End If

End Sub
