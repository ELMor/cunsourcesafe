VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form FrmModAlbaran 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Modificar Albar�n"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0560.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Productos del Albar�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5895
      Left            =   240
      TabIndex        =   14
      Top             =   1920
      Width           =   11535
      Begin SSDataWidgets_B.SSDBGrid ssgrdPendientes 
         Height          =   5415
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   11295
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   20
         AllowColumnMoving=   2
         AllowColumnSwapping=   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   20
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "L�nea"
         Columns(0).Name =   "LINEA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1614
         Columns(1).Caption=   "Referencia"
         Columns(1).Name =   "REFERENCIA"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   4313
         Columns(2).Caption=   "Producto"
         Columns(2).Name =   "PRODUCTO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1111
         Columns(3).Caption=   "C.ped."
         Columns(3).Name =   "CANTIDADPEDIDA"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16776960
         Columns(4).Width=   1349
         Columns(4).Caption=   "Precio U."
         Columns(4).Name =   "PRECIOUNITARIO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   16776960
         Columns(5).Width=   979
         Columns(5).Caption=   "%Dto."
         Columns(5).Name =   "DESCUENTO"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   16776960
         Columns(6).Width=   900
         Columns(6).Caption=   "%IVA"
         Columns(6).Name =   "IVA"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   16776960
         Columns(7).Width=   1746
         Columns(7).Caption=   "Precio Neto"
         Columns(7).Name =   "PRECIONETO"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   2037
         Columns(8).Caption=   "Importe L�nea"
         Columns(8).Name =   "IMPORTELINEA"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(9).Width=   926
         Columns(9).Caption=   "C.Ent."
         Columns(9).Name =   "CANTIDADENTREGADA"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).HasBackColor=   -1  'True
         Columns(9).BackColor=   16776960
         Columns(10).Width=   1058
         Columns(10).Caption=   "%Bon."
         Columns(10).Name=   "BONIFICACION"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(10).HasBackColor=   -1  'True
         Columns(10).BackColor=   16776960
         Columns(11).Width=   1746
         Columns(11).Caption=   "Pend.Bonif."
         Columns(11).Name=   "PENDIENTEBONIFICACION"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).HasBackColor=   -1  'True
         Columns(11).BackColor=   16776960
         Columns(12).Width=   1455
         Columns(12).Caption=   "T.Envase"
         Columns(12).Name=   "ENVASE"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(12).HasBackColor=   -1  'True
         Columns(12).BackColor=   16776960
         Columns(13).Width=   1931
         Columns(13).Caption=   "C.Pend.Fact."
         Columns(13).Name=   "PENDIENTEFACTURAR"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(13).HasBackColor=   -1  'True
         Columns(13).BackColor=   12615935
         Columns(14).Width=   1931
         Columns(14).Caption=   "N� de Pedido"
         Columns(14).Name=   "NUMEROPEDIDO"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(14).Locked=   -1  'True
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "L�nea Ped."
         Columns(15).Name=   "LINEAPEDIDO"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(15).Locked=   -1  'True
         Columns(16).Width=   1720
         Columns(16).Caption=   "%Recargo"
         Columns(16).Name=   "RECARGO"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         Columns(16).Locked=   -1  'True
         Columns(17).Width=   3200
         Columns(17).Visible=   0   'False
         Columns(17).Caption=   "Cod.Producto"
         Columns(17).Name=   "CODIGOPRODUCTO"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(17).Locked=   -1  'True
         Columns(18).Width=   1614
         Columns(18).Caption=   "Cant.Bonif."
         Columns(18).Name=   "CANTIDADBONIFICADA"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   8
         Columns(18).FieldLen=   256
         Columns(18).HasBackColor=   -1  'True
         Columns(18).BackColor=   16776960
         Columns(19).Width=   3200
         Columns(19).Visible=   0   'False
         Columns(19).Caption=   "Gr.Terap."
         Columns(19).Name=   "GRUPOTERAPEUTICO"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(19).Locked=   -1  'True
         _ExtentX        =   19923
         _ExtentY        =   9551
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdFiltrar 
      Caption         =   "Filtrar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   13
      Top             =   1440
      Width           =   1455
   End
   Begin VB.CommandButton cmdDepurar 
      Caption         =   "Depurar Cambios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   12
      Top             =   1440
      Width           =   2175
   End
   Begin VB.CommandButton cmdConfirmar 
      Caption         =   "Confirmar Cambios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4560
      TabIndex        =   11
      Top             =   1440
      Width           =   2415
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Situaci�n Inicial"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7320
      TabIndex        =   10
      Top             =   1440
      Width           =   1935
   End
   Begin VB.TextBox txtCodigoAlbaran 
      Height          =   300
      Left            =   240
      TabIndex        =   2
      Top             =   960
      Width           =   1215
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Albar�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Fecha Albar�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   255
      Left            =   2040
      TabIndex        =   8
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label lblFechaAlbaran 
      BorderStyle     =   1  'Fixed Single
      Height          =   300
      Left            =   2040
      TabIndex        =   7
      Top             =   960
      Width           =   1455
   End
   Begin VB.Label lblProveedor 
      BorderStyle     =   1  'Fixed Single
      Height          =   300
      Left            =   6600
      TabIndex        =   6
      Top             =   960
      Width           =   3495
   End
   Begin VB.Label Label3 
      Caption         =   "Proveedor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   255
      Left            =   6600
      TabIndex        =   5
      Top             =   720
      Width           =   1215
   End
   Begin VB.Label lblEstadoAlbaran 
      BorderStyle     =   1  'Fixed Single
      Height          =   300
      Left            =   4080
      TabIndex        =   4
      Top             =   960
      Width           =   1935
   End
   Begin VB.Label Label4 
      Caption         =   "Estado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   255
      Left            =   4080
      TabIndex        =   3
      Top             =   720
      Width           =   975
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmModAlbaran"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim MatrizAnterior() As LineaAnterior
Dim MatrizActual() As LineaActual
Dim MatrizCambios() As Integer
Dim LineasCambiadas As Integer
Dim nLineas As Integer
Dim blnFacturado As Boolean

Private Sub Form_Activate()
  Call Inicializar_Toolbar
  cmdfiltrar.Enabled = False
  cmdDepurar.Enabled = False
  cmdConfirmar.Enabled = False
  cmdReset.Enabled = False
  If objPipe.PipeExist("NUMALBARAN") Then
    txtCodigoAlbaran = objPipe.PipeGet("NUMALBARAN")
    Call objPipe.PipeRemove("NUMALBARAN")
    cmdFiltrar_Click
  End If
End Sub


Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String
    Dim stra As String
    Dim rsta As rdoResultset
    Dim strpaciente As String
  
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
End Sub
'Private Sub pImprimirPeticion(ssGrid As SSDBGrid)
'    Dim i%, SQL$
'
'    If ssGrid.SelBookmarks.Count = 1 Then
'      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
'    Else
'      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
'    End If
'    With crtCrystalReport1
'        .PrinterCopies = 1
'        .Destination = crptToPrinter
'        For i = 0 To ssGrid.SelBookmarks.Count - 1
'            SQL = SQL & "{PR0457J.PR04NUMACTPLAN}= " & ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(i)) & " OR "
'        Next i
'        SQL = Left$(SQL, Len(SQL) - 4)
'        .SelectionFormula = "(" & SQL & ")"
'        .Connect = objApp.rdoConnect.Connect
'        .DiscardSavedData = True
'        Screen.MousePointer = vbHourglass
'        .Action = 1
'        Screen.MousePointer = vbDefault
'    End With
'End Sub

Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub



Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub tlbtoolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
Select Case Button.Index
  Case 21:
          Call ssgrdPendientes.MoveFirst
  Case 22:
          Call ssgrdPendientes.MovePrevious
  Case 23:
          Call ssgrdPendientes.MoveNext
  Case 24:
          Call ssgrdPendientes.MoveLast
  Case 30
    Unload Me
End Select
End Sub

Private Sub CompararMatrices()
 Dim i As Integer
 Dim j As Integer
 Dim blnCambios As Boolean
 ReDim MatrizCambios(nLineas)
 j = 0
 LineasCambiadas = 0
 blnCambios = False
  For i = 0 To nLineas - 1
   If MatrizActual(i).CantidadPedida <> MatrizAnterior(i).CantidadPedida Or _
      MatrizActual(i).PrecioUnitario <> MatrizAnterior(i).PrecioUnitario Or _
      MatrizActual(i).Descuento <> MatrizAnterior(i).Descuento Or _
      MatrizActual(i).IVA <> MatrizAnterior(i).IVA Or _
      MatrizActual(i).PrecioNeto <> MatrizAnterior(i).PrecioNeto Or _
      MatrizActual(i).ImporteLinea <> MatrizAnterior(i).ImporteLinea Or _
      MatrizActual(i).CantidadEntregada <> MatrizAnterior(i).CantidadEntregada Or _
      MatrizActual(i).bonificacion <> MatrizAnterior(i).bonificacion Or _
      MatrizActual(i).PendienteBonificacion <> MatrizAnterior(i).PendienteBonificacion Or _
      MatrizActual(i).Envase <> MatrizAnterior(i).Envase Or _
      MatrizActual(i).CantidadBonificada <> MatrizAnterior(i).CantidadBonificada Then
     blnCambios = True
     j = j + 1
     LineasCambiadas = LineasCambiadas + 1
     MatrizCambios(j) = i
  End If
  Next i
  If blnCambios = False Then
    cmdConfirmar.Enabled = False
    cmdReset.Enabled = False
    MsgBox "No se han efectuado cambios a la situaci�n inicial"
  Else
    cmdConfirmar.Enabled = True
    cmdReset.Enabled = True
  End If
End Sub

Private Sub cmdConfirmar_Click()
Dim i As Integer
Dim j As Integer
Dim strupdate As String
Dim qryUpd As rdoQuery
Dim importelineapedido As Single
Dim EstadoPedido As Integer
 Dim qry As rdoQuery
 Dim rs As rdoResultset
 Dim sql As String
 Dim strinsert As String
 Dim qryInsert As rdoQuery
 
 Dim intPVLU As Single
 Dim intPVPU As Single
 
 Dim rsta As rdoResultset
 Dim stra As String
 Dim blnupdate As Boolean
 Dim k As Integer


 
 'objApp.rdoConnect.BeginTrans
 On Error GoTo Fin
 
  Screen.MousePointer = vbHourglass


For i = 1 To LineasCambiadas
  ' �ndice de la l�nea cambiada
  j = MatrizCambios(i)
  
    ' =============================
   ' actualizar l�neas del albar�n
   ' =============================

  strupdate = "Update frj300 set frj300.frj3cantpedida = ?,"
  strupdate = strupdate & "frj300.frj3preciounidad= ?,"
  strupdate = strupdate & "frj300.frj3dto = ?,frj300.frj3iva = ?,"
  strupdate = strupdate & "frj300.frj3precnet = ?,frj300.frj3imporlinea = ?,"
  strupdate = strupdate & "frj300.frj3cantentreg = ?,frj300.frj3tpbonif = ?,"
  strupdate = strupdate & "frj300.frj3ptebonif = ?,frj300.frj3tamenvase = ?,"
  strupdate = strupdate & "frj300.frj3cantptefact = ?,frj300.frj3bonif = ?,"
  strupdate = strupdate & "frj300.frj3cantentacu = ? "
  strupdate = strupdate & " where frj300.frj1codalbaran = ? and frj300.frj3numlinea = ?"
'  Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpdate)
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)

       qryUpd(0) = Format(MatrizActual(j).CantidadPedida, "0.00")
       qryUpd(1) = Format(MatrizActual(j).PrecioUnitario, "0.00")
       qryUpd(2) = Format(MatrizActual(j).Descuento, "0.00")
       qryUpd(3) = Format(MatrizActual(j).IVA, "0.00")
       qryUpd(4) = Format(MatrizActual(j).PrecioNeto, "0.00")
       qryUpd(5) = Format(MatrizActual(j).ImporteLinea, "0.00")
       qryUpd(6) = Format(MatrizActual(j).CantidadEntregada, "0.00")
       qryUpd(7) = Format(MatrizActual(j).bonificacion, "0.00")
       qryUpd(8) = Format(MatrizActual(j).PendienteBonificacion, "0.00")
       qryUpd(9) = Format(MatrizActual(j).Envase, "0.00")
       qryUpd(10) = Format(MatrizActual(j).Pendientefacturar, "0.00")
       qryUpd(11) = Format(MatrizActual(j).CantidadBonificada, "0.00")
       qryUpd(12) = Format(MatrizActual(j).CantidadEntregada, "0.00")
       qryUpd(13) = txtCodigoAlbaran
       qryUpd(14) = MatrizActual(j).linea
    qryUpd.Execute
    qryUpd.Close
    Set qryUpd = Nothing
    
   ' =============================
   ' actualizar l�neas del pedido
   ' =============================
   'calcular importe l�nea del pedido (precio neto * cantidad pedida) a diferencia de
   ' importe l�nea del albar�n(precio neto * cantidad entregada)
   
     importelineapedido = Format(( _
          MatrizActual(j).PrecioNeto * _
             MatrizActual(j).CantidadPedida), "0.00")

  strupdate = "Update fr2500 set fr2500.fr25cantpedida = ?,"
  strupdate = strupdate & "fr2500.fr25preciounidad= ?,"
  strupdate = strupdate & "fr2500.fr25descuento = ?,fr2500.fr88codtipiva = ?,"
  strupdate = strupdate & "fr2500.fr25precnet = ?,fr2500.fr25imporlinea = ?,"
  strupdate = strupdate & "fr2500.fr25cantent = ?,fr2500.fr25bonif = ?,"
  strupdate = strupdate & "fr2500.fr25udesbonif = ?,fr2500.fr25tamenvase = ?"
  strupdate = strupdate & " where fr2500.fr62codpedcompra = ? and fr2500.fr25coddetpedcomp = ?"
  
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)

       qryUpd(0) = Format(MatrizActual(j).CantidadPedida, "0.00")
       qryUpd(1) = Format(MatrizActual(j).PrecioUnitario, "0.00")
       qryUpd(2) = Format(MatrizActual(j).Descuento, "0.00")
       qryUpd(3) = Format(MatrizActual(j).IVA, "0.00")
       qryUpd(4) = Format(MatrizActual(j).PrecioNeto, "0.00")
       qryUpd(5) = Format(importelineapedido, "0.00")
       qryUpd(6) = Format(MatrizActual(j).CantidadEntregada, "0.00")
       qryUpd(7) = Format(MatrizActual(j).bonificacion, "0.00")
       qryUpd(8) = Format(MatrizActual(j).PendienteBonificacion, "0.00")
       qryUpd(9) = Format(MatrizActual(j).Envase, "0.00")
       qryUpd(10) = MatrizActual(j).NumeroPedido
       qryUpd(11) = MatrizActual(j).LineaPedido
    qryUpd.Execute
    qryUpd.Close
    Set qryUpd = Nothing
    
   ' ===============================================================
   ' actualizar entradas a almacen cuando la cantidad entregada o el
   ' tama�o envase o el precio unidad haya sido modificado
   ' se insertar� el registro de la situaci�n anterior en negativo
   ' y un registro de la situaci�n actual en positivo
   ' ===============================================================
   
  If (MatrizActual(j).CantidadEntregada <> MatrizAnterior(j).CantidadEntregada) _
     Or (MatrizActual(j).Envase <> MatrizAnterior(j).Envase) Then
      If MatrizActual(j).CantidadEntregada > 0 Then
        sql = "SELECT fr35codmovimiento_sequence.nextval from dual"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
     ' l�nea positiva
     ' ---------------
        strinsert = " insert into fr3500 (fr35codmovimiento,fr04codalmacen_ori," & _
                  " fr04codalmacen_des,fr90codtipmov,fr35fecmovimiento," & _
                  " fr35obsmovi,fr73codproducto,fr35preciounidad," & _
                  " fr35cantproducto,fr93codunimedida)" & _
                  " values (?, ?, ?, ?, sysdate, ?, ?, ?, ?, ?)"
          Set qryInsert = objApp.rdoConnect.CreateQuery("", strinsert)
          qryInsert(0) = rs(0)
          qryInsert(1) = 0
          qryInsert(2) = 0
          qryInsert(3) = 10
          qryInsert(4) = Null
          qryInsert(5) = MatrizActual(j).CodigoProducto
          qryInsert(6) = Format(MatrizActual(j).PrecioUnitario, "0.00")
          qryInsert(7) = Format((MatrizActual(j).CantidadEntregada * MatrizActual(j).Envase), "0.00")
          qryInsert(8) = "NE"
          qryInsert.Execute
          qryInsert.Close
          Set qryInsert = Nothing
      End If
   ' l�nea negativa
   ' ---------------
     If MatrizAnterior(j).CantidadEntregada > 0 Then
        sql = "SELECT fr35codmovimiento_sequence.nextval from dual"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
 
        strinsert = " insert into fr3500 (fr35codmovimiento,fr04codalmacen_ori," & _
                " fr04codalmacen_des,fr90codtipmov,fr35fecmovimiento," & _
                " fr35obsmovi,fr73codproducto,fr35preciounidad," & _
                " fr35cantproducto,fr93codunimedida)" & _
                " values (?, ?, ?, ?, sysdate, ?, ?, ?, ?, ?)"
        Set qryInsert = objApp.rdoConnect.CreateQuery("", strinsert)
        qryInsert(0) = rs(0)
        qryInsert(1) = 0
        qryInsert(2) = 0
        qryInsert(3) = 10
        qryInsert(4) = Null
        qryInsert(5) = MatrizAnterior(j).CodigoProducto
        qryInsert(6) = Format(MatrizAnterior(j).PrecioUnitario, "0.00")
        qryInsert(7) = Format((MatrizAnterior(j).CantidadEntregada * MatrizAnterior(j).Envase * -1), "0.00")
        qryInsert(8) = "NE"
        qryInsert.Execute
        qryInsert.Close
        Set qryInsert = Nothing
     End If
   End If
   ' ===============================================================
   ' actualizar entradas a almacen cuando las unidades bonificadas o el
   ' precio unidad haya sido modificado
   ' se insertar� el registro de la situaci�n anterior en negativo
   ' y un registro de la situaci�n actual en positivo
   ' ===============================================================
  If MatrizActual(j).CantidadBonificada <> MatrizAnterior(j).CantidadBonificada Then
    If MatrizActual(j).CantidadBonificada > 0 Then
      sql = "SELECT fr35codmovimiento_sequence.nextval from dual"
      Set qry = objApp.rdoConnect.CreateQuery("", sql)
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
   ' l�nea positiva
   ' ---------------
         strinsert = " insert into fr3500 (fr35codmovimiento,fr04codalmacen_ori," & _
                " fr04codalmacen_des,fr90codtipmov,fr35fecmovimiento," & _
                " fr35obsmovi,fr73codproducto,fr35preciounidad," & _
                " fr35cantproducto,fr93codunimedida)" & _
                " values (?, ?, ?, ?, sysdate, ?, ?, ?, ?, ?)"
        Set qryInsert = objApp.rdoConnect.CreateQuery("", strinsert)
        qryInsert(0) = rs(0)
        qryInsert(1) = 0
        qryInsert(2) = 0
        qryInsert(3) = 10
        qryInsert(4) = Null
        qryInsert(5) = MatrizActual(j).CodigoProducto
        qryInsert(6) = Format(MatrizActual(j).PrecioUnitario, "0.00")
        qryInsert(7) = Format(MatrizActual(j).CantidadBonificada, "0.00")
        qryInsert(8) = "NE"
        qryInsert.Execute
        qryInsert.Close
        Set qryInsert = Nothing
    End If
    If MatrizAnterior(j).CantidadBonificada > 0 Then
      sql = "SELECT fr35codmovimiento_sequence.nextval from dual"
      Set qry = objApp.rdoConnect.CreateQuery("", sql)
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
   ' l�nea positiva
   ' ---------------
         strinsert = " insert into fr3500 (fr35codmovimiento,fr04codalmacen_ori," & _
                " fr04codalmacen_des,fr90codtipmov,fr35fecmovimiento," & _
                " fr35obsmovi,fr73codproducto,fr35preciounidad," & _
                " fr35cantproducto,fr93codunimedida)" & _
                " values (?, ?, ?, ?, sysdate, ?, ?, ?, ?, ?)"
        Set qryInsert = objApp.rdoConnect.CreateQuery("", strinsert)
        qryInsert(0) = rs(0)
        qryInsert(1) = 0
        qryInsert(2) = 0
        qryInsert(3) = 10
        qryInsert(4) = Null
        qryInsert(5) = MatrizAnterior(j).CodigoProducto
        qryInsert(6) = Format(MatrizAnterior(j).PrecioUnitario, "0.00")
        qryInsert(7) = Format(MatrizAnterior(j).CantidadBonificada * -1, "0.00")
        qryInsert(8) = "NE"
        qryInsert.Execute
        qryInsert.Close
        Set qryInsert = Nothing
    End If
  End If
  'actualizar ficha del producto: existencias,cantidad pendiente, precio medio,
  ' importe almac�n,pendiente bonificaci�n,precio �ltma entrada, precio venta y
  ' precio venta con
  
  ' se actualizar� la ficha del producto con los datos anteriores con cantidad negativa
  ' y con los datos actuales con con cantidad positiva
  
  'datos anteriores
  ' ---------------
  'se calculan las existencias para ver si se modifica el PrecioMedio o no. 25/8/2000 Irene
  stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & MatrizAnterior(j).CodigoProducto
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  blnupdate = False
  
  strupdate = "Update fr7300 set fr7300.fr73existencias = fr7300.fr73existencias - ?,"
  strupdate = strupdate & "fr7300.fr73cantpend=fr7300.fr73cantpend + ?,"
  If Not IsNull(rsta.rdoColumns("FR73EXISTENCIAS").Value) Then
    If rsta.rdoColumns("FR73EXISTENCIAS").Value - _
       ((MatrizAnterior(j).CantidadEntregada * MatrizAnterior(j).Envase) + MatrizAnterior(j).CantidadBonificada) <> 0 Then
          strupdate = strupdate & "fr7300.fr73precmed = (fr7300.fr73impalmacen - ? )"
          strupdate = strupdate & "/(fr7300.fr73existencias - ?),"
          blnupdate = True
    End If
  End If
  strupdate = strupdate & "fr7300.fr73impalmacen = fr7300.fr73impalmacen - ?,"
  strupdate = strupdate & "fr7300.fr73ptebonif = fr7300.fr73ptebonif + ?"
  strupdate = strupdate & " where fr7300.fr73codproducto = ?"
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
     k = 0
     qryUpd(k) = (MatrizAnterior(j).CantidadEntregada * MatrizAnterior(j).Envase) _
                    + MatrizAnterior(j).CantidadBonificada
     k = k + 1
     qryUpd(k) = ((MatrizAnterior(j).CantidadEntregada * MatrizAnterior(j).Envase) _
                  - (MatrizAnterior(j).CantidadPedida * MatrizAnterior(j).Envase))
     k = k + 1
     If blnupdate Then
        qryUpd(k) = Format(MatrizAnterior(j).ImporteLinea, "0.00")
        k = k + 1
        qryUpd(k) = (MatrizAnterior(j).CantidadEntregada * MatrizAnterior(j).Envase) _
                      + MatrizAnterior(j).CantidadBonificada
        k = k + 1
     End If
     qryUpd(k) = Format(MatrizAnterior(j).ImporteLinea, "0.00")
     k = k + 1
     qryUpd(k) = MatrizAnterior(j).CantidadBonificada
     k = k + 1
     qryUpd(k) = MatrizAnterior(j).CodigoProducto
    qryUpd.Execute
    qryUpd.Close
    Set qryUpd = Nothing
    rsta.Close
    Set rsta = Nothing
     
  
  'datos actuales
  ' -------------

'  Call Calcular_PVPU_PVLU(MatrizActual(j).CodigoProducto,MatrizActual(j).GrupoTerapeutico, _
'                  (MatrizActual(j).PrecioNeto / MatrizActual(j).Envase), _
'                  MatrizActual(j).Envase, _
'                  intPVLU, _
'                  intPVPU)
  'se calculan las existencias para ver si se modifica el PrecioMedio o no. 30/8/2000 Irene
  stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & MatrizActual(j).CodigoProducto
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  blnupdate = False
  
  strupdate = "Update fr7300 set fr7300.fr73existencias = fr7300.fr73existencias + ?,"
  strupdate = strupdate & "fr7300.fr73cantpend=fr7300.fr73cantpend + ?,"
  If Not IsNull(rsta.rdoColumns("FR73EXISTENCIAS").Value) Then
    If rsta.rdoColumns("FR73EXISTENCIAS").Value + _
       ((MatrizActual(j).CantidadEntregada * MatrizActual(j).Envase) + MatrizActual(j).CantidadBonificada) <> 0 Then
          strupdate = strupdate & "fr7300.fr73precmed = (fr7300.fr73impalmacen + ?)"
          strupdate = strupdate & "/(fr7300.fr73existencias + ?),"
          blnupdate = True
    End If
  End If
  strupdate = strupdate & "fr7300.fr73impalmacen = fr7300.fr73impalmacen + ?,"
  'strupdate = strupdate & "fr7300.fr73ptebonif = fr7300.fr73ptebonif - ?," '25/8/2000 Irene
  strupdate = strupdate & "fr7300.fr73ptebonif = fr7300.fr73ptebonif + ?,"
  strupdate = strupdate & "fr7300.fr73preultent = ? "
'  strupdate = strupdate & "fr7300.fr73precioventa = ? ,"
'  strupdate = strupdate & "fr7300.fr73precioventacon = ? "
  strupdate = strupdate & " where fr7300.fr73codproducto = ?"
  
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
  k = 0
       qryUpd(k) = (MatrizActual(j).CantidadEntregada * MatrizActual(j).Envase) _
                    + MatrizActual(j).CantidadBonificada
  k = k + 1
       qryUpd(k) = (MatrizActual(j).CantidadPedida * MatrizActual(j).Envase) _
                    - (MatrizActual(j).CantidadEntregada * MatrizActual(j).Envase)
  k = k + 1
  If blnupdate Then
        qryUpd(k) = Format(MatrizActual(j).ImporteLinea, "0.00")
  k = k + 1
        qryUpd(k) = (MatrizActual(j).CantidadEntregada * MatrizActual(j).Envase) _
                   + MatrizActual(j).CantidadBonificada
  k = k + 1

  End If
       qryUpd(k) = Format(MatrizActual(j).ImporteLinea, "0.00")
  k = k + 1
       qryUpd(k) = -MatrizActual(j).CantidadBonificada _
                   - ((MatrizAnterior(j).CantidadPedida * MatrizAnterior(j).bonificacion) / 100) _
                   + ((MatrizActual(j).CantidadPedida * MatrizActual(j).bonificacion) / 100)
  k = k + 1
       qryUpd(k) = Format((MatrizActual(j).PrecioNeto / MatrizActual(j).Envase), "0.00")
  k = k + 1
       qryUpd(k) = MatrizActual(j).CodigoProducto
    qryUpd.Execute
    qryUpd.Close
    Set qryUpd = Nothing
    rsta.Close
    Set rsta = Nothing
  
Next i


   ' =============================
   ' actualizar estado de los pedidos afectados
   ' =============================
 For i = 1 To LineasCambiadas
  ' �ndice de la l�nea cambiada
   j = MatrizCambios(i)
   sql = "SELECT sum(fr2500.fr25cantpedida) - sum(fr2500.fr25cantent),"
   sql = sql & "sum(fr2500.fr25udesbonif)"
   sql = sql & " FROM fr2500"
   sql = sql & " WHERE"
   sql = sql & " fr2500.fr62codpedcompra = ?"
   Set qry = objApp.rdoConnect.CreateQuery("", sql)
      qry(0) = MatrizActual(j).NumeroPedido
   Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
   If Not rs.EOF Then
     If rs(0) > 0 Or rs(1) > 0 Then
        EstadoPedido = 4
     Else
        EstadoPedido = 5
     End If
     strupdate = "Update fr6200 set fr6200.fr95codestpedcompra = ?"
     strupdate = strupdate & " where fr6200.fr62codpedcompra = ? and "
     strupdate = strupdate & "fr6200.fr95codestpedcompra <> ? "
  
     Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
        qryUpd(0) = EstadoPedido
        qryUpd(1) = MatrizActual(j).NumeroPedido
        qryUpd(2) = EstadoPedido
     qryUpd.Execute
     qryUpd.Close
     Set qryUpd = Nothing
   End If
 Next i
 
 cmdConfirmar.Enabled = False
 cmdReset.Enabled = False
 Screen.MousePointer = vbArrow

' MsgBox "xxxxxxxxxxxxxxxxxxx"
Fin:
 If ERR > 0 Then
   'objApp.rdoConnect.RollbackTrans
   MsgBox ERR & "-" & Error
   Screen.MousePointer = vbArrow

 Else
   'objApp.rdoConnect.CommitTrans
 End If

End Sub

Private Sub cmdDepurar_Click()
Dim i As Integer
Dim PrecioNeto As Single
Dim IVA As Single
Dim Recargo As Single
Dim Descuento As Single
Dim ImporteLinea
ReDim MatrizActual(0 To nLineas)
ssgrdPendientes.MoveFirst
 For i = 0 To ssgrdPendientes.Rows - 1
 
   If Not IsNumeric(ssgrdPendientes.Columns("CANTIDADPEDIDA").Value) Then
          MsgBox "La cantidad pedida debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   If Not IsNumeric(ssgrdPendientes.Columns("PRECIOUNITARIO").Value) Then
          MsgBox "El precio unitario debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   If Not IsNumeric(ssgrdPendientes.Columns("DESCUENTO").Value) Then
          MsgBox "El descuento debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   If Not IsNumeric(ssgrdPendientes.Columns("IVA").Value) Then
          MsgBox "El IVA debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   If Not IsNumeric(ssgrdPendientes.Columns("CANTIDADENTREGADA").Value) Then
          MsgBox "La cantidad entregada debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   If Not IsNumeric(ssgrdPendientes.Columns("BONIFICACION").Value) Then
          MsgBox "La bonificaci�n debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   If Not IsNumeric(ssgrdPendientes.Columns("PENDIENTEBONIFICACION").Value) Then
          MsgBox "La cantidad pendiente de bonificaci�n debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   If Not IsNumeric(ssgrdPendientes.Columns("ENVASE").Value) Then
          MsgBox "El tama�o del envase debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   If ssgrdPendientes.Columns("ENVASE").Value = 0 Then
          MsgBox "El tama�o del envase debe ser mayor que 0", vbExclamation
       Exit Sub
   End If
   If Not IsNumeric(ssgrdPendientes.Columns("CANTIDADBONIFICADA").Value) Then
          MsgBox "La cantidad bonificada debe ser un valor num�rico", vbExclamation
       Exit Sub
   End If
   
   If CLng(ssgrdPendientes.Columns("CANTIDADENTREGADA").Value) _
      > CLng(ssgrdPendientes.Columns("CANTIDADPEDIDA").Value) Then
       MsgBox "La cantidad entregada no puede ser mayor a la cantidad pedida", _
             vbExclamation
       Exit Sub
   End If
   
   'calcular Precio Neto
   Descuento = (ssgrdPendientes.Columns("PRECIOUNITARIO").Value * _
              ssgrdPendientes.Columns("DESCUENTO").Value) / 100
   Recargo = (ssgrdPendientes.Columns("PRECIOUNITARIO").Value * _
              ssgrdPendientes.Columns("RECARGO").Value) / 100
   IVA = ((ssgrdPendientes.Columns("PRECIOUNITARIO").Value _
         + Recargo - Descuento) * ssgrdPendientes.Columns("IVA").Value) / 100
   PrecioNeto = ssgrdPendientes.Columns("PRECIOUNITARIO").Value _
               + IVA + Recargo - Descuento
   ssgrdPendientes.Columns("PRECIONETO").Value = Format(PrecioNeto, "0.00")
   
   'calcular Importe L�nea
    ssgrdPendientes.Columns("IMPORTELINEA").Value = Format(( _
           ssgrdPendientes.Columns("PRECIONETO").Value * _
             ssgrdPendientes.Columns("CANTIDADENTREGADA").Value), "0.00")
             
   'calcular Cantidad Pendiente de Facturar
    ssgrdPendientes.Columns("PENDIENTEFACTURAR").Value = Format( _
             ssgrdPendientes.Columns("CANTIDADENTREGADA").Value, "0.00")

        MatrizActual(i).linea = ssgrdPendientes.Columns("LINEA").Value
        MatrizActual(i).Producto = ssgrdPendientes.Columns("PRODUCTO").Value
        MatrizActual(i).CantidadPedida = ssgrdPendientes.Columns("CANTIDADPEDIDA").Value
        MatrizActual(i).PrecioUnitario = ssgrdPendientes.Columns("PRECIOUNITARIO").Value
        MatrizActual(i).Descuento = ssgrdPendientes.Columns("DESCUENTO").Value
        MatrizActual(i).IVA = ssgrdPendientes.Columns("IVA").Value
        MatrizActual(i).PrecioNeto = ssgrdPendientes.Columns("PRECIONETO").Value
        MatrizActual(i).ImporteLinea = ssgrdPendientes.Columns("IMPORTELINEA").Value
        MatrizActual(i).CantidadEntregada = ssgrdPendientes.Columns("CANTIDADENTREGADA").Value
        MatrizActual(i).bonificacion = ssgrdPendientes.Columns("BONIFICACION").Value
        MatrizActual(i).PendienteBonificacion = ssgrdPendientes.Columns("PENDIENTEBONIFICACION").Value
        MatrizActual(i).Envase = ssgrdPendientes.Columns("ENVASE").Value
        MatrizActual(i).Pendientefacturar = ssgrdPendientes.Columns("PENDIENTEFACTURAR").Value
        MatrizActual(i).NumeroPedido = ssgrdPendientes.Columns("NUMEROPEDIDO").Value
        MatrizActual(i).LineaPedido = ssgrdPendientes.Columns("LINEAPEDIDO").Value
        MatrizActual(i).Recargo = ssgrdPendientes.Columns("RECARGO").Value
        MatrizActual(i).CodigoProducto = ssgrdPendientes.Columns("CODIGOPRODUCTO").Value
        MatrizActual(i).CantidadBonificada = ssgrdPendientes.Columns("CANTIDADBONIFICADA").Value
        MatrizActual(i).GrupoTerapeutico = ssgrdPendientes.Columns("GRUPOTERAPEUTICO").Value
        MatrizActual(i).referencia = ssgrdPendientes.Columns("REFERENCIA").Value
        ssgrdPendientes.MoveNext
 Next i
 CompararMatrices
 cmdDepurar.Enabled = False
' cmdConfirmar.Enabled = True
End Sub

Private Sub cmdFiltrar_Click()
 Dim qry As rdoQuery
 Dim rs As rdoResultset
 Dim sql As String

 Screen.MousePointer = vbHourglass
 sql = "SELECT frj100.frj1fechaalbar,"
 sql = sql & "decode(frj100.frj1estado,0,'No Facturado',1,'Facturado',''),"
 sql = sql & " frj100.fr79codproveedor||' - '||fr7900.fr79proveedor,"
 sql = sql & " frj300.frj3numlinea,fr7300.fr73codintfar||'-'||fr7300.fr73desproductoprov,"
 sql = sql & " frj300.frj3cantpedida,frj300.frj3preciounidad,"
 sql = sql & " frj300.frj3dto,frj300.frj3iva,frj300.frj3precnet,"
 sql = sql & " frj300.frj3imporlinea,frj300.frj3cantentreg,"
 sql = sql & " frj300.frj3tpbonif,frj300.frj3ptebonif,frj300.frj3tamenvase,"
 sql = sql & " frj300.frj3cantptefact,frj300.fr62codpedcompra,"
 sql = sql & " frj300.fr25coddetpedcomp,fr7300.fr73recargo,fr7300.fr73codproducto,"
 sql = sql & " frj300.frj3bonif,fr7300.fr00codgrpterap,fr7300.fr73referencia"
 sql = sql & " FROM frj100,frj300,fr7900,fr7300"
 sql = sql & " WHERE"
 sql = sql & " frj100.fr79codproveedor = fr7900.fr79codproveedor and"
 sql = sql & " frj300.fr73codproducto = fr7300.fr73codproducto and"
 sql = sql & " frj100.frj1codalbaran = frj300.frj1codalbaran and"
 sql = sql & " frj100.frj1codalbaran = ? "
 Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtCodigoAlbaran.Text
 Set rs = qry.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
 If Not rs.EOF Then
   lblFechaAlbaran = rs(0)
   lblEstadoAlbaran = rs(1)
   lblProveedor = rs(2)
   If rs(1) = "Facturado" Then ' facturado
     MsgBox "Albar�n ya Facturado. No se admiten modificaciones"
     blnFacturado = True
   Else
     blnFacturado = False
   End If
    ssgrdPendientes.RemoveAll
    nLineas = 0
    Do While Not rs.EOF
         ssgrdPendientes.AddItem rs(3) & Chr$(9) & rs(22) & Chr$(9) _
            & rs(4) & Chr$(9) & rs(5) & Chr$(9) & rs(6) & Chr$(9) & rs(7) & Chr$(9) _
            & rs(8) & Chr$(9) & rs(9) & Chr$(9) & rs(10) & Chr$(9) & rs(11) & Chr$(9) _
            & rs(12) & Chr$(9) & rs(13) & Chr$(9) & rs(14) & Chr$(9) & rs(15) & Chr$(9) _
            & rs(16) & Chr$(9) & rs(17) & Chr$(9) & rs(18) & Chr$(9) & rs(19) & Chr$(9) _
            & rs(20) & Chr$(9) & rs(21)
           ReDim Preserve MatrizAnterior(0 To nLineas)
        MatrizAnterior(nLineas).linea = rs(3)
        MatrizAnterior(nLineas).Producto = rs(4)
        MatrizAnterior(nLineas).CantidadPedida = rs(5)
        MatrizAnterior(nLineas).PrecioUnitario = rs(6)
        MatrizAnterior(nLineas).Descuento = rs(7)
        MatrizAnterior(nLineas).IVA = rs(8)
        MatrizAnterior(nLineas).PrecioNeto = rs(9)
        MatrizAnterior(nLineas).ImporteLinea = rs(10)
        MatrizAnterior(nLineas).CantidadEntregada = rs(11)
        MatrizAnterior(nLineas).bonificacion = rs(12)
        MatrizAnterior(nLineas).PendienteBonificacion = rs(13)
        MatrizAnterior(nLineas).Envase = rs(14)
        MatrizAnterior(nLineas).Pendientefacturar = rs(15)
        MatrizAnterior(nLineas).NumeroPedido = rs(16)
        MatrizAnterior(nLineas).LineaPedido = rs(17)
        MatrizAnterior(nLineas).Recargo = rs(18)
        MatrizAnterior(nLineas).CodigoProducto = rs(19)
        MatrizAnterior(nLineas).CantidadBonificada = rs(20)
        If Not IsNull(rs(21)) Then
          MatrizAnterior(nLineas).GrupoTerapeutico = rs(21)
        Else
          MatrizAnterior(nLineas).GrupoTerapeutico = ""
        End If
        If Not IsNull(rs(22)) Then
          MatrizAnterior(nLineas).referencia = rs(22)
        Else
          MatrizAnterior(nLineas).referencia = ""
        End If
        nLineas = nLineas + 1
        rs.MoveNext
    Loop
 Else
    lblFechaAlbaran = ""
    lblProveedor = ""
    txtCodigoAlbaran.SetFocus
    MsgBox "Albar�n no encontrado"
 End If
 rs.Close
 Set rs = Nothing
 Screen.MousePointer = vbArrow
 cmdfiltrar.Enabled = False

End Sub

Private Sub cmdReset_Click()
Dim i As Integer
ssgrdPendientes.MoveFirst
For i = 0 To ssgrdPendientes.Rows - 1
  ssgrdPendientes.Columns("LINEA").Value = MatrizActual(i).linea
  ssgrdPendientes.Columns("PRODUCTO").Value = MatrizAnterior(i).Producto
  ssgrdPendientes.Columns("CANTIDADPEDIDA").Value = MatrizAnterior(i).CantidadPedida
  ssgrdPendientes.Columns("PRECIOUNITARIO").Value = MatrizAnterior(i).PrecioUnitario
  ssgrdPendientes.Columns("DESCUENTO").Value = MatrizAnterior(i).Descuento
  ssgrdPendientes.Columns("IVA").Value = MatrizAnterior(i).IVA
  ssgrdPendientes.Columns("PRECIONETO").Value = MatrizAnterior(i).PrecioNeto
  ssgrdPendientes.Columns("IMPORTELINEA").Value = MatrizAnterior(i).ImporteLinea
  ssgrdPendientes.Columns("CANTIDADENTREGADA").Value = MatrizAnterior(i).CantidadEntregada
  ssgrdPendientes.Columns("BONIFICACION").Value = MatrizAnterior(i).bonificacion
  ssgrdPendientes.Columns("PENDIENTEBONIFICACION").Value = MatrizAnterior(i).PendienteBonificacion
  ssgrdPendientes.Columns("ENVASE").Value = MatrizAnterior(i).Envase
  ssgrdPendientes.Columns("PENDIENTEFACTURAR").Value = MatrizAnterior(i).Pendientefacturar
  ssgrdPendientes.Columns("NUMEROPEDIDO").Value = MatrizAnterior(i).NumeroPedido
  ssgrdPendientes.Columns("LINEAPEDIDO").Value = MatrizAnterior(i).LineaPedido
  ssgrdPendientes.Columns("RECARGO").Value = MatrizAnterior(i).Recargo
  ssgrdPendientes.Columns("CODIGOPRODUCTO").Value = MatrizAnterior(i).CodigoProducto
  ssgrdPendientes.Columns("CANTIDADBONIFICADA").Value = MatrizAnterior(i).CantidadBonificada
  ssgrdPendientes.Columns("GRUPOTERAPEUTICO").Value = MatrizAnterior(i).GrupoTerapeutico
  ssgrdPendientes.Columns("REFERENCIA").Value = MatrizAnterior(i).referencia
  ssgrdPendientes.MoveNext
Next i
cmdConfirmar.Enabled = False
cmdReset.Enabled = False
End Sub




Private Sub ssgrdPendientes_Change()
   If blnFacturado = False Then  ' facturado
    cmdDepurar.Enabled = True
   End If
End Sub
Private Sub txtCodigoAlbaran_Change()
  If txtCodigoAlbaran.Text <> "" Then
    cmdfiltrar.Enabled = True
  Else
    cmdfiltrar.Enabled = False
   End If

End Sub

'Private Sub ssgrdPendientes_KeyPress(KeyAscii As Integer)
' If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
'   KeyAscii = 13
'  MsgBox "Indique un valor num�rico"
'End If
'End Sub

'Private Sub Calcular_PVPU_PVLU(strCodProd As String, strGRP As String, _
'                               intPrecNet As Single, intEnvase As Integer, _
'                               intPVLU As Single, intPVPU As Single)
'  Dim stra  As String
'  Dim rsta As rdoResultset
'  Dim Param_Gen As Single
'  Dim qry As rdoQuery
'  Dim intPVL As Single
'  Dim intIVA As Integer
'  Dim intDescuento As Integer
'  'Dim intEnvase As Integer
'  Dim intRecargo As Integer
'
'  ' obtener datos de la ficha del producto
'  ' --------------------------------------
' stra = "SELECT fr7300.fr73pvl,fr7300.fr73descuento,fr7300.fr73tamenvase,"
' stra = stra & " fr7300.fr88codtipiva,fr7300.fr73recargo"
' stra = stra & " FROM fr7300"
' stra = stra & " WHERE"
' stra = stra & " fr7300.fr73codproducto = ? "
' Set qry = objApp.rdoConnect.CreateQuery("", stra)
'    qry(0) = strCodProd
' Set rsta = qry.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
' If Not rsta.EOF Then
'     If UCase(Left(strGRP, 1)) = "Q" Then
'      intPVL = rsta(0)
'      intDescuento = Format((intPVL * rs(1)) / 100, "0.00")
'      intRecargo = Format((intPVL * rs(4)) / 100, "0.00")
'      intEnvase = rsta(2)
'      intIVA = Format(((intPVL + intRecargo - intDescuento) * rs(3)) / 100, "0.00")
'  ' si es material UCase(Left(strGRP, 1) = "Q"
'  ' -------------------------------------------
'  ' PVLU = PrecioVentaCon = (Precio Unitario del Envase de la ficha del Producto
'  '                          + IVA del Producto) / Envase del producto
'  '                      IVA = (fr7300.fr73pvl * fr7300.fr88codtipiva)/100
'  '                 PVLU = (fr7300.fr73pvl + IVA)/fr7300.fr73tamenvase
'  ' PVPU = como estaba
'  ' si no es material UCase(Left(strGRP, 1) <> "Q"
'  ' -------------------------------------------
'  ' PVPU = PrecioVentaCon = ((Precio Unitario del Envase de la ficha del Producto
'  '                 + IVA del Producto - Descuento del Producto) / Envase del producto)
'  '                 * Param_Gen
'  '               Descuento = (fr7300.fr73pvl * fr7300.fr73descuento)/100
'  '               IVA = ((fr7300.fr73pvl - Descuento) * fr7300.fr88codtipiva)/100
'  '        PVPU = ((fr7300.fr73pvl + IVA - Descuento)/fr7300.fr73tamenvase) * Param_gen
'  ' PVPU = como estaba
'
'      If intPVL <= 2500 Then ' del Albar�n o del Envase ?????????????????????????
'        stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=22"
'      ElseIf intPVL <= 5000 Then
'        stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=23"
'      ElseIf intPVL <= 10000 Then
'        stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=24"
'      ElseIf intPVL <= 25000 Then
'        stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=25"
'      Else '>25000
'        stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=26"
'      End If
'
'      Set qry = objApp.rdoConnect.CreateQuery("", stra)
'      Set rsta = qry.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'      If Not rsta.EOF Then
'        Param_Gen = (intPVL * rsta(0).Value) / 100
'      Else
'        Param_Gen = 0
'      End If
'      rsta.Close
'      Set rsta = Nothing
'
'      intPVPU = Format(((intPVL + intRecargo + intIVA - intDescuento) / intEnvase) * Param_Gen, "0.00")
''      intPVLU = 0 ' como estaba
'    Else ' no es material
'      intPVL = rsta(0)
'      intEnvase = rsta(2)
'      intIVA = Format((intPVL * rs(3)) / 100, "0.00")
'
''      intPVPU = 0 ' como estaba
'      intPVLU = Format((intPVL + intIVA) / intEnvase, "0.00")
'    End If
' End If ' producto encontrado
'End Sub
