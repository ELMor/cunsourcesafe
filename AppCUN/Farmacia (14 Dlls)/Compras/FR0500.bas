Attribute VB_Name = "COM001"
Option Explicit

Public objsecurity As clsCWSecurity
Public objmouse As clsCWMouse

Public objCW As Object      ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object





Sub Main()
End Sub
Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  'End
End Sub



'Funci�n que devuelve el separador decimal que se corresponde con
'el de la configuraci�n regional de la m�quina.
Public Function SeparadorDec() As String

If Format("12,345", "00.000") = "12,345" Then
  SeparadorDec = ","
Else
  SeparadorDec = "."
End If

End Function

Public Function intCuentaCaracteres(ByVal strCaracter As String, ByVal strCadena As String) As Integer
  Dim intCuenta As Integer
  Dim intPos As Variant
  Dim strSubCadena As String
  Dim Fin As Boolean
  
  Fin = False
  intCuenta = 0
  intPos = 0
  strSubCadena = strCadena
  While Not Fin
    intPos = InStr(strSubCadena, strCaracter)
    If intPos = 0 Then
      Fin = True
    Else
      intCuenta = intCuenta + 1
      strSubCadena = Right(strSubCadena, Len(strSubCadena) - intPos)
    End If
  Wend
  intCuentaCaracteres = intCuenta
End Function

