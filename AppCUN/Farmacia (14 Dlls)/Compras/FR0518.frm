VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmEstSolCom 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Estudiar Solicitud de Compra"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0518.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CheckBox chkdenegadas 
      Caption         =   "Denegadas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10440
      TabIndex        =   32
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Solicitud de Compra"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7455
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   480
      Width           =   10140
      Begin TabDlg.SSTab tabTab1 
         Height          =   6900
         Index           =   0
         Left            =   120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   12171
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0518.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(11)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(10)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(13)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(15)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(14)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(16)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(12)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(17)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(18)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "Line1"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "dtcDateCombo1(1)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "dtcDateCombo1(0)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(6)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(4)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(3)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(0)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(8)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(2)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(5)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(1)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(9)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(7)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(10)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(11)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(12)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(13)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).ControlCount=   32
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0518.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   13
            Left            =   120
            TabIndex        =   43
            TabStop         =   0   'False
            Tag             =   "Solicitante|Persona que solicita el producto"
            Top             =   5880
            Width           =   4545
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR83PRODUCTOVAL"
            Height          =   330
            HelpContextID   =   30101
            Index           =   12
            Left            =   120
            TabIndex        =   0
            Tag             =   "Producto Aprobado"
            Top             =   2760
            Width           =   8505
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   30101
            Index           =   11
            Left            =   120
            TabIndex        =   4
            Tag             =   "C�d.Prov.|C�digo Proveedor"
            Top             =   3960
            Width           =   705
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   10
            Left            =   840
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Proveedor|Proveedor"
            Top             =   3960
            Width           =   7065
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   7
            Left            =   6840
            TabIndex        =   39
            Tag             =   "C�digo producto"
            Top             =   6480
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR83DESSOLCOMPR"
            Height          =   1215
            Index           =   9
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   34
            TabStop         =   0   'False
            Tag             =   "Observaciones"
            Top             =   960
            Width           =   9360
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR83PRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   120
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   "Producto Solicitado"
            Top             =   360
            Width           =   8505
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "FR83CANTIDAD"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   8640
            TabIndex        =   1
            Tag             =   "Cantidad Producto"
            Top             =   2760
            Width           =   825
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR00CODGRPTERAP"
            Height          =   330
            HelpContextID   =   30101
            Index           =   2
            Left            =   120
            TabIndex        =   2
            Tag             =   "Grp.Terap.|C�digo Grupo Terap�utico"
            Top             =   3360
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   8
            Left            =   1560
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Grupo Terap�utico|Grupo Terap�utico"
            Top             =   3360
            Width           =   7065
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR83NOTASCOMPRAS"
            Height          =   1095
            Index           =   0
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   6
            Tag             =   "Notas de Compras"
            Top             =   4560
            Width           =   9360
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR83CODSOLCOMPR"
            Height          =   330
            HelpContextID   =   30101
            Index           =   3
            Left            =   4920
            TabIndex        =   7
            Tag             =   "C�digo Solicitud Compra"
            Top             =   6480
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "SG02CODSOLICI"
            Height          =   330
            HelpContextID   =   30101
            Index           =   4
            Left            =   120
            TabIndex        =   8
            Tag             =   "C�d.Sol.|C�digo Persona que Solicita Compra"
            Top             =   5880
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR83INESTCOMP"
            Height          =   330
            HelpContextID   =   30101
            Index           =   6
            Left            =   8640
            TabIndex        =   9
            Tag             =   "Indicador Estado Solicitud"
            Top             =   6480
            Visible         =   0   'False
            Width           =   465
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6705
            Index           =   0
            Left            =   -74880
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   90
            Width           =   9255
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16325
            _ExtentY        =   11827
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR83FECCIESOLCOMP"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   11
            Tag             =   "Fecha Cierre Solicitud Compra"
            Top             =   6480
            Visible         =   0   'False
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR83FECENTRSOLCOMP"
            Height          =   330
            Index           =   1
            Left            =   2880
            TabIndex        =   10
            Tag             =   "Fecha Entrada Solicitud Compra"
            Top             =   6480
            Visible         =   0   'False
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Line Line1 
            X1              =   120
            X2              =   9360
            Y1              =   2400
            Y2              =   2400
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto Aprobado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   120
            TabIndex        =   42
            Tag             =   "Producto Aprobado"
            Top             =   2520
            Width           =   1650
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   120
            TabIndex        =   41
            Top             =   3720
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.PROD"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   6840
            TabIndex        =   40
            Top             =   6240
            Visible         =   0   'False
            Width           =   930
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Grupo Terap�utico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   120
            TabIndex        =   38
            Top             =   3120
            Width           =   1605
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto Solicitado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   120
            TabIndex        =   37
            Tag             =   "Producto Solicitado"
            Top             =   120
            Width           =   1680
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Motivo de la petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   36
            Top             =   720
            Width           =   1800
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   8640
            TabIndex        =   35
            Top             =   2520
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "DENEGADA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   195
            Index           =   15
            Left            =   4560
            TabIndex        =   31
            Top             =   120
            Width           =   1035
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "APROBADA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Index           =   13
            Left            =   3360
            TabIndex        =   30
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Notas de Compas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   29
            Top             =   4320
            Width           =   1500
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Entrada Solicitud Compra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   2880
            TabIndex        =   28
            Top             =   6240
            Visible         =   0   'False
            Width           =   2745
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Cierre Solicitud Compra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   120
            TabIndex        =   27
            Top             =   6240
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Solicitud Compra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   4920
            TabIndex        =   26
            Top             =   6240
            Visible         =   0   'False
            Width           =   1830
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Persona que Solicita"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   25
            Top             =   5640
            Width           =   1770
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Indicador Estado Solicitud"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   11
            Left            =   6840
            TabIndex        =   24
            Top             =   6000
            Visible         =   0   'False
            Width           =   2610
         End
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   10440
      TabIndex        =   17
      Top             =   1200
      Width           =   1455
      Begin VB.CommandButton Command1 
         Caption         =   "&Aprobar"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton Command1 
         Caption         =   "De&negar"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   16
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "F.F."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   7440
         TabIndex        =   23
         Top             =   0
         Width           =   345
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Dosis"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   8040
         TabIndex        =   22
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "U.M."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   8640
         TabIndex        =   21
         Top             =   0
         Width           =   420
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Referencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   7440
         TabIndex        =   20
         Top             =   600
         Width           =   945
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   18
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmEstSolCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA, COMPRAS                                          *
'* NOMBRE: FrmEstSolCom (FR0518.FRM)                                    *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA:                                                               *
'* DESCRIPCION: Estudiar Solicitud de Compra                            *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gintIndice As Integer



Private Sub chkdenegadas_Click()

  If chkdenegadas.Value = 1 Then
    objWinInfo.objWinActiveForm.strWhere = " FR83INESTCOMP=2 "
  Else
    objWinInfo.objWinActiveForm.strWhere = " FR83INESTCOMP=0 "
  End If
  
  Call objWinInfo.DataRefresh

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Solicitud Compra"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FR8300"
    
    .blnAskPrimary = False
    .intAllowance = cwAllowModify
    .strWhere = " FR83INESTCOMP=0 "
    .blnHasMaint = True
    Call .FormAddOrderField("FR83PRODUCTO", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Solicitud Compra")

    Call .FormAddFilterWhere(strKey, "FR83PRODUCTO", "Producto", cwString)
    Call .FormAddFilterWhere(strKey, "FR83DESSOLCOMPR", "Motivo Solicitud", cwString)
    Call .FormAddFilterWhere(strKey, "SG02CODSOLICI", "C�digo Persona Solicita", cwString)
    Call .FormAddFilterWhere(strKey, "FR83FECENTRSOLCOMP", "Fecha Entrada Solicitud Compra", cwDate)
    Call .FormAddFilterWhere(strKey, "FR83CANTIDAD", "Unidades solicitadas", cwDecimal)
    
    Call .FormAddFilterOrder(strKey, "FR83DPRODUCTO", "Producto")
    Call .FormAddFilterOrder(strKey, "FR83DESSOLCOMPR", "Motivo Solicitud")
    Call .FormAddFilterOrder(strKey, "SG02CODSOLICI", "C�digo Persona Solicita")
    Call .FormAddFilterOrder(strKey, "FR83FECENTRSOLCOMP", "Fecha Entrada Solicitud Compra")
    Call .FormAddFilterOrder(strKey, "FR83CANTIDAD", "Unidades solicitadas")
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)

    .CtrlGetInfo(txtText1(1)).blnInFind = True 'Producto
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True 'Producto
    .CtrlGetInfo(txtText1(9)).blnReadOnly = True 'Proveedor
    .CtrlGetInfo(txtText1(2)).blnInFind = True 'C�d. Grupo Terap�utico
    .CtrlGetInfo(txtText1(0)).blnInFind = True 'Motivos
    .CtrlGetInfo(txtText1(4)).blnInFind = True 'Persona que pide
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True 'Fecha redacci�n
         
     .CtrlGetInfo(txtText1(0)).blnMandatory = True 'Notas de compras
    .CtrlGetInfo(txtText1(5)).blnMandatory = True 'Cantidad
    .CtrlGetInfo(txtText1(12)).blnMandatory = True 'producto aprovado
    .CtrlGetInfo(txtText1(11)).blnMandatory = True 'C�d. Proveedor
    .CtrlGetInfo(txtText1(2)).blnMandatory = True 'C�d. Grupo terap.
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True 'C�d. grupo terap�utico
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR00CODGRPTERAP", "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(8), "FR00DESGRPTERAP")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(10), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "SG02COD", "SELECT SG02NOM ||' '|| SG02APE1 ||' '|| SG02APE2 " & """Solicitante""" & "  FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(13), "Solicitante")
    
    Call .WinRegister
    Call .WinStabilize
  End With
        
  lblLabel1(13).Visible = False
  lblLabel1(15).Visible = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
 If strFormName = "Solicitud Compra" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR0000"
     '.strWhere = ""
     .strOrder = "ORDER BY FR00CODGRPTERAP ASC"
     
     Set objField = .AddField("FR00CODGRPTERAP")
     objField.strSmallDesc = "C�d.Grp.Terap."
     
     Set objField = .AddField("FR00DESGRPTERAP")
     objField.strSmallDesc = "Grupo Terap�utico"
         
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("FR00CODGRPTERAP"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
  If strFormName = "Solicitud Compra" And strCtrl = "txtText1(11)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     '.strWhere = ""
     .strOrder = "ORDER BY FR79PROVEEDOR ASC"
     
     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�d.Prov."
     
     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Proveedor"
         
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(11), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
   Set objSearch = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call objsecurity.LaunchProcess("FR0026")
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
        


  Select Case txtText1(6).Text
    Case 0 'Solicitudes redactadas
      Command1(0).Enabled = True
      Command1(1).Enabled = True
      lblLabel1(13).Visible = False
      lblLabel1(15).Visible = False
      txtText1(5).Locked = False
      txtText1(0).Locked = False
      txtText1(11).Locked = False
      txtText1(2).Locked = False
      txtText1(12).Locked = False
    Case 1 'Solicitudes aprobadas
      Command1(0).Enabled = False
      Command1(1).Enabled = False
      lblLabel1(13).Visible = True
      lblLabel1(15).Visible = False
      'objWinInfo.CtrlGetInfo(txtText1(5)).blnReadOnly = True
      'objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = True
      txtText1(5).Locked = True
      txtText1(0).Locked = True
      txtText1(11).Locked = True
      txtText1(2).Locked = True
      txtText1(12).Locked = True
    Case 2 'Solicitudes denegadas
      Command1(0).Enabled = False
      Command1(1).Enabled = False
      lblLabel1(13).Visible = False
      lblLabel1(15).Visible = True
      'objWinInfo.CtrlGetInfo(txtText1(5)).blnReadOnly = True
      'objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = True
      txtText1(5).Locked = True
      txtText1(0).Locked = True
      txtText1(11).Locked = True
      txtText1(2).Locked = True
      txtText1(12).Locked = True
  End Select
  
  If txtText1(6).Text = "" Then
    Command1(0).Enabled = False
    Command1(1).Enabled = False
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim rstFec As rdoResultset
Dim strFec As String
        
  Select Case btnButton.Index
  Case 2 'Nuevo
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR83CODSOLCOMPR_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(3), rsta.rdoColumns(0).Value)
    rsta.Close
    Set rsta = Nothing
    Call objWinInfo.CtrlSet(txtText1(4), objsecurity.strUser)
    strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(1), rstFec.rdoColumns(0).Value)
    rstFec.Close
    Set rstFec = Nothing
  Case Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End Select


End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub txtText1_DblClick(Index As Integer)
  If Index = 2 Then
    Call objWinInfo_cwForeign("Solicitud Compra", "txtText1(2)")
  End If
  If Index = 11 Then
    Call objWinInfo_cwForeign("Solicitud Compra", "txtText1(11)")
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


Private Sub Command1_Click(Index As Integer)
  Dim strupdate As String
  Dim strinsert As String
  Dim strPrd As String
  Dim rstPrd As rdoResultset
  Dim qryUpd  As rdoQuery
  
  If Not IsNumeric(txtText1(5).Text) Then
    'La cantidad no est� bien introducida
    Call objWinInfo.CtrlSet(txtText1(5), 1)
  End If
  If (Len(Trim(txtText1(12).Text)) = 0) Or (IsNull(Len(Trim(txtText1(12).Text)))) Then
    'En compras est�n de acuerdo con el producto descrito por el solicitante
     Call objWinInfo.CtrlSet(txtText1(12), txtText1(1))
  End If
  If Not IsNumeric(txtText1(11).Text) Then
    'No existe el proveedor
     MsgBox "Falta ProVeedor", vbInformation, "Aprobar Solicitud"
     Command1(0).Enabled = True
     Command1(1).Enabled = True
     Exit Sub
  End If
  If (IsNull(Len(Trim(txtText1(0).Text)))) Or (Len(Trim(txtText1(0).Text)) = 0) Then
    'No existen notas de compas
    MsgBox "Faltan las Notas de Compras", vbInformation, "Aprobar Solicitud"
    Command1(0).Enabled = True
    Command1(1).Enabled = True
    Exit Sub
  End If

  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataSave
  
  Command1(0).Enabled = False
  Command1(1).Enabled = False

  If txtText1(3).Text <> "" Then
    Select Case Index
      Case 0
          
        strPrd = "SELECT FR73CODPRODUCTO_SEQUENCE.nextval FROM dual"
        Set rstPrd = objApp.rdoConnect.OpenResultset(strPrd)
                        
        'lblLabel1(13).Visible = True
        lblLabel1(15).Visible = False
        
        strinsert = "INSERT INTO FR7300 (FR73CODPRODUCTO,FR73CODINTFAR,FR73CODINTFARSEG,FR73DESPRODUCTOPROV, " & _
                    "FR93CODUNIMEDIDA,FR00CODGRPTERAP,FR88CODTIPIVA,FR73PRECIONETCOMPRA,FR73PRECBASE, " & _
                    "FR73TAMENVASE, FR73FECINIVIG,FR73INDVISIBLE,FR73PVL,FR79CODPROVEEDOR_A) VALUES ("
        
        strinsert = strinsert & rstPrd.rdoColumns(0).Value & ",'" & 999999 & "',1,'" & txtText1(12).Text & "'," & _
                    "'NE','" & txtText1(2).Text & "',0,0,0," & _
                    "1,SYSDATE,0,0," & txtText1(11).Text & ")"
        objApp.rdoConnect.Execute strinsert, 64
        
        strupdate = "UPDATE FR8300 " & _
                    "   SET FR83INESTCOMP = ? , FR83PRODUCTOVAL = ?, " & _
                    "       FR73CODPRODUCTO = ? " & _
                    " WHERE FR83CODSOLCOMPR = ? "
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
        qryUpd(0) = 1
        qryUpd(1) = txtText1(12).Text
        qryUpd(2) = rstPrd.rdoColumns(0).Value
        qryUpd(3) = txtText1(3).Text
        qryUpd.Execute
        qryUpd.Close
        Set qryUpd = Nothing
        
        rstPrd.Close
        Set rstPrd = Nothing
        MsgBox "Solicitud Aprobada", vbInformation, "Estudiar Solicitud de Compra"
      Case 1
        strupdate = "UPDATE FR8300 " & _
                    "   SET FR83INESTCOMP = ? " & _
                    " WHERE FR83CODSOLCOMPR = ? "
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
        qryUpd(0) = 2
        qryUpd(1) = txtText1(3).Text
        qryUpd.Execute
        qryUpd.Close
        Set qryUpd = Nothing
        
        MsgBox "Solicitud Denegada", vbInformation, "Estudiar Solicitud de Compra"
        lblLabel1(13).Visible = False
        'lblLabel1(15).Visible = True
    End Select
  End If
  'txtText1(5).Locked = True
  'txtText1(0).Locked = True
  'txtText1(11).Locked = True
  'txtText1(2).Locked = True
  'txtText1(12).Locked = True
  
  Command1(0).Enabled = True
  Command1(1).Enabled = True
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataRefresh
End Sub
