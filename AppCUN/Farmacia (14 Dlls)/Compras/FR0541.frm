VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmEliminarFacturas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Eliminar Facturas"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "B�squeda por "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   2775
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   9255
      Begin VB.TextBox txtProveedor 
         Height          =   330
         Left            =   4920
         MaxLength       =   50
         TabIndex        =   12
         Tag             =   "Factura"
         ToolTipText     =   "Factura"
         Top             =   1440
         Width           =   4095
      End
      Begin VB.Frame Frame3 
         Caption         =   "Fecha de Conformidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008080&
         Height          =   1095
         Left            =   240
         TabIndex        =   15
         Top             =   1560
         Width           =   4215
         Begin SSCalendarWidgets_A.SSDateCombo dtcinicioconf 
            Height          =   330
            Left            =   120
            TabIndex        =   16
            Tag             =   "Fecha Inicio"
            ToolTipText     =   "Fecha Inicio"
            Top             =   600
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcfinconf 
            Height          =   330
            Left            =   2160
            TabIndex        =   17
            Tag             =   "Fecha Fin"
            ToolTipText     =   "Fecha Fin"
            Top             =   600
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            Height          =   195
            Index           =   3
            Left            =   2160
            TabIndex        =   19
            Top             =   360
            Width           =   705
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   18
            Top             =   360
            Width           =   870
         End
      End
      Begin VB.CommandButton cmdfiltrar 
         Caption         =   "FILTRAR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6120
         TabIndex        =   14
         Top             =   2040
         Width           =   1455
      End
      Begin VB.TextBox txtFactura 
         Height          =   330
         Left            =   4920
         MaxLength       =   25
         TabIndex        =   11
         Tag             =   "Factura"
         ToolTipText     =   "Factura"
         Top             =   720
         Width           =   2415
      End
      Begin VB.Frame Frame5 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008080&
         Height          =   1095
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   4215
         Begin SSCalendarWidgets_A.SSDateCombo dtcinicio 
            Height          =   330
            Left            =   120
            TabIndex        =   7
            Tag             =   "Fecha Inicio"
            ToolTipText     =   "Fecha Inicio"
            Top             =   600
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcfin 
            Height          =   330
            Left            =   2160
            TabIndex        =   8
            Tag             =   "Fecha Fin"
            ToolTipText     =   "Fecha Fin"
            Top             =   600
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   10
            Top             =   360
            Width           =   870
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            Height          =   195
            Index           =   1
            Left            =   2160
            TabIndex        =   9
            Top             =   360
            Width           =   705
         End
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Proveedor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004080&
         Height          =   195
         Index           =   4
         Left            =   4920
         TabIndex        =   20
         Top             =   1200
         Width           =   885
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d.Factura"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004080&
         Height          =   195
         Index           =   21
         Left            =   4920
         TabIndex        =   13
         Top             =   480
         Width           =   1050
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Facturas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   4575
      Left            =   120
      TabIndex        =   3
      Top             =   3480
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Height          =   3975
         Left            =   120
         TabIndex        =   4
         Top             =   480
         Width           =   11460
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   6
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�d.Factura"
         Columns(0).Name =   "C�d.Factura"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Caption=   "Fecha"
         Columns(1).Name =   "Fecha"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Caption=   "Proveedor"
         Columns(2).Name =   "Proveedor"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasForeColor=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "Importe"
         Columns(3).Name =   "Importe"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).HasForeColor=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Caption=   "Fecha Conformidad"
         Columns(4).Name =   "Fecha Conformidad"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).HasForeColor=   -1  'True
         Columns(5).Width=   3200
         Columns(5).Caption=   "Asiento Contable?"
         Columns(5).Name =   "Asiento Contable?"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(5).HasForeColor=   -1  'True
         _ExtentX        =   20214
         _ExtentY        =   7011
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdEliminarFactura 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Eliminar Factura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9600
      TabIndex        =   2
      Top             =   1680
      Width           =   2175
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmEliminarFacturas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmEliminarFacturas(FR0541.frm)                              *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: AGOSTO DEL 2000                                               *
'* DESCRIPCION: Eliminar Facturas                                       *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdEliminarFactura_Click()
Dim mensaje As Integer
Dim strdelete As String
Dim rsta As rdoResultset
Dim stra As String
Dim strupdate As String

If SSDBGrid1.Rows > 0 Then
  mensaje = MsgBox("�Est� seguro que desea eliminar la factura : " & SSDBGrid1.Columns("C�d.Factura").Value & " ?", vbYesNo, "Eliminaci�n de Facturas")
  If mensaje = 6 Then 'si
    'Se borran los asientos contables
    stra = "SELECT FRT1CODCAB FROM FRT100 WHERE FRT1INVOICEID="
    stra = stra & "'" & SSDBGrid1.Columns("C�d.Factura").Value & "'"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
        strdelete = "DELETE FRT300 WHERE FRT1CODCAB=" & rsta.rdoColumns("FRT1CODCAB").Value
        objApp.rdoConnect.Execute strdelete, 64
        objApp.rdoConnect.Execute "commit", 64
        strdelete = "DELETE FRT200 WHERE FRT1CODCAB=" & rsta.rdoColumns("FRT1CODCAB").Value
        objApp.rdoConnect.Execute strdelete, 64
        objApp.rdoConnect.Execute "commit", 64
        strdelete = "DELETE FRT100 WHERE FRT1CODCAB=" & rsta.rdoColumns("FRT1CODCAB").Value
        objApp.rdoConnect.Execute strdelete, 64
        objApp.rdoConnect.Execute "commit", 64
    End If
    rsta.Close
    Set rsta = Nothing
    'Se modifica el estado de los albaranes
    strupdate = "UPDATE FRJ100 SET FRJ1ESTADO=0 WHERE FRJ1CODALBARAN IN "
    strupdate = strupdate & " (SELECT FRJ1CODALBARAN FROM FRJ400 WHERE FR39CODFACTCOMPRA="
    strupdate = strupdate & "'" & SSDBGrid1.Columns("C�d.Factura").Value & "'" & ")"
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "commit", 64
    'Se modifica la cantidad pendiente de facturar a la cantidad pedida
    strupdate = "UPDATE FRJ300 SET FRJ3CANTPTEFACT=FRJ3CANTPEDIDA WHERE FRJ1CODALBARAN IN "
    strupdate = strupdate & " (SELECT FRJ1CODALBARAN FROM FRJ400 WHERE FR39CODFACTCOMPRA="
    strupdate = strupdate & "'" & SSDBGrid1.Columns("C�d.Factura").Value & "'" & ")"
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "commit", 64
    'Se borra la relaci�n Albar�n-Factura
    strdelete = "DELETE FRJ400 WHERE FR39CODFACTCOMPRA=" & "'" & SSDBGrid1.Columns("C�d.Factura").Value & "'"
    objApp.rdoConnect.Execute strdelete, 64
    objApp.rdoConnect.Execute "commit", 64
    'Se borran los hijos de la factura
    strdelete = "DELETE FR1500 WHERE FR39CODFACTCOMPRA=" & "'" & SSDBGrid1.Columns("C�d.Factura").Value & "'"
    objApp.rdoConnect.Execute strdelete, 64
    objApp.rdoConnect.Execute "commit", 64
    'Se borra la factura
    strdelete = "DELETE FR3900 WHERE FR39CODFACTCOMPRA=" & "'" & SSDBGrid1.Columns("C�d.Factura").Value & "'"
    objApp.rdoConnect.Execute strdelete, 64
    objApp.rdoConnect.Execute "commit", 64
    Call cmdFiltrar_Click
  End If
End If
End Sub

Private Sub cmdFiltrar_Click()
Dim strWhere As String
Dim rsta As rdoResultset
Dim stra As String
Dim fecha
Dim fechaconformidad
Dim asiento
Dim importe
Dim rstasiento As rdoResultset
Dim strasiento As String
Dim rstprov As rdoResultset
Dim strProv As String

Screen.MousePointer = vbHourglass
cmdfiltrar.Enabled = False

strWhere = "-1=-1"

'C�digo de Factura
If Trim(txtFactura.Text) <> "" Then
    strWhere = strWhere & " AND FR39CODFACTCOMPRA=" & "'" & txtFactura.Text & "'"
End If
'Proveedor
If Trim(txtProveedor.Text) <> "" Then
    strWhere = strWhere & " AND FR79CODPROVEEDOR IN (SELECT FR7900.FR79CODPROVEEDOR FROM FR7900 "
    strWhere = strWhere & " WHERE UPPER(FR7900.FR79PROVEEDOR) LIKE UPPER('%"
    strWhere = strWhere & txtProveedor.Text & "%'))"
End If
'Fecha
If IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
  strWhere = strWhere & " AND TRUNC(FR39FECFACTCOMPRA) BETWEEN " & _
           "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
           "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
End If
If IsDate(dtcinicio.Date) And Not IsDate(dtcfin.Date) Then
  strWhere = strWhere & " AND TRUNC(FR39FECFACTCOMPRA) BETWEEN " & _
           "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
           "TO_DATE('31/12/9999','DD/MM/YYYY')"
End If
If Not IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
  strWhere = strWhere & " AND TRUNC(FR39FECFACTCOMPRA) BETWEEN " & _
           "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
           "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
End If
'Fecha de Conformidad
If IsDate(dtcinicioconf.Date) And IsDate(dtcfinconf.Date) Then
  strWhere = strWhere & " AND TRUNC(FR39FECCONF) BETWEEN " & _
           "TO_DATE('" & dtcinicioconf.Date & "','DD/MM/YYYY') AND " & _
           "TO_DATE('" & dtcfinconf.Date & "','DD/MM/YYYY')"
End If
If IsDate(dtcinicioconf.Date) And Not IsDate(dtcfinconf.Date) Then
  strWhere = strWhere & " AND TRUNC(FR39FECCONF) BETWEEN " & _
           "TO_DATE('" & dtcinicioconf.Date & "','DD/MM/YYYY') AND " & _
           "TO_DATE('31/12/9999','DD/MM/YYYY')"
End If
If Not IsDate(dtcinicioconf.Date) And IsDate(dtcfinconf.Date) Then
  strWhere = strWhere & " AND TRUNC(FR39FECCONF) BETWEEN " & _
           "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
           "TO_DATE('" & dtcfinconf.Date & "','DD/MM/YYYY')"
End If

If strWhere = "-1=-1" Then
  Call MsgBox("Debe especificar un criterio de b�squeda.", vbInformation, "Aviso")
  cmdfiltrar.Enabled = True
  Screen.MousePointer = vbDefault
  Exit Sub
End If


'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
SSDBGrid1.RemoveAll

stra = "SELECT FR3900.* FROM FR3900 WHERE " & strWhere
stra = stra & " ORDER BY FR39FECFACTCOMPRA"

Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
  'Importe
  If Not IsNull(rsta.rdoColumns("FR39IMPORTE").Value) Then
    importe = rsta.rdoColumns("FR39IMPORTE").Value
  Else
    importe = " "
  End If
  'Fecha
  If Not IsNull(rsta.rdoColumns("FR39FECFACTCOMPRA").Value) Then
    fecha = rsta.rdoColumns("FR39FECFACTCOMPRA").Value
  Else
    fecha = " "
  End If
  'Fecha Conformidad
  If Not IsNull(rsta.rdoColumns("FR39FECCONF").Value) Then
    fechaconformidad = rsta.rdoColumns("FR39FECCONF").Value
  Else
    fechaconformidad = " "
  End If
  'Asiento
  strasiento = "SELECT * FROM FRT100 WHERE FRT1INVOICEID="
  strasiento = strasiento & "'" & rsta.rdoColumns("FR39CODFACTCOMPRA").Value & "'"
  Set rstasiento = objApp.rdoConnect.OpenResultset(strasiento)
  If Not rstasiento.EOF Then
    asiento = 1
  Else
    asiento = 0
  End If
  rstasiento.Close
  Set rstasiento = Nothing
  'Proveedor
  strProv = "SELECT FR79PROVEEDOR FROM FR7900 WHERE FR79CODPROVEEDOR=" & rsta.rdoColumns("FR79CODPROVEEDOR").Value
  Set rstprov = objApp.rdoConnect.OpenResultset(strProv)

  SSDBGrid1.AddItem rsta.rdoColumns("FR39CODFACTCOMPRA").Value & Chr(vbKeyTab) & _
                   fecha & Chr(vbKeyTab) & _
                   rstprov.rdoColumns("FR79PROVEEDOR").Value & Chr(vbKeyTab) & _
                   importe & Chr(vbKeyTab) & _
                   fechaconformidad & Chr(vbKeyTab) & _
                   asiento
rsta.MoveNext
rstprov.Close
Set rstprov = Nothing
Wend
rsta.Close
Set rsta = Nothing

cmdfiltrar.Enabled = True
Screen.MousePointer = vbDefault
End Sub











Private Sub SSDBGrid1_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = 0
End Sub


Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub




Private Sub Form_Activate()
Call Inicializar_Toolbar
Call Inicializar_Grid
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Select Case btnButton.Index
  Case 30:
          Unload Me
End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If intIndex = 100 Then
  Unload Me
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub



Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(21).Enabled = False
   tlbToolbar1.Buttons.Item(22).Enabled = False
   tlbToolbar1.Buttons.Item(23).Enabled = False
   tlbToolbar1.Buttons.Item(24).Enabled = False
   tlbToolbar1.Buttons.Item(26).Enabled = False
   
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuRegistroOpcion(40).Enabled = False
   mnuRegistroOpcion(50).Enabled = False
   mnuRegistroOpcion(60).Enabled = False
   mnuRegistroOpcion(70).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub
Private Sub Inicializar_Grid()
Dim i As Integer

For i = 0 To 5
  SSDBGrid1.Columns(i).Locked = True
Next i

SSDBGrid1.Columns("C�d.Factura").Width = 1700
SSDBGrid1.Columns("Fecha").Width = 1200
SSDBGrid1.Columns("Proveedor").Width = 3000
SSDBGrid1.Columns("Importe").Width = 1500
SSDBGrid1.Columns("Fecha Conformidad").Width = 1600
SSDBGrid1.Columns("Asiento Contable?").Width = 1500

txtFactura.SetFocus

End Sub

Private Sub txtFactura_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 13 Then
  Call cmdFiltrar_Click
End If
End Sub
