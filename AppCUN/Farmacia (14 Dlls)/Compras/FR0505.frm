VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmIntOfe 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Introducir Ofertas"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0505.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   32
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4305
      Index           =   1
      Left            =   120
      TabIndex        =   27
      Top             =   3600
      Width           =   11745
      Begin VB.CommandButton Command1 
         Caption         =   "Material &Sanitario"
         Enabled         =   0   'False
         Height          =   375
         Index           =   2
         Left            =   5520
         TabIndex        =   14
         Top             =   3720
         Width           =   1575
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Medicamentos"
         Enabled         =   0   'False
         Height          =   375
         Index           =   1
         Left            =   3840
         TabIndex        =   13
         Top             =   3720
         Width           =   1575
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   3180
         Index           =   1
         Left            =   120
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   360
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   5609
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0505.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(9)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(14)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(15)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(16)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(17)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(18)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(3)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(4)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(2)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(14)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(12)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(10)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(13)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(11)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(3)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(16)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(22)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0505.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR58CODOFERTPROV"
            Height          =   330
            HelpContextID   =   30101
            Index           =   22
            Left            =   6120
            TabIndex        =   46
            Tag             =   "C�digo Oferta"
            Top             =   120
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR23CODDETOFERTA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   16
            Left            =   3600
            TabIndex        =   12
            Tag             =   "C�digo Detalle Oferta"
            Top             =   120
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR23OBSERVACIONES"
            Height          =   615
            Index           =   3
            Left            =   240
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   23
            Tag             =   "Ovservaciones"
            Top             =   1680
            Width           =   10680
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8880
            Locked          =   -1  'True
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "U.M. Unidad de Medida"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   7680
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "F.F. Forma Farmac�utica"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   8280
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   9480
            Locked          =   -1  'True
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Volumen mL"
            Top             =   480
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   7680
            Locked          =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   1080
            Width           =   3225
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   240
            TabIndex        =   15
            Tag             =   "C�digo Producto"
            Top             =   480
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   1800
            TabIndex        =   16
            Tag             =   "Descripci�n Producto"
            Top             =   480
            Width           =   5850
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR23PRECFERTADO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   240
            TabIndex        =   21
            Tag             =   "Precio Ofertado"
            Top             =   1080
            Width           =   1785
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2760
            Index           =   1
            Left            =   -74880
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   240
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   4868
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "L�nea de la Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   1920
            TabIndex        =   48
            Top             =   240
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   5400
            TabIndex        =   47
            Top             =   240
            Visible         =   0   'False
            Width           =   540
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   7680
            TabIndex        =   45
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   8280
            TabIndex        =   44
            Top             =   240
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   8880
            TabIndex        =   43
            Top             =   240
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   9480
            TabIndex        =   42
            Top             =   240
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   7680
            TabIndex        =   41
            Top             =   840
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   31
            Top             =   240
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   240
            TabIndex        =   30
            Top             =   1440
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Ofertado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   240
            TabIndex        =   29
            Top             =   840
            Width           =   1350
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Oferta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3045
      Index           =   0
      Left            =   90
      TabIndex        =   25
      Top             =   450
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   2535
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   11490
         _ExtentX        =   20267
         _ExtentY        =   4471
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0505.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(8)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(10)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(11)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(12)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(13)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(19)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtcDateCombo1(2)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtcDateCombo1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(5)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(6)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(8)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(9)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(15)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(7)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(23)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(24)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).ControlCount=   22
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0505.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR67CODPETOFERT"
            Height          =   330
            HelpContextID   =   30101
            Index           =   24
            Left            =   7080
            TabIndex        =   51
            Tag             =   "C�digo Petici�n Oferta"
            Top             =   0
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR99CODESTOFERTPROV"
            Enabled         =   0   'False
            Height          =   330
            HelpContextID   =   30101
            Index           =   23
            Left            =   4080
            TabIndex        =   49
            Tag             =   "Estado"
            Text            =   "1"
            Top             =   0
            Visible         =   0   'False
            Width           =   465
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR58DESOFERTA"
            Height          =   615
            Index           =   7
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   1
            Tag             =   "Descripci�n Oferta"
            Top             =   360
            Width           =   10440
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR58CODOFERTPROV"
            Height          =   330
            HelpContextID   =   30101
            Index           =   15
            Left            =   1320
            TabIndex        =   0
            Tag             =   "C�digo Oferta"
            Top             =   120
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   9
            Left            =   1680
            TabIndex        =   9
            Tag             =   "Nombre Proveedor"
            Top             =   1920
            Width           =   8865
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   30101
            Index           =   8
            Left            =   120
            TabIndex        =   8
            Tag             =   "C�digo Proveedor"
            Top             =   1920
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR58RECARGOFINANC"
            Height          =   330
            HelpContextID   =   30101
            Index           =   6
            Left            =   9120
            TabIndex        =   7
            Tag             =   "Recargos"
            Top             =   1320
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR58EMBALAJES"
            Height          =   330
            HelpContextID   =   30101
            Index           =   5
            Left            =   7560
            TabIndex        =   6
            Tag             =   "Embalajes"
            Top             =   1320
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR58PORTES"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   6000
            TabIndex        =   5
            Tag             =   "Portes"
            Top             =   1320
            Width           =   1425
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2250
            Index           =   0
            Left            =   -74910
            TabIndex        =   10
            Top             =   120
            Width           =   10965
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19341
            _ExtentY        =   3969
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR58FECRECEPOFERT"
            Height          =   330
            Index           =   1
            Left            =   120
            TabIndex        =   2
            Tag             =   "Fecha Recepci�n"
            Top             =   1305
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR58FECINIOFERT"
            Height          =   330
            Index           =   2
            Left            =   2040
            TabIndex        =   3
            Tag             =   "Fecha Inicio"
            Top             =   1305
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR58FECFINOFERT"
            Height          =   330
            Index           =   3
            Left            =   3960
            TabIndex        =   4
            Tag             =   "Fecha Fin"
            Top             =   1305
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Petici�n Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   5040
            TabIndex        =   52
            Top             =   120
            Visible         =   0   'False
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   3360
            TabIndex        =   50
            Top             =   120
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   120
            TabIndex        =   40
            Top             =   1680
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Recargos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   9120
            TabIndex        =   39
            Top             =   1080
            Width           =   825
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Embalajes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   7560
            TabIndex        =   38
            Top             =   1080
            Width           =   870
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Portes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   6000
            TabIndex        =   37
            Top             =   1080
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   3960
            TabIndex        =   36
            Top             =   1080
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   2040
            TabIndex        =   35
            Top             =   1080
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Recepci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   34
            Top             =   1080
            Width           =   1515
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   33
            Top             =   120
            Width           =   540
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   28
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmIntOfe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmIntOfe (FR0505.FRM)                                       *
'* AUTOR: Aitor Vi�uela Garc�a                                          *
'* FECHA: Mayo DE 1999                                                  *
'* DESCRIPCION: Introducir Ofertas que pedimos a los proveedores        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm
Dim strKey As String
Dim FR58CODOFERTPROVSEQUENCE
Dim rsta As rdoResultset
Dim sqlstr As String
Dim auxFR23CODDETOFERTA
Dim rstb As rdoResultset
Dim strInsertOferta As String
   
   Set objWinInfo = New clsCWWin
   
   Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
   
   With objMasterInfo
      Set .objFormContainer = fraFrame1(0)
      Set .objFatherContainer = Nothing
      Set .tabMainTab = tabTab1(0)
      Set .grdGrid = grdDBGrid1(0)
      .strName = "Oferta"
      .strTable = "FR5800"
      .intAllowance = cwAllowAll
      .blnAskPrimary = False
      
      Call .FormAddOrderField("FR58DESOFERTA", cwDescending)
      
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Ofertas")
      Call .FormAddFilterWhere(strKey, "FR58DESOFERTA", "Descripci�n Oferta", cwString)
      Call .FormAddFilterWhere(strKey, "FR58FECRECEPOFERT", "Fecha Recepci�n", cwDate)
      Call .FormAddFilterWhere(strKey, "FR58FECINIOFERT", "Fecha Inicio", cwDate)
      Call .FormAddFilterWhere(strKey, "FR58FECFINOFERT", "Fecha Fin", cwDate)
      Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR79PROVEEDOR", "Nombre Proveedor", cwNumeric)
      Call .FormAddFilterOrder(strKey, "FR58DESOFERTA", "Descripci�n Oferta")
      Call .FormAddFilterOrder(strKey, "FR58FECRECEPOFERT", "Fecha Recepci�n")
      Call .FormAddFilterOrder(strKey, "FR58FECINIOFERT", "Fecha Inicio")
      Call .FormAddFilterOrder(strKey, "FR58FECFINOFERT", "Fecha Fin")
      Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor")
   End With
   
   With objDetailInfo
      .strName = "Productos"
      Set .objFormContainer = fraFrame1(1)
      Set .objFatherContainer = fraFrame1(0)
      Set .tabMainTab = tabTab1(1)
      Set .grdGrid = grdDBGrid1(1)
      '.strDataBase = objEnv.GetValue("Main")
      .strTable = "FR2300" ' Detalle de la oferta
      .intAllowance = cwAllowAll
      .blnAskPrimary = False
      
      Call .FormAddOrderField("FR73CODPRODUCTO", cwDescending)
      
      Call .FormAddRelation("FR58CODOFERTPROV", txtText1(15))
      .intAllowance = cwAllowAll
      'Call .objPrinter.Add("PR1281", "Listado ")
      
      '.blnHasMaint = True
      
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Productos")
      Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR23PRECFERTADO", "Precio Oferta", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR23OBSERVACIONES", "Ovservaciones", cwString)
      
      Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
      Call .FormAddFilterOrder(strKey, "FR23PRECFERTADO", "Precio Oferta")
      Call .FormAddFilterOrder(strKey, "FR23OBSERVACIONES", "Ovservaciones")
   End With
      
   With objWinInfo
      Call .FormAddInfo(objMasterInfo, cwFormDetail)
      Call .FormAddInfo(objDetailInfo, cwFormDetail)
      
      Call .FormCreateInfo(objMasterInfo)
      
      ' C�digo Detalle Oferta, FR23CODDETOFERTA
      .CtrlGetInfo(txtText1(16)).blnInGrid = False
      ' C�digo Oferta
      .CtrlGetInfo(txtText1(22)).blnInGrid = False
      ' C�digo Petici�n Oferta
      .CtrlGetInfo(txtText1(24)).blnInGrid = False
      
      ' C�digo de Estado
      .CtrlGetInfo(txtText1(23)).blnInGrid = False
      
      ' C�digo del Proveedor, FR79CODPROVEEDOR
      .CtrlGetInfo(txtText1(8)).blnInFind = True
      .CtrlGetInfo(txtText1(8)).blnForeign = True
'      ' Descripci�n del Proveedor
'      .CtrlGetInfo(txtText1(9)).blnInFind = True -> NO SE PONE PORQUE ES UN LINKED
      ' C�digo del Producto, FR73CODPRODUCTO
      .CtrlGetInfo(txtText1(0)).blnInFind = True
      .CtrlGetInfo(txtText1(0)).blnForeign = True
      
      ' Descripci�n de la oferta
      .CtrlGetInfo(txtText1(7)).blnInFind = True
      
      ' Fechas
      .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
      .CtrlGetInfo(dtcDateCombo1(2)).blnInFind = True
      .CtrlGetInfo(dtcDateCombo1(3)).blnInFind = True
                 
      ' C�digo del Producto
      .CtrlGetInfo(txtText1(0)).blnInFind = True
      ' Observaciones
      .CtrlGetInfo(txtText1(3)).blnInFind = True
      ' Precio Ofertado
      .CtrlGetInfo(txtText1(4)).blnInFind = True
                 
      'Call .FormChangeColor(objDetailInfo)
      
      ' Producto, Descripci�n del Producto
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(14), "FR73REFERENCIA")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(13), "FRH7CODFORMFAR")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(10), "FR73DOSIS")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "FR93CODUNIMEDIDA")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(12), "FR73VOLUMEN")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(2), "FR73DESPRODUCTOPROV")
                  
      ' C�digo de Proveedor, descripci�n de Proveedor
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")
      
      Call .WinRegister
      Call .WinStabilize
   End With
   
   If gintFR67CODPETOFERT <> "" Then
      
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
      
      sqlstr = "SELECT FR58CODOFERTPROV_SEQUENCE.nextval FROM dual"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      FR58CODOFERTPROVSEQUENCE = rsta(0).Value
      rsta.Close
      Set rsta = Nothing
      Call objWinInfo.CtrlSet(txtText1(15), FR58CODOFERTPROVSEQUENCE)
      
      ' Fecha Fin. Por defecto se pondr� hoy + un a�o
      sqlstr = "(SELECT TO_CHAR(SYSDATE + 365,'DD/MM/YYYY') FROM DUAL)"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      Call objWinInfo.CtrlSet(dtcDateCombo1(3), rsta.rdoColumns(0).Value)
      rsta.Close
      Set rsta = Nothing
         
      ' Fecha Recepci�n, por defecto hoy.
      sqlstr = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      Call objWinInfo.CtrlSet(dtcDateCombo1(1), rsta.rdoColumns(0).Value)
      rsta.Close
      Set rsta = Nothing
         
      txtText1(23).Text = 1 '�Enviada? 1 es recibida, enviada es 0, hay que poner 1 o 0?
      txtText1(8).Text = gintCodProveedor
      txtText1(24).Text = gintFR67CODPETOFERT

      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar

      sqlstr = "SELECT * FROM FR2900 WHERE FR67CODPETOFERT=" & gintFR67CODPETOFERT
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      While Not rsta.EOF
        ' Linea Detalle de Ofertas
        sqlstr = "SELECT MAX(FR23CODDETOFERTA) FROM FR2300 WHERE FR58CODOFERTPROV = " & FR58CODOFERTPROVSEQUENCE
        Set rstb = objApp.rdoConnect.OpenResultset(sqlstr)
        If IsNull(rstb.rdoColumns(0).Value) Then
           auxFR23CODDETOFERTA = 1
        Else
           auxFR23CODDETOFERTA = rstb.rdoColumns(0).Value + 1
        End If
        strInsertOferta = "INSERT INTO FR2300 " & _
                          "(FR58CODOFERTPROV,FR23CODDETOFERTA," & _
                          "FR73CODPRODUCTO) " & _
                          "Values  " & _
                          "(" & FR58CODOFERTPROVSEQUENCE & "," & auxFR23CODDETOFERTA & "," & _
                          rsta("FR73CODPRODUCTO").Value & ")"
        objApp.rdoConnect.Execute strInsertOferta, 64
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
         
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26)) 'Refrescar
   
   End If
   
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
   intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
   intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
   Dim objField As clsCWFieldSearch
   Dim strSentenciaWhere As String
   
   If strFormName = "Productos" And strCtrl = "txtText1(0)" Then
      Set objSearch = New clsCWSearch
      With objSearch
         .strTable = "FR7300"
         .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
         .strWhere = "WHERE FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
         
         Set objField = .AddField("FR73CODPRODUCTO")
         objField.strSmallDesc = "C�digo del Producto"
         
         Set objField = .AddField("FR73DESPRODUCTOPROV")
         objField.strSmallDesc = "Descripci�n del Producto "
         
         If .Search Then
            'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("ad02coddpto"))
            Call objWinInfo.CtrlSet(txtText1(0), .cllValues("FR73CODPRODUCTO"))
            Call objWinInfo.CtrlSet(txtText1(2), .cllValues("FR73DESPRODUCTOPROV"))
         End If
      End With
   End If
   
   If strFormName = "Oferta" And strCtrl = "txtText1(8)" Then
      Set objSearch = New clsCWSearch
      With objSearch
         .strTable = "FR7900"
         .strOrder = "ORDER BY FR79PROVEEDOR ASC"
            
         Set objField = .AddField("FR79CODPROVEEDOR")
         objField.strSmallDesc = "C�digo del Proveedor"
            
         Set objField = .AddField("FR79PROVEEDOR")
         objField.strSmallDesc = "Nombre del Proveedor"
            
         If .Search Then
            Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR79CODPROVEEDOR"))
         End If
      End With
      Set objSearch = Nothing
   End If
  
End Sub

Private Sub Command1_Click(Index As Integer)
   Call objWinInfo.FormChangeActive(tabTab1(1), False, True)
   
   ' Antes de traer un producto, comprobar exista una oferta
   If txtText1(15) = "" Then
      MsgBox "Debe tener una oferta seleccionada", vbInformation, "Aviso"
      Exit Sub
   End If
   
   Command1(Index).Enabled = False
   
   If txtText1(0).Text <> "" Then
      ' Hacer nuevo
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   ElseIf Not (objWinInfo.intWinStatus = cwModeSingleAddKey Or objWinInfo.intWinStatus = cwModeSingleAddRest) Then
      ' Hacer nuevo
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   End If
   
   Select Case Index
   Case 1
'      Call FrmBusMed.Show(vbModal)
      gintFR501Prov = Me.txtText1(8)
      gstrLlamador = "FrmGenPetOfe" 'se hace lo mismo, buscar por proveedor
      Call objsecurity.LaunchProcess("FR0502")
   Case 2
'      Call FrmBusMat.Show(vbModal)
      gintFR501Prov = Me.txtText1(8)
      gstrLlamador = "FrmGenPetOfe" 'se hace lo mismo, buscar por proveedor
      Call objsecurity.LaunchProcess("FR0503")
   End Select
   
   'Debug.Print objWinInfo.intWinStatus
   
   If gintProdBuscado <> "" Then
      txtText1(0).Text = gintProdBuscado
      txtText1(0).SetFocus
      SendKeys ("{TAB}")
   End If
   Command1(Index).Enabled = True
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
   If strFormName = "Oferta" Then
      Command1(1).Enabled = False
      Command1(2).Enabled = False
   Else
      Command1(1).Enabled = True
      Command1(2).Enabled = True
   End If
End Sub

Private Sub tabTab1_GotFocus(Index As Integer)
   
   If Index = 0 Then
      On Error Resume Next
      ' C�digo Oferta
      grdDBGrid1(0).Columns(1).Visible = False
      ' Descripci�n Oferta
      grdDBGrid1(0).Columns(2).Width = 120 * 20
      ' Fecha Recepci�n
      grdDBGrid1(0).Columns(3).Width = 120 * 15
      ' Fecha Inicio
      grdDBGrid1(0).Columns(4).Width = 120 * 10
      ' Fecha Fin
      grdDBGrid1(0).Columns(5).Width = 120 * 10
      ' C�digo Proveedor
      grdDBGrid1(0).Columns(9).Width = 120 * 16
      ' Nombre Proveedor
      grdDBGrid1(0).Columns(10).Width = 120 * 20

'      grdDBGrid1(0).Columns(21).Position = 5
      On Error GoTo 0
   End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   Dim sqlstr As String
   Dim rsta As rdoResultset

   Select Case btnButton.Index
   Case 2
   ' NUEVO
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      If objWinInfo.objWinActiveForm.strName = "Oferta" Then
         ' Secuencia de Ofertas
         sqlstr = "SELECT FR58CODOFERTPROV_SEQUENCE.nextval FROM dual"
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         Call objWinInfo.CtrlSet(txtText1(15), rsta.rdoColumns(0).Value)
         rsta.Close
         Set rsta = Nothing
         
         ' Fecha Fin. Por defecto se pondr� hoy + un a�o
         sqlstr = "(SELECT TO_CHAR(SYSDATE + 365,'DD/MM/YYYY') FROM DUAL)"
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         Call objWinInfo.CtrlSet(dtcDateCombo1(3), rsta.rdoColumns(0).Value)
         rsta.Close
         Set rsta = Nothing
         
         ' Fecha Recepci�n, por defecto hoy.
         sqlstr = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         Call objWinInfo.CtrlSet(dtcDateCombo1(1), rsta.rdoColumns(0).Value)
         rsta.Close
         Set rsta = Nothing
         
         txtText1(23).Text = 1 '�Enviada? 1 es recibida, enviada es 0, hay que poner 1 o 0?
         
      ElseIf objWinInfo.objWinActiveForm.strName = "Productos" Then
         ' Secuencia de Detalle de Ofertas
         sqlstr = "SELECT MAX(FR23CODDETOFERTA) FROM FR2300 WHERE FR58CODOFERTPROV = " & txtText1(15)
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         If IsNull(rsta.rdoColumns(0).Value) Then
            Call objWinInfo.CtrlSet(txtText1(16), 1)
         Else
            Call objWinInfo.CtrlSet(txtText1(16), rsta.rdoColumns(0).Value + 1)
         End If
         rsta.Close
         Set rsta = Nothing
      End If
   Case 3
   ' Abrir
   Case Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
   End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
   Select Case intIndex
   Case 10
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   Case 20
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
   Case 40
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
   Case 60
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
   Case 80
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
   Case 100
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
   End Select

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
   Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  
   Call objWinInfo.CtrlDataChange
   
'   If intIndex = 15 Then
'      txtText1(22).Text = txtText1(15).Text
'   End If

   
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'   Dim intReport As Integer
'   Dim objPrinter As clsCWPrinter
'   Dim blnHasFilter As Boolean
'
'   Call objWinInfo.FormPrinterDialog(True, "")
'   Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'   intReport = objPrinter.Selected
'   If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'   End If
'   Set objPrinter = Nothing
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
'
End Sub
