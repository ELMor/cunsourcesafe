VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmImprimirEtiquetas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Imprimir Etiquetas"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   375
      Left            =   2040
      TabIndex        =   5
      Top             =   7080
      Visible         =   0   'False
      Width           =   735
      _Version        =   196608
      _ExtentX        =   1296
      _ExtentY        =   661
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
   End
   Begin VB.Frame Frame1 
      Caption         =   "Productos Seleccionados "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5895
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Height          =   5295
         Left            =   120
         TabIndex        =   4
         Top             =   480
         Width           =   11460
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   7
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   2117
         Columns(0).Caption=   "C�d.Prod"
         Columns(0).Name =   "C�d.Prod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(1).Width=   1323
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "C�digo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2434
         Columns(2).Caption=   "Referencia"
         Columns(2).Name =   "Referencia"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   5927
         Columns(3).Caption=   "Descripci�n"
         Columns(3).Name =   "Descripci�n"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1482
         Columns(4).Caption=   "N�Copias"
         Columns(4).Name =   "N�Copias"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).HasForeColor=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   12615935
         Columns(5).Width=   4498
         Columns(5).Caption=   "Descripci�n Corta"
         Columns(5).Name =   "Descripci�n Corta"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).HasForeColor=   -1  'True
         Columns(6).Width=   3200
         Columns(6).Caption=   "DigitoSeguridad"
         Columns(6).Name =   "DigitoSeguridad"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   20214
         _ExtentY        =   9340
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir Etiquetas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   2
      Top             =   6840
      Width           =   2175
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmImprimirEtiquetas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmImprimirEtiquetasProductos(FR0082.frm)                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: AGOSTO DEL 2000                                               *
'* DESCRIPCION: Imprimir Etiquetas                                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub SSDBGrid1_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = 0
End Sub

Private Sub SSDBGrid1_KeyPress(KeyAscii As Integer)
If SSDBGrid1.Col = 4 Then
 Select Case KeyAscii
    Case 8, Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case Else
      KeyAscii = 0
 End Select
 If Len(SSDBGrid1.Columns(4).Value) = 4 And KeyAscii <> 8 Then   'KeyAscii=8 -> Borrar
    Call MsgBox("No puede escribir m�s de 4 d�gitos", vbInformation, "Aviso")
    KeyAscii = 0
  End If
End If
If SSDBGrid1.Col = 5 Then
  If Len(SSDBGrid1.Columns(5).Value) = 20 And KeyAscii <> 8 Then
    Call MsgBox("No puede escribir m�s de 20 caracteres", vbInformation, "Aviso")
    KeyAscii = 0
  End If
End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdImprimir_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String
Dim rsta As rdoResultset
Dim stra As String
Dim strupdate As String
Dim Num$

cmdImprimir.Enabled = False

mintNTotalSelRows = SSDBGrid1.Rows
gintprodtotal = mintNTotalSelRows
SSDBGrid1.MoveFirst
For mintisel = 0 To mintNTotalSelRows - 1
    mvarBkmrk = SSDBGrid1.SelBookmarks(mintisel)
    'Guardar Descripci�n Corta
    If Len(SSDBGrid1.Columns("Descripci�n Corta").CellValue(mvarBkmrk)) > 20 Then
      Call MsgBox("La descripci�n corta es demasiado larga. M�ximo 20 caracteres.", vbInformation, "Aviso")
      cmdImprimir.Enabled = True
      Exit Sub
    End If
    If Trim(SSDBGrid1.Columns("N�Copias").CellValue(mvarBkmrk)) = "" Then
      Call MsgBox("Debe especificar el n�mero de copias para cada etiqueta.", vbInformation, "Aviso")
      cmdImprimir.Enabled = True
      Exit Sub
    End If
    If Trim(SSDBGrid1.Columns("Descripci�n Corta").CellValue(mvarBkmrk)) = "" Then
      Call MsgBox("Debe especificar la Descripci�n Corta para cada etiqueta.", vbInformation, "Aviso")
      cmdImprimir.Enabled = True
      Exit Sub
    End If
SSDBGrid1.MoveNext
Next mintisel

SSDBGrid1.MoveFirst
mintNTotalSelRows = SSDBGrid1.Rows
gintprodtotal = mintNTotalSelRows
For mintisel = 0 To mintNTotalSelRows - 1
    mvarBkmrk = SSDBGrid1.SelBookmarks(mintisel)
    strupdate = "UPDATE FR7300 SET FR73DESPRODUCTOETIQ=" & "'" & SSDBGrid1.Columns("Descripci�n Corta").CellValue(mvarBkmrk) & "'"
    strupdate = strupdate & " WHERE FR73CODPRODUCTO=" & SSDBGrid1.Columns(0).CellValue(mvarBkmrk)
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "commit", 64
    'Imprimir Etiquetas
    Num = "001" & SSDBGrid1.Columns("C�digo").CellValue(mvarBkmrk) & SSDBGrid1.Columns("DigitoSeguridad").CellValue(mvarBkmrk)
    Call ImprimirEtiqueta1(Num, SSDBGrid1.Columns("N�Copias").CellValue(mvarBkmrk), SSDBGrid1.Columns("Descripci�n Corta").CellValue(mvarBkmrk))
    Call ImprimirEtiquetaDeSeparacion
SSDBGrid1.MoveNext
Next mintisel

cmdImprimir.Enabled = True

End Sub
Private Sub Guardar_Descripcion_Corta()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim strupdate As String

mintNTotalSelRows = SSDBGrid1.Rows
gintprodtotal = mintNTotalSelRows
SSDBGrid1.MoveFirst
For mintisel = 0 To mintNTotalSelRows - 1
  strupdate = "UPDATE FR7300 SET FR73DESPRODUCTO=" & "'" & SSDBGrid1.Columns("Descripci�n Corta").CellValue(mvarBkmrk) & "'"
  strupdate = strupdate & " WHERE FR73CODPRODUCTO=" & SSDBGrid1.Columns(0).CellValue(mvarBkmrk)
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "commit", 64
SSDBGrid1.MoveNext
Next mintisel
End Sub


Private Sub Form_Activate()
Call Inicializar_Toolbar
Call Inicializar_Grid
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

SSDBGrid1.Columns(0).Visible = False
SSDBGrid1.Columns("DigitoSeguridad").Visible = False

End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Select Case btnButton.Index
  Case 30:
          Unload Me
End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If intIndex = 100 Then
  Unload Me
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub



Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(21).Enabled = False
   tlbToolbar1.Buttons.Item(22).Enabled = False
   tlbToolbar1.Buttons.Item(23).Enabled = False
   tlbToolbar1.Buttons.Item(24).Enabled = False
   tlbToolbar1.Buttons.Item(26).Enabled = False
   
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuRegistroOpcion(40).Enabled = False
   mnuRegistroOpcion(50).Enabled = False
   mnuRegistroOpcion(60).Enabled = False
   mnuRegistroOpcion(70).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub
Private Sub Inicializar_Grid()
Dim i As Integer
Dim rsta As rdoResultset
Dim stra As String
Dim strdescripcioncorta As String
Dim referencia As String

For i = 0 To 3
  SSDBGrid1.Columns(i).Locked = True
Next i

SSDBGrid1.Columns("Descripci�n").Width = 4800
SSDBGrid1.Columns("Descripci�n Corta").Width = 3200

stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO IN " & gListaEtiquetas
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
    If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
      referencia = rsta.rdoColumns("FR73REFERENCIA").Value
    Else
      referencia = ""
    End If
    If Not IsNull(rsta.rdoColumns("FR73DESPRODUCTOETIQ").Value) Then
      strdescripcioncorta = rsta.rdoColumns("FR73DESPRODUCTOETIQ").Value
    Else
      strdescripcioncorta = ""
    End If

    SSDBGrid1.AddItem rsta.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                    rsta.rdoColumns("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
                    referencia & Chr(vbKeyTab) & _
                    rsta.rdoColumns("FR73DESPRODUCTO").Value & Chr(vbKeyTab) & _
                    "" & Chr(vbKeyTab) & _
                    strdescripcioncorta & Chr(vbKeyTab) & _
                    rsta.rdoColumns("FR73CODINTFARSEG").Value
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
End Sub

Private Sub pSelectImpresoraEtiquetas()
    Dim strImpresora As String
    Dim strImpresoraEtiq As String
    Dim i As Integer
    If vsPrinter1.Device <> "Bandit" Then
        ' Se busca la impresora llamada 'Bandit'
        For i = 0 To vsPrinter1.NDevices - 1
            strImpresora = vsPrinter1.Devices(i)
            If InStr(UCase(strImpresora), "BANDIT") > 0 Then
                strImpresoraEtiq = strImpresora
                Exit For
            End If
        Next i
        vsPrinter1.Device = strImpresoraEtiq
        If strImpresoraEtiq = "" Then
            vsPrinter1.Preview = True
        Else
             vsPrinter1.Preview = False
        End If
    End If
End Sub

Sub ImprimirEtiqueta1(Num As String, nCopias As Integer, Descrip As String)
Dim crlf$, Impr$, X$
Dim texto As String
  On Error Resume Next
  crlf = Chr$(13) & Chr$(10)
'  vsPrinter1.StartDoc
    texto = crlf
    texto = "{" & Chr$(10) & Chr$(13)
    texto = texto & "[Mn " & Chr$(34) & "45x19" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Ml " & Chr$(34) & "21.00:0:0:1:direct table 125" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Mw " & Chr$(34) & "45.00:0.00:0.00:1" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Mm " & Chr$(34) & "0:0.00:0" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Ma " & Chr$(34) & "45x19" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Fs " & Chr$(34) & "*:2" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[DW " & Chr$(34) & "49" & Chr$(34) & " " & Chr$(34) & "2" & Chr$(34) & " " & Chr$(34) & "3" & Chr$(34) & " " & Chr$(34) & "" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "A" & Chr$(34) & " " & Chr$(34) & "19.50:5.00:1" & Chr$(34) & " " & Chr$(34) & "0" & Chr$(34) & " " & Chr$(34) & "20:+:0:0" & Chr$(34) & " " & Chr$(34) & "100:1" & Chr$(34) & " " & Chr$(34) & "11.50" & Chr$(34) & " " & Chr$(34) & "15" & Chr$(34) & " " & Chr$(34) & "" & Chr$(34) & " " & Chr$(34) & "? Barras" & Chr$(34) & " " & Chr$(34) & "1:1:2:2:3:3" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "C" & Chr$(34) & " " & Chr$(34) & "2.50:0.50:1" & Chr$(34) & " " & Chr$(34) & "0" & Chr$(34) & " " & Chr$(34) & "15531807:10" & Chr$(34) & " " & Chr$(34) & "22" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "\N" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Dw " & Chr$(34) & "Cultivo" & Chr$(34) & " " & Chr$(34) & "49" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & Num & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & Descrip & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "\N" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Pb " & Chr$(34) & nCopias & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "}" & Chr$(10) & Chr$(13)
    
    '------------ seleccionar Impresora de etiquetas
Call pSelectImpresoraEtiquetas
vsPrinter1.StartDoc
vsPrinter1.Text = texto
vsPrinter1.EndDoc
vsPrinter1.PrintDoc

End Sub

Sub ImprimirEtiquetaDeSeparacion()
Dim crlf$, Impr$, X$
Dim texto As String
Dim nCopias As Integer
Dim Descrip As String
nCopias = 1
Descrip = ""
  On Error Resume Next
  crlf = Chr$(13) & Chr$(10)
'  vsPrinter1.StartDoc
    texto = crlf
    texto = "{" & Chr$(10) & Chr$(13)
    texto = texto & "[Mn " & Chr$(34) & "45x19" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Ml " & Chr$(34) & "21.00:0:0:1:direct table 125" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Mw " & Chr$(34) & "45.00:0.00:0.00:1" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Mm " & Chr$(34) & "0:0.00:0" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Ma " & Chr$(34) & "45x19" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Fs " & Chr$(34) & "*:2" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[DW " & Chr$(34) & "49" & Chr$(34) & " " & Chr$(34) & "1" & Chr$(34) & " " & Chr$(34) & "3" & Chr$(34) & " " & Chr$(34) & "" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
'    texto = texto & " " & Chr$(34) & "A" & Chr$(34) & " " & Chr$(34) & "19.50:5.00:1" & Chr$(34) & " " & Chr$(34) & "0" & Chr$(34) & " " & Chr$(34) & "20:+:0:0" & Chr$(34) & " " & Chr$(34) & "100:1" & Chr$(34) & " " & Chr$(34) & "11.50" & Chr$(34) & " " & Chr$(34) & "15" & Chr$(34) & " " & Chr$(34) & "" & Chr$(34) & " " & Chr$(34) & "? Barras" & Chr$(34) & " " & Chr$(34) & "1:1:2:2:3:3" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "C" & Chr$(34) & " " & Chr$(34) & "2.50:0.50:1" & Chr$(34) & " " & Chr$(34) & "0" & Chr$(34) & " " & Chr$(34) & "15531807:10" & Chr$(34) & " " & Chr$(34) & "22" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "\N" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Dw " & Chr$(34) & "Cultivo" & Chr$(34) & " " & Chr$(34) & "49" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
'    texto = texto & " " & Chr$(34) & Num & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & Descrip & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "\N" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Pb " & Chr$(34) & nCopias & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "}" & Chr$(10) & Chr$(13)
    
    '------------ seleccionar Impresora de etiquetas
Call pSelectImpresoraEtiquetas
vsPrinter1.StartDoc
vsPrinter1.Text = texto
vsPrinter1.EndDoc
vsPrinter1.PrintDoc

End Sub



















