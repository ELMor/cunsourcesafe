VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************


'CIM
Const FRWinControlarAlerta                  As String = "FR8200"
Const FRWinRegistrarConsultas               As String = "FR8202"
Const FRWinBuscarRespuesta                  As String = "FR8203"
Const FRWinConsultasNoRespondidas           As String = "FR8204"
Const FRWinConsultaCompleta                 As String = "FR8207"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
      
    'CIM
   Case FRWinControlarAlerta
     Load frmControlarAlerta
     'Call objsecurity.AddHelpContext(528)
     Call frmControlarAlerta.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmControlarAlerta
     Set frmControlarAlerta = Nothing
   Case FRWinRegistrarConsultas
     Load frmRegistrarConsultas
     'Call objsecurity.AddHelpContext(528)
     Call frmRegistrarConsultas.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmRegistrarConsultas
     Set frmRegistrarConsultas = Nothing
   Case FRWinBuscarRespuesta
     Load frmBuscarRespuesta
     'Call objsecurity.AddHelpContext(528)
     Call frmBuscarRespuesta.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmBuscarRespuesta
     Set frmBuscarRespuesta = Nothing
  Case FRWinConsultasNoRespondidas
     Load frmConsultasNoRespondidas
     'Call objsecurity.AddHelpContext(528)
     Call frmConsultasNoRespondidas.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmConsultasNoRespondidas
     Set frmConsultasNoRespondidas = Nothing
  Case FRWinConsultaCompleta
     Load frmConsultaCompleta
     'Call objsecurity.AddHelpContext(528)
     Call frmConsultaCompleta.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmConsultaCompleta
     Set frmConsultaCompleta = Nothing

  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 5, 1 To 4) As Variant
      
  ' VENTANAS
  'MANTENIMIENTO
  
  aProcess(1, 1) = FRWinControlarAlerta
  aProcess(1, 2) = "Controlar Alertas de Inmovilizaci�n"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = FRWinRegistrarConsultas
  aProcess(2, 2) = "Registrar Consultas al CIM"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = FRWinBuscarRespuesta
  aProcess(3, 2) = "Buscar Respuesta a una consulta hecha al CIM"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = FRWinConsultasNoRespondidas
  aProcess(4, 2) = "Consultas al CIM no respondidas"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = FRWinConsultaCompleta
  aProcess(5, 2) = "Consulta Completa No Respondida"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
  
  
  
End Sub
