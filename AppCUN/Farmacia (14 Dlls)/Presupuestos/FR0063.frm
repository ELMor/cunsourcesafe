VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmEstPresuAnu 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Establecer Presupuestos Anuales."
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Ejercicios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7200
      Index           =   1
      Left            =   120
      TabIndex        =   14
      Top             =   600
      Width           =   4695
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6555
         Index           =   0
         Left            =   360
         TabIndex        =   15
         Top             =   480
         Width           =   4080
         _ExtentX        =   7197
         _ExtentY        =   11562
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
      End
   End
   Begin VB.CommandButton cmdGenerar 
      Caption         =   "Generar Presupuestos"
      Height          =   375
      Left            =   5520
      TabIndex        =   7
      Top             =   7440
      Width           =   1815
   End
   Begin VB.TextBox txtTP 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   7680
      TabIndex        =   8
      Tag             =   "Porcentaje"
      ToolTipText     =   "Porcentaje"
      Top             =   7440
      Width           =   492
   End
   Begin VB.OptionButton optGenerar 
      Caption         =   "Incremento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Index           =   0
      Left            =   8880
      TabIndex        =   9
      ToolTipText     =   "Incremento"
      Top             =   7560
      Width           =   1455
   End
   Begin VB.OptionButton optGenerar 
      Caption         =   "Disminuci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Index           =   1
      Left            =   10320
      TabIndex        =   10
      ToolTipText     =   "Disminuci�n"
      Top             =   7560
      Width           =   1575
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Partida Presupuestaria Anual"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6435
      Index           =   0
      Left            =   4920
      TabIndex        =   13
      Top             =   600
      Width           =   6780
      Begin VB.CommandButton Command1 
         Caption         =   "..."
         Height          =   250
         Left            =   1150
         TabIndex        =   2
         Top             =   2200
         Width           =   275
      End
      Begin VB.CommandButton cmdPtas 
         Caption         =   "Calcular Ptas"
         Height          =   375
         Left            =   3480
         TabIndex        =   6
         Top             =   4920
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.CommandButton cmdEuros 
         Caption         =   "Calcular Euros"
         Height          =   375
         Left            =   3480
         TabIndex        =   5
         Top             =   3960
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         Height          =   330
         Index           =   5
         Left            =   480
         MaxLength       =   10
         TabIndex        =   3
         Tag             =   "Importe asigando"
         ToolTipText     =   "Cantidad Ptas"
         Top             =   2880
         Width           =   1212
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   2
         Left            =   2040
         Locked          =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   17
         TabStop         =   0   'False
         Tag             =   "Descripci�n de Grupo"
         Top             =   2160
         Width           =   4365
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         Height          =   330
         Index           =   6
         Left            =   480
         MaxLength       =   10
         TabIndex        =   4
         Tag             =   "Cantidad Euros"
         ToolTipText     =   "Cantidad Euros"
         Top             =   4920
         Visible         =   0   'False
         Width           =   1212
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         Height          =   330
         Index           =   0
         Left            =   480
         Locked          =   -1  'True
         MaxLength       =   4
         TabIndex        =   0
         Tag             =   "Ejercicio"
         ToolTipText     =   "Ejercicio"
         Top             =   1215
         Width           =   492
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         Height          =   330
         Index           =   1
         Left            =   480
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   1
         Tag             =   "Grupo"
         ToolTipText     =   "Grupo"
         Top             =   2160
         Width           =   612
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   2040
         TabIndex        =   22
         Top             =   1920
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Cantidad Euros"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   480
         TabIndex        =   21
         Top             =   4680
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Importe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   480
         TabIndex        =   20
         Top             =   2640
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Ejercicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   19
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   480
         TabIndex        =   18
         Top             =   1920
         Width           =   1455
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   11
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.ImageList imlImagenes 
      Left            =   0
      Top             =   7560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":005E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":00BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":011A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":0178
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":01D6
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":0234
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":0292
            Key             =   "function"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":02F0
            Key             =   "window"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":034E
            Key             =   "report"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":03AC
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":040A
            Key             =   "process"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":0468
            Key             =   "application"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":04C6
            Key             =   "big"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":0524
            Key             =   "small"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":0582
            Key             =   "list"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":05E0
            Key             =   "details"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":063E
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0063.frx":069C
            Key             =   "refresh"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "%"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   16
      Left            =   8280
      TabIndex        =   16
      Top             =   7500
      Width           =   255
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmEstPresuAnu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmEstPresuAnu (FR0063.FRM)                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Presupuestos por Anuales                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
Dim gblnabrir As Boolean
Dim arbolvacio As Boolean
Dim mintEstado As Integer
Dim marrayMeses(12) As String
Private Sub CalEuros()
    Dim strEuros As String
    Dim qryEuros As rdoQuery
    Dim rstEuros As rdoResultset
    Dim vntEuros As Variant
    Dim curEUR As Currency
    Dim curPts As Currency
    
    
    If Not IsNumeric(txtText1(5).Text) Then
      'MsgBox "El valor ha de ser num�rico", vbInformation
      'txtText1(5).SetFocus
      Exit Sub
    End If
    
    'cmdEuros.Enabled = False
    
    Select Case SeparadorDec
        Case ".":
            'txtText1(5) = objGen.ReplaceStr(txtText1(5).Text, ",", ".", 1)
            txtText1(6) = objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1)
        Case ",":
            'txtText1(5) = objGen.ReplaceStr(txtText1(5).Text, ".", ",", 1)
            txtText1(6) = objGen.ReplaceStr(txtText1(6).Text, ".", ",", 1)
    End Select
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
    'strEuros = "select FRH2PARAMGEN from frh200 where FRH2CODPARAMGEN=1"
    strEuros = "select FRH2PARAMGEN from frh200 where FRH2CODPARAMGEN=?"
    Set qryEuros = objApp.rdoConnect.CreateQuery("", strEuros)
    qryEuros(0) = 1
    Set rstEuros = qryEuros.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    'Set rstEuros = objApp.rdoConnect.OpenResultset(strEuros)
'    vntEuros = Format(CDec(Val(txtText1(5).Text) / Val(rstEuros(0).Value)), "##########0.00")
    On Error Resume Next
    vntEuros = Format(CDec(txtText1(5).Text / rstEuros(0).Value), "##########0.00")
    On Error GoTo 0
    txtText1(6) = Fix(vntEuros) '/ rstEuros(0).Value
    'tlbToolbar1.Buttons(4).Enabled = True
    'objWinInfo.objWinActiveForm.rdoCursor.Edit
    'objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FRH1CANTIDADEURO").Value = txtText1(6).Text   'objWinInfo.objWinActiveForm.blnChanged = True
    'objWinInfo.objWinActiveForm.rdoCursor.Update
    'MiCadena = Format(334.9, "###0.00") ' Devuelve "334,90".
    
    'hora = objGen.ReplaceStr(hora, ",", ".", 1)
    rstEuros.Close
    Set rstEuros = Nothing
    
    'cmdEuros.Enabled = True
    
End Sub
Private Sub rellenar_TreeView()
    Dim a�o As Variant
    Dim strejercicio As String
    Dim rstejercicio As rdoResultset
    'Dim strMes As String
    'Dim rstMes As rdoResultset
    'Dim mes As Variant
    Dim strgrupo As String
    Dim qrygrupo As rdoQuery
    Dim rstgrupo As rdoResultset
    Dim strdesGrupo As String
    Dim qrydesGrupo As rdoQuery
    Dim rstdesGrupo As rdoResultset
    Dim grupo As Variant
    Dim desGrupo As Variant
    Dim strServicio As String
    Dim rstServicio As rdoResultset
    Dim desServicio As Variant
    Dim strdesServicio As String
    Dim rstdesServicio As rdoResultset
    Dim Servicio As Variant
    
    
    tvwItems(0).Nodes.Clear
    
    strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
    Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
    
    If rstejercicio.EOF = True Then
      arbolvacio = True
    End If

    While rstejercicio.EOF = False
      a�o = rstejercicio("frg9ejercicio").Value
      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
      arbolvacio = False
      'strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
      'Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
      'While rstMes.EOF = False
        'mes = rstMes("FRH1MES").Value
        'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
        'strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
        '             " AND FR41CODGRUPPROD IN (SELECT FR41CODGRUPPROD FROM FR4100 WHERE FRI7CODTIPGRP=1) ORDER BY FR41CODGRUPPROD"
        strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=?" & _
                     " AND FR41CODGRUPPROD IN (SELECT FR41CODGRUPPROD FROM FR4100 WHERE FRI7CODTIPGRP=?) ORDER BY FR41CODGRUPPROD"
        Set qrygrupo = objApp.rdoConnect.CreateQuery("", strgrupo)
        qrygrupo(0) = a�o
        qrygrupo(1) = 1
        Set rstgrupo = qrygrupo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        'Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
        While rstgrupo.EOF = False
            grupo = rstgrupo("FR41CODGRUPPROD").Value
            'strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                          " AND FR41CODGRUPPROD=" & grupo & "ORDER BY AD02CODDPTO"
            'Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
            'strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD='" & rstgrupo("FR41CODGRUPPROD").Value & "'"
            strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=?"
            Set qrydesGrupo = objApp.rdoConnect.CreateQuery("", strdesGrupo)
            qrydesGrupo(0) = rstgrupo("FR41CODGRUPPROD").Value
            Set rstdesGrupo = qrydesGrupo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            'Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
            desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
            Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "grupo" & a�o & "/" & grupo, grupo & ".-" & desGrupo)
            'While rstServicio.EOF = False
            '    Servicio = rstServicio("AD02CODDPTO").Value
            '    strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
            '    Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
            '    desServicio = rstdesServicio("AD02DESDPTO").Value
            '    Call tvwItems(0).Nodes.Add("grupo" & a�o & "/" & grupo, tvwChild, "servicio" & a�o & "/" & grupo & "/" & Servicio, Servicio & ".-" & desServicio)
            '    rstdesServicio.Close
            '    Set rstdesServicio = Nothing
            '    rstServicio.MoveNext
            'Wend
            'rstServicio.Close
            'Set rstServicio = Nothing
            rstdesGrupo.Close
            Set rstdesGrupo = Nothing
            rstgrupo.MoveNext
        Wend
        rstgrupo.Close
        Set rstgrupo = Nothing
        'rstMes.MoveNext
      'Wend
      'rstMes.Close
      'Set rstMes = Nothing
      rstejercicio.MoveNext
    Wend
    rstejercicio.Close
    Set rstejercicio = Nothing

  If arbolvacio = False Then
    tvwItems(0).Nodes("ejerc" & a�o).Selected = True
    tvwItems(0).SelectedItem.FirstSibling.Selected = True
    Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    'tlbToolbar1.Buttons(2).Enabled = False
    tlbToolbar1.Buttons(3).Enabled = False
    tlbToolbar1.Buttons(4).Enabled = True
    tlbToolbar1.Buttons(6).Enabled = False
    tlbToolbar1.Buttons(8).Enabled = False
    tlbToolbar1.Buttons(14).Enabled = False
    tlbToolbar1.Buttons(10).Enabled = False
    tlbToolbar1.Buttons(11).Enabled = False
    tlbToolbar1.Buttons(12).Enabled = False
    tlbToolbar1.Buttons(18).Enabled = False
    tlbToolbar1.Buttons(19).Enabled = False
    tlbToolbar1.Buttons(16).Enabled = False
    tlbToolbar1.Buttons(21).Enabled = True
    tlbToolbar1.Buttons(22).Enabled = True
    tlbToolbar1.Buttons(23).Enabled = True
    tlbToolbar1.Buttons(24).Enabled = True
    tlbToolbar1.Buttons(26).Enabled = False
    tlbToolbar1.Buttons(28).Enabled = False
    'mnuDatosOpcion(10).Enabled = False
    mnuDatosOpcion(20).Enabled = False
    mnuDatosOpcion(40).Enabled = True
    mnuDatosOpcion(60).Enabled = False
    mnuDatosOpcion(80).Enabled = False
    mnuEdicionOpcion(10).Enabled = False
    mnuEdicionOpcion(30).Enabled = False
    mnuEdicionOpcion(40).Enabled = False
    mnuEdicionOpcion(50).Enabled = False
    mnuEdicionOpcion(60).Enabled = False
    mnuEdicionOpcion(62).Enabled = False
    mnuEdicionOpcion(80).Enabled = False
    mnuEdicionOpcion(90).Enabled = False
    mnuFiltroOpcion(10).Enabled = False
    mnuFiltroOpcion(20).Enabled = False
    mnuRegistroOpcion(10).Enabled = False
    mnuRegistroOpcion(20).Enabled = False
    mnuRegistroOpcion(40).Enabled = True
    mnuRegistroOpcion(50).Enabled = True
    mnuRegistroOpcion(60).Enabled = True
    mnuRegistroOpcion(70).Enabled = True
    mnuRegistroOpcion(72).Enabled = False
    mnuOpcionesOpcion(10).Enabled = False
    mnuOpcionesOpcion(20).Enabled = False
    mnuOpcionesOpcion(40).Enabled = False
    mnuOpcionesOpcion(50).Enabled = False
  Else
    'tlbToolbar1.Buttons(2).Enabled = False
    tlbToolbar1.Buttons(3).Enabled = False
    tlbToolbar1.Buttons(4).Enabled = False
    tlbToolbar1.Buttons(6).Enabled = False
    tlbToolbar1.Buttons(8).Enabled = False
    tlbToolbar1.Buttons(14).Enabled = False
    tlbToolbar1.Buttons(10).Enabled = False
    tlbToolbar1.Buttons(11).Enabled = False
    tlbToolbar1.Buttons(12).Enabled = False
    tlbToolbar1.Buttons(18).Enabled = False
    tlbToolbar1.Buttons(19).Enabled = False
    tlbToolbar1.Buttons(16).Enabled = False
    tlbToolbar1.Buttons(21).Enabled = False
    tlbToolbar1.Buttons(22).Enabled = False
    tlbToolbar1.Buttons(23).Enabled = False
    tlbToolbar1.Buttons(24).Enabled = False
    tlbToolbar1.Buttons(26).Enabled = False
    tlbToolbar1.Buttons(28).Enabled = False
    'mnuDatosOpcion(10).Enabled = False
    mnuDatosOpcion(20).Enabled = False
    mnuDatosOpcion(40).Enabled = False
    mnuDatosOpcion(60).Enabled = False
    mnuDatosOpcion(80).Enabled = False
    mnuEdicionOpcion(10).Enabled = False
    mnuEdicionOpcion(30).Enabled = False
    mnuEdicionOpcion(40).Enabled = False
    mnuEdicionOpcion(50).Enabled = False
    mnuEdicionOpcion(60).Enabled = False
    mnuEdicionOpcion(62).Enabled = False
    mnuEdicionOpcion(80).Enabled = False
    mnuEdicionOpcion(90).Enabled = False
    mnuFiltroOpcion(10).Enabled = False
    mnuFiltroOpcion(20).Enabled = False
    mnuRegistroOpcion(10).Enabled = False
    mnuRegistroOpcion(20).Enabled = False
    mnuRegistroOpcion(40).Enabled = False
    mnuRegistroOpcion(50).Enabled = False
    mnuRegistroOpcion(60).Enabled = False
    mnuRegistroOpcion(70).Enabled = False
    mnuRegistroOpcion(72).Enabled = False
    mnuOpcionesOpcion(10).Enabled = False
    mnuOpcionesOpcion(20).Enabled = False
    mnuOpcionesOpcion(40).Enabled = False
    mnuOpcionesOpcion(50).Enabled = False
  End If


End Sub





'Private Sub rellenar_TreeView_GrupServ()
'    Dim a�o As Variant
'    Dim strejercicio As String
'    Dim rstejercicio As rdoResultset
'    'Dim strMes As String
'    'Dim rstMes As rdoResultset
'    'Dim mes As Variant
'    Dim strgrupo As String
'    Dim rstgrupo As rdoResultset
'    Dim strdesGrupo As String
'    Dim rstdesGrupo As rdoResultset
'    Dim grupo As Variant
'    Dim desGrupo As Variant
'    Dim strServicio As String
'    Dim rstServicio As rdoResultset
'    Dim desServicio As Variant
'    Dim strdesServicio As String
'    Dim rstdesServicio As rdoResultset
'    Dim Servicio As Variant
    
    
 '   tvwItems(0).Nodes.Clear
 '
 '   strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
 '   Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
 '
 '   If rstejercicio.EOF = True Then
 '     arbolvacio = True
 '   End If
'
 '   While rstejercicio.EOF = False
 '     a�o = rstejercicio("frg9ejercicio").Value
 '     Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
 '     arbolvacio = False
 '     'strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
 '     'Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
 '     'While rstMes.EOF = False
 '       'mes = rstMes("FRH1MES").Value
 '       'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
 '       strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
 '                    " ORDER BY FR41CODGRUPPROD"
 '       Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
 '       While rstgrupo.EOF = False
 '           grupo = rstgrupo("FR41CODGRUPPROD").Value
 '           strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
 '                         " AND FR41CODGRUPPROD=" & grupo & "ORDER BY AD02CODDPTO"
 '           Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
 '           strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
 '           Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
 '           desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
 '           Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "grupo" & a�o & "/" & grupo, grupo & ".-" & desGrupo)
 '           While rstServicio.EOF = False
 '               Servicio = rstServicio("AD02CODDPTO").Value
 '               strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
 '               Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
 '               desServicio = rstdesServicio("AD02DESDPTO").Value
 '               Call tvwItems(0).Nodes.Add("grupo" & a�o & "/" & grupo, tvwChild, "servicio" & a�o & "/" & grupo & "/" & Servicio, Servicio & ".-" & desServicio)
 '               rstdesServicio.Close
 '               Set rstdesServicio = Nothing
 '               rstServicio.MoveNext
 '           Wend
 '           rstServicio.Close
 '           Set rstServicio = Nothing
 '           rstdesGrupo.Close
 '           Set rstdesGrupo = Nothing
 '           rstgrupo.MoveNext
 '       Wend
 '       rstgrupo.Close
 '       Set rstgrupo = Nothing
 '       'rstMes.MoveNext
 '     'Wend
 '     'rstMes.Close
 '     'Set rstMes = Nothing
 '     rstejercicio.MoveNext
 '   Wend
 '   rstejercicio.Close
 '   Set rstejercicio = Nothing

 ' If arbolvacio = False Then
 '   tvwItems(0).Nodes("ejerc" & a�o).Selected = True
 '   tvwItems(0).SelectedItem.FirstSibling.Selected = True
 '   Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
 '   tlbToolbar1.Buttons(2).Enabled = False
 '   tlbToolbar1.Buttons(3).Enabled = False
 '   tlbToolbar1.Buttons(4).Enabled = True
 '   tlbToolbar1.Buttons(6).Enabled = False
 '   tlbToolbar1.Buttons(8).Enabled = False
 '   tlbToolbar1.Buttons(14).Enabled = False
 '   tlbToolbar1.Buttons(10).Enabled = False
 '   tlbToolbar1.Buttons(11).Enabled = False
 '   tlbToolbar1.Buttons(12).Enabled = False
 '   tlbToolbar1.Buttons(18).Enabled = False
 '   tlbToolbar1.Buttons(19).Enabled = False
 '   tlbToolbar1.Buttons(16).Enabled = False
 '   tlbToolbar1.Buttons(21).Enabled = True
 '   tlbToolbar1.Buttons(22).Enabled = True
 '   tlbToolbar1.Buttons(23).Enabled = True
 '   tlbToolbar1.Buttons(24).Enabled = True
 '   tlbToolbar1.Buttons(26).Enabled = False
 '   tlbToolbar1.Buttons(28).Enabled = False
 '   mnuDatosOpcion(10).Enabled = False
 '   mnuDatosOpcion(20).Enabled = False
 '   mnuDatosOpcion(40).Enabled = True
 '   mnuDatosOpcion(60).Enabled = False
 '   mnuDatosOpcion(80).Enabled = False
 '   mnuEdicionOpcion(10).Enabled = False
 '   mnuEdicionOpcion(30).Enabled = False
 '   mnuEdicionOpcion(40).Enabled = False
 '   mnuEdicionOpcion(50).Enabled = False
 '   mnuEdicionOpcion(60).Enabled = False
 '   mnuEdicionOpcion(62).Enabled = False
 '   mnuEdicionOpcion(80).Enabled = False
 '   mnuEdicionOpcion(90).Enabled = False
 '   mnuFiltroOpcion(10).Enabled = False
 '   mnuFiltroOpcion(20).Enabled = False
 '   mnuRegistroOpcion(10).Enabled = False
 '   mnuRegistroOpcion(20).Enabled = False
 '   mnuRegistroOpcion(40).Enabled = True
 '   mnuRegistroOpcion(50).Enabled = True
 '   mnuRegistroOpcion(60).Enabled = True
 '   mnuRegistroOpcion(70).Enabled = True
 '   mnuRegistroOpcion(72).Enabled = False
 '   mnuOpcionesOpcion(10).Enabled = False
 '   mnuOpcionesOpcion(20).Enabled = False
 '   mnuOpcionesOpcion(40).Enabled = False
 '   mnuOpcionesOpcion(50).Enabled = False
 ' Else
 '   tlbToolbar1.Buttons(2).Enabled = False
 '   tlbToolbar1.Buttons(3).Enabled = False
 '   tlbToolbar1.Buttons(4).Enabled = False
 '   tlbToolbar1.Buttons(6).Enabled = False
 '   tlbToolbar1.Buttons(8).Enabled = False
 '   tlbToolbar1.Buttons(14).Enabled = False
 '   tlbToolbar1.Buttons(10).Enabled = False
 '   tlbToolbar1.Buttons(11).Enabled = False
 '   tlbToolbar1.Buttons(12).Enabled = False
 '   tlbToolbar1.Buttons(18).Enabled = False
 '   tlbToolbar1.Buttons(19).Enabled = False
 '   tlbToolbar1.Buttons(16).Enabled = False
 '   tlbToolbar1.Buttons(21).Enabled = False
 '   tlbToolbar1.Buttons(22).Enabled = False
 '   tlbToolbar1.Buttons(23).Enabled = False
 '   tlbToolbar1.Buttons(24).Enabled = False
 '   tlbToolbar1.Buttons(26).Enabled = False
 '   tlbToolbar1.Buttons(28).Enabled = False
 '   mnuDatosOpcion(10).Enabled = False
 '   mnuDatosOpcion(20).Enabled = False
 '   mnuDatosOpcion(40).Enabled = False
 '   mnuDatosOpcion(60).Enabled = False
 '   mnuDatosOpcion(80).Enabled = False
 '   mnuEdicionOpcion(10).Enabled = False
 '   mnuEdicionOpcion(30).Enabled = False
 '   mnuEdicionOpcion(40).Enabled = False
 '   mnuEdicionOpcion(50).Enabled = False
 '   mnuEdicionOpcion(60).Enabled = False
 '   mnuEdicionOpcion(62).Enabled = False
 '   mnuEdicionOpcion(80).Enabled = False
 '   mnuEdicionOpcion(90).Enabled = False
 '   mnuFiltroOpcion(10).Enabled = False
 '   mnuFiltroOpcion(20).Enabled = False
 '   mnuRegistroOpcion(10).Enabled = False
 '   mnuRegistroOpcion(20).Enabled = False
 '   mnuRegistroOpcion(40).Enabled = False
 '   mnuRegistroOpcion(50).Enabled = False
 '   mnuRegistroOpcion(60).Enabled = False
 '   mnuRegistroOpcion(70).Enabled = False
 '   mnuRegistroOpcion(72).Enabled = False
 '   mnuOpcionesOpcion(10).Enabled = False
 '   mnuOpcionesOpcion(20).Enabled = False
 '   mnuOpcionesOpcion(40).Enabled = False
 '   mnuOpcionesOpcion(50).Enabled = False
 ' End If


'End Sub

'Private Sub rellenar_TreeView_ServGrup()
'    Dim a�o As Variant
'    Dim strejercicio As String
'    Dim rstejercicio As rdoResultset
'    'Dim strMes As String
'    'Dim rstMes As rdoResultset
'    'Dim mes As Variant
'    Dim strgrupo As String
'    Dim rstgrupo As rdoResultset
'    Dim strdesGrupo As String
'    Dim rstdesGrupo As rdoResultset
'    Dim grupo As Variant
'    Dim desGrupo As Variant
'    Dim strServicio As String
'    Dim rstServicio As rdoResultset
'    Dim desServicio As Variant
'    Dim strdesServicio As String
'    Dim rstdesServicio As rdoResultset
'    Dim Servicio As Variant
'
'    tvwItems(0).Nodes.Clear
'
'    strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
'    Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
'
'    If rstejercicio.EOF = True Then
'      arbolvacio = True
'    End If
'
'    While rstejercicio.EOF = False
'      a�o = rstejercicio("frg9ejercicio").Value
'      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
'      arbolvacio = False
'      'strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
'      'Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
'      'While rstMes.EOF = False
'        'mes = rstMes("FRH1MES").Value
'        'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
'        strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
'                     " ORDER BY AD02CODDPTO"
'        Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
'        While rstServicio.EOF = False
'            Servicio = rstServicio("AD02CODDPTO").Value
'            strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
'                          " AND AD02CODDPTO=" & Servicio & "ORDER BY FR41CODGRUPPROD"
'            Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
'            strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
'            Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
'            desServicio = rstdesServicio("AD02DESDPTO").Value
'            Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "servicio" & a�o & "/" & Servicio, Servicio & ".-" & desServicio)
'            While rstgrupo.EOF = False
'                grupo = rstgrupo("FR41CODGRUPPROD").Value
'                strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
'                Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
'                desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
'                Call tvwItems(0).Nodes.Add("servicio" & a�o & "/" & Servicio, tvwChild, "grupo" & a�o & "/" & Servicio & "/" & grupo, grupo & ".-" & desGrupo)
'                rstdesGrupo.Close
'                Set rstdesGrupo = Nothing
'                rstgrupo.MoveNext
'            Wend
'            rstgrupo.Close
'            Set rstgrupo = Nothing
'            rstdesServicio.Close
'            Set rstdesServicio = Nothing
'            rstServicio.MoveNext
'        Wend
'        rstServicio.Close
'        Set rstServicio = Nothing
'        'rstMes.MoveNext
'      'Wend
'      'rstMes.Close
'      'Set rstMes = Nothing
'      rstejercicio.MoveNext
'    Wend
'    rstejercicio.Close
'    Set rstejercicio = Nothing
'
'  If arbolvacio = False Then
'    tvwItems(0).Nodes("ejerc" & a�o).Selected = True
'    tvwItems(0).SelectedItem.FirstSibling.Selected = True
'    Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
'    tlbToolbar1.Buttons(2).Enabled = False
'    tlbToolbar1.Buttons(3).Enabled = False
'    tlbToolbar1.Buttons(4).Enabled = True
'    tlbToolbar1.Buttons(6).Enabled = False
'    tlbToolbar1.Buttons(8).Enabled = False
'    tlbToolbar1.Buttons(14).Enabled = False
'    tlbToolbar1.Buttons(10).Enabled = False
'    tlbToolbar1.Buttons(11).Enabled = False
'    tlbToolbar1.Buttons(12).Enabled = False
'    tlbToolbar1.Buttons(18).Enabled = False
'    tlbToolbar1.Buttons(19).Enabled = False
'    tlbToolbar1.Buttons(16).Enabled = False
'    tlbToolbar1.Buttons(21).Enabled = True
'    tlbToolbar1.Buttons(22).Enabled = True
'    tlbToolbar1.Buttons(23).Enabled = True
'    tlbToolbar1.Buttons(24).Enabled = True
'    tlbToolbar1.Buttons(26).Enabled = False
'    tlbToolbar1.Buttons(28).Enabled = False
'    mnuDatosOpcion(10).Enabled = False
'    mnuDatosOpcion(20).Enabled = False
'    mnuDatosOpcion(40).Enabled = True
'    mnuDatosOpcion(60).Enabled = False
'    mnuDatosOpcion(80).Enabled = False
'    mnuEdicionOpcion(10).Enabled = False
'    mnuEdicionOpcion(30).Enabled = False
'    mnuEdicionOpcion(40).Enabled = False
'    mnuEdicionOpcion(50).Enabled = False
'    mnuEdicionOpcion(60).Enabled = False
'    mnuEdicionOpcion(62).Enabled = False
'    mnuEdicionOpcion(80).Enabled = False
'    mnuEdicionOpcion(90).Enabled = False
'    mnuFiltroOpcion(10).Enabled = False
'    mnuFiltroOpcion(20).Enabled = False
'    mnuRegistroOpcion(10).Enabled = False
'    mnuRegistroOpcion(20).Enabled = False
'    mnuRegistroOpcion(40).Enabled = True
'    mnuRegistroOpcion(50).Enabled = True
'    mnuRegistroOpcion(60).Enabled = True
'    mnuRegistroOpcion(70).Enabled = True
'    mnuRegistroOpcion(72).Enabled = False
'    mnuOpcionesOpcion(10).Enabled = False
'    mnuOpcionesOpcion(20).Enabled = False
'    mnuOpcionesOpcion(40).Enabled = False
'    mnuOpcionesOpcion(50).Enabled = False
'  Else
'    tlbToolbar1.Buttons(2).Enabled = False
'    tlbToolbar1.Buttons(3).Enabled = False
'    tlbToolbar1.Buttons(4).Enabled = False
'    tlbToolbar1.Buttons(6).Enabled = False
'    tlbToolbar1.Buttons(8).Enabled = False
'    tlbToolbar1.Buttons(14).Enabled = False
'    tlbToolbar1.Buttons(10).Enabled = False
'    tlbToolbar1.Buttons(11).Enabled = False
'    tlbToolbar1.Buttons(12).Enabled = False
'    tlbToolbar1.Buttons(18).Enabled = False
'    tlbToolbar1.Buttons(19).Enabled = False
'    tlbToolbar1.Buttons(16).Enabled = False
'    tlbToolbar1.Buttons(21).Enabled = False
'    tlbToolbar1.Buttons(22).Enabled = False
'    tlbToolbar1.Buttons(23).Enabled = False
'    tlbToolbar1.Buttons(24).Enabled = False
'    tlbToolbar1.Buttons(26).Enabled = False
'    tlbToolbar1.Buttons(28).Enabled = False
'    mnuDatosOpcion(10).Enabled = False
'    mnuDatosOpcion(20).Enabled = False
'    mnuDatosOpcion(40).Enabled = False
'    mnuDatosOpcion(60).Enabled = False
'    mnuDatosOpcion(80).Enabled = False
'    mnuEdicionOpcion(10).Enabled = False
'    mnuEdicionOpcion(30).Enabled = False
'    mnuEdicionOpcion(40).Enabled = False
'    mnuEdicionOpcion(50).Enabled = False
'    mnuEdicionOpcion(60).Enabled = False
'    mnuEdicionOpcion(62).Enabled = False
'    mnuEdicionOpcion(80).Enabled = False
'    mnuEdicionOpcion(90).Enabled = False
'    mnuFiltroOpcion(10).Enabled = False
'    mnuFiltroOpcion(20).Enabled = False
'    mnuRegistroOpcion(10).Enabled = False
'    mnuRegistroOpcion(20).Enabled = False
'    mnuRegistroOpcion(40).Enabled = False
'    mnuRegistroOpcion(50).Enabled = False
'    mnuRegistroOpcion(60).Enabled = False
'    mnuRegistroOpcion(70).Enabled = False
'    mnuRegistroOpcion(72).Enabled = False
'    mnuOpcionesOpcion(10).Enabled = False
'    mnuOpcionesOpcion(20).Enabled = False
'    mnuOpcionesOpcion(40).Enabled = False
'    mnuOpcionesOpcion(50).Enabled = False
'  End If
'
'
'End Sub


Private Sub cmdEuros_Click()
    Dim strEuros As String
    Dim qryEuros As rdoQuery
    Dim rstEuros As rdoResultset
    Dim vntEuros As Variant
    Dim curEUR As Currency
    Dim curPts As Currency
    
    
    If Not IsNumeric(txtText1(5).Text) Then
      MsgBox "El valor ha de ser num�rico", vbInformation
      txtText1(5).SetFocus
      Exit Sub
    End If
    
    cmdEuros.Enabled = False
    
    Select Case SeparadorDec
        Case ".":
            txtText1(5) = objGen.ReplaceStr(txtText1(5).Text, ",", ".", 1)
            txtText1(6) = objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1)
        Case ",":
            txtText1(5) = objGen.ReplaceStr(txtText1(5).Text, ".", ",", 1)
            txtText1(6) = objGen.ReplaceStr(txtText1(6).Text, ".", ",", 1)
    End Select
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
    'strEuros = "select FRH2PARAMGEN from frh200 where FRH2CODPARAMGEN=1"
    strEuros = "select FRH2PARAMGEN from frh200 where FRH2CODPARAMGEN=?"
    Set qryEuros = objApp.rdoConnect.CreateQuery("", strEuros)
    qryEuros(0) = 1
    Set rstEuros = qryEuros.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    'Set rstEuros = objApp.rdoConnect.OpenResultset(strEuros)
'    vntEuros = Format(CDec(Val(txtText1(5).Text) / Val(rstEuros(0).Value)), "##########0.00")
    On Error Resume Next
    vntEuros = Format(CDec(txtText1(5).Text / rstEuros(0).Value), "##########0.00")
    On Error GoTo 0
    txtText1(6) = Fix(vntEuros) '/ rstEuros(0).Value
    'tlbToolbar1.Buttons(4).Enabled = True
    'objWinInfo.objWinActiveForm.rdoCursor.Edit
    'objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FRH1CANTIDADEURO").Value = txtText1(6).Text   'objWinInfo.objWinActiveForm.blnChanged = True
    'objWinInfo.objWinActiveForm.rdoCursor.Update
    'MiCadena = Format(334.9, "###0.00") ' Devuelve "334,90".
    
    'hora = objGen.ReplaceStr(hora, ",", ".", 1)
    rstEuros.Close
    Set rstEuros = Nothing
    
    cmdEuros.Enabled = True
    
End Sub

Private Sub cmdGenerar_Click()
    Dim strA�o As String
    Dim qryA�o As rdoQuery
    Dim rstA�o As rdoResultset
    Dim stra�o2 As String
    Dim qrya�o2 As rdoQuery
    Dim rsta�o2 As rdoResultset
    Dim strInsert As String
    Dim strSigno As String
    Dim strPorc As String
    Dim intMensaje As Integer
    Dim strDelete As String
    cmdGenerar.Enabled = False
    
    If txtText1(0).Text <> "" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
        'strA�o = "select * from frh100 where frg9ejercicio=" & Val(txtText1(0).Text) + 1
        strA�o = "select * from frh100 where frg9ejercicio=?"
        Set qryA�o = objApp.rdoConnect.CreateQuery("", strA�o)
        qryA�o(0) = Val(txtText1(0).Text) + 1
        Set rstA�o = qryA�o.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        'Set rstA�o = objApp.rdoConnect.OpenResultset(strA�o)
        If rstA�o.EOF = True Then
            If txtTP.Text <> "" Then
                If optGenerar(0).Value = True Then
                    strSigno = "+"
                Else
                    strSigno = "-"
                End If
                strPorc = Val(txtTP.Text) / 100
                strPorc = objGen.ReplaceStr(strPorc, ",", ".", 1)
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                'stra�o2 = "SELECT * FROM FRG900 WHERE FRG9EJERCICIO=" & Val(txtText1(0).Text) + 1
                stra�o2 = "SELECT * FROM FRG900 WHERE FRG9EJERCICIO=?"
                Set qrya�o2 = objApp.rdoConnect.CreateQuery("", stra�o2)
                qrya�o2(0) = Val(txtText1(0).Text) + 1
                Set rsta�o2 = qrya�o2.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                'Set rsta�o2 = objApp.rdoConnect.OpenResultset(stra�o2)
                If rsta�o2.EOF = True Then
                    strInsert = "INSERT INTO FRG900 (FRG9EJERCICIO) VALUES (" & Val(txtText1(0).Text) + 1 & ")"
                    objApp.rdoConnect.Execute strInsert, 64
                    objApp.rdoConnect.Execute "COMMIT"
                End If
                
'                strInsert = "INSERT INTO FRH100 " & _
'                            "SELECT  FRG9EJERCICIO + 1," & _
'                            "FR41CODGRUPPROD," & _
'                            "AD02CODDPTO," & _
'                            "FRH1MES," & _
'                            "FRI7CODTIPGRP," & _
'                            "FRH1CANTIDADPTAS " & strSigno & " (FRH1CANTIDADPTAS * (" & strPorc & "))," & _
'                            "FRH1CANTIDADEURO " & strSigno & " (FRH1CANTIDADEURO * (" & strPorc & "))" & _
'                            " From FRH100" & _
'                            " Where FRG9EJERCICIO=" & txtText1(0).Text
                strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,FRH1MES,FRI7CODTIPGRP,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) " & _
                            "SELECT  FRG9EJERCICIO + 1," & _
                            "FR41CODGRUPPROD," & _
                            "FRH1MES," & _
                            "FRI7CODTIPGRP," & _
                            "FRH1CANTIDADPTAS " & strSigno & " (FRH1CANTIDADPTAS * (" & strPorc & "))," & _
                            "FRH1CANTIDADEURO " & strSigno & " (FRH1CANTIDADEURO * (" & strPorc & "))" & _
                            " From FRH100" & _
                            " Where FRG9EJERCICIO=" & txtText1(0).Text
                
                objApp.rdoConnect.Execute strInsert, 64
                objApp.rdoConnect.Execute "COMMIT"
                'If optGrupos(1).Value = True Then
                '    Call rellenar_TreeView_GrupServ
                'Else
                '    Call rellenar_TreeView_ServGrup
                'End If
                Call rellenar_TreeView
            Else
                Call MsgBox("Debe poner un Porcentaje", vbInformation)
            End If
        Else
            intMensaje = MsgBox("El Presupuesto ya ha sido introducido. �Desea Continuar?", vbInformation + vbYesNo)
            If intMensaje = vbYes Then
                strDelete = "delete frh100 where frg9ejercicio=" & Val(txtText1(0).Text) + 1
                objApp.rdoConnect.Execute strDelete, 64
                objApp.rdoConnect.Execute "COMMIT"
                Call cmdGenerar_Click
            End If
        End If
    Else
        Call MsgBox("Debe tener selecionado un a�o", vbInformation)
    End If
    
    cmdGenerar.Enabled = True
End Sub

Private Sub cmdPtas_Click()
    Dim strPtas As String
    Dim qryPtas As rdoQuery
    Dim rstPtas As rdoResultset
    Dim vntPtas As Variant
    
    If Not IsNumeric(txtText1(6).Text) Then
      MsgBox "El valor ha de ser num�rico", vbInformation
      txtText1(6).SetFocus
      Exit Sub
    End If
    
    cmdPtas.Enabled = False
    Select Case SeparadorDec
        Case ".":
            txtText1(5) = objGen.ReplaceStr(txtText1(5).Text, ",", ".", 1)
            txtText1(6) = objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1)
        Case ",":
            txtText1(5) = objGen.ReplaceStr(txtText1(5).Text, ".", ",", 1)
            txtText1(6) = objGen.ReplaceStr(txtText1(6).Text, ".", ",", 1)
    End Select
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
    'strPtas = "select FRH2PARAMGEN from frh200 where FRH2CODPARAMGEN=1"
    strPtas = "select FRH2PARAMGEN from frh200 where FRH2CODPARAMGEN=?"
    Set qryPtas = objApp.rdoConnect.CreateQuery("", strPtas)
    qryPtas(0) = 1
    Set rstPtas = qryPtas.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    'Set rstPtas = objApp.rdoConnect.OpenResultset(strPtas)
    On Error Resume Next
    vntPtas = Fix(Format(CDec(CDec(txtText1(6).Text) * CDec(rstPtas(0).Value)), "##########0.00"))
    On Error GoTo 0
    txtText1(5) = Fix(vntPtas) '/ rstEuros(0).Value
    'objWinInfo.objWinActiveForm.blnChanged = True
    'objWinInfo.objWinActiveForm.rdoCursor.Edit
    'objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FRH1CANTIDADPTAS").Value = txtText1(5).Text   'objWinInfo.objWinActiveForm.blnChanged = True
    'objWinInfo.objWinActiveForm.rdoCursor.Update
    rstPtas.Close
    Set rstPtas = Nothing
    
    cmdPtas.Enabled = True
End Sub

Private Sub Command1_Click()
    Command1.Enabled = False
        Call objsecurity.LaunchProcess("FR0076")
        txtText1(1).Text = gstrCodigoGrupo
        If Len(Trim(gstrDesGrupo)) > 0 Then
          txtText1(2).Text = gstrDesGrupo
        End If
    Command1.Enabled = True
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
    Dim vntclave As Variant
  On Error GoTo GERROR
  If gblnabrir = True And intNewStatus = cwModeSingleEdit And intOldStatus = cwModeSingleOpen Then
    gblnabrir = False
    vntclave = "servicio" & txtText1(0).Text & "/" & txtText1(1).Text & "/" & txtText1(3).Text
    Call tvwItems_NodeClick(0, tvwItems(0).Nodes(vntclave))
    Set tvwItems(0).SelectedItem = tvwItems(0).Nodes(vntclave).FirstSibling
  End If
GERROR:
    Exit Sub
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
 ' Dim objMasterInfo2 As New clsCWForm
 ' Dim objMasterInfo3 As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  'With objMasterInfo
  '  Set .objFormContainer = fraFrame1(0)
  '  Set .objFatherContainer = Nothing
  '  Set .tabMainTab = Nothing
  '  Set .grdGrid = Nothing
    
  '  .strTable = "FRH100"
    
  '  Call .FormAddOrderField("FRG9EJERCICIO", cwAscending)
  '  'Call .FormAddOrderField("FRH1MES", cwAscending)
  '  'Call .FormAddOrderField("FR41CODGRUPPROD", cwAscending)
  '  'Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    
  '  '.strWhere = "FR73CODPRODUCTO=" & frmDefProducto.txtText1(0).Text
   
  '  .intAllowance = cwAllowModify
    
  '  'strKey = .strDataBase & .strTable
  '  'Call .FormCreateFilterWhere(strKey, "Ejercicio")
  '  'Call .FormAddFilterWhere(strKey, "FRG9EJERCICIO", "Ejercicio", cwNumeric)
  '  'Call .FormAddFilterWhere(strKey, "FRH1MES", "Mes", cwString)
  '   'Call .FormAddFilterWhere(strKey, "FR41CODGRUPPROD", "Cod Grupo", cwNumeric)
  '  'Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "Cod Servicio", cwNumeric)
  '  'Call .FormAddFilterWhere(strKey, "FRH1CANTIDADPTAS", "Cantidad Ptas", cwNumeric)
  '  'Call .FormAddFilterWhere(strKey, "FRH1CANTIDADEURO", "Cantidad Euros", cwNumeric)
  '
  '  'Call .FormAddFilterOrder(strKey, "FRG9EJERCICIO", "Ejercicio")
  '  'Call .FormAddFilterOrder(strKey, "FRH1MES", "Mes")
  '  'Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "Cod Grupo")
  '  'Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "Cod Servicio")
  'End With
  
  
  
  
  'With objWinInfo
  '  Call .FormAddInfo(objMasterInfo, cwFormDetail)
  '
  '  Call .FormCreateInfo(objMasterInfo)
  '
  '
  '
  '  'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR41CODGRUPPROD", "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD = ?")
  '  'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(2), "FR41DESGRUPPROD")
  '  '.CtrlGetInfo(txtText1(1)).blnForeign = True
  '
  '  'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
  '  'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(4), "AD02DESDPTO")
  '  '.CtrlGetInfo(txtText1(3)).blnForeign = True
  '
  '
  '  Call .WinRegister
  '  Call .WinStabilize
    
  'End With
  
  
  arbolvacio = True
  tvwItems(0).Visible = True
  
  Call rellenar_TreeView
  marrayMeses(0) = "1.ENERO"
  marrayMeses(1) = "2.FEBRERO"
  marrayMeses(2) = "3.MARZO"
  marrayMeses(3) = "4.ABRIL"
  marrayMeses(4) = "5.MAYO"
  marrayMeses(5) = "6.JUNIO"
  marrayMeses(6) = "7.JULIO"
  marrayMeses(7) = "8.AGOSTO"
  marrayMeses(8) = "9.SEPTIEMBRE"
  marrayMeses(9) = "10.OCTUBRE"
  marrayMeses(10) = "11.NOVIEMBRE"
  marrayMeses(11) = "12.DICIEMBRE"
  mintEstado = 1
  Command1.Visible = False
  'optGrupos(1).Value = True
  'optGenerar(0).Value = True

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  'Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  'Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  'intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  'Call objWinInfo.WinDeRegister
  'Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Select Case SeparadorDec
        Case ".":
            Call objWinInfo.CtrlSet(txtText1(5), objGen.ReplaceStr(txtText1(5).Text, ",", ".", 1))
            Call objWinInfo.CtrlSet(txtText1(6), objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1))
        Case ",":
            Call objWinInfo.CtrlSet(txtText1(5), objGen.ReplaceStr(txtText1(5).Text, ".", ",", 1))
            Call objWinInfo.CtrlSet(txtText1(6), objGen.ReplaceStr(txtText1(6).Text, ".", ",", 1))
    End Select

End Sub


'Private Sub optGrupos_Click(Index As Integer)
'    If Index = 1 Then
'        Call rellenar_TreeView_GrupServ
'    Else
'        Call rellenar_TreeView_ServGrup
'    End If
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  'Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
      
Dim rsta As rdoResultset
Dim sqlstr As String
Dim strSelect As String
Dim qrySelect As rdoQuery
Dim rstSelect As rdoResultset
Dim strA�o As String
Dim qryA�o As rdoQuery
Dim rstA�o As rdoResultset
Dim vntclave As Variant
Dim qryupdate As rdoQuery
Dim strupdate As String
Dim vntPtas As Variant
Dim vntEuro As Variant
Dim i As Integer
Dim vntPta As Variant
Dim vntEur As Variant
Dim strInsert As String
Dim strH100 As String
Dim qryH100 As rdoQuery
Dim rstH100 As rdoResultset
  
  Select Case btnButton.Index
   Case 2:
        'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        txtText1(0).Text = ""
        txtText1(1).Text = ""
        txtText1(2).Text = ""
        txtText1(5).Text = ""
        txtText1(6).Text = ""
        txtText1(0).Locked = False
        txtText1(1).Locked = False
        mintEstado = 2
        Command1.Visible = True
        tlbToolbar1.Buttons(4).Enabled = True
        Exit Sub
        'marrayMeses (0)
  'Case 3 'Abrir
  '  objWinInfo.objWinActiveForm.strWhere = ""
  '  objWinInfo.DataRefresh
  '  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  '  gblnabrir = True
  '  Exit Sub
  Case 4:
    Me.Enabled = False
    Select Case mintEstado
        Case 1:
            Select Case Left(tvwItems(0).SelectedItem.Key, 3)  '(Node.Key, 3)
                Case "eje":
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                    'strSelect = "select count(*) from frh100 where frg9ejercicio=" & txtText1(0).Text
                    strSelect = "select count(*) from frh100 where frg9ejercicio=?"
                    Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
                    qrySelect(0) = txtText1(0).Text
                    Set rstSelect = qrySelect.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    'Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
                    vntPtas = Format(CDec(txtText1(5).Text) / rstSelect(0).Value, "0.00")
                    vntEuro = Format(CDec(txtText1(6).Text) / rstSelect(0).Value, "0.00")
                    vntPtas = objGen.ReplaceStr(Fix(vntPtas), ",", ".", 1)
                    vntEuro = objGen.ReplaceStr(Fix(vntEuro), ",", ".", 1)
                    If mintEstado = 2 Then
                        
                    Else
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                       'strupdate = "update frh100 set frh1cantidadptas=" & vntPtas & _
                       '             ",frh1cantidadeuro=" & vntEuro & _
                       '             " where frg9ejercicio=" & txtText1(0).Text
                       strupdate = "update frh100 set frh1cantidadptas=?" & _
                                    ",frh1cantidadeuro=?" & _
                                    " where frg9ejercicio=? "
                        Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
                        qryupdate(0) = Fix(vntPtas)
                        qryupdate(1) = Fix(vntEuro)
                        qryupdate(2) = txtText1(0).Text
                        qryupdate.Execute
                        'objApp.rdoConnect.Execute strupdate, 64
                    End If
                Case "gru":
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                    'strSelect = "select count(*) from frh100 where frg9ejercicio=" & txtText1(0).Text & _
                    '            " and fr41codgrupprod='" & txtText1(1).Text & "'"
                    strSelect = "select count(*) from frh100 where frg9ejercicio=?" & _
                                " and fr41codgrupprod=?"
                    Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
                    qrySelect(0) = txtText1(0).Text
                    qrySelect(1) = txtText1(1).Text
                    Set rstSelect = qrySelect.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    'Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
                    vntPtas = Format(CDec(txtText1(5).Text) / rstSelect(0).Value, "0.00")
                    vntEuro = Format(CDec(txtText1(6).Text) / rstSelect(0).Value, "0.00")
                    vntPtas = objGen.ReplaceStr(Fix(vntPtas), ",", ".", 1)
                    vntEuro = objGen.ReplaceStr(Fix(vntEuro), ",", ".", 1)
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                    'strupdate = "update frh100 set frh1cantidadptas=" & vntPtas & _
                    '            ",frh1cantidadeuro=" & vntEuro & _
                    '            " where frg9ejercicio=" & txtText1(0).Text & _
                    '            " and fr41codgrupprod='" & txtText1(1).Text & "'"
                    strupdate = "update frh100 set frh1cantidadptas=?" & _
                                ",frh1cantidadeuro=?" & _
                                " where frg9ejercicio=?" & _
                                " and fr41codgrupprod='" & txtText1(1).Text & "'"
                    Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
                    qryupdate(0) = Fix(vntPtas)
                    qryupdate(1) = Fix(vntEuro)
                    qryupdate(2) = txtText1(0).Text
                    'qryupdate(2) = txtText1(1).Text
                    qryupdate.Execute
            End Select
        Case 2:
            If txtText1(0).Text = "" Or txtText1(1).Text = "" Then
                Call MsgBox("Faltan Datos por introducir", vbExclamation, "Aviso")
                Exit Sub
            Else
                If txtText1(5).Text = "" Then
                    vntPta = 0
                Else
                    vntPta = Fix(CDec(txtText1(5).Text) / 12)
                    vntPta = objGen.ReplaceStr(vntPta, ",", ".", 1)
                End If
                If txtText1(6).Text = "" Then
                    vntEur = 0
                Else
                    vntEur = Fix(CDec(txtText1(6).Text) / 12)
                    vntEur = objGen.ReplaceStr(vntEur, ",", ".", 1)
                End If
                If IsNumeric(txtText1(0).Text) = False Then
                  Call MsgBox("Datos Incorrectos", vbExclamation, "Aviso")
                  txtText1(0).Text = ""
                  Me.Enabled = True
                  Exit Sub
                End If
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                'strH100 = "select * from frh100 where frg9ejercicio=" & txtText1(0).Text & _
                '            " and fr41codgrupprod='" & txtText1(1).Text & "'"
                strH100 = "select * from frh100 where frg9ejercicio=?" & _
                            " and fr41codgrupprod=?"
                Set qryH100 = objApp.rdoConnect.CreateQuery("", strH100)
                qryH100(0) = txtText1(0).Text
                qryH100(1) = txtText1(1).Text
                Set rstH100 = qryH100.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                'Set rstH100 = objApp.rdoConnect.OpenResultset(strH100)
                If rstH100.EOF = False Then
                    Call MsgBox("El Registro ya existe en la Base de Datos", vbExclamation, "Aviso")
                Else
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                    'strA�o = "SELECT * FROM FRG900 WHERE FRG9EJERCICIO=" & Val(txtText1(0).Text)
                    strA�o = "SELECT * FROM FRG900 WHERE FRG9EJERCICIO=?"
                    Set qryA�o = objApp.rdoConnect.CreateQuery("", strA�o)
                    qryA�o(0) = Val(txtText1(0).Text)
                    Set rstA�o = qryA�o.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    'Set rstA�o = objApp.rdoConnect.OpenResultset(strA�o)
                    If rstA�o.EOF = True Then
                        strInsert = "INSERT INTO FRG900 (FRG9EJERCICIO) VALUES (" & Val(txtText1(0).Text) & ")"
                        objApp.rdoConnect.Execute strInsert, 64
                        objApp.rdoConnect.Execute "COMMIT"
                    End If
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                    'strSelect = "select FRI7CODTIPGRP from fr4100 where fr41codgrupprod='" & txtText1(1).Text & "'"
                    strSelect = "select FRI7CODTIPGRP from fr4100 where fr41codgrupprod=?"
                    Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
                    qrySelect(0) = txtText1(1).Text
                    Set rstSelect = qrySelect.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    'Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
                    If rstSelect.EOF = False Then
                      For i = 0 To 11
                          strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD," & _
                                             "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO,FRI7CODTIPGRP) VALUES (" & _
                                             txtText1(0).Text & ",'" & _
                                             txtText1(1).Text & "','" & _
                                             marrayMeses(i) & "'," & _
                                             Fix(vntPta) & "," & _
                                             Fix(vntEur) & "," & _
                                             rstSelect("FRI7CODTIPGRP").Value & _
                                             ")"
                          objApp.rdoConnect.Execute strInsert, 64
                          objApp.rdoConnect.Execute "COMMIT"
                          'MsgBox "Datos Salvados", vbInformation, "Salvar"
                      Next i
                      MsgBox "Datos Salvados", vbInformation, "Salvar"
                    End If
                    rstSelect.Close
                    Set rstSelect = Nothing
                    rstA�o.Close
                    Set rstA�o = Nothing
                    Call rellenar_TreeView
                End If
                rstH100.Close
                Set rstH100 = Nothing
            End If
        End Select
        Me.Enabled = True
  Case 30 'salir
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Unload Me
    'Exit Sub
  Case 21 'Primero
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    Exit Sub
  Case 22 'Anterior
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Previous.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    Exit Sub
  Case 23 'Siguiente
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Next.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    Exit Sub
  Case 24 'Ultimo
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      tvwItems(0).SelectedItem.LastSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    Exit Sub
  Case Else 'Otro boton
    'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End Select
  
  mintEstado = 1
  If btnButton.Index <> 30 Then
    Command1.Visible = False
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 40 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  'Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  'Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  'Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  'Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  'Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  'Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  'Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  'Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  'Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(Index As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  'Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  'Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub


Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)

  'If EnPostWrite = True Then
  '  EnPostWrite = False
  '  Exit Sub
  'End If

  Node.EnsureVisible
  Node.Selected = True
  Call tvwItems_NodeClick(0, Node)

End Sub
Private Sub Vaciar_Campos()
    txtText1(0).Text = ""
    txtText1(1).Text = ""
    txtText1(2).Text = ""
    'txtText1(3).Text = ""
    'txtText1(4).Text = ""
    txtText1(5).Text = ""
End Sub

'Private Sub tvwItems_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'  Set nodX = tvwItems(0).SelectedItem ' Establece el elemento arrastrado.
'End Sub

Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    
Dim ejercicio As Variant
Dim mes As Variant
Dim grupo As Variant
Dim Servicio As Variant
Dim primeraparte As Variant
Dim segundaparte As Variant
'Dim terceraparte As Variant
Dim resto As Variant
Dim strWhere As String
Dim qryWhere As rdoQuery
Dim rstwhere As rdoResultset
    
    'Select Case optGrupos(1).Value
    '    Case True:
        
            Select Case Left(Node.Key, 3)
                Case "eje":
                  Call Vaciar_Campos
                  'Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  'Call objWinInfo.DataOpen
                  ejercicio = Right(Node.Key, Len(Node.Key) - 5)
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) " & _
                  '          "from frh100 where frg9ejercicio=" & ejercicio
                  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) " & _
                            "from frh100 where frg9ejercicio=?"
                  Set qryWhere = objApp.rdoConnect.CreateQuery("", strWhere)
                  qryWhere(0) = ejercicio
                  Set rstwhere = qryWhere.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                  'Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  txtText1(0).Text = ejercicio
                  txtText1(5).Text = rstwhere(0).Value
                  txtText1(6).Text = rstwhere(1).Value
                'Case "mes":
                '  Call Vaciar_Campos
                '  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                '  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                '  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                '  mes = resto
                '  ejercicio = Right(Left(Node.Key, InStr(Node.Key, "/") - 1), Len(Left(Node.Key, InStr(Node.Key, "/") - 1)) - 3)
                '  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) " & _
                '            "from frh100 where frg9ejercicio=" & ejercicio & " and frh1mes='" & mes & "'"
                '  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                '  txtText1(0).Text = ejercicio
                '  'txttext1(1).Text=
                '  'objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio & " and frh1mes='" & mes & "'"
                '  'objWinInfo.DataRefresh
                Case "gru":
                  Call Vaciar_Campos
                  'Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  'segundaparte = Left(resto, InStr(resto, "/"))
                  'resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 6)
                  'mes = Left(segundaparte, Len(segundaparte) - 1)
                  grupo = resto
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
                  'strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                  '           "frg9ejercicio=" & ejercicio & _
                  '           " and fr41codgrupprod='" & grupo & "'"
                  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                             "frg9ejercicio=?" & _
                             " and fr41codgrupprod=?"
                  Set qryWhere = objApp.rdoConnect.CreateQuery("", strWhere)
                  qryWhere(0) = ejercicio
                  qryWhere(1) = grupo
                  Set rstwhere = qryWhere.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                  'Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  txtText1(0).Text = ejercicio
                  txtText1(1).Text = grupo
                  txtText1(5).Text = rstwhere(0).Value
                  txtText1(6).Text = rstwhere(1).Value
                'Case "ser":
                '  Call Vaciar_Campos
                '  'Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                '  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                '  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                '  segundaparte = Left(resto, InStr(resto, "/"))
                '  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                '  'terceraparte = Left(resto, InStr(resto, "/"))
                '  'resto = Right(resto, Len(resto) - InStr(resto, "/"))
                '  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 9)
                '  'mes = Left(segundaparte, Len(segundaparte) - 1)
                '  grupo = Left(segundaparte, Len(segundaparte) - 1)
                '  Servicio = resto
                '  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                '             "frg9ejercicio=" & ejercicio & _
                '             " and fr41codgrupprod=" & grupo & _
                '             " and AD02CODDPTO=" & Servicio
                '  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                '  txtText1(0).Text = ejercicio
                '  txtText1(1).Text = grupo
                '  txtText1(3).Text = Servicio
                '  txtText1(5).Text = rstwhere(0).Value
                '  txtText1(6).Text = rstwhere(1).Value
            End Select
     '   Case False:
     '       Select Case Left(Node.Key, 3)
     '           Case "eje":
     '             Call Vaciar_Campos
     '             'Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
     '             'Call objWinInfo.DataOpen
     '             ejercicio = Right(Node.Key, Len(Node.Key) - 5)
     '             strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
     '                        "frg9ejercicio=" & ejercicio
     '             Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
     '             txtText1(0).Text = ejercicio
     '             txtText1(5).Text = rstwhere(0).Value
     '             txtText1(6).Text = rstwhere(1).Value
     '           'Case "mes":
     '           '  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
     '           '  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
     '           '  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
     '           '  mes = resto
     '           '  ejercicio = Right(Left(Node.Key, InStr(Node.Key, "/") - 1), Len(Left(Node.Key, InStr(Node.Key, "/") - 1)) - 3)
     '           '  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio & " and frh1mes='" & mes & "'"
     '           '  objWinInfo.DataRefresh
     '           Case "ser":
     '             Call Vaciar_Campos
     '             'Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
     '             primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
     '             resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
     '             'segundaparte = Left(resto, InStr(resto, "/"))
     '             'resto = Right(resto, Len(resto) - InStr(resto, "/"))
     '             ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 9)
     '             'mes = Left(segundaparte, Len(segundaparte) - 1)
     '             Servicio = resto
     '             strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
     '                        "frg9ejercicio=" & ejercicio & _
     '                        " and AD02CODDPTO=" & Servicio
     '             Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
     '             txtText1(0).Text = ejercicio
     '             txtText1(1).Text = Servicio
     '             txtText1(5).Text = rstwhere(0).Value
     '             txtText1(6).Text = rstwhere(1).Value
     '           Case "gru":
     '             Call Vaciar_Campos
     '             'Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
     '             primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
     '             resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
     '             segundaparte = Left(resto, InStr(resto, "/"))
     '             resto = Right(resto, Len(resto) - InStr(resto, "/"))
     '             'terceraparte = Left(resto, InStr(resto, "/"))
     '             'resto = Right(resto, Len(resto) - InStr(resto, "/"))
     '             ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 6)
     '             'mes = Left(segundaparte, Len(segundaparte) - 1)
     '             Servicio = Left(segundaparte, Len(segundaparte) - 1)
     '             grupo = resto
     '             strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
     '                        "frg9ejercicio=" & ejercicio & _
     '                        " and fr41codgrupprod=" & grupo & _
     '                        " and AD02CODDPTO=" & Servicio
     '             Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
     '             txtText1(0).Text = ejercicio
     '             txtText1(3).Text = Servicio
     '             txtText1(1).Text = grupo
     '             txtText1(5).Text = rstwhere(0).Value
     '             txtText1(6).Text = rstwhere(1).Value
     '           End Select
     '   End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  'Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Dim strgrupo As String
    Dim qrygrupo As rdoQuery
    Dim rstgrupo As rdoResultset
    Dim strServicio As String
    Dim rstServicio As rdoResultset
  'Call objWinInfo.CtrlDataChange
  Select Case intIndex
    Case 1:
        If txtText1(1).Text <> "" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
            'strgrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD='" & txtText1(1).Text & "'"
            strgrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=?"
            Set qrygrupo = objApp.rdoConnect.CreateQuery("", strgrupo)
            qrygrupo(0) = txtText1(1).Text
            Set rstgrupo = qrygrupo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            'Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
            If rstgrupo.EOF = False Then
                txtText1(2).Text = rstgrupo(0).Value
            End If
            rstgrupo.Close
            Set rstgrupo = Nothing
        End If
    Case 5:
      Call CalEuros
    'Case 3:
    '    If txtText1(3).Text <> "" Then
    '        strServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & txtText1(3).Text
    '        Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
    '        If rstServicio.EOF = False Then
    '            txtText1(4).Text = rstServicio(0).Value
    '        End If
    '        rstServicio.Close
    '        Set rstServicio = Nothing
    '    End If
  End Select
End Sub

Private Sub txtText1_LostFocus(Index As Integer)
  Select Case Index
    Case 0:
      If IsNumeric(txtText1(0).Text) = False Then
        txtText1(0).Text = ""
      End If
  End Select
End Sub

'Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
'
'  Dim objField As clsCWFieldSearch
'  Dim objSearch As clsCWSearch
'
'  If strCtrl = "txtText1(1)" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "FR4100"
'     .strOrder = "ORDER BY FR41CODGRUPPROD ASC"
'
'     Set objField = .AddField("FR41CODGRUPPROD")
'     objField.strSmallDesc = "C�digo Grupo Producto"
'
'     Set objField = .AddField("FR41DESGRUPPROD")
'     objField.strSmallDesc = "Descripci�n Grupo Producto"
'
'     If .Search Then
'        Call objWinInfo.CtrlSet(txtText1(2), .cllValues("FR41DESGRUPPROD"))
'     End If
'   End With
'   Set objSearch = Nothing
' End If

'  If strCtrl = "txtText1(3)" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "AD0200"
'     .strOrder = "ORDER BY AD02CODDPTO ASC"
'
'     Set objField = .AddField("AD02CODDPTO")
'     objField.strSmallDesc = "C�digo Servicio"
'
'     Set objField = .AddField("AD02DESDPTO")
'     objField.strSmallDesc = "Descripci�n Servicio"
'
'     If .Search Then
'        If strCtrl = "txtText1(3)" Then
'            Call objWinInfo.CtrlSet(txtText1(4), .cllValues("AD02DESDPTO"))
'        End If
'     End If
'   End With
'   Set objSearch = Nothing
' End If
'
'
'End Sub




Private Sub txtTP_LostFocus()
  If IsNumeric(txtTP.Text) = False Then
    txtTP.Text = ""
  End If
End Sub
