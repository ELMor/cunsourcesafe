VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmContrPresuFarm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Seguimiento del Gasto."
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5175
      Left            =   120
      TabIndex        =   7
      Top             =   720
      Width           =   3495
      Begin VB.CheckBox chkConsumos 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Consumos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Frame Frame5 
         Caption         =   "Intervalo"
         ForeColor       =   &H00000080&
         Height          =   975
         Left            =   240
         TabIndex        =   17
         Top             =   4080
         Visible         =   0   'False
         Width           =   3015
         Begin SSCalendarWidgets_A.SSDateCombo dtcinicio 
            Height          =   330
            Left            =   1200
            TabIndex        =   18
            Tag             =   "Fecha Inicio"
            ToolTipText     =   "Fecha Inicio"
            Top             =   240
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcfin 
            Height          =   330
            Left            =   1200
            TabIndex        =   19
            Tag             =   "Fecha Fin"
            ToolTipText     =   "Fecha Fin"
            Top             =   600
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   21
            Top             =   360
            Width           =   870
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   20
            Top             =   600
            Width           =   705
         End
      End
      Begin VB.CommandButton cmdMostrarGrafica 
         Caption         =   "Mostrar Gr�fica"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   720
         TabIndex        =   16
         Top             =   3840
         Width           =   1935
      End
      Begin VB.CheckBox chkVerPresup 
         Caption         =   "Presupuesto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   480
         Width           =   1575
      End
      Begin VB.CheckBox chkVerCompras 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Compras"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   840
         Width           =   1455
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         DataField       =   "frh5codperiodicidad"
         Height          =   330
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   1920
         Width           =   1005
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets.count =   2
         stylesets(0).Name=   "Activo"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0050.frx":0000
         stylesets(1).Name=   "Inactivo"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0050.frx":001C
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "Ejercicio"
         Columns(0).Name =   "Ejercicio"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1773
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo ComboTiposGrupo 
         Height          =   330
         Left            =   1200
         TabIndex        =   14
         Top             =   1920
         Visible         =   0   'False
         Width           =   2205
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         stylesets.count =   2
         stylesets(0).Name=   "Activo"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0050.frx":0038
         stylesets(1).Name=   "Inactivo"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0050.frx":0054
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8811
         Columns(1).Caption=   "Tipos de Grupo"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3889
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo ComboCuentas 
         Height          =   330
         Left            =   120
         TabIndex        =   22
         Top             =   2640
         Width           =   3285
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         stylesets.count =   2
         stylesets(0).Name=   "Activo"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0050.frx":0070
         stylesets(1).Name=   "Inactivo"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0050.frx":008C
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2249
         Columns(0).Caption=   "C�d.Cuenta"
         Columns(0).Name =   "C�d.Cuenta"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   9895
         Columns(1).Caption=   "Cuenta"
         Columns(1).Name =   "Cuenta"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5794
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo ComboDepartamentos 
         Height          =   330
         Left            =   120
         TabIndex        =   29
         Top             =   3360
         Visible         =   0   'False
         Width           =   3285
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         stylesets.count =   2
         stylesets(0).Name=   "Activo"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0050.frx":00A8
         stylesets(1).Name=   "Inactivo"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0050.frx":00C4
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2249
         Columns(0).Caption=   "C�d.Cuenta"
         Columns(0).Name =   "C�d.Cuenta"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   9895
         Columns(1).Caption=   "Cuenta"
         Columns(1).Name =   "Cuenta"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5794
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lbllabel1 
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   30
         Top             =   3120
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label Label3 
         BackColor       =   &H00FF0000&
         ForeColor       =   &H00000000&
         Height          =   75
         Left            =   1800
         TabIndex        =   28
         Top             =   1320
         Width           =   405
      End
      Begin VB.Label Label2 
         BackColor       =   &H0000FF00&
         ForeColor       =   &H00000000&
         Height          =   75
         Left            =   1800
         TabIndex        =   26
         Top             =   960
         Width           =   405
      End
      Begin VB.Label Label1 
         BackColor       =   &H000000FF&
         ForeColor       =   &H00000000&
         Height          =   75
         Left            =   1800
         TabIndex        =   25
         Top             =   600
         Width           =   405
      End
      Begin VB.Label lbllabel1 
         Caption         =   "Cuenta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   23
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label lbllabel1 
         Caption         =   "Tipo de Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Index           =   1
         Left            =   1200
         TabIndex        =   15
         Top             =   1680
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label lbllabel1 
         Caption         =   "Ejercicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   1680
         Width           =   855
      End
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   1365
      Left            =   0
      TabIndex        =   6
      Top             =   6600
      Width           =   11865
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   14
      stylesets.count =   3
      stylesets(0).Name=   "Rojo"
      stylesets(0).BackColor=   8421631
      stylesets(0).Picture=   "FR0050.frx":00E0
      stylesets(1).Name=   "Verde"
      stylesets(1).BackColor=   8454016
      stylesets(1).Picture=   "FR0050.frx":00FC
      stylesets(2).Name=   "Azul"
      stylesets(2).BackColor=   16744448
      stylesets(2).Picture=   "FR0050.frx":0118
      AllowUpdate     =   0   'False
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      Columns.Count   =   14
      Columns(0).Width=   1535
      Columns(0).Caption=   "1"
      Columns(0).Name =   "1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(1).Width=   1535
      Columns(1).Caption=   "2"
      Columns(1).Name =   "2"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasForeColor=   -1  'True
      Columns(2).Width=   1535
      Columns(2).Caption=   "3"
      Columns(2).Name =   "3"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasForeColor=   -1  'True
      Columns(3).Width=   1535
      Columns(3).Caption=   "4"
      Columns(3).Name =   "4"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasForeColor=   -1  'True
      Columns(4).Width=   1535
      Columns(4).Caption=   "5"
      Columns(4).Name =   "5"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasForeColor=   -1  'True
      Columns(5).Width=   1535
      Columns(5).Caption=   "6"
      Columns(5).Name =   "6"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).HasForeColor=   -1  'True
      Columns(6).Width=   1535
      Columns(6).Caption=   "7"
      Columns(6).Name =   "7"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).HasForeColor=   -1  'True
      Columns(7).Width=   1535
      Columns(7).Caption=   "8"
      Columns(7).Name =   "8"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).HasForeColor=   -1  'True
      Columns(8).Width=   1535
      Columns(8).Caption=   "9"
      Columns(8).Name =   "9"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).HasForeColor=   -1  'True
      Columns(9).Width=   1535
      Columns(9).Caption=   "10"
      Columns(9).Name =   "10"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).HasForeColor=   -1  'True
      Columns(10).Width=   1535
      Columns(10).Caption=   "11"
      Columns(10).Name=   "11"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).HasForeColor=   -1  'True
      Columns(11).Width=   1535
      Columns(11).Caption=   "12"
      Columns(11).Name=   "12"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).HasForeColor=   -1  'True
      Columns(12).Width=   1773
      Columns(12).Caption=   "Total"
      Columns(12).Name=   "Total"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(12).HasForeColor=   -1  'True
      Columns(13).Width=   3200
      Columns(13).Caption=   "Opci�n"
      Columns(13).Name=   "Opci�n"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      _ExtentX        =   20929
      _ExtentY        =   2408
      _StockProps     =   79
   End
   Begin VB.Frame Frame3 
      Height          =   975
      Left            =   960
      TabIndex        =   5
      Top             =   5880
      Visible         =   0   'False
      Width           =   975
      Begin VB.OptionButton optPtas 
         Caption         =   "Euros"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Index           =   1
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.OptionButton optPtas 
         Caption         =   "Ptas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Value           =   -1  'True
         Visible         =   0   'False
         Width           =   1095
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin MSChartLib.MSChart MSChart1 
      Height          =   5895
      Left            =   3840
      OleObjectBlob   =   "FR0050.frx":0134
      TabIndex        =   24
      Top             =   480
      Width           =   7935
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   2640
      Top             =   6120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label8 
      BackColor       =   &H000000FF&
      Height          =   105
      Left            =   0
      TabIndex        =   11
      Top             =   75
      Width           =   375
   End
   Begin VB.Label Label7 
      BackColor       =   &H0000FF00&
      Height          =   105
      Left            =   0
      TabIndex        =   10
      Top             =   315
      Width           =   375
   End
   Begin VB.Label Label6 
      Height          =   255
      Left            =   480
      TabIndex        =   9
      Top             =   0
      Width           =   4695
   End
   Begin ComctlLib.ImageList imlImagenes 
      Left            =   0
      Top             =   7560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":25E3
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2641
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":269F
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":26FD
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":275B
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":27B9
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2817
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2875
            Key             =   "function"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":28D3
            Key             =   "window"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2931
            Key             =   "report"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":298F
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":29ED
            Key             =   "process"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2A4B
            Key             =   "application"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2AA9
            Key             =   "big"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2B07
            Key             =   "small"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2B65
            Key             =   "list"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2BC3
            Key             =   "details"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2C21
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2C7F
            Key             =   "refresh"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmContrPresuFarm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmContrPresuFarm (FR0050.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Controlar Presupuestos                                  *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
Dim gblnabrir As Boolean
Dim gintMoneda As Integer
Dim gintMarca As Integer
Dim gGrafico1(1 To 2, 1 To 12)
Dim gGrafico2(1 To 2, 1 To 12)
Dim gGrafico3(1 To 2, 1 To 12)
Dim gPrimeraVez As Boolean

Dim mvarEuro As Variant
Dim mintMes As Integer
Dim mstrFechas As String
Dim mblnExpand As Boolean
Dim blnColorearGrid As Boolean

Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strProveedor As String
  Dim strAnoD As String
  Dim strAnoH As String
  Dim strIns As String
  Dim strDel As String
  Dim strInsert As String
  Dim strDelete As String
  Dim strupdate As String
  Dim stra As String
  Dim rsta As rdoResultset
  Dim ejercicio As String
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  intDes = 1
  strListado = "FR0501.RPT"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
            
  Screen.MousePointer = vbHourglass
  
  'se llena una tabla temporal FRL600
  
  ejercicio = cboDBCombo1(0).Value & "-" & cboDBCombo1(0).Value + 1
  
  strDelete = "DELETE FRL600"
  objApp.rdoConnect.Execute strDelete, 64
  objApp.rdoConnect.Execute "COMMIT"
  'Presupuestos
  strInsert = "INSERT INTO FRL600(FRL6CUENTA,FRL6PRESUPUESTO)"
  strInsert = strInsert & " SELECT FR41CODGRUPPROD,SUM(FRH1CANTIDADPTAS) CANTIDADPTAS"
  strInsert = strInsert & " FROM FRH100 "
  strInsert = strInsert & " WHERE FRG9EJERCICIO = " & Val(cboDBCombo1(0).Text)
  strInsert = strInsert & " GROUP BY FR41CODGRUPPROD "
  objApp.rdoConnect.Execute strInsert, 64
  objApp.rdoConnect.Execute "COMMIT"
  'Compras(Gasto)
  stra = "SELECT CEIL(SUM(FRJ300.FRJ3IMPORLINEA)) IP , "
  stra = stra & "       CEIL(SUM(FRJ300.FRJ3IMPORLINEA) / " & Fix(mvarEuro) & ") IE ,"
  stra = stra & "       FR7300.FR73CUENCONT CUENTA"
  stra = stra & "  FROM FRJ100,FRJ300,FR7300 "
  stra = stra & " WHERE FRJ100.FRJ1CODALBARAN=FRJ300.FRJ1CODALBARAN "
  stra = stra & "   AND FRJ100.FRJ1FECHAALBAR >= to_date('1/" & mintMes & "/" & Val(cboDBCombo1(0).Text) & "','dd/mm/yyyy')"
  stra = stra & "   AND FRJ100.FRJ1FECHAALBAR < to_date('1/" & mintMes & "/" & Val(cboDBCombo1(0).Text) + 1 & "','dd/mm/yyyy')"
  stra = stra & "   AND FR7300.FR73CODPRODUCTO=FRJ300.FR73CODPRODUCTO "
  stra = stra & " GROUP BY FR7300.FR73CUENCONT"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    strupdate = "UPDATE FRL600 SET FRL6GASTO=" & objGen.ReplaceStr(rsta.rdoColumns("IP").Value, ",", ".", 1)
    strupdate = strupdate & " WHERE FRL6CUENTA=" & "'" & rsta.rdoColumns("CUENTA").Value & "'"
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "COMMIT"
    strupdate = "UPDATE FRL600 SET FRL6EJERCICIO=" & "'" & ejercicio & "'"
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "COMMIT"
    strupdate = "UPDATE FRL600 SET FRL6GASTO=0 WHERE FRL6GASTO IS NULL"
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "COMMIT"
  rsta.MoveNext
  Wend
     
  CrystalReport1.ReportFileName = strPATH & strListado
  'CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp4
  CrystalReport1.Action = 1
  Screen.MousePointer = vbDefault
Err_imp4:

End Sub

Private Sub cboDBCombo1_CloseUp(Index As Integer)
    mstrFechas = "and fr80fecmovimiento >= to_date('1/" & mintMes & "/" & Val(cboDBCombo1(0).Text) & "','dd/mm/yyyy')" & _
                " and fr80fecmovimiento < to_date('1/" & mintMes - 1 & "/" & Val(cboDBCombo1(0).Text) + 1 & "','dd/mm/yyyy')"
    tlbToolbar1.Buttons(6).Enabled = True

End Sub

Private Sub chkConsumos_Click()
If chkConsumos.Value = 1 Then
  ComboDepartamentos.Visible = True
  lblLabel1(5).Visible = True
Else
  ComboDepartamentos.Visible = False
  lblLabel1(5).Visible = False
End If
End Sub

Private Sub chkVerPresup_Click()
  Dim i As Integer
  SSDBGrid1.MoveFirst
    For i = 0 To 11
      SSDBGrid1.Columns(i).CellStyleSet "Rojo"
    Next i
    SSDBGrid1.MoveNext
    For i = 0 To 11
      SSDBGrid1.Columns(i).CellStyleSet "Verde"
    Next i
    SSDBGrid1.MoveNext
    For i = 0 To 11
      SSDBGrid1.Columns(i).CellStyleSet "Azul"
    Next i
End Sub





Private Sub cmdMostrarGrafica_Click()
If cboDBCombo1(0).Value <> "" And ComboCuentas.Value <> "" Then
    If chkConsumos.Value = Unchecked Then
          blnColorearGrid = True
          Call Rellenar_Grafica
    Else
        If ComboDepartamentos.Value = "" Then
             Call MsgBox("Debe seleccionar un Departamento.", vbInformation, "Aviso")
        Else
           blnColorearGrid = True
           Call Rellenar_Grafica
        End If
    End If
Else
    Call MsgBox("Debe seleccionar un Ejercicio y una Cuenta Contable.", vbInformation, "Aviso")
End If
End Sub

Private Sub Form_Activate()
    
    Dim rowLabelCount As Integer
    Dim columnLabelCount As Integer
    Dim rowCount As Integer
    Dim columnCount As Integer
    Dim datagrid As datagrid
    Dim labelindex As Integer
    Dim column As Integer
    Dim row As Integer
    Dim i As Integer
    Dim strSelect As String
    Dim qrySelect As rdoQuery
    Dim rstSelect As rdoResultset
    
    blnColorearGrid = False
    
    strSelect = "select * from frg900 order by FRG9EJERCICIO"
    Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
    While rstSelect.EOF = False
        cboDBCombo1(0).AddItem rstSelect("FRG9EJERCICIO").Value
        rstSelect.MoveNext
    Wend
    strSelect = "select * from fri700 order by FRI7DESTIPGRP"
    Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
    While rstSelect.EOF = False
        ComboTiposGrupo.AddItem rstSelect("FRI7CODTIPGRP").Value & ";" & rstSelect("FRI7DESTIPGRP").Value
        rstSelect.MoveNext
    Wend
    strSelect = "SELECT * FROM FR4100 WHERE  FRI7CODTIPGRP=1 ORDER BY FR41CODGRUPPROD"
    Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
    ComboCuentas.AddItem "00" & ";" & "Todas las Cuentas"
    While Not rstSelect.EOF
      ComboCuentas.AddItem rstSelect("FR41CODGRUPPROD").Value & ";" & rstSelect("FR41DESGRUPPROD").Value
    rstSelect.MoveNext
    Wend
    rstSelect.Close
    Set rstSelect = Nothing
    strSelect = "SELECT * FROM AD0200 ORDER BY AD02DESDPTO"
    Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
    ComboDepartamentos.AddItem "00" & ";" & "Todos los Departamentos"
    While Not rstSelect.EOF
      ComboDepartamentos.AddItem rstSelect("AD02CODDPTO").Value & ";" & rstSelect("AD02DESDPTO").Value
    rstSelect.MoveNext
    Wend
    rstSelect.Close
    Set rstSelect = Nothing
    
    strSelect = "select frh2paramgen from frh200 where frh2codparamgen=?"
    Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
    qrySelect(0) = 1
    Set rstSelect = qrySelect.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    mvarEuro = objGen.ReplaceStr(rstSelect(0).Value, ",", ".", 1)
    strSelect = "select frh2paramgen from frh200 where frh2codparamgen=?"
    Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
    qrySelect(0) = 13
    Set rstSelect = qrySelect.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    mintMes = rstSelect(0).Value
    
    Set datagrid = MSChart1.datagrid
    MSChart1.ShowLegend = True
    With MSChart1.datagrid
    ' Establece los par�metros del gr�fico con
    ' los m�todos.
      rowLabelCount = 12
      columnLabelCount = 3
      rowCount = 12
      columnCount = 3
      .SetSize rowLabelCount, columnLabelCount, rowCount, columnCount
      labelindex = 1
      row = 1
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Ene"
      .RowLabel(row, labelindex) = "Enero"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 2
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Feb"
      .RowLabel(row, labelindex) = "Febrero"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 3
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Mar"
      .RowLabel(row, labelindex) = "Marzo"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 4
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Abr"
      .RowLabel(row, labelindex) = "Abril"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 5
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "May"
      .RowLabel(row, labelindex) = "Mayo"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 6
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Jun"
      .RowLabel(row, labelindex) = "Junio"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 7
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Jul"
      .RowLabel(row, labelindex) = "Julio"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 8
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Ago"
      .RowLabel(row, labelindex) = "Agosto"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 9
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Sep"
      .RowLabel(row, labelindex) = "Septiembre"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 10
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Oct"
      .RowLabel(row, labelindex) = "Octubre"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 11
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Nov"
      .RowLabel(row, labelindex) = "Noviembre"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
      row = 12
      row = ((12 - mintMes + row) Mod 12) + 1
      SSDBGrid1.Columns(row - 1).Caption = "Dic"
      .RowLabel(row, labelindex) = "Diciembre"
      Call .SetData(row, 1, 0, 0)
      Call .SetData(row, 2, 0, 0)
      Call .SetData(row, 3, 0, 0)
    End With
    
    
    SSDBGrid1.RowHeight = 275
    SSDBGrid1.AddNew
    For i = 0 To 11
        SSDBGrid1.Columns(i).Value = 0
    Next i
    SSDBGrid1.Columns(12).Value = 0
    SSDBGrid1.Columns(13).Value = "Presupuesto"
    SSDBGrid1.Update
    
    SSDBGrid1.AddNew
    For i = 0 To 12
        SSDBGrid1.Columns(i).Value = 0
    Next i
    SSDBGrid1.Columns(12).Value = 0
    SSDBGrid1.Columns(13).Value = "Compras"
    SSDBGrid1.Update
    
    SSDBGrid1.AddNew
    For i = 0 To 12
        SSDBGrid1.Columns(i).Value = 0
    Next i
    SSDBGrid1.Columns(12).Value = 0
    SSDBGrid1.Columns(13).Value = "Consumos"
    SSDBGrid1.Update
    
    SSDBGrid1.Columns(13).Visible = False
    
    gPrimeraVez = False
    
    Call controlar_menues

End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  
  gintMarca = 1
  gPrimeraVez = True
  optPtas(0).Value = True
  
End Sub



Private Sub MSChart1_SeriesSelected(Series As Integer, MouseFlags As Integer, Cancel As Integer)
    'gintMarca = Series
End Sub

Private Sub optPtas_Click(Index As Integer)
Dim i As Integer

    If Index = 0 Then
        gintMoneda = 0
    Else
        gintMoneda = 1
    End If
    If gPrimeraVez = False Then
        For i = 1 To 12
            Call MSChart1.datagrid.SetData(i, 1, gGrafico1(gintMoneda + 1, i), 0)
            Call MSChart1.datagrid.SetData(i, 2, gGrafico2(gintMoneda + 1, i), 0)
        Next i
    End If
End Sub



Private Sub SSDBGrid1_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer

If SSDBGrid1.Columns(13).Value = "Presupuesto" Then
  For i = 0 To 13
    SSDBGrid1.Columns(i).CellStyleSet "Rojo"
  Next i
End If
If SSDBGrid1.Columns(13).Value = "Compras" Then
  For i = 0 To 13
    SSDBGrid1.Columns(i).CellStyleSet "Verde"
  Next i
End If
If SSDBGrid1.Columns(13).Value = "Consumos" Then
  For i = 0 To 13
    SSDBGrid1.Columns(i).CellStyleSet "Azul"
  Next i
End If
     
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
      
Dim rsta As rdoResultset
Dim sqlstr As String
Dim strSelect As String
Dim rstSelect As rdoResultset
Dim vntclave As Variant
Dim vntPtas As Variant
Dim vntEuro As Variant
  tlbToolbar1.Buttons(6).Enabled = True
  
  Select Case btnButton.Index
  Case 11
      MSChart1.EditCopy
  Case 30 'salir
    Unload Me
  Case 6 'Imprimir
        'Call Imprimir("a", 1) 'listado
        Call Imprimir("a", 0) 'pantalla
        tlbToolbar1.Buttons(6).Enabled = True
        Exit Sub
  Case Else 'Otro boton
    tlbToolbar1.Buttons(6).Enabled = True
    Exit Sub
  End Select
errT:

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub


Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub


Private Sub controlar_menues()
    
    tlbToolbar1.Buttons(2).Enabled = False
    tlbToolbar1.Buttons(3).Enabled = False
    tlbToolbar1.Buttons(4).Enabled = False
    tlbToolbar1.Buttons(6).Enabled = False
    tlbToolbar1.Buttons(8).Enabled = False
    tlbToolbar1.Buttons(14).Enabled = False
    tlbToolbar1.Buttons(10).Enabled = False
    tlbToolbar1.Buttons(11).Enabled = False
    tlbToolbar1.Buttons(12).Enabled = False
    tlbToolbar1.Buttons(18).Enabled = False
    tlbToolbar1.Buttons(19).Enabled = False
    tlbToolbar1.Buttons(16).Enabled = False
    tlbToolbar1.Buttons(21).Enabled = False
    tlbToolbar1.Buttons(22).Enabled = False
    tlbToolbar1.Buttons(23).Enabled = False
    tlbToolbar1.Buttons(24).Enabled = False
    tlbToolbar1.Buttons(26).Enabled = False
    tlbToolbar1.Buttons(28).Enabled = False
    mnuDatosOpcion(10).Enabled = False
    mnuDatosOpcion(20).Enabled = False
    mnuDatosOpcion(40).Enabled = False
    mnuDatosOpcion(60).Enabled = False
    mnuDatosOpcion(80).Enabled = False
    mnuEdicionOpcion(10).Enabled = False
    mnuEdicionOpcion(30).Enabled = False
    mnuEdicionOpcion(40).Enabled = False
    mnuEdicionOpcion(50).Enabled = False
    mnuEdicionOpcion(60).Enabled = False
    mnuEdicionOpcion(62).Enabled = False
    mnuEdicionOpcion(80).Enabled = False
    mnuEdicionOpcion(90).Enabled = False
    mnuFiltroOpcion(10).Enabled = False
    mnuFiltroOpcion(20).Enabled = False
    mnuRegistroOpcion(10).Enabled = False
    mnuRegistroOpcion(20).Enabled = False
    mnuRegistroOpcion(40).Enabled = True
    mnuRegistroOpcion(50).Enabled = True
    mnuRegistroOpcion(60).Enabled = True
    mnuRegistroOpcion(70).Enabled = True
    mnuRegistroOpcion(72).Enabled = False
    mnuOpcionesOpcion(10).Enabled = False
    mnuOpcionesOpcion(20).Enabled = False
    mnuOpcionesOpcion(40).Enabled = False
    mnuOpcionesOpcion(50).Enabled = False

End Sub

Public Sub Rellenar_Grafica()
Dim ejercicio As Variant
Dim mes As Variant
Dim grupo As Variant
Dim Servicio As Variant
Dim primeraparte As Variant
Dim segundaparte As Variant
Dim terceraparte As Variant
Dim resto As Variant
Dim strWhere As String
Dim rstwhere As rdoResultset
Dim intmes As Integer
Dim i As Integer
Dim TipGrupo As Variant
Dim qryWhere As rdoQuery
Dim intE As Integer
Dim strDel As String
Dim strFiltroFechas As String
Dim rsta As rdoResultset
Dim stra As String
Dim strDelete As String

Me.Enabled = False
Screen.MousePointer = vbHourglass
mvarEuro = objGen.ReplaceStr(mvarEuro, ".", ",", 1)

If IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
  strFiltroFechas = " BETWEEN " & _
           "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
           "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
End If
If IsDate(dtcinicio.Date) And Not IsDate(dtcfin.Date) Then
  strFiltroFechas = " AND TRUNC(FR1300.FR13FECPETICION) BETWEEN " & _
           "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
           "TO_DATE('31/12/9999','DD/MM/YYYY')"
End If
If Not IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
  strFiltroFechas = " AND TRUNC(FR1300.FR13FECPETICION) BETWEEN " & _
           "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
           "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
End If
If Not IsDate(dtcinicio.Date) And Not IsDate(dtcfin.Date) Then
  strFiltroFechas = " BETWEEN " & _
           "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
           "TO_DATE('31/12/9999','DD/MM/YYYY')"
End If
'PRESUPUESTOS
If chkVerPresup.Value = Checked Then
  stra = "SELECT SUM(FRH1CANTIDADPTAS) CANTIDADPTAS,"
  stra = stra & "SUM(FRH1CANTIDADEURO) CANTIDADEURO,FRH1MES "
  stra = stra & " FROM FRH100 "
  stra = stra & " WHERE FRG9EJERCICIO = " & Val(cboDBCombo1(0).Text)
  If ComboCuentas.Text <> "Todas las Cuentas" Then
    stra = stra & " AND FR41CODGRUPPROD=" & "'" & ComboCuentas.Value & "'"
  End If
  stra = stra & " GROUP BY FRH1MES "
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  
  intmes = 0

  For i = 1 To 12
        gGrafico1(1, i) = 0
        gGrafico1(2, i) = 0
        SSDBGrid1.row = 0
        SSDBGrid1.Columns(i - 1).Value = 0
        SSDBGrid1.Columns(12).Value = 0
        SSDBGrid1.Update
  Next i
  
  While rsta.EOF = False
      intmes = Left(rsta("FRH1MES").Value, InStr(1, rsta("FRH1MES").Value, ".", 1) - 1)
      intmes = ((12 - mintMes + intmes) Mod 12) + 1
        SSDBGrid1.row = 0
        If IsNull(rsta("CANTIDADPTAS").Value) Then
            gGrafico1(1, intmes) = 0
            SSDBGrid1.Columns(intmes - 1).Value = 0
        Else
            gGrafico1(1, intmes) = rsta("CANTIDADPTAS").Value / 10000
            SSDBGrid1.Columns(intmes - 1).Value = rsta("CANTIDADPTAS").Value / 10000
        End If
        If IsNull(rsta("CANTIDADEURO").Value) Then
            gGrafico1(2, intmes) = 0
        Else
            gGrafico1(2, intmes) = rsta("CANTIDADEURO").Value / 10000
        End If
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing

  For i = 1 To 12
      Call MSChart1.datagrid.SetData(i, 1, Fix(gGrafico1(gintMoneda + 1, i)), 0)
      SSDBGrid1.Columns(12).Value = CDec(SSDBGrid1.Columns(12).Value) + _
                                    CDec(SSDBGrid1.Columns(i - 1).Value)
  Next i
  
  
Else 'chkVerPresup.Value = Unchecked
  For i = 1 To 12
        gGrafico1(1, i) = 0
        gGrafico1(2, i) = 0
        SSDBGrid1.row = 0
        SSDBGrid1.Columns(i - 1).Value = 0
        SSDBGrid1.Columns(12).Value = 0
        SSDBGrid1.Update
  Next i
  For i = 1 To 12
    Call MSChart1.datagrid.SetData(i, 1, 0, 0)
  Next i
End If

'COMPRAS
If chkVerCompras.Value = Checked Then

  strDelete = "DELETE FRL200"
  objApp.rdoConnect.Execute strDelete, 64
  objApp.rdoConnect.Execute "COMMIT"
  
  stra = "INSERT INTO FRL200 (FRL2CANT,FRL2VALOR,FRL2MES) "
  stra = stra & "(SELECT CEIL(SUM(FRJ300.FRJ3IMPORLINEA)) IP , "
  stra = stra & "       CEIL(SUM(FRJ300.FRJ3IMPORLINEA) / " & Fix(mvarEuro) & ") IE ,"
  stra = stra & "       TO_CHAR(FRJ100.FRJ1FECHAALBAR,'MM') MES "
  stra = stra & "  FROM FRJ100,FRJ300,FR7300 "
  stra = stra & " WHERE FRJ100.FRJ1CODALBARAN=FRJ300.FRJ1CODALBARAN "
  stra = stra & "   AND FRJ100.FRJ1FECHAALBAR >= to_date('1/" & mintMes & "/" & Val(cboDBCombo1(0).Text) & "','dd/mm/yyyy')"
  stra = stra & "   AND FRJ100.FRJ1FECHAALBAR < to_date('1/" & mintMes & "/" & Val(cboDBCombo1(0).Text) + 1 & "','dd/mm/yyyy')"
  stra = stra & "   AND FR7300.FR73CODPRODUCTO=FRJ300.FR73CODPRODUCTO "
  If ComboCuentas.Text <> "Todas las Cuentas" Then
    stra = stra & "   AND FR7300.FR73CUENCONT=" & "'" & ComboCuentas.Value & "'"
  End If
  stra = stra & " GROUP BY TO_CHAR(FRJ100.FRJ1FECHAALBAR,'MM'))"
  objApp.rdoConnect.Execute stra, 64
  objApp.rdoConnect.Execute "COMMIT"

  stra = "SELECT FRL2CANT,FRL2VALOR,FRL2MES FROM FRL200"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  
  intmes = 0

  For i = 1 To 12
        gGrafico2(1, i) = 0
        gGrafico2(2, i) = 0
        SSDBGrid1.row = 1
        SSDBGrid1.Columns(i - 1).Value = 0
        SSDBGrid1.Columns(12).Value = 0
        SSDBGrid1.Update
  Next i
  While rsta.EOF = False
      intmes = rsta("FRL2MES").Value
      intmes = ((12 - mintMes + intmes) Mod 12) + 1
        If IsNull(rsta("FRL2CANT").Value) Then
            gGrafico2(1, intmes) = 0
        Else
            gGrafico2(1, intmes) = rsta("FRL2CANT").Value / 10000
        End If
        SSDBGrid1.row = 1
        If IsNull(rsta("FRL2VALOR").Value) Then
            gGrafico2(2, intmes) = 0
            SSDBGrid1.Columns(intmes - 1).Value = 0
        Else
            gGrafico2(2, intmes) = rsta("FRL2VALOR").Value / 10000
            SSDBGrid1.Columns(intmes - 1).Value = rsta("FRL2CANT").Value / 10000
        End If
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing

  For i = 1 To 12
      Call MSChart1.datagrid.SetData(i, 2, Fix(gGrafico2(gintMoneda + 1, i)), 0)
      SSDBGrid1.Columns(12).Value = CDec(SSDBGrid1.Columns(12).Value) + _
                                    CDec(SSDBGrid1.Columns(i - 1).Value)
  Next i
 
Else 'chkVerCompras.Value = UnChecked
    For i = 1 To 12
        gGrafico2(1, i) = 0
        gGrafico2(2, i) = 0
        SSDBGrid1.row = 1
        SSDBGrid1.Columns(i - 1).Value = 0
        SSDBGrid1.Columns(12).Value = 0
        SSDBGrid1.Update
    Next i
    For i = 1 To 12
      Call MSChart1.datagrid.SetData(i, 2, 0, 0)
    Next i
End If

'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

'CONSUMOS
If chkConsumos.Value = Checked Then

  stra = "SELECT (SUM(DECODE(FR8000.FR04CODALMACEN_DES||FR8000.FR90CODTIPMOV,"
  stra = stra & "0||2,-FR8000.FR80UNISALEN* FR7300.FR73PRECMED ,0||"
  stra = stra & "4,-FR8000.FR80UNISALEN* FR7300.FR73PRECMED ,FR8000.FR80UNISALEN* FR7300.FR73PRECMED ))) CONSUMO,"
  stra = stra & "TO_CHAR(FR8000.FR80FECMOVIMIENTO,'MM') MES"
    stra = stra & " FROM FR8000,FR7300 "
    stra = stra & " WHERE FR7300.FR73CODPRODUCTO=FR8000.FR73CODPRODUCTO"
  If ComboCuentas.Text <> "Todas las Cuentas" Then
    stra = stra & "   AND FR7300.FR73CUENCONT=" & "'" & ComboCuentas.Value & "'"
  End If
  If ComboDepartamentos.Text <> "Todos los Departamentos" Then
    stra = stra & "   AND FR8000.FR04CODALMACEN_DES=" & "'" & ComboDepartamentos.Value & "'"
  End If
  stra = stra & " AND FR8000.FR80FECMOVIMIENTO >= to_date('1/" & mintMes & "/" & Val(cboDBCombo1(0).Text) & "','dd/mm/yyyy')"
  stra = stra & " AND FR8000.FR80FECMOVIMIENTO < to_date('1/" & mintMes & "/" & Val(cboDBCombo1(0).Text) + 1 & "','dd/mm/yyyy')"
  stra = stra & " AND ((FR8000.FR04CODALMACEN_ORI IN (0) AND FR8000.FR90CODTIPMOV IN (1,2,3,4,5,6,7,8,9))"
  stra = stra & "      OR (FR8000.FR04CODALMACEN_DES IN (0) AND FR8000.FR90CODTIPMOV IN (2,4)))"
  stra = stra & " GROUP BY TO_CHAR(FR8000.FR80FECMOVIMIENTO,'MM')"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
 
  intmes = 0

  For i = 1 To 12
        gGrafico3(1, i) = 0
        gGrafico3(2, i) = 0
        SSDBGrid1.row = 2
        SSDBGrid1.Columns(i - 1).Value = 0
        SSDBGrid1.Columns(12).Value = 0
        SSDBGrid1.Update
  Next i
  While rsta.EOF = False
      intmes = rsta("MES").Value
      intmes = ((12 - mintMes + intmes) Mod 12) + 1
        If IsNull(rsta("CONSUMO").Value) Then
            gGrafico3(1, intmes) = 0
        Else
            gGrafico3(1, intmes) = rsta("CONSUMO").Value / 10000
        End If
        SSDBGrid1.row = 2
        If IsNull(rsta("CONSUMO").Value) Then  'CONSUMO EUROS
            gGrafico3(2, intmes) = 0
            SSDBGrid1.Columns(intmes - 1).Value = 0
        Else
            gGrafico3(2, intmes) = rsta("CONSUMO").Value / 10000 'CONSUMO EUROS
            SSDBGrid1.Columns(intmes - 1).Value = rsta("CONSUMO").Value / 10000
        End If
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing

  For i = 1 To 12
      Call MSChart1.datagrid.SetData(i, 3, Fix(gGrafico3(gintMoneda + 1, i)), 0)
      SSDBGrid1.Columns(12).Value = CDec(SSDBGrid1.Columns(12).Value) + _
                                    CDec(SSDBGrid1.Columns(i - 1).Value)
  Next i
 
Else 'chkConsumos.Value = UnChecked
    For i = 1 To 12
        gGrafico3(1, i) = 0
        gGrafico3(2, i) = 0
        SSDBGrid1.row = 2
        SSDBGrid1.Columns(i - 1).Value = 0
        SSDBGrid1.Columns(12).Value = 0
        SSDBGrid1.Update
    Next i
    For i = 1 To 12
      Call MSChart1.datagrid.SetData(i, 3, 0, 0)
    Next i
End If
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Me.Enabled = True
Screen.MousePointer = vbDefault

End Sub

