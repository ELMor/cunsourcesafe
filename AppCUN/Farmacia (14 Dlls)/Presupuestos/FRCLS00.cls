VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

Const PRWinBusGrupos                  As String = "FR0076"
'Const PRWinDefGrpPresup               As String = "FR0047"
Const PRWinContrPresuFarm             As String = "FR0050"
Const PRWinEstPresuAnu                As String = "FR0063"
'Const PRWinEstPresuMes                As String = "FR0049"




Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case PRWinBusGrupos
      Load frmBuscarGrupos
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarGrupos.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBuscarGrupos
      Set frmBuscarGrupos = Nothing
      
    'Case PRWinDefGrpPresup
    '  Load frmDefGrpPresup
    '  'Call objsecurity.AddHelpContext(528)
    '  Call frmDefGrpPresup.Show(vbModal)
    '  'Call objsecurity.RemoveHelpContext
    '  Unload frmDefGrpPresup
    '  Set frmDefGrpPresup = Nothing
    
    'Case PRWinEstPresuMes
    '  Load frmEstPresuMes
    '  'Call objsecurity.AddHelpContext(528)
    '  Call frmEstPresuMes.Show(vbModal)
    '  'Call objsecurity.RemoveHelpContext
    '  Unload frmEstPresuMes
    '  Set frmEstPresuMes = Nothing
    
    Case PRWinEstPresuAnu
      Load frmEstPresuAnu
      'Call objsecurity.AddHelpContext(528)
      Call frmEstPresuAnu.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmEstPresuAnu
      Set frmEstPresuAnu = Nothing
      
    Case PRWinContrPresuFarm
      Load frmContrPresuFarm
      'Call objsecurity.AddHelpContext(528)
      Call frmContrPresuFarm.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmContrPresuFarm
      Set frmContrPresuFarm = Nothing
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 10, 1 To 4) As Variant
      
  aProcess(1, 1) = PRWinBusGrupos
  aProcess(1, 2) = "Buscador de Grupos"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow
  
  'aProcess(2, 1) = PRWinDefGrpPresup
  'aProcess(2, 2) = "Definir Grupos Presupuestarios"
  'aProcess(2, 3) = True
  'aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinContrPresuFarm
  aProcess(3, 2) = "Seguimiento del Gasto"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
  
  'aProcess(4, 1) = PRWinEstPresuMes
  'aProcess(4, 2) = "Establecer Presupuestos Mensuales"
  'aProcess(4, 3) = False
  'aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinEstPresuAnu
  aProcess(5, 2) = "Establecer Presupuestos Anuales"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
End Sub
