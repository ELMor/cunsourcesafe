VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmRevOMEC 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Revisar Ordenes M�dicas de Ensayo Cl�nico"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Ensayo Cl�nico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Index           =   2
      Left            =   120
      TabIndex        =   15
      Top             =   1680
      Width           =   11505
      Begin TabDlg.SSTab tabTab1 
         Height          =   1935
         Index           =   0
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   3413
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0233.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SSTab1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0233.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1665
            Index           =   2
            Left            =   -74880
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   120
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   2937
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   1695
            Index           =   0
            Left            =   120
            TabIndex        =   18
            Top             =   120
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   2990
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Datos Generales"
            TabPicture(0)   =   "FR0233.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(3)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(4)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(5)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "txtText1(4)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(5)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(6)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(7)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(8)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(9)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).ControlCount=   10
            TabCaption(1)   =   "Descripci�n"
            TabPicture(1)   =   "FR0233.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(11)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               DataField       =   "FR77DESENSAYOCLI"
               Height          =   1290
               Index           =   11
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   25
               Tag             =   "Descripci�n Ensayo Cl�nico"
               Top             =   360
               Width           =   9240
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   9
               Left            =   3960
               TabIndex        =   24
               Tag             =   "C�d. Servicio"
               Top             =   1200
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   8
               Left            =   4440
               TabIndex        =   23
               TabStop         =   0   'False
               Tag             =   "Descripci�n Servicio"
               Top             =   1200
               Width           =   4800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR77NOMENSAYOCLI"
               Height          =   330
               Index           =   7
               Left            =   1560
               TabIndex        =   22
               Tag             =   "Ensayo Cl�nico"
               Top             =   600
               Width           =   7560
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR77CODPROTEC"
               Height          =   330
               Index           =   6
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   21
               Tag             =   "C�digo Protocolo"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02CODMEDRESP"
               Height          =   330
               Index           =   5
               Left            =   120
               TabIndex        =   20
               Tag             =   "C�digo Doctor"
               Top             =   1200
               Width           =   1155
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   1320
               TabIndex        =   19
               Tag             =   "Apellido Doctor"
               Top             =   1200
               Width           =   2445
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   3960
               TabIndex        =   29
               Top             =   960
               Width           =   705
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Ensayo Cl�nico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   1560
               TabIndex        =   28
               Top             =   360
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cod. Protocolo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   120
               TabIndex        =   27
               Top             =   360
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Promotor"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   26
               Top             =   960
               Width           =   765
            End
         End
      End
   End
   Begin VB.CommandButton cmdValid 
      Caption         =   "REVISADA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   6
      Top             =   6360
      Width           =   1095
   End
   Begin VB.CommandButton cmdbuscarprod 
      Caption         =   "Modificar O.M.E.C."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   5
      Top             =   5040
      Width           =   1095
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Farmace�tico"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   11460
      Begin VB.TextBox txtApellido 
         Height          =   330
         Index           =   2
         Left            =   4800
         TabIndex        =   10
         Tag             =   "Apellido"
         Top             =   600
         Width           =   3285
      End
      Begin VB.TextBox txtPersona 
         Height          =   330
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Tag             =   "C�digo Persona"
         Top             =   600
         Width           =   1155
      End
      Begin VB.TextBox txtNombre 
         Height          =   330
         Index           =   1
         Left            =   1440
         TabIndex        =   7
         Tag             =   "Nombre"
         Top             =   600
         Width           =   3285
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecha 
         Height          =   330
         Index           =   0
         Left            =   8160
         TabIndex        =   13
         Tag             =   "Fecha Validaci�n"
         Top             =   600
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Fecha Validaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   8160
         TabIndex        =   14
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Apellido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   4800
         TabIndex        =   12
         Top             =   360
         Width           =   690
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   1440
         TabIndex        =   11
         Top             =   360
         Width           =   660
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Persona"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Ordenes M�dicas de Ensayo Cl�nico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3960
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   4080
      Width           =   10335
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3435
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   10065
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   17754
         _ExtentY        =   6059
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRevOMEC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRevOMEC (FR0130.FRM)                                 *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Validar Necesidades Quir�fano                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim fila As Integer

  If grdDBGrid1(0).SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar una linea."
    Exit Sub
  End If
  fila = grdDBGrid1(0).Row
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Call objWinInfo.DataSave
  End If
  
  cmdbuscarprod.Enabled = False
  
'  If grdDBGrid1(0).SelBookmarks.Count = 0 Then
'    MsgBox "Debe seleccionar una linea."
'  Else
    
    noinsertar = True
    Call objsecurity.LaunchProcess("FR0119")
    If gintprodtotal > 0 Then
      If gintprodtotal > 1 Then
        MsgBox "Debe traer s�lo 1 producto."
      Else
          'For v = 0 To gintprodtotal - 1
          'se mira que no se inserte 2 veces el mismo producto
          'If grdDBGrid1(0).Rows > 0 Then
          '  grdDBGrid1(0).MoveFirst
          '  For i = 0 To grdDBGrid1(0).Rows - 1
          '    If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
          '       And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
          '      'no se inserta el producto pues ya est� insertado en el grid
          '      noinsertar = False
          '      Exit For
          '    Else
          '      noinsertar = True
          '    End If
          '    grdDBGrid1(0).MoveNext
          '  Next i
          'End If
          'If noinsertar = True Then
            grdDBGrid1(0).SelBookmarks.RemoveAll
            grdDBGrid1(0).Row = fila
            grdDBGrid1(0).Col = 5
            grdDBGrid1(0).Columns(5).Value = ""
            SendKeys gintprodbuscado(v, 0)
            Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(6), gintprodbuscado(v, 1))
            Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(7), gintprodbuscado(v, 2))
          'Else
          '  mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          '  Chr(13), vbInformation)
          'End If
          'noinsertar = True
        'Next v
      End If
    End If
    gintprodtotal = 0
'  End If
  cmdbuscarprod.Enabled = True

End Sub

Private Sub cmdValid_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strb As String
Dim rstb As rdoResultset
Dim strc As String
Dim rstc As rdoResultset
Dim strInsert As String
Dim strupdate As String
Dim CodNecesVal As Variant

  cmdValid.Enabled = False
  
  stra = "SELECT FR55CODNECESUNID FROM FR5500 WHERE FR26CODESTPETIC=1"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    strb = "SELECT * FROM FR2000 WHERE FR55CODNECESUNID=" & rsta(0).Value
    Set rstb = objApp.rdoConnect.OpenResultset(strb)
    While Not rstb.EOF
      strc = "SELECT FR19CODNECESVAL_SEQUENCE.nextval FROM dual"
      Set rstc = objApp.rdoConnect.OpenResultset(strc)
      CodNecesVal = rstc(0).Value
      rstc.Close
      Set rstc = Nothing
      
      strInsert = "INSERT INTO FR1900 "
      strInsert = strInsert & "(FR20NUMLINEA,FR55CODNECESUNID"
      strInsert = strInsert & ",FR19CODNECESVAL,FR73CODPRODUCTO"
      strInsert = strInsert & ",FR93CODUNIMEDIDA,FR19CANTNECESQUIR"
      strInsert = strInsert & ",SG02COD_VAL,FR19FECVALIDACION"
      strInsert = strInsert & ",FR93CODUNIMEDIDA_SUM,FR19CANTSUMIFARM)"
      strInsert = strInsert & " VALUES (" & rstb("FR20NUMLINEA").Value
      strInsert = strInsert & "," & rstb("FR55CODNECESUNID").Value
      strInsert = strInsert & "," & CodNecesVal
      strInsert = strInsert & "," & rstb("FR73CODPRODUCTO").Value
      strInsert = strInsert & "," & "'" & rstb("FR93CODUNIMEDIDA").Value & "'"
      strInsert = strInsert & "," & rstb("FR20CANTNECESQUIR").Value
      strInsert = strInsert & ",'" & objsecurity.strUser & "'"
      strInsert = strInsert & ",TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')"
      strInsert = strInsert & "," & "'" & rstb("FR93CODUNIMEDIDA").Value & "'"
      strInsert = strInsert & ",0"
      strInsert = strInsert & ")"
      objApp.rdoConnect.Execute strInsert, 64
      
      strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=4 WHERE FR55CODNECESUNID=" & rsta(0).Value 'VALIDADA
      objApp.rdoConnect.Execute strupdate, 64
      
      rstb.MoveNext
    Wend
    rstb.Close
    Set rstb = Nothing
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  
  Call objWinInfo.DataRefresh
  
  cmdValid.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMultiInfo As New clsCWForm
Dim objMasterInfo As New clsCWForm
Dim strKey As String
Dim stra As String
Dim rsta As rdoResultset
  
    'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin

  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                  cwWithAll)
  
  With objMasterInfo
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "E.C."
      
    .strTable = "FR7700"
    
    '.blnAskPrimary = False
    
    Call .FormAddOrderField("FR77NOMENSAYOCLI", cwAscending)
    
    .intAllowance = cwAllowReadOnly
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Protocolos")
    Call .FormAddFilterWhere(strKey, "FR77CODPROTEC", "C�d.Protocolo E.C.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR77NOMENSAYOCLI", "Ensayo Cl�nico", cwString)
    Call .FormAddFilterWhere(strKey, "SG02CODMEDRESP", "M�dico Responsable", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�d.Servicio", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR77FECINIVIG", "Inicio Vigencia", cwDate)
    'Call .FormAddFilterWhere(strKey, "FR77FECFINVIG", "Fin Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR77DESENSAYOCLI", "Descripci�n de protocolo E.C.", cwString)
    Call .FormAddFilterOrder(strKey, "FR77NOMENSAYOCLI", "Ensayo Cl�nico")

  End With
  
  With objMultiInfo
    .strName = "OMEC"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(2)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR5600"
    .intAllowance = cwAllowReadOnly
    .strWhere = "FR26CODESTPETIC=1 AND FR56CODPETOMEC IS NULL" 's�lo las OM/PRN sin firmar(estado=redactada)
    
    Call .FormAddOrderField("FR56CODPETOMEC", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR56CODPETOMEC", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR56FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR56FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR56INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR56INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR56INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR56INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR56INDMEDINF", "Medicaci�n Infantil?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
  End With

    With objWinInfo
      Call .FormAddInfo(objMasterInfo, cwFormDetail)
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo Petici�n", "FR56CODPETOMEC", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Estado", "FR26CODESTPETIC", cwNumeric, 1)
        Call .GridAddColumn(objMultiInfo, "Desc.Estado", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Fecha", "FR56FECREDACCION", cwDate)
        Call .GridAddColumn(objMultiInfo, "Hora", "FR56HORAREDACCI", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "C�digo Persona", "CI21CODPERSONA", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "C�d.M�dico", "SG02COD_MED", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Dr.", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Urgencia", "FR91CODURGENCIA", cwNumeric, 2)
        Call .GridAddColumn(objMultiInfo, "Urgencia", "", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
'FR77CODPROTEC Number(9)
        
        Call .FormCreateInfo(objMasterInfo)
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(5), "FR26DESESTADOPET")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(9), "CI22NUMHISTORIA")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(10), "CI22NOMBRE")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(11), "CI22PRIAPEL")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(12), "CI22SEGAPEL")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(13)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(13)), grdDBGrid1(1).Columns(14), "SG02APE1")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(15)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91DESURGENCIA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(15)), grdDBGrid1(1).Columns(16), "FR91CODURGENCIA")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), grdDBGrid1(1).Columns(18), "AD02DESDPTO")
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
    grdDBGrid1(1).Columns(4).Visible = False 'c�d.estado
    grdDBGrid1(1).Columns(5).Visible = False 'desc.estado
    grdDBGrid1(1).Columns(8).Visible = False 'c�d.paciente
    grdDBGrid1(1).Columns(13).Visible = False 'c�d.m�dico
    grdDBGrid1(1).Columns(15).Visible = False 'c�d.urgencia
    grdDBGrid1(1).Columns(17).Visible = False 'c�d.servicio
    
    grdDBGrid1(1).Columns(3).Width = 1000 'c�d petici�n
    grdDBGrid1(1).Columns(6).Width = 1200 'fecha
    grdDBGrid1(1).Columns(7).Width = 700 'hora
    grdDBGrid1(1).Columns(9).Width = 1000 'historia
    grdDBGrid1(1).Columns(10).Width = 1000 'nombre
    grdDBGrid1(1).Columns(11).Width = 1600 'apellido 1�
    grdDBGrid1(1).Columns(12).Width = 1600 'apellido 2�
    grdDBGrid1(1).Columns(14).Width = 1600 'doctor
    grdDBGrid1(1).Columns(16).Width = 800 'urgencia
    grdDBGrid1(1).Columns(18).Width = 750 'servicio
    
    txtPersona(0).Text = objsecurity.strUser
    stra = "SELECT SG02NOM,SG02APE1 FROM SG0200 WHERE SG02COD ='" & objsecurity.strUser & "'"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    txtNombre(1).Text = rsta(0).Value
    txtApellido(2).Text = rsta(1).Value
    rsta.Close
    Set rsta = Nothing
    
    stra = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    dtcFecha(0).Date = rsta(0).Value
    rsta.Close
    Set rsta = Nothing
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub




