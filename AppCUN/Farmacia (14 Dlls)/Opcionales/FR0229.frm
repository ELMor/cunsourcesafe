VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "comdlg32.ocx"
Begin VB.Form frmDefExpEnsayClinic 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Definici�n Ensayos Cl�nicos. Expedientes"
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "E.C."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1695
      Left            =   120
      TabIndex        =   9
      Top             =   480
      Width           =   11655
      Begin VB.TextBox txtProt1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   3
         Left            =   1440
         TabIndex        =   15
         Tag             =   "Apellido Doctor"
         Top             =   1200
         Width           =   2445
      End
      Begin VB.TextBox txtProt1 
         Height          =   330
         Index           =   2
         Left            =   240
         TabIndex        =   14
         Tag             =   "C�digo Doctor"
         Top             =   1200
         Width           =   1155
      End
      Begin VB.TextBox txtProt1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         Height          =   330
         Index           =   0
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   13
         Tag             =   "C�digo Protocolo"
         Top             =   600
         Width           =   612
      End
      Begin VB.TextBox txtProt1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   1
         Left            =   1680
         TabIndex        =   12
         Tag             =   "Descripci�n Protocolo|Descripci�n Protocolo"
         Top             =   600
         Width           =   7560
      End
      Begin VB.TextBox txtProt1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   5
         Left            =   4680
         TabIndex        =   11
         TabStop         =   0   'False
         Tag             =   "Descripci�n Dpto. Propietario"
         Top             =   1200
         Width           =   4800
      End
      Begin VB.TextBox txtProt1 
         Height          =   330
         Index           =   4
         Left            =   4200
         TabIndex        =   10
         Tag             =   "C�d. Dpto. Propietario"
         Top             =   1200
         Width           =   372
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Promotor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   240
         TabIndex        =   19
         Top             =   960
         Width           =   765
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d. Protocolo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   18
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Ensayo Cl�nico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   1680
         TabIndex        =   17
         Top             =   360
         Width           =   2415
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   4200
         TabIndex        =   16
         Top             =   960
         Width           =   705
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Documentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   5160
      Width           =   11625
      Begin TabDlg.SSTab tabTab1 
         Height          =   2175
         Index           =   1
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   3836
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0229.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(4)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(18)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "CommonDialog1"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtcDateCombo1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtcDateCombo1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(7)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(8)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cmdExplorador"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(4)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0229.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR76CODPROTEXP"
            Height          =   330
            Index           =   4
            Left            =   5880
            Locked          =   -1  'True
            TabIndex        =   41
            Tag             =   "C�digo Expediente"
            Top             =   1080
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR38PATH"
            Height          =   330
            Index           =   2
            Left            =   120
            TabIndex        =   38
            Tag             =   "Archivo Asociado"
            Top             =   1680
            Width           =   5325
         End
         Begin VB.CommandButton cmdExplorador 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "MS Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   5520
            TabIndex        =   37
            Top             =   1680
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR38DESEXPEDIENTE"
            Height          =   330
            Index           =   8
            Left            =   1560
            TabIndex        =   29
            Tag             =   "Descripci�n Documento"
            Top             =   360
            Width           =   7560
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR38CODEXPEDIENTE"
            Height          =   330
            Index           =   7
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   28
            Tag             =   "C�digo Documento"
            Top             =   360
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2145
            Index           =   0
            Left            =   -74880
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   120
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   3784
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR38FECFINVIGEXP"
            Height          =   330
            Index           =   2
            Left            =   2280
            TabIndex        =   30
            Tag             =   "Fecha Fin Vigencia"
            Top             =   960
            Width           =   1620
            _Version        =   65537
            _ExtentX        =   2857
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR38FECINIVIGEXP"
            Height          =   330
            Index           =   3
            Left            =   120
            TabIndex        =   31
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   960
            Width           =   1620
            _Version        =   65537
            _ExtentX        =   2857
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2025
            Index           =   1
            Left            =   -74880
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   120
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   3572
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComDlg.CommonDialog CommonDialog1 
            Left            =   6120
            Top             =   1560
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   327681
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cod. Expediente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   5880
            TabIndex        =   42
            Top             =   840
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Archivo Asociado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   120
            TabIndex        =   39
            Top             =   1440
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Documento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   1560
            TabIndex        =   35
            Top             =   120
            Width           =   2415
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Doc."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   34
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   2280
            TabIndex        =   33
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   120
            TabIndex        =   32
            Top             =   720
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Expediente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   2160
      Width           =   11625
      Begin TabDlg.SSTab tabTab1 
         Height          =   2535
         Index           =   0
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   4471
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0229.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(24)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(23)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(22)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(3)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0229.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR77CODPROTEC"
            Height          =   330
            Index           =   3
            Left            =   6240
            Locked          =   -1  'True
            TabIndex        =   40
            Tag             =   "C�digo Protocolo"
            Top             =   360
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR76OBSERV"
            Height          =   1410
            Index           =   1
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   26
            Tag             =   "Descripci�n Expediente"
            Top             =   960
            Width           =   10680
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR76CODPROTEXP"
            Height          =   330
            Index           =   0
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   20
            Tag             =   "C�digo Expediente"
            Top             =   360
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2145
            Index           =   2
            Left            =   -74880
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   120
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   3784
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR76FECFINVIG"
            Height          =   330
            Index           =   1
            Left            =   3960
            TabIndex        =   21
            Tag             =   "Fecha Fin Vigencia"
            Top             =   360
            Width           =   1620
            _Version        =   65537
            _ExtentX        =   2857
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR76FECINIVIG"
            Height          =   330
            Index           =   0
            Left            =   1800
            TabIndex        =   22
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   360
            Width           =   1620
            _Version        =   65537
            _ExtentX        =   2857
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Expediente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   27
            Top             =   720
            Width           =   2025
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Expediente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   120
            TabIndex        =   25
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   3960
            TabIndex        =   24
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   24
            Left            =   1800
            TabIndex        =   23
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "V�a"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   26
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   975
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefExpEnsayClinic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefExpEnsayClinic (FR0229.FRM)                            *
'* AUTOR: JUAN RODR�GUEZ CORRAL                                         *
'* FECHA: ABRIL DE 1999                                                 *
'* DESCRIPCION: definir expedientes ensayos cl�nicos                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  
  Call objWinInfo.CtrlDataChange
  
End Sub






Private Sub cmdExplorador_Click()
Dim intRetVal
Dim strOpenFileName As String
Dim pathFileName As String
Dim finpath As Integer

cmdExplorador.Enabled = False
    On Error Resume Next
    pathFileName = Dir(txtText1(2).Text)
    finpath = InStr(1, txtText1(2).Text, pathFileName, 1)
    pathFileName = Left(txtText1(2).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(2).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If ERR.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(2).SetFocus
        Call objWinInfo.CtrlSet(txtText1(2), "")
        txtText1(2).SetFocus
        Call objWinInfo.CtrlSet(txtText1(2), strOpenFileName)
    End If
cmdExplorador.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm
Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Protocolo E.C.-Expediente"
      
    .strTable = "FR7600"
    
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR76CODPROTEXP", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Protocolo E.C.-Expediente")
    Call .FormAddFilterWhere(strKey, "FR76CODPROTEXP", "C�digo Expediente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR76FECINIVIG", "Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR76FECFINVIG", "Fin Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR76OBSERV", "Observaciones PECE", cwString)
    

  End With

  With objDetailInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    
    .strName = "Expedientes"
    
    .strTable = "FR3800"
    
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR38DESEXPEDIENTE", cwAscending)
    Call .FormAddRelation("FR76CODPROTEXP", txtText1(0))
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Expedientes")
    Call .FormAddFilterWhere(strKey, "FR38CODEXPEDIENTE", "C�digo Expediente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR38DESEXPEDIENTE", "Descripci�n Expediente", cwString)
    Call .FormAddFilterWhere(strKey, "FR38PATH", "Ubicaci�n Objeto OLE", cwString)
    Call .FormAddFilterWhere(strKey, "FR38FECINIVIGEXP", "Fecha Inicio Vigencia Exp", cwDate)
    Call .FormAddFilterWhere(strKey, "FR38FECFINVIGEXP", "Fecha Fin Vigencia Exp", cwDate)
    Call .FormAddFilterOrder(strKey, "FR38DESEXPEDIENTE", "Num.Linea")

  End With
  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
  
    Call .FormCreateInfo(objMasterInfo)
    
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

txtProt1(0).Text = frmDefEnsayClinic.txtText1(0).Text
txtProt1(1).Text = frmDefEnsayClinic.txtText1(1).Text
txtProt1(2).Text = frmDefEnsayClinic.txtText1(13).Text
txtProt1(3).Text = frmDefEnsayClinic.txtText1(14).Text
txtProt1(4).Text = frmDefEnsayClinic.txtText1(2).Text
txtProt1(5).Text = frmDefEnsayClinic.txtText1(3).Text

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch

 If strFormName = "Protocolo E.C.-Expediente" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�d. Dpto. Propietario"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto. Propietario"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim linea As Integer
Dim rstfec As rdoResultset
Dim strfec As String
Dim rstcontrolOP As rdoResultset
Dim strcontrolOP As String

  Select Case btnButton.Index
  Case 2 'Nuevo
    Select Case objWinInfo.objWinActiveForm.strName
    Case "Protocolo E.C.-Expediente"
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      Call objWinInfo.CtrlSet(txtText1(3), txtProt1(0).Text)
      sqlstr = "SELECT FR76CODPROTEXP_SEQUENCE.nextval FROM dual"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
      rsta.Close
      Set rsta = Nothing
      strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
      Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
      Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstfec.rdoColumns(0).Value)
      rstfec.Close
      Set rstfec = Nothing
      Exit Sub
    Case "Expedientes"
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      Call objWinInfo.CtrlSet(txtText1(4), txtText1(0).Text)
      sqlstr = "SELECT FR38CODEXPEDIENTE_SEQUENCE.nextval FROM dual"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      Call objWinInfo.CtrlSet(txtText1(7), rsta.rdoColumns(0).Value)
      rsta.Close
      Set rsta = Nothing
      
    End Select
  Case Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End Select
  
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(18)) 'Filtro
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(19)) 'No Filtro
  End Select

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(16)) 'Localizar
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  Case 50
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22)) 'Anterior
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23)) 'Siguiente
  Case 70
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24)) 'Ultimo
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26)) 'Refrescar
  Case Else
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub


Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
