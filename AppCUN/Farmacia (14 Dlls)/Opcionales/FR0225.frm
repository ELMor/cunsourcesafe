VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDefEnsayClinic 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Definici�n Ensayos Cl�nicos"
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmd2 
      Caption         =   "Expedientes Asociados"
      Height          =   495
      Left            =   10560
      TabIndex        =   134
      Top             =   1680
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Index           =   0
      Left            =   10320
      TabIndex        =   100
      Top             =   3480
      Width           =   1575
      Begin VB.CommandButton cmd1 
         Caption         =   "Medicamento Simple"
         Height          =   495
         Index           =   0
         Left            =   120
         TabIndex        =   103
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmd1 
         Caption         =   "Mezcla"
         Height          =   495
         Index           =   1
         Left            =   120
         TabIndex        =   102
         Top             =   840
         Width           =   1335
      End
      Begin VB.CommandButton cmd1 
         Caption         =   "A�adir a la Mezcla"
         Height          =   495
         Index           =   2
         Left            =   120
         TabIndex        =   101
         Top             =   1440
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Index           =   1
      Left            =   10320
      TabIndex        =   96
      Top             =   5760
      Width           =   1575
      Begin VB.CommandButton cmdbuscarprod 
         Caption         =   "Medicamentos"
         Height          =   495
         Left            =   120
         TabIndex        =   99
         Top             =   1440
         Width           =   1335
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Medicamento Grup. Tera."
         Height          =   495
         Index           =   0
         Left            =   120
         TabIndex        =   98
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Prod. Sani. Grup. Tera."
         Height          =   495
         Index           =   1
         Left            =   120
         TabIndex        =   97
         Top             =   840
         Width           =   1335
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Periodicidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   2400
      TabIndex        =   34
      Top             =   4680
      Width           =   6615
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   7
         Left            =   4080
         TabIndex        =   65
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   6
         Left            =   3480
         TabIndex        =   64
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   5
         Left            =   2910
         TabIndex        =   63
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   4
         Left            =   2280
         TabIndex        =   62
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   3
         Left            =   1680
         TabIndex        =   61
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   2
         Left            =   1080
         TabIndex        =   60
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   59
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   14
         Left            =   2310
         TabIndex        =   58
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   13
         Left            =   1710
         TabIndex        =   57
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   12
         Left            =   1110
         TabIndex        =   56
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   11
         Left            =   510
         TabIndex        =   55
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   10
         Left            =   5910
         TabIndex        =   54
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   9
         Left            =   5310
         TabIndex        =   53
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   8
         Left            =   4710
         TabIndex        =   52
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   21
         Left            =   510
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   20
         Left            =   5910
         TabIndex        =   50
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   19
         Left            =   5310
         TabIndex        =   49
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   18
         Left            =   4710
         TabIndex        =   48
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   17
         Left            =   4110
         TabIndex        =   47
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   16
         Left            =   3510
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   15
         Left            =   2910
         TabIndex        =   45
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   28
         Left            =   4710
         TabIndex        =   44
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   27
         Left            =   4110
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   26
         Left            =   3510
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   25
         Left            =   2910
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   24
         Left            =   2310
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   23
         Left            =   1710
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   22
         Left            =   1110
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   30
         Left            =   5910
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   29
         Left            =   5310
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CommandButton cmdAceptarPer 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   2640
         TabIndex        =   35
         Top             =   2280
         Width           =   1455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   480
         TabIndex        =   95
         Top             =   480
         Width           =   120
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   4110
         TabIndex        =   94
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   3510
         TabIndex        =   93
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   2910
         TabIndex        =   92
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   2310
         TabIndex        =   91
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   11
         Left            =   1710
         TabIndex        =   90
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   1110
         TabIndex        =   89
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "14"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   2310
         TabIndex        =   88
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "13"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   1710
         TabIndex        =   87
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "12"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   1110
         TabIndex        =   86
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "11"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   510
         TabIndex        =   85
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "10"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   5910
         TabIndex        =   84
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   19
         Left            =   5310
         TabIndex        =   83
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   20
         Left            =   4710
         TabIndex        =   82
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "21"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   21
         Left            =   510
         TabIndex        =   81
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "20"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   22
         Left            =   5910
         TabIndex        =   80
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "19"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   5310
         TabIndex        =   79
         Top             =   1080
         Width           =   225
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "18"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   24
         Left            =   4710
         TabIndex        =   78
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "17"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   25
         Left            =   4110
         TabIndex        =   77
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "16"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   26
         Left            =   3510
         TabIndex        =   76
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "15"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   27
         Left            =   2910
         TabIndex        =   75
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "28"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   28
         Left            =   4710
         TabIndex        =   74
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "27"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   29
         Left            =   4110
         TabIndex        =   73
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "26"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   30
         Left            =   3510
         TabIndex        =   72
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "25"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   31
         Left            =   2910
         TabIndex        =   71
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "24"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   32
         Left            =   2310
         TabIndex        =   70
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "23"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   33
         Left            =   1710
         TabIndex        =   69
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "22"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   34
         Left            =   1110
         TabIndex        =   68
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "30"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   35
         Left            =   5910
         TabIndex        =   67
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "29"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   36
         Left            =   5310
         TabIndex        =   66
         Top             =   1680
         Width           =   255
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Ensayo Cl�nico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   1
      Left            =   0
      TabIndex        =   27
      Top             =   480
      Width           =   10305
      Begin TabDlg.SSTab tabTab1 
         Height          =   2535
         Index           =   0
         Left            =   120
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   360
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   4471
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0225.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SSTab1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0225.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2145
            Index           =   2
            Left            =   -74880
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   3784
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   2295
            Index           =   0
            Left            =   120
            TabIndex        =   104
            Top             =   120
            Width           =   9495
            _ExtentX        =   16748
            _ExtentY        =   4048
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Datos Generales"
            TabPicture(0)   =   "FR0225.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(0)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(21)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(22)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(23)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(24)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(20)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "dtcDateCombo1(0)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "dtcDateCombo1(1)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(2)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(3)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(1)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(0)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(13)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(14)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).ControlCount=   14
            TabCaption(1)   =   "Descripci�n"
            TabPicture(1)   =   "FR0225.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(11)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   14
               Left            =   1320
               TabIndex        =   3
               Tag             =   "Apellido Doctor"
               Top             =   1200
               Width           =   2445
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02CODMEDRESP"
               Height          =   330
               Index           =   13
               Left            =   120
               TabIndex        =   2
               Tag             =   "C�digo Doctor"
               Top             =   1200
               Width           =   1155
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR77DESENSAYOCLI"
               Height          =   1650
               Index           =   11
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   8
               Tag             =   "Indicaciones del Protocolo"
               Top             =   480
               Width           =   9240
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR77CODPROTEC"
               Height          =   330
               Index           =   0
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   0
               Tag             =   "C�digo Protocolo E.C"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR77NOMENSAYOCLI"
               Height          =   330
               Index           =   1
               Left            =   1560
               TabIndex        =   1
               Tag             =   "Nombre Protocolo E.C"
               Top             =   600
               Width           =   7560
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   3
               Left            =   4440
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Descripci�n Servicio"
               Top             =   1200
               Width           =   4800
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   2
               Left            =   3960
               TabIndex        =   4
               Tag             =   "C�d. Servicio"
               Top             =   1200
               Width           =   372
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR77FECFINVIG"
               Height          =   330
               Index           =   1
               Left            =   2280
               TabIndex        =   7
               Tag             =   "Fecha Fin Vigencia"
               Top             =   1800
               Width           =   1620
               _Version        =   65537
               _ExtentX        =   2857
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR77FECINIVIG"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   6
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1800
               Width           =   1620
               _Version        =   65537
               _ExtentX        =   2857
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Promotor"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   20
               Left            =   120
               TabIndex        =   135
               Top             =   960
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Inicio Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Index           =   24
               Left            =   120
               TabIndex        =   109
               Top             =   1560
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fin Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   2280
               TabIndex        =   108
               Top             =   1560
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cod. Protocolo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   120
               TabIndex        =   107
               Top             =   360
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Ensayo Cl�nico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   21
               Left            =   1560
               TabIndex        =   106
               Top             =   360
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   3960
               TabIndex        =   105
               Top             =   960
               Width           =   705
            End
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4440
      Index           =   0
      Left            =   0
      TabIndex        =   25
      Top             =   3480
      Width           =   10215
      Begin TabDlg.SSTab tabTab1 
         Height          =   3975
         Index           =   1
         Left            =   120
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   7011
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0225.frx":0070
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SSTab1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0225.frx":008C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(6)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3705
            Index           =   6
            Left            =   -74880
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   120
            Width           =   9375
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            stylesets.count =   4
            stylesets(0).Name=   "blanco"
            stylesets(0).BackColor=   16777215
            stylesets(0).Picture=   "FR0225.frx":00A8
            stylesets(1).Name=   "grana"
            stylesets(1).BackColor=   14671839
            stylesets(1).Picture=   "FR0225.frx":00C4
            stylesets(2).Name=   "amarillo"
            stylesets(2).BackColor=   12566463
            stylesets(2).Picture=   "FR0225.frx":00E0
            stylesets(3).Name=   "blau"
            stylesets(3).BackColor=   11513775
            stylesets(3).Picture=   "FR0225.frx":00FC
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16536
            _ExtentY        =   6535
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   3735
            Index           =   1
            Left            =   120
            TabIndex        =   110
            Top             =   120
            Width           =   9375
            _ExtentX        =   16536
            _ExtentY        =   6588
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Datos Generales"
            TabPicture(0)   =   "FR0225.frx":0118
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(1)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(19)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(17)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(3)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(4)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(5)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(6)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(36)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(35)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(34)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(8)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(7)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "cboOperacion"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "cboSSDBCombo1(5)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "cboSSDBCombo1(4)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "cboSSDBCombo1(3)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "cboSSDBCombo1(2)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "cboSSDBCombo1(1)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "cboSSDBCombo1(0)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(4)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(5)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(6)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(29)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(7)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(30)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txtText1(28)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txtText1(43)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "txtText1(42)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).Control(29)=   "chkCheck1(1)"
            Tab(0).Control(29).Enabled=   0   'False
            Tab(0).Control(30)=   "txtText1(8)"
            Tab(0).Control(30).Enabled=   0   'False
            Tab(0).Control(31)=   "chkCheck1(2)"
            Tab(0).Control(31).Enabled=   0   'False
            Tab(0).Control(32)=   "chkCheck1(3)"
            Tab(0).Control(32).Enabled=   0   'False
            Tab(0).Control(33)=   "txtText1(9)"
            Tab(0).Control(33).Enabled=   0   'False
            Tab(0).Control(34)=   "txtText1(10)"
            Tab(0).Control(34).Enabled=   0   'False
            Tab(0).Control(35)=   "txtMinutos"
            Tab(0).Control(35).Enabled=   0   'False
            Tab(0).Control(36)=   "txtHoras"
            Tab(0).Control(36).Enabled=   0   'False
            Tab(0).Control(37)=   "chkCheck1(0)"
            Tab(0).Control(37).Enabled=   0   'False
            Tab(0).ControlCount=   38
            TabCaption(1)   =   "Indicaciones"
            TabPicture(1)   =   "FR0225.frx":0134
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(12)"
            Tab(1).ControlCount=   1
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Devolver Envase?"
               DataField       =   "FR31INDDEVENV"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   6840
               TabIndex        =   136
               Tag             =   "Devolver Envase?"
               Top             =   1800
               Width           =   1935
            End
            Begin VB.TextBox txtHoras 
               Alignment       =   1  'Right Justify
               Height          =   330
               Left            =   7920
               TabIndex        =   133
               Tag             =   "Tiempo Infusi�n(min)"
               Top             =   3120
               Width           =   375
            End
            Begin VB.TextBox txtMinutos 
               Alignment       =   1  'Right Justify
               Height          =   330
               Left            =   8400
               TabIndex        =   132
               Tag             =   "Tiempo Infusi�n(min)"
               Top             =   3120
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR31INDICACIONES"
               Height          =   3090
               Index           =   12
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   131
               Tag             =   "Indicaciones al Medicamento del Protocolo"
               Top             =   480
               Width           =   9240
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR31DIASADM"
               Height          =   330
               Index           =   10
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   20
               Tag             =   "D�as de administraci�n, separados por comas"
               Top             =   2520
               Width           =   6495
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR31NUMDIAS"
               Height          =   330
               Index           =   9
               Left            =   2400
               TabIndex        =   18
               Tag             =   "D�as"
               Top             =   1920
               Width           =   855
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Dosis por Superficie?"
               DataField       =   "FR31DOSISSUP"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   3720
               TabIndex        =   15
               Tag             =   "Dosis por Superficie Corporal?"
               Top             =   1440
               Width           =   2295
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Dosis por Peso?"
               DataField       =   "FR31DOSISPESO"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   3720
               TabIndex        =   14
               Tag             =   "Dosis por Peso?"
               Top             =   1200
               Width           =   1935
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   8
               Left            =   120
               TabIndex        =   21
               Tag             =   "Medicamento Diluyente"
               Top             =   3120
               Width           =   6360
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Indicador Via Opcional"
               DataField       =   "FR31INDVIAOPC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   6960
               TabIndex        =   115
               Tag             =   "Indicador Via Opcional?"
               Top             =   1080
               Visible         =   0   'False
               Width           =   2295
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR31CANTIDADDIL"
               Height          =   330
               Index           =   42
               Left            =   6840
               TabIndex        =   22
               Tag             =   "Volumen Diluci�n"
               Top             =   3120
               Width           =   732
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR31TIEMMININF"
               Height          =   330
               Index           =   43
               Left            =   8520
               TabIndex        =   23
               Tag             =   "Minutos Infusi�n"
               Top             =   2400
               Visible         =   0   'False
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR31CANTIDAD"
               Height          =   330
               Index           =   28
               Left            =   120
               TabIndex        =   11
               Tag             =   "Cantidad"
               Top             =   1320
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR31DOSIS"
               Height          =   330
               Index           =   30
               Left            =   1080
               TabIndex        =   12
               Tag             =   "Dosis"
               Top             =   1320
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR31NUMLINEA"
               Height          =   330
               Index           =   7
               Left            =   6960
               Locked          =   -1  'True
               TabIndex        =   114
               Tag             =   "Num. Linea"
               Top             =   2400
               Visible         =   0   'False
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR31OPERACION"
               Height          =   330
               Index           =   29
               Left            =   7680
               TabIndex        =   113
               Tag             =   "Operaci�n"
               Top             =   2400
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   6
               Left            =   6960
               Locked          =   -1  'True
               TabIndex        =   112
               Tag             =   "C�digo Medicamento"
               Top             =   2040
               Visible         =   0   'False
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   5
               Left            =   120
               TabIndex        =   9
               Tag             =   "Descripci�n Medicamento"
               Top             =   720
               Width           =   7560
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR77CODPROTEC"
               Height          =   330
               Index           =   4
               Left            =   7680
               Locked          =   -1  'True
               TabIndex        =   111
               Tag             =   "C�digo Protocolo"
               Top             =   2040
               Visible         =   0   'False
               Width           =   612
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "FRH7CODFORMFAR"
               Height          =   330
               Index           =   0
               Left            =   7920
               TabIndex        =   10
               Tag             =   "Forma Farmace�tica"
               Top             =   720
               Width           =   975
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   953
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   10610
               Columns(1).Caption=   "FORMA FARMACEUTICA"
               Columns(1).Name =   "URGENCIA"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1720
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   1
               Left            =   2040
               TabIndex        =   13
               Tag             =   "Unidad Medida"
               Top             =   1320
               Width           =   1215
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1482
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8890
               Columns(1).Caption=   "UNIDAD DE MEDIDA"
               Columns(1).Name =   "URGENCIA"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2143
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "FRG4CODFRECUENCIA"
               Height          =   330
               Index           =   2
               Left            =   3360
               TabIndex        =   19
               Tag             =   "Frecuencia"
               Top             =   1920
               Width           =   3015
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   7064
               Columns(1).Caption=   "FRECUENCIA"
               Columns(1).Name =   "URGENCIA"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5318
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "FR34CODVIA"
               Height          =   330
               Index           =   3
               Left            =   6480
               TabIndex        =   16
               Tag             =   "V�a"
               Top             =   1320
               Width           =   855
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   873
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   9948
               Columns(1).Caption=   "VIA"
               Columns(1).Name =   "URGENCIA"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1508
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "FRH5CODPERIODICIDAD"
               Height          =   330
               Index           =   4
               Left            =   120
               TabIndex        =   17
               Tag             =   "Periodicidad"
               Top             =   1920
               Width           =   2055
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2831
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   8784
               Columns(1).Caption=   "PERIODICIDAD"
               Columns(1).Name =   "URGENCIA"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3625
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "FR73CODPRODUCTO_DIL"
               Height          =   330
               Index           =   5
               Left            =   6480
               TabIndex        =   116
               Tag             =   "C�d. Medicamento para Diluir"
               Top             =   3120
               Width           =   255
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "COD"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6271
               Columns(1).Caption=   "PRODUCTO PARA DILUIR"
               Columns(1).Name =   "PRODUCTO PARA DILUIR"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2540
               Columns(2).Caption=   "VOLUMEN"
               Columns(2).Name =   "VOLUMEN"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Caption=   "PRESENTACI�N"
               Columns(3).Name =   "PRESENTACI�N"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   450
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBCombo cboOperacion 
               Height          =   330
               Left            =   7920
               TabIndex        =   117
               Tag             =   "Operaci�n"
               Top             =   2400
               Visible         =   0   'False
               Width           =   255
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RowHeight       =   423
               Columns(0).Width=   2170
               Columns(0).Caption=   "OPERACI�N"
               Columns(0).Name =   "OPERACI�N"
               Columns(0).Alignment=   1
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   450
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dias de Agenda"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   120
               TabIndex        =   130
               Top             =   2280
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "D�as"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   2400
               TabIndex        =   129
               Top             =   1680
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Medicamento para Diluir"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   34
               Left            =   120
               TabIndex        =   128
               Top             =   2880
               Width           =   2070
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   35
               Left            =   6840
               TabIndex        =   127
               Top             =   2880
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tiempo Infusi�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   36
               Left            =   7920
               TabIndex        =   126
               Top             =   2880
               Width           =   1365
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Periodicidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   120
               TabIndex        =   125
               Top             =   1680
               Width           =   1065
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "V�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   6600
               TabIndex        =   124
               Top             =   1080
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Frecuencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   3360
               TabIndex        =   123
               Top             =   1680
               Width           =   960
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Uni.Medida"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   2040
               TabIndex        =   122
               Top             =   1080
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   120
               TabIndex        =   121
               Top             =   1080
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   1080
               TabIndex        =   120
               Top             =   1080
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Medicamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   119
               Top             =   480
               Width           =   2205
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Forma Farma."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   7920
               TabIndex        =   118
               Top             =   480
               Width           =   1155
            End
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   24
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "V�a"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   26
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   975
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefEnsayClinic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefEnsayClinic (FR0225.FRM)                               *
'* AUTOR: JUAN RODR�GUEZ CORRAL                                         *
'* FECHA: ABRIL DE 1999                                                 *
'* DESCRIPCION: definir Ensayos Cl�nicos                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim strEstiloCelda1 As String
Dim strEstiloCelda2 As String
Dim nuevoDetalle As Boolean
Dim intCambioAlgo As Integer


Private Sub cboOperacion_CloseUp()

  'Call objWinInfo.CtrlSet(txtText1(29), cboOperacion.Value)

End Sub


Private Sub cmd2_Click()

cmd2.Enabled = False

If txtText1(0).Text <> "" Then
  Call objsecurity.LaunchProcess("FR0229")
End If

cmd2.Enabled = True

End Sub

Private Sub txtHoras_Change()
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
        If txtHoras.Text > 100 Then
            Beep
            txtHoras.Text = 100
        End If

        If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
            'txtText1(43).Text = (txtHoras * 60) + txtMinutos
            Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
            If intCambioAlgo = 1 Then
                objWinInfo.objWinActiveForm.blnChanged = True
                tlbToolbar1.Buttons(4).Enabled = True
            End If
        End If
    End If
End Sub

Private Sub txtHoras_KeyPress(KeyAscii As Integer)
    intCambioAlgo = 1
End Sub

Private Sub txtMinutos_Change()
    
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
        If txtMinutos.Text > 59 Then
            Beep
            txtMinutos.Text = 59
        End If
        If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
            'txtText1(43).Text = (txtHoras * 60) + txtMinutos
            Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
            If intCambioAlgo = 1 Then
                objWinInfo.objWinActiveForm.blnChanged = True
                tlbToolbar1.Buttons(4).Enabled = True
            End If
        End If
    End If
End Sub

Private Sub txtMinutos_KeyPress(KeyAscii As Integer)
    intCambioAlgo = 1
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
Dim strdias As String
Dim intpos As Integer
Dim i As Integer
  
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 4 And cboSSDBCombo1(4) = "Agenda" Then
    strdias = txtText1(10)
    For i = 1 To 30
      Check1(i).Value = 0
    Next i
    While strdias <> ""
      intpos = InStr(1, strdias, ",")
      If intpos <> 0 Then
        Check1(Val(Left(strdias, intpos - 1))).Value = 1
      Else
        Check1(strdias).Value = 1
      End If
      intpos = InStr(1, strdias, ",")
      If intpos <> 0 Then
        strdias = Right(strdias, Len(strdias) - intpos)
      Else
        strdias = ""
      End If
    Wend
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Frame2.Visible = True
    Call Bloquear2(txtText1(9), True, "textbox") 'FR31NUMDIAS
    Call objWinInfo.CtrlSet(txtText1(9), "")
  End If
  
  If intIndex = 4 And cboSSDBCombo1(4) <> "Agenda" Then
    Call Bloquear2(txtText1(9), False, "textbox") 'FR31NUMDIAS
    Call objWinInfo.CtrlSet(txtText1(9), "")
  End If
  
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  
  Call objWinInfo.CtrlDataChange
  
End Sub


Private Sub cmd1_Click(Index As Integer)
Dim stra As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim linea As Integer
Dim sqlstr As String
        
nuevoDetalle = True
  
  If txtText1(0).Text <> "" Then
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Select Case Index
    Case 0
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'sqlstr = "SELECT MAX(FR31NUMLINEA) FROM FR3100 WHERE FR77CODPROTEC=" & txtText1(0).Text
      sqlstr = "SELECT MAX(FR31NUMLINEA) FROM FR3100 WHERE FR77CODPROTEC=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
      qrya(0) = txtText1(0).Text
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If IsNull(rsta.rdoColumns(0).Value) Then
        linea = 1
      Else
        linea = rsta.rdoColumns(0).Value + 1
      End If
      Call objWinInfo.CtrlSet(txtText1(7), linea)
      Call objWinInfo.CtrlSet(txtText1(29), "/")
      rsta.Close
      Set rsta = Nothing
    Case 1
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'sqlstr = "SELECT MAX(FR31NUMLINEA) FROM FR3100 WHERE FR77CODPROTEC=" & txtText1(0).Text
      sqlstr = "SELECT MAX(FR31NUMLINEA) FROM FR3100 WHERE FR77CODPROTEC=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
      qrya(0) = txtText1(0).Text
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If IsNull(rsta.rdoColumns(0).Value) Then
        linea = 1
      Else
        linea = rsta.rdoColumns(0).Value + 1
      End If
      Call objWinInfo.CtrlSet(txtText1(7), linea)
      Call objWinInfo.CtrlSet(txtText1(29), "=")
      rsta.Close
      Set rsta = Nothing
    Case 2
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'sqlstr = "SELECT MAX(FR31NUMLINEA) FROM FR3100 WHERE FR77CODPROTEC=" & txtText1(0).Text
      sqlstr = "SELECT MAX(FR31NUMLINEA) FROM FR3100 WHERE FR77CODPROTEC=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
      qrya(0) = txtText1(0).Text
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If IsNull(rsta.rdoColumns(0).Value) Then
        linea = 1
      Else
        linea = rsta.rdoColumns(0).Value + 1
      End If
      rsta.Close
      Set rsta = Nothing
      
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT FR31OPERACION FROM FR3100 WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA < " & linea & " ORDER BY FR31NUMLINEA DESC"
      stra = "SELECT FR31OPERACION FROM FR3100 WHERE FR77CODPROTEC=? AND FR31NUMLINEA < ? ORDER BY FR31NUMLINEA DESC"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = txtText1(4).Text
      qrya(1) = linea
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        While rsta(0).Value = "+" And Not rsta.EOF
          rsta.MoveNext
        Wend
        If rsta.EOF Or rsta(0).Value = "/" Then
          MsgBox "Antes de a�adir Medicamentos a la Mezcla, hay que crearla.", vbExclamation
          'txtText1(29).Text = ""
          Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
          nuevoDetalle = False
          Exit Sub
        End If
      Else
        MsgBox "Antes de a�adir Medicamentos a la Mezcla, hay que crearla.", vbExclamation
        'txtText1(29).Text = ""
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
        nuevoDetalle = False
        Exit Sub
      End If
      rsta.Close
      Set rsta = Nothing
    
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
      
      Call objWinInfo.CtrlSet(txtText1(7), linea)
      Call objWinInfo.CtrlSet(txtText1(29), "+")
    
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT FRG4CODFRECUENCIA,FRH5CODPERIODICIDAD,"
      'stra = stra & "FR34CODVIA,"
      'stra = stra & "FR31DIASADM,FR31NUMDIAS"
      'stra = stra & " FROM FR3100 WHERE FR31OPERACION='=' AND FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA < " & linea & " ORDER BY FR31NUMLINEA DESC"
      stra = "SELECT FRG4CODFRECUENCIA,FRH5CODPERIODICIDAD,"
      stra = stra & "FR34CODVIA,"
      stra = stra & "FR31DIASADM,FR31NUMDIAS"
      stra = stra & " FROM FR3100 WHERE FR31OPERACION=?"
      stra = stra & " AND FR77CODPROTEC=? "
      stra = stra & " AND FR31NUMLINEA < ? "
      stra = stra & " ORDER BY FR31NUMLINEA DESC"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = "="
      qrya(1) = txtText1(4).Text
      qrya(2) = linea
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      Call objWinInfo.CtrlSet(cboSSDBCombo1(2), rsta("FRG4CODFRECUENCIA").Value)
      Call objWinInfo.CtrlSet(cboSSDBCombo1(4), rsta("FRH5CODPERIODICIDAD").Value)
      Call objWinInfo.CtrlSet(cboSSDBCombo1(3), rsta("FR34CODVIA").Value)
      Call objWinInfo.CtrlSet(txtText1(10), rsta("FR31DIASADM").Value)
      Call objWinInfo.CtrlSet(txtText1(9), rsta("FR31NUMDIAS").Value)
      rsta.Close
      Set rsta = Nothing
    
    End Select
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
  End If

nuevoDetalle = False
Call controlar_campos

End Sub

Private Sub cmdAceptarPer_Click()
Dim i As Integer
Dim strdias As String

  For i = 1 To 30
    If Check1(i).Value = 1 Then
      strdias = strdias & "," & i
    End If
  Next i
  If strdias <> "" Then
    strdias = Right(strdias, Len(strdias) - 1)
  End If
  Call objWinInfo.CtrlSet(txtText1(10), strdias)

  Frame2.Visible = False

  Call objWinInfo.FormChangeActive(fraframe1(0), False, True)

End Sub


Private Sub cmdbuscargruprod_Click(Index As Integer)
Dim insertarfila As Boolean
Dim v As Integer
Dim stra As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset

cmdbuscargruprod(Index).Enabled = False
If txtText1(0).Text <> "" Then
    
  If Index = 0 Then
    gstrllamador = "GrupoCGCF"
  Else
    gstrllamador = "GrupoQ"
  End If
  
  Call objWinInfo.DataSave
  Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
  Call objsecurity.LaunchProcess("FR0237")
  Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
  For v = 0 To gintprodtotal - 1
    Call objWinInfo.CtrlSet(txtText1(6), gintprodbuscado(v, 0))
    Call objWinInfo.CtrlSet(cboSSDBCombo1(1), gintprodbuscado(v, 4))
    Call objWinInfo.CtrlSet(txtText1(28), 1) 'FR31CANTIDAD
    If txtText1(29).Text <> "=" Then
      Call objWinInfo.CtrlSet(cboSSDBCombo1(5), "")
    End If
    Call objWinInfo.CtrlDataChange
    
    If txtText1(29).Text = "=" Then
      'buscar si tiene Prod.Inf.
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT FR73CODPRODUCTO_DIL,FR73VOLINFIV,FR73TIEMINF FROM FR7300 WHERE FR73CODPRODUCTO=" & gintprodbuscado(v, 0)
      stra = "SELECT FR73CODPRODUCTO_DIL,FR73VOLINFIV,FR73TIEMINF "
      stra = stra & "FROM FR7300 WHERE FR73CODPRODUCTO=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = gintprodbuscado(v, 0)
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Call objWinInfo.CtrlSet(cboSSDBCombo1(5), rsta("FR73CODPRODUCTO_DIL").Value)
        Call objWinInfo.CtrlSet(txtText1(42), rsta("FR73VOLINFIV").Value)
        Call objWinInfo.CtrlSet(txtText1(43), rsta("FR73TIEMINF").Value)
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  
    If txtText1(29).Text <> "+" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(6).Text
      stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = txtText1(6).Text
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Call objWinInfo.CtrlSet(chkCheck1(1), rsta("FR73INDVIAOPCION").Value)
        Call objWinInfo.CtrlSet(cboSSDBCombo1(3), rsta("FR34CODVIA").Value)
        Call objWinInfo.CtrlSet(cboSSDBCombo1(0), rsta("FRH7CODFORMFAR").Value)
        If rsta("FR73INDVIAOPCION").Value = 0 Then
          objWinInfo.CtrlGetInfo(cboSSDBCombo1(3)).blnReadOnly = True
          cboSSDBCombo1(3).Enabled = False
        Else
          objWinInfo.CtrlGetInfo(cboSSDBCombo1(3)).blnReadOnly = False
          cboSSDBCombo1(3).Enabled = True
          cboSSDBCombo1(3).BackColor = vbWhite
        End If
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  
    Call controlar_campos
    
    txtText1(30).SetFocus
    Call objWinInfo.CtrlGotFocus
  
  Next v
  gintprodtotal = 0
End If
cmdbuscargruprod(Index).Enabled = True


End Sub

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim stra As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset

cmdbuscarprod.Enabled = False
gstrllamador = "DefProtocolosProd"

If txtText1(0).Text <> "" Then
  Call objWinInfo.DataSave
  Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
  Call objsecurity.LaunchProcess("FR0238")
  Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
  For v = 0 To gintprodtotal - 1
    Call objWinInfo.CtrlSet(txtText1(6), gintprodbuscado(v, 0))
    Call objWinInfo.CtrlSet(cboSSDBCombo1(1), gintprodbuscado(v, 4))
    Call objWinInfo.CtrlSet(txtText1(28), 1) 'FR31CANTIDAD
    If txtText1(29).Text <> "=" Then
      Call objWinInfo.CtrlSet(cboSSDBCombo1(5), "")
    End If
    Call objWinInfo.CtrlDataChange
    
    If txtText1(29).Text = "=" Then
      'buscar si tiene Prod.Inf.
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT FR73CODPRODUCTO_DIL,FR73VOLINFIV,FR73TIEMINF FROM FR7300 WHERE FR73CODPRODUCTO=" & gintprodbuscado(v, 0)
      stra = "SELECT FR73CODPRODUCTO_DIL,FR73VOLINFIV,FR73TIEMINF FROM FR7300 WHERE FR73CODPRODUCTO=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = gintprodbuscado(v, 0)
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Call objWinInfo.CtrlSet(cboSSDBCombo1(5), rsta("FR73CODPRODUCTO_DIL").Value)
        Call objWinInfo.CtrlSet(txtText1(42), rsta("FR73VOLINFIV").Value)
        Call objWinInfo.CtrlSet(txtText1(43), rsta("FR73TIEMINF").Value)
      End If
      rsta.Close
      Set rsta = Nothing
    End If
    
    If txtText1(29).Text <> "+" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(6).Text
      stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = txtText1(6).Text
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Call objWinInfo.CtrlSet(chkCheck1(1), rsta("FR73INDVIAOPCION").Value)
        Call objWinInfo.CtrlSet(cboSSDBCombo1(3), rsta("FR34CODVIA").Value)
        Call objWinInfo.CtrlSet(cboSSDBCombo1(0), rsta("FRH7CODFORMFAR").Value)
        If rsta("FR73INDVIAOPCION").Value = 0 Then
          objWinInfo.CtrlGetInfo(cboSSDBCombo1(3)).blnReadOnly = True
          cboSSDBCombo1(3).Enabled = False
        Else
          objWinInfo.CtrlGetInfo(cboSSDBCombo1(3)).blnReadOnly = False
          cboSSDBCombo1(3).Enabled = True
          cboSSDBCombo1(3).BackColor = vbWhite
        End If
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  
    Call controlar_campos
    
    txtText1(30).SetFocus
    Call objWinInfo.CtrlGotFocus
  
  Next v
  gintprodtotal = 0
End If

gstrllamador = ""
cmdbuscarprod.Enabled = True

End Sub

Private Sub Form_Activate()

  cboOperacion.RemoveAll
  cboOperacion.AddItem "/"
  cboOperacion.AddItem "+"
  cboOperacion.AddItem "="

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm
Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "E.C."
      
    .strTable = "FR7700"
    
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR77NOMENSAYOCLI", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "E.C.")
    Call .FormAddFilterWhere(strKey, "FR77CODPROTEC", "C�d.Protocolo E.C.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR77NOMENSAYOCLI", "Ensayo Cl�nico", cwString)
    Call .FormAddFilterWhere(strKey, "SG02CODMEDRESP", "M�dico Responsable", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�d.Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR77FECINIVIG", "Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR77FECFINVIG", "Fin Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR77DESENSAYOCLI", "Descripci�n de protocolo E.C.", cwString)
    Call .FormAddFilterOrder(strKey, "FR77NOMENSAYOCLI", "Ensayo Cl�nico")

  End With

  With objDetailInfo
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(6)
    
    .strName = "Detalle Protocolo"
    
    .strTable = "FR3100"
    
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR31NUMLINEA", cwAscending)
    Call .FormAddRelation("FR77CODPROTEC", txtText1(0))
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Protocolo")
    Call .FormAddFilterWhere(strKey, "FR31NUMLINEA", "Num.Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR77CODPROTEC", "C�d.Protocolo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR31OPERACION", "operaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d. Medicamento", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmace�tica", cwString)
    Call .FormAddFilterWhere(strKey, "FR31CANTIDAD", "cantidad", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR31DOSIS", "Dosis", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR31DOSISPESO", "Dosis por Peso", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR31DOSISSUP", "Dosis por Superficie", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "FR31INDVIAOPC", "", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR34CODVIA", "C�d. V�a", cwString)
    Call .FormAddFilterWhere(strKey, "FRH5CODPERIODICIDAD", "Periodicidad", cwString)
    Call .FormAddFilterWhere(strKey, "FR31NUMDIAS", "D�as a Administrar", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRG4CODFRECUENCIA", "Frecuencia", cwString)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO_DIL", "Medicamento para diluir", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR31CANTIDADDIL", "Volumen Diluci�n", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR31TIEMMININF", "Tiempo Diluci�n", cwNumeric)
    Call .FormAddFilterOrder(strKey, "FR31NUMLINEA", "Num.Linea")

  End With
  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
  
    Call .FormCreateInfo(objMasterInfo)
    
    '' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    '.CtrlGetInfo(grdDBGrid1(0).Columns(4)).intKeyNo = 1
    ''Se indica que campos son obligatorios y cuales son clave primaria
    '.CtrlGetInfo(txtText1(0)).intKeyNo = 1
    'Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(29)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(28)).blnInFind = True
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    .CtrlGetInfo(txtText1(43)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    
    .CtrlGetInfo(txtHoras).blnNegotiated = False
    .CtrlGetInfo(txtMinutos).blnNegotiated = False
    
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    
    '.CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
   
    .CtrlGetInfo(cboSSDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(2)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(4)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(5)).blnInFind = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "FR73CODPRODUCTO", "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(5), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "SG02CODMEDRESP", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(14), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(5)), "FR73CODPRODUCTO_DIL", "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(5)), txtText1(8), "FR73DESPRODUCTO")

    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(13)).blnForeign = True
    
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    
    'Llenamos el combo cboSSDBCombo1(0) con la lista de Form.Farmaceuticas
    .CtrlGetInfo(cboSSDBCombo1(0)).strSQL = "SELECT FRH7CODFORMFAR,FRH7DESFORMFAR FROM FRH700 ORDER BY FRH7CODFORMFAR ASC"
    
    'Llenamos el combo cboSSDBCombo1(1) con la lista de Unidades de Medida
    .CtrlGetInfo(cboSSDBCombo1(1)).strSQL = "SELECT FR93CODUNIMEDIDA,FR93DESUNIMEDIDA FROM FR9300 ORDER BY FR93CODUNIMEDIDA ASC"
    
    'Llenamos el combo cboSSDBCombo1(2) con la lista de Frecuencias
    .CtrlGetInfo(cboSSDBCombo1(2)).strSQL = "SELECT FRG4CODFRECUENCIA,FRG4DESFRECUENCIA FROM FRG400 ORDER BY FRG4CODFRECUENCIA ASC"
    
    'Llenamos el combo cboSSDBCombo1(3) con la lista de V�as
    .CtrlGetInfo(cboSSDBCombo1(3)).strSQL = "SELECT FR34CODVIA,FR34DESVIA FROM FR3400 ORDER BY FR34CODVIA ASC"
    
    'Llenamos el combo cboSSDBCombo1(4) con la lista de Periodicidad
    .CtrlGetInfo(cboSSDBCombo1(4)).strSQL = "SELECT FRH5CODPERIODICIDAD,FRH5DESPERIODICIDAD FROM FRH500 ORDER BY FRH5CODPERIODICIDAD ASC"
    
    'Llenamos el combo cboSSDBCombo1(5) con la lista de Prod.Dil.
    '.CtrlGetInfo(cboSSDBCombo1(5)).strSQL = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO,FR73VOLUMEN,FRH7CODFORMFAR FROM FR7300 WHERE FR73INDPRODREC=-1 ORDER BY FR73DESPRODUCTO"
    .CtrlGetInfo(cboSSDBCombo1(5)).strSQL = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO,FR73VOLUMEN,FRH7CODFORMFAR FROM FR7300 WHERE FR73INDINFIV=-1 ORDER BY FR73DESPRODUCTO"
    
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  Frame2.Visible = False
  strEstiloCelda1 = "blanco"
  strEstiloCelda2 = "blau"
  nuevoDetalle = False

  grdDBGrid1(2).Columns("Indicaciones del Protocolo").Visible = False

  grdDBGrid1(6).Columns("Descripci�n Medicamento").Caption = "Medicamento"
  grdDBGrid1(6).Columns("Medicamento").Position = 0
  grdDBGrid1(6).Columns("Forma Farmace�tica").Caption = "FF"
  grdDBGrid1(6).Columns("FF").Position = 1
  grdDBGrid1(6).Columns("FF").Width = 400
  grdDBGrid1(6).Columns("Cantidad").Position = 2
  grdDBGrid1(6).Columns("Dosis").Position = 3
  grdDBGrid1(6).Columns("Unidad Medida").Caption = "UM"
  grdDBGrid1(6).Columns("UM").Position = 4
  grdDBGrid1(6).Columns("UM").Width = 655
  
  grdDBGrid1(6).Columns("Dosis por Peso?").Caption = "Peso?"
  grdDBGrid1(6).Columns("Peso?").Position = 5
  grdDBGrid1(6).Columns("Peso?").Width = 600
  grdDBGrid1(6).Columns("Dosis por Superficie Corporal?").Caption = "S.C.?"
  grdDBGrid1(6).Columns("S.C.?").Position = 6
  grdDBGrid1(6).Columns("S.C.?").Width = 600
  
  grdDBGrid1(6).Columns("Frecuencia").Position = 7
  grdDBGrid1(6).Columns("V�a").Position = 8
  grdDBGrid1(6).Columns("Periodicidad").Position = 9
  grdDBGrid1(6).Columns("Periodicidad").Width = 1100
  grdDBGrid1(6).Columns("D�as").Position = 10
  grdDBGrid1(6).Columns("D�as de administraci�n, separados por comas").Caption = "D�as Agenda"
  grdDBGrid1(6).Columns("D�as Agenda").Position = 11
  grdDBGrid1(6).Columns("D�as Agenda").Width = 1100
  
  grdDBGrid1(6).Columns("Medicamento Diluyente").Position = 12
  grdDBGrid1(6).Columns("Volumen Diluci�n").Caption = "Volumen"
  grdDBGrid1(6).Columns("Volumen").Position = 13
  grdDBGrid1(6).Columns("Volumen").Width = 750
  grdDBGrid1(6).Columns("Minutos Infusi�n").Caption = "Tiempo"
  grdDBGrid1(6).Columns("Tiempo").Position = 14
  grdDBGrid1(6).Columns("Tiempo").Width = 700
  
  grdDBGrid1(6).Columns("Operaci�n").Visible = False
  grdDBGrid1(6).Columns("C�digo Protocolo").Visible = False
  grdDBGrid1(6).Columns("Num. linea").Visible = False
  grdDBGrid1(6).Columns("C�digo Medicamento").Visible = False
  grdDBGrid1(6).Columns("C�d. Medicamento para Diluir").Visible = False
  grdDBGrid1(6).Columns("Indicador via opcional?").Visible = False
  grdDBGrid1(6).Columns("Indicaciones al Medicamento del Protocolo").Visible = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
  
  If grdDBGrid1(6).Rows > 0 Then
    If txtText1(29).Text <> "" Then
      
      Select Case grdDBGrid1(6).Columns("Operaci�n").Value
          Case "/":
            If strEstiloCelda1 = "blanco" Then
              strEstiloCelda1 = "amarillo"
            Else
              strEstiloCelda1 = "blanco"
            End If
            For i = 0 To 21
              grdDBGrid1(6).Columns(i).CellStyleSet strEstiloCelda1
            Next i
          Case "=":
            If strEstiloCelda2 = "blau" Then
              strEstiloCelda2 = "grana"
            Else
              strEstiloCelda2 = "blau"
            End If
            For i = 0 To 21
                grdDBGrid1(6).Columns(i).CellStyleSet strEstiloCelda2
            Next i
          Case "+":
            For i = 0 To 21
                grdDBGrid1(6).Columns(i).CellStyleSet strEstiloCelda2
            Next i
      End Select
    End If
  End If

End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch

 If strFormName = "E.C." And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�d. Dpto. Propietario"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto. Propietario"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "E.C." And strCtrl = "txtText1(13)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "SG0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�d. Doctor"

     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("SG02APE2")
     objField.strSmallDesc = "Apellido 2�"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(13), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim Finb As Boolean
Dim stra As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim qryupdate As rdoQuery
Dim strupdate As String
  
  If blnError = False Then
    'si es una mezcla arrastrar los cambios a los + siguientes
    If txtText1(29) = "=" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT * FROM FR3100 WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA > " & txtText1(7).Text & " ORDER BY FR31NUMLINEA ASC"
      stra = "SELECT * FROM FR3100 WHERE FR77CODPROTEC=? AND FR31NUMLINEA > ? ORDER BY FR31NUMLINEA ASC"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = txtText1(4).Text
      qrya(1) = txtText1(7).Text
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      Finb = False
      While Not rsta.EOF And Not Finb
        If rsta("FR31OPERACION").Value = "+" Then
          If cboSSDBCombo1(2).Value = "" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
            'strupdate = "UPDATE FR3100 SET FRG4CODFRECUENCIA=NULL WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            strupdate = "UPDATE FR3100 SET FRG4CODFRECUENCIA=NULL WHERE FR77CODPROTEC=? AND FR31NUMLINEA=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = txtText1(4).Text
            qryupdate(1) = rsta("FR31NUMLINEA").Value
            qryupdate.Execute
            'objApp.rdoConnect.Execute strupdate, 64
          Else
            'strupdate = "UPDATE FR3100 SET FRG4CODFRECUENCIA='" & cboSSDBCombo1(2).Value & "' WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            strupdate = "UPDATE FR3100 SET FRG4CODFRECUENCIA=? WHERE FR77CODPROTEC=? AND FR31NUMLINEA=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = cboSSDBCombo1(2).Value
            qryupdate(1) = txtText1(4).Text
            qryupdate(2) = rsta("FR31NUMLINEA").Value
            qryupdate.Execute
            'objApp.rdoConnect.Execute strupdate, 64
          End If
          'objApp.rdoConnect.Execute "Commit", 64
          If cboSSDBCombo1(4) = "" Then
            'strupdate = "UPDATE FR3100 SET FRH5CODPERIODICIDAD=NULL WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            strupdate = "UPDATE FR3100 SET FRH5CODPERIODICIDAD=NULL WHERE FR77CODPROTEC=? AND FR31NUMLINEA=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = txtText1(4).Text
            qryupdate(1) = rsta("FR31NUMLINEA").Value
            qryupdate.Execute
            'objApp.rdoConnect.Execute strupdate, 64
          Else
            'strupdate = "UPDATE FR3100 SET FRH5CODPERIODICIDAD='" & cboSSDBCombo1(4) & "' WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            strupdate = "UPDATE FR3100 SET FRH5CODPERIODICIDAD=? WHERE FR77CODPROTEC=? AND FR31NUMLINEA=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = cboSSDBCombo1(4).Value
            qryupdate(1) = txtText1(4).Text
            qryupdate(2) = rsta("FR31NUMLINEA").Value
            qryupdate.Execute
            'objApp.rdoConnect.Execute strupdate, 64
          End If
          'objApp.rdoConnect.Execute "Commit", 64
          If cboSSDBCombo1(3).Value = "" Then
            strupdate = "UPDATE FR3100 SET FR34CODVIA=NULL WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            objApp.rdoConnect.Execute strupdate, 64
          Else
            strupdate = "UPDATE FR3100 SET FR34CODVIA='" & cboSSDBCombo1(3).Value & "' WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            objApp.rdoConnect.Execute strupdate, 64
          End If
          'objApp.rdoConnect.Execute "Commit", 64
          If txtText1(10).Text = "" Then
            strupdate = "UPDATE FR3100 SET FR31DIASADM=NULL WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            objApp.rdoConnect.Execute strupdate, 64
          Else
            strupdate = "UPDATE FR3100 SET FR31DIASADM='" & txtText1(10).Text & "' WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            objApp.rdoConnect.Execute strupdate, 64
          End If
          'objApp.rdoConnect.Execute "Commit", 64
          If txtText1(9).Text = "" Then
            strupdate = "UPDATE FR3100 SET FR31NUMDIAS=NULL WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            objApp.rdoConnect.Execute strupdate, 64
          Else
            strupdate = "UPDATE FR3100 SET FR31NUMDIAS=" & txtText1(9).Text & " WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA=" & rsta("FR31NUMLINEA").Value
            objApp.rdoConnect.Execute strupdate, 64
          End If
          'objApp.rdoConnect.Execute "Commit", 64
        Else
          Finb = True
        End If
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
    End If
  End If
    intCambioAlgo = 0
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)

  If Index = 1 Then
    If tabTab1(1).Tab = 1 Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
    End If
  End If

End Sub

Private Sub tabTab1_DblClick(Index As Integer)

  If Index = 1 Then
    If tabTab1(1).Tab = 1 Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
    End If
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim linea As Integer
Dim rstfec As rdoResultset
Dim strfec As String
Dim rstcontrolOP As rdoResultset
Dim strcontrolOP As String

  Select Case btnButton.Index
  Case 2 'Nuevo
    Select Case objWinInfo.objWinActiveForm.strName
    Case "E.C."
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FR77CODPROTOCOLO_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
        rsta.Close
        Set rsta = Nothing
        strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
        Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
        Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstfec.rdoColumns(0).Value)
        rstfec.Close
        Set rstfec = Nothing
        Exit Sub
    Case "Detalle Protocolo"
      If nuevoDetalle = True Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Exit Sub
      Else 'Producto simple por defecto
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT MAX(FR31NUMLINEA) FROM FR3100 WHERE FR77CODPROTEC=" & txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        If IsNull(rsta.rdoColumns(0).Value) Then
          linea = 1
        Else
          linea = rsta.rdoColumns(0).Value + 1
        End If
        Call objWinInfo.CtrlSet(txtText1(7), linea)
        Call objWinInfo.CtrlSet(txtText1(29), "/")
        rsta.Close
        Set rsta = Nothing
        End If
    End Select
  Case 8
    If txtText1(29).Text = "=" And objWinInfo.objWinActiveForm.strName = "Detalle Protocolo" Then
      strcontrolOP = "SELECT FR31OPERACION FROM FR3100 WHERE FR77CODPROTEC=" & txtText1(4).Text & " AND FR31NUMLINEA > " & txtText1(7).Text & " ORDER BY FR31NUMLINEA ASC"
      Set rstcontrolOP = objApp.rdoConnect.OpenResultset(strcontrolOP)
      If Not rstcontrolOP.EOF Then
        If rstcontrolOP(0).Value = "+" Then
          MsgBox "La operaci�n no se puede borrar porque despues hay un +", vbExclamation
          Exit Sub
        End If
      Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      End If
      rstcontrolOP.Close
      Set rstcontrolOP = Nothing
    Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
    
  Case 4, 6, 18, 19, 16, 21, 22, 23, 24, 26
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Call controlar_campos
  Case Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End Select
  
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(18)) 'Filtro
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(19)) 'No Filtro
  End Select

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(16)) 'Localizar
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  Case 50
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22)) 'Anterior
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23)) 'Siguiente
  Case 70
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24)) 'Ultimo
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26)) 'Refrescar
  Case Else
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 0 Then
    If chkCheck1(0).Value = 1 Then
      objWinInfo.CtrlGetInfo(txtText1(2)).blnReadOnly = False
      txtText1(2).Locked = False
      txtText1(2).BackColor = vbWhite
    Else
      Call objWinInfo.CtrlSet(txtText1(2), "")
      txtText1(3).Text = ""
      objWinInfo.CtrlGetInfo(txtText1(2)).blnReadOnly = True
      txtText1(2).Locked = True
      txtText1(2).BackColor = txtText1(3).BackColor
    End If
  End If
  
  If intIndex = 1 Then
    If chkCheck1(1).Value = 0 Then
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(3)).blnReadOnly = True
      cboSSDBCombo1(3).Enabled = False
    Else
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(3)).blnReadOnly = False
      cboSSDBCombo1(3).Enabled = True
      cboSSDBCombo1(3).BackColor = vbWhite
    End If
  End If
  
  If intIndex = 2 Then
     If chkCheck1(2).Value <> 0 Then
        chkCheck1(3).Value = 0
     End If
  End If

  If intIndex = 3 Then
     If chkCheck1(3).Value <> 0 Then
        chkCheck1(2).Value = 0
     End If
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
  
  Call objWinInfo.CtrlLostFocus

  If intIndex = 28 Then
    If txtText1(28).Text <> "" Then
      If txtText1(6).Text = "" Then
        mensaje = MsgBox("Debe introducir antes el Medicamento.", vbInformation)
        txtText1(28).Text = ""
        Exit Sub
      End If
      stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(6).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If IsNull(rsta("FR73DOSIS").Value) Then
          Call objWinInfo.CtrlSet(txtText1(30), Val(txtText1(28).Text))
        Else
          Call objWinInfo.CtrlSet(txtText1(30), rsta("FR73DOSIS").Value * Val(txtText1(28).Text))
        End If
      Else
        Call objWinInfo.CtrlSet(txtText1(30), Val(txtText1(28).Text))
      End If
      rsta.Close
      Set rsta = Nothing
    Else
      Call objWinInfo.CtrlSet(txtText1(30), "")
    End If
  End If

  If intIndex = 30 Then
    If txtText1(30).Text <> "" Then
      If txtText1(6).Text = "" Then
        mensaje = MsgBox("Debe introducir antes el Medicamento.", vbInformation)
        txtText1(30).Text = ""
        Exit Sub
      End If
      stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(6).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If IsNull(rsta("FR73DOSIS").Value) Then
          Call objWinInfo.CtrlSet(txtText1(28), Val(txtText1(30).Text))
        Else
          Call objWinInfo.CtrlSet(txtText1(28), Val(txtText1(30).Text) / rsta("FR73DOSIS").Value)
        End If
      Else
        Call objWinInfo.CtrlSet(txtText1(28), Val(txtText1(30).Text))
      End If
      rsta.Close
      Set rsta = Nothing
    Else
      Call objWinInfo.CtrlSet(txtText1(28), "")
    End If
  End If

End Sub


Private Sub txtText1_Change(intIndex As Integer)
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
  
  Call objWinInfo.CtrlDataChange
      
  Call controlar_campos
  
  If intIndex = 0 Then
    If txtText1(0).Text = "" Then
      cmd1(0).Enabled = False
      cmd1(1).Enabled = False
      cmd1(2).Enabled = False
      cmdbuscargruprod(0).Enabled = False
      cmdbuscargruprod(0).Enabled = False
      cmdbuscarprod.Enabled = False
    Else
      Select Case txtText1(29).Text
        Case "/":
          cmd1(0).FontBold = True
          cmd1(1).FontBold = False
          cmd1(2).FontBold = False
          cmd1(0).Enabled = True
          cmd1(1).Enabled = True
          cmd1(2).Enabled = False
        Case "+":
          cmd1(0).FontBold = False
          cmd1(1).FontBold = False
          cmd1(2).FontBold = True
          cmd1(0).Enabled = True
          cmd1(1).Enabled = True
          cmd1(2).Enabled = True
        Case "=":
          cmd1(0).FontBold = False
          cmd1(1).FontBold = True
          cmd1(2).FontBold = False
          cmd1(0).Enabled = True
          cmd1(1).Enabled = True
          cmd1(2).Enabled = True
        Case Else
          cmd1(0).FontBold = True
          cmd1(1).FontBold = False
          cmd1(2).FontBold = False
          cmd1(0).Enabled = True
          cmd1(1).Enabled = True
          cmd1(2).Enabled = False
      End Select
    End If
  End If

  'If intIndex = 2 Then
'    If chkCheck1(0).Value = 1 Then
'      objWinInfo.CtrlGetInfo(txtText1(2)).blnReadOnly = False
'      txtText1(2).Locked = False
'      txtText1(2).BackColor = vbWhite
'    Else
'      Call objWinInfo.CtrlSet(txtText1(2), "")
'      txtText1(3).Text = ""
'      objWinInfo.CtrlGetInfo(txtText1(2)).blnReadOnly = True
'      txtText1(2).Locked = True
 '     txtText1(2).BackColor = txtText1(3).BackColor
 '   End If
 ' End If

  If intIndex = 43 Then
    If IsNumeric(txtText1(43).Text) Then
        txtHoras.Text = txtText1(43).Text \ 60
        txtMinutos.Text = txtText1(43).Text Mod 60
    Else
        txtHoras.Text = ""
        txtMinutos.Text = ""
    End If
  End If
End Sub

Private Sub controlar_campos()
    
      Select Case txtText1(29).Text
          Case "/":
              
              cmd1(0).FontBold = True
              cmd1(1).FontBold = False
              cmd1(2).FontBold = False
          
              cmd1(0).Enabled = True
              cmd1(1).Enabled = True
              cmd1(2).Enabled = False
          
              'txtText1(29).Visible = True 'FR31OPERACION
              txtText1(6).Visible = False 'FR73CODPRODUCTO
              txtText1(5).Visible = True 'FR73DESPRODUCTO
              cmdbuscargruprod(0).Enabled = True
              cmdbuscargruprod(1).Enabled = True
              cmdbuscarprod.Enabled = True
              Call Bloquear2(cboSSDBCombo1(0), True, "noborrar") 'FRH7CODFORMFAR
              Call Bloquear2(txtText1(28), False, "textbox") 'FR31CANTIDAD
              Call Bloquear2(txtText1(30), False, "textbox") 'FR31DOSIS
              'Call Bloquear2(chkCheck1(2), False, "") 'FR31DOSISPESO
              'Call Bloquear2(chkCheck1(3), False, "") 'FR31DOSISSUP
              Call Bloquear2(cboSSDBCombo1(1), False, "") 'FR93CODUNIMEDIDA
              Call Bloquear2(cboSSDBCombo1(2), False, "") 'FRG4CODFRECUENCIA
              Call Bloquear2(cboSSDBCombo1(3), False, "") 'FR34CODVIA
              Call Bloquear2(cboSSDBCombo1(4), False, "") 'FRH5CODPERIODICIDAD
              If cboSSDBCombo1(4) = "Agenda" Then
                Call Bloquear2(txtText1(9), True, "textbox") 'FR31NUMDIAS
              Else
                Call Bloquear2(txtText1(9), False, "textbox") 'FR31NUMDIAS
              End If
              Call Bloquear2(cboSSDBCombo1(5), True, "") 'FR73CODPRODUCTO_DIL
              Call Bloquear2(txtText1(42), True, "textbox") 'FR31CANTIDADDIL
              Call Bloquear2(txtText1(43), True, "textbox") 'FR31TIEMINF
              Call Bloquear2(txtHoras, True, "textbox")
              Call Bloquear2(txtMinutos, True, "textbox")
          Case "+":
              
              cmd1(0).FontBold = False
              cmd1(1).FontBold = False
              cmd1(2).FontBold = True
          
              cmd1(0).Enabled = True
              cmd1(1).Enabled = True
              cmd1(2).Enabled = True
              
              'txtText1(29).Visible = True 'FR31OPERACION
              txtText1(6).Visible = False 'FR73CODPRODUCTO
              txtText1(5).Visible = True 'FR73DESPRODUCTO
              cmdbuscargruprod(0).Enabled = True
              cmdbuscargruprod(1).Enabled = True
              cmdbuscarprod.Enabled = True
              Call Bloquear2(cboSSDBCombo1(0), True, "noborrar") 'FRH7CODFORMFAR
              Call Bloquear2(txtText1(28), False, "textbox") 'FR31CANTIDAD
              Call Bloquear2(txtText1(30), False, "textbox") 'FR31DOSIS
              'Call Bloquear2(chkCheck1(2), False, "") 'FR31DOSISPESO
              'Call Bloquear2(chkCheck1(3), False, "") 'FR31DOSISSUP
              Call Bloquear2(cboSSDBCombo1(1), False, "") 'FR93CODUNIMEDIDA
              Call Bloquear2(cboSSDBCombo1(2), True, "") 'FRG4CODFRECUENCIA
              Call Bloquear2(cboSSDBCombo1(3), True, "") 'FR34CODVIA
              Call Bloquear2(cboSSDBCombo1(4), True, "") 'FRH5CODPERIODICIDAD
              Call Bloquear2(txtText1(9), True, "textbox") 'FR31NUMDIAS
              Call Bloquear2(cboSSDBCombo1(5), True, "") 'FR73CODPRODUCTO_DIL
              Call Bloquear2(txtText1(42), True, "textbox") 'FR31CANTIDADDIL
              Call Bloquear2(txtText1(43), True, "textbox") 'FR31TIEMINF
              Call Bloquear2(txtHoras, True, "textbox") 'FR31TIEMINF
              Call Bloquear2(txtMinutos, True, "textbox") 'FR31TIEMINF
          Case "=":
              
              cmd1(0).FontBold = False
              cmd1(1).FontBold = True
              cmd1(2).FontBold = False
          
              cmd1(0).Enabled = True
              cmd1(1).Enabled = True
              cmd1(2).Enabled = True
          
              'txtText1(29).Visible = True 'FR31OPERACION
              txtText1(6).Visible = False 'FR73CODPRODUCTO
              txtText1(5).Visible = True 'FR73DESPRODUCTO
              cmdbuscargruprod(0).Enabled = True
              cmdbuscargruprod(1).Enabled = True
              cmdbuscarprod.Enabled = True
              Call Bloquear2(cboSSDBCombo1(0), True, "noborrar") 'FRH7CODFORMFAR
              Call Bloquear2(txtText1(28), False, "textbox") 'FR31CANTIDAD
              Call Bloquear2(txtText1(30), False, "textbox") 'FR31DOSIS
              'Call Bloquear2(chkCheck1(2), False, "") 'FR31DOSISPESO
              'Call Bloquear2(chkCheck1(3), False, "") 'FR31DOSISSUP
              Call Bloquear2(cboSSDBCombo1(1), False, "") 'FR93CODUNIMEDIDA
              Call Bloquear2(cboSSDBCombo1(2), False, "") 'FRG4CODFRECUENCIA
              Call Bloquear2(cboSSDBCombo1(3), False, "") 'FR34CODVIA
              Call Bloquear2(cboSSDBCombo1(4), False, "") 'FRH5CODPERIODICIDAD
              If cboSSDBCombo1(4) = "Agenda" Then
                Call Bloquear2(txtText1(9), True, "textbox") 'FR31NUMDIAS
              Else
                Call Bloquear2(txtText1(9), False, "textbox") 'FR31NUMDIAS
              End If
              Call Bloquear2(cboSSDBCombo1(5), False, "") 'FR73CODPRODUCTO_DIL
              Call Bloquear2(txtText1(42), False, "textbox") 'FR31CANTIDADDIL
              Call Bloquear2(txtText1(43), False, "textbox") 'FR31TIEMINF
              Call Bloquear2(txtHoras, False, "textbox") 'FR31TIEMINF
              Call Bloquear2(txtMinutos, False, "textbox") 'FR31TIEMINF
        Case ""
              
              cmd1(0).Enabled = False
              cmd1(1).Enabled = False
              cmd1(2).Enabled = False
          
              cmdbuscargruprod(0).Enabled = False
              cmdbuscargruprod(1).Enabled = False
              cmdbuscarprod.Enabled = False
        Case Else
      End Select

End Sub

Private Sub Bloquear2(campo As Control, bloqueado As Boolean, tipo As String)

  If bloqueado Then
      objWinInfo.CtrlGetInfo(campo).blnReadOnly = True
      If tipo = "textbox" Then
        campo.Locked = True
      Else
        campo.Enabled = False
      End If
      campo.BackColor = txtText1(3).BackColor
  Else
      objWinInfo.CtrlGetInfo(campo).blnReadOnly = False
      If tipo = "textbox" Then
        campo.Locked = False
      Else
        campo.Enabled = True
      End If
      campo.BackColor = vbWhite
  End If

End Sub
