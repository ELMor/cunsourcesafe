VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDispProdOMEC 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Ordenes M�dicas de Ensayo Cl�nico. Dispensar Medicamentos"
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4425
      Index           =   0
      Left            =   120
      TabIndex        =   86
      Top             =   3360
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3915
         Index           =   0
         Left            =   120
         TabIndex        =   87
         Top             =   360
         Width           =   11340
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   3
         stylesets(0).Name=   "rojo"
         stylesets(0).ForeColor=   255
         stylesets(0).Picture=   "FR0235.frx":0000
         stylesets(1).Name=   "negro"
         stylesets(1).ForeColor=   0
         stylesets(1).Picture=   "FR0235.frx":001C
         stylesets(2).Name=   "azul"
         stylesets(2).ForeColor=   16711680
         stylesets(2).Picture=   "FR0235.frx":0038
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20002
         _ExtentY        =   6906
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Index           =   1
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   11625
      Begin TabDlg.SSTab tabTab1 
         Height          =   2415
         Index           =   0
         Left            =   120
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   4260
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0235.frx":0054
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(13)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "tab1"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(26)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(25)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkCheck1(15)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(7)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0235.frx":0070
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR56INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   7440
            TabIndex        =   11
            Tag             =   "Diab�tico?"
            Top             =   120
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Al�rgico"
            DataField       =   "FR56INDALERGIA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   7440
            TabIndex        =   10
            Tag             =   "Alergico?"
            Top             =   360
            Visible         =   0   'False
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n volumen"
            DataField       =   "FR56INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   8640
            TabIndex        =   9
            Tag             =   "Restricci�n de Volumen?"
            Top             =   120
            Visible         =   0   'False
            Width           =   2175
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR56INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   8640
            TabIndex        =   8
            Tag             =   "Inter�s Cient�fico?"
            Top             =   360
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   25
            Left            =   6720
            TabIndex        =   7
            Tag             =   "Estado Petici�n"
            Top             =   360
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   26
            Left            =   7080
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Visible         =   0   'False
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR56CODPETOMEC"
            Height          =   330
            Index           =   0
            Left            =   5280
            TabIndex        =   5
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Visible         =   0   'False
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2145
            Index           =   2
            Left            =   -74880
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   120
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   3784
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab1 
            Height          =   2175
            Left            =   120
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   120
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   3836
            _Version        =   327681
            Style           =   1
            Tabs            =   6
            TabsPerRow      =   6
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0235.frx":008C
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(42)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(41)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(40)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(39)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(38)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(11)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(7)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(8)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(9)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(7)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(6)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "Text1"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(29)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(33)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(32)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(46)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(23)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(3)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(2)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(4)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(5)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).ControlCount=   22
            TabCaption(1)   =   "M�dico"
            TabPicture(1)   =   "FR0235.frx":00A8
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(12)"
            Tab(1).Control(1)=   "txtText1(20)"
            Tab(1).Control(2)=   "txtText1(9)"
            Tab(1).Control(3)=   "txtText1(8)"
            Tab(1).Control(4)=   "txtText1(22)"
            Tab(1).Control(5)=   "txtText1(11)"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "txtText1(10)"
            Tab(1).Control(7)=   "dtcDateCombo1(1)"
            Tab(1).Control(8)=   "dtcDateCombo1(2)"
            Tab(1).Control(9)=   "lblLabel1(15)"
            Tab(1).Control(10)=   "lblLabel1(18)"
            Tab(1).Control(11)=   "lblLabel1(5)"
            Tab(1).Control(12)=   "lblLabel1(20)"
            Tab(1).Control(13)=   "lblLabel1(6)"
            Tab(1).Control(14)=   "lblLabel1(10)"
            Tab(1).Control(15)=   "lblLabel1(0)"
            Tab(1).ControlCount=   16
            TabCaption(2)   =   "Servicio"
            TabPicture(2)   =   "FR0235.frx":00C4
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(17)"
            Tab(2).Control(1)=   "txtText1(16)"
            Tab(2).Control(2)=   "txtText1(24)"
            Tab(2).Control(3)=   "txtText1(14)"
            Tab(2).Control(4)=   "txtText1(15)"
            Tab(2).Control(5)=   "txtText1(19)"
            Tab(2).Control(6)=   "txtText1(21)"
            Tab(2).Control(7)=   "lblLabel1(1)"
            Tab(2).Control(8)=   "lblLabel1(12)"
            Tab(2).Control(9)=   "lblLabel1(3)"
            Tab(2).Control(10)=   "lblLabel1(4)"
            Tab(2).ControlCount=   11
            TabCaption(3)   =   "Observaciones"
            TabPicture(3)   =   "FR0235.frx":00E0
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(18)"
            Tab(3).Control(1)=   "lblLabel1(24)"
            Tab(3).ControlCount=   2
            TabCaption(4)   =   "Protocolo E.C."
            TabPicture(4)   =   "FR0235.frx":00FC
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "lblLabel1(46)"
            Tab(4).Control(0).Enabled=   0   'False
            Tab(4).Control(1)=   "lblLabel1(45)"
            Tab(4).Control(1).Enabled=   0   'False
            Tab(4).Control(2)=   "lblLabel1(44)"
            Tab(4).Control(2).Enabled=   0   'False
            Tab(4).Control(3)=   "lblLabel1(43)"
            Tab(4).Control(3).Enabled=   0   'False
            Tab(4).Control(4)=   "lblLabel1(27)"
            Tab(4).Control(4).Enabled=   0   'False
            Tab(4).Control(5)=   "lblLabel1(22)"
            Tab(4).Control(5).Enabled=   0   'False
            Tab(4).Control(6)=   "dtcDateCombo1(5)"
            Tab(4).Control(6).Enabled=   0   'False
            Tab(4).Control(7)=   "dtcDateCombo1(0)"
            Tab(4).Control(7).Enabled=   0   'False
            Tab(4).Control(8)=   "txtText1(49)"
            Tab(4).Control(8).Enabled=   0   'False
            Tab(4).Control(9)=   "txtText1(48)"
            Tab(4).Control(9).Enabled=   0   'False
            Tab(4).Control(10)=   "txtText1(47)"
            Tab(4).Control(10).Enabled=   0   'False
            Tab(4).Control(11)=   "txtText1(44)"
            Tab(4).Control(11).Enabled=   0   'False
            Tab(4).Control(12)=   "txtText1(39)"
            Tab(4).Control(12).Enabled=   0   'False
            Tab(4).Control(13)=   "txtText1(38)"
            Tab(4).Control(13).Enabled=   0   'False
            Tab(4).ControlCount=   14
            TabCaption(5)   =   "Descripci�n Protocolo E.C."
            TabPicture(5)   =   "FR0235.frx":0118
            Tab(5).ControlEnabled=   0   'False
            Tab(5).Control(0)=   "txtText1(50)"
            Tab(5).Control(1)=   "cmd2"
            Tab(5).ControlCount=   2
            Begin VB.TextBox txtText1 
               DataField       =   "FR56OBSERV"
               Height          =   1410
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   49
               Tag             =   "Observaciones"
               Top             =   720
               Width           =   9165
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   6120
               TabIndex        =   48
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   1560
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   3120
               TabIndex        =   47
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   1560
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   2
               Left            =   8880
               TabIndex        =   46
               Tag             =   "C�digo Paciente"
               Top             =   960
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   3
               Left            =   120
               TabIndex        =   45
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   1560
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   23
               Left            =   120
               TabIndex        =   44
               TabStop         =   0   'False
               Tag             =   "Historia Paciente"
               Top             =   840
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   46
               Left            =   3480
               TabIndex        =   43
               TabStop         =   0   'False
               Tag             =   "Cama"
               Top             =   840
               Width           =   900
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR56PESOPAC"
               Height          =   330
               Index           =   32
               Left            =   4800
               TabIndex        =   42
               Tag             =   "Peso"
               Top             =   840
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR56ALTUPAC"
               Height          =   330
               Index           =   33
               Left            =   5880
               TabIndex        =   41
               Tag             =   "Altura Cm"
               Top             =   840
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   29
               Left            =   6960
               TabIndex        =   40
               Tag             =   "Superficie Corporal"
               Top             =   840
               Width           =   600
            End
            Begin VB.TextBox Text1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   1920
               TabIndex        =   39
               TabStop         =   0   'False
               Tag             =   "Edad"
               Top             =   840
               Width           =   420
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO_MED"
               Height          =   330
               Index           =   12
               Left            =   -70920
               TabIndex        =   38
               Tag             =   "C�digo Doctor"
               Top             =   840
               Width           =   1275
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR56HORAFIRMMEDI"
               Height          =   330
               Index           =   20
               Left            =   -67320
               TabIndex        =   37
               Tag             =   "Hora Firma M�dico"
               Top             =   840
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   9
               Left            =   -72720
               TabIndex        =   36
               Tag             =   "Apellido Doctor"
               Top             =   840
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   8
               Left            =   -74760
               TabIndex        =   35
               Tag             =   "C�digo Doctor"
               Top             =   840
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   -72600
               TabIndex        =   34
               Tag             =   "Desc.Departamento"
               Top             =   840
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74760
               TabIndex        =   33
               Tag             =   "C�d.Departamento"
               Top             =   840
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   -70920
               TabIndex        =   32
               Tag             =   "C�digo Urgencia"
               Top             =   1560
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   -70080
               TabIndex        =   31
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   1560
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR56HORAREDACCI"
               Height          =   330
               Index           =   10
               Left            =   -72840
               TabIndex        =   30
               Tag             =   "Hora Redacci�n"
               Top             =   1560
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   6
               Left            =   2760
               TabIndex        =   29
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   840
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   7
               Left            =   8880
               TabIndex        =   28
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   480
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_FEN"
               Height          =   330
               Index           =   24
               Left            =   -74760
               TabIndex        =   27
               Tag             =   "M�dico que firma"
               Top             =   1560
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_ENF"
               Height          =   330
               Index           =   14
               Left            =   -69720
               TabIndex        =   26
               Tag             =   "M�dico que env�a"
               Top             =   1560
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   15
               Left            =   -68880
               TabIndex        =   25
               Tag             =   "Apellido Doctor"
               Top             =   1560
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   -74040
               TabIndex        =   24
               Tag             =   "Apellido Doctor"
               Top             =   1560
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   21
               Left            =   -72120
               TabIndex        =   23
               Tag             =   "Apellido Doctor"
               Top             =   1560
               Width           =   720
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   38
               Left            =   -73680
               TabIndex        =   22
               Tag             =   "Apellido Doctor"
               Top             =   1200
               Width           =   2445
            End
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   39
               Left            =   -74880
               TabIndex        =   21
               Tag             =   "C�digo Doctor"
               Top             =   1200
               Width           =   1155
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR77CODPROTEC"
               Height          =   330
               Index           =   44
               Left            =   -74880
               Locked          =   -1  'True
               TabIndex        =   20
               Tag             =   "C�digo Protocolo"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   47
               Left            =   -73440
               TabIndex        =   19
               Tag             =   "Descripci�n Protocolo|Descripci�n Protocolo"
               Top             =   600
               Width           =   7560
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   48
               Left            =   -70560
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "Descripci�n Dpto. Propietario"
               Top             =   1200
               Width           =   4800
            End
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   49
               Left            =   -71040
               TabIndex        =   17
               Tag             =   "C�d. Dpto. Propietario"
               Top             =   1200
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               Height          =   1410
               Index           =   50
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   16
               Tag             =   "Observaciones"
               Top             =   480
               Width           =   7965
            End
            Begin VB.CommandButton cmd2 
               Caption         =   "Expedientes Asociados"
               Height          =   495
               Left            =   -66840
               TabIndex        =   15
               Top             =   840
               Width           =   1215
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   50
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   51
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR56FECFIRMMEDI"
               Height          =   330
               Index           =   1
               Left            =   -69360
               TabIndex        =   52
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   840
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR56FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   -74760
               TabIndex        =   53
               Tag             =   "Fecha Redacci�n"
               Top             =   1560
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               Height          =   330
               Index           =   0
               Left            =   -72720
               TabIndex        =   54
               Tag             =   "Fecha Fin Vigencia"
               Top             =   1800
               Width           =   1620
               _Version        =   65537
               _ExtentX        =   2857
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               Height          =   330
               Index           =   5
               Left            =   -74880
               TabIndex        =   55
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1800
               Width           =   1620
               _Version        =   65537
               _ExtentX        =   2857
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   83
               Top             =   480
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   6120
               TabIndex        =   82
               Top             =   1320
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   3120
               TabIndex        =   81
               Top             =   1320
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   120
               TabIndex        =   80
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   120
               TabIndex        =   79
               Top             =   600
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cama"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   38
               Left            =   3480
               TabIndex        =   78
               Top             =   600
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   39
               Left            =   4800
               TabIndex        =   77
               Top             =   600
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Altura Cm"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   40
               Left            =   5880
               TabIndex        =   76
               Top             =   600
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Superficie Corporal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   6960
               TabIndex        =   75
               Top             =   600
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Edad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   42
               Left            =   1920
               TabIndex        =   74
               Top             =   600
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   -70920
               TabIndex        =   73
               Top             =   600
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -67320
               TabIndex        =   72
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -69360
               TabIndex        =   71
               Top             =   600
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   -74760
               TabIndex        =   70
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   69
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -70920
               TabIndex        =   68
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -72840
               TabIndex        =   67
               Top             =   1320
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74760
               TabIndex        =   66
               Top             =   1320
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   2760
               TabIndex        =   65
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -74760
               TabIndex        =   64
               Top             =   1320
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que env�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -69720
               TabIndex        =   63
               Top             =   1320
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Colegiado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -72120
               TabIndex        =   62
               Top             =   1320
               Width           =   1335
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Promotor"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   -74880
               TabIndex        =   61
               Top             =   960
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Inicio Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Index           =   27
               Left            =   -74880
               TabIndex        =   60
               Top             =   1560
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fin Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   43
               Left            =   -72720
               TabIndex        =   59
               Top             =   1560
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cod. Protocolo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   44
               Left            =   -74880
               TabIndex        =   58
               Top             =   360
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Ensayo Cl�nico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   45
               Left            =   -73440
               TabIndex        =   57
               Top             =   360
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   46
               Left            =   -71040
               TabIndex        =   56
               Top             =   960
               Width           =   705
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   5280
            TabIndex        =   85
            Top             =   120
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   6720
            TabIndex        =   84
            Top             =   120
            Visible         =   0   'False
            Width           =   735
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "V�a"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   26
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   975
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDispProdOMEC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDispProdOMEC (FR0235.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: redactar Orden M�dica                                   *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "OM"
    
    .strTable = "FR5600"
    
    Call .FormAddOrderField("FR56CODPETOMEC", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR56CODPETOMEC", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR56FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR56FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR56INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR56INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR56INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR56INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR56INDALERGIA", "Alergia?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
  End With
   

  With objMultiInfo
    .strName = "Detalle OM"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FR2100"
    
    Call .FormAddOrderField("FR22NUMLINEA", cwAscending)
    Call .FormAddRelation("FR56CODPETOMEC", txtText1(0))
    
 
    strKey = .strDataBase & .strTable
  End With

  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

    Call .GridAddColumn(objMultiInfo, "C�digo", "FR56CODPETOMEC", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR22NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "OP", "FR21OPERACION", cwString, 1)
    Call .GridAddColumn(objMultiInfo, "PRN", "FR21INDDISPPRN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "C�d.Medic", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Medic", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Medicamento", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FR21CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR22DOSIS", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Medida", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Periodicidad", "FRH5CODPERIODICIDAD", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Fecha Inicio", "FR21FECINICIO", cwDate)
    Call .GridAddColumn(objMultiInfo, "Hora Inicio", "FR21HORAINICIO", cwNumeric, 2)
    'Call .GridAddColumn(objMultiInfo, "Minuto Inicio", "FR21MININICIO", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Fecha Fin", "FR21FECFIN", cwDate)
    Call .GridAddColumn(objMultiInfo, "Hora Fin", "FR21HORAFIN", cwNumeric, 2)
    'Call .GridAddColumn(objMultiInfo, "Minuto Fin", "FR21MINFIN", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Inmediato", "FR21INDCOMIENINMED", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "SN", "FR21INDSN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Prod_Dil", "FR73CODPRODUCTO_DIL", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Diluyente", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Cantidad Dil", "FR21CANTIDADDIL", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Tiempo Inf", "FR21TIEMMININF", cwNumeric, 5)
    'Call .GridAddColumn(objMultiInfo, "Notas", "FR21NOTASCOMP", cwString, 2000)

  
    Call .FormCreateInfo(objMasterInfo)
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
 
  tab1.TabVisible(0) = False
  tab1.TabVisible(1) = False
  tab1.TabVisible(2) = False
  tab1.TabVisible(3) = False
  tab1.TabVisible(4) = True
  tab1.TabVisible(5) = True


End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch

 If strFormName = "Protocolos" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�d. Dpto. Propietario"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto. Propietario"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)

  If Index = 1 Then
    If tabTab1(1).Tab = 1 Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
    End If
  End If

End Sub

Private Sub tabTab1_DblClick(Index As Integer)

  If Index = 1 Then
    If tabTab1(1).Tab = 1 Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
    End If
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(18)) 'Filtro
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(19)) 'No Filtro
  End Select

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(16)) 'Localizar
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  Case 50
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22)) 'Anterior
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23)) 'Siguiente
  Case 70
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24)) 'Ultimo
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26)) 'Refrescar
  Case Else
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub


Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

