VERSION 5.00
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "msmapi32.ocx"
Begin VB.Form frmEnviarRespCorreo 
   Caption         =   "FARMACIA. Enviar Mensaje de Correo"
   ClientHeight    =   7530
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11625
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   11625
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSMAPI.MAPISession MAPISession1 
      Left            =   840
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   6360
      TabIndex        =   6
      Top             =   8040
      Width           =   1935
   End
   Begin VB.CommandButton cmdEnviarMensaje 
      Caption         =   "Enviar Correo"
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   8040
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Mensaje de Correo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5640
      Index           =   0
      Left            =   480
      TabIndex        =   0
      Top             =   600
      Width           =   10335
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   3090
         HelpContextID   =   30104
         Index           =   8
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "Respuesta de la Consulta"
         ToolTipText     =   "Respuesta de la Consulta"
         Top             =   2160
         Width           =   9720
      End
      Begin VB.CommandButton cmdDestCorreo 
         Caption         =   "..."
         Height          =   330
         Left            =   4560
         TabIndex        =   7
         Top             =   1080
         Width           =   330
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   1
         Left            =   5400
         TabIndex        =   2
         Tag             =   "Remitente"
         ToolTipText     =   "Remitente"
         Top             =   1080
         Width           =   4575
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   0
         Left            =   360
         TabIndex        =   1
         Tag             =   "Destinatario"
         ToolTipText     =   "Destinatario"
         Top             =   1080
         Width           =   4215
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Respuesta de la Consulta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   360
         TabIndex        =   9
         Top             =   1800
         Width           =   2415
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Remitente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   5400
         TabIndex        =   5
         Top             =   840
         Width           =   870
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Destinatario"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   4
         Top             =   840
         Width           =   1035
      End
   End
   Begin MSMAPI.MAPIMessages MAPIMessages1 
      Left            =   120
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
End
Attribute VB_Name = "frmEnviarRespCorreo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim mblnunload As Boolean

Private Sub cmdDestCorreo_Click()
  On Error GoTo Err_Ejecutar
    MAPIMessages1.Show
    'MAPIMessages1.MsgIndex = 1
    txtText1(0).Text = MAPIMessages1.RecipDisplayName
   Exit Sub
Err_Ejecutar:
  MsgBox (Error)
End Sub

Private Sub cmdEnviarMensaje_Click()
  On Error GoTo Err_Ejecutar
    MAPIMessages1.AddressCaption = txtText1(1).Text
    MAPIMessages1.RecipDisplayName = txtText1(0).Text
    MAPIMessages1.MsgNoteText = txtText1(8).Text
    MAPIMessages1.Send (True)
    'MAPISession1.SignOn
    Exit Sub
Err_Ejecutar:
  Call MsgBox("No ha indicado el Destinatario del Correo", vbExclamation)
End Sub

Private Sub cmdsalir_Click()
  Unload Me
End Sub


Private Sub Form_Activate()
  If mblnunload = True Then
    Unload Me
  End If
End Sub

Private Sub Form_Load()
On Error GoTo Err_Ejecutar
    MAPISession1.SignOn
    MAPIMessages1.MsgIndex = -1
    MAPIMessages1.SessionID = MAPISession1.SessionID
    mblnunload = False
Exit Sub
Err_Ejecutar:
  mblnunload = True
End Sub

