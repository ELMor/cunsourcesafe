VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmEstPetCestasSec 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Estudiar Petici�n Farmacia PRN"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "LISTADOS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   5520
      TabIndex        =   21
      Top             =   4440
      Visible         =   0   'False
      Width           =   3975
      Begin VB.CheckBox Check2 
         Caption         =   "Dispensaci�n de Cesta Por Paciente"
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   360
         Value           =   1  'Checked
         Width           =   3135
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Dispensaci�n de Cesta Sin Paciente"
         Height          =   375
         Left            =   240
         TabIndex        =   23
         Top             =   720
         Width           =   3015
      End
      Begin VB.CommandButton cmdAcept 
         Caption         =   "ACEPTAR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         TabIndex        =   22
         Top             =   1320
         Width           =   1215
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Acumulado de Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5640
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   2280
      Width           =   10335
      Begin SSDataWidgets_B.SSDBGrid grdAux1 
         Height          =   1995
         Index           =   0
         Left            =   1200
         TabIndex        =   15
         Top             =   1440
         Visible         =   0   'False
         Width           =   7185
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   31
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0359.frx":0000
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   31
         Columns(0).Width=   1005
         Columns(0).Caption=   "Bloq"
         Columns(0).Name =   "Bloq"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   12632256
         Columns(1).Width=   794
         Columns(1).Caption=   "Pet"
         Columns(1).Name =   "Pet"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   12632256
         Columns(2).Width=   1085
         Columns(2).Caption=   "L�nea"
         Columns(2).Name =   "L�nea"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   12632256
         Columns(3).Width=   1429
         Columns(3).Caption=   "C�d.Prod"
         Columns(3).Name =   "C�d.Prod"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   12632256
         Columns(4).Width=   1005
         Columns(4).Caption=   "Prod"
         Columns(4).Name =   "Prod"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   12632256
         Columns(5).Width=   1746
         Columns(5).Caption=   "Referencia"
         Columns(5).Name =   "Referencia"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   12632256
         Columns(6).Width=   3200
         Columns(6).Caption=   "Desc.Producto"
         Columns(6).Name =   "Desc.Producto"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   12632256
         Columns(7).Width=   1402
         Columns(7).Caption=   "Tam Env"
         Columns(7).Name =   "Tam Env"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   12632256
         Columns(8).Width=   661
         Columns(8).Caption=   "FF"
         Columns(8).Name =   "FF"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   12632256
         Columns(9).Width=   979
         Columns(9).Caption=   "Dosis"
         Columns(9).Name =   "Dosis"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(9).HasBackColor=   -1  'True
         Columns(9).BackColor=   12632256
         Columns(10).Width=   767
         Columns(10).Caption=   "UM"
         Columns(10).Name=   "UM"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(10).HasBackColor=   -1  'True
         Columns(10).BackColor=   12632256
         Columns(11).Width=   1455
         Columns(11).Caption=   "Desc.UM"
         Columns(11).Name=   "Desc.UM"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Locked=   -1  'True
         Columns(11).HasBackColor=   -1  'True
         Columns(11).BackColor=   12632256
         Columns(12).Width=   1323
         Columns(12).Caption=   "Can Ped"
         Columns(12).Name=   "Can Ped"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(12).HasBackColor=   -1  'True
         Columns(12).BackColor=   12632256
         Columns(13).Width=   1270
         Columns(13).Caption=   "Ac Ped"
         Columns(13).Name=   "Ac Ped"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(13).HasBackColor=   -1  'True
         Columns(13).BackColor=   12632256
         Columns(14).Width=   1640
         Columns(14).Caption=   "Can Disp"
         Columns(14).Name=   "Can Disp"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(14).HasBackColor=   -1  'True
         Columns(14).BackColor=   16776960
         Columns(15).Width=   1429
         Columns(15).Caption=   "Ac Disp"
         Columns(15).Name=   "Ac Disp"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   953
         Columns(16).Caption=   "V�a"
         Columns(16).Name=   "V�a"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         Columns(16).Locked=   -1  'True
         Columns(16).HasBackColor=   -1  'True
         Columns(16).BackColor=   12632256
         Columns(17).Width=   1667
         Columns(17).Caption=   "Desc.V�a"
         Columns(17).Name=   "Desc.V�a"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(17).Locked=   -1  'True
         Columns(17).HasBackColor=   -1  'True
         Columns(17).BackColor=   12632256
         Columns(18).Width=   1588
         Columns(18).Caption=   "Volumen"
         Columns(18).Name=   "Volumen"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   8
         Columns(18).FieldLen=   256
         Columns(18).Locked=   -1  'True
         Columns(18).HasBackColor=   -1  'True
         Columns(18).BackColor=   12632256
         Columns(19).Width=   2381
         Columns(19).Caption=   "Tiempo Infusi�n"
         Columns(19).Name=   "Tiempo Infusi�n"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(19).Locked=   -1  'True
         Columns(19).HasBackColor=   -1  'True
         Columns(19).BackColor=   12632256
         Columns(20).Width=   2514
         Columns(20).Caption=   "C�d.Frecuencia"
         Columns(20).Name=   "C�d.Frecuencia"
         Columns(20).DataField=   "Column 20"
         Columns(20).DataType=   8
         Columns(20).FieldLen=   256
         Columns(20).Locked=   -1  'True
         Columns(20).HasBackColor=   -1  'True
         Columns(20).BackColor=   12632256
         Columns(21).Width=   1905
         Columns(21).Caption=   "Frecuencia"
         Columns(21).Name=   "Frecuencia"
         Columns(21).DataField=   "Column 21"
         Columns(21).DataType=   8
         Columns(21).FieldLen=   256
         Columns(21).Locked=   -1  'True
         Columns(21).HasBackColor=   -1  'True
         Columns(21).BackColor=   12632256
         Columns(22).Width=   1032
         Columns(22).Caption=   "PRN"
         Columns(22).Name=   "PRN"
         Columns(22).DataField=   "Column 22"
         Columns(22).DataType=   8
         Columns(22).FieldLen=   256
         Columns(22).Locked=   -1  'True
         Columns(22).HasBackColor=   -1  'True
         Columns(22).BackColor=   12632256
         Columns(23).Width=   873
         Columns(23).Caption=   "C.I"
         Columns(23).Name=   "C.I"
         Columns(23).DataField=   "Column 23"
         Columns(23).DataType=   8
         Columns(23).FieldLen=   256
         Columns(23).Locked=   -1  'True
         Columns(23).HasBackColor=   -1  'True
         Columns(23).BackColor=   12632256
         Columns(24).Width=   979
         Columns(24).Caption=   "S.N"
         Columns(24).Name=   "S.N"
         Columns(24).DataField=   "Column 24"
         Columns(24).DataType=   8
         Columns(24).FieldLen=   256
         Columns(24).Locked=   -1  'True
         Columns(24).HasBackColor=   -1  'True
         Columns(24).BackColor=   12632256
         Columns(25).Width=   1984
         Columns(25).Caption=   "Periodicidad"
         Columns(25).Name=   "Periodicidad"
         Columns(25).DataField=   "Column 25"
         Columns(25).DataType=   8
         Columns(25).FieldLen=   256
         Columns(25).Locked=   -1  'True
         Columns(25).HasBackColor=   -1  'True
         Columns(25).BackColor=   12632256
         Columns(26).Width=   1720
         Columns(26).Caption=   "Ubicaci�n"
         Columns(26).Name=   "Ubicaci�n"
         Columns(26).DataField=   "Column 26"
         Columns(26).DataType=   8
         Columns(26).FieldLen=   256
         Columns(26).Locked=   -1  'True
         Columns(26).HasBackColor=   -1  'True
         Columns(26).BackColor=   12632256
         Columns(27).Width=   1349
         Columns(27).Caption=   "PedOri"
         Columns(27).Name=   "PedOri"
         Columns(27).DataField=   "Column 27"
         Columns(27).DataType=   8
         Columns(27).FieldLen=   256
         Columns(27).Locked=   -1  'True
         Columns(27).HasBackColor=   -1  'True
         Columns(27).BackColor=   12632256
         Columns(28).Width=   1455
         Columns(28).Caption=   "AcOri"
         Columns(28).Name=   "AcOri"
         Columns(28).DataField=   "Column 28"
         Columns(28).DataType=   8
         Columns(28).FieldLen=   256
         Columns(28).Locked=   -1  'True
         Columns(28).HasBackColor=   -1  'True
         Columns(28).BackColor=   12632256
         Columns(29).Width=   1349
         Columns(29).Caption=   "C�d.Sec."
         Columns(29).Name=   "C�d.Sec."
         Columns(29).DataField=   "Column 29"
         Columns(29).DataType=   8
         Columns(29).FieldLen=   256
         Columns(29).Locked=   -1  'True
         Columns(29).HasBackColor=   -1  'True
         Columns(29).BackColor=   12632256
         Columns(30).Width=   3200
         Columns(30).Caption=   "Secci�n"
         Columns(30).Name=   "Secci�n"
         Columns(30).DataField=   "Column 30"
         Columns(30).DataType=   8
         Columns(30).FieldLen=   256
         Columns(30).Locked=   -1  'True
         Columns(30).HasBackColor=   -1  'True
         Columns(30).BackColor=   12632256
         _ExtentX        =   12674
         _ExtentY        =   3519
         _StockProps     =   79
      End
      Begin SSDataWidgets_B.SSDBGrid grdAux2 
         Height          =   5115
         Index           =   7
         Left            =   120
         TabIndex        =   16
         Top             =   360
         Width           =   10065
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   14
         stylesets.count =   2
         stylesets(0).Name=   "CambioSec"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0359.frx":001C
         stylesets(1).Name=   "CambioSec2"
         stylesets(1).BackColor=   12632256
         stylesets(1).Picture=   "FR0359.frx":0038
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   14
         Columns(0).Width=   1164
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   12632256
         Columns(1).Width=   1773
         Columns(1).Caption=   "Referencia"
         Columns(1).Name =   "Referencia"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   12632256
         Columns(2).Width=   4577
         Columns(2).Caption=   "Producto"
         Columns(2).Name =   "Producto"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   12632256
         Columns(3).Width=   741
         Columns(3).Caption=   "FF"
         Columns(3).Name =   "FF"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   12632256
         Columns(4).Width=   1164
         Columns(4).Caption=   "Dosis"
         Columns(4).Name =   "Dosis"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   12632256
         Columns(5).Width=   794
         Columns(5).Caption=   "UM"
         Columns(5).Name =   "UM"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   12632256
         Columns(6).Width=   1852
         Columns(6).Caption=   "Cant Ped"
         Columns(6).Name =   "Cant Ped"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   12632256
         Columns(7).Width=   2064
         Columns(7).Caption=   "Cant Disp"
         Columns(7).Name =   "Cant Disp"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   16776960
         Columns(8).Width=   1799
         Columns(8).Caption=   "Cant Ori"
         Columns(8).Name =   "Cant Ori"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   12632256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "CodProd"
         Columns(9).Name =   "CodProd"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).HasBackColor=   -1  'True
         Columns(9).BackColor=   12632256
         Columns(10).Width=   3200
         Columns(10).Caption=   "Ubicaci�n"
         Columns(10).Name=   "Ubicaci�n"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(10).HasBackColor=   -1  'True
         Columns(10).BackColor=   12632256
         Columns(11).Width=   1376
         Columns(11).Caption=   "C�d.Sec."
         Columns(11).Name=   "C�d.Sec."
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Locked=   -1  'True
         Columns(11).HasBackColor=   -1  'True
         Columns(11).BackColor=   12632256
         Columns(12).Width=   3200
         Columns(12).Caption=   "Secci�n"
         Columns(12).Name=   "Secci�n"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(12).HasBackColor=   -1  'True
         Columns(12).BackColor=   12632256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "Estilo"
         Columns(13).Name=   "Estilo"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(13).HasBackColor=   -1  'True
         Columns(13).BackColor=   12632256
         _ExtentX        =   17754
         _ExtentY        =   9022
         _StockProps     =   79
      End
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Ver Todos los Productos Pedidos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   255
      Left            =   7440
      TabIndex        =   20
      Top             =   1680
      Width           =   3255
   End
   Begin VB.Frame Frame5 
      Caption         =   "Sugerencias:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1335
      Left            =   6720
      TabIndex        =   17
      Top             =   720
      Width           =   4935
      Begin VB.Label Label1 
         Caption         =   "2.- Despu�s de sacar el listado active la casilla            ""Ver Todos Los Productos Pedidos"""
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   240
         TabIndex        =   19
         Top             =   480
         Width           =   4455
      End
      Begin VB.Label Label1 
         Caption         =   "1.- Saque el Listado de Preparaci�n de la Cesta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   18
         Top             =   240
         Width           =   4455
      End
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   11160
      Top             =   7440
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdAcumulado 
      Caption         =   "DISPENSAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   10560
      TabIndex        =   9
      Top             =   4800
      Width           =   1215
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "Anular"
      Height          =   615
      Left            =   10680
      TabIndex        =   8
      Top             =   6480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdmodificar 
      Caption         =   "Sustituir Producto"
      Height          =   615
      Left            =   10680
      TabIndex        =   7
      Top             =   5520
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdbloquear 
      Caption         =   "NO DISPENSAR"
      Height          =   615
      Left            =   10560
      TabIndex        =   6
      Top             =   3240
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   1230
         Index           =   0
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   2170
         _Version        =   327681
         TabOrientation  =   3
         Tabs            =   2
         TabsPerRow      =   1
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0359.frx":0054
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(17)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(16)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0359.frx":0070
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grddbgrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   16
            Left            =   240
            TabIndex        =   11
            Tag             =   "C�d.Departamento"
            Top             =   480
            Width           =   795
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   17
            Left            =   1080
            TabIndex        =   10
            Tag             =   "Desc.Departamento"
            Top             =   480
            Width           =   5280
         End
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   825
            Index           =   2
            Left            =   -74880
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   120
            Width           =   9375
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16536
            _ExtentY        =   1455
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   1080
            TabIndex        =   13
            Top             =   240
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   240
            TabIndex        =   12
            Top             =   240
            Width           =   405
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmEstPetCestasSec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmEstPetCestasSec(FR0359.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: estudiar petici�n farmacia                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim mblnGrabar As Boolean
Dim mstrPeticion As String
Dim mstrWhere As String
Dim blnInload As Boolean

Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function

Private Sub Imprimir(strListado As String, intDes As Integer)
'JMRL 19991125
'Toma como par�metro el listado a imprimir y lo manda a la impresora;
'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
Dim strWhere As String
Dim strDNS As String
Dim strUser As String
Dim strPass As String
Dim strPATH As String
Dim contSecciones As Integer
Dim i As Integer

  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  If strListado = "FR3592.RPT" Then
    strWhere = " {FR6603J.AD02CODDPTO}=" & txtText1(16).Text
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    If frmVisPRN.lvwSec.ListItems.Count > 0 Then
      If frmVisPRN.lvwSec.ListItems(1).Selected Then
      Else
        For i = 2 To frmVisPRN.lvwSec.ListItems.Count
          If frmVisPRN.lvwSec.ListItems(i).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              strWhere = strWhere & " AND ("
            Else
              strWhere = strWhere & " OR "
            End If
            If i = 2 Then
              strWhere = strWhere & "isnull({FR6603J.AD41CODSECCION})"
            Else
              strWhere = strWhere & "{FR6603J.AD41CODSECCION}=" & frmVisPRN.lvwSec.ListItems(i).Tag
            End If
          End If
        Next i
        If contSecciones > 0 Then
          strWhere = strWhere & ") "
        End If
      End If
    End If
    If gstrEstCesta = " AND  FR26CODESTPETIC=9  " Then
      strWhere = strWhere & " AND {FR6603J.FR26CODESTPETIC}=9 "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC IN (3,9) " Then
      strWhere = strWhere & " AND ({FR6603J.FR26CODESTPETIC}=3 OR {FR6603J.FR26CODESTPETIC}=9) "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=5 " Then
      Exit Sub
    ElseIf gstrEstCesta = " AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 WHERE FR28INDBLOQUEADA=-1) " Then
      Exit Sub
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=1 " Then
      strWhere = strWhere & " AND {FR6603J.FR26CODESTPETIC}=1 "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=3 " Then
      strWhere = strWhere & " AND {FR6603J.FR26CODESTPETIC}=3 "
    End If
  Else
    strWhere = " {FR6604J.AD02CODDPTO}=" & txtText1(16).Text
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    If frmVisPRN.lvwSec.ListItems.Count > 0 Then
      If frmVisPRN.lvwSec.ListItems(1).Selected Then
      Else
        For i = 2 To frmVisPRN.lvwSec.ListItems.Count
          If frmVisPRN.lvwSec.ListItems(i).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              strWhere = strWhere & " AND ("
            Else
              strWhere = strWhere & " OR "
            End If
            If i = 2 Then
              strWhere = strWhere & "isnull({FR6604J.AD41CODSECCION})"
            Else
              strWhere = strWhere & "{FR6604J.AD41CODSECCION}=" & frmVisPRN.lvwSec.ListItems(i).Tag
            End If
          End If
        Next i
        If contSecciones > 0 Then
          strWhere = strWhere & ") "
        End If
      End If
    End If
    If gstrEstCesta = " AND  FR26CODESTPETIC=9  " Then
      strWhere = strWhere & " AND {FR6604J.FR26CODESTPETIC}=9 "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC IN (3,9) " Then
      strWhere = strWhere & " AND ({FR6604J.FR26CODESTPETIC}=3 OR {FR6604J.FR26CODESTPETIC}=9) "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=5 " Then
      Exit Sub
    ElseIf gstrEstCesta = " AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 WHERE FR28INDBLOQUEADA=-1) " Then
      Exit Sub
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=1 " Then
      strWhere = strWhere & " AND {FR6604J.FR26CODESTPETIC}=1 "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=3 " Then
      strWhere = strWhere & " AND {FR6604J.FR26CODESTPETIC}=3 "
    End If
  End If
             
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1
Err_imp1:
  
End Sub


Private Sub Check1_Click()

  Call Refrescar_Grid

End Sub

Private Sub cmdAcept_Click()
Dim i As Integer
Dim resto As Double
Dim codigo As Variant
Dim strupdateFR28 As String
Dim qryupdateFR28 As rdoQuery
Dim strupdateFR66 As String
Dim strUpd As String
Dim qryUpd As rdoQuery
Dim contSecciones As Integer
Dim k As Integer
Dim strAlmOri As String
Dim strAlmDes As String
Dim strdes As String
Dim rstdes As rdoResultset
Dim qrydes As rdoQuery
Dim strori As String
Dim rstori As rdoResultset
Dim qryori As rdoQuery
Dim strIns80 As String
Dim strIns35 As String

On Error GoTo err:


  Frame1.Visible = False
  
  If Check2.Value = 0 And Check3.Value = 0 Then
    Call MsgBox("Debe elegir por lo menos un listado.", vbInformation, "Aviso")
    Exit Sub
  End If

  Screen.MousePointer = vbHourglass
  cmdAcumulado.Enabled = False
  Me.Enabled = False
  If gstrLlamadorProd <> "Cesta" Then
    If frmVisPRN.auxDBGrid1(0).Columns("C�d.Estado").Value = 1 Then
      Call MsgBox("El PRN est� en estado de Redactado", vbInformation, "Aviso")
      cmdAcumulado.Enabled = True
      Screen.MousePointer = vbDefault
      Me.Enabled = True
      Exit Sub
    End If
  End If
  If grdAux1(0).Rows > 0 Then
    'grdAux1(0).Redraw = False
    grdAux2(7).Redraw = False
    grdAux2(7).MoveFirst
    For i = 0 To grdAux1(0).Rows - 1
      If IsNumeric(grdAux2(7).Columns("Cant Disp").Value) = False Then
        Call MsgBox("Datos Incorrectos", vbInformation, "Aviso")
        'grdAux1(0).Redraw = True
        grdAux2(7).Redraw = True
        Screen.MousePointer = vbDefault
        cmdAcumulado.Enabled = True
        Me.Enabled = True
        grdAux2(7).Col = 7
        Exit Sub
      End If
      If CCur(grdAux2(7).Columns("Cant Disp").Value) > CCur(grdAux2(7).Columns("Cant Ped").Value) Then
        Call MsgBox("No se puede dispensar mas de lo que se ha pedido", vbInformation, "Aviso")
        'grdAux1(0).Redraw = True
        grdAux2(7).Redraw = True
        Screen.MousePointer = vbDefault
        cmdAcumulado.Enabled = True
        Me.Enabled = True
        grdAux2(7).Col = 7
        Exit Sub
      End If
      grdAux2(7).MoveNext
    Next i
    
    strupdateFR28 = "UPDATE FR2800 SET FR28CANTDISP=TO_NUMBER(?) WHERE FR66CODPETICION=? AND FR28NUMLINEA=?"
    Set qryupdateFR28 = objApp.rdoConnect.CreateQuery("", strupdateFR28)
    
    grdAux1(0).MoveLast
    grdAux2(7).MoveLast
    codigo = 0
    resto = 0
    For i = 0 To grdAux1(0).Rows - 1
      If codigo <> grdAux1(0).Columns("C�d.Prod").Value Then
        grdAux1(0).Columns("Ac Disp").Value = grdAux2(7).Columns("Cant Disp").Value
        grdAux1(0).Columns("Can Disp").Value = grdAux1(0).Columns("Ac Disp").Value
        codigo = grdAux1(0).Columns("C�d.Prod").Value
        If CCur(grdAux1(0).Columns("Can Disp").Value) > CCur(grdAux1(0).Columns("Can Ped").Value) Then
          resto = grdAux1(0).Columns("Can Disp").Value - grdAux1(0).Columns("Can Ped").Value
          grdAux1(0).Columns("Can Disp").Value = grdAux1(0).Columns("Can Ped").Value
        Else
          resto = 0
        End If
        qryupdateFR28(0) = grdAux1(0).Columns("Can Disp").Value
        qryupdateFR28(1) = grdAux1(0).Columns("Pet").Value
        qryupdateFR28(2) = grdAux1(0).Columns("L�nea").Value
        qryupdateFR28.Execute
        
        grdAux1(0).MovePrevious
        grdAux2(7).MovePrevious
      Else
        grdAux1(0).Columns("Can Disp").Value = resto
        If CCur(grdAux1(0).Columns("Can Disp").Value) > CCur(grdAux1(0).Columns("Can Ped").Value) Then
          resto = grdAux1(0).Columns("Can Disp").Value - grdAux1(0).Columns("Can Ped").Value
          grdAux1(0).Columns("Can Disp").Value = grdAux1(0).Columns("Can Ped").Value
        Else
          resto = 0
        End If
        qryupdateFR28(0) = grdAux1(0).Columns("Can Disp").Value
        qryupdateFR28(1) = grdAux1(0).Columns("Pet").Value
        qryupdateFR28(2) = grdAux1(0).Columns("L�nea").Value
        qryupdateFR28.Execute
        
        grdAux1(0).MovePrevious
      End If
    Next i
    'grdAux1(0).Redraw = True
    grdAux2(7).Redraw = True
    'Screen.MousePointer = vbDefault
    If Check2.Value = 1 Then
      Call Imprimir("FR3592.RPT", 1)
    End If
    If Check3.Value = 1 Then
      Call Imprimir("FR3594.RPT", 1)
    End If
  End If


  'Movimientos de Almacen
  'se obtiene el almac�n que pide el producto
  strdes = "SELECT FR04CODALMACEN FROM FR0400 WHERE AD02CODDPTO=?"
  strdes = strdes & " AND FR0400.FR04INDPRINCIPAL=?"
  Set qrydes = objApp.rdoConnect.CreateQuery("", strdes)
  qrydes(0) = txtText1(16).Text
  qrydes(1) = -1
  Set rstdes = qrydes.OpenResultset()
  If Not rstdes.EOF Then
    strAlmDes = rstdes(0).Value
  Else
    strAlmDes = "999"
  End If
  rstdes.Close
  Set rstdes = Nothing
  qrydes.Close
  Set qrydes = Nothing
  
  'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
  strori = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=?"
  Set qryori = objApp.rdoConnect.CreateQuery("", strori)
  qryori(0) = 28
  Set rstori = qryori.OpenResultset()
  strAlmOri = rstori(0).Value
  
  'Se realiza rl insert en FR8000: salidas de alamc�n
  strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV," & _
              "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) "
  strIns80 = strIns80 & "SELECT FR80NUMMOV_SEQUENCE.nextval,"
  strIns80 = strIns80 & strAlmOri & "," & strAlmDes & ",7,SYSDATE,"
  strIns80 = strIns80 & "FR73CODPRODUCTO,FR28CANTDISP,1,'NE'"
  strIns80 = strIns80 & " FROM FR2800 WHERE FR66CODPETICION IN "
  strIns80 = strIns80 & " (SELECT FR66CODPETICION FROM FR6600"
  strIns80 = strIns80 & "  WHERE FR66INDOM=0 AND FR66INDCESTA=-1 "
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmVisPRN.lvwSec.ListItems.Count > 0 Then
    If frmVisPRN.lvwSec.ListItems(1).Selected Then
    Else
      For k = 2 To frmVisPRN.lvwSec.ListItems.Count
        If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strIns80 = strIns80 & " AND ("
          Else
            strIns80 = strIns80 & " OR "
          End If
          If k = 2 Then
            strIns80 = strIns80 & "FR6600.AD41CODSECCION IS NULL"
          Else
            strIns80 = strIns80 & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
          End If
        End If
      Next k
      If contSecciones > 0 Then
        strIns80 = strIns80 & ") "
      End If
    End If
  End If
  strIns80 = strIns80 & gstrEstCesta & " AND AD02CODDPTO=" & txtText1(16).Text
  strIns80 = strIns80 & ")"
  strIns80 = strIns80 & " AND FR28CANTDISP<>0 AND FR28INDBLOQUEADA<>-1"
  objApp.rdoConnect.Execute strIns80, 64
  
  'Se realiza rl insert en FR8000: salidas de alamc�n
  strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV," & _
              "FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) "
  strIns35 = strIns35 & "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval,"
  strIns35 = strIns35 & strAlmOri & "," & strAlmDes & ",7,SYSDATE,"
  strIns35 = strIns35 & "FR73CODPRODUCTO,FR28CANTDISP,1,'NE'"
  strIns35 = strIns35 & " FROM FR2800 WHERE FR66CODPETICION IN "
  strIns35 = strIns35 & " (SELECT FR66CODPETICION FROM FR6600"
  strIns35 = strIns35 & "  WHERE FR66INDOM=0 AND FR66INDCESTA=-1 "
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmVisPRN.lvwSec.ListItems.Count > 0 Then
    If frmVisPRN.lvwSec.ListItems(1).Selected Then
    Else
      For k = 2 To frmVisPRN.lvwSec.ListItems.Count
        If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strIns35 = strIns35 & " AND ("
          Else
            strIns35 = strIns35 & " OR "
          End If
          If k = 2 Then
            strIns35 = strIns35 & "FR6600.AD41CODSECCION IS NULL"
          Else
            strIns35 = strIns35 & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
          End If
        End If
      Next k
      If contSecciones > 0 Then
        strIns35 = strIns35 & ") "
      End If
    End If
  End If
  strIns35 = strIns35 & gstrEstCesta & " AND AD02CODDPTO=" & txtText1(16).Text
  strIns35 = strIns35 & ")"
  strIns35 = strIns35 & " AND FR28CANTDISP<>0 AND FR28INDBLOQUEADA<>-1"
  objApp.rdoConnect.Execute strIns35, 64

  'Se descuenta de la cantidad pedida  la dispensada, y la dispensada se pone a 0
  strUpd = "UPDATE FR2800 "
  strUpd = strUpd & " SET FR28CANTIDAD = FR28CANTIDAD - FR28CANTDISP , FR28CANTDISP = ? "
  'strUpd = strUpd & " WHERE " & CrearWhere("FR66CODPETICION", mstrPeticion)
  strUpd = strUpd & " WHERE FR66CODPETICION IN "
  strUpd = strUpd & " (SELECT FR66CODPETICION FROM FR6600"
  strUpd = strUpd & "  WHERE FR66INDOM=0 AND FR66INDCESTA=-1 "
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmVisPRN.lvwSec.ListItems.Count > 0 Then
    If frmVisPRN.lvwSec.ListItems(1).Selected Then
    Else
      For k = 2 To frmVisPRN.lvwSec.ListItems.Count
        If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strUpd = strUpd & " AND ("
          Else
            strUpd = strUpd & " OR "
          End If
          If k = 2 Then
            strUpd = strUpd & "FR6600.AD41CODSECCION IS NULL"
          Else
            strUpd = strUpd & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
          End If
        End If
      Next k
      If contSecciones > 0 Then
        strUpd = strUpd & ") "
      End If
    End If
  End If
  strUpd = strUpd & gstrEstCesta & " AND AD02CODDPTO=" & txtText1(16).Text
  strUpd = strUpd & ")"
  
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
  qryUpd(0) = 0
  qryUpd.Execute

  strupdateFR66 = "UPDATE FR6600 SET FR66FECDISPEN=SYSDATE,FR26CODESTPETIC=5 WHERE FR66INDCESTA=-1"
  strupdateFR66 = strupdateFR66 & " AND AD02CODDPTO=" & txtText1(16).Text
  strupdateFR66 = strupdateFR66 & " AND FR66CODPETICION NOT IN "
  strupdateFR66 = strupdateFR66 & "(SELECT FR66CODPETICION "
  strupdateFR66 = strupdateFR66 & " FROM FR2800 WHERE "
  strupdateFR66 = strupdateFR66 & " FR28CANTDISP<>FR28CANTIDAD"
  strupdateFR66 = strupdateFR66 & " AND FR28INDBLOQUEADA=0"
  strupdateFR66 = strupdateFR66 & " AND FR66CODPETICION IN "
  strupdateFR66 = strupdateFR66 & "  (SELECT FR66CODPETICION "
  strupdateFR66 = strupdateFR66 & "   FROM FR6600 "
  strupdateFR66 = strupdateFR66 & "   WHERE FR66INDOM=0 AND FR66INDCESTA=-1 "
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmVisPRN.lvwSec.ListItems.Count > 0 Then
    If frmVisPRN.lvwSec.ListItems(1).Selected Then
    Else
      For k = 2 To frmVisPRN.lvwSec.ListItems.Count
        If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strupdateFR66 = strupdateFR66 & " AND ("
          Else
            strupdateFR66 = strupdateFR66 & " OR "
          End If
          If k = 2 Then
            strupdateFR66 = strupdateFR66 & "FR6600.AD41CODSECCION IS NULL"
          Else
            strupdateFR66 = strupdateFR66 & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
          End If
        End If
      Next k
      If contSecciones > 0 Then
        strupdateFR66 = strupdateFR66 & ") "
      End If
    End If
  End If
  strupdateFR66 = strupdateFR66 & gstrEstCesta & ")"
  strupdateFR66 = strupdateFR66 & ") AND FR66INDOM=0 AND FR66INDCESTA=-1 " & gstrEstCesta
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmVisPRN.lvwSec.ListItems.Count > 0 Then
    If frmVisPRN.lvwSec.ListItems(1).Selected Then
    Else
      For k = 2 To frmVisPRN.lvwSec.ListItems.Count
        If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strupdateFR66 = strupdateFR66 & " AND ("
          Else
            strupdateFR66 = strupdateFR66 & " OR "
          End If
          If k = 2 Then
            strupdateFR66 = strupdateFR66 & "FR6600.AD41CODSECCION IS NULL"
          Else
            strupdateFR66 = strupdateFR66 & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
          End If
        End If
      Next k
      If contSecciones > 0 Then
        strupdateFR66 = strupdateFR66 & ") "
      End If
    End If
  End If
  objApp.rdoConnect.Execute strupdateFR66, 64
  
  strupdateFR66 = "UPDATE FR6600 SET FR66FECDISPEN=SYSDATE,FR26CODESTPETIC=9 WHERE FR66INDCESTA=-1"
  strupdateFR66 = strupdateFR66 & " AND AD02CODDPTO=" & txtText1(16).Text
  strupdateFR66 = strupdateFR66 & " AND FR66CODPETICION IN "
  strupdateFR66 = strupdateFR66 & "(SELECT FR66CODPETICION "
  strupdateFR66 = strupdateFR66 & " FROM FR2800 WHERE "
  strupdateFR66 = strupdateFR66 & " FR28CANTDISP<>FR28CANTIDAD"
  strupdateFR66 = strupdateFR66 & " AND FR28INDBLOQUEADA=0"
  strupdateFR66 = strupdateFR66 & " AND FR66CODPETICION IN "
  strupdateFR66 = strupdateFR66 & "  (SELECT FR66CODPETICION "
  strupdateFR66 = strupdateFR66 & "   FROM FR6600 "
  strupdateFR66 = strupdateFR66 & "   WHERE FR66INDOM=0 AND FR66INDCESTA=-1 "
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmVisPRN.lvwSec.ListItems.Count > 0 Then
    If frmVisPRN.lvwSec.ListItems(1).Selected Then
    Else
      For k = 2 To frmVisPRN.lvwSec.ListItems.Count
        If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strupdateFR66 = strupdateFR66 & " AND ("
          Else
            strupdateFR66 = strupdateFR66 & " OR "
          End If
          If k = 2 Then
            strupdateFR66 = strupdateFR66 & "FR6600.AD41CODSECCION IS NULL"
          Else
            strupdateFR66 = strupdateFR66 & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
          End If
        End If
      Next k
      If contSecciones > 0 Then
        strupdateFR66 = strupdateFR66 & ") "
      End If
    End If
  End If
  strupdateFR66 = strupdateFR66 & gstrEstCesta & ")"
  strupdateFR66 = strupdateFR66 & ") AND FR66INDOM=0 AND FR66INDCESTA=-1 " & gstrEstCesta
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmVisPRN.lvwSec.ListItems.Count > 0 Then
    If frmVisPRN.lvwSec.ListItems(1).Selected Then
    Else
      For k = 2 To frmVisPRN.lvwSec.ListItems.Count
        If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strupdateFR66 = strupdateFR66 & " AND ("
          Else
            strupdateFR66 = strupdateFR66 & " OR "
          End If
          If k = 2 Then
            strupdateFR66 = strupdateFR66 & "FR6600.AD41CODSECCION IS NULL"
          Else
            strupdateFR66 = strupdateFR66 & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
          End If
        End If
      Next k
      If contSecciones > 0 Then
        strupdateFR66 = strupdateFR66 & ") "
      End If
    End If
  End If
  objApp.rdoConnect.Execute strupdateFR66, 64
  
  
  Call Refrescar_Grid
  cmdAcumulado.Enabled = True
  Screen.MousePointer = vbDefault
  Call MsgBox("Se ha realizado la Dispensaci�n", vbExclamation, "Aviso")
  Me.Enabled = True
  Exit Sub

err:
  Me.Enabled = True
  Screen.MousePointer = vbDefault
  'grdAux1(0).Redraw = True
  grdAux2(7).Redraw = True
  Call MsgBox("No se ha podido realizar los acumulados", vbExclamation, "Aviso")
  cmdAcumulado.Enabled = True

End Sub

Private Sub cmdAcumulado_Click()
  Frame1.Visible = True
End Sub


Private Sub cmdAnular_Click()
Dim intMsg As Integer
Dim strupdate As String
On Error GoTo err
    cmdAnular.Enabled = False
  If gstrLlamadorProd <> "Cesta" Then
    If frmVisPRN.auxDBGrid1(0).Columns("C�d.Estado").Value = 1 Then
      Call MsgBox("El PRN est� en estado de Redactado", vbInformation, "Aviso")
      cmdAnular.Enabled = True
      Exit Sub
    End If
  End If
    If txtText1(0).Text <> "" Then
        intMsg = MsgBox("�Est� seguro que desea Anular la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=8 WHERE FR66CODPETICION=" & txtText1(0).Text
            objApp.rdoConnect.Execute strupdate, 64
            objWinInfo.DataRefresh
        End If
    End If
err:
    cmdAnular.Enabled = True
End Sub



Private Sub grdAux2_KeyPress(Index As Integer, KeyAscii As Integer)

Select Case KeyAscii
Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
Case Else
  KeyAscii = 0
End Select

End Sub



Private Sub grdAux2_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer

  If grdAux2(7).Columns("Estilo").Value = "CambioSec" Then
    For i = 0 To grdAux2(7).Cols - 1
      If i <> 7 Then
        grdAux2(7).Columns(i).CellStyleSet "CambioSec"
      End If
    Next i
  ElseIf grdAux2(7).Columns("Estilo").Value = "CambioSec2" Then
    For i = 0 To grdAux2(7).Cols - 1
      If i <> 7 Then
        grdAux2(7).Columns(i).CellStyleSet "CambioSec2"
      End If
    Next i
  End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
On Error GoTo err
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
err:
End Sub


Private Sub cmdbloquear_Click()
Dim qryupdate As rdoQuery
Dim strupdate As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim sqlstr As String
Dim intMsg As Integer
Dim nTotalSelRows As Integer
Dim strUpd As String
Dim qryUpd As rdoQuery
Dim i As Integer
Dim bkmrk As Variant
Dim strFR28 As String
Dim qryFR28 As rdoQuery
Dim rdoFR28 As rdoResultset
Dim strFR66 As String
Dim qryFR66 As rdoQuery
Dim rdoFR66 As rdoResultset
Dim contSecciones As Integer
Dim k As Integer
    
  intMsg = MsgBox("�Est� seguro que NO desea DISPENSAR estos productos?", vbQuestion + vbYesNo)
  If intMsg = vbNo Then
    Exit Sub
  End If
    
  cmdbloquear.Enabled = False
  Screen.MousePointer = vbHourglass
  Me.Enabled = False
  nTotalSelRows = grdAux2(7).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=?"
    strupdate = strupdate & " WHERE "
    strupdate = strupdate & " FR73CODPRODUCTO=?"
    strupdate = strupdate & " AND FR66CODPETICION IN "
    strupdate = strupdate & " (SELECT FR66CODPETICION FROM FR6600 "
    strupdate = strupdate & " WHERE FR6600.FR66INDOM=0"
    strupdate = strupdate & " AND FR6600.FR66INDCESTA=-1"
    strupdate = strupdate & " AND FR6600.AD02CODDPTO=?"
    strupdate = strupdate & " " & gstrEstCesta
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    If frmVisPRN.lvwSec.ListItems.Count > 0 Then
      If frmVisPRN.lvwSec.ListItems(1).Selected Then
      Else
        For k = 2 To frmVisPRN.lvwSec.ListItems.Count
          If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              strupdate = strupdate & " AND ("
            Else
              strupdate = strupdate & " OR "
            End If
            If k = 2 Then
              strupdate = strupdate & "FR6600.AD41CODSECCION IS NULL"
            Else
              strupdate = strupdate & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
            End If
          End If
        Next k
        If contSecciones > 0 Then
          strupdate = strupdate & ") "
        End If
      End If
    End If
    strupdate = strupdate & " ) "
    If gstrEstCesta = " AND  FR26CODESTPETIC=9  " Then
      strupdate = strupdate & " AND FR2800.FR28CANTIDAD > 0 "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC IN (3,9) " Then
      strupdate = strupdate & " AND FR2800.FR28CANTIDAD > 0 "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=5 " Then
    ElseIf gstrEstCesta = " AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 WHERE FR28INDBLOQUEADA=-1) " Then
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=1 " Then
      strupdate = strupdate & " AND FR2800.FR28CANTIDAD > 0 "
    ElseIf gstrEstCesta = " AND FR26CODESTPETIC=3 " Then
      strupdate = strupdate & " AND FR2800.FR28CANTIDAD > 0 "
    End If
    strupdate = strupdate & " AND FR28INDBLOQUEADA=0"
    Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
    
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdAux2(7).SelBookmarks(i)
      qryupdate(0) = -1
      qryupdate(1) = grdAux2(7).Columns("CodProd").CellValue(bkmrk)
      qryupdate(2) = gstrCodDpto
      qryupdate.Execute
    Next i
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    strUpd = "UPDATE FR6600 SET FR26CODESTPETIC=? "
    strUpd = strUpd & " WHERE FR66CODPETICION=? "
    strUpd = strUpd & " AND FR26CODESTPETIC<>? "
    Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
    
    strFR28 = "SELECT COUNT(*) FROM FR2800 "
    strFR28 = strFR28 & " WHERE FR66CODPETICION=?"
    strFR28 = strFR28 & " AND FR28INDBLOQUEADA=? "
    strFR28 = strFR28 & " AND FR2800.FR28CANTIDAD > 0 "
    Set qryFR28 = objApp.rdoConnect.CreateQuery("", strFR28)
    
    strFR66 = "SELECT * FROM FR6600 "
    strFR66 = strFR66 & " WHERE FR6600.FR66INDOM=0"
    strFR66 = strFR66 & " AND FR6600.FR66INDCESTA=-1"
    strFR66 = strFR66 & " AND FR6600.AD02CODDPTO=?"
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    If frmVisPRN.lvwSec.ListItems.Count > 0 Then
      If frmVisPRN.lvwSec.ListItems(1).Selected Then
      Else
        For k = 2 To frmVisPRN.lvwSec.ListItems.Count
          If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              strFR66 = strFR66 & " AND ("
            Else
              strFR66 = strFR66 & " OR "
            End If
            If k = 2 Then
              strFR66 = strFR66 & "FR6600.AD41CODSECCION IS NULL"
            Else
              strFR66 = strFR66 & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
            End If
          End If
        Next k
        If contSecciones > 0 Then
          strFR66 = strFR66 & ") "
        End If
      End If
    End If
    strFR66 = strFR66 & " " & gstrEstCesta
    Set qryFR66 = objApp.rdoConnect.CreateQuery("", strFR66)
    qryFR66(0) = gstrCodDpto
    Set rdoFR66 = qryFR66.OpenResultset()
    While Not rdoFR66.EOF
      qryFR28(0) = rdoFR66("FR66CODPETICION").Value
      qryFR28(1) = 0
      Set rdoFR28 = qryFR28.OpenResultset()
      If rdoFR28(0).Value = 0 Then
        If gstrEstCesta = " AND FR26CODESTPETIC=1 " Then
          qryUpd(0) = 8
          qryUpd(1) = rdoFR66("FR66CODPETICION").Value
          qryUpd(2) = 8
          qryUpd.Execute
        ElseIf gstrEstCesta = " AND FR26CODESTPETIC=3 " Then
          qryUpd(0) = 8
          qryUpd(1) = rdoFR66("FR66CODPETICION").Value
          qryUpd(2) = 8
          qryUpd.Execute
        Else
          qryUpd(0) = 5
          qryUpd(1) = rdoFR66("FR66CODPETICION").Value
          qryUpd(2) = 5
          qryUpd.Execute
        End If
      Else
        If gstrEstCesta = " AND FR26CODESTPETIC=1 " Then
        ElseIf gstrEstCesta = " AND FR26CODESTPETIC=3 " Then
        Else
          qryUpd(0) = 9
          qryUpd(1) = rdoFR66("FR66CODPETICION").Value
          qryUpd(2) = 9
          qryUpd.Execute
        End If
      End If
      rdoFR66.MoveNext
    Wend
    qryFR66.Close
    Set qryFR66 = Nothing
    Set rdoFR66 = Nothing
  End If
  Call Refrescar_Grid
  Screen.MousePointer = vbDefault
  Me.Enabled = True
  cmdbloquear.Enabled = True

End Sub

Private Sub cmdmodificar_Click()

Dim mensaje As String
Dim strInsert As String
Dim rsta As rdoResultset
Dim stra As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Long
Dim strupdate As String
Dim strprod As String
Dim rstprod As rdoResultset
Dim v As Integer
On Error GoTo err

  If (txtText1(25).Text = 8) Or (txtText1(25).Text = 5) Then 'Anulada o dispensada total
      Exit Sub
  End If
cmdmodificar.Enabled = False
  If gstrLlamadorProd <> "Cesta" Then
    If frmVisPRN.auxDBGrid1(0).Columns("C�d.Estado").Value = 1 Then
      Call MsgBox("El PRN est� en estado de Redactado", vbInformation, "Aviso")
      cmdmodificar.Enabled = True
      Exit Sub
    End If
  End If
If grdAux1(0).Rows > 0 Then
  If txtText1(25).Text = 4 Then 'orden validada
      mensaje = MsgBox("La Orden M�dica ya est� validada." & Chr(13) & _
                      "No puede modificar sus productos.", vbInformation, "Aviso")
  Else
    'se mira que el producto no sea estupefaciente
    strprod = "SELECT FR73INDESTUPEFACI FROM FR7300 WHERE " & _
              "FR73CODPRODUCTO=" & grdAux1(0).Columns("C�d.Prod").Value
    Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
    If rstprod.rdoColumns(0).Value = -1 Then
        mensaje = MsgBox("El producto es un estupefaciente." & Chr(13) & _
            "No puede modificar la l�nea.", vbInformation, "Aviso")
        rstprod.Close
        Set rstprod = Nothing
    Else 'no es estupefaciente
        rstprod.Close
        Set rstprod = Nothing
        'se mira si la l�nea est� bloqueada
        strprod = "SELECT FR28INDBLOQUEADA FROM FR2800 WHERE FR66CODPETICION=" & grdAux1(0).Columns("Pet").Value & _
          " AND FR28NUMLINEA=" & grdAux1(0).Columns("L�nea").Value & _
          " AND FR73CODPRODUCTO=" & grdAux1(0).Columns("C�d.Prod").Value
        Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
        If rstprod.rdoColumns(0).Value = -1 Then
          mensaje = MsgBox("La l�nea est� bloqueada. No puede modificarse", vbInformation, "Aviso")
          rstprod.Close
          Set rstprod = Nothing
        Else 'la l�nea no est� bloqueada
          rstprod.Close
          Set rstprod = Nothing
          Call objsecurity.LaunchProcess("FR0150")
          If gintprodtotal > 0 Then
            If gintprodtotal > 1 Then
              MsgBox "Debe traer s�lo 1 producto."
            Else
            'hay que crear una nueva l�nea id�ntica a la seleccionada
              strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                          grdAux1(0).Columns("Pet").Value
              Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
              If IsNull(rstlinea.rdoColumns(0).Value) = False Then
                    linea = 1 + rstlinea(0).Value
              Else
                linea = 1
              End If
              gintlinea = linea
              rstlinea.Close
              Set rstlinea = Nothing

              stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & grdAux1(0).Columns("Pet").Value & _
                    " AND FR28NUMLINEA=" & grdAux1(0).Columns("L�nea").Value & _
                    " AND FR73CODPRODUCTO=" & grdAux1(0).Columns("C�d.Prod").Value
              Set rsta = objApp.rdoConnect.OpenResultset(stra)
       
              strInsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,FR34CODVIA," & _
                "FR28DOSIS,FR28VOLUMEN," & _
                "FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR28OPERACION," & _
                "FR28CANTIDAD,FR28CANTDISP) VALUES " & _
                "(" & grdAux1(0).Columns("Pet").Value & "," & _
                linea & "," & _
                gintprodbuscado(v, 0) & ","
          
          
              If gintprodbuscado(v, 7) = "" Then
                  strInsert = strInsert & "null" & ","
              Else
                  strInsert = strInsert & "'" & gintprodbuscado(v, 7) & "'" & ","
              End If
              If gintprodbuscado(v, 3) = "" Then
                  strInsert = strInsert & "null" & ","
              Else
                  strInsert = strInsert & objGen.ReplaceStr(gintprodbuscado(v, 3), ",", ".", 1) & ","
              End If
              If gintprodbuscado(v, 6) = "" Then
                  strInsert = strInsert & "null" & ","
              Else
                  strInsert = strInsert & objGen.ReplaceStr(gintprodbuscado(v, 6), ",", ".", 1) & ","
              End If
              If gintprodbuscado(v, 4) = "" Then
                  strInsert = strInsert & "null" & ","
              Else
                  strInsert = strInsert & "'" & gintprodbuscado(v, 4) & "',"
              End If
              If gintprodbuscado(v, 5) = "" Then
                  strInsert = strInsert & "null" & ",'/'," & _
                  grdAux1(0).Columns("Can Ped").Value & "," & grdAux1(0).Columns("Can Disp").Value & ")"
              Else
                  strInsert = strInsert & "'" & gintprodbuscado(v, 5) & "'" & ",'/'," & _
                  grdAux1(0).Columns("Can Ped").Value & "," & grdAux1(0).Columns("Can Disp").Value & ")"
              End If
              objApp.rdoConnect.Execute strInsert, 64
              objApp.rdoConnect.Execute "Commit", 64
              rsta.Close
              Set rsta = Nothing
              'se bloquea la l�nea seleccionada
              strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1," & _
                  "FR28FECBLOQUEO=(SELECT SYSDATE FROM DUAL)," & _
                  "SG02CODPERSBLOQ='" & objsecurity.strUser & "'" & _
                  " WHERE FR66CODPETICION=" & grdAux1(0).Columns("Pet").Value & _
                  " AND FR28NUMLINEA=" & grdAux1(0).Columns("L�nea").Value & _
                  " AND FR73CODPRODUCTO=" & grdAux1(0).Columns("C�d.Prod").Value
              objApp.rdoConnect.Execute strupdate, 64
              objApp.rdoConnect.Execute "Commit", 64
              Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
              objWinInfo.DataRefresh
            End If
          End If
        End If
    End If
  End If
End If
err:
cmdmodificar.Enabled = True
End Sub

Private Sub Form_Activate()

If blnInload Then
  blnInload = False
  tabTab1(0).TabVisible(1) = False
  tabTab1(0).TabCaption(0) = ""

  If gstrEstCesta = " AND  FR26CODESTPETIC=9  " Then
    cmdAcumulado.Enabled = True
    cmdbloquear.Enabled = True
  ElseIf gstrEstCesta = " AND FR26CODESTPETIC IN (3,9) " Then
    cmdAcumulado.Enabled = True
    cmdbloquear.Enabled = True
  ElseIf gstrEstCesta = " AND FR26CODESTPETIC=5 " Then
    cmdAcumulado.Enabled = False
    cmdbloquear.Enabled = False
  ElseIf gstrEstCesta = " AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 WHERE FR28INDBLOQUEADA=-1) " Then
    cmdAcumulado.Enabled = False
    cmdbloquear.Enabled = False
  ElseIf gstrEstCesta = " AND FR26CODESTPETIC=1 " Then
    cmdAcumulado.Enabled = False
    cmdbloquear.Enabled = True
  ElseIf gstrEstCesta = " AND FR26CODESTPETIC=3 " Then
    cmdAcumulado.Enabled = True
    cmdbloquear.Enabled = True
  End If
  
  Screen.MousePointer = vbDefault
End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String
'Dim rsta As rdoResultset
'Dim stra As String
'Dim strPeticiones As String
'Dim intPeticiones As Integer
Dim contSecciones As Integer
Dim k As Integer
  
'  On Error GoTo err
  Set objWinInfo = New clsCWWin
  'strPeticiones = "()"
  blnInload = True
  
  'If gstrLlamadorProd = "Cesta" Then
  '  stra = "select fr66codpeticion from fr6600 where " & _
  '            "fr66indcesta=-1 " & _
  '            "and AD02CODDPTO=" & gstrCodDpto & gstrEstCesta
  '
  '  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  '  strPeticiones = "("
  '  intPeticiones = 0
  '  While rsta.EOF = False
  '    If strPeticiones <> "(" Then
  '      strPeticiones = strPeticiones & ","
  '    End If
  '    strPeticiones = strPeticiones & rsta(0).Value
  '    rsta.MoveNext
  '  Wend
  '  strPeticiones = strPeticiones & ")"
  '  rsta.Close
  '  Set rsta = Nothing
  'End If
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grddbgrid1(2)
    
    .strName = "PRN"
      
    .strTable = "FR6600"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR66CODPETICION", cwAscending)
    
    Call .objPrinter.Add("FR3591", "Preparaci�n de Cesta por Secciones")
    Call .objPrinter.Add("FR3592", "Dispensaci�n de Cesta Por Paciente por Secciones")
    Call .objPrinter.Add("FR3594", "Dispensaci�n de Cesta Sin Paciente por Secciones")
    Call .objPrinter.Add("FR3595", "Faltas por Secciones")
    
    'If gstrLlamadorProd = "Cesta" Then
    '  If strPeticiones = "()" Then
    '    .strWhere = "FR66CODPETICION is null"
    '  Else
    '    .strWhere = CrearWhere("FR66CODPETICION", strPeticiones)
    '  End If
    'Else
    '  .strWhere = "FR66CODPETICION=" & glngpeticion
    'End If
    'mstrPeticion = strPeticiones
    
    .strWhere = " fr66indom=0 and fr66indcesta=-1 " & _
      "and AD02CODDPTO=" & gstrCodDpto & gstrEstCesta
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    If frmVisPRN.lvwSec.ListItems.Count > 0 Then
      If frmVisPRN.lvwSec.ListItems(1).Selected Then
      Else
        For k = 2 To frmVisPRN.lvwSec.ListItems.Count
          If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              .strWhere = .strWhere & " AND ("
            Else
              .strWhere = .strWhere & " OR "
            End If
            If k = 2 Then
              .strWhere = .strWhere & "FR6600.AD41CODSECCION IS NULL"
            Else
              .strWhere = .strWhere & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
            End If
          End If
        Next k
        If contSecciones > 0 Then
          .strWhere = .strWhere & ") "
        End If
      End If
    End If
    
    strKey = .strDataBase & .strTable
  
  End With
  
'  With objMultiInfo
'    Set .objFormContainer = fraFrame1(0)
'    Set .objFatherContainer = fraFrame1(1)
'    Set .tabMainTab = Nothing
'    Set .grdGrid = grdAux1(0)
'    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
'    .strName = "Detalle PRN"
'
'    .strTable = "FR2800"
'    .intCursorSize = 0
'
'    Call .objPrinter.Add("FR1251", "Preparaci�n de Cesta")
'    Call .objPrinter.Add("FR1252", "Dispensaci�n de Cesta Por Paciente")
'    Call .objPrinter.Add("FR1254", "Dispensaci�n de Cesta Sin Paciente")
'    Call .objPrinter.Add("FR1255", "Faltas")
'
'    If gstrLlamadorProd = "Cesta" Then
'      Call .FormAddOrderField("FR73codproducto", cwAscending)
'      Call .FormAddOrderField("FR28INDBLOQUEADA", cwAscending)
'      Call .FormAddOrderField("FR66CODPETICION", cwAscending)
'      If strPeticiones = "()" Then
'        .strWhere = "FR66CODPETICION is null"
'      Else
'        mstrWhere = CrearWhere("FR66CODPETICION", strPeticiones) & " AND (FR28CANTIDAD > 0) "
'        .strWhere = mstrWhere
'      End If
'    Else
'      Call .FormAddRelation("FR66CODPETICION", txtText1(0))
'      Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
'      .strWhere = " (FR28CANTIDAD > 0) "
'    End If
'
'    strKey = .strDataBase & .strTable
'
'  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
'    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
'    Call .GridAddColumn(objMultiInfo, "Bloq", "FR28INDBLOQUEADA", cwBoolean)
'    Call .GridAddColumn(objMultiInfo, "Pet", "FR66CODPETICION", cwNumeric, 9)
'    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
'    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
'    Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
'    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
'    Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
'    Call .GridAddColumn(objMultiInfo, "Tam Env", "", cwDecimal, 10)
'    Call .GridAddColumn(objMultiInfo, "FF", "FRH7CODFORMFAR", cwString, 3)
'    Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwNumeric, 5)
'    Call .GridAddColumn(objMultiInfo, "UM", "FR93CODUNIMEDIDA", cwString, 5)
'    Call .GridAddColumn(objMultiInfo, "Desc.UM", "", cwString, 30)
'    Call .GridAddColumn(objMultiInfo, "Can Ped", "FR28CANTIDAD", cwDecimal, 5)
'    Call .GridAddColumn(objMultiInfo, "Ac Ped", "", cwDecimal, 2)
'    Call .GridAddColumn(objMultiInfo, "Can Disp", "FR28CANTDISP", cwDecimal, 5)
'    Call .GridAddColumn(objMultiInfo, "Ac Disp", "", cwDecimal, 5)
'    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
'    Call .GridAddColumn(objMultiInfo, "Desc.V�a", "", cwString, 30)
'    Call .GridAddColumn(objMultiInfo, "Volumen", "FR28VOLUMEN", cwDecimal, 5)
'    Call .GridAddColumn(objMultiInfo, "Tiempo Infusi�n", "FR28TIEMINFMIN", cwDecimal, 4)
'    Call .GridAddColumn(objMultiInfo, "C�d.Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
'    Call .GridAddColumn(objMultiInfo, "Frecuencia", "", cwString, 60)
'    Call .GridAddColumn(objMultiInfo, "PRN", "FR28INDDISPPRN", cwBoolean)
'    Call .GridAddColumn(objMultiInfo, "C.I", "FR28INDCOMIENINMED", cwBoolean)
'    Call .GridAddColumn(objMultiInfo, "S.N", "FR28INDSN", cwBoolean)
'    Call .GridAddColumn(objMultiInfo, "Periodicidad", "FRH5CODPERIODICIDAD", cwString, 10)
'    Call .GridAddColumn(objMultiInfo, "Ubicaci�n", "", cwString, 10)
'    Call .GridAddColumn(objMultiInfo, "PedOri", "FR28CANTPEDORI", cwDecimal, 2)

    Call .FormCreateInfo(objMasterInfo)

'    Call .FormChangeColor(objMultiInfo)
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdAux1(0).Columns("C�d.Prod")), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdAux1(0).Columns("C�d.Prod")), grdAux1(0).Columns("Prod"), "FR73CODINTFAR")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdAux1(0).Columns("C�d.Prod")), grdAux1(0).Columns("Desc.Producto"), "FR73DESPRODUCTO")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdAux1(0).Columns("C�d.Prod")), grdAux1(0).Columns("Referencia"), "FR73REFERENCIA")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdAux1(0).Columns("C�d.Prod")), grdAux1(0).Columns("Tam Env"), "FR73TAMENVASE")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdAux1(0).Columns("C�d.Prod")), grdAux1(0).Columns("Ubicaci�n"), "FRH9UBICACION")
     
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdAux1(0).Columns("V�a")), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdAux1(0).Columns("V�a")), grdAux1(0).Columns("Desc.V�a"), "FR34DESVIA")
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdAux1(0).Columns("UM")), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdAux1(0).Columns("UM")), grdAux1(0).Columns("Desc.UM"), "FR93DESUNIMEDIDA")
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdAux1(0).Columns("C�d.Frecuencia")), "FRG4CODFRECUENCIA", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdAux1(0).Columns("C�d.Frecuencia")), grdAux1(0).Columns("Frecuencia"), "FRG4DESFRECUENCIA")
    
     
'    .CtrlGetInfo(grdAux1(0).Columns("FF")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("Dosis")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("UM")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("V�a")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("Volumen")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("Tiempo Infusi�n")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("C�d.Frecuencia")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("PRN")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("C.I")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("S.N")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("Periodicidad")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("Can Ped")).blnReadOnly = True
'    .CtrlGetInfo(grdAux1(0).Columns("Ac Ped")).blnReadOnly = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With




'Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)

err:
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
err:
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
err:
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
On Error GoTo err
  intCancel = objWinInfo.WinExit
err:
End Sub

Private Sub Form_Unload(intCancel As Integer)
On Error GoTo err
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
err:
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  'On Error GoTo err
  'If strFormName = "PRN" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      Select Case intReport
        Case 1
          Call Imprimir("FR3591.RPT", 0)
        Case 2
          Call Imprimir("FR3592.RPT", 0)
        Case 3
          Call Imprimir("FR3594.RPT", 0)
        Case 4
          Call Imprimir("FR3595.RPT", 0)
        Case 5
          'Call ImprimirPRN("FR1253.RPT", 0)
      End Select
      
    End If
    Set objPrinter = Nothing
  'End If

'err:
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
 Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
  Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
     Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub

Private Sub cboCombo1_Change(Index As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)

  Call objWinInfo.CtrlLostFocus

End Sub

Private Sub txtText1_Change(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


Private Sub Refrescar_Grid()
Dim strselect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim mvntAcumulado As Variant
Dim mvntAcumuladoDisp As Variant
Dim mvntAcumuladoOri As Variant
Dim mvntProducto As Variant
Dim i As Integer
Dim mvntSeccion
Dim glstrEstilo As String
Dim contSecciones As Integer
  
Me.Enabled = False
Screen.MousePointer = vbHourglass

'grdAux1(0).Columns(0).Width = 0
grdAux1(0).Columns("Bloq").Width = 500 'bloqueada?
grdAux1(0).Columns("C�d.Prod").Width = 1000   'c�d.producto
grdAux1(0).Columns("Prod").Width = 800   'c�d interno
grdAux1(0).Columns("Referencia").Width = 1000   'REFERENCIA
grdAux1(0).Columns("FF").Width = 500   'FF
grdAux1(0).Columns("V�a").Width = 400   'c�d.v�a
grdAux1(0).Columns("Desc.V�a").Width = 1500   'desc.v�a
grdAux1(0).Columns("Volumen").Width = 1100  'volumen
grdAux1(0).Columns("Tiempo Infusi�n").Width = 1100  'tiempo infusi�n
grdAux1(0).Columns("C�d.Frecuencia").Width = 1000  'c�d.frecuencia
grdAux1(0).Columns("Frecuencia").Width = 1500  'c�d.frecuencia
grdAux1(0).Columns("PRN").Width = 500  'prn
grdAux1(0).Columns("C.I").Width = 500  'comienzo inmediato
grdAux1(0).Columns("S.N").Width = 500  'seg�n niveles
grdAux1(0).Columns("Can Ped").Width = 800  'cantidad pedida
grdAux1(0).Columns("Ac Ped").Width = 800  'ac pedido
grdAux1(0).Columns("Can Disp").Width = 800  'cantidad disp
grdAux1(0).Columns("Ac Disp").Width = 800  'ac disp
grdAux1(0).Columns("UM").Width = 500  'c�d.medida
grdAux1(0).Columns("Desc.UM").Width = 1000  'desc. medida
grdAux1(0).Columns("Dosis").Width = 600   'dosis
  
strselect = "SELECT "
strselect = strselect & " FR2800.FR28INDBLOQUEADA"
strselect = strselect & ",FR2800.FR66CODPETICION"
strselect = strselect & ",FR2800.FR28NUMLINEA"
strselect = strselect & ",FR2800.FR73CODPRODUCTO"
strselect = strselect & ",FR7300.FR73CODINTFAR"
strselect = strselect & ",FR7300.FR73REFERENCIA"
strselect = strselect & ",FR7300.FR73DESPRODUCTO"
strselect = strselect & ",FR7300.FR73TAMENVASE"
strselect = strselect & ",FR2800.FRH7CODFORMFAR"
strselect = strselect & ",FR2800.FR28DOSIS"
strselect = strselect & ",FR2800.FR93CODUNIMEDIDA"
'strSelect = strSelect & ",FR9300.FR93DESUNIMEDIDA"
strselect = strselect & ",FR2800.FR28CANTIDAD"
strselect = strselect & ",FR2800.FR28CANTDISP"
strselect = strselect & ",FR2800.FR34CODVIA"
'strSelect = strSelect & ",FR3400.FR34DESVIA"
strselect = strselect & ",FR2800.FR28VOLUMEN"
strselect = strselect & ",FR2800.FR28TIEMINFMIN"
strselect = strselect & ",FR2800.FRG4CODFRECUENCIA"
'strSelect = strSelect & ",FRG400.FRG4DESFRECUENCIA"
strselect = strselect & ",FR2800.FR28INDDISPPRN"
strselect = strselect & ",FR2800.FR28INDCOMIENINMED"
strselect = strselect & ",FR2800.FR28INDSN"
strselect = strselect & ",FR2800.FRH5CODPERIODICIDAD"
strselect = strselect & ",FR7300.FRH9UBICACION"
strselect = strselect & ",FR2800.FR28CANTPEDORI"

strselect = strselect & ",FR6600.AD41CODSECCION"
strselect = strselect & ",AD4100.AD41DESSECCION"

strselect = strselect & " FROM FR2800,FR7300,FR6600,AD4100"
'strselect = strselect & " FROM FR2800,FR7300"

strselect = strselect & " WHERE FR2800.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
'strSelect = strSelect & " AND FR2800.FR93CODUNIMEDIDA=FR9300.FR93CODUNIMEDIDA"
'strSelect = strSelect & " AND FR2800.FR34CODVIA=FR3400.FR34CODVIA"
'strSelect = strSelect & " AND FR2800.FRG4CODFRECUENCIA=FRG400.FRG4CODFRECUENCIA"

strselect = strselect & " AND FR6600.AD02CODDPTO=AD4100.AD02CODDPTO(+)"
strselect = strselect & " AND FR6600.AD41CODSECCION=AD4100.AD41CODSECCION(+)"

strselect = strselect & " AND FR2800.FR66CODPETICION=FR6600.FR66CODPETICION"
strselect = strselect & " AND FR6600.FR66INDOM=0"
strselect = strselect & " AND FR6600.FR66INDCESTA=-1"
strselect = strselect & " AND FR6600.AD02CODDPTO=" & gstrCodDpto

'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
contSecciones = 0
If frmVisPRN.lvwSec.ListItems.Count > 0 Then
  If frmVisPRN.lvwSec.ListItems(1).Selected Then
  Else
    For i = 2 To frmVisPRN.lvwSec.ListItems.Count
      If frmVisPRN.lvwSec.ListItems(i).Selected = True Then
        If contSecciones = 0 Then
          contSecciones = contSecciones + 1
          strselect = strselect & " AND ("
        Else
          strselect = strselect & " OR "
        End If
        If i = 2 Then
          strselect = strselect & "FR6600.AD41CODSECCION IS NULL"
        Else
          strselect = strselect & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(i).Tag
        End If
      End If
    Next i
    If contSecciones > 0 Then
      strselect = strselect & ") "
    End If
  End If
End If

If gstrEstCesta = " AND  FR26CODESTPETIC=9  " Then
  strselect = strselect & gstrEstCesta
  strselect = strselect & " AND FR2800.FR28CANTIDAD > 0 "
  strselect = strselect & " AND FR28INDBLOQUEADA = 0 "
ElseIf gstrEstCesta = " AND FR26CODESTPETIC IN (3,9) " Then
  strselect = strselect & gstrEstCesta
  strselect = strselect & " AND FR2800.FR28CANTIDAD > 0 "
  strselect = strselect & " AND FR28INDBLOQUEADA = 0 "
ElseIf gstrEstCesta = " AND FR26CODESTPETIC=5 " Then
  strselect = strselect & gstrEstCesta
  strselect = strselect & " AND FR28INDBLOQUEADA = 0 "
ElseIf gstrEstCesta = " AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 WHERE FR28INDBLOQUEADA=-1) " Then
  strselect = strselect & " AND FR28INDBLOQUEADA=-1"
ElseIf gstrEstCesta = " AND FR26CODESTPETIC=1 " Then
  strselect = strselect & gstrEstCesta
  strselect = strselect & " AND FR2800.FR28CANTIDAD > 0 "
  strselect = strselect & " AND FR28INDBLOQUEADA = 0 "
ElseIf gstrEstCesta = " AND FR26CODESTPETIC=3 " Then
  strselect = strselect & gstrEstCesta
  strselect = strselect & " AND FR2800.FR28CANTIDAD > 0 "
  strselect = strselect & " AND FR28INDBLOQUEADA = 0 "
End If

strselect = strselect & " ORDER BY AD4100.AD41DESSECCION,FR7300.FRH9UBICACION,"
strselect = strselect & " FR7300.FR73DESPRODUCTO,FR7300.FR73CODINTFAR,FR2800.FR28CANTIDAD"

Set qrya = objApp.rdoConnect.CreateQuery("", strselect)
Set rsta = qrya.OpenResultset()

grdAux1(0).RemoveAll
'grdAux1(0).Redraw = False
While Not rsta.EOF
  strlinea = rsta("FR28INDBLOQUEADA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66CODPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODINTFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73REFERENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73TAMENVASE").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH7CODFORMFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28DOSIS").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
'  strLinea = strLinea & rsta("FR93DESUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & "" & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28CANTIDAD").Value & Chr(vbKeyTab)
  strlinea = strlinea & "" & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28CANTDISP").Value & Chr(vbKeyTab)
  strlinea = strlinea & "" & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR34CODVIA").Value & Chr(vbKeyTab)
'  strLinea = strLinea & rsta("FR34DESVIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & "" & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28VOLUMEN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28TIEMINFMIN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRG4CODFRECUENCIA").Value & Chr(vbKeyTab)
'  strLinea = strLinea & rsta("FRG4DESFRECUENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & "" & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28INDDISPPRN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28INDCOMIENINMED").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28INDSN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH5CODPERIODICIDAD").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH9UBICACION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28CANTPEDORI").Value & Chr(vbKeyTab)
  strlinea = strlinea & "" & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD41CODSECCION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD41DESSECCION").Value
  
  grdAux1(0).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
'grdAux1(0).Redraw = True

tabTab1(0).TabVisible(1) = False
tabTab1(0).TabCaption(0) = ""
  
'grdAux1(0).Redraw = False
grdAux2(7).Redraw = False
grdAux2(7).RemoveAll

  If grdAux1(0).Rows > 0 Then
    mvntAcumulado = 0
    mvntAcumuladoDisp = 0
    mvntAcumuladoOri = 0
    mvntSeccion = -1
    glstrEstilo = "CambioSec"
    grdAux1(0).MoveFirst
    For i = 0 To grdAux1(0).Rows - 1
      If grdAux1(0).Columns("Bloq").Value = False Then
        If grdAux1(0).Columns("Can Ped").Value = "" Then
          grdAux1(0).Columns("Can Ped").Value = 0
        End If
        If grdAux1(0).Columns("Can Disp").Value = "" Then
          grdAux1(0).Columns("Can Disp").Value = 0
        End If
        mvntAcumulado = mvntAcumulado + grdAux1(0).Columns("Can Ped").Value
        mvntAcumuladoDisp = mvntAcumuladoDisp + grdAux1(0).Columns("Can Disp").Value
        If grdAux1(0).Columns("PedOri").Value <> "" Then
          mvntAcumuladoOri = mvntAcumuladoOri + grdAux1(0).Columns("PedOri").Value
        End If
      End If
      mvntProducto = grdAux1(0).Columns("C�d.Prod").Value
      mvntSeccion = grdAux1(0).Columns("C�d.Sec.").Value
      grdAux1(0).MoveNext
      If mvntProducto <> grdAux1(0).Columns("C�d.Prod").Value Or mvntSeccion <> grdAux1(0).Columns("C�d.Sec.").Value Then
        
        grdAux1(0).MovePrevious
        grdAux1(0).Columns("Ac Ped").Value = mvntAcumulado
        grdAux1(0).Columns("Ac Disp").Value = mvntAcumuladoDisp
        grdAux1(0).Columns("AcOri").Value = mvntAcumuladoOri
        
        grdAux2(7).AddNew
        grdAux2(7).Columns(0).Value = grdAux1(0).Columns("Prod").Value
        grdAux2(7).Columns(1).Value = grdAux1(0).Columns("Referencia").Value
        grdAux2(7).Columns(2).Value = grdAux1(0).Columns("Desc.Producto").Value
        grdAux2(7).Columns(3).Value = grdAux1(0).Columns("FF").Value
        grdAux2(7).Columns(4).Value = grdAux1(0).Columns("Dosis").Value
        grdAux2(7).Columns(5).Value = grdAux1(0).Columns("UM").Value
        grdAux2(7).Columns(6).Value = grdAux1(0).Columns("Ac Ped").Value
        grdAux2(7).Columns(7).Value = grdAux1(0).Columns("Ac Disp").Value
        grdAux2(7).Columns(8).Value = grdAux1(0).Columns("AcOri").Value
        grdAux2(7).Columns(9).Value = grdAux1(0).Columns("C�d.Prod").Value 'CodProd
        grdAux2(7).Columns(10).Value = grdAux1(0).Columns("Ubicaci�n").Value 'Ubicaci�n
        grdAux2(7).Columns(11).Value = grdAux1(0).Columns("C�d.Sec.").Value
        grdAux2(7).Columns(12).Value = grdAux1(0).Columns("Secci�n").Value
        grdAux2(7).Columns(13).Value = glstrEstilo
        grdAux2(7).Update
        
        mvntAcumulado = 0
        mvntAcumuladoDisp = 0
        mvntAcumuladoOri = 0
        grdAux1(0).MoveNext
        
        If mvntSeccion <> grdAux1(0).Columns("C�d.Sec.").Value Then
          If glstrEstilo = "CambioSec2" Then
            glstrEstilo = "CambioSec"
          Else
            glstrEstilo = "CambioSec2"
          End If
        End If
      
      End If
    Next i
    grdAux1(0).Columns("Ac Ped").Value = mvntAcumulado
    grdAux1(0).Columns("Ac Disp").Value = mvntAcumuladoDisp
    grdAux1(0).Columns("AcOri").Value = mvntAcumuladoOri
        
    grdAux2(7).AddNew
    grdAux2(7).Columns(0).Value = grdAux1(0).Columns("Prod").Value
    grdAux2(7).Columns(1).Value = grdAux1(0).Columns("Referencia").Value
    grdAux2(7).Columns(2).Value = grdAux1(0).Columns("Desc.Producto").Value
    grdAux2(7).Columns(3).Value = grdAux1(0).Columns("FF").Value
    grdAux2(7).Columns(4).Value = grdAux1(0).Columns("Dosis").Value
    grdAux2(7).Columns(5).Value = grdAux1(0).Columns("UM").Value
    grdAux2(7).Columns(6).Value = grdAux1(0).Columns("Ac Ped").Value
    grdAux2(7).Columns(7).Value = grdAux1(0).Columns("Ac Disp").Value
    grdAux2(7).Columns(8).Value = grdAux1(0).Columns("AcOri").Value
    grdAux2(7).Columns(9).Value = grdAux1(0).Columns("C�d.Prod").Value 'CodProd
    grdAux2(7).Columns(10).Value = grdAux1(0).Columns("Ubicaci�n").Value 'Ubicaci�n
    grdAux2(7).Columns(11).Value = grdAux1(0).Columns("C�d.Sec.").Value
    grdAux2(7).Columns(12).Value = grdAux1(0).Columns("Secci�n").Value
    grdAux2(7).Columns(13).Value = glstrEstilo
    grdAux2(7).Update
    
    grdAux1(0).MoveFirst
    grdAux2(7).MoveFirst
  End If

grdAux2(7).Redraw = True
'grdAux1(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub
