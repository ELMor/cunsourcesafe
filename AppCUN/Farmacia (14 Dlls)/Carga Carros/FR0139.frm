VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmAnalizDiferenc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Dosis Unitaria. Analizar Diferencias"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdImprTodos 
      Caption         =   "Imprimir Selecci�n"
      Height          =   675
      Left            =   10800
      TabIndex        =   27
      Top             =   3000
      Width           =   1095
   End
   Begin VB.CommandButton cmdSelTodos 
      Caption         =   "Sel.Todos"
      Height          =   315
      Left            =   10800
      TabIndex        =   24
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Frame frmmensaje 
      Height          =   1935
      Left            =   2880
      TabIndex        =   22
      Top             =   3360
      Visible         =   0   'False
      Width           =   6495
      Begin VB.Label lblmensaje2 
         Caption         =   "(Quedan 44 carros por analizar)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   28
         Top             =   1320
         Visible         =   0   'False
         Width           =   4215
      End
      Begin VB.Label lblmensaje 
         Caption         =   "Actualizando el almac�n. Espere un momento, por favor..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   480
         TabIndex        =   23
         Top             =   480
         Visible         =   0   'False
         Width           =   5775
      End
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   7920
      Top             =   4200
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdanalizardiferencias 
      Caption         =   "Analizar Diferencias"
      Height          =   255
      Left            =   4920
      TabIndex        =   21
      Top             =   4200
      Width           =   2175
   End
   Begin VB.CommandButton cmdtransdatos 
      Caption         =   "Transmitir Datos al Lector"
      Height          =   255
      Left            =   8760
      TabIndex        =   20
      Top             =   4200
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   10500
      Begin TabDlg.SSTab tabTab1 
         Height          =   3135
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   5530
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0139.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(4)=   "lblLabel1(6)"
         Tab(0).Control(5)=   "txtText1(3)"
         Tab(0).Control(6)=   "txtText1(4)"
         Tab(0).Control(7)=   "txtText1(0)"
         Tab(0).Control(8)=   "txtText1(1)"
         Tab(0).Control(9)=   "txtText1(2)"
         Tab(0).Control(10)=   "txtText1(5)"
         Tab(0).Control(11)=   "txtText1(6)"
         Tab(0).Control(12)=   "txtText1(7)"
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0139.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74HORASALIDA"
            Height          =   330
            Index           =   7
            Left            =   -73440
            TabIndex        =   7
            Tag             =   "Hora"
            Top             =   2640
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74DIA"
            Height          =   330
            Index           =   6
            Left            =   -74640
            TabIndex        =   6
            Tag             =   "D�a"
            Top             =   2640
            Width           =   500
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR87DESESTCARRO"
            Height          =   330
            Index           =   5
            Left            =   -74040
            TabIndex        =   5
            Tag             =   "Estado Carro"
            Top             =   1920
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR87CODESTCARRO"
            Height          =   330
            Index           =   2
            Left            =   -74640
            TabIndex        =   4
            Tag             =   "C�s.Estado Carro"
            Top             =   1920
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   1
            Left            =   -74040
            TabIndex        =   1
            Tag             =   "Servicio"
            Top             =   480
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   -74640
            TabIndex        =   0
            Tag             =   "C�d.Servicio"
            Top             =   480
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR07DESCARRO"
            Height          =   330
            Index           =   4
            Left            =   -74040
            TabIndex        =   3
            Tag             =   "Carro"
            Top             =   1200
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR07CODCARRO"
            Height          =   330
            Index           =   3
            Left            =   -74640
            TabIndex        =   2
            Tag             =   "C�d.Carro"
            Top             =   1200
            Width           =   400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3105
            Index           =   2
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   5477
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   -73440
            TabIndex        =   19
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D�a Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74640
            TabIndex        =   18
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74640
            TabIndex        =   17
            Top             =   1680
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74640
            TabIndex        =   15
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -74640
            TabIndex        =   13
            Top             =   960
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Diferencias"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3480
      Index           =   0
      Left            =   0
      TabIndex        =   14
      Top             =   4560
      Width           =   11895
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2985
         Index           =   0
         Left            =   75
         TabIndex        =   16
         Top             =   360
         Width           =   11745
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20717
         _ExtentY        =   5265
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cbohora 
      Height          =   315
      Left            =   10800
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   1920
      Width           =   975
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ";"
      DefColWidth     =   9
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "Hora Salida"
      Columns(0).Name =   "Hora Salida"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1720
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Hora Salida"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   10800
      TabIndex        =   26
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAnalizDiferenc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmAnalizDiferenc (FR0139.FRM)                               *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: analizar diferencias                                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cbohora_CloseUp()

Dim strFechaSer As String
Dim rstFechaSer As rdoResultset
Dim fecha As Date
Dim intDiaSem As Integer
Dim DiaSem(7) As String
    
strFechaSer = "SELECT SYSDATE FROM DUAL"
Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
fecha = rstFechaSer(0).Value
rstFechaSer.Close
intDiaSem = WeekDay(fecha, vbMonday)
DiaSem(1) = "LUN"
DiaSem(2) = "MAR"
DiaSem(3) = "MIE"
DiaSem(4) = "JUE"
DiaSem(5) = "VIE"
DiaSem(6) = "SAB"
DiaSem(7) = "DOM"

If cbohora.Text <> "" Then
  Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
  objWinInfo.objWinActiveForm.strWhere = "FR87CODESTCARRO=2 AND FR74DIA='" & DiaSem(intDiaSem) & "'"
  objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cbohora, ",", ".", 1)
  objWinInfo.DataRefresh
  'grdDBGrid1(0).RemoveAll
End If

End Sub


Private Sub cmdanalizardiferencias_Click()
Dim strupdate As String
Dim i As Integer
Dim qryprod As rdoQuery
Dim rstprod As rdoResultset
Dim strprod As String
Dim hora As String
Dim strFF As String
Dim strDosis As String
Dim strUM As String
Dim cantidad As String
Dim gcodproducto
Dim gdescproducto As String
Dim grupo As Integer
Dim strFF_dil As String
Dim strDosis_dil As String
Dim strUM_dil As String
Dim cantidad_dil As String
Dim gcodproducto_dil
Dim gdescproducto_dil As String
Dim grupo_dil As Integer
Dim strFF_mez As String
Dim strDosis_mez As String
Dim strUM_mez As String
Dim cantidad_mez As String
Dim gcodproducto_mez
Dim gdescproducto_mez As String
Dim grupo_mez As Integer
Dim numlinea As Integer
Dim stra As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim rst33 As rdoResultset
Dim str33 As String
Dim qry33 As rdoQuery
Dim strinsert33 As String
Dim qryinsert33 As rdoQuery
Dim strDispensado As String
Dim rstDispensado As rdoResultset
Dim qryDispensado As rdoQuery
Dim mintisel As Integer
Dim mvarBkmrk As Variant
Dim mintNTotalSelRows As Integer
Dim carrosrestantes As Integer
Dim strcambiocama As String
Dim rstcambiocama As rdoResultset
Dim qrycambiocama As rdoQuery
Dim strFR7400 As String
Dim rstFR7400 As rdoResultset
Dim qryFR7400 As rdoQuery
Dim strFR7400_2 As String
Dim rstFR7400_2 As rdoResultset
Dim qryFR7400_2 As rdoQuery
Dim strDiaSiguiente As String
Dim curHoraSiguiente As Currency
Dim blnValida As Boolean
Dim strDelete As String

If cbohora = "" Then
  Exit Sub
End If

'transforma la coma de separaci�n de los decimales por un punto
'de la hora a la que sale el carro
hora = objGen.ReplaceStr(cbohora, ",", ".", 1)

Screen.MousePointer = vbHourglass
cmdanalizardiferencias.Enabled = False
frmAnalizDiferenc.Enabled = False

lblmensaje.Visible = True
lblmensaje.Caption = "Analizando diferencias. Espere un momento, por favor..."
frmmensaje.Visible = True

mintNTotalSelRows = grddbgrid1(2).SelBookmarks.Count
carrosrestantes = grddbgrid1(2).SelBookmarks.Count

strDispensado = "SELECT FR87CODESTCARRO FROM FR7400 WHERE "
strDispensado = strDispensado & "FR07CODCARRO=?"
strDispensado = strDispensado & " AND FR74DIA=?"
strDispensado = strDispensado & " AND FR74HORASALIDA=" & hora
Set qryDispensado = objApp.rdoConnect.CreateQuery("", strDispensado)

'se vuelca el contenido de FR0600 en FR3300, todas las tuplas con motivo "A�adido"
strinsert33 = "INSERT INTO FR3300(FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA,"
strinsert33 = strinsert33 & "FR73CODPRODUCTO,FR66CODPETICION,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA,"
strinsert33 = strinsert33 & "FR33INDINCPAC,FR33INDINCPROD,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33FECCARGA,FR33HORACARGA,"
strinsert33 = strinsert33 & "FR28NUMLINEA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA,FR33DESPROD,FR33GRUPO,FR33HORATOMA,"
strinsert33 = strinsert33 & "FR33INDMEZCLA,FR33INDNODIF,FR33CAMBIOCAMA,FR33CODIGO)"
strinsert33 = strinsert33 & " (SELECT FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA,FR73CODPRODUCTO,"
strinsert33 = strinsert33 & "FR66CODPETICION,FR06CANTIDAD,FR06INDCARGREA,FR06INDINCCAMA,FR06INDINCPACI,"
strinsert33 = strinsert33 & "FR06INDINCPROD," & "'A�adido',0,0," & "FR06FECCARGA,FR06HORACARGA,FR28NUMLINEA,"
strinsert33 = strinsert33 & "FR06FORMFAR,FR06DOSIS,FR06UNIMEDIDA,FR06DESPROD,FR06GRUPO,FR06HORATOMA,FR06INDMEZCLA,0,FR06CAMBIOCAMA,FR33CODIGO_SEQUENCE.NEXTVAL"
strinsert33 = strinsert33 & " FROM FR0600 WHERE "
strinsert33 = strinsert33 & " FR07CODCARRO=?"
strinsert33 = strinsert33 & " AND FR06INDMEZCLA=0 AND FR06INDCAMBCAMA=0"
strinsert33 = strinsert33 & " AND FR06FECCARGA=TRUNC(SYSDATE)"
strinsert33 = strinsert33 & " AND FR06HORACARGA=" & hora & ")"
Set qryinsert33 = objApp.rdoConnect.CreateQuery("", strinsert33)
 
stra = "SELECT FRG500.FRG5HORA,"
stra = stra & "FR3200.FR66CODPETICION,"
stra = stra & "FR3200.FR28NUMLINEA,"
stra = stra & "FR3200.FR73CODPRODUCTO,"
stra = stra & "FR3200.FR32NUMMODIFIC,"
stra = stra & "FR3200.FR28DOSIS,"
stra = stra & "FR3200.FR34CODVIA,"
stra = stra & "FR3200.FR32VOLUMEN,"
stra = stra & "FR3200.FR32TIEMINFMIN,"
stra = stra & "FR3200.FRG4CODFRECUENCIA,"
stra = stra & "FR3200.FR32OBSERVFARM,"
stra = stra & "FR3200.SG02COD_FRM,"
stra = stra & "FR3200.FR32FECMODIVALI,"
stra = stra & "FR3200.FR32HORAMODIVALI,"
stra = stra & "FR3200.FR32INDSN,"
stra = stra & "FR3200.FR93CODUNIMEDIDA,"
stra = stra & "FR3200.FR32INDISPEN,"
stra = stra & "FR3200.FR32INDCOMIENINMED,"
stra = stra & "FR3200.FRH5CODPERIODICIDAD,"
stra = stra & "FR3200.FRH7CODFORMFAR,"
stra = stra & "FR3200.FR32OPERACION,"
stra = stra & "FR3200.FR32CANTIDAD,"
stra = stra & "FR3200.FR32FECINICIO,"
stra = stra & "FR3200.FR32HORAINICIO,"
stra = stra & "FR3200.FR32FECFIN,"
stra = stra & "FR3200.FR32HORAFIN,"
stra = stra & "FR3200.FR32INDVIAOPC,"
stra = stra & "FR3200.FR73CODPRODUCTO_DIL,"
stra = stra & "FR3200.FR32CANTIDADDIL,"
stra = stra & "FR3200.FR32TIEMMININF,"
stra = stra & "FR3200.FR32REFERENCIA,"
stra = stra & "FR3200.FR32INDESTFAB,"
stra = stra & "FR3200.FR32CANTDISP,"
stra = stra & "FR3200.R32UBICACION,"
stra = stra & "FR3200.FR32INDPERF,"
stra = stra & "FR3200.FR32INSTRADMIN,"
stra = stra & "FR3200.FR32VELPERFUSION,"
stra = stra & "FR3200.FR32VOLTOTAL,"
stra = stra & "FR3200.FR73CODPRODUCTO_2,"
stra = stra & "FR3200.FRH7CODFORMFAR_2,"
stra = stra & "FR3200.FR32DOSIS_2,"
stra = stra & "FR3200.FR93CODUNIMEDIDA_2,"
stra = stra & "FR3200.FR32DESPRODUCTO,"
stra = stra & "FR3200.FR32UBICACION,"
stra = stra & "FR3200.FR32DIATOMAULT,"
stra = stra & "FR3200.FR32HORATOMAULT,"
stra = stra & "FR3200.FR32VOLUMEN_2,"
stra = stra & "FR3200.FR32PATHFORMAG,"
stra = stra & "FR3200.FR32INDDISPPRN,"
stra = stra & "FR6600.FR26CODESTPETIC,"
stra = stra & "FR6600.CI21CODPERSONA,"
stra = stra & "FR6600.AD01CODASISTENCI,"
stra = stra & "FR6600.AD07CODPROCESO,"
stra = stra & "AD1500.AD15CODCAMA,"

stra = stra & "TRUNC(SYSDATE)-TRUNC(FR32FECINICIO) DIASDIFINI,"
stra = stra & "TRUNC(SYSDATE+1)-TRUNC(FR32FECINICIO) DIASDIFINI_2,"
stra = stra & "TRUNC(NVL(FR32FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))-TRUNC(SYSDATE) DIASDIFFIN,"
stra = stra & "TRUNC(NVL(FR32FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))-TRUNC(SYSDATE+1) DIASDIFFIN_2,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE,'d')) DIAHOY,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE,'dd')) DIAMES,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE+1,'d')) DIAHOY_2,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE+1,'dd')) DIAMES_2"

stra = stra & " FROM FR6600,AD1500,FR3200,FRG500,FRJ900"
stra = stra & " WHERE AD1500.AD07CODPROCESO=FR6600.AD07CODPROCESO"
stra = stra & " AND AD1500.AD01CODASISTENCI=FR6600.AD01CODASISTENCI"
stra = stra & " AND FR6600.FR66CODPETICION=fr3200.FR66CODPETICION"
stra = stra & " AND FRG500.FRG4CODFRECUENCIA=FR3200.FRG4CODFRECUENCIA"
stra = stra & " AND AD1500.AD15CODCAMA=FRJ900.AD15CODCAMA"
stra = stra & " AND FRJ900.FR07CODCARRO=?"
stra = stra & " AND FR6600.FR26CODESTPETIC=?"
stra = stra & " AND FR6600.FR66INDOM=?"
stra = stra & " AND FR3200.FR32INDPERF<>?"
stra = stra & " AND FR3200.FR32OPERACION IN ('/','M','P','F')"
Set qrya = objApp.rdoConnect.CreateQuery("", stra)

strprod = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO,"
strprod = strprod & "FRH7CODFORMFAR,FR73DOSIS,FR73VOLUMEN "
strprod = strprod & " FROM FR7300 WHERE "
strprod = strprod & " FR73CODPRODUCTO=?"
Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)
 
strFR7400 = "SELECT FR74DIA,FR74HORASALIDA"
strFR7400 = strFR7400 & " FROM FR7400"
strFR7400 = strFR7400 & " WHERE FR07CODCARRO = ? AND FR87CODESTCARRO<>4"
strFR7400 = strFR7400 & " AND DECODE(FR7400.FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7)=TO_NUMBER(TO_CHAR(SYSDATE,'d'))"
strFR7400 = strFR7400 & " ORDER BY DECODE(FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7),FR74HORASALIDA"
Set qryFR7400 = objApp.rdoConnect.CreateQuery("", strFR7400)

strFR7400_2 = "SELECT FR74DIA,FR74HORASALIDA"
strFR7400_2 = strFR7400_2 & " FROM FR7400"
strFR7400_2 = strFR7400_2 & " WHERE FR07CODCARRO = ? AND FR87CODESTCARRO<>4"
strFR7400_2 = strFR7400_2 & " AND DECODE(FR7400.FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7)=TO_NUMBER(TO_CHAR(SYSDATE+1,'d'))"
strFR7400_2 = strFR7400_2 & " ORDER BY DECODE(FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7),FR74HORASALIDA"
Set qryFR7400_2 = objApp.rdoConnect.CreateQuery("", strFR7400_2)
 
For mintisel = 0 To mintNTotalSelRows - 1
  
  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False

  mvarBkmrk = grddbgrid1(2).SelBookmarks(mintisel)
  carrosrestantes = carrosrestantes - 1
  lblmensaje.Visible = True
  lblmensaje2.Visible = True
  lblmensaje.Caption = "Analizando diferencias del carro " & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & ". Espere un momento, por favor..."
  If carrosrestantes = 1 Then
      lblmensaje2.Caption = "(Queda " & carrosrestantes & " carro por analizar)"
  Else
      lblmensaje2.Caption = "(Quedan " & carrosrestantes & " carros por analizar)"
  End If
  frmmensaje.Visible = True
  
  If grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) <> "" Then
    qryDispensado(0) = grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
    qryDispensado(1) = grddbgrid1(2).Columns("D�a").CellValue(mvarBkmrk)
    Set rstDispensado = qryDispensado.OpenResultset()
    While rstDispensado.StillExecuting
    Wend
    If rstDispensado(0).Value = 6 Then
      MsgBox "El Carro " & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
             " ya ha sido Analizado.", vbInformation, "Aviso"
      rstDispensado.Close
      Set rstDispensado = Nothing
      GoTo carro_siguiente
    Else
      strDelete = "DELETE FROM FR3300 WHERE FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
      strDelete = strDelete & " AND FR33FECCARGA=TRUNC(SYSDATE)"
      strDelete = strDelete & " AND FR33HORACARGA=" & hora
      strDelete = strDelete & " AND FR33INDMEZCLA=0"
      objApp.rdoConnect.Execute strDelete, 64
    End If
    rstDispensado.Close
    Set rstDispensado = Nothing
  End If

  'transforma la coma de separaci�n de los decimales por un punto
  'de la hora a la que sale el carro
  hora = grddbgrid1(2).Columns("Hora").CellValue(mvarBkmrk)
  hora = objGen.ReplaceStr(hora, ",", ".", 1)

  'se vuelca el contenido de FR0600 en FR3300, todas las tuplas con motivo "A�adido"
  qryinsert33(0) = grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
  qryinsert33.Execute

  qryFR7400(0) = grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
  Set rstFR7400 = qryFR7400.OpenResultset()
  While rstFR7400.StillExecuting
  Wend
  If Not rstFR7400.EOF Then
    While Not rstFR7400("FR74HORASALIDA").Value = grddbgrid1(2).Columns("Hora").CellValue(mvarBkmrk)
      rstFR7400.MoveNext
    Wend
    If Not rstFR7400.EOF Then
      rstFR7400.MoveNext
      If Not rstFR7400.EOF Then
        strDiaSiguiente = rstFR7400("FR74DIA").Value
        curHoraSiguiente = rstFR7400("FR74HORASALIDA").Value
      Else
        qryFR7400_2(0) = grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
        Set rstFR7400_2 = qryFR7400_2.OpenResultset()
        While rstFR7400_2.StillExecuting
        Wend
        If Not rstFR7400_2.EOF Then
          strDiaSiguiente = rstFR7400_2("FR74DIA").Value
          curHoraSiguiente = rstFR7400_2("FR74HORASALIDA").Value
        Else
          GoTo carro_siguiente
        End If
        rstFR7400_2.Close
        Set rstFR7400_2 = Nothing
      End If
    Else
      qryFR7400_2(0) = grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
      Set rstFR7400_2 = qryFR7400_2.OpenResultset()
      While rstFR7400_2.StillExecuting
      Wend
      If Not rstFR7400_2.EOF Then
        strDiaSiguiente = rstFR7400_2("FR74DIA").Value
        curHoraSiguiente = rstFR7400_2("FR74HORASALIDA").Value
      Else
        GoTo carro_siguiente
      End If
      rstFR7400_2.Close
      Set rstFR7400_2 = Nothing
    End If
  Else
    GoTo carro_siguiente
  End If
  rstFR7400.Close
  Set rstFR7400 = Nothing

  qrya(0) = grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
  qrya(1) = 4 'Est.Pet.
  qrya(2) = -1 'IndOM
  qrya(3) = -1 'IndPerf
  Set rsta = qrya.OpenResultset()
  While rsta.StillExecuting
  Wend
  While Not rsta.EOF
    blnValida = False
    If strDiaSiguiente = grddbgrid1(2).Columns("D�a").CellValue(mvarBkmrk) Then
      If CCur(grddbgrid1(2).Columns("Hora").CellValue(mvarBkmrk)) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < curHoraSiguiente Then
        If rsta("DIASDIFINI").Value >= 0 And rsta("DIASDIFFIN").Value >= 0 Then
          If rsta("DIASDIFINI").Value = 0 And rsta("DIASDIFFIN").Value = 0 Then
            If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          ElseIf rsta("DIASDIFINI").Value = 0 Then
            If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          ElseIf rsta("DIASDIFFIN").Value = 0 Then
            If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          Else
            If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
              blnValida = True
            End If
          End If
        End If
      Else 'blnvalida=false
      End If
    Else 'Dia salida<>Dia Hasta
      If CCur(grddbgrid1(2).Columns("Hora").CellValue(mvarBkmrk)) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < 24 Then
        If rsta("DIASDIFINI").Value >= 0 And rsta("DIASDIFFIN").Value >= 0 Then
          If rsta("DIASDIFINI").Value = 0 And rsta("DIASDIFFIN").Value = 0 Then
            If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          ElseIf rsta("DIASDIFINI").Value = 0 Then
            If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          ElseIf rsta("DIASDIFFIN").Value = 0 Then
            If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          Else
            If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
              blnValida = True
            End If
          End If
        End If
      ElseIf 0 <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < curHoraSiguiente Then
        If rsta("DIASDIFINI_2").Value >= 0 And rsta("DIASDIFFIN_2").Value >= 0 Then
          If rsta("DIASDIFINI_2").Value = 0 And rsta("DIASDIFFIN_2").Value = 0 Then
            If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                blnValida = True
              End If
            End If
          ElseIf rsta("DIASDIFINI_2").Value = 0 Then
            If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                blnValida = True
              End If
            End If
          ElseIf rsta("DIASDIFFIN_2").Value = 0 Then
            If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                blnValida = True
              End If
            End If
          Else
            If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
              blnValida = True
            End If
          End If
        End If
      Else
        'blnvalida=false
      End If
    End If
    If blnValida Then
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''???
      gcodproducto = ""
      gcodproducto_dil = ""
      gcodproducto_mez = ""
      Select Case rsta.rdoColumns("FR32OPERACION").Value
      Case "/", "P", "M"
        'producto principal
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo = 1
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto = rsta.rdoColumns("FR73CODPRODUCTO").Value
            If rsta.rdoColumns("FR73CODPRODUCTO").Value = 999999999 Then
              gdescproducto = rsta.rdoColumns("FR32DESPRODUCTO").Value
            Else
              gdescproducto = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDAD").Value) Then
             cantidad = 0
            Else
             cantidad = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDAD").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Then
             strDosis = 0
            Else
             strDosis = objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
             strUM = 0
            Else
             strUM = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
             strFF = 0
            Else
             strFF = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        'producto diluyente : puede haber o no
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_dil = 2
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_dil = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
            gdescproducto_dil = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rstprod.rdoColumns("FR73VOLUMEN").Value) Or _
                IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              cantidad_dil = 0
            Else
              If rstprod.rdoColumns("FR73VOLUMEN").Value <> 0 Then
                cantidad_dil = Format(rsta.rdoColumns("FR32CANTIDADDIL").Value / _
                              rstprod.rdoColumns("FR73VOLUMEN").Value, "0.00")
                cantidad_dil = objGen.ReplaceStr(cantidad_dil, ",", ".", 1)
              Else
                cantidad_dil = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              strDosis_dil = 0
            Else
              strDosis_dil = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDADDIL").Value, ",", ".", 1)
            End If
            strUM_dil = "ml"
            If IsNull(rstprod.rdoColumns("FRH7CODFORMFAR").Value) Then
              strFF_dil = ""
            Else
              strFF_dil = rstprod.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
        'producto_mezcla
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_mez = 3
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_mez = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
            gdescproducto_mez = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Or _
                IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
              cantidad_mez = 0
            Else
              If rstprod.rdoColumns("FR73DOSIS").Value = 0 Then
                cantidad_mez = 0
              Else
                cantidad_mez = Format(rsta.rdoColumns("FR32DOSIS_2").Value / _
                                  rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
                cantidad_mez = objGen.ReplaceStr(cantidad_mez, ",", ".", 1)
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Then
              strDosis_mez = 0
            Else
              strDosis_mez = objGen.ReplaceStr(rsta.rdoColumns("FR32DOSIS_2").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
              strUM_mez = 0
            Else
              strUM_mez = rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR_2").Value) Then
              strFF_mez = 0
            Else
              strFF_mez = rsta.rdoColumns("FRH7CODFORMFAR_2").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
      Case "F"
        'Electrolito
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_dil = 2
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_dil = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
            gdescproducto_dil = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rstprod.rdoColumns("FR73VOLUMEN").Value) Or _
                IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              cantidad_dil = 0
            Else
              If rstprod.rdoColumns("FR73VOLUMEN").Value <> 0 Then
                cantidad_dil = Format(rsta.rdoColumns("FR32CANTIDADDIL").Value / _
                        rstprod.rdoColumns("FR73VOLUMEN").Value, "0.00")
                cantidad_dil = objGen.ReplaceStr(cantidad_dil, ",", ".", 1)
              Else
                cantidad_dil = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              strDosis_dil = 0
            Else
              strDosis_dil = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDADDIL").Value, ",", ".", 1)
            End If
            strUM_dil = "ml"
            If IsNull(rstprod.rdoColumns("FRH7CODFORMFAR").Value) Then
              strFF_dil = ""
            Else
              strFF_dil = rstprod.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        'Electrolito 2
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo = 1
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto = rsta.rdoColumns("FR73CODPRODUCTO").Value
            gdescproducto = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Or _
                IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
              cantidad = 0
            Else
              If rstprod.rdoColumns("FR73DOSIS").Value <> 0 Then
                cantidad = Format(rsta.rdoColumns("FR28DOSIS").Value / _
                            rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
                cantidad = objGen.ReplaceStr(cantidad, ",", ".", 1)
              Else
                cantidad = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Then
              strDosis = 0
            Else
              strDosis = objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
              strUM = 0
            Else
              strUM = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
              strFF = 0
            Else
              strFF = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        'Medicamento 2
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_mez = 3
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_mez = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
            gdescproducto_mez = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Or _
                IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
              cantidad_mez = 0
            Else
              If rstprod.rdoColumns("FR73DOSIS").Value <> 0 Then
                cantidad_mez = Format(rsta.rdoColumns("FR32DOSIS_2").Value / _
                            rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
                cantidad_mez = objGen.ReplaceStr(cantidad_mez, ",", ".", 1)
              Else
                cantidad_mez = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Then
              strDosis_mez = 0
            Else
              strDosis_mez = objGen.ReplaceStr(rsta.rdoColumns("FR32DOSIS_2").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
              strUM_mez = 0
            Else
              strUM_mez = rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR_2").Value) Then
              strFF_mez = 0
            Else
              strFF_mez = rsta.rdoColumns("FRH7CODFORMFAR_2").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
      End Select
      'seg�n el c�digo de operaci�n se har�n 1,2 o 3 insert en FR3300 seg�n
      'haya medicamento,diluyente,producto_mezcla
      If IsNumeric(gcodproducto) Then
        'se mira si existe ya la tupla o no
        str33 = "SELECT FR28NUMLINEA FROM FR3300 WHERE " & _
                "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                "FR73CODPRODUCTO=" & gcodproducto & " AND " & _
                "FR33CANTIDAD=" & cantidad & " AND " & _
                "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                "FR33HORACARGA=" & hora & " AND " & _
                "FR33FORMFAR=" & "'" & strFF & "'" & " AND " & _
                "FR33DOSIS=" & strDosis & " AND " & _
                "FR33UNIMEDIDA=" & "'" & strUM & "'" & " AND " & _
                "FR33GRUPO=" & grupo & " AND " & _
                "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                "FR33INDDIFANA=0" & " AND " & _
                "FR33INDMEZCLA=0" & " AND " & _
                "FR33INDPRODMIRADO=0"
        Set rst33 = objApp.rdoConnect.OpenResultset(str33)
        While rst33.StillExecuting
        Wend
        If rst33.EOF Then 'no existe la tupla
          strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                        "FR73CODPRODUCTO,FR33DESPROD,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                        "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                        "FR33FECCARGA,FR33HORACARGA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA," & _
                        "FR33GRUPO,FR33HORATOMA,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33INDMEZCLA,FR33INDNODIF,FR33CODIGO)" & _
                        " VALUES (" & _
                        grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                        "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & "," & _
                        rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                        gcodproducto & "," & _
                        "'" & gdescproducto & "'" & "," & _
                        cantidad & "," & _
                        "NULL,NULL,NULL,NULL" & "," & _
                        rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                        numlinea & "," & _
                        "TRUNC(SYSDATE)" & "," & _
                        hora & ",'" & strFF & "'," & strDosis & ",'" & strUM & "'," & _
                        grupo & "," & _
                        objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                        "'A�adido'" & "," & _
                        "-1,-1," & _
                        "0,0" & _
                        ",FR33CODIGO_SEQUENCE.NEXTVAL)"
          objApp.rdoConnect.Execute strinsert33, 64
          objApp.rdoConnect.Execute "Commit", 64
        Else 'la tupla existe
          strupdate = "UPDATE FR3300 SET FR33INDPRODMIRADO=-1,FR33INDNODIF=-1,FR33INDDIFANA=-1" & _
                "WHERE " & _
                "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                "FR73CODPRODUCTO=" & gcodproducto & " AND " & _
                "FR33CANTIDAD=" & cantidad & " AND " & _
                "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                "FR33HORACARGA=" & hora & " AND " & _
                "FR33FORMFAR=" & "'" & strFF & "'" & " AND " & _
                "FR33DOSIS=" & strDosis & " AND " & _
                "FR33UNIMEDIDA=" & "'" & strUM & "'" & " AND " & _
                "FR33GRUPO=" & grupo & " AND " & _
                "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                "FR33INDDIFANA=0" & " AND " & _
                "FR28NUMLINEA=" & rst33.rdoColumns("FR28NUMLINEA").Value
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      End If
      If IsNumeric(gcodproducto_dil) Then
        If strFF_dil <> "" Then
          'se mira si existe ya la tupla o no
          str33 = "SELECT FR28NUMLINEA FROM FR3300 WHERE " & _
                  "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                  "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                  "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                  "FR73CODPRODUCTO=" & gcodproducto_dil & " AND " & _
                  "FR33CANTIDAD=" & cantidad_dil & " AND " & _
                  "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                  "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                  "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                  "FR33HORACARGA=" & hora & " AND " & _
                  "FR33FORMFAR=" & "'" & strFF_dil & "'" & " AND " & _
                  "FR33DOSIS=" & strDosis_dil & " AND " & _
                  "FR33UNIMEDIDA=" & "'" & strUM_dil & "'" & " AND " & _
                  "FR33GRUPO=" & grupo_dil & " AND " & _
                  "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                  "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                  "FR33INDDIFANA=0" & " AND " & _
                  "FR33INDMEZCLA=0" & " AND " & _
                  "FR33INDPRODMIRADO=0"
          Set rst33 = objApp.rdoConnect.OpenResultset(str33)
          While rst33.StillExecuting
          Wend
          If rst33.EOF Then  'no existe la tupla
            strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                          "FR73CODPRODUCTO,FR33DESPROD,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                          "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                          "FR33FECCARGA,FR33HORACARGA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA," & _
                          "FR33GRUPO,FR33HORATOMA,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33INDMEZCLA,FR33INDNODIF,FR33CODIGO)" & _
                          " VALUES (" & _
                          grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                          "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & "," & _
                          rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                          gcodproducto_dil & "," & _
                          "'" & gdescproducto_dil & "'" & "," & _
                          cantidad_dil & "," & _
                          "NULL,NULL,NULL,NULL" & "," & _
                          rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                          numlinea & "," & _
                          "TRUNC(SYSDATE)" & "," & _
                          hora & ",'" & strFF_dil & "'," & strDosis_dil & ",'" & strUM_dil & "'," & _
                          grupo_dil & "," & _
                          objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                          "'A�adido'" & "," & _
                          "-1,-1," & _
                          "0,0" & _
                          ",FR33CODIGO_SEQUENCE.NEXTVAL)"
            objApp.rdoConnect.Execute strinsert33, 64
            objApp.rdoConnect.Execute "Commit", 64
          Else 'la tupla existe
            strupdate = "UPDATE FR3300 SET FR33INDPRODMIRADO=-1,FR33INDNODIF=-1,FR33INDDIFANA=-1" & _
                  "WHERE " & _
                  "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                  "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                  "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                  "FR73CODPRODUCTO=" & gcodproducto_dil & " AND " & _
                  "FR33CANTIDAD=" & cantidad_dil & " AND " & _
                  "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                  "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                  "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                  "FR33HORACARGA=" & hora & " AND " & _
                  "FR33FORMFAR=" & "'" & strFF_dil & "'" & " AND " & _
                  "FR33DOSIS=" & strDosis_dil & " AND " & _
                  "FR33UNIMEDIDA=" & "'" & strUM_dil & "'" & " AND " & _
                  "FR33GRUPO=" & grupo_dil & " AND " & _
                  "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                  "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                  "FR33INDDIFANA=0" & " AND " & _
                  "FR28NUMLINEA=" & rst33.rdoColumns("FR28NUMLINEA").Value
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
          End If
        Else
          'se mira si existe ya la tupla o no
          str33 = "SELECT FR28NUMLINEA FROM FR3300 WHERE " & _
                  "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                  "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                  "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                  "FR73CODPRODUCTO=" & gcodproducto_dil & " AND " & _
                  "FR33CANTIDAD=" & cantidad_dil & " AND " & _
                  "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                  "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                  "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                  "FR33HORACARGA=" & hora & " AND " & _
                  "FR33FORMFAR IS NULL" & " AND " & _
                  "FR33DOSIS=" & strDosis_dil & " AND " & _
                  "FR33UNIMEDIDA=" & "'" & strUM_dil & "'" & " AND " & _
                  "FR33GRUPO=" & grupo_dil & " AND " & _
                  "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                  "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                  "FR33INDDIFANA=0" & " AND " & _
                  "FR33INDMEZCLA=0" & " AND " & _
                  "FR33INDPRODMIRADO=0"
          Set rst33 = objApp.rdoConnect.OpenResultset(str33)
          While rst33.StillExecuting
          Wend
          If rst33.EOF Then  'no existe la tupla
            strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                          "FR73CODPRODUCTO,FR33DESPROD,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                          "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                          "FR33FECCARGA,FR33HORACARGA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA," & _
                          "FR33GRUPO,FR33HORATOMA,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33INDMEZCLA,FR33INDNODIF,FR33CODIGO)" & _
                          " VALUES (" & _
                          grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                          "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & "," & _
                          rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                          gcodproducto_dil & "," & _
                          "'" & gdescproducto_dil & "'" & "," & _
                          cantidad_dil & "," & _
                          "NULL,NULL,NULL,NULL" & "," & _
                          rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                          numlinea & "," & _
                          "TRUNC(SYSDATE)" & "," & _
                          hora & ",NULL," & strDosis_dil & ",'" & strUM_dil & "'," & _
                          grupo_dil & "," & _
                          objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                          "'A�adido'" & "," & _
                          "-1,-1," & _
                          "0,0" & _
                          ",FR33CODIGO_SEQUENCE.NEXTVAL)"
            objApp.rdoConnect.Execute strinsert33, 64
            objApp.rdoConnect.Execute "Commit", 64
          Else 'la tupla existe
            strupdate = "UPDATE FR3300 SET FR33INDPRODMIRADO=-1,FR33INDNODIF=-1,FR33INDDIFANA=-1" & _
                        "WHERE " & _
                        "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                        "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                        "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                        "FR73CODPRODUCTO=" & gcodproducto_dil & " AND " & _
                        "FR33CANTIDAD=" & cantidad_dil & " AND " & _
                        "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                        "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                        "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                        "FR33HORACARGA=" & hora & " AND " & _
                        "FR33FORMFAR IS NULL" & " AND " & _
                        "FR33DOSIS=" & strDosis_dil & " AND " & _
                        "FR33UNIMEDIDA=" & "'" & strUM_dil & "'" & " AND " & _
                        "FR33GRUPO=" & grupo_dil & " AND " & _
                        "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                        "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                        "FR33INDDIFANA=0" & " AND " & _
                        "FR28NUMLINEA=" & rst33.rdoColumns("FR28NUMLINEA").Value
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
          End If
        End If
      End If
      If IsNumeric(gcodproducto_mez) Then
        'se mira si existe ya la tupla o no
        str33 = "SELECT FR28NUMLINEA FROM FR3300 WHERE " & _
                "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                "FR73CODPRODUCTO=" & gcodproducto_mez & " AND " & _
                "FR33CANTIDAD=" & cantidad_mez & " AND " & _
                "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                "FR33HORACARGA=" & hora & " AND " & _
                "FR33FORMFAR=" & "'" & strFF_mez & "'" & " AND " & _
                "FR33DOSIS=" & strDosis_mez & " AND " & _
                "FR33UNIMEDIDA=" & "'" & strUM_mez & "'" & " AND " & _
                "FR33GRUPO=" & grupo_mez & " AND " & _
                "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                "FR33INDDIFANA=0" & " AND " & _
                "FR33INDMEZCLA=0" & " AND " & _
                "FR33INDPRODMIRADO=0"
        Set rst33 = objApp.rdoConnect.OpenResultset(str33)
        While rst33.StillExecuting
        Wend
        If rst33.EOF Then  'no existe la tupla
          strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                        "FR73CODPRODUCTO,FR33DESPROD,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                        "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                        "FR33FECCARGA,FR33HORACARGA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA," & _
                        "FR33GRUPO,FR33HORATOMA,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33INDMEZCLA,FR33INDNODIF,FR33CODIGO)" & _
                        " VALUES (" & _
                        grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                        "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & "," & _
                        rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                        gcodproducto_mez & "," & _
                        "'" & gdescproducto_mez & "'" & "," & _
                        cantidad_mez & "," & _
                        "NULL,NULL,NULL,NULL" & "," & _
                        rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                        numlinea & "," & _
                        "TRUNC(SYSDATE)" & "," & _
                        hora & ",'" & strFF_mez & "'," & strDosis_mez & ",'" & strUM_mez & "'," & _
                        grupo_mez & "," & _
                        objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                        "'A�adido'" & "," & _
                        "-1,-1," & _
                        "0,0" & _
                        ",FR33CODIGO_SEQUENCE.NEXTVAL)"
          objApp.rdoConnect.Execute strinsert33, 64
          objApp.rdoConnect.Execute "Commit", 64
        Else 'la tupla existe
          strupdate = "UPDATE FR3300 SET FR33INDPRODMIRADO=-1,FR33INDNODIF=-1,FR33INDDIFANA=-1" & _
                      "WHERE " & _
                      "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                      "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                      "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                      "FR73CODPRODUCTO=" & gcodproducto_mez & " AND " & _
                      "FR33CANTIDAD=" & cantidad_mez & " AND " & _
                      "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                      "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                      "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                      "FR33HORACARGA=" & hora & " AND " & _
                      "FR33FORMFAR=" & "'" & strFF_mez & "'" & " AND " & _
                      "FR33DOSIS=" & strDosis_mez & " AND " & _
                      "FR33UNIMEDIDA=" & "'" & strUM_mez & "'" & " AND " & _
                      "FR33GRUPO=" & grupo_mez & " AND " & _
                      "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                      "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                      "FR33INDDIFANA=0" & " AND " & _
                      "FR28NUMLINEA=" & rst33.rdoColumns("FR28NUMLINEA").Value
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      End If
    End If
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing

  'Si hay registros con estado "A�adido" pero �stos no han venido en las nuevas �rdenes
  'm�dicas,es decir, tienen el campo FR33INDDIFANA=0 y FR33INDPRODMIRADO=0
  'entonces dichos registros se marcan como eliminados.
  strupdate = "UPDATE FR3300 SET FR33MOTDIFERENCIA='Eliminado' " & _
            "WHERE FR33INDDIFANA = 0"
  strupdate = strupdate & " AND FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
  strupdate = strupdate & " AND FR33FECCARGA=TRUNC(SYSDATE)"
  strupdate = strupdate & " AND FR33HORACARGA=" & hora
  strupdate = strupdate & " AND FR33MOTDIFERENCIA='A�adido' AND FR33INDPRODMIRADO = 0"
  strupdate = strupdate & " AND FR33INDMEZCLA=0 AND FR33INDNODIF=0"
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64
  
  'se cambia el estado del carro (estado=2 Carro Analizado)
  strupdate = "UPDATE FR7400 SET FR87CODESTCARRO=6 WHERE " & _
            "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
            " AND FR74DIA='" & grddbgrid1(2).Columns("D�a").CellValue(mvarBkmrk) & "'" & _
            " AND FR74HORASALIDA=" & hora
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64


'**********************************************************************************
Dim strpersonacama As String
Dim rstpersonacama As rdoResultset
Dim strpeticion As String
Dim rstpeticion As rdoResultset
Dim strorden As String
Dim rstorden As rdoResultset
Dim strcama As String
Dim rstcama As rdoResultset
Dim strcama06 As String
Dim rstcama06 As rdoResultset

  'se saca  n� de cama del carro en FR3300
  strpersonacama = "SELECT DISTINCT(AD15CODCAMA)" & _
        " FROM FR3300 WHERE " & _
        " FR33INDMEZCLA=0" & _
        " AND FR33FECCARGA=TRUNC(SYSDATE)" & _
        " AND FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
        " AND FR33HORACARGA=" & hora
  Set rstpersonacama = objApp.rdoConnect.OpenResultset(strpersonacama)
  While rstpersonacama.StillExecuting
  Wend
  While Not rstpersonacama.EOF
    strpeticion = "SELECT FR66CODPETICION,CI21CODPERSONA " & _
                  " FROM FR3300 WHERE " & _
                  " FR33INDMEZCLA=0" & _
                  " AND FR33FECCARGA=TRUNC(SYSDATE)" & _
                  " AND FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
                  " AND FR33HORACARGA=" & hora & _
                  " AND AD15CODCAMA=" & rstpersonacama.rdoColumns("AD15CODCAMA").Value
    Set rstpeticion = objApp.rdoConnect.OpenResultset(strpeticion)
    While rstpeticion.StillExecuting
    Wend
    'se busca su proceso y asistencia
    strorden = "SELECT AD01CODASISTENCI,AD07CODPROCESO FROM FR6600" & _
                " WHERE FR66CODPETICION=" & rstpeticion.rdoColumns("FR66CODPETICION").Value
    Set rstorden = objApp.rdoConnect.OpenResultset(strorden)
    While rstorden.StillExecuting
    Wend
    'se mira cu�l es su cama actual
    strcama = "SELECT AD15CODCAMA FROM AD1500 " & _
              " WHERE AD01CODASISTENCI=" & rstorden.rdoColumns("AD01CODASISTENCI").Value & _
              " AND AD07CODPROCESO=" & rstorden.rdoColumns("AD07CODPROCESO").Value
    Set rstcama = objApp.rdoConnect.OpenResultset(strcama)
    While rstcama.StillExecuting
    Wend
    rstorden.Close
    Set rstorden = Nothing
    'se busca la cama en FR0600
    strcama06 = "SELECT DISTINCT(AD15CODCAMA)" & _
                " FROM FR0600 WHERE " & _
                " FR06INDMEZCLA=0 " & _
                " AND FR06FECCARGA=TRUNC(SYSDATE)" & _
                " AND FR06FECCARGA=TRUNC(SYSDATE)" & _
                " AND FR06HORACARGA=" & hora & _
                " AND CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
    Set rstcama06 = objApp.rdoConnect.OpenResultset(strcama06)
    While rstcama06.StillExecuting
    Wend
    'se mira si cama_Actual<>cama_en_FR0600
    If Not rstcama06.EOF And Not rstcama.EOF Then
      If Not IsNull(rstcama06.rdoColumns("AD15CODCAMA").Value) And Not IsNull(rstcama.rdoColumns("AD15CODCAMA").Value) Then
        If (rstcama.rdoColumns("AD15CODCAMA").Value <> _
            rstcama06.rdoColumns("AD15CODCAMA").Value) Then
          strupdate = "UPDATE FR3300 SET FR33CAMBIOCAMA='Cambio de Cama' WHERE " & _
                      " FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
                      " AND FR33FECCARGA=TRUNC(SYSDATE)" & _
                      " AND FR33HORACARGA=" & hora & _
                      " AND FR33INDMEZCLA=0" & _
                      " AND CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Else
      If rstcama.EOF Then  'el paciente ha sido dado de alta, ya no tiene cama
        strupdate = "UPDATE FR3300 SET FR33CAMBIOCAMA='Alta del Paciente' WHERE " & _
                    " FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
                    " AND FR33FECCARGA=TRUNC(SYSDATE)" & _
                    " AND FR33HORACARGA=" & hora & _
                    " AND FR33INDMEZCLA=0" & _
                    " AND CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End If
    rstcama06.Close
    Set rstcama06 = Nothing
    rstpeticion.Close
    Set rstpeticion = Nothing
    rstcama.Close
    Set rstcama = Nothing
    rstpersonacama.MoveNext
  Wend
  rstpersonacama.Close
  Set rstpersonacama = Nothing
  '***************************************************************************************+
carro_siguiente:
Next mintisel

Call Imprimir("FR1391.RPT", 1)

Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
objWinInfo.DataRefresh

lblmensaje.Visible = False
lblmensaje2.Visible = False
frmmensaje.Visible = False

Screen.MousePointer = vbDefault
frmAnalizDiferenc.Enabled = True
cmdanalizardiferencias.Enabled = True

End Sub

Private Sub cmdImprTodos_Click()
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
tlbToolbar1.Buttons(6).Enabled = True
Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6))
End Sub

Private Sub cmdSelTodos_Click()
Dim i As Integer

cmdSelTodos.Enabled = False

grddbgrid1(2).Redraw = False
grddbgrid1(2).SelBookmarks.RemoveAll
grddbgrid1(2).MoveFirst ' Position at the first row
For i = 0 To grddbgrid1(2).Rows
  grddbgrid1(2).SelBookmarks.Add grddbgrid1(2).Bookmark
  grddbgrid1(2).MoveNext
Next i
grddbgrid1(2).Redraw = True
cmdSelTodos.Enabled = True
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
    If Index = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
If strFormName = "Peticiones del Carro" And strCtrlName = "grdDBGrid1(0).Petici�n" Then
    aValues(2) = grddbgrid1(0).Columns(20).Value
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
If objWinInfo.objWinActiveForm.strName = "Carros" Then
    tlbToolbar1.Buttons(3).Enabled = False
    mnuDatosOpcion(20).Enabled = False
End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cmdtransdatos_Click()
Dim rstdes As rdoResultset
Dim strdes As String
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String
Dim str35 As String
Dim rst35 As rdoResultset
Dim str80 As String
Dim rst80 As rdoResultset
Dim i As Integer
Dim struni As String
Dim rstuni As rdoResultset
Dim mintisel As Integer

cmdtransdatos.Enabled = False

'se obtiene el almac�n que pide el producto
strdes = "SELECT FR04CODALMACEN FROM FR0400 WHERE AD02CODDPTO=" & txtText1(0).Text
Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
         "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
         "AND FRH2CODPARAMGEN=3"
Set rstori = objApp.rdoConnect.OpenResultset(strori)

If (Not rstdes.EOF And Not rstori.EOF) And Not IsNull(rstori.rdoColumns(0).Value) _
  And Not IsNull(rstdes.rdoColumns(0).Value) Then
  
'se hace un bucle con todos los productos que van en el carro para llenar las
'tablas FR3500 y FR8000
grddbgrid1(0).MoveFirst
For mintisel = 0 To grddbgrid1(0).Rows - 1
    'Guardamos el n�mero de fila que est� seleccionada
    
    str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
    Set rst35 = objApp.rdoConnect.OpenResultset(str35)
            
    str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
    Set rst80 = objApp.rdoConnect.OpenResultset(str80)
          
    If grddbgrid1(0).Columns(18).Value = "A�adido" Then
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es el almac�n al que se dispensa
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                        "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                        "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
                        rst35.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & _
                        rstdes.rdoColumns(0).Value & "," & _
                        "8" & "," & _
                        "SYSDATE" & "," & _
                        "NULL" & "," & _
                        grddbgrid1(0).Columns(10).Value & "," & _
                        objGen.ReplaceStr(grddbgrid1(0).Columns(12).Value, ",", ".", 1) & "," & _
                        "0" & "," & _
                        "'" & grddbgrid1(0).Columns(16).Value & "'" & ")"
                        
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
                
        'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
        'y el destino de la salida el almac�n al que se dispensa
        strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                        "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                        "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES(" & _
                        rst80.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & _
                        rstdes.rdoColumns(0).Value & "," & _
                        "7" & "," & _
                        "SYSDATE" & "," & _
                        "NULL" & "," & _
                        grddbgrid1(0).Columns(10).Value & "," & _
                        objGen.ReplaceStr(grddbgrid1(0).Columns(12).Value, ",", ".", 1) & "," & _
                        "0" & "," & _
                        "'" & grddbgrid1(0).Columns(16).Value & "'" & ")"
        
        objApp.rdoConnect.Execute strinsertsalida, 64
        objApp.rdoConnect.Execute "Commit", 64
    End If
    If grddbgrid1(0).Columns(18).Value = "Eliminado" Then
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es el almac�n al que se dispensa
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                        "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                        "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
                        rst35.rdoColumns(0).Value & "," & _
                        rstdes.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & _
                        "8" & "," & _
                        "SYSDATE" & "," & _
                        "NULL" & "," & _
                        grddbgrid1(0).Columns(10).Value & "," & _
                        objGen.ReplaceStr(grddbgrid1(0).Columns(12).Value, ",", ".", 1) & "," & _
                        "0" & "," & _
                        "'" & grddbgrid1(0).Columns(16).Value & "'" & ")"
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
                
        'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
        'y el destino de la salida el almac�n al que se dispensa
        strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                        "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                        "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES(" & _
                        rst80.rdoColumns(0).Value & "," & _
                        rstdes.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & _
                        "7" & "," & _
                        "SYSDATE" & "," & _
                        "NULL" & "," & _
                        grddbgrid1(0).Columns(10).Value & "," & _
                        objGen.ReplaceStr(grddbgrid1(0).Columns(12).Value, ",", ".", 1) & "," & _
                        "0" & "," & _
                        "'" & grddbgrid1(0).Columns(16).Value & "'" & ")"
        objApp.rdoConnect.Execute strinsertsalida, 64
        objApp.rdoConnect.Execute "Commit", 64
    End If
            
    rst35.Close
    Set rst35 = Nothing
    rst80.Close
    Set rst80 = Nothing
    
 grddbgrid1(0).MoveNext
 Next mintisel
End If

rstdes.Close
Set rstdes = Nothing
rstori.Close
Set rstori = Nothing

'---------------------------------------------------------------------
'se llama a frmCrtlIncidencias
frmCrtlIncidenciasDif.txtcodservicio.Text = txtText1(0).Text
frmCrtlIncidenciasDif.txtdescservicio.Text = txtText1(1).Text
frmCrtlIncidenciasDif.txtcodcarro.Text = txtText1(3).Text
frmCrtlIncidenciasDif.txtdesccarro.Text = txtText1(4).Text
frmCrtlIncidenciasDif.txtcodestado.Text = txtText1(2).Text
frmCrtlIncidenciasDif.txtdescestado.Text = txtText1(5).Text
frmCrtlIncidenciasDif.txtdia.Text = txtText1(6).Text
frmCrtlIncidenciasDif.txthora.Text = txtText1(7).Text

Call objsecurity.LaunchProcess("FR0151")
cmdtransdatos.Enabled = True
End Sub

Private Sub Form_Activate()
If txtText1(3).Text <> "" Then
    cmdanalizardiferencias.Enabled = True
    cmdtransdatos.Enabled = True
Else
    cmdanalizardiferencias.Enabled = False
    cmdtransdatos.Enabled = False
End If
If objWinInfo.objWinActiveForm.strName = "Carros" Then
    tlbToolbar1.Buttons(3).Enabled = False
    mnuDatosOpcion(20).Enabled = False
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim DiaSem(7) As String
  Dim fecha As Date
  Dim strFechaSer As String
  Dim rstFechaSer As rdoResultset
  Dim intDiaSem As Integer
  Dim stra As String
  Dim rsta As rdoResultset
  
  strFechaSer = "SELECT SYSDATE FROM DUAL"
  Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
  fecha = rstFechaSer(0).Value
  intDiaSem = WeekDay(fecha, vbMonday)
  DiaSem(1) = "LUN"
  DiaSem(2) = "MAR"
  DiaSem(3) = "MIE"
  DiaSem(4) = "JUE"
  DiaSem(5) = "VIE"
  DiaSem(6) = "SAB"
  DiaSem(7) = "DOM"
  
cbohora.RemoveAll
stra = "SELECT DISTINCT FR74HORASALIDA FROM FR0701J " & _
" WHERE FR87CODESTCARRO=2 AND FR74DIA='" & DiaSem(intDiaSem) & "'"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While (Not rsta.EOF)
    Call cbohora.AddItem(rsta.rdoColumns("FR74HORASALIDA").Value)
    rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
cbohora.MoveFirst
cbohora = cbohora.Columns(0).Value
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grddbgrid1(2)
    
    .strName = "Carros"
      
    .strTable = "FR0701J"
    .intAllowance = cwAllowReadOnly
    'estado=2 <Se ha comenzado la carga>
    .strWhere = "FR87CODESTCARRO=2 AND FR74DIA='" & DiaSem(intDiaSem) & "'"
    If cbohora <> "" Then
      .strWhere = .strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cbohora, ",", ".", 1)
    End If
    
    .intCursorSize = 0
    
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    Call .FormAddOrderField("FR74DIA", cwAscending)
    Call .FormAddOrderField("FR74HORASALIDA", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Carros")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "C�digo Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR07DESCARRO", "Descripci�n Carro", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO", "C�d.Estado Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR87DESESTCARRO", "Desc.Estado Carro", cwString)
    Call .FormAddFilterWhere(strKey, "FR74DIA", "D�a Salida", cwString)
    Call .FormAddFilterWhere(strKey, "FR74HORASALIDA", "Hora Salida", cwNumeric)
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grddbgrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Peticiones del Carro"
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    
    .strTable = "FR3300"
    
    .strWhere = "FR33FECCARGA=TRUNC(SYSDATE)" & _
                " AND FR33INDMEZCLA=0 AND ((FR33INDNODIF=0) OR (FR33CAMBIOCAMA IS NOT NULL))"
    
    Call .objPrinter.Add("FR1391", "Peticiones del Carro")
    
    Call .FormAddOrderField("FR07CODCARRO", cwAscending)
    Call .FormAddOrderField("FR33HORATOMA", cwAscending)
    Call .FormAddRelation("FR07CODCARRO", txtText1(3))
    Call .FormAddRelation("FR33HORACARGA", txtText1(7))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones del Carro")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD15CODCAMA", "Cama", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR33CANTIDAD", "Cantidad", cwNumeric)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "Carro", "FR07CODCARRO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Cama", "GCFN06(AD15CODCAMA)", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Persona", "CI21CODPERSONA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7) 'historia
    Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25) 'nombre
    Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25) 'apellido 1�
    Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25) 'apellido 2�
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Int", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Cant", "FR33CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Producto", "FR33DESPROD", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "FR33FORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR33DOSIS", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "FR33UNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Toma", "FR33HORATOMA", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Estado", "FR33MOTDIFERENCIA", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Petici�n", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Motivo", "FR33CAMBIOCAMA", cwString, 20)
    
    
    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    
    
    .CtrlGetInfo(grddbgrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(12)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(18)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(19)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(10)).blnInFind = True

  
    .CtrlGetInfo(txtText1(2)).blnInGrid = False 'c�d.estado carro
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), grddbgrid1(0).Columns(6), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), grddbgrid1(0).Columns(7), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), grddbgrid1(0).Columns(8), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), grddbgrid1(0).Columns(9), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(10)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(10)), grddbgrid1(0).Columns(11), "FR73CODINTFAR")

    grddbgrid1(0).Columns(3).Visible = False
    grddbgrid1(0).Columns(19).Visible = False
    grddbgrid1(0).Columns(20).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
 grddbgrid1(2).Columns(1).Width = 1100 'c�d.servicio
 grddbgrid1(2).Columns(2).Width = 2100 'servicio
 grddbgrid1(2).Columns(3).Width = 900 'c�d.carro
 grddbgrid1(2).Columns(4).Width = 2200 'carro
 grddbgrid1(2).Columns(5).Width = 2200 'estado carro
 grddbgrid1(2).Columns(6).Width = 550 'd�a
 grddbgrid1(2).Columns(7).Width = 550 'hora
  
 grddbgrid1(0).Columns(4).Width = 700 'cama
 grddbgrid1(0).Columns(5).Width = 1000 'c�d.persona
 grddbgrid1(0).Columns(6).Width = 950 'historia
 grddbgrid1(0).Columns(7).Width = 800 'nombre
 grddbgrid1(0).Columns(8).Width = 1300 'apellido 1�
 grddbgrid1(0).Columns(9).Width = 1300 'apellido 2�
 grddbgrid1(0).Columns(10).Width = 1000 'c�d.prod
 grddbgrid1(0).Columns(11).Width = 700 'c�d.interno
 grddbgrid1(0).Columns(13).Width = 1850 'desc producto
 grddbgrid1(0).Columns(14).Width = 450  'F.F
 grddbgrid1(0).Columns(15).Width = 700  'Dosis
 grddbgrid1(0).Columns(16).Width = 650  'U.M
 grddbgrid1(0).Columns(12).Width = 500 'cantidad
 grddbgrid1(0).Columns(17).Width = 550 'hora de toma
 grddbgrid1(0).Columns(18).Width = 800 'estado
 
 grddbgrid1(0).Columns(5).Visible = False 'c�d.producto
 grddbgrid1(0).Columns(10).Visible = False 'c�d.paciente
 
 
 Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean

Call objWinInfo.FormPrinterDialog(True, "")
Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
intReport = objPrinter.Selected
If intReport > 0 Then
  Select Case intReport
    Case 1
      Call Imprimir("FR1391.RPT", 0)
  End Select
End If
Set objPrinter = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  If intIndex = 1 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
If intIndex = 3 Then
    If txtText1(3).Text <> "" Then
        cmdanalizardiferencias.Enabled = True
        'cmdtransdatos.Enabled = True
    Else
        cmdanalizardiferencias.Enabled = False
        'cmdtransdatos.Enabled = False
    End If
End If
  Call objWinInfo.CtrlDataChange
End Sub



Private Sub Imprimir(strListado As String, intDes As Integer)
 'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strCarros As String
  Dim nTotal As Long
  Dim nTotalSelRows As Integer
  Dim i As Integer
  Dim bkmrk As Variant
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  strCarros = ""
  nTotalSelRows = grddbgrid1(2).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grddbgrid1(2).SelBookmarks(i)
      strCarros = strCarros & grddbgrid1(2).Columns("C�d.Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grddbgrid1(2).Columns("C�d.Carro").Value
  End If
  strWhere = ""
  
  CrystalReport1.ReportFileName = strPATH & strListado
  'strWhere = "{FR0701J.FR87CODESTCARRO}=6 AND " & _
            "{FR0701J.FR74DIA}='" & grdDBGrid1(2).Columns(6).Value & "'" & " AND " & _
            "{FR0701J.FR74HORASALIDA}= " & objGen.ReplaceStr(grdDBGrid1(2).Columns(7).Value, ",", ".", 1) & " AND " & _
            CrearWhere("{FR0701J.FR07CODCARRO}", "(" & strCarros & ")")
  strWhere = "{FR3302J.FR87CODESTCARRO}=6 AND " & _
            "{FR3302J.HORASAL}= " & objGen.ReplaceStr(grddbgrid1(2).Columns(7).Value, ",", ".", 1) & " AND " & _
            CrearWhere("{FR3302J.FR07CODCARRO}", "(" & strCarros & ")")
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1

Err_imp1:
  
End Sub


Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function


