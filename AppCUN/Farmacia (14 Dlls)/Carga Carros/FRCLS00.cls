VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

Const PRWinPrepCargaCarro             As String = "FR0138"
Const PRWinCrtlIncidencias            As String = "FR0140"
Const PRWinAnalizDiferenc             As String = "FR0139"
Const PRWinRecogerCarro               As String = "FR0141"
Const PRWinCrtlIncidenciasDif         As String = "FR0151"
Const PRWinSitCarro                   As String = "FR0157"
Const PRWinVisPRN                     As String = "FR0124"
Const PRWinEstPetFarm                 As String = "FR0125"
Const PRWinEstPetCestas               As String = "FR0159"
Const PRWinVerIntMedicaCCA            As String = "FR0181"
Const PRWinModLinea                   As String = "FR0150"
Const PRWinBloquearLineas             As String = "FR0105"
Const PRWinVerPerfilFTPCCA            As String = "FR0162"
Const PRWinVisOMPRNBloq               As String = "FR0175"
Const PRWinVerLineasBloq              As String = "FR0176"
Const PRWinObservaciones              As String = "FR0177"
Const PRWinVisOM                      As String = "FR0178"
Const PRWinEstPetFarmOM               As String = "FR0179"
Const PRWinAlergiasPacienteCCA        As String = "FR0183"
Const PRWinModLineaOM                 As String = "FR0189"
Const FRRepPrepCargaCarro             As String = "FR1381"
Const FRRepAnalizDiferencias          As String = "FR1391"
Const FRRepRecogerCarro               As String = "FR1411"
Const FRRepPrepCesta                  As String = "FR1251"
Const FRRepDispCesta                  As String = "FR1252"
Const FRRepPRN                        As String = "FR1253"
Const FRRepPrepCestaSinPac            As String = "FR1254"
Const FRRepFaltas                     As String = "FR1255"
Const FRRepDispenCestas               As String = "FR1241"
Const FRRepDispenPRNs                 As String = "FR1242"
Const FRRepPrepCestaSec               As String = "FR3591"
Const FRRepDispCestaSec               As String = "FR3592"
Const FRRepPrepCestaSinPacSec         As String = "FR3594"
Const FRRepFaltasSec                  As String = "FR3595"
Const PRWinEstPetCestasSec            As String = "FR0359"
Const FRRepDispenCestasSec            As String = "FR1245"



Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
  
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False
  
  
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case PRWinPrepCargaCarro
      Load frmPrepCargaCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmPrepCargaCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmPrepCargaCarro
      Set frmPrepCargaCarro = Nothing
      
    Case PRWinCrtlIncidencias
      Load frmCrtlIncidencias
      'Call objsecurity.AddHelpContext(528)
      Call frmCrtlIncidencias.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmCrtlIncidencias
      Set frmCrtlIncidencias = Nothing
      
    Case PRWinCrtlIncidenciasDif
      Load frmCrtlIncidenciasDif
      'Call objsecurity.AddHelpContext(528)
      Call frmCrtlIncidenciasDif.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmCrtlIncidenciasDif
      Set frmCrtlIncidenciasDif = Nothing
      
    Case PRWinAnalizDiferenc
      Load frmAnalizDiferenc
      'Call objsecurity.AddHelpContext(528)
      Call frmAnalizDiferenc.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmAnalizDiferenc
      Set frmAnalizDiferenc = Nothing
      
    Case PRWinRecogerCarro
      Load frmRecogerCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmRecogerCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRecogerCarro
      Set frmRecogerCarro = Nothing
      
    Case PRWinSitCarro
      Load frmSitCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmSitCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmSitCarro
      Set frmSitCarro = Nothing

   Case PRWinVisPRN
      Load frmVisPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmVisPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVisPRN
      Set frmVisPRN = Nothing
  Case PRWinEstPetFarm
      Load frmEstPetFarm
      'Call objsecurity.AddHelpContext(528)
      Call frmEstPetFarm.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstPetFarm
      Set frmEstPetFarm = Nothing
      
  Case PRWinVerPerfilFTPCCA
      Load frmVerPerfilFTPCCA
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPerfilFTPCCA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerPerfilFTPCCA
      Set frmVerPerfilFTPCCA = Nothing
      glngPaciente = 0

  Case PRWinVerIntMedicaCCA
      Load frmVerIntMedicaCCA
      'Call objsecurity.AddHelpContext(528)
      Call frmVerIntMedicaCCA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerIntMedicaCCA
      Set frmVerIntMedicaCCA = Nothing

 Case PRWinModLinea
      Load frmModLinea
      'Call objsecurity.AddHelpContext(528)
      Call frmModLinea.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmModLinea
      Set frmModLinea = Nothing

Case PRWinBloquearLineas
      gvntdatafr0105(1) = vntData(1)
      gvntdatafr0105(2) = vntData(2)
      gvntdatafr0105(3) = vntData(3)
      gvntdatafr0105(4) = vntData(4)
      gvntdatafr0105(5) = vntData(5)
      gvntdatafr0105(6) = vntData(6)
      gvntdatafr0105(7) = vntData(7)
      gvntdatafr0105(8) = vntData(8)
      gvntdatafr0105(9) = vntData(9)
      
      Load frmBloqueoLinea
      'Call objsecurity.AddHelpContext(528)
      Call frmBloqueoLinea.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBloqueoLinea
      Set frmBloqueoLinea = Nothing
      
 Case PRWinVisOMPRNBloq
      Load frmVisOMPRNBloq
      'Call objsecurity.AddHelpContext(528)
      Call frmVisOMPRNBloq.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVisOMPRNBloq
      Set frmVisOMPRNBloq = Nothing
      
 Case PRWinVerLineasBloq
      Load frmVerLineasBloq
      'Call objsecurity.AddHelpContext(528)
      Call frmVerLineasBloq.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerLineasBloq
      Set frmVerLineasBloq = Nothing
      
 Case PRWinObservaciones
      Load frmObservaciones
      'Call objsecurity.AddHelpContext(528)
      Call frmObservaciones.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmObservaciones
      Set frmObservaciones = Nothing
 Case PRWinVisOM
      Load frmVisOM
      'Call objsecurity.AddHelpContext(528)
      Call frmVisOM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVisOM
      Set frmVisOM = Nothing
 Case PRWinAlergiasPacienteCCA
      Load frmAlergiasPacienteCCA
      'Call objsecurity.AddHelpContext(528)
      Call frmAlergiasPacienteCCA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmAlergiasPacienteCCA
      Set frmAlergiasPacienteCCA = Nothing
   
 Case PRWinEstPetFarmOM
      Load frmEstPetFarmOM
      'Call objsecurity.AddHelpContext(528)
      Call frmEstPetFarmOM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstPetFarmOM
      Set frmEstPetFarmOM = Nothing
 Case PRWinModLineaOM
      Load frmModLineaOM
      'Call objsecurity.AddHelpContext(528)
      Call frmModLineaOM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmModLineaOM
      Set frmModLineaOM = Nothing
  Case PRWinEstPetCestas
      Load frmEstPetCestas
      'Call objsecurity.AddHelpContext(528)
      Call frmEstPetCestas.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstPetCestas
      Set frmEstPetCestas = Nothing
  Case PRWinEstPetCestasSec
      Load frmEstPetCestasSec
      'Call objsecurity.AddHelpContext(528)
      Call frmEstPetCestasSec.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstPetCestasSec
      Set frmEstPetCestasSec = Nothing
  
  End Select

  Call err.Clear

End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 36, 1 To 4) As Variant
       
  aProcess(1, 1) = PRWinPrepCargaCarro
  aProcess(1, 2) = "Preparar Carga Carro"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinCrtlIncidencias
  aProcess(2, 2) = "Controlar Incidencias"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinCrtlIncidenciasDif
  aProcess(3, 2) = "Controlar Incidencias Diferencias"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinAnalizDiferenc
  aProcess(4, 2) = "Analizar Diferencias"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinRecogerCarro
  aProcess(5, 2) = "Salida de Carros"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = PRWinSitCarro
  aProcess(6, 2) = "Situaci�n de Carros"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinVisPRN
  aProcess(7, 2) = "Visualizar PRN"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = PRWinEstPetFarm
  aProcess(8, 2) = "Estudiar Petici�n Farmacia"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinModLinea
  aProcess(9, 2) = "Modificar L�nea"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinVerIntMedicaCCA
  aProcess(10, 2) = "Ver Interacciones"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = PRWinBloquearLineas
  aProcess(11, 2) = "Bloquear L�neas"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = PRWinVerPerfilFTPCCA
  aProcess(12, 2) = "Consultar Perfil FTP CCA"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWinVisOMPRNBloq
  aProcess(13, 2) = "Ver Peticiones Bloqueadas"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = PRWinVerLineasBloq
  aProcess(14, 2) = "Ver Lineas Bloqueadas"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeWindow
  
  aProcess(15, 1) = PRWinObservaciones
  aProcess(15, 2) = "Ver Observaciones Bloqueo"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = PRWinVisOM
  aProcess(16, 2) = "Visualizar OM"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
  
  aProcess(17, 1) = PRWinEstPetFarmOM
  aProcess(17, 2) = "Estudiar Petici�n OM"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeWindow
  
  aProcess(18, 1) = PRWinAlergiasPacienteCCA
  aProcess(18, 2) = "Alergias Paciente CCA"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = PRWinModLineaOM
  aProcess(19, 2) = "Modificar L�nea"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = FRRepPrepCargaCarro
  aProcess(20, 2) = "Peticiones del Carro"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeReport
  
  aProcess(21, 1) = FRRepAnalizDiferencias
  aProcess(21, 2) = "Diferencias del Carro"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeReport
  
  aProcess(22, 1) = FRRepRecogerCarro
  aProcess(22, 2) = "Contenido del Carro"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeReport
  
  aProcess(23, 1) = FRRepPrepCesta
  aProcess(23, 2) = "Preparar Cesta"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeReport
  
  aProcess(24, 1) = FRRepDispCesta
  aProcess(24, 2) = "Dispensar Cesta Por Paciente"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeReport
  
  aProcess(25, 1) = FRRepPRN
  aProcess(25, 2) = "Dispensar PRN"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeReport
  
  aProcess(26, 1) = FRRepPrepCestaSinPac
  aProcess(26, 2) = "Dispensar Cesta Sin Paciente"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeReport
  
  aProcess(27, 1) = FRRepFaltas
  aProcess(27, 2) = "Faltas"
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeReport

  aProcess(28, 1) = PRWinEstPetCestas
  aProcess(28, 2) = "Dispensar Cestas"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeWindow

  aProcess(29, 1) = FRRepDispenCestas
  aProcess(29, 2) = "Dispensacion de Cestas Dpto./Fecha"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeReport

  aProcess(30, 1) = FRRepDispenPRNs
  aProcess(30, 2) = "Dispensacion de PRNs Dpto./Fecha"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeReport

  aProcess(31, 1) = FRRepPrepCestaSec
  aProcess(31, 2) = "Preparar Cesta por Secciones"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeReport
  
  aProcess(32, 1) = FRRepDispCestaSec
  aProcess(32, 2) = "Dispensar Cesta Por Paciente por Secciones"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeReport
  
  aProcess(33, 1) = FRRepPrepCestaSinPacSec
  aProcess(33, 2) = "Dispensar Cesta Sin Paciente por Secciones"
  aProcess(33, 3) = False
  aProcess(33, 4) = cwTypeReport
  
  aProcess(34, 1) = FRRepFaltasSec
  aProcess(34, 2) = "Faltas por Secciones"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeReport

  aProcess(35, 1) = PRWinEstPetCestasSec
  aProcess(35, 2) = "Dispensar Cestas por Secciones"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeWindow

  aProcess(36, 1) = FRRepDispenCestasSec
  aProcess(36, 2) = "Dispensacion de Cestas Dpto./Sec./Fecha"
  aProcess(36, 3) = False
  aProcess(36, 4) = cwTypeReport

End Sub

