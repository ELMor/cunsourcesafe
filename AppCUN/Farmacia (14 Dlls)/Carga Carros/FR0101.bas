Attribute VB_Name = "DEF001"
Option Explicit

Public objsecurity As clsCWSecurity
Public objmouse As clsCWMouse

Public objCW As clsCW      ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object
'gintprodbuscado es un array que guarda los c�digos de los productos de un protocolo
Public gintprodbuscado()
'Public gintcamabuscada()
Public gintpricipioactivobuscado()
'gintprodtotal guarda el n�mero total de productos seleccionados
Public gintprodtotal As Integer
'Public gintcamatotal
Public gintpricipioactivototal As Integer
Public gintbuscargruprot As Integer
Public gintbuscargruprod As Integer
Public gstrLlamador As String
Public gstrLista As String
Public glngPaciente As Long
Public gintfirmarOM As Integer
Public glngpeticion As Long
Public gintservicio As Long
Public gintlinea As Integer
Public gintbusprod As Integer
Public gvntdatafr0105(1 To 9) As Variant
Public gintcodhojaquiro As Integer
Public gintcodhojaanest As Integer
Public gvntdataPedFarm(1 To 2) As Variant
Public gintfirmarStPlanta As Integer
Public glngnumpeticion As Long
'glngselpaciente recoge el c�digo de paciente seleccionado
'antes de redactar la orden m�dica
Public glngselpaciente As Long
Public gintredactarorden As Integer
Public ginteventloadRedOM As Integer 'Para saber que el la primera llamada en frmRedactarOMPRN
Public gstrLlamadorProd As String
Public glngcodpeticion As Long
Public glngcodprod As Long
Public glngMasInfProd As Long
Public gstrInstAdmin As String
Public gstrCodDpto As String
Public gstrEstCesta As String
Public gintEstCesta As Integer
Public Const gstrversion = "20001003"
Public blnImpresoraSeleccionada As Boolean

Sub Main()
  

End Sub
Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  'End
End Sub

'Funci�n que devuelve el separador decimal que se corresponde con
'el de la configuraci�n regional de la m�quina.
Public Function SeparadorDec() As String

If Format("12,345", "00.000") = "12,345" Then
  SeparadorDec = ","
Else
  SeparadorDec = "."
End If

End Function

Private Function Saltar_Coment()
  Dim strLetra As String
  
  strLetra = " "
  While (Not EOF(1)) And (strLetra <> "*")
    strLetra = Input(1, 1)
  Wend
  If strLetra = "*" Then
    Saltar_Coment = ""
  Else
    Saltar_Coment = strLetra
  End If
End Function
Private Sub Leer_Palabra(strPalabra)
  Dim strLetra As String
  
  strLetra = ""
  While (Not EOF(1)) And (strLetra <> ";")
    strLetra = Input(1, 1)
    strPalabra = strPalabra & strLetra
  Wend
End Sub
Public Sub LeerCWPrint(ByRef strDNS As String, ByRef strUser As String, _
                          ByRef strPass As String, ByRef strPATH As String)
  Dim strLetra As String
  Dim strFicEntrada As String
  
  strFicEntrada = "C:\CWPrint.ini"
    
  Open strFicEntrada For Input As #1
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  
  strDNS = Saltar_Coment
  
  While Not EOF(1)
    Call Leer_Palabra(strDNS)
    Call Leer_Palabra(strUser)
    Call Leer_Palabra(strPass)
    Call Leer_Palabra(strPATH)
  Wend
  Close #1
  strDNS = Left(strDNS, Len(strDNS) - 1)
  'strDNS = objWinInfo.objsecurity.GetOracleSID
  strUser = Left(strUser, Len(strUser) - 1)
  'strUser = objsecurity.GetDataBaseUser
  strPass = Left(strPass, Len(strPass) - 1)
  'strUser = objsecurity.GetDataBasePassword
  strPATH = Left(strPATH, Len(strPATH) - 1)
End Sub



Public Function Es_Para_Hoy(strPeriodicidad As String, numdiasdif As Currency, numdiasem As Integer, numdiames As Integer) As Boolean

  Select Case strPeriodicidad
  Case "Diario"
    Es_Para_Hoy = True
  Case "S�bado No"
      If numdiasem <> 6 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case "Domingo No"
      If numdiasem <> 7 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case "Lunes no"
      If numdiasem <> 1 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case "Pares"
      If numdiames Mod 2 = 0 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case "Impares"
      If numdiames Mod 2 = 1 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case "Alternos"
      If numdiasdif Mod 2 = 0 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case "Lun a Vier"
      If numdiasem <> 6 And numdiasem <> 7 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case "L/X/V"
      If numdiasem = 1 Or numdiasem = 3 Or numdiasem = 5 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case "M/J/S"
      If numdiasem = 2 Or numdiasem = 4 Or numdiasem = 6 Then
        Es_Para_Hoy = True
      Else
        Es_Para_Hoy = False
      End If
  Case Else
      Es_Para_Hoy = False
  End Select

End Function



