VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmRecogerCarro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Dosis Unitaria. Salida de Carros"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdRecTodos 
      Caption         =   "Recoger TODOS"
      Height          =   735
      Left            =   10680
      TabIndex        =   28
      Top             =   3120
      Width           =   1095
   End
   Begin VB.CommandButton cmdImprTodos 
      Caption         =   "Imprimir Selecci�n"
      Height          =   675
      Left            =   10680
      TabIndex        =   26
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Frame frmmensaje 
      Height          =   1935
      Left            =   3000
      TabIndex        =   24
      Top             =   3240
      Visible         =   0   'False
      Width           =   5655
      Begin VB.Label lblmensaje2 
         Caption         =   "(Quedan 44 carros por dispensar)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   27
         Top             =   1320
         Visible         =   0   'False
         Width           =   4455
      End
      Begin VB.Label lblmensaje 
         Caption         =   "Dispensando la carga. Espere un momento, por favor..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   360
         TabIndex        =   25
         Top             =   480
         Visible         =   0   'False
         Width           =   5055
      End
   End
   Begin VB.CommandButton cmdSelTodos 
      Caption         =   "Sel.Todos"
      Height          =   315
      Left            =   10680
      TabIndex        =   21
      Top             =   1320
      Width           =   1095
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   11040
      Top             =   3600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdrecogercarro 
      Caption         =   "Recoger Carro"
      Height          =   255
      Left            =   5040
      TabIndex        =   20
      Top             =   3960
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3375
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   10380
      Begin TabDlg.SSTab tabTab1 
         Height          =   2895
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   5106
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0141.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(4)=   "lblLabel1(6)"
         Tab(0).Control(5)=   "txtText1(3)"
         Tab(0).Control(6)=   "txtText1(4)"
         Tab(0).Control(7)=   "txtText1(0)"
         Tab(0).Control(8)=   "txtText1(1)"
         Tab(0).Control(9)=   "txtText1(2)"
         Tab(0).Control(10)=   "txtText1(5)"
         Tab(0).Control(11)=   "txtText1(6)"
         Tab(0).Control(12)=   "txtText1(7)"
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0141.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74HORASALIDA"
            Height          =   330
            Index           =   7
            Left            =   -73440
            TabIndex        =   7
            Tag             =   "Hora"
            Top             =   2520
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74DIA"
            Height          =   330
            Index           =   6
            Left            =   -74640
            TabIndex        =   6
            Tag             =   "D�a"
            Top             =   2520
            Width           =   500
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR87DESESTCARRO"
            Height          =   330
            Index           =   5
            Left            =   -74040
            TabIndex        =   5
            Tag             =   "Estado Carro"
            Top             =   1800
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR87CODESTCARRO"
            Height          =   330
            Index           =   2
            Left            =   -74640
            TabIndex        =   4
            Tag             =   "C�d.Estado Carro"
            Top             =   1800
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   1
            Left            =   -74040
            TabIndex        =   1
            Tag             =   "Servicio"
            Top             =   360
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   -74640
            TabIndex        =   0
            Tag             =   "C�d.Servicio"
            Top             =   360
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR07DESCARRO"
            Height          =   330
            Index           =   4
            Left            =   -74040
            TabIndex        =   3
            Tag             =   "Carro"
            Top             =   1080
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR07CODCARRO"
            Height          =   330
            Index           =   3
            Left            =   -74640
            TabIndex        =   2
            Tag             =   "C�d.Carro"
            Top             =   1080
            Width           =   400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   2
            Left            =   120
            TabIndex        =   12
            Top             =   0
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   5054
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   -73440
            TabIndex        =   19
            Top             =   2280
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D�a Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74640
            TabIndex        =   18
            Top             =   2280
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74640
            TabIndex        =   17
            Top             =   1560
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74640
            TabIndex        =   15
            Top             =   120
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -74640
            TabIndex        =   13
            Top             =   840
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Peticiones del Carro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3720
      Index           =   0
      Left            =   0
      TabIndex        =   14
      Top             =   4320
      Width           =   11895
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3225
         Index           =   0
         Left            =   75
         TabIndex        =   16
         Top             =   360
         Width           =   11745
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20717
         _ExtentY        =   5689
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cbohora 
      Height          =   315
      Left            =   10680
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   840
      Width           =   975
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ";"
      DefColWidth     =   9
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "Hora Salida"
      Columns(0).Name =   "Hora Salida"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1720
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Hora Salida"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   10680
      TabIndex        =   23
      Top             =   600
      Width           =   1095
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRecogerCarro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRecogerCarro (FR0141.FRM)                                 *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: recoger carro                                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Private Sub cbohora_CloseUp()

Dim strFechaSer As String
Dim rstFechaSer As rdoResultset
Dim fecha As Date
Dim intDiaSem As Integer
Dim DiaSem(7) As String
    
strFechaSer = "SELECT SYSDATE FROM DUAL"
Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
fecha = rstFechaSer(0).Value
rstFechaSer.Close
intDiaSem = WeekDay(fecha, vbMonday)
DiaSem(1) = "LUN"
DiaSem(2) = "MAR"
DiaSem(3) = "MIE"
DiaSem(4) = "JUE"
DiaSem(5) = "VIE"
DiaSem(6) = "SAB"
DiaSem(7) = "DOM"

If cbohora.Text <> "" Then
  Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
  objWinInfo.objWinActiveForm.strWhere = "FR87CODESTCARRO=6 AND FR74DIA='" & DiaSem(intDiaSem) & "'"
  objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cbohora, ",", ".", 1)
  objWinInfo.DataRefresh
  'grdDBGrid1(0).RemoveAll
End If

End Sub


Private Sub cmdImprTodos_Click()
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
tlbToolbar1.Buttons(6).Enabled = True
Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6))
End Sub

Private Sub cmdRecTodos_Click()
Dim strupdate As String
Dim hora As String
Dim mensaje As String
Dim strSql As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
Dim qryUpd As rdoQuery
Dim strInsert As String
Dim qryInsert As rdoQuery

Screen.MousePointer = vbHourglass
cmdRecTodos.Enabled = False
Me.Enabled = False

Call cmdSelTodos_Click

  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False
  
  lblmensaje.Visible = True
  lblmensaje2.Visible = False
  frmmensaje.Visible = True

  'transforma la coma de separaci�n de los decimales por un punto
  hora = objGen.ReplaceStr(cbohora, ",", ".", 1)
    
  strInsert = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,"
  strInsert = strInsert & " FR65CANTIDAD,FR73CODPRODUCTO_DIL,"
  strInsert = strInsert & " FR65CANTIDADDIL,FR73CODPRODUCTO_2,FR65DOSIS_2,"
  strInsert = strInsert & " FR65DESPRODUCTO,"
  strInsert = strInsert & " FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,"
  strInsert = strInsert & " AD07CODPROCESO,FR65INDINTERCIENT,AD02CODDPTO_CRG) "
  strInsert = strInsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
  strInsert = strInsert & " CI21CODPERSONA,"
  strInsert = strInsert & " CODPRODUCTO_1,SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI'))" & ","
  strInsert = strInsert & " CANTIDAD_1,CODPRODUCTO_DIL,"
  strInsert = strInsert & " CANTDIL,CODPRODUCTO_2,"
  strInsert = strInsert & " DOSIS_2,DESPRODUCTO_1,"
  strInsert = strInsert & " FR66CODPETICION,0,"
  strInsert = strInsert & " AD01CODASISTENCI,"
  strInsert = strInsert & " AD07CODPROCESO,FR66INDINTERCIENT,AD02CODDPTO_CRG"
  strInsert = strInsert & " FROM "
  strInsert = strInsert & " ( SELECT DISTINCT "
  strInsert = strInsert & "  FR2800.FR66CODPETICION"
  strInsert = strInsert & "  ,FR2800.FR28NUMLINEA"
  strInsert = strInsert & "  ,FR3300.FR33HORATOMA"
  strInsert = strInsert & "  ,FR6600.CI21CODPERSONA"
  strInsert = strInsert & "  ,FR6600.AD07CODPROCESO"
  strInsert = strInsert & "  ,FR6600.AD01CODASISTENCI"
  strInsert = strInsert & "  ,FR6600.FR66INDINTERCIENT"
  strInsert = strInsert & "  ,FR6600.AD02CODDPTO_CRG"
  strInsert = strInsert & "  ,FR7300_1.FR73CODPRODUCTO CODPRODUCTO_1"
  strInsert = strInsert & "  ,DECODE(FR7300_1.FR73CODPRODUCTO,999999999,FR2800.FR28DESPRODUCTO,FR7300_1.FR73DESPRODUCTO)"
  strInsert = strInsert & "  DESPRODUCTO_1"
  strInsert = strInsert & "  ,FR2800.FR28CANTIDAD CANTIDAD_1"
  strInsert = strInsert & "  ,FR2800.FR73CODPRODUCTO_DIL CODPRODUCTO_DIL"
  strInsert = strInsert & "  ,FR2800.FR28CANTIDADDIL CANTDIL"
  strInsert = strInsert & "  ,FR7300_2.FR73CODPRODUCTO CODPRODUCTO_2"
  strInsert = strInsert & "  ,FR2800.FR28DOSIS_2 DOSIS_2"
  strInsert = strInsert & " FROM FR3300,FR2800,FR6600,FR7300"
  strInsert = strInsert & " FR7300_1,FR7300"
  strInsert = strInsert & " FR7300_2,FR7300 FR7300_DIL"
  strInsert = strInsert & " WHERE FR3300.FR33INDMEZCLA=?"
  strInsert = strInsert & " AND  FR3300.FR66CODPETICION=FR2800.FR66CODPETICION"
  strInsert = strInsert & " AND  FR3300.FR33MOTDIFERENCIA=?"
  strInsert = strInsert & " AND  FR3300.FR28NUMLINEA=FR2800.FR28NUMLINEA"
  strInsert = strInsert & " AND  FR3300.FR66CODPETICION=FR6600.FR66CODPETICION"
  strInsert = strInsert & " AND  FR6600.FR66INDOM=?"
  strInsert = strInsert & " AND  FR2800.FR73CODPRODUCTO=FR7300_1.FR73CODPRODUCTO(+)"
  strInsert = strInsert & " AND  FR2800.FR73CODPRODUCTO_2=FR7300_2.FR73CODPRODUCTO(+)"
  strInsert = strInsert & " AND  FR2800.FR73CODPRODUCTO_DIL=FR7300_DIL.FR73CODPRODUCTO(+)"
  strInsert = strInsert & " AND  FR3300.FR33FECCARGA=TRUNC(sysdate)"
  strInsert = strInsert & " AND  FR3300.FR33HORACARGA=" & hora
  strInsert = strInsert & ")"
  Set qryInsert = objApp.rdoConnect.CreateQuery("", strInsert)
  qryInsert(0) = 0
  qryInsert(1) = "A�adido"
  qryInsert(2) = -1
  qryInsert.Execute
                  
                  
  Call Movimientos_Almacen_2(hora, grddbgrid1(2).Columns("D�a").Value)
    
  'se cambia el estado del carro (estado=3 Carro Cerrado)
  strupdate = "UPDATE FR7400 SET FR87CODESTCARRO=3 WHERE " & _
            " FR74DIA=" & "'" & grddbgrid1(2).Columns("D�a").Value & "'" & _
            " AND FR74HORASALIDA=" & hora
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64
    
  'Se cambia el estado de los carros (estado=1 No se ha iniciado la carga)
  'si el carro recogido es el �ltimo del d�a
  strSql = "SELECT MAX(FR74HORASALIDA)" & _
           " FROM FR7400" & _
           " WHERE FR74DIA = " & "'" & grddbgrid1(2).Columns("D�a").Value & "'" & _
           " AND FR87CODESTCARRO IN (1,6,2,3)"
  Set rstSQL = objApp.rdoConnect.OpenResultset(strSql)
  While rstSQL.StillExecuting
  Wend
  If rstSQL.rdoColumns(0).Value = cbohora Or rstSQL.rdoColumns(0).Value = 0 Then
    strupdate = "UPDATE FR7400 SET FR87CODESTCARRO=1 WHERE " & _
        " FR74DIA=" & "'" & grddbgrid1(2).Columns("D�a").Value & "'" & _
        " AND FR87CODESTCARRO=3"
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "Commit", 64
  End If
  rstSQL.Close
  Set rstSQL = Nothing
    

Call Imprimir("FR1411.RPT", 1)

lblmensaje.Visible = False
lblmensaje2.Visible = False
frmmensaje.Visible = False
Screen.MousePointer = vbDefault
Me.Enabled = True
cmdRecTodos.Enabled = True

End Sub


Private Sub cmdSelTodos_Click()
Dim i As Integer

cmdSelTodos.Enabled = False

grddbgrid1(2).Redraw = False
grddbgrid1(2).SelBookmarks.RemoveAll
grddbgrid1(2).MoveFirst ' Position at the first row
For i = 0 To grddbgrid1(2).Rows
  grddbgrid1(2).SelBookmarks.Add grddbgrid1(2).Bookmark
  grddbgrid1(2).MoveNext
Next i
grddbgrid1(2).Redraw = True
cmdSelTodos.Enabled = True
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    If objWinInfo.objWinActiveForm.strName = "Carros" Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub



Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdrecogercarro_Click()
Dim strupdate As String
Dim hora As String
Dim mensaje As String
Dim strSql As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
Dim qryUpd As rdoQuery
Dim strRecogido As String
Dim rstRecogido As rdoResultset
Dim mintNTotalSelRows As Integer
Dim mintisel As Integer
Dim mvarBkmrk As Variant
Dim carrosrestantes As Integer


Screen.MousePointer = vbHourglass
cmdrecogercarro.Enabled = False
frmRecogerCarro.Enabled = False

mintNTotalSelRows = grddbgrid1(2).SelBookmarks.Count
carrosrestantes = grddbgrid1(2).SelBookmarks.Count
For mintisel = 0 To mintNTotalSelRows - 1

  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False
  
  carrosrestantes = carrosrestantes - 1
  mvarBkmrk = grddbgrid1(2).SelBookmarks(mintisel)
  lblmensaje.Visible = True
  lblmensaje2.Visible = True
  lblmensaje.Caption = "Dispensando el carro " & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & ". Espere un momento, por favor..."
  If carrosrestantes = 1 Then
    lblmensaje2.Caption = "(Queda " & carrosrestantes & " carro por dispensar)"
  Else
    lblmensaje2.Caption = "(Quedan " & carrosrestantes & " carros por dispensar)"
  End If
  frmmensaje.Visible = True

  If grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) <> "" Then
    'transforma la coma de separaci�n de los decimales por un punto
    hora = grddbgrid1(2).Columns("Hora").CellValue(mvarBkmrk)
    hora = objGen.ReplaceStr(hora, ",", ".", 1)
    
    strRecogido = "SELECT FR87CODESTCARRO FROM FR7400 WHERE " & _
              "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
              " AND FR74DIA='" & grddbgrid1(2).Columns("D�a").CellValue(mvarBkmrk) & "'" & _
              " AND FR74HORASALIDA=" & hora
    Set rstRecogido = objApp.rdoConnect.OpenResultset(strRecogido)
    While rstRecogido.StillExecuting
    Wend
    If rstRecogido(0).Value <> 6 Then
      rstRecogido.Close
      Set rstRecogido = Nothing
      MsgBox "El carro " & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
             " ya ha sido Recogido.", vbInformation, "Aviso"
      GoTo siguiente_carro
    End If
    
    Call Insertar_FR6500(grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk), _
                         grddbgrid1(2).Columns("Hora").CellValue(mvarBkmrk))
                    
                    
    Call Movimientos_Almacen(grddbgrid1(2).Columns("C�d.Servicio").CellValue(mvarBkmrk), _
                         grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk), hora)
    
    'se cambia el estado del carro (estado=3 Carro Cerrado)
    strupdate = "UPDATE FR7400 SET FR87CODESTCARRO=3 WHERE " & _
              "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
              " AND FR74DIA=" & "'" & grddbgrid1(2).Columns("D�a").CellValue(mvarBkmrk) & "'" & _
              " AND FR74HORASALIDA=" & hora
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "Commit", 64
    
    'Se cambia el estado de los carros (estado=1 No se ha iniciado la carga)
    'si el carro recogido es el �ltimo del d�a
    strSql = "SELECT MAX(FR74HORASALIDA)" & _
             " FROM FR7400" & _
             " WHERE FR07CODCARRO= " & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
             " AND FR74DIA = " & "'" & grddbgrid1(2).Columns("D�a").CellValue(mvarBkmrk) & "'" & _
             " AND FR87CODESTCARRO IN (1,6,2,3)"
    Set rstSQL = objApp.rdoConnect.OpenResultset(strSql)
    While rstSQL.StillExecuting
    Wend
    
    If rstSQL.rdoColumns(0).Value = hora Or rstSQL.rdoColumns(0).Value = 0 Then
        strupdate = "UPDATE FR7400 SET FR87CODESTCARRO=1 WHERE " & _
            "FR07CODCARRO=" & grddbgrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
            " AND FR74DIA=" & "'" & grddbgrid1(2).Columns("D�a").CellValue(mvarBkmrk) & "'" & _
            " AND FR87CODESTCARRO=3"
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "Commit", 64
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    
  rstRecogido.Close
  Set rstRecogido = Nothing
  End If
siguiente_carro:
Next mintisel

Call Imprimir("FR1411.RPT", 1)

lblmensaje.Visible = False
lblmensaje2.Visible = False
frmmensaje.Visible = False
Screen.MousePointer = vbDefault
frmRecogerCarro.Enabled = True
cmdrecogercarro.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Activate()
    If objWinInfo.objWinActiveForm.strName = "Carros" Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim DiaSem(7) As String
  Dim fecha As Date
  Dim strFechaSer As String
  Dim rstFechaSer As rdoResultset
  Dim intDiaSem As Integer
  Dim stra As String
  Dim rsta As rdoResultset

  strFechaSer = "SELECT SYSDATE FROM DUAL"
  Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
  fecha = rstFechaSer(0).Value
  intDiaSem = WeekDay(fecha, vbMonday)
  DiaSem(1) = "LUN"
  DiaSem(2) = "MAR"
  DiaSem(3) = "MIE"
  DiaSem(4) = "JUE"
  DiaSem(5) = "VIE"
  DiaSem(6) = "SAB"
  DiaSem(7) = "DOM"
    
  cbohora.RemoveAll
  stra = "SELECT DISTINCT FR74HORASALIDA FROM FR0701J " & _
  " WHERE FR87CODESTCARRO=6 AND FR74DIA='" & DiaSem(intDiaSem) & "'"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While (Not rsta.EOF)
      Call cbohora.AddItem(rsta.rdoColumns("FR74HORASALIDA").Value)
      rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  cbohora.MoveFirst
  cbohora = cbohora.Columns(0).Value
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grddbgrid1(2)
    
    .strName = "Carros"
      
    .strTable = "FR0701J"
    .intAllowance = cwAllowReadOnly
    'estado=6 <Carga Analizada>
    .strWhere = "FR87CODESTCARRO=6 AND FR74DIA='" & DiaSem(intDiaSem) & "'"
    If cbohora <> "" Then
      .strWhere = .strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cbohora, ",", ".", 1)
    End If

    .intCursorSize = 0
    
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    Call .FormAddOrderField("FR74DIA", cwAscending)
    Call .FormAddOrderField("FR74HORASALIDA", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Carros")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "C�digo Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR07DESCARRO", "Descripci�n Carro", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO", "C�d.Estado Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR87DESESTCARRO", "Desc.Estado Carro", cwString)
    Call .FormAddFilterWhere(strKey, "FR74DIA", "D�a Salida", cwString)
    Call .FormAddFilterWhere(strKey, "FR74HORASALIDA", "Hora Salida", cwNumeric)
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grddbgrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Peticiones del Carro"
    .intAllowance = cwAllowReadOnly
    
    .strTable = "FR3300"
    
    .strWhere = "FR33FECCARGA=TRUNC(SYSDATE)" & _
                " AND FR33INDMEZCLA=0 AND FR33MOTDIFERENCIA='A�adido'"
                
    .intCursorSize = 0
    
    Call .objPrinter.Add("FR1411", "Peticiones del Carro")
    
    Call .FormAddOrderField("FR07CODCARRO", cwAscending)
    Call .FormAddOrderField("FR33HORATOMA", cwAscending)
    Call .FormAddRelation("FR07CODCARRO", txtText1(3))
    Call .FormAddRelation("FR33HORACARGA", txtText1(7))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones del Carro")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD15CODCAMA", "Cama", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR33CANTIDAD", "Cantidad", cwNumeric)

    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "Carro", "FR07CODCARRO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Cama", "GCFN06(AD15CODCAMA)", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Persona", "CI21CODPERSONA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7) 'historia
    Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25) 'nombre
    Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25) 'apellido 1�
    Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25) 'apellido 2�
    'Call .GridAddColumn(objMultiInfo, "Estado", "FR33MOTDIFERENCIA", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Int", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Cant", "FR33CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Producto", "FR33DESPROD", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "FR33FORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR33DOSIS", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "FR33UNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Toma", "FR33HORATOMA", cwNumeric, 2)
    
    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    
    .CtrlGetInfo(grddbgrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(11)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(13)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(14)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(15)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(16)).blnInFind = True
    .CtrlGetInfo(grddbgrid1(0).Columns(12)).blnInFind = True
  
    '.CtrlGetInfo(txtText1(3)).blnInGrid = False 'c�d.carro
    .CtrlGetInfo(txtText1(2)).blnInGrid = False 'c�d.estado carro

    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), grddbgrid1(0).Columns(6), "CI22NUMHISTORIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), grddbgrid1(0).Columns(7), "CI22NOMBRE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), grddbgrid1(0).Columns(8), "CI22PRIAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(5)), grddbgrid1(0).Columns(9), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(10)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(10)), grddbgrid1(0).Columns(11), "FR73CODINTFAR")
     
    grddbgrid1(0).Columns(3).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
 grddbgrid1(2).Columns("C�d.Servicio").Width = 1100 'c�d.servicio
 grddbgrid1(2).Columns("Servicio").Width = 2300 'servicio
 grddbgrid1(2).Columns("C�d.Carro").Width = 900 'c�d.carro
 grddbgrid1(2).Columns("Carro").Width = 2500 'carro
 grddbgrid1(2).Columns("Estado Carro").Width = 1800
 grddbgrid1(2).Columns("D�a").Width = 500 'd�a
 grddbgrid1(2).Columns("Hora").Width = 500 'hora
  

 grddbgrid1(0).Columns(4).Width = 800 'cama
 grddbgrid1(0).Columns(5).Width = 900 'c�d.persona
 grddbgrid1(0).Columns(6).Width = 950 'historia
 grddbgrid1(0).Columns(7).Width = 1000 'nombre
 grddbgrid1(0).Columns(8).Width = 1300 'apellido 1�
 grddbgrid1(0).Columns(9).Width = 1300 'apellido 2�
 grddbgrid1(0).Columns(10).Width = 700 'c�d.interno
 grddbgrid1(0).Columns(13).Width = 2100 'desc.producto
 grddbgrid1(0).Columns(14).Width = 450 'FF
 grddbgrid1(0).Columns(15).Width = 600 'Dosis
 grddbgrid1(0).Columns(16).Width = 550 'UM
 grddbgrid1(0).Columns(12).Width = 550 'cantidad
 grddbgrid1(0).Columns(17).Width = 550 'toma
 
 grddbgrid1(0).Columns(5).Visible = False 'c�d.persona
 grddbgrid1(0).Columns(10).Visible = False 'c�d.producto
 
 Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
Dim strWhere  As String
Dim strFilter As String
Dim vntResp As Variant
Dim strupdate As String
Dim hora As String
Dim strSql As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
Dim qryUpd As rdoQuery


  
If strFormName = "Peticiones del Carro" Then
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    Select Case intReport
    Case 1
      Call Imprimir("FR1411.RPT", 0)
    End Select
  End If
  Set objPrinter = Nothing
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdDBGrid1_Click(Index As Integer)
If Index = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  If intIndex = 1 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strcamas As String
  Dim qryCamas As rdoQuery
  Dim rstcamas As rdoResultset
  Dim strLisCam As String
  Dim strCarros As String
  Dim nTotal As Long
  Dim nTotalSelRows As Integer
  Dim i As Integer
  Dim bkmrk As Variant
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  strCarros = ""
  nTotalSelRows = grddbgrid1(2).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grddbgrid1(2).SelBookmarks(i)
      strCarros = strCarros & grddbgrid1(2).Columns("C�d.Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grddbgrid1(2).Columns("C�d.Carro").Value
  End If
  strWhere = ""
  
  CrystalReport1.ReportFileName = strPATH & strListado
  'strWhere = "({FR0701J.FR87CODESTCARRO}=6 OR {FR0701J.FR87CODESTCARRO}=3 OR {FR0701J.FR87CODESTCARRO}=1) AND " & _
            "{FR0701J.FR74DIA}='" & grdDBGrid1(2).Columns(6).Value & "'" & " AND " & _
            "{FR0701J.FR74HORASALIDA}= " & objGen.ReplaceStr(grdDBGrid1(2).Columns(7).Value, ",", ".", 1) & " AND " & _
            CrearWhere("{FR0701J.FR07CODCARRO}", "(" & strCarros & ")")
  strWhere = "({FR3305J.FR87CODESTCARRO}=6 OR {FR3305J.FR87CODESTCARRO}=3 OR {FR3305J.FR87CODESTCARRO}=1) AND " & _
            "{FR3305J.HORASALIDA}= " & objGen.ReplaceStr(grddbgrid1(2).Columns(7).Value, ",", ".", 1) & " AND " & _
            CrearWhere("{FR3305J.FR07CODCARRO}", "(" & strCarros & ")")
            
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1
Err_imp1:
  
End Sub

Private Sub Movimientos_Almacen(dpto, carro, hora)
Dim qrydes As rdoQuery
Dim rstdes As rdoResultset
Dim strdes As String
Dim qryori As rdoQuery
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String


'se obtiene el almac�n que pide el producto
strdes = "SELECT FR04CODALMACEN FROM FR0400 WHERE AD02CODDPTO=?"
strdes = strdes & " AND FR0400.FR04INDPRINCIPAL=?"
Set qrydes = objApp.rdoConnect.CreateQuery("", strdes)
qrydes(0) = dpto
qrydes(1) = -1
Set rstdes = qrydes.OpenResultset()
While rstdes.StillExecuting
Wend
'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
'strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
         "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
         "AND FRH2CODPARAMGEN=?"
strori = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=?"
Set qryori = objApp.rdoConnect.CreateQuery("", strori)
'qryori(0) = 3
qryori(0) = 28
Set rstori = qryori.OpenResultset()
While rstori.StillExecuting
Wend

  If (Not rstdes.EOF And Not rstori.EOF) Then
    If Not IsNull(rstori.rdoColumns(0).Value) And Not IsNull(rstdes.rdoColumns(0).Value) Then
          
    'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
    'Farmacia y el destino es el almac�n al que se dispensa
      strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) "
      strinsertentrada = strinsertentrada & "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval,"
      strinsertentrada = strinsertentrada & rstori.rdoColumns(0).Value & ","
      strinsertentrada = strinsertentrada & rstdes.rdoColumns(0).Value & ","
      strinsertentrada = strinsertentrada & "8,SYSDATE,NULL,"
      strinsertentrada = strinsertentrada & "FR3300.FR73CODPRODUCTO,"
      strinsertentrada = strinsertentrada & "FR3300.FR33CANTIDAD,0,"
      strinsertentrada = strinsertentrada & "FR3300.FR33UNIMEDIDA"
      strinsertentrada = strinsertentrada & " FROM FR3300"
      strinsertentrada = strinsertentrada & " WHERE  "
      strinsertentrada = strinsertentrada & " FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
      strinsertentrada = strinsertentrada & " AND FR3300.FR07CODCARRO=" & carro
      strinsertentrada = strinsertentrada & " AND FR3300.FR33HORACARGA=" & hora
      strinsertentrada = strinsertentrada & " AND FR3300.FR33INDMEZCLA=0 AND FR3300.FR33MOTDIFERENCIA='A�adido'"
      
      objApp.rdoConnect.Execute strinsertentrada, 64
      objApp.rdoConnect.Execute "Commit", 64
            
      'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
      'y el destino de la salida el almac�n al que se dispensa
      strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV," & _
                      "FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) "
      strinsertsalida = strinsertsalida & "SELECT FR80NUMMOV_SEQUENCE.nextval,"
      strinsertsalida = strinsertsalida & rstori.rdoColumns(0).Value & ","
      strinsertsalida = strinsertsalida & rstdes.rdoColumns(0).Value & ","
      strinsertsalida = strinsertsalida & "8,SYSDATE,NULL,"
      strinsertsalida = strinsertsalida & "FR3300.FR73CODPRODUCTO,"
      strinsertsalida = strinsertsalida & "FR3300.FR33CANTIDAD,0,"
      strinsertsalida = strinsertsalida & "FR3300.FR33UNIMEDIDA"
      strinsertsalida = strinsertsalida & " FROM FR3300"
      strinsertsalida = strinsertsalida & " WHERE  "
      strinsertsalida = strinsertsalida & " FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
      strinsertsalida = strinsertsalida & " AND FR3300.FR07CODCARRO=" & carro
      strinsertsalida = strinsertsalida & " AND FR3300.FR33HORACARGA=" & hora
      strinsertsalida = strinsertsalida & " AND FR3300.FR33INDMEZCLA=0 AND FR3300.FR33MOTDIFERENCIA='A�adido'"

      objApp.rdoConnect.Execute strinsertsalida, 64
      objApp.rdoConnect.Execute "Commit", 64
    End If
End If

rstdes.Close
Set rstdes = Nothing
rstori.Close
Set rstori = Nothing

End Sub

Private Sub Insertar_FR6500(carro As Variant, hora As Variant)
Dim strInsert As String
Dim stra As String
Dim qrya As rdoQuery

strInsert = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,"
strInsert = strInsert & " FR65CANTIDAD,FR73CODPRODUCTO_DIL,"
strInsert = strInsert & " FR65CANTIDADDIL,FR73CODPRODUCTO_2,FR65DOSIS_2,"
strInsert = strInsert & " FR65DESPRODUCTO,"
strInsert = strInsert & " FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO,"
strInsert = strInsert & " FR65INDINTERCIENT,AD02CODDPTO_CRG) "

strInsert = strInsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
strInsert = strInsert & " CI21CODPERSONA,"
strInsert = strInsert & " CODPRODUCTO_1,SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI'))" & ","
strInsert = strInsert & " CANTIDAD_1,CODPRODUCTO_DIL,"
strInsert = strInsert & " CANTDIL,CODPRODUCTO_2,"
strInsert = strInsert & " DOSIS_2,DESPRODUCTO_1,"
strInsert = strInsert & " FR66CODPETICION,0,"
strInsert = strInsert & " AD01CODASISTENCI,"
strInsert = strInsert & " AD07CODPROCESO,FR66INDINTERCIENT,AD02CODDPTO_CRG"
strInsert = strInsert & " FROM "

stra = stra & " ( SELECT DISTINCT "
stra = stra & "  FR2800.FR66CODPETICION"
stra = stra & "  ,FR2800.FR28NUMLINEA"
stra = stra & "  ,FR3300.FR33HORATOMA"
stra = stra & "  ,FR6600.CI21CODPERSONA"
stra = stra & "  ,FR6600.AD07CODPROCESO"
stra = stra & "  ,FR6600.AD01CODASISTENCI"
stra = stra & "  ,FR6600.FR66INDINTERCIENT"
stra = stra & "  ,FR6600.AD02CODDPTO_CRG"
stra = stra & "  ,FR7300_1.FR73CODPRODUCTO CODPRODUCTO_1"
stra = stra & "  ,DECODE(FR7300_1.FR73CODPRODUCTO,999999999,FR2800.FR28DESPRODUCTO,FR7300_1.FR73DESPRODUCTO)"
stra = stra & "  DESPRODUCTO_1"
stra = stra & "  ,FR2800.FR28CANTIDAD CANTIDAD_1"
stra = stra & "  ,FR2800.FR73CODPRODUCTO_DIL CODPRODUCTO_DIL"
stra = stra & "  ,FR2800.FR28CANTIDADDIL CANTDIL"
stra = stra & "  ,FR7300_2.FR73CODPRODUCTO CODPRODUCTO_2"
stra = stra & "  ,FR2800.FR28DOSIS_2 DOSIS_2"
stra = stra & " FROM FR3300,FR2800,FR6600,FR7300"
stra = stra & " FR7300_1,FR7300"
stra = stra & " FR7300_2,FR7300 FR7300_DIL"
stra = stra & " WHERE FR3300.FR33INDMEZCLA=?"
stra = stra & " AND  FR3300.FR66CODPETICION=FR2800.FR66CODPETICION"
stra = stra & " AND  FR3300.FR33MOTDIFERENCIA=?"
stra = stra & " AND  FR3300.FR28NUMLINEA=FR2800.FR28NUMLINEA"
stra = stra & " AND  FR3300.FR66CODPETICION=FR6600.FR66CODPETICION"
stra = stra & " AND  FR6600.FR66INDOM=?"
stra = stra & " AND  FR2800.FR73CODPRODUCTO=FR7300_1.FR73CODPRODUCTO(+)"
stra = stra & " AND  FR2800.FR73CODPRODUCTO_2=FR7300_2.FR73CODPRODUCTO(+)"
stra = stra & " AND  FR2800.FR73CODPRODUCTO_DIL=FR7300_DIL.FR73CODPRODUCTO(+)"
stra = stra & " AND  FR3300.FR07CODCARRO=?"
stra = stra & " AND  FR3300.FR33FECCARGA=TRUNC(sysdate)"
stra = stra & " AND  FR3300.FR33HORACARGA=" & objGen.ReplaceStr(hora, ",", ".", 1) & ")"

strInsert = strInsert & stra
Set qrya = objApp.rdoConnect.CreateQuery("", strInsert)
qrya(0) = 0
qrya(1) = "A�adido"
qrya(2) = -1
qrya(3) = carro
qrya.Execute

End Sub

Private Sub Movimientos_Almacen_2(hora, dia)
Dim qryori As rdoQuery
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String



'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
'strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
         "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
         "AND FRH2CODPARAMGEN=?"
strori = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=?"
Set qryori = objApp.rdoConnect.CreateQuery("", strori)
'qryori(0) = 3
qryori(0) = 28
Set rstori = qryori.OpenResultset()
While rstori.StillExecuting
Wend

  If Not rstori.EOF Then
    If Not IsNull(rstori.rdoColumns(0).Value) Then
          
    'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
    'Farmacia y el destino es el almac�n al que se dispensa
      strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) "
      strinsertentrada = strinsertentrada & "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval,"
      strinsertentrada = strinsertentrada & rstori.rdoColumns(0).Value & ","
      strinsertentrada = strinsertentrada & "FR0400.FR04CODALMACEN,"
      strinsertentrada = strinsertentrada & "8,SYSDATE,NULL,"
      strinsertentrada = strinsertentrada & "FR3300.FR73CODPRODUCTO,"
      strinsertentrada = strinsertentrada & "FR3300.FR33CANTIDAD,0,"
      strinsertentrada = strinsertentrada & "FR3300.FR33UNIMEDIDA"
      strinsertentrada = strinsertentrada & " FROM FR3300,FR0700,FR0400"
      strinsertentrada = strinsertentrada & " WHERE  "
      strinsertentrada = strinsertentrada & " FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
      strinsertentrada = strinsertentrada & " AND FR3300.FR33HORACARGA=" & hora
      strinsertentrada = strinsertentrada & " AND FR3300.FR33INDMEZCLA=0 AND FR3300.FR33MOTDIFERENCIA='A�adido'"
      strinsertentrada = strinsertentrada & " AND FR3300.FR07CODCARRO=FR0700.FR07CODCARRO"
      strinsertentrada = strinsertentrada & " AND FR0400.AD02CODDPTO=FR0700.AD02CODDPTO"
      strinsertentrada = strinsertentrada & " AND FR0400.FR04INDPRINCIPAL=-1"

      objApp.rdoConnect.Execute strinsertentrada, 64
      objApp.rdoConnect.Execute "Commit", 64
            
      'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
      'y el destino de la salida el almac�n al que se dispensa
      strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV," & _
                      "FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) "
      strinsertsalida = strinsertsalida & "SELECT FR80NUMMOV_SEQUENCE.nextval,"
      strinsertsalida = strinsertsalida & rstori.rdoColumns(0).Value & ","
      strinsertsalida = strinsertsalida & "FR0400.FR04CODALMACEN,"
      strinsertsalida = strinsertsalida & "8,SYSDATE,NULL,"
      strinsertsalida = strinsertsalida & "FR3300.FR73CODPRODUCTO,"
      strinsertsalida = strinsertsalida & "FR3300.FR33CANTIDAD,0,"
      strinsertsalida = strinsertsalida & "FR3300.FR33UNIMEDIDA"
      strinsertsalida = strinsertsalida & " FROM FR3300,FR0700,FR0400"
      strinsertsalida = strinsertsalida & " WHERE  "
      strinsertsalida = strinsertsalida & " FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
      strinsertsalida = strinsertsalida & " AND FR3300.FR33HORACARGA=" & hora
      strinsertsalida = strinsertsalida & " AND FR3300.FR33INDMEZCLA=0 AND FR3300.FR33MOTDIFERENCIA='A�adido'"
      strinsertsalida = strinsertsalida & " AND FR3300.FR07CODCARRO=FR0700.FR07CODCARRO"
      strinsertsalida = strinsertsalida & " AND FR0400.AD02CODDPTO=FR0700.AD02CODDPTO"
      strinsertsalida = strinsertsalida & " AND FR0400.FR04INDPRINCIPAL=-1"

      objApp.rdoConnect.Execute strinsertsalida, 64
      objApp.rdoConnect.Execute "Commit", 64
    End If
  End If

rstori.Close
Set rstori = Nothing

End Sub


