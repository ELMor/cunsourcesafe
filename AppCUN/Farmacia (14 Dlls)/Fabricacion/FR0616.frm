VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form FrmFirmarPeticionesFM 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Fabricaci�n. Firmar Peticiones. F�rmulas Magistrales"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0616.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "Anular"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6360
      TabIndex        =   28
      Top             =   7680
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      Caption         =   "B�squeda por"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   1935
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Width           =   11775
      Begin VB.Frame Frame3 
         Caption         =   "Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1095
         Left            =   1680
         TabIndex        =   23
         Top             =   240
         Width           =   4335
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            Index           =   1
            Left            =   120
            MaxLength       =   7
            TabIndex        =   25
            Tag             =   "Historia"
            ToolTipText     =   "Historia"
            Top             =   600
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            BackColor       =   &H00E0E0E0&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            Index           =   2
            Left            =   1320
            Locked          =   -1  'True
            TabIndex        =   24
            Tag             =   "Paciente"
            ToolTipText     =   "Paciente"
            Top             =   600
            Width           =   2865
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Historia"
            Height          =   195
            Index           =   21
            Left            =   120
            TabIndex        =   27
            Top             =   360
            Width           =   525
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Paciente"
            Height          =   195
            Index           =   17
            Left            =   1320
            TabIndex        =   26
            Top             =   360
            Width           =   630
         End
      End
      Begin VB.CommandButton cmdfiltrar 
         Caption         =   "FILTRAR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   3240
         TabIndex        =   22
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Frame Frame1 
         Caption         =   "Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1215
         Left            =   8160
         TabIndex        =   18
         Top             =   240
         Width           =   3495
         Begin VB.TextBox txtServicio 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Left            =   840
            Locked          =   -1  'True
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio"
            ToolTipText     =   "Desc.Servicio"
            Top             =   360
            Width           =   2565
         End
         Begin VB.CheckBox chkservicio 
            Caption         =   "Todos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   120
            TabIndex        =   19
            ToolTipText     =   "Todos Servicios"
            Top             =   840
            Value           =   1  'Checked
            Width           =   855
         End
         Begin SSDataWidgets_B.SSDBCombo cboservicio 
            Height          =   315
            Left            =   120
            TabIndex        =   21
            ToolTipText     =   "C�digo Servicio"
            Top             =   360
            Width           =   645
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            FieldSeparator  =   ";"
            RowHeight       =   609
            Columns.Count   =   2
            Columns(0).Width=   1879
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).Alignment=   1
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5609
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1147
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   17
         Top             =   1075
         Width           =   975
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   16
         Top             =   780
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   15
         Top             =   480
         Width           =   1095
      End
      Begin VB.Frame Frame5 
         Caption         =   "Intervalo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1575
         Left            =   6120
         TabIndex        =   10
         Top             =   240
         Width           =   1935
         Begin SSCalendarWidgets_A.SSDateCombo dtcinicio 
            Height          =   330
            Left            =   120
            TabIndex        =   11
            Tag             =   "Fecha Inicio"
            ToolTipText     =   "Fecha Inicio"
            Top             =   480
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcfin 
            Height          =   330
            Left            =   120
            TabIndex        =   12
            Tag             =   "Fecha Fin"
            ToolTipText     =   "Fecha Fin"
            Top             =   1080
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2999
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   14
            Top             =   240
            Width           =   870
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   13
            Top             =   840
            Width           =   705
         End
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Todo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   9
         Top             =   1390
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdOM 
      Caption         =   " Firmar / Enviar "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   5
      Top             =   7680
      Width           =   2055
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5040
      Left            =   120
      TabIndex        =   2
      Top             =   2520
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   8890
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   8388736
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Pacientes"
      TabPicture(0)   =   "FR0616.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Servicios"
      TabPicture(1)   =   "FR0616.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(2)"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFrame1 
         Caption         =   "F�rmulas Magistrales pendientes de Firmar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   4575
         Index           =   2
         Left            =   -74880
         TabIndex        =   4
         Top             =   360
         Width           =   11445
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   4095
            Index           =   6
            Left            =   120
            TabIndex        =   7
            Top             =   360
            Width           =   11175
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   6
            stylesets(0).Name=   "Estupefacientes"
            stylesets(0).BackColor=   14671839
            stylesets(0).Picture=   "FR0616.frx":0044
            stylesets(1).Name=   "FormMagistral"
            stylesets(1).BackColor=   8454016
            stylesets(1).Picture=   "FR0616.frx":0060
            stylesets(2).Name=   "Medicamentos"
            stylesets(2).BackColor=   16777215
            stylesets(2).Picture=   "FR0616.frx":007C
            stylesets(3).Name=   "MezclaIV2M"
            stylesets(3).BackColor=   8454143
            stylesets(3).Picture=   "FR0616.frx":0098
            stylesets(4).Name=   "Fluidoterapia"
            stylesets(4).BackColor=   16776960
            stylesets(4).Picture=   "FR0616.frx":00B4
            stylesets(5).Name=   "PRNenOM"
            stylesets(5).BackColor=   12615935
            stylesets(5).Picture=   "FR0616.frx":00D0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19711
            _ExtentY        =   7223
            _StockProps     =   79
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "F�rmulas Magistrales pendientes de Firmar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   4575
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   11445
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   4095
            Index           =   0
            Left            =   120
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   360
            Width           =   11175
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19711
            _ExtentY        =   7223
            _StockProps     =   79
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Visible         =   0   'False
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Visible         =   0   'False
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Visible         =   0   'False
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Visible         =   0   'False
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmFirmarPeticionesFM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmFirmarPeticionesFM (FR0615.FRM)                           *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: JUNIO DEL 2000                                                *
'* DESCRIPCION: Firmar Peticiones de F�rmulas Magistrales               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gstrWhere_PAC As String
Dim gstrWhere_SER As String




Private Sub cboservicio_Click()
Dim intDptoSel As Integer

  auxDBGrid1(0).RemoveAll
  auxDBGrid1(6).RemoveAll
  SSTab1.TabCaption(0) = "Pacientes"
  SSTab1.TabCaption(1) = "Servicios"

  If cboservicio <> "" Then
    intDptoSel = cboservicio
    'Call pCargarSecciones(intDptoSel)
    txtServicio.Text = cboservicio.Columns(1).Value
    chkservicio = 0
  Else
    txtServicio.Text = ""
  End If
  
End Sub

Private Sub cboservicio_CloseUp()
Dim intDptoSel As Integer

  auxDBGrid1(0).RemoveAll
  auxDBGrid1(6).RemoveAll
  SSTab1.TabCaption(0) = "Pacientes"
  SSTab1.TabCaption(1) = "Servicios"

  If cboservicio <> "" Then
    intDptoSel = cboservicio
    'Call pCargarSecciones(intDptoSel)
    txtServicio.Text = cboservicio.Columns(1).Value
    chkservicio = 0
  Else
    txtServicio.Text = ""
  End If

End Sub


Private Sub chkservicio_Click()
  
  If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
    'lvwSec.ListItems.Clear
  End If
  auxDBGrid1(0).RemoveAll
  auxDBGrid1(6).RemoveAll
  SSTab1.TabCaption(0) = "Pacientes"
  SSTab1.TabCaption(1) = "Servicios"

End Sub

Private Sub cmdAnular_Click()
Screen.MousePointer = vbHourglass
cmdAnular.Enabled = False

  If SSTab1.Tab = 0 Then
    If auxDBGrid1(0).Rows > 0 Then
      gstrLlamador = "FrmFirmarPeticionesFM.Anular.Paciente"
      gcodigopeticion = auxDBGrid1(0).Columns("C�d.Petici�n").Value
    Else
      gstrLlamador = ""
      cmdAnular.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  Else
    If auxDBGrid1(6).Rows > 0 Then
      gstrLlamador = "FrmFirmarPeticionesFM.Anular.Servicio"
      gcodigopeticion = auxDBGrid1(6).Columns("C�d.Necesidad").Value
    Else
      gstrLlamador = ""
      cmdAnular.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  End If
  Screen.MousePointer = vbDefault
  Call objsecurity.LaunchProcess("FR0606")
  Screen.MousePointer = vbHourglass
  gstrLlamador = ""
  Call cmdfiltrar_Click

cmdAnular.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdfiltrar_Click()
Dim strpersona As String
Dim rstpersona As rdoResultset


Screen.MousePointer = vbHourglass
cmdfiltrar.Enabled = False
gstrWhere_PAC = "-1=-1"
gstrWhere_SER = "-1=-1"

If Option1(0).Value = True Then 'Historia
  If IsNumeric(Text1(1).Text) Then
    strpersona = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
    Set rstpersona = objApp.rdoConnect.OpenResultset(strpersona)
    gstrWhere_PAC = gstrWhere_PAC & " AND CI21CODPERSONA=" & rstpersona.rdoColumns("CI21CODPERSONA").Value
    rstpersona.Close
    Set rstpersona = Nothing
'  Else
'    auxDBGrid1(0).RemoveAll
'    cmdfiltrar.Enabled = True
'    Screen.MousePointer = vbDefault
'    Exit Sub
  End If
Else
   If Option1(1).Value = True Then 'Servicio
      If IsNumeric(cboservicio.Value) Then
        gstrWhere_PAC = gstrWhere_PAC & " AND AD02CODDPTO=" & cboservicio.Value
        gstrWhere_SER = gstrWhere_SER & " AND AD02CODDPTO=" & cboservicio.Value
      End If
   Else
      If Option1(2).Value = True Then 'Fechas
          'fecha inicio/fecha fin
          If IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
            gstrWhere_PAC = gstrWhere_PAC & " AND TRUNC(FR66FECREDACCION) BETWEEN " & _
                     "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
                     "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
            gstrWhere_SER = gstrWhere_SER & " AND TRUNC(FR55FECPETICION) BETWEEN " & _
                     "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
                     "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcinicio.Date) And Not IsDate(dtcfin.Date) Then
            gstrWhere_PAC = gstrWhere_PAC & " AND TRUNC(FR66FECREDACCION) BETWEEN " & _
                     "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
                     "TO_DATE('31/12/9999','DD/MM/YYYY')"
            gstrWhere_SER = gstrWhere_SER & " AND TRUNC(FR55FECPETICION) BETWEEN " & _
                     "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
                     "TO_DATE('31/12/9999','DD/MM/YYYY')"
          End If
          If Not IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
            gstrWhere_PAC = gstrWhere_PAC & " AND TRUNC(FR66FECREDACCION) BETWEEN " & _
                     "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
                     "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
            gstrWhere_SER = gstrWhere_SER & " AND TRUNC(FR55FECPETICION) BETWEEN " & _
                     "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
                     "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
          End If
      End If
    End If
End If

Call Refrescar_Grid_Pac
Call Refrescar_Grid_Serv

Call Calcular_Registros

cmdfiltrar.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdOM_Click()

Screen.MousePointer = vbHourglass
cmdOM.Enabled = False

  If SSTab1.Tab = 0 Then
    If auxDBGrid1(0).Rows > 0 Then
      gstrLlamador = "FrmFirmarPeticionesFM.Paciente"
      gcodigopeticion = auxDBGrid1(0).Columns("C�d.Petici�n").Value
    Else
      gstrLlamador = ""
      cmdOM.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  Else
    If auxDBGrid1(6).Rows > 0 Then
      gstrLlamador = "FrmFirmarPeticionesFM.Servicio"
      gcodigopeticion = auxDBGrid1(6).Columns("C�d.Necesidad").Value
    Else
      gstrLlamador = ""
      cmdOM.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  End If
  Screen.MousePointer = vbDefault
  Call objsecurity.LaunchProcess("FR0606")
  Screen.MousePointer = vbHourglass
  gstrLlamador = ""
  Call cmdfiltrar_Click

cmdOM.Enabled = True
Screen.MousePointer = vbDefault

End Sub



Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset

    cboservicio.RemoveAll
    stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
           "WHERE SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) " & _
           " order by AD02DESDPTO "
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While (Not rsta.EOF)
        Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
    
    Call cmdfiltrar_Click

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim strKey As String
Dim k As Integer
Dim j As Integer
Dim i As Integer

Set objWinInfo = New clsCWWin

Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                              Me, tlbToolbar1, stbStatusBar1, _
                              cwWithAll)

For k = 1 To 29
  If k <> 6 Then
    tlbToolbar1.Buttons(k).Enabled = False
  End If
Next k

auxDBGrid1(0).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 10
  Call auxDBGrid1(0).Columns.Add(j)
Next j
i = 0
auxDBGrid1(0).Columns(i).Caption = "C�d.Petici�n"
auxDBGrid1(0).Columns(i).Visible = False
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1300
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Fecha"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Hora"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 600
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Historia"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 700
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Nombre"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Apellido 1�"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1300
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Apellido 2�"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1300
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Dr."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1100
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Servicio"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1400
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Proceso"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1100
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Asistencia"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1100

auxDBGrid1(0).Redraw = True

gstrWhere_PAC = " -1=0 "
'Call Refrescar_Grid_Pac

auxDBGrid1(6).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 4
  Call auxDBGrid1(6).Columns.Add(j)
Next j
i = 0
auxDBGrid1(6).Columns(i).Caption = "C�d.Necesidad"
auxDBGrid1(6).Columns(i).Visible = False
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 1300
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Fecha"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Servicio"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Secci�n"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Dr."
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 1500

auxDBGrid1(6).Redraw = True

gstrWhere_SER = " -1=0 "
'lvwSec.ColumnHeaders.Add , , , 2000
'Call Refrescar_Grid_Serv

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
'  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
'  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
'  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
'  Call objWinInfo.WinDeRegister
'  Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean

  If strFormName = "Tipos de Interacci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub




Private Sub SSTab1_Click(PreviousTab As Integer)
Dim intDptoSel As Integer

  If cboservicio <> "" Then
    intDptoSel = cboservicio
    'Call pCargarSecciones(intDptoSel)
    txtServicio.Text = cboservicio.Columns(1).Value
    chkservicio = 0
  Else
    txtServicio.Text = ""
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub Text1_Change(Index As Integer)
If Index = 1 Then
  auxDBGrid1(0).RemoveAll
  auxDBGrid1(6).RemoveAll
  Text1(2).Text = ""
  SSTab1.TabCaption(0) = "Pacientes"
  SSTab1.TabCaption(1) = "Servicios"
End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente As String

If KeyAscii = 13 Then
  If IsNumeric(Text1(1).Text) Then
      stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
        strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
      End If
      If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
        strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
      End If
      If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
        strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
      End If
      Text1(2).Text = strpaciente
      rsta.Close
      Set rsta = Nothing
  End If
End If
End Sub

Private Sub Text1_LostFocus(Index As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente As String

If IsNumeric(Text1(1).Text) Then
    stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
      strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
    End If
    If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
      strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
    End If
    If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
      strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
    End If
    Text1(2).Text = strpaciente
    rsta.Close
    Set rsta = Nothing
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
'   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
'  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Refrescar_Grid_Serv()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim Departamento As String
Dim Seccion As String
Dim Medico As String
Dim strAux As String
Dim rstaux As rdoResultset

Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(6).Redraw = False
auxDBGrid1(6).RemoveAll
  
'peticiones redactadas de F�rmulas Magistrales
strSelect = "SELECT * FROM FR5500"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR55INDFM=-1 AND FR26CODESTPETIC=1 AND " & gstrWhere_SER
strSelect = strSelect & " ORDER BY FR55CODNECESUNID DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF

  If Not IsNull(rsta("AD02CODDPTO").Value) Then
    strAux = "SELECT AD02DESDPTO FROM AD0200 WHERE "
    strAux = strAux & " AD02CODDPTO=" & rsta("AD02CODDPTO").Value
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Departamento = rstaux("AD02DESDPTO").Value
  Else
    Departamento = " "
  End If
  If Not IsNull(rsta("AD41CODSECCION").Value) Then
    strAux = "SELECT AD41DESSECCION FROM AD4100 WHERE "
    strAux = strAux & " AD41CODSECCION=" & rsta("AD41CODSECCION").Value
    strAux = strAux & " AND "
    strAux = strAux & " AD02CODDPTO=" & rsta("AD02CODDPTO").Value
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Seccion = rstaux("AD41DESSECCION").Value
  Else
    Seccion = " "
  End If
  If Not IsNull(rsta("SG02COD_PDS").Value) Then
    strAux = "SELECT SG02APE1 FROM SG0200 WHERE "
    strAux = strAux & " SG02COD=" & "'" & rsta("SG02COD_PDS").Value & "'"
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Medico = rstaux("SG02APE1").Value
  Else
    Medico = " "
  End If
  rstaux.Close
  Set rstaux = Nothing

  strlinea = rsta("FR55CODNECESUNID").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55FECPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & Departamento & Chr(vbKeyTab)
  strlinea = strlinea & Seccion & Chr(vbKeyTab)
  strlinea = strlinea & Medico
  
  auxDBGrid1(6).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(6).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Refrescar_Grid_Pac()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim rstaux As rdoResultset
Dim strAux As String
Dim Medico As String
Dim dpto As String
Dim NumHistoria As String
Dim NombrePac As String
Dim PriApel As String
Dim SegApel As String
Dim Proceso As String
Dim asistencia As String


Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(0).Redraw = False
auxDBGrid1(0).RemoveAll
  
'peticiones redactadas de F�rmulas Magistrales
strSelect = "SELECT * FROM FR6600"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR66INDFM=-1 AND FR26CODESTPETIC=1 AND " & gstrWhere_PAC

strSelect = strSelect & " ORDER BY FR66CODPETICION DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF

  'Persona
  strAux = "SELECT CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 "
  strAux = strAux & " WHERE CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not IsNull(rstaux.rdoColumns("CI22NUMHISTORIA").Value) Then
    NumHistoria = rstaux.rdoColumns("CI22NUMHISTORIA").Value
  Else
    NumHistoria = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22NOMBRE").Value) Then
    NombrePac = rstaux.rdoColumns("CI22NOMBRE").Value
  Else
    NombrePac = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22PRIAPEL").Value) Then
    PriApel = rstaux.rdoColumns("CI22PRIAPEL").Value
  Else
    PriApel = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22SEGAPEL").Value) Then
    SegApel = rstaux.rdoColumns("CI22SEGAPEL").Value
  Else
    SegApel = " "
  End If
  'M�dico solicitante
  strAux = "SELECT SG02APE1,SG02NUMCOLEGIADO FROM SG0200 WHERE "
  strAux = strAux & " SG02COD=" & "'" & rsta.rdoColumns("SG02COD_MED").Value & "'"
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not rstaux.EOF Then
    If Not IsNull(rstaux.rdoColumns("SG02APE1").Value) Then
      Medico = rstaux.rdoColumns("SG02APE1").Value
    Else
      Medico = " "
    End If
  Else
    Medico = " "
  End If
  'Departamento
  strAux = "SELECT AD02DESDPTO FROM AD0200 WHERE "
  strAux = strAux & " AD02CODDPTO=" & rsta.rdoColumns("AD02CODDPTO").Value
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not IsNull(rstaux.rdoColumns("AD02DESDPTO").Value) Then
    dpto = rstaux.rdoColumns("AD02DESDPTO").Value
  Else
    dpto = " "
  End If
  rstaux.Close
  Set rstaux = Nothing
  'Proceso
  If Not IsNull(rsta.rdoColumns("AD07CODPROCESO").Value) Then
    Proceso = rsta.rdoColumns("AD07CODPROCESO").Value
  Else
    Proceso = " "
  End If
  'Asistencia
  If Not IsNull(rsta.rdoColumns("AD01CODASISTENCI").Value) Then
    asistencia = rsta.rdoColumns("AD01CODASISTENCI").Value
  Else
    asistencia = " "
  End If

  strlinea = rsta("FR66CODPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECREDACCION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66HORAREDACCI").Value & Chr(vbKeyTab)
  strlinea = strlinea & NumHistoria & Chr(vbKeyTab)
  strlinea = strlinea & NombrePac & Chr(vbKeyTab)
  strlinea = strlinea & PriApel & Chr(vbKeyTab)
  strlinea = strlinea & SegApel & Chr(vbKeyTab)
  strlinea = strlinea & Medico & Chr(vbKeyTab)
  strlinea = strlinea & dpto & Chr(vbKeyTab)
  strlinea = strlinea & Proceso & Chr(vbKeyTab)
  strlinea = strlinea & asistencia
    
  auxDBGrid1(0).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub


Private Sub pCargarSecciones(intDptoSel%)
'Dim SQL$, qry As rdoQuery, rs As rdoResultset
'Dim item As ListItem
'
'    'se cargan los secciones del Dpto. seleccionado
'    lvwSec.ListItems.Clear
'
'    If SSTab1.Tab = 0 Then
'      lvwSec.Visible = False
'      Label1(1).Visible = False
'      Exit Sub
'    End If
'
'    SQL = "SELECT AD41CODSECCION,AD41DESSECCION"
'    SQL = SQL & " FROM AD4100"
'    SQL = SQL & " WHERE AD02CODDPTO = ?"
'    SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
'    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'    qry(0) = intDptoSel
'    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'
'    If Not rs.EOF Then
'      Set item = lvwSec.ListItems.Add(, , "TODAS")
'      item.Tag = 0
'      item.Selected = True
'      Set item = lvwSec.ListItems.Add(, , "Sin Secci�n")
'      item.Tag = intDptoSel
'      lvwSec.Visible = True
'      Label1(1).Visible = True
'    Else
'      lvwSec.Visible = False
'      Label1(1).Visible = False
'    End If
'
'    Do While Not rs.EOF
'      Set item = lvwSec.ListItems.Add(, , rs!AD41DESSECCION)
'      item.Tag = rs!AD41CODSECCION
'      rs.MoveNext
'    Loop
'    rs.Close
'    qry.Close

End Sub

Private Sub Calcular_Registros()
Dim stra As String
Dim rsta As rdoResultset
Dim cantservicio As Long
Dim cantpaciente As Long

stra = "SELECT COUNT(*) FROM FR6600 WHERE  FR66INDFM=-1 "
stra = stra & " AND FR26CODESTPETIC=1 AND " & gstrWhere_PAC
Set rsta = objApp.rdoConnect.OpenResultset(stra)
If Not rsta.EOF Then
    cantpaciente = rsta.rdoColumns(0).Value
    SSTab1.TabCaption(0) = "Pacientes : " & cantpaciente
Else
    SSTab1.TabCaption(0) = "Pacientes : " & 0
End If
stra = "SELECT COUNT(*) FROM FR5500 WHERE  FR55INDFM=-1 AND FR26CODESTPETIC=1"
stra = stra & " AND " & gstrWhere_SER
Set rsta = objApp.rdoConnect.OpenResultset(stra)
If Not rsta.EOF Then
    cantservicio = rsta.rdoColumns(0).Value
    SSTab1.TabCaption(1) = "Servicios : " & cantservicio
Else
    SSTab1.TabCaption(1) = "Servicios : " & 0
End If

rsta.Close
Set rsta = Nothing
End Sub


