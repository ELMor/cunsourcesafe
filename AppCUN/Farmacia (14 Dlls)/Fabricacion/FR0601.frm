VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form FrmProcFab 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Fabricaci�n. F�rmulas Magistrales Pendientes. Proceso de Fabricaci�n"
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   40
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Fabricaci�n Producto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7200
      Index           =   1
      Left            =   120
      TabIndex        =   41
      Top             =   600
      Width           =   4695
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6555
         Index           =   0
         Left            =   120
         TabIndex        =   0
         Top             =   360
         Width           =   4440
         _ExtentX        =   7832
         _ExtentY        =   11562
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   5
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   39
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabGeneral1 
      Height          =   7095
      Left            =   4920
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   720
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   12515
      _Version        =   327681
      Tabs            =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Proceso"
      TabPicture(0)   =   "FR0601.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Operaci�n"
      TabPicture(1)   =   "FR0601.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(2)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6615
         Index           =   2
         Left            =   -74760
         TabIndex        =   48
         Top             =   360
         Width           =   6300
         Begin TabDlg.SSTab tabTab1 
            Height          =   6255
            Index           =   2
            Left            =   120
            TabIndex        =   49
            TabStop         =   0   'False
            Top             =   240
            Width           =   6135
            _ExtentX        =   10821
            _ExtentY        =   11033
            _Version        =   327681
            TabOrientation  =   3
            Tabs            =   2
            TabsPerRow      =   1
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0601.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(3)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(14)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(15)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(24)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(25)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(26)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(16)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(2)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(5)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(4)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(17)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "CommonDialog1"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(5)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(14)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtCodFarmaceutico"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(13)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(4)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(7)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(25)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(26)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(27)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(28)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "Frame1(1)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "cmdIniciarOperacion"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "cmdFinalizarOperacion"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(8)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txtText1(15)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txtText1(16)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "txtText1(10)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).Control(29)=   "txtTiempoEjecucion"
            Tab(0).Control(29).Enabled=   0   'False
            Tab(0).Control(30)=   "txtEstadoOperacionCod"
            Tab(0).Control(30).Enabled=   0   'False
            Tab(0).Control(31)=   "txtEstadoOperacion"
            Tab(0).Control(31).Enabled=   0   'False
            Tab(0).Control(32)=   "cmdWord"
            Tab(0).Control(32).Enabled=   0   'False
            Tab(0).Control(33)=   "cmdExplorador(1)"
            Tab(0).Control(33).Enabled=   0   'False
            Tab(0).Control(34)=   "txtText1(17)"
            Tab(0).Control(34).Enabled=   0   'False
            Tab(0).ControlCount=   35
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0601.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   120
               ScrollBars      =   2  'Vertical
               TabIndex        =   70
               Tag             =   "Path del Archivo Descripci�n Operaci�n|Path del Archivo Descripci�n Operaci�n"
               Top             =   4560
               Width           =   4350
            End
            Begin VB.CommandButton cmdExplorador 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   350
               Index           =   1
               Left            =   5040
               TabIndex        =   69
               Top             =   4560
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.CommandButton cmdWord 
               Caption         =   "W"
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   350
               Left            =   4560
               TabIndex        =   68
               Top             =   4560
               Width           =   375
            End
            Begin VB.TextBox txtEstadoOperacion 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   3000
               Locked          =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   26
               TabStop         =   0   'False
               Tag             =   "Estado Operaci�n"
               Top             =   2160
               Width           =   2520
            End
            Begin VB.TextBox txtEstadoOperacionCod 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR37CODESTOF"
               Height          =   330
               HelpContextID   =   30101
               Left            =   4680
               TabIndex        =   25
               Tag             =   "Estado Operaci�n"
               Top             =   1920
               Visible         =   0   'False
               Width           =   420
            End
            Begin VB.TextBox txtTiempoEjecucion 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Left            =   2280
               TabIndex        =   28
               Tag             =   "Tiempo de Ejecuci�n|Tiempo de Ejecuci�n"
               Top             =   2760
               Width           =   990
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR70CODOPERACION"
               Height          =   330
               Index           =   10
               Left            =   2160
               Locked          =   -1  'True
               TabIndex        =   16
               TabStop         =   0   'False
               Tag             =   "C�digo Operaci�n"
               Top             =   120
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   16
               Left            =   1320
               TabIndex        =   33
               Tag             =   "C�digo de Persona"
               Top             =   4800
               Visible         =   0   'False
               Width           =   990
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR24TIEMPREAL"
               Height          =   330
               Index           =   15
               Left            =   3360
               TabIndex        =   29
               Tag             =   "Tiempo de Ejecuci�n|Tiempo de Ejecuci�n"
               Top             =   2760
               Visible         =   0   'False
               Width           =   990
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   8
               Left            =   720
               Locked          =   -1  'True
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "C�digo Producto|C�digo Producto"
               Top             =   1080
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.CommandButton cmdFinalizarOperacion 
               Caption         =   "Finalizar"
               Height          =   375
               Left            =   2640
               TabIndex        =   37
               Top             =   5640
               Width           =   1215
            End
            Begin VB.CommandButton cmdIniciarOperacion 
               Caption         =   "Iniciar"
               Height          =   375
               Left            =   1320
               TabIndex        =   36
               Top             =   5640
               Width           =   1215
            End
            Begin VB.Frame Frame1 
               Caption         =   "Se debe tener en cuenta"
               ForeColor       =   &H00FF0000&
               Height          =   1215
               Index           =   1
               Left            =   120
               TabIndex        =   63
               Top             =   3120
               Width           =   5175
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Guardarlo en el frigor�fico"
                  DataField       =   "FR73INDFRIGO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   5
                  Left            =   360
                  TabIndex        =   31
                  Tag             =   "Guardarlo en Frigor�fico"
                  Top             =   570
                  Width           =   2775
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Protegerlo de la luz"
                  DataField       =   "FR73INDPROTLUZ"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   360
                  TabIndex        =   30
                  Tag             =   "Protegerlo de la luz"
                  Top             =   240
                  Width           =   2415
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Guardarlo en el congelador"
                  DataField       =   "FR73INDCONGE"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   360
                  TabIndex        =   32
                  Tag             =   "Guardarlo en Congelador"
                  Top             =   840
                  Width           =   3015
               End
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   28
               Left            =   2040
               Locked          =   -1  'True
               TabIndex        =   24
               TabStop         =   0   'False
               Tag             =   "U.M. Unidad de Medida"
               Top             =   2160
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR24CANTNECESARIA"
               Height          =   330
               Index           =   27
               Left            =   1080
               Locked          =   -1  'True
               TabIndex        =   23
               TabStop         =   0   'False
               Tag             =   "Cantidad"
               Top             =   2160
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FRH7CODFORMFAR"
               Height          =   330
               Index           =   26
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "F.F. Forma Farmac�utica"
               Top             =   2160
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CODINTFARSEG"
               Height          =   330
               Index           =   25
               Left            =   960
               Locked          =   -1  'True
               TabIndex        =   20
               TabStop         =   0   'False
               Tag             =   "Seguridad"
               Top             =   1560
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73DESPRODUCTO"
               Enabled         =   0   'False
               Height          =   330
               Index           =   7
               Left            =   1200
               Locked          =   -1  'True
               TabIndex        =   21
               TabStop         =   0   'False
               Tag             =   "Descripci�n Producto"
               Top             =   1560
               Width           =   4335
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CODINTFAR"
               Height          =   330
               Index           =   4
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   19
               TabStop         =   0   'False
               Tag             =   "C�digo Interno Farmacia"
               Top             =   1560
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   13
               Left            =   960
               Locked          =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   35
               TabStop         =   0   'False
               Tag             =   "Nombre"
               Top             =   5160
               Width           =   4560
            End
            Begin VB.TextBox txtCodFarmaceutico 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Left            =   120
               TabIndex        =   34
               Tag             =   "C�digo de Persona"
               Top             =   5160
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR24TIEMPPREV"
               Height          =   330
               Index           =   14
               Left            =   120
               TabIndex        =   27
               Tag             =   "Tiempo de Preparaci�n|Tiempo de Preparaci�n"
               Top             =   2760
               Width           =   990
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR70DESOPERACION"
               Height          =   810
               Index           =   5
               Left            =   120
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   17
               Tag             =   "Descripci�n Operaci�n"
               Top             =   480
               Width           =   5415
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5835
               Index           =   0
               Left            =   -74880
               TabIndex        =   38
               TabStop         =   0   'False
               Top             =   120
               Width           =   5175
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   9128
               _ExtentY        =   10292
               _StockProps     =   79
            End
            Begin MSComDlg.CommonDialog CommonDialog1 
               Left            =   5280
               Top             =   4560
               _ExtentX        =   847
               _ExtentY        =   847
               _Version        =   327681
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Archivo Descripci�n Operaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   120
               TabIndex        =   71
               Top             =   4320
               Width           =   2775
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Estado Operaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   3000
               TabIndex        =   65
               Top             =   1920
               Width           =   1530
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   1200
               TabIndex        =   58
               Top             =   1320
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   57
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Farmac�utico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   120
               TabIndex        =   62
               Top             =   4920
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   26
               Left            =   2040
               TabIndex        =   61
               Top             =   1920
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   1080
               TabIndex        =   60
               Top             =   1920
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   120
               TabIndex        =   59
               Top             =   1920
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               BackColor       =   &H8000000A&
               Caption         =   "Tiempo de Ejecuci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   2280
               TabIndex        =   52
               Top             =   2520
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tiempo de Preparaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   120
               TabIndex        =   51
               Top             =   2520
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Operaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   120
               TabIndex        =   50
               Top             =   240
               Width           =   2175
            End
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6615
         Index           =   0
         Left            =   300
         TabIndex        =   43
         Top             =   330
         Width           =   6300
         Begin TabDlg.SSTab tabTab1 
            Height          =   6255
            Index           =   0
            Left            =   120
            TabIndex        =   44
            TabStop         =   0   'False
            Top             =   240
            Width           =   6015
            _ExtentX        =   10610
            _ExtentY        =   11033
            _Version        =   327681
            TabOrientation  =   3
            Tabs            =   2
            TabsPerRow      =   1
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0601.frx":0070
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(1)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(10)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(6)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(0)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(22)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(23)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(12)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(1)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(3)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(6)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(0)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(22)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(23)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(24)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "Frame1(0)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "cmdIniciarProceso"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "cmdFinalizarProceso"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(2)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(9)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtEstadoProceso"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtEstadoProcesoCod"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).ControlCount=   21
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0601.frx":008C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(2)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtEstadoProcesoCod 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30101
               Left            =   4560
               TabIndex        =   67
               Tag             =   "Estado Proceso"
               Top             =   2280
               Visible         =   0   'False
               Width           =   420
            End
            Begin VB.TextBox txtEstadoProceso 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   3120
               Locked          =   -1  'True
               TabIndex        =   66
               TabStop         =   0   'False
               Tag             =   "Estado Proceso"
               Top             =   2520
               Width           =   2040
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR59CODORDFABR"
               Height          =   330
               Index           =   9
               Left            =   2040
               Locked          =   -1  'True
               TabIndex        =   1
               TabStop         =   0   'False
               Tag             =   "C�digo Proceso"
               Top             =   240
               Visible         =   0   'False
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   2
               Left            =   1080
               Locked          =   -1  'True
               TabIndex        =   3
               TabStop         =   0   'False
               Tag             =   "C�digo Producto|C�digo Producto"
               Top             =   1440
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.CommandButton cmdFinalizarProceso 
               Caption         =   "Finalizar"
               Height          =   375
               Left            =   2760
               TabIndex        =   14
               Top             =   4680
               Width           =   1215
            End
            Begin VB.CommandButton cmdIniciarProceso 
               Caption         =   "Iniciar"
               Height          =   375
               Left            =   1440
               TabIndex        =   13
               Top             =   4680
               Width           =   1215
            End
            Begin VB.Frame Frame1 
               Caption         =   "Se debe tener en cuenta"
               ForeColor       =   &H00FF0000&
               Height          =   1575
               Index           =   0
               Left            =   240
               TabIndex        =   56
               Top             =   3000
               Width           =   5175
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Guardarlo en el congelador"
                  DataField       =   "FR73INDCONGE"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   2
                  Left            =   360
                  TabIndex        =   12
                  Tag             =   "Guardarlo en el congelador"
                  Top             =   1080
                  Width           =   3015
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Protegerlo de la luz"
                  DataField       =   "FR73INDPROTLUZ"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   360
                  TabIndex        =   10
                  Tag             =   "Protegerlo de la luz"
                  Top             =   360
                  Width           =   2415
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Guardarlo en el frigor�fico"
                  DataField       =   "FR73INDFRIGO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   360
                  TabIndex        =   11
                  Tag             =   "Guardarlo en el frigor�fico"
                  Top             =   720
                  Width           =   2775
               End
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   24
               Left            =   2160
               Locked          =   -1  'True
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "U.M. Unidad de Medida"
               Top             =   2520
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR59CANTFABRICAR"
               Height          =   330
               Index           =   23
               Left            =   1200
               Locked          =   -1  'True
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Cantidad"
               Top             =   2520
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FRH7CODFORMFAR"
               Height          =   330
               Index           =   22
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "F.F. Forma Farmac�utica"
               Top             =   2520
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CODINTFARSEG"
               Height          =   330
               Index           =   0
               Left            =   1080
               Locked          =   -1  'True
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Seuridad"
               Top             =   1920
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73CODINTFAR"
               Height          =   330
               Index           =   6
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   4
               TabStop         =   0   'False
               Tag             =   "C�digo Interno Farmacia"
               Top             =   1920
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR73DESPRODUCTO"
               Enabled         =   0   'False
               Height          =   330
               Index           =   3
               Left            =   1320
               Locked          =   -1  'True
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Descripci�n Producto"
               Top             =   1920
               Width           =   4095
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR59DESPROCESO"
               Height          =   810
               Index           =   1
               Left            =   150
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   2
               Tag             =   "Descripci�n Proceso|Descripci�n Proceso"
               Top             =   600
               Width           =   5280
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5745
               Index           =   2
               Left            =   -74850
               TabIndex        =   15
               TabStop         =   0   'False
               Top             =   120
               Width           =   5295
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   9340
               _ExtentY        =   10134
               _StockProps     =   79
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Estado Proceso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   3120
               TabIndex        =   64
               Top             =   2280
               Width           =   1350
            End
            Begin VB.Label lblLabel1 
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   2160
               TabIndex        =   55
               Top             =   2280
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   1200
               TabIndex        =   54
               Top             =   2280
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   240
               TabIndex        =   53
               Top             =   2280
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   240
               TabIndex        =   47
               Top             =   1680
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   1320
               TabIndex        =   46
               Top             =   1680
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Proceso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   45
               Top             =   360
               Width           =   1815
            End
         End
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   8280
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0601.frx":00A8
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0601.frx":0282
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0601.frx":045C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0601.frx":0636
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmProcFab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmProcFab (FR0601.FRM)                                      *
'* AUTOR: AITOR VI�UELA GARC�A                                          *
'* FECHA: MAYO DE 1999                                                  *
'* DESCRIPCION: Ver Proceso de Fabricaci�n de un producto               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
'* Copiada de

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
' Declara variables globales al m�dulo.
Dim Enarbol As Boolean
Dim Enabrir As Boolean
Dim EnPostWrite As Boolean

Private Sub rellenar_TreeView()
Dim strRelproc As String
Dim rstRelproc As rdoResultset
Dim strRelOper As String
Dim rstRelOper As rdoResultset
Dim strRelProd As String
Dim rstRelProd As rdoResultset
Dim producto, DesProducto As Variant
Dim Proceso, DesProceso As Variant
Dim Operacion, DesOperacion As Variant
Dim ProdHijo, DesProdHijo As Variant
Dim CodPeticion As Variant
Dim CodOrdFab As Variant
Dim strFR7300 As String
Dim rstFR7300 As rdoResultset
  
   'producto = FrmVerPteFab.txtText1(3).Text
   'DesProducto = FrmVerPteFab.txtText1(5).Text
   'CodPeticion = FrmVerPteFab.txtText1(0).Text
    If FrmVerPteFab.auxDBGrid1(1).SelBookmarks.Count > 0 Then
      producto = FrmVerPteFab.auxDBGrid1(1).Columns("C�d.Prod.").Value
      CodPeticion = FrmVerPteFab.auxDBGrid1(1).Columns("C�d.Pet.").Value
    ElseIf FrmVerPteFab.auxDBGrid1(3).SelBookmarks.Count > 0 Then
      producto = FrmVerPteFab.auxDBGrid1(3).Columns("C�d.Prod.").Value
      CodPeticion = FrmVerPteFab.auxDBGrid1(3).Columns("C�d.Nec.").Value
    Else
      Exit Sub
    End If
    

   tvwItems(0).Nodes.Clear
   If FrmVerPteFab.auxDBGrid1(1).SelBookmarks.Count > 0 Then
     strRelproc = "SELECT * FROM FR5900 WHERE FR73CODPRODUCTO=" & producto & " AND FR66CODPETICION = " & CodPeticion
   Else
     strRelproc = "SELECT * FROM FR5900 WHERE FR73CODPRODUCTO=" & producto & " AND FR55CODNECESUNID = " & CodPeticion
   End If
   Set rstRelproc = objApp.rdoConnect.OpenResultset(strRelproc)
   
   strFR7300 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & producto
   Set rstFR7300 = objApp.rdoConnect.OpenResultset(strFR7300)
   DesProducto = rstFR7300("FR73DESPRODUCTO").Value
   rstFR7300.Close
   Set rstFR7300 = Nothing
   
   Call tvwItems(0).Nodes.Add(, , "Producto", producto & ".-" & DesProducto, 4)
   While rstRelproc.EOF = False
      Proceso = rstRelproc("FR59CODORDFABR").Value
      DesProceso = rstRelproc("FR59DESPROCESO").Value
      
      Select Case txtEstadoProcesoCod.Text
      Case 0
         Call tvwItems(0).Nodes.Add("Producto", tvwChild, "Proceso" & Proceso, Proceso & ".-" & DesProceso, 3)
      Case 1
         Call tvwItems(0).Nodes.Add("Producto", tvwChild, "Proceso" & Proceso, Proceso & ".-" & DesProceso, 2)
      Case 2
         Call tvwItems(0).Nodes.Add("Producto", tvwChild, "Proceso" & Proceso, Proceso & ".-" & DesProceso, 1)
      Case Else
         Call tvwItems(0).Nodes.Add("Producto", tvwChild, "Proceso" & Proceso, Proceso & ".-" & DesProceso)
      End Select

      strRelOper = "SELECT * FROM FR2401J WHERE FR59CODORDFABR=" & Proceso & " ORDER BY FR70CODOPERACION"
      Set rstRelOper = objApp.rdoConnect.OpenResultset(strRelOper)
      
      While rstRelOper.EOF = False
         Operacion = rstRelOper("FR70CODOPERACION").Value
         DesOperacion = rstRelOper("FR70DESOPERACION").Value
         ProdHijo = rstRelOper("FR73CODPRODUCTO").Value
         DesProdHijo = rstRelOper("FR73DESPRODUCTO").Value
         Select Case rstRelOper("FR37CODESTOF").Value
         Case 1, 2
            Call tvwItems(0).Nodes.Add("Proceso" & Proceso, tvwChild, "Operacion" & Operacion & "/" & ProdHijo & "/" & "Proceso" & Proceso, Operacion & ".-" & DesOperacion & ";  " & ProdHijo & ".-" & DesProdHijo, 3)
         Case 3, 4
            Call tvwItems(0).Nodes.Add("Proceso" & Proceso, tvwChild, "Operacion" & Operacion & "/" & ProdHijo & "/" & "Proceso" & Proceso, Operacion & ".-" & DesOperacion & ";  " & ProdHijo & ".-" & DesProdHijo, 2)
         Case 5
            Call tvwItems(0).Nodes.Add("Proceso" & Proceso, tvwChild, "Operacion" & Operacion & "/" & ProdHijo & "/" & "Proceso" & Proceso, Operacion & ".-" & DesOperacion & ";  " & ProdHijo & ".-" & DesProdHijo, 1)
         Case Else
            Call tvwItems(0).Nodes.Add("Proceso" & Proceso, tvwChild, "Operacion" & Operacion & "/" & ProdHijo & "/" & "Proceso" & Proceso, Operacion & ".-" & DesOperacion & ";  " & ProdHijo & ".-" & DesProdHijo)
         End Select
         rstRelOper.MoveNext
      Wend
      rstRelOper.Close
      Set rstRelOper = Nothing
      rstRelproc.MoveNext
   Wend
   rstRelproc.Close
   Set rstRelproc = Nothing

   tvwItems(0).Nodes("Producto").Selected = True
   tvwItems(0).Nodes("Producto").Expanded = True

End Sub

Private Sub cmdExplorador_Click(Index As Integer)
Dim intRetVal
Dim strOpenFileName As String
Dim pathFileName As String
Dim finpath As Integer



If Index = 0 Then
Else
    On Error Resume Next
    pathFileName = Dir(txtText1(17).Text)
    finpath = InStr(1, txtText1(17).Text, pathFileName, 1)
    pathFileName = Left(txtText1(17).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(17).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If Err.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(17).SetFocus
        Call objWinInfo.CtrlSet(txtText1(17), "")
        txtText1(17).SetFocus
        Call objWinInfo.CtrlSet(txtText1(17), strOpenFileName)
    End If
End If

End Sub

Private Sub cmdFinalizarOperacion_Click()
Dim strUpdate As String
Dim strSelect As String
Dim rsta As rdoResultset
Dim strAlmFar As String
Dim rstAlmFar As rdoResultset
Dim AlmFar
Dim strAlmFab As String
Dim rstAlmFab As rdoResultset
Dim AlmFab
   
cmdFinalizarOperacion.Enabled = False

If txtText1(9).Text <> "" And txtText1(10).Text <> "" And txtText1(8).Text <> "" Then
   ' Comprobar que est� Iniciada y que no est� Finalizada
   strSelect = "SELECT FR37CODESTOF FROM FR2400 WHERE " & _
               " FR59CODORDFABR=" & txtText1(9).Text & " AND " & _
               " FR70CODOPERACION=" & txtText1(10).Text & _
               " AND FR73CODPRODUCTO=" & txtText1(8).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
   If IsNull(rsta.rdoColumns(0).Value) Then
      ' Pendiente de Iniciar
      ' Ok
      MsgBox "La Operaci�n no esta Iniciada", vbInformation
      cmdFinalizarOperacion.Enabled = True
      Exit Sub
   ElseIf rsta.rdoColumns(0).Value = 1 Then
      ' Generada -> Pendiente de Iniciar
      MsgBox "La Operaci�n no esta Iniciada", vbInformation
      cmdFinalizarOperacion.Enabled = True
      Exit Sub
   ElseIf rsta.rdoColumns(0).Value = 2 Then
      ' Planificada -> Pendiente de Iniciar
      MsgBox "La Operaci�n no esta Iniciada", vbInformation
      cmdFinalizarOperacion.Enabled = True
      Exit Sub
   ElseIf rsta.rdoColumns(0).Value = 3 Or rsta.rdoColumns(0).Value = 4 Then
      ' Iniciada o En Curso
      ' Ok
   ElseIf rsta.rdoColumns(0).Value = 5 Then
      'Terminada
      MsgBox "La Operaci�n ya esta Finalizada", vbInformation
      cmdFinalizarOperacion.Enabled = True
      Exit Sub
   End If
   
   rsta.Close
   Set rsta = Nothing

   ' Poner FR2400.FR32INDESTFAB = 5 -> Fabricaci�n Finalizada
   strUpdate = " UPDATE FR2400 SET FR37CODESTOF=5 WHERE " & _
               " FR59CODORDFABR=" & txtText1(9).Text & " AND " & _
               " FR70CODOPERACION=" & txtText1(10).Text & _
               " AND FR73CODPRODUCTO=" & txtText1(8).Text
   objApp.rdoConnect.Execute strUpdate, 64
   objApp.rdoConnect.Execute "Commit", 64
      
   ' Refrescar
   'Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)
   txtEstadoOperacion.Text = "Terminada"
   tvwItems(0).Nodes("Operacion" & txtText1(10).Text & "/" & txtText1(8).Text & "/" & "Proceso" & txtText1(9).Text).Image = 1
   
'MOVIMIENTOS ALMACEN Insertar_FR8000,Insertar_FR3500
  'se obtiene el almac�n de Farmacia
  strAlmFar = "SELECT FRH2PARAMGEN FROM FRH200 " & _
          "WHERE FRH2CODPARAMGEN=28 "
  Set rstAlmFar = objApp.rdoConnect.OpenResultset(strAlmFar)
  AlmFar = rstAlmFar(0).Value
  rstAlmFar.Close
  Set rstAlmFar = Nothing
  
  'se obtiene el almac�n de Fabricaci�n
  strAlmFab = "SELECT FRH2PARAMGEN FROM FRH200 " & _
          "WHERE FRH2CODPARAMGEN=18"
  Set rstAlmFab = objApp.rdoConnect.OpenResultset(strAlmFab)
  AlmFab = rstAlmFab(0).Value
  rstAlmFab.Close
  Set rstAlmFab = Nothing
  
  Call Insertar_FR8000(AlmFar, AlmFab, 12, txtText1(8), txtText1(27), txtText1(28))
  Call Insertar_FR3500(AlmFar, AlmFab, 12, txtText1(8), txtText1(27), txtText1(28))
End If
cmdFinalizarOperacion.Enabled = True

End Sub

Private Sub cmdFinalizarProceso_Click()
Dim strUpdate As String
Dim strSelect As String
Dim rsta As rdoResultset
Dim strAlmFar As String
Dim rstAlmFar As rdoResultset
Dim AlmFar
Dim strAlmFab As String
Dim rstAlmFab As rdoResultset
Dim AlmFab
   
cmdFinalizarProceso.Enabled = False
If txtText1(9).Text <> "" Then
   ' Comprobar que no est� finalizado
   strSelect = "SELECT FR37CODESTOF FROM FR5900 WHERE " & _
               " FR59CODORDFABR=" & txtText1(9).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
   If rsta.rdoColumns(0).Value = 1 Then
      ' Pendiente de Fabricar
      MsgBox "El Proceso No se puede Finalizar porque No se ha Iniciado", vbInformation
      cmdFinalizarProceso.Enabled = True
      Exit Sub
   ElseIf rsta.rdoColumns(0).Value = 3 Then
      ' Ok
   ElseIf rsta.rdoColumns(0).Value = 5 Then
      MsgBox "El Proceso ya estaba Finalizado", vbInformation
      cmdFinalizarProceso.Enabled = True
      Exit Sub
   End If
   rsta.Close
   Set rsta = Nothing
   
   ' Comprobar que las Operaciones Asociadas est�n Finalizadas
   strSelect = "SELECT COUNT(FR37CODESTOF) FROM FR2400 WHERE " & _
               " FR59CODORDFABR=" & txtText1(9).Text & " AND " & _
               " (FR37CODESTOF <> 5 OR FR37CODESTOF IS NULL)"
   Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
   If rsta.rdoColumns(0).Value <> 0 Then
      ' Pendiente de Fabricar
      MsgBox "El Proceso No se puede Finalizar porque tiene Operaciones Sin Finalizar", vbInformation
      rsta.Close
      Set rsta = Nothing
      cmdFinalizarProceso.Enabled = True
      Exit Sub
   End If
   rsta.Close
   Set rsta = Nothing
   
  strUpdate = " UPDATE FR5900 SET FR37CODESTOF=5 WHERE " & _
              " FR59CODORDFABR=" & txtText1(9).Text
  objApp.rdoConnect.Execute strUpdate, 64
  objApp.rdoConnect.Execute "Commit", 64
   
  If FrmVerPteFab.auxDBGrid1(1).SelBookmarks.Count > 0 Then
   strUpdate = " UPDATE FR6600 SET FR26CODESTPETIC=15 WHERE " & _
               " FR66CODPETICION=" & FrmVerPteFab.auxDBGrid1(1).Columns("C�d.Pet.").Value
  Else
   strUpdate = " UPDATE FR5500 SET FR26CODESTPETIC=15 WHERE " & _
               " FR55CODNECESUNID=" & FrmVerPteFab.auxDBGrid1(3).Columns("C�d.Nec.").Value
  End If
  objApp.rdoConnect.Execute strUpdate, 64
  objApp.rdoConnect.Execute "Commit", 64
   
'MOVIMIENTOS ALMACEN Insertar_FR8000,Insertar_FR3500
  
  'se obtiene el almac�n de Farmacia
  strAlmFar = "SELECT FRH2PARAMGEN FROM FRH200 " & _
          "WHERE FRH2CODPARAMGEN=28 "
  Set rstAlmFar = objApp.rdoConnect.OpenResultset(strAlmFar)
  AlmFar = rstAlmFar(0).Value
  rstAlmFar.Close
  Set rstAlmFar = Nothing
  
  'se obtiene el almac�n de Fabricaci�n
  strAlmFab = "SELECT FRH2PARAMGEN FROM FRH200 " & _
          "WHERE FRH2CODPARAMGEN=18"
  Set rstAlmFab = objApp.rdoConnect.OpenResultset(strAlmFab)
  AlmFab = rstAlmFab(0).Value
  rstAlmFab.Close
  Set rstAlmFab = Nothing
  
  Call Insertar_FR8000(AlmFab, AlmFar, 12, txtText1(2), txtText1(23), txtText1(24))
  Call Insertar_FR3500(AlmFab, AlmFar, 12, txtText1(2), txtText1(23), txtText1(24))
  
  MsgBox "Proceso Finalizado", vbInformation
End If

cmdFinalizarProceso.Enabled = True

Unload Me
   
End Sub

Private Sub cmdIniciarOperacion_Click()
   Dim strSelect As String
   Dim rsta As rdoResultset
   Dim strUpdate As String

   cmdIniciarOperacion.Enabled = False
   cmdFinalizarOperacion.Enabled = False
   
   ' Comprobar que el Proceso asociado est� Iniciado
   If txtEstadoProcesoCod.Text = 1 Then
      MsgBox "Para iniciar una Operacion, ha de haberse iniciado un Proceso", vbInformation
      cmdIniciarOperacion.Enabled = True
      cmdFinalizarOperacion.Enabled = True
      Exit Sub
   End If
   
   ' Comprobar que no est� Iniciada ni Finalizada
   strSelect = "SELECT FR37CODESTOF FROM FR2400 WHERE " & _
               " FR59CODORDFABR=" & txtText1(9).Text & " AND " & _
               " FR70CODOPERACION=" & txtText1(10).Text & _
               " AND FR73CODPRODUCTO=" & txtText1(8).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
   If rsta.EOF Then
      MsgBox "Para iniciar una Operacion, ha de haberse iniciado un Proceso", vbInformation
   Else
      If IsNull(rsta.rdoColumns(0).Value) Then
         ' Pendiente de Iniciar
         ' Ok
      ElseIf rsta.rdoColumns(0).Value = 1 Then
         ' Generada -> Pendiente de Iniciar
         ' Ok
      ElseIf rsta.rdoColumns(0).Value = 2 Then
         ' Planificada -> Pendiente de Iniciar
         ' Ok
      ElseIf rsta.rdoColumns(0).Value = 3 Or rsta.rdoColumns(0).Value = 4 Then
         ' Iniciada o En Curso
         ' Ok
         MsgBox "La Operaci�n ya estaba Iniciada", vbInformation
         cmdIniciarOperacion.Enabled = True
         Exit Sub
      ElseIf rsta.rdoColumns(0).Value = 5 Then
         'Terminada
         MsgBox "La Operaci�n ya esta Finalizada", vbInformation
         cmdIniciarOperacion.Enabled = True
         Exit Sub
      End If
      
      rsta.Close
      Set rsta = Nothing
   
      ' Poner FR2400.FR32INDESTFAB = 3 -> Fabricaci�n Iniciada
      strUpdate = " UPDATE FR2400 SET FR37CODESTOF=3 WHERE " & _
                  " FR59CODORDFABR=" & txtText1(9).Text & " AND " & _
                  " FR70CODOPERACION=" & txtText1(10).Text & _
                  " AND FR73CODPRODUCTO=" & txtText1(8).Text
      objApp.rdoConnect.Execute strUpdate, 64
      objApp.rdoConnect.Execute "Commit", 64
      
      ' Refrescar
      ' Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)
      txtEstadoOperacion.Text = "Iniciada"
'             Call tvwItems(0).Nodes.Add("Proceso" & Proceso, tvwChild, "Operacion" & Operacion & "/" & ProdHijo & "/" & "Proceso" & Proceso, Operacion & ".-" & DesOperacion & ";  " & ProdHijo & ".-" & DesProdHijo, 2)
      tvwItems(0).Nodes("Operacion" & txtText1(10).Text & "/" & txtText1(8).Text & "/" & "Proceso" & txtText1(9).Text).Image = 2
   End If
   cmdIniciarOperacion.Enabled = True
   cmdFinalizarOperacion.Enabled = True

End Sub

Private Sub cmdIniciarProceso_Click()
Dim strUpdate As String
Dim strSelect As String
Dim strDelete As String
Dim rsta As rdoResultset
   
   cmdIniciarProceso.Enabled = False
   
   ' Comprobar que no est� Iniciado ni Finalizado
   strSelect = "SELECT FR37CODESTOF FROM FR5900 WHERE " & _
               " FR59CODORDFABR=" & txtText1(9).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
   If rsta.rdoColumns(0).Value = 1 Then
      ' Pendiente de Fabricar
      ' Ok
   ElseIf rsta.rdoColumns(0).Value = 3 Then
      ' Ok
      MsgBox "El Proceso ya estaba Iniciado", vbInformation
      cmdIniciarProceso.Enabled = True
      Exit Sub
   ElseIf rsta.rdoColumns(0).Value = 5 Then
      MsgBox "El Proceso ya esta Finalizado", vbInformation
      cmdIniciarProceso.Enabled = True
      Exit Sub
   End If
   rsta.Close
   Set rsta = Nothing

  strUpdate = " UPDATE FR5900 SET FR37CODESTOF=3 WHERE " & _
              " FR59CODORDFABR=" & txtText1(9).Text
  objApp.rdoConnect.Execute strUpdate, 64
  objApp.rdoConnect.Execute "Commit", 64
   
  ' Borrar los dem�s procesos asociados al producto
  ' 1� Borrar los productos asociados a los procesos
  ' 2� Borrar los procesos asociados
  If FrmVerPteFab.auxDBGrid1(1).SelBookmarks.Count > 0 Then
    strDelete = " DELETE FR2400 WHERE " & _
                " FR59CODORDFABR IN " & _
                " (SELECT FR59CODORDFABR FROM FR5900 WHERE" & _
                " FR66CODPETICION=" & FrmVerPteFab.auxDBGrid1(1).Columns("C�d.Pet.").Value & _
                " AND FR59CODORDFABR <> " & txtText1(9).Text & ")"
    objApp.rdoConnect.Execute strDelete, 64
    objApp.rdoConnect.Execute "Commit", 64
    strDelete = " DELETE FR5900 WHERE " & _
                " FR66CODPETICION=" & FrmVerPteFab.auxDBGrid1(1).Columns("C�d.Pet.").Value & _
                " AND FR59CODORDFABR <> " & txtText1(9).Text
    objApp.rdoConnect.Execute strDelete, 64
    objApp.rdoConnect.Execute "Commit", 64
  ElseIf FrmVerPteFab.auxDBGrid1(3).SelBookmarks.Count > 0 Then
    strDelete = " DELETE FR2400 WHERE " & _
                " FR59CODORDFABR IN " & _
                " (SELECT FR59CODORDFABR FROM FR5900 WHERE" & _
                " FR55CODNECESUNID=" & FrmVerPteFab.auxDBGrid1(3).Columns("C�d.Nec.").Value & _
                " AND FR59CODORDFABR <> " & txtText1(9).Text & ")"
    objApp.rdoConnect.Execute strDelete, 64
    objApp.rdoConnect.Execute "Commit", 64
    strDelete = " DELETE FR5900 WHERE " & _
                " FR55CODNECESUNID=" & FrmVerPteFab.auxDBGrid1(3).Columns("C�d.Nec.").Value & _
                " AND FR59CODORDFABR <> " & txtText1(9).Text
    objApp.rdoConnect.Execute strDelete, 64
    objApp.rdoConnect.Execute "Commit", 64
  End If
      
  ' Recargar el arbol
  Call rellenar_TreeView
  txtEstadoProcesoCod.Text = 3 ' Fabricaci�n Iniciada
  tvwItems(0).Nodes("Proceso" & txtText1(9).Text).Image = 2
  
  cmdIniciarProceso.Enabled = True

End Sub


Private Sub cmdWord_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strpathword As String
  
On Error GoTo error_shell
  
'Par�metros generales
'5,Ubicaci�n de Word,C:\Archivos de programa\Microsoft Office\Office\winword.exe,TEXTO
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=5"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strpathword = rsta(0).Value
  Else
    rsta.Close
    Set rsta = Nothing
    MsgBox "Debe introducir el Path del Word en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 5 " & Chr(13) & _
           "Ej.: C:\Archivos de programa\Microsoft Office\Office\winword.exe", vbInformation
    Exit Sub
  End If
  rsta.Close
  Set rsta = Nothing
  
  If txtText1(17).Text = "" Then
    MsgBox "Debe introducir el Path y el nombre del fichero del documento", vbInformation
    Exit Sub
  End If
  
  'Shell strpathword & " " & txtText1(17).Text, vbMaximizedFocus
  'Shell strpathword, vbMaximizedFocus
  Shell strpathword & " " & Chr(34) & txtText1(17).Text & Chr(34), vbMaximizedFocus

error_shell:

  If Err.Number <> 0 Then
    MsgBox "Error N�mero: " & Err.Number & Chr(13) & Err.Description, vbCritical
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objMasterInfo2 As New clsCWForm
Dim strKey As String
Dim strSelEstado As String
Dim rstEstado As rdoResultset
   
   Set objWinInfo = New clsCWWin
   
   Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                 Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)
                                
   With objMasterInfo
      Set .objFormContainer = fraFrame1(0)
      Set .objFatherContainer = Nothing
      Set .tabMainTab = tabTab1(0)
      Set .grdGrid = grdDBGrid1(2)
      ' Tabla FR5900 -> Orden Fabricaci�n
      .strTable = "FR5901J"
      .intAllowance = cwAllowReadOnly
      Call .FormAddOrderField("FR59CODORDFABR", cwAscending)

      '.strWhere = " FR73CODPRODUCTO = " & FrmVerPteFab.txtText1(3).Text & _
                  " AND FR66CODPETICION = " & FrmVerPteFab.txtText1(0).Text
      If FrmVerPteFab.auxDBGrid1(1).SelBookmarks.Count > 0 Then
        .strWhere = "FR73CODPRODUCTO=" & FrmVerPteFab.auxDBGrid1(1).Columns("C�d.Prod.").Value & _
                  " AND FR66CODPETICION = " & FrmVerPteFab.auxDBGrid1(1).Columns("C�d.Pet.").Value
      ElseIf FrmVerPteFab.auxDBGrid1(3).SelBookmarks.Count > 0 Then
        .strWhere = "FR73CODPRODUCTO=" & FrmVerPteFab.auxDBGrid1(3).Columns("C�d.Prod.").Value & _
                  " AND FR55CODNECESUNID= " & FrmVerPteFab.auxDBGrid1(3).Columns("C�d.Nec.").Value
      Else
        .strWhere = "-1=0"
      End If
      strKey = .strDataBase & .strTable
      'Call .FormCreateFilterWhere(strKey, "Proceso")
      'Call .FormAddFilterWhere(strKey, "FR69CODPROCFABRIC", "C�digo Proceso Fabricaci�n", cwNumeric)
      'Call .FormAddFilterOrder(strKey, "FR69CODPROCFABRIC", "C�digo Proceso Fabricaci�n")
   End With

   With objMasterInfo2
      Set .objFormContainer = fraFrame1(2)
      Set .objFatherContainer = fraFrame1(0)
      Set .tabMainTab = tabTab1(2)
      Set .grdGrid = grdDBGrid1(0)
      ' Tabla FR2400 -> Detalle Orden fabricaci�n
      .strTable = "FR2401J"
      .intAllowance = cwAllowReadOnly
      Call .FormAddOrderField("FR70CODOPERACION", cwAscending)
      Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
      Call .FormAddRelation("FR59CODORDFABR", txtText1(9))
      strKey = .strDataBase & .strTable
      'Call .FormCreateFilterWhere(strKey, "Operacion")
      'Call .FormAddFilterWhere(strKey, "FR70CODOPERACION", "C�digo Operacion", cwNumeric)
      'Call .FormAddFilterOrder(strKey, "FR70CODOPERACION", "C�digo Operacion")
   End With
  
   With objWinInfo
      Call .FormAddInfo(objMasterInfo, cwFormDetail)
      Call .FormAddInfo(objMasterInfo2, cwFormDetail)
      Call .FormCreateInfo(objMasterInfo)
      
      
      ' Estado Operaci�n
'      Call .CtrlCreateLinked(.CtrlGetInfo(txtEstadoOperacionCod), "FR37CODESTOF", "SELECT * FROM FR3700 WHERE FR37CODESTOF = ?")
'      Call .CtrlAddLinked(.CtrlGetInfo(txtEstadoOperacionCod), txtEstadoOperacion, "FR37DESESTOF")
'      .CtrlGetInfo(txtEstadoOperacionCod).blnForeign = True
      
      ' Farmac�utico
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(13), "SG02NOM")
      .CtrlGetInfo(txtText1(16)).blnForeign = True

'      ' Producto -> Descripci�n
'      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO = ?")
'      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "FR73DESPRODUCTO")
'      .CtrlGetInfo(txtText1(2)).blnForeign = True
'
'      ' Proceso Fabricaci�n -> Descripci�n Proceso
'      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "FR69CODPROCFABRIC", "SELECT FR69DESPROCESO FROM FR6900 WHERE FR69CODPROCFABRIC = ?")
'      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(6), "FR69DESPROCESO")
'      .CtrlGetInfo(txtText1(7)).blnForeign = True
'
'      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "FR73CODPRODUCTO", "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO = ?")
'      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(12), "FR73DESPRODUCTO")
'      .CtrlGetInfo(txtText1(13)).blnForeign = True
'
'      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR69CODPROCFABRIC", "SELECT FR69DESPROCESO FROM FR6900 WHERE FR69CODPROCFABRIC = ?")
'      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR69DESPROCESO")
'      .CtrlGetInfo(txtText1(8)).blnForeign = True
'
'    Call .CtrlCreateLinked(.CtrlGetInfo(txtEstadoOperacionCod), "FR70CODOPERACION", "SELECT FR70DESOPERACION FROM FR7000 WHERE FR70CODOPERACION = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(txtEstadoOperacionCod), txtText1(10), "FR70DESOPERACION")
'    .CtrlGetInfo(txtEstadoOperacionCod).blnForeign = True
'
'    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "FR08CODCENTRCOSTE", "SELECT FR08DESCENTRCOSTE FROM FR0800 WHERE FR08CODCENTRCOSTE = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtEstadoProcesoCod, "FR08DESCENTRCOSTE")
'    .CtrlGetInfo(txtText1(16)).blnForeign = True
      
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(10)), "FR70CODOPERACION", "SELECT * FROM FR7000 WHERE FR70CODOPERACION = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(17), "FR70PATHDESOPE")
      
      .CtrlGetInfo(txtTiempoEjecucion).blnNegotiated = False
      .CtrlGetInfo(txtCodFarmaceutico).blnNegotiated = False
      .CtrlGetInfo(txtEstadoProcesoCod).blnNegotiated = False
      .CtrlGetInfo(txtEstadoProceso).blnNegotiated = False
      .CtrlGetInfo(txtEstadoOperacion).blnNegotiated = False
'      .CtrlGetInfo(txtEstadoOperacionCod).blnNegotiated = False

      Call .WinRegister
      Call .WinStabilize
    
   End With
  
  
  tabTab1(0).TabVisible(1) = False
  tabTab1(2).TabVisible(1) = False
  
   
   ' Estado de la Fabricaci�n
  If FrmVerPteFab.auxDBGrid1(1).SelBookmarks.Count > 0 Then
   strSelEstado = "SELECT FR37CODESTOF FROM FR5900 WHERE " & _
               " FR59CODORDFABR=" & FrmVerPteFab.auxDBGrid1(1).Columns("C�d.O.F.").Value
   Set rstEstado = objApp.rdoConnect.OpenResultset(strSelEstado)
   If rstEstado.EOF Then
    txtEstadoProcesoCod.Text = "1"
   Else
    txtEstadoProcesoCod.Text = rstEstado("FR37CODESTOF").Value
   End If
   rstEstado.Close
   Set rstEstado = Nothing
  ElseIf FrmVerPteFab.auxDBGrid1(3).SelBookmarks.Count > 0 Then
   strSelEstado = "SELECT FR37CODESTOF FROM FR5900 WHERE " & _
               " FR59CODORDFABR=" & FrmVerPteFab.auxDBGrid1(3).Columns("C�d.O.F.").Value
   Set rstEstado = objApp.rdoConnect.OpenResultset(strSelEstado)
   If rstEstado.EOF Then
    txtEstadoProcesoCod.Text = "1"
   Else
    txtEstadoProcesoCod.Text = rstEstado("FR37CODESTOF").Value
   End If
   rstEstado.Close
   Set rstEstado = Nothing
  Else
    txtEstadoProcesoCod.Text = "1"
  End If
   
   tvwItems(0).Visible = True
   Call rellenar_TreeView
   Me.txtTiempoEjecucion.BackColor = &HFFFFFF
   Me.txtCodFarmaceutico.BackColor = &HFFFFFF

   ' Refrescar
   Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)

'   Me.Refresh
'  tabTab1(0).Caption = ""
'  tabTab1(2).Caption = ""

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabGeneral1_Click(PreviousTab As Integer)
   If Not Enarbol Then
      If tabGeneral1.Tab = 0 Then ' Proceso
         tvwItems(0).Nodes("Producto").Selected = True
      Else ' operacion
        If txtText1(8).Text <> "" And txtText1(9).Text <> "" And txtText1(10).Text <> "" Then
         tvwItems(0).Nodes("Operacion" & txtText1(10).Text & "/" & txtText1(8).Text & "/" & "Proceso" & txtText1(9).Text).Selected = True
        End If
      End If
   End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   
   Select Case btnButton.Index
   Case 21 'Primero
      On Error Resume Next
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      On Error GoTo 0
      Exit Sub
   Case 22 'Anterior
      On Error Resume Next
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
         tvwItems(0).SelectedItem.Previous.Selected = True
         Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
      On Error GoTo 0
      Exit Sub
   Case 23 'Siguiente
      On Error Resume Next
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
         tvwItems(0).SelectedItem.Next.Selected = True
         Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
      On Error GoTo 0
      Exit Sub
   Case 24 'Ultimo
      On Error Resume Next
      tvwItems(0).SelectedItem.LastSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      On Error GoTo 0
      Exit Sub
   Case 26 'Refrescar
      ' No hacer nada
   Case Else 'Otro boton
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
   End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
   
   Select Case intIndex
   Case 40 'Primero
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
   Case 50 'Anterior
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
   Case 60 'Siguiente
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
   Case 70 'Ultimo
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
   Case Else
      'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
   End Select


End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)
   Node.EnsureVisible
   Node.Selected = True
   Call tvwItems_NodeClick(0, Node)
End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)

   If EnPostWrite = True Then
      EnPostWrite = False
      Exit Sub
   End If
   
   Node.EnsureVisible
   Node.Selected = True
   Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
   Dim aux As Variant
   Dim aux2 As Variant
   Dim vProceso As Variant
   Dim vOperacion_ProdHijo As Variant
   Dim vOperacion As Variant
   Dim vProdHijo As Variant

   Enarbol = True

   aux2 = Me.MousePointer
   Me.MousePointer = vbHourglass
   Select Case Left(Node.Key, 7)
   Case Left("Producto", 7):
      ' Se ha pinchado en Raiz
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
      Call objWinInfo.DataMoveFirst
      tabGeneral1.Tab = 0
   Case Left("Proceso", 7):
      ' Se ha pinchado en un Proceso
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
      Call objWinInfo.DataRefreshGoFirst
      Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
      vProceso = "Proceso" & objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR59CODORDFABR").Value
      While vProceso <> Node.Key
'         Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
         Call objWinInfo.WinProcess(cwProcessToolBar, 23, 0)
         vProceso = "Proceso" & objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR59CODORDFABR").Value
      Wend
      tabGeneral1.Tab = 0
   Case Left("Operacion", 7):
      ' Se ha pinchado en una Operaci�n
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
      Call objWinInfo.DataRefreshGoFirst
      Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
      vProceso = "Proceso" & objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR59CODORDFABR").Value
      While vProceso <> Node.Parent.Key
         Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
         vProceso = "Proceso" & objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR59CODORDFABR").Value
      Wend
      
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
      Call objWinInfo.DataRefreshGoFirst
      Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
      vOperacion_ProdHijo = "Operacion" & _
         objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR70CODOPERACION").Value & _
         "/" & _
         objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR73CODPRODUCTO").Value & "/" & vProceso
      While vOperacion_ProdHijo <> Node.Key
'         Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
         Call objWinInfo.WinProcess(cwProcessToolBar, 23, 0)
         vOperacion_ProdHijo = "Operacion" & _
            objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR70CODOPERACION").Value & _
            "/" & _
            objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR73CODPRODUCTO").Value & "/" & vProceso
      Wend
      tabGeneral1.Tab = 1
           
   End Select
   Me.MousePointer = aux2

   Enarbol = False
   'Call objWinInfo.DataRefresh

End Sub


Private Sub txtCodFarmaceutico_LostFocus()
   Dim strUpdate As String

   strUpdate = " UPDATE FR2400 SET SG02COD = '" & Me.txtCodFarmaceutico.Text & "' WHERE " & _
               " FR59CODORDFABR=" & txtText1(9).Text & " AND " & _
               " FR70CODOPERACION=" & txtText1(10).Text & _
               " AND FR73CODPRODUCTO=" & txtText1(8).Text
   On Error GoTo Tratar_Error
   objApp.rdoConnect.Execute strUpdate, 64
   objApp.rdoConnect.Execute "Commit", 64
   ' Refrescar
   ' Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)
   Exit Sub
   
Tratar_Error:
   MsgBox "Error al actulizar C�digo del Farmac�utico", vbInformation

End Sub


Private Sub txtEstadoOperacionCod_Change()
   Select Case txtEstadoOperacionCod.Text
   Case 1
      txtEstadoOperacion.Text = "Generada"
   Case 2
      txtEstadoOperacion.Text = "Planificada"
   Case 3
      txtEstadoOperacion.Text = "Iniciada"
   Case 4
      txtEstadoOperacion.Text = "En Curso"
   Case 5
      txtEstadoOperacion.Text = "Terminada"
   Case Else
      txtEstadoOperacion.Text = ""
   End Select
End Sub

Private Sub txtEstadoProcesoCod_Change()
   ' Estado Proceso
   Select Case txtEstadoProcesoCod.Text
   Case 1
      txtEstadoProceso.Text = "Fabricaci�n Generada"
   Case 5
      txtEstadoProceso.Text = "Fabricaci�n Terminada"
   Case 3
      txtEstadoProceso.Text = "Fabricaci�n Iniciada"
   Case Else
      txtEstadoProceso.Text = ""
   End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
   Call objWinInfo.CtrlDataChange
   ' Tiempo Ejecucion
   If intIndex = 15 Then
      txtTiempoEjecucion = txtText1(15)
   End If
   ' Cod Farmaceutico
   If intIndex = 16 Then
      txtCodFarmaceutico = txtText1(16)
   End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

   Dim objField As clsCWFieldSearch
   Dim objSearch As clsCWSearch
  
'   MsgBox " strFormName: " & strFormName
'   If And strCtrl = "txtText1(26)" Then
'   If strCtrl = "txtText1(12)" Then
'      Set objSearch = New clsCWSearch
'      With objSearch
'         .strTable = "FR3700"
'
'         Set objField = .AddField("FR37CODESTOF")
'         objField.strSmallDesc = "C�digo Estado Operaci�n"
'
'         Set objField = .AddField("FR37DESESTOF")
'         objField.strSmallDesc = "Descripci�n Estado Operaci�n"
'
'         If .Search Then
'            Call objWinInfo.CtrlSet(txtEstadoOperacionCod, .cllValues("FR37CODESTOF"))
'         End If
'      End With
'      Set objSearch = Nothing
'   End If
 
   If strCtrl = "txtText1(13)" Then
      Set objSearch = New clsCWSearch
      With objSearch
         .strTable = "SG0200"
         
         Set objField = .AddField("SG02COD")
         objField.strSmallDesc = "C�digo del Farmac�utico"
         
         Set objField = .AddField("SG02NOM")
         objField.strSmallDesc = "Nombre del Farmac�utico"
         
         If .Search Then
            Call objWinInfo.CtrlSet(txtText1(13), .cllValues("SG02COD"))
         End If
      End With
      Set objSearch = Nothing
   End If
 
  
  
  
'  If strCtrl = "txtText1(2)" Or strCtrl = "txtText1(13)" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "FR7300"
'     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
'
'     Set objField = .AddField("FR73CODPRODUCTO")
'     objField.strSmallDesc = "C�digo Producto"
'
'    Set objField = .AddField("FR73DESPRODUCTO")
'     objField.strSmallDesc = "Descripci�n Producto"
'
'     If .Search Then
'        If strCtrl = "txtText1(2)" Then
'            Call objWinInfo.CtrlSet(txtText1(2), .cllValues("FR73CODPRODUCTO"))
'        Else
'            Call objWinInfo.CtrlSet(txtText1(13), .cllValues("FR73CODPRODUCTO"))
'        End If
'     End If
'   End With
'   Set objSearch = Nothing
' End If

'  If strCtrl = "txtText1(7)" Or strCtrl = "txtText1(8)" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "FR6900"
'     .strOrder = "ORDER BY FR69CODPROCFABRIC ASC"
'
'     Set objField = .AddField("FR69CODPROCFABRIC")
'     objField.strSmallDesc = "C�digo Proceso fabricaci�n"
'
'     Set objField = .AddField("FR69DESPROCESO")
'     objField.strSmallDesc = "Descripci�n Proceso"
'
'     If .Search Then
'        If strCtrl = "txtText1(7)" Then
'            Call objWinInfo.CtrlSet(txtText1(7), .cllValues("FR69CODPROCFABRIC"))
'        Else
'            Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR69CODPROCFABRIC"))
'        End If
'     End If
'   End With
'   Set objSearch = Nothing
' End If
'
'  If strCtrl = "txtEstadoOperacionCod" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "FR7000"
'     .strOrder = "ORDER BY FR70CODOPERACION ASC"
'
'     Set objField = .AddField("FR70CODOPERACION")
'     objField.strSmallDesc = "C�digo Operaci�n"
'
'     Set objField = .AddField("FR70DESOPERACION")
'     objField.strSmallDesc = "Descripci�n Operaci�n"
'
'     If .Search Then
'        Call objWinInfo.CtrlSet(txtEstadoOperacionCod, .cllValues("FR70CODOPERACION"))
'     End If
'   End With
'   Set objSearch = Nothing
' End If
'
' If strCtrl = "txtText1(16)" Then
'   Set objSearch = New clsCWSearch
'   With objSearch
'    .strTable = "FR0800"
'    .strOrder = "ORDER BY FR08CODCENTRCOSTE ASC"
'
'    Set objField = .AddField("FR08CODCENTRCOSTE")
'    objField.strSmallDesc = "C�digo Centro Coste"
'
'    Set objField = .AddField("FR08DESCENTRCOSTE")
'    objField.strSmallDesc = "Descripci�n Centro Coste"
'
'    If .Search Then
'       Call objWinInfo.CtrlSet(txtText1(16), .cllValues("FR08CODCENTRCOSTE"))
'    End If
'  End With
'  Set objSearch = Nothing
' End If

End Sub


Private Sub txtTiempoEjecucion_LostFocus()
   Dim strUpdate As String

   If Me.txtTiempoEjecucion.Text = "" Or IsNumeric(Me.txtTiempoEjecucion.Text) Then
      strUpdate = " UPDATE FR2400 SET FR24TIEMPREAL = '" & Me.txtTiempoEjecucion.Text & "' WHERE " & _
                  " FR59CODORDFABR=" & txtText1(9).Text & " AND " & _
                  " FR70CODOPERACION=" & txtText1(10).Text & _
                  " AND FR73CODPRODUCTO=" & txtText1(8).Text
      On Error GoTo Tratar_Error
      objApp.rdoConnect.Execute strUpdate, 64
      On Error GoTo 0
      objApp.rdoConnect.Execute "Commit", 64
      ' Refrescar
      ' Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)
   Else
      MsgBox "El tiempo de ejecuci�n ha de ser num�rico", vbInformation
   End If
   
   Exit Sub
   
Tratar_Error:
   MsgBox "El tiempo de ejecuci�n ha de ser num�rico", vbInformation
End Sub

Private Sub Insertar_FR3500(origen, destino, tipomovi, producto, cantidad, medida)
Dim rst35 As rdoResultset
Dim str35 As String
Dim strinsert As String

str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
Set rst35 = objApp.rdoConnect.OpenResultset(str35)
 
strinsert = "INSERT INTO FR3500(FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
            "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI,FR73CODPRODUCTO," & _
            "FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & _
            rst35.rdoColumns(0).Value & "," & _
            origen & "," & _
            destino & "," & _
            tipomovi & "," & _
            "SYSDATE" & "," & _
            "NULL" & "," & _
            producto & "," & _
            objGen.ReplaceStr(cantidad, ",", ".", 1) & "," & _
            "1" & "," & _
            "'" & medida & "'" & "," & _
            "NULL" & ")"
objApp.rdoConnect.Execute strinsert, 64
objApp.rdoConnect.Execute "Commit", 64
rst35.Close
Set rst35 = Nothing
End Sub

Private Sub Insertar_FR8000(origen, destino, tipomovi, producto, cantidad, medida)
Dim rst80 As rdoResultset
Dim str80 As String
Dim strinsert As String

str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
Set rst80 = objApp.rdoConnect.OpenResultset(str80)

strinsert = "INSERT INTO FR8000(FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
            "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
            "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES (" & _
            rst80.rdoColumns(0).Value & "," & _
            origen & "," & _
            destino & "," & _
            tipomovi & "," & _
            "SYSDATE" & "," & _
            "NULL" & "," & _
            producto & "," & _
            objGen.ReplaceStr(cantidad, ",", ".", 1) & "," & _
            "1" & "," & _
            "'" & medida & "'" & ")"
objApp.rdoConnect.Execute strinsert, 64
objApp.rdoConnect.Execute "Commit", 64
rst80.Close
Set rst80 = Nothing
End Sub


