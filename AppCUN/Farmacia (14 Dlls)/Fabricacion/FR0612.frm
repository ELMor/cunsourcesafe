VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form FrmFabKit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FABRICACION FARMACIA. Fabricar Kit"
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Kits"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5655
      Index           =   4
      Left            =   120
      TabIndex        =   15
      Top             =   2280
      Width           =   4605
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   0
         Left            =   1200
         TabIndex        =   19
         Tag             =   "Texto a buscar"
         Top             =   5040
         Width           =   1170
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Ver Kit"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   18
         Top             =   5040
         Width           =   855
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Fabricar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   17
         Top             =   5040
         Width           =   855
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4440
         Index           =   3
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   360
         Width           =   4335
         ScrollBars      =   3
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   7646
         _ExtentY        =   7832
         _StockProps     =   79
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cantidad:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   26
         Left            =   360
         TabIndex        =   20
         Top             =   5160
         Width           =   825
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1695
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   11655
      Begin Crystal.CrystalReport CrystalReport1 
         Left            =   10800
         Top             =   1080
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   120
         TabIndex        =   12
         Tag             =   "Texto a buscar"
         Top             =   480
         Width           =   5130
      End
      Begin VB.Frame Frame2 
         Caption         =   "Buscarlo en"
         ForeColor       =   &H00FF0000&
         Height          =   615
         Left            =   5760
         TabIndex        =   8
         Top             =   240
         Width           =   5535
         Begin VB.CheckBox chkBusq1 
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Value           =   1  'Checked
            Width           =   1065
         End
         Begin VB.CheckBox chkBusq1 
            Caption         =   "Cod.Int.Componentes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   2
            Left            =   1200
            TabIndex        =   10
            Top             =   240
            Width           =   2145
         End
         Begin VB.CheckBox chkBusq1 
            Caption         =   "Grupo Terap�utico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   3
            Left            =   3480
            TabIndex        =   9
            Top             =   240
            Width           =   2025
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9480
         TabIndex        =   7
         Top             =   1080
         Width           =   855
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   120
         TabIndex        =   6
         Tag             =   "C�d. Grp. Terap�utico"
         Top             =   1200
         Width           =   1905
      End
      Begin VB.CheckBox chkBusq1 
         Caption         =   "Productos Bajo Stock"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   5880
         TabIndex        =   5
         Top             =   1200
         Width           =   2265
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Texto a buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   55
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   1290
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Grp. Terap�utico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   120
         TabIndex        =   13
         Top             =   960
         Width           =   1905
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Fabricaci�n Kit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5640
      Index           =   1
      Left            =   4800
      TabIndex        =   2
      Top             =   2280
      Width           =   6975
      Begin ComctlLib.TreeView tvwItems 
         Height          =   4995
         Index           =   0
         Left            =   360
         TabIndex        =   3
         Top             =   480
         Width           =   6480
         _ExtentX        =   11430
         _ExtentY        =   8811
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.ImageList imlImagenes 
      Left            =   0
      Top             =   7440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":005E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":00BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":011A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":0178
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":01D6
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":0234
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":0292
            Key             =   "function"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":02F0
            Key             =   "window"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":034E
            Key             =   "report"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":03AC
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":040A
            Key             =   "process"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":0468
            Key             =   "application"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":04C6
            Key             =   "big"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":0524
            Key             =   "small"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":0582
            Key             =   "list"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":05E0
            Key             =   "details"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":063E
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0612.frx":069C
            Key             =   "refresh"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmFabKit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmFabKit (FR0612.FRM)                                   *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: Definir Proceso de Fabricaci�n de un producto           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
' Declara variables globales.
Dim Enarbol As Boolean
Dim Enabrir As Boolean
Dim EnPostWrite As Boolean

Private Sub rellenar_TreeView()
  Dim strRelproc As String
  Dim rstRelproc As rdoResultset
  Dim strRelOper As String
  Dim rstRelOper As rdoResultset
  Dim strRelProd As String
  Dim rstRelProd As rdoResultset
  Dim strReldesprod As String
  Dim rstReldesprod As rdoResultset
  Dim producto, DesProducto As Variant
  Dim Proceso, DesProceso As Variant
  Dim Operacion, DesOperacion As Variant
  Dim ProdHijo, DesProdHijo As Variant
  Dim rdoQ As rdoQuery
  
    producto = grdDBGrid1(3).Columns(3).Value
    DesProducto = grdDBGrid1(3).Columns(4).Value
    
    tvwItems(0).Nodes.Clear
    
    'bind variables
    'strRelproc = "SELECT * FROM FR6900 WHERE FR73CODPRODUCTO=" & producto & " ORDER BY FR69CODPROCFABRIC"
    strRelproc = "SELECT * FROM FR6900 WHERE FR73CODPRODUCTO=? ORDER BY FR69CODPROCFABRIC"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", strRelproc)
    rdoQ(0) = producto
    Set rstRelproc = rdoQ.OpenResultset(strRelproc)
  
    Call tvwItems(0).Nodes.Add(, , "product" & producto, producto & ".-" & DesProducto)
  
    While rstRelproc.EOF = False
      Proceso = rstRelproc("FR69CODPROCFABRIC").Value
      DesProceso = rstRelproc("FR69DESPROCESO").Value
      Call tvwItems(0).Nodes.Add("product" & producto, tvwChild, "proceso" & producto & "/" & Proceso, Proceso & ".-" & DesProceso)
      'bind variables
      'strReloper = "SELECT * FROM FR7000 WHERE FR69CODPROCFABRIC=" & proceso & " ORDER BY FR70CODOPERACION"
      strRelOper = "SELECT * FROM FR7000 WHERE FR69CODPROCFABRIC=? ORDER BY FR70CODOPERACION"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strRelOper)
      rdoQ(0) = Proceso
      Set rstRelOper = rdoQ.OpenResultset(strRelOper)
      While rstRelOper.EOF = False
        Operacion = rstRelOper("FR70CODOPERACION").Value
        DesOperacion = rstRelOper("FR70DESOPERACION").Value
        Call tvwItems(0).Nodes.Add("proceso" & producto & "/" & Proceso, tvwChild, "operaci" & producto & "/" & Proceso & "/" & Operacion, Operacion & ".-" & DesOperacion)
        'bind variables
        'strRelprod = "SELECT * FROM FR7100 WHERE FR69CODPROCFABRIC=" & proceso & _
                     " AND FR70CODOPERACION=" & operacion & " ORDER BY FR73CODPRODUCTO"
        strRelProd = "SELECT * FROM FR7100 WHERE FR69CODPROCFABRIC=? AND " & _
                     "FR70CODOPERACION=? ORDER BY FR73CODPRODUCTO"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", strRelProd)
        rdoQ(0) = Proceso
        rdoQ(1) = Operacion
        Set rstRelProd = rdoQ.OpenResultset(strRelProd)
        While rstRelProd.EOF = False
            ProdHijo = rstRelProd("FR73CODPRODUCTO").Value
            'bind variables
            'strReldesprod = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=" & prodhijo
            strReldesprod = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
            Set rdoQ = objApp.rdoConnect.CreateQuery("", strReldesprod)
            rdoQ(0) = ProdHijo
            Set rstReldesprod = rdoQ.OpenResultset(strReldesprod)
            If Not IsNull(rstRelProd("FR93CODUNIMEDIDA").Value) Then
              DesProdHijo = rstReldesprod("FR73CODINTFAR").Value & ".-" & rstReldesprod("FR73DESPRODUCTO").Value & " Cant.:" & rstRelProd("FR71CANTNECESARIA").Value & " UM.:" & rstRelProd("FR93CODUNIMEDIDA").Value
            Else
              DesProdHijo = rstReldesprod("FR73CODINTFAR").Value & ".-" & rstReldesprod("FR73DESPRODUCTO").Value & " Cant.:" & rstRelProd("FR71CANTNECESARIA").Value & " UM.:" & rstReldesprod("FR93CODUNIMEDIDA").Value
            End If
            'Call tvwItems(0).Nodes.Add("operaci" & producto & "/" & proceso & "/" & operacion, tvwChild, "prodhij" & producto & "/" & proceso & "/" & operacion & "/" & prodhijo, prodhijo & ".-" & desprodhijo)
            Call tvwItems(0).Nodes.Add("operaci" & producto & "/" & Proceso & "/" & Operacion, tvwChild, "prodhij" & producto & "/" & Proceso & "/" & Operacion & "/" & ProdHijo, DesProdHijo)
            rstReldesprod.Close
            Set rstReldesprod = Nothing
            rstRelProd.MoveNext
        Wend
        rstRelProd.Close
        Set rstRelProd = Nothing
        rstRelOper.MoveNext
      Wend
      rstRelOper.Close
      Set rstRelOper = Nothing
      rstRelproc.MoveNext
    Wend
    rstRelproc.Close
    Set rstRelproc = Nothing

    tvwItems(0).Nodes("product" & producto).Selected = True
    tvwItems(0).Nodes("product" & producto).Expanded = True

End Sub



Private Sub Command1_Click()
Dim strTextoABuscar As String
Dim strCodGrpTerap As String
Dim strProdBajoStock As String
Dim strclausulawhere As String
  
  strTextoABuscar = txtBusq1(8).Text
  strCodGrpTerap = txtBusq1(9).Text
  strProdBajoStock = chkBusq1(4).Value
  
  If strTextoABuscar <> "" Then
    If chkBusq1(1).Value = 1 Then
      strclausulawhere = " FR73DESPRODUCTO LIKE UPPER('" & strTextoABuscar & "%') "
    End If
    
    If chkBusq1(2).Value = 1 Then
      If strclausulawhere <> "" Then
        strclausulawhere = strclausulawhere & " AND (FR73CODPRODUCTO IN (SELECT KIT FROM fr6901j WHERE FR73CODINTFAR='" & strTextoABuscar & "') )"
      Else
        strclausulawhere = " (FR73CODPRODUCTO IN (SELECT KIT FROM fr6901j WHERE FR73CODINTFAR='" & strTextoABuscar & "') )"
      End If
    End If
    
    If chkBusq1(3).Value = 1 Then
      If strclausulawhere <> "" Then
        strclausulawhere = strclausulawhere & " AND FR00CODGRPTERAP IN (SELECT FR00CODGRPTERAP FROM FR0000 WHERE FR00DESGRPTERAP LIKE UPPER('" & strTextoABuscar & "%')) "
      Else
        strclausulawhere = " FR00CODGRPTERAP IN (SELECT FR00CODGRPTERAP FROM FR0000 WHERE FR00DESGRPTERAP LIKE UPPER('" & strTextoABuscar & "%')) "
      End If
    End If
  End If
  
  If strCodGrpTerap <> "" Then
    If strclausulawhere <> "" Then
      strclausulawhere = strclausulawhere & " AND FR00CODGRPTERAP LIKE UPPER('" & strCodGrpTerap & "%') "
    Else
      strclausulawhere = " FR00CODGRPTERAP LIKE UPPER('" & strCodGrpTerap & "%') "
    End If
  End If
  
  If strProdBajoStock = 1 Then
    If strclausulawhere <> "" Then
      strclausulawhere = strclausulawhere & " AND FR73INDCTRLLOTE=-1 "
    Else
      strclausulawhere = " FR73INDCTRLLOTE=-1 "
    End If
  End If
  
  If strclausulawhere <> "" Then
    strclausulawhere = strclausulawhere & " AND FR73INDKIT=-1 "
  Else
    strclausulawhere = " FR73INDKIT=-1 "
  End If
  
  Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
  objWinInfo.objWinActiveForm.strWhere = strclausulawhere
  Call objWinInfo.DataRefresh

End Sub

Private Sub Command2_Click()
Dim strProdsKit As String
Dim rdoQ As rdoQuery
Dim rstProdsKit As rdoResultset
Dim strAlmFar As String
Dim rstAlmFar As rdoResultset
Dim strAlmFab As String
Dim rstAlmFab As rdoResultset
Dim strUMKit As String
Dim qryUMKit As rdoQuery
Dim rstUMKit As rdoResultset
Dim strUM As String
Dim auxCantidad As Currency

  If IsNumeric(txtBusq1(0).Text) Then
    If grdDBGrid1(3).Columns(3).Value <> "" Then
      Call objWinInfo_cwPrint("Kits")
    
      'se obtiene el almac�n de Farmacia
      'strAlmFar = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
              "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
              "AND FRH2CODPARAMGEN=3 " & _
              "AND FR04INDPRINCIPAL=-1"
      'Set rstAlmFar = objApp.rdoConnect.OpenResultset(strAlmFar)
      'If rstAlmFar.EOF Then
      '  strAlmFar = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
                "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
                "AND FRH2CODPARAMGEN=3 "
      '  Set rstAlmFar = objApp.rdoConnect.OpenResultset(strAlmFar)
      'End If
      strAlmFar = "SELECT FRH2PARAMGEN FROM FRH200 " & _
              "WHERE FRH2CODPARAMGEN=28 "
      Set rstAlmFar = objApp.rdoConnect.OpenResultset(strAlmFar)
      
      'se obtiene el almac�n de Fabricaci�n
      strAlmFab = "SELECT FRH2PARAMGEN FROM FRH200 " & _
              "WHERE FRH2CODPARAMGEN=18"
      Set rstAlmFab = objApp.rdoConnect.OpenResultset(strAlmFab)
      If rstAlmFar.EOF Or rstAlmFab.EOF Then
        MsgBox "No hay definido almac�n de farmacia o de Fabricaci�n en Par�metros generales.", vbCritical, "Aviso"
        Exit Sub
      End If
    
      strProdsKit = "SELECT * FROM FR6901J WHERE KIT=?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strProdsKit)
      rdoQ(0) = grdDBGrid1(3).Columns(3).Value
      Set rstProdsKit = rdoQ.OpenResultset(strProdsKit)
      While Not rstProdsKit.EOF
        auxCantidad = rstProdsKit.rdoColumns("FR71CANTNECESARIA").Value * CCur(txtBusq1(0).Text)
        Call Insertar_FR8000(rstAlmFar.rdoColumns(0).Value, _
                             rstAlmFab.rdoColumns(0).Value, _
                             rstProdsKit.rdoColumns("FR73CODPRODUCTO").Value, _
                             auxCantidad, _
                             rstProdsKit.rdoColumns("UNIMEDIDA").Value)
        'entrada del almac�n de Fabricaci�n a Farmacia del producto
        Call Insertar_FR3500(rstAlmFar.rdoColumns(0).Value, _
                             rstAlmFab.rdoColumns(0).Value, _
                             rstProdsKit.rdoColumns("FR73CODPRODUCTO").Value, _
                             auxCantidad, _
                             rstProdsKit.rdoColumns("UNIMEDIDA").Value)
        rstProdsKit.MoveNext
      Wend
      
      strUMKit = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
      Set qryUMKit = objApp.rdoConnect.CreateQuery("", strUMKit)
      qryUMKit(0) = grdDBGrid1(3).Columns(3).Value
      Set rstUMKit = qryUMKit.OpenResultset(strUMKit)
      If IsNull(rstUMKit.rdoColumns("FR93CODUNIMEDIDA")) Then
        strUM = rstUMKit.rdoColumns("FR93CODUNIMEDIDA")
      Else
        strUM = "NE"
      End If
      rstUMKit.Close
      Set rstUMKit = Nothing
      qryUMKit.Close
      Set qryUMKit = Nothing
      
      Call Insertar_FR8000(rstAlmFab.rdoColumns(0).Value, _
                           rstAlmFar.rdoColumns(0).Value, _
                           grdDBGrid1(3).Columns(3).Value, _
                           txtBusq1(0).Text, _
                           strUM)
      Call Insertar_FR3500(rstAlmFab.rdoColumns(0).Value, _
                           rstAlmFar.rdoColumns(0).Value, _
                           grdDBGrid1(3).Columns(3).Value, _
                           txtBusq1(0).Text, _
                           strUM)
      rstProdsKit.Close
      Set rstProdsKit = Nothing
      rdoQ.Close
      Set rdoQ = Nothing
    
    Else
      MsgBox "Debe seleccionar un Kit", vbCritical, "Aviso"
    End If
  Else
    MsgBox "El campo cantidad es incorrecto", vbCritical, "Aviso"
  End If

End Sub

Private Sub Command3_Click()
If grdDBGrid1(3).Columns(3).Value <> "" Then
  Call rellenar_TreeView
End If
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(4)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Kits"
    .intAllowance = cwAllowReadOnly
    
    .strTable = "FR7300"
    
    .strWhere = " FR73INDKIT=-1 "
    
    .intCursorSize = 0
    
    Call .objPrinter.Add("FR6121", "Composici�n Kit")
    
    Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones del Carro")
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Kit", cwString)
    
  End With
  
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "Cod.Kit", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Kit", "FR73DESPRODUCTO", cwString, 50)
  
    Call .FormCreateInfo(objMultiInfo)
    Call .FormChangeColor(objMultiInfo)
    
    
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
  
  'tvwItems(0).ImageList = imlImagenes
  tvwItems(0).Visible = True
'  Call rellenar_TreeView
  
  grdDBGrid1(3).Columns(3).Visible = False
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_SelChange(Index As Integer, ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)

If Index = 3 Then
  If grdDBGrid1(3).Columns(3).Value <> "" Then
    Call rellenar_TreeView
  End If
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
      
  If btnButton.Index = 6 Then
    If IsNumeric(txtBusq1(0).Text) Then
    Else
      MsgBox "El campo cantidad es incorrecto", vbCritical, "Aviso"
      Exit Sub
    End If
  End If
      
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 80 'Imprimir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)

  If EnPostWrite = True Then
    EnPostWrite = False
    Exit Sub
  End If

  Node.EnsureVisible
  Node.Selected = True
  Call tvwItems_NodeClick(0, Node)

End Sub


Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
'
'Dim aux As Variant
'Dim aux2 As Variant
'
'Enarbol = True
'
'aux2 = Me.MousePointer
'Me.MousePointer = vbHourglass
'    Select Case Left(Node.Key, 7)
'        Case "product":
'          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'          Call objWinInfo.DataMoveFirst
'          tabTab1(0).Enabled = True
'          tabTab1(2).Enabled = False
'          tabTab1(1).Enabled = False
'          tabGeneral1(1).Tab = 0
'        Case "proceso":
'          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'          aux = CInt(Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/")))
'          Call objWinInfo.DataRefreshGoFirst
'          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
'          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR69CODPROCFABRIC").Value <> aux
'            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
'          Wend
'          tabTab1(0).Enabled = True
'          tabTab1(2).Enabled = False
'          tabTab1(1).Enabled = False
'          tabGeneral1(1).Tab = 0
'        Case "operaci":
'          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
'          aux = CInt(Left(aux, InStr(aux, "/") - 1))
'          Call objWinInfo.DataRefreshGoFirst
'          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
'          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR69CODPROCFABRIC").Value <> aux
'            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
'          Wend
'          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
'          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
'          aux = CInt(Right(aux, Len(aux) - InStr(aux, "/")))
'          Call objWinInfo.DataRefreshGoFirst
'          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
'          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR70CODOPERACION").Value <> aux
'            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
'          Wend
'          tabTab1(0).Enabled = False
'          tabTab1(2).Enabled = True
'          tabTab1(1).Enabled = False
'          tabGeneral1(1).Tab = 1
'        Case "prodhij":
'          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
'          aux = CInt(Left(aux, InStr(aux, "/") - 1))
'          Call objWinInfo.DataRefreshGoFirst
'          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
'          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR69CODPROCFABRIC").Value <> aux
'            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
'          Wend
'
'          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
'          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
'          aux = Right(aux, Len(aux) - InStr(aux, "/"))
'          aux = CInt(Left(aux, InStr(aux, "/") - 1))
'          Call objWinInfo.DataRefreshGoFirst
'          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
'          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR70CODOPERACION").Value <> aux
'            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
'          Wend
'
'          Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
'          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
'          aux = Right(aux, Len(aux) - InStr(aux, "/"))
'          aux = CInt(Right(aux, Len(aux) - InStr(aux, "/")))
'          Call objWinInfo.DataRefreshGoFirst
'          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
'          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR73CODPRODUCTO").Value <> aux
'            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
'          Wend
'
'          tabTab1(0).Enabled = False
'          tabTab1(2).Enabled = False
'          tabTab1(1).Enabled = True
'          tabGeneral1(1).Tab = 2
'    End Select
'Me.MousePointer = aux2
'
'Enarbol = False
'
'Call objWinInfo.DataRefresh
'
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)

  Select Case Index
  Case 14, 15
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case Else
      KeyAscii = 0
    End Select
  Case Else
  End Select

End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strFilter As String

  If strFormName = "Kits" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      If intReport = 1 Then
        Call Imprimir("FR6121.RPT", 0)
      Else
        Call Imprimir("FR6121.RPT", 1)
      End If
    End If
    Set objPrinter = Nothing
  End If

End Sub

Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
Dim strCarros As String
Dim nTotal As Long
Dim nTotalSelRows As Integer
Dim i As Integer
Dim bkmrk As Variant
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.getdatabasepasswordold
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado

  strWhere = "{FR6901J.KIT}=" & grdDBGrid1(3).Columns(3).Value
  CrystalReport1.SelectionFormula = strWhere
  CrystalReport1.Formulas(0) = "NUMKITS=" & txtBusq1(0)
  
  'On Error GoTo Err_imp3
  CrystalReport1.Action = 1
Err_imp3:

End Sub

Private Sub Insertar_FR3500(origen, destino, producto, cantidad, medida)
Dim rst35 As rdoResultset
Dim str35 As String
Dim strinsert As String

str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
Set rst35 = objApp.rdoConnect.OpenResultset(str35)
 
strinsert = "INSERT INTO FR3500(FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
            "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI,FR73CODPRODUCTO," & _
            "FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & _
            rst35.rdoColumns(0).Value & "," & _
            origen & "," & _
            destino & "," & _
            "1" & "," & _
            "SYSDATE" & "," & _
            "NULL" & "," & _
            producto & "," & _
            objGen.ReplaceStr(cantidad, ",", ".", 1) & "," & _
            "1" & "," & _
            "'" & medida & "'" & "," & _
            "NULL" & ")"
objApp.rdoConnect.Execute strinsert, 64
objApp.rdoConnect.Execute "Commit", 64
rst35.Close
Set rst35 = Nothing
End Sub

Private Sub Insertar_FR8000(origen, destino, producto, cantidad, medida)
Dim rst80 As rdoResultset
Dim str80 As String
Dim strinsert As String

str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
Set rst80 = objApp.rdoConnect.OpenResultset(str80)

strinsert = "INSERT INTO FR8000(FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
            "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
            "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES (" & _
            rst80.rdoColumns(0).Value & "," & _
            origen & "," & _
            destino & "," & _
            "1" & "," & _
            "SYSDATE" & "," & _
            "NULL" & "," & _
            producto & "," & _
            objGen.ReplaceStr(cantidad, ",", ".", 1) & "," & _
            "1" & "," & _
            "'" & medida & "'" & ")"
objApp.rdoConnect.Execute strinsert, 64
objApp.rdoConnect.Execute "Commit", 64
rst80.Close
Set rst80 = Nothing
End Sub


Private Sub txtBusq1_KeyPress(Index As Integer, KeyAscii As Integer)

  Select Case Index
  Case 0
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
    Case Else
      KeyAscii = 0
    End Select
  Case Else
  End Select

End Sub
