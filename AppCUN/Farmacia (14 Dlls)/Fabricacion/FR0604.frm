VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmBusMed 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Buscar Productos"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11685
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1695
      Left            =   0
      TabIndex        =   50
      Top             =   600
      Width           =   11655
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   17
         Left            =   4200
         TabIndex        =   59
         Tag             =   "Desviaci�n Mayor que"
         Top             =   1200
         Width           =   1905
      End
      Begin VB.CheckBox chkBusq1 
         Caption         =   "Productos Bajo Stock"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   6480
         TabIndex        =   58
         Top             =   1200
         Width           =   2265
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   1920
         TabIndex        =   57
         Tag             =   "C�d. Grp. Terap�utico"
         Top             =   1200
         Width           =   1905
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9480
         TabIndex        =   56
         Top             =   1080
         Width           =   855
      End
      Begin VB.Frame Frame2 
         Caption         =   "Buscarlo en"
         ForeColor       =   &H00FF0000&
         Height          =   615
         Left            =   5760
         TabIndex        =   52
         Top             =   240
         Width           =   5295
         Begin VB.CheckBox chkBusq1 
            Caption         =   "Grupo Terap�utico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   3
            Left            =   3240
            TabIndex        =   55
            Top             =   240
            Width           =   2025
         End
         Begin VB.CheckBox chkBusq1 
            Caption         =   "Principio Activo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   2
            Left            =   1440
            TabIndex        =   54
            Top             =   240
            Width           =   1665
         End
         Begin VB.CheckBox chkBusq1 
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   120
            TabIndex        =   53
            Top             =   240
            Width           =   1185
         End
      End
      Begin VB.TextBox txtBusq1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   120
         TabIndex        =   51
         Tag             =   "Texto a buscar"
         Top             =   480
         Width           =   5130
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0604.frx":0000
         Height          =   330
         Index           =   1
         Left            =   120
         TabIndex        =   60
         Tag             =   "ABC(C)"
         Top             =   1200
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboBusq1 
         Bindings        =   "FR0604.frx":0012
         Height          =   330
         Index           =   2
         Left            =   960
         TabIndex        =   61
         Tag             =   "ABC(CE)"
         Top             =   1200
         Width           =   690
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   4
         Row(1)          =   "A"
         Row(2)          =   "B"
         Row(3)          =   "C"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1217
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desviaci�n Mayor que"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   4200
         TabIndex        =   66
         Top             =   960
         Width           =   1905
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Grp. Terap�utico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   1920
         TabIndex        =   65
         Top             =   960
         Width           =   1905
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(CE)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   960
         TabIndex        =   64
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "ABC(C)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   63
         Top             =   960
         Width           =   615
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Texto a buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   55
         Left            =   120
         TabIndex        =   62
         Top             =   240
         Width           =   1290
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5295
      Index           =   0
      Left            =   0
      TabIndex        =   2
      Top             =   2520
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   4740
         Index           =   0
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   8361
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0604.frx":0024
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(12)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(11)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(28)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(27)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(26)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(25)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(24)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(23)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(22)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(21)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(20)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(18)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(17)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(16)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(15)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(14)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(0)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(2)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblLabel1(4)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(8)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(10)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(6)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(11)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(0)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(27)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(26)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(25)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(24)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(23)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(22)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(21)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(20)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(19)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(18)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtText1(16)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(5)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(14)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(13)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(12)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "txtText1(7)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "chkCheck1(0)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "txtText1(4)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "Command2"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).ControlCount=   45
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0604.frx":0040
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.CommandButton Command2 
            Caption         =   "Traer Mat. Sanitario"
            Height          =   375
            Left            =   8160
            TabIndex        =   27
            Top             =   2160
            Width           =   1935
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CANTPEND"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   120
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "Cantidad pendiente de recibir"
            Top             =   960
            Width           =   1650
         End
         Begin VB.CheckBox chkCheck1 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Consumo Regular"
            DataField       =   "FR73INDCONSREGULAR"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   7440
            TabIndex        =   25
            TabStop         =   0   'False
            Tag             =   "Consumo Regular"
            Top             =   1440
            Width           =   1905
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PTEBONIF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   1800
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "Pendiente Bonificar"
            Top             =   960
            Width           =   1650
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESCUENTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   5040
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Descuento"
            Top             =   2160
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSDIARIO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   7440
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Consumo Diario"
            Top             =   960
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSMES"
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   8520
            TabIndex        =   21
            TabStop         =   0   'False
            Tag             =   "Consumo mensual"
            Top             =   960
            Width           =   1170
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   20
            Tag             =   "Medicamentos"
            Top             =   360
            Width           =   7380
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CONSANUAL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   16
            Left            =   9720
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Consumo Anual"
            Top             =   960
            Width           =   1170
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73STOCKMIN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   18
            Left            =   120
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Stock m�nimo"
            Top             =   1560
            Width           =   1905
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73STOCKMAX"
            Height          =   330
            HelpContextID   =   30104
            Index           =   19
            Left            =   2160
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Stock m�ximo"
            Top             =   1560
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73EXISTENCIAS"
            Height          =   330
            HelpContextID   =   30104
            Index           =   20
            Left            =   4080
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Existencias"
            Top             =   1560
            Width           =   2010
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73RAPPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   6000
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Rappel"
            Top             =   2160
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DIASSEG"
            Height          =   330
            HelpContextID   =   30104
            Index           =   22
            Left            =   3600
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "D�as de Seguridad"
            Top             =   960
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73TAMPEDI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   5040
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Tama�o del pedido en d�as"
            Top             =   960
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PREULTENT"
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   120
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Precio de la �ltima entrada"
            Top             =   2160
            Width           =   2010
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73PRECOFR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   25
            Left            =   2160
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Precio Oferta de Compra"
            Top             =   2160
            Width           =   2010
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73BONIFICACION"
            Height          =   330
            HelpContextID   =   30104
            Index           =   26
            Left            =   6960
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Tanto por ciento de Bonificaci�n"
            Top             =   2160
            Width           =   930
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   27
            Left            =   4200
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Moneda"
            Top             =   2160
            Width           =   570
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   0
            Left            =   9840
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   1560
            Visible         =   0   'False
            Width           =   570
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR93CODUNIMEDIDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8760
            Locked          =   -1  'True
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "U.M. Unidad de Medida"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH7CODFORMFAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   6
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "F.F. Forma Farmac�utica"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DOSIS"
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   8160
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73VOLUMEN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   8
            Left            =   9360
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Volumen mL"
            Top             =   360
            Width           =   1380
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4545
            Index           =   0
            Left            =   -74880
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   90
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   8017
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Mon."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   4200
            TabIndex        =   49
            Top             =   1920
            Width           =   435
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Ultima Entrada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   48
            Top             =   1920
            Width           =   1860
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Medicamentos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   47
            Top             =   120
            Width           =   1230
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pendiente Entregar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   120
            TabIndex        =   46
            Top             =   720
            Width           =   1650
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pendiente Bonificar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   1800
            TabIndex        =   45
            Top             =   720
            Width           =   1680
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Consumo D."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   7440
            TabIndex        =   44
            Top             =   720
            Width           =   1035
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Consumo M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   8520
            TabIndex        =   43
            Top             =   720
            Width           =   1050
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Consumo A."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   9720
            TabIndex        =   42
            Top             =   720
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Stock M�nimo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   120
            TabIndex        =   41
            Top             =   1320
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Stock M�ximo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   2160
            TabIndex        =   40
            Top             =   1320
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Existencias"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   4080
            TabIndex        =   39
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Desc."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   5040
            TabIndex        =   38
            Top             =   1920
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dias Seguridad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   3600
            TabIndex        =   37
            Top             =   720
            Width           =   1305
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tama�o Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   25
            Left            =   5040
            TabIndex        =   36
            Top             =   720
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Rappel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   26
            Left            =   6000
            TabIndex        =   35
            Top             =   1920
            Width           =   810
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   27
            Left            =   2160
            TabIndex        =   34
            Top             =   1920
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "% Bonif."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   6960
            TabIndex        =   33
            Top             =   1920
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   7560
            TabIndex        =   32
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   8160
            TabIndex        =   31
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   8760
            TabIndex        =   30
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   9360
            TabIndex        =   29
            Top             =   120
            Width           =   1155
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   0
      Top             =   7935
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmBusMed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA FABRICACI�N                                       *
'* NOMBRE: FrmBusMed (FR0604.FRM)                                       *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA:                                                               *
'* DESCRIPCION: Buscar Medicamentos                                     *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*                                                                      *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gintIndice As Integer


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Command1_Click()
Dim strTextoABuscar As String
Dim strCodGrpTerap As String
Dim strDesvMayQue As String
Dim strProdBajoStock As String
Dim strABC_C As String
Dim strABC_CE As String
Dim strclausulawhere As String
  
  strTextoABuscar = txtBusq1(8).Text
  strCodGrpTerap = txtBusq1(9).Text
  strDesvMayQue = txtBusq1(17).Text
  strProdBajoStock = chkBusq1(4).Value
  strABC_C = cboBusq1(1).Value
  strABC_CE = cboBusq1(2).Value
  
  If strTextoABuscar <> "" Then
    If chkBusq1(1).Value = 1 Then
      strclausulawhere = " FR73DESPRODUCTO LIKE '" & strTextoABuscar & "%' "
    End If
    
    If chkBusq1(2).Value = 1 Then
      If strclausulawhere <> "" Then
        strclausulawhere = strclausulawhere & " AND (FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400,FR6800 WHERE FR6400.FR68CODPRINCACTIV=FR6800.FR68CODPRINCACTIV AND FR68DESPRINCACTIV LIKE '" & strTextoABuscar & "%' )" & _
                    " OR " & _
                    "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400 WHERE FR68CODPRINCACTIV IN (SELECT FR68CODPRINCACTIV FROM FRH400 WHERE FRH4DESSINONIMO LIKE '" & strTextoABuscar & "%'))" & _
                    ") "
      Else
        strclausulawhere = " (FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400,FR6800 WHERE FR6400.FR68CODPRINCACTIV=FR6800.FR68CODPRINCACTIV AND FR68DESPRINCACTIV LIKE '" & strTextoABuscar & "%' )" & _
                    " OR " & _
                    "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR6400 WHERE FR68CODPRINCACTIV IN (SELECT FR68CODPRINCACTIV FROM FRH400 WHERE FRH4DESSINONIMO LIKE '" & strTextoABuscar & "%'))" & _
                    ") "
      End If
    End If
    
    If chkBusq1(3).Value = 1 Then
      If strclausulawhere <> "" Then
        strclausulawhere = strclausulawhere & " AND FR00CODGRPTERAP IN (SELECT FR00CODGRPTERAP FROM FR0000 WHERE FR00DESGRPTERAP LIKE '" & strTextoABuscar & "%') "
      Else
        strclausulawhere = " FR00CODGRPTERAP IN (SELECT FR00CODGRPTERAP FROM FR0000 WHERE FR00DESGRPTERAP LIKE '" & strTextoABuscar & "%') "
      End If
    End If
  End If
  
  If strCodGrpTerap <> "" Then
    If Left(strCodGrpTerap, 1) = "Q" Then
      MsgBox "El Grupo Terape�tico no ha de comenzar por Q (Medicamentos)", vbInformation
      Exit Sub
    End If
    If strclausulawhere <> "" Then
      strclausulawhere = strclausulawhere & " AND FR00CODGRPTERAP LIKE '" & strCodGrpTerap & "%' "
    Else
      strclausulawhere = " FR00CODGRPTERAP LIKE '" & strCodGrpTerap & "%' "
    End If
  Else
    If strclausulawhere <> "" Then
      strclausulawhere = strclausulawhere & " AND (FR00CODGRPTERAP NOT LIKE 'Q%' OR FR00CODGRPTERAP IS NULL) "
    Else
      strclausulawhere = " (FR00CODGRPTERAP NOT LIKE 'Q%' OR FR00CODGRPTERAP IS NULL) "
    End If
  End If
  
  If strProdBajoStock = 1 Then
    If strclausulawhere <> "" Then
      strclausulawhere = strclausulawhere & " AND FR73INDCTRLLOTE=-1 "
    Else
      strclausulawhere = " FR73INDCTRLLOTE=-1 "
    End If
  End If
  
  If strABC_C <> "" Then
    If strclausulawhere <> "" Then
      strclausulawhere = strclausulawhere & " AND FR73ABCCONS='" & strABC_C & "' "
    Else
      strclausulawhere = " FR73ABCCONS='" & strABC_C & "' "
    End If
  End If

  If strABC_CE <> "" Then
    If strclausulawhere <> "" Then
      strclausulawhere = strclausulawhere & " AND FR73ABCCONS='" & strABC_CE & "' "
    Else
      strclausulawhere = " FR73ABCCONS='" & strABC_CE & "' "
    End If
  End If

  If strDesvMayQue <> "" Then
    If strclausulawhere <> "" Then
      strclausulawhere = strclausulawhere & " AND FR73DESVIACION>" & strDesvMayQue & " "
    Else
      strclausulawhere = " FR73DESVIACION>" & strDesvMayQue & " "
    End If
  End If
  
 ' If gstrLlamador = "FrmGenPetOfe" Then
 '   strclausulawhere = strclausulawhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov & ") "
 ' End If

  objWinInfo.objWinActiveForm.strWhere = strclausulawhere & _
                                        " AND (FR73INDREENV=-1 OR FR73INDREECGAL=-1)"
  Call objWinInfo.DataRefresh
  txtText1(5).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Medicamentos"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR7300"
    
    .blnAskPrimary = False
    .intAllowance = cwAllowReadOnly
    .strWhere = " (FR00CODGRPTERAP NOT LIKE 'Q%' OR FR00CODGRPTERAP IS NULL) " & _
                " AND (FR73INDREENV=-1 OR FR73INDREECGAL=-1)"
    
    'If gstrLlamador = "FrmGenPetOfe" Then
    '  .strWhere = .strWhere & " AND (FR79CODPROVEEDOR_A=" & gintFR501Prov & " OR FR79CODPROVEEDOR_B=" & gintFR501Prov & " OR FR79CODPROVEEDOR_C=" & gintFR501Prov & ") "
    'End If
    
    Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Medicamentos")
    Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno Medicamento", cwString)
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Medicamento", cwString)
    Call .FormAddFilterOrder(strKey, "FR73CODINTFAR", "C�digo Interno Medicamento")
    Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n Medicamento")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    Call .WinRegister
    Call .WinStabilize
  End With

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
  If strFormName = "Medicamentos" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     '.strWhere = ""
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
     
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno Producto"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Producto"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad Medida"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtText1_Click(Index As Integer)
  txtText1(5).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub Command2_Click()
  gintprodbuscado = txtText1(0).Text
  Unload Me
End Sub

