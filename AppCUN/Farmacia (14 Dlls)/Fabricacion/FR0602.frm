VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form FrmCrtlFabMez 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Fabricaci�n. Controlar Fabricaci�n de Mezclas"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0602.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdEtiquetas 
      Caption         =   "Repetir Etiquetas Carro"
      Height          =   350
      Left            =   3720
      TabIndex        =   28
      Top             =   7680
      Width           =   2055
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Mezclas del Carro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4320
      Index           =   2
      Left            =   120
      TabIndex        =   26
      Top             =   3240
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3825
         Index           =   1
         Left            =   120
         TabIndex        =   27
         Top             =   360
         Width           =   11385
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "BLANCO"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0602.frx":000C
         stylesets(1).Name=   "AZUL"
         stylesets(1).BackColor=   16776960
         stylesets(1).Picture=   "FR0602.frx":0028
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20082
         _ExtentY        =   6747
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdPrepararCarro 
      Caption         =   "Preparar Carro"
      Height          =   375
      Left            =   10560
      TabIndex        =   25
      Top             =   2160
      Width           =   1335
   End
   Begin VB.OptionButton Option2 
      Caption         =   "CITO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000014&
      Height          =   255
      Index           =   1
      Left            =   10560
      TabIndex        =   24
      Top             =   1800
      Width           =   855
   End
   Begin VB.OptionButton Option2 
      Caption         =   "MIV"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFF00&
      Height          =   255
      Index           =   0
      Left            =   10560
      TabIndex        =   23
      Top             =   1560
      Value           =   -1  'True
      Width           =   855
   End
   Begin VB.CommandButton cmdMezclas 
      Caption         =   "Mostrar"
      Default         =   -1  'True
      Height          =   315
      Left            =   10560
      TabIndex        =   20
      Top             =   2880
      Width           =   1095
   End
   Begin VB.CommandButton cmdSelTodos 
      Caption         =   "Sel.Todos"
      Height          =   315
      Left            =   10560
      TabIndex        =   19
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   480
      Width           =   10260
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         Index           =   1
         Left            =   120
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   360
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0602.frx":0044
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(203)"
         Tab(0).Control(1)=   "txtText1(204)"
         Tab(0).Control(2)=   "txtText1(200)"
         Tab(0).Control(3)=   "txtText1(201)"
         Tab(0).Control(4)=   "txtText1(202)"
         Tab(0).Control(5)=   "txtText1(205)"
         Tab(0).Control(6)=   "txtText1(206)"
         Tab(0).Control(7)=   "txtText1(207)"
         Tab(0).Control(8)=   "lblLabel1(24)"
         Tab(0).Control(9)=   "lblLabel1(230)"
         Tab(0).Control(10)=   "lblLabel1(22)"
         Tab(0).Control(11)=   "lblLabel1(210)"
         Tab(0).Control(12)=   "lblLabel1(20)"
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0602.frx":0060
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR07CODCARRO"
            Height          =   330
            Index           =   203
            Left            =   -74760
            TabIndex        =   2
            Tag             =   "C�d.Carro"
            Top             =   1080
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR07DESCARRO"
            Height          =   330
            Index           =   204
            Left            =   -74160
            TabIndex        =   3
            Tag             =   "Carro"
            Top             =   1080
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   200
            Left            =   -74760
            TabIndex        =   0
            Tag             =   "C�d.Servicio"
            Top             =   360
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   201
            Left            =   -74160
            TabIndex        =   1
            Tag             =   "Servicio"
            Top             =   360
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR87CODESTCARRO"
            Height          =   330
            Index           =   202
            Left            =   -74760
            TabIndex        =   4
            Tag             =   "C�d.Estado Carro"
            Top             =   1800
            Visible         =   0   'False
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR87DESESTCARRO"
            Height          =   330
            Index           =   205
            Left            =   -74160
            TabIndex        =   5
            Tag             =   "Estado Carro"
            Top             =   1800
            Visible         =   0   'False
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74DIA"
            Height          =   330
            Index           =   206
            Left            =   -68400
            TabIndex        =   6
            Tag             =   "D�a"
            Top             =   1200
            Width           =   500
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74HORASALIDA"
            Height          =   330
            Index           =   207
            Left            =   -67200
            TabIndex        =   7
            Tag             =   "Hora"
            Top             =   1200
            Width           =   700
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2265
            Index           =   2
            Left            =   0
            TabIndex        =   13
            Top             =   0
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   3995
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   24
            Left            =   -74760
            TabIndex        =   18
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   230
            Left            =   -74760
            TabIndex        =   17
            Top             =   120
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   -74760
            TabIndex        =   16
            Top             =   1560
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D�a Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   210
            Left            =   -68400
            TabIndex        =   15
            Top             =   960
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   -67200
            TabIndex        =   14
            Top             =   960
            Width           =   1455
         End
      End
   End
   Begin VB.CommandButton cmdOM 
      Caption         =   "Ver OM"
      Height          =   350
      Left            =   7320
      TabIndex        =   10
      Top             =   7680
      Width           =   1335
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cbohora 
      Height          =   315
      Left            =   10560
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   720
      Width           =   975
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ";"
      DefColWidth     =   9
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "Hora Salida"
      Columns(0).Name =   "Hora Salida"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1720
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   11520
      Top             =   1680
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Hora Salida"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   10560
      TabIndex        =   22
      Top             =   480
      Width           =   1095
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmCrtlFabMez"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmCtrlFabMez(FR0602.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: MAYO DE 1999                                                  *
'* DESCRIPCION: Controlar Fabricaci�n de Mezclas                        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'********************************************************************************
'*                                                                              *
'* txtText1(0) Cod.Carro
'* txtText1(34)  Cod.Peticion
'* txtText1(45)  Num.Linea
'* txtText1(27)  Cod.Producto
'* txtText1(52) Cod.Producto_2
'* txtText1(40)  Cod.Producto_dil
'* txtText1(14) Num.Modif.
'* txtText1(28) FR32CANTIDAD
'* txtText1(1)  Hora de administraci�n, se saca por la frecuencia
'* cboDBCombo1(1) Cod.Frecuencia
'* txtText1(3) Cod.Persona
'* txtText1(4) Cod.Cama
'*********************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim gstrEstilo As String
Dim gintlinea As Integer
Dim gintHora As Integer
Dim gPet



Private Sub cbohora_CloseUp()
Dim strFechaSer As String
Dim rstFechaSer As rdoResultset
Dim fecha As Date
Dim intDiaSem As Integer
Dim DiaSem(7) As String

Me.Enabled = False
    strFechaSer = "SELECT SYSDATE FROM DUAL"
    Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
    While rstFechaSer.StillExecuting
    Wend
    
    fecha = rstFechaSer(0).Value
    rstFechaSer.Close
    intDiaSem = WeekDay(fecha, vbMonday)
    DiaSem(1) = "LUN"
    DiaSem(2) = "MAR"
    DiaSem(3) = "MIE"
    DiaSem(4) = "JUE"
    DiaSem(5) = "VIE"
    DiaSem(6) = "SAB"
    DiaSem(7) = "DOM"
    
    If cbohora.Text <> "" Then
      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
      objWinInfo.objWinActiveForm.strWhere = "FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
      objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cbohora, ",", ".", 1)
      'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND SYSDATE BETWEEN FR07FECINIVIG AND NVL(FR07FECFINVIG,TO_DATE('31/12/9999','DD/MM/YYYY'))"
      objWinInfo.DataRefresh
      'Call cmdMezclas_Click
    End If
  
  grdDBGrid1(1).Redraw = False
  Call grdDBGrid1(1).RemoveAll
  grdDBGrid1(1).Redraw = True

  Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
  objWinInfo.objWinActiveForm.strWhere = " -1=0"

Me.Enabled = True

End Sub

Private Sub cmdEtiquetas_Click()
Dim intNumLin As Integer

If grdDBGrid1(2).Rows > 0 Then
  intNumLin = grdDBGrid1(1).SelBookmarks.Count
  If intNumLin > 0 Then
    Call fabricar_etiquetas(grdDBGrid1(2).Columns("C�d.Carro").Value, objGen.ReplaceStr(grdDBGrid1(2).Columns("Hora").Value, ",", ".", 1))
  Else
    Call MsgBox("No ha seleccionado ninguna mezcla.", vbInformation, "Aviso")
  End If
End If

End Sub



Private Sub cmdMezclas_Click()
Dim strCarros As String
Dim nTotal As Long
Dim nTotalSelRows As Integer
Dim i As Integer
Dim bkmrk As Variant

cmdMezclas.Enabled = False
Screen.MousePointer = vbHourglass
Me.Enabled = False

  If grdDBGrid1(2).Rows = 0 Then
    Me.Enabled = True
    Screen.MousePointer = vbDefault
    cmdMezclas.Enabled = True
    Exit Sub
  End If

  strCarros = ""
  nTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(2).SelBookmarks(i)
      strCarros = strCarros & grdDBGrid1(2).Columns("C�d.Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grdDBGrid1(2).Columns("C�d.Carro").Value
  End If
  
      
  Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
  objWinInfo.objWinActiveForm.strWhere = " FR06FECCARGA=TRUNC(SYSDATE)"
  objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR06HORACARGA=" & objGen.ReplaceStr(cbohora, ",", ".", 1)
  objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR06INDMEZCLA=-1 "
  objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR07CODCARRO IN (" & strCarros & ")"
  If Option2(0).Value = True Then
    objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR06INDCITOSTATICO=0"
  Else
    objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR06INDCITOSTATICO=-1"
  End If

  objWinInfo.DataRefresh

Me.Enabled = True
Screen.MousePointer = vbDefault
cmdMezclas.Enabled = True

End Sub



Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If

End Sub


Private Sub cmdSelTodos_Click()
Dim i As Integer

cmdSelTodos.Enabled = False
Me.Enabled = False

grdDBGrid1(2).Redraw = False
grdDBGrid1(2).SelBookmarks.RemoveAll
grdDBGrid1(2).MoveFirst ' Position at the first row
For i = 0 To grdDBGrid1(2).Rows
  grdDBGrid1(2).SelBookmarks.Add grdDBGrid1(2).Bookmark
  grdDBGrid1(2).MoveNext
Next i
grdDBGrid1(2).Redraw = True
'Call cmdMezclas_Click

Me.Enabled = True
cmdSelTodos.Enabled = True

End Sub




Private Sub cmdOM_Click()
cmdOM.Enabled = False
  If grdDBGrid1(1).Rows > 0 Then
    If grdDBGrid1(1).Columns("Petici�n").Value <> "" Then
      gstrLlamadorProd = "CtrlFabMez"
      gcodigopeticion = grdDBGrid1(1).Columns("Petici�n").Value
      Call objsecurity.LaunchProcess("FR0610")
      gstrLlamadorProd = ""
    End If
  End If
cmdOM.Enabled = True
End Sub





Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
Dim v As Integer

  If Index = 1 Then
    If grdDBGrid1(1).Rows > 0 Then
      If gintlinea <> grdDBGrid1(1).Columns("L�nea").Value Then
        If gstrEstilo = "BLANCO" Then
          gstrEstilo = "AZUL"
        Else
          gstrEstilo = "BLANCO"
        End If
      Else
        If gintHora <> grdDBGrid1(1).Columns("H.Adm.").Value Then
          If gstrEstilo = "BLANCO" Then
            gstrEstilo = "AZUL"
          Else
            gstrEstilo = "BLANCO"
          End If
        Else
          If gPet <> grdDBGrid1(1).Columns("Petici�n").Value Then
            If gstrEstilo = "BLANCO" Then
              gstrEstilo = "AZUL"
            Else
              gstrEstilo = "BLANCO"
            End If
          End If
        End If
      End If
      gintHora = grdDBGrid1(1).Columns("H.Adm.").Value
      gintlinea = grdDBGrid1(1).Columns("L�nea").Value
      gPet = grdDBGrid1(1).Columns("Petici�n").Value
      For i = 0 To 22
        grdDBGrid1(1).Columns(i).CellStyleSet gstrEstilo
      Next i
      Select Case grdDBGrid1(1).Columns("Grupo").Value
      Case 1
        grdDBGrid1(1).Columns("Tipo").Value = "Prod."
      Case 2
        grdDBGrid1(1).Columns("Tipo").Value = "Dil."
      Case 3
        grdDBGrid1(1).Columns("Tipo").Value = "Prod2"
      End Select
    End If
  End If
  

End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
Dim mensaje As String
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String
Dim strFechaSer As String
Dim rstFechaSer As rdoResultset
Dim fecha As Date
Dim intDiaSem As Integer
Dim DiaSem(7) As String
Dim stra As String
Dim rsta As rdoResultset
Dim strIniciado As String
Dim rstIniciado As rdoResultset
Dim strupdIniciado As String

  strFechaSer = "SELECT SYSDATE FROM DUAL"
  Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
  While rstFechaSer.StillExecuting
  Wend
  
  fecha = rstFechaSer(0).Value
  intDiaSem = WeekDay(fecha, vbMonday)
  DiaSem(1) = "LUN"
  DiaSem(2) = "MAR"
  DiaSem(3) = "MIE"
  DiaSem(4) = "JUE"
  DiaSem(5) = "VIE"
  DiaSem(6) = "SAB"
  DiaSem(7) = "DOM"
    
  strIniciado = "SELECT COUNT(*) FROM FR0600 WHERE FR06FECCARGA=TRUNC(SYSDATE) AND FR06INDMEZCLA=-1 AND FR06INDCITOSTATICO=0"
  Set rstIniciado = objApp.rdoConnect.OpenResultset(strIniciado)
  If rstIniciado(0).Value = 0 Then
    strupdIniciado = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=1 WHERE " & _
        " FR74DIA=" & "'" & DiaSem(intDiaSem) & "' AND FR87CODESTCARRO_MIV NOT IN (1,4) "
    objApp.rdoConnect.Execute strupdIniciado, 64
  End If
  rstIniciado.Close
  Set rstIniciado = Nothing
    
  strIniciado = "SELECT COUNT(*) FROM FR0600 WHERE FR06FECCARGA=TRUNC(SYSDATE) AND FR06INDMEZCLA=-1 AND FR06INDCITOSTATICO=-1"
  Set rstIniciado = objApp.rdoConnect.OpenResultset(strIniciado)
  If rstIniciado(0).Value = 0 Then
    strupdIniciado = "UPDATE FR7400 SET FR87CODESTCARRO_CIT=1 WHERE " & _
        " FR74DIA=" & "'" & DiaSem(intDiaSem) & "' AND FR87CODESTCARRO_CIT NOT IN (1,4) "
    objApp.rdoConnect.Execute strupdIniciado, 64
  End If
  rstIniciado.Close
  Set rstIniciado = Nothing
    
  cbohora.RemoveAll
  stra = "SELECT DISTINCT FR74HORASALIDA FROM FR0701J " & _
  " WHERE FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'" '& _
  " AND SYSDATE BETWEEN FR07FECINIVIG AND NVL(FR07FECFINVIG,TO_DATE('31/12/9999','DD/MM/YYYY'))"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While rsta.StillExecuting
  Wend
  
  While (Not rsta.EOF)
      Call cbohora.AddItem(rsta.rdoColumns("FR74HORASALIDA").Value)
      rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  cbohora.MoveFirst
  cbohora = cbohora.Columns(0).Value
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Carros"
      
    .strTable = "FR0701J" 'JRC FRJ904J
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    's�lo los carros cuyo estado es <No se ha iniciado la carga>
    .strWhere = "FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
    .strWhere = .strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cbohora, ",", ".", 1)
    '.strWhere = .strWhere & " AND SYSDATE BETWEEN FR07FECINIVIG AND NVL(FR07FECFINVIG,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    'mstrDiaSem = DiaSem(intDiaSem)
    
    Call .FormAddOrderField("FR74DIA", cwAscending)
    Call .FormAddOrderField("FR74HORASALIDA", cwAscending)
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Carros")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "C�digo Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR07DESCARRO", "Descripci�n Carro", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO", "C�d.Estado Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR87DESESTCARRO", "Desc.Estado Carro", cwString)
    Call .FormAddFilterWhere(strKey, "FR74DIA", "D�a Salida", cwString)
    Call .FormAddFilterWhere(strKey, "FR74HORASALIDA", "Hora Salida", cwNumeric)
  End With
   
  With objMultiInfo
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Mezclas del Carro"
    .intAllowance = cwAllowReadOnly
    

    '.strTable = "FR0602J"
    '.strTable = "FR0608J"
    .strTable = "FR0600"
    .intCursorSize = 0
    
'jrc        Call .FormAddRelation("FR07CODCARRO", txtText1(203))
'    Call .FormAddRelation("AD15CODCAMA", txtText1(9))
'    Call .FormAddRelation("CI21CODPERSONA", txtText1(10))
'    Call .FormAddRelation("FR66CODPETICION", txtText1(11))
'    Call .FormAddRelation("FR28NUMLINEA", txtText1(12))
'    Call .FormAddRelation("FR06FECCARGA", txtText1(14)) 'FR32DIATOMAULT
'jrc        Call .FormAddRelation("FR06HORACARGA", txtText1(207)) 'FR32HORATOMAULT
    
'jrc        .strWhere = " FR06FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')"
    
    .strWhere = " -1=0 "
    
    Call .objPrinter.Add("FR6021", "Mezclas del Carro")
    Call .objPrinter.Add("FR6022", "Citost�ticos del Carro")
    
    Call .FormAddOrderField("FR07CODCARRO", cwAscending)
    Call .FormAddOrderField("FR06HORATOMA", cwAscending)
    Call .FormAddOrderField("FR66CODPETICION", cwAscending)
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddOrderField("FR06GRUPO", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Mezclas del Carro")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR06CANTIDAD", "Cantidad", cwNumeric)
    
  End With
   
  With objWinInfo

    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "Carro", "FR07CODCARRO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Persona", "CI21CODPERSONA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Cama", "GCFN06(AD15CODCAMA)", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7) 'historia
    Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25) 'nombre
    Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25) 'apellido 1�
    Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25) 'apellido 2�
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Int", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Petici�n", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Ref", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Cant", "FR06CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "FR06FORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR06DOSIS", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "FR06UNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Grupo", "FR06GRUPO", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "H.Adm.", "FR06HORATOMA", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Tipo", "", cwString, 5)
    
    
    Call .FormCreateInfo(objMasterInfo)
    'Call .FormCreateInfo(objDetailInfo)
    Call .FormChangeColor(objMultiInfo)

    .CtrlGetInfo(txtText1(202)).blnInGrid = False
    .CtrlGetInfo(txtText1(205)).blnInGrid = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(6), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(7), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(8), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(9), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(10)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(10)), grdDBGrid1(1).Columns(11), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(10)), grdDBGrid1(1).Columns("Ref"), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(10)), grdDBGrid1(1).Columns(16), "FR73DESPRODUCTO")
    
    Call .WinRegister
    Call .WinStabilize
  End With
 
 Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
 Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))

 grdDBGrid1(2).Columns("C�d.Servicio").Width = 1200
 grdDBGrid1(2).Columns("Servicio").Width = 3000
 grdDBGrid1(2).Columns("C�d.Carro").Width = 1000
 grdDBGrid1(2).Columns("Carro").Width = 3000
 grdDBGrid1(2).Columns("D�a").Width = 600
 grdDBGrid1(2).Columns("Hora").Width = 600
  
  grdDBGrid1(1).Columns(3).Visible = True
  grdDBGrid1(1).Columns(12).Visible = False
  grdDBGrid1(1).Columns(13).Visible = False
  grdDBGrid1(1).Columns(4).Visible = False
  grdDBGrid1(1).Columns(10).Visible = False
  grdDBGrid1(1).Columns(5).Width = 600 'cama
  grdDBGrid1(1).Columns(4).Width = 1000 'c�d.persona
  grdDBGrid1(1).Columns(6).Width = 800 'historia
  grdDBGrid1(1).Columns(6).Visible = False 'historia
  grdDBGrid1(1).Columns(7).Width = 950 'nombre
  grdDBGrid1(1).Columns(8).Width = 1200 'apellido 1�
  grdDBGrid1(1).Columns(9).Width = 1200 'apellido 2�
  grdDBGrid1(1).Columns(10).Width = 1000 'c�d.prod
  grdDBGrid1(1).Columns(11).Width = 700 'c�d.int
  grdDBGrid1(1).Columns(16).Width = 2800 'desc.prod
  grdDBGrid1(1).Columns(17).Width = 450 'F.F
  grdDBGrid1(1).Columns(18).Width = 700 'dosis
  grdDBGrid1(1).Columns(19).Width = 650 'U.M
  grdDBGrid1(1).Columns(14).Width = 1000 'referencia
  grdDBGrid1(1).Columns(15).Width = 600 'cantidad
  grdDBGrid1(1).Columns(14).Visible = False
  
  grdDBGrid1(1).Columns(20).Visible = False 'Grupo
  grdDBGrid1(1).Columns(21).Position = 4 'H.Adm.
  grdDBGrid1(1).Columns(22).Position = 5 'Tipo
  
  
  
  gstrEstilo = "BLANCO"
  gintlinea = 0
  gintHora = -1
  gPet = -1
       
       
  blnImpresoraSeleccionada = False
       
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strFilter As String

  If strFormName = "Mezclas del Carro" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      If intReport = 1 Then
        Call Imprimir_Mez("FR6021.RPT", 0) 'mezclas
      Else
        Call Imprimir_Cit("FR6022.RPT", 0) 'citostaticos
      End If
    End If
    Set objPrinter = Nothing
  End If

End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub



Private Sub Option2_Click(Index As Integer)

  Call cmdMezclas_Click

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



Private Sub Imprimir_Mez(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
Dim strCarros As String
Dim nTotal As Long
Dim nTotalSelRows As Integer
Dim i As Integer
Dim bkmrk As Variant
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.getdatabasepasswordold
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado

  strCarros = ""
  nTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(2).SelBookmarks(i)
      strCarros = strCarros & grdDBGrid1(2).Columns("C�d.Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grdDBGrid1(2).Columns("C�d.Carro").Value
  End If
        
  strWhere = ""
  strWhere = strWhere & " {FR0608J.HORACAR} = " & objGen.ReplaceStr(cbohora, ",", ".", 1)
  strWhere = strWhere & " AND {FR0608J.HORASAL} = " & objGen.ReplaceStr(cbohora, ",", ".", 1)
  strWhere = strWhere & " AND " & CrearWhere("{FR0608J.CODCARRO}", "(" & strCarros & ")")
  strWhere = strWhere & " AND {FR0608J.FR06INDCITOSTATICO}=0"

  CrystalReport1.SelectionFormula = strWhere
  'On Error GoTo Err_imp3
  CrystalReport1.Action = 1
Err_imp3:

End Sub
Private Sub Imprimir_Cit(strListado As String, intDes As Integer)
'JMRL 19991125
'Toma como par�metro el listado a imprimir y lo manda a la impresora;
'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
Dim strWhere As String
Dim strDNS As String
Dim strUser As String
Dim strPass As String
Dim strPATH As String
Dim strCarros As String
Dim nTotal As Long
Dim nTotalSelRows As Integer
Dim i As Integer
Dim bkmrk As Variant
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.getdatabasepasswordold
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado

  strCarros = ""
  nTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(2).SelBookmarks(i)
      strCarros = strCarros & grdDBGrid1(2).Columns("C�d.Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grdDBGrid1(2).Columns("C�d.Carro").Value
  End If
        
  strWhere = ""
  strWhere = strWhere & " {FR0608J.HORACAR} = " & objGen.ReplaceStr(cbohora, ",", ".", 1)
  strWhere = strWhere & " AND {FR0608J.HORASAL} = " & objGen.ReplaceStr(cbohora, ",", ".", 1)
  strWhere = strWhere & " AND " & CrearWhere("{FR0608J.CODCARRO}", "(" & strCarros & ")")
  strWhere = strWhere & " AND {FR0608J.FR06INDCITOSTATICO}=-1"

  CrystalReport1.SelectionFormula = strWhere
  'On Error GoTo Err_imp3
  CrystalReport1.Action = 1
Err_imp3:

End Sub


Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function


Private Function formatear(cadena As String, espacios As Integer, izq As Boolean, cortar As Boolean) As String
Dim i As Integer
Dim strMask As String
Dim strAux As String
  
  strAux = cadena
  If cortar Then
    If Len(strAux) > espacios Then
      strAux = Left$(strAux, espacios)
    End If
  End If
  strMask = ""
  For i = 1 To espacios
    strMask = strMask & "@"
  Next i
  If izq Then
    formatear = Format(strAux, "!" & strMask)
  Else
    formatear = Format(strAux, strMask)
  End If

End Function


Private Sub ImpresoraDefecto(Nombre As String)
Dim x As Printer

For Each x In Printers
  If UCase(x.DeviceName) = UCase(Nombre) Then
    ' La define como predeterminada del sistema.
    Set Printer = x
    ' Sale del bucle.
    Exit For
  End If
Next

End Sub

Private Function Texto_Etiqueta(cadena As String, x As Integer, y As Integer, rot As Integer, letra As String, horMult As Integer, vertMult As Integer, TipoImg As String) As String
 
Texto_Etiqueta = "A" & x & "," & y & "," & rot & "," & letra & "," & horMult & "," & vertMult & "," & TipoImg & "," & Chr(34) & transfCadena(cadena) & Chr(34)

End Function

Private Function transfCadena(cadena As String) As String
Dim i As Integer

  For i = 1 To Len(cadena)
    Select Case Mid(cadena, i, 1)
      Case "�"
        Mid(cadena, i, 1) = Chr$(160)
      Case "�"
        Mid(cadena, i, 1) = Chr$(130)
      Case "�"
        Mid(cadena, i, 1) = Chr$(161)
      Case "�"
        Mid(cadena, i, 1) = Chr$(162)
      Case "�"
        Mid(cadena, i, 1) = Chr$(163)
      Case "�"
        Mid(cadena, i, 1) = Chr$(168)
      Case "�"
        Mid(cadena, i, 1) = Chr$(164)
      Case "�"
        Mid(cadena, i, 1) = Chr$(165)
      Case "�"
        Mid(cadena, i, 1) = Chr$(129)
      Case "�"
        Mid(cadena, i, 1) = Chr$(166)
      Case "�"
        Mid(cadena, i, 1) = Chr$(167)
    End Select
  Next

  transfCadena = cadena

End Function

Private Sub fabricar_etiquetas_temp(carro, hora)
Dim strUpdate As String
Dim rstori As rdoResultset
Dim strori As String
Dim rstalm As rdoResultset
Dim stralm As String
Dim rstmezcla As rdoResultset
Dim strmezcla As String
Dim strmedidadil As String
Dim curDosis2 As Currency
Dim curCantidad2 As Currency
Dim auxProd2 As String
Dim strProd2 As String
Dim qryProd2 As rdoQuery
Dim rstProd2 As rdoResultset
Dim auxProd1 As String
Dim strProd1 As String
Dim qryProd1 As rdoQuery
Dim rstProd1 As rdoResultset
Dim curDosis1 As Currency
Dim curCantidad1 As Currency
Dim vntAuxCantidad1 As Variant
Dim vntAuxCantidad2 As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim qryupdate As rdoQuery
Dim strlinea As String
Dim strpaciente As String
Dim qrypaciente As rdoQuery
Dim rstpaciente As rdoResultset
Dim strcama As String
Dim qrycama As rdoQuery
Dim rstcama As rdoResultset
Dim strdpto As String
Dim qrydpto As rdoQuery
Dim rstdpto As rdoResultset
Dim auxSolucion As String
Dim strSolucion As String
Dim qrySolucion As rdoQuery
Dim rstSolucion As rdoResultset
Dim strcaducidad As String
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim Auxfecha As String
Dim auxVia As String
Dim strVia As String
Dim qryVia As rdoQuery
Dim rstVia As rdoResultset
Dim strHoras As String
Dim strMinutos As String
Dim strHAdm As String
Dim strInstrucctiones As String
Dim blnEtiquetas As Boolean
Dim auxdpto As String
Dim Auxcama As String
Dim i  As Integer
Dim auxInstrucctiones As String
Dim auxVolTotal As String
Dim auxStr As String
Dim auxcant As String
Dim strFab As String
Dim rstFab As rdoResultset
Dim qryFab As rdoQuery
Dim crlf As String
Dim Incremento As Integer
Dim contLinea As Integer
Dim strListado As String
Dim auxStrListado As String
Dim peticionlinea As String
Dim strparejas As Variant
Dim intNumLin As Integer
Dim contador As Integer
Dim mvarBkmrk As Variant

intNumLin = grdDBGrid1(1).SelBookmarks.Count
strparejas = "("
For contador = 0 To intNumLin - 1
  mvarBkmrk = grdDBGrid1(1).SelBookmarks(contador)
  peticionlinea = "(FR0600.FR66CODPETICION=" & grdDBGrid1(1).Columns(12).CellValue(mvarBkmrk) & _
             " AND FR0600.FR28NUMLINEA=" & grdDBGrid1(1).Columns(13).CellValue(mvarBkmrk) & ")"
  If contador = intNumLin - 1 Then
    strparejas = strparejas & peticionlinea
  Else
    strparejas = strparejas & peticionlinea & " OR "
  End If
Next contador
strparejas = strparejas & ")"

strFab = "SELECT DISTINCT FRJ902J.* FROM FR0600,FRJ902J"
strFab = strFab & " WHERE"
strFab = strFab & " FR0600.FR66CODPETICION=FRJ902J.FR66CODPETICION"
strFab = strFab & " AND FR0600.FR28NUMLINEA=FRJ902J.FR28NUMLINEA"
strFab = strFab & " AND FRJ902J.FR32NUMMODIFIC=1"
strFab = strFab & " AND FR0600.FR06INDMEZCLA=-1"
strFab = strFab & " AND FR0600.FR07CODCARRO=?"
strFab = strFab & " AND FRJ902J.HORASALIDA=?"
strFab = strFab & " AND FRJ902J.DIASALIDA=TO_NUMBER(TO_CHAR(SYSDATE,'d'))"
strFab = strFab & " AND FR0600.FR06HORACARGA=?"
strFab = strFab & " AND FR0600.FR06FECCARGA=TRUNC(sysdate)"
strFab = strFab & " AND FRJ902J.FR26CODESTPETIC=4"
strFab = strFab & " AND FRJ902J.FR32INDPERF=-1 AND FRJ902J.FR32OPERACION IN ('/','M','P','F')"
strFab = strFab & " AND FRJ902J.FR07CODCARRO=?" & " AND " & strparejas
If Option2(0) Then
    strFab = strFab & " AND (FR0600.FR06INDCITOSTATICO=0 OR FR0600.FR06INDCITOSTATICO IS NULL)"
Else
    strFab = strFab & " AND FR0600.FR06INDCITOSTATICO=-1"
End If
strFab = strFab & " AND FRFN06(FRJ902J.FR07CODCARRO,FRJ902J.DIASALIDA,FRJ902J.HORASALIDA,FRJ902J.DIAHASTA,FRJ902J.HORAHASTA,"
strFab = strFab & "FRJ902J.FRH5CODPERIODICIDAD,FRJ902J.FR32FECINICIO,FRJ902J.FR32HORAINICIO,FRJ902J.FR32FECFIN,FRJ902J.FR32HORAFIN,FRJ902J.FR66CODPETICION,"
strFab = strFab & "FRJ902J.FR28NUMLINEA,FRJ902J.FR73CODPRODUCTO,FRJ902J.FR73CODPRODUCTO_DIL,"
strFab = strFab & "FRJ902J.FR73CODPRODUCTO_2,FRG5HORA)=1"

Set qryFab = objApp.rdoConnect.CreateQuery("", strFab)
qryFab(0) = carro 'txtText1(3)
qryFab(1) = hora 'txtText1(7)
qryFab(2) = hora 'txtText1(7)
qryFab(3) = carro 'txtText1(3)
Set rstFab = qryFab.OpenResultset(strFab)
While rstFab.StillExecuting
Wend

mintNTotalSelRows = 0
Dim Listado() As String

While Not rstFab.EOF
  mintNTotalSelRows = mintNTotalSelRows + 1
  ReDim Preserve Listado(mintNTotalSelRows) As String

    Listado(mintNTotalSelRows) = ""
    auxSolucion = ""
    auxProd1 = ""
    auxProd2 = ""
    strmedidadil = "ml"
    strlinea = ""
    'If grdDBGrid1(0).Columns("C�digo Petici�n").CellValue(mvarBkmrk) <> "" Then 'petici�n
    If Not IsNull(rstFab("FR66CODPETICION")) Then 'petici�n
        'se obtiene el almac�n de Farmacia
        strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
                "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
                "AND FRH2CODPARAMGEN=3 " & _
                "AND FR04INDPRINCIPAL=-1"
        Set rstori = objApp.rdoConnect.OpenResultset(strori)
        While rstori.StillExecuting
        Wend
        
        If rstori.EOF Then
          strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
                  "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
                  "AND FRH2CODPARAMGEN=3 "
          Set rstori = objApp.rdoConnect.OpenResultset(strori)
          While rstori.StillExecuting
          Wend
          
        End If
        'se obtiene el almac�n de Fabricaci�n
        stralm = "SELECT FRH2PARAMGEN FROM FRH200 " & _
                "WHERE FRH2CODPARAMGEN=18"
    
        Set rstalm = objApp.rdoConnect.OpenResultset(stralm)
        While rstalm.StillExecuting
        Wend
        
        'salida de Farmacia al almac�n de Fabricaci�n del producto
        If Not rstori.EOF And Not rstalm.EOF Then
          'If grdDBGrid1(0).Columns("C�digo Medicamento").CellValue(mvarBkmrk) <> "" Then
          If Not IsNull(rstFab("FR73CODPRODUCTO")) Then
            'If grdDBGrid1(0).Columns("Operaci�n").CellValue(mvarBkmrk) = "F" Then 'Cantidad es del diluyente
            If rstFab("FR32OPERACION") = "F" Then 'Cantidad es del diluyente
              strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
              Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
              'qryProd1(0) = grdDBGrid1(0).Columns("C�digo Medicamento").CellValue(mvarBkmrk)
              qryProd1(0) = rstFab("FR73CODPRODUCTO")
              Set rstProd1 = qryProd1.OpenResultset(strProd1)
              While rstProd1.StillExecuting
              Wend
              
              If Not rstProd1.EOF Then
                If Not IsNull(rstProd1("FR73DOSIS")) Then
                  curDosis1 = CCur(rstProd1("FR73DOSIS").Value)
                Else
                  curDosis1 = 0
                End If
              Else
                '???
              End If
              If curDosis1 <> 0 Then
                'auxProd1 = rstProd1("FR73DESPRODUCTO") & "     " & vntAuxCantidad1 * curDosis1 & "   " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
                If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                  auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
                Else
                  auxStr = rstProd1("FR73DESPRODUCTO")
                End If
                auxStr = formatear(auxStr, 30, True, True)
                auxcant = formatear(vntAuxCantidad1 * curDosis1, 8, False, False)
                'auxProd1 = auxStr & "  " & auxcant & " " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
                auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
              Else
                'auxProd1 = rstProd1("FR73DESPRODUCTO") & "     " & vntAuxCantidad1 & "   " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
                If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                  auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
                Else
                  auxStr = rstProd1("FR73DESPRODUCTO")
                End If
                auxStr = formatear(auxStr, 30, True, True)
                auxcant = formatear(Str(vntAuxCantidad1), 8, False, False)
                'auxProd1 = auxStr & "  " & auxcant & " " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
                auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
              End If
              If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
                strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
              Else
                strcaducidad = " 24 HORAS"
              End If
              rstProd1.Close
              qryProd1.Close
              'If grdDBGrid1(0).Columns("Dosis").CellValue(mvarBkmrk) <> "" Then
              If Not IsNull(rstFab("FR28DOSIS")) Then
                If curDosis1 <> 0 Then
                  'curCantidad1 = Format(CCur(grdDBGrid1(0).Columns("Dosis").CellValue(mvarBkmrk)) / curDosis1, "0.00")
                  curCantidad1 = Format(CCur(rstFab("FR28DOSIS")) / curDosis1, "0.00")
                  vntAuxCantidad1 = objGen.ReplaceStr(curCantidad1, ",", ".", 1)
                Else
                  'vntAuxCantidad1 = objGen.ReplaceStr(grdDBGrid1(0).Columns("Dosis").CellValue(mvarBkmrk), ",", ".", 1)
                  vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR28DOSIS"), ",", ".", 1)
                End If
              Else
                vntAuxCantidad1 = 0
              End If
            Else
              strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
              Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
              'qryProd1(0) = grdDBGrid1(0).Columns("C�digo Medicamento").CellValue(mvarBkmrk)
              qryProd1(0) = rstFab("FR73CODPRODUCTO")
              Set rstProd1 = qryProd1.OpenResultset(strProd1)
              While rstProd1.StillExecuting
              Wend
              
              'vntAuxCantidad1 = objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").CellValue(mvarBkmrk), ",", ".", 1)
              vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR32CANTIDAD"), ",", ".", 1)
              If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
              Else
                auxStr = rstProd1("FR73DESPRODUCTO")
              End If
              auxStr = formatear(auxStr, 30, True, True)
              'auxcant = formatear(grdDBGrid1(0).Columns("Dosis").CellValue(mvarBkmrk), 8, False, False)
              auxcant = formatear(rstFab("FR28DOSIS"), 8, False, False)
              'auxProd1 = auxStr & "  " & auxcant & " " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
              auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
              If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
                strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
              Else
                strcaducidad = " 24 HORAS"
              End If
              rstProd1.Close
              qryProd1.Close
            End If
          End If
          'If grdDBGrid1(0).Columns("C�digo Medicamento 2").CellValue(mvarBkmrk) <> "" Then
          If Not IsNull(rstFab("FR73CODPRODUCTO_2")) Then
            strProd2 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
            Set qryProd2 = objApp.rdoConnect.CreateQuery("", strProd2)
            'qryProd2(0) = grdDBGrid1(0).Columns("C�digo Medicamento 2").CellValue(mvarBkmrk)
            qryProd2(0) = rstFab("FR73CODPRODUCTO_2")
            Set rstProd2 = qryProd2.OpenResultset(strProd2)
            While rstProd2.StillExecuting
            Wend
            
            If InStr(rstProd2("FR73DESPRODUCTO"), " ") > 0 Then
              auxStr = Left$(rstProd2("FR73DESPRODUCTO"), InStr(rstProd2("FR73DESPRODUCTO"), " "))
            Else
              auxStr = rstProd2("FR73DESPRODUCTO")
            End If
            auxStr = formatear(auxStr, 30, True, True)
            'auxcant = formatear(grdDBGrid1(0).Columns("Dosis2").CellValue(mvarBkmrk), 8, False, False)
            auxcant = formatear(rstFab("FR32DOSIS_2"), 8, False, False)
            'auxProd2 = auxStr & "  " & auxcant & " " & grdDBGrid1(0).Columns("UM2").CellValue(mvarBkmrk)
            auxProd2 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA_2")
            If Not rstProd2.EOF Then
              If Not IsNull(rstProd2("FR73DOSIS")) Then
                curDosis2 = CCur(rstProd2("FR73DOSIS").Value)
              Else
                curDosis2 = 0
              End If
            Else
              '???
            End If
            rstProd2.Close
            qryProd2.Close
            'If grdDBGrid1(0).Columns("Dosis2").CellValue(mvarBkmrk) <> "" Then
            If Not IsNull(rstFab("FR32DOSIS_2")) Then
              If curDosis2 <> 0 Then
                'curCantidad2 = Format(CCur(grdDBGrid1(0).Columns("Dosis2").CellValue(mvarBkmrk)) / curDosis2, "0.00")
                curCantidad2 = Format(CCur(rstFab("FR32DOSIS_2")) / curDosis2, "0.00")
                vntAuxCantidad2 = objGen.ReplaceStr(curCantidad2, ",", ".", 1)
              Else
                'vntAuxCantidad2 = objGen.ReplaceStr(grdDBGrid1(0).Columns("Dosis2").CellValue(mvarBkmrk), ",", ".", 1)
                vntAuxCantidad2 = objGen.ReplaceStr(rstFab("FR32DOSIS_2"), ",", ".", 1)
              End If
            Else
              vntAuxCantidad2 = 0
            End If
          End If
          'If grdDBGrid1(0).Columns("C�digo Producto diluir").CellValue(mvarBkmrk) <> "" Then
          If Not IsNull(rstFab("FR73CODPRODUCTO_DIL")) Then
            strSolucion = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO= ? "
            Set qrySolucion = objApp.rdoConnect.CreateQuery("", strSolucion)
            'qrySolucion(0) = grdDBGrid1(0).Columns("C�digo Producto diluir").CellValue(mvarBkmrk)
            qrySolucion(0) = rstFab("FR73CODPRODUCTO_DIL")
            Set rstSolucion = qrySolucion.OpenResultset(strSolucion)
            While rstSolucion.StillExecuting
            Wend
            
            If InStr(rstSolucion("FR73DESPRODUCTO"), " ") > 0 Then
              auxStr = Left$(rstSolucion("FR73DESPRODUCTO"), InStr(rstSolucion("FR73DESPRODUCTO"), " "))
            Else
              auxStr = rstSolucion("FR73DESPRODUCTO")
            End If
            auxStr = formatear(auxStr, 30, True, True)
            'auxcant = formatear(grdDBGrid1(0).Columns("Vol.Dil.").CellValue(mvarBkmrk), 8, False, False)
            auxcant = formatear(rstFab("FR32CANTIDADDIL"), 8, False, False)
            auxSolucion = auxStr & "  " & auxcant & " " & strmedidadil
            rstSolucion.Close
            qrySolucion.Close
          End If
        End If
        
    rstalm.Close
    Set rstalm = Nothing
    rstori.Close
    Set rstori = Nothing
    End If
  
  
    strdpto = "SELECT AD02DESDPTO FROM AD0200,FR6600 WHERE AD0200.AD02CODDPTO=FR6600.AD02CODDPTO AND FR66CODPETICION = ? "
    Set qrydpto = objApp.rdoConnect.CreateQuery("", strdpto)
    'qrydpto(0) = grdDBGrid1(0).Columns("C�digo Petici�n").CellValue(mvarBkmrk)
    qrydpto(0) = rstFab("FR66CODPETICION")
    Set rstdpto = qrydpto.OpenResultset(strdpto)
    While rstdpto.StillExecuting
    Wend
    If Not rstdpto.EOF Then
      'strlinea = strlinea & "   Serv.Resp.:   " & rstdpto(0) & Chr(13)
      auxdpto = rstdpto(0)
    Else
      'strlinea = strlinea & "   Serv.Resp.:   " & Chr(13)
      auxdpto = ""
    End If
    rstdpto.Close
    qrydpto.Close
    
    strcama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD15CODCAMA= ? "
    Set qrycama = objApp.rdoConnect.CreateQuery("", strcama)
    'qrycama(0) = grdDBGrid1(0).Columns("Cama").CellValue(mvarBkmrk)
    qrycama(0) = rstFab("AD15CODCAMA")
    Set rstcama = qrycama.OpenResultset(strcama)
    While rstcama.StillExecuting
    Wend
    
    If Not rstcama.EOF Then
      'strlinea = strlinea & "Cama:  " & Format(rstcama(0), "@@@@@") & "     "
      Auxcama = rstcama(0)
    Else
      'strlinea = strlinea & "Cama:  " & "     " & "     "
      Auxcama = ""
    End If
    rstcama.Close
    qrycama.Close
  
    strpaciente = "SELECT CI2200.CI22PRIAPEL || ',' || CI2200.CI22SEGAPEL || ',' || CI2200.CI22NOMBRE,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA= ? "
    Set qrypaciente = objApp.rdoConnect.CreateQuery("", strpaciente)
    'qrypaciente(0) = grdDBGrid1(0).Columns("Persona").CellValue(mvarBkmrk)
    qrypaciente(0) = rstFab("CI21CODPERSONA")
    Set rstpaciente = qrypaciente.OpenResultset(strpaciente)
    While rstpaciente.StillExecuting
    Wend
    
    If Not rstpaciente.EOF Then
      If Not IsNull(rstpaciente(0)) Then
        'strlinea = strlinea & rstpaciente(0) & "           " & auxdpto & Chr(13)
        strlinea = strlinea & formatear(rstpaciente(0), 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
      Else
        'strlinea = strlinea & "                             " & auxdpto & Chr(13)
        strlinea = strlinea & formatear("", 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
      End If
      If Not IsNull(rstpaciente(1)) Then
        'strlinea = strlinea & rstpaciente(1) & "                                   " & Auxcama & Chr(13)
        strlinea = strlinea & formatear(rstpaciente(1), 30, True, True) & " " & Auxcama & Chr(13)
      Else
        'strlinea = strlinea & "     " & "                                   " & Auxcama & Chr(13)
        strlinea = strlinea & formatear("", 30, True, True) & " " & Auxcama & Chr(13)
      End If
    End If
    rstpaciente.Close
    qrypaciente.Close
    
    
    strfecha = "SELECT TO_CHAR(SYSDATE,'DD-MM-YYYY') FROM DUAL"
    Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
    While rstfecha.StillExecuting
    Wend
    
    Auxfecha = rstfecha(0).Value
    
    strVia = "SELECT * FROM FR3400 WHERE FR34CODVIA= ? "
    Set qryVia = objApp.rdoConnect.CreateQuery("", strVia)
    'qryVia(0) = grdDBGrid1(0).Columns("V�a").CellValue(mvarBkmrk)
    qryVia(0) = rstFab("FR34CODVIA")
    Set rstVia = qryVia.OpenResultset(strVia)
    While rstVia.StillExecuting
    Wend
    
    If Not IsNull(rstVia("FR34DESVIA")) Then
      auxVia = rstVia("FR34DESVIA")
    Else
      auxVia = ""
    End If
    auxVia = formatear(auxVia, 15, True, True)
    rstVia.Close
    qryVia.Close
    
    'If IsNumeric(grdDBGrid1(0).Columns("T.Inf.(min)").CellValue(mvarBkmrk)) Then
    If IsNumeric(rstFab("FR32TIEMMININF")) Then
      strHoras = Format(rstFab("FR32TIEMMININF") \ 60, "00") & "H"
      strMinutos = Format(rstFab("FR32TIEMMININF") Mod 60, "00") & "M"
    Else
      strHoras = ""
      strMinutos = ""
    End If
    
    'strHAdm = Format(grdDBGrid1(0).Columns("H.Adm.").CellValue(mvarBkmrk), "00.00")
    strHAdm = Format(rstFab("FRG5HORA"), "00.00")
    
    'strInstrucctiones = Left$(grdDBGrid1(0).Columns("Inst.Admin.").CellValue(mvarBkmrk), 80)
    'If InStr(strInstrucctiones, Chr(13)) > 0 Then
    '  strInstrucctiones = Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)) - 1)
    'End If
    'strInstrucctiones = grdDBGrid1(0).Columns("Inst.Admin.").CellValue(mvarBkmrk)
    If Not IsNull(rstFab("FR32INSTRADMIN")) Then
      strInstrucctiones = rstFab("FR32INSTRADMIN")
    Else
      strInstrucctiones = ""
    End If
    i = 0
    auxInstrucctiones = ""
    While strInstrucctiones <> "" And i <> 5
      If InStr(strInstrucctiones, Chr(13)) > 0 Then
        auxInstrucctiones = auxInstrucctiones & Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)))
        strInstrucctiones = Right$(strInstrucctiones, Len(strInstrucctiones) - InStr(strInstrucctiones, Chr(13)))
      Else
        auxInstrucctiones = auxInstrucctiones & strInstrucctiones
        strInstrucctiones = ""
      End If
      i = i + 1
    Wend

    auxVolTotal = "Vtotal: " & rstFab("FR32VOLTOTAL") & " ml" '11/02/2000
    
    strlinea = strlinea & Chr(13)
    strlinea = strlinea & "  " & auxProd1 & Chr(13)
    If Trim(auxProd2) <> "" Then
      strlinea = strlinea & "  " & auxProd2 & Chr(13)
      strlinea = strlinea & "  " & auxSolucion & Chr(13)
    Else
      strlinea = strlinea & "  " & auxSolucion & Chr(13)
      strlinea = strlinea & "  " & auxProd2 & Chr(13)
    End If
    strlinea = strlinea & Chr(13)
    strlinea = strlinea & "Adm.: " & objGen.ReplaceStr(strHAdm, ",", ":", 1) & "  " & auxVia & "  " & auxVolTotal & Chr(13)
    strlinea = strlinea & "Inf. " & strHoras & " " & strMinutos & " (" & rstFab("FR32VELPERFUSION") & "ML/H" & ")" & Chr(13)
    strlinea = strlinea & "Prep. " & Auxfecha & "     Cad." & strcaducidad & Chr(13)
    strlinea = strlinea & auxInstrucctiones
  
    Listado(mintNTotalSelRows) = strlinea
  
  rstFab.MoveNext
Wend

rstFab.Close
Set rstFab = Nothing



'''''''''''''''''''''''''''''''''''''''''''''''''''''
blnEtiquetas = False
If blnImpresoraSeleccionada = False Then
  If mintNTotalSelRows > 0 Then
    'Screen.MousePointer = vbDefault
    Load frmFabPrinter
    Call frmFabPrinter.Show(vbModal)
    'Screen.MousePointer = vbHourglass
  End If
End If

If blnImpresoraSeleccionada = True Then
  blnEtiquetas = True
End If

If blnEtiquetas = True Then
  For mintisel = 0 To mintNTotalSelRows - 1
      crlf = Chr$(13) & Chr$(10)
      Incremento = 30
      contLinea = 0
      
      strlinea = crlf
      strlinea = strlinea & "N" & crlf
      strlinea = strlinea & "I8,1,034" & crlf
      strlinea = strlinea & "Q599,24" & crlf
      strlinea = strlinea & "R0,0" & crlf
      strlinea = strlinea & "S2" & crlf
      strlinea = strlinea & "D5" & crlf
      strlinea = strlinea & "ZB" & crlf
      
      strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
      
      strlinea = strlinea & Texto_Etiqueta("CLINICA UNIVERSITARIA. Servicio Farmacia", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
      
      strlinea = strlinea & "LE0," & (contLinea * Incremento) + 8 & ",740,8" & crlf
      contLinea = contLinea + 1
      strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
  
      strListado = Listado(mintisel + 1)
      auxStrListado = ""
      While strListado <> ""
        If InStr(strListado, Chr(13)) > 0 Then
          auxStrListado = Left$(strListado, InStr(strListado, Chr(13)) - 1)
          strListado = Right$(strListado, Len(strListado) - InStr(strListado, Chr(13)))
          strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          If Chr(10) <> strListado Then
            auxStrListado = strListado
            strListado = ""
            strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
            contLinea = contLinea + 1
          Else
            strListado = ""
          End If
        End If
      Wend
      
      strlinea = strlinea & "P1" & crlf
      strlinea = strlinea & " " & crlf
      
      Printer.Print strlinea
      Printer.EndDoc
    
  Next mintisel
End If


End Sub

Private Sub fabricar_etiquetas_todas_temp(carro, hora)
Dim strUpdate As String
Dim rstori As rdoResultset
Dim strori As String
Dim rstalm As rdoResultset
Dim stralm As String
Dim rstmezcla As rdoResultset
Dim strmezcla As String
Dim strmedidadil As String
Dim curDosis2 As Currency
Dim curCantidad2 As Currency
Dim auxProd2 As String
Dim strProd2 As String
Dim qryProd2 As rdoQuery
Dim rstProd2 As rdoResultset
Dim auxProd1 As String
Dim strProd1 As String
Dim qryProd1 As rdoQuery
Dim rstProd1 As rdoResultset
Dim curDosis1 As Currency
Dim curCantidad1 As Currency
Dim vntAuxCantidad1 As Variant
Dim vntAuxCantidad2 As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim qryupdate As rdoQuery
Dim strlinea As String
Dim strpaciente As String
Dim qrypaciente As rdoQuery
Dim rstpaciente As rdoResultset
Dim strcama As String
Dim qrycama As rdoQuery
Dim rstcama As rdoResultset
Dim strdpto As String
Dim qrydpto As rdoQuery
Dim rstdpto As rdoResultset
Dim auxSolucion As String
Dim strSolucion As String
Dim qrySolucion As rdoQuery
Dim rstSolucion As rdoResultset
Dim strcaducidad As String
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim Auxfecha As String
Dim auxVia As String
Dim strVia As String
Dim qryVia As rdoQuery
Dim rstVia As rdoResultset
Dim strHoras As String
Dim strMinutos As String
Dim strHAdm As String
Dim strInstrucctiones As String
Dim blnEtiquetas As Boolean
Dim auxdpto As String
Dim Auxcama As String
Dim i  As Integer
Dim auxInstrucctiones As String
Dim auxVolTotal As String
Dim auxStr As String
Dim auxcant As String
Dim strFab As String
Dim rstFab As rdoResultset
Dim qryFab As rdoQuery
Dim crlf As String
Dim Incremento As Integer
Dim contLinea As Integer
Dim strListado As String
Dim auxStrListado As String
Dim mvarBkmrk As Variant

strFab = "SELECT DISTINCT FRJ902J.* FROM FR0600,FRJ902J"
strFab = strFab & " WHERE"
strFab = strFab & " FR0600.FR66CODPETICION=FRJ902J.FR66CODPETICION"
strFab = strFab & " AND FR0600.FR28NUMLINEA=FRJ902J.FR28NUMLINEA"
strFab = strFab & " AND FRJ902J.FR32NUMMODIFIC=1"
strFab = strFab & " AND FR0600.FR06INDMEZCLA=-1"
strFab = strFab & " AND FR0600.FR07CODCARRO=?"
strFab = strFab & " AND FRJ902J.HORASALIDA=?"
strFab = strFab & " AND FRJ902J.DIASALIDA=TO_NUMBER(TO_CHAR(SYSDATE,'d'))"
strFab = strFab & " AND FR0600.FR06HORACARGA=?"
strFab = strFab & " AND FR0600.FR06FECCARGA=TRUNC(sysdate)"
strFab = strFab & " AND FRJ902J.FR26CODESTPETIC=4"
strFab = strFab & " AND FRJ902J.FR32INDPERF=-1 AND FRJ902J.FR32OPERACION IN ('/','M','P','F')"
strFab = strFab & " AND FRJ902J.FR07CODCARRO=?"
If Option2(0) Then
    strFab = strFab & " AND (FR0600.FR06INDCITOSTATICO=0 OR FR0600.FR06INDCITOSTATICO IS NULL)"
Else
    strFab = strFab & " AND FR0600.FR06INDCITOSTATICO=-1"
End If
strFab = strFab & " AND FRFN06(FRJ902J.FR07CODCARRO,FRJ902J.DIASALIDA,FRJ902J.HORASALIDA,FRJ902J.DIAHASTA,FRJ902J.HORAHASTA,"
strFab = strFab & "FRJ902J.FRH5CODPERIODICIDAD,FRJ902J.FR32FECINICIO,FRJ902J.FR32HORAINICIO,FRJ902J.FR32FECFIN,FRJ902J.FR32HORAFIN,FRJ902J.FR66CODPETICION,"
strFab = strFab & "FRJ902J.FR28NUMLINEA,FRJ902J.FR73CODPRODUCTO,FRJ902J.FR73CODPRODUCTO_DIL,"
strFab = strFab & "FRJ902J.FR73CODPRODUCTO_2,FRG5HORA)=1"

Set qryFab = objApp.rdoConnect.CreateQuery("", strFab)
qryFab(0) = carro 'txtText1(3)
qryFab(1) = hora 'txtText1(7)
qryFab(2) = hora 'txtText1(7)
qryFab(3) = carro 'txtText1(3)
Set rstFab = qryFab.OpenResultset(strFab)
While rstFab.StillExecuting
Wend

mintNTotalSelRows = 0
Dim Listado() As String

While Not rstFab.EOF
  mintNTotalSelRows = mintNTotalSelRows + 1
  ReDim Preserve Listado(mintNTotalSelRows) As String

    Listado(mintNTotalSelRows) = ""
    auxSolucion = ""
    auxProd1 = ""
    auxProd2 = ""
    strmedidadil = "ml"
    strlinea = ""
    'If grdDBGrid1(0).Columns("C�digo Petici�n").CellValue(mvarBkmrk) <> "" Then 'petici�n
    If Not IsNull(rstFab("FR66CODPETICION")) Then 'petici�n
        'se obtiene el almac�n de Farmacia
        strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
                "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
                "AND FRH2CODPARAMGEN=3 " & _
                "AND FR04INDPRINCIPAL=-1"
        Set rstori = objApp.rdoConnect.OpenResultset(strori)
        While rstori.StillExecuting
        Wend
        
        If rstori.EOF Then
          strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
                  "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
                  "AND FRH2CODPARAMGEN=3 "
          Set rstori = objApp.rdoConnect.OpenResultset(strori)
          While rstori.StillExecuting
          Wend
          
        End If
        'se obtiene el almac�n de Fabricaci�n
        stralm = "SELECT FRH2PARAMGEN FROM FRH200 " & _
                "WHERE FRH2CODPARAMGEN=18"
    
        Set rstalm = objApp.rdoConnect.OpenResultset(stralm)
        While rstalm.StillExecuting
        Wend
        
        'salida de Farmacia al almac�n de Fabricaci�n del producto
        If Not rstori.EOF And Not rstalm.EOF Then
          'If grdDBGrid1(0).Columns("C�digo Medicamento").CellValue(mvarBkmrk) <> "" Then
          If Not IsNull(rstFab("FR73CODPRODUCTO")) Then
            'If grdDBGrid1(0).Columns("Operaci�n").CellValue(mvarBkmrk) = "F" Then 'Cantidad es del diluyente
            If rstFab("FR32OPERACION") = "F" Then 'Cantidad es del diluyente
              strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
              Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
              'qryProd1(0) = grdDBGrid1(0).Columns("C�digo Medicamento").CellValue(mvarBkmrk)
              qryProd1(0) = rstFab("FR73CODPRODUCTO")
              Set rstProd1 = qryProd1.OpenResultset(strProd1)
              While rstProd1.StillExecuting
              Wend
              
              If Not rstProd1.EOF Then
                If Not IsNull(rstProd1("FR73DOSIS")) Then
                  curDosis1 = CCur(rstProd1("FR73DOSIS").Value)
                Else
                  curDosis1 = 0
                End If
              Else
                '???
              End If
              If curDosis1 <> 0 Then
                'auxProd1 = rstProd1("FR73DESPRODUCTO") & "     " & vntAuxCantidad1 * curDosis1 & "   " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
                If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                  auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
                Else
                  auxStr = rstProd1("FR73DESPRODUCTO")
                End If
                auxStr = formatear(auxStr, 30, True, True)
                auxcant = formatear(vntAuxCantidad1 * curDosis1, 8, False, False)
                'auxProd1 = auxStr & "  " & auxcant & " " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
                auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
              Else
                'auxProd1 = rstProd1("FR73DESPRODUCTO") & "     " & vntAuxCantidad1 & "   " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
                If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                  auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
                Else
                  auxStr = rstProd1("FR73DESPRODUCTO")
                End If
                auxStr = formatear(auxStr, 30, True, True)
                auxcant = formatear(Str(vntAuxCantidad1), 8, False, False)
                'auxProd1 = auxStr & "  " & auxcant & " " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
                auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
              End If
              If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
                strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
              Else
                strcaducidad = " 24 HORAS"
              End If
              rstProd1.Close
              qryProd1.Close
              'If grdDBGrid1(0).Columns("Dosis").CellValue(mvarBkmrk) <> "" Then
              If Not IsNull(rstFab("FR28DOSIS")) Then
                If curDosis1 <> 0 Then
                  'curCantidad1 = Format(CCur(grdDBGrid1(0).Columns("Dosis").CellValue(mvarBkmrk)) / curDosis1, "0.00")
                  curCantidad1 = Format(CCur(rstFab("FR28DOSIS")) / curDosis1, "0.00")
                  vntAuxCantidad1 = objGen.ReplaceStr(curCantidad1, ",", ".", 1)
                Else
                  'vntAuxCantidad1 = objGen.ReplaceStr(grdDBGrid1(0).Columns("Dosis").CellValue(mvarBkmrk), ",", ".", 1)
                  vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR28DOSIS"), ",", ".", 1)
                End If
              Else
                vntAuxCantidad1 = 0
              End If
            Else
              strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
              Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
              'qryProd1(0) = grdDBGrid1(0).Columns("C�digo Medicamento").CellValue(mvarBkmrk)
              qryProd1(0) = rstFab("FR73CODPRODUCTO")
              Set rstProd1 = qryProd1.OpenResultset(strProd1)
              While rstProd1.StillExecuting
              Wend
              
              'vntAuxCantidad1 = objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").CellValue(mvarBkmrk), ",", ".", 1)
              vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR32CANTIDAD"), ",", ".", 1)
              If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
              Else
                auxStr = rstProd1("FR73DESPRODUCTO")
              End If
              auxStr = formatear(auxStr, 30, True, True)
              'auxcant = formatear(grdDBGrid1(0).Columns("Dosis").CellValue(mvarBkmrk), 8, False, False)
              auxcant = formatear(rstFab("FR28DOSIS"), 8, False, False)
              'auxProd1 = auxStr & "  " & auxcant & " " & grdDBGrid1(0).Columns("UM").CellValue(mvarBkmrk)
              auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
              If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
                strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
              Else
                strcaducidad = " 24 HORAS"
              End If
              rstProd1.Close
              qryProd1.Close
            End If
          End If
          'If grdDBGrid1(0).Columns("C�digo Medicamento 2").CellValue(mvarBkmrk) <> "" Then
          If Not IsNull(rstFab("FR73CODPRODUCTO_2")) Then
            strProd2 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
            Set qryProd2 = objApp.rdoConnect.CreateQuery("", strProd2)
            'qryProd2(0) = grdDBGrid1(0).Columns("C�digo Medicamento 2").CellValue(mvarBkmrk)
            qryProd2(0) = rstFab("FR73CODPRODUCTO_2")
            Set rstProd2 = qryProd2.OpenResultset(strProd2)
            While rstProd2.StillExecuting
            Wend
            
            If InStr(rstProd2("FR73DESPRODUCTO"), " ") > 0 Then
              auxStr = Left$(rstProd2("FR73DESPRODUCTO"), InStr(rstProd2("FR73DESPRODUCTO"), " "))
            Else
              auxStr = rstProd2("FR73DESPRODUCTO")
            End If
            auxStr = formatear(auxStr, 30, True, True)
            'auxcant = formatear(grdDBGrid1(0).Columns("Dosis2").CellValue(mvarBkmrk), 8, False, False)
            auxcant = formatear(rstFab("FR32DOSIS_2"), 8, False, False)
            'auxProd2 = auxStr & "  " & auxcant & " " & grdDBGrid1(0).Columns("UM2").CellValue(mvarBkmrk)
            auxProd2 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA_2")
            If Not rstProd2.EOF Then
              If Not IsNull(rstProd2("FR73DOSIS")) Then
                curDosis2 = CCur(rstProd2("FR73DOSIS").Value)
              Else
                curDosis2 = 0
              End If
            Else
              '???
            End If
            rstProd2.Close
            qryProd2.Close
            'If grdDBGrid1(0).Columns("Dosis2").CellValue(mvarBkmrk) <> "" Then
            If Not IsNull(rstFab("FR32DOSIS_2")) Then
              If curDosis2 <> 0 Then
                'curCantidad2 = Format(CCur(grdDBGrid1(0).Columns("Dosis2").CellValue(mvarBkmrk)) / curDosis2, "0.00")
                curCantidad2 = Format(CCur(rstFab("FR32DOSIS_2")) / curDosis2, "0.00")
                vntAuxCantidad2 = objGen.ReplaceStr(curCantidad2, ",", ".", 1)
              Else
                'vntAuxCantidad2 = objGen.ReplaceStr(grdDBGrid1(0).Columns("Dosis2").CellValue(mvarBkmrk), ",", ".", 1)
                vntAuxCantidad2 = objGen.ReplaceStr(rstFab("FR32DOSIS_2"), ",", ".", 1)
              End If
            Else
              vntAuxCantidad2 = 0
            End If
          End If
          'If grdDBGrid1(0).Columns("C�digo Producto diluir").CellValue(mvarBkmrk) <> "" Then
          If Not IsNull(rstFab("FR73CODPRODUCTO_DIL")) Then
            strSolucion = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO= ? "
            Set qrySolucion = objApp.rdoConnect.CreateQuery("", strSolucion)
            'qrySolucion(0) = grdDBGrid1(0).Columns("C�digo Producto diluir").CellValue(mvarBkmrk)
            qrySolucion(0) = rstFab("FR73CODPRODUCTO_DIL")
            Set rstSolucion = qrySolucion.OpenResultset(strSolucion)
            While rstSolucion.StillExecuting
            Wend
            
            If InStr(rstSolucion("FR73DESPRODUCTO"), " ") > 0 Then
              auxStr = Left$(rstSolucion("FR73DESPRODUCTO"), InStr(rstSolucion("FR73DESPRODUCTO"), " "))
            Else
              auxStr = rstSolucion("FR73DESPRODUCTO")
            End If
            auxStr = formatear(auxStr, 30, True, True)
            'auxcant = formatear(grdDBGrid1(0).Columns("Vol.Dil.").CellValue(mvarBkmrk), 8, False, False)
            auxcant = formatear(rstFab("FR32CANTIDADDIL"), 8, False, False)
            auxSolucion = auxStr & "  " & auxcant & " " & strmedidadil
            rstSolucion.Close
            qrySolucion.Close
          End If
        End If
        
    rstalm.Close
    Set rstalm = Nothing
    rstori.Close
    Set rstori = Nothing
    End If
  
  
    strdpto = "SELECT AD02DESDPTO FROM AD0200,FR6600 WHERE AD0200.AD02CODDPTO=FR6600.AD02CODDPTO AND FR66CODPETICION = ? "
    Set qrydpto = objApp.rdoConnect.CreateQuery("", strdpto)
    'qrydpto(0) = grdDBGrid1(0).Columns("C�digo Petici�n").CellValue(mvarBkmrk)
    qrydpto(0) = rstFab("FR66CODPETICION")
    Set rstdpto = qrydpto.OpenResultset(strdpto)
    While rstdpto.StillExecuting
    Wend
    If Not rstdpto.EOF Then
      'strlinea = strlinea & "   Serv.Resp.:   " & rstdpto(0) & Chr(13)
      auxdpto = rstdpto(0)
    Else
      'strlinea = strlinea & "   Serv.Resp.:   " & Chr(13)
      auxdpto = ""
    End If
    rstdpto.Close
    qrydpto.Close
    
    strcama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD15CODCAMA= ? "
    Set qrycama = objApp.rdoConnect.CreateQuery("", strcama)
    'qrycama(0) = grdDBGrid1(0).Columns("Cama").CellValue(mvarBkmrk)
    qrycama(0) = rstFab("AD15CODCAMA")
    Set rstcama = qrycama.OpenResultset(strcama)
    While rstcama.StillExecuting
    Wend
    
    If Not rstcama.EOF Then
      'strlinea = strlinea & "Cama:  " & Format(rstcama(0), "@@@@@") & "     "
      Auxcama = rstcama(0)
    Else
      'strlinea = strlinea & "Cama:  " & "     " & "     "
      Auxcama = ""
    End If
    rstcama.Close
    qrycama.Close
  
    strpaciente = "SELECT CI2200.CI22PRIAPEL || ',' || CI2200.CI22SEGAPEL || ',' || CI2200.CI22NOMBRE,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA= ? "
    Set qrypaciente = objApp.rdoConnect.CreateQuery("", strpaciente)
    'qrypaciente(0) = grdDBGrid1(0).Columns("Persona").CellValue(mvarBkmrk)
    qrypaciente(0) = rstFab("CI21CODPERSONA")
    Set rstpaciente = qrypaciente.OpenResultset(strpaciente)
    While rstpaciente.StillExecuting
    Wend
    
    If Not rstpaciente.EOF Then
      If Not IsNull(rstpaciente(0)) Then
        'strlinea = strlinea & rstpaciente(0) & "           " & auxdpto & Chr(13)
        strlinea = strlinea & formatear(rstpaciente(0), 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
      Else
        'strlinea = strlinea & "                             " & auxdpto & Chr(13)
        strlinea = strlinea & formatear("", 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
      End If
      If Not IsNull(rstpaciente(1)) Then
        'strlinea = strlinea & rstpaciente(1) & "                                   " & Auxcama & Chr(13)
        strlinea = strlinea & formatear(rstpaciente(1), 30, True, True) & " " & Auxcama & Chr(13)
      Else
        'strlinea = strlinea & "     " & "                                   " & Auxcama & Chr(13)
        strlinea = strlinea & formatear("", 30, True, True) & " " & Auxcama & Chr(13)
      End If
    End If
    rstpaciente.Close
    qrypaciente.Close
    
    
    strfecha = "SELECT TO_CHAR(SYSDATE,'DD-MM-YYYY') FROM DUAL"
    Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
    While rstfecha.StillExecuting
    Wend
    
    Auxfecha = rstfecha(0).Value
    
    strVia = "SELECT * FROM FR3400 WHERE FR34CODVIA= ? "
    Set qryVia = objApp.rdoConnect.CreateQuery("", strVia)
    'qryVia(0) = grdDBGrid1(0).Columns("V�a").CellValue(mvarBkmrk)
    qryVia(0) = rstFab("FR34CODVIA")
    Set rstVia = qryVia.OpenResultset(strVia)
    While rstVia.StillExecuting
    Wend
    
    If Not IsNull(rstVia("FR34DESVIA")) Then
      auxVia = rstVia("FR34DESVIA")
    Else
      auxVia = ""
    End If
    auxVia = formatear(auxVia, 15, True, True)
    rstVia.Close
    qryVia.Close
    
    'If IsNumeric(grdDBGrid1(0).Columns("T.Inf.(min)").CellValue(mvarBkmrk)) Then
    If IsNumeric(rstFab("FR32TIEMMININF")) Then
      strHoras = Format(rstFab("FR32TIEMMININF") \ 60, "00") & "H"
      strMinutos = Format(rstFab("FR32TIEMMININF") Mod 60, "00") & "M"
    Else
      strHoras = ""
      strMinutos = ""
    End If
    
    'strHAdm = Format(grdDBGrid1(0).Columns("H.Adm.").CellValue(mvarBkmrk), "00.00")
    strHAdm = Format(rstFab("FRG5HORA"), "00.00")
    
    'strInstrucctiones = Left$(grdDBGrid1(0).Columns("Inst.Admin.").CellValue(mvarBkmrk), 80)
    'If InStr(strInstrucctiones, Chr(13)) > 0 Then
    '  strInstrucctiones = Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)) - 1)
    'End If
    'strInstrucctiones = grdDBGrid1(0).Columns("Inst.Admin.").CellValue(mvarBkmrk)
    If Not IsNull(rstFab("FR32INSTRADMIN")) Then
      strInstrucctiones = rstFab("FR32INSTRADMIN")
    Else
      strInstrucctiones = ""
    End If
    i = 0
    auxInstrucctiones = ""
    While strInstrucctiones <> "" And i <> 5
      If InStr(strInstrucctiones, Chr(13)) > 0 Then
        auxInstrucctiones = auxInstrucctiones & Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)))
        strInstrucctiones = Right$(strInstrucctiones, Len(strInstrucctiones) - InStr(strInstrucctiones, Chr(13)))
      Else
        auxInstrucctiones = auxInstrucctiones & strInstrucctiones
        strInstrucctiones = ""
      End If
      i = i + 1
    Wend

    auxVolTotal = "Vtotal: " & rstFab("FR32VOLTOTAL") & " ml" '11/02/2000
    
    strlinea = strlinea & Chr(13)
    strlinea = strlinea & "  " & auxProd1 & Chr(13)
    If Trim(auxProd2) <> "" Then
      strlinea = strlinea & "  " & auxProd2 & Chr(13)
      strlinea = strlinea & "  " & auxSolucion & Chr(13)
    Else
      strlinea = strlinea & "  " & auxSolucion & Chr(13)
      strlinea = strlinea & "  " & auxProd2 & Chr(13)
    End If
    strlinea = strlinea & Chr(13)
    strlinea = strlinea & "Adm.: " & objGen.ReplaceStr(strHAdm, ",", ":", 1) & "  " & auxVia & "  " & auxVolTotal & Chr(13)
    strlinea = strlinea & "Inf. " & strHoras & " " & strMinutos & " (" & rstFab("FR32VELPERFUSION") & "ML/H" & ")" & Chr(13)
    strlinea = strlinea & "Prep. " & Auxfecha & "     Cad." & strcaducidad & Chr(13)
    strlinea = strlinea & auxInstrucctiones
  
    Listado(mintNTotalSelRows) = strlinea
  
  rstFab.MoveNext
Wend

rstFab.Close
Set rstFab = Nothing



'''''''''''''''''''''''''''''''''''''''''''''''''''''
blnEtiquetas = False
If blnImpresoraSeleccionada = False Then
  If mintNTotalSelRows > 0 Then
    Screen.MousePointer = vbDefault
    Load frmFabPrinter
    Call frmFabPrinter.Show(vbModal)
    Screen.MousePointer = vbHourglass
  End If
End If

If blnImpresoraSeleccionada = True Then
  blnEtiquetas = True
End If

If blnEtiquetas = True Then
  For mintisel = 0 To mintNTotalSelRows - 1
      crlf = Chr$(13) & Chr$(10)
      Incremento = 30
      contLinea = 0
      
      strlinea = crlf
      strlinea = strlinea & "N" & crlf
      strlinea = strlinea & "I8,1,034" & crlf
      strlinea = strlinea & "Q599,24" & crlf
      strlinea = strlinea & "R0,0" & crlf
      strlinea = strlinea & "S2" & crlf
      strlinea = strlinea & "D5" & crlf
      strlinea = strlinea & "ZB" & crlf
      
      strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
      
      strlinea = strlinea & Texto_Etiqueta("CLINICA UNIVERSITARIA. Servicio Farmacia", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
      
      strlinea = strlinea & "LE0," & (contLinea * Incremento) + 8 & ",740,8" & crlf
      contLinea = contLinea + 1
      strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
  
      strListado = Listado(mintisel + 1)
      auxStrListado = ""
      While strListado <> ""
        If InStr(strListado, Chr(13)) > 0 Then
          auxStrListado = Left$(strListado, InStr(strListado, Chr(13)) - 1)
          strListado = Right$(strListado, Len(strListado) - InStr(strListado, Chr(13)))
          strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          If Chr(10) <> strListado Then
            auxStrListado = strListado
            strListado = ""
            strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
            contLinea = contLinea + 1
          Else
            strListado = ""
          End If
        End If
      Wend
      
      strlinea = strlinea & "P1" & crlf
      strlinea = strlinea & " " & crlf
      
      Printer.Print strlinea
      Printer.EndDoc
    
  Next mintisel
End If


End Sub


Private Sub cmdPrepararCarro_Click()
Dim strUpdate As String
Dim i As Integer
Dim hora As String
Dim stra As String
Dim rsta As rdoResultset
Dim qrya As rdoQuery
Dim mintNTotalSelRows As Integer
Dim mintisel As Integer
Dim mvarBkmrk As Variant
Dim strDispensado As String
Dim rstDispensado As rdoResultset
Dim qryDispensado As rdoQuery
Dim carrosrestantes As Integer
Dim strinsert06 As String
Dim rstprod As rdoResultset
Dim strprod As String
Dim qryprod As rdoQuery
Dim strFF As String
Dim strDosis As String
Dim strUM As String
Dim cantidad As String
Dim gcodproducto
Dim gdescproducto As String
Dim grupo As Integer
Dim strFF_dil As String
Dim strDosis_dil As String
Dim strUM_dil As String
Dim cantidad_dil As String
Dim gcodproducto_dil
Dim gdescproducto_dil As String
Dim grupo_dil As Integer
Dim strFF_mez As String
Dim strDosis_mez As String
Dim strUM_mez As String
Dim cantidad_mez As String
Dim gcodproducto_mez
Dim gdescproducto_mez As String
Dim grupo_mez As Integer
Dim numlinea As Integer
Dim blnValida As Boolean
Dim strFR7400 As String
Dim rstFR7400 As rdoResultset
Dim qryFR7400 As rdoQuery
Dim strFR7400_2 As String
Dim rstFR7400_2 As rdoResultset
Dim qryFR7400_2 As rdoQuery
Dim strDiaSiguiente As String
Dim curHoraSiguiente As Currency
Dim indMIVCito As Integer
Dim blnInsertar As Boolean
Dim strMIV As String
Dim qryMIV As rdoQuery
Dim rstMIV As rdoResultset
Dim updEstCarMIV As String
Dim qryupdEstCarMIV As rdoQuery
Dim updEstCarCIT As String
Dim qryupdEstCarCIT As rdoQuery

If cbohora = "" Then
  Exit Sub
End If

Screen.MousePointer = vbHourglass
cmdPrepararCarro.Enabled = False
Me.Enabled = False

mintNTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
carrosrestantes = grdDBGrid1(2).SelBookmarks.Count

'transforma la coma de separaci�n de los decimales por un punto
'de la hora a la que sale el carro
hora = objGen.ReplaceStr(cbohora, ",", ".", 1)

If Option2(0).Value Then 'MIV
  indMIVCito = 0
Else
  indMIVCito = -1
End If

stra = "SELECT FRG500.FRG5HORA,"
stra = stra & "FR3200.FR66CODPETICION,"
stra = stra & "FR3200.FR28NUMLINEA,"
stra = stra & "FR3200.FR73CODPRODUCTO,"
stra = stra & "FR3200.FR32NUMMODIFIC,"
stra = stra & "FR3200.FR28DOSIS,"
stra = stra & "FR3200.FR34CODVIA,"
stra = stra & "FR3200.FR32VOLUMEN,"
stra = stra & "FR3200.FR32TIEMINFMIN,"
stra = stra & "FR3200.FRG4CODFRECUENCIA,"
stra = stra & "FR3200.FR32OBSERVFARM,"
stra = stra & "FR3200.SG02COD_FRM,"
stra = stra & "FR3200.FR32FECMODIVALI,"
stra = stra & "FR3200.FR32HORAMODIVALI,"
stra = stra & "FR3200.FR32INDSN,"
stra = stra & "FR3200.FR93CODUNIMEDIDA,"
stra = stra & "FR3200.FR32INDISPEN,"
stra = stra & "FR3200.FR32INDCOMIENINMED,"
stra = stra & "FR3200.FRH5CODPERIODICIDAD,"
stra = stra & "FR3200.FRH7CODFORMFAR,"
stra = stra & "FR3200.FR32OPERACION,"
stra = stra & "FR3200.FR32CANTIDAD,"
stra = stra & "FR3200.FR32FECINICIO,"
stra = stra & "FR3200.FR32HORAINICIO,"
stra = stra & "FR3200.FR32FECFIN,"
stra = stra & "FR3200.FR32HORAFIN,"
stra = stra & "FR3200.FR32INDVIAOPC,"
stra = stra & "FR3200.FR73CODPRODUCTO_DIL,"
stra = stra & "FR3200.FR32CANTIDADDIL,"
stra = stra & "FR3200.FR32TIEMMININF,"
stra = stra & "FR3200.FR32REFERENCIA,"
stra = stra & "FR3200.FR32INDESTFAB,"
stra = stra & "FR3200.FR32CANTDISP,"
stra = stra & "FR3200.R32UBICACION,"
stra = stra & "FR3200.FR32INDPERF,"
stra = stra & "FR3200.FR32INSTRADMIN,"
stra = stra & "FR3200.FR32VELPERFUSION,"
stra = stra & "FR3200.FR32VOLTOTAL,"
stra = stra & "FR3200.FR73CODPRODUCTO_2,"
stra = stra & "FR3200.FRH7CODFORMFAR_2,"
stra = stra & "FR3200.FR32DOSIS_2,"
stra = stra & "FR3200.FR93CODUNIMEDIDA_2,"
stra = stra & "FR3200.FR32DESPRODUCTO,"
stra = stra & "FR3200.FR32UBICACION,"
stra = stra & "FR3200.FR32DIATOMAULT,"
stra = stra & "FR3200.FR32HORATOMAULT,"
stra = stra & "FR3200.FR32VOLUMEN_2,"
stra = stra & "FR3200.FR32PATHFORMAG,"
stra = stra & "FR3200.FR32INDDISPPRN,"
stra = stra & "FR6600.FR26CODESTPETIC,"
stra = stra & "FR6600.CI21CODPERSONA,"
stra = stra & "FR6600.AD01CODASISTENCI,"
stra = stra & "FR6600.AD07CODPROCESO,"
stra = stra & "AD1500.AD15CODCAMA,"

stra = stra & "TRUNC(SYSDATE)-TRUNC(FR32FECINICIO) DIASDIFINI,"
stra = stra & "TRUNC(SYSDATE+1)-TRUNC(FR32FECINICIO) DIASDIFINI_2,"
stra = stra & "TRUNC(NVL(FR32FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))-TRUNC(SYSDATE) DIASDIFFIN,"
stra = stra & "TRUNC(NVL(FR32FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))-TRUNC(SYSDATE+1) DIASDIFFIN_2,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE,'d')) DIAHOY,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE,'dd')) DIAMES,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE+1,'d')) DIAHOY_2,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE+1,'dd')) DIAMES_2"

stra = stra & " FROM FR6600,AD1500,FR3200,FRG500,FRJ900"
stra = stra & " WHERE AD1500.AD07CODPROCESO=FR6600.AD07CODPROCESO"
stra = stra & " AND AD1500.AD01CODASISTENCI=FR6600.AD01CODASISTENCI"
stra = stra & " AND FR6600.FR66CODPETICION=fr3200.FR66CODPETICION"
stra = stra & " AND FRG500.FRG4CODFRECUENCIA=FR3200.FRG4CODFRECUENCIA"
stra = stra & " AND AD1500.AD15CODCAMA=FRJ900.AD15CODCAMA"
stra = stra & " AND FRJ900.FR07CODCARRO=?"
stra = stra & " AND FR6600.FR26CODESTPETIC=?"
stra = stra & " AND FR6600.FR66INDOM=?"
stra = stra & " AND FR3200.FR32INDPERF=?"
stra = stra & " AND FR3200.FR32OPERACION IN ('/','M','P','F')"

Set qrya = objApp.rdoConnect.CreateQuery("", stra)

strprod = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO,"
strprod = strprod & "FRH7CODFORMFAR,FR73DOSIS,FR73VOLUMEN "
strprod = strprod & " FROM FR7300 WHERE "
strprod = strprod & " FR73CODPRODUCTO=?"
Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)

strDispensado = "SELECT * FROM FR0600 WHERE " & _
                " FR07CODCARRO=?" & _
                " AND FR06FECCARGA=TRUNC(SYSDATE)" & _
                " AND FR06HORACARGA=" & hora & _
                " AND FR06INDMEZCLA=?" & _
                " AND FR06INDCITOSTATICO=?"
Set qryDispensado = objApp.rdoConnect.CreateQuery("", strDispensado)

strFR7400 = "SELECT FR74DIA,FR74HORASALIDA"
strFR7400 = strFR7400 & " FROM FR7400"
strFR7400 = strFR7400 & " WHERE FR07CODCARRO = ? AND FR87CODESTCARRO<>4"
strFR7400 = strFR7400 & " AND DECODE(FR7400.FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7)=TO_NUMBER(TO_CHAR(SYSDATE,'d'))"
strFR7400 = strFR7400 & " ORDER BY DECODE(FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7),FR74HORASALIDA"
Set qryFR7400 = objApp.rdoConnect.CreateQuery("", strFR7400)

strFR7400_2 = "SELECT FR74DIA,FR74HORASALIDA"
strFR7400_2 = strFR7400_2 & " FROM FR7400"
strFR7400_2 = strFR7400_2 & " WHERE FR07CODCARRO = ? AND FR87CODESTCARRO<>4"
strFR7400_2 = strFR7400_2 & " AND DECODE(FR7400.FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7)=TO_NUMBER(TO_CHAR(SYSDATE+1,'d'))"
strFR7400_2 = strFR7400_2 & " ORDER BY DECODE(FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7),FR74HORASALIDA"
Set qryFR7400_2 = objApp.rdoConnect.CreateQuery("", strFR7400_2)

strMIV = "SELECT COUNT(*) FROM FR7300 WHERE FR73CODPRODUCTO=?"
strMIV = strMIV & " AND FR00CODGRPTERAP LIKE 'L%'"
Set qryMIV = objApp.rdoConnect.CreateQuery("", strMIV)

'se cambia el estado del carro (estado=2 Se ha comenzado la carga)
updEstCarMIV = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=2 WHERE "
updEstCarMIV = updEstCarMIV & "FR07CODCARRO=?"
updEstCarMIV = updEstCarMIV & " AND FR74DIA=?"
updEstCarMIV = updEstCarMIV & " AND FR74HORASALIDA=" & hora
Set qryupdEstCarMIV = objApp.rdoConnect.CreateQuery("", updEstCarMIV)

updEstCarCIT = "UPDATE FR7400 SET FR87CODESTCARRO_CIT=2 WHERE "
updEstCarCIT = updEstCarCIT & "FR07CODCARRO=?"
updEstCarCIT = updEstCarCIT & " AND FR74DIA=?"
updEstCarCIT = updEstCarCIT & " AND FR74HORASALIDA=" & hora
Set qryupdEstCarCIT = objApp.rdoConnect.CreateQuery("", updEstCarCIT)

For mintisel = 0 To mintNTotalSelRows - 1

  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False
  
  carrosrestantes = carrosrestantes - 1
  mvarBkmrk = grdDBGrid1(2).SelBookmarks(mintisel)

  'transforma la coma de separaci�n de los decimales por un punto
  'de la hora a la que sale el carro
  hora = grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)
  hora = objGen.ReplaceStr(hora, ",", ".", 1)

  If grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) <> "" Then
      qryDispensado(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
      'qryDispensado(1) = hora 'grddbgrid1(2).Columns("Hora").CellValue(mvarBkmrk)
      qryDispensado(1) = -1
      qryDispensado(2) = indMIVCito
      Set rstDispensado = qryDispensado.OpenResultset()
      While rstDispensado.StillExecuting
      Wend
      If Not rstDispensado.EOF Then
        MsgBox "El Carro " & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
                " ya ha sido Preparado", vbInformation, "Aviso"
        rstDispensado.Close
        Set rstDispensado = Nothing
        GoTo carro_siguiente
      End If
      rstDispensado.Close
      Set rstDispensado = Nothing
  End If

  qryFR7400(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
  Set rstFR7400 = qryFR7400.OpenResultset()
  While rstFR7400.StillExecuting
  Wend
  If Not rstFR7400.EOF Then
    While Not rstFR7400("FR74HORASALIDA").Value = grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)
      rstFR7400.MoveNext
    Wend
    If Not rstFR7400.EOF Then
      rstFR7400.MoveNext
      If Not rstFR7400.EOF Then
        strDiaSiguiente = rstFR7400("FR74DIA").Value
        curHoraSiguiente = rstFR7400("FR74HORASALIDA").Value
      Else
        qryFR7400_2(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
        Set rstFR7400_2 = qryFR7400_2.OpenResultset()
        While rstFR7400_2.StillExecuting
        Wend
        If Not rstFR7400_2.EOF Then
          strDiaSiguiente = rstFR7400_2("FR74DIA").Value
          curHoraSiguiente = rstFR7400_2("FR74HORASALIDA").Value
        Else
          GoTo carro_siguiente
        End If
        rstFR7400_2.Close
        Set rstFR7400_2 = Nothing
      End If
    Else
      qryFR7400_2(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
      Set rstFR7400_2 = qryFR7400_2.OpenResultset()
      While rstFR7400_2.StillExecuting
      Wend
      If Not rstFR7400_2.EOF Then
        strDiaSiguiente = rstFR7400_2("FR74DIA").Value
        curHoraSiguiente = rstFR7400_2("FR74HORASALIDA").Value
      Else
        GoTo carro_siguiente
      End If
      rstFR7400_2.Close
      Set rstFR7400_2 = Nothing
    End If
  Else
    GoTo carro_siguiente
  End If
  rstFR7400.Close
  Set rstFR7400 = Nothing

  qrya(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
  qrya(1) = 4 'Est.Pet.
  qrya(2) = -1 'IndOM
  qrya(3) = -1 'IndPerf
  Set rsta = qrya.OpenResultset()
  While rsta.StillExecuting
  Wend
  While Not rsta.EOF
    blnInsertar = False
    blnValida = False
    If Option2(0).Value Then 'MIV
      indMIVCito = 0
      blnInsertar = True
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = False
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = False
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = False
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
    Else 'CITOSTATICOS
      indMIVCito = -1
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = True
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = True
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = True
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
    End If
    If blnInsertar = True Then
      If strDiaSiguiente = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk) Then
        If CCur(grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < curHoraSiguiente Then
          If rsta("DIASDIFINI").Value >= 0 And rsta("DIASDIFFIN").Value >= 0 Then
            If rsta("DIASDIFINI").Value = 0 And rsta("DIASDIFFIN").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFINI").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFFIN").Value = 0 Then
              If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            Else
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          End If
        Else 'blnvalida=false
        End If
      Else 'Dia salida<>Dia Hasta
        If CCur(grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < 24 Then
          If rsta("DIASDIFINI").Value >= 0 And rsta("DIASDIFFIN").Value >= 0 Then
            If rsta("DIASDIFINI").Value = 0 And rsta("DIASDIFFIN").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFINI").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFFIN").Value = 0 Then
              If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            Else
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          End If
        ElseIf 0 <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < curHoraSiguiente Then
          If rsta("DIASDIFINI_2").Value >= 0 And rsta("DIASDIFFIN_2").Value >= 0 Then
            If rsta("DIASDIFINI_2").Value = 0 And rsta("DIASDIFFIN_2").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFINI_2").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFFIN_2").Value = 0 Then
              If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                  blnValida = True
                End If
              End If
            Else
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                blnValida = True
              End If
            End If
          End If
        Else
          'blnvalida=false
        End If
      End If
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If blnValida Then
      gcodproducto = ""
      gcodproducto_dil = ""
      gcodproducto_mez = ""
      Select Case rsta.rdoColumns("FR32OPERACION").Value
      Case "/", "P", "M", "E"
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
          'producto principal
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo = 1
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto = rsta.rdoColumns("FR73CODPRODUCTO").Value 'irene
            If rsta.rdoColumns("FR73CODPRODUCTO").Value = 999999999 Then
              gdescproducto = rsta.rdoColumns("FR32DESPRODUCTO").Value
            Else
              gdescproducto = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDAD").Value) Then
             cantidad = 0
            Else
             cantidad = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDAD").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Then
             strDosis = 0
            Else
             strDosis = objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
             strUM = 0
            Else
             strUM = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
             strFF = 0
            Else
             strFF = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
          'producto diluyente : puede haber o no
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_dil = 2
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_dil = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
            gdescproducto_dil = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rstprod.rdoColumns("FR73VOLUMEN").Value) Or _
              IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              cantidad_dil = 0
            Else
              If rstprod.rdoColumns("FR73VOLUMEN").Value <> 0 Then
                cantidad_dil = Format(rsta.rdoColumns("FR32CANTIDADDIL").Value / _
                  rstprod.rdoColumns("FR73VOLUMEN").Value, "0.00")
                cantidad_dil = objGen.ReplaceStr(cantidad_dil, ",", ".", 1)
              Else
              cantidad_dil = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
             strDosis_dil = 0
            Else
             strDosis_dil = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDADDIL").Value, ",", ".", 1)
            End If
            strUM_dil = "ml"
            If IsNull(rstprod.rdoColumns("FRH7CODFORMFAR").Value) Then
             strFF_dil = ""
            Else
             strFF_dil = rstprod.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
          'producto_mezcla
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_mez = 3
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_mez = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
            gdescproducto_mez = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Or _
              IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
              cantidad_mez = 0
            Else
              If rsta.rdoColumns("FR32DOSIS_2").Value = 0 Then
                cantidad_mez = 0
              Else
                If Not IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
                  If rstprod.rdoColumns("FR73DOSIS").Value > 0 Then
                    cantidad_mez = Format(rsta.rdoColumns("FR32DOSIS_2").Value / _
                      rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
                    cantidad_mez = objGen.ReplaceStr(cantidad_mez, ",", ".", 1)
                  Else
                    cantidad_mez = Format(rsta.rdoColumns("FR32DOSIS_2").Value, "0.00")
                    cantidad_mez = objGen.ReplaceStr(cantidad_mez, ",", ".", 1)
                  End If
                Else
                  cantidad_mez = Format(rsta.rdoColumns("FR32DOSIS_2").Value, "0.00")
                  cantidad_mez = objGen.ReplaceStr(cantidad_mez, ",", ".", 1)
                End If
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Then
              strDosis_mez = 0
            Else
              strDosis_mez = objGen.ReplaceStr(rsta.rdoColumns("FR32DOSIS_2").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
              strUM_mez = 0
            Else
              strUM_mez = rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR_2").Value) Then
              strFF_mez = 0
            Else
              strFF_mez = rsta.rdoColumns("FRH7CODFORMFAR_2").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
      Case "F"
        'Electrolito
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_dil = 2
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_dil = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
            gdescproducto_dil = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rstprod.rdoColumns("FR73VOLUMEN").Value) Or _
              IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              cantidad_dil = 0
            Else
              If rstprod.rdoColumns("FR73VOLUMEN").Value <> 0 Then
                cantidad_dil = Format(rsta.rdoColumns("FR32CANTIDADDIL").Value / _
                rstprod.rdoColumns("FR73VOLUMEN").Value, "0.00")
                cantidad_dil = objGen.ReplaceStr(cantidad_dil, ",", ".", 1)
              Else
                cantidad_dil = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              strDosis_dil = 0
            Else
              strDosis_dil = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDADDIL").Value, ",", ".", 1)
            End If
              strUM_dil = "ml"
            If IsNull(rstprod.rdoColumns("FRH7CODFORMFAR").Value) Then
              strFF_dil = ""
            Else
              strFF_dil = rstprod.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        'Electrolito 2
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo = 1
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto = rsta.rdoColumns("FR73CODPRODUCTO").Value
            If rsta.rdoColumns("FR73CODPRODUCTO").Value = 999999999 Then
              If Not IsNull(rsta.rdoColumns("FR32DESPRODUCTO").Value) Then
                gdescproducto = rsta.rdoColumns("FR32DESPRODUCTO").Value
              Else
                gdescproducto = rstprod.rdoColumns("FR73DESPRODUCTO").Value
              End If
            Else
              gdescproducto = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Or _
              IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
              cantidad = 0
            Else
              If rstprod.rdoColumns("FR73DOSIS").Value = 0 Then
                cantidad = 0
              Else
                cantidad = Format(rsta.rdoColumns("FR28DOSIS").Value / _
                rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
                cantidad = objGen.ReplaceStr(cantidad, ",", ".", 1)
              End If
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Then
              strDosis = 0
            Else
              strDosis = objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
              strUM = 0
            Else
              strUM = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
              strFF = 0
            Else
              strFF = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        'Medicamento 2
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_mez = 3
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_mez = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
            If rsta.rdoColumns("FR73CODPRODUCTO").Value = 999999999 Then
              gdescproducto_mez = rsta.rdoColumns("FR32DESPRODUCTO").Value
            Else
              gdescproducto_mez = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS_2").Value) Or _
               IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
                   cantidad_mez = 0
            Else
              If rstprod.rdoColumns("FR73DOSIS").Value = 0 Then
              cantidad_mez = 0
              Else
              cantidad_mez = Format(rsta.rdoColumns("FR28DOSIS_2").Value / _
              rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
              cantidad_mez = objGen.ReplaceStr(cantidad_mez, ",", ".", 1)
              End If
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS_2").Value) Then
              strDosis_mez = 0
            Else
              strDosis_mez = objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS_2").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
              strUM_mez = 0
            Else
              strUM_mez = rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR_2").Value) Then
              strFF_mez = 0
            Else
             strFF_mez = rsta.rdoColumns("FRH7CODFORMFAR_2").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
      End Select
      'seg�n el c�digo de operaci�n se har�n 1,2 o 3 insert en FR0600 seg�n
      'haya medicamento,diluyente,producto_mezcla
      If IsNumeric(gcodproducto) Then
        strinsert06 = "INSERT INTO FR0600 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                      "FR73CODPRODUCTO,FR06DESPROD,FR06CANTIDAD,FR06INDCARGREA,FR06INDINCCAMA," & _
                      "FR06INDINCPACI,FR06INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                      "FR06FECCARGA,FR06HORACARGA,FR06FORMFAR,FR06DOSIS,FR06UNIMEDIDA," & _
                      "FR06GRUPO,FR06HORATOMA,FR06INDMEZCLA,FR06INDCAMBCAMA,FR06INDCITOSTATICO,FR06CODIGO)" & _
                      " VALUES (" & _
                      grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                      "'" & rsta("AD15CODCAMA").Value & "'" & "," & _
                      rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                      gcodproducto & "," & _
                      "'" & gdescproducto & "'" & "," & _
                      cantidad & "," & _
                      "NULL,NULL,NULL,NULL" & "," & _
                      rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                      numlinea & "," & _
                      "TRUNC(SYSDATE)" & "," & _
                      hora & ",'" & strFF & "'," & strDosis & ",'" & strUM & "'," & _
                      grupo & "," & _
                      objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                      "-1,0," & indMIVCito & _
                      ",FR06CODIGO_SEQUENCE.NEXTVAL)"
        objApp.rdoConnect.Execute strinsert06, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      If IsNumeric(gcodproducto_dil) Then
        If strFF_dil <> "" Then
          strinsert06 = "INSERT INTO FR0600 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                        "FR73CODPRODUCTO,FR06DESPROD,FR06CANTIDAD,FR06INDCARGREA,FR06INDINCCAMA," & _
                        "FR06INDINCPACI,FR06INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                        "FR06FECCARGA,FR06HORACARGA,FR06FORMFAR,FR06DOSIS,FR06UNIMEDIDA," & _
                        "FR06GRUPO,FR06HORATOMA,FR06INDMEZCLA,FR06INDCAMBCAMA,FR06INDCITOSTATICO,FR06CODIGO)" & _
                        " VALUES (" & _
                        grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                        "'" & rsta("AD15CODCAMA").Value & "'" & "," & _
                        rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                        gcodproducto_dil & "," & _
                        "'" & gdescproducto_dil & "'" & "," & _
                        cantidad_dil & "," & _
                        "NULL,NULL,NULL,NULL" & "," & _
                        rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                        numlinea & "," & _
                        "TRUNC(SYSDATE)" & "," & _
                        hora & ",'" & strFF_dil & "'," & strDosis_dil & ",'" & strUM_dil & "'," & _
                        grupo_dil & "," & _
                        objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                        "-1,0," & indMIVCito & _
                        ",FR06CODIGO_SEQUENCE.NEXTVAL)"
          objApp.rdoConnect.Execute strinsert06, 64
          objApp.rdoConnect.Execute "Commit", 64
        Else
          strinsert06 = "INSERT INTO FR0600 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                        "FR73CODPRODUCTO,FR06DESPROD,FR06CANTIDAD,FR06INDCARGREA,FR06INDINCCAMA," & _
                        "FR06INDINCPACI,FR06INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                        "FR06FECCARGA,FR06HORACARGA,FR06FORMFAR,FR06DOSIS,FR06UNIMEDIDA," & _
                        "FR06GRUPO,FR06HORATOMA,FR06INDMEZCLA,FR06INDCAMBCAMA,FR06INDCITOSTATICO,FR06CODIGO)" & _
                        " VALUES (" & _
                        grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                        "'" & rsta("AD15CODCAMA").Value & "'" & "," & _
                        rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                        gcodproducto_dil & "," & _
                        "'" & gdescproducto_dil & "'" & "," & _
                        cantidad_dil & "," & _
                        "NULL,NULL,NULL,NULL" & "," & _
                        rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                        numlinea & "," & _
                        "TRUNC(SYSDATE)" & "," & _
                        hora & ",NULL," & strDosis_dil & ",'" & strUM_dil & "'," & _
                        grupo_dil & "," & _
                        objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                        "-1,0," & indMIVCito & _
                        ",FR06CODIGO_SEQUENCE.NEXTVAL)"
          objApp.rdoConnect.Execute strinsert06, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      End If
      If IsNumeric(gcodproducto_mez) Then
        strinsert06 = "INSERT INTO FR0600 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                      "FR73CODPRODUCTO,FR06DESPROD,FR06CANTIDAD,FR06INDCARGREA,FR06INDINCCAMA," & _
                      "FR06INDINCPACI,FR06INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                      "FR06FECCARGA,FR06HORACARGA,FR06FORMFAR,FR06DOSIS,FR06UNIMEDIDA," & _
                      "FR06GRUPO,FR06HORATOMA,FR06INDMEZCLA,FR06INDCAMBCAMA,FR06INDCITOSTATICO,FR06CODIGO)" & _
                      " VALUES (" & _
                      grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                      "'" & rsta("AD15CODCAMA").Value & "'" & "," & _
                      rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                      gcodproducto_mez & "," & _
                      "'" & gdescproducto_mez & "'" & "," & _
                      cantidad_mez & "," & _
                      "NULL,NULL,NULL,NULL" & "," & _
                      rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                      numlinea & "," & _
                      "TRUNC(SYSDATE)" & "," & _
                      hora & ",'" & strFF_mez & "'," & strDosis_mez & ",'" & strUM_mez & "'," & _
                      grupo_mez & "," & _
                      objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                      "-1,0," & indMIVCito & _
                      ",FR06CODIGO_SEQUENCE.NEXTVAL)"
        objApp.rdoConnect.Execute strinsert06, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing

  Call fabricar_etiquetas_todas(grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk), hora)

  If Option2(0).Value Then 'MIV
    'se cambia el estado del carro (estado=2 Se ha comenzado la carga)
    qryupdEstCarMIV(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
    qryupdEstCarMIV(1) = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk) 'D�a
    qryupdEstCarMIV.Execute
  Else
    qryupdEstCarCIT(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
    qryupdEstCarCIT(1) = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk) 'D�a
    qryupdEstCarCIT.Execute
  End If

carro_siguiente:
Next mintisel


Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
objWinInfo.DataRefresh


Screen.MousePointer = vbDefault
Me.Enabled = True
cmdPrepararCarro.Enabled = True

  MsgBox "Preparaci�n Terminada", vbInformation, "Aviso"

End Sub

Private Sub fabricar_etiquetas_todas(carro, hora)
Dim rstmezcla As rdoResultset
Dim strmezcla As String
Dim strmedidadil As String
Dim curDosis2 As Currency
Dim curCantidad2 As Currency
Dim auxProd2 As String
Dim strProd2 As String
Dim qryProd2 As rdoQuery
Dim rstProd2 As rdoResultset
Dim auxProd1 As String
Dim strProd1 As String
Dim qryProd1 As rdoQuery
Dim rstProd1 As rdoResultset
Dim curDosis1 As Currency
Dim curCantidad1 As Currency
Dim vntAuxCantidad1 As Variant
Dim vntAuxCantidad2 As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim qryupdate As rdoQuery
Dim strlinea As String
Dim strpaciente As String
Dim qrypaciente As rdoQuery
Dim rstpaciente As rdoResultset
Dim strcama As String
Dim qrycama As rdoQuery
Dim rstcama As rdoResultset
Dim strdpto As String
Dim qrydpto As rdoQuery
Dim rstdpto As rdoResultset
Dim auxSolucion As String
Dim strSolucion As String
Dim qrySolucion As rdoQuery
Dim rstSolucion As rdoResultset
Dim strcaducidad As String
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim Auxfecha As String
Dim auxVia As String
Dim strVia As String
Dim qryVia As rdoQuery
Dim rstVia As rdoResultset
Dim strHoras As String
Dim strMinutos As String
Dim strHAdm As String
Dim strInstrucctiones As String
Dim blnEtiquetas As Boolean
Dim auxdpto As String
Dim Auxcama As String
Dim i  As Integer
Dim auxInstrucctiones As String
Dim auxVolTotal As String
Dim auxStr As String
Dim auxcant As String
Dim strFab As String
Dim rstFab As rdoResultset
Dim qryFab As rdoQuery
Dim crlf As String
Dim Incremento As Integer
Dim contLinea As Integer
Dim strListado As String
Dim auxStrListado As String

strFab = " SELECT DISTINCT"
strFab = strFab & " FR0600.FR07CODCARRO,"
strFab = strFab & " FR0600.FR06HORATOMA,"
strFab = strFab & " FR6600.FR66CODPETICION,"
strFab = strFab & " FR3200.FR28NUMLINEA,"
strFab = strFab & " AD1500.AD15CODCAMA,"
strFab = strFab & " FR6600.CI21CODPERSONA,"
strFab = strFab & " FR3200.FR32OPERACION,"
strFab = strFab & " FR3200.FR73CODPRODUCTO,"
strFab = strFab & " FR3200.FR93CODUNIMEDIDA,"
strFab = strFab & " FR3200.FR28DOSIS,"
strFab = strFab & " FR3200.FR32CANTIDAD,"
strFab = strFab & " FR3200.FR73CODPRODUCTO_2,"
strFab = strFab & " FR3200.FR32DOSIS_2,"
strFab = strFab & " FR3200.FR93CODUNIMEDIDA_2,"
strFab = strFab & " FR3200.FR73CODPRODUCTO_DIL,"
strFab = strFab & " FR3200.FR32TIEMMININF,"
strFab = strFab & " FR3200.FR32CANTIDADDIL,"
strFab = strFab & " FR3200.FR32VELPERFUSION,"
strFab = strFab & " FR3200.FR32VOLTOTAL,"
strFab = strFab & " FR3200.FR34CODVIA,"
strFab = strFab & " FR3200.FR32INSTRADMIN"
strFab = strFab & " FROM FR6600,FR3200,FR0600,AD1500"
strFab = strFab & " WHERE FR6600.FR66CODPETICION=FR3200.FR66CODPETICION"
strFab = strFab & " AND FR6600.FR66CODPETICION=FR0600.FR66CODPETICION"
strFab = strFab & " AND FR3200.FR66CODPETICION=FR0600.FR66CODPETICION"
strFab = strFab & " AND FR3200.FR28NUMLINEA=FR0600.FR28NUMLINEA"
strFab = strFab & " AND FR6600.AD01CODASISTENCI=AD1500.AD01CODASISTENCI"
strFab = strFab & " AND FR6600.AD07CODPROCESO=AD1500.AD07CODPROCESO"
strFab = strFab & " AND FR0600.FR06INDMEZCLA=?"
strFab = strFab & " AND FR0600.FR06FECCARGA=TRUNC(SYSDATE)"
strFab = strFab & " AND FR0600.FR06HORACARGA=?"
strFab = strFab & " AND FR0600.FR07CODCARRO=?"
If Option2(0) Then
    strFab = strFab & " AND (FR0600.FR06INDCITOSTATICO=0 OR FR0600.FR06INDCITOSTATICO IS NULL)"
Else
    strFab = strFab & " AND FR0600.FR06INDCITOSTATICO=-1"
End If

Set qryFab = objApp.rdoConnect.CreateQuery("", strFab)
qryFab(0) = -1
qryFab(1) = hora
qryFab(2) = carro
Set rstFab = qryFab.OpenResultset(strFab)
While rstFab.StillExecuting
Wend

mintNTotalSelRows = 0
Dim Listado() As String

While Not rstFab.EOF
  mintNTotalSelRows = mintNTotalSelRows + 1
  ReDim Preserve Listado(mintNTotalSelRows) As String

  Listado(mintNTotalSelRows) = ""
  auxSolucion = ""
  auxProd1 = ""
  auxProd2 = ""
  strmedidadil = "ml"
  strlinea = ""
  If Not IsNull(rstFab("FR66CODPETICION")) Then 'petici�n
    If Not IsNull(rstFab("FR73CODPRODUCTO")) Then
      If rstFab("FR32OPERACION") = "F" Then 'Cantidad es del diluyente
        strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
        qryProd1(0) = rstFab("FR73CODPRODUCTO")
        Set rstProd1 = qryProd1.OpenResultset(strProd1)
        While rstProd1.StillExecuting
        Wend
        If Not rstProd1.EOF Then
          If Not IsNull(rstProd1("FR73DOSIS")) Then
            curDosis1 = CCur(rstProd1("FR73DOSIS").Value)
          Else
            curDosis1 = 0
          End If
        Else
          '???
        End If
        If curDosis1 <> 0 Then
          If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
            auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
          Else
            auxStr = rstProd1("FR73DESPRODUCTO")
          End If
          auxStr = formatear(auxStr, 30, True, True)
          auxcant = formatear(vntAuxCantidad1 * curDosis1, 8, False, False)
          auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        Else
          If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
            auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
          Else
            auxStr = rstProd1("FR73DESPRODUCTO")
          End If
          auxStr = formatear(auxStr, 30, True, True)
          auxcant = formatear(Str(vntAuxCantidad1), 8, False, False)
          auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        End If
        If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
          strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
        Else
          strcaducidad = " 24 HORAS"
        End If
        rstProd1.Close
        qryProd1.Close
        If Not IsNull(rstFab("FR28DOSIS")) Then
          If curDosis1 <> 0 Then
            curCantidad1 = Format(CCur(rstFab("FR28DOSIS")) / curDosis1, "0.00")
            vntAuxCantidad1 = objGen.ReplaceStr(curCantidad1, ",", ".", 1)
          Else
            vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR28DOSIS"), ",", ".", 1)
          End If
        Else
          vntAuxCantidad1 = 0
        End If
      Else
        strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
        qryProd1(0) = rstFab("FR73CODPRODUCTO")
        Set rstProd1 = qryProd1.OpenResultset(strProd1)
        While rstProd1.StillExecuting
        Wend
        vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR32CANTIDAD"), ",", ".", 1)
        If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
          auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
        Else
          auxStr = rstProd1("FR73DESPRODUCTO")
        End If
        auxStr = formatear(auxStr, 30, True, True)
        auxcant = formatear(rstFab("FR28DOSIS"), 8, False, False)
        auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
          strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
        Else
          strcaducidad = " 24 HORAS"
        End If
        rstProd1.Close
        qryProd1.Close
      End If
    End If
    If Not IsNull(rstFab("FR73CODPRODUCTO_2")) Then
      strProd2 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
      Set qryProd2 = objApp.rdoConnect.CreateQuery("", strProd2)
      qryProd2(0) = rstFab("FR73CODPRODUCTO_2")
      Set rstProd2 = qryProd2.OpenResultset(strProd2)
      While rstProd2.StillExecuting
      Wend
      If InStr(rstProd2("FR73DESPRODUCTO"), " ") > 0 Then
        auxStr = Left$(rstProd2("FR73DESPRODUCTO"), InStr(rstProd2("FR73DESPRODUCTO"), " "))
      Else
        auxStr = rstProd2("FR73DESPRODUCTO")
      End If
      auxStr = formatear(auxStr, 30, True, True)
      auxcant = formatear(rstFab("FR32DOSIS_2"), 8, False, False)
      auxProd2 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA_2")
      If Not rstProd2.EOF Then
        If Not IsNull(rstProd2("FR73DOSIS")) Then
          curDosis2 = CCur(rstProd2("FR73DOSIS").Value)
        Else
          curDosis2 = 0
        End If
      Else
        '???
      End If
      rstProd2.Close
      qryProd2.Close
      If Not IsNull(rstFab("FR32DOSIS_2")) Then
        If curDosis2 <> 0 Then
          curCantidad2 = Format(CCur(rstFab("FR32DOSIS_2")) / curDosis2, "0.00")
          vntAuxCantidad2 = objGen.ReplaceStr(curCantidad2, ",", ".", 1)
        Else
          vntAuxCantidad2 = objGen.ReplaceStr(rstFab("FR32DOSIS_2"), ",", ".", 1)
        End If
      Else
        vntAuxCantidad2 = 0
      End If
    End If
    If Not IsNull(rstFab("FR73CODPRODUCTO_DIL")) Then
      strSolucion = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO= ? "
      Set qrySolucion = objApp.rdoConnect.CreateQuery("", strSolucion)
      qrySolucion(0) = rstFab("FR73CODPRODUCTO_DIL")
      Set rstSolucion = qrySolucion.OpenResultset(strSolucion)
      While rstSolucion.StillExecuting
      Wend
      If InStr(rstSolucion("FR73DESPRODUCTO"), " ") > 0 Then
        auxStr = Left$(rstSolucion("FR73DESPRODUCTO"), InStr(rstSolucion("FR73DESPRODUCTO"), " "))
      Else
        auxStr = rstSolucion("FR73DESPRODUCTO")
      End If
      auxStr = formatear(auxStr, 30, True, True)
      auxcant = formatear(rstFab("FR32CANTIDADDIL"), 8, False, False)
      auxSolucion = auxStr & "  " & auxcant & " " & strmedidadil
      rstSolucion.Close
      qrySolucion.Close
    End If
  End If
    
  strdpto = "SELECT AD02DESDPTO FROM AD0200,FR6600 WHERE AD0200.AD02CODDPTO=FR6600.AD02CODDPTO AND FR66CODPETICION = ? "
  Set qrydpto = objApp.rdoConnect.CreateQuery("", strdpto)
  qrydpto(0) = rstFab("FR66CODPETICION")
  Set rstdpto = qrydpto.OpenResultset(strdpto)
  While rstdpto.StillExecuting
  Wend
  If Not rstdpto.EOF Then
    auxdpto = rstdpto(0)
  Else
    auxdpto = ""
  End If
  rstdpto.Close
  qrydpto.Close
    
  strcama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD15CODCAMA= ? "
  Set qrycama = objApp.rdoConnect.CreateQuery("", strcama)
  qrycama(0) = rstFab("AD15CODCAMA")
  Set rstcama = qrycama.OpenResultset(strcama)
  While rstcama.StillExecuting
  Wend
  If Not rstcama.EOF Then
    Auxcama = rstcama(0)
  Else
    Auxcama = ""
  End If
  rstcama.Close
  qrycama.Close
  
  strpaciente = "SELECT CI2200.CI22PRIAPEL || ',' || CI2200.CI22SEGAPEL || ',' || CI2200.CI22NOMBRE,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA= ? "
  Set qrypaciente = objApp.rdoConnect.CreateQuery("", strpaciente)
  qrypaciente(0) = rstFab("CI21CODPERSONA")
  Set rstpaciente = qrypaciente.OpenResultset(strpaciente)
  While rstpaciente.StillExecuting
  Wend
  If Not rstpaciente.EOF Then
    If Not IsNull(rstpaciente(0)) Then
      strlinea = strlinea & formatear(rstpaciente(0), 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
    Else
      strlinea = strlinea & formatear("", 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
    End If
    If Not IsNull(rstpaciente(1)) Then
      strlinea = strlinea & formatear(rstpaciente(1), 30, True, True) & " " & Auxcama & Chr(13)
    Else
      strlinea = strlinea & formatear("", 30, True, True) & " " & Auxcama & Chr(13)
    End If
  End If
  rstpaciente.Close
  qrypaciente.Close
  
  strfecha = "SELECT TO_CHAR(SYSDATE,'DD-MM-YYYY') FROM DUAL"
  Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
  While rstfecha.StillExecuting
  Wend
  Auxfecha = rstfecha(0).Value
    
  strVia = "SELECT * FROM FR3400 WHERE FR34CODVIA= ? "
  Set qryVia = objApp.rdoConnect.CreateQuery("", strVia)
  qryVia(0) = rstFab("FR34CODVIA")
  Set rstVia = qryVia.OpenResultset(strVia)
  While rstVia.StillExecuting
  Wend
  If Not IsNull(rstVia("FR34DESVIA")) Then
    auxVia = rstVia("FR34DESVIA")
  Else
    auxVia = ""
  End If
  auxVia = formatear(auxVia, 15, True, True)
  rstVia.Close
  qryVia.Close
    
  If IsNumeric(rstFab("FR32TIEMMININF")) Then
    strHoras = Format(rstFab("FR32TIEMMININF") \ 60, "00") & "H"
    strMinutos = Format(rstFab("FR32TIEMMININF") Mod 60, "00") & "M"
  Else
    strHoras = ""
    strMinutos = ""
  End If
    
  strHAdm = Format(rstFab("FR06HORATOMA"), "00.00")
    
  If Not IsNull(rstFab("FR32INSTRADMIN")) Then
    strInstrucctiones = rstFab("FR32INSTRADMIN")
  Else
    strInstrucctiones = ""
  End If
  i = 0
  auxInstrucctiones = ""
  While strInstrucctiones <> "" And i <> 5
    If InStr(strInstrucctiones, Chr(13)) > 0 Then
      auxInstrucctiones = auxInstrucctiones & Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)))
      strInstrucctiones = Right$(strInstrucctiones, Len(strInstrucctiones) - InStr(strInstrucctiones, Chr(13)))
    Else
      auxInstrucctiones = auxInstrucctiones & strInstrucctiones
      strInstrucctiones = ""
    End If
    i = i + 1
  Wend
  auxVolTotal = "Vtotal: " & rstFab("FR32VOLTOTAL") & " ml" '11/02/2000
    
  strlinea = strlinea & Chr(13)
  strlinea = strlinea & "  " & auxProd1 & Chr(13)
  If Trim(auxProd2) <> "" Then
    strlinea = strlinea & "  " & auxProd2 & Chr(13)
    strlinea = strlinea & "  " & auxSolucion & Chr(13)
  Else
    strlinea = strlinea & "  " & auxSolucion & Chr(13)
    strlinea = strlinea & "  " & auxProd2 & Chr(13)
  End If
  strlinea = strlinea & Chr(13)
  strlinea = strlinea & "Adm.: " & objGen.ReplaceStr(strHAdm, ",", ":", 1) & "  " & auxVia & "  " & auxVolTotal & Chr(13)
  strlinea = strlinea & "Inf. " & strHoras & " " & strMinutos & " (" & rstFab("FR32VELPERFUSION") & "ML/H" & ")" & Chr(13)
  strlinea = strlinea & "Prep. " & Auxfecha & "     Cad." & strcaducidad & Chr(13)
  strlinea = strlinea & auxInstrucctiones

  Listado(mintNTotalSelRows) = strlinea
  
  rstFab.MoveNext
Wend
rstFab.Close
Set rstFab = Nothing
qryFab.Close
Set qryFab = Nothing

'''''''''''''''''''''''''''''''''''''''''''''''''''''
blnEtiquetas = False
If blnImpresoraSeleccionada = False Then
  If mintNTotalSelRows > 0 Then
    Screen.MousePointer = vbDefault
    Load frmFabPrinter
    Call frmFabPrinter.Show(vbModal)
    Screen.MousePointer = vbHourglass
  End If
End If

If blnImpresoraSeleccionada = True Then
  blnEtiquetas = True
End If

If blnEtiquetas = True Then
  For mintisel = 0 To mintNTotalSelRows - 1
    crlf = Chr$(13) & Chr$(10)
    Incremento = 30
    contLinea = 0
    
    strlinea = crlf
    strlinea = strlinea & "N" & crlf
    strlinea = strlinea & "I8,1,034" & crlf
    strlinea = strlinea & "Q599,24" & crlf
    strlinea = strlinea & "R0,0" & crlf
    strlinea = strlinea & "S2" & crlf
    strlinea = strlinea & "D5" & crlf
    strlinea = strlinea & "ZB" & crlf
    
    strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1
    
    strlinea = strlinea & Texto_Etiqueta("CLINICA UNIVERSITARIA. Servicio Farmacia", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1
    
    strlinea = strlinea & "LE0," & (contLinea * Incremento) + 8 & ",740,8" & crlf
    contLinea = contLinea + 1
    strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1

    strListado = Listado(mintisel + 1)
    auxStrListado = ""
    While strListado <> ""
      If InStr(strListado, Chr(13)) > 0 Then
        auxStrListado = Left$(strListado, InStr(strListado, Chr(13)) - 1)
        strListado = Right$(strListado, Len(strListado) - InStr(strListado, Chr(13)))
        strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
        contLinea = contLinea + 1
      Else
        If Chr(10) <> strListado Then
          auxStrListado = strListado
          strListado = ""
          strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          strListado = ""
        End If
      End If
    Wend
    strlinea = strlinea & "P1" & crlf
    strlinea = strlinea & " " & crlf
    
    Printer.Print strlinea
    Printer.EndDoc
  Next mintisel
End If

End Sub

Private Sub fabricar_etiquetas(carro, hora)
Dim rstmezcla As rdoResultset
Dim strmezcla As String
Dim strmedidadil As String
Dim curDosis2 As Currency
Dim curCantidad2 As Currency
Dim auxProd2 As String
Dim strProd2 As String
Dim qryProd2 As rdoQuery
Dim rstProd2 As rdoResultset
Dim auxProd1 As String
Dim strProd1 As String
Dim qryProd1 As rdoQuery
Dim rstProd1 As rdoResultset
Dim curDosis1 As Currency
Dim curCantidad1 As Currency
Dim vntAuxCantidad1 As Variant
Dim vntAuxCantidad2 As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim qryupdate As rdoQuery
Dim strlinea As String
Dim strpaciente As String
Dim qrypaciente As rdoQuery
Dim rstpaciente As rdoResultset
Dim strcama As String
Dim qrycama As rdoQuery
Dim rstcama As rdoResultset
Dim strdpto As String
Dim qrydpto As rdoQuery
Dim rstdpto As rdoResultset
Dim auxSolucion As String
Dim strSolucion As String
Dim qrySolucion As rdoQuery
Dim rstSolucion As rdoResultset
Dim strcaducidad As String
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim Auxfecha As String
Dim auxVia As String
Dim strVia As String
Dim qryVia As rdoQuery
Dim rstVia As rdoResultset
Dim strHoras As String
Dim strMinutos As String
Dim strHAdm As String
Dim strInstrucctiones As String
Dim blnEtiquetas As Boolean
Dim auxdpto As String
Dim Auxcama As String
Dim i  As Integer
Dim auxInstrucctiones As String
Dim auxVolTotal As String
Dim auxStr As String
Dim auxcant As String
Dim strFab As String
Dim rstFab As rdoResultset
Dim qryFab As rdoQuery
Dim crlf As String
Dim Incremento As Integer
Dim contLinea As Integer
Dim strListado As String
Dim auxStrListado As String
Dim peticionlinea As String
Dim strparejas As Variant
Dim intNumLin As Integer
Dim contador As Integer
Dim mvarBkmrk As Variant

intNumLin = grdDBGrid1(1).SelBookmarks.Count
strparejas = "("
For contador = 0 To intNumLin - 1
  mvarBkmrk = grdDBGrid1(1).SelBookmarks(contador)
  peticionlinea = "(FR0600.FR66CODPETICION=" & grdDBGrid1(1).Columns(12).CellValue(mvarBkmrk) & _
             " AND FR0600.FR28NUMLINEA=" & grdDBGrid1(1).Columns(13).CellValue(mvarBkmrk) & ")"
  If contador = intNumLin - 1 Then
    strparejas = strparejas & peticionlinea
  Else
    strparejas = strparejas & peticionlinea & " OR "
  End If
Next contador
strparejas = strparejas & ")"

strFab = " SELECT DISTINCT"
strFab = strFab & " FR0600.FR07CODCARRO,"
strFab = strFab & " FR0600.FR06HORATOMA,"
strFab = strFab & " FR6600.FR66CODPETICION,"
strFab = strFab & " FR3200.FR28NUMLINEA,"
strFab = strFab & " AD1500.AD15CODCAMA,"
strFab = strFab & " FR6600.CI21CODPERSONA,"
strFab = strFab & " FR3200.FR32OPERACION,"
strFab = strFab & " FR3200.FR73CODPRODUCTO,"
strFab = strFab & " FR3200.FR93CODUNIMEDIDA,"
strFab = strFab & " FR3200.FR28DOSIS,"
strFab = strFab & " FR3200.FR32CANTIDAD,"
strFab = strFab & " FR3200.FR73CODPRODUCTO_2,"
strFab = strFab & " FR3200.FR32DOSIS_2,"
strFab = strFab & " FR3200.FR93CODUNIMEDIDA_2,"
strFab = strFab & " FR3200.FR73CODPRODUCTO_DIL,"
strFab = strFab & " FR3200.FR32TIEMMININF,"
strFab = strFab & " FR3200.FR32CANTIDADDIL,"
strFab = strFab & " FR3200.FR32VELPERFUSION,"
strFab = strFab & " FR3200.FR32VOLTOTAL,"
strFab = strFab & " FR3200.FR34CODVIA,"
strFab = strFab & " FR3200.FR32INSTRADMIN"
strFab = strFab & " FROM FR6600,FR3200,FR0600,AD1500"
strFab = strFab & " WHERE FR6600.FR66CODPETICION=FR3200.FR66CODPETICION"
strFab = strFab & " AND FR6600.FR66CODPETICION=FR0600.FR66CODPETICION"
strFab = strFab & " AND FR3200.FR66CODPETICION=FR0600.FR66CODPETICION"
strFab = strFab & " AND FR3200.FR28NUMLINEA=FR0600.FR28NUMLINEA"
strFab = strFab & " AND FR6600.AD01CODASISTENCI=AD1500.AD01CODASISTENCI"
strFab = strFab & " AND FR6600.AD07CODPROCESO=AD1500.AD07CODPROCESO"
strFab = strFab & " AND FR0600.FR06INDMEZCLA=?"
strFab = strFab & " AND FR0600.FR06FECCARGA=TRUNC(SYSDATE)"
strFab = strFab & " AND FR0600.FR06HORACARGA=?"
strFab = strFab & " AND FR0600.FR07CODCARRO=?"
If Option2(0) Then
    strFab = strFab & " AND (FR0600.FR06INDCITOSTATICO=0 OR FR0600.FR06INDCITOSTATICO IS NULL)"
Else
    strFab = strFab & " AND FR0600.FR06INDCITOSTATICO=-1"
End If
If strparejas <> "" Then
  strFab = strFab & " AND " & strparejas
End If

Set qryFab = objApp.rdoConnect.CreateQuery("", strFab)
qryFab(0) = -1
qryFab(1) = hora
qryFab(2) = carro
Set rstFab = qryFab.OpenResultset(strFab)
While rstFab.StillExecuting
Wend

mintNTotalSelRows = 0
Dim Listado() As String

While Not rstFab.EOF
  mintNTotalSelRows = mintNTotalSelRows + 1
  ReDim Preserve Listado(mintNTotalSelRows) As String

  Listado(mintNTotalSelRows) = ""
  auxSolucion = ""
  auxProd1 = ""
  auxProd2 = ""
  strmedidadil = "ml"
  strlinea = ""
  If Not IsNull(rstFab("FR66CODPETICION")) Then 'petici�n
    If Not IsNull(rstFab("FR73CODPRODUCTO")) Then
      If rstFab("FR32OPERACION") = "F" Then 'Cantidad es del diluyente
        strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
        qryProd1(0) = rstFab("FR73CODPRODUCTO")
        Set rstProd1 = qryProd1.OpenResultset(strProd1)
        While rstProd1.StillExecuting
        Wend
        If Not rstProd1.EOF Then
          If Not IsNull(rstProd1("FR73DOSIS")) Then
            curDosis1 = CCur(rstProd1("FR73DOSIS").Value)
          Else
            curDosis1 = 0
          End If
        Else
          '???
        End If
        If curDosis1 <> 0 Then
          If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
            auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
          Else
            auxStr = rstProd1("FR73DESPRODUCTO")
          End If
          auxStr = formatear(auxStr, 30, True, True)
          auxcant = formatear(vntAuxCantidad1 * curDosis1, 8, False, False)
          auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        Else
          If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
            auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
          Else
            auxStr = rstProd1("FR73DESPRODUCTO")
          End If
          auxStr = formatear(auxStr, 30, True, True)
          auxcant = formatear(Str(vntAuxCantidad1), 8, False, False)
          auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        End If
        If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
          strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
        Else
          strcaducidad = " 24 HORAS"
        End If
        rstProd1.Close
        qryProd1.Close
        If Not IsNull(rstFab("FR28DOSIS")) Then
          If curDosis1 <> 0 Then
            curCantidad1 = Format(CCur(rstFab("FR28DOSIS")) / curDosis1, "0.00")
            vntAuxCantidad1 = objGen.ReplaceStr(curCantidad1, ",", ".", 1)
          Else
            vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR28DOSIS"), ",", ".", 1)
          End If
        Else
          vntAuxCantidad1 = 0
        End If
      Else
        strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
        qryProd1(0) = rstFab("FR73CODPRODUCTO")
        Set rstProd1 = qryProd1.OpenResultset(strProd1)
        While rstProd1.StillExecuting
        Wend
        
        vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR32CANTIDAD"), ",", ".", 1)
        If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
          auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
        Else
          auxStr = rstProd1("FR73DESPRODUCTO")
        End If
        auxStr = formatear(auxStr, 30, True, True)
        auxcant = formatear(rstFab("FR28DOSIS"), 8, False, False)
        auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
          strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
        Else
          strcaducidad = " 24 HORAS"
        End If
        rstProd1.Close
        qryProd1.Close
      End If
    End If
    If Not IsNull(rstFab("FR73CODPRODUCTO_2")) Then
      strProd2 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
      Set qryProd2 = objApp.rdoConnect.CreateQuery("", strProd2)
      qryProd2(0) = rstFab("FR73CODPRODUCTO_2")
      Set rstProd2 = qryProd2.OpenResultset(strProd2)
      While rstProd2.StillExecuting
      Wend
      
      If InStr(rstProd2("FR73DESPRODUCTO"), " ") > 0 Then
        auxStr = Left$(rstProd2("FR73DESPRODUCTO"), InStr(rstProd2("FR73DESPRODUCTO"), " "))
      Else
        auxStr = rstProd2("FR73DESPRODUCTO")
      End If
      auxStr = formatear(auxStr, 30, True, True)
      auxcant = formatear(rstFab("FR32DOSIS_2"), 8, False, False)
      auxProd2 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA_2")
      If Not rstProd2.EOF Then
        If Not IsNull(rstProd2("FR73DOSIS")) Then
          curDosis2 = CCur(rstProd2("FR73DOSIS").Value)
        Else
          curDosis2 = 0
        End If
      Else
        '???
      End If
      rstProd2.Close
      qryProd2.Close
      If Not IsNull(rstFab("FR32DOSIS_2")) Then
        If curDosis2 <> 0 Then
          curCantidad2 = Format(CCur(rstFab("FR32DOSIS_2")) / curDosis2, "0.00")
          vntAuxCantidad2 = objGen.ReplaceStr(curCantidad2, ",", ".", 1)
        Else
          vntAuxCantidad2 = objGen.ReplaceStr(rstFab("FR32DOSIS_2"), ",", ".", 1)
        End If
      Else
        vntAuxCantidad2 = 0
      End If
    End If
    If Not IsNull(rstFab("FR73CODPRODUCTO_DIL")) Then
      strSolucion = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO= ? "
      Set qrySolucion = objApp.rdoConnect.CreateQuery("", strSolucion)
      qrySolucion(0) = rstFab("FR73CODPRODUCTO_DIL")
      Set rstSolucion = qrySolucion.OpenResultset(strSolucion)
      While rstSolucion.StillExecuting
      Wend
      
      If InStr(rstSolucion("FR73DESPRODUCTO"), " ") > 0 Then
        auxStr = Left$(rstSolucion("FR73DESPRODUCTO"), InStr(rstSolucion("FR73DESPRODUCTO"), " "))
      Else
        auxStr = rstSolucion("FR73DESPRODUCTO")
      End If
      auxStr = formatear(auxStr, 30, True, True)
      auxcant = formatear(rstFab("FR32CANTIDADDIL"), 8, False, False)
      auxSolucion = auxStr & "  " & auxcant & " " & strmedidadil
      rstSolucion.Close
      qrySolucion.Close
    End If
  End If
    
  strdpto = "SELECT AD02DESDPTO FROM AD0200,FR6600 WHERE AD0200.AD02CODDPTO=FR6600.AD02CODDPTO AND FR66CODPETICION = ? "
  Set qrydpto = objApp.rdoConnect.CreateQuery("", strdpto)
  qrydpto(0) = rstFab("FR66CODPETICION")
  Set rstdpto = qrydpto.OpenResultset(strdpto)
  While rstdpto.StillExecuting
  Wend
  If Not rstdpto.EOF Then
    auxdpto = rstdpto(0)
  Else
    auxdpto = ""
  End If
  rstdpto.Close
  qrydpto.Close
  
  strcama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD15CODCAMA= ? "
  Set qrycama = objApp.rdoConnect.CreateQuery("", strcama)
  qrycama(0) = rstFab("AD15CODCAMA")
  Set rstcama = qrycama.OpenResultset(strcama)
  While rstcama.StillExecuting
  Wend
  If Not rstcama.EOF Then
    Auxcama = rstcama(0)
  Else
    Auxcama = ""
  End If
  rstcama.Close
  qrycama.Close

  strpaciente = "SELECT CI2200.CI22PRIAPEL || ',' || CI2200.CI22SEGAPEL || ',' || CI2200.CI22NOMBRE,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA= ? "
  Set qrypaciente = objApp.rdoConnect.CreateQuery("", strpaciente)
  qrypaciente(0) = rstFab("CI21CODPERSONA")
  Set rstpaciente = qrypaciente.OpenResultset(strpaciente)
  While rstpaciente.StillExecuting
  Wend
  If Not rstpaciente.EOF Then
    If Not IsNull(rstpaciente(0)) Then
      strlinea = strlinea & formatear(rstpaciente(0), 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
    Else
      strlinea = strlinea & formatear("", 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
    End If
    If Not IsNull(rstpaciente(1)) Then
      strlinea = strlinea & formatear(rstpaciente(1), 30, True, True) & " " & Auxcama & Chr(13)
    Else
      strlinea = strlinea & formatear("", 30, True, True) & " " & Auxcama & Chr(13)
    End If
  End If
  rstpaciente.Close
  qrypaciente.Close
  
  strfecha = "SELECT TO_CHAR(SYSDATE,'DD-MM-YYYY') FROM DUAL"
  Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
  While rstfecha.StillExecuting
  Wend
  
  Auxfecha = rstfecha(0).Value
  
  strVia = "SELECT * FROM FR3400 WHERE FR34CODVIA= ? "
  Set qryVia = objApp.rdoConnect.CreateQuery("", strVia)
  qryVia(0) = rstFab("FR34CODVIA")
  Set rstVia = qryVia.OpenResultset(strVia)
  While rstVia.StillExecuting
  Wend
  If Not IsNull(rstVia("FR34DESVIA")) Then
    auxVia = rstVia("FR34DESVIA")
  Else
    auxVia = ""
  End If
  auxVia = formatear(auxVia, 15, True, True)
  rstVia.Close
  qryVia.Close
  
  If IsNumeric(rstFab("FR32TIEMMININF")) Then
    strHoras = Format(rstFab("FR32TIEMMININF") \ 60, "00") & "H"
    strMinutos = Format(rstFab("FR32TIEMMININF") Mod 60, "00") & "M"
  Else
    strHoras = ""
    strMinutos = ""
  End If
  strHAdm = Format(rstFab("FR06HORATOMA"), "00.00")
    
  If Not IsNull(rstFab("FR32INSTRADMIN")) Then
    strInstrucctiones = rstFab("FR32INSTRADMIN")
  Else
    strInstrucctiones = ""
  End If
  i = 0
  auxInstrucctiones = ""
  While strInstrucctiones <> "" And i <> 5
    If InStr(strInstrucctiones, Chr(13)) > 0 Then
      auxInstrucctiones = auxInstrucctiones & Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)))
      strInstrucctiones = Right$(strInstrucctiones, Len(strInstrucctiones) - InStr(strInstrucctiones, Chr(13)))
    Else
      auxInstrucctiones = auxInstrucctiones & strInstrucctiones
      strInstrucctiones = ""
    End If
    i = i + 1
  Wend

  auxVolTotal = "Vtotal: " & rstFab("FR32VOLTOTAL") & " ml" '11/02/2000
  
  strlinea = strlinea & Chr(13)
  strlinea = strlinea & "  " & auxProd1 & Chr(13)
  If Trim(auxProd2) <> "" Then
    strlinea = strlinea & "  " & auxProd2 & Chr(13)
    strlinea = strlinea & "  " & auxSolucion & Chr(13)
  Else
    strlinea = strlinea & "  " & auxSolucion & Chr(13)
    strlinea = strlinea & "  " & auxProd2 & Chr(13)
  End If
  strlinea = strlinea & Chr(13)
  strlinea = strlinea & "Adm.: " & objGen.ReplaceStr(strHAdm, ",", ":", 1) & "  " & auxVia & "  " & auxVolTotal & Chr(13)
  strlinea = strlinea & "Inf. " & strHoras & " " & strMinutos & " (" & rstFab("FR32VELPERFUSION") & "ML/H" & ")" & Chr(13)
  strlinea = strlinea & "Prep. " & Auxfecha & "     Cad." & strcaducidad & Chr(13)
  strlinea = strlinea & auxInstrucctiones

  Listado(mintNTotalSelRows) = strlinea
  
  rstFab.MoveNext
Wend
rstFab.Close
Set rstFab = Nothing
qryFab.Close
Set qryFab = Nothing

'''''''''''''''''''''''''''''''''''''''''''''''''''''
blnEtiquetas = False
If blnImpresoraSeleccionada = False Then
  If mintNTotalSelRows > 0 Then
    'Screen.MousePointer = vbDefault
    Load frmFabPrinter
    Call frmFabPrinter.Show(vbModal)
    'Screen.MousePointer = vbHourglass
  End If
End If

If blnImpresoraSeleccionada = True Then
  blnEtiquetas = True
End If

If blnEtiquetas = True Then
  For mintisel = 0 To mintNTotalSelRows - 1
    crlf = Chr$(13) & Chr$(10)
    Incremento = 30
    contLinea = 0
    
    strlinea = crlf
    strlinea = strlinea & "N" & crlf
    strlinea = strlinea & "I8,1,034" & crlf
    strlinea = strlinea & "Q599,24" & crlf
    strlinea = strlinea & "R0,0" & crlf
    strlinea = strlinea & "S2" & crlf
    strlinea = strlinea & "D5" & crlf
    strlinea = strlinea & "ZB" & crlf
    
    strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1
    
    strlinea = strlinea & Texto_Etiqueta("CLINICA UNIVERSITARIA. Servicio Farmacia", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1
    
    strlinea = strlinea & "LE0," & (contLinea * Incremento) + 8 & ",740,8" & crlf
    contLinea = contLinea + 1
    strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1

    strListado = Listado(mintisel + 1)
    auxStrListado = ""
    While strListado <> ""
      If InStr(strListado, Chr(13)) > 0 Then
        auxStrListado = Left$(strListado, InStr(strListado, Chr(13)) - 1)
        strListado = Right$(strListado, Len(strListado) - InStr(strListado, Chr(13)))
        strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
        contLinea = contLinea + 1
      Else
        If Chr(10) <> strListado Then
          auxStrListado = strListado
          strListado = ""
          strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          strListado = ""
        End If
      End If
    Wend
    
    strlinea = strlinea & "P1" & crlf
    strlinea = strlinea & " " & crlf
    
    Printer.Print strlinea
    Printer.EndDoc
    
  Next mintisel
End If

End Sub


