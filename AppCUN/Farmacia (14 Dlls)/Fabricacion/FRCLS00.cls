VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

Const PRWinCrtlFabMez                 As String = "FR0602"
Const PRWinCrtlFabFM                  As String = "FR0605"
Const PRWinOMPRN                      As String = "FR0610"
Const PRWinPerfilFTP                  As String = "FR0611"
Const PRWinReenvasado                 As String = "FR0603"
Const PRWinBusMed                     As String = "FR0604"
Const PRWinVerPteFab                  As String = "FR0600"
Const PRWinProcFab                    As String = "FR0601"
Const PRWinMedFM                      As String = "FR0606"
Const PRWinVerCargaCarroMez           As String = "FR0607"
Const PRWinFabKit                     As String = "FR0612"
Const FRRepCrtlFabMez                 As String = "FR6021"
Const FRRepCrtlFabCit                 As String = "FR6022"
Const FRRepFabKit                     As String = "FR6121"
Const PRWinAnalizDifMez               As String = "FR0609"
Const PRWinCrtlInciDifMez             As String = "FR0608"
Const FRRepDifMezCarro                As String = "FR6091"
Const PRWinRecCarroMez                As String = "FR0613"
Const FRRepRecCarroMez                As String = "FR6131"
Const FRRepRecCarroCit                As String = "FR6132"
Const PRWinSitCarMez                  As String = "FR0614"
Const PRWinFirmarPeticionesFM         As String = "FR0616"
Const FRRepDifMezCarCit               As String = "FR6092"
Const PRWinMovimientosAlmacen         As String = "FR0617"
Const PRWinBusProductosAlmacen        As String = "FR0618"
Const PRWinBusGruposProductosAlmacen  As String = "FR0619"

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
  
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False
  
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    
    Case PRWinCrtlFabMez
      Load FrmCrtlFabMez
      'Call objsecurity.AddHelpContext(528)
      Call FrmCrtlFabMez.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmCrtlFabMez
      Set FrmCrtlFabMez = Nothing
    Case PRWinOMPRN
      Load frmOMModAsigFM
      'Call objsecurity.AddHelpContext(528)
      Call frmOMModAsigFM.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmOMModAsigFM
      Set frmOMModAsigFM = Nothing
    Case PRWinPerfilFTP
      Load frmPerfilFTP
      'Call objsecurity.AddHelpContext(528)
      Call frmPerfilFTP.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmPerfilFTP
      Set frmPerfilFTP = Nothing
    Case PRWinReenvasado
      Load FrmReenvasado
      'Call objsecurity.AddHelpContext(528)
      Call FrmReenvasado.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmReenvasado
      Set FrmReenvasado = Nothing
    Case PRWinBusMed
      Load FrmBusMed
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusMed.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmBusMed
      Set FrmBusMed = Nothing
    Case PRWinVerPteFab
      Load FrmVerPteFab
      'Call objsecurity.AddHelpContext(528)
      Call FrmVerPteFab.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmVerPteFab
      Set FrmVerPteFab = Nothing
    Case PRWinProcFab
      Load FrmProcFab
      'Call objsecurity.AddHelpContext(528)
      Call FrmProcFab.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmProcFab
      Set FrmProcFab = Nothing
    Case PRWinCrtlFabFM
      Load FrmCrtlFabFM
      'Call objsecurity.AddHelpContext(528)
      Call FrmCrtlFabFM.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmCrtlFabFM
      Set FrmCrtlFabFM = Nothing
    Case PRWinMedFM
      Load frmOMModDispFM
      'Call objsecurity.AddHelpContext(528)
      Call frmOMModDispFM.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmOMModDispFM
      Set frmOMModDispFM = Nothing
    Case PRWinVerCargaCarroMez
      Load frmVerCargaCarroMez
      'Call objsecurity.AddHelpContext(528)
      Call frmVerCargaCarroMez.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmVerCargaCarroMez
      Set frmVerCargaCarroMez = Nothing
    Case PRWinFabKit
      Load FrmFabKit
      'Call objsecurity.AddHelpContext(528)
      Call FrmFabKit.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmFabKit
      Set FrmFabKit = Nothing
    Case PRWinAnalizDifMez
      Load frmAnalizDifMez
      'Call objsecurity.AddHelpContext(528)
      Call frmAnalizDifMez.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmAnalizDifMez
      Set frmAnalizDifMez = Nothing
    Case PRWinCrtlInciDifMez
      Load frmCrtlInciDifMez
      'Call objsecurity.AddHelpContext(528)
      Call frmCrtlInciDifMez.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmCrtlInciDifMez
      Set frmCrtlInciDifMez = Nothing
  Case PRWinRecCarroMez
      Load frmRecCarroMez
      'Call objsecurity.AddHelpContext(528)
      Call frmRecCarroMez.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmRecCarroMez
      Set frmRecCarroMez = Nothing
  Case PRWinSitCarMez
      Load frmSitCarMez
      'Call objsecurity.AddHelpContext(528)
      Call frmSitCarMez.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmSitCarMez
      Set frmSitCarMez = Nothing
  Case PRWinFirmarPeticionesFM
      Load FrmFirmarPeticionesFM
      'Call objsecurity.AddHelpContext(528)
      Call FrmFirmarPeticionesFM.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmFirmarPeticionesFM
      Set FrmFirmarPeticionesFM = Nothing
    Case PRWinMovimientosAlmacen
      Load frmMovimientosAlmacen
      'Call objsecurity.AddHelpContext(528)
      Call frmMovimientosAlmacen.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmMovimientosAlmacen
      Set frmMovimientosAlmacen = Nothing
    Case PRWinBusProductosAlmacen
      Load frmBusProductosAlmacen
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProductosAlmacen.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmBusProductosAlmacen
      Set frmBusProductosAlmacen = Nothing
    Case PRWinBusGruposProductosAlmacen
      Load frmBusGruposProductosAlmacen
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGruposProductosAlmacen.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmBusGruposProductosAlmacen
      Set frmBusGruposProductosAlmacen = Nothing
      
      
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 36, 1 To 4) As Variant
      
  ' VENTANAS
  'MANTENIMIENTO
  
  aProcess(1, 1) = PRWinCrtlFabMez
  aProcess(1, 2) = "Controlar Fabricaci�n de Mezclas"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinOMPRN
  aProcess(2, 2) = "Ver/Asignar Peticiones F�rm.Mag."
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinPerfilFTP
  aProcess(3, 2) = "Ver Perfil FTP"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinReenvasado
  aProcess(4, 2) = "Reenvasado"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinBusMed
  aProcess(5, 2) = "Buscar Medicamentos"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = PRWinVerPteFab
  aProcess(6, 2) = "F�rmulas Magistrales Pendientes"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinProcFab
  aProcess(7, 2) = "Proceso de Fabricaci�n"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
 
  aProcess(8, 1) = PRWinCrtlFabFM
  aProcess(8, 2) = "Consultar Fabricaci�n de F�rm.Mag."
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinMedFM
  aProcess(9, 2) = "Ver/Dispensar Peticiones F�rm.Mag."
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinVerCargaCarroMez
  aProcess(10, 2) = "Peticiones de Mezclas en Carros"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow

  aProcess(11, 1) = PRWinFabKit
  aProcess(11, 2) = "Fabricaci�n de Kits"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow

  aProcess(12, 1) = FRRepCrtlFabMez
  aProcess(12, 2) = "Mezclas del Carro"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeReport

  aProcess(13, 1) = PRWinAnalizDifMez
  aProcess(13, 2) = "Analizar Diferencias Carro Mezclas"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow

  aProcess(14, 1) = PRWinCrtlInciDifMez
  aProcess(14, 2) = "Analizar Incidencias Carro Mezclas"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeWindow

  aProcess(15, 1) = FRRepFabKit
  aProcess(15, 2) = "Composici�n Kit"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeReport

  aProcess(16, 1) = FRRepDifMezCarro
  aProcess(16, 2) = "Diferencias del Carro, Mezclas"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeReport

  aProcess(17, 1) = PRWinRecCarroMez
  aProcess(17, 2) = "Salida de Carros, Mezclas"
  aProcess(17, 3) = True
  aProcess(17, 4) = cwTypeWindow
  
  aProcess(18, 1) = PRWinFirmarPeticionesFM
  aProcess(18, 2) = "Firmar Peticiones F�rmulas Magistrales"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow

  aProcess(19, 1) = FRRepRecCarroMez
  aProcess(19, 2) = "Dispensaci�n del Carro, Mezclas"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeReport
  
  aProcess(20, 1) = FRRepRecCarroCit
  aProcess(20, 2) = "Dispensaci�n del Carro, Citost�ticos"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeReport
  
  aProcess(21, 1) = FRRepCrtlFabCit
  aProcess(21, 2) = "Citost�ticos del Carro"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeReport

  aProcess(22, 1) = PRWinSitCarMez
  aProcess(22, 2) = "Situaci�n Carros Mezclas"
  aProcess(22, 3) = True
  aProcess(22, 4) = cwTypeWindow

  aProcess(23, 1) = FRRepDifMezCarCit
  aProcess(23, 2) = "Diferencias del Carro, Citost�ticos"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeReport
  
  aProcess(24, 1) = PRWinMovimientosAlmacen
  aProcess(24, 2) = "Movimientos Almac�n"
  aProcess(24, 3) = True
  aProcess(24, 4) = cwTypeWindow
  
  aProcess(25, 1) = PRWinBusProductosAlmacen
  aProcess(25, 2) = "Buscar Productos para Movimientos Almac�n"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeWindow
  
  aProcess(26, 1) = PRWinBusGruposProductosAlmacen
  aProcess(26, 2) = "Buscar Grupos de Productos para Movimientos Almac�n"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeWindow

End Sub


