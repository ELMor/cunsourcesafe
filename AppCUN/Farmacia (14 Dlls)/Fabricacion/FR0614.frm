VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmSitCarMez 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Situaci�n de los Carros Unidosis"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0614.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdAnulPrepCit 
      Caption         =   "Anular Preparaci�n Citost�ticos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   18
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton cmdAnulDifCit 
      Caption         =   "Anular Diferencias Citost�ticos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   17
      Top             =   6120
      Width           =   1215
   End
   Begin VB.CommandButton cmdAnulDif 
      Caption         =   "Anular Diferencias MIV"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   16
      Top             =   3480
      Width           =   1215
   End
   Begin VB.CommandButton cmdAnulPrep 
      Caption         =   "Anular Preparaci�n MIV"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   15
      Top             =   2400
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "Servicio :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   11655
      Begin Crystal.CrystalReport CrystalReport1 
         Left            =   8520
         Top             =   0
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin VB.CommandButton Command1 
         Caption         =   "FILTRAR"
         Height          =   495
         Left            =   9840
         TabIndex        =   14
         Top             =   480
         Width           =   1575
      End
      Begin VB.CheckBox chkservicio 
         Caption         =   "Todos los servicios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7320
         TabIndex        =   6
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "Desc.Servicio"
         Top             =   720
         Width           =   3405
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   240
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   720
         Width           =   1095
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1931
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo cboDia 
         Height          =   315
         Left            =   5040
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   720
         Width           =   975
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns(0).Width=   2831
         Columns(0).Caption=   "D�a"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo cboHora 
         Height          =   315
         Left            =   6120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   720
         Width           =   975
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns(0).Width=   2831
         Columns(0).Caption=   "Hora Salida"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hora Salida"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   6120
         TabIndex        =   12
         Top             =   480
         Width           =   1005
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "D�a"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   5040
         TabIndex        =   11
         Top             =   480
         Width           =   330
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   1440
         TabIndex        =   9
         Top             =   480
         Width           =   1020
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Cod.Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   240
         TabIndex        =   8
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Estado de los Carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6135
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   1800
      Width           =   10455
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5655
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   10170
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   17939
         _ExtentY        =   9975
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSitCarMez"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA   OOOO                                                *
'* NOMBRE: FR0614.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: ENERO 1999                                                    *
'* DESCRIPCION: situaci�n de los carros                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdAnulDif_Click()
Dim strCarros As String
Dim nTotal As Long
Dim nTotalSelRows As Integer
Dim i As Integer
Dim bkmrk As Variant
Dim diasalida As Integer
Dim diacarro As Integer
Dim strDelete As String
Dim strFechaDia As String
Dim rdoFechaDia As rdoResultset
Dim strUpdate As String
Dim intpreg As Integer

cmdAnulDif.Enabled = False
Me.Enabled = False

  intpreg = MsgBox("�Est� seguro de que desea ANULAR las Diferencias de los carros seleccionados?", vbQuestion + vbYesNo)
  If intpreg = vbNo Then
      cmdAnulDif.Enabled = True
      Me.Enabled = True
      Exit Sub
  End If
  
Screen.MousePointer = vbHourglass
  
  strFechaDia = "SELECT TO_NUMBER(TO_CHAR(SYSDATE,'D')) FROM DUAL"
  Set rdoFechaDia = objApp.rdoConnect.OpenResultset(strFechaDia)
  diasalida = rdoFechaDia(0).Value
  rdoFechaDia.Close
  Set rdoFechaDia = Nothing
  
  strCarros = ""
  nTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(1).SelBookmarks(i)
      If grdDBGrid1(1).Columns("Est.Carro MIV").CellValue(bkmrk) = 2 Or grdDBGrid1(1).Columns("Est.Carro MIV").CellValue(bkmrk) = 6 Then
        Select Case grdDBGrid1(1).Columns("D�a").CellValue(bkmrk)
          Case "LUN"
             diacarro = 1
          Case "MAR"
             diacarro = 2
          Case "MIE"
             diacarro = 3
          Case "JUE"
             diacarro = 4
          Case "VIE"
             diacarro = 5
          Case "SAB"
             diacarro = 6
          Case "DOM"
             diacarro = 7
        End Select
        If diacarro = diasalida Then
          strDelete = "DELETE FROM FR3300 WHERE "
          strDelete = strDelete & " FR33FECCARGA=TRUNC(SYSDATE) AND "
          strDelete = strDelete & " FR33HORACARGA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          strDelete = strDelete & " AND FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strDelete = strDelete & " AND FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=0"
          objApp.rdoConnect.Execute strDelete, 64
          
          strUpdate = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=2"
          strUpdate = strUpdate & " WHERE FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strUpdate = strUpdate & " AND FR74DIA='" & grdDBGrid1(1).Columns("D�a").CellValue(bkmrk) & "'"
          strUpdate = strUpdate & " AND FR74HORASALIDA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          objApp.rdoConnect.Execute strUpdate, 64
        Else
          strCarros = strCarros & "El carro: " & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & " no es de hoy." & Chr(13)
        End If
      Else
        strCarros = strCarros & "El carro: " & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & " no est� en estado 2-Preparado � 6-Analizado." & Chr(13)
      End If
    Next i
    If strCarros <> "" Then
      strCarros = Left$(strCarros, Len(strCarros) - 1)
    End If
  Else
    Me.Enabled = True
    cmdAnulDif.Enabled = True
    Screen.MousePointer = vbDefault
    MsgBox "Debe seleccionar el/los Carros para anular la Preparaci�n", vbCritical, "Aviso"
    Exit Sub
  End If
  
  Screen.MousePointer = vbDefault
  If strCarros = "" Then
    MsgBox "Se han ANULADO las Diferencias de todos los carros correctamente.", vbInformation, "Aviso"
  Else
    MsgBox "No Se han ANULADO las Diferencias de los siguientes carros." & Chr(13) & Chr(13) & strCarros, vbInformation, "Aviso"
  End If
  
Me.Enabled = True
cmdAnulDif.Enabled = True


End Sub

Private Sub cmdAnulDifCit_Click()
Dim strCarros As String
Dim nTotal As Long
Dim nTotalSelRows As Integer
Dim i As Integer
Dim bkmrk As Variant
Dim diasalida As Integer
Dim diacarro As Integer
Dim strDelete As String
Dim strFechaDia As String
Dim rdoFechaDia As rdoResultset
Dim strUpdate As String
Dim intpreg As Integer

cmdAnulDifCit.Enabled = False
Me.Enabled = False

  intpreg = MsgBox("�Est� seguro de que desea ANULAR las Diferencias de los carros seleccionados?", vbQuestion + vbYesNo)
  If intpreg = vbNo Then
      cmdAnulDifCit.Enabled = True
      Me.Enabled = True
      Exit Sub
  End If
  
Screen.MousePointer = vbHourglass
  
  strFechaDia = "SELECT TO_NUMBER(TO_CHAR(SYSDATE,'D')) FROM DUAL"
  Set rdoFechaDia = objApp.rdoConnect.OpenResultset(strFechaDia)
  diasalida = rdoFechaDia(0).Value
  rdoFechaDia.Close
  Set rdoFechaDia = Nothing
  
  strCarros = ""
  nTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(1).SelBookmarks(i)
      If grdDBGrid1(1).Columns("Est.Carro Cit.").CellValue(bkmrk) = 2 Or grdDBGrid1(1).Columns("Est.Carro Cit.").CellValue(bkmrk) = 6 Then
        Select Case grdDBGrid1(1).Columns("D�a").CellValue(bkmrk)
          Case "LUN"
             diacarro = 1
          Case "MAR"
             diacarro = 2
          Case "MIE"
             diacarro = 3
          Case "JUE"
             diacarro = 4
          Case "VIE"
             diacarro = 5
          Case "SAB"
             diacarro = 6
          Case "DOM"
             diacarro = 7
        End Select
        If diacarro = diasalida Then
          strDelete = "DELETE FROM FR3300 WHERE "
          strDelete = strDelete & " FR33FECCARGA=TRUNC(SYSDATE) AND "
          strDelete = strDelete & " FR33HORACARGA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          strDelete = strDelete & " AND FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strDelete = strDelete & " AND FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=-1"
          objApp.rdoConnect.Execute strDelete, 64
          
          strUpdate = "UPDATE FR7400 SET FR87CODESTCARRO_CIT=2"
          strUpdate = strUpdate & " WHERE FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strUpdate = strUpdate & " AND FR74DIA='" & grdDBGrid1(1).Columns("D�a").CellValue(bkmrk) & "'"
          strUpdate = strUpdate & " AND FR74HORASALIDA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          objApp.rdoConnect.Execute strUpdate, 64
        Else
          strCarros = strCarros & "El carro: " & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & " no es de hoy." & Chr(13)
        End If
      Else
        strCarros = strCarros & "El carro: " & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & " no est� en estado 2-Preparado � 6-Analizado." & Chr(13)
      End If
    Next i
    If strCarros <> "" Then
      strCarros = Left$(strCarros, Len(strCarros) - 1)
    End If
  Else
    Me.Enabled = True
    cmdAnulDifCit.Enabled = True
    Screen.MousePointer = vbDefault
    MsgBox "Debe seleccionar el/los Carros para anular la Preparaci�n", vbCritical, "Aviso"
    Exit Sub
  End If
  
  Screen.MousePointer = vbDefault
  If strCarros = "" Then
    MsgBox "Se han ANULADO las Diferencias de todos los carros correctamente.", vbInformation, "Aviso"
  Else
    MsgBox "No Se han ANULADO las Diferencias de los siguientes carros." & Chr(13) & Chr(13) & strCarros, vbInformation, "Aviso"
  End If
  
Me.Enabled = True
cmdAnulDifCit.Enabled = True

End Sub

Private Sub cmdAnulPrep_Click()
Dim strCarros As String
Dim nTotal As Long
Dim nTotalSelRows As Integer
Dim i As Integer
Dim bkmrk As Variant
Dim diasalida As Integer
Dim diacarro As Integer
Dim strDelete As String
Dim strFechaDia As String
Dim rdoFechaDia As rdoResultset
Dim strUpdate As String
Dim intpreg As Integer

cmdAnulPrep.Enabled = False
Me.Enabled = False

  intpreg = MsgBox("�Est� seguro de que desea ANULAR la Preparaci�n de los carros seleccionados?", vbQuestion + vbYesNo)
  If intpreg = vbNo Then
      cmdAnulPrep.Enabled = True
      Me.Enabled = True
      Exit Sub
  End If
  
Screen.MousePointer = vbHourglass
  
  strFechaDia = "SELECT TO_NUMBER(TO_CHAR(SYSDATE,'D')) FROM DUAL"
  Set rdoFechaDia = objApp.rdoConnect.OpenResultset(strFechaDia)
  diasalida = rdoFechaDia(0).Value
  
  strCarros = ""
  nTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(1).SelBookmarks(i)
      If grdDBGrid1(1).Columns("Est.Carro MIV").CellValue(bkmrk) = 1 Or grdDBGrid1(1).Columns("Est.Carro MIV").CellValue(bkmrk) = 2 Then
        Select Case grdDBGrid1(1).Columns("D�a").CellValue(bkmrk)
          Case "LUN"
             diacarro = 1
          Case "MAR"
             diacarro = 2
          Case "MIE"
             diacarro = 3
          Case "JUE"
             diacarro = 4
          Case "VIE"
             diacarro = 5
          Case "SAB"
             diacarro = 6
          Case "DOM"
             diacarro = 7
        End Select
        If diacarro = diasalida Then
          strDelete = "DELETE FROM FR0600 WHERE "
          strDelete = strDelete & " FR06FECCARGA=TRUNC(SYSDATE) AND "
          strDelete = strDelete & " FR06HORACARGA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          strDelete = strDelete & " AND FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strDelete = strDelete & " AND FR06INDMEZCLA=-1 AND FR06INDCITOSTATICO=0"
          objApp.rdoConnect.Execute strDelete, 64
          
          strDelete = "DELETE FROM FR3300 WHERE "
          strDelete = strDelete & " FR33FECCARGA=TRUNC(SYSDATE) AND "
          strDelete = strDelete & " FR33HORACARGA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          strDelete = strDelete & " AND FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strDelete = strDelete & " AND FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=0"
          objApp.rdoConnect.Execute strDelete, 64
          
          strUpdate = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=1"
          strUpdate = strUpdate & " WHERE FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strUpdate = strUpdate & " AND FR74DIA='" & grdDBGrid1(1).Columns("D�a").CellValue(bkmrk) & "'"
          strUpdate = strUpdate & " AND FR74HORASALIDA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          objApp.rdoConnect.Execute strUpdate, 64
        Else
          strCarros = strCarros & "El carro: " & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & " no es de hoy." & Chr(13)
        End If
      Else
        strCarros = strCarros & "El carro: " & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & " no est� en estado 1-Iniciado � 2-Preparado." & Chr(13)
      End If
    Next i
    If strCarros <> "" Then
      strCarros = Left$(strCarros, Len(strCarros) - 1)
    End If
  Else
    Me.Enabled = True
    cmdAnulPrep.Enabled = True
    Screen.MousePointer = vbDefault
    MsgBox "Debe seleccionar el/los Carros para anular la Preparaci�n", vbCritical, "Aviso"
    Exit Sub
  End If
  
  If strCarros = "" Then
    MsgBox "Se ha ANULADO la Preparaci�n de todos los carros correctamente.", vbInformation, "Aviso"
  Else
    MsgBox "No se han ANULADO los siguientes carros." & Chr(13) & Chr(13) & strCarros, vbInformation, "Aviso"
  End If
  
Me.Enabled = True
cmdAnulPrep.Enabled = True
Screen.MousePointer = vbDefault

End Sub

Private Sub cmdAnulPrepCit_Click()
Dim strCarros As String
Dim nTotal As Long
Dim nTotalSelRows As Integer
Dim i As Integer
Dim bkmrk As Variant
Dim diasalida As Integer
Dim diacarro As Integer
Dim strDelete As String
Dim strFechaDia As String
Dim rdoFechaDia As rdoResultset
Dim strUpdate As String
Dim intpreg As Integer

cmdAnulPrepCit.Enabled = False
Me.Enabled = False

  intpreg = MsgBox("�Est� seguro de que desea ANULAR la Preparaci�n de los carros seleccionados?", vbQuestion + vbYesNo)
  If intpreg = vbNo Then
      cmdAnulPrepCit.Enabled = True
      Me.Enabled = True
      Exit Sub
  End If
  
Screen.MousePointer = vbHourglass
  
  strFechaDia = "SELECT TO_NUMBER(TO_CHAR(SYSDATE,'D')) FROM DUAL"
  Set rdoFechaDia = objApp.rdoConnect.OpenResultset(strFechaDia)
  diasalida = rdoFechaDia(0).Value
  
  strCarros = ""
  nTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(1).SelBookmarks(i)
      If grdDBGrid1(1).Columns("Est.Carro Cit.").CellValue(bkmrk) = 1 Or grdDBGrid1(1).Columns("Est.Carro Cit.").CellValue(bkmrk) = 2 Then
        Select Case grdDBGrid1(1).Columns("D�a").CellValue(bkmrk)
          Case "LUN"
             diacarro = 1
          Case "MAR"
             diacarro = 2
          Case "MIE"
             diacarro = 3
          Case "JUE"
             diacarro = 4
          Case "VIE"
             diacarro = 5
          Case "SAB"
             diacarro = 6
          Case "DOM"
             diacarro = 7
        End Select
        If diacarro = diasalida Then
          strDelete = "DELETE FROM FR0600 WHERE "
          strDelete = strDelete & " FR06FECCARGA=TRUNC(SYSDATE) AND "
          strDelete = strDelete & " FR06HORACARGA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          strDelete = strDelete & " AND FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strDelete = strDelete & " AND FR06INDMEZCLA=-1 AND FR06INDCITOSTATICO=-1"
          objApp.rdoConnect.Execute strDelete, 64
          
          strDelete = "DELETE FROM FR3300 WHERE "
          strDelete = strDelete & " FR33FECCARGA=TRUNC(SYSDATE) AND "
          strDelete = strDelete & " FR33HORACARGA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          strDelete = strDelete & " AND FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strDelete = strDelete & " AND FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=-1"
          objApp.rdoConnect.Execute strDelete, 64
          
          strUpdate = "UPDATE FR7400 SET FR87CODESTCARRO_CIT=1"
          strUpdate = strUpdate & " WHERE FR07CODCARRO=" & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk)
          strUpdate = strUpdate & " AND FR74DIA='" & grdDBGrid1(1).Columns("D�a").CellValue(bkmrk) & "'"
          strUpdate = strUpdate & " AND FR74HORASALIDA=" & objGen.ReplaceStr(grdDBGrid1(1).Columns("H.Sal.").CellValue(bkmrk), ",", ".", 1)
          objApp.rdoConnect.Execute strUpdate, 64
        Else
          strCarros = strCarros & "El carro: " & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & " no es de hoy." & Chr(13)
        End If
      Else
        strCarros = strCarros & "El carro: " & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & " no est� en estado 1-Iniciado � 2-Preparado." & Chr(13)
      End If
    Next i
    If strCarros <> "" Then
      strCarros = Left$(strCarros, Len(strCarros) - 1)
    End If
  Else
    Me.Enabled = True
    cmdAnulPrepCit.Enabled = True
    Screen.MousePointer = vbDefault
    MsgBox "Debe seleccionar el/los Carros para anular la Preparaci�n", vbCritical, "Aviso"
    Exit Sub
  End If
  
  If strCarros = "" Then
    MsgBox "Se ha ANULADO la Preparaci�n de todos los carros correctamente.", vbInformation, "Aviso"
  Else
    MsgBox "No se han ANULADO los siguientes carros." & Chr(13) & Chr(13) & strCarros, vbInformation, "Aviso"
  End If
  
Me.Enabled = True
cmdAnulPrepCit.Enabled = True
Screen.MousePointer = vbDefault

End Sub

Private Sub Command1_Click()
    
    objWinInfo.objWinActiveForm.strWhere = " 1=1 "
    
    If chkservicio.Value = 0 Then
      If cboservicio.Text <> "" Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR07CODCARRO IN (SELECT FR07CODCARRO FROM FR0700 " & _
                                               "WHERE AD02CODDPTO=" & _
                                               cboservicio.Text & ") "
      End If
    Else
    End If
    
    If cboDia.Text <> "" Then
      objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR74DIA='" & cboDia.Text & "'"
    End If
    
    If cboHora.Text <> "" Then
      objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR74HORASALIDA='" & cboHora.Text & "'"
    End If
    
    objWinInfo.DataRefresh

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean

Call objWinInfo.FormPrinterDialog(True, "")
Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
intReport = objPrinter.Selected
If intReport > 0 Then
  Select Case intReport
    Case 1
      Call Imprimir("FR6021.RPT", 0, intReport)
    Case 2
      Call Imprimir("FR6022.RPT", 0, intReport)
    Case 3
      Call Imprimir("FR6091.RPT", 0, intReport)
    Case 4
      Call Imprimir("FR6092.RPT", 0, intReport)
    Case 5
      Call Imprimir("FR6131.RPT", 0, intReport)
    Case 6
      Call Imprimir("FR6132.RPT", 0, intReport)
  End Select
End If
Set objPrinter = Nothing

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cboservicio_CloseUp()
  txtServicio.Text = cboservicio.Columns(1).Value
  chkservicio.Value = 0
End Sub

Private Sub chkservicio_Click()
  cboservicio.Text = ""
  txtServicio.Text = ""
End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset
Dim strHora As String
Dim rstHora As rdoResultset
    
    cboservicio.RemoveAll
    stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
           "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND" & _
           " AD02CODDPTO IN (SELECT AD02CODDPTO FROM FR0700) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While (Not rsta.EOF)
        Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
    
    cboHora.RemoveAll
    strHora = "SELECT DISTINCT FR74HORASALIDA FROM FR7400 ORDER BY FR74HORASALIDA"
    Set rstHora = objApp.rdoConnect.OpenResultset(strHora)
    While (Not rstHora.EOF)
        Call cboHora.AddItem(rstHora.rdoColumns(0).Value)
        rstHora.MoveNext
    Wend
    rstHora.Close
    Set rstHora = Nothing
    
    
    cboDia.RemoveAll
    Call cboDia.AddItem("LUN")
    Call cboDia.AddItem("MAR")
    Call cboDia.AddItem("MIE")
    Call cboDia.AddItem("JUE")
    Call cboDia.AddItem("VIE")
    Call cboDia.AddItem("SAB")
    Call cboDia.AddItem("DOM")
    
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Situaci�n Carros"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "FR7400"
        
        .strWhere = "-1=0"
        .intAllowance = cwAllowModify
        
        .intCursorSize = 0
        Call .FormAddOrderField("FR07CODCARRO", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Situaci�n Carros")
        Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "C�digo Carro", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR74DIA", "D�a", cwString)
        Call .FormAddFilterWhere(strKey, "FR74HORASALIDA", "Hora Salida", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO_MIV", "C�d.Estado Carro MIV", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO_CIT", "C�d.Estado Carro Cit.", cwNumeric)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR74DIA", "D�a")
        Call .FormAddFilterOrder(strKey, "FR74HORASALIDA", "Hora Salida")
        Call .FormAddFilterOrder(strKey, "FR07CODCARRO", "C�digo Carro")
        
        Call .objPrinter.Add("FR6021", "Preparaci�n Mezclas del Carro")
        Call .objPrinter.Add("FR6022", "Preparaci�n Citost�ticos del Carro")
        Call .objPrinter.Add("FR6091", "Diferencias del Carro, Mezclas")
        Call .objPrinter.Add("FR6092", "Diferencias del Carro, Citost�ticos")
        Call .objPrinter.Add("FR6131", "Dispensaci�n de Mezclas del Carro")
        Call .objPrinter.Add("FR6132", "Dispensaci�n de Citost�ticos del Carro")
        
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo Carro", "FR07CODCARRO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Descripci�n Carro", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "D�a", "FR74DIA", cwString, 3)
        Call .GridAddColumn(objMultiInfo, "H.Sal.", "FR74HORASALIDA", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "Est.Carro MIV", "FR87CODESTCARRO_MIV", cwNumeric, 2)
        Call .GridAddColumn(objMultiInfo, "Desc.Estado Carro MIV", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Est.Carro Cit.", "FR87CODESTCARRO_CIT", cwNumeric, 2)
        Call .GridAddColumn(objMultiInfo, "Desc.Estado Carro Citostaticos", "", cwString, 30)
        
  
        Call .FormCreateInfo(objMultiInfo)
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
        
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnForeign = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnForeign = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), "FR07CODCARRO", "SELECT * FROM FR0700 WHERE FR07CODCARRO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), grdDBGrid1(1).Columns(4), "FR07DESCARRO")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), "FR87CODESTCARRO_MIV", "SELECT * FROM FR8700 WHERE FR87CODESTCARRO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), grdDBGrid1(1).Columns(8), "FR87DESESTCARRO")
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnForeign = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnMandatory = True
   
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), "FR87CODESTCARRO_CIT", "SELECT * FROM FR8700 WHERE FR87CODESTCARRO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(10), "FR87DESESTCARRO")
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnForeign = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnMandatory = True
   
        Call .WinRegister
        Call .WinStabilize
    End With
    grdDBGrid1(1).Columns("C�digo Carro").Width = 700
    grdDBGrid1(1).Columns("Descripci�n Carro").Width = 2000
    grdDBGrid1(1).Columns("D�a").Width = 600
    grdDBGrid1(1).Columns("H.Sal.").Width = 600
    grdDBGrid1(1).Columns("Est.Carro MIV").Width = 1250
    grdDBGrid1(1).Columns("Desc.Estado Carro MIV").Width = 2400
    grdDBGrid1(1).Columns("Est.Carro Cit.").Width = 1250
    grdDBGrid1(1).Columns("Desc.Estado Carro Citostaticos").Width = 2400

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

Dim objField As clsCWFieldSearch

 
 If strCtrl = "grdDBGrid1(1).Est.Carro MIV" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR8700"

     Set objField = .AddField("FR87CODESTCARRO")
     objField.strSmallDesc = "C�digo Estado Carro"

     Set objField = .AddField("FR87DESESTCARRO")
     objField.strSmallDesc = "Descripci�n Estado Carro"

     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(7), .cllValues("FR87CODESTCARRO"))
      'objWinInfo.objWinActiveForm.blnChanged = True
      Call objWinInfo.CtrlDataChange
     End If
   End With
   Set objSearch = Nothing
 End If

 If strCtrl = "grdDBGrid1(1).Est.Carro Cit." Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR8700"

     Set objField = .AddField("FR87CODESTCARRO")
     objField.strSmallDesc = "C�digo Estado Carro"

     Set objField = .AddField("FR87DESESTCARRO")
     objField.strSmallDesc = "Descripci�n Estado Carro"

     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(9), .cllValues("FR87CODESTCARRO"))
      'objWinInfo.objWinActiveForm.blnChanged = True
      Call objWinInfo.CtrlDataChange
     End If
   End With
   Set objSearch = Nothing
 End If


End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


Private Sub Imprimir(strListado As String, intDes As Integer, intReport As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strCarros As String
  Dim nTotal As Long
  Dim nTotalSelRows As Integer
  Dim i As Integer
  Dim bkmrk As Variant
  Dim diasalida As Integer

  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  strCarros = ""
  nTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(1).SelBookmarks(i)
      strCarros = strCarros & grdDBGrid1(1).Columns("C�digo Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grdDBGrid1(1).Columns("C�digo Carro").Value
  End If
  strWhere = ""
  
  
  Select Case grdDBGrid1(1).Columns("D�a").Value
    Case "LUN"
       diasalida = 1
    Case "MAR"
       diasalida = 2
    Case "MIE"
       diasalida = 3
    Case "JUE"
       diasalida = 4
    Case "VIE"
       diasalida = 5
    Case "SAB"
       diasalida = 6
    Case "DOM"
       diasalida = 7
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  Select Case intReport
  Case 1 '"FR6021.RPT"
    strWhere = ""
    strWhere = strWhere & " {FR0608J.HORACAR} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    strWhere = strWhere & " AND {FR0608J.HORASAL} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    strWhere = strWhere & " AND " & CrearWhere("{FR0608J.CODCARRO}", "(" & strCarros & ")")
    strWhere = strWhere & " AND {FR0608J.FR06INDCITOSTATICO}=0"
  Case 2 '"FR6022.RPT"
    strWhere = ""
    strWhere = strWhere & " {FR0608J.HORACAR} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    strWhere = strWhere & " AND {FR0608J.HORASAL} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    strWhere = strWhere & " AND " & CrearWhere("{FR0608J.CODCARRO}", "(" & strCarros & ")")
    strWhere = strWhere & " AND {FR0608J.FR06INDCITOSTATICO}=-1"
  Case 3 '"FR6091.RPT"
      strWhere = ""
      strWhere = strWhere & " {FR3304J.HORACAR} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
      strWhere = strWhere & " AND {FR3304J.HORASAL} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
      strWhere = strWhere & " AND " & CrearWhere("{FR3304J.CODCARRO}", "(" & strCarros & ")")
      strWhere = strWhere & " AND {FR3304J.FR33INDCITOSTATICO}=0"
  Case 4 '"FR6092.RPT"
      strWhere = ""
      strWhere = strWhere & " {FR3304J.HORACAR} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
      strWhere = strWhere & " AND {FR3304J.HORASAL} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
      strWhere = strWhere & " AND " & CrearWhere("{FR3304J.CODCARRO}", "(" & strCarros & ")")
      strWhere = strWhere & " AND {FR3304J.FR33INDCITOSTATICO}=-1"
  Case 5 '"FR6131.RPT"
    strWhere = ""
    strWhere = strWhere & " {FR3304J.HORACAR} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    strWhere = strWhere & " AND {FR3304J.HORASAL} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    strWhere = strWhere & " AND " & CrearWhere("{FR3304J.CODCARRO}", "(" & strCarros & ")")
    strWhere = strWhere & " AND {FR3304J.FR33INDCITOSTATICO}=0"
  Case 6 '"FR6132.RPT"
    strWhere = ""
    strWhere = strWhere & " {FR3304J.HORACAR} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    strWhere = strWhere & " AND {FR3304J.HORASAL} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    strWhere = strWhere & " AND " & CrearWhere("{FR3304J.CODCARRO}", "(" & strCarros & ")")
    strWhere = strWhere & " AND {FR3304J.FR33INDCITOSTATICO}=-1"
  End Select
             
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1
Err_imp1:
  
End Sub


Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function

