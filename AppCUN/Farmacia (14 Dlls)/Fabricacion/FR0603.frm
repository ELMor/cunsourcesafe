VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmReenvasado 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Fabricaci�n. Reenvasado"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0603.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdproducto 
      Caption         =   "Producto"
      Height          =   375
      Left            =   4080
      TabIndex        =   43
      Top             =   7680
      Width           =   1575
   End
   Begin VB.CommandButton cmdterminar 
      Caption         =   "Terminar"
      Height          =   375
      Left            =   6240
      TabIndex        =   35
      Top             =   7680
      Width           =   1575
   End
   Begin VB.CommandButton cmdreenvasar 
      Caption         =   "Reenvasar"
      Height          =   375
      Left            =   6240
      TabIndex        =   30
      Top             =   7680
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Ver "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   855
      Left            =   120
      TabIndex        =   27
      Top             =   600
      Width           =   6255
      Begin VB.OptionButton Option1 
         Caption         =   "Reenvasado Iniciado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   3480
         TabIndex        =   29
         Top             =   360
         Width           =   2415
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pendientes de Reenvasar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   28
         Top             =   360
         Value           =   -1  'True
         Width           =   2655
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Reenvasados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5895
      Index           =   0
      Left            =   120
      TabIndex        =   23
      Top             =   1560
      Width           =   11685
      Begin TabDlg.SSTab tabTab1 
         Height          =   5220
         Index           =   0
         Left            =   120
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   9208
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0603.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(9)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(7)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(11)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(12)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(15)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(16)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(17)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "dtcDateCombo1(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(6)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(5)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(1)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(0)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(8)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(9)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(10)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(11)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(12)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(13)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(2)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(3)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(4)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(7)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(14)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(15)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(16)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(17)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(18)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(19)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtText1(20)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(21)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(22)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).ControlCount=   39
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0603.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   22
            Left            =   8520
            TabIndex        =   48
            Tag             =   "UM Envase"
            Top             =   3000
            Visible         =   0   'False
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRI9INDESTFAB"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   8640
            MaxLength       =   10
            TabIndex        =   46
            Tag             =   "Estado"
            Top             =   4200
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO_ENV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   20
            Left            =   120
            Locked          =   -1  'True
            MaxLength       =   10
            TabIndex        =   16
            Tag             =   "Prod Envase"
            Top             =   3000
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO_TRF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   19
            Left            =   120
            Locked          =   -1  'True
            MaxLength       =   10
            TabIndex        =   8
            Tag             =   "Prod Transformar"
            Top             =   2280
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   18
            Left            =   120
            MaxLength       =   10
            TabIndex        =   0
            Tag             =   "Prod Final"
            Top             =   1560
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CANTENV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   17
            Left            =   7200
            MaxLength       =   10
            TabIndex        =   20
            Tag             =   "Cantidad Envase"
            Top             =   3000
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   16
            Left            =   1320
            TabIndex        =   17
            Tag             =   "C�digo Departamento|Departamento"
            Top             =   3000
            Width           =   780
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   15
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "C�d.Seg. Prod.Envase"
            Top             =   3000
            Width           =   195
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Desc. Prod.Envase"
            Top             =   3000
            Width           =   4740
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRI9CANTTRF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   9840
            MaxLength       =   10
            TabIndex        =   15
            Tag             =   "Cantidad Transformar"
            Top             =   2280
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   4
            Left            =   7200
            TabIndex        =   12
            Tag             =   "FF Transformar"
            Top             =   2280
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   3
            Left            =   7800
            TabIndex        =   13
            Tag             =   "Dosis Transformar"
            Top             =   2280
            Width           =   1350
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   2
            Left            =   9240
            TabIndex        =   14
            Tag             =   "UM Transformar"
            Top             =   2280
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRI9CANTFABRICAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   9840
            MaxLength       =   10
            TabIndex        =   7
            Tag             =   "Cantidad Final"
            Top             =   1560
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Desc. Prod.Transformar"
            Top             =   2280
            Width           =   4740
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "C�d.Seg. Prod.Transformar"
            Top             =   2280
            Width           =   195
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   10
            Left            =   1320
            TabIndex        =   9
            Tag             =   "C�d.Interno Prod.Transformar"
            Top             =   2280
            Width           =   780
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   9
            Left            =   9240
            TabIndex        =   6
            Tag             =   "UM Final"
            Top             =   1560
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   8
            Left            =   7800
            TabIndex        =   5
            Tag             =   "Dosis Final"
            Top             =   1560
            Width           =   1350
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   7200
            TabIndex        =   4
            Tag             =   "FF Final"
            Top             =   1560
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   1320
            TabIndex        =   1
            Tag             =   "C�d.Interno Prod.Final"
            Top             =   1560
            Width           =   780
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "C�d.Seg. Prod.Final"
            Top             =   1560
            Width           =   195
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   6
            Left            =   2400
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Desc. Prod.Final"
            Top             =   1560
            Width           =   4740
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4905
            Index           =   0
            Left            =   -74910
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   120
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19076
            _ExtentY        =   8652
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRI9FECFAB"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   44
            Tag             =   "Fecha Fabricaci�n"
            Top             =   720
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "UM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   8520
            TabIndex        =   49
            Top             =   2760
            Visible         =   0   'False
            Width           =   300
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   8640
            TabIndex        =   47
            Top             =   3960
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   45
            Top             =   480
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cantidad E."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   7200
            TabIndex        =   42
            Top             =   2760
            Width           =   1005
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Envase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   120
            TabIndex        =   41
            Top             =   2760
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cantidad P.T."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   9840
            TabIndex        =   40
            Top             =   2040
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cantidad P.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   9840
            TabIndex        =   39
            Top             =   1320
            Width           =   1170
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "FF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   7200
            TabIndex        =   38
            Top             =   2040
            Width           =   225
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   7800
            TabIndex        =   37
            Top             =   2040
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "UM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   9240
            TabIndex        =   36
            Top             =   2040
            Width           =   300
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto a Transformar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   34
            Top             =   2040
            Width           =   2010
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "UM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   9240
            TabIndex        =   33
            Top             =   1320
            Width           =   300
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   7800
            TabIndex        =   32
            Top             =   1320
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "FF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   7200
            TabIndex        =   31
            Top             =   1320
            Width           =   225
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto Final"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   25
            Top             =   1320
            Width           =   1245
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   24
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmReenvasado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmReenvasado(FR0602.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: MAYO DE 1999                                                  *
'* DESCRIPCION: Reenvasado                                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim intultimofoco As Integer


Private Sub cmdproducto_Click()
Dim rsta As rdoResultset
Dim stra As String
Dim intultimo As Integer

cmdproducto.Enabled = False

If intultimofoco = 18 Or intultimofoco = 19 Or intultimofoco = 20 Then
    intultimo = intultimofoco
    Call objsecurity.LaunchProcess("FR0604")
    If gintprodbuscado <> "" Then
        stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintprodbuscado
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If Not rsta.EOF Then
            If intultimo = 18 Then
                txtText1(18).Text = rsta.rdoColumns("FR73CODPRODUCTO").Value
                txtText1(1).Text = rsta.rdoColumns("FR73CODINTFAR").Value
                txtText1(5).Text = rsta.rdoColumns("FR73CODINTFARSEG").Value
                txtText1(6).Text = rsta.rdoColumns("FR73DESPRODUCTO").Value
                If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
                    txtText1(0).Text = rsta.rdoColumns("FRH7CODFORMFAR").Value
                End If
                If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
                    txtText1(8).Text = rsta.rdoColumns("FR73DOSIS").Value
                End If
                If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                    txtText1(9).Text = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
                End If
            End If
            If intultimo = 19 Then
                txtText1(19).Text = rsta.rdoColumns("FR73CODPRODUCTO").Value
                txtText1(10).Text = rsta.rdoColumns("FR73CODINTFAR").Value
                txtText1(11).Text = rsta.rdoColumns("FR73CODINTFARSEG").Value
                txtText1(12).Text = rsta.rdoColumns("FR73DESPRODUCTO").Value
                If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
                    txtText1(4).Text = rsta.rdoColumns("FRH7CODFORMFAR").Value
                End If
                If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
                    txtText1(3).Text = rsta.rdoColumns("FR73DOSIS").Value
                End If
                If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                    txtText1(2).Text = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
                End If
            End If
            If intultimo = 20 Then
                txtText1(20).Text = rsta.rdoColumns("FR73CODPRODUCTO").Value
                txtText1(16).Text = rsta.rdoColumns("FR73CODINTFAR").Value
                txtText1(15).Text = rsta.rdoColumns("FR73CODINTFARSEG").Value
                txtText1(14).Text = rsta.rdoColumns("FR73DESPRODUCTO").Value
            End If
        End If
    End If
txtText1(intultimo).SetFocus
End If
cmdproducto.Enabled = True
End Sub


Private Sub cmdreenvasar_Click()
Dim strUpdate As String
Dim strori As String
Dim rstori As rdoResultset
Dim strdes As String
Dim rstdes As rdoResultset
Dim strprod As String
Dim rstprod As rdoResultset
Dim str80 As String
Dim rst80 As rdoResultset
Dim str35 As String
Dim rst35 As rdoResultset
Dim strinsert As String
Dim fecha As Date

cmdreenvasar.Enabled = False

If txtText1(18).Text <> "" Then
    'se cambia el estado
    fecha = dtcDateCombo1(0).Text
    strUpdate = "UPDATE FRI900 SET FRI9INDESTFAB=1 WHERE " & _
                "FR73CODPRODUCTO=" & txtText1(18).Text & _
                " AND " & _
                "FRI9FECFAB=TO_DATE('" & fecha & "','DD/MM/YYYY')"
    objApp.rdoConnect.Execute strUpdate, 64
    objApp.rdoConnect.Execute "Commit", 64
    
    'se obtiene el almac�n de Farmacia
    strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
            "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
            "AND FRH2CODPARAMGEN=3"
    Set rstori = objApp.rdoConnect.OpenResultset(strori)
    'producto a transformar
    If txtText1(19).Text <> "" Then
        strprod = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(19).Text
        Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
        If rstprod.rdoColumns("FR73INDREECGAL").Value = -1 Then
        'el almac�n de Centro Gal�nico
            strdes = "SELECT FRH2PARAMGEN FROM FR0400 " & _
                     "WHERE FRH2CODPARAMGEN=16"
            Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        Else
        'el almac�n de Universidad
            strdes = "SELECT FRH2PARAMGEN FROM FRH200 " & _
                     "WHERE FRH2CODPARAMGEN=17"
            Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        End If
        'se registra la salida de Farmacia
        str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
        Set rst80 = objApp.rdoConnect.OpenResultset(str80)
        strinsert = "INSERT INTO FR8000(FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                   "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                   "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES (" & _
                   rst80.rdoColumns(0).Value & "," & _
                   rstori.rdoColumns(0).Value & "," & _
                   rstdes.rdoColumns(0).Value & "," & _
                   "8" & "," & _
                   "SYSDATE" & "," & _
                   "NULL" & "," & _
                   txtText1(19).Text & "," & _
                   txtText1(7).Text & "," & _
                   "1" & "," & _
                   "'" & txtText1(2).Text & "'" & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst80.Close
        Set rst80 = Nothing
        'se registra la entrada Gal�nico o Universidad desde Farmacia
        str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        Set rst35 = objApp.rdoConnect.OpenResultset(str80)
        strinsert = "INSERT INTO FR3500(FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                   "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI,FR73CODPRODUCTO," & _
                   "FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & _
                   rst35.rdoColumns(0).Value & "," & _
                   rstori.rdoColumns(0).Value & "," & _
                   rstdes.rdoColumns(0).Value & "," & _
                   "8" & "," & _
                   "SYSDATE" & "," & _
                   "NULL" & "," & _
                   txtText1(19).Text & "," & _
                   txtText1(7).Text & "," & _
                   "1" & "," & _
                   "'" & txtText1(2).Text & "'" & "," & _
                   "NULL" & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst35.Close
        Set rst35 = Nothing
        rstdes.Close
        Set rstdes = Nothing
     End If
     'producto envase
     If txtText1(20).Text <> "" Then
        strprod = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(20).Text
        Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
        If rstprod.rdoColumns("FR73INDREECGAL").Value = -1 Then
        'el almac�n de destino es Centro Gal�nico
            strdes = "SELECT FRH2PARAMGEN FROM FRH200 " & _
                     "WHERE FRH2CODPARAMGEN=16"
            Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        Else
        'el almac�n de destino es Universidad
            strdes = "SELECT FRH2PARAMGEN FROM FRH200 " & _
                     "WHERE FRH2CODPARAMGEN=17"
            Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        End If
        'se registra la salida de Farmacia
        str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
        Set rst80 = objApp.rdoConnect.OpenResultset(str80)
        strinsert = "INSERT INTO FR8000(FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                   "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                   "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES (" & _
                   rst80.rdoColumns(0).Value & "," & _
                   rstori.rdoColumns(0).Value & "," & _
                   rstdes.rdoColumns(0).Value & "," & _
                   "8" & "," & _
                   "SYSDATE" & "," & _
                   "NULL" & "," & _
                   txtText1(20).Text & "," & _
                   txtText1(17).Text & "," & _
                   "1" & "," & _
                   "'" & txtText1(22).Text & "'" & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst80.Close
        Set rst80 = Nothing
        'se registra la entrada Gal�nico o Universidad desde Farmacia
        str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        Set rst35 = objApp.rdoConnect.OpenResultset(str80)
        strinsert = "INSERT INTO FR3500(FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                   "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI,FR73CODPRODUCTO," & _
                   "FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & _
                   rst35.rdoColumns(0).Value & "," & _
                   rstori.rdoColumns(0).Value & "," & _
                   rstdes.rdoColumns(0).Value & "," & _
                   "8" & "," & _
                   "SYSDATE" & "," & _
                   "NULL" & "," & _
                   txtText1(20).Text & "," & _
                   txtText1(17).Text & "," & _
                   "1" & "," & _
                   "'" & txtText1(22).Text & "'" & "," & _
                   "NULL" & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst35.Close
        Set rst35 = Nothing
        rstdes.Close
        Set rstdes = Nothing
     End If
     rstori.Close
     Set rstori = Nothing
     
objWinInfo.DataRefresh
End If

cmdreenvasar.Enabled = True
End Sub

Private Sub cmdterminar_Click()
Dim strUpdate As String
Dim strori As String
Dim rstori As rdoResultset
Dim strdes As String
Dim rstdes As rdoResultset
Dim strprod As String
Dim rstprod As rdoResultset
Dim str80 As String
Dim rst80 As rdoResultset
Dim str35 As String
Dim rst35 As rdoResultset
Dim strinsert As String
Dim fecha As Date

cmdterminar.Enabled = False

If txtText1(18).Text <> "" Then
    'se cambia el estado
    fecha = dtcDateCombo1(0).Text
    strUpdate = "UPDATE FRI900 SET FRI9INDESTFAB=2 WHERE " & _
                "FR73CODPRODUCTO=" & txtText1(18).Text & _
                " AND " & _
                "FRI9FECFAB=TO_DATE('" & fecha & "','DD/MM/YYYY')"
    objApp.rdoConnect.Execute strUpdate, 64
    objApp.rdoConnect.Execute "Commit", 64
    
    'se obtiene el almac�n de Farmacia
    strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
            "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
            "AND FRH2CODPARAMGEN=3"
    Set rstori = objApp.rdoConnect.OpenResultset(strori)
    'producto a transformar
    If txtText1(19).Text <> "" Then
        strprod = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(19).Text
        Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
        If rstprod.rdoColumns("FR73INDREECGAL").Value = -1 Then
        'el almac�n de Centro Gal�nico
            strdes = "SELECT FRH2PARAMGEN FROM FRH200 " & _
                     "WHERE FRH2CODPARAMGEN=16"
            Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        Else
        'el almac�n de Universidad
            strdes = "SELECT FRH2PARAMGEN FROM FRH200 " & _
                     "WHERE FRH2CODPARAMGEN=17"
            Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        End If
        'se registra la salida de Gal�nico o Universidad a Farmacia
        str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
        Set rst80 = objApp.rdoConnect.OpenResultset(str80)
        strinsert = "INSERT INTO FR8000(FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                   "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                   "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES (" & _
                   rst80.rdoColumns(0).Value & "," & _
                   rstdes.rdoColumns(0).Value & "," & _
                   rstori.rdoColumns(0).Value & "," & _
                   "8" & "," & _
                   "SYSDATE" & "," & _
                   "NULL" & "," & _
                   txtText1(19).Text & "," & _
                   txtText1(7).Text & "," & _
                   "1" & "," & _
                   "'" & txtText1(2).Text & "'" & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst80.Close
        Set rst80 = Nothing
        'se registra la entrada a Farmacia desde Gal�nico o Universidad
        str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        Set rst35 = objApp.rdoConnect.OpenResultset(str35)
        strinsert = "INSERT INTO FR3500(FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                   "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI,FR73CODPRODUCTO," & _
                   "FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & _
                   rst35.rdoColumns(0).Value & "," & _
                   rstdes.rdoColumns(0).Value & "," & _
                   rstori.rdoColumns(0).Value & "," & _
                   "8" & "," & _
                   "SYSDATE" & "," & _
                   "NULL" & "," & _
                   txtText1(19).Text & "," & _
                   txtText1(7).Text & "," & _
                   "1" & "," & _
                   "'" & txtText1(2).Text & "'" & "," & _
                   "NULL" & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst35.Close
        Set rst35 = Nothing
        rstdes.Close
        Set rstdes = Nothing
     End If
     'producto envase
     If txtText1(20).Text <> "" Then
        strprod = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(20).Text
        Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
        If rstprod.rdoColumns("FR73INDREECGAL").Value = -1 Then
        'el almac�n de Centro Gal�nico
            strdes = "SELECT FR04CODALMACEN FROM FR0400 " & _
                     "WHERE FRH2CODPARAMGEN=16"
            Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        Else
        'el almac�n de Universidad
            strdes = "SELECT FRH2PARAMGEN FROM FRH200 " & _
                     "WHERE FRH2CODPARAMGEN=17"
            Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        End If
        'se registra la salida de Gal�nico o Universidad hacia Farmacia
        str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
        Set rst80 = objApp.rdoConnect.OpenResultset(str80)
        strinsert = "INSERT INTO FR8000(FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                   "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                   "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES (" & _
                   rst80.rdoColumns(0).Value & "," & _
                   rstdes.rdoColumns(0).Value & "," & _
                   rstori.rdoColumns(0).Value & "," & _
                   "8" & "," & _
                   "SYSDATE" & "," & _
                   "NULL" & "," & _
                   txtText1(20).Text & "," & _
                   txtText1(17).Text & "," & _
                   "1" & "," & _
                   "'" & txtText1(22).Text & "')"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst80.Close
        Set rst80 = Nothing
        'se registra la entrada a Farmacia desde Gal�nico o Universidad
        str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        Set rst35 = objApp.rdoConnect.OpenResultset(str35)
        strinsert = "INSERT INTO FR3500(FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                   "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI,FR73CODPRODUCTO," & _
                   "FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & _
                   rst35.rdoColumns(0).Value & "," & _
                   rstdes.rdoColumns(0).Value & "," & _
                   rstori.rdoColumns(0).Value & "," & _
                   "8" & "," & _
                   "SYSDATE" & "," & _
                   "NULL" & "," & _
                   txtText1(20).Text & "," & _
                   txtText1(17).Text & "," & _
                   "1" & "," & _
                   "'" & txtText1(22).Text & "'" & "," & _
                   "NULL" & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst35.Close
        Set rst35 = Nothing
        rstdes.Close
        Set rstdes = Nothing
     End If
     rstori.Close
     Set rstori = Nothing
     
objWinInfo.DataRefresh
End If

cmdterminar.Enabled = True
End Sub

Private Sub Form_Activate()
    cmdreenvasar.Visible = True
    cmdreenvasar.Enabled = True
    cmdterminar.Visible = False
    cmdterminar.Enabled = False
    
    ''para que pinte los campos en blanco ya que son ReadOnly
    'txtText1(19).BackColor = &HFFFFFF
    'txtText1(20).BackColor = &HFFFFFF
End Sub


Private Sub Option1_Click(Index As Integer)
  If Index = 0 Then
    cmdreenvasar.Visible = True
    cmdreenvasar.Enabled = True
    cmdterminar.Visible = False
    cmdterminar.Enabled = False
    objWinInfo.objWinActiveForm.strWhere = "FRI9INDESTFAB=0"
    objWinInfo.DataRefresh
  Else
    If Index = 1 Then
      cmdreenvasar.Visible = False
      cmdreenvasar.Enabled = False
      cmdterminar.Visible = True
      cmdterminar.Enabled = True
      objWinInfo.objWinActiveForm.strWhere = "FRI9INDESTFAB=1"
      objWinInfo.DataRefresh
    Else
      cmdreenvasar.Visible = False
      cmdreenvasar.Enabled = False
      cmdterminar.Visible = False
      cmdterminar.Enabled = False
    End If
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Reenvasado"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FRI900"
    .strWhere = "FRI9INDESTFAB=0"
    
    Call .FormAddOrderField("FRI9FECFAB", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Reenvasado")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d.Producto Final", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRI9CANTFABRICAR", "Cantidad Prod Final", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO_TRF", "C�d.Producto a Transformar", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRI9CANTTRF", "Cantidad Prod a Transformar", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO_ENV", "C�d.Producto Envase", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CANTENV", "Cantidad Prod Envase", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRI9FECFAB", "Fecha", cwDate)
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)


    .CtrlGetInfo(txtText1(18)).blnInFind = False
    .CtrlGetInfo(txtText1(13)).blnInFind = False
    .CtrlGetInfo(txtText1(19)).blnInFind = False
    .CtrlGetInfo(txtText1(7)).blnInFind = False
    .CtrlGetInfo(txtText1(20)).blnInFind = False
    .CtrlGetInfo(txtText1(17)).blnInFind = False
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = False

    .CtrlGetInfo(txtText1(21)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(19)).blnReadOnly = True
    .CtrlGetInfo(txtText1(20)).blnReadOnly = True

    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(18)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(18)), txtText1(1), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(18)), txtText1(5), "FR73CODINTFARSEG")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(18)), txtText1(6), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(18)), txtText1(0), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(18)), txtText1(8), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(18)), txtText1(9), "FR93CODUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(19)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(10), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(11), "FR73CODINTFARSEG")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(12), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(4), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(3), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(2), "FR93CODUNIMEDIDA")

    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(20)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(20)), txtText1(16), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(20)), txtText1(15), "FR73CODINTFARSEG")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(20)), txtText1(14), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(20)), txtText1(22), "FR93CODUNIMEDIDA")

   
    Call .WinRegister
    Call .WinStabilize
  End With
     
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Tipos de Interacci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim strfec As String
Dim rstfec As rdoResultset

If btnButton.Index = 2 And Option1(0).Value = True Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Call objWinInfo.CtrlSet(txtText1(21), 0)
    strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstfec.rdoColumns(0).Value)
    rstfec.Close
    Set rstfec = Nothing
Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim strfec As String
Dim rstfec As rdoResultset

If intIndex = 10 And Option1(0).Value = True Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    Call objWinInfo.CtrlSet(txtText1(21), 0)
    strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstfec.rdoColumns(0).Value)
Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lbllabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  'If intIndex = 18 Or intIndex = 19 Or intIndex = 20 Then
        intultimofoco = intIndex
   '     cmdproducto.Enabled = True
  'Else
  '      cmdproducto.Enabled = False
  'End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


