VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form FrmCrtlFabFM 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Consultar Fabricaci�n de F�rmulas Magistrales"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0605.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdOM 
      Caption         =   "Ver Petici�n"
      Height          =   375
      Left            =   3120
      TabIndex        =   120
      Top             =   7680
      Width           =   1575
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5280
      Left            =   120
      TabIndex        =   13
      Top             =   2280
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   9313
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   8388736
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Pacientes"
      TabPicture(0)   =   "FR0605.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Servicios"
      TabPicture(1)   =   "FR0605.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(2)"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFrame1 
         Caption         =   "F�rmulas Magistrales"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4815
         Index           =   2
         Left            =   -74880
         TabIndex        =   85
         Top             =   360
         Width           =   11325
         Begin TabDlg.SSTab tabTab1 
            Height          =   4380
            Index           =   1
            Left            =   120
            TabIndex        =   86
            TabStop         =   0   'False
            Top             =   360
            Width           =   11055
            _ExtentX        =   19500
            _ExtentY        =   7726
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            Tab             =   1
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0605.frx":0044
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "Frame3"
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0605.frx":0060
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "auxDBGrid1(6)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            Begin VB.Frame Frame3 
               BorderStyle     =   0  'None
               Caption         =   "Medicamentos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   3840
               Left            =   -74760
               TabIndex        =   87
               Top             =   120
               Width           =   8415
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR93CODUNIMEDIDA"
                  Height          =   330
                  Index           =   3
                  Left            =   6360
                  Locked          =   -1  'True
                  TabIndex        =   95
                  Tag             =   "U.M.P."
                  Top             =   600
                  Width           =   540
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR20DESFORMAG"
                  Height          =   330
                  Index           =   2
                  Left            =   0
                  Locked          =   -1  'True
                  TabIndex        =   94
                  Tag             =   "Form.Magistral"
                  Top             =   600
                  Width           =   5295
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR20PATHFORMAG"
                  Height          =   330
                  Index           =   1
                  Left            =   0
                  Locked          =   -1  'True
                  TabIndex        =   93
                  Tag             =   "Arch.Inf.F.M.|Archivo Inf. F�rmula Magistral"
                  Top             =   1680
                  Width           =   3765
               End
               Begin VB.CommandButton cmdWord2 
                  Caption         =   "W"
                  BeginProperty Font 
                     Name            =   "MS Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   350
                  Left            =   3960
                  TabIndex        =   92
                  Top             =   1680
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR20CANTNECESQUIR"
                  Height          =   330
                  Index           =   0
                  Left            =   5400
                  Locked          =   -1  'True
                  TabIndex        =   91
                  Tag             =   "Cantidad"
                  Top             =   600
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR55CODNECESUNID"
                  Height          =   330
                  Index           =   9
                  Left            =   0
                  Locked          =   -1  'True
                  TabIndex        =   90
                  Tag             =   "C�d. Necesidad"
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   1100
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR20NUMLINEA"
                  Height          =   330
                  Index           =   10
                  Left            =   1560
                  Locked          =   -1  'True
                  TabIndex        =   89
                  Tag             =   "Linea"
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   1100
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR73CODPRODUCTO"
                  Height          =   330
                  Index           =   11
                  Left            =   3240
                  Locked          =   -1  'True
                  TabIndex        =   88
                  Tag             =   "C�d.Producto"
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   1100
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "F�rmula Magistral"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   3
                  Left            =   0
                  TabIndex        =   101
                  Top             =   360
                  Width           =   1500
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Cantidad   preparar"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   1
                  Left            =   5400
                  TabIndex        =   100
                  Top             =   360
                  Width           =   1650
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Archivo Inf. F�rmula Magistral"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   0
                  Left            =   0
                  TabIndex        =   99
                  Top             =   1440
                  Width           =   2550
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "C�d. Necesidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   0
                  TabIndex        =   98
                  Top             =   2520
                  Visible         =   0   'False
                  Width           =   1815
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Linea"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   2040
                  TabIndex        =   97
                  Top             =   2520
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "C�d.Producto"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   3240
                  TabIndex        =   96
                  Top             =   2520
                  Visible         =   0   'False
                  Width           =   1815
               End
            End
            Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
               Height          =   4035
               Index           =   6
               Left            =   90
               TabIndex        =   102
               Top             =   120
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               stylesets.count =   6
               stylesets(0).Name=   "Estupefacientes"
               stylesets(0).BackColor=   14671839
               stylesets(0).Picture=   "FR0605.frx":007C
               stylesets(1).Name=   "FormMagistral"
               stylesets(1).BackColor=   8454016
               stylesets(1).Picture=   "FR0605.frx":0098
               stylesets(2).Name=   "Medicamentos"
               stylesets(2).BackColor=   16777215
               stylesets(2).Picture=   "FR0605.frx":00B4
               stylesets(3).Name=   "MezclaIV2M"
               stylesets(3).BackColor=   8454143
               stylesets(3).Picture=   "FR0605.frx":00D0
               stylesets(4).Name=   "Fluidoterapia"
               stylesets(4).BackColor=   16776960
               stylesets(4).Picture=   "FR0605.frx":00EC
               stylesets(5).Name=   "PRNenOM"
               stylesets(5).BackColor=   12615935
               stylesets(5).Picture=   "FR0605.frx":0108
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorEven   =   12632256
               BackColorOdd    =   12632256
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7117
               _StockProps     =   79
            End
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "F�rmulas Magistrales"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   4815
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   11325
         Begin TabDlg.SSTab tabTab1 
            Height          =   4380
            Index           =   0
            Left            =   120
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "      If txtText1(2).Text <> """" Then"
            Top             =   360
            Width           =   11055
            _ExtentX        =   19500
            _ExtentY        =   7726
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            Tab             =   1
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0605.frx":0124
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "Frame2"
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0605.frx":0140
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "auxDBGrid1(0)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            Begin VB.Frame Frame2 
               BorderStyle     =   0  'None
               Caption         =   "Medicamentos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   4080
               Left            =   -74640
               TabIndex        =   16
               Top             =   240
               Width           =   10095
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "fr66codpeticion"
                  Height          =   405
                  Index           =   34
                  Left            =   6240
                  TabIndex        =   49
                  Tag             =   "C�digo Petici�n"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "fr28numlinea"
                  Height          =   405
                  Index           =   45
                  Left            =   6480
                  TabIndex        =   48
                  Tag             =   "Num.Linea"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Cambiar V�a"
                  DataField       =   "FR28INDVIAOPC"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   7080
                  TabIndex        =   47
                  Tag             =   "Cambiar V�a?"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   1455
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "PRN"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   9120
                  TabIndex        =   46
                  Tag             =   "Prn?"
                  Top             =   2040
                  Visible         =   0   'False
                  Width           =   735
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CODPRODUCTO"
                  Height          =   330
                  Index           =   27
                  Left            =   5160
                  TabIndex        =   45
                  Tag             =   "C�digo Medicamento"
                  Top             =   2640
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR28CANTIDAD"
                  Height          =   330
                  Index           =   28
                  Left            =   5400
                  Locked          =   -1  'True
                  TabIndex        =   44
                  Tag             =   "Cantidad"
                  Top             =   240
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR28DOSIS"
                  Height          =   330
                  Index           =   30
                  Left            =   0
                  Locked          =   -1  'True
                  TabIndex        =   3
                  Tag             =   "Dosis"
                  Top             =   840
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR93CODUNIMEDIDA"
                  Height          =   330
                  Index           =   31
                  Left            =   960
                  Locked          =   -1  'True
                  TabIndex        =   4
                  Tag             =   "UM"
                  Top             =   840
                  Width           =   660
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR28horainicio"
                  Height          =   330
                  Index           =   37
                  Left            =   1920
                  Locked          =   -1  'True
                  TabIndex        =   43
                  Tag             =   "H.I.|Hora Inicio"
                  Top             =   2160
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR28horafin"
                  Height          =   330
                  Index           =   36
                  Left            =   4320
                  Locked          =   -1  'True
                  TabIndex        =   42
                  Tag             =   "H.F.|Hora Fin"
                  Top             =   2160
                  Width           =   375
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "STAT"
                  DataField       =   "FR28indcomieninmed"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   8280
                  TabIndex        =   41
                  Tag             =   "STAT?"
                  Top             =   675
                  Visible         =   0   'False
                  Width           =   855
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "PERF"
                  DataField       =   "FR28INDPERF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   8280
                  TabIndex        =   40
                  Tag             =   "PERF?"
                  Top             =   915
                  Visible         =   0   'False
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CODPRODUCTO_DIL"
                  Height          =   330
                  Index           =   40
                  Left            =   5640
                  TabIndex        =   39
                  Tag             =   "C�digo Producto diluir"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   41
                  Left            =   5640
                  Locked          =   -1  'True
                  TabIndex        =   38
                  Tag             =   "Descripci�n Producto Diluir"
                  Top             =   2160
                  Visible         =   0   'False
                  Width           =   585
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28CANTIDADDIL"
                  Height          =   330
                  Index           =   42
                  Left            =   7080
                  TabIndex        =   37
                  Tag             =   "Vol.Dil.|Volumen(ml)"
                  Top             =   840
                  Visible         =   0   'False
                  Width           =   732
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28tiemmininf"
                  Height          =   330
                  Index           =   43
                  Left            =   8160
                  TabIndex        =   36
                  Tag             =   "T.Inf.(min)|Tiempo Infusi�n(min)"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FRH7CODFORMFAR"
                  Height          =   330
                  Index           =   35
                  Left            =   8400
                  TabIndex        =   2
                  Tag             =   "FF"
                  Top             =   2040
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtHoras 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Left            =   6600
                  TabIndex        =   35
                  Tag             =   "Tiempo Infusi�n(hor)"
                  Top             =   1440
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtMinutos 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Left            =   7080
                  TabIndex        =   34
                  Tag             =   "Tiempo Infusi�n(min)"
                  Top             =   1440
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28VOLUMEN"
                  Height          =   330
                  Index           =   38
                  Left            =   7800
                  TabIndex        =   33
                  Tag             =   "Volum.|Volumen(ml)"
                  Top             =   240
                  Visible         =   0   'False
                  Width           =   732
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28VOLTOTAL"
                  Height          =   330
                  Index           =   44
                  Left            =   8040
                  TabIndex        =   32
                  Tag             =   "Vol.Total|Volumen Total (ml)"
                  Top             =   1440
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28VELPERFUSION"
                  Height          =   330
                  Index           =   82
                  Left            =   6600
                  TabIndex        =   31
                  Tag             =   "Perfusi�n|Velocidad de Perfusi�n"
                  Top             =   2040
                  Visible         =   0   'False
                  Width           =   975
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28VOLUMEN_2"
                  Height          =   330
                  Index           =   47
                  Left            =   8880
                  TabIndex        =   30
                  Tag             =   "Volumen(ml) Producto 2"
                  Top             =   2640
                  Visible         =   0   'False
                  Width           =   732
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FRH7CODFORMFAR_2"
                  Height          =   330
                  Index           =   48
                  Left            =   6840
                  TabIndex        =   29
                  Tag             =   "FF2"
                  Top             =   2640
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR93CODUNIMEDIDA_2"
                  Height          =   330
                  Index           =   49
                  Left            =   6360
                  Locked          =   -1  'True
                  TabIndex        =   28
                  Tag             =   "UM2"
                  Top             =   240
                  Width           =   660
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28DOSIS_2"
                  Height          =   330
                  Index           =   50
                  Left            =   7440
                  TabIndex        =   27
                  Tag             =   "Dosis2"
                  Top             =   2640
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   51
                  Left            =   8520
                  TabIndex        =   26
                  Tag             =   "Medicamento 2"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   660
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CODPRODUCTO_2"
                  Height          =   330
                  Index           =   52
                  Left            =   5160
                  TabIndex        =   25
                  Tag             =   "C�digo Medicamento 2"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28OPERACION"
                  Height          =   330
                  Index           =   53
                  Left            =   7440
                  TabIndex        =   24
                  Tag             =   "Operaci�n"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28UBICACION"
                  Height          =   330
                  Index           =   54
                  Left            =   6960
                  TabIndex        =   23
                  Tag             =   "Ubicaci�n"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   13
                  Left            =   8760
                  TabIndex        =   1
                  Tag             =   "Medicamento"
                  Top             =   240
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR28DESPRODUCTO"
                  Height          =   330
                  Index           =   55
                  Left            =   0
                  Locked          =   -1  'True
                  TabIndex        =   0
                  Tag             =   "Medic.No Form.|""Medic. No Formulario"""
                  Top             =   240
                  Width           =   5295
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28REFERENCIA"
                  Height          =   330
                  Index           =   56
                  Left            =   7200
                  TabIndex        =   22
                  Tag             =   "Referencia"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR28PATHFORMAG"
                  Height          =   330
                  Index           =   57
                  Left            =   0
                  Locked          =   -1  'True
                  TabIndex        =   21
                  Tag             =   "Arch.Inf.F.M."
                  Top             =   1440
                  Width           =   4005
               End
               Begin VB.CommandButton cmdWord 
                  Caption         =   "W"
                  BeginProperty Font 
                     Name            =   "MS Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   350
                  Left            =   4080
                  TabIndex        =   20
                  Top             =   1440
                  Width           =   495
               End
               Begin VB.CommandButton cmdExplorador 
                  Caption         =   "..."
                  BeginProperty Font 
                     Name            =   "MS Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   525
                  Index           =   1
                  Left            =   4800
                  TabIndex        =   19
                  Top             =   1320
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28CANTDISP"
                  Height          =   330
                  Index           =   58
                  Left            =   7680
                  TabIndex        =   18
                  Tag             =   "Cant.Dispens."
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  DataField       =   "FR28INSTRADMIN"
                  Height          =   1290
                  Index           =   39
                  Left            =   0
                  Locked          =   -1  'True
                  MultiLine       =   -1  'True
                  ScrollBars      =   3  'Both
                  TabIndex        =   17
                  Tag             =   "Inst.Admin.|Instrucciones para administraci�n"
                  Top             =   2760
                  Width           =   7965
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  DataField       =   "FR28fecinicio"
                  Height          =   330
                  Index           =   3
                  Left            =   0
                  TabIndex        =   50
                  Tag             =   "Inicio|Fecha Inicio Vigencia"
                  Top             =   2160
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   12632256
                  Enabled         =   0   'False
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  DataField       =   "FR28fecfin"
                  Height          =   330
                  Index           =   4
                  Left            =   2400
                  TabIndex        =   51
                  Tag             =   "Fin|Fecha Fin"
                  Top             =   2160
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   12632256
                  Enabled         =   0   'False
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  DataField       =   "fr34codvia"
                  Height          =   330
                  Index           =   1
                  Left            =   3480
                  TabIndex        =   52
                  Tag             =   "V�a|C�digo V�a"
                  Top             =   840
                  Width           =   765
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0605.frx":015C
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0605.frx":0178
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1349
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   12632256
                  Enabled         =   0   'False
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  DataField       =   "frg4codfrecuencia"
                  Height          =   330
                  Index           =   0
                  Left            =   1680
                  TabIndex        =   53
                  Tag             =   "Frec.|Cod.Frecuencia"
                  Top             =   840
                  Width           =   1725
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0605.frx":0194
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0605.frx":01B0
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   3043
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   12632256
                  Enabled         =   0   'False
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  DataField       =   "frh5codperiodicidad"
                  Height          =   330
                  Index           =   3
                  Left            =   4320
                  TabIndex        =   54
                  Tag             =   "Period.|Cod. Periodicidad"
                  Top             =   840
                  Width           =   1845
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0605.frx":01CC
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0605.frx":01E8
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   3254
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   12632256
                  Enabled         =   0   'False
                  DataFieldToDisplay=   "Column 0"
               End
               Begin MSComDlg.CommonDialog CommonDialog1 
                  Left            =   6000
                  Top             =   1320
                  _ExtentX        =   847
                  _ExtentY        =   847
                  _Version        =   327681
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "F�rmula Magistral"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   16
                  Left            =   0
                  TabIndex        =   83
                  Top             =   0
                  Width           =   1500
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cantidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   17
                  Left            =   5400
                  TabIndex        =   82
                  Top             =   0
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosis"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   19
                  Left            =   0
                  TabIndex        =   81
                  Top             =   600
                  Width           =   480
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "UM"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   21
                  Left            =   960
                  TabIndex        =   80
                  Top             =   600
                  Width           =   300
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Frecuencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   23
                  Left            =   1680
                  TabIndex        =   79
                  Top             =   600
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "V�a"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   25
                  Left            =   3480
                  TabIndex        =   78
                  Top             =   600
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Periodicidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   29
                  Left            =   4320
                  TabIndex        =   77
                  Top             =   600
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Fin"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   31
                  Left            =   2400
                  TabIndex        =   76
                  Top             =   1920
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   32
                  Left            =   1920
                  TabIndex        =   75
                  Top             =   1920
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Inicio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   33
                  Left            =   0
                  TabIndex        =   74
                  Top             =   1920
                  Width           =   1815
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   30
                  Left            =   4320
                  TabIndex        =   73
                  Top             =   1920
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Soluci�n para Diluir"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   34
                  Left            =   5640
                  TabIndex        =   72
                  Top             =   1920
                  Visible         =   0   'False
                  Width           =   735
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   35
                  Left            =   7080
                  TabIndex        =   71
                  Top             =   600
                  Visible         =   0   'False
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Tiempo Infusi�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   36
                  Left            =   6600
                  TabIndex        =   70
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "FF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   37
                  Left            =   8400
                  TabIndex        =   69
                  Top             =   1800
                  Visible         =   0   'False
                  Width           =   225
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�digo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   47
                  Left            =   5640
                  TabIndex        =   68
                  Top             =   2520
                  Visible         =   0   'False
                  Width           =   600
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�digo Sol"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   48
                  Left            =   5400
                  TabIndex        =   67
                  Top             =   3360
                  Visible         =   0   'False
                  Width           =   930
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   49
                  Left            =   7800
                  TabIndex        =   66
                  Top             =   0
                  Visible         =   0   'False
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Instrucciones para administraci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   50
                  Left            =   0
                  TabIndex        =   65
                  Top             =   2520
                  Width           =   5625
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Vol.Total(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   51
                  Left            =   8040
                  TabIndex        =   64
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   1080
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Vel.Perfus."
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   52
                  Left            =   6600
                  TabIndex        =   63
                  Top             =   1800
                  Visible         =   0   'False
                  Width           =   945
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "ml/hora"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   77
                  Left            =   7620
                  TabIndex        =   62
                  Top             =   2085
                  Visible         =   0   'False
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   78
                  Left            =   8880
                  TabIndex        =   61
                  Top             =   2400
                  Visible         =   0   'False
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�digo Med2"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   79
                  Left            =   5640
                  TabIndex        =   60
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   1125
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "FF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   80
                  Left            =   6840
                  TabIndex        =   59
                  Top             =   2400
                  Visible         =   0   'False
                  Width           =   225
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "UM"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   81
                  Left            =   6360
                  TabIndex        =   58
                  Top             =   0
                  Width           =   300
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosis"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   82
                  Left            =   7440
                  TabIndex        =   57
                  Top             =   2400
                  Visible         =   0   'False
                  Width           =   480
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Medicamento 2"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   83
                  Left            =   8520
                  TabIndex        =   56
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   1305
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Archivo Inf. F�rmula Magistral"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   84
                  Left            =   0
                  TabIndex        =   55
                  Top             =   1200
                  Width           =   2550
               End
            End
            Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
               Height          =   4035
               Index           =   0
               Left            =   90
               TabIndex        =   84
               TabStop         =   0   'False
               Top             =   120
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorEven   =   12632256
               BackColorOdd    =   12632256
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7117
               _StockProps     =   79
            End
         End
      End
   End
   Begin VB.CommandButton cmddispensar 
      Caption         =   "Dispensar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7080
      TabIndex        =   12
      Top             =   7680
      Width           =   1575
   End
   Begin VB.CommandButton cmdfabfm 
      Caption         =   "Fabricar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10440
      TabIndex        =   11
      Top             =   7680
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Ver F�rmulas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   1680
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   11775
      Begin VB.OptionButton Option1 
         Caption         =   "Dispensadas Parciales"
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   119
         Top             =   1320
         Visible         =   0   'False
         Width           =   2175
      End
      Begin VB.CheckBox chkservicio 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   5280
         TabIndex        =   108
         ToolTipText     =   "Todos Servicios"
         Top             =   480
         Value           =   1  'Checked
         Width           =   975
      End
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   3120
         Locked          =   -1  'True
         TabIndex        =   105
         TabStop         =   0   'False
         Tag             =   "Desc.Servicio"
         ToolTipText     =   "Desc.Servicio"
         Top             =   480
         Width           =   2085
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dispensadas"
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   104
         Top             =   1080
         Width           =   1695
      End
      Begin VB.CommandButton Command1 
         Caption         =   "FILTRAR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10440
         TabIndex        =   103
         Top             =   765
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Fabricaci�n Terminada"
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   840
         Width           =   2175
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Fabricaci�n Iniciada"
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   600
         Width           =   2175
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pendientes de Fabricar"
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Value           =   -1  'True
         Width           =   2175
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   2400
         TabIndex        =   106
         ToolTipText     =   "C�digo Servicio"
         Top             =   480
         Width           =   645
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1535
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6429
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1147
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin ComctlLib.ListView lvwSec 
         Height          =   780
         Left            =   2400
         TabIndex        =   109
         Top             =   840
         Visible         =   0   'False
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   1376
         View            =   3
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcredaccion 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   6600
         TabIndex        =   111
         Top             =   480
         Width           =   1740
         _Version        =   65537
         _ExtentX        =   3069
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcEnvio 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   6600
         TabIndex        =   112
         Top             =   1200
         Width           =   1740
         _Version        =   65537
         _ExtentX        =   3069
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDispensacion 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   8520
         TabIndex        =   113
         Top             =   1200
         Width           =   1740
         _Version        =   65537
         _ExtentX        =   3069
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcPendiente 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   8520
         TabIndex        =   114
         Top             =   480
         Width           =   1740
         _Version        =   65537
         _ExtentX        =   3069
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Petici�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   6600
         TabIndex        =   118
         Top             =   240
         Width           =   1290
      End
      Begin VB.Label lblEnvio 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Iniciada"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   6600
         TabIndex        =   117
         Top             =   960
         Width           =   1275
      End
      Begin VB.Label lblDispen 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Dispensaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   8520
         TabIndex        =   116
         Top             =   960
         Width           =   1740
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Pendiente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Index           =   0
         Left            =   8520
         TabIndex        =   115
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Secciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   5280
         TabIndex        =   110
         Top             =   960
         Visible         =   0   'False
         Width           =   900
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   2400
         TabIndex        =   107
         Top             =   240
         Width           =   705
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Visible         =   0   'False
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Visible         =   0   'False
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Visible         =   0   'False
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Visible         =   0   'False
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmCrtlFabFM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmCtrlFabMez(FR0605.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: MAYO DE 1999                                                  *
'* DESCRIPCION: Controlar Fabricaci�n de Mezclas                        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
'Dim WithEvents objSearch As clsCWSearch
Dim gstrWhere_PAC As String
Dim gstrWhere_SER As String
'

Private Sub auxDBGrid1_DblClick(Index As Integer)

  If auxDBGrid1(Index).Rows > 0 Then
    If Index = 0 Then
      tabTab1(0).Tab = 0
    Else
      tabTab1(1).Tab = 0
    End If
  End If
  
End Sub

Private Sub cboservicio_Click()
Dim intDptoSel As Integer

  auxDBGrid1(0).RemoveAll
  auxDBGrid1(6).RemoveAll

  If cboservicio <> "" Then
    intDptoSel = cboservicio
    Call pCargarSecciones(intDptoSel)
    txtServicio.Text = cboservicio.Columns(1).Value
    chkservicio = 0
  Else
    txtServicio.Text = ""
  End If
  
End Sub

Private Sub cboservicio_CloseUp()
Dim intDptoSel As Integer

  auxDBGrid1(0).RemoveAll
  auxDBGrid1(6).RemoveAll

  If cboservicio <> "" Then
    intDptoSel = cboservicio
    Call pCargarSecciones(intDptoSel)
    txtServicio.Text = cboservicio.Columns(1).Value
    chkservicio = 0
  Else
    txtServicio.Text = ""
  End If

End Sub


Private Sub chkservicio_Click()
  
  If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
    lvwSec.ListItems.Clear
  End If
  auxDBGrid1(0).RemoveAll
  auxDBGrid1(6).RemoveAll

End Sub

Private Sub cmddispensar_Click()

cmddispensar.Enabled = False

  If SSTab1.Tab = 0 Then
    If auxDBGrid1(0).Rows > 0 Then
      gstrLlamador = "FrmCtrlFabFM.Pac"
      gcodigopeticion = auxDBGrid1(0).Columns("C�d.Petici�n").Value
    Else
      gstrLlamador = ""
      cmddispensar.Enabled = True
      Exit Sub
    End If
  Else
    If auxDBGrid1(6).Rows > 0 Then
      gstrLlamador = "FrmCtrlFabFM.Ser"
      gcodigopeticion = auxDBGrid1(6).Columns("C�d.Necesidad").Value
    Else
      gstrLlamador = ""
      cmddispensar.Enabled = True
      Exit Sub
    End If
  End If
  Call objsecurity.LaunchProcess("FR0606")
  gstrLlamador = ""

cmddispensar.Enabled = True

Call Command1_Click

End Sub

Private Sub cmdfabfm_Click()
Dim strSelect As String
Dim strinsert As String
Dim bCrearOFNueva As Boolean
Dim rsta As rdoResultset
Dim strSelectFR6900 As String
Dim rstFR6900 As rdoResultset
Dim strSelectFR7000 As String
Dim rstFR7000 As rdoResultset
Dim strSelectFR7100 As String
Dim rstFR7100   As rdoResultset
Dim auxcant

Screen.MousePointer = vbHourglass
cmdfabfm.Enabled = False

  If SSTab1.Tab = 0 Then
    If auxDBGrid1(0).Rows = 0 Then
      cmdfabfm.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  Else
    If auxDBGrid1(6).Rows = 0 Then
      cmdfabfm.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  End If

  cmdfabfm.Enabled = False

  If SSTab1.Tab = 0 Then
    ' Mirar si existe una orden de fabricaci�n para este c�digo de petici�n
    strSelect = "SELECT COUNT(FR66CODPETICION) FROM FR5900 WHERE " & _
                " FR66CODPETICION=" & auxDBGrid1(0).Columns("C�d.Petici�n").Value & _
                " AND FR28NUMLINEA=" & auxDBGrid1(0).Columns("Linea").Value
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    If IsNull(rsta.rdoColumns(0).Value) Then
       ' Ok, NO EXISTE -> CREAR UNA NUEVA
       bCrearOFNueva = True
    ElseIf rsta.rdoColumns(0).Value = 0 Then
       ' Ok, NO EXISTE -> CREAR UNA NUEVA
       bCrearOFNueva = True
    Else
       ' Ok, SI EXISTE -> HACER REFERENCIA A LA QUE EXISTE
       bCrearOFNueva = False
    End If
    rsta.Close
    Set rsta = Nothing
    If bCrearOFNueva Then
       ' Crear una OF nueva por cada forma de hacer un producto
       strSelectFR6900 = " SELECT FR69CODPROCFABRIC, FR73CODPRODUCTO, FR69DESPROCESO FROM FR6900 " & _
                         " WHERE FR73CODPRODUCTO = " & auxDBGrid1(0).Columns("C�d.Prod.").Value
       Set rstFR6900 = objApp.rdoConnect.OpenResultset(strSelectFR6900)
       If rstFR6900.EOF Then
          strinsert = "insert into fr5900 " & _
                      "  (FR59CODORDFABR, " & _
                      "  FR59OBSORDEN,    " & _
                      "  FR73CODPRODUCTO, " & _
                      "  FR93CODUNIMEDIDA, " & _
                      "  FR59CANTFABRICAR, " & _
                      "  FR37CODESTOF, " & _
                      "  FR66CODPETICION, " & _
                      "  FR28NUMLINEA, " & _
                      "  FR59DESPROCESO,FR59INDPAC) "
          strinsert = strinsert & _
                      "Values " & _
                      "  (FR59CODORDFABR_SEQUENCE.nextval," & _
                      "  NULL," & _
                         auxDBGrid1(0).Columns("C�d.Prod.").Value & "," & _
                      "'" & auxDBGrid1(0).Columns("U.M.P.").Value & "'" & "," & _
                         auxDBGrid1(0).Columns("Cantidad").Value & "," & _
                      "1," & _
                         auxDBGrid1(0).Columns("C�d.Petici�n").Value & "," & _
                         auxDBGrid1(0).Columns("Linea").Value & "," & _
                      "NULL" & _
                      ",0)"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
       End If
       While Not rstFR6900.EOF
          strSelect = "SELECT FR59CODORDFABR_SEQUENCE.nextval FROM dual"
          Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
          strinsert = "insert into fr5900 " & _
                      "  (FR59CODORDFABR, " & _
                      "  FR59OBSORDEN,    " & _
                      "  FR73CODPRODUCTO, " & _
                      "  FR93CODUNIMEDIDA, " & _
                      "  FR59CANTFABRICAR, " & _
                      "  FR37CODESTOF, " & _
                      "  FR66CODPETICION, " & _
                      "  FR28NUMLINEA, " & _
                      "  FR59DESPROCESO,FR59INDPAC) "
          strinsert = strinsert & _
                      "Values " & _
                      "  (" & rsta.rdoColumns(0).Value & "," & _
                      "  NULL," & _
                         auxDBGrid1(0).Columns("C�d.Prod.").Value & "," & _
                      "'" & auxDBGrid1(0).Columns("U.M.P.").Value & "'" & "," & _
                         auxDBGrid1(0).Columns("Cantidad").Value & "," & _
                      "1," & _
                         auxDBGrid1(0).Columns("C�d.Petici�n").Value & "," & _
                         auxDBGrid1(0).Columns("Linea").Value & "," & _
                      "'" & rstFR6900.rdoColumns("FR69DESPROCESO").Value & _
                      "',0)"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64

          strSelectFR7000 = "SELECT * " & _
                            " FROM FR7000 " & _
                            " WHERE FR69CODPROCFABRIC = " & rstFR6900.rdoColumns("FR69CODPROCFABRIC").Value

          Set rstFR7000 = objApp.rdoConnect.OpenResultset(strSelectFR7000)

          ' Crear una nueva Operaci�n por cada Operaci�n/Producto
          While Not rstFR7000.EOF ' Recorrer la tabla Proceso Operacion
             strSelectFR7100 = "SELECT * "
             strSelectFR7100 = strSelectFR7100 & "  FROM FR7100 WHERE "
             strSelectFR7100 = strSelectFR7100 & "  FR69CODPROCFABRIC = " & rstFR7000.rdoColumns("FR69CODPROCFABRIC").Value & " And "
             strSelectFR7100 = strSelectFR7100 & "  FR70CODOPERACION = " & rstFR7000.rdoColumns("FR70CODOPERACION").Value

             Set rstFR7100 = objApp.rdoConnect.OpenResultset(strSelectFR7100)
             While Not rstFR7100.EOF ' Recorrer la tabla Proceso Producto
                strinsert = "insert into fr2400 " & _
                            "  (FR59CODORDFABR, " & _
                            "  FR70CODOPERACION, " & _
                            "  FR70DESOPERACION ," & _
                            "  FR24TIEMPPREV, " & _
                            "  SG02COD, " & _
                            "  FR24TIEMPREAL, " & _
                            "  FR73CODPRODUCTO, " & _
                            "  FR24CANTNECESARIA, " & _
                            "  FR93CODUNIMEDIDA, " & _
                            "  FR37CODESTOF)" & _
                            "Values " & _
                            "  (" & rsta.rdoColumns(0).Value & "," & _
                            rstFR7000.rdoColumns("FR70CODOPERACION").Value & "," & _
                            "'" & rstFR7000.rdoColumns("FR70DESOPERACION").Value & "'" & "," & _
                            rstFR7000.rdoColumns("FR70TIEMPOPREPACI").Value & "," & _
                            "'" & objsecurity.strUser & "'" & ","
                If Not IsNull(rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value) Then
                   strinsert = strinsert & rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value & ","
                Else
                   strinsert = strinsert & " NULL,"
                End If
                strinsert = strinsert & rstFR7100.rdoColumns("FR73CODPRODUCTO").Value & ","
                auxcant = auxDBGrid1(0).Columns("Cantidad").Value * rstFR7100.rdoColumns("FR71CANTNECESARIA").Value
                strinsert = strinsert & objGen.ReplaceStr(auxcant, ",", ".", 1) & ","
                If Not IsNull(rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                   strinsert = strinsert & "'" & rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value & "'" & ","
                Else
                   strinsert = strinsert & " NULL,"
                End If
                strinsert = strinsert & "  1)"
                objApp.rdoConnect.Execute strinsert, 64
                objApp.rdoConnect.Execute "Commit", 64
                rstFR7100.MoveNext
             Wend
             rstFR7100.Close
             Set rstFR7100 = Nothing
             rstFR7000.MoveNext
          Wend
          rstFR7000.Close
          Set rstFR7000 = Nothing
          rstFR6900.MoveNext
       Wend
       rstFR6900.Close
       Set rstFR6900 = Nothing
    End If
    objApp.rdoConnect.Execute "UPDATE FR6600 SET FR26CODESTPETIC=14 WHERE FR66CODPETICION=" & auxDBGrid1(0).Columns("C�d.Petici�n").Value, 64
    objApp.rdoConnect.Execute "Commit", 64
  Else 'Servicio
    ' Mirar si existe una orden de fabricaci�n para este c�digo de petici�n
    strSelect = "SELECT COUNT(FR55CODNECESUNID) FROM FR5900 WHERE " & _
                " FR55CODNECESUNID=" & auxDBGrid1(6).Columns("C�d.Necesidad").Value & _
                " AND FR20NUMLINEA=" & auxDBGrid1(6).Columns("Linea").Value
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    If IsNull(rsta.rdoColumns(0).Value) Then
       ' Ok, NO EXISTE -> CREAR UNA NUEVA
       bCrearOFNueva = True
    ElseIf rsta.rdoColumns(0).Value = 0 Then
       ' Ok, NO EXISTE -> CREAR UNA NUEVA
       bCrearOFNueva = True
    Else
       ' Ok, SI EXISTE -> HACER REFERENCIA A LA QUE EXISTE
       bCrearOFNueva = False
    End If
    rsta.Close
    Set rsta = Nothing
    If bCrearOFNueva Then
       ' Crear una OF nueva por cada forma de hacer un producto
       strSelectFR6900 = " SELECT FR69CODPROCFABRIC, FR73CODPRODUCTO, FR69DESPROCESO FROM FR6900 " & _
                         " WHERE FR73CODPRODUCTO = " & auxDBGrid1(6).Columns("C�d.Prod.").Value
       Set rstFR6900 = objApp.rdoConnect.OpenResultset(strSelectFR6900)
       If rstFR6900.EOF Then
          strinsert = "insert into fr5900 " & _
                      "  (FR59CODORDFABR, " & _
                      "  FR59OBSORDEN,    " & _
                      "  FR73CODPRODUCTO, " & _
                      "  FR93CODUNIMEDIDA, " & _
                      "  FR59CANTFABRICAR, " & _
                      "  FR37CODESTOF, " & _
                      "  FR55CODNECESUNID, " & _
                      "  FR20NUMLINEA, " & _
                      "  FR59DESPROCESO,FR59INDPAC) "
          strinsert = strinsert & _
                      "Values " & _
                      "  (FR59CODORDFABR_SEQUENCE.nextval," & _
                      "  NULL," & _
                         auxDBGrid1(6).Columns("C�d.Prod.").Value & "," & _
                      "'" & auxDBGrid1(6).Columns("U.M.").Value & "'" & "," & _
                         auxDBGrid1(6).Columns("Cantidad").Value & "," & _
                      "1," & _
                         auxDBGrid1(6).Columns("C�d.Necesidad").Value & "," & _
                         auxDBGrid1(6).Columns("Linea").Value & "," & _
                      "NULL" & _
                      ",-1)"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
       End If
       While Not rstFR6900.EOF
          strSelect = "SELECT FR59CODORDFABR_SEQUENCE.nextval FROM dual"
          Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
          strinsert = "insert into fr5900 " & _
                      "  (FR59CODORDFABR, " & _
                      "  FR59OBSORDEN,    " & _
                      "  FR73CODPRODUCTO, " & _
                      "  FR93CODUNIMEDIDA, " & _
                      "  FR59CANTFABRICAR, " & _
                      "  FR37CODESTOF, " & _
                      "  FR55CODNECESUNID, " & _
                      "  FR20NUMLINEA, " & _
                      "  FR59DESPROCESO,FR59INDPAC) "
          strinsert = strinsert & _
                      "Values " & _
                      "  (" & rsta.rdoColumns(0).Value & "," & _
                      "  NULL," & _
                         auxDBGrid1(6).Columns("C�d.Prod.").Value & "," & _
                      "'" & auxDBGrid1(6).Columns("U.M.").Value & "'" & "," & _
                         auxDBGrid1(6).Columns("Cantidad").Value & "," & _
                      "1," & _
                         auxDBGrid1(6).Columns("C�d.Necesidad").Value & "," & _
                         auxDBGrid1(6).Columns("Linea").Value & "," & _
                      "'" & rstFR6900.rdoColumns("FR69DESPROCESO").Value & _
                      "',-1)"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64

          strSelectFR7000 = "SELECT * " & _
                            " FROM FR7000 " & _
                            " WHERE FR69CODPROCFABRIC = " & rstFR6900.rdoColumns("FR69CODPROCFABRIC").Value

          Set rstFR7000 = objApp.rdoConnect.OpenResultset(strSelectFR7000)

          ' Crear una nueva Operaci�n por cada Operaci�n/Producto
          While Not rstFR7000.EOF ' Recorrer la tabla Proceso Operacion
             strSelectFR7100 = "SELECT * "
             strSelectFR7100 = strSelectFR7100 & "  FROM FR7100 WHERE "
             strSelectFR7100 = strSelectFR7100 & "  FR69CODPROCFABRIC = " & rstFR7000.rdoColumns("FR69CODPROCFABRIC").Value & " And "
             strSelectFR7100 = strSelectFR7100 & "  FR70CODOPERACION = " & rstFR7000.rdoColumns("FR70CODOPERACION").Value

             Set rstFR7100 = objApp.rdoConnect.OpenResultset(strSelectFR7100)
             While Not rstFR7100.EOF ' Recorrer la tabla Proceso Producto
                strinsert = "insert into fr2400 " & _
                            "  (FR59CODORDFABR, " & _
                            "  FR70CODOPERACION, " & _
                            "  FR70DESOPERACION ," & _
                            "  FR24TIEMPPREV, " & _
                            "  SG02COD, " & _
                            "  FR24TIEMPREAL, " & _
                            "  FR73CODPRODUCTO, " & _
                            "  FR24CANTNECESARIA, " & _
                            "  FR93CODUNIMEDIDA, " & _
                            "  FR37CODESTOF)" & _
                            "Values " & _
                            "  (" & rsta.rdoColumns(0).Value & "," & _
                            rstFR7000.rdoColumns("FR70CODOPERACION").Value & "," & _
                            "'" & rstFR7000.rdoColumns("FR70DESOPERACION").Value & "'" & "," & _
                            rstFR7000.rdoColumns("FR70TIEMPOPREPACI").Value & "," & _
                            "'" & objsecurity.strUser & "'" & ","
                If Not IsNull(rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value) Then
                   strinsert = strinsert & rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value & ","
                Else
                   strinsert = strinsert & " NULL,"
                End If
                strinsert = strinsert & rstFR7100.rdoColumns("FR73CODPRODUCTO").Value & ","
                auxcant = auxDBGrid1(6).Columns("Cantidad").Value * rstFR7100.rdoColumns("FR71CANTNECESARIA").Value
                strinsert = strinsert & objGen.ReplaceStr(auxcant, ",", ".", 1) & ","
                If Not IsNull(rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                   strinsert = strinsert & "'" & rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value & "'" & ","
                Else
                   strinsert = strinsert & " NULL,"
                End If
                strinsert = strinsert & "  1)"
                objApp.rdoConnect.Execute strinsert, 64
                objApp.rdoConnect.Execute "Commit", 64
                rstFR7100.MoveNext
             Wend
             rstFR7100.Close
             Set rstFR7100 = Nothing
             rstFR7000.MoveNext
          Wend
          rstFR7000.Close
          Set rstFR7000 = Nothing
          rstFR6900.MoveNext
       Wend
       rstFR6900.Close
       Set rstFR6900 = Nothing
    End If
    objApp.rdoConnect.Execute "UPDATE FR5500 SET FR26CODESTPETIC=14 WHERE FR55CODNECESUNID=" & auxDBGrid1(6).Columns("C�d.Necesidad").Value, 64
    objApp.rdoConnect.Execute "Commit", 64
  End If

  cmdfabfm.Enabled = True

Call Command1_Click
cmdfabfm.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdOM_Click()

cmdOM.Enabled = False

  If SSTab1.Tab = 0 Then
    If auxDBGrid1(0).Rows > 0 Then
      gstrLlamador = "FrmCtrlFabFM.Pac.Ver"
      gcodigopeticion = auxDBGrid1(0).Columns("C�d.Petici�n").Value
    Else
      gstrLlamador = ""
      cmdOM.Enabled = True
      Exit Sub
    End If
  Else
    If auxDBGrid1(6).Rows > 0 Then
      gstrLlamador = "FrmCtrlFabFM.Ser.Ver"
      gcodigopeticion = auxDBGrid1(6).Columns("C�d.Necesidad").Value
    Else
      gstrLlamador = ""
      cmdOM.Enabled = True
      Exit Sub
    End If
  End If
  Call objsecurity.LaunchProcess("FR0606")
  gstrLlamador = ""

cmdOM.Enabled = True

End Sub

Private Sub cmdWord_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strplantilla As String
Dim strubiplantilla As String
Dim wword As Word.Application
Dim strUbiDocumento As String
Dim strUbicacion As String
  
On Error GoTo error_shell
  
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=37"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strplantilla = rsta(0).Value
  Else
    rsta.Close
    Set rsta = Nothing
    MsgBox "Debe introducir el nombre de la plantilla Word en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 37 " & Chr(13) & _
           "Ej.: PETFM.DOT", vbInformation
    Exit Sub
  End If
  rsta.Close
  Set rsta = Nothing
  
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=36"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strUbicacion = rsta(0).Value
  Else
    MsgBox "Debe introducir el Path de la ubicaci�n de los ficheros Word de Form.Magistrales en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 36 " & Chr(13) & _
           "Ej.: C:\Archivos de programa\cun\", vbInformation
  End If
  rsta.Close
  Set rsta = Nothing
  
  strubiplantilla = strplantilla

  If txtText1(57).Text = "" Then
    MsgBox "Debe introducir el Path y el nombre del fichero del documento", vbInformation
    Exit Sub
  End If
  
  
  strUbiDocumento = strUbicacion & txtText1(57).Text
  
  Set wword = New Word.Application
  If Dir(strUbiDocumento) = "" Then
    wword.Documents.Add strubiplantilla
    wword.ActiveDocument.SaveAs strUbiDocumento
  Else
    wword.Documents.Open strUbiDocumento
  End If
  wword.Visible = True

error_shell:

  If Err.Number <> 0 Then
    MsgBox "Error N�mero: " & Err.Number & Chr(13) & Err.Description, vbCritical
  End If



End Sub

Private Sub cmdWord2_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strplantilla As String
Dim strubiplantilla As String
Dim wword As Word.Application
Dim strUbiDocumento As String
Dim strUbicacion As String
  
On Error GoTo error_shell
  
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=37"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strplantilla = rsta(0).Value
  Else
    rsta.Close
    Set rsta = Nothing
    MsgBox "Debe introducir el nombre de la plantilla Word en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 37 " & Chr(13) & _
           "Ej.: PETFM.DOT", vbInformation
    Exit Sub
  End If
  rsta.Close
  Set rsta = Nothing
  
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=36"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strUbicacion = rsta(0).Value
  Else
    MsgBox "Debe introducir el Path de la ubicaci�n de los ficheros Word de Form.Magistrales en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 36 " & Chr(13) & _
           "Ej.: C:\Archivos de programa\cun\", vbInformation
  End If
  rsta.Close
  Set rsta = Nothing
  
  strubiplantilla = strplantilla

  If txtText1(1).Text = "" Then
    MsgBox "Debe introducir el Path y el nombre del fichero del documento", vbInformation
    Exit Sub
  End If
  
  
  strUbiDocumento = strUbicacion & txtText1(1).Text
  
  Set wword = New Word.Application
  If Dir(strUbiDocumento) = "" Then
    wword.Documents.Add strubiplantilla
    wword.ActiveDocument.SaveAs strUbiDocumento
  Else
    wword.Documents.Open strUbiDocumento
  End If
  wword.Visible = True

error_shell:

  If Err.Number <> 0 Then
    MsgBox "Error N�mero: " & Err.Number & Chr(13) & Err.Description, vbCritical
  End If


End Sub

Private Sub Command1_Click()
Dim contSecciones, i As Integer
Dim glstrWhere As String

Screen.MousePointer = vbHourglass
Command1.Enabled = False

  If Option1(0) Then
    'cmddispensar.Enabled = True
    cmddispensar.Enabled = False
    cmdfabfm.Enabled = True
    'objWinInfo.objWinActiveForm.strWhere = "FR28OPERACION='L' AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR6600 WHERE FR26CODESTPETIC=3)"
    gstrWhere_PAC = " FR6600.FR26CODESTPETIC=3"
    gstrWhere_SER = " FR5500.FR26CODESTPETIC=3"
    'objWinInfo.DataRefresh
  Else
    If Option1(1) Then
      cmddispensar.Enabled = False
      cmdfabfm.Enabled = False
      'objWinInfo.objWinActiveForm.strWhere = "FR28OPERACION='L' AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR6600 WHERE FR26CODESTPETIC=4)"
      gstrWhere_PAC = " FR6600.FR26CODESTPETIC=14"
      gstrWhere_SER = " FR5500.FR26CODESTPETIC=14"
      'objWinInfo.DataRefresh
    ElseIf Option1(2) Then
      cmddispensar.Enabled = True
      cmdfabfm.Enabled = False
      gstrWhere_PAC = " FR6600.FR26CODESTPETIC=15"
      gstrWhere_SER = " FR5500.FR26CODESTPETIC=15"
    ElseIf Option1(3) Then
      cmddispensar.Enabled = False
      cmdfabfm.Enabled = False
      gstrWhere_PAC = " FR6600.FR26CODESTPETIC=5"
      gstrWhere_SER = " FR5500.FR26CODESTPETIC=5"
    Else
      'cmddispensar.Enabled = True
      cmddispensar.Enabled = False
      cmdfabfm.Enabled = False
      'objWinInfo.objWinActiveForm.strWhere = "FR28OPERACION='L' AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR6600 WHERE FR26CODESTPETIC=5)"
      gstrWhere_PAC = " FR6600.FR26CODESTPETIC=9"
      gstrWhere_SER = " FR5500.FR26CODESTPETIC=9"
      'objWinInfo.DataRefresh
    End If
  End If
    
  If Trim(cboservicio) <> "" Then
    gstrWhere_PAC = gstrWhere_PAC & " AND FR6600.AD02CODDPTO=" & cboservicio
    gstrWhere_SER = gstrWhere_SER & " AND FR5500.AD02CODDPTO=" & cboservicio
  End If
  
  If SSTab1.Tab = 1 Then
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    glstrWhere = ""
    If lvwSec.ListItems.Count > 0 Then
      If lvwSec.ListItems(1).Selected Then
      Else
        For i = 2 To lvwSec.ListItems.Count
          If lvwSec.ListItems(i).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              glstrWhere = glstrWhere & " AND ("
            Else
              glstrWhere = glstrWhere & " OR "
            End If
            If i = 2 Then
              glstrWhere = glstrWhere & "FR5500.AD41CODSECCION IS NULL"
            Else
              glstrWhere = glstrWhere & "FR5500.AD41CODSECCION=" & lvwSec.ListItems(i).Tag
            End If
          End If
        Next i
        If contSecciones > 0 Then
          glstrWhere = glstrWhere & ") "
        End If
      End If
    End If
  End If
  
  If glstrWhere <> "" Then
    gstrWhere_PAC = gstrWhere_PAC & glstrWhere
    gstrWhere_SER = gstrWhere_SER & glstrWhere
  End If
  
  Call Refrescar_Grid_Pac
  Call Refrescar_Grid_Serv

  If auxDBGrid1(0).Rows = 0 Then
    txtText1(34).Text = ""
    txtText1(55).Text = ""
    txtText1(28).Text = ""
    txtText1(49).Text = ""
    txtText1(30).Text = ""
    txtText1(31).Text = ""
    cboDBCombo1(0) = ""
    cboDBCombo1(1) = ""
    cboDBCombo1(3) = ""
    txtText1(57).Text = ""
    dtcDateCombo1(3) = ""
    txtText1(37).Text = ""
    dtcDateCombo1(4) = ""
    txtText1(36).Text = ""
    txtText1(39).Text = ""
  End If
  If auxDBGrid1(6).Rows = 0 Then
    txtText1(9).Text = ""
    txtText1(2).Text = ""
    txtText1(0).Text = ""
    txtText1(3).Text = ""
    txtText1(1).Text = ""
  End If
  
  tabTab1(0).Tab = 1
  tabTab1(1).Tab = 1
  
  SSTab1.TabCaption(0) = "Pacientes : " & auxDBGrid1(0).Rows
  SSTab1.TabCaption(1) = "Servicios : " & auxDBGrid1(6).Rows

  Call Calcular_Registros
Command1.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset

    cboservicio.RemoveAll
    stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
           "WHERE SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) " & _
           " order by AD02DESDPTO "
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While (Not rsta.EOF)
        Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing

End Sub


'Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
'
'  If strFormName = "FM" And strCtrlName = "txtText1(34)" Then
'    aValues(2) = txtText1(45)
'  End If
'
'End Sub
'
'Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'Call presentacion_pantalla
'End Sub

Private Sub Option1_Click(Index As Integer)
  
  If Option1(0) Then
    'cmddispensar.Enabled = True
    cmddispensar.Enabled = False
    cmdfabfm.Enabled = True
  Else
    If Option1(1) Then
      cmddispensar.Enabled = False
      cmdfabfm.Enabled = False
    ElseIf Option1(3) Then
      cmddispensar.Enabled = False
      cmdfabfm.Enabled = False
    Else
      cmddispensar.Enabled = True
      cmdfabfm.Enabled = False
    End If
  End If
  
  auxDBGrid1(0).RemoveAll
  auxDBGrid1(6).RemoveAll

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  'Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim strKey As String
Dim k As Integer
Dim j As Integer
Dim i As Integer

'Call objApp.SplashOn

Set objWinInfo = New clsCWWin

Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                              Me, tlbToolbar1, stbStatusBar1, _
                              cwWithAll)

For k = 1 To 29
  If k <> 6 Then
    tlbToolbar1.Buttons(k).Enabled = False
  End If
Next k

auxDBGrid1(0).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 16
  Call auxDBGrid1(0).Columns.Add(j)
Next j
i = 0
auxDBGrid1(0).Columns(i).Caption = "Fecha"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "C�d.Petici�n"
auxDBGrid1(0).Columns(i).Visible = False
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1300
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "F�rmula Magistral"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 2500
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Cantidad"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 800
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "U.M.P."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 600
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Historia"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 800
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Nombre"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Apellido 1�"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Apellido 2�"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Dr."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Departamento"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Dosis"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "U.M."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 600
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Frecuencia"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "V�a"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Periodicidad"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Archivo Inf. F�rmula Magistral"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 3500
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Fecha Inicio"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "H.I."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 500
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Fecha Fin"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "H.F."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 500
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Instrucciones para Administraci�n"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 4000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Linea"
auxDBGrid1(0).Columns(i).Visible = False
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "C�d.Prod."
auxDBGrid1(0).Columns(i).Visible = False
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000

auxDBGrid1(0).Redraw = True

gstrWhere_PAC = " -1=0 "
Call Refrescar_Grid_Pac

auxDBGrid1(6).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 6
  Call auxDBGrid1(6).Columns.Add(j)
Next j
i = 0
auxDBGrid1(6).Columns(i).Caption = "Fecha"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "C�d.Necesidad"
auxDBGrid1(6).Columns(i).Visible = False
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 1300
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "F�rmula Magistral"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 4000
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Cantidad"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 800
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "U.M."
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 600
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Archivo Inf. F�rmula Magistral"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 2500
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Servicio"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Secci�n"
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Dr."
auxDBGrid1(6).Columns(i).Visible = True
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 1500
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "Linea"
auxDBGrid1(6).Columns(i).Visible = False
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(6).Columns(i).Caption = "C�d.Prod."
auxDBGrid1(6).Columns(i).Visible = False
auxDBGrid1(6).Columns(i).Locked = True
auxDBGrid1(6).Columns(i).Width = 1000

auxDBGrid1(6).Redraw = True

gstrWhere_SER = " -1=0 "
lvwSec.ColumnHeaders.Add , , , 2000
Call Refrescar_Grid_Serv
     
Call Calcular_Registros

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
'  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
'  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
'  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
'  Call objWinInfo.WinDeRegister
'  Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean

  If strFormName = "Tipos de Interacci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub




Private Sub SSTab1_Click(PreviousTab As Integer)
Dim intDptoSel As Integer

  If cboservicio <> "" Then
    intDptoSel = cboservicio
    Call pCargarSecciones(intDptoSel)
    txtServicio.Text = cboservicio.Columns(1).Value
    chkservicio = 0
  Else
    txtServicio.Text = ""
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)

If Index <> 1 Then
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
  If auxDBGrid1(0).Rows > 0 Then
    txtText1(34).Text = auxDBGrid1(0).Columns("C�d.Petici�n").Value
    txtText1(55).Text = auxDBGrid1(0).Columns("F�rmula Magistral").Value
    txtText1(28).Text = auxDBGrid1(0).Columns("Cantidad").Value
    txtText1(49).Text = auxDBGrid1(0).Columns("U.M.P.").Value
    txtText1(30).Text = auxDBGrid1(0).Columns("Dosis").Value
    txtText1(31).Text = auxDBGrid1(0).Columns("U.M.").Value
    cboDBCombo1(0) = auxDBGrid1(0).Columns("Frecuencia").Value
    cboDBCombo1(1) = auxDBGrid1(0).Columns("V�a").Value
    cboDBCombo1(3) = auxDBGrid1(0).Columns("Periodicidad").Value
    txtText1(57).Text = auxDBGrid1(0).Columns("Archivo Inf. F�rmula Magistral").Value
    dtcDateCombo1(3) = auxDBGrid1(0).Columns("Fecha Inicio").Value
    txtText1(37).Text = auxDBGrid1(0).Columns("H.I.").Value
    dtcDateCombo1(4) = auxDBGrid1(0).Columns("Fecha Fin").Value
    txtText1(36).Text = auxDBGrid1(0).Columns("H.F.").Value
    txtText1(39).Text = auxDBGrid1(0).Columns("Instrucciones para Administraci�n").Value
  Else
    txtText1(34).Text = ""
    txtText1(55).Text = ""
    txtText1(28).Text = ""
    txtText1(49).Text = ""
    txtText1(30).Text = ""
    txtText1(31).Text = ""
    cboDBCombo1(0) = ""
    cboDBCombo1(1) = ""
    cboDBCombo1(3) = ""
    txtText1(57).Text = ""
    dtcDateCombo1(3) = ""
    txtText1(37).Text = ""
    dtcDateCombo1(4) = ""
    txtText1(36).Text = ""
    txtText1(39).Text = ""
  End If
Else
  If auxDBGrid1(6).Rows > 0 Then
    txtText1(9).Text = auxDBGrid1(6).Columns("C�d.Necesidad").Value
    txtText1(2).Text = auxDBGrid1(6).Columns("F�rmula Magistral").Value
    txtText1(0).Text = auxDBGrid1(6).Columns("Cantidad").Value
    txtText1(3).Text = auxDBGrid1(6).Columns("U.M.").Value
    txtText1(1).Text = auxDBGrid1(6).Columns("Archivo Inf. F�rmula Magistral").Value
  Else
    txtText1(9).Text = ""
    txtText1(2).Text = ""
    txtText1(0).Text = ""
    txtText1(3).Text = ""
    txtText1(1).Text = ""
  End If
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
'   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
'  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
If intIndex <> 1 Then
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
  If auxDBGrid1(0).Rows > 0 Then
    txtText1(34).Text = auxDBGrid1(0).Columns("C�d.Petici�n").Value
    txtText1(55).Text = auxDBGrid1(0).Columns("F�rmula Magistral").Value
    txtText1(28).Text = auxDBGrid1(0).Columns("Cantidad").Value
    txtText1(49).Text = auxDBGrid1(0).Columns("U.M.P.").Value
    txtText1(30).Text = auxDBGrid1(0).Columns("Dosis").Value
    txtText1(31).Text = auxDBGrid1(0).Columns("U.M.").Value
    cboDBCombo1(0) = auxDBGrid1(0).Columns("Frecuencia").Value
    cboDBCombo1(1) = auxDBGrid1(0).Columns("V�a").Value
    cboDBCombo1(3) = auxDBGrid1(0).Columns("Periodicidad").Value
    txtText1(57).Text = auxDBGrid1(0).Columns("Archivo Inf. F�rmula Magistral").Value
    dtcDateCombo1(3) = auxDBGrid1(0).Columns("Fecha Inicio").Value
    txtText1(37).Text = auxDBGrid1(0).Columns("H.I.").Value
    dtcDateCombo1(4) = auxDBGrid1(0).Columns("Fecha Fin").Value
    txtText1(36).Text = auxDBGrid1(0).Columns("H.F.").Value
    txtText1(39).Text = auxDBGrid1(0).Columns("Instrucciones para Administraci�n").Value
  End If
Else
  If auxDBGrid1(6).Rows > 0 Then
    txtText1(9).Text = auxDBGrid1(6).Columns("C�d.Necesidad").Value
    txtText1(2).Text = auxDBGrid1(6).Columns("F�rmula Magistral").Value
    txtText1(0).Text = auxDBGrid1(6).Columns("Cantidad").Value
    txtText1(3).Text = auxDBGrid1(6).Columns("U.M.").Value
    txtText1(1).Text = auxDBGrid1(6).Columns("Archivo Inf. F�rmula Magistral").Value
  End If
End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub



Private Sub presentacion_pantalla()

    lbllabel1(84).Left = 0
    lbllabel1(84).Top = 1200
    lbllabel1(84).Visible = True
    txtText1(57).Left = 0
    txtText1(57).Top = 1440
    txtText1(57).Visible = True
    cmdExplorador(1).Left = 4920
    cmdExplorador(1).Top = 1240
    'cmdExplorador(1).Visible = True
    cmdExplorador(1).Visible = False
    cmdWord.Left = 5520
    cmdWord.Top = 1240
    cmdWord.Visible = True
    
    'lblLabel1(16).Visible = False 'Linea Medicamento....
    txtText1(13).Visible = False
    lbllabel1(37).Visible = False
    txtText1(35).Visible = False
    lbllabel1(19).Top = 600
    lbllabel1(19).Left = 0
    lbllabel1(19).Visible = True 'dosis
    txtText1(30).Top = 840
    txtText1(30).Left = 0
    txtText1(30).Visible = True
    lbllabel1(21).Top = 600
    lbllabel1(21).Left = 1000
    lbllabel1(21).Visible = True
    txtText1(31).Top = 840
    txtText1(31).Left = 1000
    txtText1(31).Visible = True
    lbllabel1(49).Visible = False
    txtText1(38).Visible = False
    
    lbllabel1(17).Visible = False 'Linea Cantidad...
    txtText1(28).Visible = False
    lbllabel1(23).Left = 1960
    lbllabel1(23).Visible = True
    cboDBCombo1(0).Left = 1960
    cboDBCombo1(0).Visible = True
    lbllabel1(25).Left = 3760
    lbllabel1(25).Visible = True
    cboDBCombo1(1).Left = 3760
    cboDBCombo1(1).Visible = True
    lbllabel1(29).Left = 4600
    lbllabel1(29).Visible = True
    cboDBCombo1(3).Left = 4600
    cboDBCombo1(3).Visible = True
    
    lbllabel1(33).Left = 5520
    lbllabel1(33).Top = 0 'Linea Fecha Inicio...
    lbllabel1(33).Visible = True
    dtcDateCombo1(3).Left = 5520
    dtcDateCombo1(3).Top = 240
    dtcDateCombo1(3).Visible = True
    lbllabel1(32).Left = 7440
    lbllabel1(32).Top = 0
    lbllabel1(32).Visible = True
    txtText1(37).Left = 7440
    txtText1(37).Top = 240
    txtText1(37).Visible = True
    
    lbllabel1(34).Visible = False 'Linea Sol.Dil....
    txtText1(41).Visible = False
    lbllabel1(35).Visible = False
    txtText1(42).Visible = False
    lbllabel1(36).Visible = False
    txtHoras.Visible = False
    txtMinutos.Visible = False
    
    lbllabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
    lbllabel1(50).Visible = True
    txtText1(39).Top = 2640
    txtText1(39).Visible = True
    lbllabel1(52).Visible = False
    txtText1(82).Visible = False
    'lblLabel1(62).Visible = False
    'txtText1(81).Visible = False
    lbllabel1(77).Visible = False
    'txtText1(80).Visible = False
    lbllabel1(51).Visible = False
    txtText1(44).Visible = False
    
    lbllabel1(31).Left = 0
    lbllabel1(31).Top = 1800 'Linea Fecha fin...
    lbllabel1(31).Visible = True
    dtcDateCombo1(4).Left = 0
    dtcDateCombo1(4).Top = 2040
    dtcDateCombo1(4).Visible = True
    lbllabel1(30).Left = 1920
    lbllabel1(30).Top = 1800
    lbllabel1(30).Visible = True
    txtText1(36).Left = 1920
    txtText1(36).Top = 2040
    txtText1(36).Visible = True
    
    'lblLabel1(79).Top = 1800
    'lblLabel1(79).Visible = False
    'txtText1(52).Top = 2040
    'txtText1(52).Visible = False
    lbllabel1(83).Visible = False 'Linea Prod2....
    txtText1(51).Visible = False
    lbllabel1(80).Visible = False
    txtText1(48).Visible = False
    lbllabel1(82).Visible = False
    txtText1(50).Visible = False
    lbllabel1(81).Visible = False
    txtText1(49).Visible = False
    lbllabel1(78).Visible = False
    txtText1(47).Visible = False
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = False
    chkCheck1(4).Visible = False

    txtText1(55).Top = 240
    txtText1(55).Left = 0
    txtText1(55).Visible = True
    lbllabel1(16).Top = 0
    lbllabel1(16).Caption = "F�rmula Magistral"

'If grdDBGrid1(0).Rows > 0 Then
'grdDBGrid1(0).Columns("Inicio").Width = 1000
'grdDBGrid1(0).Columns("H.I.").Width = 400
'grdDBGrid1(0).Columns("Medic.No Form.").Width = 2500
'grdDBGrid1(0).Columns("Dosis").Width = 1000
'grdDBGrid1(0).Columns("UM").Width = 600
'grdDBGrid1(0).Columns("Frec.").Width = 600
'grdDBGrid1(0).Columns("V�a").Width = 600
'grdDBGrid1(0).Columns("Period.").Width = 600
'grdDBGrid1(0).Columns("Fin").Width = 1000
'grdDBGrid1(0).Columns("H.F.").Width = 400
'grdDBGrid1(0).Columns("Inst.Admin.").Width = 2000
'grdDBGrid1(0).Columns(12).Visible = False
'grdDBGrid1(0).Columns(13).Visible = True
'grdDBGrid1(0).Columns(14).Visible = False
'grdDBGrid1(0).Columns(15).Visible = False
'grdDBGrid1(0).Columns(16).Visible = False
'grdDBGrid1(0).Columns(17).Visible = False
'grdDBGrid1(0).Columns(19).Visible = False
'grdDBGrid1(0).Columns(20).Visible = False
'grdDBGrid1(0).Columns(21).Visible = False
'grdDBGrid1(0).Columns(22).Visible = False
'grdDBGrid1(0).Columns(23).Visible = False
'grdDBGrid1(0).Columns(24).Visible = False
'grdDBGrid1(0).Columns(25).Visible = False
'grdDBGrid1(0).Columns(26).Visible = False
'grdDBGrid1(0).Columns(27).Visible = False
'grdDBGrid1(0).Columns(28).Visible = False
'grdDBGrid1(0).Columns(29).Visible = False
'grdDBGrid1(0).Columns(30).Visible = False
'grdDBGrid1(0).Columns(31).Visible = False
'grdDBGrid1(0).Columns(32).Visible = False
'grdDBGrid1(0).Columns(33).Visible = False
'grdDBGrid1(0).Columns(34).Visible = False
'grdDBGrid1(0).Columns(35).Visible = False
'grdDBGrid1(0).Columns(36).Visible = False
'End If

End Sub

Private Sub Refrescar_Grid_Serv()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim Departamento As String
Dim Seccion As String
Dim Medico As String
Dim strAux As String
Dim rstaux As rdoResultset
Dim strpeticion As String
Dim rstpeticion As rdoResultset

Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(6).Redraw = False
auxDBGrid1(6).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & " FR2000.FR55CODNECESUNID"
strSelect = strSelect & ",FR2000.FR20DESFORMAG"
strSelect = strSelect & ",FR2000.FR20CANTNECESQUIR"
strSelect = strSelect & ",FR2000.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR2000.FR20PATHFORMAG"
strSelect = strSelect & ",FR2000.FR20NUMLINEA"
strSelect = strSelect & ",FR2000.FR73CODPRODUCTO"

strSelect = strSelect & " FROM FR2000,FR5500"
strSelect = strSelect & " WHERE "

strSelect = strSelect & " FR2000.FR55CODNECESUNID=FR5500.FR55CODNECESUNID"
strSelect = strSelect & " AND FR5500.FR55INDFM=-1 AND FR2000.FR73CODPRODUCTO<>999999999 AND NVL(FR2000.FR20INDBLOQUEO,0)=0 AND " & gstrWhere_SER

strSelect = strSelect & " ORDER BY FR55CODNECESUNID DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF

 strpeticion = "SELECT AD02CODDPTO,AD41CODSECCION,FR55FECENVIO,SG02COD_FIR"
  strpeticion = strpeticion & " FROM FR5500 WHERE "
  strpeticion = strpeticion & " FR55CODNECESUNID=" & rsta("FR55CODNECESUNID").Value
  Set rstpeticion = objApp.rdoConnect.OpenResultset(strpeticion)
  If Not IsNull(rstpeticion("AD02CODDPTO").Value) Then
    strAux = "SELECT AD02DESDPTO FROM AD0200 WHERE "
    strAux = strAux & " AD02CODDPTO=" & rstpeticion("AD02CODDPTO").Value
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Departamento = rstaux("AD02DESDPTO").Value
  Else
    Departamento = " "
  End If
  If Not IsNull(rstpeticion("AD41CODSECCION").Value) Then
    strAux = "SELECT AD41DESSECCION FROM AD4100 WHERE "
    strAux = strAux & " AD41CODSECCION=" & rstpeticion("AD41CODSECCION").Value
    strAux = strAux & " AND "
    strAux = strAux & " AD02CODDPTO=" & rstpeticion("AD02CODDPTO").Value
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Seccion = rstaux("AD41DESSECCION").Value
  Else
    Seccion = " "
  End If
  If Not IsNull(rstpeticion("SG02COD_FIR").Value) Then
    strAux = "SELECT SG02APE1 FROM SG0200 WHERE "
    strAux = strAux & " SG02COD=" & "'" & rstpeticion("SG02COD_FIR").Value & "'"
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Medico = rstaux("SG02APE1").Value
  Else
    Medico = " "
  End If
  rstaux.Close
  Set rstaux = Nothing

  strlinea = rstpeticion("FR55FECENVIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55CODNECESUNID").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20DESFORMAG").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20CANTNECESQUIR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20PATHFORMAG").Value & Chr(vbKeyTab)
  strlinea = strlinea & Departamento & Chr(vbKeyTab)
  strlinea = strlinea & Seccion & Chr(vbKeyTab)
  strlinea = strlinea & Medico & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value
  
  auxDBGrid1(6).AddItem strlinea
  rsta.MoveNext
rstpeticion.Close
Set rstpeticion = Nothing
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(6).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Refrescar_Grid_Pac()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim strpeticion As String
Dim rstpeticion As rdoResultset
Dim rstaux As rdoResultset
Dim strAux As String
Dim Medico As String
Dim dpto As String
Dim NumHistoria As String
Dim NombrePac As String
Dim PriApel As String
Dim SegApel As String


Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(0).Redraw = False
auxDBGrid1(0).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & " FR2800.FR66CODPETICION"
strSelect = strSelect & ",FR2800.FR28DESPRODUCTO"
strSelect = strSelect & ",FR2800.FR28CANTIDAD"
strSelect = strSelect & ",FR2800.FR93CODUNIMEDIDA_2"
strSelect = strSelect & ",FR2800.FR28DOSIS"
strSelect = strSelect & ",FR2800.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR2800.FRG4CODFRECUENCIA"
strSelect = strSelect & ",FR2800.FR34CODVIA"
strSelect = strSelect & ",FR2800.FRH5CODPERIODICIDAD"
strSelect = strSelect & ",FR2800.FR28PATHFORMAG"
strSelect = strSelect & ",FR2800.FR28FECINICIO"
strSelect = strSelect & ",FR2800.FR28HORAINICIO"
strSelect = strSelect & ",FR2800.FR28FECFIN"
strSelect = strSelect & ",FR2800.FR28HORAFIN"
strSelect = strSelect & ",FR2800.FR28INSTRADMIN"
strSelect = strSelect & ",FR2800.FR28NUMLINEA"
strSelect = strSelect & ",FR2800.FR73CODPRODUCTO"

strSelect = strSelect & " FROM FR2800,FR6600"
strSelect = strSelect & " WHERE "

strSelect = strSelect & " FR2800.FR66CODPETICION=FR6600.FR66CODPETICION"
strSelect = strSelect & " AND FR6600.FR66INDFM=-1 AND FR2800.FR73CODPRODUCTO IS NOT NULL AND NVL(FR2800.FR28INDBLOQUEADA,0)=0 AND FR2800.FR28OPERACION='L' AND " & gstrWhere_PAC

strSelect = strSelect & " ORDER BY FR66CODPETICION DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF

  'Paciente,M�dico que firma,Fecha y Hora de Firma,Dpto.solicitante
  strpeticion = "SELECT CI21CODPERSONA,SG02COD_FEN,FR66FECFIRMMEDI,"
  strpeticion = strpeticion & "FR66HORAFIRMMEDI,AD02CODDPTO FROM FR6600 WHERE "
  strpeticion = strpeticion & " FR66CODPETICION=" & rsta("FR66CODPETICION").Value
  Set rstpeticion = objApp.rdoConnect.OpenResultset(strpeticion)
  'Persona
  strAux = "SELECT CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 "
  strAux = strAux & " WHERE CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not IsNull(rstaux.rdoColumns("CI22NUMHISTORIA").Value) Then
    NumHistoria = rstaux.rdoColumns("CI22NUMHISTORIA").Value
  Else
    NumHistoria = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22NOMBRE").Value) Then
    NombrePac = rstaux.rdoColumns("CI22NOMBRE").Value
  Else
    NombrePac = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22PRIAPEL").Value) Then
    PriApel = rstaux.rdoColumns("CI22PRIAPEL").Value
  Else
    PriApel = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22SEGAPEL").Value) Then
    SegApel = rstaux.rdoColumns("CI22SEGAPEL").Value
  Else
    SegApel = " "
  End If
  'M�dico que firma la petici�n
  strAux = "SELECT SG02APE1,SG02NUMCOLEGIADO FROM SG0200 WHERE "
  strAux = strAux & " SG02COD=" & "'" & rstpeticion.rdoColumns("SG02COD_FEN").Value & "'"
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not rstaux.EOF Then
    If Not IsNull(rstaux.rdoColumns("SG02APE1").Value) Then
      Medico = rstaux.rdoColumns("SG02APE1").Value
    Else
      Medico = " "
    End If
  Else
    Medico = " "
  End If
  'Departamento
  strAux = "SELECT AD02DESDPTO FROM AD0200 WHERE "
  strAux = strAux & " AD02CODDPTO=" & rstpeticion.rdoColumns("AD02CODDPTO").Value
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not IsNull(rstaux.rdoColumns("AD02DESDPTO").Value) Then
    dpto = rstaux.rdoColumns("AD02DESDPTO").Value
  Else
    dpto = " "
  End If
  rstaux.Close
  Set rstaux = Nothing

  strlinea = rstpeticion("FR66FECFIRMMEDI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66CODPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28CANTIDAD").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA_2").Value & Chr(vbKeyTab)
  strlinea = strlinea & NumHistoria & Chr(vbKeyTab)
  strlinea = strlinea & NombrePac & Chr(vbKeyTab)
  strlinea = strlinea & PriApel & Chr(vbKeyTab)
  strlinea = strlinea & SegApel & Chr(vbKeyTab)
  strlinea = strlinea & Medico & Chr(vbKeyTab)
  strlinea = strlinea & dpto & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28DOSIS").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRG4CODFRECUENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR34CODVIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH5CODPERIODICIDAD").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28PATHFORMAG").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28FECINICIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28HORAINICIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28FECFIN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28HORAFIN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28INSTRADMIN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value
  
  auxDBGrid1(0).AddItem strlinea
  rsta.MoveNext
rstpeticion.Close
Set rstpeticion = Nothing
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub


Private Sub pCargarSecciones(intDptoSel%)
Dim SQL$, qry As rdoQuery, rs As rdoResultset
Dim item As ListItem
    
    'se cargan los secciones del Dpto. seleccionado
    lvwSec.ListItems.Clear
    
    If SSTab1.Tab = 0 Then
      lvwSec.Visible = False
      Label1(1).Visible = False
      Exit Sub
    End If

    SQL = "SELECT AD41CODSECCION,AD41DESSECCION"
    SQL = SQL & " FROM AD4100"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If Not rs.EOF Then
      Set item = lvwSec.ListItems.Add(, , "TODAS")
      item.Tag = 0
      item.Selected = True
      Set item = lvwSec.ListItems.Add(, , "Sin Secci�n")
      item.Tag = intDptoSel
      lvwSec.Visible = True
      Label1(1).Visible = True
    Else
      lvwSec.Visible = False
      Label1(1).Visible = False
    End If
    
    Do While Not rs.EOF
      Set item = lvwSec.ListItems.Add(, , rs!AD41DESSECCION)
      item.Tag = rs!AD41CODSECCION
      rs.MoveNext
    Loop
    rs.Close
    qry.Close

End Sub


Private Sub Calcular_Registros()
Dim strSelFR28 As String
Dim qryFR28 As rdoQuery
Dim rstFR28 As rdoResultset
Dim strSelFR20 As String
Dim qryFR20 As rdoQuery
Dim rstFR20 As rdoResultset
Dim aux As Integer

Screen.MousePointer = vbHourglass

strSelFR28 = "SELECT COUNT(*) "
strSelFR28 = strSelFR28 & " FROM FR2800,FR6600"
strSelFR28 = strSelFR28 & " WHERE "
strSelFR28 = strSelFR28 & " FR2800.FR66CODPETICION=FR6600.FR66CODPETICION"
strSelFR28 = strSelFR28 & " AND FR6600.FR66INDFM=-1 AND FR2800.FR73CODPRODUCTO IS NOT NULL AND NVL(FR2800.FR28INDBLOQUEADA,0)=0 AND FR2800.FR28OPERACION='L' AND FR6600.FR26CODESTPETIC=?"
Set qryFR28 = objApp.rdoConnect.CreateQuery("", strSelFR28)
    
strSelFR20 = "SELECT COUNT(*) "
strSelFR20 = strSelFR20 & " FROM FR2000,FR5500"
strSelFR20 = strSelFR20 & " WHERE "
strSelFR20 = strSelFR20 & " FR2000.FR55CODNECESUNID=FR5500.FR55CODNECESUNID"
strSelFR20 = strSelFR20 & " AND FR5500.FR55INDFM=-1 AND FR2000.FR73CODPRODUCTO<>999999999 AND NVL(FR2000.FR20INDBLOQUEO,0)=0 AND FR5500.FR26CODESTPETIC=?"

Set qryFR20 = objApp.rdoConnect.CreateQuery("", strSelFR20)
    
      
  'Enviadas
  qryFR28(0) = 3
  Set rstFR28 = qryFR28.OpenResultset()
  qryFR20(0) = 3
  Set rstFR20 = qryFR20.OpenResultset()
  aux = rstFR28(0).Value + rstFR20(0).Value
  Option1(0).Caption = aux & "-Pendientes de Fabricar"
  rstFR28.Close
  Set rstFR28 = Nothing
  rstFR20.Close
  Set rstFR20 = Nothing
  
  'Fab.Iniciada
  qryFR28(0) = 14
  Set rstFR28 = qryFR28.OpenResultset()
  qryFR20(0) = 14
  Set rstFR20 = qryFR20.OpenResultset()
  aux = rstFR28(0).Value + rstFR20(0).Value
  Option1(1).Caption = aux & "-Fabricaci�n Iniciada"
  rstFR28.Close
  Set rstFR28 = Nothing
  rstFR20.Close
  Set rstFR20 = Nothing
      
  'Fab.Terminada
  qryFR28(0) = 15
  Set rstFR28 = qryFR28.OpenResultset()
  qryFR20(0) = 15
  Set rstFR20 = qryFR20.OpenResultset()
  aux = rstFR28(0).Value + rstFR20(0).Value
  Option1(2).Caption = aux & "-Fabricaci�n Terminada"
  rstFR28.Close
  Set rstFR28 = Nothing
  rstFR20.Close
  Set rstFR20 = Nothing
Screen.MousePointer = vbDefault
End Sub
