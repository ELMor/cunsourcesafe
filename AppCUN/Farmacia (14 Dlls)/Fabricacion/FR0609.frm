VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmAnalizDifMez 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Mezclas Farmacia. Analizar Diferencias"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdRepEtiq 
      Caption         =   "Repetir Etiquetas Carro"
      Height          =   315
      Left            =   4920
      TabIndex        =   30
      Top             =   7680
      Width           =   2655
   End
   Begin VB.CommandButton cmdImprTodos 
      Caption         =   "Imprimir Selecci�n"
      Height          =   675
      Left            =   10680
      TabIndex        =   29
      Top             =   3240
      Width           =   1095
   End
   Begin VB.Frame frmmensaje 
      Height          =   1695
      Left            =   3600
      TabIndex        =   27
      Top             =   3480
      Visible         =   0   'False
      Width           =   5535
      Begin VB.Label lblmensaje 
         Caption         =   "Actualizando el almac�n. Espere un momento, por favor..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   480
         TabIndex        =   28
         Top             =   480
         Visible         =   0   'False
         Width           =   4815
      End
   End
   Begin VB.CheckBox Checkcito 
      Caption         =   "CITO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   255
      Left            =   10680
      TabIndex        =   26
      Top             =   2880
      Width           =   975
   End
   Begin VB.CheckBox Checkmezcla 
      Caption         =   "MIV"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   255
      Left            =   10680
      TabIndex        =   25
      Top             =   2520
      Value           =   1  'Checked
      Width           =   855
   End
   Begin VB.CommandButton cmdSelTodos 
      Caption         =   "Sel.Todos"
      Height          =   315
      Left            =   10680
      TabIndex        =   22
      Top             =   1920
      Width           =   1095
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   7920
      Top             =   4200
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdanalizardiferencias 
      Caption         =   "Analizar Diferencias"
      Height          =   255
      Left            =   4920
      TabIndex        =   21
      Top             =   4200
      Width           =   2175
   End
   Begin VB.CommandButton cmdtransdatos 
      Caption         =   "Transmitir Datos al Lector"
      Height          =   255
      Left            =   8760
      TabIndex        =   20
      Top             =   7800
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   10260
      Begin TabDlg.SSTab tabTab1 
         Height          =   3135
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   5530
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0609.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(7)"
         Tab(0).Control(1)=   "txtText1(6)"
         Tab(0).Control(2)=   "txtText1(5)"
         Tab(0).Control(3)=   "txtText1(2)"
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(6)=   "txtText1(4)"
         Tab(0).Control(7)=   "txtText1(3)"
         Tab(0).Control(8)=   "lblLabel1(6)"
         Tab(0).Control(9)=   "lblLabel1(1)"
         Tab(0).Control(10)=   "lblLabel1(3)"
         Tab(0).Control(11)=   "lblLabel1(0)"
         Tab(0).Control(12)=   "lblLabel1(4)"
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0609.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74HORASALIDA"
            Height          =   330
            Index           =   7
            Left            =   -73440
            TabIndex        =   7
            Tag             =   "Hora"
            Top             =   2640
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74DIA"
            Height          =   330
            Index           =   6
            Left            =   -74640
            TabIndex        =   6
            Tag             =   "D�a"
            Top             =   2640
            Width           =   500
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR87DESESTCARRO"
            Height          =   330
            Index           =   5
            Left            =   -74040
            TabIndex        =   5
            Tag             =   "Estado Carro"
            Top             =   1920
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR87CODESTCARRO"
            Height          =   330
            Index           =   2
            Left            =   -74640
            TabIndex        =   4
            Tag             =   "C�d.Estado Carro"
            Top             =   1920
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   1
            Left            =   -74040
            TabIndex        =   1
            Tag             =   "Servicio"
            Top             =   480
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   -74640
            TabIndex        =   0
            Tag             =   "C�d.Servicio"
            Top             =   480
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR07DESCARRO"
            Height          =   330
            Index           =   4
            Left            =   -74040
            TabIndex        =   3
            Tag             =   "Carro"
            Top             =   1200
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR07CODCARRO"
            Height          =   330
            Index           =   3
            Left            =   -74640
            TabIndex        =   2
            Tag             =   "C�d.Carro"
            Top             =   1200
            Width           =   400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3105
            Index           =   2
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   5477
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   -73440
            TabIndex        =   19
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D�a Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74640
            TabIndex        =   18
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74640
            TabIndex        =   17
            Top             =   1680
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74640
            TabIndex        =   15
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -74640
            TabIndex        =   13
            Top             =   960
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Diferencias"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3000
      Index           =   0
      Left            =   0
      TabIndex        =   14
      Top             =   4560
      Width           =   11895
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2505
         Index           =   0
         Left            =   75
         TabIndex        =   16
         Top             =   360
         Width           =   11745
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "BLANCO"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0609.frx":0038
         stylesets(1).Name=   "AZUL"
         stylesets(1).BackColor=   16776960
         stylesets(1).Picture=   "FR0609.frx":0054
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20717
         _ExtentY        =   4419
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cbohora 
      Height          =   315
      Left            =   10680
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   1440
      Width           =   975
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ";"
      DefColWidth     =   9
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "Hora Salida"
      Columns(0).Name =   "Hora Salida"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1720
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Hora Salida"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   10680
      TabIndex        =   24
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAnalizDifMez"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmAnalizDifMez (FR0609.FRM)                               *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: analizar diferencias                                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim Insertar_en_el_Carro As Boolean
Dim hora_de_toma As Double
Dim gintlinea As Integer
Dim gstrEstilo As String
Dim gPet As Variant
Dim gintHora As Integer


Private Sub cbohora_CloseUp()

Dim strFechaSer As String
Dim rstFechaSer As rdoResultset
Dim fecha As Date
Dim intDiaSem As Integer
Dim DiaSem(7) As String
    
strFechaSer = "SELECT SYSDATE FROM DUAL"
Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
While rstFechaSer.StillExecuting
Wend

fecha = rstFechaSer(0).Value
rstFechaSer.Close
intDiaSem = WeekDay(fecha, vbMonday)
DiaSem(1) = "LUN"
DiaSem(2) = "MAR"
DiaSem(3) = "MIE"
DiaSem(4) = "JUE"
DiaSem(5) = "VIE"
DiaSem(6) = "SAB"
DiaSem(7) = "DOM"

If cbohora.Text <> "" Then
  Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
  objWinInfo.objWinActiveForm.strWhere = "FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
  objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cbohora, ",", ".", 1)
  objWinInfo.DataRefresh
  'grdDBGrid1(0).RemoveAll
End If

End Sub

Private Sub Checkcito_Click()
If Checkcito.Value = 1 Then
   Checkmezcla.Value = 0
   Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
   objWinInfo.objWinActiveForm.strWhere = "FR33FECCARGA=TRUNC(SYSDATE)" & _
                  " AND FR33INDMEZCLA=-1 AND (FR33INDNODIF=0 OR FR33CAMBIOCAMA IS NOT NULL)" & _
                  " AND FR33INDCITOSTATICO=-1"
   objWinInfo.DataRefresh
ElseIf Checkmezcla.Value = 0 And Checkcito.Value = 0 Then
  Checkmezcla.Value = 1
   Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
   objWinInfo.objWinActiveForm.strWhere = "FR33FECCARGA=TRUNC(SYSDATE)" & _
                  " AND FR33INDMEZCLA=-1 AND (FR33INDNODIF=0 OR FR33CAMBIOCAMA IS NOT NULL)" & _
                  " AND (FR33INDCITOSTATICO=0 OR FR33INDCITOSTATICO IS NULL)"
   objWinInfo.DataRefresh
End If
End Sub

Private Sub Checkmezcla_Click()
If Checkmezcla.Value = 1 Then
   Checkcito.Value = 0
   Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
   objWinInfo.objWinActiveForm.strWhere = "FR33FECCARGA=TRUNC(SYSDATE)" & _
                  " AND FR33INDMEZCLA=-1 AND (FR33INDNODIF=0 OR FR33CAMBIOCAMA IS NOT NULL)" & _
                  " AND (FR33INDCITOSTATICO=0 OR FR33INDCITOSTATICO IS NULL)"
   objWinInfo.DataRefresh
ElseIf Checkmezcla.Value = 0 And Checkcito.Value = 0 Then
  Checkcito.Value = 1
   Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
   objWinInfo.objWinActiveForm.strWhere = "FR33FECCARGA=TRUNC(SYSDATE)" & _
                  " AND FR33INDMEZCLA=-1 AND (FR33INDNODIF=0 OR FR33CAMBIOCAMA IS NOT NULL)" & _
                  " AND FR33INDCITOSTATICO=-1"
   objWinInfo.DataRefresh
End If
End Sub

Private Sub cmdImprTodos_Click()

'Call objWinInfo_cwPrint("Peticiones del Carro")
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
tlbToolbar1.Buttons(6).Enabled = True
Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6))

End Sub


Private Sub cmdSelTodos_Click()
Dim i As Integer

cmdSelTodos.Enabled = False

grdDBGrid1(2).Redraw = False
grdDBGrid1(2).SelBookmarks.RemoveAll
grdDBGrid1(2).MoveFirst ' Position at the first row
For i = 0 To grdDBGrid1(2).Rows
  grdDBGrid1(2).SelBookmarks.Add grdDBGrid1(2).Bookmark
  grdDBGrid1(2).MoveNext
Next i
grdDBGrid1(2).Redraw = True
cmdSelTodos.Enabled = True
End Sub


Private Sub grdDBGrid1_Click(Index As Integer)
    If Index = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub


'Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
'Dim i As Integer
'Dim v As Integer
'Dim strProdCit As String
'Dim qryProdCit As rdoQuery
'Dim rstProdCit As rdoResultset
'Dim blncit As Boolean
'
'If Index = 0 Then
'  If grdDBGrid1(0).Rows > 0 Then
'    v = grdDBGrid1(0).Cols - 1
'    If gintlinea <> grdDBGrid1(0).Columns("L�nea").Value Then
'      If gstrEstilo = "BLANCO" Then
'        gstrEstilo = "AZUL"
'      Else
'        gstrEstilo = "BLANCO"
'      End If
'    Else
'      If gintHora <> grdDBGrid1(0).Columns("Toma").Value Then
'        If gstrEstilo = "BLANCO" Then
'          gstrEstilo = "AZUL"
'        Else
'          gstrEstilo = "BLANCO"
'        End If
'      Else
'        If gPet <> grdDBGrid1(0).Columns("Petici�n").Value Then
'          If gstrEstilo = "BLANCO" Then
'            gstrEstilo = "AZUL"
'          Else
'            gstrEstilo = "BLANCO"
'          End If
'        End If
'      End If
'    End If
'
'    gintHora = grdDBGrid1(0).Columns("Toma").Value
'    gintlinea = grdDBGrid1(0).Columns("L�nea").Value
'    gPet = grdDBGrid1(0).Columns("Petici�n").Value
'    For i = 0 To v
'      grdDBGrid1(0).Columns(i).CellStyleSet gstrEstilo
'    Next i
'End If
'End If
'End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
If strFormName = "Peticiones del Carro" And strCtrlName = "grdDBGrid1(0).Petici�n" Then
    aValues(2) = grdDBGrid1(0).Columns(20).Value
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
If objWinInfo.objWinActiveForm.strName = "Carros" Then
    tlbToolbar1.Buttons(3).Enabled = False
    mnuDatosOpcion(20).Enabled = False
End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub





Private Sub Form_Activate()
If txtText1(3).Text <> "" Then
    cmdanalizardiferencias.Enabled = True
    cmdtransdatos.Enabled = True
Else
    cmdanalizardiferencias.Enabled = False
    cmdtransdatos.Enabled = False
End If
If objWinInfo.objWinActiveForm.strName = "Carros" Then
    tlbToolbar1.Buttons(3).Enabled = False
    mnuDatosOpcion(20).Enabled = False
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String
Dim DiaSem(7) As String
Dim fecha As Date
Dim strFechaSer As String
Dim rstFechaSer As rdoResultset
Dim intDiaSem As Integer
Dim stra As String
Dim rsta As rdoResultset

strFechaSer = "SELECT SYSDATE FROM DUAL"
Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
While rstFechaSer.StillExecuting
Wend

fecha = rstFechaSer(0).Value
intDiaSem = WeekDay(fecha, vbMonday)
DiaSem(1) = "LUN"
DiaSem(2) = "MAR"
DiaSem(3) = "MIE"
DiaSem(4) = "JUE"
DiaSem(5) = "VIE"
DiaSem(6) = "SAB"
DiaSem(7) = "DOM"
  
cbohora.RemoveAll
stra = "SELECT DISTINCT FR74HORASALIDA FROM FR0701J " & _
" WHERE FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While rsta.StillExecuting
Wend

While (Not rsta.EOF)
    Call cbohora.AddItem(rsta.rdoColumns("FR74HORASALIDA").Value)
    rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
cbohora.MoveFirst
cbohora = cbohora.Columns(0).Value
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Carros"
      
    .strTable = "FR0701J"
    .intAllowance = cwAllowReadOnly
    'estado=2 <Se ha comenzado la carga>
    .strWhere = "FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
    .strWhere = .strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cbohora, ",", ".", 1)

    .intCursorSize = 0
    
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    Call .FormAddOrderField("FR74DIA", cwAscending)
    Call .FormAddOrderField("FR74HORASALIDA", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Carros")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "C�digo Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR07DESCARRO", "Descripci�n Carro", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO", "C�d.Estado Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR87DESESTCARRO", "Desc.Estado Carro", cwString)
    Call .FormAddFilterWhere(strKey, "FR74DIA", "D�a Salida", cwString)
    Call .FormAddFilterWhere(strKey, "FR74HORASALIDA", "Hora Salida", cwNumeric)
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Peticiones del Carro"
    .intAllowance = cwAllowReadOnly
    
    .intCursorSize = 0
    
    .strTable = "FR3300"
    If Checkcito.Value = 1 Then
      .strWhere = "FR33FECCARGA=TRUNC(SYSDATE)" & _
                  " AND FR33INDMEZCLA=-1" & _
                  " AND (FR33INDNODIF=0 OR FR33CAMBIOCAMA IS NOT NULL)" & _
                  " AND FR33INDCITOSTATICO=-1"
    Else
      .strWhere = "FR33FECCARGA=TRUNC(SYSDATE)" & _
                  " AND FR33INDMEZCLA=-1" & _
                  " AND (FR33INDNODIF=0 OR FR33CAMBIOCAMA IS NOT NULL)" & _
                  " AND (FR33INDCITOSTATICO=0 OR FR33INDCITOSTATICO IS NULL)"
    End If
    
    Call .objPrinter.Add("FR6091", "Diferencias del Carro, Mezclas")
    Call .objPrinter.Add("FR6092", "Diferencias del Carro, Citost�ticos")
    
    Call .FormAddOrderField("FR07CODCARRO", cwAscending)
    Call .FormAddOrderField("FR33HORATOMA", cwAscending)
    Call .FormAddOrderField("FR66CODPETICION", cwAscending)
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddOrderField("FR33GRUPO", cwAscending)
    
    Call .FormAddRelation("FR07CODCARRO", txtText1(3))
    Call .FormAddRelation("FR33HORACARGA", txtText1(7))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones del Carro")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD15CODCAMA", "Cama", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR33CANTIDAD", "Cantidad", cwNumeric)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "Carro", "FR07CODCARRO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Cama", "GCFN06(AD15CODCAMA)", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Persona", "CI21CODPERSONA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7) 'historia
    Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25) 'nombre
    Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25) 'apellido 1�
    Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25) 'apellido 2�
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Int", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Cant", "FR33CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Producto", "FR33DESPROD", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "FR33FORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR33DOSIS", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "FR33UNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Toma", "FR33HORATOMA", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Estado", "FR33MOTDIFERENCIA", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Petici�n", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Motivo", "FR33CAMBIOCAMA", cwString, 20)

    
    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    
    .CtrlGetInfo(txtText1(2)).blnInGrid = False
    .CtrlGetInfo(txtText1(5)).blnInGrid = False
    
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(18)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(19)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnInFind = True

  
    .CtrlGetInfo(txtText1(2)).blnInGrid = False 'c�d.estado carro
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(8), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(9), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), grdDBGrid1(0).Columns(11), "FR73CODINTFAR")

    grdDBGrid1(0).Columns(3).Visible = False
    grdDBGrid1(0).Columns(19).Visible = False
    grdDBGrid1(0).Columns(20).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
 grdDBGrid1(2).Columns("C�d.Servicio").Width = 1200
 grdDBGrid1(2).Columns("Servicio").Width = 2950
 grdDBGrid1(2).Columns("C�d.Carro").Width = 900
 grdDBGrid1(2).Columns("Carro").Width = 2950
 grdDBGrid1(2).Columns("D�a").Width = 600
 grdDBGrid1(2).Columns("Hora").Width = 600
  
 grdDBGrid1(0).Columns(4).Width = 700 'cama
 grdDBGrid1(0).Columns(5).Width = 1000 'c�d.persona
 grdDBGrid1(0).Columns(6).Width = 950 'historia
 grdDBGrid1(0).Columns(7).Width = 800 'nombre
 grdDBGrid1(0).Columns(8).Width = 1300 'apellido 1�
 grdDBGrid1(0).Columns(9).Width = 1300 'apellido 2�
 grdDBGrid1(0).Columns(10).Width = 1000 'c�d.prod
 grdDBGrid1(0).Columns(11).Width = 700 'c�d.interno
 grdDBGrid1(0).Columns(13).Width = 1850 'desc producto
 grdDBGrid1(0).Columns(14).Width = 450  'F.F
 grdDBGrid1(0).Columns(15).Width = 700  'Dosis
 grdDBGrid1(0).Columns(16).Width = 650  'U.M
 grdDBGrid1(0).Columns(12).Width = 500 'cantidad
 grdDBGrid1(0).Columns(17).Width = 550 'hora de toma
 grdDBGrid1(0).Columns(18).Width = 800 'estado
 
 grdDBGrid1(0).Columns(5).Visible = False 'c�d.producto
 grdDBGrid1(0).Columns(10).Visible = False 'c�d.paciente
 
 
 Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
gintlinea = 0
gstrEstilo = "BLANCO"

blnImpresoraSeleccionada = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'Dim intReport As Integer
'Dim objPrinter As clsCWPrinter
'Dim blnHasFilter As Boolean
'Dim strWhere As String
'Dim strFilter As String
 
'  If strFormName = "Peticiones del Carro" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      strWhere = "WHERE FR0701J." & Chr(34) & "FR87CODESTCARRO" & Chr(34) & "=6 AND FR0701J." & Chr(34) & "FR74DIA" & Chr(34) & "='" & grdDBGrid1(2).Columns(6).Value & "'" & " AND FR0701J." & Chr(34) & "FR74HORASALIDA" & Chr(34) & " = " & grdDBGrid1(2).Columns(7).Value & " AND FR0701J." & Chr(34) & "FR07CODCARRO" & Chr(34) & "=" & grdDBGrid1(2).Columns(3).Value
'      strFilter = ""
'      Call objPrinter.ShowReport(strWhere, strFilter)
'    End If
'    Set objPrinter = Nothing
'  End If
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean

Call objWinInfo.FormPrinterDialog(True, "")
Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
intReport = objPrinter.Selected
If intReport > 0 Then
  Select Case intReport
    Case 1
      Call Imprimir("FR6091.RPT", 0)
    Case 2
      Call Imprimir("FR6092.RPT", 0)
  End Select
End If
Set objPrinter = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  If intIndex = 1 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
If intIndex = 3 Then
    If txtText1(3).Text <> "" Then
        cmdanalizardiferencias.Enabled = True
    Else
        cmdanalizardiferencias.Enabled = False
    End If
End If
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub Imprimir(strListado As String, intDes As Integer)
 'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strCarros As String
  Dim nTotal As Long
  Dim nTotalSelRows As Integer
  Dim i As Integer
  Dim bkmrk As Variant
  
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.getdatabasepasswordold
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  
  strCarros = ""
  nTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(2).SelBookmarks(i)
      strCarros = strCarros & grdDBGrid1(2).Columns("C�d.Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grdDBGrid1(2).Columns("C�d.Carro").Value
  End If
  strWhere = ""
  
 If Checkcito.Value = 1 Then
    strWhere = strWhere & " {FR3304J.HORACAR} = " & objGen.ReplaceStr(cbohora, ",", ".", 1)
    strWhere = strWhere & " AND {FR3304J.HORASAL} = " & objGen.ReplaceStr(cbohora, ",", ".", 1)
    strWhere = strWhere & " AND " & CrearWhere("{FR3304J.CODCARRO}", "(" & strCarros & ")")
    strWhere = strWhere & " AND {FR3304J.FR33INDCITOSTATICO}=-1"
 Else
    strWhere = strWhere & " {FR3304J.HORACAR} = " & objGen.ReplaceStr(cbohora, ",", ".", 1)
    strWhere = strWhere & " AND {FR3304J.HORASAL} = " & objGen.ReplaceStr(cbohora, ",", ".", 1)
    strWhere = strWhere & " AND " & CrearWhere("{FR3304J.CODCARRO}", "(" & strCarros & ")")
    strWhere = strWhere & " AND {FR3304J.FR33INDCITOSTATICO}=0"
 End If
  
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1

Err_imp1:

End Sub


Private Function formatear(cadena As String, espacios As Integer, izq As Boolean, cortar As Boolean) As String
Dim i As Integer
Dim strMask As String
Dim strAux As String
  
  strAux = cadena
  If cortar Then
    If Len(strAux) > espacios Then
      strAux = Left$(strAux, espacios)
    End If
  End If
  strMask = ""
  For i = 1 To espacios
    strMask = strMask & "@"
  Next i
  If izq Then
    formatear = Format(strAux, "!" & strMask)
  Else
    formatear = Format(strAux, strMask)
  End If

End Function

Private Function Texto_Etiqueta(cadena As String, x As Integer, y As Integer, rot As Integer, letra As String, horMult As Integer, vertMult As Integer, TipoImg As String) As String
 
Texto_Etiqueta = "A" & x & "," & y & "," & rot & "," & letra & "," & horMult & "," & vertMult & "," & TipoImg & "," & Chr(34) & transfCadena(cadena) & Chr(34)

End Function

Private Function transfCadena(cadena As String) As String
Dim i As Integer

  For i = 1 To Len(cadena)
    Select Case Mid(cadena, i, 1)
      Case "�"
        Mid(cadena, i, 1) = Chr$(160)
      Case "�"
        Mid(cadena, i, 1) = Chr$(130)
      Case "�"
        Mid(cadena, i, 1) = Chr$(161)
      Case "�"
        Mid(cadena, i, 1) = Chr$(162)
      Case "�"
        Mid(cadena, i, 1) = Chr$(163)
      Case "�"
        Mid(cadena, i, 1) = Chr$(168)
      Case "�"
        Mid(cadena, i, 1) = Chr$(164)
      Case "�"
        Mid(cadena, i, 1) = Chr$(165)
      Case "�"
        Mid(cadena, i, 1) = Chr$(129)
      Case "�"
        Mid(cadena, i, 1) = Chr$(166)
      Case "�"
        Mid(cadena, i, 1) = Chr$(167)
    End Select
  Next

  transfCadena = cadena

End Function

Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function




Private Sub cmdanalizardiferencias_Click()
Dim strUpdate As String
Dim i As Integer
Dim qryprod As rdoQuery
Dim rstprod As rdoResultset
Dim strprod As String
Dim hora As String
Dim strFF As String
Dim strDosis As String
Dim strUM As String
Dim cantidad As String
Dim gcodproducto
Dim gdescproducto As String
Dim grupo As Integer
Dim strFF_dil As String
Dim strDosis_dil As String
Dim strUM_dil As String
Dim cantidad_dil As String
Dim gcodproducto_dil
Dim gdescproducto_dil As String
Dim grupo_dil As Integer
Dim strFF_mez As String
Dim strDosis_mez As String
Dim strUM_mez As String
Dim cantidad_mez As String
Dim gcodproducto_mez
Dim gdescproducto_mez As String
Dim grupo_mez As Integer
Dim numlinea As Integer
Dim stra As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim rst33 As rdoResultset
Dim str33 As String
Dim qry33 As rdoQuery
Dim strinsert33 As String
Dim qryinsert33 As rdoQuery
Dim strDispensado As String
Dim rstDispensado As rdoResultset
Dim qryDispensado As rdoQuery
Dim mintisel As Integer
Dim mvarBkmrk As Variant
Dim mintNTotalSelRows As Integer
Dim carrosrestantes As Integer
Dim strcambiocama As String
Dim rstcambiocama As rdoResultset
Dim qrycambiocama As rdoQuery
Dim strFR7400 As String
Dim rstFR7400 As rdoResultset
Dim qryFR7400 As rdoQuery
Dim strFR7400_2 As String
Dim rstFR7400_2 As rdoResultset
Dim qryFR7400_2 As rdoQuery
Dim strDiaSiguiente As String
Dim curHoraSiguiente As Currency
Dim blnValida As Boolean
Dim strDelete As String
Dim indMIVCito As Integer
Dim strMIV As String
Dim qryMIV As rdoQuery
Dim rstMIV As rdoResultset
Dim blnInsertar As Boolean
Dim AUXFR28NUMLINEA
Dim blnCambiaAlgo As Boolean
Dim updEstCarMIV As String
Dim qryupdEstCarMIV As rdoQuery
Dim updEstCarCIT  As String
Dim qryupdEstCarCIT As rdoQuery

If cbohora = "" Then
  Exit Sub
End If

If Checkcito.Value = 1 Then
  indMIVCito = -1
Else
  indMIVCito = 0
End If

'transforma la coma de separaci�n de los decimales por un punto
'de la hora a la que sale el carro
hora = objGen.ReplaceStr(cbohora, ",", ".", 1)

Screen.MousePointer = vbHourglass
cmdanalizardiferencias.Enabled = False
Me.Enabled = False

lblmensaje.Visible = True
frmmensaje.Visible = True

mintNTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
carrosrestantes = grdDBGrid1(2).SelBookmarks.Count

strDispensado = "SELECT COUNT(*) FROM FR3300 WHERE "
strDispensado = strDispensado & "FR07CODCARRO=?"
strDispensado = strDispensado & " AND FR33INDMEZCLA=? AND FR33FECCARGA=TRUNC(SYSDATE) "
strDispensado = strDispensado & " AND FR33HORACARGA=" & hora
strDispensado = strDispensado & " AND FR33INDCITOSTATICO=?"
Set qryDispensado = objApp.rdoConnect.CreateQuery("", strDispensado)

'se vuelca el contenido de FR0600 en FR3300, todas las tuplas con motivo "A�adido"
strinsert33 = "INSERT INTO FR3300(FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA,"
strinsert33 = strinsert33 & "FR73CODPRODUCTO,FR66CODPETICION,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA,"
strinsert33 = strinsert33 & "FR33INDINCPAC,FR33INDINCPROD,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33FECCARGA,FR33HORACARGA,"
strinsert33 = strinsert33 & "FR28NUMLINEA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA,FR33DESPROD,FR33GRUPO,FR33HORATOMA,"
strinsert33 = strinsert33 & "FR33INDMEZCLA,FR33INDNODIF,FR33CAMBIOCAMA,FR33INDCITOSTATICO,FR33CODIGO)"
strinsert33 = strinsert33 & " (SELECT FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA,FR73CODPRODUCTO,"
strinsert33 = strinsert33 & "FR66CODPETICION,FR06CANTIDAD,FR06INDCARGREA,FR06INDINCCAMA,FR06INDINCPACI,"
strinsert33 = strinsert33 & "FR06INDINCPROD," & "'A�adido',0,0," & "FR06FECCARGA,FR06HORACARGA,FR28NUMLINEA,"
strinsert33 = strinsert33 & "FR06FORMFAR,FR06DOSIS,FR06UNIMEDIDA,FR06DESPROD,FR06GRUPO,FR06HORATOMA,FR06INDMEZCLA,0,FR06CAMBIOCAMA,FR06INDCITOSTATICO,FR33CODIGO_SEQUENCE.NEXTVAL"
strinsert33 = strinsert33 & " FROM FR0600 WHERE "
strinsert33 = strinsert33 & " FR07CODCARRO=?"
strinsert33 = strinsert33 & " AND FR06INDMEZCLA=-1 AND FR06INDCAMBCAMA=0"
strinsert33 = strinsert33 & " AND FR06INDCITOSTATICO=" & indMIVCito
strinsert33 = strinsert33 & " AND FR06FECCARGA=TRUNC(SYSDATE)"
strinsert33 = strinsert33 & " AND FR06HORACARGA=" & hora & ")"
Set qryinsert33 = objApp.rdoConnect.CreateQuery("", strinsert33)
 
stra = "SELECT FRG500.FRG5HORA,"
stra = stra & "FR3200.FR66CODPETICION,"
stra = stra & "FR3200.FR28NUMLINEA,"
stra = stra & "FR3200.FR73CODPRODUCTO,"
stra = stra & "FR3200.FR32NUMMODIFIC,"
stra = stra & "FR3200.FR28DOSIS,"
stra = stra & "FR3200.FR34CODVIA,"
stra = stra & "FR3200.FR32VOLUMEN,"
stra = stra & "FR3200.FR32TIEMINFMIN,"
stra = stra & "FR3200.FRG4CODFRECUENCIA,"
stra = stra & "FR3200.FR32OBSERVFARM,"
stra = stra & "FR3200.SG02COD_FRM,"
stra = stra & "FR3200.FR32FECMODIVALI,"
stra = stra & "FR3200.FR32HORAMODIVALI,"
stra = stra & "FR3200.FR32INDSN,"
stra = stra & "FR3200.FR93CODUNIMEDIDA,"
stra = stra & "FR3200.FR32INDISPEN,"
stra = stra & "FR3200.FR32INDCOMIENINMED,"
stra = stra & "FR3200.FRH5CODPERIODICIDAD,"
stra = stra & "FR3200.FRH7CODFORMFAR,"
stra = stra & "FR3200.FR32OPERACION,"
stra = stra & "FR3200.FR32CANTIDAD,"
stra = stra & "FR3200.FR32FECINICIO,"
stra = stra & "FR3200.FR32HORAINICIO,"
stra = stra & "FR3200.FR32FECFIN,"
stra = stra & "FR3200.FR32HORAFIN,"
stra = stra & "FR3200.FR32INDVIAOPC,"
stra = stra & "FR3200.FR73CODPRODUCTO_DIL,"
stra = stra & "FR3200.FR32CANTIDADDIL,"
stra = stra & "FR3200.FR32TIEMMININF,"
stra = stra & "FR3200.FR32REFERENCIA,"
stra = stra & "FR3200.FR32INDESTFAB,"
stra = stra & "FR3200.FR32CANTDISP,"
stra = stra & "FR3200.R32UBICACION,"
stra = stra & "FR3200.FR32INDPERF,"
stra = stra & "FR3200.FR32INSTRADMIN,"
stra = stra & "FR3200.FR32VELPERFUSION,"
stra = stra & "FR3200.FR32VOLTOTAL,"
stra = stra & "FR3200.FR73CODPRODUCTO_2,"
stra = stra & "FR3200.FRH7CODFORMFAR_2,"
stra = stra & "FR3200.FR32DOSIS_2,"
stra = stra & "FR3200.FR93CODUNIMEDIDA_2,"
stra = stra & "FR3200.FR32DESPRODUCTO,"
stra = stra & "FR3200.FR32UBICACION,"
stra = stra & "FR3200.FR32DIATOMAULT,"
stra = stra & "FR3200.FR32HORATOMAULT,"
stra = stra & "FR3200.FR32VOLUMEN_2,"
stra = stra & "FR3200.FR32PATHFORMAG,"
stra = stra & "FR3200.FR32INDDISPPRN,"
stra = stra & "FR6600.FR26CODESTPETIC,"
stra = stra & "FR6600.CI21CODPERSONA,"
stra = stra & "FR6600.AD01CODASISTENCI,"
stra = stra & "FR6600.AD07CODPROCESO,"
stra = stra & "AD1500.AD15CODCAMA,"

stra = stra & "TRUNC(SYSDATE)-TRUNC(FR32FECINICIO) DIASDIFINI,"
stra = stra & "TRUNC(SYSDATE+1)-TRUNC(FR32FECINICIO) DIASDIFINI_2,"
stra = stra & "TRUNC(NVL(FR32FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))-TRUNC(SYSDATE) DIASDIFFIN,"
stra = stra & "TRUNC(NVL(FR32FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))-TRUNC(SYSDATE+1) DIASDIFFIN_2,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE,'d')) DIAHOY,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE,'dd')) DIAMES,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE+1,'d')) DIAHOY_2,"
stra = stra & "TO_NUMBER(TO_CHAR(SYSDATE+1,'dd')) DIAMES_2"

stra = stra & " FROM FR6600,AD1500,FR3200,FRG500,FRJ900"
stra = stra & " WHERE AD1500.AD07CODPROCESO=FR6600.AD07CODPROCESO"
stra = stra & " AND AD1500.AD01CODASISTENCI=FR6600.AD01CODASISTENCI"
stra = stra & " AND FR6600.FR66CODPETICION=fr3200.FR66CODPETICION"
stra = stra & " AND FRG500.FRG4CODFRECUENCIA=FR3200.FRG4CODFRECUENCIA"
stra = stra & " AND AD1500.AD15CODCAMA=FRJ900.AD15CODCAMA"
stra = stra & " AND FRJ900.FR07CODCARRO=?"
stra = stra & " AND FR6600.FR26CODESTPETIC=?"
stra = stra & " AND FR6600.FR66INDOM=?"
stra = stra & " AND FR3200.FR32INDPERF=?"
stra = stra & " AND FR3200.FR32OPERACION IN ('/','M','P','F')"
Set qrya = objApp.rdoConnect.CreateQuery("", stra)

strprod = "SELECT FR73CODPRODUCTO,FR73DESPRODUCTO,"
strprod = strprod & "FRH7CODFORMFAR,FR73DOSIS,FR73VOLUMEN "
strprod = strprod & " FROM FR7300 WHERE "
strprod = strprod & " FR73CODPRODUCTO=?"
Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)
 
strFR7400 = "SELECT FR74DIA,FR74HORASALIDA"
strFR7400 = strFR7400 & " FROM FR7400"
strFR7400 = strFR7400 & " WHERE FR07CODCARRO = ? AND FR87CODESTCARRO<>4"
strFR7400 = strFR7400 & " AND DECODE(FR7400.FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7)=TO_NUMBER(TO_CHAR(SYSDATE,'d'))"
strFR7400 = strFR7400 & " ORDER BY DECODE(FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7),FR74HORASALIDA"
Set qryFR7400 = objApp.rdoConnect.CreateQuery("", strFR7400)

strFR7400_2 = "SELECT FR74DIA,FR74HORASALIDA"
strFR7400_2 = strFR7400_2 & " FROM FR7400"
strFR7400_2 = strFR7400_2 & " WHERE FR07CODCARRO = ? AND FR87CODESTCARRO<>4"
strFR7400_2 = strFR7400_2 & " AND DECODE(FR7400.FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7)=TO_NUMBER(TO_CHAR(SYSDATE+1,'d'))"
strFR7400_2 = strFR7400_2 & " ORDER BY DECODE(FR74DIA,'LUN',1,'MAR',2,'MIE',3,'JUE',4,'VIE',5,'SAB',6,7),FR74HORASALIDA"
Set qryFR7400_2 = objApp.rdoConnect.CreateQuery("", strFR7400_2)
 
strMIV = "SELECT COUNT(*) FROM FR7300 WHERE FR73CODPRODUCTO=?"
strMIV = strMIV & " AND FR00CODGRPTERAP LIKE 'L%'"
Set qryMIV = objApp.rdoConnect.CreateQuery("", strMIV)
 
'se cambia el estado del carro (estado=6 Analizado las diferencias)
updEstCarMIV = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=6 WHERE "
updEstCarMIV = updEstCarMIV & "FR07CODCARRO=?"
updEstCarMIV = updEstCarMIV & " AND FR74DIA=?"
updEstCarMIV = updEstCarMIV & " AND FR74HORASALIDA=" & hora
Set qryupdEstCarMIV = objApp.rdoConnect.CreateQuery("", updEstCarMIV)

updEstCarCIT = "UPDATE FR7400 SET FR87CODESTCARRO_CIT=6 WHERE "
updEstCarCIT = updEstCarCIT & "FR07CODCARRO=?"
updEstCarCIT = updEstCarCIT & " AND FR74DIA=?"
updEstCarCIT = updEstCarCIT & " AND FR74HORASALIDA=" & hora
Set qryupdEstCarCIT = objApp.rdoConnect.CreateQuery("", updEstCarCIT)
 
For mintisel = 0 To mintNTotalSelRows - 1
  
  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False

  mvarBkmrk = grdDBGrid1(2).SelBookmarks(mintisel)
  lblmensaje.Caption = "Analizando diferencias Carro: " & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & ". Espere un momento, por favor..."
  carrosrestantes = carrosrestantes - 1
  lblmensaje.Visible = True
  
  If grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) <> "" Then
    qryDispensado(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
    qryDispensado(1) = -1
    qryDispensado(2) = indMIVCito
    Set rstDispensado = qryDispensado.OpenResultset()
    While rstDispensado.StillExecuting
    Wend
    If rstDispensado(0).Value > 0 Then
      MsgBox "El Carro " & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
             " ya ha sido Analizado.", vbInformation, "Aviso"
      rstDispensado.Close
      Set rstDispensado = Nothing
      GoTo carro_siguiente
    Else
      strDelete = "DELETE FROM FR3300 WHERE FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
      strDelete = strDelete & " AND FR33FECCARGA=TRUNC(SYSDATE)"
      strDelete = strDelete & " AND FR33HORACARGA=" & hora
      strDelete = strDelete & " AND FR33INDMEZCLA=-1"
      strDelete = strDelete & " AND FR33INDCITOSTATICO=" & indMIVCito
      objApp.rdoConnect.Execute strDelete, 64
    End If
    rstDispensado.Close
    Set rstDispensado = Nothing
  End If

  'transforma la coma de separaci�n de los decimales por un punto
  'de la hora a la que sale el carro
  hora = grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)
  hora = objGen.ReplaceStr(hora, ",", ".", 1)

  'se vuelca el contenido de FR0600 en FR3300, todas las tuplas con motivo "A�adido"
  qryinsert33(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
  qryinsert33.Execute

  qryFR7400(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
  Set rstFR7400 = qryFR7400.OpenResultset()
  While rstFR7400.StillExecuting
  Wend
  If Not rstFR7400.EOF Then
    While Not rstFR7400("FR74HORASALIDA").Value = grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)
      rstFR7400.MoveNext
    Wend
    If Not rstFR7400.EOF Then
      rstFR7400.MoveNext
      If Not rstFR7400.EOF Then
        strDiaSiguiente = rstFR7400("FR74DIA").Value
        curHoraSiguiente = rstFR7400("FR74HORASALIDA").Value
      Else
        qryFR7400_2(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
        Set rstFR7400_2 = qryFR7400_2.OpenResultset()
        While rstFR7400_2.StillExecuting
        Wend
        If Not rstFR7400_2.EOF Then
          strDiaSiguiente = rstFR7400_2("FR74DIA").Value
          curHoraSiguiente = rstFR7400_2("FR74HORASALIDA").Value
        Else
          GoTo carro_siguiente
        End If
        rstFR7400_2.Close
        Set rstFR7400_2 = Nothing
      End If
    Else
      qryFR7400_2(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
      Set rstFR7400_2 = qryFR7400_2.OpenResultset()
      While rstFR7400_2.StillExecuting
      Wend
      If Not rstFR7400_2.EOF Then
        strDiaSiguiente = rstFR7400_2("FR74DIA").Value
        curHoraSiguiente = rstFR7400_2("FR74HORASALIDA").Value
      Else
        GoTo carro_siguiente
      End If
      rstFR7400_2.Close
      Set rstFR7400_2 = Nothing
    End If
  Else
    GoTo carro_siguiente
  End If
  rstFR7400.Close
  Set rstFR7400 = Nothing

  qrya(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
  qrya(1) = 4 'Est.Pet.
  qrya(2) = -1 'IndOM
  qrya(3) = -1 'IndPerf
  Set rsta = qrya.OpenResultset()
  While rsta.StillExecuting
  Wend
  While Not rsta.EOF
    blnInsertar = False
    blnValida = False
    If Checkmezcla.Value = 1 Then 'MIV
      indMIVCito = 0
      blnInsertar = True
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = False
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = False
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = False
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
    Else 'CITOSTATICOS
      indMIVCito = -1
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = True
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = True
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
      If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
        qryMIV(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
        Set rstMIV = qryMIV.OpenResultset()
        If rstMIV(0).Value > 0 Then
          blnInsertar = True
        End If
        rstMIV.Close
        Set rstMIV = Nothing
      End If
    End If
    If blnInsertar = True Then
      If strDiaSiguiente = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk) Then
        If CCur(grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < curHoraSiguiente Then
          If rsta("DIASDIFINI").Value >= 0 And rsta("DIASDIFFIN").Value >= 0 Then
            If rsta("DIASDIFINI").Value = 0 And rsta("DIASDIFFIN").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFINI").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFFIN").Value = 0 Then
              If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            Else
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          End If
        Else 'blnvalida=false
        End If
      Else 'Dia salida<>Dia Hasta
        If CCur(grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < 24 Then
          If rsta("DIASDIFINI").Value >= 0 And rsta("DIASDIFFIN").Value >= 0 Then
            If rsta("DIASDIFINI").Value = 0 And rsta("DIASDIFFIN").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFINI").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFFIN").Value = 0 Then
              If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                  blnValida = True
                End If
              End If
            Else
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI").Value), rsta("DIAHOY").Value, rsta("DIAMES").Value) Then
                blnValida = True
              End If
            End If
          End If
        ElseIf 0 <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) < curHoraSiguiente Then
          If rsta("DIASDIFINI_2").Value >= 0 And rsta("DIASDIFFIN_2").Value >= 0 Then
            If rsta("DIASDIFINI_2").Value = 0 And rsta("DIASDIFFIN_2").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) And CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFINI_2").Value = 0 Then
              If CCur(rsta("FR32HORAINICIO").Value) <= CCur(rsta("FRG5HORA").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                  blnValida = True
                End If
              End If
            ElseIf rsta("DIASDIFFIN_2").Value = 0 Then
              If CCur(rsta("FRG5HORA").Value) <= CCur(rsta("FR32HORAFIN").Value) Then
                If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                  blnValida = True
                End If
              End If
            Else
              If Es_Para_Hoy(rsta("FRH5CODPERIODICIDAD").Value, CCur(rsta("DIASDIFINI_2").Value), rsta("DIAHOY_2").Value, rsta("DIAMES_2").Value) Then
                blnValida = True
              End If
            End If
          End If
        Else
          'blnvalida=false
        End If
      End If
    End If
    If blnValida Then
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''???
      gcodproducto = ""
      gcodproducto_dil = ""
      gcodproducto_mez = ""
      Select Case rsta.rdoColumns("FR32OPERACION").Value
      Case "/", "P", "M", "E"
        'producto principal
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo = 1
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto = rsta.rdoColumns("FR73CODPRODUCTO").Value
            If rsta.rdoColumns("FR73CODPRODUCTO").Value = 999999999 Then
              gdescproducto = rsta.rdoColumns("FR32DESPRODUCTO").Value
            Else
              gdescproducto = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDAD").Value) Then
             cantidad = 0
            Else
             cantidad = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDAD").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Then
             strDosis = 0
            Else
             strDosis = objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
             strUM = 0
            Else
             strUM = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
             strFF = 0
            Else
             strFF = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        'producto diluyente : puede haber o no
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_dil = 2
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_dil = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
            gdescproducto_dil = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rstprod.rdoColumns("FR73VOLUMEN").Value) Or _
                IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              cantidad_dil = 0
            Else
              If rstprod.rdoColumns("FR73VOLUMEN").Value <> 0 Then
                cantidad_dil = Format(rsta.rdoColumns("FR32CANTIDADDIL").Value / _
                              rstprod.rdoColumns("FR73VOLUMEN").Value, "0.00")
                cantidad_dil = objGen.ReplaceStr(cantidad_dil, ",", ".", 1)
              Else
                cantidad_dil = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              strDosis_dil = 0
            Else
              strDosis_dil = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDADDIL").Value, ",", ".", 1)
            End If
            strUM_dil = "ml"
            If IsNull(rstprod.rdoColumns("FRH7CODFORMFAR").Value) Then
              strFF_dil = ""
            Else
              strFF_dil = rstprod.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
        'producto_mezcla
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_mez = 3
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_mez = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
            gdescproducto_mez = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Or _
                IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
              cantidad_mez = 0
            Else
              If rstprod.rdoColumns("FR73DOSIS").Value = 0 Then
                cantidad_mez = 0
              Else
                cantidad_mez = Format(rsta.rdoColumns("FR32DOSIS_2").Value / _
                                  rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
                cantidad_mez = objGen.ReplaceStr(cantidad_mez, ",", ".", 1)
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Then
              strDosis_mez = 0
            Else
              strDosis_mez = objGen.ReplaceStr(rsta.rdoColumns("FR32DOSIS_2").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
              strUM_mez = 0
            Else
              strUM_mez = rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR_2").Value) Then
              strFF_mez = 0
            Else
              strFF_mez = rsta.rdoColumns("FRH7CODFORMFAR_2").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
      Case "F"
        'Electrolito
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_dil = 2
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_dil = rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
            gdescproducto_dil = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rstprod.rdoColumns("FR73VOLUMEN").Value) Or _
                IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              cantidad_dil = 0
            Else
              If rstprod.rdoColumns("FR73VOLUMEN").Value <> 0 Then
                cantidad_dil = Format(rsta.rdoColumns("FR32CANTIDADDIL").Value / _
                        rstprod.rdoColumns("FR73VOLUMEN").Value, "0.00")
                cantidad_dil = objGen.ReplaceStr(cantidad_dil, ",", ".", 1)
              Else
                cantidad_dil = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32CANTIDADDIL").Value) Then
              strDosis_dil = 0
            Else
              strDosis_dil = objGen.ReplaceStr(rsta.rdoColumns("FR32CANTIDADDIL").Value, ",", ".", 1)
            End If
            strUM_dil = "ml"
            If IsNull(rstprod.rdoColumns("FRH7CODFORMFAR").Value) Then
              strFF_dil = ""
            Else
              strFF_dil = rstprod.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        'Electrolito 2
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo = 1
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto = rsta.rdoColumns("FR73CODPRODUCTO").Value
            gdescproducto = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Or _
                IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
              cantidad = 0
            Else
              If rstprod.rdoColumns("FR73DOSIS").Value <> 0 Then
                cantidad = Format(rsta.rdoColumns("FR28DOSIS").Value / _
                            rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
                cantidad = objGen.ReplaceStr(cantidad, ",", ".", 1)
              Else
                cantidad = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Then
              strDosis = 0
            Else
              strDosis = objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
              strUM = 0
            Else
              strUM = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
              strFF = 0
            Else
              strFF = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
        'Medicamento 2
        If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
          qryprod(0) = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
          Set rstprod = qryprod.OpenResultset()
          While rstprod.StillExecuting
          Wend
          If Not rstprod.EOF Then
            grupo_mez = 3
            numlinea = rsta.rdoColumns("FR28NUMLINEA").Value
            gcodproducto_mez = rsta.rdoColumns("FR73CODPRODUCTO_2").Value
            gdescproducto_mez = rstprod.rdoColumns("FR73DESPRODUCTO").Value
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Or _
                IsNull(rstprod.rdoColumns("FR73DOSIS").Value) Then
              cantidad_mez = 0
            Else
              If rstprod.rdoColumns("FR73DOSIS").Value <> 0 Then
                cantidad_mez = Format(rsta.rdoColumns("FR32DOSIS_2").Value / _
                            rstprod.rdoColumns("FR73DOSIS").Value, "0.00")
                cantidad_mez = objGen.ReplaceStr(cantidad_mez, ",", ".", 1)
              Else
                cantidad_mez = 0
              End If
            End If
            If IsNull(rsta.rdoColumns("FR32DOSIS_2").Value) Then
              strDosis_mez = 0
            Else
              strDosis_mez = objGen.ReplaceStr(rsta.rdoColumns("FR32DOSIS_2").Value, ",", ".", 1)
            End If
            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
              strUM_mez = 0
            Else
              strUM_mez = rsta.rdoColumns("FR93CODUNIMEDIDA_2").Value
            End If
            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR_2").Value) Then
              strFF_mez = 0
            Else
              strFF_mez = rsta.rdoColumns("FRH7CODFORMFAR_2").Value
            End If
          End If
          rstprod.Close
          Set rstprod = Nothing
        End If
      End Select
      blnCambiaAlgo = False
      AUXFR28NUMLINEA = -1
      'seg�n el c�digo de operaci�n se har�n 1,2 o 3 insert en FR3300 seg�n
      'haya medicamento,diluyente,producto_mezcla
      If IsNumeric(gcodproducto) Then
        'se mira si existe ya la tupla o no
        str33 = "SELECT FR28NUMLINEA FROM FR3300 WHERE " & _
                "FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                "FR73CODPRODUCTO=" & gcodproducto & " AND " & _
                "FR33CANTIDAD=" & cantidad & " AND " & _
                "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                "FR33HORACARGA=" & hora & " AND " & _
                "FR33FORMFAR=" & "'" & strFF & "'" & " AND " & _
                "FR33DOSIS=" & strDosis & " AND " & _
                "FR33UNIMEDIDA=" & "'" & strUM & "'" & " AND " & _
                "FR33GRUPO=" & grupo & " AND " & _
                "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                "FR33INDDIFANA=0" & " AND " & _
                "FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=" & indMIVCito & " AND " & _
                "FR33INDPRODMIRADO=0"
        Set rst33 = objApp.rdoConnect.OpenResultset(str33)
        While rst33.StillExecuting
        Wend
        If rst33.EOF Then 'no existe la tupla
          blnCambiaAlgo = True
        Else
          AUXFR28NUMLINEA = rst33.rdoColumns("FR28NUMLINEA").Value
        End If
      End If
      If blnCambiaAlgo = False Then
        If IsNumeric(gcodproducto_dil) Then
          If strFF_dil <> "" Then
            'se mira si existe ya la tupla o no
            str33 = "SELECT FR28NUMLINEA FROM FR3300 WHERE " & _
                    "FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                    "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                    "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                    "FR73CODPRODUCTO=" & gcodproducto_dil & " AND " & _
                    "FR33CANTIDAD=" & cantidad_dil & " AND " & _
                    "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                    "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                    "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                    "FR33HORACARGA=" & hora & " AND " & _
                    "FR33FORMFAR=" & "'" & strFF_dil & "'" & " AND " & _
                    "FR33DOSIS=" & strDosis_dil & " AND " & _
                    "FR33UNIMEDIDA=" & "'" & strUM_dil & "'" & " AND " & _
                    "FR33GRUPO=" & grupo_dil & " AND " & _
                    "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                    "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                    "FR33INDDIFANA=0" & " AND " & _
                    "FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=" & indMIVCito & " AND " & _
                    "FR33INDPRODMIRADO=0"
            If AUXFR28NUMLINEA <> -1 Then
              str33 = str33 & " AND FR28NUMLINEA=" & AUXFR28NUMLINEA
            End If
            Set rst33 = objApp.rdoConnect.OpenResultset(str33)
            While rst33.StillExecuting
            Wend
            If rst33.EOF Then  'no existe la tupla
              blnCambiaAlgo = True
            Else
              AUXFR28NUMLINEA = rst33.rdoColumns("FR28NUMLINEA").Value
            End If
          Else
            'se mira si existe ya la tupla o no
            str33 = "SELECT FR28NUMLINEA FROM FR3300 WHERE " & _
                    "FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                    "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                    "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                    "FR73CODPRODUCTO=" & gcodproducto_dil & " AND " & _
                    "FR33CANTIDAD=" & cantidad_dil & " AND " & _
                    "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                    "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                    "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                    "FR33HORACARGA=" & hora & " AND " & _
                    "FR33FORMFAR IS NULL" & " AND " & _
                    "FR33DOSIS=" & strDosis_dil & " AND " & _
                    "FR33UNIMEDIDA=" & "'" & strUM_dil & "'" & " AND " & _
                    "FR33GRUPO=" & grupo_dil & " AND " & _
                    "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                    "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                    "FR33INDDIFANA=0" & " AND " & _
                    "FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=" & indMIVCito & " AND " & _
                    "FR33INDPRODMIRADO=0"
            If AUXFR28NUMLINEA <> -1 Then
              str33 = str33 & " AND FR28NUMLINEA=" & AUXFR28NUMLINEA
            End If
            Set rst33 = objApp.rdoConnect.OpenResultset(str33)
            While rst33.StillExecuting
            Wend
            If rst33.EOF Then  'no existe la tupla
              blnCambiaAlgo = True
            Else
              AUXFR28NUMLINEA = rst33.rdoColumns("FR28NUMLINEA").Value
            End If
          End If
        End If
      End If
      If blnCambiaAlgo = False Then
        If IsNumeric(gcodproducto_mez) Then
          'se mira si existe ya la tupla o no
          str33 = "SELECT FR28NUMLINEA FROM FR3300 WHERE " & _
                  "FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                  "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                  "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                  "FR73CODPRODUCTO=" & gcodproducto_mez & " AND " & _
                  "FR33CANTIDAD=" & cantidad_mez & " AND " & _
                  "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                  "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                  "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                  "FR33HORACARGA=" & hora & " AND " & _
                  "FR33FORMFAR=" & "'" & strFF_mez & "'" & " AND " & _
                  "FR33DOSIS=" & strDosis_mez & " AND " & _
                  "FR33UNIMEDIDA=" & "'" & strUM_mez & "'" & " AND " & _
                  "FR33GRUPO=" & grupo_mez & " AND " & _
                  "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                  "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                  "FR33INDDIFANA=0" & " AND " & _
                  "FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=" & indMIVCito & " AND " & _
                  "FR33INDPRODMIRADO=0"
          If AUXFR28NUMLINEA <> -1 Then
            str33 = str33 & " AND FR28NUMLINEA=" & AUXFR28NUMLINEA
          End If
          Set rst33 = objApp.rdoConnect.OpenResultset(str33)
          While rst33.StillExecuting
          Wend
          If rst33.EOF Then  'no existe la tupla
            blnCambiaAlgo = True
          Else
            AUXFR28NUMLINEA = rst33.rdoColumns("FR28NUMLINEA").Value
          End If
        End If
      End If
      If blnCambiaAlgo = True Then 'Inserts
        If IsNumeric(gcodproducto) Then
          strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                        "FR73CODPRODUCTO,FR33DESPROD,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                        "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                        "FR33FECCARGA,FR33HORACARGA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA," & _
                        "FR33GRUPO,FR33HORATOMA,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33INDMEZCLA,FR33INDNODIF,FR33INDCITOSTATICO,FR33CODIGO)" & _
                        " VALUES (" & _
                        grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                        "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & "," & _
                        rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                        gcodproducto & "," & _
                        "'" & gdescproducto & "'" & "," & _
                        cantidad & "," & _
                        "NULL,NULL,NULL,NULL" & "," & _
                        rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                        numlinea & "," & _
                        "TRUNC(SYSDATE)" & "," & _
                        hora & ",'" & strFF & "'," & strDosis & ",'" & strUM & "'," & _
                        grupo & "," & _
                        objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                        "'A�adido'" & "," & _
                        "-1,-1," & _
                        "-1,0," & indMIVCito & _
                        ",FR33CODIGO_SEQUENCE.NEXTVAL)"
          objApp.rdoConnect.Execute strinsert33, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
        If IsNumeric(gcodproducto_dil) Then
          If strFF_dil <> "" Then
            strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                          "FR73CODPRODUCTO,FR33DESPROD,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                          "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                          "FR33FECCARGA,FR33HORACARGA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA," & _
                          "FR33GRUPO,FR33HORATOMA,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33INDMEZCLA,FR33INDNODIF,FR33INDCITOSTATICO,FR33CODIGO)" & _
                          " VALUES (" & _
                          grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                          "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & "," & _
                          rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                          gcodproducto_dil & "," & _
                          "'" & gdescproducto_dil & "'" & "," & _
                          cantidad_dil & "," & _
                          "NULL,NULL,NULL,NULL" & "," & _
                          rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                          numlinea & "," & _
                          "TRUNC(SYSDATE)" & "," & _
                          hora & ",'" & strFF_dil & "'," & strDosis_dil & ",'" & strUM_dil & "'," & _
                          grupo_dil & "," & _
                          objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                          "'A�adido'" & "," & _
                          "-1,-1," & _
                          "-1,0," & indMIVCito & _
                          ",FR33CODIGO_SEQUENCE.NEXTVAL)"
            objApp.rdoConnect.Execute strinsert33, 64
            objApp.rdoConnect.Execute "Commit", 64
          Else 'strFF_dil = ""
            strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                          "FR73CODPRODUCTO,FR33DESPROD,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                          "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                          "FR33FECCARGA,FR33HORACARGA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA," & _
                          "FR33GRUPO,FR33HORATOMA,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33INDMEZCLA,FR33INDNODIF,FR33INDCITOSTATICO,FR33CODIGO)" & _
                          " VALUES (" & _
                          grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                          "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & "," & _
                          rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                          gcodproducto_dil & "," & _
                          "'" & gdescproducto_dil & "'" & "," & _
                          cantidad_dil & "," & _
                          "NULL,NULL,NULL,NULL" & "," & _
                          rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                          numlinea & "," & _
                          "TRUNC(SYSDATE)" & "," & _
                          hora & ",NULL," & strDosis_dil & ",'" & strUM_dil & "'," & _
                          grupo_dil & "," & _
                          objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                          "'A�adido'" & "," & _
                          "-1,-1," & _
                          "-1,0," & indMIVCito & _
                          ",FR33CODIGO_SEQUENCE.NEXTVAL)"
            objApp.rdoConnect.Execute strinsert33, 64
            objApp.rdoConnect.Execute "Commit", 64
          End If
        End If
        If IsNumeric(gcodproducto_mez) Then
          strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                        "FR73CODPRODUCTO,FR33DESPROD,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                        "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR28NUMLINEA," & _
                        "FR33FECCARGA,FR33HORACARGA,FR33FORMFAR,FR33DOSIS,FR33UNIMEDIDA," & _
                        "FR33GRUPO,FR33HORATOMA,FR33MOTDIFERENCIA,FR33INDDIFANA,FR33INDPRODMIRADO,FR33INDMEZCLA,FR33INDNODIF,FR33INDCITOSTATICO,FR33CODIGO)" & _
                        " VALUES (" & _
                        grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & "," & _
                        "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & "," & _
                        rsta.rdoColumns("CI21CODPERSONA").Value & "," & _
                        gcodproducto_mez & "," & _
                        "'" & gdescproducto_mez & "'" & "," & _
                        cantidad_mez & "," & _
                        "NULL,NULL,NULL,NULL" & "," & _
                        rsta.rdoColumns("FR66CODPETICION").Value & "," & _
                        numlinea & "," & _
                        "TRUNC(SYSDATE)" & "," & _
                        hora & ",'" & strFF_mez & "'," & strDosis_mez & ",'" & strUM_mez & "'," & _
                        grupo_mez & "," & _
                        objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & "," & _
                        "'A�adido'" & "," & _
                        "-1,-1," & _
                        "-1,0," & indMIVCito & _
                        ",FR33CODIGO_SEQUENCE.NEXTVAL)"
          objApp.rdoConnect.Execute strinsert33, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      Else 'blnCambiaAlgo = false Updates
        If IsNumeric(gcodproducto) Then
          strUpdate = "UPDATE FR3300 SET FR33INDPRODMIRADO=-1,FR33INDNODIF=-1,FR33INDDIFANA=-1" & _
                "WHERE " & _
                "FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                "FR73CODPRODUCTO=" & gcodproducto & " AND " & _
                "FR33CANTIDAD=" & cantidad & " AND " & _
                "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                "FR33HORACARGA=" & hora & " AND " & _
                "FR33FORMFAR=" & "'" & strFF & "'" & " AND " & _
                "FR33DOSIS=" & strDosis & " AND " & _
                "FR33UNIMEDIDA=" & "'" & strUM & "'" & " AND " & _
                "FR33GRUPO=" & grupo & " AND " & _
                "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                "FR33INDDIFANA=0" & " AND " & _
                "FR28NUMLINEA=" & AUXFR28NUMLINEA
          objApp.rdoConnect.Execute strUpdate, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
        If IsNumeric(gcodproducto_dil) Then
          If strFF_dil <> "" Then
            strUpdate = "UPDATE FR3300 SET FR33INDPRODMIRADO=-1,FR33INDNODIF=-1,FR33INDDIFANA=-1" & _
                  "WHERE " & _
                  "FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                  "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                  "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                  "FR73CODPRODUCTO=" & gcodproducto_dil & " AND " & _
                  "FR33CANTIDAD=" & cantidad_dil & " AND " & _
                  "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                  "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                  "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                  "FR33HORACARGA=" & hora & " AND " & _
                  "FR33FORMFAR=" & "'" & strFF_dil & "'" & " AND " & _
                  "FR33DOSIS=" & strDosis_dil & " AND " & _
                  "FR33UNIMEDIDA=" & "'" & strUM_dil & "'" & " AND " & _
                  "FR33GRUPO=" & grupo_dil & " AND " & _
                  "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                  "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                  "FR33INDDIFANA=0" & " AND " & _
                  "FR28NUMLINEA=" & AUXFR28NUMLINEA
            objApp.rdoConnect.Execute strUpdate, 64
            objApp.rdoConnect.Execute "Commit", 64
          Else 'strFF_dil = ""
            strUpdate = "UPDATE FR3300 SET FR33INDPRODMIRADO=-1,FR33INDNODIF=-1,FR33INDDIFANA=-1" & _
                        "WHERE " & _
                        "FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                        "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                        "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                        "FR73CODPRODUCTO=" & gcodproducto_dil & " AND " & _
                        "FR33CANTIDAD=" & cantidad_dil & " AND " & _
                        "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                        "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                        "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                        "FR33HORACARGA=" & hora & " AND " & _
                        "FR33FORMFAR IS NULL" & " AND " & _
                        "FR33DOSIS=" & strDosis_dil & " AND " & _
                        "FR33UNIMEDIDA=" & "'" & strUM_dil & "'" & " AND " & _
                        "FR33GRUPO=" & grupo_dil & " AND " & _
                        "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                        "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                        "FR33INDDIFANA=0" & " AND " & _
                        "FR28NUMLINEA=" & rst33.rdoColumns("FR28NUMLINEA").Value
            objApp.rdoConnect.Execute strUpdate, 64
            objApp.rdoConnect.Execute "Commit", 64
          End If
        End If
        If IsNumeric(gcodproducto_mez) Then
          strUpdate = "UPDATE FR3300 SET FR33INDPRODMIRADO=-1,FR33INDNODIF=-1,FR33INDDIFANA=-1" & _
                      "WHERE " & _
                      "FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & " AND " & _
                      "AD15CODCAMA=" & "'" & rsta.rdoColumns("AD15CODCAMA").Value & "'" & " AND " & _
                      "CI21CODPERSONA=" & rsta.rdoColumns("CI21CODPERSONA").Value & " AND " & _
                      "FR73CODPRODUCTO=" & gcodproducto_mez & " AND " & _
                      "FR33CANTIDAD=" & cantidad_mez & " AND " & _
                      "FR33INDCARGAREA IS NULL AND FR33INDINCCAMA IS NULL" & " AND " & _
                      "FR33INDINCPAC IS NULL AND FR33INDINCPROD IS NULL" & " AND " & _
                      "FR33FECCARGA=TRUNC(SYSDATE)" & " AND " & _
                      "FR33HORACARGA=" & hora & " AND " & _
                      "FR33FORMFAR=" & "'" & strFF_mez & "'" & " AND " & _
                      "FR33DOSIS=" & strDosis_mez & " AND " & _
                      "FR33UNIMEDIDA=" & "'" & strUM_mez & "'" & " AND " & _
                      "FR33GRUPO=" & grupo_mez & " AND " & _
                      "FR33HORATOMA=" & objGen.ReplaceStr(rsta.rdoColumns("FRG5HORA").Value, ",", ".", 1) & " AND " & _
                      "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                      "FR33INDDIFANA=0" & " AND " & _
                      "FR28NUMLINEA=" & rst33.rdoColumns("FR28NUMLINEA").Value
          objApp.rdoConnect.Execute strUpdate, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      End If
    End If
    rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing

  'Si hay registros con estado "A�adido" pero �stos no han venido en las nuevas �rdenes
  'm�dicas,es decir, tienen el campo FR33INDDIFANA=0 y FR33INDPRODMIRADO=0
  'entonces dichos registros se marcan como eliminados.
  strUpdate = "UPDATE FR3300 SET FR33MOTDIFERENCIA='Eliminado' " & _
            "WHERE FR33INDDIFANA = 0"
  strUpdate = strUpdate & " AND FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
  strUpdate = strUpdate & " AND FR33FECCARGA=TRUNC(SYSDATE)"
  strUpdate = strUpdate & " AND FR33HORACARGA=" & hora
  strUpdate = strUpdate & " AND FR33MOTDIFERENCIA='A�adido' AND FR33INDPRODMIRADO = 0"
  strUpdate = strUpdate & " AND FR33INDMEZCLA=-1 AND FR33INDNODIF=0 "
  strUpdate = strUpdate & " AND FR33INDCITOSTATICO=" & indMIVCito
  objApp.rdoConnect.Execute strUpdate, 64
  objApp.rdoConnect.Execute "Commit", 64


'**********************************************************************************
Dim strpersonacama As String
Dim rstpersonacama As rdoResultset
Dim strpeticion As String
Dim rstpeticion As rdoResultset
Dim strorden As String
Dim rstorden As rdoResultset
Dim strcama As String
Dim rstcama As rdoResultset
Dim strcama06 As String
Dim rstcama06 As rdoResultset

  'se saca  n� de cama del carro en FR3300
  strpersonacama = "SELECT DISTINCT(AD15CODCAMA)" & _
        " FROM FR3300 WHERE " & _
        " FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=" & indMIVCito & _
        " AND FR33FECCARGA=TRUNC(SYSDATE)" & _
        " AND FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
        " AND FR33HORACARGA=" & hora
  Set rstpersonacama = objApp.rdoConnect.OpenResultset(strpersonacama)
  While rstpersonacama.StillExecuting
  Wend
  While Not rstpersonacama.EOF
    strpeticion = "SELECT FR66CODPETICION,CI21CODPERSONA " & _
                  " FROM FR3300 WHERE " & _
                  " FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=" & indMIVCito & _
                  " AND FR33FECCARGA=TRUNC(SYSDATE)" & _
                  " AND FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
                  " AND FR33HORACARGA=" & hora & _
                  " AND AD15CODCAMA=" & rstpersonacama.rdoColumns("AD15CODCAMA").Value
    Set rstpeticion = objApp.rdoConnect.OpenResultset(strpeticion)
    While rstpeticion.StillExecuting
    Wend
    'se busca su proceso y asistencia
    strorden = "SELECT AD01CODASISTENCI,AD07CODPROCESO FROM FR6600" & _
                " WHERE FR66CODPETICION=" & rstpeticion.rdoColumns("FR66CODPETICION").Value
    Set rstorden = objApp.rdoConnect.OpenResultset(strorden)
    While rstorden.StillExecuting
    Wend
    'se mira cu�l es su cama actual
    strcama = "SELECT AD15CODCAMA FROM AD1500 " & _
              " WHERE AD01CODASISTENCI=" & rstorden.rdoColumns("AD01CODASISTENCI").Value & _
              " AND AD07CODPROCESO=" & rstorden.rdoColumns("AD07CODPROCESO").Value
    Set rstcama = objApp.rdoConnect.OpenResultset(strcama)
    While rstcama.StillExecuting
    Wend
    rstorden.Close
    Set rstorden = Nothing
    'se busca la cama en FR0600
    strcama06 = "SELECT DISTINCT(AD15CODCAMA)" & _
                " FROM FR0600 WHERE " & _
                " FR06INDMEZCLA=-1  AND FR06INDCITOSTATICO=" & indMIVCito & _
                " AND FR06FECCARGA=TRUNC(SYSDATE)" & _
                " AND FR06FECCARGA=TRUNC(SYSDATE)" & _
                " AND FR06HORACARGA=" & hora & _
                " AND CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
    Set rstcama06 = objApp.rdoConnect.OpenResultset(strcama06)
    While rstcama06.StillExecuting
    Wend
    'se mira si cama_Actual<>cama_en_FR0600
    If Not rstcama06.EOF And Not rstcama.EOF Then
      If Not IsNull(rstcama06.rdoColumns("AD15CODCAMA").Value) And Not IsNull(rstcama.rdoColumns("AD15CODCAMA").Value) Then
        If (rstcama.rdoColumns("AD15CODCAMA").Value <> _
            rstcama06.rdoColumns("AD15CODCAMA").Value) Then
          strUpdate = "UPDATE FR3300 SET FR33CAMBIOCAMA='Cambio de Cama' WHERE " & _
                      " FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
                      " AND FR33FECCARGA=TRUNC(SYSDATE)" & _
                      " AND FR33HORACARGA=" & hora & _
                      " AND FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=" & indMIVCito & _
                      " AND CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
          objApp.rdoConnect.Execute strUpdate, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Else
      If rstcama.EOF Then  'el paciente ha sido dado de alta, ya no tiene cama
        strUpdate = "UPDATE FR3300 SET FR33CAMBIOCAMA='Alta del Paciente' WHERE " & _
                    " FR07CODCARRO=" & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & _
                    " AND FR33FECCARGA=TRUNC(SYSDATE)" & _
                    " AND FR33HORACARGA=" & hora & _
                    " AND FR33INDMEZCLA=-1 AND FR33INDCITOSTATICO=" & indMIVCito & _
                    " AND CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
        objApp.rdoConnect.Execute strUpdate, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End If
    rstcama06.Close
    Set rstcama06 = Nothing
    rstpeticion.Close
    Set rstpeticion = Nothing
    rstcama.Close
    Set rstcama = Nothing
    rstpersonacama.MoveNext
  Wend
  rstpersonacama.Close
  Set rstpersonacama = Nothing
  '***************************************************************************************+
      
  lblmensaje.Caption = "Generando etiquetas Carro: " & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & ". Espere un momento, por favor..."
  Call fabricar_etiquetas(grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk), hora)

  'se cambia el estado del carro (estado=6 Analizado las diferencias)
  If Checkcito.Value = 1 Then
    qryupdEstCarCIT(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
    qryupdEstCarCIT(1) = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk) 'D�a
    qryupdEstCarCIT.Execute
  Else 'MIV
    qryupdEstCarMIV(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) 'Carro
    qryupdEstCarMIV(1) = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk) 'D�a
    qryupdEstCarMIV.Execute
  End If

carro_siguiente:
Next mintisel


Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
objWinInfo.DataRefresh

lblmensaje.Visible = False
frmmensaje.Visible = False

Screen.MousePointer = vbDefault
Me.Enabled = True
cmdanalizardiferencias.Enabled = True

  MsgBox "Analisis Terminado", vbInformation, "Aviso"

End Sub

Private Sub fabricar_etiquetas(carro, hora)
Dim strmedidadil As String
Dim curDosis2 As Currency
Dim curCantidad2 As Currency
Dim auxProd2 As String
Dim strProd2 As String
Dim qryProd2 As rdoQuery
Dim rstProd2 As rdoResultset
Dim auxProd1 As String
Dim strProd1 As String
Dim qryProd1 As rdoQuery
Dim rstProd1 As rdoResultset
Dim curDosis1 As Currency
Dim curCantidad1 As Currency
Dim vntAuxCantidad1 As Variant
Dim vntAuxCantidad2 As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim qryupdate As rdoQuery
Dim strlinea As String
Dim strpaciente As String
Dim qrypaciente As rdoQuery
Dim rstpaciente As rdoResultset
Dim strcama As String
Dim qrycama As rdoQuery
Dim rstcama As rdoResultset
Dim strdpto As String
Dim qrydpto As rdoQuery
Dim rstdpto As rdoResultset
Dim auxSolucion As String
Dim strSolucion As String
Dim qrySolucion As rdoQuery
Dim rstSolucion As rdoResultset
Dim strcaducidad As String
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim Auxfecha As String
Dim auxVia As String
Dim strVia As String
Dim qryVia As rdoQuery
Dim rstVia As rdoResultset
Dim strHoras As String
Dim strMinutos As String
Dim strHAdm As String
Dim strInstrucctiones As String
Dim blnEtiquetas As Boolean
Dim auxdpto As String
Dim Auxcama As String
Dim i  As Integer
Dim auxInstrucctiones As String
Dim auxVolTotal As String
Dim auxStr As String
Dim auxcant As String
Dim strFab As String
Dim rstFab As rdoResultset
Dim qryFab As rdoQuery
Dim crlf As String
Dim Incremento As Integer
Dim contLinea As Integer
Dim strListado As String
Dim auxStrListado As String

strFab = "SELECT DISTINCT"
strFab = strFab & " FR3300.FR07CODCARRO,"
strFab = strFab & " FR3300.FR33HORATOMA,"
strFab = strFab & " FR6600.FR66CODPETICION,"
strFab = strFab & " FR3200.FR28NUMLINEA,"
strFab = strFab & " AD1500.AD15CODCAMA,"
strFab = strFab & " FR6600.CI21CODPERSONA,"
strFab = strFab & " FR3200.FR32OPERACION,"
strFab = strFab & " FR3200.FR73CODPRODUCTO,"
strFab = strFab & " FR3200.FR93CODUNIMEDIDA,"
strFab = strFab & " FR3200.FR28DOSIS,"
strFab = strFab & " FR3200.FR32CANTIDAD,"
strFab = strFab & " FR3200.FR73CODPRODUCTO_2,"
strFab = strFab & " FR3200.FR32DOSIS_2,"
strFab = strFab & " FR3200.FR93CODUNIMEDIDA_2,"
strFab = strFab & " FR3200.FR73CODPRODUCTO_DIL,"
strFab = strFab & " FR3200.FR32TIEMMININF,"
strFab = strFab & " FR3200.FR32CANTIDADDIL,"
strFab = strFab & " FR3200.FR32VELPERFUSION,"
strFab = strFab & " FR3200.FR32VOLTOTAL,"
strFab = strFab & " FR3200.FR34CODVIA,"
strFab = strFab & " FR3200.FR32INSTRADMIN"
strFab = strFab & " FROM FR6600,FR3200,FR3300,AD1500"
strFab = strFab & " WHERE FR6600.FR66CODPETICION=FR3200.FR66CODPETICION"
strFab = strFab & " AND FR6600.FR66CODPETICION=FR3300.FR66CODPETICION"
strFab = strFab & " AND FR3200.FR66CODPETICION=FR3300.FR66CODPETICION"
strFab = strFab & " AND FR3200.FR28NUMLINEA=FR3300.FR28NUMLINEA"
strFab = strFab & " AND FR6600.AD01CODASISTENCI=AD1500.AD01CODASISTENCI"
strFab = strFab & " AND FR6600.AD07CODPROCESO=AD1500.AD07CODPROCESO"
strFab = strFab & " AND FR3300.FR33INDNODIF=0"
strFab = strFab & " AND FR3300.FR33MOTDIFERENCIA='A�adido'"
strFab = strFab & " AND FR3300.FR33INDMEZCLA=-1"
strFab = strFab & " AND FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
strFab = strFab & " AND FR3300.FR33HORACARGA=?"
strFab = strFab & " AND FR3300.FR07CODCARRO=?"
If Checkcito.Value = 1 Then
  strFab = strFab & " AND FR3300.FR33INDCITOSTATICO=-1"
Else
  strFab = strFab & " AND (FR3300.FR33INDCITOSTATICO=0 OR FR3300.FR33INDCITOSTATICO IS NULL)"
End If

Set qryFab = objApp.rdoConnect.CreateQuery("", strFab)
qryFab(0) = hora
qryFab(1) = carro
Set rstFab = qryFab.OpenResultset(strFab)
While rstFab.StillExecuting
Wend

mintNTotalSelRows = 0
Dim Listado() As String

While Not rstFab.EOF
  mintNTotalSelRows = mintNTotalSelRows + 1
  ReDim Preserve Listado(mintNTotalSelRows) As String
  Listado(mintNTotalSelRows) = ""
  auxSolucion = ""
  auxProd1 = ""
  auxProd2 = ""
  strmedidadil = "ml"
  strlinea = ""
  If Not IsNull(rstFab("FR66CODPETICION")) Then 'petici�n
    If Not IsNull(rstFab("FR73CODPRODUCTO")) Then
      If rstFab("FR32OPERACION") = "F" Then 'Cantidad es del diluyente
        strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
        qryProd1(0) = rstFab("FR73CODPRODUCTO")
        Set rstProd1 = qryProd1.OpenResultset(strProd1)
        While rstProd1.StillExecuting
        Wend
        If Not rstProd1.EOF Then
          If Not IsNull(rstProd1("FR73DOSIS")) Then
            curDosis1 = CCur(rstProd1("FR73DOSIS").Value)
          Else
            curDosis1 = 0
          End If
        Else
          '???
        End If
        If curDosis1 <> 0 Then
          If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
            auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
          Else
            auxStr = rstProd1("FR73DESPRODUCTO")
          End If
          auxStr = formatear(auxStr, 30, True, True)
          auxcant = formatear(vntAuxCantidad1 * curDosis1, 8, False, False)
          auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        Else
          If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
            auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
          Else
            auxStr = rstProd1("FR73DESPRODUCTO")
          End If
          auxStr = formatear(auxStr, 30, True, True)
          auxcant = formatear(Str(vntAuxCantidad1), 8, False, False)
          auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        End If
        If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
          strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
        Else
          strcaducidad = " 24 HORAS"
        End If
        rstProd1.Close
        qryProd1.Close
        If Not IsNull(rstFab("FR28DOSIS")) Then
          If curDosis1 <> 0 Then
            curCantidad1 = Format(CCur(rstFab("FR28DOSIS")) / curDosis1, "0.00")
            vntAuxCantidad1 = objGen.ReplaceStr(curCantidad1, ",", ".", 1)
          Else
            vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR28DOSIS"), ",", ".", 1)
          End If
        Else
          vntAuxCantidad1 = 0
        End If
      Else
        strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
        qryProd1(0) = rstFab("FR73CODPRODUCTO")
        Set rstProd1 = qryProd1.OpenResultset(strProd1)
        While rstProd1.StillExecuting
        Wend
        vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR32CANTIDAD"), ",", ".", 1)
        If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
          auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
        Else
          auxStr = rstProd1("FR73DESPRODUCTO")
        End If
        auxStr = formatear(auxStr, 30, True, True)
        auxcant = formatear(rstFab("FR28DOSIS"), 8, False, False)
        auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
        If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
          strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
        Else
          strcaducidad = " 24 HORAS"
        End If
        rstProd1.Close
        qryProd1.Close
      End If
    End If
    If Not IsNull(rstFab("FR73CODPRODUCTO_2")) Then
      strProd2 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
      Set qryProd2 = objApp.rdoConnect.CreateQuery("", strProd2)
      qryProd2(0) = rstFab("FR73CODPRODUCTO_2")
      Set rstProd2 = qryProd2.OpenResultset(strProd2)
      While rstProd2.StillExecuting
      Wend
      If InStr(rstProd2("FR73DESPRODUCTO"), " ") > 0 Then
        auxStr = Left$(rstProd2("FR73DESPRODUCTO"), InStr(rstProd2("FR73DESPRODUCTO"), " "))
      Else
        auxStr = rstProd2("FR73DESPRODUCTO")
      End If
      auxStr = formatear(auxStr, 30, True, True)
      auxcant = formatear(rstFab("FR32DOSIS_2"), 8, False, False)
      auxProd2 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA_2")
      If Not rstProd2.EOF Then
        If Not IsNull(rstProd2("FR73DOSIS")) Then
          curDosis2 = CCur(rstProd2("FR73DOSIS").Value)
        Else
          curDosis2 = 0
        End If
      Else
        '???
      End If
      rstProd2.Close
      qryProd2.Close
      If Not IsNull(rstFab("FR32DOSIS_2")) Then
        If curDosis2 <> 0 Then
          curCantidad2 = Format(CCur(rstFab("FR32DOSIS_2")) / curDosis2, "0.00")
          vntAuxCantidad2 = objGen.ReplaceStr(curCantidad2, ",", ".", 1)
        Else
          vntAuxCantidad2 = objGen.ReplaceStr(rstFab("FR32DOSIS_2"), ",", ".", 1)
        End If
      Else
        vntAuxCantidad2 = 0
      End If
    End If
    If Not IsNull(rstFab("FR73CODPRODUCTO_DIL")) Then
      strSolucion = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO= ? "
      Set qrySolucion = objApp.rdoConnect.CreateQuery("", strSolucion)
      qrySolucion(0) = rstFab("FR73CODPRODUCTO_DIL")
      Set rstSolucion = qrySolucion.OpenResultset(strSolucion)
      While rstSolucion.StillExecuting
      Wend
      
      If InStr(rstSolucion("FR73DESPRODUCTO"), " ") > 0 Then
        auxStr = Left$(rstSolucion("FR73DESPRODUCTO"), InStr(rstSolucion("FR73DESPRODUCTO"), " "))
      Else
        auxStr = rstSolucion("FR73DESPRODUCTO")
      End If
      auxStr = formatear(auxStr, 30, True, True)
      auxcant = formatear(rstFab("FR32CANTIDADDIL"), 8, False, False)
      auxSolucion = auxStr & "  " & auxcant & " " & strmedidadil
      rstSolucion.Close
      qrySolucion.Close
    
    End If
  End If
    
  strdpto = "SELECT AD02DESDPTO FROM AD0200,FR6600 WHERE AD0200.AD02CODDPTO=FR6600.AD02CODDPTO AND FR66CODPETICION = ? "
  Set qrydpto = objApp.rdoConnect.CreateQuery("", strdpto)
  qrydpto(0) = rstFab("FR66CODPETICION")
  Set rstdpto = qrydpto.OpenResultset(strdpto)
  While rstdpto.StillExecuting
  Wend
  If Not rstdpto.EOF Then
    auxdpto = rstdpto(0)
  Else
    auxdpto = ""
  End If
  rstdpto.Close
  qrydpto.Close
    
  strcama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD15CODCAMA= ? "
  Set qrycama = objApp.rdoConnect.CreateQuery("", strcama)
  qrycama(0) = rstFab("AD15CODCAMA")
  Set rstcama = qrycama.OpenResultset(strcama)
  While rstcama.StillExecuting
  Wend
  If Not rstcama.EOF Then
    Auxcama = rstcama(0)
  Else
    Auxcama = ""
  End If
  rstcama.Close
  qrycama.Close
  
  strpaciente = "SELECT CI2200.CI22PRIAPEL || ',' || CI2200.CI22SEGAPEL || ',' || CI2200.CI22NOMBRE,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA= ? "
  Set qrypaciente = objApp.rdoConnect.CreateQuery("", strpaciente)
  qrypaciente(0) = rstFab("CI21CODPERSONA")
  Set rstpaciente = qrypaciente.OpenResultset(strpaciente)
  While rstpaciente.StillExecuting
  Wend
  If Not rstpaciente.EOF Then
    If Not IsNull(rstpaciente(0)) Then
      strlinea = strlinea & formatear(rstpaciente(0), 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
    Else
      strlinea = strlinea & formatear("", 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
    End If
    If Not IsNull(rstpaciente(1)) Then
      strlinea = strlinea & formatear(rstpaciente(1), 30, True, True) & " " & Auxcama & Chr(13)
    Else
      strlinea = strlinea & formatear("", 30, True, True) & " " & Auxcama & Chr(13)
    End If
  End If
  rstpaciente.Close
  qrypaciente.Close
    
    
  strfecha = "SELECT TO_CHAR(SYSDATE,'DD-MM-YYYY') FROM DUAL"
  Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
  While rstfecha.StillExecuting
  Wend
  Auxfecha = rstfecha(0).Value
    
  strVia = "SELECT * FROM FR3400 WHERE FR34CODVIA= ? "
  Set qryVia = objApp.rdoConnect.CreateQuery("", strVia)
  qryVia(0) = rstFab("FR34CODVIA")
  Set rstVia = qryVia.OpenResultset(strVia)
  While rstVia.StillExecuting
  Wend
  If Not IsNull(rstVia("FR34DESVIA")) Then
    auxVia = rstVia("FR34DESVIA")
  Else
    auxVia = ""
  End If
  auxVia = formatear(auxVia, 15, True, True)
  rstVia.Close
  qryVia.Close
    
  If IsNumeric(rstFab("FR32TIEMMININF")) Then
    strHoras = Format(rstFab("FR32TIEMMININF") \ 60, "00") & "H"
    strMinutos = Format(rstFab("FR32TIEMMININF") Mod 60, "00") & "M"
  Else
    strHoras = ""
    strMinutos = ""
  End If
  strHAdm = Format(rstFab("FR33HORATOMA"), "00.00")
  If Not IsNull(rstFab("FR32INSTRADMIN")) Then
    strInstrucctiones = rstFab("FR32INSTRADMIN")
  Else
    strInstrucctiones = ""
  End If
  i = 0
  auxInstrucctiones = ""
  While strInstrucctiones <> "" And i <> 5
    If InStr(strInstrucctiones, Chr(13)) > 0 Then
      auxInstrucctiones = auxInstrucctiones & Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)))
      strInstrucctiones = Right$(strInstrucctiones, Len(strInstrucctiones) - InStr(strInstrucctiones, Chr(13)))
    Else
      auxInstrucctiones = auxInstrucctiones & strInstrucctiones
      strInstrucctiones = ""
    End If
    i = i + 1
  Wend
  auxVolTotal = "Vtotal: " & rstFab("FR32VOLTOTAL") & " ml" '11/02/2000
    
  strlinea = strlinea & Chr(13)
  strlinea = strlinea & "  " & auxProd1 & Chr(13)
  If Trim(auxProd2) <> "" Then
    strlinea = strlinea & "  " & auxProd2 & Chr(13)
    strlinea = strlinea & "  " & auxSolucion & Chr(13)
  Else
    strlinea = strlinea & "  " & auxSolucion & Chr(13)
    strlinea = strlinea & "  " & auxProd2 & Chr(13)
  End If
  strlinea = strlinea & Chr(13)
  strlinea = strlinea & "Adm.: " & objGen.ReplaceStr(strHAdm, ",", ":", 1) & "  " & auxVia & "  " & auxVolTotal & Chr(13)
  strlinea = strlinea & "Inf. " & strHoras & " " & strMinutos & " (" & rstFab("FR32VELPERFUSION") & "ML/H" & ")" & Chr(13)
  strlinea = strlinea & "Prep. " & Auxfecha & "     Cad." & strcaducidad & Chr(13)
  strlinea = strlinea & auxInstrucctiones
  
  Listado(mintNTotalSelRows) = strlinea
  
  rstFab.MoveNext
Wend
rstFab.Close
Set rstFab = Nothing
qryFab.Close
Set qryFab = Nothing

blnEtiquetas = False
If blnImpresoraSeleccionada = False Then
  If mintNTotalSelRows > 0 Then
    Screen.MousePointer = vbDefault
    Load frmFabPrinter
    Call frmFabPrinter.Show(vbModal)
    Screen.MousePointer = vbHourglass
  End If
End If

If blnImpresoraSeleccionada = True Then
  blnEtiquetas = True
End If

If blnEtiquetas = True Then
  For mintisel = 0 To mintNTotalSelRows - 1
    crlf = Chr$(13) & Chr$(10)
    Incremento = 30
    contLinea = 0
    
    strlinea = crlf
    strlinea = strlinea & "N" & crlf
    strlinea = strlinea & "I8,1,034" & crlf
    strlinea = strlinea & "Q599,24" & crlf
    strlinea = strlinea & "R0,0" & crlf
    strlinea = strlinea & "S2" & crlf
    strlinea = strlinea & "D5" & crlf
    strlinea = strlinea & "ZB" & crlf
    
    strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1
    
    strlinea = strlinea & Texto_Etiqueta("CLINICA UNIVERSITARIA. Servicio Farmacia", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1
    
    strlinea = strlinea & "LE0," & (contLinea * Incremento) + 8 & ",740,8" & crlf
    contLinea = contLinea + 1
    strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
    contLinea = contLinea + 1

    strListado = Listado(mintisel + 1)
    auxStrListado = ""
    While strListado <> ""
      If InStr(strListado, Chr(13)) > 0 Then
        auxStrListado = Left$(strListado, InStr(strListado, Chr(13)) - 1)
        strListado = Right$(strListado, Len(strListado) - InStr(strListado, Chr(13)))
        strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
        contLinea = contLinea + 1
      Else
        If Chr(10) <> strListado Then
          auxStrListado = strListado
          strListado = ""
          strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          strListado = ""
        End If
      End If
    Wend
    
    strlinea = strlinea & "P1" & crlf
    strlinea = strlinea & " " & crlf
    
    Printer.Print strlinea
    Printer.EndDoc
  Next mintisel
End If

End Sub

Private Sub cmdRepEtiq_Click()
Dim rstmezcla As rdoResultset
Dim strmezcla As String
Dim strmedidadil As String
Dim curDosis2 As Currency
Dim curCantidad2 As Currency
Dim auxProd2 As String
Dim strProd2 As String
Dim qryProd2 As rdoQuery
Dim rstProd2 As rdoResultset
Dim auxProd1 As String
Dim strProd1 As String
Dim qryProd1 As rdoQuery
Dim rstProd1 As rdoResultset
Dim curDosis1 As Currency
Dim curCantidad1 As Currency
Dim vntAuxCantidad1 As Variant
Dim vntAuxCantidad2 As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim qryupdate As rdoQuery
Dim strlinea As String
Dim strpaciente As String
Dim qrypaciente As rdoQuery
Dim rstpaciente As rdoResultset
Dim strcama As String
Dim qrycama As rdoQuery
Dim rstcama As rdoResultset
Dim strdpto As String
Dim qrydpto As rdoQuery
Dim rstdpto As rdoResultset
Dim auxSolucion As String
Dim strSolucion As String
Dim qrySolucion As rdoQuery
Dim rstSolucion As rdoResultset
Dim strcaducidad As String
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim Auxfecha As String
Dim auxVia As String
Dim strVia As String
Dim qryVia As rdoQuery
Dim rstVia As rdoResultset
Dim strHoras As String
Dim strMinutos As String
Dim strHAdm As String
Dim strInstrucctiones As String
Dim blnEtiquetas As Boolean
Dim auxdpto As String
Dim Auxcama As String
Dim i  As Integer
Dim auxInstrucctiones As String
Dim auxVolTotal As String
Dim auxStr As String
Dim auxcant As String
Dim strFab As String
Dim rstFab As rdoResultset
Dim qryFab As rdoQuery
Dim crlf As String
Dim Incremento As Integer
Dim contLinea As Integer
Dim strListado As String
Dim auxStrListado As String
Dim hora
Dim carro
Dim mvarBkmrk As Variant
Dim peticionlinea As String
Dim strparejas As Variant
Dim intNumLin As Integer
Dim contador As Integer


If grdDBGrid1(2).Rows > 0 Then
Else
  Exit Sub
End If

cmdRepEtiq.Enabled = False
Screen.MousePointer = vbHourglass

intNumLin = grdDBGrid1(0).SelBookmarks.Count
If intNumLin > 0 Then
    strparejas = "("
    For contador = 0 To intNumLin - 1
      mvarBkmrk = grdDBGrid1(0).SelBookmarks(contador)
      peticionlinea = "(FR3300.FR66CODPETICION=" & grdDBGrid1(0).Columns(19).CellValue(mvarBkmrk) & _
                 " AND FR3300.FR28NUMLINEA=" & grdDBGrid1(0).Columns(20).CellValue(mvarBkmrk) & ")"
      If contador = intNumLin - 1 Then
        strparejas = strparejas & peticionlinea
      Else
        strparejas = strparejas & peticionlinea & " OR "
      End If
    Next contador
    strparejas = strparejas & ")"

    'transforma la coma de separaci�n de los decimales por un punto
    'de la hora a la que sale el carro
    hora = grdDBGrid1(2).Columns("Hora").Value
    hora = objGen.ReplaceStr(hora, ",", ".", 1)
    carro = grdDBGrid1(2).Columns("C�d.Carro").Value
    
    strFab = "SELECT DISTINCT"
    strFab = strFab & " FR3300.FR07CODCARRO,"
    strFab = strFab & " FR3300.FR33HORATOMA,"
    strFab = strFab & " FR6600.FR66CODPETICION,"
    strFab = strFab & " FR3200.FR28NUMLINEA,"
    strFab = strFab & " AD1500.AD15CODCAMA,"
    strFab = strFab & " FR6600.CI21CODPERSONA,"
    strFab = strFab & " FR3200.FR32OPERACION,"
    strFab = strFab & " FR3200.FR73CODPRODUCTO,"
    strFab = strFab & " FR3200.FR93CODUNIMEDIDA,"
    strFab = strFab & " FR3200.FR28DOSIS,"
    strFab = strFab & " FR3200.FR32CANTIDAD,"
    strFab = strFab & " FR3200.FR73CODPRODUCTO_2,"
    strFab = strFab & " FR3200.FR32DOSIS_2,"
    strFab = strFab & " FR3200.FR93CODUNIMEDIDA_2,"
    strFab = strFab & " FR3200.FR73CODPRODUCTO_DIL,"
    strFab = strFab & " FR3200.FR32TIEMMININF,"
    strFab = strFab & " FR3200.FR32CANTIDADDIL,"
    strFab = strFab & " FR3200.FR32VELPERFUSION,"
    strFab = strFab & " FR3200.FR32VOLTOTAL,"
    strFab = strFab & " FR3200.FR34CODVIA,"
    strFab = strFab & " FR3200.FR32INSTRADMIN"
    strFab = strFab & " FROM FR6600,FR3200,FR3300,AD1500"
    strFab = strFab & " WHERE FR6600.FR66CODPETICION=FR3200.FR66CODPETICION"
    strFab = strFab & " AND FR6600.FR66CODPETICION=FR3300.FR66CODPETICION"
    strFab = strFab & " AND FR3200.FR66CODPETICION=FR3300.FR66CODPETICION"
    strFab = strFab & " AND FR3200.FR28NUMLINEA=FR3300.FR28NUMLINEA"
    strFab = strFab & " AND FR6600.AD01CODASISTENCI=AD1500.AD01CODASISTENCI"
    strFab = strFab & " AND FR6600.AD07CODPROCESO=AD1500.AD07CODPROCESO"
    strFab = strFab & " AND FR3300.FR33INDNODIF=0"
    strFab = strFab & " AND FR3300.FR33MOTDIFERENCIA='A�adido'"
    strFab = strFab & " AND FR3300.FR33INDMEZCLA=-1"
    strFab = strFab & " AND FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
    strFab = strFab & " AND FR3300.FR33HORACARGA=?"
    strFab = strFab & " AND FR3300.FR07CODCARRO=?"
    If Checkcito.Value = 1 Then
      strFab = strFab & " AND FR3300.FR33INDCITOSTATICO=-1"
    Else
      strFab = strFab & " AND (FR3300.FR33INDCITOSTATICO=0 OR FR3300.FR33INDCITOSTATICO IS NULL)"
    End If
    If strparejas <> "" Then
      strFab = strFab & " AND " & strparejas
    End If
    
    Set qryFab = objApp.rdoConnect.CreateQuery("", strFab)
    qryFab(0) = hora 'txtText1(7)
    qryFab(1) = carro 'txtText1(3)
    Set rstFab = qryFab.OpenResultset(strFab)
    While rstFab.StillExecuting
    Wend
    
    mintNTotalSelRows = 0
    Dim Listado() As String
    
    While Not rstFab.EOF
      mintNTotalSelRows = mintNTotalSelRows + 1
      ReDim Preserve Listado(mintNTotalSelRows) As String
        'Mezclas
        Listado(mintNTotalSelRows) = ""
        auxSolucion = ""
        auxProd1 = ""
        auxProd2 = ""
        strmedidadil = "ml"
        strlinea = ""
        If Not IsNull(rstFab("FR66CODPETICION")) Then 'petici�n
            
              If Not IsNull(rstFab("FR73CODPRODUCTO")) Then
                If rstFab("FR32OPERACION") = "F" Then 'Cantidad es del diluyente
                  strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
                  Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
                  qryProd1(0) = rstFab("FR73CODPRODUCTO")
                  Set rstProd1 = qryProd1.OpenResultset(strProd1)
                  While rstProd1.StillExecuting
                  Wend
                  
                  If Not rstProd1.EOF Then
                    If Not IsNull(rstProd1("FR73DOSIS")) Then
                      curDosis1 = CCur(rstProd1("FR73DOSIS").Value)
                    Else
                      curDosis1 = 0
                    End If
                  Else
                    '???
                  End If
                  If curDosis1 <> 0 Then
                    If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                      auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
                    Else
                      auxStr = rstProd1("FR73DESPRODUCTO")
                    End If
                    auxStr = formatear(auxStr, 30, True, True)
                    auxcant = formatear(vntAuxCantidad1 * curDosis1, 8, False, False)
                    auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
                  Else
                    If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                      auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
                    Else
                      auxStr = rstProd1("FR73DESPRODUCTO")
                    End If
                    auxStr = formatear(auxStr, 30, True, True)
                    auxcant = formatear(Str(vntAuxCantidad1), 8, False, False)
                    auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
                  End If
                  If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
                    strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
                  Else
                    strcaducidad = " 24 HORAS"
                  End If
                  rstProd1.Close
                  qryProd1.Close
                  If Not IsNull(rstFab("FR28DOSIS")) Then
                    If curDosis1 <> 0 Then
                      curCantidad1 = Format(CCur(rstFab("FR28DOSIS")) / curDosis1, "0.00")
                      vntAuxCantidad1 = objGen.ReplaceStr(curCantidad1, ",", ".", 1)
                    Else
                      vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR28DOSIS"), ",", ".", 1)
                    End If
                  Else
                    vntAuxCantidad1 = 0
                  End If
                Else
                  strProd1 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
                  Set qryProd1 = objApp.rdoConnect.CreateQuery("", strProd1)
                  qryProd1(0) = rstFab("FR73CODPRODUCTO")
                  Set rstProd1 = qryProd1.OpenResultset(strProd1)
                  While rstProd1.StillExecuting
                  Wend
                  
                  vntAuxCantidad1 = objGen.ReplaceStr(rstFab("FR32CANTIDAD"), ",", ".", 1)
                  If InStr(rstProd1("FR73DESPRODUCTO"), " ") > 0 Then
                    auxStr = Left$(rstProd1("FR73DESPRODUCTO"), InStr(rstProd1("FR73DESPRODUCTO"), " "))
                  Else
                    auxStr = rstProd1("FR73DESPRODUCTO")
                  End If
                  auxStr = formatear(auxStr, 30, True, True)
                  auxcant = formatear(rstFab("FR28DOSIS"), 8, False, False)
                  auxProd1 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA")
                  If Not IsNull(rstProd1("FR73CADUCIDAD")) Then
                    strcaducidad = rstProd1("FR73CADUCIDAD") & " HORAS"
                  Else
                    strcaducidad = " 24 HORAS"
                  End If
                  rstProd1.Close
                  qryProd1.Close
                End If
              End If
              If Not IsNull(rstFab("FR73CODPRODUCTO_2")) Then
                strProd2 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
                Set qryProd2 = objApp.rdoConnect.CreateQuery("", strProd2)
                qryProd2(0) = rstFab("FR73CODPRODUCTO_2")
                Set rstProd2 = qryProd2.OpenResultset(strProd2)
                While rstProd2.StillExecuting
                Wend
                
                If InStr(rstProd2("FR73DESPRODUCTO"), " ") > 0 Then
                  auxStr = Left$(rstProd2("FR73DESPRODUCTO"), InStr(rstProd2("FR73DESPRODUCTO"), " "))
                Else
                  auxStr = rstProd2("FR73DESPRODUCTO")
                End If
                auxStr = formatear(auxStr, 30, True, True)
                auxcant = formatear(rstFab("FR32DOSIS_2"), 8, False, False)
                auxProd2 = auxStr & "  " & auxcant & " " & rstFab("FR93CODUNIMEDIDA_2")
                If Not rstProd2.EOF Then
                  If Not IsNull(rstProd2("FR73DOSIS")) Then
                    curDosis2 = CCur(rstProd2("FR73DOSIS").Value)
                  Else
                    curDosis2 = 0
                  End If
                Else
                  '???
                End If
                rstProd2.Close
                qryProd2.Close
                If Not IsNull(rstFab("FR32DOSIS_2")) Then
                  If curDosis2 <> 0 Then
                    curCantidad2 = Format(CCur(rstFab("FR32DOSIS_2")) / curDosis2, "0.00")
                    vntAuxCantidad2 = objGen.ReplaceStr(curCantidad2, ",", ".", 1)
                  Else
                    vntAuxCantidad2 = objGen.ReplaceStr(rstFab("FR32DOSIS_2"), ",", ".", 1)
                  End If
                Else
                  vntAuxCantidad2 = 0
                End If
              End If
              If Not IsNull(rstFab("FR73CODPRODUCTO_DIL")) Then
                strSolucion = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO= ? "
                Set qrySolucion = objApp.rdoConnect.CreateQuery("", strSolucion)
                qrySolucion(0) = rstFab("FR73CODPRODUCTO_DIL")
                Set rstSolucion = qrySolucion.OpenResultset(strSolucion)
                While rstSolucion.StillExecuting
                Wend
                
                If InStr(rstSolucion("FR73DESPRODUCTO"), " ") > 0 Then
                  auxStr = Left$(rstSolucion("FR73DESPRODUCTO"), InStr(rstSolucion("FR73DESPRODUCTO"), " "))
                Else
                  auxStr = rstSolucion("FR73DESPRODUCTO")
                End If
                auxStr = formatear(auxStr, 30, True, True)
                auxcant = formatear(rstFab("FR32CANTIDADDIL"), 8, False, False)
                auxSolucion = auxStr & "  " & auxcant & " " & strmedidadil
                rstSolucion.Close
                qrySolucion.Close
              End If
        End If
      
      
        strdpto = "SELECT AD02DESDPTO FROM AD0200,FR6600 WHERE AD0200.AD02CODDPTO=FR6600.AD02CODDPTO AND FR66CODPETICION = ? "
        Set qrydpto = objApp.rdoConnect.CreateQuery("", strdpto)
        qrydpto(0) = rstFab("FR66CODPETICION")
        Set rstdpto = qrydpto.OpenResultset(strdpto)
        While rstdpto.StillExecuting
        Wend
        If Not rstdpto.EOF Then
          auxdpto = rstdpto(0)
        Else
          auxdpto = ""
        End If
        rstdpto.Close
        qrydpto.Close
        
        strcama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD15CODCAMA= ? "
        Set qrycama = objApp.rdoConnect.CreateQuery("", strcama)
        qrycama(0) = rstFab("AD15CODCAMA")
        Set rstcama = qrycama.OpenResultset(strcama)
        While rstcama.StillExecuting
        Wend
        
        If Not rstcama.EOF Then
          Auxcama = rstcama(0)
        Else
          Auxcama = ""
        End If
        rstcama.Close
        qrycama.Close
      
        strpaciente = "SELECT CI2200.CI22PRIAPEL || ',' || CI2200.CI22SEGAPEL || ',' || CI2200.CI22NOMBRE,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA= ? "
        Set qrypaciente = objApp.rdoConnect.CreateQuery("", strpaciente)
        qrypaciente(0) = rstFab("CI21CODPERSONA")
        Set rstpaciente = qrypaciente.OpenResultset(strpaciente)
        While rstpaciente.StillExecuting
        Wend
        
        If Not rstpaciente.EOF Then
          If Not IsNull(rstpaciente(0)) Then
            strlinea = strlinea & formatear(rstpaciente(0), 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
          Else
            strlinea = strlinea & formatear("", 30, True, True) & " " & formatear(auxdpto, 20, True, True) & Chr(13)
          End If
          If Not IsNull(rstpaciente(1)) Then
            strlinea = strlinea & formatear(rstpaciente(1), 30, True, True) & " " & Auxcama & Chr(13)
          Else
            strlinea = strlinea & formatear("", 30, True, True) & " " & Auxcama & Chr(13)
          End If
        End If
        rstpaciente.Close
        qrypaciente.Close
        
        
        strfecha = "SELECT TO_CHAR(SYSDATE,'DD-MM-YYYY') FROM DUAL"
        Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
        While rstfecha.StillExecuting
        Wend
        
        Auxfecha = rstfecha(0).Value
        
        strVia = "SELECT * FROM FR3400 WHERE FR34CODVIA= ? "
        Set qryVia = objApp.rdoConnect.CreateQuery("", strVia)
        qryVia(0) = rstFab("FR34CODVIA")
        Set rstVia = qryVia.OpenResultset(strVia)
        While rstVia.StillExecuting
        Wend
        
        If Not IsNull(rstVia("FR34DESVIA")) Then
          auxVia = rstVia("FR34DESVIA")
        Else
          auxVia = ""
        End If
        auxVia = formatear(auxVia, 15, True, True)
        rstVia.Close
        qryVia.Close
        
        If IsNumeric(rstFab("FR32TIEMMININF")) Then
          strHoras = Format(rstFab("FR32TIEMMININF") \ 60, "00") & "H"
          strMinutos = Format(rstFab("FR32TIEMMININF") Mod 60, "00") & "M"
        Else
          strHoras = ""
          strMinutos = ""
        End If
        
        strHAdm = Format(rstFab("FR33HORATOMA"), "00.00")
        
        If Not IsNull(rstFab("FR32INSTRADMIN")) Then
          strInstrucctiones = rstFab("FR32INSTRADMIN")
        Else
          strInstrucctiones = ""
        End If
        i = 0
        auxInstrucctiones = ""
        While strInstrucctiones <> "" And i <> 5
          If InStr(strInstrucctiones, Chr(13)) > 0 Then
            auxInstrucctiones = auxInstrucctiones & Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)))
            strInstrucctiones = Right$(strInstrucctiones, Len(strInstrucctiones) - InStr(strInstrucctiones, Chr(13)))
          Else
            auxInstrucctiones = auxInstrucctiones & strInstrucctiones
            strInstrucctiones = ""
          End If
          i = i + 1
        Wend
    
        auxVolTotal = "Vtotal: " & rstFab("FR32VOLTOTAL") & " ml" '11/02/2000
        
        strlinea = strlinea & Chr(13)
        strlinea = strlinea & "  " & auxProd1 & Chr(13)
        If Trim(auxProd2) <> "" Then
          strlinea = strlinea & "  " & auxProd2 & Chr(13)
          strlinea = strlinea & "  " & auxSolucion & Chr(13)
        Else
          strlinea = strlinea & "  " & auxSolucion & Chr(13)
          strlinea = strlinea & "  " & auxProd2 & Chr(13)
        End If
        strlinea = strlinea & Chr(13)
        strlinea = strlinea & "Adm.: " & objGen.ReplaceStr(strHAdm, ",", ":", 1) & "  " & auxVia & "  " & auxVolTotal & Chr(13)
        strlinea = strlinea & "Inf. " & strHoras & " " & strMinutos & " (" & rstFab("FR32VELPERFUSION") & "ML/H" & ")" & Chr(13)
        strlinea = strlinea & "Prep. " & Auxfecha & "     Cad." & strcaducidad & Chr(13)
        strlinea = strlinea & auxInstrucctiones
      
        Listado(mintNTotalSelRows) = strlinea
      
      rstFab.MoveNext
    Wend
    rstFab.Close
    Set rstFab = Nothing
    qryFab.Close
    Set qryFab = Nothing
    
    blnEtiquetas = False
    
    If blnImpresoraSeleccionada = False Then
      If mintNTotalSelRows > 0 Then
        Screen.MousePointer = vbDefault
        Load frmFabPrinter
        Call frmFabPrinter.Show(vbModal)
        Screen.MousePointer = vbHourglass
      End If
    End If
    
    If blnImpresoraSeleccionada = True Then
      blnEtiquetas = True
    End If
    
    If blnEtiquetas = True Then
      For mintisel = 0 To mintNTotalSelRows - 1
          crlf = Chr$(13) & Chr$(10)
          Incremento = 30
          contLinea = 0
          
          strlinea = crlf
          strlinea = strlinea & "N" & crlf
          strlinea = strlinea & "I8,1,034" & crlf
          strlinea = strlinea & "Q599,24" & crlf
          strlinea = strlinea & "R0,0" & crlf
          strlinea = strlinea & "S2" & crlf
          strlinea = strlinea & "D5" & crlf
          strlinea = strlinea & "ZB" & crlf
          
          strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
          
          strlinea = strlinea & Texto_Etiqueta("CLINICA UNIVERSITARIA. Servicio Farmacia", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
          
          strlinea = strlinea & "LE0," & (contLinea * Incremento) + 8 & ",740,8" & crlf
          contLinea = contLinea + 1
          strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
      
          strListado = Listado(mintisel + 1)
          auxStrListado = ""
          While strListado <> ""
            If InStr(strListado, Chr(13)) > 0 Then
              auxStrListado = Left$(strListado, InStr(strListado, Chr(13)) - 1)
              strListado = Right$(strListado, Len(strListado) - InStr(strListado, Chr(13)))
              strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
              contLinea = contLinea + 1
            Else
              If Chr(10) <> strListado Then
                auxStrListado = strListado
                strListado = ""
                strlinea = strlinea & Texto_Etiqueta(formatear(auxStrListado, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
                contLinea = contLinea + 1
              Else
                strListado = ""
              End If
            End If
          Wend
          
          strlinea = strlinea & "P1" & crlf
          strlinea = strlinea & " " & crlf
          
          Printer.Print strlinea
          Printer.EndDoc
        
      Next mintisel
    End If
Else
    Call MsgBox("No ha seleccionado ninguna mezcla.", vbInformation, "Aviso")
End If

Screen.MousePointer = vbDefault
cmdRepEtiq.Enabled = True

End Sub
