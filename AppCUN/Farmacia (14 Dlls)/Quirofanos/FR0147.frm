VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmConfNecQuirof 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Confeccionar Necesidades de Quir�fano"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdTotal 
      Caption         =   "Generar Necesidades Totales y Enviar"
      Height          =   1095
      Left            =   10680
      TabIndex        =   31
      Top             =   2520
      Width           =   1095
   End
   Begin VB.CommandButton cmdFirmarEnviar 
      Caption         =   "Firmar y Enviar"
      Height          =   735
      Left            =   10680
      TabIndex        =   30
      Top             =   1560
      Width           =   1095
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4095
      Left            =   120
      TabIndex        =   25
      Top             =   3840
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   7223
      _Version        =   327681
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Necesidades Quir�fano"
      TabPicture(0)   =   "FR0147.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(2)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Necesidades Anestesia"
      TabPicture(1)   =   "FR0147.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(0)"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFrame1 
         Caption         =   "Necesidades Quir�fano"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3600
         Index           =   2
         Left            =   120
         TabIndex        =   28
         Top             =   360
         Width           =   10095
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3195
            Index           =   1
            Left            =   120
            TabIndex        =   29
            Top             =   360
            Width           =   9825
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17330
            _ExtentY        =   5636
            _StockProps     =   79
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Necesidades Anestesia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3600
         Index           =   0
         Left            =   -74880
         TabIndex        =   26
         Top             =   360
         Width           =   10095
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3195
            Index           =   0
            Left            =   120
            TabIndex        =   27
            Top             =   360
            Width           =   9825
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17330
            _ExtentY        =   5636
            _StockProps     =   79
         End
      End
   End
   Begin VB.CommandButton cmdValid 
      Caption         =   "Generar Necesidades"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10680
      TabIndex        =   24
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton cmdgrupterapmaterial 
      Caption         =   "Grp.Terap�ut. Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   425
      Left            =   10560
      TabIndex        =   13
      Top             =   6480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdgrupterapmedicamentos 
      Caption         =   "Grp.Terap�ut. Medicamentos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   10560
      TabIndex        =   12
      Top             =   6000
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdprotocolos 
      Caption         =   "Protocolos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   425
      Left            =   10560
      TabIndex        =   11
      Top             =   5520
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdbuscarmat 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   425
      Left            =   10560
      TabIndex        =   9
      Top             =   4560
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdbuscarprod 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   425
      Left            =   10560
      TabIndex        =   8
      Top             =   4080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdbuscargruprod 
      Caption         =   "Grupo Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   425
      Left            =   10560
      TabIndex        =   10
      Top             =   5040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n Necesidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Index           =   1
      Left            =   120
      TabIndex        =   16
      Top             =   600
      Width           =   10380
      Begin TabDlg.SSTab tabTab1 
         Height          =   2655
         Index           =   0
         Left            =   120
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   360
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   4683
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0147.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(13)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(20)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(4)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(5)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0147.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR97CODQUIROFANO"
            Height          =   330
            Index           =   5
            Left            =   120
            TabIndex        =   5
            Tag             =   "C�d.Quir�fano"
            Top             =   1560
            Width           =   555
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   720
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Quir�fano"
            Top             =   1560
            Width           =   3645
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   4
            Left            =   1080
            TabIndex        =   4
            Tag             =   " Peticionario"
            Top             =   960
            Width           =   3285
         End
         Begin VB.TextBox txtText1 
            DataField       =   "SG02COD_PDS"
            Height          =   330
            Index           =   3
            Left            =   120
            TabIndex        =   3
            Tag             =   "C�d. Peticionario"
            Top             =   960
            Width           =   915
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   3720
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   1
            Left            =   3360
            TabIndex        =   1
            Tag             =   "C�d.Estado "
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FRK3CODNECESUNID"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Necesidad "
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2385
            Index           =   2
            Left            =   -74880
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   4207
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRK3FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   7
            Tag             =   "Fecha "
            Top             =   2160
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Quirofano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   23
            Top             =   1320
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   22
            Top             =   1920
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Peticionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   120
            TabIndex        =   21
            Top             =   720
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   3360
            TabIndex        =   20
            Top             =   120
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   19
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmConfNecQuirof"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmConfNecQuirof (FR0147.FRM)                                *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Confeccionar Necesidades Quir�fano                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdbuscarmat_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscarmat.Enabled = False
  
If SSTab1.Tab = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    gstrMedicamentoMaterial = "Material"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(1).Rows > 0 Then
          grdDBGrid1(1).MoveFirst
          For i = 0 To grdDBGrid1(1).Rows - 1
            If grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(1).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(1).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(1).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(1).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(1).Columns(11).Value = "" Then
            grdDBGrid1(1).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El material: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrMedicamentoMaterial = "Material"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(0).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(0).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(0).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(0).Columns(11).Value = "" Then
            grdDBGrid1(0).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El material: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
End If
cmdbuscarmat.Enabled = True

End Sub

Private Sub cmdFirmarEnviar_Click()
    Dim intMsg As Integer
    Dim strupdate As String
   
    cmdFirmarEnviar.Enabled = False
    If IsNumeric(txtText1(0).Text) Then
        intMsg = MsgBox("�Est� seguro que desea firmar y enviar la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            cmdFirmarEnviar.Enabled = True
            strupdate = "UPDATE FRK300 SET FR26CODESTPETIC=3 WHERE FRK3CODNECESUNID=" & txtText1(0).Text
            objApp.rdoConnect.Execute strupdate, 64
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            objWinInfo.DataRefresh
            cmdFirmarEnviar.Enabled = True
        Else
          cmdFirmarEnviar.Enabled = True
        End If
    Else
      cmdFirmarEnviar.Enabled = True
    End If
   cmdFirmarEnviar.Enabled = True

End Sub

Private Sub cmdgrupterapmaterial_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

  'cmdbuscargruprod.SetFocus
  cmdgrupterapmaterial.Enabled = False

If SSTab1.Tab = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    gstrLlamador = "Material Sanitario"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(1).Rows > 0 Then
          grdDBGrid1(1).MoveFirst
          For i = 0 To grdDBGrid1(1).Rows - 1
            If grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(1).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(1).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(1).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(1).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(1).Columns(11).Value = "" Then
            grdDBGrid1(1).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El material: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrLlamador = "Material Sanitario"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(0).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(0).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(0).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(0).Columns(11).Value = "" Then
            grdDBGrid1(0).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El material: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
End If
  
  cmdgrupterapmaterial.Enabled = True

End Sub

Private Sub cmdgrupterapmedicamentos_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

  'cmdbuscargruprod.SetFocus
  cmdgrupterapmedicamentos.Enabled = False
  
If SSTab1.Tab = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    gstrLlamador = "Medicamentos"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(1).Rows > 0 Then
          grdDBGrid1(1).MoveFirst
          For i = 0 To grdDBGrid1(1).Rows - 1
            If grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(1).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(1).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(1).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(1).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(1).Columns(11).Value = "" Then
            grdDBGrid1(1).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrLlamador = "Medicamentos"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(0).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(0).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(0).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(0).Columns(11).Value = "" Then
            grdDBGrid1(0).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
End If
  
  cmdgrupterapmedicamentos.Enabled = True

End Sub

Private Sub cmdprotocolos_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdprotocolos.SetFocus
cmdprotocolos.Enabled = False
  
If SSTab1.Tab = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    Call objsecurity.LaunchProcess("FR0174")
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(1).Rows > 0 Then
          grdDBGrid1(1).MoveFirst
          For i = 0 To grdDBGrid1(1).Rows - 1
            If grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(1).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(1).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(1).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(1).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(1).Columns(11).Value = "" Then
            grdDBGrid1(1).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    Call objsecurity.LaunchProcess("FR0174")
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(0).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(0).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(0).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(0).Columns(11).Value = "" Then
            grdDBGrid1(0).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
End If
cmdprotocolos.Enabled = True

End Sub

Private Sub cmdTotal_Click()
Dim rsta, rstb As rdoResultset
Dim sqlstr, strInsert, strupdate As String
Dim numlinea As Integer
Dim rstUM As rdoResultset
Dim strUM As String
Dim auxCodNeces
Dim strQui As String
Dim rstQui As rdoResultset
Dim strdelete As String


cmdTotal.Enabled = False

    strQui = "SELECT FR97CODQUIROFANO FROM FR9700" & _
         " WHERE FR97FECINIVIG<(SELECT SYSDATE FROM DUAL) AND " & _
         "((FR97FECFINVIG IS NULL) OR (FR97FECFINVIG>(SELECT SYSDATE FROM DUAL)))"
    Set rstQui = objApp.rdoConnect.OpenResultset(strQui)
    While Not rstQui.EOF
      sqlstr = "SELECT FRK3CODNECESUNID_SEQUENCE.nextval FROM dual"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      auxCodNeces = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing

      strInsert = "INSERT INTO FRK300 (FRK3CODNECESUNID," & _
                  "FR97CODQUIROFANO,FR26CODESTPETIC,SG02COD_PDS," & _
                  "FRK3FECPETICION)" & _
                  " VALUES ("
      strInsert = strInsert & auxCodNeces & ",'"
      strInsert = strInsert & rstQui(0) & "',"
      strInsert = strInsert & "3,'" '3 FR26CODESTPETIC, enviada a farmacia
      strInsert = strInsert & objsecurity.strUser & "',"
      strInsert = strInsert & "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY'))"
      objApp.rdoConnect.Execute strInsert, 64

      'Buscar hojas de quir�fano y anestesia fr4300,fr4400
      sqlstr = "SELECT FR44CODHOJAQUIRO FROM FR4400 WHERE SG02COD IS NOT NULL AND FR44INDESTHQ=2 AND FR97CODQUIROFANO='" & rstQui(0) & "'"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      numlinea = 1
      While Not rsta.EOF
        sqlstr = "SELECT FR1800.FR73CODPRODUCTO,SUM(FR1800.FR18CANTCONSUMIDA) FROM FR1800 WHERE "
        sqlstr = sqlstr & " FR1800.FR44CODHOJAQUIRO=" & rsta(0).Value & " AND FR1800.FR18CANTCONSUMIDA>0 GROUP BY FR1800.FR73CODPRODUCTO"
        Set rstb = objApp.rdoConnect.OpenResultset(sqlstr)
        
        While Not rstb.EOF
          strUM = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO=" & rstb(0).Value
          Set rstUM = objApp.rdoConnect.OpenResultset(strUM)
          If IsNull(rstUM(0).Value) Then
            strUM = "NE"
          Else
            strUM = rstUM(0).Value
          End If
          rstUM.Close
          Set rstUM = Nothing
          
          strInsert = "INSERT INTO FRK400 (FRK3CODNECESUNID," & _
                      "FRK4NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA," & _
                      "FRK4CANTNECESQUIR,FRK4INDBLOQUEO,FRK4INDQUIANE) VALUES (" & _
                      auxCodNeces & "," & _
                      numlinea & "," & _
                      rstb(0).Value & "," & _
                      "'" & strUM & "'" & "," & _
                      rstb(1).Value & ",0,0)"
          objApp.rdoConnect.Execute strInsert, 64
          numlinea = numlinea + 1
          rstb.MoveNext
        Wend
        rstb.Close
        Set rstb = Nothing
        strupdate = "UPDATE FR4400 SET FR44INDESTHQ=3 WHERE FR44CODHOJAQUIRO=" & rsta(0).Value
        objApp.rdoConnect.Execute strupdate, 64
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      
      sqlstr = "SELECT FR43CODHOJAANEST FROM FR4300 WHERE SG02COD IS NOT NULL AND FR43INDESTHA=2 AND FR97CODQUIROFANO='" & rstQui(0) & "'"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      While Not rsta.EOF
        sqlstr = "SELECT FR1700.FR73CODPRODUCTO,SUM(FR1700.FR17CANTIDAD)" & _
                 " FROM FR1700 WHERE FR1700.FR43CODHOJAANEST=" & rsta(0).Value
        sqlstr = sqlstr & " AND FR1700.FR17CANTIDAD>0 GROUP BY FR1700.FR73CODPRODUCTO"
        Set rstb = objApp.rdoConnect.OpenResultset(sqlstr)
        
        While Not rstb.EOF
          strUM = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO=" & rstb(0).Value
          Set rstUM = objApp.rdoConnect.OpenResultset(strUM)
          If IsNull(rstUM(0).Value) Then
            strUM = "NE"
          Else
            strUM = rstUM(0).Value
          End If
          rstUM.Close
          Set rstUM = Nothing
            
          strInsert = "INSERT INTO FRK400 (FRK3CODNECESUNID," & _
                      "FRK4NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA," & _
                      "FRK4CANTNECESQUIR,FRK4INDBLOQUEO,FRK4INDQUIANE) VALUES (" & _
                      auxCodNeces & "," & _
                      numlinea & "," & _
                      rstb(0).Value & "," & _
                      "'" & strUM & "'" & "," & _
                      rstb(1).Value & ",0,1)"
          objApp.rdoConnect.Execute strInsert, 64
          numlinea = numlinea + 1
          rstb.MoveNext
        Wend
        rstb.Close
        Set rstb = Nothing
        strupdate = "UPDATE FR4300 SET FR43INDESTHA=3 WHERE FR43CODHOJAANEST=" & rsta(0).Value
        objApp.rdoConnect.Execute strupdate, 64
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      rstQui.MoveNext
    Wend
    rstQui.Close
    Set rstQui = Nothing

  strdelete = "DELETE FROM FRK300 WHERE FRK3CODNECESUNID NOT IN (SELECT FRK3CODNECESUNID FROM FRK400)"
  objApp.rdoConnect.Execute strdelete, 64

MsgBox "Necesidades de quir�fano generadas y enviadas a Farmacia", vbInformation, "Aviso"

cmdTotal.Enabled = True


End Sub

Private Sub cmdValid_Click()
Dim rsta, rstb As rdoResultset
Dim sqlstr, strInsert, strupdate As String
Dim numlinea As Integer
Dim rstUM As rdoResultset
Dim strUM As String

Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
Call objWinInfo.DataSave

cmdValid.Enabled = False

    sqlstr = "SELECT COUNT(*) FROM FRK400 WHERE FRK3CODNECESUNID=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta(0).Value = 0 Then
      'Buscar hojas de quir�fano y anestesia fr4300,fr4400
      sqlstr = "SELECT FR44CODHOJAQUIRO FROM FR4400 WHERE SG02COD IS NOT NULL AND FR44INDESTHQ=2 AND FR97CODQUIROFANO='" & txtText1(5) & "'"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      numlinea = 1
      While Not rsta.EOF
        sqlstr = "SELECT FR1800.FR73CODPRODUCTO,SUM(FR1800.FR18CANTCONSUMIDA) FROM FR1800 WHERE "
        sqlstr = sqlstr & " FR1800.FR44CODHOJAQUIRO=" & rsta(0).Value & " AND FR1800.FR18CANTCONSUMIDA>0 GROUP BY FR1800.FR73CODPRODUCTO"
        Set rstb = objApp.rdoConnect.OpenResultset(sqlstr)
        
        While Not rstb.EOF
          strUM = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO=" & rstb(0).Value
          Set rstUM = objApp.rdoConnect.OpenResultset(strUM)
          If IsNull(rstUM(0).Value) Then
            strUM = "NE"
          Else
            strUM = rstUM(0).Value
          End If
          rstUM.Close
          Set rstUM = Nothing
          
          strInsert = "INSERT INTO FRK400 (FRK3CODNECESUNID," & _
                      "FRK4NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA," & _
                      "FRK4CANTNECESQUIR,FRK4INDBLOQUEO,FRK4INDQUIANE) VALUES (" & _
                      txtText1(0).Text & "," & _
                      numlinea & "," & _
                      rstb(0).Value & "," & _
                      "'" & strUM & "'" & "," & _
                      rstb(1).Value & ",0,0)"
          objApp.rdoConnect.Execute strInsert, 64
          strupdate = "UPDATE FR4400 SET FR44INDESTHQ=3 WHERE FR44CODHOJAQUIRO=" & rsta(0).Value
          objApp.rdoConnect.Execute strupdate, 64
          numlinea = numlinea + 1
          rstb.MoveNext
        Wend
        rstb.Close
        Set rstb = Nothing
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      
      sqlstr = "SELECT FR43CODHOJAANEST FROM FR4300 WHERE SG02COD IS NOT NULL AND FR43INDESTHA=2 AND FR97CODQUIROFANO='" & txtText1(5) & "'"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      While Not rsta.EOF
        sqlstr = "SELECT FR1700.FR73CODPRODUCTO,SUM(FR1700.FR17CANTIDAD)" & _
                 " FROM FR1700 WHERE FR1700.FR43CODHOJAANEST=" & rsta(0).Value
        sqlstr = sqlstr & " AND FR1700.FR17CANTIDAD>0 GROUP BY FR1700.FR73CODPRODUCTO"
        Set rstb = objApp.rdoConnect.OpenResultset(sqlstr)
        
        While Not rstb.EOF
          strUM = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO=" & rstb(0).Value
          Set rstUM = objApp.rdoConnect.OpenResultset(strUM)
          If IsNull(rstUM(0).Value) Then
            strUM = "NE"
          Else
            strUM = rstUM(0).Value
          End If
          rstUM.Close
          Set rstUM = Nothing
            
          strInsert = "INSERT INTO FRK400 (FRK3CODNECESUNID," & _
                      "FRK4NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA," & _
                      "FRK4CANTNECESQUIR,FRK4INDBLOQUEO,FRK4INDQUIANE) VALUES (" & _
                      txtText1(0).Text & "," & _
                      numlinea & "," & _
                      rstb(0).Value & "," & _
                      "'" & strUM & "'" & "," & _
                      rstb(1).Value & ",0,1)"
          objApp.rdoConnect.Execute strInsert, 64
          numlinea = numlinea + 1
          strupdate = "UPDATE FR4300 SET FR43INDESTHA=3 WHERE FR43CODHOJAANEST=" & rsta(0).Value
          objApp.rdoConnect.Execute strupdate, 64
          rstb.MoveNext
        Wend
        rstb.Close
        Set rstb = Nothing
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    End If

cmdValid.Enabled = True

End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)

If SSTab1.Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
Else
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdbuscargruprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscargruprod.Enabled = False
  
If SSTab1.Tab = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    Call objsecurity.LaunchProcess("FR0168")
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(1).Rows > 0 Then
          grdDBGrid1(1).MoveFirst
          For i = 0 To grdDBGrid1(1).Rows - 1
            If grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(1).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(1).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(1).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(1).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(1).Columns(11).Value = "" Then
            grdDBGrid1(1).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    Call objsecurity.LaunchProcess("FR0168")
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(0).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(0).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(0).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(0).Columns(11).Value = "" Then
            grdDBGrid1(0).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
End If
cmdbuscargruprod.Enabled = True
  
End Sub

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

If SSTab1.Tab = 0 Then
  cmdbuscargruprod.SetFocus
  cmdbuscarprod.Enabled = False
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    gstrMedicamentoMaterial = "Medicamento"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(1).Rows > 0 Then
          grdDBGrid1(1).MoveFirst
          For i = 0 To grdDBGrid1(1).Rows - 1
            If grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(1).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(1).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(1).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(1).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(1).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(1).Columns(11).Value = "" Then
            grdDBGrid1(1).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
  cmdbuscarprod.Enabled = True
Else
  cmdbuscargruprod.SetFocus
  cmdbuscarprod.Enabled = False
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrMedicamentoMaterial = "Medicamento"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) 'c�digo
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'c�digo interno
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2) 'descripci�n
          stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          grdDBGrid1(0).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
          grdDBGrid1(0).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
          grdDBGrid1(0).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
          grdDBGrid1(0).Columns(7).Value = rsta.rdoColumns("FR73REFERENCIA").Value
          rsta.Close
          Set rsta = Nothing
          If grdDBGrid1(0).Columns(11).Value = "" Then
            grdDBGrid1(0).Columns(11).Value = "NE"
          End If
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If
  cmdbuscarprod.Enabled = True

End If


End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Necesidad Unidad"
    .blnAskPrimary = False
    .strTable = "FRK300"
    .strWhere = "FR26CODESTPETIC=1" 'REDACTADA
    
    Call .FormAddOrderField("FRK3CODNECESUNID", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
    Call .FormAddFilterWhere(strKey, "FRK3CODNECESUNID", "C�d. Necesidad Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Peticionario", cwString)
    Call .FormAddFilterWhere(strKey, "FRK3FECPETICION", "Fecha Petici�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR97CODQUIROFANO", "C�digo Quir�fano", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
    Call .FormAddFilterOrder(strKey, "FRK3CODNECESUNID", "C�digo Necesidad Unidad")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle Necesidad Quirofano"
    
    .intAllowance = cwAllowDelete + cwAllowModify
    
    .strTable = "FRK400"
    
    .strWhere = "FRK4INDQUIANE=0"
    
    Call .FormAddOrderField("FRK4NUMLINEA", cwAscending)
    Call .FormAddRelation("FRK3CODNECESUNID", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objMultiInfo2
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle Necesidad Anestesia"
    
    .intAllowance = cwAllowDelete + cwAllowModify
    
    .strTable = "FRK400"
    
    .strWhere = "FRK4INDQUIANE=1"
    
    Call .FormAddOrderField("FRK4NUMLINEA", cwAscending)
    Call .FormAddRelation("FRK3CODNECESUNID", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d. Nec. Quir�fano", "FRK3CODNECESUNID", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "N�mero L�nea", "FRK4NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�digo Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�d.Int", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Desc.U.M", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FRK4CANTNECESQUIR", cwNumeric, 11)
    Call .GridAddColumn(objMultiInfo, "Q_A", "FRK4INDQUIANE", cwNumeric, 1)
    
    Call .GridAddColumn(objMultiInfo2, "C�d. Nec. Quir�fano", "FRK3CODNECESUNID", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo2, "N�mero L�nea", "FRK4NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo2, "C�digo Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo2, "C�d.Int", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo2, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo2, "Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo2, "F.F", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo2, "Dosis", "", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo2, "U.M", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo2, "Desc.U.M", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo2, "Cantidad", "FRK4CANTNECESQUIR", cwNumeric, 11)
    Call .GridAddColumn(objMultiInfo2, "Q_A", "FRK4INDQUIANE", cwNumeric, 1)
    
    Call .FormCreateInfo(objMasterInfo)

    Call .FormChangeColor(objMultiInfo)
    Call .FormChangeColor(objMultiInfo2)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "SG02COD_PDS", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(4), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "FR97CODQUIROFANO", "SELECT * FROM FR9700 WHERE FR97CODQUIROFANO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "FR97DESQUIROFANO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(2), "FR26DESESTADOPET")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(8), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(9), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(10), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(7), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), grdDBGrid1(1).Columns(12), "FR93DESUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(8), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(9), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(10), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(11)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(11)), grdDBGrid1(0).Columns(12), "FR93DESUNIMEDIDA")
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnReadOnly = True
    .CtrlGetInfo(txtText1(3)).blnForeign = True
    .CtrlGetInfo(txtText1(5)).blnForeign = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
  grdDBGrid1(2).Columns(1).Width = 1000
  grdDBGrid1(2).Columns(2).Width = 700
  grdDBGrid1(2).Columns(3).Width = 1400
  grdDBGrid1(2).Columns(4).Width = 1000
  grdDBGrid1(2).Columns(5).Width = 1600
  grdDBGrid1(2).Columns(6).Width = 700
  grdDBGrid1(2).Columns(7).Width = 1600
  grdDBGrid1(2).Columns(8).Width = 1000
  
  grdDBGrid1(1).Columns(0).Width = 0
  grdDBGrid1(1).Columns(6).Width = 800
  grdDBGrid1(1).Columns(7).Width = 1500
  grdDBGrid1(1).Columns(8).Width = 3300
  grdDBGrid1(1).Columns(9).Width = 800
  grdDBGrid1(1).Columns(10).Width = 1000
  grdDBGrid1(1).Columns(11).Width = 900
  
  grdDBGrid1(1).RemoveAll
  grdDBGrid1(1).Refresh
  
  grdDBGrid1(1).Columns(3).Visible = False
  grdDBGrid1(1).Columns(4).Visible = False
  grdDBGrid1(1).Columns(5).Visible = False
  grdDBGrid1(1).Columns(12).Visible = False

  grdDBGrid1(1).Columns(14).Visible = False

  grdDBGrid1(0).Columns(0).Width = 0
  grdDBGrid1(0).Columns(6).Width = 800
  grdDBGrid1(0).Columns(7).Width = 1500
  grdDBGrid1(0).Columns(8).Width = 3300
  grdDBGrid1(0).Columns(9).Width = 800
  grdDBGrid1(0).Columns(10).Width = 1000
  grdDBGrid1(0).Columns(11).Width = 900
  
  grdDBGrid1(0).RemoveAll
  grdDBGrid1(0).Refresh
  
  grdDBGrid1(0).Columns(3).Visible = False
  grdDBGrid1(0).Columns(4).Visible = False
  grdDBGrid1(0).Columns(5).Visible = False
  grdDBGrid1(0).Columns(12).Visible = False

  grdDBGrid1(0).Columns(14).Visible = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset

  If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
    sqlstrPreg = "SELECT * FROM FRK400 WHERE "
    sqlstrPreg = sqlstrPreg & " FRK3CODNECESUNID=" & txtText1(0).Text
    Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
    If rstPreg.EOF Then
      strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
      " no tiene asociado ningun Producto." & Chr(13) & _
      "� Desea borrar dicha Necesidad ?" & Chr(13) & _
      "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
      "asociarle al menos un Producto. ", 36, "Necesidades")
      If strPregunta = vbYes Then
        strdelete = "DELETE FROM FRK300 WHERE FRK3CODNECESUNID = " & txtText1(0).Text
        objApp.rdoConnect.Execute strdelete, 64
        objWinInfo.objWinActiveForm.blnChanged = False
      Else
        rstPreg.Close
        Set rstPreg = Nothing
        intCancel = 1
        Exit Sub
      End If
    End If
    rstPreg.Close
    Set rstPreg = Nothing
    intCancel = objWinInfo.WinExit
  Else
    intCancel = objWinInfo.WinExit
  End If
  
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txtText1(3)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"
     .strOrder = "ORDER BY SG02APE1"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Peticionario"

     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(3), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Necesidad Unidad" And strCtrl = "txtText1(5)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR97CODQUIROFANO"
     .strWhere = " WHERE " & _
         "FR97FECINIVIG<(SELECT SYSDATE FROM DUAL) AND " & _
         "((FR97FECFINVIG IS NULL) OR (FR97FECFINVIG>(SELECT SYSDATE FROM DUAL)))"
         
     .strOrder = "ORDER BY FR97CODQUIROFANO"

     Set objField = .AddField("FR97CODQUIROFANO")
     objField.strSmallDesc = "C�digo Quir�fano"

     Set objField = .AddField("FR97DESQUIROFANO")
     objField.strSmallDesc = "Descripci�n Quir�fano"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(5), .cllValues("FR97CODQUIROFANO"))
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub


Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim mensaje As String

If txtText1(5).Text <> "" Then
    stra = "SELECT * FROM FR9700 " & _
           "WHERE " & _
           "FR97FECINIVIG<(SELECT SYSDATE FROM DUAL) AND " & _
           "((FR97FECFINVIG IS NULL) OR (FR97FECFINVIG>(SELECT SYSDATE FROM DUAL)))" & _
           " AND FR97CODQUIROFANO=" & txtText1(5).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.EOF Then
        mensaje = MsgBox("El servicio es incorrecto.", vbInformation, "Aviso")
        blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset

  '2 Nuevo,3 Abrir,4 Guardar,6 Imprimir,8 Borrar,10 Cortar,11 Copiar
  '12 Pegar,14 Deshacer,16 Localizar,18 Filtro,19 No Filtro,21 Primer Registro
  '22 Anterior,23 Siguiente,24 Ultimo,26 Refrescar,28 Mantenimiento,30 Salir
  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case btnButton.Index
    Case 2, 3, 16, 18, 19, 21, 22, 23, 24, 26, 28, 30
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FRK400 WHERE "
        sqlstrPreg = sqlstrPreg & " FRK3CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FRK300 WHERE FRK3CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FRK3CODNECESUNID_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    Call objWinInfo.CtrlSet(txtText1(1), 1) 'FR26CODESTPETIC, REDACTADA
    Call objWinInfo.CtrlSet(txtText1(3), objsecurity.strUser)
    rsta.Close
    Set rsta = Nothing
    txtText1(3).SetFocus
    Call objWinInfo.CtrlGotFocus
    Call objWinInfo.CtrlLostFocus
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle Necesidad Quirofano" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FRK4NUMLINEA) FROM FRK400 WHERE FRK3CODNECESUNID=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = grdDBGrid1(1).Rows
        Else
          linea = rstlinea.rdoColumns(0).Value + grdDBGrid1(1).Rows
        End If
        grdDBGrid1(1).Columns(4).Value = linea
        grdDBGrid1(1).Columns(14).Value = 0
        grdDBGrid1(1).Col = 5
        SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    ElseIf btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle Necesidad Anestesia" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FRK4NUMLINEA) FROM FRK400 WHERE FRK3CODNECESUNID=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = grdDBGrid1(0).Rows
        Else
          linea = rstlinea.rdoColumns(0).Value + grdDBGrid1(0).Rows
        End If
        grdDBGrid1(0).Columns(4).Value = linea
        grdDBGrid1(0).Columns(14).Value = 1
        grdDBGrid1(0).Col = 5
        SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
'Menu Datos                                Botones
'10 Nuevo ................................ 2 Nuevo
'20 Abrir ................................ 3 Abrir
'40 Guardar .............................. 4 Guardar
'60 Eliminar ............................. 8 Borrar
'80 Imprimir ............................. 6 Imprimir
'100 Salir ............................... 30 Salir
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset

  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case intIndex
    Case 20, 100
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FRK400 WHERE "
        sqlstrPreg = sqlstrPreg & " FRK3CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FRK300 WHERE FRK3CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If

  If intIndex = 10 Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)

End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
'Menu Filtro                               Botones
'10 Poner Filtro ......................... 18 Filtro
'20 Quitar Filtro ........................ 19 No Filtro
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset
  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case intIndex
    Case 10, 20
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FRK400 WHERE "
        sqlstrPreg = sqlstrPreg & " FRK3CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FRK300 WHERE FRK3CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If

  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
'Menu registro                             Botones
'10 Localizar ............................ 16 Localizar
'20 Restaurar ............................
'40 Primero .............................. 21 Primer Registro
'50 Anterior ............................. 22 Anterior
'60 Siguiente ............................ 23 Siguiente
'70 Ultimo ............................... 24 Ultimo
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset
  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case intIndex
    Case 10, 40, 50, 60, 70
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FRK400 WHERE "
        sqlstrPreg = sqlstrPreg & " FRK3CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FRK300 WHERE FRK3CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If
  
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
'Menu Opciones                             Botones
'10 Refrescar ............................ 26 Refrescar
'20 Mantenimiento ........................ 28 Mantenimiento
'40 Colores ..............................
'50 Alta Masiva ..........................
Dim strdelete, strPregunta, sqlstrPreg As String
Dim rstPreg As rdoResultset
  If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
    Select Case intIndex
    Case 10, 20
      If grdDBGrid1(0).Rows = 0 And txtText1(0).Text <> "" Then
        sqlstrPreg = "SELECT * FROM FRK400 WHERE "
        sqlstrPreg = sqlstrPreg & " FRK3CODNECESUNID=" & txtText1(0).Text
        Set rstPreg = objApp.rdoConnect.OpenResultset(sqlstrPreg)
        If rstPreg.EOF Then
          strPregunta = MsgBox("La Necesidad con C�digo = " & txtText1(0).Text & _
          " no tiene asociado ningun Producto." & Chr(13) & _
          "� Desea borrar dicha Necesidad ?" & Chr(13) & _
          "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
          "asociarle al menos un Producto. ", 36, "Necesidades")
          If strPregunta = vbYes Then
            strdelete = "DELETE FROM FRK300 WHERE FRK3CODNECESUNID = " & txtText1(0).Text
            objApp.rdoConnect.Execute strdelete, 64
          Else
            Exit Sub
          End If
        End If
        rstPreg.Close
        Set rstPreg = Nothing
      End If
    End Select
  End If
  
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)

End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
Dim stra As String
Dim rsta As rdoResultset
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 0 And grdDBGrid1(0).Columns(5).Value <> "" And grdDBGrid1(0).Columns(11).Value = "" Then
      stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(5).Value
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(11), rsta(0).Value)
      rsta.Close
      Set rsta = Nothing
    End If
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


