VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmRedHojQuirFar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Redactar Hoja de Quirofano"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3570
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   120
      TabIndex        =   20
      Top             =   120
      Width           =   11655
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "Consultar"
         Height          =   330
         Left            =   9240
         TabIndex        =   37
         Top             =   480
         Width           =   975
      End
      Begin VB.TextBox txtEstado 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   9480
         Locked          =   -1  'True
         TabIndex        =   35
         Top             =   360
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CommandButton cmdTerminar 
         Caption         =   "FACTURAR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   7560
         TabIndex        =   34
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton cmdSalir 
         Caption         =   "SALIR"
         Height          =   330
         Left            =   10320
         TabIndex        =   33
         Top             =   480
         Width           =   975
      End
      Begin VB.TextBox txtCodPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   9960
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   360
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.TextBox txtHistoria 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   6120
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   480
         Width           =   1095
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   480
         Width           =   4695
      End
      Begin VB.TextBox txtHojaQuirofano 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   480
         Width           =   1095
      End
      Begin VB.TextBox txtProceso 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   10680
         Locked          =   -1  'True
         TabIndex        =   23
         Top             =   360
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtAsistencia 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   11160
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   360
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Est."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   9480
         TabIndex        =   36
         Top             =   120
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d.Pac."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   9960
         TabIndex        =   31
         Top             =   120
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   6120
         TabIndex        =   29
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   1320
         TabIndex        =   27
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Proc."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   10680
         TabIndex        =   25
         Top             =   120
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Asi."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   11160
         TabIndex        =   24
         Top             =   120
         Visible         =   0   'False
         Width           =   330
      End
      Begin VB.Label lblLabel1 
         Caption         =   "H.Quir�fano"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   28
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   1455
      End
   End
   Begin TabDlg.SSTab tabGeneral1 
      Height          =   6855
      Index           =   1
      Left            =   120
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1200
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   12091
      _Version        =   327681
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Material y Medicaci�n Quir�fano"
      TabPicture(0)   =   "FR0226.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "cmdinter(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdgruterapmaterial(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdbuscarprot(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdbuscarmaterial(0)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdgruterapmedicamento(0)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdbuscargruprod(0)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdbuscarprod(0)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdbuscarprotesis"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "fraFrame1(3)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "cmdleerlector(0)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).ControlCount=   10
      TabCaption(1)   =   "Material y Medicaci�n Anestesia"
      TabPicture(1)   =   "FR0226.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdleerlector(1)"
      Tab(1).Control(1)=   "fraFrame1(4)"
      Tab(1).Control(2)=   "cmdinter(1)"
      Tab(1).Control(3)=   "cmdbuscarprod(1)"
      Tab(1).Control(4)=   "cmdbuscargruprod(1)"
      Tab(1).Control(5)=   "cmdgruterapmedicamento(1)"
      Tab(1).Control(6)=   "cmdbuscarmaterial(1)"
      Tab(1).Control(7)=   "cmdbuscarprot(1)"
      Tab(1).Control(8)=   "cmdgruterapmaterial(1)"
      Tab(1).ControlCount=   9
      Begin VB.CommandButton cmdleerlector 
         Caption         =   "LECTOR"
         Height          =   1035
         Index           =   1
         Left            =   -64680
         Picture         =   "FR0226.frx":0038
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   4440
         Width           =   975
      End
      Begin VB.CommandButton cmdleerlector 
         Caption         =   "LECTOR"
         Height          =   1035
         Index           =   0
         Left            =   10320
         Picture         =   "FR0226.frx":087A
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   4800
         Width           =   975
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6240
         Index           =   3
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   9735
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5835
            Index           =   3
            Left            =   120
            TabIndex        =   19
            Top             =   360
            Width           =   9465
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   2
            stylesets(0).Name=   "Material"
            stylesets(0).BackColor=   12615935
            stylesets(0).Picture=   "FR0226.frx":10BC
            stylesets(1).Name=   "Medicamento"
            stylesets(1).BackColor=   16777215
            stylesets(1).Picture=   "FR0226.frx":10D8
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   16695
            _ExtentY        =   10292
            _StockProps     =   79
         End
      End
      Begin VB.CommandButton cmdbuscarprotesis 
         Caption         =   "Protesis"
         Height          =   375
         Left            =   9960
         TabIndex        =   17
         Top             =   1800
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprod 
         Caption         =   "Medicamentos"
         Height          =   375
         Index           =   0
         Left            =   9960
         TabIndex        =   16
         Top             =   840
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Grupo Productos"
         Height          =   375
         Index           =   0
         Left            =   9960
         TabIndex        =   15
         Top             =   2280
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmedicamento 
         Caption         =   "Grupos Terap�uticos      Medicamentos"
         Height          =   615
         Index           =   0
         Left            =   9960
         TabIndex        =   14
         Top             =   3240
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarmaterial 
         Caption         =   "Productos"
         Height          =   375
         Index           =   0
         Left            =   9960
         TabIndex        =   13
         Top             =   1320
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprot 
         Caption         =   "Protocolos"
         Height          =   375
         Index           =   0
         Left            =   9960
         TabIndex        =   12
         Top             =   2760
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmaterial 
         Caption         =   "Grupos Terap�uticos         Productos"
         Height          =   615
         Index           =   0
         Left            =   9960
         TabIndex        =   11
         Top             =   3960
         Width           =   1600
      End
      Begin VB.CommandButton cmdinter 
         Caption         =   "Interacciones"
         Height          =   375
         Index           =   0
         Left            =   9960
         TabIndex        =   10
         Top             =   6000
         Width           =   1575
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6360
         Index           =   4
         Left            =   -74880
         TabIndex        =   8
         Top             =   360
         Width           =   9735
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5955
            Index           =   4
            Left            =   120
            TabIndex        =   9
            Top             =   360
            Width           =   9465
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   2
            stylesets(0).Name=   "Material"
            stylesets(0).BackColor=   12615935
            stylesets(0).Picture=   "FR0226.frx":10F4
            stylesets(1).Name=   "Medicamento"
            stylesets(1).BackColor=   16777215
            stylesets(1).Picture=   "FR0226.frx":1110
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   16695
            _ExtentY        =   10504
            _StockProps     =   79
         End
      End
      Begin VB.CommandButton cmdinter 
         Caption         =   "Interacciones"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   7
         Top             =   5760
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprod 
         Caption         =   "Medicamentos"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   6
         Top             =   840
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Grupo Productos"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   5
         Top             =   1800
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmedicamento 
         Caption         =   "Grupos Terap�uticos      Medicamentos"
         Height          =   615
         Index           =   1
         Left            =   -65040
         TabIndex        =   4
         Top             =   2760
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarmaterial 
         Caption         =   "Productos"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   3
         Top             =   1320
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprot 
         Caption         =   "Protocolos"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   2
         Top             =   2280
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmaterial 
         Caption         =   "Grupos Terap�uticos         Productos"
         Height          =   615
         Index           =   1
         Left            =   -65040
         TabIndex        =   1
         Top             =   3480
         Width           =   1600
      End
   End
End
Attribute VB_Name = "frmRedHojQuirFar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedHojQuirFar (FR0226_TEMP.FRM)                                 *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Redactar Hoja Quir�fano                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim blnCancel As Boolean


Private Sub cmdbuscarprotesis_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

cmdbuscarprotesis.Enabled = False

  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrMedicamentoMaterial = "Protesis"
    Call objsecurity.LaunchProcess("FR0166")
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Quirofano
      grdDBGrid1(3).Redraw = False
      grdDBGrid1(3).MoveLast
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If

cmdbuscarprotesis.Enabled = True


End Sub

Private Sub cmdConsultar_Click()
Dim strNumHQ As String

  cmdConsultar.Enabled = False
  
  strNumHQ = txtHojaQuirofano
  Call objsecurity.LaunchProcess("PR0553", strNumHQ)

  cmdConsultar.Enabled = True

  'gstrLlamador = "frmFirmarHojQuirof"
  'gintcodhojaquiro = txtHojaQuirofano.Text
  'Call objsecurity.LaunchProcess("FR0126")
  'gstrLlamador = ""
  'gintcodhojaquiro = Empty

End Sub

Private Sub cmdleerlector_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

cmdleerlector(Index).Enabled = False

Load frmLECTOR
Call frmLECTOR.Show(vbModal)

If Index = 0 Then 'material quirofano
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & gintprodbuscado(v, 3)
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Quirofano
      grdDBGrid1(3).Redraw = False
      grdDBGrid1(3).MoveLast
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else 'material anestesia
  If txtHojaQuirofano.Text Then
    noinsertar = True
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & gintprodbuscado(v, 3)
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Anestesia
      grdDBGrid1(4).Redraw = False
      grdDBGrid1(4).MoveLast
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If

End If

cmdleerlector(Index).Enabled = True


End Sub

Private Sub cmdSalir_Click()

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

  Unload Me

End Sub

Private Sub cmdTerminar_Click()
Dim strInsert, strupdate As String
Dim rstSelect As rdoResultset
Dim strSelect As String
Dim rstDpto As rdoResultset
Dim strDpto As String
Dim mensaje, servicio

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If
    
  mensaje = MsgBox("Esta seguro que desea facturar la hoja.", vbCritical + vbYesNo, "Aviso")
  If mensaje = vbNo Then
    Exit Sub
  End If
    
cmdTerminar.Enabled = False
Screen.MousePointer = vbHourglass
Me.Enabled = False
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    strDpto = "SELECT AD02CODDPTO FROM PR0400 WHERE PR62CODHOJAQUIR=" & txtHojaQuirofano.Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strDpto)
    'servicio = txtText1(38).Text
    servicio = rstDpto(0).Value
    rstDpto.Close
    Set rstDpto = Nothing

    strInsert = "INSERT INTO FR6500 ("
    strInsert = strInsert & "FR65CODIGO,CI21CODPERSONA,"
    strInsert = strInsert & "FR73CODPRODUCTO,FR65FECHA,"
    strInsert = strInsert & "FR65HORA,AD02CODDPTO,"
    strInsert = strInsert & "FR65CANTIDAD,FR65INDQUIROFANO,FR65INDIMPUTQUIR,"
    strInsert = strInsert & "PR62CODHOJAQUIR,AD07CODPROCESO,"
    strInsert = strInsert & "AD01CODASISTENCI)"
    strInsert = strInsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
    strInsert = strInsert & txtCodPaciente.Text & ","
    strInsert = strInsert & "FR1800.FR73CODPRODUCTO,"
    strInsert = strInsert & "SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')),"
    strInsert = strInsert & servicio & ","
    strInsert = strInsert & "FR1800.FR18CANTCONSUMIDA,-1,-1,"
    'strInsert = strInsert & "DECODE(FR7300.FR73INDFACTDOSIS,-1,FR1800.FR18CANTCONSUMIDA,CEIL(FR1800.FR18CANTCONSUMIDA)),-1,"
    strInsert = strInsert & txtHojaQuirofano.Text & ","
    strInsert = strInsert & txtProceso.Text & "," & txtAsistencia.Text
    strInsert = strInsert & " FROM FR1800 WHERE FR1800.PR62CODHOJAQUIR=" & txtHojaQuirofano.Text
    strInsert = strInsert & " AND FR1800.FR18CANTCONSUMIDA>0 "
    'strInsert = strInsert & " AND FR1800.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
    
    objApp.rdoConnect.Execute strInsert, 64

    strInsert = "INSERT INTO FR6500 ("
    strInsert = strInsert & "FR65CODIGO,CI21CODPERSONA,"
    strInsert = strInsert & "FR73CODPRODUCTO,FR65FECHA,"
    strInsert = strInsert & "FR65HORA,AD02CODDPTO,"
    strInsert = strInsert & "FR65CANTIDAD,FR65INDQUIROFANO,FR65INDIMPUTQUIR,"
    strInsert = strInsert & "PR62CODHOJAQUIR,AD07CODPROCESO,"
    strInsert = strInsert & "AD01CODASISTENCI)"
    strInsert = strInsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
    strInsert = strInsert & txtCodPaciente.Text & ","
    strInsert = strInsert & "FR1700.FR73CODPRODUCTO,"
    strInsert = strInsert & "SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')),"
    strInsert = strInsert & servicio & ","
    strInsert = strInsert & "FR1700.FR17CANTIDAD,-1,0," 'Anestesia
    'strInsert = strInsert & "DECODE(FR7300.FR73INDFACTDOSIS,-1,FR1700.FR17CANTIDAD,CEIL(FR1700.FR17CANTIDAD)),-1,"
    strInsert = strInsert & txtHojaQuirofano.Text & ","
    strInsert = strInsert & txtProceso.Text & "," & txtAsistencia.Text
    strInsert = strInsert & " FROM FR1700 WHERE FR1700.PR62CODHOJAQUIR=" & txtHojaQuirofano.Text
    strInsert = strInsert & " AND FR1700.FR17CANTIDAD>0 "
    'strInsert = strInsert & " AND FR1700.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
  
    objApp.rdoConnect.Execute strInsert, 64
  

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    strupdate = "UPDATE PR6200 SET PR62CODESTADO_FAR=2 WHERE PR62CODHOJAQUIR=" & txtHojaQuirofano.Text
    objApp.rdoConnect.Execute strupdate, 64

    Call MsgBox("La Hoja de Quir�fano ha sido FACTURADA", vbInformation, "Aviso")
  
    cmdTerminar.Enabled = False
    cmdbuscarprod(0).Enabled = False
    cmdbuscarprod(1).Enabled = False
    cmdbuscarmaterial(0).Enabled = False
    cmdbuscarmaterial(1).Enabled = False
    cmdbuscarprotesis.Enabled = False
    cmdbuscargruprod(0).Enabled = False
    cmdbuscargruprod(1).Enabled = False
    cmdbuscarprot(0).Enabled = False
    cmdbuscarprot(1).Enabled = False
    cmdgruterapmedicamento(0).Enabled = False
    cmdgruterapmedicamento(1).Enabled = False
    cmdgruterapmaterial(0).Enabled = False
    cmdgruterapmaterial(1).Enabled = False
    cmdleerlector(0).Enabled = False
    cmdleerlector(1).Enabled = False
    grdDBGrid1(3).AllowDelete = False
    grdDBGrid1(4).AllowDelete = False
  
Me.Enabled = True
Screen.MousePointer = vbDefault

End Sub


Private Sub grdDBGrid1_BeforeDelete(Index As Integer, Cancel As Integer, DispPromptMsg As Integer)
Dim i As Integer
Dim mvarBkmrk As Variant
Dim strdeleteFR18 As String
Dim strdeleteFR17 As String

DispPromptMsg = 0
Select Case MsgBox("Desea borrar los Registros", vbYesNoCancel, "Aviso")
Case vbYes
  'Borrar
  If Index = 3 Then
    For i = 0 To grdDBGrid1(Index).SelBookmarks.Count
      mvarBkmrk = grdDBGrid1(Index).SelBookmarks(i)
      strdeleteFR18 = "DELETE FROM FR1800 "
      strdeleteFR18 = strdeleteFR18 & " WHERE PR62CODHOJAQUIR="
      strdeleteFR18 = strdeleteFR18 & grdDBGrid1(3).Columns("Hoja Quir�fano").CellValue(mvarBkmrk)
      strdeleteFR18 = strdeleteFR18 & " AND FR18NUMLINEA="
      strdeleteFR18 = strdeleteFR18 & grdDBGrid1(3).Columns("L�nea").CellValue(mvarBkmrk)
      objApp.rdoConnect.Execute strdeleteFR18, 64
    Next i
  Else
    For i = 0 To grdDBGrid1(Index).SelBookmarks.Count
      mvarBkmrk = grdDBGrid1(Index).SelBookmarks(i)
      strdeleteFR17 = "DELETE FROM FR1700 "
      strdeleteFR17 = strdeleteFR17 & " WHERE PR62CODHOJAQUIR="
      strdeleteFR17 = strdeleteFR17 & grdDBGrid1(4).Columns("Hoja Quir�fano").CellValue(mvarBkmrk)
      strdeleteFR17 = strdeleteFR17 & " AND FR17NUMLINEA="
      strdeleteFR17 = strdeleteFR17 & grdDBGrid1(4).Columns("L�nea").CellValue(mvarBkmrk)
      objApp.rdoConnect.Execute strdeleteFR17, 64
    Next i
  End If
Case vbNo
  Cancel = 1
Case vbCancel
  Cancel = 1
End Select

End Sub


Private Sub grdDBGrid1_KeyPress(Index As Integer, KeyAscii As Integer)

If grdDBGrid1(Index).Col = 9 Then
  Select Case KeyAscii
  Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8, Asc(",")
    If txtEstado.Text <> "" Then
      If CInt(txtEstado.Text) < 2 Then
        grdDBGrid1(Index).Columns("Estado").Value = "Modificado"
      Else
        KeyAscii = 0
      End If
    End If
  Case Else
    KeyAscii = 0
  End Select
End If


End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
 Dim i As Integer
    
    If Index = 3 Then
      If grdDBGrid1(3).Columns("C�d.Prod.").Value <> "" Then
        If grdDBGrid1(3).Columns(10).Value <> True Then
          For i = 0 To 11
            If i <> 9 Then
              grdDBGrid1(3).Columns(i).CellStyleSet "Medicamento"
            End If
          Next i
        Else
          For i = 0 To 11
            If i <> 9 Then
              grdDBGrid1(3).Columns(i).CellStyleSet "Material"
            End If
          Next i
        End If
      End If
    End If

    If Index = 4 Then
      If grdDBGrid1(4).Columns(5).Value <> "" Then
        If grdDBGrid1(4).Columns(10).Value <> True Then
          For i = 0 To 11
            If i <> 9 Then
              grdDBGrid1(4).Columns(i).CellStyleSet "Medicamento"
            End If
          Next i
        Else
          For i = 0 To 11
            If i <> 9 Then
              grdDBGrid1(4).Columns(i).CellStyleSet "Material"
            End If
          Next i
        End If
      End If
    End If

End Sub

Private Sub cmdinter_Click(Index As Integer)
Dim rsta As rdoResultset
Dim stra As String
Dim rstprin As rdoResultset
Dim strprin As String
Dim i As Integer
Dim strTerminada As String
Dim rstTerminada As rdoResultset

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

cmdinter(Index).Enabled = False
        
If Index = 0 Then
  gstrLista = ""
  'se cogen todos los principios activos de los productos que est�n en el grid y se
  'meten en una lista
  grdDBGrid1(3).MoveFirst
  For i = 0 To grdDBGrid1(3).Rows - 1
    stra = "SELECT count(*) FROM FR6400 WHERE FR73CODPRODUCTO=" & _
    grdDBGrid1(3).Columns("C�d.Prod.").Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value > 0 Then
      strprin = "SELECT FR68CODPRINCACTIV FROM FR6400 WHERE FR73CODPRODUCTO=" & _
                grdDBGrid1(3).Columns("C�d.Prod.").Value
      Set rstprin = objApp.rdoConnect.OpenResultset(strprin)
      If gstrLista = "" Then
        gstrLista = gstrLista & rstprin.rdoColumns(0).Value
      Else
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
      End If
      rstprin.MoveNext
      While Not rstprin.EOF
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
        rstprin.MoveNext
      Wend
      rstprin.Close
      Set rstprin = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    grdDBGrid1(3).MoveNext
  Next i
Else
  gstrLista = ""
  'se cogen todos los principios activos de los productos que est�n en el grid y se
  'meten en una lista
  grdDBGrid1(4).MoveFirst
  For i = 0 To grdDBGrid1(4).Rows - 1
    stra = "SELECT count(*) FROM FR6400 WHERE FR73CODPRODUCTO=" & _
    grdDBGrid1(4).Columns(5).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value > 0 Then
      strprin = "SELECT FR68CODPRINCACTIV FROM FR6400 WHERE FR73CODPRODUCTO=" & _
                grdDBGrid1(4).Columns(5).Value
      Set rstprin = objApp.rdoConnect.OpenResultset(strprin)
      If gstrLista = "" Then
        gstrLista = gstrLista & rstprin.rdoColumns(0).Value
      Else
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
      End If
      rstprin.MoveNext
      While Not rstprin.EOF
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
        rstprin.MoveNext
      Wend
      rstprin.Close
      Set rstprin = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    grdDBGrid1(4).MoveNext
  Next i
End If
  
  If gstrLista <> "" Then
    gstrLista = "(" & gstrLista & ")"
    Call objsecurity.LaunchProcess("FR0163")
  End If

cmdinter(Index).Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim j As Integer

  If gActuacionPedida = Empty And gstrLlamador = "" Then
    MsgBox "No ha seleccionado ninguna Actuaci�n", vbCritical, "Aviso"
    Unload Me
    Exit Sub
  End If

  Call Buscar_Hoja

  blnCancel = False
  
grdDBGrid1(3).Redraw = False
For j = 0 To 11
  Call grdDBGrid1(3).Columns.Add(j)
Next j
grdDBGrid1(3).Columns(0).Caption = "L�nea"
grdDBGrid1(3).Columns(0).Visible = False
grdDBGrid1(3).Columns(0).Locked = True
grdDBGrid1(3).Columns(1).Caption = "Hoja Quir�fano"
grdDBGrid1(3).Columns(1).Visible = False
grdDBGrid1(3).Columns(1).Locked = True
grdDBGrid1(3).Columns(2).Caption = "C�d.Prod."
grdDBGrid1(3).Columns(2).Visible = False
grdDBGrid1(3).Columns(2).Locked = True
grdDBGrid1(3).Columns(3).Caption = "C�d.Int"
grdDBGrid1(3).Columns(3).Visible = True
grdDBGrid1(3).Columns(3).Locked = True
grdDBGrid1(3).Columns(3).Width = 700
grdDBGrid1(3).Columns(4).Caption = "Referencia"
grdDBGrid1(3).Columns(4).Visible = True
grdDBGrid1(3).Columns(4).Locked = True
grdDBGrid1(3).Columns(4).Width = 1300
grdDBGrid1(3).Columns(5).Caption = "Descripci�n Producto"
grdDBGrid1(3).Columns(5).Visible = True
grdDBGrid1(3).Columns(5).Locked = True
grdDBGrid1(3).Columns(5).Width = 3500
grdDBGrid1(3).Columns(6).Caption = "F.F"
grdDBGrid1(3).Columns(6).Visible = True
grdDBGrid1(3).Columns(6).Locked = True
grdDBGrid1(3).Columns(6).Width = 450
grdDBGrid1(3).Columns(7).Caption = "Dosis"
grdDBGrid1(3).Columns(7).Visible = True
grdDBGrid1(3).Columns(7).Locked = True
grdDBGrid1(3).Columns(7).Width = 900
grdDBGrid1(3).Columns(8).Caption = "U.M"
grdDBGrid1(3).Columns(8).Visible = True
grdDBGrid1(3).Columns(8).Locked = True
grdDBGrid1(3).Columns(8).Width = 750
grdDBGrid1(3).Columns(9).Caption = "Cant.Consumida"
grdDBGrid1(3).Columns(9).Visible = True
grdDBGrid1(3).Columns(9).Locked = False
grdDBGrid1(3).Columns(9).BackColor = vbCyan
grdDBGrid1(3).Columns(9).Width = 1400
grdDBGrid1(3).Columns(10).Caption = "Medicamento/Material"
grdDBGrid1(3).Columns(10).Visible = False
grdDBGrid1(3).Columns(10).Locked = True
grdDBGrid1(3).Columns(11).Caption = "Estado"
grdDBGrid1(3).Columns(11).Visible = False
grdDBGrid1(3).Columns(11).Locked = True
grdDBGrid1(3).Redraw = True

grdDBGrid1(4).Redraw = False
For j = 0 To 11
  Call grdDBGrid1(4).Columns.Add(j)
Next j
grdDBGrid1(4).Columns(0).Caption = "L�nea"
grdDBGrid1(4).Columns(0).Visible = False
grdDBGrid1(4).Columns(0).Locked = True
grdDBGrid1(4).Columns(1).Caption = "Hoja Quir�fano"
grdDBGrid1(4).Columns(1).Visible = False
grdDBGrid1(4).Columns(1).Locked = True
grdDBGrid1(4).Columns(2).Caption = "C�d.Prod."
grdDBGrid1(4).Columns(2).Visible = False
grdDBGrid1(4).Columns(2).Locked = True
grdDBGrid1(4).Columns(3).Caption = "C�d.Int"
grdDBGrid1(4).Columns(3).Visible = True
grdDBGrid1(4).Columns(3).Locked = True
grdDBGrid1(4).Columns(3).Width = 700
grdDBGrid1(4).Columns(4).Caption = "Referencia"
grdDBGrid1(4).Columns(4).Visible = True
grdDBGrid1(4).Columns(4).Locked = True
grdDBGrid1(4).Columns(4).Width = 1300
grdDBGrid1(4).Columns(5).Caption = "Descripci�n Producto"
grdDBGrid1(4).Columns(5).Visible = True
grdDBGrid1(4).Columns(5).Locked = True
grdDBGrid1(4).Columns(5).Width = 3500
grdDBGrid1(4).Columns(6).Caption = "F.F"
grdDBGrid1(4).Columns(6).Visible = True
grdDBGrid1(4).Columns(6).Locked = True
grdDBGrid1(4).Columns(6).Width = 450
grdDBGrid1(4).Columns(7).Caption = "Dosis"
grdDBGrid1(4).Columns(7).Visible = True
grdDBGrid1(4).Columns(7).Locked = True
grdDBGrid1(4).Columns(7).Width = 900
grdDBGrid1(4).Columns(8).Caption = "U.M"
grdDBGrid1(4).Columns(8).Visible = True
grdDBGrid1(4).Columns(8).Locked = True
grdDBGrid1(4).Columns(8).Width = 750
grdDBGrid1(4).Columns(9).Caption = "Cant.Consumida"
grdDBGrid1(4).Columns(9).Visible = True
grdDBGrid1(4).Columns(9).Locked = False
grdDBGrid1(4).Columns(9).BackColor = vbCyan
grdDBGrid1(4).Columns(9).Width = 1400
grdDBGrid1(4).Columns(10).Caption = "Medicamento/Material"
grdDBGrid1(4).Columns(10).Visible = False
grdDBGrid1(4).Columns(10).Locked = True
grdDBGrid1(4).Columns(11).Caption = "Estado"
grdDBGrid1(4).Columns(11).Visible = False
grdDBGrid1(4).Columns(11).Locked = True
grdDBGrid1(4).Redraw = True

  
Call Refrescar_Grid_Quirofano
Call Refrescar_Grid_Anestesia

If txtEstado.Text <> "" Then
  If CInt(txtEstado.Text) > 1 Then
    cmdTerminar.Enabled = False
    cmdbuscarprod(0).Enabled = False
    cmdbuscarprod(1).Enabled = False
    cmdbuscarmaterial(0).Enabled = False
    cmdbuscarmaterial(1).Enabled = False
    cmdbuscarprotesis.Enabled = False
    cmdbuscargruprod(0).Enabled = False
    cmdbuscargruprod(1).Enabled = False
    cmdbuscarprot(0).Enabled = False
    cmdbuscarprot(1).Enabled = False
    cmdgruterapmedicamento(0).Enabled = False
    cmdgruterapmedicamento(1).Enabled = False
    cmdgruterapmaterial(0).Enabled = False
    cmdgruterapmaterial(1).Enabled = False
    cmdleerlector(0).Enabled = False
    cmdleerlector(1).Enabled = False
    grdDBGrid1(3).AllowDelete = False
    grdDBGrid1(4).AllowDelete = False
  End If
End If

End Sub


Private Sub cmdbuscarprod_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

cmdbuscarprod(Index).Enabled = False

If Index = 0 Then
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrMedicamentoMaterial = "Medicamento"
    Call objsecurity.LaunchProcess("FR0166")
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Quirofano
      grdDBGrid1(3).Redraw = False
      grdDBGrid1(3).MoveLast
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrMedicamentoMaterial = "Medicamento"
    Call objsecurity.LaunchProcess("FR0166")
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Anestesia
      grdDBGrid1(4).Redraw = False
      grdDBGrid1(4).MoveLast
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdbuscarprod(Index).Enabled = True

End Sub

Private Sub cmdbuscarmaterial_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If


cmdbuscarmaterial(Index).Enabled = False
If Index = 0 Then
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrMedicamentoMaterial = "Material"
    Call objsecurity.LaunchProcess("FR0166")
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El material sanitario: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Quirofano
      grdDBGrid1(3).Redraw = False
      grdDBGrid1(3).MoveLast
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrMedicamentoMaterial = "Material"
    Call objsecurity.LaunchProcess("FR0166")
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El material sanitario: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Anestesia
      grdDBGrid1(4).Redraw = False
      grdDBGrid1(4).MoveLast
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdbuscarmaterial(Index).Enabled = True
End Sub

Private Sub cmdbuscargruprod_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

cmdbuscargruprod(Index).Enabled = False

If Index = 0 Then
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    Call objsecurity.LaunchProcess("FR0168")
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                             Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Quirofano
      grdDBGrid1(3).Redraw = False
      grdDBGrid1(3).MoveLast
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    Call objsecurity.LaunchProcess("FR0168")
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                             Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Anestesia
      grdDBGrid1(4).Redraw = False
      grdDBGrid1(4).MoveLast
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdbuscargruprod(Index).Enabled = True

End Sub

Private Sub cmdbuscarprot_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

cmdbuscarprot(Index).Enabled = False

If Index = 0 Then
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrLlamador = "HojaQuirofano"
    Call objsecurity.LaunchProcess("FR0174")
    gstrLlamador = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & gintprodbuscado(v, 3)
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Quirofano
      grdDBGrid1(3).Redraw = False
      grdDBGrid1(3).MoveLast
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrLlamador = "HojaAnestesia"
    Call objsecurity.LaunchProcess("FR0174")
    gstrLlamador = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & gintprodbuscado(v, 3)
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Anestesia
      grdDBGrid1(4).Redraw = False
      grdDBGrid1(4).MoveLast
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdbuscarprot(Index).Enabled = True
End Sub

Private Sub cmdgruterapmedicamento_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

cmdgruterapmedicamento(Index).Enabled = False

If Index = 0 Then
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrLlamador = "Medicamentos"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Quirofano
      grdDBGrid1(3).Redraw = False
      grdDBGrid1(3).MoveLast
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrLlamador = "Medicamentos"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Anestesia
      grdDBGrid1(4).Redraw = False
      grdDBGrid1(4).MoveLast
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdgruterapmedicamento(Index).Enabled = True
End Sub

Private Sub cmdgruterapmaterial_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

  If grdDBGrid1(3).Rows > 0 Then
    Call Guardar_Quirofano
  End If
  If grdDBGrid1(4).Rows > 0 Then
    Call Guardar_Anestesia
  End If
  If blnCancel = True Then
    Exit Sub
  End If

cmdgruterapmaterial(Index).Enabled = False

If Index = 0 Then
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrLlamador = "Material Sanitario"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Quirofano
      grdDBGrid1(3).Redraw = False
      grdDBGrid1(3).MoveLast
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtHojaQuirofano.Text <> "" Then
    noinsertar = True
    gstrLlamador = "Material Sanitario"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE PR62CODHOJAQUIR=" & _
                    txtHojaQuirofano.Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "PR62CODHOJAQUIR,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtHojaQuirofano.Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call Refrescar_Grid_Anestesia
      grdDBGrid1(4).Redraw = False
      grdDBGrid1(4).MoveLast
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdgruterapmaterial(Index).Enabled = True
End Sub



Private Sub Refrescar_Grid_Quirofano()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String

Me.Enabled = False
Screen.MousePointer = vbHourglass

grdDBGrid1(3).Redraw = False
grdDBGrid1(3).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & " FR1800.FR18NUMLINEA"
strSelect = strSelect & ",FR1800.PR62CODHOJAQUIR"
strSelect = strSelect & ",FR1800.FR73CODPRODUCTO"
strSelect = strSelect & ",FR7300.FR73CODINTFAR"
strSelect = strSelect & ",FR7300.FR73REFERENCIA"
strSelect = strSelect & ",FR7300.FR73DESPRODUCTO"
strSelect = strSelect & ",FR7300.FRH7CODFORMFAR"
strSelect = strSelect & ",FR7300.FR73DOSIS"
strSelect = strSelect & ",FR7300.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR1800.FR18CANTCONSUMIDA"
strSelect = strSelect & ",FR7300.FR73INDPRODSAN"

strSelect = strSelect & " FROM FR1800,FR7300"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR1800.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
strSelect = strSelect & " AND FR1800.PR62CODHOJAQUIR=" & txtHojaQuirofano.Text
strSelect = strSelect & " ORDER BY FR18NUMLINEA"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
  strlinea = rsta("FR18NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("PR62CODHOJAQUIR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODINTFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73REFERENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH7CODFORMFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DOSIS").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR18CANTCONSUMIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73INDPRODSAN").Value & Chr(vbKeyTab)
  strlinea = strlinea & ""
  grdDBGrid1(3).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
grdDBGrid1(3).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub



Private Sub Guardar_Quirofano()
Dim i As Integer
Dim strUpdFR18 As String
  
Me.Enabled = False
Screen.MousePointer = vbHourglass
  
  blnCancel = False
  grdDBGrid1(3).Redraw = False
  grdDBGrid1(3).MoveFirst
  For i = 0 To grdDBGrid1(3).Rows
    If grdDBGrid1(3).Columns("Cant.Consumida").Value = "" Then
      MsgBox "El campo Cantidad es Obligatorio", vbInformation, "Aviso"
      blnCancel = True
      tabGeneral1(1).Tab = 0
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf Not IsNumeric(grdDBGrid1(3).Columns("Cant.Consumida").Value) Then
      MsgBox "El campo Cantidad es Incorrecto", vbInformation, "Aviso"
      blnCancel = True
      tabGeneral1(1).Tab = 0
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf grdDBGrid1(3).Columns("Cant.Consumida").Value > 999.99 Or grdDBGrid1(3).Columns("Cant.Consumida").Value < -999.99 Then
      MsgBox "El campo Cantidad es Incorrecto", vbInformation, "Aviso"
      blnCancel = True
      tabGeneral1(1).Tab = 0
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf grdDBGrid1(3).Columns("Cant.Consumida").Value = 0 Then
      MsgBox "El campo Cantidad no puede ser 0", vbInformation, "Aviso"
      blnCancel = True
      tabGeneral1(1).Tab = 0
      grdDBGrid1(3).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    grdDBGrid1(3).MoveNext
  Next i
  grdDBGrid1(3).Redraw = True
  
  grdDBGrid1(3).Redraw = False
  grdDBGrid1(3).MoveFirst
  For i = 0 To grdDBGrid1(3).Rows - 1
    If grdDBGrid1(3).Columns("Estado").Value <> "" Then
        strUpdFR18 = "UPDATE FR1800 SET FR18CANTCONSUMIDA="
        strUpdFR18 = strUpdFR18 & objGen.ReplaceStr(grdDBGrid1(3).Columns("Cant.Consumida").Value, ",", ".", 1)
        strUpdFR18 = strUpdFR18 & " WHERE PR62CODHOJAQUIR="
        strUpdFR18 = strUpdFR18 & objGen.ReplaceStr(grdDBGrid1(3).Columns("Hoja Quir�fano").Value, ",", ".", 1)
        strUpdFR18 = strUpdFR18 & " AND FR18NUMLINEA="
        strUpdFR18 = strUpdFR18 & objGen.ReplaceStr(grdDBGrid1(3).Columns("L�nea").Value, ",", ".", 1)
        objApp.rdoConnect.Execute strUpdFR18, 64
    End If
    grdDBGrid1(3).MoveNext
  Next i
  grdDBGrid1(3).MoveFirst
  grdDBGrid1(3).Redraw = True


Call Refrescar_Grid_Quirofano
Me.Enabled = True
Screen.MousePointer = vbDefault

End Sub

Private Sub Refrescar_Grid_Anestesia()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String

Me.Enabled = False
Screen.MousePointer = vbHourglass

grdDBGrid1(4).Redraw = False
grdDBGrid1(4).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & " FR1700.FR17NUMLINEA"
strSelect = strSelect & ",FR1700.PR62CODHOJAQUIR"
strSelect = strSelect & ",FR1700.FR73CODPRODUCTO"
strSelect = strSelect & ",FR7300.FR73CODINTFAR"
strSelect = strSelect & ",FR7300.FR73REFERENCIA"
strSelect = strSelect & ",FR7300.FR73DESPRODUCTO"
strSelect = strSelect & ",FR7300.FRH7CODFORMFAR"
strSelect = strSelect & ",FR7300.FR73DOSIS"
strSelect = strSelect & ",FR7300.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR1700.FR17CANTIDAD"
strSelect = strSelect & ",FR7300.FR73INDPRODSAN"

strSelect = strSelect & " FROM FR1700,FR7300"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR1700.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
strSelect = strSelect & " AND FR1700.PR62CODHOJAQUIR=" & txtHojaQuirofano.Text
strSelect = strSelect & " ORDER BY FR17NUMLINEA"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
  strlinea = rsta("FR17NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("PR62CODHOJAQUIR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODINTFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73REFERENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH7CODFORMFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DOSIS").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR17CANTIDAD").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73INDPRODSAN").Value & Chr(vbKeyTab)
  strlinea = strlinea & ""
  grdDBGrid1(4).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
grdDBGrid1(4).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Guardar_Anestesia()
Dim i As Integer
Dim strUpdFR17 As String
  
Me.Enabled = False
Screen.MousePointer = vbHourglass
  
  blnCancel = False
  grdDBGrid1(4).Redraw = False
  grdDBGrid1(4).MoveFirst
  For i = 0 To grdDBGrid1(4).Rows
    If grdDBGrid1(4).Columns("Cant.Consumida").Value = "" Then
      MsgBox "El campo Cantidad es Obligatorio", vbInformation, "Aviso"
      blnCancel = True
      tabGeneral1(1).Tab = 1
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf Not IsNumeric(grdDBGrid1(4).Columns("Cant.Consumida").Value) Then
      MsgBox "El campo Cantidad es Incorrecto", vbInformation, "Aviso"
      blnCancel = True
      tabGeneral1(1).Tab = 1
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf grdDBGrid1(4).Columns("Cant.Consumida").Value > 999.99 Or grdDBGrid1(4).Columns("Cant.Consumida").Value < -999.99 Then
      MsgBox "El campo Cantidad es Incorrecto", vbInformation, "Aviso"
      blnCancel = True
      tabGeneral1(1).Tab = 1
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    ElseIf grdDBGrid1(4).Columns("Cant.Consumida").Value = 0 Then
      MsgBox "El campo Cantidad no puede ser 0", vbInformation, "Aviso"
      blnCancel = True
      tabGeneral1(1).Tab = 1
      grdDBGrid1(4).Redraw = True
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    grdDBGrid1(4).MoveNext
  Next i
  grdDBGrid1(4).Redraw = True
  
  grdDBGrid1(4).Redraw = False
  grdDBGrid1(4).MoveFirst
  For i = 0 To grdDBGrid1(4).Rows - 1
    If grdDBGrid1(4).Columns("Estado").Value <> "" Then
        strUpdFR17 = "UPDATE FR1700 SET FR17CANTIDAD="
        strUpdFR17 = strUpdFR17 & objGen.ReplaceStr(grdDBGrid1(4).Columns("Cant.Consumida").Value, ",", ".", 1)
        strUpdFR17 = strUpdFR17 & " WHERE PR62CODHOJAQUIR="
        strUpdFR17 = strUpdFR17 & objGen.ReplaceStr(grdDBGrid1(4).Columns("Hoja Quir�fano").Value, ",", ".", 1)
        strUpdFR17 = strUpdFR17 & " AND FR17NUMLINEA="
        strUpdFR17 = strUpdFR17 & objGen.ReplaceStr(grdDBGrid1(4).Columns("L�nea").Value, ",", ".", 1)
        objApp.rdoConnect.Execute strUpdFR17, 64
    End If
    grdDBGrid1(4).MoveNext
  Next i
  grdDBGrid1(4).MoveFirst
  grdDBGrid1(4).Redraw = True


Call Refrescar_Grid_Anestesia
Me.Enabled = True
Screen.MousePointer = vbDefault

End Sub

Private Sub Buscar_Hoja()
Dim sqla As String
Dim rsta As rdoResultset
Dim stra As String

If gstrLlamador = "frmFirmarHojQuirof" Then
  
  txtHojaQuirofano.Text = gintcodhojaquiro
  
  sqla = "SELECT CI2200.CI22NOMBRE ||' ' || CI2200.CI22PRIAPEL || ' ' || CI2200.CI22SEGAPEL PACIENTE,CI2200.CI22NUMHISTORIA,CI2200.CI21CODPERSONA,PR6200.PR62CODESTADO_FAR,PR0400.AD07CODPROCESO,PR0400.AD01CODASISTENCI FROM PR0400,CI2200,PR6200 "
  sqla = sqla & " WHERE PR0400.CI21CODPERSONA=CI2200.CI21CODPERSONA "
  sqla = sqla & " AND PR0400.PR62CODHOJAQUIR=PR6200.PR62CODHOJAQUIR "
  sqla = sqla & " AND PR0400.PR62CODHOJAQUIR=" & txtHojaQuirofano.Text
  Set rsta = objApp.rdoConnect.OpenResultset(sqla)
  If Not rsta.EOF Then
    If Not IsNull(rsta("PACIENTE").Value) Then
      txtPaciente.Text = rsta("PACIENTE").Value
    Else
      txtPaciente.Text = ""
    End If
    If Not IsNull(rsta("CI22NUMHISTORIA").Value) Then
      txtHistoria.Text = rsta("CI22NUMHISTORIA").Value
    Else
      txtHistoria.Text = ""
    End If
    If Not IsNull(rsta("CI21CODPERSONA").Value) Then
      txtCodPaciente.Text = rsta("CI21CODPERSONA").Value
    Else
      txtCodPaciente.Text = ""
    End If
    If Not IsNull(rsta("PR62CODESTADO_FAR").Value) Then
      txtEstado.Text = rsta("PR62CODESTADO_FAR").Value
    Else
      txtEstado.Text = ""
    End If
    If Not IsNull(rsta("AD07CODPROCESO").Value) Then
      txtProceso.Text = rsta("AD07CODPROCESO").Value
    Else
      txtProceso.Text = ""
    End If
    If Not IsNull(rsta("AD01CODASISTENCI").Value) Then
      txtAsistencia.Text = rsta("AD01CODASISTENCI").Value
    Else
      txtAsistencia.Text = ""
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  Exit Sub
End If

If gActuacionPedida <> Empty Then
  
  sqla = "SELECT CI2200.CI22NOMBRE ||' ' || CI2200.CI22PRIAPEL || ' ' || CI2200.CI22SEGAPEL PACIENTE,CI2200.CI22NUMHISTORIA,CI2200.CI21CODPERSONA,PR6200.PR62CODESTADO_FAR,PR0400.AD07CODPROCESO,PR0400.AD01CODASISTENCI,PR0400.PR62CODHOJAQUIR FROM PR0400,CI2200,PR6200 "
  sqla = sqla & " WHERE PR0400.CI21CODPERSONA=CI2200.CI21CODPERSONA "
  sqla = sqla & " AND PR0400.PR62CODHOJAQUIR=PR6200.PR62CODHOJAQUIR "
  sqla = sqla & " AND PR04NUMACTPLAN=" & gActuacionPedida
  Set rsta = objApp.rdoConnect.OpenResultset(sqla)
  If Not rsta.EOF Then
    If Not IsNull(rsta("PACIENTE").Value) Then
      txtPaciente.Text = rsta("PACIENTE").Value
    Else
      txtPaciente.Text = ""
    End If
    If Not IsNull(rsta("CI22NUMHISTORIA").Value) Then
      txtHistoria.Text = rsta("CI22NUMHISTORIA").Value
    Else
      txtHistoria.Text = ""
    End If
    If Not IsNull(rsta("CI21CODPERSONA").Value) Then
      txtCodPaciente.Text = rsta("CI21CODPERSONA").Value
    Else
      txtCodPaciente.Text = ""
    End If
    If Not IsNull(rsta("PR62CODESTADO_FAR").Value) Then
      txtEstado.Text = rsta("PR62CODESTADO_FAR").Value
    Else
      txtEstado.Text = ""
    End If
    If Not IsNull(rsta("AD07CODPROCESO").Value) Then
      txtProceso.Text = rsta("AD07CODPROCESO").Value
    Else
      txtProceso.Text = ""
    End If
    If Not IsNull(rsta("AD01CODASISTENCI").Value) Then
      txtAsistencia.Text = rsta("AD01CODASISTENCI").Value
    Else
      txtAsistencia.Text = ""
    End If
    If Not IsNull(rsta("PR62CODHOJAQUIR").Value) Then
      txtHojaQuirofano.Text = rsta("PR62CODHOJAQUIR").Value
    Else
      txtHojaQuirofano.Text = ""
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  Exit Sub
End If

End Sub

