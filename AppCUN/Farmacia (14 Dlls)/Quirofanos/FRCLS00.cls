VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

Const PRWinBuscarGruProdQUI           As String = "FR0168"
Const PRWinBuscarProdQUI              As String = "FR0166"
Const PRWinVerIntMedicaQUI            As String = "FR0163"
Const PRWinRedHojQuirof               As String = "FR0126"
Const PRWinFirmarHojQuirof            As String = "FR0128"
Const PRWinRedHojAnest                As String = "FR0127"
Const PRWinFirmarHojAnest             As String = "FR0129"
Const PRWinConfNecQuirof              As String = "FR0147"
Const PRWinDefProtQuirofano           As String = "FR0154"
Const PRWinVerPerfilFTPQuirofano      As String = "FR0170"
Const PRWinBusGrpTera                 As String = "FR0173"
Const PRWinBusProtocolos              As String = "FR0174"
Const PRWinVerHojasInt                As String = "FR0149"
Const PRWinRedHojQuirFar              As String = "FR0226"

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
  
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case PRWinBuscarGruProdQUI
      Load frmBusGrpProdQUI
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpProdQUI.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusGrpProdQUI
      Set frmBusGrpProdQUI = Nothing
      
    
    Case PRWinBuscarProdQUI
      Load frmBusProductosQUI
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProductosQUI.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusProductosQUI
      Set frmBusProductosQUI = Nothing
    
    
    Case PRWinBusProtocolos
      Load frmBusProtocolos
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProtocolos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusProtocolos
      Set frmBusProtocolos = Nothing
      
    Case PRWinVerIntMedicaQUI
      Load frmVerIntMedicaQUI
      'Call objsecurity.AddHelpContext(528)
      Call frmVerIntMedicaQUI.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerIntMedicaQUI
      Set frmVerIntMedicaQUI = Nothing
      
    Case PRWinRedHojQuirof
      gActuacionPedida = vntData(1)
      'gstrLlamador = vntData(1)
      'gintcodhojaquiro = vntData(2)
    
      Load frmRedHojQuirof
      'Call objsecurity.AddHelpContext(528)
      Call frmRedHojQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRedHojQuirof
      Set frmRedHojQuirof = Nothing
    
      'gstrLlamador = ""
      'gintcodhojaquiro = 0
    
    Case PRWinRedHojQuirFar
      gActuacionPedida = vntData(1)
      'gstrLlamador = vntData(1)
      'gintcodhojaquiro = vntData(2)
    
      Load frmRedHojQuirFar
      'Call objsecurity.AddHelpContext(528)
      Call frmRedHojQuirFar.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRedHojQuirFar
      Set frmRedHojQuirFar = Nothing
      
    Case PRWinFirmarHojQuirof
      Load frmFirmarHojQuirof
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarHojQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmFirmarHojQuirof
      Set frmFirmarHojQuirof = Nothing
    
     Case PRWinRedHojAnest
    
      gstrLlamador = vntData(1)
      gintcodhojaanest = vntData(2)
    
      Load frmRedHojAnest
      'Call objsecurity.AddHelpContext(528)
      Call frmRedHojAnest.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRedHojAnest
      Set frmRedHojAnest = Nothing
    
      gstrLlamador = ""
      gintcodhojaquiro = 0
      
    Case PRWinFirmarHojAnest
      Load frmFirmarHojAnest
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarHojAnest.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmFirmarHojAnest
      Set frmFirmarHojAnest = Nothing
     
    Case PRWinConfNecQuirof
      Load frmConfNecQuirof
      'Call objsecurity.AddHelpContext(528)
      Call frmConfNecQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmConfNecQuirof
      Set frmConfNecQuirof = Nothing
      
    Case PRWinDefProtQuirofano
      Load frmDefProtQuirofano
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProtQuirofano.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDefProtQuirofano
      Set frmDefProtQuirofano = Nothing
      
    Case PRWinVerPerfilFTPQuirofano
      Load frmVerPerfilFTPQuirofano
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPerfilFTPQuirofano.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmVerPerfilFTPQuirofano
      Set frmVerPerfilFTPQuirofano = Nothing
      
    Case PRWinBusGrpTera
      Load frmBusGrpTera
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpTera.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusGrpTera
      Set frmBusGrpTera = Nothing
    
    Case PRWinVerHojasInt
      Load frmVerHojasInt
      'Call objsecurity.AddHelpContext(528)
      Call frmVerHojasInt.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmVerHojasInt
      Set frmVerHojasInt = Nothing
  
  
  End Select
  Call ERR.Clear

End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 20, 1 To 4) As Variant
  
  aProcess(1, 1) = PRWinBuscarGruProdQUI
  aProcess(1, 2) = "Buscar Grupo de Productos"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinBuscarProdQUI
  aProcess(2, 2) = "Buscar Productos"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinVerIntMedicaQUI
  aProcess(3, 2) = "Ver Interacciones"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinRedHojQuirof
  aProcess(4, 2) = "Redactar Hoja de Quir�fano"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinFirmarHojQuirof
  aProcess(5, 2) = "Firmar Hoja de Quir�fano"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinRedHojAnest
  aProcess(7, 2) = "Redactar Hoja de Anestesia"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = PRWinFirmarHojAnest
  aProcess(8, 2) = "Firmar Hoja de Anestesia"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinConfNecQuirof
  aProcess(9, 2) = "Confeccionar Necesidades Quir�fano"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinDefProtQuirofano
  aProcess(10, 2) = "Protocolos del Quir�fano"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = PRWinVerPerfilFTPQuirofano
  aProcess(11, 2) = "Perfil FTP Quir�fano"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = PRWinBusGrpTera
  aProcess(12, 2) = "Buscar Grupo Terapeutico"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWinBusProtocolos
  aProcess(13, 2) = "Buscar Protocolos de Quir�fano"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow

  aProcess(14, 1) = PRWinVerHojasInt
  aProcess(14, 2) = "Ver Hojas de Intervenci�n"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow

  aProcess(15, 1) = PRWinRedHojQuirFar
  aProcess(15, 2) = "Rellenar Hoja de Quir�fano Farmacia"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow

End Sub
