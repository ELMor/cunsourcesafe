VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmRedHojAnest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Redactar Hoja de Anestesia"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   32
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabGeneral1 
      Height          =   7335
      Index           =   1
      Left            =   120
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   600
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   12938
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos Generales"
      TabPicture(0)   =   "FR0127.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Material y Medicaci�n"
      TabPicture(1)   =   "FR0127.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdgruterapmaterial"
      Tab(1).Control(1)=   "cmdbuscarprot"
      Tab(1).Control(2)=   "cmdbuscarmaterial"
      Tab(1).Control(3)=   "cmdgruterapmedicamento"
      Tab(1).Control(4)=   "cmdbuscargruprod"
      Tab(1).Control(5)=   "cmdbuscarprod"
      Tab(1).Control(6)=   "cmdinter"
      Tab(1).Control(7)=   "fraFrame1(4)"
      Tab(1).ControlCount=   8
      Begin VB.CommandButton cmdgruterapmaterial 
         Caption         =   "Grupos Terap�uticos         Productos"
         Height          =   615
         Left            =   -65040
         TabIndex        =   29
         Top             =   4560
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprot 
         Caption         =   "Protocolos"
         Height          =   375
         Left            =   -65040
         TabIndex        =   27
         Top             =   3360
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarmaterial 
         Caption         =   "Productos"
         Height          =   375
         Left            =   -65040
         TabIndex        =   25
         Top             =   2400
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmedicamento 
         Caption         =   "Grupos Terap�uticos      Medicamentos"
         Height          =   615
         Left            =   -65040
         TabIndex        =   28
         Top             =   3840
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Grupo Productos"
         Height          =   375
         Left            =   -65040
         TabIndex        =   26
         Top             =   2880
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprod 
         Caption         =   "Medicamentos"
         Height          =   375
         Left            =   -65040
         TabIndex        =   24
         Top             =   1920
         Width           =   1600
      End
      Begin VB.CommandButton cmdinter 
         Caption         =   "Interacciones"
         Height          =   375
         Left            =   -65040
         TabIndex        =   30
         Top             =   5400
         Width           =   1600
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6480
         Index           =   4
         Left            =   -74880
         TabIndex        =   54
         Top             =   600
         Width           =   9735
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5955
            Index           =   4
            Left            =   120
            TabIndex        =   23
            Top             =   360
            Width           =   9465
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   2
            stylesets(0).Name=   "Material"
            stylesets(0).BackColor=   12615935
            stylesets(0).Picture=   "FR0127.frx":0038
            stylesets(1).Name=   "Medicamento"
            stylesets(1).BackColor=   16777215
            stylesets(1).Picture=   "FR0127.frx":0054
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   16695
            _ExtentY        =   10504
            _StockProps     =   79
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6975
         Index           =   0
         Left            =   120
         TabIndex        =   35
         Top             =   360
         Width           =   11340
         Begin TabDlg.SSTab tabTab1 
            Height          =   6375
            Index           =   0
            Left            =   120
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   360
            Width           =   11055
            _ExtentX        =   19500
            _ExtentY        =   11245
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0127.frx":0070
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(29)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(23)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(22)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(28)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(15)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(16)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "dtcDateCombo1(0)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "tab1"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "cmdFirmar"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(5)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(4)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(3)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(2)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(0)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(1)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).ControlCount=   16
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0127.frx":008C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR43HORAINTERVEN"
               Height          =   330
               Index           =   1
               Left            =   2640
               TabIndex        =   2
               Tag             =   "Hora Intervenci�n"
               Top             =   1080
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR43CODHOJAANEST"
               Height          =   330
               Index           =   0
               Left            =   360
               TabIndex        =   0
               Tag             =   "C�d. Hoja Anestesia"
               Top             =   360
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR43DURACCIRUGIA"
               Height          =   330
               Index           =   2
               Left            =   360
               TabIndex        =   3
               Tag             =   "Duraci�n Cirug�a"
               Top             =   1800
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR43DESINTERVENCION"
               Height          =   810
               Index           =   3
               Left            =   360
               MultiLine       =   -1  'True
               ScrollBars      =   1  'Horizontal
               TabIndex        =   4
               Tag             =   "Descripci�n Intervenci�n"
               Top             =   2520
               Width           =   4815
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   4
               Left            =   5880
               TabIndex        =   5
               Tag             =   "Firma Digital"
               Top             =   1680
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   7080
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Persona"
               Top             =   1680
               Width           =   2925
            End
            Begin VB.CommandButton cmdFirmar 
               Caption         =   "Firmar"
               Height          =   330
               Left            =   6600
               TabIndex        =   7
               Top             =   2160
               Width           =   1155
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5865
               Index           =   0
               Left            =   -74760
               TabIndex        =   31
               TabStop         =   0   'False
               Top             =   240
               Width           =   10215
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18018
               _ExtentY        =   10345
               _StockProps     =   79
            End
            Begin TabDlg.SSTab tab1 
               Height          =   2655
               Left            =   360
               TabIndex        =   37
               TabStop         =   0   'False
               Top             =   3480
               Width           =   9855
               _ExtentX        =   17383
               _ExtentY        =   4683
               _Version        =   327681
               Style           =   1
               Tab             =   1
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Paciente"
               TabPicture(0)   =   "FR0127.frx":00A8
               Tab(0).ControlEnabled=   0   'False
               Tab(0).Control(0)=   "txtText1(7)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "txtText1(8)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "txtText1(6)"
               Tab(0).Control(3)=   "txtText1(9)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "txtText1(10)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "cmdHistClin"
               Tab(0).Control(6)=   "cmdPerfFTP"
               Tab(0).Control(7)=   "lblLabel1(11)"
               Tab(0).Control(8)=   "lblLabel1(4)"
               Tab(0).Control(9)=   "lblLabel1(7)"
               Tab(0).Control(10)=   "lblLabel1(8)"
               Tab(0).Control(11)=   "lblLabel1(9)"
               Tab(0).ControlCount=   12
               TabCaption(1)   =   "Equipo Anestesia"
               TabPicture(1)   =   "FR0127.frx":00C4
               Tab(1).ControlEnabled=   -1  'True
               Tab(1).Control(0)=   "lblLabel1(3)"
               Tab(1).Control(0).Enabled=   0   'False
               Tab(1).Control(1)=   "lblLabel1(0)"
               Tab(1).Control(1).Enabled=   0   'False
               Tab(1).Control(2)=   "lblLabel1(20)"
               Tab(1).Control(2).Enabled=   0   'False
               Tab(1).Control(3)=   "txtText1(16)"
               Tab(1).Control(3).Enabled=   0   'False
               Tab(1).Control(4)=   "txtText1(15)"
               Tab(1).Control(4).Enabled=   0   'False
               Tab(1).Control(5)=   "txtText1(14)"
               Tab(1).Control(5).Enabled=   0   'False
               Tab(1).Control(6)=   "txtText1(13)"
               Tab(1).Control(6).Enabled=   0   'False
               Tab(1).Control(7)=   "txtText1(12)"
               Tab(1).Control(7).Enabled=   0   'False
               Tab(1).Control(8)=   "txtText1(11)"
               Tab(1).Control(8).Enabled=   0   'False
               Tab(1).ControlCount=   9
               TabCaption(2)   =   "Servicio"
               TabPicture(2)   =   "FR0127.frx":00E0
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "lblLabel1(1)"
               Tab(2).Control(1)=   "txtText1(17)"
               Tab(2).Control(2)=   "txtText1(18)"
               Tab(2).Control(2).Enabled=   0   'False
               Tab(2).ControlCount=   3
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ANE"
                  Height          =   330
                  Index           =   11
                  Left            =   360
                  TabIndex        =   15
                  Tag             =   "C�digo Anestesista"
                  Top             =   720
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   12
                  Left            =   1560
                  TabIndex        =   16
                  TabStop         =   0   'False
                  Tag             =   "Apellido Anestesista"
                  Top             =   720
                  Width           =   4485
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   7
                  Left            =   -72600
                  TabIndex        =   9
                  TabStop         =   0   'False
                  Tag             =   "C�d. Historia Paciente"
                  Top             =   720
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   8
                  Left            =   -74640
                  TabIndex        =   10
                  TabStop         =   0   'False
                  Tag             =   "Nombre Paciente"
                  Top             =   1440
                  Width           =   2565
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "CI21CODPERSONA"
                  Height          =   330
                  Index           =   6
                  Left            =   -74640
                  TabIndex        =   8
                  Tag             =   "C�digo Paciente"
                  Top             =   720
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   9
                  Left            =   -72000
                  TabIndex        =   11
                  TabStop         =   0   'False
                  Tag             =   "Apellido 1� Paciente"
                  Top             =   1440
                  Width           =   2925
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   10
                  Left            =   -69000
                  TabIndex        =   12
                  TabStop         =   0   'False
                  Tag             =   "Apellido 2� Paciente"
                  Top             =   1440
                  Width           =   2925
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ENF"
                  Height          =   330
                  Index           =   13
                  Left            =   360
                  TabIndex        =   17
                  Tag             =   "C�digo Enfermera"
                  Top             =   1440
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   14
                  Left            =   1560
                  TabIndex        =   18
                  TabStop         =   0   'False
                  Tag             =   "Apellido Enfermera"
                  Top             =   1440
                  Width           =   4485
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_CIR"
                  Height          =   330
                  Index           =   15
                  Left            =   360
                  TabIndex        =   19
                  Tag             =   "C�digo Cirujano"
                  Top             =   2160
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   16
                  Left            =   1560
                  TabIndex        =   20
                  TabStop         =   0   'False
                  Tag             =   "Apellido Cirujano"
                  Top             =   2160
                  Width           =   4485
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   18
                  Left            =   -73200
                  TabIndex        =   22
                  TabStop         =   0   'False
                  Tag             =   "Desc.Servicio"
                  Top             =   1020
                  Width           =   5445
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "AD02CODDPTO"
                  Height          =   330
                  Index           =   17
                  Left            =   -74520
                  TabIndex        =   21
                  Tag             =   "C�d.Servicio"
                  Top             =   1020
                  Width           =   1035
               End
               Begin VB.CommandButton cmdHistClin 
                  Caption         =   "Historia Cl�nica"
                  Height          =   330
                  Left            =   -71760
                  TabIndex        =   13
                  Top             =   2100
                  Width           =   1400
               End
               Begin VB.CommandButton cmdPerfFTP 
                  Caption         =   "Perfil FTP"
                  Height          =   330
                  Left            =   -69960
                  TabIndex        =   14
                  Top             =   2100
                  Width           =   1400
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Anestesista"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   20
                  Left            =   360
                  TabIndex        =   46
                  Top             =   480
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Historia "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   -72600
                  TabIndex        =   45
                  Top             =   480
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Persona"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   -74640
                  TabIndex        =   44
                  Top             =   480
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   -74640
                  TabIndex        =   43
                  Top             =   1200
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 1�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   -72000
                  TabIndex        =   42
                  Top             =   1200
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 2�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   -69000
                  TabIndex        =   41
                  Top             =   1200
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Enfermera"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   360
                  TabIndex        =   40
                  Top             =   1200
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cirujano"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   360
                  TabIndex        =   39
                  Top             =   1920
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Servicio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   -74520
                  TabIndex        =   38
                  Top             =   780
                  Width           =   2055
               End
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR43FECINTERVEN"
               Height          =   330
               Index           =   0
               Left            =   360
               TabIndex        =   1
               Tag             =   "Fecha Intervenci�n"
               Top             =   1080
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   2640
               TabIndex        =   53
               Top             =   840
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   360
               TabIndex        =   52
               Top             =   840
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hoja Anestesia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   360
               TabIndex        =   51
               Top             =   120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   360
               TabIndex        =   50
               Top             =   2280
               Width           =   2145
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Duraci�n Cirug�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   360
               TabIndex        =   49
               Top             =   1560
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Firma Digital"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   5880
               TabIndex        =   48
               Top             =   1440
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Horas"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   1560
               TabIndex        =   47
               Top             =   1920
               Width           =   510
            End
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRedHojAnest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedHojAnest (FR0127.FRM)                                  *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Redactar Hoja Anestesia                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1





Private Sub cmdbuscarmaterial_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscarmaterial.Enabled = False

If txtText1(0).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  gstrMedicamentoMaterial = "Material"
  Call objsecurity.LaunchProcess("FR0166")
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(4).Rows > 0 Then
        grdDBGrid1(4).MoveFirst
'        For i = 0 To grdDBGrid1(4).Rows - 1
'          If grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0) _
'             And grdDBGrid1(4).Columns(0).Value <> "Eliminado" Then
'            'no se inserta el producto pues ya est� insertado en el grid
'            noinsertar = False
'            Exit For
'          Else
'            noinsertar = True
'          End If
'          grdDBGrid1(4).MoveNext
'        Next i
      End If

      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(4).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(4).Columns(8).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(4).Columns(5).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(9), rsta.rdoColumns("FRH7CODFORMFAR").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(10), rsta.rdoColumns("FR73DOSIS").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(7), rsta.rdoColumns("FR73REFERENCIA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(11), rsta.rdoColumns("FR93CODUNIMEDIDA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(17), rsta.rdoColumns("FR73INDPRODSAN").Value)
        grdDBGrid1(4).Update
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El material sanitario: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdbuscarmaterial.Enabled = True
End Sub

Private Sub cmdbuscarprot_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscarprot.Enabled = False

If txtText1(0).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  Call objsecurity.LaunchProcess("FR0174")
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(4).Rows > 0 Then
        grdDBGrid1(4).MoveFirst
'        For i = 0 To grdDBGrid1(4).Rows - 1
'          If grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0) _
'             And grdDBGrid1(4).Columns(0).Value <> "Eliminado" Then
'            'no se inserta el producto pues ya est� insertado en el grid
'            noinsertar = False
'            Exit For
'          Else
'            noinsertar = True
'          End If
'          grdDBGrid1(4).MoveNext
'        Next i
      End If
      
      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(4).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(4).Columns(8).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(4).Columns(5).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(9), rsta.rdoColumns("FRH7CODFORMFAR").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(10), rsta.rdoColumns("FR73DOSIS").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(7), rsta.rdoColumns("FR73REFERENCIA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(11), rsta.rdoColumns("FR93CODUNIMEDIDA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(17), rsta.rdoColumns("FR73INDPRODSAN").Value)
        grdDBGrid1(4).Update
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdbuscarprot.Enabled = True
End Sub

Private Sub cmdgruterapmaterial_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdgruterapmaterial.Enabled = False

If txtText1(0).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  gstrLlamador = "Material Sanitario"
  Call objsecurity.LaunchProcess("FR0173")
  gstrLlamador = ""
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(4).Rows > 0 Then
        grdDBGrid1(4).MoveFirst
'        For i = 0 To grdDBGrid1(3).Rows - 1
'          If grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0) _
'             And grdDBGrid1(4).Columns(0).Value <> "Eliminado" Then
'            'no se inserta el producto pues ya est� insertado en el grid
'            noinsertar = False
'            Exit For
'          Else
'            noinsertar = True
'          End If
'          grdDBGrid1(4).MoveNext
'        Next i
      End If

      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(4).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(4).Columns(8).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(4).Columns(5).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(9), rsta.rdoColumns("FRH7CODFORMFAR").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(10), rsta.rdoColumns("FR73DOSIS").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(7), rsta.rdoColumns("FR73REFERENCIA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(11), rsta.rdoColumns("FR93CODUNIMEDIDA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(17), rsta.rdoColumns("FR73INDPRODSAN").Value)
        grdDBGrid1(4).Update
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdgruterapmaterial.Enabled = True
End Sub

Private Sub cmdgruterapmedicamento_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdgruterapmedicamento.Enabled = False

If txtText1(0).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  gstrLlamador = "Medicamentos"
  Call objsecurity.LaunchProcess("FR0173")
  gstrLlamador = ""
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(4).Rows > 0 Then
        grdDBGrid1(4).MoveFirst
'        For i = 0 To grdDBGrid1(4).Rows - 1
'          If grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0) _
'             And grdDBGrid1(4).Columns(0).Value <> "Eliminado" Then
'            'no se inserta el producto pues ya est� insertado en el grid
'            noinsertar = False
'            Exit For
'          Else
'            noinsertar = True
'          End If
'          grdDBGrid1(4).MoveNext
'        Next i
      End If

      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(4).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(4).Columns(8).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(4).Columns(5).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(9), rsta.rdoColumns("FRH7CODFORMFAR").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(10), rsta.rdoColumns("FR73DOSIS").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(7), rsta.rdoColumns("FR73REFERENCIA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(11), rsta.rdoColumns("FR93CODUNIMEDIDA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(17), rsta.rdoColumns("FR73INDPRODSAN").Value)
        grdDBGrid1(4).Update
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdgruterapmedicamento.Enabled = True
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    Dim i As Integer
    
    If Index = 4 Then
      If grdDBGrid1(4).Columns(5).Value <> "" Then
        If grdDBGrid1(4).Columns(17).Value <> True Then
          For i = 3 To 11
              grdDBGrid1(4).Columns(i).CellStyleSet "Medicamento"
          Next i
          For i = 13 To 17
              grdDBGrid1(4).Columns(i).CellStyleSet "Medicamento"
          Next i
        Else
          For i = 3 To 11
              grdDBGrid1(4).Columns(i).CellStyleSet "Material"
          Next i
          For i = 13 To 17
              grdDBGrid1(4).Columns(i).CellStyleSet "Material"
          Next i
        End If
      End If
    End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cmdbuscargruprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscargruprod.Enabled = False

  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    Call objsecurity.LaunchProcess("FR0168")
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(4).Rows > 0 Then
          grdDBGrid1(4).MoveFirst
          For i = 0 To grdDBGrid1(4).Rows - 1
            If grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(4).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(4).MoveNext
          Next i
        End If

        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(4).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(4).Columns(8).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(4).Columns(5).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(9), rsta.rdoColumns("FRH7CODFORMFAR").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(10), rsta.rdoColumns("FR73DOSIS").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(7), rsta.rdoColumns("FR73REFERENCIA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(11), rsta.rdoColumns("FR93CODUNIMEDIDA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(17), rsta.rdoColumns("FR73INDPRODSAN").Value)
        grdDBGrid1(4).Update
          rsta.Close
          Set rsta = Nothing
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                             Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If

cmdbuscargruprod.Enabled = True

End Sub

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscarprod.Enabled = False

If txtText1(0).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  gstrMedicamentoMaterial = "Medicamento"
  Call objsecurity.LaunchProcess("FR0166")
  Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(4).Rows > 0 Then
        grdDBGrid1(4).MoveFirst
'        For i = 0 To grdDBGrid1(4).Rows - 1
'          If grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0) _
'             And grdDBGrid1(4).Columns(0).Value <> "Eliminado" Then
'            'no se inserta el producto pues ya est� insertado en el grid
'            noinsertar = False
'            Exit For
'          Else
'            noinsertar = True
'          End If
'          grdDBGrid1(4).MoveNext
'        Next i
      End If

      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(4).Columns(5).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(4).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(4).Columns(8).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(4).Columns(5).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(9), rsta.rdoColumns("FRH7CODFORMFAR").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(10), rsta.rdoColumns("FR73DOSIS").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(7), rsta.rdoColumns("FR73REFERENCIA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(11), rsta.rdoColumns("FR93CODUNIMEDIDA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(17), rsta.rdoColumns("FR73INDPRODSAN").Value)
        grdDBGrid1(4).Update
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdbuscarprod.Enabled = True

End Sub

Private Sub cmdfirmar_Click()
Dim rstcantidad As rdoResultset
Dim strcantidad As String
Dim strInsert As String
Dim rstSelect As rdoResultset
Dim strSelect As String
Dim persona, producto, fecha, hora, servicio, cantreceta, indquirofano

  If txtText1(0).Text = "" Then
    Exit Sub
  End If

  If txtText1(4).Text = "" Then
    
    strcantidad = "SELECT MAX(FR17CANTIDAD) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
               txtText1(0).Text
    Set rstcantidad = objApp.rdoConnect.OpenResultset(strcantidad)
    If IsNull(rstcantidad(0).Value) Then
      MsgBox "No se puede firmar, ya que no ha rellenado la lista de productos."
    Else
      If rstcantidad(0).Value = 0 Then
        MsgBox "No se puede firmar, ya que no ha rellenado la lista de productos."
      Else
        strSelect = "SELECT FR73CODPRODUCTO,FR17CANTIDAD FROM FR1700 WHERE FR43CODHOJAANEST=" & _
               txtText1(0).Text & " AND FR17CANTIDAD>0 "
        Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
        While Not rstSelect.EOF
          persona = txtText1(6).Text
          producto = rstSelect("FR73CODPRODUCTO").Value
          fecha = "TO_DATE('" & dtcDateCombo1(0) & "','DD/MM/YYYY')"
          If InStr(txtText1(1).Text, ",") > 0 Then
            hora = Left(txtText1(1).Text, InStr(txtText1(1).Text, ",") - 1) & "." & Right(txtText1(1).Text, Len(txtText1(1).Text) - InStr(txtText1(1).Text, ","))
          Else
            hora = txtText1(1).Text
          End If
          servicio = txtText1(17).Text
          cantreceta = rstSelect("FR17CANTIDAD").Value
          indquirofano = -1
          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO,FR65CANTIDAD)" & _
                      " VALUES (" & persona & "," & producto & "," & fecha & "," & hora & "," & servicio & "," & cantreceta & "," & indquirofano & "," & cantreceta & ")"
          On Error Resume Next
          ERR.Clear
          objApp.rdoConnect.Execute strInsert, 64
          Select Case ERR.Number
          Case 0
            ' NO HACER NADA
          Case 40002
'            MsgBox "A este paciente ya se la ha recetado el producto: " & producto & " a esta misma hora", vbExclamation
          Case Else
             MsgBox ERR.Number & " " & ERR.Description, vbCritical
          End Select
          On Error GoTo 0
          rstSelect.MoveNext
        Wend
        objApp.rdoConnect.Execute "Commit", 64
        Call MsgBox("La Hoja de Anestesia ha sido Firmada", vbInformation, "Aviso")
        rstSelect.Close
        Set rstSelect = Nothing
        
'        strInsert = ""
'        If chkCheck1(0).Value = True Then
'          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
'          strInsert = strInsert & " SELECT " & persona & "," _
'                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
'                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
'                      " FROM FR4300,FR5200 WHERE FR43INDANESTGENADUL<>0 "
'          objApp.rdoConnect.Execute strInsert, 64
'          objApp.rdoConnect.Execute "Commit", 64
'        End If
'        If chkCheck1(1).Value = True Then
'          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
'          strInsert = strInsert & " SELECT " & persona & "," _
'                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
'                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
'                      " FROM FR4300,FR5200 WHERE FR43INDANESTGENPED<>0 "
'          objApp.rdoConnect.Execute strInsert, 64
'          objApp.rdoConnect.Execute "Commit", 64
'        End If
'        If chkCheck1(2).Value = True Then
'          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
'          strInsert = strInsert & " SELECT " & persona & "," _
'                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
'                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
'                      " FROM FR4300,FR5200 WHERE FR43INDANALGLOCOR<>0 "
'          objApp.rdoConnect.Execute strInsert, 64
'          objApp.rdoConnect.Execute "Commit", 64
'        End If
'        If chkCheck1(3).Value = True Then
'          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
'          strInsert = strInsert & " SELECT " & persona & "," _
'                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
'                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
'                      " FROM FR4300,FR5200 WHERE FR43INDCEC<>0 "
'          objApp.rdoConnect.Execute strInsert, 64
'          objApp.rdoConnect.Execute "Commit", 64
'        End If
'        If chkCheck1(4).Value = True Then
'          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)"
'          strInsert = strInsert & " SELECT " & persona & "," _
'                      & "FR7300.FR73CODPRODUCTO" & "," & fecha & "," & hora & "," & _
'                      servicio & "," & "FR5200.FR52DOSISPORUM" & "," & indquirofano & _
'                      " FROM FR4300,FR5200 WHERE FR43INDTRANSPHEPA<>0 "
'          objApp.rdoConnect.Execute strInsert, 64
'          objApp.rdoConnect.Execute "Commit", 64
'        End If

        txtText1(4).SetFocus
        Call objWinInfo.CtrlSet(txtText1(4), objsecurity.strUser)
        Call objWinInfo.DataSave
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        Call objWinInfo.DataRefresh
      End If
    End If
    rstcantidad.Close
    Set rstcantidad = Nothing
  Else
    MsgBox "La Hoja de Quir�fano ya esta firmada."
  End If

End Sub

Private Sub cmdHistClin_Click()

cmdHistClin.Enabled = False

cmdHistClin.Enabled = True

End Sub

Private Sub cmdinter_Click()
Dim rsta As rdoResultset
Dim stra As String
Dim rstprin As rdoResultset
Dim strprin As String
Dim i As Integer

cmdinter.Enabled = False
        
  gstrLista = ""
  'se cogen todos los principios activos de los productos que est�n en el grid y se
  'meten en una lista
  grdDBGrid1(4).MoveFirst
  For i = 0 To grdDBGrid1(4).Rows - 1
    stra = "SELECT count(*) FROM FR6400 WHERE FR73CODPRODUCTO=" & _
    grdDBGrid1(4).Columns(5).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value > 0 Then
      strprin = "SELECT FR68CODPRINCACTIV FROM FR6400 WHERE FR73CODPRODUCTO=" & _
                grdDBGrid1(4).Columns(5).Value
      Set rstprin = objApp.rdoConnect.OpenResultset(strprin)
      If gstrLista = "" Then
        gstrLista = gstrLista & rstprin.rdoColumns(0).Value
      Else
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
      End If
      rstprin.MoveNext
      While Not rstprin.EOF
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
        rstprin.MoveNext
      Wend
      rstprin.Close
      Set rstprin = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    grdDBGrid1(4).MoveNext
  Next i
  
  If gstrLista <> "" Then
    gstrLista = "(" & gstrLista & ")"
    Call objsecurity.LaunchProcess("FR0163")
  End If

cmdinter.Enabled = True

End Sub

Private Sub cmdPerfFTP_Click()
Dim mensaje As String

cmdPerfFTP.Enabled = False

  If IsNumeric(txtText1(0).Text) Then
    If IsNumeric(txtText1(6).Text) Then
      Call objsecurity.LaunchProcess("FR0170")
    Else
      mensaje = MsgBox("No hay ning�n paciente.", vbInformation, "Aviso")
    End If
  End If

cmdPerfFTP.Enabled = True


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo3 As New clsCWForm
  Dim objMultiInfo4 As New clsCWForm
  Dim strKey As String
  Dim sqlstr As String
  Dim rsta As rdoResultset
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .blnAskPrimary = False
    
    .strName = "DATOS GENERALES"
    
    .strTable = "FR4300"
    
    Call .FormAddOrderField("FR43CODHOJAANEST", cwAscending)
  
    If gstrLlamador = "frmFirmarHojAnest" Then
      .strWhere = "FR43CODHOJAANEST=" & gintcodhojaanest & " AND SG02COD IS NULL " 'y NO FIRMADA
      .intAllowance = cwAllowModify
    Else
      .strWhere = "SG02COD IS NULL "
    End If
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Hoja Anestesia")
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "C�digo Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR43FECINTERVEN", "Fecha Intervenci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR43HORAINTERVEN", "Hora Intervenci�n", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR43DESINTERVENCION", "Descripci�n Intervenci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR43DURACCIRUGIA", "Duraci�n Cirug�a", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ANE", "Anestesista", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_CIR", "Cirujano", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD", "Firma Digital", cwString)
    Call .FormAddFilterWhere(strKey, "FR43INDANESTGENADUL", "Ind. Anest. General Adulto", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR43INDANESTGENPED", "Ind. Anest. General Pedi�trica", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR43INDANALGLOCOR", "Ind. Analgosedaci�n y Anest. Locorregional", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR43INDCEC", "Ind. Anest. para CEC", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR43INDTRANSPHEPA", "Ind. Anest. para Transplante Hep�tico", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR43CODHOJAANEST", "C�digo Hoja Anestesia")

  End With
  
  With objMultiInfo4
    Set .objFormContainer = fraFrame1(4)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(4)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

    .strName = "OTROS CONSUMOS"

    .strTable = "FR1700"
    .intAllowance = cwAllowModify + cwAllowDelete

    Call .FormAddOrderField("FR17NUMLINEA", cwAscending)

    Call .FormAddRelation("FR43CODHOJAANEST", txtText1(0))

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OTROS CONSUMOS")
    Call .FormAddFilterWhere(strKey, "FR17NUMLINEA", "N�mero de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "FR17CANTIDAD", "Cantidad Consumida", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR17INDLEIDO", "Leida?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDBLOQUEO", "Bloqueada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDESTHA", "Estado Linea?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR17NUMLINEA", "N�mero de Linea")

  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo4, cwFormMultiLine)

    Call .GridAddColumn(objMultiInfo4, "L�nea", "FR17NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo4, "Hoja Quir�fano", "FR43CODHOJAANEST", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo4, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo4, "C�d.Int", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo4, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo4, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo4, "F.F", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo4, "Dosis", "", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo4, "U.M", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo4, "Cant.Consumida", "FR17CANTIDAD", cwDecimal)
    Call .GridAddColumn(objMultiInfo4, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo4, "Leida?", "FR17INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo4, "Bloqueada?", "FR17INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo4, "Estado Linea?", "FR17INDESTHA", cwBoolean)
    Call .GridAddColumn(objMultiInfo4, "Medicamento/Material", "", cwBoolean)
    
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(grdDBGrid1(4).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(4).Columns(4)).intKeyNo = 1
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo4)
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(8), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(9), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(10), "CI22SEGAPEL")
    .CtrlGetInfo(txtText1(6)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "SG02COD_ANE", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(12), "SG02APE1")
    .CtrlGetInfo(txtText1(11)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(14), "SG02APE1")
    .CtrlGetInfo(txtText1(13)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "SG02COD_CIR", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(16), "SG02APE1")
    .CtrlGetInfo(txtText1(15)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(17)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(17)), txtText1(18), "AD02DESDPTO")
    .CtrlGetInfo(txtText1(17)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "SG02COD", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(5), "SG02APE1")
    .CtrlGetInfo(txtText1(4)).blnForeign = True
    .CtrlGetInfo(txtText1(4)).blnReadOnly = True
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(4).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(4).Columns(8), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(4).Columns(9), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(4).Columns(10), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(4).Columns(7), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(4).Columns(17), "FR73INDPRODSAN")
    .CtrlGetInfo(grdDBGrid1(4).Columns(5)).blnReadOnly = True
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(11)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(11)), grdDBGrid1(4).Columns(13), "FR93DESUNIMEDIDA")
    .CtrlGetInfo(grdDBGrid1(4).Columns(11)).blnReadOnly = True
     
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(17)).blnInFind = True
    .CtrlGetInfo(txtText1(18)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
        
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
grdDBGrid1(0).Columns(1).Width = 1000
grdDBGrid1(0).Columns(2).Width = 1000
grdDBGrid1(0).Columns(3).Width = 600
grdDBGrid1(0).Columns(4).Width = 800

grdDBGrid1(4).Columns(6).Width = 700 'c�d.interno
grdDBGrid1(4).Columns(8).Width = 3500  'descripci�n
grdDBGrid1(4).Columns(9).Width = 450   'F.F
grdDBGrid1(4).Columns(10).Width = 900   'dosis
grdDBGrid1(4).Columns(7).Width = 1300 'referencia
grdDBGrid1(4).Columns(12).Width = 1400 'cantidad
grdDBGrid1(4).Columns(11).Width = 750  'U.M

grdDBGrid1(4).Columns(0).Width = 0
grdDBGrid1(4).Columns(0).Visible = False
grdDBGrid1(4).Columns(3).Visible = False
grdDBGrid1(4).Columns(4).Visible = False
grdDBGrid1(4).Columns(5).Visible = False
grdDBGrid1(4).Columns(13).Visible = False
grdDBGrid1(4).Columns(14).Visible = False
grdDBGrid1(4).Columns(15).Visible = False
grdDBGrid1(4).Columns(16).Visible = False

grdDBGrid1(4).Columns(12).BackColor = &HFFFF00

grdDBGrid1(4).Columns(17).Visible = False

grdDBGrid1(4).RemoveAll
grdDBGrid1(4).Refresh

    If gstrLlamador = "frmFirmarHojAnest" Then
      Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
    End If

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "CI2200"
      
      Set objField = .AddField("CI21CODPERSONA")
      objField.strSmallDesc = "C�digo Paciente"
      
      Set objField = .AddField("CI22NUMHISTORIA")
      objField.strSmallDesc = "Historia"
      
      Set objField = .AddField("CI22NOMBRE")
      objField.strSmallDesc = "Nombre"
      
      Set objField = .AddField("CI22PRIAPEL")
      objField.strSmallDesc = "Apellido 1�"
      
      Set objField = .AddField("CI22SEGAPEL")
      objField.strSmallDesc = "Apellido 2�"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(6), .cllValues("CI21CODPERSONA"))
      End If
    End With
    Set objSearch = Nothing
  End If
  
  If strCtrl = "txtText1(11)" Or strCtrl = "txtText1(13)" Or _
     strCtrl = "txtText1(15)" Or strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "SG0200"
      .strOrder = "ORDER BY SG02APE1"
      
      Set objField = .AddField("SG02COD")
      objField.strSmallDesc = "C�digo Usuario"
      
      Set objField = .AddField("SG02APE1")
      objField.strSmallDesc = "Primer Apellido"
      
      If .Search Then
        Select Case strCtrl
          Case "txtText1(11)"
            Call objWinInfo.CtrlSet(txtText1(11), .cllValues("SG02COD"))
          Case "txtText1(13)"
            Call objWinInfo.CtrlSet(txtText1(13), .cllValues("SG02COD"))
          Case "txtText1(15)"
            Call objWinInfo.CtrlSet(txtText1(15), .cllValues("SG02COD"))
          Case "txtText1(4)"
            Call objWinInfo.CtrlSet(txtText1(4), .cllValues("SG02COD"))
        End Select
      End If
    End With
    Set objSearch = Nothing
  End If

  If strCtrl = "txtText1(17)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
     .strWhere = " WHERE AD32CODTIPODPTO = 3" & _
                 " AND AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
                 " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)))"
       .strOrder = "ORDER BY AD02DESDPTO"
                 
      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�digo Servicio"
      
      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Descripci�n Servicio"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(17), .cllValues("AD02CODDPTO"))
      End If
    End With
    Set objSearch = Nothing
  End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

  If txtText1(4).Text <> "" Then
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        cmdbuscargruprod.Enabled = False
        cmdbuscarprod.Enabled = False
        cmdinter.Enabled = False

        Select Case strFormName
        Case "DATOS GENERALES"
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        Case "OTROS CONSUMOS"
          Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
        End Select
  Else
    Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    If gstrLlamador = "frmFirmarHojAnest" Then
      objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    Else
      objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
    End If
    Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowModify + cwAllowDelete
    cmdbuscargruprod.Enabled = True
    cmdbuscarprod.Enabled = True
    cmdinter.Enabled = True

    Select Case strFormName
    Case "DATOS GENERALES"
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    Case "OTROS CONSUMOS"
      Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
    End Select
  End If

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'Dim rsta As rdoResultset
'Dim sqlstr, strInsert As String
'Dim cuenta, numlinea As Integer
'
'  If blnError = False And strFormName = "DATOS GENERALES" Then
'    sqlstr = "SELECT COUNT(FR43CODHOJAANEST) FROM FR1700 WHERE FR43CODHOJAANEST=" & txtText1(0).Text
'    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'    cuenta = rsta.rdoColumns(0).Value
'    rsta.Close
'    Set rsta = Nothing
'
'    If cuenta = 0 Then
'      'A�adir Sueros
'      numlinea = 1
'      sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA FROM FR7300 WHERE FR73INDANESTSUERO<>0"
'      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'      While Not rsta.EOF
'
'        'insertar linea
'        strInsert = "INSERT INTO FR1700 (FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD)" & _
'                    " VALUES (" & txtText1(0).Text & "," & numlinea & "," & rsta.rdoColumns(0).Value & "," & "'" & rsta.rdoColumns(1).Value & "'" & ",0)"
'        objApp.rdoConnect.Execute strInsert, 64
'        objApp.rdoConnect.Execute "Commit", 64
'
'        numlinea = numlinea + 1
'        rsta.MoveNext
'      Wend
'      rsta.Close
'      Set rsta = Nothing
'
'      'A�adir Medicamentos
'      sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA FROM FR7300 WHERE FR73INDANESTMED<>0"
'      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'      While Not rsta.EOF
'
'        'insertar linea
'        strInsert = "INSERT INTO FR1700 (FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD)" & _
'                    " VALUES (" & txtText1(0).Text & "," & numlinea & "," & rsta.rdoColumns(0).Value & "," & "'" & rsta.rdoColumns(1).Value & "'" & ",0)"
'        objApp.rdoConnect.Execute strInsert, 64
'        objApp.rdoConnect.Execute "Commit", 64
'
'        numlinea = numlinea + 1
'        rsta.MoveNext
'      Wend
'      rsta.Close
'      Set rsta = Nothing
'
'      'A�adir Especificos
'      sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA FROM FR7300 WHERE FR73INDANESTESPEC<>0"
'      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'      While Not rsta.EOF
'
'        'insertar linea
'        strInsert = "INSERT INTO FR1700 (FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD)" & _
'                    " VALUES (" & txtText1(0).Text & "," & numlinea & "," & rsta.rdoColumns(0).Value & "," & "'" & rsta.rdoColumns(1).Value & "'" & ",0)"
'        objApp.rdoConnect.Execute strInsert, 64
'        objApp.rdoConnect.Execute "Commit", 64
'
'        numlinea = numlinea + 1
'        rsta.MoveNext
'      Wend
'      rsta.Close
'      Set rsta = Nothing
'
'      objWinInfo.objWinActiveForm.blnChanged = False
'      Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
'      Call objWinInfo.DataRefresh
'      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'    End If
'  End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
'
'
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabGeneral1_Click(Index As Integer, PreviousTab As Integer)
  
  Select Case tabGeneral1(1).Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 1
      Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
  End Select

End Sub

Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
   tab1.Tab = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "DATOS GENERALES" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR43CODHOJAANEST_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    Call objWinInfo.CtrlGotFocus
    Call objWinInfo.CtrlLostFocus
    rsta.Close
    Set rsta = Nothing
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "OTROS CONSUMOS" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea(0).Value) Then
           linea = grdDBGrid1(4).Rows
        Else
           linea = rstlinea(0).Value + grdDBGrid1(4).Rows
        End If
        grdDBGrid1(4).Columns(3).Value = linea
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna hoja de anestesia", vbInformation
        Exit Sub
      End If
    Else
      'cuando sea Guardar y Alta Masiva que meta el c�digo autom�ticamente
      If btnButton.Index = 4 And mnuOpcionesOpcion(50).Checked = True _
         And objWinInfo.objWinActiveForm.strName = "DATOS GENERALES" Then
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                sqlstr = "SELECT FR43CODHOJAANEST_SEQUENCE.nextval FROM dual"
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
                Call objWinInfo.CtrlGotFocus
                Call objWinInfo.CtrlLostFocus
                rsta.Close
                Set rsta = Nothing
      Else
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      End If
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer

  If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "DATOS GENERALES" Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    sqlstr = "SELECT FR43CODHOJAANEST_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    Call objWinInfo.CtrlGotFocus
    Call objWinInfo.CtrlLostFocus
    rsta.Close
    Set rsta = Nothing
  Else
    If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "OTROS CONSUMOS" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea(0).Value) Then
           linea = grdDBGrid1(4).Rows
        Else
           linea = rstlinea(0).Value + grdDBGrid1(4).Rows
        End If
        grdDBGrid1(4).Columns(3).Value = linea
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna hoja de anestesia", vbInformation
        Exit Sub
      End If
    Else
      'cuando sea Guardar y Alta Masiva que meta el c�digo autom�ticamente
      If intIndex = 40 And mnuOpcionesOpcion(50).Checked = True _
         And objWinInfo.objWinActiveForm.strName = "DATOS GENERALES" Then
                Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
                sqlstr = "SELECT FR43CODHOJAANEST_SEQUENCE.nextval FROM dual"
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
                Call objWinInfo.CtrlGotFocus
                Call objWinInfo.CtrlLostFocus
                rsta.Close
                Set rsta = Nothing
      Else
                Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      End If
    End If
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   tab1.Tab = 1
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
'Private Sub chkCheck1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
'End Sub

'Private Sub chkCheck1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
'End Sub

'Private Sub chkCheck1_Click(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Call objWinInfo.CtrlDataChange

'If intIndex = 0 Then
'   If txtText1(0).Text = "" Then
'      cmdCargarProdProt.Enabled = False
'   Else
'      cmdCargarProdProt.Enabled = True
'   End If
'End If

If intIndex = 1 Then
    If IsNumeric(txtText1(1).Text) Then
        If txtText1(1).Text > 24 Or txtText1(1).Text < 0 Then
            Call MsgBox("Hora de Intervenci�n fuera de rango", vbInformation, "Aviso")
            txtText1(1).Text = ""
        End If
    End If
End If

End Sub


