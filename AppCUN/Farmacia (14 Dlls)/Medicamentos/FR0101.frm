VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmDefPedBajCtrl 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Definir Medicamento Bajo Control"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0101.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdquitar 
      Caption         =   "Eliminar"
      Height          =   375
      Left            =   6240
      TabIndex        =   7
      Top             =   4080
      Width           =   1695
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Medicamentos Bajo Control"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Tag             =   "Actuaciones Asociadas"
      Top             =   4560
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2985
         Index           =   0
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   11445
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   20188
         _ExtentY        =   5265
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmda�adir 
      Caption         =   "A�adir"
      Height          =   375
      Left            =   4320
      TabIndex        =   4
      Top             =   4080
      Width           =   1695
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3000
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   11445
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   20188
         _ExtentY        =   5292
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefPedBajCtrl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0101.FRM                                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: definir medicamento bajo control                        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmda�adir_Click()
Dim mvarBkmrk As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim insertar As Integer
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim stra As String
Dim strinsertar As String
Dim mensaje As String

cmda�adir.Enabled = False

  'se mira que no se meta el mismo producto 2 veces
  If grdDBGrid1(1).SelBookmarks.Count > 0 Then
  insertar = 0
  mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  For mintisel = 0 To mintNTotalSelRows - 1
    mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintisel)
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
    'stra = "SELECT COUNT(*) FROM FR1200 WHERE FR73CODPRODUCTO=" & _
    '        grdDBGrid1(1).Columns(5).CellValue(mvarBkmrk)
    stra = "SELECT COUNT(*) FROM FR1200 WHERE FR73CODPRODUCTO=?"
    Set qrya = objApp.rdoConnect.CreateQuery("", stra)
    qrya(0) = grdDBGrid1(1).Columns(5).CellValue(mvarBkmrk)
    Set rsta = qrya.OpenResultset(stra)
    'Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value = 0 Then
     strinsertar = "INSERT INTO FR1200 (FR73CODPRODUCTO,FR00CODGRPTERAP,FRA4SUBGRPTERAPE)" & _
                  " VALUES (" & _
                  grdDBGrid1(1).Columns(5).CellValue(mvarBkmrk) & ","
     If grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk) = "" Then
        strinsertar = strinsertar & "NULL" & ","
     Else
        strinsertar = strinsertar & "'" & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk) & "'" & ","
     End If
     If grdDBGrid1(1).Columns(4).CellValue(mvarBkmrk) = "" Then
        strinsertar = strinsertar & "NULL"
     Else
        strinsertar = strinsertar & "'" & grdDBGrid1(1).Columns(4).CellValue(mvarBkmrk) & "'"
     End If
     strinsertar = strinsertar & ")"
    
     objApp.rdoConnect.Execute strinsertar, 64
     objApp.rdoConnect.Execute "Commit", 64
    End If
    rsta.Close
    Set rsta = Nothing
  Next mintisel
    
'se refresca el multil�nea de medicamentos bajo control
Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
objWinInfo.DataRefresh
  
Else
    mensaje = MsgBox("No ha seleccionado ning�n producto.", vbInformation, "Aviso")
End If
cmda�adir.Enabled = True
End Sub

Private Sub cmdquitar_Click()
Dim mvarBkmrk As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim strdelete As String
Dim mensaje As String

cmdquitar.Enabled = False

If grdDBGrid1(0).SelBookmarks.Count > 0 Then
  mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
  For mintisel = 0 To mintNTotalSelRows - 1
    mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
    strdelete = "DELETE FR1200 WHERE FR73CODPRODUCTO=" & _
                 grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk)
    objApp.rdoConnect.Execute strdelete, 64
    objApp.rdoConnect.Execute "Commit", 64
  Next mintisel
    
    'se refresca el multil�nea de medicamentos bajo control
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    objWinInfo.DataRefresh
  
Else
    mensaje = MsgBox("No ha seleccionado ning�n producto.", vbInformation, "Aviso")
End If
cmdquitar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm
    Dim objMultiInfo1 As New clsCWForm

    Dim strKey As String
  
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Productos"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR7300"
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Productos")
        Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�d.Interno", cwString)
        Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n", cwString)
        Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "F.F", cwString)
        Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "U.M", cwString)
        Call .FormAddFilterWhere(strKey, "FR73REFERENCIA", "Referencia", cwString)
        Call .FormAddFilterWhere(strKey, "FR73PRECIONETCOMPRA", "Precio Compra", cwDecimal)
        Call .FormAddFilterWhere(strKey, "FR73PRECIOVENTA", "Precio Venta", cwDecimal)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR73CODINTFAR", "C�digo")
        Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n")
        Call .FormAddFilterOrder(strKey, "FR73PRECIONETCOMPRA", "Precio Compra")
        Call .FormAddFilterOrder(strKey, "FR73PRECIOVENTA", "Precio Venta")
        
        
    End With
    
    
    With objMultiInfo1
        .strName = "Medicamentos Bajo Control"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR1201J"
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Productos")
        Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�d.Interno", cwString)
        Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Producto", cwString)
        Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "F.F", cwString)
        Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "U.M", cwString)
        Call .FormAddFilterWhere(strKey, "FR73REFERENCIA", "Referencia", cwString)
        Call .FormAddFilterWhere(strKey, "GRUPO", "Grupo Terape�tico", cwString)
        Call .FormAddFilterWhere(strKey, "DESCGRUPO", "Desc.Grupo Terape�tico", cwString)
   
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
        Call .FormAddFilterOrder(strKey, "FR73CODINTFAR", "C�digo Interno")
        Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n Producto")
        Call .FormAddFilterOrder(strKey, "GRUPO", "Grupo Terape�tico")
        Call .FormAddFilterOrder(strKey, "DESCGRUPO", "Desc.Grupo Terape�tico")

    End With

    With objWinInfo
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "Grupo Terape�tico", "FR00CODGRPTERAP", cwString, 10)
        Call .GridAddColumn(objMultiInfo, "Subgrupo Terape�tico", "", cwString, 10)
        Call .GridAddColumn(objMultiInfo, "C�digo", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "C�d.Int", "FR73CODINTFAR", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Descripci�n", "FR73DESPRODUCTO", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "F.F", "FRH7CODFORMFAR", cwString, 3)
        Call .GridAddColumn(objMultiInfo, "Dosis", "FR73DOSIS", cwDecimal, 9)
        Call .GridAddColumn(objMultiInfo, "U.M", "FR93CODUNIMEDIDA", cwString, 5)
        Call .GridAddColumn(objMultiInfo, "Referencia", "FR73REFERENCIA", cwString, 15)
        Call .GridAddColumn(objMultiInfo, "Precio Compra", "FR73PRECIONETCOMPRA", cwDecimal, 15)
        Call .GridAddColumn(objMultiInfo, "Precio Venta", "FR73PRECIOVENTA", cwDecimal, 15)
  
        Call .FormCreateInfo(objMultiInfo)
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnInFind = True
              
        Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo1, "C�digo", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo1, "C�d.Int", "FR73CODINTFAR", cwString, 6)
        Call .GridAddColumn(objMultiInfo1, "Descripci�n", "FR73DESPRODUCTO", cwString, 50)
        Call .GridAddColumn(objMultiInfo1, "F.F", "FRH7CODFORMFAR", cwString, 3)
        Call .GridAddColumn(objMultiInfo1, "Dosis", "FR73DOSIS", cwDecimal, 9)
        Call .GridAddColumn(objMultiInfo1, "U.M", "FR93CODUNIMEDIDA", cwString, 5)
        Call .GridAddColumn(objMultiInfo1, "Referencia", "FR73REFERENCIA", cwString, 15)
        Call .GridAddColumn(objMultiInfo1, "C�d.Grupo Terape�tico", "GRUPO", cwString, 10)
        Call .GridAddColumn(objMultiInfo1, "Grupo Terape�tico", "DESCGRUPO", cwString, 75)
        
  
        Call .FormCreateInfo(objMultiInfo1)
       
        Call .FormChangeColor(objMultiInfo1)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(4).Visible = False
    grdDBGrid1(1).Columns(5).Visible = False
    
    grdDBGrid1(1).Columns(6).Width = 800
    grdDBGrid1(1).Columns(7).Width = 4000
    grdDBGrid1(1).Columns(8).Width = 500
    grdDBGrid1(1).Columns(9).Width = 700
    grdDBGrid1(1).Columns(10).Width = 750
    grdDBGrid1(1).Columns(11).Width = 1700
    grdDBGrid1(1).Columns(12).Width = 1200
    grdDBGrid1(1).Columns(13).Width = 1150
    
    grdDBGrid1(0).Columns(3).Visible = False
    
    grdDBGrid1(0).Columns(4).Width = 800
    grdDBGrid1(0).Columns(5).Width = 2800
    grdDBGrid1(0).Columns(6).Width = 500
    grdDBGrid1(0).Columns(7).Width = 700
    grdDBGrid1(0).Columns(8).Width = 750
    grdDBGrid1(0).Columns(9).Width = 1700
    
    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.DataRefresh
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


