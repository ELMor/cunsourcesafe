VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************
Const FRRepConsCaducidad                    As String = "FRR441" 'Consultar Caducidades
Const FRRepAlmacenes                        As String = "FRR041" 'Almacenes
Const FRRepRealInventario                   As String = "FRR421" 'Realizar Inventario
Const FRRepRealInventario_COPIA             As String = "FRR999" 'Realizar Inventario_COPIA
Const FRRepMaestroPorCuentaContable_COPIA   As String = "FRR998" 'Maestro por Cuenta Contable_COPIA


Const PRWinConsCaducidad                    As String = "FR0044"
Const PRWinRealInvent                       As String = "FR0042"
Const PRWinRealInvent_COPIA                 As String = "FR9999"
Const PRWinMantInventario                   As String = "FR0064"
Const PRWinMantAlmacen                      As String = "FR0004"
Const PRWinBuscarProductosAlm               As String = "FR0080"
Const PRWinImprimirEtiquetasProductos       As String = "FR0082"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
  Case PRWinConsCaducidad
      Load frmConsCaducidad
      'Call objsecurity.AddHelpContext(528)
      Call frmConsCaducidad.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmConsCaducidad
      Set frmConsCaducidad = Nothing
  Case PRWinRealInvent
      Load frmRealInvent
      'Call objsecurity.AddHelpContext(528)
      Call frmRealInvent.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRealInvent
      Set frmRealInvent = Nothing
  Case PRWinRealInvent_COPIA
      Load frmRealInvent_COPIA
      'Call objsecurity.AddHelpContext(528)
      Call frmRealInvent_COPIA.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRealInvent_COPIA
      Set frmRealInvent_COPIA = Nothing
  Case PRWinMantInventario
       Load frmMantInventario
      'Call objsecurity.AddHelpContext(528)
      Call frmMantInventario.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantInventario
      Set frmMantInventario = Nothing
  Case PRWinMantAlmacen
      Load frmMantAlmacen
      'Call objsecurity.AddHelpContext(528)
      Call frmMantAlmacen.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMantAlmacen
      Set frmMantAlmacen = Nothing
 Case PRWinBuscarProductosAlm
      Load frmBuscarProductosAlm
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarProductosAlm.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBuscarProductosAlm
      Set frmBuscarProductosAlm = Nothing
      
  Case PRWinImprimirEtiquetasProductos
      Load frmImprimirEtiquetasProductos
      'Call objsecurity.AddHelpContext(528)
      Call frmImprimirEtiquetasProductos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmImprimirEtiquetasProductos
      Set frmImprimirEtiquetasProductos = Nothing
      
      
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 12, 1 To 4) As Variant

  aProcess(1, 1) = PRWinMantAlmacen
  aProcess(1, 2) = "Mantenimiento Almac�n"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinConsCaducidad
  aProcess(2, 2) = "Consultar Caducidades"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
 
  aProcess(3, 1) = PRWinRealInvent
  aProcess(3, 2) = "Realizar Inventario"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinRealInvent_COPIA
  aProcess(10, 2) = "Realizar Inventario COPIA"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
    
  aProcess(4, 1) = PRWinMantInventario
  aProcess(4, 2) = "Mantenimiento Inventario"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = FRRepConsCaducidad
  aProcess(5, 2) = "Consultar Caducidades"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeReport
  
  aProcess(6, 1) = FRRepAlmacenes
  aProcess(6, 2) = "Almacenes"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeReport
  
  aProcess(7, 1) = FRRepRealInventario
  aProcess(7, 2) = "Realizar Inventario"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeReport
  
  aProcess(11, 1) = FRRepRealInventario_COPIA
  aProcess(11, 2) = "Realizar Inventario_COPIA"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeReport
  
  aProcess(12, 1) = FRRepMaestroPorCuentaContable_COPIA
  aProcess(12, 2) = "Maestro por Cuenta Contable_COPIA"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeReport
  
  
  aProcess(8, 1) = PRWinBuscarProductosAlm
  aProcess(8, 2) = "Buscador de Productos"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinImprimirEtiquetasProductos
  aProcess(9, 2) = "Imprimir Etiquetas Productos"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
  
End Sub
