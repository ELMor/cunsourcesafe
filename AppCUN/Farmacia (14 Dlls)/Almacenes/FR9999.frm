VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmRealInvent_COPIA 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Realizar Inventario _ COPIA"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11550
   ControlBox      =   0   'False
   ForeColor       =   &H00808080&
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   11550
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11550
      _ExtentX        =   20373
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "Buscar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   615
      Left            =   120
      TabIndex        =   21
      Top             =   3600
      Width           =   11415
      Begin VB.TextBox txtUbicacion 
         Height          =   285
         Left            =   10560
         TabIndex        =   28
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtCodProv 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   8880
         MaxLength       =   3
         TabIndex        =   24
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtProd 
         Height          =   285
         Left            =   3240
         MaxLength       =   50
         TabIndex        =   23
         Top             =   240
         Width           =   4455
      End
      Begin VB.TextBox txtCodInt 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1080
         MaxLength       =   6
         TabIndex        =   22
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Ub."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   10080
         TabIndex        =   29
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "C�d. Prov."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   7920
         TabIndex        =   27
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2400
         TabIndex        =   26
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "C�d. Int."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   25
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame FrameInventario 
      Caption         =   "Inventario Copia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3855
      Left            =   120
      TabIndex        =   15
      Tag             =   "Actuaciones Asociadas"
      Top             =   4200
      Width           =   11775
      Begin SSDataWidgets_B.SSDBGrid GridInventario 
         Height          =   3375
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   360
         Width           =   11610
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   20479
         _ExtentY        =   5953
         _StockProps     =   79
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
      End
   End
   Begin VB.CommandButton cmdactinvent 
      Caption         =   "Actualizar Inventario"
      Height          =   375
      Left            =   9480
      TabIndex        =   14
      Top             =   2760
      Width           =   1815
   End
   Begin VB.CommandButton cmdejecutarrec 
      Caption         =   "Ejecutar Recuento"
      Height          =   375
      Left            =   9480
      TabIndex        =   13
      Top             =   2280
      Width           =   1815
   End
   Begin VB.Frame frame1 
      Caption         =   "Tipo de Recuento "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1335
      Index           =   0
      Left            =   9000
      TabIndex        =   10
      Top             =   600
      Width           =   2775
      Begin VB.TextBox txtdecimales 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2160
         MaxLength       =   2
         TabIndex        =   19
         Top             =   840
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtporcentaje 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1560
         MaxLength       =   2
         TabIndex        =   18
         Top             =   840
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Porcentaje "
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   17
         Top             =   840
         Width           =   1215
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Total"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   600
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Aleatorio"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   360
         Width           =   975
      End
      Begin VB.Label lblcoma 
         Caption         =   ","
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2040
         TabIndex        =   20
         Top             =   880
         Visible         =   0   'False
         Width           =   135
      End
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   4200
      TabIndex        =   9
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   4200
      TabIndex        =   8
      Top             =   2280
      Width           =   615
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   4200
      TabIndex        =   7
      Top             =   1800
      Width           =   615
   End
   Begin VB.CommandButton cmdrellenar 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   4200
      TabIndex        =   6
      Top             =   1320
      Width           =   615
   End
   Begin VB.Frame frame1 
      Caption         =   "Almacenes Seleccionados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Index           =   1
      Left            =   4920
      TabIndex        =   4
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   3975
      Begin SSDataWidgets_B.SSDBGrid Grid1 
         Height          =   2535
         Index           =   1
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   3690
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   2
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   5106
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         _ExtentX        =   6509
         _ExtentY        =   4471
         _StockProps     =   79
         Caption         =   "ALMACENES SELECCIONADOS"
         ForeColor       =   -2147483630
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Almacenes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   480
      Width           =   3975
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2535
         Index           =   0
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   3690
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   6509
         _ExtentY        =   4471
         _StockProps     =   79
         Caption         =   "ALMACENES"
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   11550
      _ExtentX        =   20373
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   4320
      Top             =   720
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRealInvent_COPIA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRealInvent_COPIA (FR0042_COPIA.FRM)                       *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 2000                                            *
'* DESCRIPCION: realizar inventario_COPIA                               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim almacenes As String
Dim lista_productos As String
Dim mdblPrimListProd As Double
Dim mdblUltListProd As Double
Dim mstrCI As String
Dim mstrPrd As String
Dim mstrCP As String
Dim mstrUB As String
Dim mstrCla As String


Private Sub Recuento_Aleatorio(almacen, tp, numproductos)
'funci�n que coge el % de productos a examinar y el n� de productos a examinar
'y devuelve una lista con los c�digos de los productos
Dim porcentaje As Variant
Dim i As Integer
Dim stra As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset

If IsNull(tp) Then
    tp = 99
End If

porcentaje = (numproductos * CCur(objGen.ReplaceStr(tp, ".", ",", 1))) / 100
If porcentaje = 0 Then
    Exit Sub
End If
porcentaje = Int(porcentaje)   ' + 1
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
'stra = "SELECT FR73CODPRODUCTO FROM FR4700 WHERE " & _
'         "FR04CODALMACEN=" & almacen
stra = "SELECT FR73CODPRODUCTO FROM FR4700 WHERE " & _
         "FR04CODALMACEN=?"
Set qrya = objApp.rdoConnect.CreateQuery("", stra)
qrya(0) = almacen
Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'Set rsta = objApp.rdoConnect.OpenResultset(stra)
For i = 1 To porcentaje
  If Not rsta.EOF Then
    If lista_productos <> "" Then
        lista_productos = lista_productos & ","
    End If
    If Not rsta.EOF Then
        If i = 1 Then
          mdblPrimListProd = rsta.rdoColumns(0).Value
        End If
        lista_productos = lista_productos & rsta.rdoColumns(0).Value
        mdblUltListProd = rsta.rdoColumns(0).Value
        rsta.MoveNext
    End If
  End If
Next i
rsta.Close
Set rsta = Nothing
End Sub

Private Sub cmdactinvent_Click()
Dim strinsertar As String
Dim qryupdate As rdoQuery
Dim strupdate As String
Dim strinsert1030 As String
Dim qryinsert1030 As rdoQuery
Dim mensaje As String
Dim faltan As Boolean
Dim continuar As Boolean
Dim resultado As Variant
Dim rsta As rdoResultset
Dim stra As String
Dim i As Integer
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim strsel As String
Dim qrysel As rdoQuery
Dim rstsel As rdoResultset
Dim rstinventario As rdoResultset
Dim strinventario As String
Dim Ubicacion
Dim ExistBD
Dim Referencia

Screen.MousePointer = vbHourglass
continuar = False
faltan = False
objWinInfo.objWinActiveForm.blnChanged = False
'se mira en la variable <faltan> si alguna existencia real no se ha metido
GridInventario.MoveFirst
For i = 0 To GridInventario.Rows - 1
  If GridInventario.Columns("Exist.Reales").Value = "" Then
    faltan = True
    Exit For
  End If
  GridInventario.MoveNext
Next i
If faltan = True Then
    mensaje = MsgBox("Todas las existencias reales no est�n introducidas." & Chr(13) & _
                     "�Desea continuar?", vbYesNo)
    If mensaje = vbNo Then
        Screen.MousePointer = vbDefault
       Exit Sub
    Else
       continuar = True
    End If
End If
    
If continuar = True Or faltan = False Then
    GridInventario.MoveFirst
    For i = 0 To GridInventario.Rows - 1
        If GridInventario.Columns("Exist.Reales").Value <> "" And GridInventario.Columns("Exist.BD").Value <> "" Then
            If Not IsNumeric(GridInventario.Columns("Exist.Reales").Value) Then
              Call MsgBox("Las existencias reales contienen datos no num�ricos", vbInformation, "Aviso")
              Screen.MousePointer = vbDefault
              Exit Sub
            End If
            resultado = GridInventario.Columns("Exist.BD").Value - GridInventario.Columns("Exist.Reales").Value
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
               'strinsertar = "UPDATE FR4700 SET " & _
               '             "FR47ACUSALIDA=FR47ACUSALIDA+" & resultado & _
               '             " WHERE FR04CODALMACEN=" & GridInventario.Columns("C�digo Almac�n").Value & _
               '             " AND FR73CODPRODUCTO=" & GridInventario.Columns("C�digo Producto").Value
            strupdate = "UPDATE FR4700 SET " & _
                        "FR47ACUSALIDA=FR47ACUSALIDA + ? " & _
                        " WHERE FR04CODALMACEN=?" & _
                        " AND FR73CODPRODUCTO=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = Format(resultado, "###0.00")
            qryupdate(1) = GridInventario.Columns("C�digo Almac�n").Value
            qryupdate(2) = GridInventario.Columns("C�digo Producto").Value
            qryupdate.Execute
            'objApp.rdoConnect.Execute strinsertar, 64
            'objApp.rdoConnect.Execute "Commit", 64
            strupdate = "UPDATE FR7300_COPIA " & _
                        "   SET FR73EXISTENCIAS = ? , FR73IMPALMACEN = ? * FR73PRECMED" & _
                        " WHERE FR73CODPRODUCTO= ? "
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = GridInventario.Columns("Exist.Reales").Value
            qryupdate(1) = GridInventario.Columns("Exist.Reales").Value
            qryupdate(2) = GridInventario.Columns("C�digo Producto").Value
            qryupdate.Execute
            qryupdate.Close
            Set qryupdate = Nothing
        End If
        If GridInventario.Columns("Exist.Reales").Value <> "" And GridInventario.Columns("Exist.BD").Value <> "" Then
            'fecha actual
            strfecha = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
            Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
            'se mira si ya existe tupla en FRA500 para hacer insert o update
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
            'strsel = "SELECT COUNT(*) FROM FRA500 WHERE " & _
            '         "FR04CODALMACEN=" & GridInventario.Columns("C�digo Almac�n").Value & " AND " & _
            '         "SG02COD='" & objsecurity.strUser & "' AND " & _
            '         "FR73CODPRODUCTO=" & GridInventario.Columns("C�digo Producto").Value & " AND " & _
            '         "FRA5FECRECALE=" & "TO_DATE('" & rstfecha.rdoColumns(0).Value & "','DD/MM/YYYY HH24:MI')"
            strsel = "SELECT COUNT(*) FROM FRA500 WHERE " & _
                     "FR04CODALMACEN=? AND " & _
                     "SG02COD=? AND " & _
                     "FR73CODPRODUCTO=? AND " & _
                     "FRA5FECRECALE=TO_DATE(?,'DD/MM/YYYY HH24:MI')"
            Set qrysel = objApp.rdoConnect.CreateQuery("", strsel)
            qrysel(0) = GridInventario.Columns("C�digo Almac�n").Value
            qrysel(1) = objsecurity.strUser
            qrysel(2) = GridInventario.Columns("C�digo Producto").Value
            qrysel(3) = rstfecha.rdoColumns(0).Value
            Set rstsel = qrysel.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            'Set rstsel = objApp.rdoConnect.OpenResultset(strsel)
            If rstsel.rdoColumns(0).Value = 0 Then
'                strinsert1030 = "INSERT INTO FRA500 (FR04CODALMACEN,SG02COD,FR73CODPRODUCTO," & _
'                      "FRA5FECRECALE,FRA5EXISTBD,FRA5EXISTREAL)" & _
'                      " VALUES (" & _
'                      GridInventario.Columns("C�digo Almac�n").Value & "," & _
'                      "'" & objsecurity.strUser & "'" & "," & _
'                      GridInventario.Columns("C�digo Producto").Value & "," & _
'                      "TO_DATE('" & rstfecha.rdoColumns(0).Value & "','DD/MM/YYYY HH24:MI')" & "," & _
'                      GridInventario.Columns("Exist.BD").Value & "," & _
'                      GridInventario.Columns("Exist.Reales").Value & ")"
                      
                strinsert1030 = "INSERT INTO FRA500 (FR04CODALMACEN,SG02COD,FR73CODPRODUCTO," & _
                      "FRA5FECRECALE,FRA5EXISTBD,FRA5EXISTREAL)" & _
                       " values (?, ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI'), ?, ?)"
                Set qryinsert1030 = objApp.rdoConnect.CreateQuery("", strinsert1030)
                    qryinsert1030(0) = GridInventario.Columns("C�digo Almac�n").Value
                    qryinsert1030(1) = objsecurity.strUser
                    qryinsert1030(2) = GridInventario.Columns("C�digo Producto").Value
                    qryinsert1030(3) = rstfecha.rdoColumns(0).Value
                    qryinsert1030(4) = GridInventario.Columns("Exist.BD").Value
                    qryinsert1030(5) = GridInventario.Columns("Exist.Reales").Value
                 qryinsert1030.Execute
                 qryinsert1030.Close
                Set qryinsert1030 = Nothing
    
                    
            Else
'                strinsert1030 = "UPDATE FRA500 SET " & _
'                                "FRA5EXISTBD=" & GridInventario.Columns("Exist.BD").Value & "," & _
'                                "FRA5EXISTREAL=" & GridInventario.Columns("Exist.Reales").Value
              strinsert1030 = "Update FRA500 set FRA5EXISTBD = ?,"
              strinsert1030 = strinsert1030 & " FRA5EXISTREAL= ?"
              strinsert1030 = strinsert1030 & " WHERE " & _
                     "FR04CODALMACEN=" & GridInventario.Columns("C�digo Almac�n").Value & " AND " & _
                     "SG02COD='" & objsecurity.strUser & "' AND " & _
                     "FR73CODPRODUCTO=" & GridInventario.Columns("C�digo Producto").Value & " AND " & _
                     "FRA5FECRECALE=" & "TO_DATE('" & rstfecha.rdoColumns(0).Value & "','DD/MM/YYYY HH24:MI') AND FR04CODALMACEN=" & GridInventario.Columns("C�digo Almac�n").Value
                     
                Set qryinsert1030 = objApp.rdoConnect.CreateQuery("", strinsert1030)
                   qryinsert1030(0) = GridInventario.Columns("Exist.BD").Value
                   qryinsert1030(1) = GridInventario.Columns("Exist.Reales").Value
      
              qryinsert1030.Execute
              qryinsert1030.Close
              Set qryinsert1030 = Nothing
            End If
            rstsel.Close
            Set rstsel = Nothing
            rstfecha.Close
            Set rstfecha = Nothing
'            objApp.rdoConnect.Execute strinsert1030, 64
            objApp.rdoConnect.Execute "Commit", 64
       End If
     GridInventario.MoveNext
     Next i
End If
'RECUENTO TOTAL
If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    If Len(Trim(txtUbicacion.Text)) > 0 Then
      mstrUB = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FRH9UBICACION = '" & txtUbicacion.Text & "') "
    Else
      mstrUB = ""
    End If
    
    mstrCla = mstrCP & mstrCI & mstrPrd & mstrUB

'    mstrCla = mstrCP & mstrCI & mstrPrd
    
If Option2.Value = True Then
 If almacenes <> "" Then
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    strinventario = strinventario & "FR04CODALMACEN IN " & "(" & almacenes & ") " & mstrCla
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
 End If
End If
'RECUENTO ALEATORIO
If Option1.Value = True Then
 If almacenes <> "" And lista_productos <> "" Then
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    strinventario = strinventario & "FR04CODALMACEN IN " & "(" & almacenes & ")" & " AND (FR73CODPRODUCTO BETWEEN " & mdblPrimListProd & " AND " & mdblUltListProd & ") " & mstrCla
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
 End If
End If
'PORCENTAJE PUNTUAL
If Option3.Value = True And txtporcentaje.Text <> "" Then
 If almacenes <> "" And lista_productos <> "" Then
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    strinventario = strinventario & "FR04CODALMACEN IN " & "(" & almacenes & ")" & " AND (FR73CODPRODUCTO BETWEEN " & mdblPrimListProd & " AND " & mdblUltListProd & ") " & mstrCla
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
 End If
End If

 Screen.MousePointer = vbDefault
End Sub

Private Sub cmdejecutarrec_Click()
Dim resultado As Variant
Dim i As Integer
'se calcula el % de error

 Screen.MousePointer = vbHourglass
objWinInfo.objWinActiveForm.blnChanged = False
GridInventario.MoveFirst
For i = 0 To GridInventario.Rows - 1
  If GridInventario.Columns("Exist.Reales").Value <> "" And GridInventario.Columns("Exist.BD").Value <> "" Then
        resultado = (GridInventario.Columns("Exist.Reales").Value - GridInventario.Columns("Exist.BD").Value)
        If GridInventario.Columns("Exist.BD").Value <> 0 Then
            resultado = (resultado / GridInventario.Columns("Exist.BD").Value) * 100
        End If
        GridInventario.Columns("%Error Real").Value = Format(resultado, "###0.00") 's�lo 2 decimales

  End If
  GridInventario.MoveNext
Next i
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdrellenar_Click(Index As Integer)
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mvarfilatotal As Variant
Dim filas() As Integer
Dim filasordenadas() As Integer
Dim i As Integer
Dim j As Integer
Dim v As Integer
Dim max As Integer
Dim insertar As Integer
Dim rstinventario As rdoResultset
Dim strinventario As String
Dim Ubicacion
Dim ExistBD
Dim Referencia

Screen.MousePointer = vbHourglass
Select Case Index
Case 0
  insertar = 0
  'se mira que no se meta el mismo almac�n 2 veces
  Grid1(1).MoveFirst
  If Grid1(1).Rows > 0 Then
        For mintisel = 0 To Grid1(1).Rows - 1
            If Grid1(1).Columns(0).Value <> grdDBGrid1(0).Columns(3).Value Then
              insertar = 1
            Else
              insertar = 0
              Exit For
            End If
        Grid1(1).MoveNext
        Next mintisel
        If insertar = 1 Then
           insertar = 0
           Grid1(1).AddNew
           Grid1(1).Columns(0).Value = grdDBGrid1(0).Columns(3).Value
           Grid1(1).Columns(1).Value = grdDBGrid1(0).Columns(4).Value
           Grid1(1).Update
       End If
  Else
        Grid1(1).AddNew
        Grid1(1).Columns(0).Value = grdDBGrid1(0).Columns(3).Value
        Grid1(1).Columns(1).Value = grdDBGrid1(0).Columns(4).Value
        Grid1(1).Update
  End If
Case 1
  'se mira que no se meta el mismo almac�n 2 veces
  insertar = 0
  mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
  For mintisel = 0 To mintNTotalSelRows - 1
    Grid1(1).MoveFirst
    If Grid1(1).Rows > 0 Then
        For i = 0 To Grid1(1).Rows - 1
            mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
            If Grid1(1).Columns(0).Value <> grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk) Then
                insertar = 1
            Else
                insertar = 0
                Exit For
            End If
        Grid1(1).MoveNext
        Next i
        If insertar = 1 Then
                insertar = 0
                Grid1(1).AddNew
                Grid1(1).Columns(0).Value = grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk)
                Grid1(1).Columns(1).Value = grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk)
                Grid1(1).Update
        End If
    Else
        mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
        Grid1(1).AddNew
        Grid1(1).Columns(0).Value = grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk)
        Grid1(1).Columns(1).Value = grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk)
        Grid1(1).Update
    End If
  Next mintisel
Case 2
  If Grid1(1).Rows = 0 Or Grid1(1).Rows = 1 Then
    Grid1(1).RemoveAll
  Else
    mvarBkmrk = Grid1(1).Bookmark
    mvarfilatotal = Grid1(1).AddItemRowIndex(mvarBkmrk)
    Call Grid1(1).RemoveItem(mvarfilatotal)
  End If
Case 3
        mintNTotalSelRows = Grid1(1).SelBookmarks.Count
        ReDim filas(mintNTotalSelRows)
        ReDim filasordenadas(mintNTotalSelRows)
        For mintisel = 0 To mintNTotalSelRows - 1
          mvarBkmrk = Grid1(1).SelBookmarks(mintisel)
          mvarfilatotal = Grid1(1).AddItemRowIndex(mvarBkmrk)
          filas(mintisel) = mvarfilatotal
        Next mintisel
        'se ordena el array <filas> de mayor a menor en el array <filasordenadas>
        For j = 0 To mintNTotalSelRows - 1
            max = filas(0)
            For i = 0 To mintNTotalSelRows - 1
                If filas(i) >= max Then
                    max = filas(i)
                    v = i
                End If
            Next i
            filas(v) = 0
            filasordenadas(j) = max
        Next j
        For mintisel = 0 To mintNTotalSelRows - 1
          Call Grid1(1).RemoveItem(filasordenadas(mintisel))
        Next mintisel
End Select

'se meten en una lista los c�digos de los almacenes seleccionados para refrescar
'el grid de inventario
almacenes = ""
Grid1(1).MoveFirst
If Grid1(1).Rows = 0 Then
    GridInventario.RemoveAll
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Screen.MousePointer = vbDefault
    Exit Sub
End If

If Grid1(1).Rows = 1 Then
    almacenes = almacenes & Grid1(1).Columns(0).Value
Else
    almacenes = almacenes & Grid1(1).Columns(0).Value
    Grid1(1).MoveNext
    For i = 1 To Grid1(1).Rows - 1
        almacenes = almacenes & "," & Grid1(1).Columns(0).Value
        Grid1(1).MoveNext
    Next i
End If


'RECUENTO TOTAL
If Option2.Value = True Then
    If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('%" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    If Len(Trim(txtUbicacion.Text)) > 0 Then
      mstrUB = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FRH9UBICACION = '" & txtUbicacion.Text & "') "
    Else
      mstrUB = ""
    End If
    
    mstrCla = mstrCP & mstrCI & mstrPrd & mstrUB
    
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    strinventario = strinventario & "FR4701J_COPIA.FR04CODALMACEN IN " & "(" & almacenes & ")" & mstrCla
    strinventario = strinventario & " ORDER BY FR4701J_COPIA.FR04CODALMACEN,FR4701J_COPIA.FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
'RECUENTO ALEATORIO
Dim indice As Integer
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim stra As String
Dim qryprod As rdoQuery
Dim rstprod As rdoResultset
Dim strprod As String

If Option1.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 Then
    Grid1(1).MoveFirst
    For indice = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira el % de productos a examinar
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT FR04TPEXAMRECALE FROM FR0400 WHERE FR04CODALMACEN=" & _
      '     Grid1(1).Columns(0).Value
      stra = "SELECT FR04TPEXAMRECALE FROM FR0400 WHERE FR04CODALMACEN=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = Grid1(1).Columns(0).Value
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      'para cada almac�n se mira cu�ntos productos tiene
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
      '     Grid1(1).Columns(0).Value
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=?"
      Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)
      qryprod(0) = Grid1(1).Columns(0).Value
      Set rstprod = qryprod.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, rsta.rdoColumns(0).Value, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next indice
 
    'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    mstrCla = mstrCP & mstrCI & mstrPrd
    
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    If lista_productos <> "" Then
        strinventario = strinventario & "(FR73CODPRODUCTO BETWEEN " & mdblPrimListProd & " AND " & mdblUltListProd & ") AND FR04CODALMACEN IN " & "(" & almacenes & ")" & mstrCla
    Else
        strinventario = strinventario & "FR04CODALMACEN IS NULL " & mstrCla
    End If
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
  
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End If

'PORCENTAJE PUNTUAL
Dim k As Integer
Dim strporcentaje As String

If Option3.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 And txtporcentaje.Text <> "" Then
    'se concatena la parte entera y la decimal del porcentaje introducido
    If txtporcentaje.Text <> "" Then
        strporcentaje = txtporcentaje.Text
        If txtdecimales.Text <> "" Then
            strporcentaje = strporcentaje & "." & txtdecimales.Text
        End If
    End If
    Grid1(1).MoveFirst
    For k = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira cu�ntos productos tiene
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
      '     Grid1(1).Columns(0).Value
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=?"
      Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)
      qryprod(0) = Grid1(1).Columns(0).Value
      Set rstprod = qryprod.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, strporcentaje, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next k
 
    'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    mstrCla = mstrCP & mstrCI & mstrPrd
    
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    If lista_productos <> "" Then
        strinventario = strinventario & "(FR73CODPRODUCTO BETWEEN " & mdblPrimListProd & " AND " & mdblUltListProd & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ")" & mstrCla
    Else
        strinventario = strinventario & "FR04CODALMACEN IS NULL " & mstrCla
    End If
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
    
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
'si no hay ning�n porcentaje introducido que se limpie el grid
If txtporcentaje.Text = "" Then
  If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    mstrCla = mstrCP & mstrCI & mstrPrd
    
    GridInventario.RemoveAll
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If

End If
 Screen.MousePointer = vbDefault
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Almacenes"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR0400"
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("FR04CODALMACEN", cwAscending)
        Call .objPrinter.Add("FRR999", "Realizar Inventario_COPIA")
        Call .objPrinter.Add("FRR998", "Maestro por Cuenta Contable_COPIA")

    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Almacenes")
        Call .FormAddFilterWhere(strKey, "FR04CODALMACEN", "C�digo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR04DESALMACEN", "Descripci�n", cwString)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR04CODALMACEN", "C�digo")
        Call .FormAddFilterOrder(strKey, "FR04DESALMACEN", "Descripci�n")
    
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
'        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo", "FR04CODALMACEN", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Descripci�n", "FR04DESALMACEN", cwString, 30)
  
        Call .FormCreateInfo(objMultiInfo)
    
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
Call GridInventario.Columns.Add(0)
GridInventario.Columns(0).Visible = False
GridInventario.Columns(0).Caption = "C�digo Almac�n"
GridInventario.Columns(0).Locked = True
Call GridInventario.Columns.Add(1)
GridInventario.Columns(1).Visible = True
GridInventario.Columns(1).Caption = "Almac�n"
GridInventario.Columns(1).Locked = True
GridInventario.Columns(1).Width = 2000
Call GridInventario.Columns.Add(2)
GridInventario.Columns(2).Visible = False
GridInventario.Columns(2).Caption = "C�digo Producto"
GridInventario.Columns(2).Locked = True
Call GridInventario.Columns.Add(3)
GridInventario.Columns(3).Visible = True
GridInventario.Columns(3).Caption = "Producto"
GridInventario.Columns(3).Locked = True
GridInventario.Columns(3).Width = 4600
''''''''''''''''''''''''''''''''''''''''''''
Call GridInventario.Columns.Add(4)
GridInventario.Columns(4).Visible = True
GridInventario.Columns(4).Caption = "Referencia"
GridInventario.Columns(4).Locked = True
GridInventario.Columns(4).Width = 1300
''''''''''''''''''''''''''''''''''''''''''''
Call GridInventario.Columns.Add(5)
GridInventario.Columns(5).Visible = True
GridInventario.Columns(5).Caption = "Ubicaci�n"
GridInventario.Columns(5).Locked = True
GridInventario.Columns(5).Width = 850
Call GridInventario.Columns.Add(6)
GridInventario.Columns(6).Visible = True
GridInventario.Columns(6).Caption = "Exist.BD"
GridInventario.Columns(6).Locked = True
GridInventario.Columns(6).Width = 1100
Call GridInventario.Columns.Add(7)
GridInventario.Columns(7).Visible = True
GridInventario.Columns(7).Caption = "Exist.Reales"
GridInventario.Columns(7).Locked = False
GridInventario.Columns(7).BackColor = &HFFC0FF
GridInventario.Columns(7).Width = 1100

grdDBGrid1(0).Columns(3).Visible = False

'se refresca el grid de almacenes
Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
objWinInfo.DataRefresh

tlbToolbar1.Buttons(6).Enabled = True

End Sub



Private Sub GridInventario_KeyPress(KeyAscii As Integer)
 Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), _
         Asc("7"), Asc("8"), Asc("9"), Asc("-"), Asc(","), 8
    Case Else
      KeyAscii = 0
 End Select
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)

Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean

Call objWinInfo.FormPrinterDialog(True, "")
Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
intReport = objPrinter.Selected
If intReport > 0 Then
  Select Case intReport
    Case 1
      Call Imprimir("FRR999.rpt", 0)
    Case 2
      Call Imprimir("FRR998.RPT", 0)
  End Select
End If
Set objPrinter = Nothing
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub



Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

Private Sub Option1_Click()
'Recuento Aleatorio
Dim i As Integer
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim stra As String
Dim qryprod As rdoQuery
Dim rstprod As rdoResultset
Dim strprod As String
Dim rstinventario As rdoResultset
Dim strinventario As String
Dim Ubicacion
Dim ExistBD
Dim Referencia

txtporcentaje.Visible = False
txtdecimales.Visible = False
lblcoma.Visible = False
If Option1.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 Then
    Grid1(1).MoveFirst
    For i = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira el % de productos a examinar
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT FR04TPEXAMRECALE FROM FR0400 WHERE FR04CODALMACEN=" & _
      '     Grid1(1).Columns(0).Value
      stra = "SELECT FR04TPEXAMRECALE FROM FR0400 WHERE FR04CODALMACEN=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = Grid1(1).Columns(0).Value
      Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      'para cada almac�n se mira cu�ntos productos tiene
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
      '     Grid1(1).Columns(0).Value
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=?"
      Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)
      qryprod(0) = Grid1(1).Columns(0).Value
      Set rstprod = qryprod.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, rsta.rdoColumns(0).Value, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next i
 
    'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    mstrCla = mstrCP & mstrCI & mstrPrd
    
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    If lista_productos <> "" Then
        strinventario = strinventario & "(FR73CODPRODUCTO BETWEEN " & mdblPrimListProd & " AND " & mdblUltListProd & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ") " & mstrCla
    Else
        strinventario = strinventario & "FR04CODALMACEN IS NULL" & mstrCla
    End If
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
    
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End If
End Sub

Private Sub Option2_Click()
Dim rstinventario As rdoResultset
Dim strinventario As String
Dim Ubicacion
Dim ExistBD
Dim Referencia

'Recuento total
txtporcentaje.Visible = False
txtdecimales.Visible = False
lblcoma.Visible = False
If Option2.Value = True Then
    'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    mstrCla = mstrCP & mstrCI & mstrPrd
    
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    If almacenes <> "" Then
        strinventario = strinventario & "FR04CODALMACEN IN " & "(" & almacenes & ") " & mstrCla
    Else
        strinventario = strinventario & "FR04CODALMACEN IS NULL " & mstrCla
    End If
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End Sub

Private Sub Option3_Click()
Dim rstinventario As rdoResultset
Dim strinventario As String
Dim Ubicacion
Dim ExistBD
Dim Referencia

'Porcentaje Puntual. El usuario introduce el porcentaje que quiera en txtporcentaje y
'txtdecimales y as� no se coge por defecto el de la definici�n.
Dim i As Integer
Dim qryprod As rdoQuery
Dim rstprod As rdoResultset
Dim strprod As String
Dim strporcentaje As String

If Option3.Value = True Then
    txtporcentaje.Visible = True
    txtdecimales.Visible = True
    lblcoma.Visible = True
Else
    txtporcentaje.Visible = False
    txtdecimales.Visible = False
    lblcoma.Visible = False
End If

If Option3.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 And txtporcentaje.Text <> "" Then
    'se concatena la parte entera y la decimal del porcentaje introducido
    If txtporcentaje.Text <> "" Then
        strporcentaje = txtporcentaje.Text
        If txtdecimales.Text <> "" Then
            strporcentaje = strporcentaje & "." & txtdecimales.Text
        End If
    End If
    Grid1(1).MoveFirst
    For i = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira cu�ntos productos tiene
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
      '     Grid1(1).Columns(0).Value
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=?"
      Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)
      qryprod(0) = Grid1(1).Columns(0).Value
      Set rstprod = qryprod.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, strporcentaje, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next i
 
    'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    mstrCla = mstrCP & mstrCI & mstrPrd
    

    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    If lista_productos <> "" Then
        strinventario = strinventario & "(FR73CODPRODUCTO BETWEEN " & mdblPrimListProd & " AND " & mdblUltListProd & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ") " & mstrCla
    Else
        strinventario = strinventario & "FR04CODALMACEN IS NULL " & mstrCla
    End If
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
    
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
'si no hay ning�n porcentaje introducido que se limpie el grid
If txtporcentaje.Text = "" Then
    GridInventario.RemoveAll
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If

End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
objWinInfo.objWinActiveForm.blnChanged = False
'<>Guardar
If btnButton.Index <> 4 Then
    If Me.ActiveControl.Name = "Grid1" Then
      If Grid1(1).Rows > 0 Then
        If btnButton.Index = 21 Then
          Grid1(1).MoveFirst
        End If
        If btnButton.Index = 22 Then
          Grid1(1).MovePrevious
        End If
        If btnButton.Index = 23 Then
          Grid1(1).MoveNext
        End If
        If btnButton.Index = 24 Then
          Grid1(1).MoveLast
        End If
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
Else
    MsgBox "Para guardar el inventario pulse <Actualizar Inventario>", vbInformation
End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

  objWinInfo.objWinActiveForm.blnChanged = False
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(16)) 'Localizar
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21)) 'Primero
  Case 50
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22)) 'Anterior
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23)) 'Siguiente
  Case 70
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24)) 'Ultimo
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    Call objWinInfo.CtrlGotFocus
If intIndex = 2 Then
    GridInventario.Columns("C�digo Almac�n").Locked = True
    GridInventario.Columns("Almac�n").Locked = True
    GridInventario.Columns("C�digo Producto").Locked = True
    GridInventario.Columns("Producto").Locked = True
    GridInventario.Columns("Ubicaci�n").Locked = True
    GridInventario.Columns("Exist.BD").Locked = True
    GridInventario.Columns("%Error m�x").Locked = True
    GridInventario.Columns("%Error Real").Locked = True
End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    Call objWinInfo.GridDblClick
If intIndex = 2 Then
    GridInventario.Columns("C�digo Almac�n").Locked = True
    GridInventario.Columns("Almac�n").Locked = True
    GridInventario.Columns("C�digo Producto").Locked = True
    GridInventario.Columns("Producto").Locked = True
    GridInventario.Columns("Ubicaci�n").Locked = True
    GridInventario.Columns("Exist.BD").Locked = True
    GridInventario.Columns("%Error m�x").Locked = True
    GridInventario.Columns("%Error Real").Locked = True
End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
If intIndex = 2 Then
    GridInventario.Columns("C�digo Almac�n").Locked = True
    GridInventario.Columns("Almac�n").Locked = True
    GridInventario.Columns("C�digo Producto").Locked = True
    GridInventario.Columns("Producto").Locked = True
    GridInventario.Columns("Ubicaci�n").Locked = True
    GridInventario.Columns("Exist.BD").Locked = True
    GridInventario.Columns("%Error m�x").Locked = True
    GridInventario.Columns("%Error Real").Locked = True
End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    objWinInfo.objWinActiveForm.blnChanged = False
    Call objWinInfo.CtrlDataChange
If intIndex = 2 Then
    GridInventario.Columns("C�digo Almac�n").Locked = True
    GridInventario.Columns("Almac�n").Locked = True
    GridInventario.Columns("C�digo Producto").Locked = True
    GridInventario.Columns("Producto").Locked = True
    GridInventario.Columns("Ubicaci�n").Locked = True
    GridInventario.Columns("Exist.BD").Locked = True
    GridInventario.Columns("%Error m�x").Locked = True
    GridInventario.Columns("%Error Real").Locked = True
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
If intIndex = 1 Then
    Exit Sub
End If
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


Private Sub txtdecimales_Change()
If txtdecimales.Text <> "" Then
    If Not IsNumeric(txtdecimales.Text) Then
        Call MsgBox("Datos no num�ricos", vbCritical, "Aviso")
        txtdecimales.Text = ""
    End If
End If
End Sub

Private Sub txtdecimales_KeyPress(KeyAscii As Integer)
'Porcentaje Puntual. El usuario introduce el porcentaje que quiera en txtporcentaje y
'txtdecimales y as� no se coge por defecto el de la definici�n.
Dim i As Integer
Dim qryprod As rdoQuery
Dim rstprod As rdoResultset
Dim strprod As String
Dim strporcentaje As String
Dim rstinventario As rdoResultset
Dim strinventario As String
Dim Ubicacion
Dim ExistBD
Dim Referencia

If KeyAscii = 13 Then   'si se pulsa Return
If Option3.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 And txtporcentaje.Text <> "" Then
    'se concatena la parte entera y la decimal del porcentaje introducido
    If txtporcentaje.Text <> "" Then
        strporcentaje = txtporcentaje.Text
        If txtdecimales.Text <> "" Then
            strporcentaje = strporcentaje & "." & txtdecimales.Text
        End If
    End If
    Grid1(1).MoveFirst
    For i = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira cu�ntos productos tiene
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
      '     Grid1(1).Columns(0).Value
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=?"
      Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)
      qryprod(0) = Grid1(1).Columns(0).Value
      Set rstprod = qryprod.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, strporcentaje, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next i
 
    'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    mstrCla = mstrCP & mstrCI & mstrPrd
    

    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    If lista_productos <> "" Then
        strinventario = strinventario & "(FR73CODPRODUCTO BETWEEN " & mdblPrimListProd & " AND " & mdblUltListProd & ")" & " AND FR04CODALMACEN IN " & "(" & almacenes & ") " & mstrCla
    Else
        strinventario = strinventario & "FR04CODALMACEN IS NULL"
    End If
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End If
End If
End Sub

Private Sub txtporcentaje_Change()
If txtporcentaje.Text <> "" Then
    If Not IsNumeric(txtporcentaje.Text) Then
        Call MsgBox("Datos no num�ricos", vbCritical, "Aviso")
        txtporcentaje.Text = ""
    End If
End If
End Sub



Private Sub txtporcentaje_KeyPress(KeyAscii As Integer)

'Porcentaje Puntual. El usuario introduce el porcentaje que quiera en txtporcentaje y
'txtdecimales y as� no se coge por defecto el de la definici�n.
Dim i As Integer
Dim qryprod As rdoQuery
Dim rstprod As rdoResultset
Dim strprod As String
Dim strporcentaje As String
Dim rstinventario As rdoResultset
Dim strinventario As String
Dim Ubicacion
Dim ExistBD
Dim Referencia

If KeyAscii = 13 Then   'si se pulsa Return
If Option3.Value = True Then
 lista_productos = ""
 If Grid1(1).Rows > 0 And txtporcentaje.Text <> "" Then
    'se concatena la parte entera y la decimal del porcentaje introducido
    If txtporcentaje.Text <> "" Then
        strporcentaje = txtporcentaje.Text
        If txtdecimales.Text <> "" Then
            strporcentaje = strporcentaje & "." & txtdecimales.Text
        End If
    End If
    Grid1(1).MoveFirst
    For i = 0 To Grid1(1).Rows - 1
      'para cada almac�n se mira cu�ntos productos tiene
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=" & _
      '     Grid1(1).Columns(0).Value
      strprod = "SELECT count(*) FROM FR4700 WHERE FR04CODALMACEN=?"
      Set qryprod = objApp.rdoConnect.CreateQuery("", strprod)
      qryprod(0) = Grid1(1).Columns(0).Value
      Set rstprod = qryprod.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
      'se llama a la funci�n Recuento_Aleatorio
      Call Recuento_Aleatorio(Grid1(1).Columns(0).Value, strporcentaje, rstprod.rdoColumns(0).Value)
      Grid1(1).MoveNext
    Next i
 
    'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    If Len(Trim(txtCodProv.Text)) > 0 Then
      mstrCP = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR79CODPROVEEDOR_A = " & txtCodProv.Text & ") "
    Else
      mstrCP = " "
    End If
    If Len(Trim(txtCodInt.Text)) > 0 Then
      mstrCI = " AND FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR = " & txtCodInt.Text & ") "
    Else
      mstrCI = " "
    End If
    If Len(Trim(txtProd.Text)) > 0 Then
      mstrPrd = " AND UPPER(FR73DESPRODUCTO) LIKE UPPER('" & txtProd.Text & "%')"
    Else
      mstrPrd = " "
    End If
    mstrCla = mstrCP & mstrCI & mstrPrd
    
    strinventario = "SELECT * FROM FR4701J_COPIA WHERE "
    If lista_productos <> "" Then
        strinventario = strinventario & "(FR73CODPRODUCTO BETWEEN " & mdblPrimListProd & " AND " & mdblUltListProd & ") AND FR04CODALMACEN IN " & "(" & almacenes & ") " & mstrCla
    Else
        strinventario = strinventario & "FR04CODALMACEN IS NULL"
    End If
    strinventario = strinventario & " ORDER BY FR04CODALMACEN,FR73DESPRODUCTO"
    Set rstinventario = objApp.rdoConnect.OpenResultset(strinventario)
    GridInventario.RemoveAll
    While Not rstinventario.EOF
      If Not IsNull(rstinventario.rdoColumns("FR47UBICACION").Value) Then
        Ubicacion = rstinventario.rdoColumns("FR47UBICACION").Value
      Else
        Ubicacion = " "
      End If
      If Not IsNull(rstinventario.rdoColumns("EXISTENCIAS_BD").Value) Then
        ExistBD = rstinventario.rdoColumns("EXISTENCIAS_BD").Value
      Else
        ExistBD = ""
      End If
      If Not IsNull(rstinventario.rdoColumns("FR73REFERENCIA").Value) Then
        Referencia = rstinventario.rdoColumns("FR73REFERENCIA").Value
      Else
        Referencia = ""
      End If
      
      GridInventario.AddItem rstinventario.rdoColumns("FR04CODALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR04DESALMACEN").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstinventario.rdoColumns("PRODUCTO").Value & Chr(vbKeyTab) & _
                   Referencia & Chr(vbKeyTab) & _
                   Ubicacion & Chr(vbKeyTab) & _
                   ExistBD & Chr(vbKeyTab) & "" & Chr(vbKeyTab)

    rstinventario.MoveNext
    Wend
    rstinventario.Close
    Set rstinventario = Nothing
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End If
End If
End If
End Sub


'*************************************************************
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strAlmacenes As String
  Dim nTotal As Long
  Dim nTotalSelRows As Integer
  Dim i As Integer
  Dim bkmrk As Variant
  Dim rsta As rdoResultset
  Dim stra As String
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  strAlmacenes = ""
  nTotalSelRows = Grid1(1).Rows
  If nTotalSelRows > 0 Then
    Grid1(1).MoveFirst
    strAlmacenes = Grid1(1).Columns(0).Value
    For i = 1 To nTotalSelRows - 1
      Grid1(1).MoveNext
      strAlmacenes = strAlmacenes & "," & Grid1(1).Columns(0).Value
    Next i
  End If

  CrystalReport1.ReportFileName = strPATH & strListado
  
  strWhere = "-1=-1"
  
  
  If UCase(strListado) = "FRR999.RPT" Then
      'Proveedor
      If Len(Trim(txtCodProv.Text)) > 0 Then
        strWhere = strWhere & " AND {FR4701J_COPIA.FR79CODPROVEEDOR_A}= " & txtCodProv.Text
      End If
      'C�digo Interno
      If Len(Trim(txtCodInt.Text)) > 0 Then
        stra = "SELECT FR73CODPRODUCTO FROM FR7300_COPIA WHERE FR73CODINTFAR ='" & txtCodInt.Text & "'"
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If Not rsta.EOF Then
          If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
          strWhere = strWhere & " AND {FR4701J_COPIA.FR73CODPRODUCTO}=" & rsta.rdoColumns("FR73CODPRODUCTO").Value
          End If
        End If
      End If
      'Descripci�n
      If Len(Trim(txtProd.Text)) > 0 Then
        strWhere = strWhere & " AND {FR4701J_COPIA.FR73DESPRODUCTO} startswith " & Chr(34) & txtProd.Text & Chr(34)
      End If
      
      If strAlmacenes <> "" Then
          strWhere = strWhere & " AND " & _
                CrearWhere("{FR4701J_COPIA.FR04CODALMACEN}", "(" & strAlmacenes & ")")
      Else
          strWhere = strWhere
      End If
    End If
 CrystalReport1.SelectionFormula = strWhere
  'On Error GoTo Err_imp1
  CrystalReport1.Action = 1
Err_imp1:
  
End Sub

Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function

