VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmImprimirEtiquetasProductos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Imprimir Etiquetas"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Filtrar B�squeda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004000&
      Height          =   1695
      Left            =   240
      TabIndex        =   9
      Top             =   600
      Width           =   9135
      Begin VB.CommandButton cmdfiltrar 
         Caption         =   "Filtrar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6840
         TabIndex        =   16
         Top             =   840
         Width           =   1815
      End
      Begin VB.TextBox txtReferencia 
         Height          =   315
         Left            =   240
         MaxLength       =   15
         TabIndex        =   14
         ToolTipText     =   "C�d. Producto"
         Top             =   1200
         Width           =   1815
      End
      Begin VB.TextBox txtdescripcion 
         Height          =   315
         Left            =   1680
         MaxLength       =   50
         TabIndex        =   12
         ToolTipText     =   "DEscripci�n"
         Top             =   600
         Width           =   4455
      End
      Begin VB.TextBox txtinterno 
         Height          =   315
         Left            =   240
         MaxLength       =   6
         TabIndex        =   10
         ToolTipText     =   "C�d. Producto"
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Referencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label lbldescripcion 
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Left            =   1680
         TabIndex        =   13
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label2 
         Caption         =   "C�d. Producto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   360
         Width           =   1455
      End
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   375
      Left            =   10680
      TabIndex        =   8
      Top             =   1440
      Visible         =   0   'False
      Width           =   735
      _Version        =   196608
      _ExtentX        =   1296
      _ExtentY        =   661
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1413783674
   End
   Begin VB.CommandButton cmda�adir 
      Caption         =   "->"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6150
      TabIndex        =   5
      Top             =   4680
      Width           =   375
   End
   Begin VB.Frame Frame1 
      Caption         =   "Productos Seleccionados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   5055
      Left            =   6600
      TabIndex        =   4
      Top             =   2400
      Width           =   5175
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Height          =   4575
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   4980
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   5
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2117
         Columns(0).Caption=   "C�d.Prod"
         Columns(0).Name =   "C�d.Prod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1402
         Columns(1).Caption=   "N�Copias"
         Columns(1).Name =   "N�Copias"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1323
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "C�digo"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   5371
         Columns(3).Caption=   "Desc.Corta"
         Columns(3).Name =   "Desc.Corta"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Caption=   "DigitoSeguridad"
         Columns(4).Name =   "DigitoSeguridad"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   8784
         _ExtentY        =   8070
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir Etiquetas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8400
      TabIndex        =   3
      Top             =   7560
      Width           =   2175
   End
   Begin VB.Frame frame2 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5535
      Left            =   120
      TabIndex        =   2
      Top             =   2400
      Width           =   5985
      Begin SSDataWidgets_B.SSDBGrid Gridmed 
         Height          =   5055
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   5700
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   6
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   2090
         Columns(0).Caption=   "Codigo"
         Columns(0).Name =   "Codigo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1588
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "Interno"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2223
         Columns(2).Caption=   "Referencia"
         Columns(2).Name =   "Referencia"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   7170
         Columns(3).Caption=   "Descripci�n"
         Columns(3).Name =   "Descripci�n"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3863
         Columns(4).Caption=   "Desc.Corta"
         Columns(4).Name =   "Descripci�n Corta"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   2514
         Columns(5).Caption=   "DigitoSeguridad"
         Columns(5).Name =   "DigitoSeguridad"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         _ExtentX        =   10054
         _ExtentY        =   8916
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmImprimirEtiquetasProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmImprimirEtiquetasProductos(FR0082.frm)                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: AGOSTO DEL 2000                                               *
'* DESCRIPCION: Imprimir Etiquetas                                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1



Private Sub cmda�adir_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
    
cmda�adir.Enabled = False
mintNTotalSelRows = Gridmed.SelBookmarks.Count
For mintisel = 0 To mintNTotalSelRows - 1
  mvarBkmrk = Gridmed.SelBookmarks(mintisel)
  'c�d.producto,c�d.interno,descripci�n corta y d�gito de seguridad
  SSDBGrid1.AddItem Gridmed.Columns(0).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Chr(vbKeyTab) & _
                    Gridmed.Columns(1).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(4).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(5).CellValue(mvarBkmrk)
                    
Next mintisel

cmda�adir.Enabled = True
End Sub



Private Sub SSDBGrid1_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = 0
End Sub

Private Sub SSDBGrid1_KeyPress(KeyAscii As Integer)
If SSDBGrid1.Col = 1 Then
 Select Case KeyAscii
    Case 8, Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case Else
      KeyAscii = 0
 End Select
End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdImprimir_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String
Dim rsta As rdoResultset
Dim stra As String
Dim strupdate As String
Dim Num$

cmdImprimir.Enabled = False

mintNTotalSelRows = SSDBGrid1.Rows
gintprodtotal = mintNTotalSelRows
SSDBGrid1.MoveFirst
For mintisel = 0 To mintNTotalSelRows - 1
    'Guardar Descripci�n Corta
    If Len(SSDBGrid1.Columns("Desc.Corta").CellValue(mvarBkmrk)) > 20 Then
      Call MsgBox("La descripci�n corta es demasiado larga. M�ximo 20 caracteres.", vbInformation, "Aviso")
      cmdImprimir.Enabled = True
      Exit Sub
    End If
    If Trim(SSDBGrid1.Columns("N�Copias").CellValue(mvarBkmrk)) = "" Then
      Call MsgBox("Debe especificar el n�mero de copias para cada etiqueta.", vbInformation, "Aviso")
      cmdImprimir.Enabled = True
      Exit Sub
    End If
    strupdate = "UPDATE FR7300 SET FR73DESPRODUCTOETIQ=" & "'" & SSDBGrid1.Columns("Desc.Corta").CellValue(mvarBkmrk) & "'"
    strupdate = strupdate & " WHERE FR73CODPRODUCTO=" & SSDBGrid1.Columns(0).CellValue(mvarBkmrk)
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "commit", 64
    'Imprimir Etiquetas
    Num = "001" & SSDBGrid1.Columns("C�digo").CellValue(mvarBkmrk) & SSDBGrid1.Columns("DigitoSeguridad").CellValue(mvarBkmrk)
    Call ImprimirEtiqueta1(Num, SSDBGrid1.Columns("N�Copias").CellValue(mvarBkmrk), SSDBGrid1.Columns("Desc.Corta").CellValue(mvarBkmrk))
    Call ImprimirEtiquetaDeSeparacion

SSDBGrid1.MoveNext
Next mintisel

cmdImprimir.Enabled = True

End Sub
Private Sub Guardar_Descripcion_Corta()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim strupdate As String

mintNTotalSelRows = SSDBGrid1.Rows
gintprodtotal = mintNTotalSelRows
SSDBGrid1.MoveFirst
For mintisel = 0 To mintNTotalSelRows - 1
  strupdate = "UPDATE FR7300 SET FR73DESPRODUCTO=" & "'" & SSDBGrid1.Columns("Desc.Corta").CellValue(mvarBkmrk) & "'"
  strupdate = strupdate & " WHERE FR73CODPRODUCTO=" & SSDBGrid1.Columns(0).CellValue(mvarBkmrk)
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "commit", 64
SSDBGrid1.MoveNext
Next mintisel
End Sub

Private Sub cmdfiltrar_Click()
Dim strclausulawhere As String
Dim rsta As rdoResultset
Dim stra As String
Dim strclausulafiltro As String
Dim rstclausulafiltro As rdoResultset

On Error GoTo error_filtrar:

Screen.MousePointer = vbHourglass
cmdfiltrar.Enabled = False
Me.Enabled = False

strclausulawhere = "-1=-1"

If Trim(txtinterno.Text) <> "" Then
  strclausulawhere = strclausulawhere & " AND  FR73CODINTFAR=" & "'" & txtinterno.Text & "'"
End If
If Trim(txtdescripcion.Text) <> "" Then
  strclausulawhere = strclausulawhere & " AND UPPER(FR73DESPRODUCTO) like UPPER('" & txtdescripcion.Text & "%')"
End If
If Trim(txtReferencia.Text) <> "" Then
  strclausulawhere = strclausulawhere & " AND UPPER(FR73REFERENCIA) like UPPER('" & txtReferencia.Text & "%')"
End If

strclausulafiltro = "SELECT * FROM FR7300 WHERE " & strclausulawhere
strclausulafiltro = strclausulafiltro & " AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL) AND "
strclausulafiltro = strclausulafiltro & "((FR73FECFINVIG IS NULL) OR (FR73FECFINVIG>(SELECT SYSDATE FROM DUAL)))"
strclausulafiltro = strclausulafiltro & " ORDER BY FR73DESPRODUCTO ASC"

Set rstclausulafiltro = objApp.rdoConnect.OpenResultset(strclausulafiltro)
Gridmed.RemoveAll

While Not rstclausulafiltro.EOF
  Gridmed.AddItem rstclausulafiltro.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                  rstclausulafiltro.rdoColumns("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
                  rstclausulafiltro.rdoColumns("FR73REFERENCIA").Value & Chr(vbKeyTab) & _
                  rstclausulafiltro.rdoColumns("FR73DESPRODUCTO").Value & Chr(vbKeyTab) & rstclausulafiltro.rdoColumns("FR73DESPRODUCTOETIQ").Value & Chr(vbKeyTab) & rstclausulafiltro.rdoColumns("FR73CODINTFARSEG").Value
rstclausulafiltro.MoveNext
Wend
rstclausulafiltro.Close
Set rstclausulafiltro = Nothing

Me.Enabled = True
cmdfiltrar.Enabled = True
Screen.MousePointer = vbDefault
Exit Sub

error_filtrar:
MsgBox "Error al filtrar, int�ntelo de nuevo." & Chr(13) & "  Error: " & Err.Number & " " & Err.Description, vbInformation, "Aviso"
Err.Clear
Me.Enabled = True
cmdfiltrar.Enabled = True

End Sub

Private Sub Form_Activate()
Call Inicializar_Toolbar
Call Inicializar_Grid
txtinterno.SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

Gridmed.Columns(0).Visible = False 'c�d.producto
SSDBGrid1.Columns(0).Visible = False
Gridmed.Columns("DigitoSeguridad").Visible = False
SSDBGrid1.Columns("DigitoSeguridad").Visible = False

Gridmed.Columns(1).Width = 700
Gridmed.Columns(2).Width = 1200
Gridmed.Columns(3).Width = 3300


End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Select Case btnButton.Index
  Case 30:
          Unload Me
End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If intIndex = 100 Then
  Unload Me
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub



Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(21).Enabled = False
   tlbToolbar1.Buttons.Item(22).Enabled = False
   tlbToolbar1.Buttons.Item(23).Enabled = False
   tlbToolbar1.Buttons.Item(24).Enabled = False
   tlbToolbar1.Buttons.Item(26).Enabled = False
   
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuRegistroOpcion(40).Enabled = False
   mnuRegistroOpcion(50).Enabled = False
   mnuRegistroOpcion(60).Enabled = False
   mnuRegistroOpcion(70).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub
Private Sub Inicializar_Grid()
Dim i As Integer

For i = 0 To 4
  Gridmed.Columns(i).Locked = True
Next i

For i = 2 To 2
  SSDBGrid1.Columns(i).Locked = True
Next i

End Sub

Private Sub pSelectImpresoraEtiquetas()
    Dim strImpresora As String
    Dim strImpresoraEtiq As String
    Dim i As Integer
    If vsPrinter1.Device <> "Bandit" Then
        ' Se busca la impresora llamada 'Bandit'
        For i = 0 To vsPrinter1.NDevices - 1
            strImpresora = vsPrinter1.Devices(i)
            If InStr(UCase(strImpresora), "BANDIT") > 0 Then
                strImpresoraEtiq = strImpresora
                Exit For
            End If
        Next i
        vsPrinter1.Device = strImpresoraEtiq
        If strImpresoraEtiq = "" Then
            vsPrinter1.Preview = True
        Else
             vsPrinter1.Preview = False
        End If
    End If
End Sub

Sub ImprimirEtiqueta1(Num As String, nCopias As Integer, Descrip As String)
Dim crlf$, Impr$, X$
Dim texto As String
  On Error Resume Next
  crlf = Chr$(13) & Chr$(10)
'  vsPrinter1.StartDoc
    texto = crlf
    texto = "{" & Chr$(10) & Chr$(13)
    texto = texto & "[Mn " & Chr$(34) & "45x19" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Ml " & Chr$(34) & "21.00:0:0:1:direct table 125" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Mw " & Chr$(34) & "45.00:0.00:0.00:1" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Mm " & Chr$(34) & "0:0.00:0" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Ma " & Chr$(34) & "45x19" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Fs " & Chr$(34) & "*:2" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[DW " & Chr$(34) & "49" & Chr$(34) & " " & Chr$(34) & "2" & Chr$(34) & " " & Chr$(34) & "3" & Chr$(34) & " " & Chr$(34) & "" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "A" & Chr$(34) & " " & Chr$(34) & "19.50:5.00:1" & Chr$(34) & " " & Chr$(34) & "0" & Chr$(34) & " " & Chr$(34) & "20:+:0:0" & Chr$(34) & " " & Chr$(34) & "100:1" & Chr$(34) & " " & Chr$(34) & "11.50" & Chr$(34) & " " & Chr$(34) & "15" & Chr$(34) & " " & Chr$(34) & "" & Chr$(34) & " " & Chr$(34) & "? Barras" & Chr$(34) & " " & Chr$(34) & "1:1:2:2:3:3" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "C" & Chr$(34) & " " & Chr$(34) & "2.50:0.50:1" & Chr$(34) & " " & Chr$(34) & "0" & Chr$(34) & " " & Chr$(34) & "15531807:10" & Chr$(34) & " " & Chr$(34) & "22" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "\N" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Dw " & Chr$(34) & "Cultivo" & Chr$(34) & " " & Chr$(34) & "49" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & Num & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & Descrip & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "\N" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Pb " & Chr$(34) & nCopias & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "}" & Chr$(10) & Chr$(13)
    
    '------------ seleccionar Impresora de etiquetas
Call pSelectImpresoraEtiquetas
vsPrinter1.StartDoc
vsPrinter1.Text = texto
vsPrinter1.EndDoc
vsPrinter1.PrintDoc

End Sub

Sub ImprimirEtiquetaDeSeparacion()
Dim crlf$, Impr$, X$
Dim texto As String
Dim nCopias As Integer
Dim Descrip As String
nCopias = 1
Descrip = ""
  On Error Resume Next
  crlf = Chr$(13) & Chr$(10)
'  vsPrinter1.StartDoc
    texto = crlf
    texto = "{" & Chr$(10) & Chr$(13)
    texto = texto & "[Mn " & Chr$(34) & "45x19" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Ml " & Chr$(34) & "21.00:0:0:1:direct table 125" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Mw " & Chr$(34) & "45.00:0.00:0.00:1" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Mm " & Chr$(34) & "0:0.00:0" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Ma " & Chr$(34) & "45x19" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Fs " & Chr$(34) & "*:2" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[DW " & Chr$(34) & "49" & Chr$(34) & " " & Chr$(34) & "1" & Chr$(34) & " " & Chr$(34) & "3" & Chr$(34) & " " & Chr$(34) & "" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
'    texto = texto & " " & Chr$(34) & "A" & Chr$(34) & " " & Chr$(34) & "19.50:5.00:1" & Chr$(34) & " " & Chr$(34) & "0" & Chr$(34) & " " & Chr$(34) & "20:+:0:0" & Chr$(34) & " " & Chr$(34) & "100:1" & Chr$(34) & " " & Chr$(34) & "11.50" & Chr$(34) & " " & Chr$(34) & "15" & Chr$(34) & " " & Chr$(34) & "" & Chr$(34) & " " & Chr$(34) & "? Barras" & Chr$(34) & " " & Chr$(34) & "1:1:2:2:3:3" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "C" & Chr$(34) & " " & Chr$(34) & "2.50:0.50:1" & Chr$(34) & " " & Chr$(34) & "0" & Chr$(34) & " " & Chr$(34) & "15531807:10" & Chr$(34) & " " & Chr$(34) & "22" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "\N" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Dw " & Chr$(34) & "Cultivo" & Chr$(34) & " " & Chr$(34) & "49" & Chr$(34) & " " & Chr$(34) & "\N" & Chr$(34) & "" & Chr$(10) & Chr$(13)
'    texto = texto & " " & Chr$(34) & Num & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & Descrip & Chr$(34) & "" & Chr$(10) & Chr$(13)
    texto = texto & " " & Chr$(34) & "\N" & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "[Pb " & Chr$(34) & nCopias & Chr$(34) & "]" & Chr$(10) & Chr$(13)
    texto = texto & "}" & Chr$(10) & Chr$(13)
    
    '------------ seleccionar Impresora de etiquetas
Call pSelectImpresoraEtiquetas
vsPrinter1.StartDoc
vsPrinter1.Text = texto
vsPrinter1.EndDoc
vsPrinter1.PrintDoc

End Sub



















