VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmBuscaValores 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actuaciones"
   ClientHeight    =   4485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6105
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   6105
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdB 
      Caption         =   "A&nterior"
      Enabled         =   0   'False
      Height          =   375
      Index           =   1
      Left            =   5280
      TabIndex        =   7
      Top             =   120
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      Caption         =   "Si&guiente"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   4320
      TabIndex        =   6
      Top             =   120
      Width           =   855
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   4080
      Width           =   855
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   5160
      TabIndex        =   4
      Top             =   4080
      Width           =   855
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   375
      Left            =   3480
      TabIndex        =   2
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox txtDescripcion 
      Height          =   285
      Left            =   960
      TabIndex        =   0
      Top             =   120
      Width           =   2415
   End
   Begin SSDataWidgets_B.SSDBGrid grdValores 
      Height          =   3300
      Left            =   0
      TabIndex        =   3
      Top             =   600
      Width           =   5985
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   10557
      _ExtentY        =   5821
      _StockProps     =   79
      Caption         =   "Valores"
      ForeColor       =   12582912
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Descripci�n"
      Height          =   195
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Top             =   120
      Width           =   840
   End
End
Attribute VB_Name = "frmBuscaValores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intBuscar%, blnBuscarOtro As Boolean
Dim cllBuscar As New Collection
Dim intCodRest As Integer
Dim blnTabla As Boolean 'Indica si la restrincion tiene los valores en una tabla del sistema (TRUE) o
                        'son valores que se han introducido en la AG1700O (FALSE)


Private Sub cmdAceptar_Click()
If grdValores.SelBookmarks.Count = 0 Then
    MsgBox "No se a A�adido ninguna Actuacion", vbInformation, Me.Caption
    Unload Me
Else
    If blnTabla = True Then
        Call objPipe.PipeSet("AG0220_AG0209_AG12VALDESDERES", grdValores.Columns("C�digo").CellText(grdValores.SelBookmarks(0)))
    Else
        Call objPipe.PipeSet("AG0220_AG0209_AG12VALDESDERES", grdValores.Columns("Valor Inicio").CellText(grdValores.SelBookmarks(0)))
        Call objPipe.PipeSet("AG0220_AG0209_AG12VALHASTARES", grdValores.Columns("Valor Fin").CellText(grdValores.SelBookmarks(0)))
    End If
    Unload Me
End If
End Sub

Private Sub cmdB_Click(Index As Integer)
    If Index = 0 Then intBuscar = intBuscar + 1 Else intBuscar = intBuscar - 1
    If intBuscar < 0 Then intBuscar = 0
    blnBuscarOtro = True
    cmdBuscar_Click
End Sub



Private Sub Form_Load()
If objPipe.PipeExist("AG0209_AG0220_AG16CODTIPREST") Then
    intCodRest = objPipe.PipeGet("AG0209_AG0220_AG16CODTIPREST")
    objPipe.PipeRemove ("AG0209_AG0220_AG16CODTIPREST")
End If
pFormatearGrid
pCargarDatos
End Sub

Private Sub cmdBuscar_Click()
Dim intN As Integer
Dim i As Integer
Dim blnE As Boolean
If fBlnBuscar Then
        cmdB(0).Enabled = True: cmdB(1).Enabled = True
        If blnBuscarOtro = True Then
            blnBuscarOtro = False
            If intBuscar = 0 Then intBuscar = 1: Exit Sub
        Else
            intBuscar = 1
        End If
        For i = 1 To cllBuscar.Count
            blnE = False
            If UCase(Left$(cllBuscar(i), Len(txtDescripcion.Text))) = UCase(txtDescripcion.Text) Then
    
                    blnE = True

            End If
            If blnE Then
                intN = intN + 1
                If intN = intBuscar Then
                    grdValores.SelBookmarks.RemoveAll
                    LockWindowUpdate Me.hWnd
                    grdValores.MoveFirst
                    grdValores.MoveRecords i - 1
                    LockWindowUpdate 0&
                    grdValores.SelBookmarks.Add (grdValores.RowBookmark(grdValores.Row))
                    cmdBuscar.SetFocus
                    Exit Sub
                End If
            End If
        Next i
        If intBuscar = 1 Then

            MsgBox "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda.", vbInformation, Me.Caption
            Exit Sub
        Else
            intBuscar = intBuscar - 1
            Exit Sub
        End If
Else
        blnBuscarOtro = False
End If
End Sub
Private Sub cmdCancelar_Click()
Unload Me
End Sub
Private Function fBlnBuscar() As Boolean
    Dim i%
    If txtDescripcion.Text <> "" Then fBlnBuscar = True: Exit Function
End Function
Private Sub pFormatearGrid()

With grdValores
    .Columns(0).Caption = "C�digo"
    .Columns(1).Width = 975
    .Columns(1).Caption = "Valor Inicio"
    .Columns(1).Width = 2000
    .Columns(2).Caption = "Valor Fin"
    .Columns(2).Width = 2000
End With
End Sub
Private Sub pCargarDatos()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strValor As String

sql = "SELECT AG16TABLAVALOR,AG16COLCODVALO,AG16COLDESVALO"
sql = sql & " FROM AG1600 WHERE "
sql = sql & "AG16CODTIPREST = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intCodRest
Set rs = qry.OpenResultset
If Not IsNull(rs!AG16TABLAVALOR) Then
'tenemos una tabla de donde coger los valores de la restriccion
    blnTabla = True
    sql = "SELECT " & rs!AG16COLCODVALO & " COD," & rs!AG16COLDESVALO & " VALINI,"
    sql = sql & " ' ' VALFIN FROM " & rs!AG16TABLAVALOR
    rs.Close
    qry.Close
    Set rs = objApp.rdoConnect.OpenResultset(sql)
Else
'tenemos que leer los valores de la ag1700
    blnTabla = False
    sql = "SELECT AG17VALTIRES COD,AG17VALDESDETRE VALINI,AG17VALHASTATRE VALFIN"
    sql = sql & " FROM AG1700 WHERE AG16CODTIPREST = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = intCodRest
    Set rs = qry.OpenResultset()
End If
    While Not rs.EOF
        grdValores.AddItem rs!COD & Chr$(9) & _
                               rs!VALINI & Chr$(9) & _
                               rs!VALFIN
        strValor = rs!VALINI
        cllBuscar.Add strValor
        rs.MoveNext
    Wend
End Sub


Private Sub txtCitasAdmitidas_KeyPress(KeyAscii As Integer)
If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
End If
End Sub

Private Sub txtDescripcion_Change()
cmdB(0).Enabled = False: cmdB(1).Enabled = False
End Sub

