VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmUsuarios 
   Caption         =   "Buscar Usuarios"
   ClientHeight    =   4260
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5940
   LinkTopic       =   "Form1"
   ScaleHeight     =   4260
   ScaleWidth      =   5940
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4920
      TabIndex        =   6
      Top             =   3840
      Width           =   975
   End
   Begin VB.Frame frmFrame1 
      Caption         =   "Datos Busqueda"
      ForeColor       =   &H00C00000&
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5895
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   960
         TabIndex        =   1
         Top             =   300
         Width           =   1995
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   4800
         TabIndex        =   4
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   2
         Left            =   960
         TabIndex        =   3
         Top             =   720
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   3
         Left            =   3780
         TabIndex        =   2
         Top             =   240
         Width           =   1995
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   9
         Top             =   300
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   8
         Top             =   780
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido"
         Height          =   195
         Index           =   3
         Left            =   3000
         TabIndex        =   7
         Top             =   300
         Width           =   750
      End
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBUsuarios 
      Height          =   2490
      Left            =   0
      TabIndex        =   5
      Top             =   1200
      Width           =   5925
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   10451
      _ExtentY        =   4392
      _StockProps     =   79
   End
End
Attribute VB_Name = "frmUsuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub Form_Load()
pFormatearGrid
End Sub
Private Sub cmdBuscar_Click()
pbuscar
End Sub
Private Sub pbuscar()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strAnd As String

strAnd = ""
If Trim(txtBuscar(1)) = "" And Trim(txtBuscar(2)) = "" And Trim(txtBuscar(3)) = "" Then
    MsgBox "Rellene Algun campo para buscar", vbOKOnly
    Exit Sub
End If

sql = "SELECT SG02COD, SG02APE1||' '||SG02APE2||', '||SG02NOM USUARIO, AD30DESCATEGORIA "
sql = sql & " FROM SG0200, AD3000 WHERE "

If Trim(txtBuscar(1) <> "") Then
    sql = sql & "UPPER(SG02NOM) LIKE '%" & UCase(txtBuscar(1)) & "%'"
    strAnd = " AND "
End If

If Trim(txtBuscar(2) <> "") Then
    sql = sql & strAnd & "UPPER(SG02APE1) LIKE '%" & UCase(txtBuscar(2)) & "%'"
    strAnd = " AND "
End If

If Trim(txtBuscar(3) <> "") Then _
    sql = sql & strAnd & "UPPER(SG02APE1) LIKE '%" & UCase(txtBuscar(3)) & "%'"


sql = sql & " AND SG02FECDES IS NULL"
sql = sql & " AND SG0200.AD30CODCATEGORIA = AD3000.AD30CODCATEGORIA"
Set rs = objApp.rdoConnect.OpenResultset(sql)
While Not rs.EOF
    SSDBUsuarios.AddItem rs!sg02cod & Chr$(9) & rs!USUARIO _
                        & Chr$(9) & rs!AD30DESCATEGORIA
    rs.MoveNext
Wend
End Sub

Private Sub pFormatearGrid()

With SSDBUsuarios
    .Columns(0).Caption = "SG02COD"
    .Columns(0).Visible = False
    .Columns(1).Caption = "Usuario"
    .Columns(1).Width = 3000
    .Columns(2).Caption = "Categoria"
    .Columns(2).Width = 2000
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
End With
    
End Sub

Private Sub SSDBUsuarios_DblClick()
If SSDBUsuarios.SelBookmarks.Count <> 0 Then
    Call objPipe.PipeSet("AG0204_AG0201_SG02COD", SSDBUsuarios.Columns(0).Value)
End If
Unload Me
End Sub

Private Sub cmdSalir_Click()
If SSDBUsuarios.SelBookmarks.Count <> 0 Then
    Call objPipe.PipeSet("AG0204_AG0201_SG02COD", SSDBUsuarios.Columns(0).Value)
End If
Unload Me
End Sub
Private Sub SSDBUsuarios_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
SSDBUsuarios.SelBookmarks.RemoveAll
SSDBUsuarios.SelBookmarks.Add (SSDBUsuarios.RowBookmark(SSDBUsuarios.Row))
End Sub
