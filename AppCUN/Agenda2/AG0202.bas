Attribute VB_Name = "Module2"
 '-------------------------------------------
'
' Schedule/OCX Constants and Declarations
'
'    Version 0.14 - May 11, 1996
'
'-------------------------------------------

' Timescale constants
Global Const SCHED_YEAR = 0
Global Const SCHED_QUARTER = 1
Global Const SCHED_MONTH = 2
Global Const SCHED_WEEK = 3
Global Const SCHED_DAY = 4
Global Const SCHED_HOUR = 5
Global Const SCHED_MINUTE = 6

' Symbol Shape constants
Global Const SCHED_SYM_NONE = 0
Global Const SCHED_SYM_PENTUP = 1
Global Const SCHED_SYM_PENTDOWN = 2
Global Const SCHED_SYM_DIAMOND = 3
Global Const SCHED_SYM_CDIAMOND = 4
Global Const SCHED_SYM_ARROWHDUP = 5
Global Const SCHED_SYM_ARROWHDDOWN = 6
Global Const SCHED_SYM_ARROWHDRIGHT = 7
Global Const SCHED_SYM_ARROWHDLEFT = 8
Global Const SCHED_SYM_CARROWHDUP = 9
Global Const SCHED_SYM_CARROWHDDOWN = 0
Global Const SCHED_SYM_ARROWUP = 11
Global Const SCHED_SYM_ARROWDOWN = 12
Global Const SCHED_SYM_CARROWUP = 13
Global Const SCHED_SYM_CARROWDOWN = 14
Global Const SCHED_SYM_ARROWRIGHT = 15
Global Const SCHED_SYM_ARROWLEFT = 16
Global Const SCHED_SYM_CARROWRIGHT = 17
Global Const SCHED_SYM_CARROWLEFT = 18
Global Const SCHED_SYM_CARROWHDRIGHT = 19
Global Const SCHED_SYM_CARROWHDLEFT = 20
Global Const SCHED_SYM_PENTRIGHT = 21
Global Const SCHED_SYM_PENTLEFT = 22

' Symbol Type constants
Global Const SCHED_SYMTYPE_SOLID = 0
Global Const SCHED_SYMTYPE_FRAMED = 1

' Bar Text Position constants
Global Const SCHED_BARTEXTPOS_NONE = 0
Global Const SCHED_BARTEXTPOS_LEFT = 1
Global Const SCHED_BARTEXTPOS_RIGHT = 2
Global Const SCHED_BARTEXTPOS_ABOVE = 3
Global Const SCHED_BARTEXTPOS_BELOW = 4
Global Const SCHED_BARTEXTPOS_INSIDELEFT = 5
Global Const SCHED_BARTEXTPOS_INSIDECENTER = 6
Global Const SCHED_BARTEXTPOS_INSIDERIGHT = 7

' Bar Text Field constants
Global Const SCHED_BARTEXTFIELD_NONE = 0
Global Const SCHED_BARTEXTFIELD_RESOURCENAME = 1
Global Const SCHED_BARTEXTFIELD_RESOURCEUSER1 = 2
Global Const SCHED_BARTEXTFIELD_RESOURCEUSER2 = 3
Global Const SCHED_BARTEXTFIELD_RESOURCEUSER3 = 4
Global Const SCHED_BARTEXTFIELD_RESOURCEUSER4 = 5
Global Const SCHED_BARTEXTFIELD_TBBEG = 6
Global Const SCHED_BARTEXTFIELD_TBEND = 7
Global Const SCHED_BARTEXTFIELD_TBDUR = 8
Global Const SCHED_BARTEXTFIELD_TBGROUP = 9
Global Const SCHED_BARTEXTFIELD_TBUSER1 = 10
Global Const SCHED_BARTEXTFIELD_TBUSER2 = 11
Global Const SCHED_BARTEXTFIELD_TBUSER3 = 12
Global Const SCHED_BARTEXTFIELD_TBUSER4 = 13
Global Const SCHED_BARTEXTFIELD_BARNAME = 14

' Bar Pattern constants
Global Const SCHED_BARPATTERN_NONE = 0
Global Const SCHED_BARPATTERN_HORIZONTAL = 1
Global Const SCHED_BARPATTERN_VERTICAL = 2
Global Const SCHED_BARPATTERN_FDIAGONAL = 3
Global Const SCHED_BARPATTERN_BDIAGONAL = 4
Global Const SCHED_BARPATTERN_CROSS = 5
Global Const SCHED_BARPATTERN_DIAGCROSS = 6

' Bar Adjustable constants
Global Const SCHED_BARADJUSTABLE_FULL = 0
Global Const SCHED_BARADJUSTABLE_BEG = 1
Global Const SCHED_BARADJUSTABLE_MID = 2
Global Const SCHED_BARADJUSTABLE_END = 3
Global Const SCHED_BARADJUSTABLE_NONE = 4

' Display Mode constants
Global Const SCHED_DISPLAYMODE_RESOURCE = 0
Global Const SCHED_DISPLAYMODE_YEARLYHORZ = 1
Global Const SCHED_DISPLAYMODE_YEARLYVERT = 2
Global Const SCHED_DISPLAYMODE_MONTHLYHORZ = 3
Global Const SCHED_DISPLAYMODE_MONTHLYVERT = 4
Global Const SCHED_DISPLAYMODE_WEEKLYHORZ = 5
Global Const SCHED_DISPLAYMODE_WEEKLYVERT = 6
Global Const SCHED_DISPLAYMODE_DAILYHORZ = 7
Global Const SCHED_DISPLAYMODE_DAILYVERT = 8
Global Const SCHED_DISPLAYMODE_CALHORZ = 9
Global Const SCHED_DISPLAYMODE_CALVERT = 10

' Column Alignment constants
Global Const SCHED_COLALIGNMENT_LEFT = 0
Global Const SCHED_COLALIGNMENT_CENTER = 1
Global Const SCHED_COLALIGNMENT_RIGHT = 2

' Bevel Type constants
Global Const SCHED_BEVELTYPE_NONE = 0
Global Const SCHED_BEVELTYPE_INSET = 1
Global Const SCHED_BEVELTYPE_RAISED = 2

' Resource Column Field constants
Global Const SCHED_RESCOL_NONE = 0
Global Const SCHED_RESCOL_ROWNUM = 1
Global Const SCHED_RESCOL_RESNUM = 2
Global Const SCHED_RESCOL_RESNAME = 3
Global Const SCHED_RESCOL_BARNUM = 4
Global Const SCHED_RESCOL_TIMEBLOCKS = 5
Global Const SCHED_RESCOL_RESUSER1 = 6
Global Const SCHED_RESCOL_RESUSER2 = 7
Global Const SCHED_RESCOL_RESUSER3 = 8
Global Const SCHED_RESCOL_RESUSER4 = 9
Global Const SCHED_RESCOL_LINEHEIGHT = 10
Global Const SCHED_RESCOL_BACKCOLOR = 11
Global Const SCHED_RESCOL_FONTCOLOR = 12
Global Const SCHED_RESCOL_FONTNAME = 13
Global Const SCHED_RESCOL_FONTSIZEPERCENT = 14
Global Const SCHED_RESCOL_FONTBOLD = 15
Global Const SCHED_RESCOL_FONTITALIC = 16

' Timeblock Column Field constants
Global Const SCHED_TBCOL_NONE = 0
Global Const SCHED_TBCOL_ROWNUM = 1
Global Const SCHED_TBCOL_RESNUM = 2
Global Const SCHED_TBCOL_TBNUM = 3
Global Const SCHED_TBCOL_TBBEG = 4
Global Const SCHED_TBCOL_TBEND = 5
Global Const SCHED_TBCOL_TBDUR = 6
Global Const SCHED_TBCOL_BARNUM = 7
Global Const SCHED_TBCOL_GROUPNUM = 8
Global Const SCHED_TBCOL_TBUSER1 = 9
Global Const SCHED_TBCOL_TBUSER2 = 10
Global Const SCHED_TBCOL_TBUSER3 = 11
Global Const SCHED_TBCOL_TBUSER4 = 12

' Legend Position constants
Global Const SCHED_LEGENDPOS_RIGHT = 0
Global Const SCHED_LEGENDPOS_BOTTOM = 1
Global Const SCHED_LEGENDPOS_LEFT = 2
Global Const SCHED_LEGENDPOS_TOP = 3

' Legend Type constants
Global Const SCHED_LEGENDTYPE_RESOURCES = 0
Global Const SCHED_LEGENDTYPE_BARS = 1

' Font Orientation constants
Global Const SCHED_FONTORIENT_RIGHT = 0
Global Const SCHED_FONTORIENT_UP = 1
Global Const SCHED_FONTORIENT_LEFT = 2
Global Const SCHED_FONTORIENT_DOWN = 3

' Printer Header/Footer constants
Global Const SCHED_PRINTHDRFTR_ALLPAGES = 0
Global Const SCHED_PRINTHDRFTR_1STPAGE = 1
Global Const SCHED_PRINTHDRFTR_1STHORZ = 2
Global Const SCHED_PRINTHDRFTR_1STVERT = 3
Global Const SCHED_PRINTHDRFTR_NONE = 4

'colores
Global Const SCHED_COLOR_BLUE = 7
Global Const SCHED_COLOR_YELLOW = 2
Global Const SCHED_COLOR_GREEN = 1
