Attribute VB_Name = "modConstantes"
Option Explicit

'Constantes de los estados de las actuaciones
Public Const constESTACT_PLANIFICADA = 1
Public Const constESTACT_CITADA = 2
Public Const constESTACT_REALIZANDOSE = 3
Public Const constESTACT_REALIZADA = 4
Public Const constESTACT_INFORMADA = 5
Public Const constESTACT_CANCELADA = 6

'Constantes de los estados de las citas
Public Const constESTCITA_CITADA = 1
Public Const constESTCITA_ANULADA = 2
Public Const constESTCITA_RECITADA = 3
Public Const constESTCITA_PENDRECITAR = 4
Public Const constESTCITA_RESERVADA = 5

'Constantes para los tipos de actividades
Public Const constACTIV_CONSULTA = 201
Public Const constACTIV_PRUEBA = 203
Public Const constACTIV_INTERVENCION = 207
Public Const constACTIV_INFORME = 215
Public Const constACTIV_HOSPITALIZACION = 209

'Constantes de los Departamentos
Public Const constDPTO_QUIROFANO = 213
Public Const constDPTO_ANESTESIA = 223
Public Const constDPTO_URGENCIAS = 216
Public Const constDPTO_RAYOS = 209
Public Const constDPTO_REHABILITACION = 211

'Constantes para el Tipo de Departamento
Public Const constDPTO_SERVCLINICOS = 1
Public Const constDPTO_SERVBASICOS = 2

'Constantes de los modos de asignaci�n de las citas
Public Const constCITA_PORCANTIDAD = 1
Public Const constCITA_SECUENCIAL = 2
Public Const constCITA_PORINTERVALOS = 3
Public Const constCITA_PORINTERVALOSOSCILANTES = 4

'Constantes de los Tipos de Restricciones para las citas
Public Const constRESTRIC_TIPOPAC = 2
Public Const constRESTRIC_TIPOECON = 3
Public Const constRESTRIC_EDAD = 4
Public Const constRESTRIC_SEXO = 8
Public Const constRESTRIC_USUARIO = 16

'Constantes de Sexo
Public Const constSEXO_HOMBRE = 1
Public Const constSEXO_MUJER = 1

'Constantes para el acceso a la citaci�n por huecos
Public Const constACCESOCPH_LIBRE = 1       'pantalla en blanco (hay que pedir y citar)
Public Const constACCESOCPH_NAPLAN = 2      'citar y/o recitar actuaciones ya pedidas
Public Const constACCESOCPH_PETICION = 3    'citar una petici�n completa
Public Const constACCESOCPH_PERSONA = 4     'pedir y citar para una persona concreta

'Constantes para la citaci�n de consultas de Urgencias
Public Const constURGENCIAS_DPTO_NAME = "Urgencias"
Public Const constURGENCIAS_PR_CONSULTA = 4293
Public Const constURGENCIAS_PR_VISITA = 4298
Public Const constURGENCIAS_REC_PASTRANA = 650
Public Const constURGENCIAS_DR_PASTRANA = "HPA"
Public Const constURGENCIAS_DR_PASTRANA_NAME = "Dr. Pastrana"
Public Const constURGENCIAS_REC_CONSCOLURG = 311
Public Const constURGENCIAS_DR_CONSCOLURG = "CCU"
Public Const constURGENCIAS_REQDOC = 0

'Const para la reserva de huecos
Public Const constPERSONA_RESERVA = 1416157

'Constantes de pa�ses
Public Const constPAIS_ESPA�A = 45
