VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmSelTipoEcon 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de datos del paciente"
   ClientHeight    =   1080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4365
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1080
   ScaleWidth      =   4365
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3300
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   3300
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin SSDataWidgets_B.SSDBCombo cboTipoEcon 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   420
      Width           =   2985
      DataFieldList   =   "Column 1"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4445
      Columns(1).Caption=   "TipoEcon"
      Columns(1).Name =   "TipoEcon"
      Columns(1).DataField=   "Column 2"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5265
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Tipo econ�mico:"
      ForeColor       =   &H00FF0000&
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   180
      Width           =   1515
   End
End
Attribute VB_Name = "frmSelTipoEcon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
    If cboTipoEcon.Text = "" Then
        MsgBox "Existen datos obligatorios sin rellenar.", vbExclamation, Me.Caption
    Else
        Call objPipe.PipeSet("SEL_TIPOECON", cboTipoEcon.Columns("Cod").Text)
        Unload Me
    End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim SQL$, rs As rdoResultset
    
    cboTipoEcon.BackColor = objApp.objUserColor.lngMandatory
    
    SQL = "SELECT CI32CODTIPECON, CI32DESTIPECON"
    SQL = SQL & " FROM CI3200"
    SQL = SQL & " WHERE CI32FECFIVGTEC IS NULL"
    SQL = SQL & " ORDER BY CI32CODTIPECON"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboTipoEcon.AddItem rs!CI32CODTIPECON & Chr$(9) & rs!CI32CODTIPECON & " - " & rs!CI32DESTIPECON
        rs.MoveNext
    Loop
    rs.Close
End Sub
