Attribute VB_Name = "modFunciones"
Option Explicit

Public Sub pAsociadas(lngNAPlan&, strFecPlan$)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    sql = "UPDATE PR0400 SET PR04FECPLANIFIC = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    sql = sql & " WHERE PR04NUMACTPLAN IN"
    sql = sql & " (SELECT PR04NUMACTPLAN"
    sql = sql & " FROM PR6100, PR0400"
    sql = sql & " WHERE PR6100.PR03NUMACTPEDI_ASO = (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?)"
    sql = sql & " AND PR6100.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR0400.PR37CODESTADO IN (" & constESTACT_PLANIFICADA & "," & constESTACT_CITADA & "))"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(strFecPlan, "dd/mm/yyyy hh:mm:ss")
    qry(1) = lngNAPlan
    qry.Execute
    qry.Close
End Sub
Public Function blnSacarPendientes(strFI As String, strFF As String, lngCR As Long) As Boolean
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim intR As Integer
Dim msg As String

sql = "SELECT COUNT(*) FROM CI0100,PR0400 WHERE AG11CODRECURSO = ?"
sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
sql = sql & " AND CI01SITCITA = '1' "
sql = sql & " AND CI01FECCONCERT >= TO_DATE('" & strFI & " 00:00','DD/MM/YYYY HH24:MI')"
sql = sql & " AND CI01FECCONCERT <= TO_DATE('" & strFF & " 23:59','DD/MM/YYYY HH24:MI')"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCR
Set rs = qry.OpenResultset()
If rs(0) > 0 Then
    intR = MsgBox("Quedaran afectados " & rs(0) & " pacientes" & Chr$(13) & _
                "Desea Continuar?", vbYesNo + vbQuestion, "Mantenimento de Perfiles")
     If intR = vbNo Then
        blnSacarPendientes = False
        Exit Function
     End If
End If
rs.Close
qry.Close

sql = "SELECT TO_CHAR(CI01FECCONCERT,'DD/MM/YYYY HH24:MI'), PR08DESOBSERV FROM"
sql = sql & " CI0100,PR0400,PR0800 WHERE  AG11CODRECURSO = ?"
sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
sql = sql & " AND CI01SITCITA = '5' "
sql = sql & " AND CI01FECCONCERT >= TO_DATE('" & strFI & " 00:00','DD/MM/YYYY HH24:MI')"
sql = sql & " AND CI01FECCONCERT <= TO_DATE('" & strFF & " 23:59','DD/MM/YYYY HH24:MI')"
sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCR
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    While Not rs.EOF
        If Not IsNull(rs(1)) Then
            msg = msg & rs(0) & "-> " & rs(1) & Chr$(13)
        Else
            msg = msg & rs(0) & Chr$(13)
        End If
        rs.MoveNext
    Wend
End If
MsgBox "Las siguientes reservas quedaran afectadas :" & Chr$(13) & msg, vbOKOnly, "Reservas"
rs.Close
qry.Close

sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_PLANIFICADA
sql = sql & " WHERE PR37CODESTADO = " & constESTACT_CITADA
sql = sql & " AND PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM CI0100"
sql = sql & " WHERE CI01SITCITA = '1' AND AG11CODRECURSO = ? "
sql = sql & " AND CI01FECCONCERT >= TO_DATE(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " AND CI01FECCONCERT <= TO_DATE(?,'DD/MM/YYYY HH24:MI'))"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCR
    qry(1) = strFI & " 00:00"
    qry(2) = strFF & " 23:59"
qry.Execute
qry.Close

sql = "UPDATE CI0100 SET CI01SITCITA = '4'"
sql = sql & " WHERE PR04NUMACTPLAN IN (SELECT PR0400.PR04NUMACTPLAN FROM"
sql = sql & " CI0100, PR0400 WHERE"
sql = sql & " PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
sql = sql & " AND CI01SITCITA = '1' AND AG11CODRECURSO = ? "
sql = sql & " AND PR37CODESTADO = " & constESTACT_PLANIFICADA
sql = sql & " AND CI01FECCONCERT >= TO_DATE(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " AND CI01FECCONCERT <= TO_DATE(?,'DD/MM/YYYY HH24:MI'))"
sql = sql & " AND CI01SITCITA = '1'"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCR
    qry(1) = strFI & " 00:00"
    qry(2) = strFF & " 23:59"
qry.Execute
qry.Close
If Err > 0 Then
    blnSacarPendientes = False
Else
    blnSacarPendientes = True
End If
End Function

Public Function fBlnCPEP(lngCR As Long, intP As Integer) As Boolean
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim rs1 As rdoResultset
Dim intR As Integer
Dim sql1$
Dim strAhora As String
Dim strIniPerfil As String

strAhora = fAhora
'Funcion que saca a pendientes de recitar toas las citas que estan
'en los periodos de vigencias del perfil que queremos eliminar

'Buscamos lo periodosd de vigencia a partir del dia de hoy

sql = "SELECT AG09FECINVIPER,AG09FECFIVIPER FROM AG0900 "
sql = sql & " WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ?"
sql = sql & " AND AG09FECFIVIPER > SYSDATE "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCR
    qry(1) = intP
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    'miramos cuantos pacientes hay en esos periodos de vigencia
    sql = "SELECT COUNT(*) FROM CI0100,PR0400 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
    sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND CI01SITCITA = '1' AND ("
    While Not rs.EOF
        If CDate(Format(rs(0), "DD/MM/YYYY")) < CDate(strAhora) Then _
        strIniPerfil = CDate(strAhora) Else strIniPerfil = rs(0)
        sql1 = sql1 & "(CI01FECCONCERT >= TO_DATE( '" & Format$(strIniPerfil, "DD/MM/YYYY") & "','DD/MM/YYYY')"
        sql1 = sql1 & " AND CI01FECCONCERT <= TO_DATE( '" & Format$(rs(1), "DD/MM/YYYY") & "','DD/MM/YYYY')) OR"
        rs.MoveNext
    Wend
    rs.Close
    qry.Close
    sql1 = Left(sql1, Len(sql1) - 3)
    sql = sql & sql1 & ")"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCR
    Set rs1 = qry.OpenResultset()
    intR = MsgBox("Quedar�n afectados " & rs1(0) & " pacientes" & Chr$(13) & _
            "Desea Continuar? ", vbYesNo + vbQuestion, "Mantenimiento de Perfiles")
    If intR = vbNo Then
        fBlnCPEP = False
        Exit Function
    End If
    If rs1(0) > 0 Then
        rs1.Close
        sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_PLANIFICADA
        sql = sql & " WHERE PR37CODESTADO = " & constESTACT_CITADA
        sql = sql & " AND PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM CI0100 "
        sql = sql & " WHERE CI01SITCITA = '1' AND (" & sql1 & ")"
        sql = sql & " AND AG11CODRECURSO = ? )"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCR
        qry.Execute
        qry.Close
        sql = "UPDATE CI0100 SET CI01SITCITA = '4' WHERE AG11CODRECURSO = ? "
        sql = sql & " AND CI01SITCITA = '1' AND PR04NUMACTPLAN IN (SELECT"
        sql = sql & " PR0400.PR04NUMACTPLAN FROM CI0100,PR0400 WHERE "
        sql = sql & " PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
        sql = sql & " AND PR37CODESTADO = 1 AND (" & sql1 & "))"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCR
        qry.Execute
        qry.Close
    End If
    fBlnCPEP = True
Else
    fBlnCPEP = True
End If
End Function
Public Function fblnComprobaciones(lngCR As Long, intCP As Integer, intCPdo As Integer, _
strFI As String, strFF As String, strMotivo As String, intTipo) As Boolean
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim qry1 As rdoQuery
Dim rs2 As rdoResultset
Dim intPerNuevo As Integer
Dim intR As Integer
'intTIPO = 1 MODIFICACION DEL PERFIL
'INTTIPO = 0 PERFIL NUEVO

'miramos si coincide con algun otro periodo del mismo perfil
sql = "SELECT COUNT(*) FROM AG0900,AG0700 WHERE "
sql = sql & " AG0900.AG11CODRECURSO = ? AND AG0900.AG07CODPERFIL = ? "
sql = sql & " AND AG09NUMPVIGPER <> ? "
sql = sql & " AND AG09FECINVIPER < TO_DATE(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " AND AG09FECFIVIPER > TO_DATE(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " AND AG0900.AG07CODPERFIL = AG0700.AG07CODPERFIL"
sql = sql & " AND AG0700.AG07INDPERGENE = 0"


Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCR
    qry(1) = intCP
    qry(2) = intCPdo
    qry(3) = Format$(strFF, "dd/mm/yyyy")
    qry(4) = Format$(strFI, "dd/mm/yyyy")
Set rs = qry.OpenResultset()
If rs(0) > 0 Then
    MsgBox "Este periodo se solapa con un periodo del mismo perfil.Compruebe las fechas ", vbInformation, "Mantenimiento de Periodos"
    fblnComprobaciones = False
    Exit Function
End If
rs.Close
qry.Close
If intTipo = 0 Then
    If Not blnSacarPendientes(strFI, strFF, lngCR) Then
        fblnComprobaciones = False
        Exit Function
    End If
End If
    
'buscamos el periodo de otro perfil  para arreglarlo
sql = "SELECT AG07DESPERFIL,AG0900.AG09FECINVIPER,AG0900.AG09FECFIVIPER,"
sql = sql & " AG0900.AG09NUMPVIGPER,AG0900.AG07CODPERFIL "
sql = sql & " FROM AG0900, AG0700 WHERE "
sql = sql & " AG0900.AG11CODRECURSO = ? AND AG0900.AG07CODPERFIL <> ?  "
sql = sql & " AND AG09NUMPVIGPER <> ? "
sql = sql & " AND AG09FECINVIPER <= TO_DATE(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " AND AG09FECFIVIPER >= TO_DATE(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " AND AG0900.AG07CODPERFIL = AG0700.AG07CODPERFIL"
sql = sql & " AND AG0900.AG11CODRECURSO = AG0700.AG11CODRECURSO"
sql = sql & " AND AG0700.AG07INDPERGENE = 0"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCR
    qry(1) = intCP
    qry(2) = intCPdo
    qry(3) = Format$(strFF, "dd/mm/yyyy")
    qry(4) = Format$(strFI, "dd/mm/yyyy")
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    While Not rs.EOF
        'si esta totalmente comprendido lo eliminamos
        If CDate(Format(rs!AG09FECINVIPER, "dd/mm/yyyy")) >= CDate(Format(strFI, "dd/mm/yyyy")) And _
        CDate(Format(rs!AG09FECFIVIPER, "dd/mm/yyyy")) <= CDate(Format(strFF, "dd/mm/yyyy")) Then
             intR = MsgBox("Hay un periodo del perfil: " & rs!AG07DESPERFIL & Chr$(13) & _
             "que esta totalmente comprendido y se eliminara �Desea continuar?", vbYesNo, "Cambios Periodos")
             If intR = vbNo Then
                fblnComprobaciones = False
                Exit Function
             End If
            If intTipo = 1 Then
                If blnSacarPendientes(Format(rs!AG09FECINVIPER, "dd/mm/yyyy"), _
                Format(rs!AG09FECFIVIPER, "dd/mm/yyyy"), lngCR) Then
                    sql = "DELETE FROM AG0900 WHERE AG09NUMPVIGPER = ?"
                    sql = sql & " AND AG07CODPERFIL = ? AND AG11CODRECURSO = ? "
                    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                        qry1(0) = rs!AG09NUMPVIGPER
                        qry1(1) = rs!AG07CODPERFIL
                        qry1(2) = lngCR
                    qry1.Execute
                    qry1.Close
                Else 'blnSacarPendientes
                    fblnComprobaciones = False
                    Exit Function
                End If 'blnSacarPendientes
            Else 'intTipo
                sql = "DELETE FROM AG0900 WHERE AG09NUMPVIGPER = ?"
                sql = sql & " AND AG07CODPERFIL = ? AND AG11CODRECURSO = ? "
                Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                    qry1(0) = rs!AG09NUMPVIGPER
                    qry1(1) = rs!AG07CODPERFIL
                    qry1(2) = lngCR
                qry1.Execute
                qry1.Close
            End If 'intTipo
            
        'si es el nuevo perfil el que esta comprendido partimos el auntiguo
        ElseIf CDate(Format(rs!AG09FECINVIPER, "dd/mm/yyyy")) < CDate(strFI) And _
        CDate(Format(rs!AG09FECFIVIPER, "dd/mm/yyyy")) > CDate(strFF) Then
             intR = MsgBox("El periodo del perfil: " & rs!AG07DESPERFIL & Chr$(13) & _
             "quedar� divido en dos  �Desea continuar?", vbYesNo, "Cambios Periodos")
             If intR = vbNo Then
                fblnComprobaciones = False
                Exit Function
             End If
            If intTipo = 1 Then
                If blnSacarPendientes(strFI, strFF, lngCR) Then
                    sql = "UPDATE AG0900 SET AG09FECFIVIPER = TO_DATE(?,'DD/MM/YYYY')"
                    sql = sql & " WHERE AG09NUMPVIGPER = ?"
                    sql = sql & " AND AG07CODPERFIL = ? AND AG11CODRECURSO = ? "
                    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                        qry1(0) = DateAdd("d", -1, strFI)
                        qry1(1) = rs!AG09NUMPVIGPER
                        qry1(2) = rs!AG07CODPERFIL
                        qry1(3) = lngCR
                    qry1.Execute
                    qry1.Close
                    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                        qry1(0) = lngCR
                        qry1(1) = rs!AG07CODPERFIL
                    Set rs2 = qry1.OpenResultset()
                    If rs2.EOF Then intPerNuevo = 1 Else intPerNuevo = Val(rs2(0)) + 1
                    rs2.Close
                    qry1.Close
                    sql = "INSERT INTO AG0900 (AG11CODRECURSO, AG07CODPERFIL,"
                    sql = sql & "AG09NUMPVIGPER, AG09FECINVIPER, AG09FECFIVIPER, AG09MOTIVOPERF)"
                    sql = sql & " VALUES (?,?,?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'),?)"
                    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                        qry1(0) = lngCR
                        qry1(1) = intCP
                        qry1(2) = intCPdo
                        qry1(3) = DateAdd("d", 1, strFF)
                        qry1(4) = Format(rs!AG09FECFIVIPER, "dd/mm/yyyy")
                        qry1(5) = strMotivo
                    qry1.Execute
                    qry1.Close
                Else 'blnSacarPendientes
                    fblnComprobaciones = False
                    Exit Function
                End If 'blnSacarPendientes
            Else 'INTTIPO = 1
                sql = "UPDATE AG0900 SET AG09FECFIVIPER = TO_DATE(?,'DD/MM/YYYY')"
                sql = sql & " WHERE AG09NUMPVIGPER = ?"
                sql = sql & " AND AG07CODPERFIL = ? AND AG11CODRECURSO = ? "
                Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                    qry1(0) = Format(DateAdd("d", -1, strFI), "DD/MM/YYYY")
                    qry1(1) = rs!AG09NUMPVIGPER
                    qry1(2) = rs!AG07CODPERFIL
                    qry1(3) = lngCR
                qry1.Execute
                qry1.Close
                sql = "SELECT MAX(AG09NUMPVIGPER) FROM AG0900 WHERE AG11CODRECURSO = ? AND"
                sql = sql & " AG07CODPERFIL = ? "
                Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                    qry1(0) = lngCR
                    qry1(1) = rs!AG07CODPERFIL
                Set rs2 = qry1.OpenResultset()
                If rs2.EOF Then intPerNuevo = 1 Else intPerNuevo = Val(rs2(0)) + 1
                rs2.Close
                qry1.Close
                sql = "INSERT INTO AG0900 (AG11CODRECURSO, AG07CODPERFIL,"
                sql = sql & "AG09NUMPVIGPER, AG09FECINVIPER, AG09FECFIVIPER, AG09MOTIVOPERF)"
                sql = sql & " VALUES (?,?,?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'),?)"
                Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                    qry1(0) = lngCR
                    qry1(1) = rs!AG07CODPERFIL
                    qry1(2) = intPerNuevo
                    qry1(3) = Format(DateAdd("d", 1, strFF), "dd/mm/yyyy")
                    qry1(4) = Format(rs!AG09FECFIVIPER, "dd/mm/yyyy")
                    qry1(5) = strMotivo
                qry1.Execute
                qry1.Close
            End If  'INTTIPO = 1
        'si solo tiene dentro la fecha fin ajustamos esta a la fecha inicio del nuevo
        ElseIf CDate(Format(rs!AG09FECINVIPER, "dd/mm/yyyy")) < CDate(strFI) And _
        CDate(Format(rs!AG09FECFIVIPER, "dd/mm/yyyy")) <= CDate(strFF) Then
            intR = MsgBox("El periodo del perfil: " & rs!AG07DESPERFIL & Chr$(13) & _
             "quedar� modificado en su fecha fin para ajustarla a la de incio del nuevo/modificado" & Chr$(13) & _
             "periodo �Desea continuar?", vbYesNo, "Cambios Periodos")
             If intR = vbNo Then
                fblnComprobaciones = False
                Exit Function
             End If
            If intTipo = 1 Then
                If blnSacarPendientes(strFI, Format(rs!AG09FECFIVIPER, "dd/mm/yyyy"), lngCR) Then
                    sql = "UPDATE AG0900 SET AG09FECFIVIPER = TO_DATE(?,'DD/MM/YYYY')"
                    sql = sql & " WHERE AG09NUMPVIGPER = ?"
                    sql = sql & " AND AG07CODPERFIL = ? AND AG11CODRECURSO = ? "
                    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                        qry1(0) = Format(DateAdd("d", -1, strFI), "dd/mm/yyyy")
                        qry1(1) = rs!AG09NUMPVIGPER
                        qry1(2) = rs!AG07CODPERFIL
                        qry1(3) = lngCR
                    qry1.Execute
                    qry1.Close
                Else
                    fblnComprobaciones = False
                    Exit Function
                End If
            Else
                sql = "UPDATE AG0900 SET AG09FECFIVIPER = TO_DATE(?,'DD/MM/YYYY')"
                sql = sql & " WHERE AG09NUMPVIGPER = ?"
                sql = sql & " AND AG07CODPERFIL = ? AND AG11CODRECURSO = ? "
                Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                    qry1(0) = Format(DateAdd("d", -1, strFI), "dd/mm/yyyy")
                    qry1(1) = rs!AG09NUMPVIGPER
                    qry1(2) = rs!AG07CODPERFIL
                    qry1(3) = lngCR
                qry1.Execute
                
                qry1.Close
            End If
        'si solo tiene dentro la fecha incio ajustamos esta a la fecha fin del nuevo
        ElseIf CDate(Format(rs!AG09FECINVIPER, "dd/mm/yyyy")) >= CDate(strFI) And _
        CDate(Format(rs!AG09FECFIVIPER, "dd/mm/yyyy")) > CDate(strFF) Then
            intR = MsgBox("El periodo del perfil: " & rs!AG07DESPERFIL & Chr$(13) & _
             "quedar� modificado en su fecha inicio para ajustarla a la de fin del nuevo/modificado" & Chr$(13) & _
             "periodo �Desea continuar?", vbYesNo, "Cambios Periodos")
             If intR = vbNo Then
                fblnComprobaciones = False
                Exit Function
             End If
            If intTipo = 1 Then
                If blnSacarPendientes(Format(rs!AG09FECINVIPER, "dd/mm/yyyy"), strFF, lngCR) Then
                    sql = "UPDATE AG0900 SET AG09FECINVIPER = TO_DATE(?,'DD/MM/YYYY')"
                    sql = sql & " WHERE AG09NUMPVIGPER = ?"
                    sql = sql & " AND AG07CODPERFIL = ? AND AG11CODRECURSO = ? "
                    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                        qry1(0) = Format(DateAdd("d", 1, strFF), "dd/mm/yyyy")
                        qry1(1) = rs!AG09NUMPVIGPER
                        qry1(2) = rs!AG07CODPERFIL
                        qry1(3) = lngCR
                    qry1.Execute
                    qry1.Close
                Else
                    fblnComprobaciones = False
                    Exit Function
                End If
            Else
                sql = "UPDATE AG0900 SET AG09FECINVIPER = TO_DATE(?,'DD/MM/YYYY')"
                sql = sql & " WHERE AG09NUMPVIGPER = ?"
                sql = sql & " AND AG07CODPERFIL = ? AND AG11CODRECURSO = ? "
                Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                    qry1(0) = Format(DateAdd("d", 1, strFF), "dd/mm/yyyy")
                    qry1(1) = rs!AG09NUMPVIGPER
                    qry1(2) = rs!AG07CODPERFIL
                    qry1(3) = lngCR
                qry1.Execute
                qry1.Close
            End If 'blnSacarPendientes
        End If 'INT TIPO
        rs.MoveNext
    Wend
Else 'si no solapamos con ningun perfil miramos el genereico
    rs.Close
    qry.Close
    If intTipo = 1 Then
        sql = "SELECT AG09FECINVIPER,AG09FECFIVIPER FROM AG0900 WHERE "
        sql = sql & " AG0900.AG11CODRECURSO = ? AND AG0900.AG07CODPERFIL = ? "
        sql = sql & " AND AG09NUMPVIGPER = ? "
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCR
            qry(1) = intCP
            qry(2) = intCPdo
        Set rs = qry.OpenResultset()
        If Not rs.EOF Then
            If CDate(Format(rs!AG09FECINVIPER, "dd/mm/yyyy")) > CDate(strFI) Then
                If Not blnSacarPendientes(strFI, Format$(rs!AG09FECINVIPER, "dd/mm/yyyy"), lngCR) Then
                    fblnComprobaciones = False
                    Exit Function
                End If
            End If
            If CDate(Format(rs!AG09FECFIVIPER, "dd/mm/yyyy")) < CDate(strFF) Then
                If Not blnSacarPendientes(Format(rs!AG09FECFIVIPER, "dd/mm/yyyy"), CDate(strFF), lngCR) Then
                    fblnComprobaciones = False
                    Exit Function
                End If
            End If
        End If 'RS.EOF
    End If 'INTIPOT = 1
End If ' si solapamos
   
fblnComprobaciones = True
End Function

