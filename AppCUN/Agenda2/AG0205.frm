VERSION 5.00
Begin VB.Form frmNuevoPerfil 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nuevo Pefil"
   ClientHeight    =   1005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4890
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1005
   ScaleWidth      =   4890
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   975
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   3840
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.TextBox txtPrioridad 
      BackColor       =   &H00FFFF00&
      Height          =   375
      Left            =   4320
      TabIndex        =   2
      Top             =   120
      Width           =   495
   End
   Begin VB.TextBox txtDesPerfil 
      BackColor       =   &H00FFFF00&
      Height          =   375
      Left            =   840
      TabIndex        =   1
      Top             =   120
      Width           =   2535
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Prioridad"
      Height          =   195
      Left            =   3600
      TabIndex        =   5
      Top             =   120
      Width           =   615
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Nombre"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   555
   End
End
Attribute VB_Name = "frmNuevoPerfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim lngCodRecurso As Long
Dim intPerfil%
Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub Form_Load()
lngCodRecurso = objPipe.PipeGet("AG0201_AG0205_AG11CODRECURSO")
Call objPipe.PipeRemove("AG0201_AG0205_AG11CODRECURSO")
If objPipe.PipeExist("AG0201_AG0205_AG07CODPERFIL") Then
    intPerfil = objPipe.PipeGet("AG0201_AG0205_AG07CODPERFIL")
    objPipe.PipeRemove ("AG0201_AG0205_AG07CODPERFIL")
    Call pCargarPerfil(intPerfil)
Else
    intPerfil = 0
End If
End Sub

Private Sub cmdAceptar_Click()
If Trim(txtDesPerfil) = "" Or Trim(txtPrioridad) = "" Then
    MsgBox "Rellene la descripcion y la prioridad", vbCritical
Else
    If intPerfil = 0 Then pCrearPerfil Else pGuardarPerfil
    Unload Me
End If
End Sub
Private Sub pCrearPerfil()
Dim sql As String
Dim rs As rdoResultset
Dim intCodperfil As Integer
Dim qry As rdoQuery

'codigo del perfil
sql = "SELECT MAX(AG07CODPERFIL) FROM AG0700 WHERE "
sql = sql & " AG11CODRECURSO = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
Set rs = qry.OpenResultset()
If Not IsNull(rs(0)) Then intCodperfil = Int(rs(0)) + 1 Else intCodperfil = 1
rs.Close
qry.Close

sql = "INSERT INTO AG0700 (AG11CODRECURSO,AG07CODPERFIL,AG07DESPERFIL,"
sql = sql & " AG07PRIORIDAD) VALUES (?,?,?,?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodperfil
    qry(2) = txtDesPerfil
    qry(3) = txtPrioridad
qry.Execute
qry.Close
Call objPipe.PipeSet("AG0205_AG0202_AG07CODPERFIL", intCodperfil)
End Sub

Private Sub pCargarPerfil(intCP As Integer)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT AG07DESPERFIL,AG07PRIORIDAD FROM AG0700 WHERE "
sql = sql & " AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCP
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    txtDesPerfil = rs!AG07DESPERFIL
    If IsNull(rs!AG07PRIORIDAD) Then txtPrioridad = "0" Else txtPrioridad = rs!AG07PRIORIDAD
End If

End Sub

Private Sub pGuardarPerfil()
Dim sql As String
Dim qry As rdoQuery
On Error Resume Next
sql = "UPDATE AG0700 SET AG07DESPERFIL = ?, AG07PRIORIDAD = ? "
sql = sql & " WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtDesPerfil
    qry(1) = txtPrioridad
    qry(2) = lngCodRecurso
    qry(3) = intPerfil
qry.Execute
If Err > 0 Then
    MsgBox "No se han guardado las modificaciones", vbExclamation, Me.Caption
End If
End Sub



