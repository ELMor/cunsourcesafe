VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmCitacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Citaci�n"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Enabled         =   0   'False
      Height          =   390
      Left            =   8880
      TabIndex        =   95
      Top             =   1860
      Visible         =   0   'False
      Width           =   870
   End
   Begin VB.Frame Frame5 
      Caption         =   "Actuaciones"
      ForeColor       =   &H00C00000&
      Height          =   6315
      Left            =   60
      TabIndex        =   31
      Top             =   2280
      Width           =   11820
      Begin VB.Frame fraContainer 
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   5970
         Left            =   50
         TabIndex        =   32
         Top             =   250
         Width           =   11700
         Begin VB.Frame fraNivel 
            BorderStyle     =   0  'None
            Height          =   2600
            Index           =   2
            Left            =   0
            TabIndex        =   66
            Top             =   5300
            Width           =   11475
            Begin VB.TextBox txtRecSel 
               BackColor       =   &H00C0C0C0&
               Height          =   315
               Index           =   2
               Left            =   8760
               Locked          =   -1  'True
               TabIndex        =   75
               Top             =   1910
               Width           =   2655
            End
            Begin VB.Frame fraHora 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   360
               Index           =   2
               Left            =   8220
               TabIndex        =   73
               Top             =   1910
               Width           =   555
               Begin MSMask.MaskEdBox mskHora 
                  Height          =   315
                  Index           =   2
                  Left            =   0
                  TabIndex        =   74
                  Top             =   0
                  Width           =   555
                  _ExtentX        =   979
                  _ExtentY        =   556
                  _Version        =   327681
                  MaxLength       =   5
                  Mask            =   "##:##"
                  PromptChar      =   "_"
               End
            End
            Begin VB.Frame fraAct 
               BorderStyle     =   0  'None
               Height          =   1920
               Index           =   2
               Left            =   0
               TabIndex        =   69
               Top             =   250
               Width           =   3135
               Begin VB.TextBox txtActiv 
                  Height          =   285
                  Index           =   2
                  Left            =   0
                  TabIndex        =   70
                  Top             =   120
                  Visible         =   0   'False
                  Width           =   195
               End
               Begin SSDataWidgets_B.SSDBCombo cboDpto 
                  Height          =   315
                  Index           =   2
                  Left            =   630
                  TabIndex        =   6
                  Top             =   0
                  Width           =   2445
                  DataFieldList   =   "Column 1"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  DividerType     =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "Cod"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3704
                  Columns(1).Caption=   "Dpto"
                  Columns(1).Name =   "Dpto"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4322
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cboAct 
                  Height          =   315
                  Index           =   2
                  Left            =   630
                  TabIndex        =   7
                  Top             =   400
                  Width           =   2445
                  DataFieldList   =   "Column 1"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  DividerType     =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   5
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "Cod"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3704
                  Columns(1).Caption=   "Act"
                  Columns(1).Name =   "Act"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "Min"
                  Columns(2).Name =   "Min"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   3200
                  Columns(3).Visible=   0   'False
                  Columns(3).Caption=   "NAPlan"
                  Columns(3).Name =   "NAPlan"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   3200
                  Columns(4).Visible=   0   'False
                  Columns(4).Caption=   "ReqDoc"
                  Columns(4).Name =   "ReqDoc"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  _ExtentX        =   4322
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblTOPac 
                  Caption         =   "T.O. Pac:"
                  Height          =   255
                  Index           =   2
                  Left            =   630
                  TabIndex        =   90
                  Top             =   1390
                  Width           =   2475
               End
               Begin VB.Label lblTORec 
                  Caption         =   "T.O .Rec:"
                  Height          =   255
                  Index           =   2
                  Left            =   630
                  TabIndex        =   89
                  Top             =   1150
                  Width           =   2475
               End
               Begin VB.Label lblFecPlan 
                  Caption         =   "Fecha Plan:"
                  Height          =   255
                  Index           =   2
                  Left            =   630
                  TabIndex        =   88
                  Top             =   1630
                  Width           =   2475
               End
               Begin VB.Label lblDpto 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Dpto:"
                  Height          =   255
                  Index           =   2
                  Left            =   180
                  TabIndex        =   72
                  Top             =   75
                  Width           =   420
               End
               Begin VB.Label lblAct 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Prueba:"
                  Height          =   315
                  Index           =   2
                  Left            =   0
                  TabIndex        =   71
                  Top             =   460
                  Width           =   615
               End
            End
            Begin VB.TextBox txtObserv 
               Height          =   315
               Index           =   2
               Left            =   8340
               MaxLength       =   2000
               MultiLine       =   -1  'True
               TabIndex        =   68
               Top             =   2250
               Width           =   2655
            End
            Begin VB.CommandButton cmdObserv 
               Caption         =   "..."
               Height          =   315
               Index           =   2
               Left            =   11040
               TabIndex        =   67
               Top             =   2250
               Width           =   375
            End
            Begin SSDataWidgets_B.SSDBGrid grdHoras 
               Height          =   1750
               Index           =   2
               Left            =   7695
               TabIndex        =   76
               Top             =   125
               Width           =   3750
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RecordSelectors =   0   'False
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   3200
               _ExtentX        =   6615
               _ExtentY        =   3087
               _StockProps     =   79
               Caption         =   " "
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSMonth mthHuecos 
               Height          =   2415
               Index           =   2
               Left            =   3135
               TabIndex        =   77
               Top             =   125
               Width           =   4545
               _Version        =   65537
               _ExtentX        =   8017
               _ExtentY        =   4260
               _StockProps     =   76
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "2000/1/1"
               MaxDate         =   "2999/12/31"
               stylesets.count =   3
               stylesets(0).Name=   "normal"
               stylesets(0).Picture=   "AG0210.frx":0000
               stylesets(1).Name=   "hueco_comun"
               stylesets(1).BackColor=   8454016
               stylesets(1).Picture=   "AG0210.frx":001C
               stylesets(2).Name=   "hueco"
               stylesets(2).BackColor=   16744576
               stylesets(2).Picture=   "AG0210.frx":0038
               StartofWeek     =   2
               NumberOfMonths  =   2
               ShowCentury     =   -1  'True
            End
            Begin SSDataWidgets_B.SSDBCombo cboRec 
               Height          =   315
               Index           =   2
               Left            =   690
               TabIndex        =   8
               Top             =   2215
               Width           =   2385
               DataFieldList   =   "Column 1"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               DividerType     =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "Cod"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3704
               Columns(1).Caption=   "Rec"
               Columns(1).Name =   "Rec"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Cal"
               Columns(2).Name =   "Cal"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "Dr"
               Columns(3).Name =   "Dr"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   4207
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdAct 
               Height          =   795
               Index           =   2
               Left            =   630
               TabIndex        =   94
               Top             =   600
               Visible         =   0   'False
               Width           =   2445
               _Version        =   131078
               DataMode        =   2
               RecordSelectors =   0   'False
               GroupHeaders    =   0   'False
               ColumnHeaders   =   0   'False
               Col.Count       =   5
               DividerType     =   0
               DividerStyle    =   0
               AllowUpdate     =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   0
               ForeColorEven   =   0
               BackColorEven   =   12632256
               BackColorOdd    =   12632256
               RowHeight       =   423
               Columns.Count   =   5
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "Cod"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   12356
               Columns(1).Caption=   "Act"
               Columns(1).Name =   "Act"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Min"
               Columns(2).Name =   "Min"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "NAPLan"
               Columns(3).Name =   "NAPLan"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   3200
               Columns(4).Visible=   0   'False
               Columns(4).Caption=   "ReqDoc"
               Columns(4).Name =   "ReqDoc"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               _ExtentX        =   4313
               _ExtentY        =   1402
               _StockProps     =   79
            End
            Begin VB.Line lnLine 
               BorderWidth     =   2
               Index           =   2
               X1              =   540
               X2              =   11340
               Y1              =   60
               Y2              =   60
            End
            Begin VB.Label lblHora 
               Caption         =   "Hora:"
               Height          =   240
               Index           =   2
               Left            =   7800
               TabIndex        =   81
               Top             =   1990
               Width           =   375
            End
            Begin VB.Label lblRec 
               Alignment       =   1  'Right Justify
               Caption         =   "Recurso:"
               Height          =   250
               Index           =   2
               Left            =   15
               TabIndex        =   80
               Top             =   2255
               Width           =   660
            End
            Begin VB.Label lblNivel 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "3"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   255
               Index           =   2
               Left            =   60
               TabIndex        =   79
               Top             =   0
               Width           =   435
            End
            Begin VB.Label lblObserv 
               Caption         =   "Observ:"
               Height          =   195
               Index           =   2
               Left            =   7740
               TabIndex        =   78
               Top             =   2310
               Width           =   555
            End
         End
         Begin VB.Frame fraNivel 
            BorderStyle     =   0  'None
            Height          =   2600
            Index           =   1
            Left            =   0
            TabIndex        =   50
            Top             =   2700
            Width           =   11475
            Begin SSDataWidgets_B.SSDBGrid grdAct 
               Height          =   795
               Index           =   1
               Left            =   630
               TabIndex        =   93
               Top             =   600
               Visible         =   0   'False
               Width           =   2445
               _Version        =   131078
               DataMode        =   2
               RecordSelectors =   0   'False
               GroupHeaders    =   0   'False
               ColumnHeaders   =   0   'False
               Col.Count       =   5
               DividerType     =   0
               DividerStyle    =   0
               AllowUpdate     =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   0
               ForeColorEven   =   0
               BackColorEven   =   12632256
               BackColorOdd    =   12632256
               RowHeight       =   423
               Columns.Count   =   5
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "Cod"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   12356
               Columns(1).Caption=   "Act"
               Columns(1).Name =   "Act"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Min"
               Columns(2).Name =   "Min"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "NAPLan"
               Columns(3).Name =   "NAPLan"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   3200
               Columns(4).Visible=   0   'False
               Columns(4).Caption=   "ReqDoc"
               Columns(4).Name =   "ReqDoc"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               _ExtentX        =   4313
               _ExtentY        =   1402
               _StockProps     =   79
            End
            Begin VB.TextBox txtRecSel 
               BackColor       =   &H00C0C0C0&
               Height          =   315
               Index           =   1
               Left            =   8760
               Locked          =   -1  'True
               TabIndex        =   59
               Top             =   1910
               Width           =   2655
            End
            Begin VB.Frame fraHora 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   360
               Index           =   1
               Left            =   8220
               TabIndex        =   57
               Top             =   1910
               Width           =   555
               Begin MSMask.MaskEdBox mskHora 
                  Height          =   315
                  Index           =   1
                  Left            =   0
                  TabIndex        =   58
                  Top             =   0
                  Width           =   555
                  _ExtentX        =   979
                  _ExtentY        =   556
                  _Version        =   327681
                  MaxLength       =   5
                  Mask            =   "##:##"
                  PromptChar      =   "_"
               End
            End
            Begin VB.Frame fraAct 
               BorderStyle     =   0  'None
               Height          =   1920
               Index           =   1
               Left            =   0
               TabIndex        =   53
               Top             =   250
               Width           =   3135
               Begin VB.TextBox txtActiv 
                  Height          =   285
                  Index           =   1
                  Left            =   0
                  TabIndex        =   54
                  Top             =   120
                  Visible         =   0   'False
                  Width           =   195
               End
               Begin SSDataWidgets_B.SSDBCombo cboDpto 
                  Height          =   315
                  Index           =   1
                  Left            =   630
                  TabIndex        =   3
                  Top             =   0
                  Width           =   2445
                  DataFieldList   =   "Column 1"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  DividerType     =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "Cod"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3704
                  Columns(1).Caption=   "Dpto"
                  Columns(1).Name =   "Dpto"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4322
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cboAct 
                  Height          =   315
                  Index           =   1
                  Left            =   630
                  TabIndex        =   4
                  Top             =   400
                  Width           =   2445
                  DataFieldList   =   "Column 1"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  DividerType     =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   5
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "Cod"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3704
                  Columns(1).Caption=   "Act"
                  Columns(1).Name =   "Act"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "Min"
                  Columns(2).Name =   "Min"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   3200
                  Columns(3).Visible=   0   'False
                  Columns(3).Caption=   "NAPlan"
                  Columns(3).Name =   "NAPlan"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   3200
                  Columns(4).Visible=   0   'False
                  Columns(4).Caption=   "ReqDoc"
                  Columns(4).Name =   "ReqDoc"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  _ExtentX        =   4322
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblTOPac 
                  Caption         =   "T.O. Pac:"
                  Height          =   255
                  Index           =   1
                  Left            =   630
                  TabIndex        =   87
                  Top             =   1390
                  Width           =   2475
               End
               Begin VB.Label lblTORec 
                  Caption         =   "T.O. Rec:"
                  Height          =   255
                  Index           =   1
                  Left            =   630
                  TabIndex        =   86
                  Top             =   1150
                  Width           =   2475
               End
               Begin VB.Label lblFecPlan 
                  Caption         =   "Fecha Plan:"
                  Height          =   255
                  Index           =   1
                  Left            =   630
                  TabIndex        =   85
                  Top             =   1630
                  Width           =   2475
               End
               Begin VB.Label lblDpto 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Dpto:"
                  Height          =   255
                  Index           =   1
                  Left            =   180
                  TabIndex        =   56
                  Top             =   75
                  Width           =   420
               End
               Begin VB.Label lblAct 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Prueba:"
                  Height          =   255
                  Index           =   1
                  Left            =   0
                  TabIndex        =   55
                  Top             =   460
                  Width           =   615
               End
            End
            Begin VB.TextBox txtObserv 
               Height          =   315
               Index           =   1
               Left            =   8340
               MaxLength       =   2000
               MultiLine       =   -1  'True
               TabIndex        =   52
               Top             =   2250
               Width           =   2655
            End
            Begin VB.CommandButton cmdObserv 
               Caption         =   "..."
               Height          =   315
               Index           =   1
               Left            =   11040
               TabIndex        =   51
               Top             =   2250
               Width           =   375
            End
            Begin SSDataWidgets_B.SSDBGrid grdHoras 
               Height          =   1750
               Index           =   1
               Left            =   7695
               TabIndex        =   60
               Top             =   125
               Width           =   3750
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RecordSelectors =   0   'False
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   3200
               _ExtentX        =   6615
               _ExtentY        =   3087
               _StockProps     =   79
               Caption         =   " "
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSMonth mthHuecos 
               Height          =   2415
               Index           =   1
               Left            =   3135
               TabIndex        =   61
               Top             =   125
               Width           =   4545
               _Version        =   65537
               _ExtentX        =   8017
               _ExtentY        =   4260
               _StockProps     =   76
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "2000/1/1"
               MaxDate         =   "2999/12/31"
               stylesets.count =   3
               stylesets(0).Name=   "normal"
               stylesets(0).Picture=   "AG0210.frx":0054
               stylesets(1).Name=   "hueco_comun"
               stylesets(1).BackColor=   8454016
               stylesets(1).Picture=   "AG0210.frx":0070
               stylesets(2).Name=   "hueco"
               stylesets(2).BackColor=   16744576
               stylesets(2).Picture=   "AG0210.frx":008C
               StartofWeek     =   2
               NumberOfMonths  =   2
               ShowCentury     =   -1  'True
            End
            Begin SSDataWidgets_B.SSDBCombo cboRec 
               Height          =   315
               Index           =   1
               Left            =   690
               TabIndex        =   5
               Top             =   2215
               Width           =   2385
               DataFieldList   =   "Column 1"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               DividerType     =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "Cod"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3704
               Columns(1).Caption=   "Rec"
               Columns(1).Name =   "Rec"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Cal"
               Columns(2).Name =   "Cal"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "Dr"
               Columns(3).Name =   "Dr"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   4207
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Line lnLine 
               BorderWidth     =   2
               Index           =   1
               X1              =   540
               X2              =   11340
               Y1              =   60
               Y2              =   60
            End
            Begin VB.Label lblHora 
               Caption         =   "Hora:"
               Height          =   240
               Index           =   1
               Left            =   7800
               TabIndex        =   65
               Top             =   1990
               Width           =   375
            End
            Begin VB.Label lblRec 
               Alignment       =   1  'Right Justify
               Caption         =   "Recurso:"
               Height          =   250
               Index           =   1
               Left            =   15
               TabIndex        =   64
               Top             =   2255
               Width           =   660
            End
            Begin VB.Label lblNivel 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "2"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   255
               Index           =   1
               Left            =   60
               TabIndex        =   63
               Top             =   0
               Width           =   435
            End
            Begin VB.Label lblObserv 
               Caption         =   "Observ:"
               Height          =   195
               Index           =   1
               Left            =   7740
               TabIndex        =   62
               Top             =   2310
               Width           =   555
            End
         End
         Begin VB.VScrollBar vscNivel 
            Height          =   5850
            LargeChange     =   2
            Left            =   11460
            Max             =   10
            TabIndex        =   44
            Top             =   100
            Width           =   240
         End
         Begin VB.Frame fraNivel 
            BorderStyle     =   0  'None
            Height          =   2600
            Index           =   0
            Left            =   0
            TabIndex        =   33
            Top             =   100
            Width           =   11475
            Begin SSDataWidgets_B.SSDBGrid grdAct 
               Height          =   795
               Index           =   0
               Left            =   630
               TabIndex        =   92
               Top             =   600
               Visible         =   0   'False
               Width           =   2445
               _Version        =   131078
               DataMode        =   2
               RecordSelectors =   0   'False
               GroupHeaders    =   0   'False
               ColumnHeaders   =   0   'False
               Col.Count       =   4
               DividerType     =   0
               DividerStyle    =   0
               AllowUpdate     =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   0
               ForeColorEven   =   0
               BackColorEven   =   12632256
               BackColorOdd    =   12632256
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "Cod"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   12356
               Columns(1).Caption=   "Act"
               Columns(1).Name =   "Act"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Min"
               Columns(2).Name =   "Min"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "NAPlan"
               Columns(3).Name =   "NAPlan"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   4313
               _ExtentY        =   1402
               _StockProps     =   79
            End
            Begin VB.CommandButton cmdObserv 
               Caption         =   "..."
               Height          =   315
               Index           =   0
               Left            =   11040
               TabIndex        =   48
               Top             =   2250
               Width           =   375
            End
            Begin VB.TextBox txtObserv 
               Height          =   315
               Index           =   0
               Left            =   8340
               MaxLength       =   2000
               MultiLine       =   -1  'True
               TabIndex        =   47
               Top             =   2250
               Width           =   2655
            End
            Begin VB.Frame fraAct 
               BorderStyle     =   0  'None
               Height          =   1920
               Index           =   0
               Left            =   0
               TabIndex        =   37
               Top             =   250
               Width           =   3135
               Begin VB.TextBox txtActiv 
                  Height          =   285
                  Index           =   0
                  Left            =   0
                  TabIndex        =   49
                  Top             =   120
                  Visible         =   0   'False
                  Width           =   195
               End
               Begin SSDataWidgets_B.SSDBCombo cboDpto 
                  Height          =   315
                  Index           =   0
                  Left            =   630
                  TabIndex        =   0
                  Top             =   0
                  Width           =   2445
                  DataFieldList   =   "Column 1"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  DividerType     =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "Cod"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3704
                  Columns(1).Caption=   "Dpto"
                  Columns(1).Name =   "Dpto"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4322
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cboAct 
                  Height          =   315
                  Index           =   0
                  Left            =   630
                  TabIndex        =   1
                  Top             =   400
                  Width           =   2445
                  DataFieldList   =   "Column 1"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  DividerType     =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   5
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "Cod"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3704
                  Columns(1).Caption=   "Act"
                  Columns(1).Name =   "Act"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "Min"
                  Columns(2).Name =   "Min"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   3200
                  Columns(3).Visible=   0   'False
                  Columns(3).Caption=   "NAPlan"
                  Columns(3).Name =   "NAPlan"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   3200
                  Columns(4).Visible=   0   'False
                  Columns(4).Caption=   "ReqDoc"
                  Columns(4).Name =   "ReqDoc"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  _ExtentX        =   4322
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblTOPac 
                  Caption         =   "T.O. Pac:"
                  Height          =   255
                  Index           =   0
                  Left            =   630
                  TabIndex        =   84
                  Top             =   1390
                  Width           =   2475
               End
               Begin VB.Label lblTORec 
                  Caption         =   "T.O. Rec:"
                  Height          =   255
                  Index           =   0
                  Left            =   630
                  TabIndex        =   83
                  Top             =   1150
                  Width           =   2475
               End
               Begin VB.Label lblFecPlan 
                  Caption         =   "Fecha Plan:"
                  Height          =   255
                  Index           =   0
                  Left            =   630
                  TabIndex        =   82
                  Top             =   1630
                  Width           =   2475
               End
               Begin VB.Label lblAct 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Prueba:"
                  Height          =   255
                  Index           =   0
                  Left            =   0
                  TabIndex        =   39
                  Top             =   460
                  Width           =   615
               End
               Begin VB.Label lblDpto 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Dpto:"
                  Height          =   255
                  Index           =   0
                  Left            =   180
                  TabIndex        =   38
                  Top             =   75
                  Width           =   420
               End
            End
            Begin VB.Frame fraHora 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   360
               Index           =   0
               Left            =   8220
               TabIndex        =   35
               Top             =   1910
               Width           =   555
               Begin MSMask.MaskEdBox mskHora 
                  Height          =   315
                  Index           =   0
                  Left            =   0
                  TabIndex        =   36
                  Top             =   0
                  Width           =   555
                  _ExtentX        =   979
                  _ExtentY        =   556
                  _Version        =   327681
                  MaxLength       =   5
                  Mask            =   "##:##"
                  PromptChar      =   "_"
               End
            End
            Begin VB.TextBox txtRecSel 
               BackColor       =   &H00C0C0C0&
               Height          =   315
               Index           =   0
               Left            =   8760
               Locked          =   -1  'True
               TabIndex        =   34
               Top             =   1910
               Width           =   2655
            End
            Begin SSDataWidgets_B.SSDBGrid grdHoras 
               Height          =   1750
               Index           =   0
               Left            =   7695
               TabIndex        =   40
               Top             =   125
               Width           =   3750
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RecordSelectors =   0   'False
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   3200
               _ExtentX        =   6615
               _ExtentY        =   3087
               _StockProps     =   79
               Caption         =   " "
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSMonth mthHuecos 
               Height          =   2415
               Index           =   0
               Left            =   3135
               TabIndex        =   41
               Top             =   125
               Width           =   4545
               _Version        =   65537
               _ExtentX        =   8017
               _ExtentY        =   4260
               _StockProps     =   76
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "2000/1/1"
               MaxDate         =   "2999/12/31"
               stylesets.count =   3
               stylesets(0).Name=   "normal"
               stylesets(0).Picture=   "AG0210.frx":00A8
               stylesets(1).Name=   "hueco_comun"
               stylesets(1).BackColor=   8454016
               stylesets(1).Picture=   "AG0210.frx":00C4
               stylesets(2).Name=   "hueco"
               stylesets(2).BackColor=   16744576
               stylesets(2).Picture=   "AG0210.frx":00E0
               StartofWeek     =   2
               NumberOfMonths  =   2
               ShowCentury     =   -1  'True
            End
            Begin SSDataWidgets_B.SSDBCombo cboRec 
               Height          =   315
               Index           =   0
               Left            =   690
               TabIndex        =   2
               Top             =   2215
               Width           =   2385
               DataFieldList   =   "Column 1"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               DividerType     =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "Cod"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3704
               Columns(1).Caption=   "Rec"
               Columns(1).Name =   "Rec"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Cal"
               Columns(2).Name =   "Cal"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "Dr"
               Columns(3).Name =   "Dr"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   4207
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblObserv 
               Caption         =   "Observ:"
               Height          =   195
               Index           =   0
               Left            =   7740
               TabIndex        =   46
               Top             =   2310
               Width           =   555
            End
            Begin VB.Label lblNivel 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "1"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   255
               Index           =   0
               Left            =   60
               TabIndex        =   45
               Top             =   0
               Width           =   435
            End
            Begin VB.Label lblRec 
               Alignment       =   1  'Right Justify
               Caption         =   "Recurso:"
               Height          =   250
               Index           =   0
               Left            =   15
               TabIndex        =   43
               Top             =   2255
               Width           =   660
            End
            Begin VB.Label lblHora 
               Caption         =   "Hora:"
               Height          =   240
               Index           =   0
               Left            =   7800
               TabIndex        =   42
               Top             =   1990
               Width           =   375
            End
            Begin VB.Line lnLine 
               BorderWidth     =   2
               Index           =   0
               X1              =   540
               X2              =   11340
               Y1              =   60
               Y2              =   60
            End
         End
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Citas previstas del paciente"
      ForeColor       =   &H00C00000&
      Height          =   1530
      Left            =   4920
      TabIndex        =   28
      Top             =   0
      Width           =   6975
      Begin SSDataWidgets_B.SSDBGrid grdCitasPac 
         Height          =   1260
         Left            =   90
         TabIndex        =   29
         Top             =   200
         Width           =   6810
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   12012
         _ExtentY        =   2222
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdLimpiar 
      Caption         =   "&Limpiar"
      Height          =   390
      Left            =   9900
      TabIndex        =   27
      Top             =   1860
      Visible         =   0   'False
      Width           =   870
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Citar"
      Height          =   390
      Left            =   6180
      TabIndex        =   24
      Top             =   1860
      Width           =   870
   End
   Begin VB.Frame Frame3 
      Caption         =   "Huecos"
      ForeColor       =   &H00C00000&
      Height          =   675
      Left            =   60
      TabIndex        =   18
      Top             =   1560
      Width           =   6015
      Begin VB.CommandButton cmdHuecos 
         Caption         =   "B&uscar"
         Height          =   345
         Left            =   4860
         TabIndex        =   23
         Top             =   240
         Width           =   915
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecDesde 
         Height          =   315
         Left            =   900
         TabIndex        =   19
         Top             =   240
         Width           =   1515
         _Version        =   65537
         _ExtentX        =   2672
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecHasta 
         Height          =   315
         Left            =   3180
         TabIndex        =   20
         Top             =   240
         Width           =   1515
         _Version        =   65537
         _ExtentX        =   2672
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Index           =   2
         Left            =   300
         TabIndex        =   22
         Top             =   300
         Width           =   555
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Index           =   3
         Left            =   2640
         TabIndex        =   21
         Top             =   300
         Width           =   495
      End
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   390
      Left            =   10920
      TabIndex        =   11
      Top             =   1860
      Width           =   870
   End
   Begin VB.Frame Frame1 
      Caption         =   "Paciente"
      ForeColor       =   &H00C00000&
      Height          =   1530
      Left            =   60
      TabIndex        =   10
      Top             =   0
      Width           =   4815
      Begin VB.CommandButton cmdHojaFiliacion 
         Caption         =   "&Hoja de Filiaci�n"
         Enabled         =   0   'False
         Height          =   495
         Left            =   3840
         TabIndex        =   91
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton cmdVisionGlobal 
         Caption         =   "&Visi�n Global"
         Enabled         =   0   'False
         Height          =   495
         Left            =   2880
         TabIndex        =   30
         Top             =   300
         Width           =   855
      End
      Begin VB.Frame fraPac 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   1275
         Left            =   60
         TabIndex        =   12
         Top             =   180
         Width           =   4695
         Begin VB.TextBox txtCodPers 
            Height          =   315
            Left            =   60
            TabIndex        =   13
            Top             =   540
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtTipoEcon 
            Height          =   315
            Left            =   675
            TabIndex        =   17
            Top             =   540
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtSexo 
            Height          =   315
            Left            =   360
            TabIndex        =   16
            Top             =   540
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.CommandButton cmdBuscarPac 
            Caption         =   "&Buscar"
            Height          =   345
            Left            =   1740
            TabIndex        =   15
            Top             =   240
            Width           =   810
         End
         Begin VB.TextBox txtPac 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Left            =   720
            Locked          =   -1  'True
            TabIndex        =   14
            Top             =   840
            Width           =   3930
         End
         Begin VB.TextBox txtNH 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Left            =   720
            Locked          =   -1  'True
            TabIndex        =   9
            Top             =   240
            Width           =   795
         End
         Begin VB.Label Label1 
            Caption         =   "Nombre:"
            Height          =   255
            Index           =   1
            Left            =   60
            TabIndex        =   26
            Top             =   900
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "N� Hist.:"
            Height          =   255
            Index           =   0
            Left            =   60
            TabIndex        =   25
            Top             =   300
            Width           =   615
         End
      End
   End
   Begin VsOcxLib.VideoSoftAwk awk 
      Left            =   11460
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   " |"
   End
End
Attribute VB_Name = "frmCitacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strAhora$
Dim intTipoAcceso%
Dim strNH$
Dim strFecIni$, strFecFin$, intNDias% 'fechas de inicio y fin y n� de d�as a procesar
Dim blnCancel As Boolean 'para detectar si el usuario ha cancelado la b�squeda de huecos
Dim lngNPet& 'para poder asociar consultas cuando se va a citar un Petici�n o una Prueba
Dim cllNAPlan As New Collection 'para poder identificar entre las citas del paciente las que se est�n recitando

Dim arHuecos() As typeCHDias 'estructura que contiene los huecos
Dim cllFechasNoFiesta As New Collection 'fechas que hay que trabajar
Dim cllFechasFiesta As New Collection  'fechas especiales festivas
Dim cllDiasFiesta As New Collection  'd�as de la semana festivos
Dim cllPerfilesRec As New Collection 'perfiles vigentes del recurso (CodPerf|FecIni|FecFin)
Dim cllRestricRec As New Collection 'franjas restringidas del recurso (CodPerf|CodFrja)
Dim arFranjas() As typeFranjas 'franjas del recurso
Dim arCitas() As typeCitas 'citas actuales del recurso
Dim cllDptoUser As New Collection 'colecci�n de Dptos a los que tiene acceso el usuario

'Constantes
Private Const constNIVEL_NUMINI As Integer = 3
Private Const constNIVEL_TOP As Long = 100
Private Const constNIVEL_HEIGHT As Long = 2600

Private Sub cboAct_Click(Index As Integer)
    'se muestra en lblTORec(Index) la duraci�n de la actuaci�n
    If cboAct(Index).Text <> "" Then
        lblTORec(Index).Caption = "T.O. Rec: " & cboAct(Index).Columns("Min").Text & " min."
        lblTOPac(Index).Caption = "T.O. Pac: " & fTOcupPac(cboAct(Index).Columns("Cod").Text) & " min."
    Else
        lblTORec(Index).Caption = "T.O. Rec:"
        lblTOPac(Index).Caption = "T.O. Pac:"
    End If
    Call pCargarRecursosConsultas(Index)
    Call pLimpiarHuecos
End Sub

Private Sub cboAct_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Dim strFecha$
    If KeyCode = 46 Then
        cboAct(Index).Text = ""
        lblTORec(Index).Caption = "T.O. Rec:": lblTOPac(Index).Caption = "T.O. Pac:": lblFecPlan(Index).Caption = "Fecha Plan:"
        cboRec(Index).RemoveAll: cboRec(Index).Text = ""
        'se vac�an los grids de huecos
        grdHoras(Index).RemoveAll
        'se quitan los style de los calendarios
        If strFecIni <> "" Then
          strFecha = strFecIni
          Do While CDate(strFecha) <= CDate(strFecFin)
              mthHuecos(Index).X.Day(strFecha).StyleSet = "normal"
              strFecha = DateAdd("d", 1, strFecha)
          Loop
        End If
        mthHuecos(Index).MinDate = Format(strAhora, "dd/mm/yyyy")
        mthHuecos(Index).MaxDate = Format(DateAdd("yyyy", 3, strAhora), "dd/mm/yyyy")
        mthHuecos(Index).Date = mthHuecos(Index).MinDate
        'se quitan los datos del hueco (hora - recurso) seleccionado para la cita
        mskHora(Index).BackColor = objApp.objUserColor.lngNormal
        mskHora(Index).Text = "__:__"
        fraHora(Index).Enabled = False
        txtRecSel(Index).Text = ""
        txtObserv(Index).Text = ""
    End If
End Sub

Private Sub cboDpto_Click(Index As Integer)
    Call pCargarActuaciones(Index)
    Call pLimpiarHuecos
End Sub

Private Sub cboRec_Click(Index As Integer)
    Call pLimpiarHuecos
End Sub

Private Sub cboRec_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboRec(Index).Text = ""
End Sub

Private Sub cmdBuscarPac_Click()
    frmBuscaPersonas.Show vbModal
    Set frmBuscaPersonas = Nothing
    If objPipe.PipeExist("AG0214_CodPersona") Then
        Call pCargarPaciente(objPipe.PipeGet("AG0214_CodPersona"))
        Call objPipe.PipeRemove("AG0214_CodPersona")
        Call objPipe.PipeRemove("AG0214_NumHistoria")
    End If
End Sub

Private Sub cmdCitar_Click()
    If fDatosCitaCorrectos Then
        If Not fSolapamientoCitasPac Then
            If fHuecosSelTodaviaActivos Then
                Call pCitar
            End If
        End If
    End If
End Sub

Private Sub cmdHojaFiliacion_Click()
    If txtCodPers.Text <> "" Then
        Screen.MousePointer = vbHourglass
        Call objSecurity.LaunchProcess("CI4017", txtCodPers.Text)
        Screen.MousePointer = vbDefault
    End If
End Sub

Private Sub cmdHuecos_Click()
    Call pBuscarHuecos
End Sub

Private Sub cmdImprimir_Click()
    Call pImprimirCitas
End Sub

Private Sub cmdLimpiar_Click()
    Call pLimpiarPantalla
End Sub

Private Sub cmdObserv_Click(Index As Integer)
    Call objPipe.PipeSet("AG0213_OBSERV", txtObserv(Index).Text)
    Call objPipe.PipeSet("AG0213_USERCOLOR", txtObserv(Index).BackColor)
    frmObserv.Show vbModal
    Set frmObserv = Nothing
    txtObserv(Index).Text = objPipe.PipeGet("AG0213_OBSERV")
    On Error Resume Next
    Call objPipe.PipeRemove("AG0213_OBSERV")
    Call objPipe.PipeRemove("AG0213_USERCOLOR")
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdVisionGlobal_Click()
    Dim vntData()
    If txtCodPers.Text <> "" Then
        Screen.MousePointer = vbHourglass
        ReDim vntData(1 To 2) As Variant
        vntData(1) = txtCodPers.Text
        vntData(2) = 2
        Call objSecurity.LaunchProcess("HC02", vntData())
        Screen.MousePointer = vbDefault
    End If
End Sub

Private Sub dcboFecDesde_Change()
    On Error Resume Next
    If dcboFecDesde.Date <> "" Then
        If CDate(dcboFecHasta.Date) < CDate(dcboFecDesde.Date) Then
            dcboFecHasta.Date = dcboFecDesde.Date
            If Err > 0 Then dcboFecDesde.Date = dcboFecHasta.Date
        End If
        Call pLimpiarHuecos
    End If
End Sub

Private Sub dcboFecDesde_Click()
    On Error Resume Next
    If dcboFecDesde.Date <> "" Then
        If CDate(dcboFecHasta.Date) < CDate(dcboFecDesde.Date) Then
            dcboFecHasta.Date = dcboFecDesde.Date
            If Err > 0 Then dcboFecDesde.Date = dcboFecHasta.Date
        End If
        Call pLimpiarHuecos
    End If
End Sub

Private Sub dcboFecHasta_Change()
    On Error Resume Next
    If dcboFecHasta.Date <> "" Then
        If CDate(dcboFecHasta.Date) < CDate(dcboFecDesde.Date) Then
            dcboFecDesde.Date = dcboFecHasta.Date
            If Err > 0 Then dcboFecHasta.Date = dcboFecDesde.Date
        End If
        Call pLimpiarHuecos
    End If
End Sub

Private Sub dcboFecHasta_Click()
    On Error Resume Next
    If dcboFecHasta.Date <> "" Then
        If CDate(dcboFecHasta.Date) < CDate(dcboFecDesde.Date) Then
            dcboFecDesde.Date = dcboFecHasta.Date
            If Err > 0 Then dcboFecHasta.Date = dcboFecDesde.Date
        End If
    End If
    Call pLimpiarHuecos
End Sub

Private Sub Form_Load()
    
    Call pFormatearControles
    vscNivel.Max = (fraNivel.Count - constNIVEL_NUMINI + 1) * 2

    strAhora = fAhora
    dcboFecDesde.MinDate = Format(strAhora, "dd/mm/yyyy")
    dcboFecDesde.MaxDate = Format(DateAdd("yyyy", 3, strAhora), "dd/mm/yyyy")
    dcboFecDesde.Date = dcboFecDesde.MinDate
    dcboFecHasta.MinDate = dcboFecDesde.MinDate
    dcboFecHasta.MaxDate = dcboFecDesde.MaxDate
    dcboFecHasta.Date = Format(DateAdd("d", 30, strAhora), "dd/mm/yyyy")
        
    Call pCargarDptoUser

    'Entrada para una recita
    If objPipe.PipeExist("AG0210_PR04NUMACTPLAN") Then
        intTipoAcceso = constACCESOCPH_NAPLAN
        Call pEntradaActPlan(objPipe.PipeGet("AG0210_PR04NUMACTPLAN"))
        Call objPipe.PipeRemove("AG0210_PR04NUMACTPLAN")
        Exit Sub
    End If
    
    'Entrada para citar una petici�n
    If objPipe.PipeExist("AG0210_PR09NUMPETICION") Then
        intTipoAcceso = constACCESOCPH_PETICION
        Call pEntradaPeticion(objPipe.PipeGet("AG0210_PR09NUMPETICION"))
        Call objPipe.PipeRemove("AG0210_PR09NUMPETICION")
        Exit Sub
    End If
    
    'Entrada con una persona
    If objPipe.PipeExist("AG0210_CI21CODPERSONA") Then
        intTipoAcceso = constACCESOCPH_PERSONA
        Call pEntradaPersona(objPipe.PipeGet("AG0210_CI21CODPERSONA"))
        Call objPipe.PipeRemove("AG0210_CI21CODPERSONA")
        Exit Sub
    End If
    
    'Entrada para pedir y citar consultas
    intTipoAcceso = constACCESOCPH_LIBRE
    Call pEntradaLibre(0)
    cmdLimpiar.Visible = True
    cmdImprimir.Visible = True
    txtNH.BackColor = objApp.objUserColor.lngNormal
    txtNH.Locked = False
End Sub

Private Sub grdHoras_Click(Index As Integer)
    If grdHoras(Index).Rows > 0 Then
        'se muestra el hueco y el recurso seleccionado para la cita
        grdHoras(Index).SelBookmarks.RemoveAll
        grdHoras(Index).SelBookmarks.Add (grdHoras(Index).RowBookmark(grdHoras(Index).Row))
        mskHora(Index).Text = grdHoras(Index).Columns("De").Text
        txtRecSel(Index).Text = grdHoras(Index).Columns("Recurso").Text
        'se formatea el hueco seleccionado seg�n el tipo de citaci�n...
        If grdHoras(Index).Columns("A").Text = "" Then '... por cantidad, intervalos o intervalos oscilantes
            mskHora(Index).BackColor = objApp.objUserColor.lngReadOnly
            fraHora(Index).Enabled = False
        Else '... secuencial
            mskHora(Index).BackColor = objApp.objUserColor.lngMandatory
            fraHora(Index).Enabled = True
        End If
    End If
End Sub

Private Sub grdHoras_HeadClick(Index As Integer, ByVal ColIndex As Integer)
    'se ordena el grdHoras por Horas o por Recursos
    Select Case ColIndex
    Case 0, 1: Call pVerHorasHuecosPorHora(mthHuecos(Index).Date, Index)
    Case 3: Call pVerHorasHuecosPorRecurso(mthHuecos(Index).Date, Index)
    End Select
End Sub

Private Sub lblNivel_DblClick(Index As Integer)
    Screen.MousePointer = vbHourglass
    Call pCrearNivel
    If lngNPet > 0 Then Call pEntradaLibre(lblNivel.Count - 1) 'Entrada por NAPlan o Petici�n
    vscNivel_Change
    Screen.MousePointer = vbDefault
End Sub

Private Sub mskHora_GotFocus(Index As Integer)
    mskHora(Index).SelStart = 0
    mskHora(Index).SelLength = Len(mskHora(Index).Text)
End Sub

Private Sub mskHora_LostFocus(Index As Integer)
    Dim msg$, strHI$, strHF$
    
    'se comprueba la hora establecidad para la cita
    If mskHora(Index).Text <> "__:__" Then
        mskHora(Index).Text = objGen.ReplaceStr(mskHora(Index).Text, "_", "0", 0)
        If Not IsDate(mskHora(Index).Text) Then 'no es una hora correcta
            mskHora(Index).Text = "__:__"
            mskHora(Index).SetFocus
        Else
            strHI = grdHoras(Index).Columns("De").CellText(grdHoras(Index).SelBookmarks(0))
            strHF = grdHoras(Index).Columns("A").CellText(grdHoras(Index).SelBookmarks(0))
            'si la citaci�n es secuencial se comprueba la cita (hora inicio + duraci�n) _
            encaje en el hueco
            If strHF <> "" Then
                If CDate(mskHora(Index).Text) < CDate(strHI) _
                Or CDate(DateAdd("n", Val(Mid$(lblTORec(Index).Caption, 11)), mskHora(Index).Text)) > CDate(strHF) Then
                    msg = "La hora de la cita es incorrecta."
                    msg = msg & Chr$(13) & Chr$(13)
                    msg = msg & "Tenga en cuenta que el hueco para citar va de las "
                    msg = msg & strHI & " a las " & strHF & " y la duraci�n de la actuaci�n "
                    msg = msg & "es de " & Mid$(lblTORec(Index).Caption, 9)
                    MsgBox msg, vbExclamation, Me.Caption
                    mskHora(Index).Text = "__:__"
                    mskHora(Index).SetFocus
                End If
            End If
        End If
    End If
End Sub

Private Sub mthHuecos_SelChange(Index As Integer, SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
    'se muestran los huecos del d�a y actuaci�n seleccionados
    Call pVerHorasHuecosPorHora(SelDate, Index)
End Sub

Private Sub txtNH_LostFocus()
    If strNH <> txtNH.Text Then
        If txtNH.Text = "" Then
            txtCodPers.Text = ""
            txtPac.Text = ""
            txtSexo.Text = ""
            txtTipoEcon.Text = ""
            grdCitasPac.RemoveAll
            cmdVisionGlobal.Enabled = False
            cmdHojaFiliacion.Enabled = False
        Else
            Call pCargarPaciente(txtNH.Text, True)
        End If
    End If
End Sub

Private Sub txtNH_GotFocus()
    txtNH.SelStart = 0
    txtNH.SelLength = Len(txtNH.Text)
End Sub

Private Sub txtNH_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case 8
    Case 13: SendKeys "{TAB}"
    Case Is < Asc("0"), Is > Asc("9"): KeyAscii = 0
    End Select
End Sub

Private Sub vscNivel_Change()
    Dim i%
    
    vscNivel.Max = (fraNivel.Count - constNIVEL_NUMINI + 1) * 2
    For i = 0 To fraNivel.Count - 1
        fraNivel(i).Top = constNIVEL_TOP + i * constNIVEL_HEIGHT - vscNivel.Value * constNIVEL_HEIGHT / 2
    Next i
End Sub

Public Function fblnGenerarCita(strNumSolicit$, intNSecCita%, iAct%, lngNAPlan&, strCodAct$) As Boolean
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strCodDpto$, strCodRec$, strFechaCita$, strObserv$
    Dim lngTORec&, lngTOPac&
    
    lngTORec = Val(Mid$(lblTORec(iAct).Caption, 11))
    lngTOPac = Val(Mid$(lblTOPac(iAct).Caption, 11))
    strCodDpto = cboDpto(iAct).Columns("Cod").Text
    strCodRec = grdHoras(iAct).Columns("CodRec").CellText(grdHoras(iAct).SelBookmarks(0))
    strFechaCita = mthHuecos(iAct).Date & " " & mskHora(iAct).Text
    strObserv = Left$(txtObserv(iAct).Text, 255)

    On Error Resume Next
    
    'si es una recita se actualiza el estado de la cita antigua a RECITADA
    SQL = "SELECT COUNT(CI31NUMSOLICIT)"
    SQL = SQL & " FROM CI0100"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND (CI01SITCITA = '" & constESTCITA_CITADA & "'"
    SQL = SQL & " OR CI01SITCITA = '" & constESTCITA_PENDRECITAR & "')"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNAPlan
    Set rs = qry.OpenResultset()
    If rs(0) > 0 Then 'RECITAR
        SQL = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_RECITADA & "'"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        SQL = SQL & " AND (CI01SITCITA = '" & constESTCITA_CITADA & "'"
        SQL = SQL & " OR CI01SITCITA = '" & constESTCITA_PENDRECITAR & "')"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNAPlan
        qry.Execute
        If Err > 0 Then Exit Function
    End If
    rs.Close
    qry.Close

    'PR0400: estado de la actuaci�n
    If strCodDpto = constDPTO_RAYOS And CDate(strFechaCita) <= CDate(DateAdd("n", 10, strAhora)) Then
        'Para Rayos, si se cita para antes de 10 min., se anota tambi�n la entrada en cola
        SQL = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_CITADA & ","
        SQL = SQL & " PR04FECENTRCOLA = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
        qry(1) = lngNAPlan
    Else
        SQL = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_CITADA
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNAPlan
    End If
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function
    
    'CI3100: a�adir la solicitud
    If intNSecCita = 1 Then
        SQL = "INSERT INTO CI3100 (CI31NUMSOLICIT, CI31INDLUNPREF, CI31INDMARPREF, CI31INDMIEPREF,"
        SQL = SQL & " CI31INDJUEPREF, CI31INDVIEPREF, CI31INDSABPREF, CI31INDDOMPREF)"
        SQL = SQL & " VALUES (?, 1, 1, 1, 1, 1, 1, 1)"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strNumSolicit
        qry.Execute
        qry.Close
        If Err > 0 Then Exit Function
    End If
    
    'CI0100: a�adir cita
    SQL = "INSERT INTO CI0100 (CI31NUMSOLICIT, CI01NUMCITA, AG11CODRECURSO, PR04NUMACTPLAN,"
    SQL = SQL & " CI01FECCONCERT, CI01SITCITA, CI01INDRECORDA, CI01TIPRECORDA, CI01NUMENVRECO,"
    SQL = SQL & " CI01INDLISESPE, CI01INDASIG, CI01OBSERV, CI01NUMMIN, CI01NUMMINPAC)"
    SQL = SQL & " VALUES (?, ?, ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'), ?, -1, 1, 1, 0, -1, ?, ?, ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumSolicit
    qry(1) = intNSecCita
    qry(2) = strCodRec
    qry(3) = lngNAPlan
    qry(4) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(5) = constESTCITA_CITADA
    If strObserv <> "" Then qry(6) = strObserv Else qry(6) = Null
    If grdAct(iAct).Visible And grdAct(iAct).Rows > 0 Then 'citaci�n conjunta
        qry(7) = lngTORec
        qry(8) = lngTOPac
    Else
        qry(7) = Null
        qry(8) = Null
    End If
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function

    'CI1500: a�adir fase citada
    SQL = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15FECCONCPAC, CI15NUMFASECITA,"
    SQL = SQL & " CI15DESFASECITA, CI15NUMDIASPAC, CI15NUMHORAPAC,"
    SQL = SQL & " CI15NUMMINUPAC)"
    SQL = SQL & " SELECT ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI'), PR05NUMFASE,"
    SQL = SQL & " PR05DESFASE, FLOOR(PR05NUMOCUPACI/1440), FLOOR(MOD(PR05NUMOCUPACI,1440)/60),"
    SQL = SQL & " MOD(MOD(PR05NUMOCUPACI,1440),60)"
    SQL = SQL & " FROM PR0500"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumSolicit
    qry(1) = intNSecCita
    qry(2) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(3) = strCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function

    'CI2700: A�adir recurso citado
    SQL = "INSERT INTO CI2700 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, AG11CODRECURSO,"
    SQL = SQL & " CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC)"
    SQL = SQL & " SELECT ?, ?, PR05NUMFASE, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'),"
    SQL = SQL & " FLOOR(PR13NUMTIEMPREC/1440), FLOOR(MOD(PR13NUMTIEMPREC,1440)/60),"
    SQL = SQL & " MOD(MOD(PR13NUMTIEMPREC,1440),60)"
    SQL = SQL & " FROM PR1300"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    SQL = SQL & " AND PR13INDPREFEREN = -1"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumSolicit
    qry(1) = intNSecCita
    qry(2) = strCodRec
    qry(3) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(4) = strCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function

    'Se anota la fecha de planificaci�n de las actuaciones asociadas
    Call pAsociadas(lngNAPlan, strFechaCita)
    
    fblnGenerarCita = True
    On Error GoTo 0
End Function

Private Function fblnGenerarPeticion(iAct%) As Boolean
    Dim SQL$, qry As rdoQuery
    Dim strCodPers$, strCodDpto$, strCodAct$, intReqDoc%, strCodRec$, strDr$, strCodProc$
    Dim lngNAPedi&, lngNAPlan&, strNumPetic$

    strCodPers = txtCodPers.Text
    strCodDpto = cboDpto(iAct).Columns("Cod").Text
    strCodAct = cboAct(iAct).Columns("Cod").Text
    intReqDoc = cboAct(iAct).Columns("ReqDoc").Text
    strCodRec = grdHoras(iAct).Columns("CodRec").CellText(grdHoras(iAct).SelBookmarks(0))
    strDr = grdHoras(iAct).Columns("Dr").CellText(grdHoras(iAct).SelBookmarks(0))
    strCodProc = fBuscarProceso(strCodDpto, strDr)
    
    On Error Resume Next

    'PR0900: Petici�n
    If lngNPet > 0 Then 'Entrada por Petici�n o NAPlan
        'la Consulta se asocia a la Petici�n ya existente (lngNPet)
        strNumPetic = lngNPet
    Else 'Entrada Libre o por Persona (s�lo se citan consultas)
        'cada Consulta va a una Petici�n distinta
        strNumPetic = fNextClave("PR09NUMPETICION")
        SQL = "INSERT INTO PR0900 (PR09NUMPETICION, CI21CODPERSONA, AD02CODDPTO, SG02COD,"
        SQL = SQL & " PR09FECPETICION, PR09NUMGRUPO)"
        SQL = SQL & " VALUES (?, ?, ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strNumPetic
        qry(1) = strCodPers
        qry(2) = strCodDpto
        qry(3) = strDr
        qry(4) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qry(5) = strNumPetic
        qry.Execute
        qry.Close
        If Err > 0 Then Exit Function
    End If

    'PR0300: Actuaci�n pedida
    SQL = "INSERT INTO PR0300 (PR03NUMACTPEDI, AD02CODDPTO, CI21CODPERSONA, PR09NUMPETICION,"
    SQL = SQL & " PR01CODACTUACION, PR03INDCONSFIRM, PR03INDCITABLE, PR03INDCITAANT)"
    SQL = SQL & " VALUES (?, ?, ?, ?, ?, 0, -1, -1)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    lngNAPedi = fNextClave("PR03NUMACTPEDI")
    qry(0) = lngNAPedi
    qry(1) = strCodDpto
    qry(2) = strCodPers
    qry(3) = strNumPetic
    qry(4) = strCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function

    'PR0800: Petici�n - Actuaci�n pedida
    'PR08NUMSECUENCIA siempre es 1; es un campo que sobra
    SQL = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA, AD07CODPROCESO)"
    SQL = SQL & " VALUES (?, ?, 1, ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumPetic
    qry(1) = lngNAPedi
    qry(2) = strCodProc
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function

    'PR0600: Fase pedida
    SQL = "INSERT INTO PR0600 (PR03NUMACTPEDI, PR06NUMFASE, PR06DESFASE, PR06NUMMINOCUPAC,"
    SQL = SQL & " PR06NUMFASE_PRE, PR06NUMMINFPRE, PR06NUMMAXFPRE, PR06INDHABNATU, PR06INDINIFIN) "
    SQL = SQL & " SELECT ?, PR05NUMFASE, PR05DESFASE, PR05NUMOCUPACI, PR05NUMFASE_PRE,"
    SQL = SQL & " PR05NUMTMINFPRE, PR05NUMTMAXFPRE, PR05INDHABILNATU, PR05INDINICIOFIN"
    SQL = SQL & " FROM PR0500"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNAPedi
    qry(1) = strCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function
    
    'PR1400: Tipo recurso pedido
    SQL = "INSERT INTO PR1400 (PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID, AG14CODTIPRECU,"
    SQL = SQL & " AG11CODRECURSO, AD02CODDPTO, PR14NUMUNIREC, PR14NUMMINOCU, PR14NUMMINDESREC,"
    SQL = SQL & " PR14INDRECPREFE, PR14INDPLANIF)"
    SQL = SQL & " SELECT ?, PR05NUMFASE, PR13NUMNECESID, AG14CODTIPRECU, ?, ?,"
    SQL = SQL & " PR13NUMUNIREC, PR13NUMTIEMPREC, PR13NUMMINDESF, PR13INDPREFEREN, PR13INDPLANIF"
    SQL = SQL & " FROM PR1300"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
''    SQL = SQL & " AND PR14INDRECPREFE = -1"
''    SQL = SQL & " AND PR14INDPLANIF = -1"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNAPedi
    qry(1) = strCodRec
    qry(2) = strCodDpto 'se coge strCodDpto en lugar de AD02CODDPTO de PR1300 ya que puede ser NULL
    qry(3) = strCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function

    'PR0400: Actuaci�n planificada
    If strCodDpto = constDPTO_URGENCIAS Then
        SQL = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, CI21CODPERSONA,"
        SQL = SQL & " PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC, AD07CODPROCESO, PR04FECENTRCOLA)"
        SQL = SQL & " VALUES (?, ?, ?, ?, ?, ?, ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    Else
        SQL = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, CI21CODPERSONA,"
        SQL = SQL & " PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC, AD07CODPROCESO)"
        SQL = SQL & " VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
    End If
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    lngNAPlan = fNextClave("PR04NUMACTPLAN")
    cboAct(iAct).Columns("NAPlan").Text = lngNAPlan 'se anota el NAPlan para la cita posterior
    qry(0) = lngNAPlan
    qry(1) = strCodAct
    qry(2) = strCodDpto
    qry(3) = strCodPers
    qry(4) = constESTACT_PLANIFICADA
    qry(5) = lngNAPedi
    qry(6) = intReqDoc
    qry(7) = strCodProc
    If strCodDpto = constDPTO_URGENCIAS Then qry(8) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Function
    
    'PR4400
    SQL = "UPDATE PR4400 SET PR44NUMCUENTA = PR44NUMCUENTA + 1"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND PR01CODACTUACION = ?"
    SQL = SQL & " AND AD02CODDPTO_REA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodDpto
    qry(1) = strCodAct
    qry(2) = strCodDpto
    qry.Execute
    If qry.RowsAffected = 0 Then
        SQL = "INSERT INTO PR4400 (AD02CODDPTO, PR01CODACTUACION, AD02CODDPTO_REA, PR44NUMCUENTA)"
        SQL = SQL & " VALUES (?, ?, ?, 1)"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCodDpto
        qry(1) = strCodAct
        qry(2) = strCodDpto
        qry.Execute
    End If
    qry.Close
    If Err > 0 Then Exit Function
    
    fblnGenerarPeticion = True
    On Error GoTo 0
End Function

Private Function fCodPerfil(strFecha$) As Integer
'******************************************************************************************
'*  Determina el perfil del recurso que hay que aplicar para la fecha strFecha
'******************************************************************************************
    Dim Item
    
    strFecha = Format(strFecha, "dd/mm/yyyy")
    awk.FS = "|"
    For Each Item In cllPerfilesRec
        awk = Item
        If CDate(awk.f(2)) <= CDate(strFecha) And CDate(awk.f(3)) >= CDate(strFecha) Then
            fCodPerfil = awk.f(1)
            Exit Function
        End If
    Next
End Function

Private Function fDatosCitaCorrectos() As Boolean
'****************************************************************************************
'*  1 - Comprueba que existe una persona para realizar la cita
'*  2 - Comprueba que existan actuaciones para citar
'*  3 - Comprueba si todas las actuaciones que se van a citar tienen establecida la
'*      fecha-hora y el recurso
'****************************************************************************************
    Dim iAct%, msg$, msgAct$, k%, blnExiste As Boolean
    
    fDatosCitaCorrectos = True
    
    'comprobaci�n 1
    If txtCodPers.Text = "" Then
        msg = "No se ha encontrado ninguna persona para realizar la cita."
        MsgBox msg, vbExclamation, Me.Caption
        fDatosCitaCorrectos = False
        Exit Function
    End If
    
    'comprobaciones 2 y 3
    For iAct = 0 To cboAct.Count - 1
        If cboAct(iAct).Text <> "" Then
            blnExiste = True 'existe alguna actuaci�n para citar
            If mskHora(iAct).Text = "__:__" Then
                msg = msg & " --> " & fTextoVariasAct(iAct) & Chr$(13) 'no existe hora de cita
            End If
        End If
    Next iAct
    If blnExiste Then
        If msg <> "" Then
            msg = "No se ha se�alado hora de cita para las siguientes actuaciones:" _
                    & Chr$(13) & Chr$(13) & msg
        End If
    Else
        msg = "No hay ninguna actuaci�n para citar."
    End If
    If msg <> "" Then
        MsgBox msg, vbExclamation, Me.Caption
        fDatosCitaCorrectos = False
        Exit Function
    End If
  End Function

Private Function fFechaLaborable(strFecha$) As Boolean
'******************************************************************************************
'*  Comprueba si la fecha pasada por ventana es Laborable seg�n el calendario cargado
'*  en la funci�n fCargarCalendario
'*  Dicha fecha ser� Laborable si est� en cllFechasNoFiesta o no est� en cllFechasFiesta
'*  ni cllDiasFiesta
'******************************************************************************************
    Dim intDayOfWeek%
    Dim Item
    
    fFechaLaborable = True
    
    strFecha = Format(strFecha, "dd/mm/yyyy")
    For Each Item In cllFechasNoFiesta
        If strFecha = Item Then Exit Function
    Next
    
    intDayOfWeek = WeekDay(strFecha, vbUseSystemDayOfWeek)
    For Each Item In cllDiasFiesta
        If intDayOfWeek = Item Then fFechaLaborable = False: Exit Function
    Next
     
    For Each Item In cllFechasFiesta
        If strFecha = Item Then fFechaLaborable = False: Exit Function
    Next
End Function

Private Function fFranjaDiaFranjaHabil(strFecha$, intIndexFranja%) As Boolean
    Dim intDayOfWeek%
    
    intDayOfWeek = WeekDay(strFecha, vbUseSystemDayOfWeek)
    Select Case intDayOfWeek
    Case 1: If arFranjas(intIndexFranja).intL = -1 Then fFranjaDiaFranjaHabil = True
    Case 2: If arFranjas(intIndexFranja).intM = -1 Then fFranjaDiaFranjaHabil = True
    Case 3: If arFranjas(intIndexFranja).intX = -1 Then fFranjaDiaFranjaHabil = True
    Case 4: If arFranjas(intIndexFranja).intJ = -1 Then fFranjaDiaFranjaHabil = True
    Case 5: If arFranjas(intIndexFranja).intV = -1 Then fFranjaDiaFranjaHabil = True
    Case 6: If arFranjas(intIndexFranja).intS = -1 Then fFranjaDiaFranjaHabil = True
    Case 7: If arFranjas(intIndexFranja).intD = -1 Then fFranjaDiaFranjaHabil = True
    End Select
End Function

Private Function fFranjaValida(intCodPerf%, intCodFrja%) As Boolean
    Dim Item
    fFranjaValida = True
    For Each Item In cllRestricRec
        If intCodPerf & "|" & intCodFrja = Item Then
            fFranjaValida = False
            Exit Function
        End If
    Next
End Function

Private Function fHuecosSelTodaviaActivos() As Boolean
'****************************************************************************************
'*  Comprueba que el hueco seleccionado para realizar la cita no ha sido ocupado ya por
'*  otra cita realizada desde alg�n otro sitio
'****************************************************************************************
    Dim iAct%
    Dim strCodRec$, intCodPerf%, intCodFrja%, strCodAct$
    Dim maxFecCitas%, d%, maxDatCitas%
    Dim intNCitas%, intNCitasAct%, intNCitasLibres%
    Dim arInterv() As typeIntervalos, strHoraI$, strHoraF$
    Dim msg$, msgAct$
    Dim i%, j%, k%
    Dim lngNMinAct&

    Screen.MousePointer = vbHourglass
    fHuecosSelTodaviaActivos = True
    For iAct = 0 To cboAct.Count - 1
        If cboAct(iAct).Text <> "" Then
            lngNMinAct = Val(Mid$(lblTORec(iAct).Caption, 11)) 'duraci�n de la actuaci�n que se va a citar
            strCodRec = grdHoras(iAct).Columns("CodRec").CellText(grdHoras(iAct).SelBookmarks(0))
            intCodPerf = grdHoras(iAct).Columns("CodPerf").CellText(grdHoras(iAct).SelBookmarks(0))
            intCodFrja = grdHoras(iAct).Columns("CodFrja").CellText(grdHoras(iAct).SelBookmarks(0))
            strCodAct = cboAct(iAct).Columns("Cod").Text
            'se cargan las citas del recurso para el d�a seleccionado
            Call pCargarCitasRec(strCodRec, mthHuecos(iAct).Date, mthHuecos(iAct).Date)
            'se cargan los datos de la franja (perfil+franja) seleccionada
            Call pCargarFranjas(strCodRec, strCodAct, intCodPerf, intCodFrja)
            Select Case arFranjas(1).strModo 'modo de asignaci�n de la cita
            Case constCITA_PORCANTIDAD '*** POR CANTIDAD ***
                'se mira el n� de citas ya existentes para la franja
                intNCitas = 0: intNCitasAct = 0
                maxFecCitas = 0
                On Error Resume Next
                maxFecCitas = UBound(arCitas)
                On Error GoTo 0
                If maxFecCitas > 0 Then 'si existe alguna cita para el d�a seleccionado...
                    maxDatCitas = 0
                    On Error Resume Next
                    maxDatCitas = UBound(arCitas(1).arCitaDatos)
                    On Error GoTo 0
                    For d = 1 To maxDatCitas
                        If arCitas(1).arCitaDatos(d).lngNumMinOcu > 0 Then 'Cita NO manual
                            If CDate(arCitas(1).arCitaDatos(d).strHora) >= CDate(arFranjas(1).strHoraIni) _
                            And CDate(arCitas(1).arCitaDatos(d).strHora) < CDate(arFranjas(1).strHoraFin) Then
                                intNCitas = intNCitas + 1 'N� citas en franja
                                If arCitas(1).arCitaDatos(d).strCodAct = strCodAct Then
                                    intNCitasAct = intNCitasAct + 1 'N� citas de la  Actuaci�n en franja
                                End If
                            End If
                        End If
                    Next d
                End If
                'se mira si el hueco sigue libre
                If fNCitasLibres(arFranjas(1).intNumCitaAdmi - intNCitas, arFranjas(1).intNumCitaActAdmi - intNCitasAct) = 0 Then
                    'la franja est� completa
                    msg = msg & "--> " & fTextoVariasAct(iAct) & " (" _
                        & Format(mthHuecos(iAct).Date & " " & mskHora(iAct).Text, "dd/mm/yyyy hh:mm") _
                        & Space(5) & txtRecSel(iAct).Text & ")" & Chr$(13)
                End If
                
            Case constCITA_PORINTERVALOS '*** POR INTERVALOS ***
                'fraccionamiento de la franja en intervalos
                ReDim arInterv(1 To arFranjas(1).intNumCitaAdmi)
                For i = 1 To arFranjas(1).intNumCitaAdmi
                    If i = 1 Then
                        strHoraI = arFranjas(1).strHoraIni
                    Else
                        strHoraI = strHoraF
                    End If
                    strHoraF = Format(DateAdd("n", arFranjas(1).intInterv, strHoraI), "hh:mm")
                    arInterv(i).strHoraIni = strHoraI
                    arInterv(i).strHoraFin = strHoraF
                Next i
            
                'se mira el n� de citas ya existentes para la franja
                intNCitas = 0: intNCitasAct = 0
                maxFecCitas = 0
                On Error Resume Next
                maxFecCitas = UBound(arCitas)
                On Error GoTo 0
                If maxFecCitas > 0 Then 'si existe alguna cita para el d�a seleccionado...
                    maxDatCitas = 0
                    On Error Resume Next
                    maxDatCitas = UBound(arCitas(1).arCitaDatos)
                    On Error GoTo 0
                    For d = 1 To maxDatCitas
                        If arCitas(1).arCitaDatos(d).lngNumMinOcu > 0 Then 'Cita NO manual
                            If CDate(arCitas(1).arCitaDatos(d).strHora) >= CDate(arFranjas(1).strHoraIni) _
                            And CDate(arCitas(1).arCitaDatos(d).strHora) < CDate(arFranjas(1).strHoraFin) Then
                                intNCitas = intNCitas + 1 'N� citas en franja
                                If arCitas(1).arCitaDatos(d).strCodAct = strCodAct Then
                                    intNCitasAct = intNCitasAct + 1 'N� citas de la Actuaci�n en franja
                                End If
                                'se anotan los intervalos ya ocupados por citas existentes
                                For i = 1 To UBound(arInterv)
                                    If Not arInterv(i).blnOcupado Then
                                        If CDate(arCitas(1).arCitaDatos(d).strHora) < CDate(arInterv(i).strHoraFin) Then
                                            arInterv(i).blnOcupado = True
                                            Exit For
                                        End If
                                    End If
                                Next i
                            End If
                        End If
                    Next d
                End If
                'se mira si el hueco sigue libre
                msgAct = fTextoVariasAct(iAct)
                If fNCitasLibres(arFranjas(1).intNumCitaAdmi - intNCitas, arFranjas(1).intNumCitaActAdmi - intNCitasAct) = 0 Then
                    'la franja est� completa
                    msg = msg & "--> " & msgAct & " (" _
                        & Format(mthHuecos(iAct).Date & " " & mskHora(iAct).Text, "dd/mm/yyyy hh:mm") _
                        & Space(5) & txtRecSel(iAct).Text & ")" & Chr$(13)
                Else
                    For i = 1 To UBound(arInterv)
                        If arInterv(i).strHoraIni = mskHora(iAct).Text Then
                            If arInterv(i).blnOcupado Then
                                'el intervalo est� ocupado
                                msg = msg & "--> " & fTextoVariasAct(iAct) & " (" _
                                    & Format(mthHuecos(iAct).Date & " " & mskHora(iAct).Text, "dd/mm/yyyy hh:mm") _
                                    & Space(5) & txtRecSel(iAct).Text & ")" & Chr$(13)
                            End If
                            Exit For
                        End If
                    Next i
                End If
                
            Case constCITA_PORINTERVALOSOSCILANTES  '*** INTERVALOS OSCILANTES ***
                'se mira el n� de citas ya existentes para la franja y el d�a procesado
                Erase arInterv: i = 0
                maxFecCitas = 0
                On Error Resume Next
                maxFecCitas = UBound(arCitas)
                On Error GoTo 0
                If maxFecCitas > 0 Then  'si existe alguna cita para el d�a seleccionado...
                    maxDatCitas = 0
                    On Error Resume Next
                    maxDatCitas = UBound(arCitas(1).arCitaDatos)
                    On Error GoTo 0
                    For d = 1 To maxDatCitas
                        If arCitas(1).arCitaDatos(d).lngNumMinOcu > 0 Then 'Cita que ocupa hueco
                            strHoraI = arCitas(1).arCitaDatos(d).strHora
                            strHoraF = Format(DateAdd("n", arCitas(1).arCitaDatos(d).lngNumMinOcu, arCitas(1).arCitaDatos(d).strHora), "hh:mm")
                            If CDate(strHoraF) >= CDate(arFranjas(1).strHoraIni) _
                            Or CDate(strHoraI) < CDate(arFranjas(1).strHoraFin) Then
                                i = i + 1
                                'se anota el hueco ocupado por la cita
                                ReDim Preserve arInterv(1 To i)
                                arInterv(i).strHoraIni = strHoraI
                                arInterv(i).strHoraFin = strHoraF
                            End If
                        End If
                    Next d
                    Exit For
                End If
                'se mira si el hueco sigue libre
                If i > 0 Then
                    strHoraI = mskHora(iAct).Text
                    strHoraF = DateAdd("n", lngNMinAct, mskHora(iAct).Text)
                    For j = 1 To i
                        If (CDate(arInterv(j).strHoraIni) < CDate(strHoraF)) _
                        And (CDate(arInterv(j).strHoraFin) > CDate(strHoraI)) Then
                            'el hueco seleccionado o parte de �l ha sido ocupado
                            msg = msg & "--> " & fTextoVariasAct(iAct) & " (" _
                                & Format(mthHuecos(iAct).Date & " " & mskHora(iAct).Text, "dd/mm/yyyy hh:mm") _
                                & Space(5) & txtRecSel(iAct).Text & ")" & Chr$(13)
                        End If
                    Next j
                End If
                
            Case constCITA_SECUENCIAL  '*** SECUENCIAL ***
                'se mira el n� de citas ya existentes para la franja
                Erase arInterv: i = 0
                maxFecCitas = 0
                On Error Resume Next
                maxFecCitas = UBound(arCitas)
                On Error GoTo 0
                If maxFecCitas > 0 Then 'si existe alguna cita para el d�a seleccionado...
                    maxDatCitas = 0
                    On Error Resume Next
                    maxDatCitas = UBound(arCitas(1).arCitaDatos)
                    On Error GoTo 0
                    For d = 1 To maxDatCitas
                        If arCitas(1).arCitaDatos(d).lngNumMinOcu > 0 Then 'Cita NO manual
                            If CDate(arCitas(1).arCitaDatos(d).strHora) >= CDate(arFranjas(1).strHoraIni) _
                            And CDate(arCitas(1).arCitaDatos(d).strHora) < CDate(arFranjas(1).strHoraFin) Then
                                i = i + 1
                                'se anota el hueco ocupado por la cita
                                ReDim Preserve arInterv(1 To i)
                                arInterv(i).strHoraIni = arCitas(1).arCitaDatos(d).strHora
                                arInterv(i).strHoraFin = Format(DateAdd("n", arCitas(1).arCitaDatos(d).lngNumMinOcu, arCitas(1).arCitaDatos(d).strHora), "hh:mm")
                            End If
                        End If
                    Next d
                End If
                'se mira si el hueco sigue libre
                If i > 0 Then 'si existen citas en la franja...
                    strHoraI = mskHora(iAct).Text
                    strHoraF = DateAdd("n", lngNMinAct, mskHora(iAct).Text)
                    For j = 1 To i
                        If (CDate(arInterv(j).strHoraIni) < CDate(strHoraF)) _
                        And (CDate(arInterv(j).strHoraFin) > CDate(strHoraI)) Then
                            'el hueco seleccionado o parte de �l ha sido ocupado
                            msg = msg & "--> " & fTextoVariasAct(iAct) & " (" _
                                & Format(mthHuecos(iAct).Date & " " & mskHora(iAct).Text, "dd/mm/yyyy hh:mm") _
                                & Space(5) & txtRecSel(iAct).Text & ")" & Chr$(13)
                        End If
                    Next j
                End If
            End Select
        End If
    Next iAct
    
    If msg <> "" Then
        msg = "Alg�n hueco de los seleccionados para citar acaba de ser ocupado: " & Chr$(13) & Chr$(13) & msg
        msg = msg & Chr$(13) & "Elija otro hueco o vuelva a realizar la b�squeda de huecos"
        msg = msg & " para tener la informaci�n actualizada."
        Screen.MousePointer = vbDefault
        MsgBox msg, vbInformation, Me.Caption
        fHuecosSelTodaviaActivos = False
    End If
    Screen.MousePointer = vbDefault
End Function

Private Function fNCitasLibres(intNCitasLibres%, intNCitasLibresAct%) As Integer
    fNCitasLibres = 0
    If intNCitasLibres > 0 Then
        If intNCitasLibresAct > 0 Then
            If intNCitasLibresAct < intNCitasLibres Then
                fNCitasLibres = intNCitasLibresAct
            Else
                fNCitasLibres = intNCitasLibres
            End If
        End If
    End If
End Function

Private Function fBuscarProceso(strCodDpto$, strCodDr$) As String
'************************************************************************************
'*  Devuelve el proceso al que hay que asociar la petici�n
'*  Si existe un proceso activo para el Dpto/Dr solicitante, se toma �ste. Si no existe,
'*  se crea
'************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strCodProc$
    
    SQL = "SELECT AD0700.AD07CODPROCESO"
    SQL = SQL & " FROM AD0400, AD0700"
    SQL = SQL & " WHERE CI21CODPERSONA = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AD07FECHORAINICI AND NVL(AD07FECHORAFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    SQL = SQL & " AND AD0400.AD07CODPROCESO = AD0700.AD07CODPROCESO"
    SQL = SQL & " AND AD02CODDPTO = ?"
    SQL = SQL & " AND SG02COD = ?"
    SQL = SQL & " AND AD04FECFINRESPON IS NULL"
    SQL = SQL & " ORDER BY AD0700.AD07CODPROCESO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = txtCodPers.Text
    qry(1) = strCodDpto
    qry(2) = strCodDr
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strCodProc = rs!AD07CODPROCESO
    Else
        strCodProc = fNextClave("AD07CODPROCESO")
        SQL = "INSERT INTO AD0700 (CI21CODPERSONA, AD07CODPROCESO, AD07FECHORAINICI,"
        SQL = SQL & " CI22NUMHISTORIA, AD34CODESTADO)"
        SQL = SQL & " VALUES (?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?, 1)"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = txtCodPers.Text
        qry(1) = strCodProc
        qry(2) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        If txtNH.Text <> "" Then qry(3) = txtNH.Text Else qry(3) = Null
        qry.Execute
        
        SQL = "INSERT INTO AD0400 (AD07CODPROCESO, AD04FECINIRESPON, AD02CODDPTO, SG02COD)"
        SQL = SQL & " VALUES(?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?, ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCodProc
        qry(1) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qry(2) = strCodDpto
        qry(3) = strCodDr
        qry.Execute
    End If
    
    fBuscarProceso = strCodProc
End Function

Private Function fTextoVariasAct(iAct%) As String
    Dim k%, strAct$
    If grdAct(iAct).Visible Then
        LockWindowUpdate grdAct(iAct).hWnd
        For k = 1 To grdAct(iAct).Rows
            If k = 1 Then
                grdAct(iAct).MoveFirst
                strAct = ""
            Else
                grdAct(iAct).MoveNext
                strAct = ", "
            End If
            strAct = strAct & grdAct(iAct).Columns("Act").Text
        Next k
        LockWindowUpdate 0&
    Else
        strAct = cboAct(iAct).Text
    End If
    fTextoVariasAct = strAct
End Function

Private Function fTOcupPac(strCodAct$) As Long
'***************************************************************************************
'*  Determina el tiempo de ocupaci�n de un paciente durante la realizaci�n de una actuaci�n
'***************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT SUM(PR05NUMOCUPACI)"
    SQL = SQL & " FROM PR0500"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodAct
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then fTOcupPac = rs(0)
    rs.Close
    qry.Close
End Function

Private Sub pBloquearControles(intIndexBlocked%)
    Dim iAct%
    
    fraPac.Enabled = False
    cmdBuscarPac.Enabled = False
    
    For iAct = 0 To intIndexBlocked
        fraAct(iAct).Enabled = False
        cboDpto(iAct).BackColor = objApp.objUserColor.lngReadOnly
        cboAct(iAct).Visible = False
        grdAct(iAct).Visible = True
        lblNivel(iAct).Enabled = False
    Next iAct
    Call pEntradaLibre(intIndexBlocked + 1)
End Sub

Private Sub pBuscarHuecos()
    Dim iAct% 'Index del cboAct (0 --> cboAct.Count-1)
    Dim iRec% 'Index del Rec en cada cboRec (1 --> cboRec(iAct).Rows)
    Dim strCodRecSel$ 'C�digo del recurso seleccionado en cboRec(iAct) (si existe)
    Dim intNFrja% 'no de franjas del recurso procesado
    Dim msg$
    
    'se mira si hay alguna actuaci�n seleccionada
    For iAct = 0 To cboAct.Count - 1
        If cboAct(iAct).Text <> "" Then Exit For
    Next iAct
    If iAct = cboAct.Count Then
        MsgBox "No hay ninguna actuaci�n seleccionada.", vbExclamation, Me.Caption
        Exit Sub
    End If
    
    Call pLimpiarHuecos
    
    'se establece el n� de d�as para el que se est�n buscando los huecos
    strFecIni = dcboFecDesde.Text: strFecFin = dcboFecHasta.Text
    intNDias = DateDiff("d", strFecIni, strFecFin) + 1
    If intNDias > 100 Then
        msg = "El n� de d�as para b�squeda de huecos es muy grande (" & intNDias & " d�as)."
        msg = msg & Chr$(13)
        msg = msg & "�Desea Ud. realizar la b�squeda de huecos de todas formas?"
        If MsgBox(msg, vbQuestion + vbYesNo, Me.Caption) = vbNo Then Exit Sub
    End If
    'se dimensiona la estructura que contendr� los huecos localizdos
    ReDim arHuecos(1 To intNDias)
    
    'se bloquea la pantalla mientras se realiza la b�squeda
    LockWindowUpdate Me.hWnd
    Screen.MousePointer = vbHourglass
    
    'bucle que recorrer cada una de las actuaciones
    strAhora = fAhora
    blnCancel = False
    For iAct = 0 To cboAct.Count - 1
        If cboAct(iAct).Text <> "" Then
            'se mira si hay un recurso seleccionado para realizar la b�squeda s�lo para ese recurso
            If cboRec(iAct).Text <> "" Then
                strCodRecSel = cboRec(iAct).Columns("Cod").Text
            Else
                strCodRecSel = ""
            End If
            'bucle para recorrer cada uno de los recursos
            For iRec = 1 To cboRec(iAct).Rows
                If iRec = 1 Then cboRec(iAct).MoveFirst Else cboRec(iAct).MoveNext
                'se tiene en cuenta el recurso si: no hay ninguno seleccionado o _
                el recurso seleccionado coincide con el que se va a procesar
                If strCodRecSel = "" Or cboRec(iAct).Columns("Cod").Text = strCodRecSel Then
                    'se carga el calendario del recurso
                    Call pCargarCalendario(cboRec(iAct).Columns("Cal").Text, strFecIni, strFecFin)
                    'se cargan los perfiles del recurso
                    Call pCargarPerfilesRecurso(cboRec(iAct).Columns("Cod").Text, strFecIni, strFecFin)
                    'se cargan las franjas del recurso
                    Call pCargarFranjas(cboRec(iAct).Columns("Cod").Text, cboAct(iAct).Columns("Cod").Text)
                    If blnCancel Then 'b�squeda cancelada
                        Screen.MousePointer = vbDefault
                        LockWindowUpdate 0&
                        MsgBox "Se ha cancelado la b�squeda de huecos.", vbInformation, Me.Caption
                        Exit Sub
                    End If
                    On Error Resume Next
                    intNFrja = UBound(arFranjas)
                    On Error GoTo 0
                    If intNFrja > 0 Then 'si hay alguna franja...
                        '... se cargan las citas
                        Call pCargarCitasRec(cboRec(iAct).Columns("Cod").Text, strFecIni, strFecFin)
                        '... y se buscan los huecos
                        Call pCargarHuecos(strFecIni, strFecFin, iAct, iRec, cboAct(iAct).Columns("Cod").Text)
                    End If
                    If strCodRecSel <> "" Then Exit For 'No hay que seguir buscando m�s
                End If
            Next iRec
        End If
    Next iAct
    
    'se muestran en los calendarios los d�as con huecos
    Call pVerDiasHuecos
    
    'se desbloquea la pantalla
    Screen.MousePointer = vbDefault
    LockWindowUpdate 0&
End Sub

Private Sub pCargarActuaciones(iAct%)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    cboAct(iAct).Text = "": cboAct(iAct).RemoveAll
    lblTORec(iAct).Caption = "T.O. Rec:": lblTOPac(iAct).Caption = "T.O. Pac:": lblFecPlan(iAct).Caption = "Fecha Plan:"
    
    SQL = "SELECT DISTINCT PR0100.PR01CODACTUACION, PR01DESCORTA, PR13NUMTIEMPREC, PR01INDREQDOC"
    SQL = SQL & " FROM PR0100, PR1300, PR0200"
    SQL = SQL & " WHERE PR0200.AD02CODDPTO = ?"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0200.PR01CODACTUACION"
    SQL = SQL & " AND PR12CODACTIVIDAD = ?"
    SQL = SQL & " AND PR01INDINSTRREA = 0"
    SQL = SQL & " AND PR01FECINICO <= SYSDATE"
    SQL = SQL & " AND (PR01FECFIN >= SYSDATE OR PR01FECFIN IS NULL)"
    SQL = SQL & " AND PR1300.PR01CODACTUACION = PR0200.PR01CODACTUACION"
    SQL = SQL & " AND PR13INDPREFEREN = -1"
    SQL = SQL & " AND PR13INDPLANIF = -1"
    SQL = SQL & " ORDER BY PR01DESCORTA"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = cboDpto(iAct).Columns("Cod").Text
    qry(1) = txtActiv(iAct).Text
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboAct(iAct).AddItem rs!PR01CODACTUACION & Chr$(9) _
                            & rs!PR01DESCORTA & Chr$(9) _
                            & rs!PR13NUMTIEMPREC & Chr$(9) _
                            & Chr$(9) _
                            & rs!PR01INDREQDOC
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    If cboAct(iAct).Rows = 1 Then
        cboAct(iAct).Text = cboAct(iAct).Columns(1).Text
        cboAct_Click (iAct)
    Else
        cboRec(iAct).Text = "": cboRec(iAct).RemoveAll
    End If
End Sub

Private Sub pCargarCalendario(intCodCal%, strFecIni$, strFecFin$)
'******************************************************************************************
'*  Carga en unas colecciones las caracter�sticas del calendario entre las fechas strFecIni
'*  y strFecFin
'*  cllFechasNoFiesta: d�as que se consideran laborables; tienen prioridad sobre lo
'*      establecido en el calendario normal
'*  cllFechasFiesta: d�as que se consideran festivoss; tienen prioridad sobre lo
'*      establecido en el calendario normal
'*  cllDiasFiesta: d�as de la semana que se consideran festivos en el calendario normal
'******************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, i%
    Static strCalendarActual$ 'datos de la �ltima carga de calendarios
    Dim blnRecargar As Boolean
    
    'se mira si es necesario volver a cargar los datos del calendario, es decir, si las
    'condiciones del nuevo calendario no est�n contempladas en la �ltima carga
    If strCalendarActual = "" Then 'es la 1� vez que se carga calendarios
        blnRecargar = True
    Else
        awk.FS = "|"
        awk = strCalendarActual
        If awk.f(1) <> intCodCal Then 'el calendario es distinto
            blnRecargar = True
        Else
            If CDate(strFecIni) < CDate(awk.f(2)) Or CDate(strFecFin) > CDate(awk.f(3)) Then
                'los rangos de fechas no est�n incluidos en los ya cargados
                blnRecargar = True
            End If
        End If
    End If
    If Not blnRecargar Then Exit Sub 'no hace falta volver a cargar los datos del calendario
    strCalendarActual = intCodCal & "|" & strFecIni & "|" & strFecFin
    
    'se vuelven a cargar los datos del calendario
    Do While cllFechasNoFiesta.Count > 0: cllFechasNoFiesta.Remove 1: Loop
    Do While cllFechasFiesta.Count > 0: cllFechasFiesta.Remove 1: Loop
    Do While cllDiasFiesta.Count > 0: cllDiasFiesta.Remove 1: Loop

    'Fechas especiales (festivos y laborables especiales)
    SQL = "SELECT AG03FECDIAESPE, AG03INDFESTIVO"
    SQL = SQL & " FROM AG0300"
    SQL = SQL & " WHERE AG02CODCALENDA = ?"
    SQL = SQL & " AND AG03FECDIAESPE BETWEEN TO_DATE(?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & " AND AG03FECBAJA IS NULL"
    SQL = SQL & " ORDER BY AG03INDFESTIVO, AG03FECDIAESPE"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intCodCal
    qry(1) = Format(strFecIni, "dd/mm/yyyy")
    qry(2) = Format(strFecFin, "dd/mm/yyyy")
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        If rs!AG03INDFESTIVO = 0 Then 'fechas laborables
            cllFechasNoFiesta.Add CStr(Format(rs!AG03FECDIAESPE, "dd/mm/yyyy"))
        Else 'fechas festivos
            cllFechasFiesta.Add CStr(Format(rs!AG03FECDIAESPE, "dd/mm/yyyy"))
        End If
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'd�as festivos de la semana
    SQL = "SELECT AG08INDLUNFEST, AG08INDMARFEST, AG08INDMIEFEST, AG08INDJUEFEST,"
    SQL = SQL & " AG08INDVIEFEST, AG08INDSABFEST, AG08INDDOMFEST"
    SQL = SQL & " FROM AG0800"
    SQL = SQL & " WHERE AG02CODCALENDA = ?"
    SQL = SQL & " AND TO_DATE(?,'DD/MM/YYYY') BETWEEN AG08FECINIPERI AND AG08FECFINPERI"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intCodCal
    qry(1) = Format(strFecIni, "dd/mm/yyyy")
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        For i = 0 To 6
            If rs(i) = -1 Then: cllDiasFiesta.Add i + 1
        Next i
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pCargarCitasPac()
'******************************************************************************************
'*  Carga en un grid citas previstas para el paciente
'*  La duraci�n de la actuaci�n se obtiene como suma de los tiempos de ocupaci�n del
'*  paciente para cada una de las fases, AUNQUE ESTO PUEDE NO SER CORRECTO DEL TODO
'******************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim lngTOPac&
    
    If txtCodPers.Text = "" Then Exit Sub

    grdCitasPac.RemoveAll
    
''    SQL = "SELECT /*+ ORDERED */ PR0400.PR04NUMACTPLAN, CI01FECCONCERT,"
''    SQL = SQL & " PR01DESCORTA, AG11DESRECURSO, SUM(PR05NUMOCUPACI) OCUPACI"
''    SQL = SQL & " FROM PR0400, PR0100, PR0500, CI0100, AG1100"
''    SQL = SQL & " WHERE PR0400.CI21CODPERSONA = ?"
''    SQL = SQL & " AND PR0400.PR37CODESTADO = ?"
''    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
''    SQL = SQL & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
''    SQL = SQL & " AND CI01SITCITA = ?"
''    SQL = SQL & " AND CI01FECCONCERT >= SYSDATE"
''    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
''    SQL = SQL & " AND PR0500.PR01CODACTUACION = PR0400.PR01CODACTUACION"
''    SQL = SQL & " GROUP BY PR0400.PR04NUMACTPLAN, CI01FECCONCERT,"
''    SQL = SQL & " PR01DESCORTA, AG11DESRECURSO"
''    SQL = SQL & " ORDER BY CI01FECCONCERT"
    
    SQL = "SELECT /*+ ORDERED */ PR0400.PR04NUMACTPLAN, CI01FECCONCERT,"
    SQL = SQL & " PR01DESCORTA, AG11DESRECURSO, CI01NUMMINPAC, SUM(PR05NUMOCUPACI) OCUPACI"
    SQL = SQL & " FROM PR0400, PR0100, PR0500, CI0100, AG1100"
    SQL = SQL & " WHERE PR0400.CI21CODPERSONA = ?"
    SQL = SQL & " AND PR0400.PR37CODESTADO = ?"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    SQL = SQL & " AND CI01SITCITA = ?"
    SQL = SQL & " AND CI01FECCONCERT >= SYSDATE"
    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    SQL = SQL & " AND PR0500.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " GROUP BY PR0400.PR04NUMACTPLAN, CI01FECCONCERT,"
    SQL = SQL & " PR01DESCORTA, AG11DESRECURSO, CI01NUMMINPAC"
    SQL = SQL & " ORDER BY CI01FECCONCERT"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = txtCodPers.Text
    qry(1) = constESTACT_CITADA
    qry(2) = constESTCITA_CITADA
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        If IsNull(rs!CI01NUMMINPAC) Then
            lngTOPac = rs!OCUPACI
        Else
            lngTOPac = rs!CI01NUMMINPAC
        End If
        grdCitasPac.AddItem Format(rs!CI01FECCONCERT, "dd/mm/yyyy hh:mm") _
                            & Chr$(9) & lngTOPac _
                            & Chr$(9) & rs!PR01DESCORTA _
                            & Chr$(9) & rs!AG11DESRECURSO _
                            & Chr$(9) & rs!PR04NUMACTPLAN
        rs.MoveNext
    Loop
    rs.Close
    qry.Close

End Sub

Private Sub pCargarCitasRec(strCodRec$, strFecIni$, strFecFin$)
'******************************************************************************************
'*  Carga en una estructura las citas del recursos entre las fechas de b�squeda
'******************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strAux$, dim1%, dim2%

    Erase arCitas

    SQL = "SELECT CI01FECCONCERT,"
    SQL = SQL & " NVL(CI01NUMMIN, 1440 * CI27NUMDIASREC + 60 * CI27NUMHORAREC + CI27NUMMINUREC) MINOCUPREC,"
    SQL = SQL & " PR0400.PR01CODACTUACION"
    SQL = SQL & " FROM CI2700, CI0100, PR0400"
    SQL = SQL & " WHERE CI0100.AG11CODRECURSO = ? "
    SQL = SQL & " AND CI01FECCONCERT >= TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & " AND CI01FECCONCERT < TO_DATE(?,'DD/MM/YYYY')"
    '* (21/08/00) El indicador CI01INDASIG s�lo indica que la cita se ha realizado por
    'huecos o por overbooking pero siempre hay que tenerlas en cuenta ya que pueden ocupar
    'huecos (seg�n sea la ocupaci�n del recurso pedido)
    SQL = SQL & " AND (CI01SITCITA = '" & constESTCITA_CITADA & "'"
    SQL = SQL & " OR CI01SITCITA = '" & constESTCITA_RESERVADA & "')"
    '****
    SQL = SQL & " AND CI2700.CI31NUMSOLICIT = CI0100.CI31NUMSOLICIT"
    SQL = SQL & " AND CI2700.CI01NUMCITA = CI0100.CI01NUMCITA"
    SQL = SQL & " AND CI2700.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    SQL = SQL & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
    SQL = SQL & " ORDER BY CI01FECCONCERT"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodRec
    qry(1) = Format(strFecIni, "dd/mm/yyyy")
    qry(2) = Format(DateAdd("d", 1, strFecFin), "dd/mm/yyyy")
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        If strAux <> Format(rs!CI01FECCONCERT, "dd/mm/yyyy") Then
            strAux = Format(rs!CI01FECCONCERT, "dd/mm/yyyy")
            dim1 = dim1 + 1
            ReDim Preserve arCitas(1 To dim1)
            arCitas(dim1).strFecha = strAux
            dim2 = 0
        End If
        dim2 = dim2 + 1
        ReDim Preserve arCitas(dim1).arCitaDatos(1 To dim2)
        With arCitas(dim1).arCitaDatos(dim2)
            .strHora = Format(rs!CI01FECCONCERT, "hh:mm")
            .lngNumMinOcu = rs!MINOCUPREC
            .strCodAct = rs!PR01CODACTUACION
        End With
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pCargarDptoUser()
'****************************************************************************************
'*  Carga en una colecci�n todos los Dptos a los que tiene acceso el usuario
'****************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    Do While cllDptoUser.Count > 0: cllDptoUser.Remove 1: Loop
    SQL = "SELECT AD02CODDPTO"
    SQL = SQL & " FROM AD0300"
    SQL = SQL & " WHERE SG02COD = ?"
    SQL = SQL & " ORDER BY AD02CODDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = objSecurity.strUser
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cllDptoUser.Add CStr(rs(0))
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pCargarFranjas(strCodRecurso$, strCodAct$, Optional intCodPerf%, Optional intCodFrja%)
'******************************************************************************************
'*  Carga en una estructura las franjas del recursos en las que se pueden realizar la
'*  actuaci�n seleccionada
'******************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim dim1%, i%
    
    Erase arFranjas
    
    'NOTA: DE MOMENTO SE HA PUESTO LA RESTRICCI�N PARA USARLO S�LO CON PRUEBAS Y NO CON CONSULTAS
    
    'NOTA: PARECE QUE AG04MODASIGCITA=NULL QUIERE INDICAR QUE NO HAY FRANJA DISPONIBLE,
    'INDEPENDIENTEMENTE DE QUE S� EST� DEFINIDA LA FRANJA
    
    SQL = "SELECT AG0400.AG04CODFRANJA,"
    SQL = SQL & " AG04INDLUNFRJA, AG04INDMARFRJA, AG04INDMIEFRJA, AG04INDJUEFRJA,"
    SQL = SQL & " AG04INDVIEFRJA, AG04INDSABFRJA, AG04INDDOMFRJA,"
    SQL = SQL & " LPAD(AG04HORINFRJHH,2,'00')||':'||LPAD(AG04HORINFRJMM,2,'00') AG04HORAINI,"
    SQL = SQL & " LPAD(AG04HORFIFRJHH,2,'00')||':'||LPAD(AG04HORFIFRJMM,2,'00') AG04HORAFIN,"
    SQL = SQL & " NVL(AG04MODASIGCITA,0) AG04MODASIGCITA, AG04INTERVCITA, AG04NUMCITADMI,"
    SQL = SQL & " AG0100.AG01NUMASIGADM,"
    SQL = SQL & " AG0400.AG07CODPERFIL, NVL(AG04FECBAJA,TO_DATE('31/12/9999','DD/MM/YYYY')) AG04FECBAJA" 'PARA CONTROL POSTERIOR
    SQL = SQL & " FROM AG0100, AG0400"
    SQL = SQL & " WHERE AG0400.AG11CODRECURSO = ?"
    SQL = SQL & " AND (AG0400.PR12CODACTIVIDAD IN (" & constACTIV_PRUEBA & "," & constACTIV_CONSULTA & ")"
    SQL = SQL & " OR AG0400.PR12CODACTIVIDAD IS NULL)"
    SQL = SQL & " AND AG0100.AG11CODRECURSO = AG0400.AG11CODRECURSO"
    SQL = SQL & " AND AG0100.AG07CODPERFIL = AG0400.AG07CODPERFIL"
    SQL = SQL & " AND AG0100.AG04CODFRANJA = AG0400.AG04CODFRANJA"
    SQL = SQL & " AND AG0100.PR01CODACTUACION = ?"
    If intCodPerf > 0 Then SQL = SQL & " AND AG0400.AG07CODPERFIL = ?"
    If intCodFrja > 0 Then SQL = SQL & " AND AG0400.AG04CODFRANJA = ?"
    SQL = SQL & " ORDER BY AG0400.AG07CODPERFIL, AG04HORINFRJHH, AG04HORINFRJMM"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodRecurso
    qry(1) = strCodAct
    i = 2
    If intCodPerf > 0 Then qry(i) = intCodPerf: i = i + 1
    If intCodFrja > 0 Then qry(i) = intCodFrja: i = i + 1
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        Call pCargarRestricRec(strCodRecurso) 'restricciones
        If blnCancel Then Exit Sub 'b�squeda cancelada
        Do While Not rs.EOF
            If fFranjaValida(rs!AG07CODPERFIL, rs!AG04CODFRANJA) Then '...seg�n restricciones
                dim1 = dim1 + 1
                ReDim Preserve arFranjas(1 To dim1)
                With arFranjas(dim1)
                    .intCodFranja = rs!AG04CODFRANJA
                    .intCodPerfil = rs!AG07CODPERFIL
                    .intL = rs!AG04INDLUNFRJA
                    .intM = rs!AG04INDMARFRJA
                    .intX = rs!AG04INDMIEFRJA
                    .intJ = rs!AG04INDJUEFRJA
                    .intV = rs!AG04INDVIEFRJA
                    .intS = rs!AG04INDSABFRJA
                    .intD = rs!AG04INDDOMFRJA
                    .strHoraIni = rs!AG04HORAINI
                    .strHoraFin = rs!AG04HORAFIN
                    .strModo = rs!AG04MODASIGCITA
                    If Not IsNull(rs!AG04INTERVCITA) Then .intInterv = rs!AG04INTERVCITA
                    .intNumCitaAdmi = rs!AG04NUMCITADMI
                    .intNumCitaActAdmi = rs!AG01NUMASIGADM
                    .strFecBaja = rs!AG04FECBAJA
                End With
            End If
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pCargarHuecos(strFechaIni$, strFechaFin$, iAct%, iRec%, strCodAct$)
'***************************************************************************************
'*  Subrutina que busca los huecos en las franjas de los recursos seg�n el modo de
'*  asignaci�n de las citas para cada una de las franjas
'*  Para HOY (d�a actual) s�lo se cargan los huecos si son de una hora posterior a la actual
'*  (citaci�n por cantidad y por intervalos) o si el hueco termina en una hora posterior
'*  a la actual (citaci�n secuencial)
'*
'*  El 30/05/00 (I�igo, M� Esther, Alberto y Ra�l) se decide que el modo SECUENCIAL es el
'*  m�s correcto y se tender� a que todas las franjas posibles se citen en dicho modo
'*
'*  CITACI�N POR CANTIDAD
'*  Se limita el n� de citas y el n� de citas de una cierta actuaci�n en una franja.
'*  Todas las citas se realizan para la hora de inicio de la franja
'*
'*  CITACI�N POR INTERVALOS
'*  Se limita el n� de citas y el n� de citas de una cierta actuaci�n en una franja.
'*  Las citas se realizan para las horas se�aladas por el intervalo de la franja
'*
'*  CITACI�N POR INTERVALOS OSCILANTES (21/08/00 Alberto y Ra�l)
'*  Se recomienda utilizar para todos aquellos recursos que realizan pruebas de diferentes
'*  duraciones pero existe alguna de ellas que es la m�s habitual. Se toma la duraci�n
'*  de esta prueba como el intervalo de la franja
'*  NO se tiene en cuenta las limitaciones existentes para el n� de citas y el n� de
'*  citas de una cierta actuaci�n
'*  Las citas se realizan para las horas se�aladas, obtenidas en funci�n del intervalo de
'*  de la franja y de la duraci�n de las actuaciones ya citadas
'*  Los intervalos no son fijos en le horario sino que var�an en funci�n de las citas ya
'*  realizadas para un mejor ajuste de los tiempos
'*
'*  CITACI�N SECUENCIAL
'*  NO se tiene en cuenta las limitaciones existentes para el n� de citas y el n� de
'*  citas de una cierta actuaci�n
'*  Las citas se realizan para las horas libres, obtenidas en funci�n de la duraci�n
'*  de las actuaciones ya citadas
'***************************************************************************************
    Dim strFecha$ 'fecha que se procesa
    Dim strFechaAux$ 'fecha auxiliar para comparaci�n de fechas
    Dim nDia% 'n� del d�a procesado desde la strFechaIni
    Dim intCodPerfil% 'c�digo de perfil a usar (puede variar seg�n la fecha)
    Dim f%, maxFranjas%, c%, maxFecCitas%, d%, maxDatCitas%, maxHuecos%
    Dim intNCitas%, intNCitasAct%, intNCitasLibres%
    Dim arInterv() As typeIntervalos, nInterv%, strHoraI$, strHoraF$, strHora$
    Dim i%, j%
    Dim lngTORec&
    
    lngTORec = Val(Mid$(lblTORec(iAct).Caption, 11)) 'duraci�n de la actuaci�n que se va a citar
    
    strFecha = Format(strFechaIni, "dd/mm/yyyy")
    Do While CDate(strFecha) <= CDate(strFechaFin)
        nDia = nDia + 1
        If fFechaLaborable(strFecha) Then 'd�a laborable
            intCodPerfil = fCodPerfil(strFecha)
            maxFranjas = 0
            On Error Resume Next
            maxFranjas = UBound(arFranjas)
            On Error GoTo 0
            For f = 1 To maxFranjas
                If CDate(arFranjas(f).strFecBaja) >= CDate(strFecha) Then
                    If arFranjas(f).intCodPerfil = intCodPerfil Then 'franja aplicable
                        If fFranjaDiaFranjaHabil(strFecha, f) Then 'd�a h�bil en franja
                            Select Case arFranjas(f).strModo 'modo de asignaci�n de la cita
                            Case constCITA_PORCANTIDAD '*** POR CANTIDAD ***
                                If CDate(strFecha & " " & arFranjas(f).strHoraIni) >= CDate(strAhora) Then
                                    'se mira el n� de citas ya existentes para la franja y el d�a procesado
                                    intNCitas = 0: intNCitasAct = 0
                                    maxFecCitas = 0
                                    On Error Resume Next
                                    maxFecCitas = UBound(arCitas)
                                    On Error GoTo 0
                                    For c = 1 To maxFecCitas
                                        If arCitas(c).strFecha = strFecha Then
                                            maxDatCitas = 0
                                            On Error Resume Next
                                            maxDatCitas = UBound(arCitas(c).arCitaDatos)
                                            On Error GoTo 0
                                            For d = 1 To maxDatCitas
                                                If arCitas(c).arCitaDatos(d).lngNumMinOcu > 0 Then 'Cita NO manual
                                                    If CDate(arCitas(c).arCitaDatos(d).strHora) >= CDate(arFranjas(f).strHoraIni) _
                                                    And CDate(arCitas(c).arCitaDatos(d).strHora) < CDate(arFranjas(f).strHoraFin) Then
                                                        intNCitas = intNCitas + 1 'N� citas en franja
                                                        If arCitas(c).arCitaDatos(d).strCodAct = strCodAct Then
                                                            intNCitasAct = intNCitasAct + 1 'N� citas de la  Actuaci�n en franja
                                                        End If
                                                    End If
                                                End If
                                            Next d
                                            Exit For
                                        End If
                                    Next c
                                    'se anotan los huecos libres para el d�a - actuaci�n - recurso
                                    intNCitasLibres = fNCitasLibres(arFranjas(f).intNumCitaAdmi - intNCitas, arFranjas(f).intNumCitaActAdmi - intNCitasAct)
                                    If intNCitasLibres > 0 Then
                                        If strFechaAux <> strFecha Then
                                            strFechaAux = strFecha
                                            arHuecos(nDia).blnHueco = True
                                            ReDim Preserve arHuecos(nDia).arAct(0 To cboAct.Count - 1)
                                            arHuecos(nDia).arAct(iAct).blnHueco = True
                                            maxHuecos = 0
                                        End If
                                        ReDim Preserve arHuecos(nDia).arAct(iAct).arRec(1 To cboRec(iAct).Rows)
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).blnHueco = True
                                        maxHuecos = maxHuecos + 1
                                        ReDim Preserve arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(1 To maxHuecos)
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).strHoraIni = arFranjas(f).strHoraIni
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).strNHuecos = intNCitasLibres
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).intCodFranja = arFranjas(f).intCodFranja
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).intCodPerfil = arFranjas(f).intCodPerfil
                                    End If
                                End If
                            
                            Case constCITA_PORINTERVALOS '*** POR INTERVALOS ***
                                If CDate(strFecha & " " & arFranjas(f).strHoraFin) >= CDate(strAhora) Then
                                    'fraccionamiento de la franja en intervalos
                                    nInterv = 0: Erase arInterv
                                    For i = 1 To arFranjas(f).intNumCitaAdmi
                                        If i = 1 Then
                                            strHoraI = arFranjas(f).strHoraIni
                                        Else
                                            strHoraI = strHoraF
                                        End If
                                        strHoraF = Format(DateAdd("n", arFranjas(f).intInterv, strHoraI), "hh:mm")
                                        If CDate(strFecha & " " & strHoraI) >= CDate(strAhora) Then
                                            nInterv = nInterv + 1
                                            ReDim Preserve arInterv(1 To nInterv)
                                            arInterv(nInterv).strHoraIni = strHoraI
                                            arInterv(nInterv).strHoraFin = strHoraF
                                        End If
                                    Next i
                                
                                    'se mira el n� de citas ya existentes para la franja y el d�a procesado
                                    intNCitas = 0: intNCitasAct = 0
                                    maxFecCitas = 0
                                    On Error Resume Next
                                    maxFecCitas = UBound(arCitas)
                                    On Error GoTo 0
                                    For c = 1 To maxFecCitas
                                        If arCitas(c).strFecha = strFecha Then
                                            maxDatCitas = 0
                                            On Error Resume Next
                                            maxDatCitas = UBound(arCitas(c).arCitaDatos)
                                            On Error GoTo 0
                                            For d = 1 To maxDatCitas
                                                If arCitas(c).arCitaDatos(d).lngNumMinOcu > 0 Then 'Cita NO manual
                                                    If CDate(arCitas(c).arCitaDatos(d).strHora) >= CDate(arFranjas(f).strHoraIni) _
                                                    And CDate(arCitas(c).arCitaDatos(d).strHora) < CDate(arFranjas(f).strHoraFin) Then
                                                        intNCitas = intNCitas + 1 'N� citas en franja
                                                        If arCitas(c).arCitaDatos(d).strCodAct = strCodAct Then
                                                            intNCitasAct = intNCitasAct + 1 'N� citas de la  Actuaci�n en franja
                                                        End If
                                                        'se anotan los intervalos ya ocupados por citas existentes
                                                        For i = 1 To nInterv
                                                            If Not arInterv(i).blnOcupado Then
                                                                If CDate(arCitas(c).arCitaDatos(d).strHora) < CDate(arInterv(i).strHoraFin) _
                                                                And CDate(arCitas(c).arCitaDatos(d).strHora) >= CDate(arInterv(i).strHoraIni) Then
                                                                    arInterv(i).blnOcupado = True
                                                                    Exit For
                                                                End If
                                                            End If
                                                        Next i
                                                    End If
                                                End If
                                            Next d
                                            Exit For
                                        End If
                                    Next c
                                    'se anotan los huecos libres para el d�a - actuaci�n - recurso
                                    intNCitasLibres = fNCitasLibres(arFranjas(f).intNumCitaAdmi - intNCitas, arFranjas(f).intNumCitaActAdmi - intNCitasAct)
                                    If intNCitasLibres > 0 Then
                                        For i = 1 To nInterv
                                            If Not arInterv(i).blnOcupado Then
                                                If strFechaAux <> strFecha Then
                                                    strFechaAux = strFecha
                                                    arHuecos(nDia).blnHueco = True
                                                    ReDim Preserve arHuecos(nDia).arAct(0 To cboAct.Count - 1)
                                                    arHuecos(nDia).arAct(iAct).blnHueco = True
                                                    maxHuecos = 0
                                                End If
                                                ReDim Preserve arHuecos(nDia).arAct(iAct).arRec(1 To cboRec(iAct).Rows)
                                                arHuecos(nDia).arAct(iAct).arRec(iRec).blnHueco = True
                                                maxHuecos = maxHuecos + 1
                                                ReDim Preserve arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(1 To maxHuecos)
                                                arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).strHoraIni = arInterv(i).strHoraIni
                                                arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).strNHuecos = 1
                                                arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).intCodFranja = arFranjas(f).intCodFranja
                                                arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).intCodPerfil = arFranjas(f).intCodPerfil
                                            End If
                                        Next i
                                    End If
                                End If
   
                            Case constCITA_PORINTERVALOSOSCILANTES  '*** INTERVALOS OSCILANTES ***
                                'se mira el n� de citas ya existentes para la franja y el d�a procesado
                                Erase arInterv: i = 0
                                maxFecCitas = 0
                                On Error Resume Next
                                maxFecCitas = UBound(arCitas)
                                On Error GoTo 0
                                For c = 1 To maxFecCitas
                                    If arCitas(c).strFecha = strFecha Then
                                        maxDatCitas = 0
                                        On Error Resume Next
                                        maxDatCitas = UBound(arCitas(c).arCitaDatos)
                                        On Error GoTo 0
                                        For d = 1 To maxDatCitas
                                            If arCitas(c).arCitaDatos(d).lngNumMinOcu > 0 Then 'Cita que ocupa hueco
                                                strHoraI = arCitas(c).arCitaDatos(d).strHora
                                                strHoraF = Format(DateAdd("n", arCitas(c).arCitaDatos(d).lngNumMinOcu, arCitas(c).arCitaDatos(d).strHora), "hh:mm")
                                                If CDate(strHoraF) >= CDate(arFranjas(f).strHoraIni) _
                                                Or CDate(strHoraI) < CDate(arFranjas(f).strHoraFin) Then
                                                    i = i + 1
                                                    'se anota el hueco ocupado por la cita
                                                    ReDim Preserve arInterv(1 To i)
                                                    arInterv(i).strHoraIni = strHoraI
                                                    arInterv(i).strHoraFin = strHoraF
                                                End If
                                            End If
                                        Next d
                                        Exit For
                                    End If
                                Next c
                                'se anotan los huecos libres para el d�a - actuaci�n - recurso
                                If CDate(strFecha & " " & arFranjas(f).strHoraIni) >= CDate(strAhora) Then
                                    strHoraI = arFranjas(f).strHoraIni
                                Else
                                    strHoraI = Format(strAhora, "hh:mm")
                                End If
                                For j = 1 To i + 1 '+1 para tener en cuenta los posibles huecos al final de la franja
                                    If j = i + 1 Then
                                        strHoraF = arFranjas(f).strHoraFin
                                    Else
                                        If CDate(strFecha & " " & arFranjas(f).strHoraFin) <= CDate(strFecha & " " & arInterv(j).strHoraIni) Then
                                            strHoraF = arFranjas(f).strHoraFin
                                        Else
                                            strHoraF = arInterv(j).strHoraIni
                                        End If
                                    End If
                                    If DateDiff("n", strHoraI, strHoraF) >= lngTORec Then
                                        If strFechaAux <> strFecha Then
                                            strFechaAux = strFecha
                                            arHuecos(nDia).blnHueco = True
                                            ReDim Preserve arHuecos(nDia).arAct(0 To cboAct.Count - 1)
                                            arHuecos(nDia).arAct(iAct).blnHueco = True
                                            maxHuecos = 0
                                        End If
                                        ReDim Preserve arHuecos(nDia).arAct(iAct).arRec(1 To cboRec(iAct).Rows)
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).blnHueco = True
                                        strHora = Format(DateAdd("n", lngTORec, strHoraI), "hh:mm")
                                        Do While CDate(strHora) <= CDate(strHoraF)
                                            maxHuecos = maxHuecos + 1
                                            ReDim Preserve arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(1 To maxHuecos)
                                            arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).strHoraIni = strHoraI
                                            arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).strNHuecos = 1
                                            arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).intCodFranja = arFranjas(f).intCodFranja
                                            arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).intCodPerfil = arFranjas(f).intCodPerfil
                                            strHoraI = Format(DateAdd("n", arFranjas(f).intInterv, strHoraI), "hh:mm")
                                            strHora = Format(DateAdd("n", lngTORec, strHoraI), "hh:mm")
                                        Loop
                                    End If
                                    If j < i + 1 Then
                                        If CDate(strFecha & " " & arInterv(j).strHoraFin) >= CDate(strAhora) Then
                                            If CDate(arInterv(j).strHoraFin) >= CDate(strHoraI) Then
                                                strHoraI = arInterv(j).strHoraFin
                                            End If
                                        Else
                                            strHoraI = Format(strAhora, "hh:mm")
                                        End If
                                    End If
                                Next j
                            
                            Case constCITA_SECUENCIAL  '*** SECUENCIAL ***
                                'se mira el n� de citas ya existentes para la franja y el d�a procesado
                                Erase arInterv: i = 0
                                maxFecCitas = 0
                                On Error Resume Next
                                maxFecCitas = UBound(arCitas)
                                On Error GoTo 0
                                For c = 1 To maxFecCitas
                                    If arCitas(c).strFecha = strFecha Then
                                        maxDatCitas = 0
                                        On Error Resume Next
                                        maxDatCitas = UBound(arCitas(c).arCitaDatos)
                                        On Error GoTo 0
                                        For d = 1 To maxDatCitas
                                            If arCitas(c).arCitaDatos(d).lngNumMinOcu > 0 Then 'Cita NO manual
                                                If CDate(arCitas(c).arCitaDatos(d).strHora) >= CDate(arFranjas(f).strHoraIni) _
                                                And CDate(arCitas(c).arCitaDatos(d).strHora) < CDate(arFranjas(f).strHoraFin) Then
                                                    i = i + 1
                                                    'se anota el hueco ocupado por la cita
                                                    ReDim Preserve arInterv(1 To i)
                                                    arInterv(i).strHoraIni = arCitas(c).arCitaDatos(d).strHora
                                                    arInterv(i).strHoraFin = Format(DateAdd("n", arCitas(c).arCitaDatos(d).lngNumMinOcu, arCitas(c).arCitaDatos(d).strHora), "hh:mm")
                                                End If
                                            End If
                                        Next d
                                        Exit For
                                    End If
                                Next c
                                'se anotan los huecos libres para el d�a - actuaci�n - recurso
                                If CDate(strFecha & " " & arFranjas(f).strHoraIni) >= CDate(strAhora) Then
                                    strHoraI = arFranjas(f).strHoraIni
                                Else
                                    strHoraI = Format(strAhora, "hh:mm")
                                End If
                                For j = 1 To i + 1 '+1 para tener en cuenta los posibles huecos al final de la franja
                                    If j = i + 1 Then
                                        strHoraF = arFranjas(f).strHoraFin
                                    Else
                                        If CDate(strFecha & " " & arFranjas(f).strHoraFin) <= CDate(strFecha & " " & arInterv(j).strHoraIni) Then
                                            strHoraF = arFranjas(f).strHoraFin
                                        Else
                                            strHoraF = arInterv(j).strHoraIni
                                        End If
                                    End If
                                    If DateDiff("n", strHoraI, strHoraF) >= lngTORec Then
                                        If strFechaAux <> strFecha Then
                                            strFechaAux = strFecha
                                            arHuecos(nDia).blnHueco = True
                                            ReDim Preserve arHuecos(nDia).arAct(0 To cboAct.Count - 1)
                                            arHuecos(nDia).arAct(iAct).blnHueco = True
                                            maxHuecos = 0
                                        End If
                                        ReDim Preserve arHuecos(nDia).arAct(iAct).arRec(1 To cboRec(iAct).Rows)
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).blnHueco = True
                                        maxHuecos = maxHuecos + 1
                                        ReDim Preserve arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(1 To maxHuecos)
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).strHoraIni = strHoraI
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).strHoraFin = strHoraF
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).intCodFranja = arFranjas(f).intCodFranja
                                        arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(maxHuecos).intCodPerfil = arFranjas(f).intCodPerfil
                                    End If
                                    If j < i + 1 Then
                                        If CDate(strFecha & " " & arInterv(j).strHoraFin) >= CDate(strAhora) Then
                                            strHoraI = arInterv(j).strHoraFin
                                        Else
                                            strHoraI = Format(strAhora, "hh:mm")
                                        End If
                                    End If
                                Next j
                            End Select
                        End If
                    End If
                End If
            Next f
        End If
        strFecha = Format(DateAdd("d", 1, strFecha), "dd/mm/yyyy")
    Loop
End Sub

Private Sub pCargarPaciente(strIdentifPac$, Optional blnNH As Boolean)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    'datos del paciente
    SQL = "SELECT CI21CODPERSONA, CI22NUMHISTORIA,"
    SQL = SQL & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC,"
    SQL = SQL & " CI30CODSEXO"
    SQL = SQL & " FROM CI2200"
    If blnNH Then
        SQL = SQL & " WHERE CI22NUMHISTORIA = ?"
    Else
        SQL = SQL & " WHERE CI21CODPERSONA = ?"
    End If
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strIdentifPac
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        txtCodPers.Text = rs!CI21CODPERSONA
        If Not IsNull(rs!CI22NUMHISTORIA) Then txtNH.Text = rs!CI22NUMHISTORIA Else txtNH.Text = ""
        txtPac.Text = rs!PAC
        txtSexo.Text = rs!CI30CODSEXO
        Call pCargarCitasPac
        cmdVisionGlobal.Enabled = True
        cmdHojaFiliacion.Enabled = True
    Else
        txtCodPers.Text = ""
        txtNH.Text = ""
        txtPac.Text = ""
        txtSexo.Text = ""
        txtTipoEcon.Text = ""
        grdCitasPac.RemoveAll
        cmdVisionGlobal.Enabled = False
        cmdHojaFiliacion.Enabled = False
    End If
    rs.Close
    qry.Close
    strNH = txtNH.Text
End Sub

Private Sub pCargarPerfilesRecurso(strCodRec$, strFecIni$, strFecFin$)
'******************************************************************************************
'*  Carga en una colecci�n los perfiles del recursos que pueden verse involucrados en
'*  los d�as entre los que se va a realizar la b�squeda.
'*  Se puede quitar la limitaci�n de los d�as en el WHERE para cargar todos los perfiles
'*  ya que la b�squeda posterior (fCodPerfil) se encargar� de localizar el perfil correcto
'*  En este segundo caso el tama�o de la colecci�n es mayor que en el primer caso pero
'*  aqu� estan contemplados todos los periodos de vigencia de los recursos
'******************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset

    Do While cllPerfilesRec.Count > 0: cllPerfilesRec.Remove 1: Loop

    SQL = "SELECT AG0900.AG07CODPERFIL, AG09FECINVIPER, AG09FECFIVIPER"
    SQL = SQL & " FROM AG0900, AG0700"
    SQL = SQL & " WHERE AG0900.AG11CODRECURSO = ?"
    SQL = SQL & " AND AG09FECFIVIPER >= TO_DATE(?,'DD/MM/YYYY')" 'strFecIni
    SQL = SQL & " AND AG09FECINVIPER <= TO_DATE(?,'DD/MM/YYYY')" 'strFecFin
    SQL = SQL & " AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO"
    SQL = SQL & " AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL"
    SQL = SQL & " ORDER BY AG07INDPERGENE DESC, AG09FECINVIPER"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodRec
    qry(1) = Format(strFecIni, "dd/mm/yyyy")
    qry(2) = Format(strFecFin, "dd/mm/yyyy")
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cllPerfilesRec.Add CStr(rs!AG07CODPERFIL) _
                    & "|" & CStr(Format(rs!AG09FECINVIPER, "dd/mm/yyyy")) _
                    & "|" & CStr(Format(rs!AG09FECFIVIPER, "dd/mm/yyyy"))
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pCargarRecursosConsultas(iAct%, Optional strCodRec$)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, i%
    
    cboRec(iAct).Text = "": cboRec(iAct).RemoveAll
    
    SQL = "SELECT AG11CODRECURSO, AG11DESRECURSO, AG02CODCALENDA, AG1100.SG02COD"
    SQL = SQL & " FROM AG1100, AD0300, PR1300"
    SQL = SQL & " WHERE AD0300.AD02CODDPTO = ?"
    SQL = SQL & " AND AD03FECINICIO <= SYSDATE"
    SQL = SQL & " AND (AD03FECFIN >= SYSDATE OR AD03FECFIN IS NULL)"
    SQL = SQL & " AND PR1300.PR01CODACTUACION = ?"
    SQL = SQL & " AND PR1300.PR13INDPREFEREN = -1"
    SQL = SQL & " AND PR1300.PR13INDPLANIF = -1"
    SQL = SQL & " AND AG1100.AG14CODTIPRECU = PR1300.AG14CODTIPRECU"
    SQL = SQL & " AND AD0300.SG02COD = AG1100.SG02COD"
    SQL = SQL & " AND AD0300.AD02CODDPTO = AG1100.AD02CODDPTO"
    SQL = SQL & " AND AG11INDPLANIFI = -1"
    SQL = SQL & " AND AG11FECINIVREC <= SYSDATE"
    SQL = SQL & " AND (AG11FECFINVREC >= SYSDATE OR AG11FECFINVREC IS NULL)"
    SQL = SQL & " ORDER BY AG11ORDEN, AG11DESRECURSO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = cboDpto(iAct).Columns(0).Text
    qry(1) = cboAct(iAct).Columns(0).Text
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboRec(iAct).AddItem rs!AG11CODRECURSO _
                    & Chr$(9) & rs!AG11DESRECURSO _
                    & Chr$(9) & rs!AG02CODCALENDA _
                    & Chr$(9) & rs!SG02COD
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    If strCodRec <> "" Then
        For i = 1 To cboRec(iAct).Rows
            If i = 1 Then cboRec(iAct).MoveFirst Else cboRec(iAct).MoveNext
            If cboRec(iAct).Columns("Cod").Text = strCodRec Then
                cboRec(iAct).Text = cboRec(iAct).Columns("Rec").Text
                Exit For
            End If
        Next i
    Else
        If cboRec(iAct).Rows = 1 Then
            cboRec(iAct).Text = cboRec(iAct).Columns("Rec").Text
        End If
    End If
End Sub

Private Sub pCargarRecursosPruebas(iAct%, Optional strCodRec$)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, i%
    
    cboRec(iAct).Text = "": cboRec(iAct).RemoveAll

    SQL = "SELECT AG11CODRECURSO, AG11DESRECURSO, AG02CODCALENDA, AG1100.SG02COD"
    SQL = SQL & " FROM AG1100, PR1300"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    SQL = SQL & " AND PR13INDPREFEREN = -1"
    SQL = SQL & " AND PR13INDPLANIF = -1"
    SQL = SQL & " AND AG1100.AG14CODTIPRECU = PR1300.AG14CODTIPRECU"
    SQL = SQL & " AND AG1100.AD02CODDPTO = ?"
    SQL = SQL & " AND AG11INDPLANIFI = -1"
    SQL = SQL & " AND AG11FECINIVREC <= SYSDATE"
    SQL = SQL & " AND (AG11FECFINVREC >= SYSDATE OR AG11FECFINVREC IS NULL)"
    SQL = SQL & " ORDER BY AG11ORDEN, AG11DESRECURSO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = cboAct(iAct).Columns(0).Text
    qry(1) = cboDpto(iAct).Columns(0).Text
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboRec(iAct).AddItem rs!AG11CODRECURSO _
                    & Chr$(9) & rs!AG11DESRECURSO _
                    & Chr$(9) & rs!AG02CODCALENDA _
                    & Chr$(9) & rs!SG02COD
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
        
    If strCodRec <> "" Then
        For i = 1 To cboRec(iAct).Rows
            If i = 1 Then cboRec(iAct).MoveFirst Else cboRec(iAct).MoveNext
            If cboRec(iAct).Columns("Cod").Text = strCodRec Then
                cboRec(iAct).Text = cboRec(iAct).Columns("Rec").Text
                Exit For
            End If
        Next i
    Else
        If cboRec(iAct).Rows = 1 Then
            cboRec(iAct).Text = cboRec(iAct).Columns("Rec").Text
        End If
    End If
End Sub

Private Sub pCargarRestricRec(strCodRec$)
'******************************************************************************************
'*  Carga en una colecci�n las restricciones en las franjas del recurso, es decir, aquellas
'*  franjas para las que no se va a poder citar
'*  Antes de realizar las b�squedas de los distintos tipos de restricciones, se comprueba
'*  cuales de ellas existen
'******************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, qry1 As rdoQuery, rs1 As rdoResultset

    Do While cllRestricRec.Count > 0: cllRestricRec.Remove 1: Loop
    
    'se mira si existe alg�n tipo de restricci�n para el recusro
    SQL = "SELECT DISTINCT AG16CODTIPREST"
    SQL = SQL & " FROM AG1200"
    SQL = SQL & " WHERE AG11CODRECURSO = ?"
    SQL = SQL & " ORDER BY AG16CODTIPREST"
    Set qry1 = objApp.rdoConnect.CreateQuery("", SQL)
    qry1(0) = strCodRec
    Set rs1 = qry1.OpenResultset()
    If Not rs1.EOF Then
        SQL = "SELECT AG07CODPERFIL, AG04CODFRANJA"
        SQL = SQL & " FROM AG1200"
        SQL = SQL & " WHERE AG11CODRECURSO = ?"
        SQL = SQL & " AND AG16CODTIPREST = ?"
        SQL = SQL & " AND AG12VALDESDERES = ?"
        SQL = SQL & " AND AG12INDINCEXCL = 1"
        SQL = SQL & " UNION"
        SQL = SQL & " (SELECT AG07CODPERFIL, AG04CODFRANJA"
        SQL = SQL & " FROM AG1200"
        SQL = SQL & " WHERE AG11CODRECURSO = ?"
        SQL = SQL & " AND AG16CODTIPREST = ?"
        SQL = SQL & " AND AG12INDINCEXCL = 0"
        SQL = SQL & " MINUS"
        SQL = SQL & " SELECT AG07CODPERFIL, AG04CODFRANJA"
        SQL = SQL & " FROM AG1200"
        SQL = SQL & " WHERE AG11CODRECURSO = ?"
        SQL = SQL & " AND AG16CODTIPREST = ?"
        SQL = SQL & " AND AG12VALDESDERES = ?"
        SQL = SQL & " AND AG12INDINCEXCL = 0)"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strCodRec: qry(3) = strCodRec: qry(5) = strCodRec
        'se buscan posibles restricciones de cada tipo
        Do While Not rs1.EOF
            Select Case rs1!AG16CODTIPREST
            Case constRESTRIC_TIPOECON
                If txtTipoEcon.Text = "" Then
                    LockWindowUpdate 0&
                    Screen.MousePointer = vbDefault
                    frmSelTipoEcon.Show vbModal
                    Set frmSelTipoEcon = Nothing
                    If objPipe.PipeExist("SEL_TIPOECON") Then
                        txtTipoEcon.Text = objPipe.PipeGet("SEL_TIPOECON")
                        Call objPipe.PipeRemove("SEL_TIPOECON")
                    Else
                        blnCancel = True 'b�squeda cancelada
                        Exit Sub
                    End If
                    Me.Refresh
                    LockWindowUpdate Me.hWnd
                    Screen.MousePointer = vbHourglass
                End If
                qry(1) = constRESTRIC_TIPOECON: qry(2) = txtTipoEcon.Text
                qry(4) = constRESTRIC_TIPOECON
                qry(6) = constRESTRIC_TIPOECON: qry(7) = txtTipoEcon.Text
                Set rs = qry.OpenResultset()
                Do While Not rs.EOF
                    cllRestricRec.Add CStr(rs!AG07CODPERFIL) & "|" & CStr(rs!AG04CODFRANJA)
                    rs.MoveNext
                Loop
            Case constRESTRIC_SEXO
                If txtSexo.Text = "" Then
                    LockWindowUpdate 0&
                    Screen.MousePointer = vbDefault
                    frmSelSexo.Show vbModal
                    Set frmSelSexo = Nothing
                    txtSexo.Text = objPipe.PipeGet("SEL_SEXO")
                    Call objPipe.PipeRemove("SEL_SEXO")
                    Me.Refresh
                    LockWindowUpdate Me.hWnd
                    Screen.MousePointer = vbHourglass
                End If
                qry(1) = constRESTRIC_SEXO: qry(2) = txtSexo.Text
                qry(4) = constRESTRIC_SEXO
                qry(6) = constRESTRIC_SEXO: qry(7) = txtSexo.Text
                Set rs = qry.OpenResultset()
                Do While Not rs.EOF
                    cllRestricRec.Add CStr(rs!AG07CODPERFIL) & "|" & CStr(rs!AG04CODFRANJA)
                    rs.MoveNext
                Loop
            Case constRESTRIC_USUARIO
                qry(1) = constRESTRIC_USUARIO: qry(2) = objSecurity.strUser
                qry(4) = constRESTRIC_USUARIO
                qry(6) = constRESTRIC_USUARIO: qry(7) = objSecurity.strUser
                Set rs = qry.OpenResultset()
                Do While Not rs.EOF
                    cllRestricRec.Add CStr(rs!AG07CODPERFIL) & "|" & CStr(rs!AG04CODFRANJA)
                    rs.MoveNext
                Loop
            End Select
            rs1.MoveNext
        Loop
        rs.Close
        qry.Close
    End If
    rs1.Close
    qry1.Close
End Sub

Private Sub pCitar()
    Dim iAct%, msg$, i%
    Dim strNumSolicit$, intNSecCita% 'n� solicitud y de secuencia para la cita (CI0100)
    Dim lngNAPlan&, strCodAct$
    
    Screen.MousePointer = vbHourglass
    
    objApp.BeginTrans
    'bucle que recorrer cada una de las actuaciones
    For iAct = 0 To cboAct.Count - 1
        If cboAct(iAct).Text <> "" Then
            'PETICI�N: si el nivel no est� bloqueado se trata de una consulta y hay que
            'realizar tambi�n la petici�n, no s�lo la cita
            If lblNivel(iAct).Enabled = True Then
                'se genera la petici�n
                If Not fblnGenerarPeticion(iAct) Then
                    Screen.MousePointer = vbDefault
                    msg = "Se ha producido un error al realizar la cita."
                    msg = msg & Chr$(13)
                    msg = msg & "Int�ntelo de nuevo."
                    MsgBox msg, vbExclamation, Me.Caption
                    objApp.RollBackTrans
                    Exit Sub
                End If
            End If
            
            'CITA
            If grdAct(iAct).Visible Then 'Puede ser una cita conjunta; los datos est�n en grdAct
                LockWindowUpdate grdAct(iAct).hWnd
                For i = 1 To grdAct(iAct).Rows
                    If i = 1 Then grdAct(iAct).MoveFirst Else grdAct(iAct).MoveNext
                    lngNAPlan = grdAct(iAct).Columns("NAPlan").Text
                    strCodAct = grdAct(iAct).Columns("Cod").Text
                    'se obtiene el n� de solicitud de la cita y la secuencia
                    intNSecCita = intNSecCita + 1
                    If intNSecCita = 1 Then strNumSolicit = fNextClave("CI31NUMSOLICIT")
                    'se genera la cita
                    If Not fblnGenerarCita(strNumSolicit, intNSecCita, iAct, lngNAPlan, strCodAct) Then
                        Screen.MousePointer = vbDefault
                        msg = "Se ha producido un error al realizar la cita."
                        msg = msg & Chr$(13)
                        msg = msg & "Int�ntelo de nuevo."
                        MsgBox msg, vbExclamation, Me.Caption
                        objApp.RollBackTrans
                        Exit Sub
                    End If
                Next i
                LockWindowUpdate 0&
            Else 'Los datos est�n en cboAct
                lngNAPlan = cboAct(iAct).Columns("NAPlan").Text
                strCodAct = cboAct(iAct).Columns("Cod").Text
                'se obtiene el n� de solicitud de la cita y la secuencia
                intNSecCita = intNSecCita + 1
                If intNSecCita = 1 Then strNumSolicit = fNextClave("CI31NUMSOLICIT")
                'se genera la cita
                If Not fblnGenerarCita(strNumSolicit, intNSecCita, iAct, lngNAPlan, strCodAct) Then
                    Screen.MousePointer = vbDefault
                    msg = "Se ha producido un error al realizar la cita."
                    msg = msg & Chr$(13)
                    msg = msg & "Int�ntelo de nuevo."
                    MsgBox msg, vbExclamation, Me.Caption
                    objApp.RollBackTrans
                    Exit Sub
                End If
            End If
        End If
    Next iAct
    objApp.CommitTrans
    
    Select Case intTipoAcceso
    Case constACCESOCPH_LIBRE
        Call pCargarCitasPac
        cmdCitar.Enabled = False
        cmdImprimir.Enabled = True
        cmdHuecos.Enabled = False
        fraPac.Enabled = False
        cmdBuscarPac.Enabled = False
        txtNH.BackColor = objApp.objUserColor.lngReadOnly
        txtNH.Locked = True
        Screen.MousePointer = vbDefault
        msg = "Cita realizada correctamente." & Chr$(13) & Chr$(13)
        msg = msg & "�Desea Ud. imprimir la confirmaci�n de la cita?"
        If MsgBox(msg, vbQuestion + vbYesNo, Me.Caption) = vbYes Then
            Call pImprimirCitas
        End If
    Case Else
        Screen.MousePointer = vbDefault
''        msg = "Cita realizada correctamente." & Chr$(13) & Chr$(13)
''        msg = msg & "�Desea Ud. imprimir la confirmaci�n de la cita?"
''        If MsgBox(msg, vbQuestion + vbYesNo, Me.Caption) = vbYes Then
''            Call pImprimirCitas
''        End If
        Unload Me
    End Select
End Sub

Private Sub pCrearNivel()
    Dim intN%, strFecha$
    
    LockWindowUpdate Me.hWnd
    
    intN = fraNivel.Count
    
    Load fraNivel(intN)
    Set fraNivel(intN).Container = fraContainer
    fraNivel(intN).Top = constNIVEL_TOP + intN * constNIVEL_HEIGHT
    fraNivel(intN).Visible = True
    
    Load fraAct(intN)
    Set fraAct(intN).Container = fraNivel(intN)
    fraAct(intN).Top = fraAct(intN - 1).Top
    fraAct(intN).Visible = True
    fraAct(intN).Enabled = True
    
    Load lblDpto(intN)
    Set lblDpto(intN).Container = fraAct(intN)
    lblDpto(intN).Top = lblDpto(intN - 1).Top
    lblDpto(intN).Visible = True
    
    Load cboDpto(intN)
    Set cboDpto(intN).Container = fraAct(intN)
    cboDpto(intN).Top = cboDpto(intN - 1).Top
    cboDpto(intN).Text = ""
    If lngNPet > 0 Then cboDpto(intN).RemoveAll 'Entrada por NAPlan o Petici�n
    cboDpto(intN).Visible = True
    cboDpto(intN).BackColor = objApp.objUserColor.lngNormal
            
    Load txtActiv(intN)
    Set txtActiv(intN).Container = fraAct(intN)
    txtActiv(intN).Top = txtActiv(intN - 1).Top
    
    Load lblAct(intN)
    Set lblAct(intN).Container = fraAct(intN)
    lblAct(intN).Top = lblAct(intN - 1).Top
    lblAct(intN).Visible = True
    
    Load cboAct(intN)
    Set cboAct(intN).Container = fraAct(intN)
    cboAct(intN).Top = cboAct(intN - 1).Top
    cboAct(intN).Text = ""
    cboAct(intN).RemoveAll
    cboAct(intN).Visible = True
    cboAct(intN).BackColor = objApp.objUserColor.lngNormal
    
    Load grdAct(intN)
    Set grdAct(intN).Container = fraNivel(intN)
    grdAct(intN).Top = grdAct(intN - 1).Top
    grdAct(intN).RemoveAll
    grdAct(intN).Visible = False
        
    Load lblFecPlan(intN)
    Set lblFecPlan(intN).Container = fraAct(intN)
    lblFecPlan(intN).Top = lblFecPlan(intN - 1).Top
    lblFecPlan(intN).Caption = "Fecha Plan:"
    lblFecPlan(intN).Visible = True
    
    Load lblTORec(intN)
    Set lblTORec(intN).Container = fraAct(intN)
    lblTORec(intN).Top = lblTORec(intN - 1).Top
    lblTORec(intN).Caption = "T.O. Rec:"
    lblTORec(intN).Visible = True
    
    Load lblTOPac(intN)
    Set lblTOPac(intN).Container = fraAct(intN)
    lblTOPac(intN).Top = lblTOPac(intN - 1).Top
    lblTOPac(intN).Caption = "T.O. Pac:"
    lblTOPac(intN).Visible = True
    
    Load lblRec(intN)
    Set lblRec(intN).Container = fraNivel(intN)
    lblRec(intN).Top = lblRec(intN - 1).Top
    lblRec(intN).Visible = True
    
    Load cboRec(intN)
    Set cboRec(intN).Container = fraNivel(intN)
    cboRec(intN).Top = cboRec(intN - 1).Top
    cboRec(intN).Text = ""
    cboRec(intN).RemoveAll
    cboRec(intN).Visible = True
    
    Load mthHuecos(intN)
    Set mthHuecos(intN).Container = fraNivel(intN)
    mthHuecos(intN).Top = mthHuecos(intN - 1).Top
    mthHuecos(intN).Visible = True
    If strFecIni <> "" Then 'si se ha realizado una b�squeda
        strFecha = strFecIni
        'para que el nuevo calendario no aparezca "pintado"
        Do While CDate(strFecha) <= CDate(strFecFin)
            mthHuecos(intN).X.Day(strFecha).StyleSet = "normal"
            strFecha = DateAdd("d", 1, strFecha)
        Loop
    End If

    Load grdHoras(intN)
    Set grdHoras(intN).Container = fraNivel(intN)
    grdHoras(intN).Top = grdHoras(intN - 1).Top
    grdHoras(intN).Visible = True
    
    Load lblHora(intN)
    Set lblHora(intN).Container = fraNivel(intN)
    lblHora(intN).Top = lblHora(intN - 1).Top
    lblHora(intN).Visible = True
    
    Load fraHora(intN)
    Set fraHora(intN).Container = fraNivel(intN)
    fraHora(intN).Top = fraHora(intN - 1).Top
    fraHora(intN).Visible = True
    
    Load mskHora(intN)
    Set mskHora(intN).Container = fraHora(intN)
    mskHora(intN).Top = mskHora(intN - 1).Top
    mskHora(intN).Visible = True
    
    Load txtRecSel(intN)
    Set txtRecSel(intN).Container = fraNivel(intN)
    txtRecSel(intN).Top = txtRecSel(intN - 1).Top
    txtRecSel(intN).Visible = True
    
    Load lblObserv(intN)
    Set lblObserv(intN).Container = fraNivel(intN)
    lblObserv(intN).Top = lblObserv(intN - 1).Top
    lblObserv(intN).Visible = True
    
    Load txtObserv(intN)
    Set txtObserv(intN).Container = fraNivel(intN)
    txtObserv(intN).Top = txtObserv(intN - 1).Top
    txtObserv(intN).Text = ""
    txtObserv(intN).Visible = True
    
    Load cmdObserv(intN)
    Set cmdObserv(intN).Container = fraNivel(intN)
    cmdObserv(intN).Top = cmdObserv(intN - 1).Top
    cmdObserv(intN).Visible = True
    
    Load lblNivel(intN)
    Set lblNivel(intN).Container = fraNivel(intN)
    lblNivel(intN).Top = lblNivel(intN - 1).Top
    lblNivel(intN).Caption = intN + 1
    lblNivel(intN).Visible = True
    lblNivel(intN).Enabled = True
        
    Load lnLine(intN)
    Set lnLine(intN).Container = fraNivel(intN)
    lnLine(intN).X1 = lnLine(intN - 1).X1
    lnLine(intN).X2 = lnLine(intN - 1).X2
    lnLine(intN).Y1 = lnLine(intN - 1).Y1
    lnLine(intN).Y2 = lnLine(intN - 1).Y2
    lnLine(intN).Visible = True
    
    LockWindowUpdate 0&
End Sub

Private Sub pEntradaActPlan(strNAPlan$)
'****************************************************************************************
'*  Carga los datos correspondientes a la actuaci�n o actuaciones que se han pasado como
'*  par�metro para citar o recitar
'*  Si se trata de una recita, se carga por defecto el recurso para el cual hab�a sido
'*  citada anteriormente la actuaci�n. Para ello hay que hacer un outer-join con la
'*  tabla CI0100 y traer los registros, si los hubiera, en estado constESTCITA_CITADA y
'*  y constESTCITA_PENDRECITA. Como no se puede hacer un outer-join con un IN o un OR, se
'*  traen todos los registros, independientemente del estado CI01SITCITA y luego, al
'*  procesarlos se determina cuales interesan y cuales no
'****************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim iAct% 'contador del n� de pruebas cargadas
    Dim strCodRec$, intGrupo%, lngTORec&, lngTOPac&

    
    If Right$(strNAPlan, 1) = "," Then strNAPlan = Left$(strNAPlan, Len(strNAPlan) - 1)
    awk.FS = ",": awk = strNAPlan
    SQL = "SELECT PR0400.AD02CODDPTO, AD02DESDPTO,"
    SQL = SQL & " PR0400.PR01CODACTUACION, PR01DESCORTA, PR12CODACTIVIDAD,"
    SQL = SQL & " PR0400.PR04NUMACTPLAN, PR0400.PR37CODESTADO, PR0400.PR04FECPLANIFIC,"
    SQL = SQL & " PR0400.CI21CODPERSONA,"
    SQL = SQL & " AD1100.CI32CODTIPECON,"
    SQL = SQL & " PR0800.PR09NUMPETICION,"
    SQL = SQL & " NVL(CI01SITCITA,'0') CI01SITCITA, CI0100.AG11CODRECURSO,"
    SQL = SQL & " PR1300.PR13NUMTIEMPREC,"
    SQL = SQL & " NVL(PR6600.PR66CODGRUPO,0) PR66CODGRUPO"
    SQL = SQL & " FROM PR1300, PR0300, AD0200, PR0100, AD1100, CI0100, PR0800, PR6600, PR0400"
    If awk.NF = 1 Then
        SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    Else
        SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN IN (" & strNAPlan & ")"
    End If
    '* S�LO CONSULTAS O PRUEBAS INCLUIDAS EN LA PR6600
    SQL = SQL & " AND (PR12CODACTIVIDAD = " & constACTIV_CONSULTA
    SQL = SQL & " OR EXISTS (SELECT PR01CODACTUACION FROM PR6600, AD0300"
    SQL = SQL & " WHERE PR6600.AD02CODDPTO = PR0400.AD02CODDPTO"
    SQL = SQL & " AND PR6600.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND AD0300.AD02CODDPTO = PR6600.AD02CODDPTO"
    SQL = SQL & " AND (PR66INDCITAEXTERNA = -1 OR (PR66INDCITAEXTERNA = 0 AND AD0300.SG02COD = ?))))"
    '********
    SQL = SQL & " AND PR0400.PR37CODESTADO IN (" & constESTACT_PLANIFICADA & "," & constESTACT_CITADA & ")"
    SQL = SQL & " AND AD0200.AD02CODDPTO = PR0400.AD02CODDPTO"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND AD1100.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    SQL = SQL & " AND AD1100.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    SQL = SQL & " AND AD11FECFIN IS NULL"
    SQL = SQL & " AND PR0800.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND CI0100.PR04NUMACTPLAN (+)= PR0400.PR04NUMACTPLAN"
    SQL = SQL & " AND PR1300.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR1300.PR13INDPREFEREN = -1"
    SQL = SQL & " AND PR1300.PR13INDPLANIF = -1"
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0300.PR03INDCITABLE = -1"
    SQL = SQL & " AND PR6600.AD02CODDPTO (+)= PR0400.AD02CODDPTO"
    SQL = SQL & " AND PR6600.PR01CODACTUACION (+)= PR0400.PR01CODACTUACION"
    SQL = SQL & " ORDER BY AD02DESDPTO, PR0100.PR12CODACTIVIDAD, PR66CODGRUPO DESC, PR01DESCORTA"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    If awk.NF = 1 Then
      qry(0) = strNAPlan
      qry(1) = objSecurity.strUser
    Else
      qry(0) = objSecurity.strUser
    End If
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        lngNPet = rs!PR09NUMPETICION 'Se utiliza para saber que el tipo de entrada NO ES LIBRE
        iAct = -1
        Call pCargarPaciente(rs!CI21CODPERSONA)
        'NOTA: EL TIPO ECON�MICO DEBER�A ASOCIARSE A CADA UNA DE LAS ACTUACIONES YA QUE
        'PUEDEN SER DISTINTOS EN FUNCI�N EL PROCESO/ASISTENCIA. DE MOMENTO, SUPONIENDO
        'QUE S�LO VAMOS A CITAR UNA ACTUACI�N, SUPONEMOS QUE ES EL MISMO.
        'EN EL FUTURO HABR� QUE CAMBIARLO
        If Not IsNull(rs!CI32CODTIPECON) Then txtTipoEcon.Text = rs!CI32CODTIPECON
        Do While Not rs.EOF
            Select Case Val(rs!CI01SITCITA)
            '0 --> cita, constESTCITA_CITADA, constESTCITA_PENDRECITAR --> recita
            Case 0, constESTCITA_CITADA, constESTCITA_PENDRECITAR
                cllNAPlan.Add CStr(rs!PR04NUMACTPLAN)
                If intGrupo <> rs!PR66CODGRUPO Or rs!PR66CODGRUPO = 0 Then
                    lngTORec = 0: lngTOPac = 0
                    intGrupo = rs!PR66CODGRUPO
                    iAct = iAct + 1
                    If iAct >= constNIVEL_NUMINI Then Call pCrearNivel
                    If Not IsNull(rs!AG11CODRECURSO) Then
                        strCodRec = rs!AG11CODRECURSO 'recita
                    Else
                        strCodRec = "" 'cita
                    End If
                    cboDpto(iAct).AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
                    cboDpto(iAct).Text = rs!AD02DESDPTO
                    cboAct(iAct).AddItem rs!PR01CODACTUACION & Chr$(9) _
                                    & rs!PR01DESCORTA & Chr$(9) _
                                    & rs!PR13NUMTIEMPREC & Chr$(9) _
                                    & rs!PR04NUMACTPLAN
                    cboAct(iAct).Text = rs!PR01DESCORTA
                    If Not IsNull(rs!PR04FECPLANIFIC) Then lblFecPlan(iAct).Caption = "Fecha Plan: " & Format(rs!PR04FECPLANIFIC, "dd/mm/yy hh:mm")
                    txtActiv(iAct).Text = rs!PR12CODACTIVIDAD
                    Select Case rs!PR12CODACTIVIDAD
                    Case constACTIV_PRUEBA
                        Call pCargarRecursosPruebas(iAct, strCodRec)
                    Case constACTIV_CONSULTA
                        Call pCargarRecursosConsultas(iAct, strCodRec)
                    End Select
                End If
                'OBTENER LOS TIEMPOS CORRECTOS A TRAV�S DE ALGUNA FUNCI�N
                'DE MOMENTO, SE SUMAN LOS TIEMPOS TANTO DEL RECURSO COMO DEL PACIENTE
                lngTORec = lngTORec + rs!PR13NUMTIEMPREC
                lngTOPac = lngTOPac + fTOcupPac(rs!PR01CODACTUACION)
                lblTORec(iAct).Caption = "T.O. Rec: " & lngTORec & " min."
                lblTOPac(iAct).Caption = "T.O. Pac: " & lngTOPac & " min."
                grdAct(iAct).AddItem rs!PR01CODACTUACION & Chr$(9) _
                                & rs!PR01DESCORTA & Chr$(9) _
                                & rs!PR13NUMTIEMPREC & Chr$(9) _
                                & rs!PR04NUMACTPLAN
            End Select
            rs.MoveNext
        Loop
        If iAct + 1 >= constNIVEL_NUMINI Then Call pCrearNivel 'para poder a�adir Consultas
        Call pBloquearControles(iAct)
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pEntradaLibre(intIndexInicial%)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, i%
    Dim iAct%
    
    SQL = "SELECT DISTINCT AD0200.AD02CODDPTO, AD02DESDPTO"
    SQL = SQL & " FROM AD0200, PR0200, PR0100"
    SQL = SQL & " WHERE PR0100.PR12CODACTIVIDAD = ?"
    SQL = SQL & " AND PR01FECINICO <= SYSDATE"
    SQL = SQL & " AND (PR01FECFIN >= SYSDATE OR PR01FECFIN IS NULL)"
    SQL = SQL & " AND PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    SQL = SQL & " AND AD0200.AD02CODDPTO = PR0200.AD02CODDPTO"
    SQL = SQL & " AND AD02FECINICIO <= SYSDATE"
    SQL = SQL & " AND (AD02FECFIN >= SYSDATE OR AD02FECFIN IS NULL)"
    SQL = SQL & " ORDER BY AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = constACTIV_CONSULTA
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        For iAct = intIndexInicial To cboAct.Count - 1
            cboDpto(iAct).AddItem rs(0) & Chr$(9) & rs(1)
            txtActiv(iAct).Text = constACTIV_CONSULTA
        Next iAct
        rs.MoveNext
    Loop
    rs.Close
    If cllDptoUser.Count = 1 Then
        For iAct = intIndexInicial To cboAct.Count - 1
            For i = 1 To cboDpto(iAct).Rows
                If i = 1 Then cboDpto(iAct).MoveFirst Else cboDpto(iAct).MoveNext
                If cboDpto(iAct).Columns(0).Text = cllDptoUser.Item(1) Then
                    cboDpto(iAct).Text = cboDpto(iAct).Columns(1).Text
                    cboDpto_Click (iAct)
                    Exit For
                End If
            Next i
        Next iAct
    End If
End Sub

Private Sub pEntradaPersona(strCodPersona$)
    Call pCargarPaciente(strCodPersona)
    fraPac.Enabled = False
    cmdBuscarPac.Enabled = False
    Call pEntradaLibre(0)
    cmdLimpiar.Visible = True
    cmdImprimir.Visible = True
End Sub

Private Sub pEntradaPeticion(lngNPeticion&)
'****************************************************************************************
'*  Carga los datos correspondientes a las actuaciones de la petici�n que se ha pasado
'*  como par�metro para citar
'*  De momento s�lo se admiten citas y NO RECITAS
'****************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim iAct% 'contador del n� de pruebas cargadas
    Dim intGrupo%, lngTORec&, lngTOPac&
 
    'se obtienen todas las pruebas y consultas de la petici�n que se encuentren planificadas
    SQL = "SELECT PR0400.AD02CODDPTO, AD02DESDPTO,"
    SQL = SQL & " PR0400.PR01CODACTUACION, PR01DESCORTA, PR12CODACTIVIDAD,"
    SQL = SQL & " PR04NUMACTPLAN, PR0400.PR37CODESTADO, PR0400.PR04FECPLANIFIC,"
    SQL = SQL & " PR0400.CI21CODPERSONA,"
    SQL = SQL & " PR1300.PR13NUMTIEMPREC,"
    SQL = SQL & " NVL(PR6600.PR66CODGRUPO,0) PR66CODGRUPO"
    SQL = SQL & " FROM PR0400, AD0200, PR0100, PR1300, PR6600, PR0300"
    SQL = SQL & " WHERE PR0300.PR09NUMPETICION = ?"
    SQL = SQL & " AND PR0300.PR03INDCITABLE = -1"
    SQL = SQL & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    '* S�LO CONSULTAS O PRUEBAS INCLUIDAS EN LA PR6600
    SQL = SQL & " AND (PR12CODACTIVIDAD = " & constACTIV_CONSULTA
    SQL = SQL & " OR EXISTS (SELECT PR01CODACTUACION FROM PR6600, AD0300"
    SQL = SQL & " WHERE PR6600.AD02CODDPTO = PR0400.AD02CODDPTO"
    SQL = SQL & " AND PR6600.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND AD0300.AD02CODDPTO = PR6600.AD02CODDPTO"
    SQL = SQL & " AND (PR66INDCITAEXTERNA = -1 OR (PR66INDCITAEXTERNA = 0 AND AD0300.SG02COD = ?))))"
    '********
    SQL = SQL & " AND PR0400.PR37CODESTADO IN (" & constESTACT_PLANIFICADA & ")"
    'SQL = SQL & " AND PR0400.PR37CODESTADO IN (" & constESTACT_PLANIFICADA & "," & constESTACT_CITADA & ")"
    SQL = SQL & " AND AD0200.AD02CODDPTO = PR0400.AD02CODDPTO"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR1300.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR1300.PR13INDPREFEREN = -1"
    SQL = SQL & " AND PR1300.PR13INDPLANIF = -1"
    SQL = SQL & " AND PR6600.AD02CODDPTO (+)= PR0400.AD02CODDPTO"
    SQL = SQL & " AND PR6600.PR01CODACTUACION (+)= PR0400.PR01CODACTUACION"
    SQL = SQL & " ORDER BY AD02DESDPTO, PR0100.PR12CODACTIVIDAD, PR66CODGRUPO DESC, PR01DESCORTA"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNPeticion
    qry(1) = objSecurity.strUser
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        lngNPet = lngNPeticion 'Se utiliza para saber que el tipo de entrada NO ES LIBRE
        iAct = -1
        Call pCargarPaciente(rs!CI21CODPERSONA)
        Do While Not rs.EOF
            cllNAPlan.Add CStr(rs!PR04NUMACTPLAN)
            If intGrupo <> rs!PR66CODGRUPO Or rs!PR66CODGRUPO = 0 Then
                lngTORec = 0: lngTOPac = 0
                intGrupo = rs!PR66CODGRUPO
                iAct = iAct + 1
                If iAct >= constNIVEL_NUMINI Then Call pCrearNivel
                cboDpto(iAct).AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
                cboDpto(iAct).Text = rs!AD02DESDPTO
                cboAct(iAct).AddItem rs!PR01CODACTUACION & Chr$(9) _
                                & rs!PR01DESCORTA & Chr$(9) _
                                & rs!PR13NUMTIEMPREC & Chr$(9) _
                                & rs!PR04NUMACTPLAN
                cboAct(iAct).Text = rs!PR01DESCORTA
                If Not IsNull(rs!PR04FECPLANIFIC) Then lblFecPlan(iAct).Caption = "Fecha Plan: " & Format(rs!PR04FECPLANIFIC, "dd/mm/yy hh:mm")
                txtActiv(iAct).Text = rs!PR12CODACTIVIDAD
                Select Case rs!PR12CODACTIVIDAD
                Case constACTIV_PRUEBA
                    Call pCargarRecursosPruebas(iAct)
                Case constACTIV_CONSULTA
                    Call pCargarRecursosConsultas(iAct)
                End Select
            End If
            'OBTENER LOS TIEMPOS CORRECTOS A TRAV�S DE ALGUNA FUNCI�N
            'DE MOMENTO, SE SUMAN LOS TIEMPOS TANTO DEL RECURSO COMO DEL PACIENTE
            lngTORec = lngTORec + rs!PR13NUMTIEMPREC
            lngTOPac = lngTOPac + fTOcupPac(rs!PR01CODACTUACION)
            lblTORec(iAct).Caption = "T.O. Rec: " & lngTORec & " min."
            lblTOPac(iAct).Caption = "T.O. Pac: " & lngTOPac & " min."
            grdAct(iAct).AddItem rs!PR01CODACTUACION & Chr$(9) _
                            & rs!PR01DESCORTA & Chr$(9) _
                            & rs!PR13NUMTIEMPREC & Chr$(9) _
                            & rs!PR04NUMACTPLAN
            rs.MoveNext
        Loop
        If iAct + 1 >= constNIVEL_NUMINI Then Call pCrearNivel 'para poder a�adir Consultas
        Call pBloquearControles(iAct)
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pFormatearControles()
    Dim iAct%
    
    'grdCitasPac
    With grdCitasPac
        .Columns(0).Caption = "Fecha"
        .Columns(0).Width = 1500
        .Columns(1).Caption = "Ocup. (min.)"
        .Columns(1).Width = 1000 '1200
        .Columns(1).Alignment = ssCaptionAlignmentCenter
        .Columns(2).Caption = "Actuaci�n"
        .Columns(2).Width = 2000
        .Columns(3).Caption = "Recurso"
        .Columns(3).Width = 2000
        .Columns(4).Caption = "NAPlan"
        .Columns(4).Visible = False
            
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    
    'grdHoras
    For iAct = 0 To cboAct.Count - 1
        With grdHoras(iAct)
            .Columns(0).Caption = "De"
            .Columns(0).Width = 550
            .Columns(1).Caption = "A"
            .Columns(1).Width = 550
            .Columns(2).Caption = "N�"
            .Columns(2).Width = 350
            .Columns(2).Alignment = ssCaptionAlignmentCenter
            .Columns(3).Caption = "Recurso"
            .Columns(3).Width = 2000
            .Columns(4).Caption = "CodRec"
            .Columns(4).Visible = False
            .Columns(5).Caption = "Dr"
            .Columns(5).Visible = False
            .Columns(6).Caption = "CodFrja"
            .Columns(6).Visible = False
            .Columns(7).Caption = "CodPerf"
            .Columns(7).Visible = False
            
            .BackColorEven = objApp.objUserColor.lngReadOnly
            .BackColorOdd = objApp.objUserColor.lngReadOnly
        End With
        
        txtActiv(iAct).Text = constACTIV_CONSULTA
        fraHora(iAct).Enabled = False
    Next iAct
    
    'cboDpto
    '   Cod: c�digo del departamento
    '   Dpto: nombre del departamento
    
    'cboAct
    '   Cod: c�digo de la actuaci�n
    '   Act: nombre de la actuaci�n
    '   Min: duraci�n de la actuaci�n en minutos
    '   NAPlan: n� actuaci�n planificada
    '   ReqDoc: indicador de requiere documento
    
    'cboRec
    '   Cod: c�digo del recurso
    '   Rec: nombre del recurso
    '   Cal: c�digo del calendario que emplea el recurso
    '   Dr: C�digo de usuario (Doctor) asociado al recurso
End Sub

Private Sub pImprimirCitas()
    Dim iAct%, strNAPlan$
    
    For iAct = 0 To fraNivel.Count - 1
        If cboAct(iAct).Text <> "" Then
            strNAPlan = strNAPlan & "," & cboAct(iAct).Columns("NAPlan").Text
        End If
    Next iAct
    strNAPlan = Mid$(strNAPlan, 2)
    Call objPipe.PipeSet("AG0218_PR04NUMACTPLAN", strNAPlan)
    frmImprimirCitas.Show vbModal
    Set frmImprimirCitas = Nothing
End Sub

Private Sub pLimpiarHuecos()
'se limpia la pantalla para realizar una nueva b�squeda de huecos
    Dim strFecha$
    Dim iAct%
    
    If strFecIni <> "" Then 'si se ha realizado una b�squeda
        LockWindowUpdate Me.hWnd
        Erase arHuecos
        For iAct = 0 To cboAct.Count - 1
            'se vac�an los grids de huecos
            grdHoras(iAct).RemoveAll
            'se quitan los style de los calendarios
            strFecha = strFecIni
            Do While CDate(strFecha) <= CDate(strFecFin)
                mthHuecos(iAct).X.Day(strFecha).StyleSet = "normal"
                strFecha = DateAdd("d", 1, strFecha)
            Loop
            mthHuecos(iAct).MinDate = Format(strAhora, "dd/mm/yyyy")
            mthHuecos(iAct).MaxDate = Format(DateAdd("yyyy", 3, strAhora), "dd/mm/yyyy")
            mthHuecos(iAct).Date = mthHuecos(iAct).MinDate
            'se quitan los datos del hueco (hora - recurso) seleccionado para la cita
            mskHora(iAct).BackColor = objApp.objUserColor.lngNormal
            mskHora(iAct).Text = "__:__"
            fraHora(iAct).Enabled = False
            txtRecSel(iAct).Text = ""
        Next iAct
    
        'se quita la strFecIni para indicar que ya no se est� en un proceso de b�squeda
        strFecIni = "": strFecFin = ""
        LockWindowUpdate 0&
    End If
End Sub

Private Sub pLimpiarPantalla()
'se limpia la pantalla para realizar una nueva cita
    Dim iAct%
    
    If intTipoAcceso = constACCESOCPH_LIBRE Then
        txtCodPers.Text = ""
        txtNH.Text = "": strNH = ""
        txtPac.Text = ""
        txtSexo.Text = ""
        txtTipoEcon.Text = ""
        txtNH.BackColor = objApp.objUserColor.lngNormal
        txtNH.Locked = False
        fraPac.Enabled = True
        cmdBuscarPac.Enabled = True
        cmdCitar.Enabled = True
        cmdImprimir.Enabled = False
        cmdHuecos.Enabled = True
        grdCitasPac.RemoveAll
        cmdVisionGlobal.Enabled = False
        cmdHojaFiliacion.Enabled = False
    End If
    
    dcboFecDesde.Date = Format(strAhora, "dd/mm/yyyy")
    dcboFecHasta.Date = DateAdd("d", 30, dcboFecDesde.Date)
    
    For iAct = 0 To cboAct.Count - 1
        fraAct(iAct).Enabled = True
        cboDpto(iAct).MoveFirst: cboDpto(iAct).Text = ""
        cboDpto(iAct).BackColor = objApp.objUserColor.lngNormal
        txtActiv(iAct).Text = constACTIV_CONSULTA
        cboAct(iAct).RemoveAll: cboAct(iAct).Text = ""
        cboAct(iAct).BackColor = objApp.objUserColor.lngNormal
        grdAct(iAct).RemoveAll
        lblTORec(iAct).Caption = "T.O. Rec:"
        lblTOPac(iAct).Caption = "T.O. Pac:"
        lblFecPlan(iAct).Caption = "Fecha Plan:"
        cboRec(iAct).RemoveAll: cboRec(iAct).Text = ""
        txtObserv(iAct).Text = ""
    Next iAct
    Call pLimpiarHuecos
    
    If intTipoAcceso = constACCESOCPH_LIBRE Then txtNH.SetFocus
End Sub

Private Sub pVerDiasHuecos()
    Dim nDia%, strFecha$, iAct%
    Dim strStyle$
    Dim msg$
    
    For nDia = 1 To intNDias
        If arHuecos(nDia).blnHueco Then 'si el d�a tiene alg�n hueco...
            strFecha = DateAdd("d", nDia - 1, strFecIni)
            '... se determina si es com�n para todas las actuaciones o no
            strStyle = "hueco_comun"
            For iAct = 0 To cboAct.Count - 1
                'si alguna de las actuaciones no tiene hueco, el hueco no es com�n
                If cboAct(iAct).Text <> "" And Not arHuecos(nDia).arAct(iAct).blnHueco Then
                    strStyle = "hueco"
                    Exit For
                End If
            Next iAct
            'se establece el style de cada d�a en cada calendario
            For iAct = 0 To cboAct.Count - 1
                If arHuecos(nDia).arAct(iAct).blnHueco Then
                    mthHuecos(iAct).X.Day(strFecha).StyleSet = strStyle
                    cboAct(iAct).Tag = "HUECO" 'para avisar si no se han encontrado huecos
                Else
                    mthHuecos(iAct).X.Day(strFecha).StyleSet = "normal"
                End If
            Next iAct
        End If
    Next nDia
    
    'se acotan los calendarios y se muestran los huecos del d�a seleccionado
    For iAct = 0 To cboAct.Count - 1
        mthHuecos(iAct).MinDate = strFecIni
        mthHuecos(iAct).MaxDate = strFecFin
        mthHuecos(iAct).Date = mthHuecos(iAct).MaxDate 'para que desaparezca el primer mes si no tiene ning�n d�a
        mthHuecos(iAct).Date = mthHuecos(iAct).MinDate
        If cboAct(iAct).Text <> "" And cboAct(iAct).Tag = "" Then
            msg = msg & "--> " & fTextoVariasAct(iAct) & Chr$(13)
        Else
            Call pVerHorasHuecosPorHora(mthHuecos(iAct).Date, iAct)
        End If
        cboAct(iAct).Tag = ""
    Next iAct
    
    'mensaje para indicar si se ha encontrado huecos
    If msg <> "" Then
        Screen.MousePointer = vbDefault
        msg = "No se ha encontrado ning�n hueco entre el " & dcboFecDesde.Date _
            & " y el " & dcboFecHasta & " para las siguientes actuaciones:" _
            & Chr$(13) & Chr$(13) & msg
        MsgBox msg, vbInformation, Me.Caption
    End If
End Sub

Private Sub pVerHorasHuecosPorHora(strFecha$, iAct%)
'se muestran los huecos de una d�a - actuaci�n ordenados por hora de inicio del hueco
    Dim nDia%, iRec%, iHueco%, i%, pos%, nAct%
    Dim strAux$
    
    If strFecIni <> "" Then 'si se ha realizado una b�squeda de huecos
        Screen.MousePointer = vbHourglass
        LockWindowUpdate grdHoras(iAct).hWnd
        'inicializaci�n de controles
        grdHoras(iAct).RemoveAll
        grdHoras(iAct).Caption = Format(strFecha, "Long Date")
        mskHora(iAct).BackColor = objApp.objUserColor.lngNormal
        mskHora(iAct).Text = "__:__"
        fraHora(iAct).Enabled = False
        txtRecSel(iAct).Text = ""
        
        nDia = DateDiff("d", strFecIni, strFecha) + 1
        On Error Resume Next
        nAct = UBound(arHuecos(nDia).arAct) 'si no hab�a franja, el array no est� dimensionado
        On Error GoTo 0
        If nAct > 0 Then
            If arHuecos(nDia).arAct(iAct).blnHueco Then
                'se anota el registro en el que est� posicionado el cboRec(iAct)
                strAux = cboRec(iAct).Columns("Cod").Text
                For iRec = 1 To cboRec(iAct).Rows 'se busca en todos los posibles recursos
                    If iRec = 1 Then cboRec(iAct).MoveFirst Else cboRec(iAct).MoveNext
                    If arHuecos(nDia).arAct(iAct).arRec(iRec).blnHueco Then
                        For iHueco = 1 To UBound(arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras)
                            'se busca la posici�n en la que se debe a�adir
                            If grdHoras(iAct).Rows = 0 Then
                                pos = 0 'la primera
                            Else
                                pos = grdHoras(iAct).Rows 'la �ltima (por defcto)
                                For i = 1 To grdHoras(iAct).Rows
                                    If i = 1 Then grdHoras(iAct).MoveFirst Else grdHoras(iAct).MoveNext
                                    If CDate(grdHoras(iAct).Columns("De").Text) > CDate(arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).strHoraIni) Then
                                        pos = i - 1 'delante de la actual
                                        Exit For
                                    End If
                                Next i
                            End If
                            'se a�ade el hueco en su posici�n correspondiente
                            grdHoras(iAct).AddItem arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).strHoraIni _
                                & Chr$(9) & arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).strHoraFin _
                                & Chr$(9) & arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).strNHuecos _
                                & Chr$(9) & cboRec(iAct).Columns("Rec").Text _
                                & Chr$(9) & cboRec(iAct).Columns("Cod").Text _
                                & Chr$(9) & cboRec(iAct).Columns("Dr").Text _
                                & Chr$(9) & arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).intCodFranja _
                                & Chr$(9) & arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).intCodPerfil, pos
                        Next iHueco
                    End If
                Next iRec
                'se vuelve a posicionar el cboRec(iAct) en el registro correcto
                For iRec = 1 To cboRec(iAct).Rows 'se busca en todos los posibles recursos
                    If iRec = 1 Then cboRec(iAct).MoveFirst Else cboRec(iAct).MoveNext
                    If strAux = cboRec(iAct).Columns("Cod").Text Then Exit For
                Next
                grdHoras(iAct).MoveFirst
                grdHoras_Click (iAct)
            End If
        End If
        LockWindowUpdate 0&
        Screen.MousePointer = vbDefault
    End If
End Sub

Private Sub pVerHorasHuecosPorRecurso(strFecha$, iAct%)
'se muestran los huecos de una d�a - actuaci�n ordenados por recurso
    Dim nDia%, iRec%, iHueco%, nAct%
    Dim strAux$
    
    If strFecIni <> "" Then 'si se ha realizado una b�squeda de huecos
        Screen.MousePointer = vbHourglass
        LockWindowUpdate grdHoras(iAct).hWnd
        grdHoras(iAct).RemoveAll
        grdHoras(iAct).Caption = Format(strFecha, "Long Date")
        nDia = DateDiff("d", strFecIni, strFecha) + 1
        On Error Resume Next
        nAct = UBound(arHuecos(nDia).arAct) 'si no hab�a franja, el array no est� dimensionado
        On Error GoTo 0
        If nAct > 0 Then
            If arHuecos(nDia).arAct(iAct).blnHueco Then
                'se anota el registro en el que est� posicionado el cboRec(iAct)
                strAux = cboRec(iAct).Columns("Cod").Text
                For iRec = 1 To cboRec(iAct).Rows 'se busca en todos los posibles recursos
                    If iRec = 1 Then cboRec(iAct).MoveFirst Else cboRec(iAct).MoveNext
                    If arHuecos(nDia).arAct(iAct).arRec(iRec).blnHueco Then
                        For iHueco = 1 To UBound(arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras)
                            grdHoras(iAct).AddItem arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).strHoraIni _
                                & Chr$(9) & arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).strHoraFin _
                                & Chr$(9) & arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).strNHuecos _
                                & Chr$(9) & cboRec(iAct).Columns("Rec").Text _
                                & Chr$(9) & cboRec(iAct).Columns("Cod").Text _
                                & Chr$(9) & cboRec(iAct).Columns("Dr").Text _
                                & Chr$(9) & arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).intCodFranja _
                                & Chr$(9) & arHuecos(nDia).arAct(iAct).arRec(iRec).arHoras(iHueco).intCodPerfil
                        Next iHueco
                    End If
                Next iRec
                'se vuelve a posicionar el cboRec(iAct) en el registro correcto
                For iRec = 1 To cboRec(iAct).Rows 'se busca en todos los posibles recursos
                    If iRec = 1 Then cboRec(iAct).MoveFirst Else cboRec(iAct).MoveNext
                    If strAux = cboRec(iAct).Columns("Cod").Text Then Exit For
                Next
                grdHoras_Click (iAct)
            End If
        End If
        LockWindowUpdate 0&
        Screen.MousePointer = vbDefault
    End If
End Sub

Private Function fSolapamientoCitasPac() As Boolean
'***************************************************************************************
'*  Comprueba si para las horas en las que se va a realizar la cita se produce alg�n
'*  solapamiento de horas para el paciente
'***************************************************************************************
    Dim iAct%, nCitas%
    Dim i%, j%, k%, cllCitas As New Collection, lngTOcupPac&
    Dim msg$, msgAct$
    
    Screen.MousePointer = vbHourglass
    'comprobaci�n previa para no tener que realizar b�squedas innecesarias a la Base de Datos
    For iAct = 0 To cboAct.Count - 1
        If cboAct(iAct).Text <> "" Then
            nCitas = nCitas + 1
        End If
    Next iAct
    If nCitas <= 1 And grdCitasPac.Rows = 0 Then
        'no hay solapamiento ya que s�lo va a haber una cita
        Exit Function
    End If

    'se comprueba si entre las citas actuales del paciente y las que se van a dar existe _
    alg�n solapamiento
    If grdCitasPac.Rows > 0 Then
        LockWindowUpdate grdCitasPac.hWnd
        For i = 1 To grdCitasPac.Rows
            If i = 1 Then grdCitasPac.MoveFirst Else grdCitasPac.MoveNext
            If Not fIsItemInCll(cllNAPlan, grdCitasPac.Columns("NAPlan").Text) Then
                cllCitas.Add grdCitasPac.Columns("Fecha").Text _
                    & "|" & grdCitasPac.Columns("Ocup. (min.)").Text _
                    & "|" & grdCitasPac.Columns("Actuaci�n").Text
            End If
        Next i
        grdCitasPac.MoveFirst
        LockWindowUpdate 0&
    End If
    awk.FS = "|"
    For iAct = 0 To cboAct.Count - 1
        If cboAct(iAct).Text <> "" Then
            'lngTOcupPac = fTOcupPac(cboAct(iAct).Columns("Cod").Text)
            lngTOcupPac = Val(Mid$(lblTOPac(iAct).Caption, 11))
            msgAct = fTextoVariasAct(iAct)
            For j = 1 To cllCitas.Count
                awk = cllCitas.Item(j)
                If Format(awk.f(1), "dd/mm/yyyy") = Format(mthHuecos(iAct).Date, "dd/mm/yyyy") Then
                    If CDate(awk.f(1)) < CDate(DateAdd("n", lngTOcupPac, mthHuecos(iAct).Date & " " & mskHora(iAct).Text)) _
                        And CDate(DateAdd("n", awk.f(2), awk.f(1))) > CDate(mthHuecos(iAct).Date & " " & mskHora(iAct).Text) Then
                        msg = msg & "--> " & msgAct & Space(5) & mthHuecos(iAct).Date & " " & mskHora(iAct).Text & " (" & lngTOcupPac & " min.)" & Chr$(13)
                        msg = msg & "--> " & awk.f(3) & Space(5) & awk.f(1) & " (" & awk.f(2) & " min.)" & Chr$(13) & Chr$(13)
                        Exit For
                    End If
                End If
            Next j
            cllCitas.Add mthHuecos(iAct).Date & " " & mskHora(iAct).Text & "|" & lngTOcupPac & "|" & msgAct
        End If
    Next iAct
    If msg <> "" Then
        msg = "Con las horas seleccionadas para realizar las citas se producen solapamientos" _
            & " en los tiempos de ocupaci�n del paciente entre las siguientes actuaciones:" & Chr$(13) & Chr$(13) & msg
        Screen.MousePointer = vbDefault
        MsgBox msg, vbExclamation, Me.Caption
        fSolapamientoCitasPac = True
        Exit Function
    End If
    Screen.MousePointer = vbDefault
End Function
