VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmBuscaActuaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actuaciones"
   ClientHeight    =   4485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6105
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   6105
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtCitasAdmitidas 
      BackColor       =   &H00FFFF00&
      Height          =   285
      Left            =   4680
      MaxLength       =   2
      TabIndex        =   9
      Top             =   4080
      Width           =   375
   End
   Begin VB.CommandButton cmdB 
      Caption         =   "A&nterior"
      Enabled         =   0   'False
      Height          =   375
      Index           =   1
      Left            =   5280
      TabIndex        =   7
      Top             =   120
      Width           =   735
   End
   Begin VB.CommandButton cmdB 
      Caption         =   "Si&guiente"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   4320
      TabIndex        =   6
      Top             =   120
      Width           =   855
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   4080
      Width           =   855
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   5160
      TabIndex        =   4
      Top             =   4080
      Width           =   855
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   375
      Left            =   3480
      TabIndex        =   2
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox txtDescripcion 
      Height          =   285
      Left            =   960
      TabIndex        =   0
      Top             =   120
      Width           =   2415
   End
   Begin SSDataWidgets_B.SSDBGrid grdActuaciones 
      Height          =   3300
      Left            =   0
      TabIndex        =   3
      Top             =   600
      Width           =   5985
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   10557
      _ExtentY        =   5821
      _StockProps     =   79
      Caption         =   $"AG0208.frx":0000
      ForeColor       =   12582912
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Citas Admitidas:"
      Height          =   195
      Index           =   1
      Left            =   3480
      TabIndex        =   8
      Top             =   4080
      Width           =   1110
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Descripci�n"
      Height          =   195
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Top             =   120
      Width           =   840
   End
End
Attribute VB_Name = "frmBuscaActuaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngCodRecurso As Long
Dim intCodPerfil As Integer
Dim intCodFranja As Integer
Dim intBuscar%, blnBuscarOtro As Boolean
Dim cllBuscar As New Collection



Private Sub cmdAceptar_Click()

If grdActuaciones.SelBookmarks.Count = 0 Then
    MsgBox "No se a A�adido ninguna Actuacion", vbInformation, Me.Caption
    Unload Me
ElseIf Trim(txtCitasAdmitidas) = "" Then
    MsgBox "Indique las Citas admitidas", vbExclamation, Me.Caption
Else
    If Val(txtCitasAdmitidas.Text) > _
    Val(frmMantPerfiles.ssgrdFranjas.Columns("Citas").CellValue(frmMantPerfiles.ssgrdFranjas.SelBookmarks(0))) Then
        MsgBox "La franja admite como m�ximo: " & _
        frmMantPerfiles.ssgrdFranjas.Columns("Citas").CellValue(frmMantPerfiles.ssgrdFranjas.SelBookmarks(0)) & " Citas", _
        vbInformation, Me.Caption
        Exit Sub
    End If
    pA�adirActuacion
    Unload Me
End If
End Sub

Private Sub cmdB_Click(Index As Integer)
    If Index = 0 Then intBuscar = intBuscar + 1 Else intBuscar = intBuscar - 1
    If intBuscar < 0 Then intBuscar = 0
    blnBuscarOtro = True
    cmdBuscar_Click
End Sub



Private Sub Form_Load()
If objPipe.PipeExist("AG0202_AG0208_AG11CODRECURSO") Then
    lngCodRecurso = objPipe.PipeGet("AG0202_AG0208_AG11CODRECURSO")
    objPipe.PipeRemove ("AG0202_AG0208_AG11CODRECURSO")
End If
If objPipe.PipeExist("AG0202_AG0208_AG07CODPERFIL") Then
    intCodPerfil = objPipe.PipeGet("AG0202_AG0208_AG07CODPERFIL")
    objPipe.PipeRemove ("AG0202_AG0208_AG07CODPERFIL")
End If
If objPipe.PipeExist("AG0202_AG0208_AG04CODFRANJA") Then
    intCodFranja = objPipe.PipeGet("AG0202_AG0208_AG04CODFRANJA")
    objPipe.PipeRemove ("AG0202_AG0208_AG04CODFRANJA")
End If
pFormatearGrid
pCargarDatos

    
End Sub

Private Sub cmdBuscar_Click()
Dim intN As Integer
Dim i As Integer
Dim blnE As Boolean
If fBlnBuscar Then
        cmdB(0).Enabled = True: cmdB(1).Enabled = True
        If blnBuscarOtro = True Then
            blnBuscarOtro = False
            If intBuscar = 0 Then intBuscar = 1: Exit Sub
        Else
            intBuscar = 1
        End If
        For i = 1 To cllBuscar.Count
            blnE = False
            If UCase(Left$(cllBuscar(i), Len(txtDescripcion.Text))) = UCase(txtDescripcion.Text) Then
    
                    blnE = True

            End If
            If blnE Then
                intN = intN + 1
                If intN = intBuscar Then
                    grdActuaciones.SelBookmarks.RemoveAll
                    LockWindowUpdate Me.hWnd
                    grdActuaciones.MoveFirst
                    grdActuaciones.MoveRecords i - 1
                    LockWindowUpdate 0&
                    grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
                    cmdBuscar.SetFocus
                    Exit Sub
                End If
            End If
        Next i
        If intBuscar = 1 Then

            MsgBox "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda.", vbInformation, Me.Caption
            Exit Sub
        Else
            intBuscar = intBuscar - 1
            Exit Sub
        End If
Else
        blnBuscarOtro = False
End If
End Sub
Private Sub cmdCancelar_Click()
Unload Me
End Sub
Private Function fBlnBuscar() As Boolean
    Dim i%
    If txtDescripcion.Text <> "" Then fBlnBuscar = True: Exit Function
End Function
Private Sub pFormatearGrid()

With grdActuaciones
    .Columns(0).Caption = "CodActuacion"
    .Columns(0).Visible = False
    .Columns(1).Caption = "Actuacion"
    .Columns(1).Width = 2500
    .Columns(2).Caption = "CodDpto"
    .Columns(2).Visible = False
    .Columns(3).Caption = "Departamento"
    .Columns(3).Width = 1300
End With
End Sub
Private Sub pCargarDatos()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strDesCorta As String
Dim intCodActividad As Integer

sql = "SELECT PR12CODACTIVIDAD FROM AG0400 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ?"
sql = sql & " AND AG04CODFRANJA = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intCodFranja
Set rs = qry.OpenResultset()
If IsNull(rs!PR12CODACTIVIDAD) Then intCodActividad = 0 Else intCodActividad = Val(rs!PR12CODACTIVIDAD)
rs.Close
qry.Close

sql = "SELECT /*+ ORDERED INDEX (PR1300 PR1303) */"
sql = sql & "DISTINCT  PR01DESCORTA,PR1300.PR01CODACTUACION,AD0200.AD02CODDPTO,AD02DESDPTO "
sql = sql & "FROM PR1300, PR0100, PR0200, AD0200"
sql = sql & " WHERE PR1300.AG14CODTIPRECU = (SELECT AG14CODTIPRECU FROM AG1100"
sql = sql & " WHERE AG11CODRECURSO = ?)"
sql = sql & " AND PR1300.PR01CODACTUACION = PR0100.PR01CODACTUACION"
sql = sql & " AND PR01FECFIN IS NULL"
If intCodActividad = 0 Then
sql = sql & " AND PR0100.PR12CODACTIVIDAD NOT IN (" & constACTIV_INTERVENCION & "," & constACTIV_HOSPITALIZACION & ")"
Else
sql = sql & " AND PR0100.PR12CODACTIVIDAD = '" & intCodActividad & "'"
End If
sql = sql & " AND PR1300.PR01CODACTUACION = PR0200.PR01CODACTUACION"
sql = sql & " AND PR0200.AD02CODDPTO = AD0200.AD02CODDPTO"
sql = sql & " AND PR1300.PR01CODACTUACION NOT IN (SELECT PR01CODACTUACION FROM AG0100 WHERE "
sql = sql & " AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
sql = sql & " AND AG04CODFRANJA = ?)"
sql = sql & " ORDER BY PR01DESCORTA"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = lngCodRecurso
    qry(2) = intCodPerfil
    qry(3) = intCodFranja
Set rs = qry.OpenResultset()
While Not rs.EOF
    grdActuaciones.AddItem rs!PR01CODACTUACION & Chr$(9) & _
                           rs!PR01DESCORTA & Chr$(9) & _
                           rs!AD02CODDPTO & Chr$(9) & _
                           rs!AD02DESDPTO
    strDesCorta = rs!PR01DESCORTA
    cllBuscar.Add strDesCorta
    rs.MoveNext
Wend

End Sub


Private Sub txtCitasAdmitidas_KeyPress(KeyAscii As Integer)
If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
End If
End Sub

Private Sub txtDescripcion_Change()
cmdB(0).Enabled = False: cmdB(1).Enabled = False
End Sub
Private Sub pA�adirActuacion()
Dim sql As String
Dim qry As rdoQuery

sql = "INSERT INTO AG0100 (AG11CODRECURSO,AG07CODPERFIL,AG04CODFRANJA,AG01NUMASIGADM,AD02CODDPTO,"
sql = sql & "PR01CODACTUACION,AG01FECINVGACF) VALUES (?,?,?,?,?,?,SYSDATE)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intCodFranja
    qry(3) = txtCitasAdmitidas
    qry(4) = grdActuaciones.Columns("CodDpto").CellText(grdActuaciones.SelBookmarks(0))
    qry(5) = grdActuaciones.Columns("CodActuacion").CellText(grdActuaciones.SelBookmarks(0))
qry.Execute
qry.Close
If Err = 0 Then
    MsgBox "Actuacion A�adida", vbOKOnly, Me.Caption
Else
    MsgBox "La Actuacion NO se ha podido a�adir. Error:" & Error, vbOKOnly, Me.Caption
End If
End Sub
