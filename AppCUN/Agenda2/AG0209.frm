VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmRestricciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Restrinciones"
   ClientHeight    =   4680
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5625
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4680
   ScaleWidth      =   5625
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkExclusivo 
      Caption         =   "Exclusivo"
      Height          =   255
      Index           =   0
      Left            =   3360
      TabIndex        =   11
      Top             =   1080
      Width           =   1095
   End
   Begin VB.OptionButton optExclusion 
      Caption         =   "Exclusi�n"
      Height          =   255
      Left            =   1680
      TabIndex        =   10
      Top             =   1080
      Width           =   1095
   End
   Begin VB.OptionButton optInclusion 
      Caption         =   "Inclusi�n"
      Height          =   255
      Left            =   480
      TabIndex        =   9
      Top             =   1080
      Width           =   1095
   End
   Begin VB.TextBox txtFin 
      BackColor       =   &H80000003&
      Height          =   300
      Left            =   3480
      TabIndex        =   8
      Top             =   600
      Width           =   975
   End
   Begin VB.TextBox txtInicio 
      BackColor       =   &H80000004&
      Height          =   300
      Left            =   1320
      TabIndex        =   6
      Top             =   600
      Width           =   975
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4680
      TabIndex        =   3
      Top             =   960
      Width           =   855
   End
   Begin VB.CommandButton cmdEliminar 
      Caption         =   "&Eliminar"
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   480
      Width           =   855
   End
   Begin VB.CommandButton cmdNueva 
      Caption         =   "&A�adir"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   0
      Width           =   855
   End
   Begin SSDataWidgets_B.SSDBGrid grdRestricciones 
      Height          =   3180
      Left            =   0
      TabIndex        =   0
      Top             =   1440
      Width           =   5505
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   9710
      _ExtentY        =   5609
      _StockProps     =   79
      Caption         =   "Restricciones"
      ForeColor       =   8388608
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBTipRest 
      Height          =   300
      Left            =   1320
      TabIndex        =   12
      Tag             =   "Departamento"
      Top             =   120
      Width           =   1935
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AutoRestore     =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1667
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   5609
      Columns(1).Caption=   "Departamento"
      Columns(1).Name =   "Departamento"
      Columns(1).DataField=   "Column 2"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3413
      _ExtentY        =   529
      _StockProps     =   93
      BackColor       =   16776960
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label lblLabel 
      Caption         =   "Valor Fin:"
      Height          =   255
      Index           =   2
      Left            =   2760
      TabIndex        =   7
      Top             =   600
      Width           =   735
   End
   Begin VB.Label lblLabel 
      Caption         =   "Valor Incio:"
      Height          =   255
      Index           =   1
      Left            =   480
      TabIndex        =   5
      Top             =   600
      Width           =   855
   End
   Begin VB.Label lblLabel 
      Caption         =   "Tipo Restricci�n:"
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   4
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmRestricciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngCodRecurso As Long
Dim intCodPerfil As Integer
Dim intCodFranja As Integer


Private Sub cmdNueva_Click()
If Trim(SSDBTipRest.Text) = "" Or Trim(txtInicio) = "" Then
 MsgBox "Indique la Restriccion", vbExclamation, Me.Caption
 Exit Sub
End If
If optInclusion.Value = False And optExclusion.Value = False Then
    MsgBox "Indique si es Inclusion o Exclusion"
    Exit Sub
End If
pA�adirRestriccion

End Sub

Private Sub Form_Load()
If objPipe.PipeExist("AG0202_AG0209_AG11CODRECURSO") Then
    lngCodRecurso = objPipe.PipeGet("AG0202_AG0209_AG11CODRECURSO")
    objPipe.PipeRemove ("AG0202_AG0209_AG11CODRECURSO")
End If
If objPipe.PipeExist("AG0202_AG0209_AG07CODPERFIL") Then
    intCodPerfil = objPipe.PipeGet("AG0202_AG0209_AG07CODPERFIL")
    objPipe.PipeRemove ("AG0202_AG0209_AG07CODPERFIL")
End If
If objPipe.PipeExist("AG0202_AG0209_AG04CODFRANJA") Then
    intCodFranja = objPipe.PipeGet("AG0202_AG0209_AG04CODFRANJA")
    objPipe.PipeRemove ("AG0202_AG0209_AG04CODFRANJA")
End If
pFormatearGrids
pCargarRest
End Sub
Private Sub cmdEliminar_Click()
Dim intR As Integer
If grdRestricciones.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione una Restriccion", vbExclamation, Me.Caption
Else
    intR = MsgBox("Desea Eliminar la Restriccion seleccionada", vbYesNo, Me.Caption)
    If intR = vbYes Then pEliminar
End If
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub pFormatearGrids()
With grdRestricciones
    .Columns(0).Caption = "CodRes"
    .Columns(0).Visible = False
    .Columns(1).Caption = "Tipo Restrinccion"
    .Columns(1).Width = 1500
    .Columns(1).Style = ssStyleComboBox
    .Columns(2).Caption = "CodValIni"
    .Columns(2).Visible = False
    .Columns(3).Caption = "Varlor Inicial"
    .Columns(3).Width = 1500
    .Columns(4).Caption = "CodValFin"
    .Columns(4).Visible = False
    .Columns(5).Caption = "Varlor Final"
    .Columns(5).Width = 1500
    .Columns(6).Caption = "Incl./Exclu."
    .Columns(6).Width = 1000
    .Columns(7).Caption = "NumRest"
    .Columns(7).Visible = False
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
End With

End Sub
Private Sub pCargarRest()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql1 As String
Dim qry1 As rdoQuery
Dim rs1 As rdoResultset

Dim strInEx As String
sql = "SELECT AG16CODTIPREST, AG16DESTIPREST FROM AG1600"
Set rs = objApp.rdoConnect.OpenResultset(sql)
While Not rs.EOF
    SSDBTipRest.AddItem rs!AG16CODTIPREST & Chr$(9) & rs!AG16DESTIPREST
    rs.MoveNext
Wend
rs.Close

sql = "SELECT AG12NUMRESFRJA,"
sql = sql & "AG1200.AG16CODTIPREST, AG16DESTIPREST,AG12VALDESDERES, AG12VALHASTARES,"
sql = sql & "AG12INDINCEXCL,AG16TABLAVALOR,"
sql = sql & "AG16COLCODVALO,AG16COLDESVALO "
sql = sql & "FROM AG1200, AG1600 WHERE "
sql = sql & " AG11CODRECURSO= ? AND AG07CODPERFIL = ? AND AG04CODFRANJA = ?"
sql = sql & " AND AG1200.AG16CODTIPREST = AG1600.AG16CODTIPREST"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intCodFranja
Set rs = qry.OpenResultset()
While Not rs.EOF
    If rs!AG12INDINCEXCL = 1 Then strInEx = "Exclusi�n" Else strInEx = "Inclusi�n"
    If Not IsNull(rs!AG16TABLAVALOR) Then
        sql1 = "SELECT " & rs!AG16COLCODVALO & " COD," & rs!AG16COLDESVALO & " VALINI"
        sql1 = sql1 & " FROM " & rs!AG16TABLAVALOR
        sql1 = sql1 & " WHERE " & rs!AG16COLCODVALO & " = ?"
        Set qry1 = objApp.rdoConnect.CreateQuery("", sql1)
            qry1(0) = rs!AG12VALDESDERES
        Set rs1 = qry1.OpenResultset()
        grdRestricciones.AddItem rs!AG16CODTIPREST & Chr$(9) & _
                         rs!AG16DESTIPREST & Chr$(9) & _
                         rs!AG12VALDESDERES & Chr$(9) & _
                         rs1(1) & Chr$(9) & _
                         rs!AG12VALHASTARES & Chr$(9) & _
                         rs!AG12VALHASTARES & Chr$(9) & _
                         strInEx & Chr$(9) & rs!AG12NUMRESFRJA
    Else
        grdRestricciones.AddItem rs!AG16CODTIPREST & Chr$(9) & _
                     rs!AG16DESTIPREST & Chr$(9) & _
                     rs!AG12VALDESDERES & Chr$(9) & _
                     rs!AG12VALDESDERES & Chr$(9) & _
                     rs!AG12VALHASTARES & Chr$(9) & _
                     rs!AG12VALHASTARES & Chr$(9) & _
                     strInEx & Chr$(9) & rs!AG12NUMRESFRJA
    End If

    rs.MoveNext
Wend

End Sub
Private Sub pEliminar()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "DELETE FROM AG1200 WHERE "
sql = sql & " AG11CODRECURSO= ? AND AG07CODPERFIL = ? AND AG04CODFRANJA = ?"
sql = sql & " AND AG12NUMRESFRJA = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intCodFranja
    qry(3) = grdRestricciones.Columns("NumRest").CellValue(grdRestricciones.SelBookmarks(0))
qry.Execute

If Err > 0 Or qry.RowsAffected = 0 Then
    MsgBox "La restriccion NO se puede eliminar", vbExclamation, Me.Caption
Else
    grdRestricciones.DeleteSelected
End If
qry.Close
End Sub

Private Sub grdRestricciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub



Private Sub SSDBTipRest_CloseUp()
pCargarValores
End Sub

Private Sub pCargarValores()
Call objPipe.PipeSet("AG0209_AG0220_AG16CODTIPREST", SSDBTipRest.Columns(0).Value)
frmBuscaValores.Show vbModal
Set frmBuscaValores = Nothing
If objPipe.PipeExist("AG0220_AG0209_AG12VALDESDERES") Then
    txtInicio.Text = objPipe.PipeGet("AG0220_AG0209_AG12VALDESDERES")
    Call objPipe.PipeRemove("AG0220_AG0209_AG12VALDESDERES")
Else
    txtInicio.Text = ""
End If
If objPipe.PipeExist("AG0220_AG0209_AG12VALHASTARES") Then
    txtFin.Text = objPipe.PipeGet("AG0220_AG0209_AG12VALHASTARES")
    Call objPipe.PipeRemove("AG0220_AG0209_AG12VALHASTARES")
Else
    txtFin.Text = ""
End If

End Sub
Private Sub pA�adirRestriccion()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim sql1 As String
Dim rs1 As rdoResultset
Dim qry1 As rdoQuery
Dim intNumRes As Integer
'sacamos el numero de restriccion
sql = "SELECT MAX(AG12NUMRESFRJA) FROM AG1200 WHERE "
sql = sql & "AG11CODRECURSO = ? AND AG07CODPERFIL = ?"
sql = sql & " AND AG04CODFRANJA = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intCodFranja
Set rs = qry.OpenResultset()
If IsNull(rs(0)) Then intNumRes = 1 Else intNumRes = Val(rs(0)) + 1
rs.Close
objApp.BeginTrans
sql = "INSERT INTO AG1200 (AG11CODRECURSO,AG07CODPERFIL,AG04CODFRANJA,AG12NUMRESFRJA,"
sql = sql & "AG16CODTIPREST,AG12VALDESDERES,AG12VALHASTARES,AG12NIVELJERAR,AG12INDINCEXCL)"
sql = sql & "Values (?,?,?,?,?,?,?,0,?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intCodFranja
    qry(3) = intNumRes
    qry(4) = SSDBTipRest.Columns(0).Value
    qry(5) = txtInicio
    If Trim(txtFin) = "" Then qry(6) = Null Else qry(6) = txtFin
    If optInclusion = True Then qry(7) = 0 Else qry(7) = "1"
qry.Execute
qry.Close
If chkExclusivo(0).Value = 1 Then
    sql = "SELECT AG04CODFRANJA FROM AG0400 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ?"
    sql = sql & " AND AG04CODFRANJA <> ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = intCodPerfil
        qry(2) = intCodFranja
    Set rs = qry.OpenResultset()
    While Not rs.EOF
        intNumRes = intNumRes + 1
        sql1 = "INSERT INTO AG1200 (AG11CODRECURSO,AG07CODPERFIL,AG04CODFRANJA,AG12NUMRESFRJA,"
        sql1 = sql1 & "AG16CODTIPREST,AG12VALDESDERES,AG12VALHASTARES,AG12NIVELJERAR,AG12INDINCEXCL)"
        sql1 = sql1 & "Values (?,?,?,?,?,?,?,0,?)"
        Set qry1 = objApp.rdoConnect.CreateQuery("", sql1)
            qry1(0) = lngCodRecurso
            qry1(1) = intCodPerfil
            qry1(2) = rs!AG04CODFRANJA
            qry1(3) = intNumRes
            qry1(4) = SSDBTipRest.Columns(0).Value
            qry1(5) = txtInicio
            If Trim(txtFin) = "" Then qry1(6) = Null Else qry1(6) = txtFin
            If optInclusion = True Then qry1(7) = "1" Else qry1(7) = "0"
        qry1.Execute
        qry1.Close
    rs.MoveNext
    Wend
End If
If Err = 0 Then
    objApp.CommitTrans
    MsgBox "Restriccion A�adida", vbOKOnly, Me.Caption
    grdRestricciones.RemoveAll
    pCargarRest
Else
    objApp.RollBackTrans
    MsgBox "La Restriccion NO se ha podido a�adir", vbOKOnly, Me.Caption
End If
End Sub
