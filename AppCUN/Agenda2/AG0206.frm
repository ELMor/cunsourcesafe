VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmNuevoPeriodo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nuevo Periodo"
   ClientHeight    =   1365
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4530
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1365
   ScaleWidth      =   4530
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtMotivo 
      BackColor       =   &H00FFFF00&
      Height          =   285
      Left            =   720
      MaxLength       =   30
      TabIndex        =   7
      Top             =   480
      Width           =   3735
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   3480
      TabIndex        =   5
      Top             =   960
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   960
      Width           =   975
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcFechaIni 
      Height          =   285
      Left            =   720
      TabIndex        =   1
      Tag             =   "Fecha superior de las citas"
      Top             =   120
      Width           =   1605
      _Version        =   65537
      _ExtentX        =   2831
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcFechaFin 
      Height          =   285
      Left            =   2880
      TabIndex        =   3
      Tag             =   "Fecha superior de las citas"
      Top             =   120
      Width           =   1605
      _Version        =   65537
      _ExtentX        =   2831
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   " Motivo"
      Height          =   195
      Index           =   2
      Left            =   0
      TabIndex        =   6
      Top             =   480
      Width           =   525
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   " Fin"
      Height          =   195
      Index           =   1
      Left            =   2520
      TabIndex        =   2
      Top             =   120
      Width           =   255
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   " Inicio"
      Height          =   195
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   420
   End
End
Attribute VB_Name = "frmNuevoPeriodo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngCodRecurso As Long
Dim intCodPerfil As Integer
Dim intPeriodo As Integer
Dim strFecIni As String
Dim strFecFin As String



Private Sub dtcFechaFin_CloseUp()
If CDate(dtcFechaFin.Date) < CDate(dtcFechaIni.Date) Then
    MsgBox "Fecha fin menor que fecha inicio", vbCritical, Me.Caption
    dtcFechaFin.Text = dtcFechaIni.Text
    dtcFechaFin.Date = dtcFechaIni.Date
End If
End Sub

Private Sub dtcFechaFin_LostFocus()
If CDate(dtcFechaFin.Date) < CDate(dtcFechaIni.Date) Then
    MsgBox "Fecha fin menor que fecha inicio", vbCritical, Me.Caption
    dtcFechaFin.Text = dtcFechaIni.Text
    dtcFechaFin.Date = dtcFechaIni.Date
End If
End Sub

Private Sub dtcFechaIni_CloseUp()
dtcFechaFin.Text = dtcFechaIni.Text
dtcFechaFin.Date = dtcFechaIni.Date
End Sub

Private Sub dtcFechaIni_LostFocus()
dtcFechaFin.Text = dtcFechaIni.Text
dtcFechaFin.Date = dtcFechaIni.Date
End Sub

Private Sub Form_Load()
If objPipe.PipeExist("AG0202_AG0206_AG11CODRECURSO") Then
    lngCodRecurso = objPipe.PipeGet("AG0202_AG0206_AG11CODRECURSO")
    Call objPipe.PipeRemove("AG0202_AG0206_AG11CODRECURSO")
Else
    lngCodRecurso = 0
End If
If objPipe.PipeExist("AG0202_AG0206_AG07CODPERFIL") Then
    intCodPerfil = objPipe.PipeGet("AG0202_AG0206_AG07CODPERFIL")
    Call objPipe.PipeRemove("AG0202_AG0206_AG07CODPERFIL")
Else
    intCodPerfil = 0
End If
If objPipe.PipeExist("AG0202_AG0206_AG09NUMPVIGPER") Then
    intPeriodo = objPipe.PipeGet("AG0202_AG0206_AG09NUMPVIGPER")
    Call objPipe.PipeRemove("AG0202_AG0206_AG09NUMPVIGPER")
    Call pCargarPeriodo
Else
    intPeriodo = 0
End If
End Sub
Private Sub cmdAceptar_Click()
If dtcFechaIni.Date = "" Or dtcFechaFin = "" Or Trim(txtMotivo) = "" Then
    MsgBox "Rellene todos los campos", vbCritical, Me.Caption
Else
    If intPeriodo = 0 Then pCrearPeriodo Else pModPeriodo
    Unload Me
End If
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub pCrearPeriodo()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim rs1 As rdoResultset
Dim qry1 As rdoQuery
Dim intCodPeriodo As Integer
On Error GoTo canceltrans

'CREAMOS EL NUEVO PERIODO
sql = "SELECT MAX(AG09NUMPVIGPER) FROM AG0900 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
Set rs = qry.OpenResultset()
If Not IsNull(rs(0)) Then intCodPeriodo = Val(rs(0)) + 1 Else intCodPeriodo = 1
rs.Close
qry.Close
objApp.BeginTrans
If fblnComprobaciones(lngCodRecurso, intCodPerfil, intCodPeriodo, _
Format$(dtcFechaIni.Date, "dd/mm/yyyy"), Format$(dtcFechaFin.Date, "dd/mm/yyyy"), txtMotivo.Text, 0) Then

    sql = "INSERT INTO AG0900 (AG11CODRECURSO, AG07CODPERFIL,"
    sql = sql & "AG09NUMPVIGPER, AG09FECINVIPER, AG09FECFIVIPER, AG09MOTIVOPERF)"
    sql = sql & " VALUES (?,?,?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'),?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = intCodPerfil
        qry(2) = intCodPeriodo
        qry(3) = Format$(dtcFechaIni.Date, "dd/mm/yyyy")
        qry(4) = Format$(dtcFechaFin.Date, "dd/mm/yyyy")
        qry(5) = txtMotivo.Text
    qry.Execute
    qry.Close
    objApp.CommitTrans
    MsgBox "Periodo Creado", vbOKOnly
    Call objPipe.PipeSet("AG0206_AG0202_AG09NUMPVIGPER", intCodPeriodo)
    
End If
Exit Sub
canceltrans:
    MsgBox "No se ha podido crear el periodo", vbCritical
    objApp.RollBackTrans

End Sub
Private Sub pModPeriodo()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
On Error Resume Next
objApp.BeginTrans
'SI DISMINUIMOS TANTO LA FECHA INICIO COMO LA DE FIN. SACAMOS A PENDIENTE Y PUNTO.
If CDate(Format(strFecIni, "DD/MM/YYYY")) < CDate(Format$(dtcFechaIni.Date, "dd/mm/yyyy")) Or _
CDate(Format(strFecFin, "DD/MM/YYYY")) > CDate(Format$(dtcFechaFin.Date, "dd/mm/yyyy")) Then
    If Not blnSacarPendientes(Format(strFecIni, "DD/MM/YYYY"), Format$(dtcFechaIni.Text, "dd/mm/yyyy"), lngCodRecurso) _
    Then Exit Sub
    If Not blnSacarPendientes(Format$(dtcFechaFin.Text, "dd/mm/yyyy"), Format(strFecFin, "DD/MM/YYYY"), lngCodRecurso) _
    Then Exit Sub
'si aumentamos
ElseIf CDate(Format(strFecIni, "DD/MM/YYYY")) > CDate(Format$(dtcFechaIni.Date, "dd/mm/yyyy")) Or _
CDate(Format(strFecFin, "DD/MM/YYYY")) < CDate(Format$(dtcFechaFin.Date, "dd/mm/yyyy")) Then
    If Not fblnComprobaciones(lngCodRecurso, intCodPerfil, intPeriodo, Format$(dtcFechaIni.Text, "dd/mm/yyyy"), _
    Format$(dtcFechaFin.Text, "dd/mm/yyyy"), txtMotivo.Text, 1) Then Exit Sub
End If


sql = "UPDATE AG0900 SET AG09FECINVIPER = ?, AG09FECFIVIPER = ?, AG09MOTIVOPERF = ?"
sql = sql & " WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? AND AG09NUMPVIGPER=?"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format$(dtcFechaIni.Date, "dd/mm/yyyy")
    qry(1) = Format$(dtcFechaFin.Date, "dd/mm/yyyy")
    qry(2) = txtMotivo.Text
    qry(3) = lngCodRecurso
    qry(4) = intCodPerfil
    qry(5) = intPeriodo
qry.Execute
qry.Close
If Err > 0 Then
    MsgBox "El Periodo NO se ha podido modificar", vbExclamation, Me.Caption
    objApp.RollBackTrans
Else
    objApp.CommitTrans
    MsgBox "Periodo Mofificado", vbOKOnly
End If
End Sub
Private Sub pCargarPeriodo()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT AG09FECINVIPER, AG09FECFIVIPER, AG09MOTIVOPERF FROM AG0900"
sql = sql & " WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? AND AG09NUMPVIGPER=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intPeriodo
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    dtcFechaIni.Date = Format$(rs!AG09FECINVIPER, "dd/mm/yyyy")
    dtcFechaFin.Date = Format$(rs!AG09FECFIVIPER, "dd/mm/yyyy")
    strFecIni = Format$(rs!AG09FECINVIPER, "dd/mm/yyyy")
    strFecFin = Format$(rs!AG09FECFIVIPER, "dd/mm/yyyy")
    txtMotivo.Text = rs!AG09MOTIVOPERF
End If
rs.Close
qry.Close
End Sub

