VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmMantPerfiles 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Perfiles"
   ClientHeight    =   6390
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11760
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6390
   ScaleWidth      =   11760
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   10800
      TabIndex        =   20
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox txtRecurso 
      BackColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   840
      TabIndex        =   19
      Top             =   120
      Width           =   4575
   End
   Begin VB.CommandButton cmdRestrincciones 
      Caption         =   "&Restric."
      Height          =   375
      Left            =   3600
      TabIndex        =   17
      Top             =   6000
      Width           =   855
   End
   Begin VB.CommandButton cmdModificarFranja 
      Caption         =   "Modificar"
      Height          =   375
      Left            =   960
      TabIndex        =   16
      Top             =   6000
      Width           =   855
   End
   Begin VB.CommandButton cmdModPefil 
      Caption         =   "Modificar"
      Height          =   375
      Left            =   4440
      TabIndex        =   15
      Top             =   1080
      Width           =   855
   End
   Begin VB.CommandButton cmdNuevaAct 
      Caption         =   "Nuevo"
      Height          =   375
      Left            =   6480
      TabIndex        =   11
      Top             =   6000
      Width           =   855
   End
   Begin VB.CommandButton cmdEliminarAct 
      Caption         =   "Eliminar"
      Height          =   375
      Left            =   10800
      TabIndex        =   10
      Top             =   6000
      Width           =   855
   End
   Begin VB.CommandButton cmdNuevaFranja 
      Caption         =   "Nuevo"
      Height          =   375
      Left            =   0
      TabIndex        =   9
      Top             =   6000
      Width           =   855
   End
   Begin VB.CommandButton cmdEliminarFranja 
      Caption         =   "Eliminar"
      Height          =   375
      Left            =   5520
      TabIndex        =   8
      Top             =   6000
      Width           =   855
   End
   Begin VB.CommandButton cmdDuplicarFranja 
      Caption         =   "Duplicar"
      Height          =   375
      Left            =   1920
      TabIndex        =   7
      Top             =   6000
      Width           =   855
   End
   Begin VB.CommandButton cmdNuevoPerfil 
      Caption         =   "Nuevo"
      Height          =   375
      Left            =   4440
      TabIndex        =   5
      Top             =   600
      Width           =   855
   End
   Begin VB.CommandButton cmdEliminarP 
      Caption         =   "Eliminar"
      Height          =   375
      Index           =   0
      Left            =   4440
      TabIndex        =   4
      Top             =   2160
      Width           =   855
   End
   Begin VB.CommandButton cmdNuevoPeriodo 
      Caption         =   "Nuevo"
      Height          =   375
      Left            =   10800
      TabIndex        =   3
      Top             =   600
      Width           =   855
   End
   Begin VB.CommandButton cmdEliminarPeriodo 
      Caption         =   "Eliminar"
      Height          =   375
      Left            =   10800
      TabIndex        =   2
      Top             =   2160
      Width           =   855
   End
   Begin VB.CommandButton cmdModPeriodo 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   10800
      TabIndex        =   1
      Top             =   1080
      Width           =   855
   End
   Begin VB.CommandButton cmdDuplicar 
      Caption         =   "Duplicar"
      Height          =   375
      Left            =   4440
      TabIndex        =   0
      Top             =   1560
      Width           =   855
   End
   Begin SSDataWidgets_B.SSDBGrid ssgrdPeriodos 
      Height          =   2010
      Left            =   5400
      TabIndex        =   6
      Top             =   600
      Width           =   5295
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   9340
      _ExtentY        =   3545
      _StockProps     =   79
      Caption         =   "Periodos de vigencia"
   End
   Begin SSDataWidgets_B.SSDBGrid ssgrdFranjas 
      Height          =   3255
      Left            =   0
      TabIndex        =   12
      Top             =   2640
      Width           =   6375
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      ForeColorEven   =   0
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   11245
      _ExtentY        =   5741
      _StockProps     =   79
      Caption         =   "Franjas"
   End
   Begin SSDataWidgets_B.SSDBGrid ssgrdActuacion 
      Height          =   3255
      Left            =   6480
      TabIndex        =   13
      Top             =   2640
      Width           =   5175
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   0
      AllowUpdate     =   0   'False
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      ForeColorEven   =   0
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   9128
      _ExtentY        =   5741
      _StockProps     =   79
      Caption         =   "Tipos de Actuaci�n"
   End
   Begin SSDataWidgets_B.SSDBGrid ssgrdPerfiles 
      Height          =   2010
      Left            =   0
      TabIndex        =   14
      Top             =   600
      Width           =   4365
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   7699
      _ExtentY        =   3545
      _StockProps     =   79
      Caption         =   "Perfiles"
   End
   Begin VB.Label Label1 
      Caption         =   "Recurso:"
      Height          =   255
      Left            =   0
      TabIndex        =   18
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "frmMantPerfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngCodRecurso As Long
Dim lngCodperfil As Long
Dim strAsig As String

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
pFormatearGrids
lngCodperfil = 0
lngCodRecurso = 0
If objPipe.PipeExist("AG0201_AG0202_AG11CODRECURSO") Then
    lngCodRecurso = objPipe.PipeGet("AG0201_AG0202_AG11CODRECURSO")
    objPipe.PipeRemove ("AG0201_AG0202_AG11CODRECURSO")
End If
If objPipe.PipeExist("AG0201_AG0202_AG07CODPERFIL") Then
    lngCodperfil = objPipe.PipeGet("AG0201_AG0202_AG07CODPERFIL")
    objPipe.PipeRemove ("AG0201_AG0202_AG07CODPERFIL")
    cmdNuevoPerfil.Enabled = False
End If
If lngCodRecurso <> 0 Then
    pCargarRecurso
    pCargarPerfiles
End If
ssgrdPerfiles.SelBookmarks.RemoveAll

End Sub

'*********** BOTONES DEL PERFIL **************
Private Sub cmdNuevoPerfil_Click()
pNuevoPerfil
End Sub
Private Sub cmdModPefil_Click()
If ssgrdPerfiles.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione un Perfil", vbCritical
    Exit Sub
Else
    pModPerfil
End If
End Sub
Private Sub cmdDuplicar_Click()
If ssgrdPerfiles.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione el perfil a duplicar", vbCritical
    Exit Sub
Else
    pDuplicarPerfil
End If
End Sub


Private Sub cmdEliminarP_Click(Index As Integer)
Dim intR As Integer
If ssgrdPerfiles.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione el perfil para eliminar", vbCritical
    Exit Sub
End If
intR = MsgBox("Al borrar el perfil se borrar todos sus periodos de vigencia y franjas" & _
Chr$(13) & "Desea continuar ?", vbYesNo)
If intR = vbYes Then pEliminarPerfil
End Sub
'************* BOTONES DEL PERIODO **************
Private Sub cmdNuevoPeriodo_Click()
If ssgrdPerfiles.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione un perfil", vbExclamation, Me.Caption
    Exit Sub
Else
    pNuevoPeriodo
End If
End Sub
Private Sub cmdModPeriodo_Click()
If ssgrdPeriodos.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione un periodo", vbExclamation, Me.Caption
    Exit Sub
Else
    pModificarPeriodo
End If
End Sub
Private Sub cmdEliminarPeriodo_Click()
Dim intR As Integer
If ssgrdPeriodos.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione un periodo", vbCritical
    Exit Sub
End If
intR = MsgBox("Desea eliminar el periodo de vigencia seleccionado?", vbYesNo)
If intR = vbNo Then Exit Sub Else pEliminarPeriodo
End Sub

'************ BOTONES DE LAS FRANJAS ******************
Private Sub cmdNuevaFranja_Click()
If ssgrdPerfiles.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione un perfil", vbCritical
    Exit Sub
Else
    Call pNuevaFranja(0)
End If
End Sub

Private Sub cmdModificarFranja_Click()
If ssgrdFranjas.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione una Franja", vbCritical
Else
    pModificarFranja
End If
End Sub

Private Sub cmdDuplicarFranja_Click()
If ssgrdPerfiles.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione un perfil", vbCritical, Me.Caption
    Exit Sub
ElseIf ssgrdFranjas.SelBookmarks.Count = 0 Then
        MsgBox "Seleccione una Franja", vbCritical, Me.Caption
        Exit Sub
Else
    Call pNuevaFranja(-1)
End If
End Sub
Private Sub cmdRestrincciones_Click()
If ssgrdPerfiles.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione un perfil", vbCritical, Me.Caption
    Exit Sub
ElseIf ssgrdFranjas.SelBookmarks.Count = 0 Then
        MsgBox "Seleccione una Franja", vbCritical, Me.Caption
        Exit Sub
Else
    Call pRestricciones
End If
End Sub

Private Sub cmdEliminarFranja_Click()
Dim intR As Integer
If ssgrdFranjas.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione una Franja", vbCritical
    Exit Sub
End If
intR = MsgBox("Desea eliminar la franja Seleccionada?", vbYesNo)
If intR = vbNo Then Exit Sub Else pEliminarFranja
End Sub

'************* BOTONES DE LAS ACTUACIONES ********
Private Sub cmdNuevaAct_Click()
If ssgrdFranjas.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione una Franja", vbCritical
Else
    pNuevaActuacion
End If
End Sub
Private Sub cmdEliminarAct_Click()
Dim intR%
If ssgrdActuacion.SelBookmarks.Count = 0 Then
    MsgBox "Seleccione una Actuacion", vbCritical
Else
    intR = MsgBox("Esta Seguro que desea eliminar la actuacion" & Chr$(13) & _
    "--> " & ssgrdActuacion.Columns("Actuacion").CellText(ssgrdActuacion.SelBookmarks(0)), vbYesNo, Me.Caption)
    If intR = vbYes Then pEliminarActuacion
End If
End Sub
Private Sub pFormatearGrids()
'perfiles
With ssgrdPerfiles
    .Columns(0).Caption = "Nombre"
    .Columns(0).Width = 2715
    .Columns(1).Caption = "Prioridad"
    .Columns(1).Width = 300
    .Columns(2).Caption = "Fecha Baja"
    .Columns(2).Width = 1300
    .Columns(3).Caption = "CodPerfil"
    .Columns(3).Visible = False
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
End With
With ssgrdPeriodos
    .Columns(0).Caption = "Fecha Inicio"
    .Columns(0).Width = 1300
    .Columns(1).Caption = "Fecha Fin"
    .Columns(1).Width = 1300
    .Columns(2).Caption = "Motivo"
    .Columns(2).Width = 2640
    .Columns(3).Caption = "CodPeriodo"
    .Columns(3).Visible = False
    .Columns(4).Caption = "CodPerfil"
    .Columns(4).Visible = False
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
End With
With ssgrdFranjas
    .Columns(0).Caption = "Inicio"
    .Columns(0).Width = 645
    .Columns(0).Locked = True
    .Columns(1).Caption = "Fin"
    .Columns(1).Width = 645
    .Columns(1).Locked = True
    .Columns(2).Caption = "L"
    .Columns(2).Width = 380
    .Columns(2).Style = ssStyleCheckBox
    .Columns(3).Caption = "M"
    .Columns(3).Width = 380
    .Columns(3).Style = ssStyleCheckBox
    .Columns(4).Caption = "X"
    .Columns(4).Width = 380
    .Columns(4).Style = ssStyleCheckBox
    .Columns(5).Caption = "J"
    .Columns(5).Width = 380
    .Columns(5).Style = ssStyleCheckBox
    .Columns(6).Caption = "V"
    .Columns(6).Width = 380
    .Columns(6).Style = ssStyleCheckBox
    .Columns(7).Caption = "S"
    .Columns(7).Width = 380
    .Columns(7).Style = ssStyleCheckBox
    .Columns(8).Caption = "D"
    .Columns(8).Width = 380
    .Columns(8).Style = ssStyleCheckBox
    .Columns(9).Caption = "CodActividad"
    .Columns(9).Visible = False
    .Columns(10).Caption = "Actividad"
    .Columns(10).Width = 780
    .Columns(11).Caption = "Modo de Asignacion"
    .Columns(11).Width = 794
    .Columns(12).Caption = "Citas"
    .Columns(12).Width = 290
    .Columns(13).Caption = "Intervalo"
    .Columns(13).Width = 290
    .Columns(14).Caption = "CodFranja"
    .Columns(14).Visible = False
    .Columns(15).Caption = "CodPerfil"
    .Columns(15).Visible = False
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
End With
With ssgrdActuacion
    .AllowUpdate = True
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
    .Columns(0).Caption = "CodActuacion"
    .Columns(0).Visible = False
    .Columns(0).Locked = True
    .Columns(1).Caption = "Actuacion"
    .Columns(1).Width = 2500
    .Columns(1).Locked = True
    .Columns(2).Caption = "CodDpo"
    .Columns(2).Visible = False
    .Columns(1).Locked = True
    .Columns(3).Caption = "Departamento"
    .Columns(3).Width = 1300
    .Columns(3).Locked = True
    .Columns(4).Caption = "Asign"
    .Columns(4).Width = 380
    .Columns(4).BackColor = objApp.objUserColor.lngNormal
    .Columns(5).Caption = "H. Antelacion"
    .Columns(5).Width = 1000
    .Columns(5).Locked = True
    .Columns(6).Caption = "CodFraja"
    .Columns(6).Visible = False
    .Columns(6).Locked = True
    .Columns(7).Caption = "CodPerfil"
    .Columns(7).Visible = False
    .Columns(7).Locked = True

End With
End Sub
Private Sub pCargarPerfiles()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strFechaBaja As String

sql = "SELECT AG07CODPERFIL, AG07DESPERFIL, AG07PRIORIDAD, AG07FECBAJA"
sql = sql & " FROM AG0700 WHERE AG11CODRECURSO = ? "
If lngCodperfil <> 0 Then
    sql = sql & " AND AG07CODPERFIL = ?"
End If
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    If lngCodperfil <> 0 Then qry(1) = lngCodperfil
Set rs = qry.OpenResultset()
While Not rs.EOF
    If IsNull(rs!AG07FECBAJA) Then strFechaBaja = "" Else _
    strFechaBaja = Format$(rs!AG07FECBAJA, "dd/mm/yyyy")
    
    ssgrdPerfiles.AddItem rs!AG07DESPERFIL & Chr$(9) & _
                          rs!AG07PRIORIDAD & Chr$(9) & _
                          strFechaBaja & Chr$(9) & rs!AG07CODPERFIL
    rs.MoveNext
Wend
rs.Close
End Sub



Private Sub ssgrdActuacion_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

On Error GoTo CancelTrans

objApp.BeginTrans


If Not IsNumeric(ssgrdActuacion.Columns("Asign").Value) Then
    MsgBox "Introduzca un n�mero de citas admitidas", vbInformation, Me.Caption
    ssgrdActuacion.Columns("Asign").Value = strAsig
    Exit Sub
End If
If Val(ssgrdActuacion.Columns("Asign").Value) > Val(ssgrdFranjas.Columns("Citas").CellValue(ssgrdFranjas.SelBookmarks(0))) Then
    MsgBox "La franja admite como m�ximo " & ssgrdFranjas.Columns("Citas").CellValue(ssgrdFranjas.SelBookmarks(0)) _
    & " Citas", vbInformation, Me.Caption
    ssgrdActuacion.Columns("Asign").Value = strAsig
    Exit Sub
End If
If Val(strAsig) > Val(ssgrdActuacion.Columns("Asign").Value) Then
    If Not fblnPacAfectAct(ssgrdFranjas.Columns("Inicio").CellValue(ssgrdFranjas.SelBookmarks(0)), ssgrdFranjas.Columns("Fin").CellValue(ssgrdFranjas.SelBookmarks(0))) Then
        ssgrdActuacion.Columns("Asign").Value = strAsig
        Exit Sub
    End If
End If

sql = "UPDATE AG0100 SET AG01NUMASIGADM = ? WHERE AG11CODRECURSO = ? AND"
sql = sql & " AG07CODPERFIL = ? AND AG04CODFRANJA = ? AND PR01CODACTUACION = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = ssgrdActuacion.Columns("Asign").Value
    qry(1) = lngCodRecurso
    qry(2) = ssgrdPerfiles.Columns("CodPerfil").CellValue(ssgrdPerfiles.SelBookmarks(0))
    qry(3) = ssgrdFranjas.Columns("CodFranja").CellValue(ssgrdFranjas.SelBookmarks(0))
    qry(4) = ssgrdActuacion.Columns("CodActuacion").Value
qry.Execute

If Err > 0 Or qry.RowsAffected = 0 Then
    MsgBox "No se ha podido actualizar. Error: " & Error, vbExclamation, Me.Caption
Else
    ssgrdActuacion.Update
End If
qry.Close
objApp.CommitTrans
Exit Sub

CancelTrans:
objApp.RollBackTrans
MsgBox "No se ha podido actualizar. Error: " & Error, vbExclamation, Me.Caption
End Sub

Private Sub ssgrdActuacion_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub


Private Sub ssgrdActuacion_Click()
strAsig = ssgrdActuacion.Columns("Asign").Value
End Sub

Private Sub ssgrdFranjas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub

Private Sub ssgrdFranjas_Click()
ssgrdActuacion.RemoveAll
pCargarActuaciones
End Sub

Private Sub ssgrdPerfiles_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub

Private Sub ssgrdPerfiles_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    ssgrdFranjas.RemoveAll
    ssgrdPeriodos.RemoveAll
    ssgrdActuacion.RemoveAll
    pCargarPeriodos
    pCargarFranjas
End Sub

Private Sub ssgrdPeriodos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub

Private Sub ssgrdPeriodos_Change()
cmdModPeriodo.Enabled = True
End Sub

Private Sub ssgrdFranjas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
'ssgrdActuacion.RemoveAll
'pCargarActuaciones
End Sub
Private Sub pCargarPeriodos()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strMotivo As String

sql = "SELECT  AG09NUMPVIGPER, AG09FECINVIPER, AG09FECFIVIPER,"
sql = sql & "AG09MOTIVOPERF FROM AG0900 WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ?"
sql = sql & " ORDER BY AG09FECINVIPER DESC"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
Set rs = qry.OpenResultset()
While Not rs.EOF
    If IsNull(rs!AG09MOTIVOPERF) Then strMotivo = "" Else strMotivo = rs!AG09MOTIVOPERF
    ssgrdPeriodos.AddItem rs!AG09FECINVIPER & Chr$(9) & _
                        rs!AG09FECFIVIPER & Chr$(9) & _
                        strMotivo & Chr$(9) & _
                        rs!AG09NUMPVIGPER & Chr$(9) & _
                        ssgrdPerfiles.Columns("CodPerfil").Value
                        
    rs.MoveNext
Wend
End Sub
Private Sub pCargarFranjas()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strActividad$
Dim strCodActividad$
Dim strModAsig$
sql = "SELECT AG04CODFRANJA,AG0400.PR12CODACTIVIDAD,PR12DESACTIVIDAD, AG04INDLUNFRJA,AG04INDMARFRJA,AG04INDMIEFRJA,"
sql = sql & "AG04INDJUEFRJA,AG04INDVIEFRJA,AG04INDSABFRJA,AG04INDDOMFRJA,AG04HORINFRJHH,"
sql = sql & "AG04HORINFRJMM,AG04HORFIFRJHH,AG04HORFIFRJMM,AG04NUMCITADMI,"
sql = sql & "AG04HORASOVEHH,AG04HORASOVEMM,AG04MODASIGCITA,AG04INTERVCITA"
sql = sql & " FROM AG0400,PR1200 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
sql = sql & " AND AG04FECBAJA IS NULL"
sql = sql & " AND AG0400.PR12CODACTIVIDAD = PR1200.PR12CODACTIVIDAD(+)"
sql = sql & " ORDER BY AG04HORINFRJHH"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
Set rs = qry.OpenResultset()
While Not rs.EOF
    If IsNull(rs!PR12CODACTIVIDAD) Then strCodActividad = " " Else strCodActividad = rs!PR12CODACTIVIDAD
    If IsNull(rs!PR12DESACTIVIDAD) Then strActividad = " " Else strActividad = rs!PR12DESACTIVIDAD
    Select Case rs!AG04MODASIGCITA
        Case 1: strModAsig = "Cantidad"
        Case 2: strModAsig = "Secuencial"
        Case 3: strModAsig = "Inter. Pred."
        Case 4: strModAsig = "Inter. Osci."
        Case Else: strModAsig = " "
    End Select
    ssgrdFranjas.AddItem Format(rs!AG04HORINFRJHH, "##00") & ":" & Format(rs!AG04HORINFRJMM, "##00") & Chr$(9) & _
                         Format(rs!AG04HORFIFRJHH, "##00") & ":" & Format(rs!AG04HORFIFRJMM, "##00") & Chr$(9) & _
                         rs!AG04INDLUNFRJA & Chr$(9) & _
                         rs!AG04INDMARFRJA& & Chr$(9) & _
                         rs!AG04INDMIEFRJA & Chr$(9) & _
                         rs!AG04INDJUEFRJA & Chr$(9) & _
                         rs!AG04INDVIEFRJA & Chr$(9) & _
                         rs!AG04INDSABFRJA & Chr$(9) & rs!AG04INDDOMFRJA & Chr$(9) & _
                         strCodActividad & Chr$(9) & _
                         strActividad & Chr$(9) & _
                         strModAsig & Chr$(9) & _
                         rs!AG04NUMCITADMI & Chr$(9) & _
                         rs!AG04INTERVCITA & Chr$(9) & _
                         rs!AG04CODFRANJA & Chr$(9) & _
                         ssgrdPerfiles.Columns("CodPerfil").Value
    rs.MoveNext
Wend
rs.Close
qry.Close
End Sub
Private Sub pCargarActuaciones()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT AG04CODFRANJA,AG01NUMASIGADM,AG0100.AD02CODDPTO,AD02DESDPTO,"
sql = sql & " AG0100.PR01CODACTUACION, PR01DESCORTA,AG01NUMHORANT"
sql = sql & " FROM AG0100, PR0100, AD0200   WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ? "
sql = sql & " AND AG04CODFRANJA = ? "
sql = sql & " AND AG0100.PR01CODACTUACION = PR0100.PR01CODACTUACION"
sql = sql & " AND AG0100.AD02CODDPTO = AD0200.AD02CODDPTO"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
    qry(2) = ssgrdFranjas.Columns("CodFranja").Value
Set rs = qry.OpenResultset()
While Not rs.EOF
    ssgrdActuacion.AddItem rs!PR01CODACTUACION & Chr$(9) & _
                         rs!PR01DESCORTA & Chr$(9) & _
                         rs!AD02CODDPTO & Chr$(9) & _
                         rs!AD02DESDPTO & Chr$(9) & _
                         rs!AG01NUMASIGADM & Chr$(9) & rs!AG01NUMHORANT & Chr$(9) & _
                         ssgrdFranjas.Columns("CodFranja").Value & Chr$(9) & _
                         ssgrdPerfiles.Columns("CodPerfil").Value
                     
   rs.MoveNext
Wend

End Sub


Private Sub pNuevoPerfil()
Call objPipe.PipeSet("AG0201_AG0205_AG11CODRECURSO", lngCodRecurso)
frmNuevoPerfil.Show vbModal
ssgrdPerfiles.RemoveAll
ssgrdFranjas.RemoveAll
ssgrdPeriodos.RemoveAll
ssgrdActuacion.RemoveAll
pCargarPerfiles
End Sub
Private Sub pModPerfil()
Call objPipe.PipeSet("AG0201_AG0205_AG11CODRECURSO", lngCodRecurso)
Call objPipe.PipeSet("AG0201_AG0205_AG07CODPERFIL", ssgrdPerfiles.Columns("CodPerfil").Value)
frmNuevoPerfil.Show vbModal
ssgrdPerfiles.RemoveAll
ssgrdFranjas.RemoveAll
ssgrdPeriodos.RemoveAll
ssgrdActuacion.RemoveAll
pCargarPerfiles
End Sub
Private Sub pDuplicarPerfil()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim intCodPerfil As Integer
Dim strFechaBaja As String

On Error GoTo CancelTrans


'AG0700 --> CREAR PERFIL PREGUNTAR PRIORIDAD
Call objPipe.PipeSet("AG0201_AG0205_AG11CODRECURSO", lngCodRecurso)
frmNuevoPerfil.Show vbModal
'obtenemos el codigo del nuevo perfil creado
If objPipe.PipeExist("AG0205_AG0202_AG07CODPERFIL") Then
    intCodPerfil = objPipe.PipeGet("AG0205_AG0202_AG07CODPERFIL")
    objPipe.PipeRemove ("AG0205_AG0202_AG07CODPERFIL")
    sql = "SELECT AG07DESPERFIL,AG07PRIORIDAD,AG07FECBAJA FROM AG0700 "
    sql = sql & " WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = intCodPerfil 'CODIGO DEL PERFIL CREADO
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        If Not IsNull(rs!AG07FECBAJA) Then _
            strFechaBaja = Format(rs!AG07FECBAJA, "dd/mm/yyyy") _
        Else _
             strFechaBaja = ""
        
        ssgrdPerfiles.AddItem rs!AG07DESPERFIL & Chr$(9) & _
                              rs!AG07PRIORIDAD & Chr$(9) & _
                              strFechaBaja & Chr$(9) & intCodPerfil
    End If
Else
    MsgBox "Debe dar nombre y prioridad al nuevo perfil", vbCritical
    Exit Sub
End If

objApp.BeginTrans

'AG0400 --> COPIAR FRANJAS DEL PERFIL SELECCIONADO
sql = "INSERT INTO AG0400 (AG11CODRECURSO, AG07CODPERFIL,"
sql = sql & "AG04CODFRANJA, AG04INDLUNFRJA, PR12CODACTIVIDAD, AG04INDMARFRJA,"
sql = sql & "AG04INDMIEFRJA, AG04INDJUEFRJA, AG04INDVIEFRJA, AG04INDSABFRJA,"
sql = sql & "AG04INDDOMFRJA, AG04HORINFRJHH, AG04HORINFRJMM, AG04HORFIFRJHH,"
sql = sql & "AG04HORFIFRJMM, AG04NUMCITADMI, AG04PORCTIERES, AG04HORASOVEHH,"
sql = sql & "AG04HORASOVEMM, AG04FECBAJA, AG05NUMINCIDEN, AG04INTERVCITA,"
sql = sql & "AG04MODASIGCITA) SELECT AG11CODRECURSO,?,"
sql = sql & "AG04CODFRANJA, AG04INDLUNFRJA, PR12CODACTIVIDAD, AG04INDMARFRJA,"
sql = sql & "AG04INDMIEFRJA, AG04INDJUEFRJA, AG04INDVIEFRJA, AG04INDSABFRJA,"
sql = sql & "AG04INDDOMFRJA, AG04HORINFRJHH, AG04HORINFRJMM, AG04HORFIFRJHH,"
sql = sql & "AG04HORFIFRJMM, AG04NUMCITADMI, AG04PORCTIERES, AG04HORASOVEHH,"
sql = sql & "AG04HORASOVEMM, AG04FECBAJA, AG05NUMINCIDEN, AG04INTERVCITA,"
sql = sql & "AG04MODASIGCITA FROM AG0400 WHERE AG11CODRECURSO = ? "
sql = sql & "AND AG07CODPERFIL = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intCodPerfil 'CODIGO DEL PERFIL CREADO
    qry(1) = lngCodRecurso
    qry(2) = ssgrdPerfiles.Columns(3).Value 'CODIGO DEL PERFIL QUE DUPLICAMOS
qry.Execute
qry.Close
'objApp.RollBackTrans
'AG1000 --> PERMISO POR FRANJA
sql = "INSERT INTO AG1000 (AG11CODRECURSO,AG07CODPERFIL,AG04CODFRANJA,"
sql = sql & "AG10NUMPERMISO,AD02CODDPTO,SG02COD,AG10TIPAUTNOR,AG10TIPAUTOVE,"
sql = sql & "AG10TIPAUTRES,AG10NUMDIANOR,AG10NUMDIAOVE,AG10NUMDIARES"
sql = sql & ")SELECT AG11CODRECURSO,?,AG04CODFRANJA,"
sql = sql & "AG10NUMPERMISO,AD02CODDPTO,SG02COD,AG10TIPAUTNOR,AG10TIPAUTOVE,"
sql = sql & "AG10TIPAUTRES,AG10NUMDIANOR,AG10NUMDIAOVE,AG10NUMDIARES"
sql = sql & " FROM AG1000 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intCodPerfil 'CODIGO DEL PERFIL CREADO
    qry(1) = lngCodRecurso
    qry(2) = ssgrdPerfiles.Columns(3).Value 'CODIGO DEL PERFIL QUE DUPLICAMOS
qry.Execute
qry.Close

'AG1200 --> RESTRINCION POR FRANJA
sql = "INSERT INTO AG1200 (AG11CODRECURSO,AG07CODPERFIL,AG04CODFRANJA,"
sql = sql & "AG12NUMRESFRJA,AG16CODTIPREST,AG12VALDESDERES,AG12VALHASTARES,"
sql = sql & "AG12NIVELJERAR,AG12INDINCEXCL,AG12NIVELSUPER"
sql = sql & ") SELECT AG11CODRECURSO,?,AG04CODFRANJA,"
sql = sql & "AG12NUMRESFRJA,AG16CODTIPREST,AG12VALDESDERES,AG12VALHASTARES,"
sql = sql & "AG12NIVELJERAR,AG12INDINCEXCL,AG12NIVELSUPER"
sql = sql & " FROM AG1200 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intCodPerfil 'CODIGO DEL PERFIL CREADO
    qry(1) = lngCodRecurso
    qry(2) = ssgrdPerfiles.Columns(3).Value 'CODIGO DEL PERFIL QUE DUPLICAMOS
qry.Execute
qry.Close

'AG0100 --> ACTUACION POR FRANJA
sql = "INSERT INTO AG0100 (AG11CODRECURSO,AG07CODPERFIL,AG04CODFRANJA,"
sql = sql & "AG01NUMASIGADM,AD02CODDPTO,PR01CODACTUACION,AG01FECINVGACF,AG01NUMHORANT,"
sql = sql & "AG01FECFIVGACF"
sql = sql & ") SELECT AG11CODRECURSO,?,AG04CODFRANJA,"
sql = sql & "AG01NUMASIGADM,AD02CODDPTO,PR01CODACTUACION,AG01FECINVGACF,AG01NUMHORANT,"
sql = sql & "AG01FECFIVGACF FROM AG0100 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intCodPerfil 'CODIGO DEL PERFIL CREADO
    qry(1) = lngCodRecurso
    qry(2) = ssgrdPerfiles.Columns(3).Value 'CODIGO DEL PERFIL QUE DUPLICAMOS
qry.Execute
qry.Close


objApp.CommitTrans

Exit Sub
CancelTrans:
MsgBox "El perfil no se puede duplicar", vbCritical
objApp.RollBackTrans
End Sub

Private Sub pEliminarPerfil()
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim sql As String
Dim intCodPefil As Integer
Screen.MousePointer = vbHourglass
If fBlnCPEP(lngCodRecurso, ssgrdPerfiles.Columns(3).Value) Then
    On Error GoTo CancelTrans
    objApp.BeginTrans
    'AG0100 --> ACTUACION POR FRANJA
    sql = "DELETE FROM AG0100 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns(3).Value
    qry.Execute
    qry.Close
    
    'AG1200 --> RESTRINCION POR FRANJA
    sql = "DELETE FROM AG1200 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns(3).Value
    qry.Execute
    qry.Close
    
    'AG1000 --> PERMISO POR FRANJA
    sql = "DELETE FROM AG1000 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns(3).Value
    qry.Execute
    qry.Close
    
    'AG0400 --> FRANJAS
    
    sql = "DELETE FROM AG0400 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns(3).Value
    qry.Execute
    qry.Close
    
    'AG0900 --> PERIODOS DE VIGENCIA
    
    sql = "DELETE FROM AG0900 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns(3).Value
    qry.Execute
    qry.Close
    
    'AG0700 --> PERFIL
    
    sql = "DELETE FROM AG0700 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns(3).Value
    qry.Execute
    qry.Close
    
    'BORRAMOS DE LOS GRIDS
    ssgrdActuacion.RemoveAll
    ssgrdFranjas.RemoveAll
    ssgrdPeriodos.RemoveAll
    ssgrdPerfiles.DeleteSelected
    objApp.CommitTrans
    Screen.MousePointer = vbDefault
    Exit Sub
CancelTrans:
    MsgBox "Error borrando el perfil", vbCritical
    objApp.RollBackTrans
Else
    Screen.MousePointer = vbDefault
End If
End Sub

Private Sub pNuevoPeriodo()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strMotivo As String

Call objPipe.PipeSet("AG0202_AG0206_AG11CODRECURSO", lngCodRecurso)
Call objPipe.PipeSet("AG0202_AG0206_AG07CODPERFIL", ssgrdPerfiles.Columns(3).Value)
frmNuevoPeriodo.Show vbModal
Set frmNuevoPeriodo = Nothing
If objPipe.PipeExist("AG0206_AG0202_AG09NUMPVIGPER") Then
    sql = "SELECT  AG09NUMPVIGPER, AG09FECINVIPER, AG09FECFIVIPER,"
    sql = sql & "AG09MOTIVOPERF FROM AG0900 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND AG07CODPERFIL = ?"
    sql = sql & " AND AG09NUMPVIGPER = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
        qry(2) = objPipe.PipeGet("AG0206_AG0202_AG09NUMPVIGPER")
    Set rs = qry.OpenResultset()
    If IsNull(rs!AG09MOTIVOPERF) Then strMotivo = "" Else strMotivo = rs!AG09MOTIVOPERF
    ssgrdPeriodos.AddItem rs!AG09FECINVIPER & Chr$(9) & _
                            rs!AG09FECFIVIPER & Chr$(9) & _
                            strMotivo & Chr$(9) & _
                            rs!AG09NUMPVIGPER & Chr$(9) & _
                            ssgrdPerfiles.Columns("CodPerfil").Value
                
    objPipe.PipeRemove ("AG0206_AG0202_AG09NUMPVIGPER")
End If
End Sub
Private Sub pModificarPeriodo()
Call objPipe.PipeSet("AG0202_AG0206_AG11CODRECURSO", lngCodRecurso)
Call objPipe.PipeSet("AG0202_AG0206_AG07CODPERFIL", ssgrdPerfiles.Columns("CodPerfil").Value)
Call objPipe.PipeSet("AG0202_AG0206_AG09NUMPVIGPER", ssgrdPeriodos.Columns("CodPeriodo").Value)

frmNuevoPeriodo.Show vbModal
Set frmNuevoPeriodo = Nothing
ssgrdPeriodos.RemoveAll
pCargarPeriodos
End Sub

Private Sub pEliminarPeriodo()
Dim sql As String
Dim qry As rdoQuery

On Error GoTo CancelTrans

objApp.BeginTrans

If blnSacarPendientes(ssgrdPeriodos.Columns("Fecha Inicio").Value, ssgrdPeriodos.Columns("Fecha Fin").Value, lngCodRecurso) Then
    sql = "DELETE FROM AG0900 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND AG07CODPERFIL = ?"
    sql = sql & " AND AG09NUMPVIGPER = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
        qry(2) = ssgrdPeriodos.Columns("CodPeriodo").Value
    qry.Execute
    qry.Close
    ssgrdPeriodos.DeleteSelected
End If
objApp.CommitTrans
Exit Sub
CancelTrans:
objApp.RollBackTrans
MsgBox "Error al Eliminar el periodo", vbExclamation, Me.Caption
End Sub

Private Sub pNuevaFranja(intD As Integer)
Dim intCodFranja As Integer
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strCodActividad$
Dim strActividad$
Dim strModAsig$
'********
'*Funcion para crear una nueva franja si se seleciona la opcion de duplicar (indD = -1)
'*se copian tambien todas las actuaciones de la franja seleccionada.
'*********
Call objPipe.PipeSet("AG0202_AG0207_AG11CODRECURSO", lngCodRecurso)
Call objPipe.PipeSet("AG0202_AG0207_AG07CODPERFIL", ssgrdPerfiles.Columns(3).Value)
frmNuevaFranja.Show vbModal
Set frmNuevaFranja = Nothing
If objPipe.PipeExist("AG0207_AG0202_AG04CODFRANJA") Then
    intCodFranja = objPipe.PipeGet("AG0207_AG0202_AG04CODFRANJA")
    Call objPipe.PipeRemove("AG0207_AG0202_AG04CODFRANJA")
    sql = "SELECT AG11CODRECURSO,AG07CODPERFIL,AG04CODFRANJA,"
    sql = sql & "AG0400.PR12CODACTIVIDAD,PR12DESACTIVIDAD,AG04INDLUNFRJA,AG04INDMARFRJA,"
    sql = sql & "AG04INDMIEFRJA,AG04INDJUEFRJA,AG04INDVIEFRJA,"
    sql = sql & "AG04INDSABFRJA,AG04INDDOMFRJA,AG04HORINFRJHH,"
    sql = sql & "AG04HORINFRJMM,AG04HORFIFRJHH,AG04HORFIFRJMM,"
    sql = sql & "AG04NUMCITADMI,AG04INTERVCITA,AG04MODASIGCITA"
    sql = sql & " FROM AG0400,PR1200 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
    sql = sql & " AND AG04CODFRANJA = ? "
    sql = sql & " AND AG0400.PR12CODACTIVIDAD = PR1200.PR12CODACTIVIDAD(+)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
        qry(2) = intCodFranja
    Set rs = qry.OpenResultset()
If IsNull(rs!PR12CODACTIVIDAD) Then strCodActividad = " " Else strCodActividad = rs!PR12CODACTIVIDAD
If IsNull(rs!PR12DESACTIVIDAD) Then strActividad = " " Else strActividad = rs!PR12DESACTIVIDAD
Select Case rs!AG04MODASIGCITA
    Case 1: strModAsig = "Cantidad"
    Case 2: strModAsig = "Secuencial"
    Case 3: strModAsig = "Inter. Pred."
    Case 4: strModAsig = "Inter. Osci."
    Case Else: strModAsig = " "
End Select
    ssgrdFranjas.AddItem rs!AG04HORINFRJHH & ":" & rs!AG04HORINFRJMM & Chr$(9) & _
                 rs!AG04HORFIFRJHH & ":" & rs!AG04HORFIFRJMM & Chr$(9) & _
                 rs!AG04INDLUNFRJA & Chr$(9) & _
                 rs!AG04INDMARFRJA& & Chr$(9) & _
                 rs!AG04INDMIEFRJA & Chr$(9) & _
                 rs!AG04INDJUEFRJA & Chr$(9) & _
                 rs!AG04INDVIEFRJA & Chr$(9) & _
                 rs!AG04INDSABFRJA & Chr$(9) & rs!AG04INDDOMFRJA & Chr$(9) & _
                 strCodActividad & Chr$(9) & _
                 strActividad & Chr$(9) & _
                 strModAsig & Chr$(9) & _
                 rs!AG04NUMCITADMI & Chr$(9) & _
                 rs!AG04INTERVCITA & Chr$(9) & _
                 rs!AG04CODFRANJA & Chr$(9) & _
                 ssgrdPerfiles.Columns("CodPerfil").Value
End If
'rs.Close
'qry.Close

If intD = -1 Then 'duplicamos
    sql = "INSERT INTO AG0100 (AG11CODRECURSO,AG07CODPERFIL,"
    sql = sql & " AG04CODFRANJA,AG01NUMASIGADM,AD02CODDPTO,"
    sql = sql & " PR01CODACTUACION,AG01FECINVGACF) SELECT AG11CODRECURSO,AG07CODPERFIL,?,"
    sql = sql & " AG01NUMASIGADM,AD02CODDPTO,"
    sql = sql & " PR01CODACTUACION,AG01FECINVGACF"
    sql = sql & " FROM AG0100 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND AG07CODPERFIL = ? "
    sql = sql & " AND AG04CODFRANJA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = intCodFranja
        qry(1) = lngCodRecurso
        qry(2) = ssgrdPerfiles.Columns("CodPerfil").Value
        qry(3) = ssgrdFranjas.Columns("CodFranja").Value
    qry.Execute
    qry.Close
End If
End Sub
Private Sub pModificarFranja()
Call objPipe.PipeSet("AG0202_AG0207_AG11CODRECURSO", lngCodRecurso)
Call objPipe.PipeSet("AG0202_AG0207_AG07CODPERFIL", ssgrdPerfiles.Columns("CodPerfil").Value)
Call objPipe.PipeSet("AG0202_AG0207_AG04CODFRANJA", ssgrdFranjas.Columns("CodFranja").Value)
frmNuevaFranja.Show vbModal
Set frmNuevaFranja = Nothing
ssgrdFranjas.RemoveAll
pCargarFranjas
End Sub
Private Sub pEliminarFranja()
Dim sql As String
Dim sql1 As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim rs1 As rdoResultset
Dim strHoraIni As String
Dim strHoraFin As String
Dim intR As Integer
Dim msg As String
Dim strAhora
Dim strIniPerfil

strAhora = fAhora


strHoraIni = Format(Trim(ssgrdFranjas.Columns("Inicio").Value), "HH:NN")
strHoraFin = Format(Trim(ssgrdFranjas.Columns("fin").Value), "HH:NN")

On Error GoTo CancelTrans
'Buscamos lo periodosd de vigencia a partir del dia de hoy
Screen.MousePointer = vbHourglass

objApp.BeginTrans
sql = "SELECT AG09FECINVIPER,AG09FECFIVIPER FROM AG0900 "
sql = sql & " WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ?"
sql = sql & " AND AG09FECFIVIPER > SYSDATE "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    'miramos cuantos pacientes hay en esos periodos de vigencia
    sql = "SELECT COUNT(*) FROM CI0100, PR0400 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
    sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND CI01SITCITA = '1' AND ("
    While Not rs.EOF
        If CDate(Format(rs(0), "DD/MM/YYYY")) < CDate(strAhora) Then _
        strIniPerfil = CDate(strAhora) Else strIniPerfil = rs(0)
        sql1 = sql1 & "(CI01FECCONCERT >= TO_DATE( '" & Format$(strIniPerfil, "DD/MM/YYYY") & "','DD/MM/YYYY')"
        sql1 = sql1 & " AND CI01FECCONCERT <= TO_DATE( '" & Format$(rs(1), "DD/MM/YYYY") & "','DD/MM/YYYY')) OR "
        rs.MoveNext
    Wend
    rs.Close
    qry.Close
    sql1 = Left(sql1, Len(sql1) - 3)
    sql1 = sql1 & ") AND ("
    If ssgrdFranjas.Columns("L").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%LUNES%' OR"
    If ssgrdFranjas.Columns("M").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%MARTES%' OR"
    If ssgrdFranjas.Columns("X").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%MI�RCOLES%' OR"
    If ssgrdFranjas.Columns("J").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%JUEVES%' OR"
    If ssgrdFranjas.Columns("V").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%VIERNES%' OR"
    If ssgrdFranjas.Columns("S").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%S�BADO%' OR"
    If ssgrdFranjas.Columns("D").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%DOMINGO%' OR"
    sql1 = Left(sql1, Len(sql1) - 3)
    sql1 = sql1 & ") AND TO_DATE(TO_CHAR(CI01FECCONCERT,'HH24:MI'),'HH24:MI') >= TO_DATE('" & strHoraIni & "','HH24:MI')"
    sql1 = sql1 & " AND TO_DATE(TO_CHAR(CI01FECCONCERT,'HH24:MI'),'HH24:MI') < TO_DATE('" & strHoraFin & "','HH24:MI')"
    sql = sql & sql1
    
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
    Set rs1 = qry.OpenResultset()
    intR = MsgBox("Quedar�n afectados " & rs1(0) & " pacientes" & Chr$(13) & _
            "Desea Continuar? ", vbYesNo + vbQuestion, Me.Caption)
    If intR = vbNo Then
        Screen.MousePointer = vbDefault
        objApp.RollBackTrans
        Exit Sub
    End If
    If rs1(0) > 0 Then
        rs1.Close
        sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_PLANIFICADA
        sql = sql & " WHERE PR37CODESTADO = " & constESTACT_CITADA
        sql = sql & " AND PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM CI0100 "
        sql = sql & " WHERE CI01SITCITA = '1' AND (" & sql1
        sql = sql & " AND AG11CODRECURSO = ? )"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCodRecurso
        qry.Execute
        qry.Close
        sql = "UPDATE CI0100 SET CI01SITCITA = '4' WHERE AG11CODRECURSO = ? "
        sql = sql & " AND CI01SITCITA = '1' AND PR04NUMACTPLAN IN (SELECT"
        sql = sql & " PR0400.PR04NUMACTPLAN FROM CI0100,PR0400 WHERE "
        sql = sql & " PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
        sql = sql & " AND PR37CODESTADO = 1 AND (" & sql1
        sql = sql & " AND AG11CODRECURSO = ? AND CI01SITCITA = '1' )"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCodRecurso
            qry(1) = qry(0)
        qry.Execute
        qry.Close
    End If
    'MIRAMOS LAS RESERVAS

    sql = "SELECT TO_CHAR(CI01FECCONCERT,'DD/MM/YYYY HH24:MI'), PR08DESOBSERV"
    sql = sql & " FROM CI0100, PR0400, PR0800 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI "
    sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND CI01SITCITA = '5' AND ("
    sql = sql & sql1
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
    While Not rs.EOF
            If Not IsNull(rs(1)) Then
                msg = msg & rs(0) & "-> " & rs(1) & Chr$(13)
            Else
                msg = msg & rs(0) & Chr$(13)
            End If
            rs.MoveNext
        Wend
        MsgBox "Las siguientes reservas quedaran afectadas :" & Chr$(13) & msg, vbOKOnly, "Reservas"
    End If
    rs.Close
    qry.Close

End If



sql = "DELETE FROM AG0100 WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ? "
sql = sql & " AND AG04CODFRANJA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdFranjas.Columns("CodPerfil").Value
    qry(2) = ssgrdFranjas.Columns("CodFranja").Value
qry.Execute
qry.Close

sql = "DELETE FROM AG1200 WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ? "
sql = sql & " AND AG04CODFRANJA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdFranjas.Columns("CodPerfil").Value
    qry(2) = ssgrdFranjas.Columns("CodFranja").Value
qry.Execute
qry.Close

sql = "DELETE FROM AG0400 WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ? "
sql = sql & " AND AG04CODFRANJA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdFranjas.Columns("CodPerfil").Value
    qry(2) = ssgrdFranjas.Columns("CodFranja").Value
qry.Execute

objApp.CommitTrans

Screen.MousePointer = vbDefault
ssgrdFranjas.DeleteSelected
Exit Sub
CancelTrans:
Screen.MousePointer = vbDefault
MsgBox "Error al Eliminar la Franja", vbExclamation, Me.Caption
objApp.RollBackTrans
End Sub

Private Sub pNuevaActuacion()
Call objPipe.PipeSet("AG0202_AG0208_AG11CODRECURSO", lngCodRecurso)
Call objPipe.PipeSet("AG0202_AG0208_AG07CODPERFIL", ssgrdPerfiles.Columns("CodPerfil").Value)
Call objPipe.PipeSet("AG0202_AG0208_AG04CODFRANJA", ssgrdFranjas.Columns("CodFranja").Value)
frmBuscaActuaciones.Show vbModal
Set frmBuscaActuaciones = Nothing
ssgrdActuacion.RemoveAll
pCargarActuaciones
End Sub


Private Sub pEliminarActuacion()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql1 As String
Dim strHoraIni As String
Dim strHoraFin As String
Dim rs1 As rdoResultset
Dim intR As Integer
Dim msg As String

strHoraIni = Format(Trim(ssgrdFranjas.Columns("Inicio").Value), "HH:NN")
strHoraFin = Format(Trim(ssgrdFranjas.Columns("fin").Value), "HH:NN")
On Error GoTo CancelTrans
'BUSCAMOS LOS PACIENTES AFECTADOS AL QUITAR ESTA ACTUACION
'seleccionamos los periodos de la franja.
Screen.MousePointer = vbHourglass

objApp.BeginTrans

If Not fblnPacAfectAct(strHoraIni, strHoraFin) Then Exit Sub

'borramos la actukacion
sql = "DELETE FROM AG0100 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
sql = sql & " AND AG04CODFRANJA = ? AND PR01CODACTUACION = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
    qry(2) = ssgrdFranjas.Columns("CodFranja").Value
    qry(3) = ssgrdActuacion.Columns("CodActuacion").Value
qry.Execute

Screen.MousePointer = vbDefault
ssgrdActuacion.DeleteSelected
Exit Sub
CancelTrans:
 MsgBox "No se puede Elimiar la Actuacion. Error:" & Error, vbExclamation, Me.Caption
 objApp.RollBackTrans
 Screen.MousePointer = vbDefault
End Sub
Private Sub pRestricciones()
Call objPipe.PipeSet("AG0202_AG0209_AG11CODRECURSO", lngCodRecurso)
Call objPipe.PipeSet("AG0202_AG0209_AG07CODPERFIL", ssgrdPerfiles.Columns("CodPerfil").Value)
Call objPipe.PipeSet("AG0202_AG0209_AG04CODFRANJA", ssgrdFranjas.Columns("CodFranja").Value)
frmRestricciones.Show vbModal
Set frmRestricciones = Nothing

End Sub

Private Sub pCargarRecurso()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
Set rs = qry.OpenResultset()
If Not rs.EOF Then txtRecurso = rs(0)
rs.Close
qry.Close

End Sub

Private Function fblnPacAfectAct(strHoraIni As String, strHoraFin) As Boolean
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql1 As String
Dim rs1 As rdoResultset
Dim intR As Integer
Dim msg As String
Dim strIniPerfil As String
Dim strAhora As String

strAhora = fAhora



sql = "SELECT AG09FECINVIPER,AG09FECFIVIPER FROM AG0900 "
sql = sql & " WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ?"
sql = sql & " AND AG09FECFIVIPER > SYSDATE "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdPerfiles.Columns("CodPerfil").Value
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    'miramos cuantos pacientes hay en esos periodos de vigencia
    sql = "SELECT COUNT(*) FROM CI0100, PR0400 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND CI01SITCITA = '1' "
    sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
    sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND PR01CODACTUACION = ? AND ("
    While Not rs.EOF
        If CDate(Format(rs(0), "DD/MM/YYYY")) < CDate(strAhora) Then _
        strIniPerfil = CDate(strAhora) Else strIniPerfil = rs(0)
        sql1 = sql1 & "(CI01FECCONCERT >= TO_DATE( '" & Format$(strIniPerfil, "DD/MM/YYYY") & "','DD/MM/YYYY')"
        sql1 = sql1 & " AND CI01FECCONCERT <= TO_DATE( '" & Format$(rs(1), "DD/MM/YYYY") & "','DD/MM/YYYY')) OR "
        rs.MoveNext
    Wend
    rs.Close
    qry.Close
    sql1 = Left(sql1, Len(sql1) - 3)
    sql1 = sql1 & ") AND ("
    If ssgrdFranjas.Columns("L").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%LUNES%' OR"
    If ssgrdFranjas.Columns("M").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%MARTES%' OR"
    If ssgrdFranjas.Columns("X").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%MI�RCOLES%' OR"
    If ssgrdFranjas.Columns("J").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%JUEVES%' OR"
    If ssgrdFranjas.Columns("V").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%VIERNES%' OR"
    If ssgrdFranjas.Columns("S").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%S�BADO%' OR"
    If ssgrdFranjas.Columns("D").Value = -1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%DOMINGO%' OR"
    sql1 = Left(sql1, Len(sql1) - 3)
    sql1 = sql1 & ") AND TO_DATE(TO_CHAR(CI01FECCONCERT,'HH24:MI'),'HH24:MI') >= TO_DATE('" & strHoraIni & "','HH24:MI')"
    sql1 = sql1 & " AND TO_DATE(TO_CHAR(CI01FECCONCERT,'HH24:MI'),'HH24:MI') < TO_DATE('" & strHoraFin & "','HH24:MI')"
    sql = sql & sql1
    
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
        qry(1) = ssgrdActuacion.Columns("CodActuacion").Value
    Set rs1 = qry.OpenResultset()
    intR = MsgBox("Quedar�n afectados " & rs1(0) & " pacientes" & Chr$(13) & _
            "Desea Continuar? ", vbYesNo + vbQuestion, Me.Caption)
    If intR = vbNo Then
        Screen.MousePointer = vbDefault
        objApp.RollBackTrans
        fblnPacAfectAct = False
        Exit Function
    End If
    If rs1(0) > 0 Then
        rs1.Close
        sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_PLANIFICADA
        sql = sql & " WHERE PR37CODESTADO = " & constESTACT_CITADA
        sql = sql & " AND PR01CODACTUACION = ? "
        sql = sql & " AND PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM CI0100 "
        sql = sql & " WHERE CI01SITCITA = '1' AND (" & sql1
        sql = sql & " AND AG11CODRECURSO = ? )"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(1) = lngCodRecurso
            qry(0) = ssgrdActuacion.Columns("CodActuacion").Value
        qry.Execute
        qry.Close
        sql = "UPDATE CI0100 SET CI01SITCITA = '4' WHERE AG11CODRECURSO = ? "
        sql = sql & " AND CI01SITCITA = '1' AND PR04NUMACTPLAN IN (SELECT"
        sql = sql & " PR0400. PR04NUMACTPLAN FROM PR0400,CI0100 "
        sql = sql & " WHERE CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
        sql = sql & " AND CI01SITCITA = '1' AND PR37CODESTADO = " & constESTACT_PLANIFICADA
        sql = sql & " AND PR0400.PR01CODACTUACION = ? AND (" & sql1
        sql = sql & " AND AG11CODRECURSO = ? AND CI01SITCITA = '1' )"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCodRecurso
            qry(1) = ssgrdActuacion.Columns("CodActuacion").Value
            qry(2) = lngCodRecurso
        qry.Execute
        qry.Close
    End If
End If
'miramos las reservas
sql = "SELECT TO_CHAR(CI01FECCONCERT,'DD/MM/YYYY HH24:MI'), PR08DESOBSERV"
sql = sql & " FROM CI0100, PR0400, PR0800 WHERE AG11CODRECURSO = ? "
sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI "
sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
sql = sql & " AND PR01CODACTUACION = ?"
sql = sql & " AND CI01SITCITA = '5' AND ("
sql = sql & sql1
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = ssgrdActuacion.Columns("CodActuacion").Value
Set rs = qry.OpenResultset()
If Not rs.EOF Then
While Not rs.EOF
        If Not IsNull(rs(1)) Then
            msg = msg & rs(0) & "-> " & rs(1) & Chr$(13)
        Else
            msg = msg & rs(0) & Chr$(13)
        End If
        rs.MoveNext
    Wend
    MsgBox "Las siguientes reservas quedaran afectadas :" & Chr$(13) & msg, vbOKOnly, "Reservas"
End If
fblnPacAfectAct = True
rs.Close
qry.Close
End Function

