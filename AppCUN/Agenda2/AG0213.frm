VERSION 5.00
Begin VB.Form frmObserv 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Observaciones"
   ClientHeight    =   2055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8160
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2055
   ScaleWidth      =   8160
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   7140
      TabIndex        =   1
      Top             =   120
      Width           =   915
   End
   Begin VB.TextBox txtObserv 
      Height          =   1815
      Left            =   60
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   6975
   End
End
Attribute VB_Name = "frmObserv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
    Call objPipe.PipeSet("AG0213_OBSERV", txtObserv.Text)
    Unload Me
End Sub

Private Sub Form_Load()
    Dim lngUserColor&
    
    If objPipe.PipeExist("AG0213_OBSERV") Then
        txtObserv.Text = objPipe.PipeGet("AG0213_OBSERV")
    End If
    If objPipe.PipeExist("AG0213_USERCOLOR") Then
        lngUserColor = objPipe.PipeGet("AG0213_USERCOLOR")
    End If
    If lngUserColor = objApp.objUserColor.lngReadOnly Then
        txtObserv.Locked = True
        txtObserv.BackColor = lngUserColor
    End If
End Sub
