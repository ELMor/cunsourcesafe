VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{7D246A04-1B14-11D2-BEFA-00006E228339}#2.0#0"; "schedocx.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmRecursos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Recursos"
   ClientHeight    =   8595
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   11400
      Top             =   2160
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdImprimir 
      Appearance      =   0  'Flat
      Caption         =   "&Imp. Perf."
      Height          =   300
      Left            =   7920
      TabIndex        =   33
      Top             =   2280
      Width           =   855
   End
   Begin VB.CommandButton cmdAgenda 
      Appearance      =   0  'Flat
      Caption         =   "&Agenda"
      Height          =   375
      Left            =   11040
      TabIndex        =   32
      Top             =   1200
      Width           =   735
   End
   Begin VB.CommandButton cmdCitasRecurso 
      Caption         =   "&C. Rec."
      Height          =   375
      Left            =   11040
      TabIndex        =   31
      Top             =   720
      Width           =   735
   End
   Begin VB.CommandButton cmdVer 
      Caption         =   "Consultar"
      Height          =   300
      Left            =   6720
      TabIndex        =   30
      Top             =   2280
      Width           =   975
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   11040
      TabIndex        =   28
      Top             =   240
      Width           =   735
   End
   Begin VB.CommandButton cmdMantenimiento 
      Caption         =   "Manteni."
      Height          =   375
      Left            =   11040
      TabIndex        =   25
      Top             =   1680
      Width           =   735
   End
   Begin SCHEDOCX2Lib.SchedOCX SchedOCX1 
      Height          =   5895
      Left            =   0
      TabIndex        =   24
      Top             =   2640
      Width           =   11775
      _Version        =   131072
      _ExtentX        =   20770
      _ExtentY        =   10398
      _StockProps     =   35
      Caption         =   "SchedOCX1"
      BorderStyle     =   1
      DisplayMode     =   0
      BevelHighlightColor=   16777215
      BevelShadowColor=   8421504
      BevelWidth      =   4
      GridMajorColor  =   0
      GridMajorThickness=   2
      ProcessTab      =   -1  'True
      DoubleClickDivide=   4
      ScrollBarColor  =   12632256
      ScrollBarWidth  =   14
      CurrentTimeColor=   255
      CurrentTimeLine =   -1  'True
      DefaultDateTimeFormat=   "INTLSHORTDATE INTLTIME"
      DefaultDurationFormat=   "%D %lds %H %lhs %M %lns"
      BarMoveTextFormat=   "INTLSHORTDATE INTLTIME"
      ResHdrHeight    =   30
      ResHdrBackColor =   12632256
      ResHdrBevel     =   2
      ResHdrGridColor =   0
      ResHdrGridThickness=   1
      ResHdrResize    =   0   'False
      ResHdrFontName  =   "Arial"
      ResHdrFontSizePercent=   30
      ResHdrFontBold  =   0   'False
      ResHdrFontItalic=   0   'False
      ResHdrFontOrient=   0
      ResHdrFontColor =   0
      ResColMax       =   4
      ResView         =   0
      ResWidth        =   140
      ResUnusedBackColor=   12632256
      ResBevel        =   2
      ResGridColor    =   0
      ResGridThickness=   1
      ResWidthResize  =   -1  'True
      ResLineResize   =   0   'False
      TBHdrBackColor  =   65535
      TBHdrBevel      =   2
      TBHdrGridColor  =   0
      TBHdrHeight     =   35
      TBColMax        =   4
      TBHeight        =   150
      TBLineHeight    =   16
      TBView          =   0
      TBWidth         =   325
      TBBackColor     =   12632256
      TBBevel         =   2
      TBGridColor     =   0
      TBFontName      =   "Arial"
      TBFontSizePercent=   90
      TBFontBold      =   0   'False
      TBFontItalic    =   0   'False
      TBFontOrient    =   0
      TBFontColor     =   0
      SchedBackColor  =   16776960
      SchedBevel      =   0
      SchedGridColor  =   0
      SchedGridThickness=   1
      LegendTitle     =   "Legend"
      LegendPosition  =   2
      LegendType      =   0
      LegendSize      =   125
      LegendLineHeight=   20
      LegendBackColor =   49344
      LegendBevel     =   2
      LegendView      =   0
      LegendResize    =   -1  'True
      PrintShowDialog =   -1  'True
      PrintHeader     =   0
      PrintFooter     =   0
      PrintWidth      =   0
      PrintHeight     =   0
      PrintLeftMargin =   1
      PrintTopMargin  =   1
      PrintRightMargin=   1
      PrintBottomMargin=   1
      PrintHeaderHeight=   0,5
      PrintHeaderText =   "%k (%d %t)"
      PrintHeaderTextHeight=   0,25
      PrintFooterHeight=   0,5
      PrintFooterText =   "Page %p"
      PrintFooterTextHeight=   0,25
      MonthNumColor   =   0
      MonthNumSize    =   25
      MonthNonNumColor=   192
      MonthNonNumSize =   20
      ResColLabel0    =   "Perfiles"
      ResColField0    =   3
      ResColFormat0   =   ""
      ResColWidth0    =   100
      ResColAlignment0=   0
      ResColVisible0  =   -1  'True
      ResColFrozen0   =   -1  'True
      ResColEditable0 =   0   'False
      ResColResizable0=   0   'False
      ResColLabel1    =   "Pri."
      ResColField1    =   6
      ResColFormat1   =   ""
      ResColWidth1    =   30
      ResColAlignment1=   1
      ResColVisible1  =   0   'False
      ResColFrozen1   =   -1  'True
      ResColEditable1 =   -1  'True
      ResColResizable1=   0   'False
      ResColLabel2    =   "Prdos"
      ResColField2    =   5
      ResColFormat2   =   ""
      ResColWidth2    =   35
      ResColAlignment2=   1
      ResColVisible2  =   -1  'True
      ResColFrozen2   =   -1  'True
      ResColEditable2 =   0   'False
      ResColResizable2=   0   'False
      ResColLabel3    =   "CODPERFIL"
      ResColField3    =   7
      ResColFormat3   =   ""
      ResColWidth3    =   50
      ResColAlignment3=   0
      ResColVisible3  =   0   'False
      ResColFrozen3   =   0   'False
      ResColEditable3 =   -1  'True
      ResColResizable3=   -1  'True
      TBColLabel0     =   "\n#"
      TBColField0     =   1
      TBColFormat0    =   ""
      TBColWidth0     =   25
      TBColAlignment0 =   1
      TBColVisible0   =   -1  'True
      TBColFrozen0    =   -1  'True
      TBColEditable0  =   0   'False
      TBColResizable0 =   -1  'True
      TBColLabel1     =   "Inicio"
      TBColField1     =   4
      TBColFormat1    =   "INTLSHORTDATE INTLTIME"
      TBColWidth1     =   120
      TBColAlignment1 =   0
      TBColVisible1   =   -1  'True
      TBColFrozen1    =   0   'False
      TBColEditable1  =   0   'False
      TBColResizable1 =   -1  'True
      TBColLabel2     =   "Fin"
      TBColField2     =   5
      TBColFormat2    =   "INTLSHORTDATE INTLTIME"
      TBColWidth2     =   120
      TBColAlignment2 =   0
      TBColVisible2   =   -1  'True
      TBColFrozen2    =   0   'False
      TBColEditable2  =   0   'False
      TBColResizable2 =   -1  'True
      TBColLabel3     =   "Label"
      TBColField3     =   0
      TBColFormat3    =   ""
      TBColWidth3     =   50
      TBColAlignment3 =   0
      TBColVisible3   =   -1  'True
      TBColFrozen3    =   0   'False
      TBColEditable3  =   -1  'True
      TBColResizable3 =   -1  'True
      BarName0        =   "Red Bar"
      BarSize0        =   50
      BarOffset0      =   0
      BarColor0       =   192
      BarPattern0     =   0
      BarAdjustable0  =   0
      BarTextPosition0=   6
      BarTextField0   =   1
      BarTextFormat0  =   ""
      BarTextClip0    =   0   'False
      BarFontColor0   =   16777215
      BarFontName0    =   "Arial"
      BarFontSizePercent0=   90
      BarFontBold0    =   0   'False
      BarFontItalic0  =   0   'False
      BarBegSymbol0   =   2
      BarBegType0     =   0
      BarBegColor0    =   12582912
      BarBegSize0     =   50
      BarBegOffset0   =   0
      BarEndSymbol0   =   2
      BarEndType0     =   0
      BarEndColor0    =   12582912
      BarEndSize0     =   50
      BarEndOffset0   =   0
      BarName1        =   "Green Bar"
      BarSize1        =   50
      BarOffset1      =   0
      BarColor1       =   65280
      BarPattern1     =   0
      BarAdjustable1  =   0
      BarTextPosition1=   6
      BarTextField1   =   1
      BarTextFormat1  =   ""
      BarTextClip1    =   0   'False
      BarFontColor1   =   0
      BarFontName1    =   "Arial"
      BarFontSizePercent1=   90
      BarFontBold1    =   0   'False
      BarFontItalic1  =   0   'False
      BarBegSymbol1   =   2
      BarBegType1     =   0
      BarBegColor1    =   0
      BarBegSize1     =   50
      BarBegOffset1   =   0
      BarEndSymbol1   =   2
      BarEndType1     =   0
      BarEndColor1    =   0
      BarEndSize1     =   50
      BarEndOffset1   =   0
      BarName2        =   "Yellow Bar"
      BarSize2        =   50
      BarOffset2      =   0
      BarColor2       =   65535
      BarPattern2     =   0
      BarAdjustable2  =   0
      BarTextPosition2=   6
      BarTextField2   =   1
      BarTextFormat2  =   ""
      BarTextClip2    =   0   'False
      BarFontColor2   =   0
      BarFontName2    =   "Arial"
      BarFontSizePercent2=   90
      BarFontBold2    =   0   'False
      BarFontItalic2  =   0   'False
      BarBegSymbol2   =   2
      BarBegType2     =   0
      BarBegColor2    =   255
      BarBegSize2     =   50
      BarBegOffset2   =   0
      BarEndSymbol2   =   2
      BarEndType2     =   0
      BarEndColor2    =   255
      BarEndSize2     =   50
      BarEndOffset2   =   0
      BarName3        =   "Black Bar"
      BarSize3        =   50
      BarOffset3      =   0
      BarColor3       =   0
      BarPattern3     =   0
      BarAdjustable3  =   0
      BarTextPosition3=   6
      BarTextField3   =   1
      BarTextFormat3  =   ""
      BarTextClip3    =   0   'False
      BarFontColor3   =   16777215
      BarFontName3    =   "Arial"
      BarFontSizePercent3=   90
      BarFontBold3    =   0   'False
      BarFontItalic3  =   0   'False
      BarBegSymbol3   =   5
      BarBegType3     =   0
      BarBegColor3    =   192
      BarBegSize3     =   50
      BarBegOffset3   =   0
      BarEndSymbol3   =   5
      BarEndType3     =   0
      BarEndColor3    =   192
      BarEndSize3     =   50
      BarEndOffset3   =   0
      BarName4        =   "White Bar"
      BarSize4        =   50
      BarOffset4      =   0
      BarColor4       =   16777215
      BarPattern4     =   0
      BarAdjustable4  =   0
      BarTextPosition4=   6
      BarTextField4   =   1
      BarTextFormat4  =   ""
      BarTextClip4    =   0   'False
      BarFontColor4   =   0
      BarFontName4    =   "Arial"
      BarFontSizePercent4=   90
      BarFontBold4    =   0   'False
      BarFontItalic4  =   0   'False
      BarBegSymbol4   =   5
      BarBegType4     =   0
      BarBegColor4    =   65535
      BarBegSize4     =   50
      BarBegOffset4   =   0
      BarEndSymbol4   =   5
      BarEndType4     =   0
      BarEndColor4    =   65535
      BarEndSize4     =   50
      BarEndOffset4   =   0
      BarName5        =   "Purple Bar"
      BarSize5        =   50
      BarOffset5      =   0
      BarColor5       =   12583104
      BarPattern5     =   0
      BarAdjustable5  =   0
      BarTextPosition5=   6
      BarTextField5   =   1
      BarTextFormat5  =   ""
      BarTextClip5    =   0   'False
      BarFontColor5   =   16777215
      BarFontName5    =   "Arial"
      BarFontSizePercent5=   90
      BarFontBold5    =   0   'False
      BarFontItalic5  =   0   'False
      BarBegSymbol5   =   8
      BarBegType5     =   0
      BarBegColor5    =   12583104
      BarBegSize5     =   50
      BarBegOffset5   =   0
      BarEndSymbol5   =   7
      BarEndType5     =   0
      BarEndColor5    =   12583104
      BarEndSize5     =   50
      BarEndOffset5   =   0
      BarName6        =   "Bright Red Bar"
      BarSize6        =   50
      BarOffset6      =   0
      BarColor6       =   255
      BarPattern6     =   0
      BarAdjustable6  =   0
      BarTextPosition6=   6
      BarTextField6   =   1
      BarTextFormat6  =   ""
      BarTextClip6    =   0   'False
      BarFontColor6   =   0
      BarFontName6    =   "Arial"
      BarFontSizePercent6=   90
      BarFontBold6    =   0   'False
      BarFontItalic6  =   0   'False
      BarBegSymbol6   =   3
      BarBegType6     =   0
      BarBegColor6    =   16711680
      BarBegSize6     =   50
      BarBegOffset6   =   0
      BarEndSymbol6   =   3
      BarEndType6     =   0
      BarEndColor6    =   16711680
      BarEndSize6     =   50
      BarEndOffset6   =   0
      BarName7        =   "Bright Blue Bar"
      BarSize7        =   50
      BarOffset7      =   0
      BarColor7       =   16711680
      BarPattern7     =   0
      BarAdjustable7  =   0
      BarTextPosition7=   6
      BarTextField7   =   1
      BarTextFormat7  =   ""
      BarTextClip7    =   0   'False
      BarFontColor7   =   16777215
      BarFontName7    =   "Arial"
      BarFontSizePercent7=   90
      BarFontBold7    =   0   'False
      BarFontItalic7  =   0   'False
      BarBegSymbol7   =   3
      BarBegType7     =   0
      BarBegColor7    =   255
      BarBegSize7     =   50
      BarBegOffset7   =   0
      BarEndSymbol7   =   3
      BarEndType7     =   0
      BarEndColor7    =   255
      BarEndSize7     =   50
      BarEndOffset7   =   0
      TSThumbWindow0  =   -1  'True
      TSBackColor0    =   192
      TSBevel0        =   2
      TSGridColor0    =   0
      TSGridThickness0=   1
      TSMajorPercent0 =   40
      TSNumMinorTicks0=   0
      TSSizeToTick0   =   0   'False
      TSSize0         =   40
      TSMajor0        =   2
      TSMajorCount0   =   1
      TSMajorFontColor0=   16777215
      TSMajorFontName0=   "Arial"
      TSMajorFontSizePercent0=   90
      TSMajorFontBold0=   0   'False
      TSMajorFontItalic0=   0   'False
      TSMajorFontOrient0=   0
      TSMajorFormat0  =   0
      TSMajorGrid0    =   -1  'True
      TSMinor0        =   4
      TSMinorCount0   =   1
      TSMinorFontColor0=   65535
      TSMinorFontName0=   "Arial"
      TSMinorFontSizePercent0=   75
      TSMinorFontBold0=   0   'False
      TSMinorFontItalic0=   0   'False
      TSMinorFontOrient0=   0
      TSMinorFormat0  =   0
      TSMinorGrid0    =   0   'False
      TSRulerLarge0   =   1
      TSRulerSmall0   =   3
      TSRulerColor0   =   65535
      TSDateTimeBeg0  =   "01/01/2000 0:00"
      TSDateTimeEnd0  =   "01/01/2001 0:00"
      TSDateTimeView0 =   "04/07/2000 17:14"
      TSThumbWindow1  =   -1  'True
      TSBackColor1    =   192
      TSBevel1        =   2
      TSGridColor1    =   0
      TSGridThickness1=   1
      TSMajorPercent1 =   40
      TSNumMinorTicks1=   0
      TSSizeToTick1   =   0   'False
      TSSize1         =   40
      TSMajor1        =   0
      TSMajorCount1   =   1
      TSMajorFontColor1=   16777215
      TSMajorFontName1=   "Arial"
      TSMajorFontSizePercent1=   90
      TSMajorFontBold1=   0   'False
      TSMajorFontItalic1=   0   'False
      TSMajorFontOrient1=   0
      TSMajorFormat1  =   0
      TSMajorGrid1    =   -1  'True
      TSMinor1        =   0
      TSMinorCount1   =   1
      TSMinorFontColor1=   65535
      TSMinorFontName1=   "Arial"
      TSMinorFontSizePercent1=   75
      TSMinorFontBold1=   0   'False
      TSMinorFontItalic1=   0   'False
      TSMinorFontOrient1=   0
      TSMinorFormat1  =   0
      TSMinorGrid1    =   -1  'True
      TSRulerLarge1   =   1
      TSRulerSmall1   =   3
      TSRulerColor1   =   65535
      TSDateTimeBeg1  =   "01/01/2000 17:00"
      TSDateTimeEnd1  =   "01/01/2000 8:00"
      TSDateTimeView1 =   "01/01/2000 8:00"
      WeekBeginning   =   0
      MonthNameLong0  =   "January"
      MonthNameShort0 =   "Jan"
      MonthNameLetter0=   "J"
      MonthNameLong1  =   "February"
      MonthNameShort1 =   "Feb"
      MonthNameLetter1=   "F"
      MonthNameLong2  =   "March"
      MonthNameShort2 =   "Mar"
      MonthNameLetter2=   "M"
      MonthNameLong3  =   "April"
      MonthNameShort3 =   "Apr"
      MonthNameLetter3=   "A"
      MonthNameLong4  =   "May"
      MonthNameShort4 =   "May"
      MonthNameLetter4=   "M"
      MonthNameLong5  =   "June"
      MonthNameShort5 =   "Jun"
      MonthNameLetter5=   "J"
      MonthNameLong6  =   "July"
      MonthNameShort6 =   "Jul"
      MonthNameLetter6=   "J"
      MonthNameLong7  =   "August"
      MonthNameShort7 =   "Aug"
      MonthNameLetter7=   "A"
      MonthNameLong8  =   "September"
      MonthNameShort8 =   "Sep"
      MonthNameLetter8=   "S"
      MonthNameLong9  =   "October"
      MonthNameShort9 =   "Oct"
      MonthNameLetter9=   "O"
      MonthNameLong10 =   "November"
      MonthNameShort10=   "Nov"
      MonthNameLetter10=   "N"
      MonthNameLong11 =   "December"
      MonthNameShort11=   "Dec"
      MonthNameLetter11=   "D"
      WeekDayLong0    =   "Sunday"
      WeekDayShort0   =   "Sun"
      WeekDayLetter0  =   "S"
      WeekDayLong1    =   "Monday"
      WeekDayShort1   =   "Mon"
      WeekDayLetter1  =   "M"
      WeekDayLong2    =   "Tuesday"
      WeekDayShort2   =   "Tue"
      WeekDayLetter2  =   "T"
      WeekDayLong3    =   "Wednesday"
      WeekDayShort3   =   "Wed"
      WeekDayLetter3  =   "W"
      WeekDayLong4    =   "Thursday"
      WeekDayShort4   =   "Thu"
      WeekDayLetter4  =   "T"
      WeekDayLong5    =   "Friday"
      WeekDayShort5   =   "Fri"
      WeekDayLetter5  =   "F"
      WeekDayLong6    =   "Saturday"
      WeekDayShort6   =   "Sat"
      WeekDayLetter6  =   "S"
      QuarterNum0     =   "1st"
      QuarterNum1     =   "2nd"
      QuarterNum2     =   "3rd"
      QuarterNum3     =   "4th"
      TimeUnitString0 =   "Year"
      TimeUnitString1 =   "Quarter"
      TimeUnitString2 =   "Month"
      TimeUnitString3 =   "Week"
      TimeUnitString4 =   "Day"
      TimeUnitString5 =   "Hour"
      TimeUnitString6 =   "Minute"
      TimeUnitString7 =   "Second"
      FirstWeek       =   2
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos Recuros"
      ForeColor       =   &H00C00000&
      Height          =   2100
      Left            =   3480
      TabIndex        =   8
      Top             =   0
      Width           =   7455
      Begin VB.CheckBox chkPlan 
         Caption         =   "Planificable"
         Height          =   240
         Left            =   5280
         TabIndex        =   5
         Tag             =   "Indicador Planificable"
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox txtCodRecurso 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         DataField       =   "AG11CODRECURSO"
         Height          =   330
         HelpContextID   =   30101
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   0
         TabStop         =   0   'False
         Tag             =   "C�digo Recurso"
         Top             =   960
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.TextBox txtDesRecurso 
         BackColor       =   &H00FFFF00&
         DataField       =   "AG11DESRECURSO"
         Height          =   300
         HelpContextID   =   30101
         Left            =   1080
         TabIndex        =   1
         Tag             =   "Descripci�n"
         Top             =   240
         Width           =   2295
      End
      Begin VB.Frame fraFrame2 
         Caption         =   "Persona Asociada"
         Height          =   660
         Left            =   120
         TabIndex        =   11
         Top             =   1320
         Width           =   6375
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "Buscar"
            Height          =   255
            Left            =   5400
            TabIndex        =   26
            Top             =   240
            Width           =   855
         End
         Begin VB.TextBox txtNombre 
            BackColor       =   &H00C0C0C0&
            Height          =   300
            HelpContextID   =   30101
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Nombre Persona"
            Top             =   240
            Width           =   3405
         End
         Begin VB.TextBox txtSG02COD 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "SG02COD"
            Height          =   300
            HelpContextID   =   30101
            Left            =   480
            MaxLength       =   4
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "C�digo Persona"
            Top             =   240
            Width           =   615
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            Height          =   195
            Index           =   11
            Left            =   1200
            TabIndex        =   14
            Top             =   285
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d"
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   13
            Top             =   285
            Width           =   285
         End
      End
      Begin VB.TextBox txtUnidades 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         DataField       =   "AG11NUMUNIDREC"
         Height          =   300
         HelpContextID   =   30104
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   3
         Tag             =   "N� Unidades"
         Top             =   570
         Width           =   495
      End
      Begin VB.CommandButton cmdNuevoR 
         Caption         =   "Nuevo"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6600
         TabIndex        =   10
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton cmdGuardarR 
         Caption         =   "Guardar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6600
         TabIndex        =   9
         Top             =   1560
         Width           =   735
      End
      Begin SSCalendarWidgets_A.SSDateCombo SSDateFecIni 
         Height          =   300
         Left            =   1080
         TabIndex        =   6
         Top             =   960
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   16776960
         ShowCentury     =   -1  'True
         Mask            =   2
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBTipoRecurso 
         Height          =   300
         Left            =   4080
         TabIndex        =   2
         Tag             =   "Tipo de Recurso"
         Top             =   240
         Width           =   2415
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AutoRestore     =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   5609
         Columns(1).Caption=   "Tipo Recurso"
         Columns(1).Name =   "Departamento"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4260
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   16776960
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCalendario 
         Height          =   300
         Left            =   2280
         TabIndex        =   4
         Tag             =   "Departamento"
         Top             =   570
         Width           =   2295
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AutoRestore     =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   5609
         Columns(1).Caption=   "Calendario"
         Columns(1).Name =   "Departamento"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4048
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSCalendarWidgets_A.SSDateCombo SSDateFecFin 
         Height          =   300
         Left            =   3720
         TabIndex        =   27
         Top             =   960
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   16777215
         ShowCentury     =   -1  'True
         Mask            =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         Height          =   195
         Index           =   4
         Left            =   3600
         TabIndex        =   20
         Top             =   360
         Width           =   315
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   840
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Uni."
         Height          =   195
         Index           =   8
         Left            =   600
         TabIndex        =   18
         Top             =   705
         Width           =   285
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Calen.:"
         Height          =   195
         Index           =   2
         Left            =   1680
         TabIndex        =   17
         Top             =   600
         Width           =   495
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha  Inicio"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   16
         Top             =   1050
         Width           =   915
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Fin"
         Height          =   195
         Index           =   6
         Left            =   2880
         TabIndex        =   15
         Top             =   1050
         Width           =   705
      End
   End
   Begin SSDataWidgets_B.SSDBCombo cboSSDBdpto 
      Height          =   330
      Left            =   600
      TabIndex        =   21
      Tag             =   "Departamento"
      Top             =   0
      Width           =   2775
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AutoRestore     =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1667
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   5609
      Columns(1).Caption=   "Departamento"
      Columns(1).Name =   "Departamento"
      Columns(1).DataField=   "Column 2"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4895
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBRecursos 
      Height          =   2250
      Left            =   0
      TabIndex        =   22
      Top             =   360
      Width           =   3405
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   6006
      _ExtentY        =   3969
      _StockProps     =   79
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDiaInicio 
      Height          =   300
      Left            =   4680
      TabIndex        =   29
      Top             =   2280
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   529
      _StockProps     =   93
      BackColor       =   16777215
      ShowCentury     =   -1  'True
      Mask            =   2
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde"
      Height          =   195
      Left            =   3600
      TabIndex        =   34
      Top             =   2280
      Width           =   960
   End
   Begin VB.Label label1 
      AutoSize        =   -1  'True
      Caption         =   "Dpto:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   480
   End
   Begin VB.Menu mnu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu mnuOption 
         Caption         =   "Mantenimiento"
         Index           =   10
      End
   End
End
Attribute VB_Name = "frmRecursos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim strDateBeg As String
Dim strDateEnd As String



Private Sub cmdAgenda_Click()
    Call objPipe.PipeSet("DPTO", cboSSDBdpto.Text)
    Call objPipe.PipeSet("COD_REC", txtCodRecurso.Text)
    Call objPipe.PipeSet("REC", txtDesRecurso.Text)
    Screen.MousePointer = vbHourglass
    Call objSecurity.LaunchProcess("AG1008")
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdBuscar_Click()
frmUsuarios.Show vbModal
txtSG02COD.SetFocus
If objPipe.PipeExist("AG0204_AG0201_SG02COD") Then
    txtSG02COD = objPipe.PipeGet("AG0204_AG0201_SG02COD")
    objPipe.PipeRemove ("AG0204_AG0201_SG02COD")
    txtNombre.SetFocus
End If
End Sub

Private Sub cmdCitasRecurso_Click()
Dim vntData(0 To 1)
If txtCodRecurso <> "" Then
    vntData(0) = cboSSDBdpto.Columns(0).Value
    vntData(1) = txtCodRecurso.Text
    Call objSecurity.LaunchProcess("CI0208", vntData)
Else
 Call objSecurity.LaunchProcess("CI0207")
End If
End Sub

Private Sub cmdImprimir_Click()
If Trim(txtCodRecurso) = "" Then
    MsgBox "Seleccione un recurso", vbInformation, Me.Caption
Else
    pImprimir
End If

End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub


Private Sub Form_Load()
'EMPEZAMOS EN EL DIA DE HOY
dtcDiaInicio.Date = Format(Now, "dd/mm/yyyy")
'Cargar combo de departamentos
    pCargarCombo
'FORMATEAR GRIDS
    pFormatearGrids
'FORMATEAR PERFILES
    pFormatearPerfiles


End Sub
Private Sub cboSSDBdpto_CloseUp()
Dim intR%
 If cmdGuardarR.Enabled = True Then
    intR = MsgBox("Desea Guardar los cambios?", vbYesNo)
    If intR = vbYes Then pGuardar
    cmdGuardarR.Enabled = False
 End If

SchedOCX1.TBClear

PLimpiarDatosRecurso
pCargarRecursos
cmdNuevoR.Enabled = True

End Sub
Private Sub chkPlan_Click()
    cmdGuardarR.Enabled = True
End Sub

Private Sub cmdGuardarR_Click()
pGuardar
cmdGuardarR.Enabled = False
cmdNuevoR.Enabled = True
End Sub

Private Sub cmdMantenimiento_Click()
Call objPipe.PipeSet("AG0201_AG0202_AG11CODRECURSO", txtCodRecurso.Text)
frmMantPerfiles.Show vbModal
pCargarPerfiles
End Sub



Private Sub cmdNuevoR_Click()
Dim intR As Integer

If cmdGuardarR.Enabled = True Then
   intR = MsgBox("Desea guardar los cambios?", vbYesNo, Me.Caption)
   If intR = vbYes Then pGuardar
End If
pNuevo
cmdGuardarR.Enabled = True
cmdNuevoR.Enabled = False
End Sub
Private Sub cboSSDBdpto_KeyUp(KeyCode As Integer, Shift As Integer)
    pCargarRecursos
    PLimpiarDatosRecurso
    cmdNuevoR.Enabled = True
End Sub

Private Sub mnuOption_Click(Index As Integer)
Call objPipe.PipeSet("AG0201_AG0202_AG11CODRECURSO", txtCodRecurso.Text)
    frmMantPerfiles.Show vbModal
End Sub

Private Sub SchedOCX1_BarDoubleClick(TimeBlock As Integer)
'cargar franjas
Call pCargarFranjas(SchedOCX1.ResUser2(SchedOCX1.TBResource(TimeBlock)))
End Sub

Private Sub SchedOCX1_BarRightSingleClick(TimeBlock As Integer)
'menu de mantenimiento
Call objPipe.PipeSet("AG0201_AG0202_AG07CODPERFIL", SchedOCX1.ResUser2(SchedOCX1.TBResource(TimeBlock)))
    PopupMenu mnu
End Sub



Private Sub SchedOCX1_CellRightSingleClick(Row As Integer, col As Integer)
Call objPipe.PipeSet("AG0201_AG0202_AG07CODPERFIL", SchedOCX1.ResUser2(Row))
    PopupMenu mnu
End Sub

Private Sub SchedOCX1_TBChangeBegin(TimeBlock As Integer, ChangeType As Integer, Abort As Integer)
    strDateBeg = SchedOCX1.TBBeg(TimeBlock)
    strDateEnd = SchedOCX1.TBEnd(TimeBlock)

End Sub




Private Sub SchedOCX1_TBChanged(TimeBlock As Integer)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim i As Integer
Dim strMotivo As String

On Error GoTo Canceltrans
If CDate(Format$(strDateEnd, "dd/mm/yyyy")) <> CDate(Format$(SchedOCX1.TBEnd(TimeBlock), "dd/mm/yyyy")) _
Or CDate(Format$(strDateBeg, "dd/mm/yyyy")) <> CDate(Format$(SchedOCX1.TBBeg(TimeBlock), "dd/mm/yyyy")) Then 'si hemos cambiado la fecha
    'comprobamos que no solape ningun periodo de ese perfil
    sql = "SELECT COUNT(*) FROM AG0900 WHERE "
    sql = sql & " AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
    sql = sql & " AND AG09NUMPVIGPER <> ? "
    sql = sql & " AND AG09FECINVIPER < TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " AND AG09FECFIVIPER > TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = txtCodRecurso.Text
        qry(1) = SchedOCX1.ResUser2(SchedOCX1.TBResource(TimeBlock))
        qry(2) = SchedOCX1.TBUser1(TimeBlock)
        qry(3) = Format$(SchedOCX1.TBEnd(TimeBlock), "dd/mm/yyyy")
        qry(4) = Format$(SchedOCX1.TBBeg(TimeBlock), "dd/mm/yyyy")
    Set rs = qry.OpenResultset()
    If rs(0) > 0 Then
        MsgBox "Se esta solapando con otro periode de vigencia"
        SchedOCX1.TBBeg(TimeBlock) = strDateBeg
        SchedOCX1.TBEnd(TimeBlock) = strDateEnd
        rs.Close
 
    Else
        rs.Close
        i = MsgBox("El periodo de Vigencia queda con Fecha de inicio: " & _
        Format$(SchedOCX1.TBBeg(TimeBlock), "dd/mm/yyyy") & Chr$(13) & _
        "y fecha fin: " & Format$(SchedOCX1.TBEnd(TimeBlock), "dd/mm/yyyy") & _
        Chr$(13) & " Desea Continuar?", vbYesNo, Me.Caption)
        If i = vbYes Then
            Screen.MousePointer = vbHourglass
            objApp.BeginTrans
            'SI DISMINUIMOS TANTO LA FECHA INICIO COMO LA DE FIN. SACAMOS A PENDIENTE Y PUNTO.
            If CDate(Format(strDateBeg, "DD/MM/YYYY")) < CDate(Format(SchedOCX1.TBBeg(TimeBlock), "dd/mm/yyyy")) And _
            CDate(Format(strDateEnd, "DD/MM/YYYY")) > CDate(Format(SchedOCX1.TBEnd(TimeBlock), "dd/mm/yyyy")) Then
                If Not blnSacarPendientes(Format(strDateBeg, "DD/MM/YYYY"), Format$(SchedOCX1.TBBeg(TimeBlock), "dd/mm/yyyy"), CLng(txtCodRecurso.Text)) _
                Then
                    SchedOCX1.TBBeg(TimeBlock) = strDateBeg
                    SchedOCX1.TBEnd(TimeBlock) = strDateEnd
                    Screen.MousePointer = vbDefault
                    Exit Sub
                End If
                If Not blnSacarPendientes(Format$(SchedOCX1.TBEnd(TimeBlock), "dd/mm/yyyy"), Format(strDateEnd, "DD/MM/YYYY"), CLng(txtCodRecurso.Text)) _
                Then
                    SchedOCX1.TBBeg(TimeBlock) = strDateBeg
                    SchedOCX1.TBEnd(TimeBlock) = strDateEnd
                    Screen.MousePointer = vbDefault
                    Exit Sub
                End If
            Else
                If Not fblnComprobaciones(CLng(txtCodRecurso.Text), SchedOCX1.ResUser2(SchedOCX1.TBResource(TimeBlock)), _
                SchedOCX1.TBUser1(TimeBlock), Format$(SchedOCX1.TBBeg(TimeBlock), "dd/mm/yyyy"), _
                Format$(SchedOCX1.TBEnd(TimeBlock), "dd/mm/yyyy"), "Cambio de Periodo", 1) Then
                    SchedOCX1.TBBeg(TimeBlock) = strDateBeg
                    SchedOCX1.TBEnd(TimeBlock) = strDateEnd
                    Screen.MousePointer = vbDefault
                    Exit Sub
                End If
            End If
            sql = "SELECT AG09MOTIVOPERF FROM AG0900 WHERE AG11CODRECURSO = ? "
            sql = sql & " AND AG07CODPERFIL = ? "
            sql = sql & " AND AG09NUMPVIGPER = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = txtCodRecurso.Text
                qry(1) = SchedOCX1.ResUser2(SchedOCX1.TBResource(TimeBlock))
                qry(2) = SchedOCX1.TBUser1(TimeBlock)
            Set rs = qry.OpenResultset()
            If Not IsNull(rs.EOF) Then
                sql = "Motivo Anterior: " & rs(0) & Chr$(13)

            Else
                sql = "No hay motivo anterior" & Chr$(13)
            End If
            sql = sql & "Introduzca el motivo del cambio "
            strMotivo = Trim$(InputBox(sql, "Mod. Pefil"))
            If strMotivo = "" Then strMotivo = rs(0)
    

            sql = "UPDATE AG0900 SET AG09FECINVIPER = TO_DATE(?,'DD/MM/YYYY'), "
            sql = sql & " AG09FECFIVIPER = TO_DATE(?,'DD/MM/YYYY'), AG09MOTIVOPERF = ?"
            sql = sql & " WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? "
            sql = sql & " AND AG09NUMPVIGPER = ? "
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = Format$(SchedOCX1.TBBeg(TimeBlock), "dd/mm/yyyy")
                qry(1) = Format$(SchedOCX1.TBEnd(TimeBlock), "dd/mm/yyyy")
                qry(2) = strMotivo
                qry(3) = txtCodRecurso.Text
                qry(4) = SchedOCX1.ResUser2(SchedOCX1.TBResource(TimeBlock))
                qry(5) = SchedOCX1.TBUser1(TimeBlock)
            qry.Execute
            If qry.RowsAffected = 0 Then
                MsgBox "No se ha modificado el Peridiodo ", vbInformation, Me.Caption
            End If
            qry.Close
            objApp.CommitTrans
            pCargarPerfiles
            Screen.MousePointer = vbDefault
        Else
            SchedOCX1.TBBeg(TimeBlock) = strDateBeg
            SchedOCX1.TBEnd(TimeBlock) = strDateEnd
        End If 'si queremos o no cambiar
    End If 'si se solapa o no
End If 'Hemos cambiado la fecha
Exit Sub
Canceltrans:
objApp.RollBackTrans
MsgBox "No se ha modificado el Peridiodo ", vbInformation, Me.Caption
SchedOCX1.TBBeg(TimeBlock) = strDateBeg
SchedOCX1.TBEnd(TimeBlock) = strDateEnd
End Sub





Private Sub SSDateFecFin_Change()
    cmdGuardarR.Enabled = True
End Sub

Private Sub SSDateFecFin_CloseUp()
cmdGuardarR.Enabled = True
End Sub

Private Sub SSDateFecIni_Change()
cmdGuardarR.Enabled = True
End Sub

Private Sub SSDateFecIni_CloseUp()
cmdGuardarR.Enabled = True
End Sub

Private Sub SSDBCalendario_Change()
    cmdGuardarR.Enabled = True
End Sub


Private Sub SSDBCalendario_CloseUp()
cmdGuardarR.Enabled = True
End Sub

Private Sub SSDBRecursos_Click()
Dim intR As Integer
 If cmdGuardarR.Enabled = True Then
    intR = MsgBox("Desea Guardar los cambios?", vbYesNo)
    If intR = vbYes Then pGuardar
    cmdGuardarR.Enabled = False
    cmdNuevoR.Enabled = True
 End If
 If SSDBRecursos.SelBookmarks.Count > 0 Then
    'rellenamos los datos del recursos
    pCargarDatosRecurso (SSDBRecursos.Columns(0).CellValue(SSDBRecursos.SelBookmarks(1)))
    cmdGuardarR.Enabled = False
    pCargarPerfiles
 End If
 
End Sub

Private Sub cmdVer_Click()

If txtCodRecurso.Text = "" Then
    MsgBox "Seleccione un recurso", vbCritical
Else
    pCargarPerfiles
    pFormatearPerfiles
End If

End Sub
Private Sub SSDBTipoRecurso_Change()
    cmdGuardarR.Enabled = True
End Sub


Private Sub SSDBTipoRecurso_CloseUp()
cmdGuardarR.Enabled = True
End Sub

Private Sub txtDesRecurso_Change()
    cmdGuardarR.Enabled = True
End Sub

Private Sub txtSG02COD_Change()
    cmdGuardarR.Enabled = True
End Sub

Private Sub txtSG02COD_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    If Trim(txtSG02COD) <> "" Then pCargarusuario (txtSG02COD)
End If
End Sub

Private Sub txtSG02COD_LostFocus()
If txtSG02COD.Text <> "" Then pCargarusuario (txtSG02COD.Text)
End Sub

Private Sub txtUnidades_Change()
    cmdGuardarR.Enabled = True
End Sub
Private Sub txtUnidades_KeyPress(KeyAscii As Integer)
If KeyAscii < 48 Or KeyAscii > 57 Then
    txtUnidades.Text = ""
    KeyAscii = 8
End If
End Sub
Private Sub pCargarCombo()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT AD0200.AD02CODDPTO, AD02DESDPTO FROM AD0200, AD0300"
sql = sql & " WHERE AD0200.AD02CODDPTO = AD0300.AD02CODDPTO"
sql = sql & " AND AD0300.SG02COD = ?"
sql = sql & " ORDER BY AD02DESDPTO"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objSecurity.strUser
Set rs = qry.OpenResultset()
While Not rs.EOF
    cboSSDBdpto.AddItem rs(0) & Chr$(9) & rs(1)
    rs.MoveNext
Wend
rs.Close
qry.Close

'combo de tipos de recurso
sql = "SELECT AG14CODTIPRECU, AG14DESTIPRECU FROM AG1400 WHERE AG14FECFIVGTR IS NULL"
sql = sql & " ORDER BY AG14DESTIPRECU"
Set rs = objApp.rdoConnect.OpenResultset(sql)
While Not rs.EOF
    SSDBTipoRecurso.AddItem rs(0) & Chr$(9) & rs(1)
    rs.MoveNext
Wend
rs.Close

'CALENDARIO
sql = "SELECT  AG02CODCALENDA, AG02DESCALENDA FROM AG0200 WHERE AG02FECBAJA IS NULL"
Set rs = objApp.rdoConnect.OpenResultset(sql)
While Not rs.EOF
    SSDBCalendario.AddItem rs(0) & Chr$(9) & rs(1)
    rs.MoveNext
Wend
rs.Close

End Sub

Private Sub pFormatearGrids()
With SSDBRecursos
    .Columns(0).Name = "C�d Recurso"
    .Columns(0).Visible = False
    .Columns(1).Caption = "Recurso"
    .Columns(1).Width = 3405
    .CaptionAlignment = ssCaptionAlignmentCenter
    .BackColorEven = objApp.objUserColor.lngReadOnly
    .BackColorOdd = objApp.objUserColor.lngReadOnly
End With
End Sub
Private Sub pCargarRecursos()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

SSDBRecursos.RemoveAll
sql = "SELECT AG11CODRECURSO, AG11DESRECURSO "
sql = sql & " FROM AG1100 WHERE AD02CODDPTO = ?"
sql = sql & " ORDER BY AG11DESRECURSO"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = cboSSDBdpto.Columns(0).Value
Set rs = qry.OpenResultset()
While Not rs.EOF
    SSDBRecursos.AddItem rs(0) & Chr$(9) & rs(1)
    rs.MoveNext
Wend
End Sub
Private Sub pCargarDatosRecurso(intCodRec As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim rs1 As rdoResultset
Dim qry As rdoQuery
Dim strCodTipRec As String
Dim strCodCalen As String
Dim blnBaja As Boolean

sql = "SELECT AG11CODRECURSO,AG1100.SG02COD, AG02CODCALENDA,"
sql = sql & " AD02CODDPTO,AG14CODTIPRECU,AG11DESRECURSO,"
sql = sql & " AG11INDPLANIFI,AG11NUMUNIDREC,AG11MODASIGCITA,"
sql = sql & " AG11FECINIVREC,AG11FECFINVREC,"
sql = sql & " NVL(SG02TXTFIRMA,SG02APE1||' '||SG02APE2||', '||SG02NOM) NOMBRE"
sql = sql & " FROM AG1100, SG0200"
sql = sql & " WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG1100.SG02COD = SG0200.SG02COD(+)"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intCodRec
Set rs = qry.OpenResultset
If Not rs.EOF Then
    'Rellenamos las cajas de texto
    txtCodRecurso = rs!AG11CODRECURSO
    txtDesRecurso = rs!AG11DESRECURSO
    txtUnidades = rs!AG11NUMUNIDREC
    If Not IsNull(rs!SG02COD) Then
        txtSG02COD = rs!SG02COD
        txtNombre = rs!NOMBRE
    Else
        txtSG02COD = ""
        txtNombre = ""
    End If
    'rellenamos las fechas
    SSDateFecIni.Date = Format$(rs!AG11FECINIVREC, "dd/mm/yyyy")
    If Not IsNull(rs!AG11FECFINVREC) Then _
    SSDateFecFin.Date = Format$(rs!AG11FECFINVREC, "dd/mm/yyyy") _
    Else: SSDateFecFin.Text = ""
    
    

    'rellenamos los combos
    SSDBTipoRecurso.MoveFirst
    Do While Trim(SSDBTipoRecurso.Columns(0).Value) <> Trim(rs!AG14CODTIPRECU)
        strCodTipRec = Trim(SSDBTipoRecurso.Columns(0).Value)
        SSDBTipoRecurso.MoveNext
        If strCodTipRec = Trim(SSDBTipoRecurso.Columns(0).Value) Then
            sql = "SELECT AG14DESTIPRECU FROM AG1400 WHERE AG14CODTIPRECU = ?"
            sql = sql & " ORDER BY AG14DESTIPRECU"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = rs!AG14CODTIPRECU
            Set rs1 = qry.OpenResultset()
            SSDBTipoRecurso.AddItem rs!AG14CODTIPRECU & Chr$(9) & rs1!AG14DESTIPRECU
            SSDBTipoRecurso.Text = rs1!AG14DESTIPRECU
            blnBaja = True
            rs1.Close
            Exit Do
        End If
    Loop
    If Not blnBaja Then SSDBTipoRecurso.Text = SSDBTipoRecurso.Columns(1).Value
    blnBaja = False
  
    'los calendarios
    SSDBCalendario.MoveFirst
    Do While Trim(SSDBCalendario.Columns(0).Value) <> Trim(rs!AG02CODCALENDA)
        strCodCalen = Trim(SSDBCalendario.Columns(0).Value)
        SSDBCalendario.MoveNext
        If strCodCalen = Trim(SSDBCalendario.Columns(0).Value) Then
            sql = "SELECT AG02DESCALENDA FROM AG0200 WHERE AG02CODCALENDA = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = rs!AG02CODCALENDA
            Set rs1 = qry.OpenResultset()
            SSDBCalendario.AddItem rs!AG02CODCALENDA & Chr$(9) & rs1!AG02DESCALENDA
            SSDBCalendario.Text = rs1!AG02DESCALENDA
            blnBaja = True
            rs1.Close
            Exit Do
        End If
    Loop
    If Not blnBaja Then SSDBCalendario.Text = SSDBCalendario.Columns(1).Value
    'El indicador de planificable
    If rs!AG11INDPLANIFI = 0 Then chkPlan.Value = 0 Else chkPlan.Value = 1
    rs.Close

End If
End Sub

Private Sub pCargarPerfiles()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim rs1 As rdoResultset
Dim qry1 As rdoQuery
Dim intPref As Integer
Dim intPeriodos As Integer
Dim iResult As Integer
Dim intPrior As Integer
Dim intColor As Integer

SchedOCX1.ResClear
SchedOCX1.TBClear

SchedOCX1.TSDateTimeBeg(0) = Format(dtcDiaInicio.Text, "dd/mm/yyyy")
SchedOCX1.TSDateTimeEnd(0) = Format(DateAdd("yyyy", 1, dtcDiaInicio.Date))


sql = "SELECT  AG07CODPERFIL , AG07DESPERFIL,AG07PRIORIDAD FROM AG0700 WHERE AG11CODRECURSO = ?"
sql = sql & " order by AG07PRIORIDAD DESC"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtCodRecurso
Set rs = qry.OpenResultset()
intPref = 0
intPeriodos = 0
Do While Not rs.EOF 'recorremos los perfiles en orden de prioridad.
    'buscamos el color
    sql = "SELECT COUNT(*) FROM AG0400 WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ?"
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = txtCodRecurso.Text
        qry1(1) = rs!AG07CODPERFIL
    Set rs1 = qry1.OpenResultset()
    If rs1(0) > 0 Then
        intColor = SCHED_COLOR_BLUE
    Else
        If InStr(1, rs!AG07DESPERFIL, "VACACIONES", vbTextCompare) <> 0 Then intColor = SCHED_COLOR_GREEN Else _
            intColor = SCHED_COLOR_YELLOW
    End If
    rs1.Close
    qry1.Close
    'buscamos los periodos de vigencia
    If IsNull(rs!AG07PRIORIDAD) Then intPrior = 0 Else intPrior = Val(rs!AG07PRIORIDAD)
    SchedOCX1.ResName(intPref) = rs!AG07DESPERFIL
    SchedOCX1.ResUser1(intPref) = intPrior
    SchedOCX1.ResUser2(intPref) = rs!AG07CODPERFIL
    sql = "SELECT AG09FECINVIPER, AG09FECFIVIPER, AG09NUMPVIGPER FROM AG0900 "
    sql = sql & " WHERE AG11CODRECURSO = ? "
    sql = sql & " AND AG07CODPERFIL = ?"
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = txtCodRecurso.Text
        qry1(1) = rs!AG07CODPERFIL
    Set rs1 = qry1.OpenResultset()
    Do While Not rs1.EOF
        iResult = SchedOCX1.AddTimeBlock(intPeriodos, intPref, Format$(rs1(0), "DD/MM/YYYY"), Format$(rs1(1), "DD/MM/YYYY"), "", intColor, -1, rs1!AG09NUMPVIGPER, "", "", "")
        intPeriodos = intPeriodos + 1
        rs1.MoveNext
    Loop
    intPref = intPref + 1
    rs.MoveNext
Loop

End Sub


Private Sub pFormatearPerfiles()
Dim i As Integer

SchedOCX1.TSBackColor(0) = &HC0C0C0
SchedOCX1.TSMajorFontColor(0) = &H0&
SchedOCX1.TSMinorFontColor(0) = &H0&
SchedOCX1.TSRulerColor(0) = &H0&
SchedOCX1.TSRulerLarge(0) = 0
SchedOCX1.TSBevel(0) = 0
SchedOCX1.TSMinorFontSizePercent(0) = 60
SchedOCX1.TSMajorFormat(0) = 1


SchedOCX1.BarMoveIncrement = "1 Day"
SchedOCX1.ResBevel = Bevel_None
SchedOCX1.ResGridThickness = 1
SchedOCX1.LegendBevel = Bevel_None
SchedOCX1.ResHdrBevel = Bevel_None
SchedOCX1.GridMajorThickness = 1
SchedOCX1.LegendLineHeight = 1
SchedOCX1.ResHdrGridThickness = 1
    ' turn off bar text for weekly view
    For i = 0 To SchedOCX1.BarsMax - 1
        SchedOCX1.BarBegSymbol(i) = SCHED_SYM_NONE
        SchedOCX1.BarEndSymbol(i) = SCHED_SYM_NONE
    Next i

End Sub
Private Sub pGuardar()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim lngCodRec As Long
Dim blnObligatorios As Boolean
'On Error Resume Next
'Comprobamos los campos obligatorios.
blnObligatorios = True
If Trim(txtDesRecurso.Text) = "" Then blnObligatorios = False
If Trim(txtUnidades.Text) = "" Then blnObligatorios = False
If Trim(SSDBTipoRecurso.Text) = "" Then blnObligatorios = False
If Trim(SSDateFecIni.Date) = "" Then blnObligatorios = False

If blnObligatorios Then

    If Trim(txtCodRecurso.Text) <> "" Then 'CASO DE QUE YA EXISTA EL RECURSO
        sql = "UPDATE AG1100 SET AG11DESRECURSO = ?, SG02COD = ?, AG02CODCALENDA = ?, "
        sql = sql & "AG14CODTIPRECU = ?,  AG11INDPLANIFI = ?, AG11NUMUNIDREC = ?, "
        sql = sql & "AG11FECINIVREC = TO_DATE(?,'DD/MM/YYYY'),"
        sql = sql & "AG11FECFINVREC = TO_DATE(?,'DD/MM/YYYY')"
        sql = sql & " WHERE AG11CODRECURSO = ? "
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = txtDesRecurso.Text
            If Trim(txtSG02COD.Text) <> "" Then qry(1) = txtSG02COD.Text Else qry(1) = Null
            If Trim(SSDBCalendario.Text) <> "" Then qry(2) = SSDBCalendario.Columns(0).Value Else qry(2) = Null
            qry(3) = SSDBTipoRecurso.Columns(0).Value
            If chkPlan.Value = 0 Then qry(4) = 0 Else qry(4) = -1
            qry(5) = txtUnidades.Text
            qry(6) = Format$(SSDateFecIni.Date, "dd/mm/yyyy")
            If Trim(SSDateFecFin.Date) <> "" Then qry(7) = Format$(SSDateFecFin.Date, "dd/mm/yyyy") Else qry(7) = Null
            qry(8) = txtCodRecurso.Text
        qry.Execute
        If Err > 0 Then
            MsgBox "Error Grabando los registros.", vbCritical
        End If
    Else 'RECURSO NUEVO
        'BUSCAMOS LA CLAVE DEL RECURSO
        sql = "SELECT MAX(AG11CODRECURSO) FROM AG1100"
        Set rs = objApp.rdoConnect.OpenResultset(sql)
        If Not IsNull(rs(0)) Then lngCodRec = Val(rs(0)) + 1 Else lngCodRec = 1
        rs.Close
        txtCodRecurso = Str(lngCodRec)
        sql = "INSERT INTO AG1100 (AG11CODRECURSO,SG02COD, AG02CODCALENDA,"
        sql = sql & " AD02CODDPTO,AG14CODTIPRECU,AG11DESRECURSO,"
        sql = sql & " AG11INDPLANIFI,AG11NUMUNIDREC,AG11MODASIGCITA,"
        sql = sql & " AG11FECINIVREC,AG11FECFINVREC) VALUES (?,?,?,?,?,?,?,?,0,"
        sql = sql & " TO_DATE(?,'DD/MM/YYYY'), TO_DATE(?,'DD/MM/YYYY'))"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCodRec
            If Trim(txtSG02COD) <> "" Then qry(1) = txtSG02COD Else qry(1) = Null
            qry(2) = SSDBCalendario.Columns(0).Value
            qry(3) = cboSSDBdpto.Columns(0).Value
            qry(4) = SSDBTipoRecurso.Columns(0).Value
            qry(5) = txtDesRecurso
            If chkPlan.Value = 0 Then qry(6) = 0 Else qry(6) = -1
            qry(7) = txtUnidades
            qry(8) = Format$(SSDateFecIni.Date, "dd/mm/yyyy")
            If Trim(SSDateFecFin.Date) <> "" Then qry(9) = Format$(SSDateFecFin.Date, "dd/mm/yyyy") Else _
            qry(9) = Null
            qry.Execute
    End If
Else ' no se han rellenado los campos obligatorios
    MsgBox "Rellene los campos obligatorios", vbCritical
    Exit Sub
End If
End Sub


Private Sub pNuevo()
'limpiar los controles
'texto
txtCodRecurso = ""
txtDesRecurso = ""
txtNombre = ""
txtUnidades = ""
txtSG02COD = ""
'combos
SSDBCalendario.MoveFirst
SSDBCalendario.Text = ""

SSDBTipoRecurso.MoveFirst
SSDBTipoRecurso.Text = ""

'Fechas

SSDateFecIni.Text = Format$(Now, "dd/mm/yyyy")
SSDateFecFin.Text = ""

txtDesRecurso.SetFocus

End Sub


Private Sub pCargarusuario(strCodUsu As String)
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

sql = "SELECT NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM)"
sql = sql & " FROM SG0200 WHERE SG02COD = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Trim(txtSG02COD.Text)
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    txtNombre = rs(0)
    rs.Close
Else
    rs.Close
    MsgBox "El usuario " & txtSG02COD.Text & "no existe", vbCritical, Me.Caption
End If

End Sub

Private Sub pLimparRecurso()
txtNombre = ""
txtDesRecurso = ""
txtCodRecurso = ""
txtUnidades = ""
SSDBCalendario.Text = ""
SSDBTipoRecurso.Text = ""
txtSG02COD = ""
SSDateFecIni.Date = ""
SSDateFecFin.Date = ""
End Sub

Sub PLimpiarDatosRecurso()
txtDesRecurso.Text = ""
txtCodRecurso.Text = ""
'SSDBTipoRecurso.RemoveAll
SSDBTipoRecurso.Text = ""
'SSDBCalendario.RemoveAll
SSDBCalendario.Text = ""
txtUnidades.Text = ""
txtSG02COD.Text = ""
txtNombre.Text = ""
SSDateFecIni.Text = ""
SSDateFecFin.Text = ""
cmdGuardarR.Enabled = False

End Sub

Private Sub pImprimir()
Dim strWherePerfiles As String

 strWherePerfiles = "(IsNull({AG1400.AG14FECFIVGTR}) OR {AG1400.AG14FECFIVGTR}>=CurrentDate) AND " & _
  "(IsNull({AG1100.AG11FECFINVREC}) OR {AG1100.AG11FECFINVREC}>=CurrentDate) " & _
  "AND {AD0200.AD02CODDPTO}= " & cboSSDBdpto.Columns(0).Value & _
  " AND {AG1100.AG11CODRECURSO}=" & txtCodRecurso.Text
  CrystalReport1.ReportFileName = objApp.strReportsPath & "Perfiles.rpt"
  With CrystalReport1
        .PrinterCopies = 1
        .Destination = crptToPrinter
        .SelectionFormula = objGen.ReplaceStr(strWherePerfiles, "#", Chr(34), 0)
        .Destination = crptToPrinter
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Me.MousePointer = vbHourglass
        .Action = 1
        Me.MousePointer = vbDefault
    End With
End Sub
