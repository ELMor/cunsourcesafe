Attribute VB_Name = "General"
Option Explicit

Public objApp As clsCWApp
Public objGen As clsCWGen
Public vntPersonaAviso As Variant
Public strHistoria As String
Public strDni As String
Public strNombre As String
Public strApellido1 As String
Public strApellido2 As String
Public lngcolorreadonly As Long
Public CodPersona As Long
Public blnBus As Boolean

Public Function Nuevo_Codigo(strSql As String) As Long
 'Creaci�n de variables recordset
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  
 'Apertura del cursor
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

 'Establezco el n� de registros a recuperar a 1
  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
 'Devuelvo el nuevo n� de c�digo
  If IsNull(rdoCodigo(0).Value) Then
    Nuevo_Codigo = 1
  Else
    Nuevo_Codigo = rdoCodigo(0) + 1
  End If
 
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

