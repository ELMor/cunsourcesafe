VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "cOMCTL32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "MSRDC20.OCX"
Begin VB.Form frmBusqueda 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "B�squeda de Personas"
   ClientHeight    =   8670
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   ScaleHeight     =   8670
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Height          =   375
      Left            =   10200
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   10560
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   34
      Top             =   0
      Width           =   375
   End
   Begin VB.ComboBox Combo3 
      Height          =   315
      Left            =   8760
      TabIndex        =   32
      Top             =   2760
      Width           =   2535
   End
   Begin VB.ComboBox Combo2 
      Height          =   315
      Left            =   8760
      TabIndex        =   31
      Top             =   1680
      Width           =   2535
   End
   Begin SSDataWidgets_B.SSDBGrid ssGrid 
      Bindings        =   "frmBusqueda.frx":0000
      Height          =   4995
      Left            =   0
      TabIndex        =   16
      Top             =   3240
      Width           =   11805
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowUpdate     =   0   'False
      SelectTypeRow   =   1
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20823
      _ExtentY        =   8811
      _StockProps     =   79
      Caption         =   "Resultado"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   9
      Left            =   8760
      TabIndex        =   5
      Top             =   600
      Width           =   2535
   End
   Begin VB.CheckBox Check1 
      Alignment       =   1  'Right Justify
      Caption         =   "B�squeda Fon�tica"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4380
      TabIndex        =   29
      Top             =   540
      Width           =   2655
   End
   Begin VB.ComboBox cboSearch 
      Height          =   315
      ItemData        =   "frmBusqueda.frx":0011
      Left            =   4500
      List            =   "frmBusqueda.frx":001B
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   27
      Top             =   1020
      Width           =   2430
   End
   Begin VB.Frame Frame1 
      Caption         =   "Sexo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   4860
      TabIndex        =   24
      Top             =   1440
      Width           =   1695
      Begin VB.OptionButton Option1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   26
         Top             =   180
         Width           =   1335
      End
      Begin VB.OptionButton Option2 
         Alignment       =   1  'Right Justify
         Caption         =   "Mujer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   180
         TabIndex        =   25
         Top             =   540
         Width           =   1335
      End
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   8
      Left            =   8460
      TabIndex        =   10
      Top             =   3540
      Visible         =   0   'False
      Width           =   2535
   End
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   22
      Top             =   8370
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   529
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Text            =   "Preparado"
            TextSave        =   "Preparado"
            Key             =   ""
            Object.Tag             =   "0"
         EndProperty
      EndProperty
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   7
      Left            =   8760
      TabIndex        =   9
      Top             =   2220
      Width           =   2535
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   6
      Left            =   7080
      TabIndex        =   8
      Top             =   1680
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   5
      Left            =   1200
      TabIndex        =   4
      Top             =   2760
      Width           =   1215
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   8460
      TabIndex        =   7
      Top             =   3540
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   4
      Left            =   8760
      TabIndex        =   6
      Top             =   1140
      Width           =   2535
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   3
      Left            =   1200
      TabIndex        =   3
      Top             =   2220
      Width           =   2535
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   2
      Left            =   1200
      TabIndex        =   2
      Top             =   1680
      Width           =   2535
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   1
      Left            =   1200
      TabIndex        =   1
      Top             =   1140
      Width           =   2535
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   0
      Left            =   1200
      TabIndex        =   0
      Top             =   600
      Width           =   2535
   End
   Begin VB.CommandButton cmdConsulta 
      Caption         =   "Consultar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   4920
      Picture         =   "frmBusqueda.frx":0051
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   2520
      Width           =   1575
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   330
      Left            =   2520
      Top             =   3060
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      _Version        =   327681
      Options         =   0
      CursorDriver    =   0
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox Check2 
      Alignment       =   1  'Right Justify
      Caption         =   "MAY�SCULAS/min�sculas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7680
      TabIndex        =   28
      Top             =   3300
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   2655
   End
   Begin MSRDC.MSRDC MSRDC2 
      Height          =   330
      Left            =   8040
      Top             =   3120
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      _Version        =   327681
      Options         =   0
      CursorDriver    =   0
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC2"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSRDC.MSRDC MSRDC3 
      Height          =   330
      Left            =   5520
      Top             =   3360
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      _Version        =   327681
      Options         =   0
      CursorDriver    =   0
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC2"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label12 
      Caption         =   "Pa�s"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8160
      TabIndex        =   33
      Top             =   2760
      Width           =   495
   End
   Begin VB.Label Label11 
      Caption         =   "N�Historia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7740
      TabIndex        =   30
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label10 
      Caption         =   "Tel�fono"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7440
      TabIndex        =   23
      Top             =   3600
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label9 
      Caption         =   "Localidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7860
      TabIndex        =   21
      Top             =   2220
      Width           =   855
   End
   Begin VB.Label Label8 
      Caption         =   "Provincia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7860
      TabIndex        =   20
      Top             =   1680
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "Edad Aprox ."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   60
      TabIndex        =   19
      Top             =   2760
      Width           =   1095
   End
   Begin VB.Label Label6 
      Caption         =   "N�Seg.Social"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7500
      TabIndex        =   18
      Top             =   1140
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Profesi�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7500
      TabIndex        =   17
      Top             =   3600
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label5 
      Caption         =   "D.N.I."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   660
      TabIndex        =   14
      Top             =   2220
      Width           =   495
   End
   Begin VB.Label Label4 
      Caption         =   "2� Apellido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   180
      TabIndex        =   13
      Top             =   1680
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "1�Apellido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   300
      TabIndex        =   12
      Top             =   1140
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Nombre: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   420
      TabIndex        =   11
      Top             =   600
      Width           =   735
   End
End
Attribute VB_Name = "frmBusqueda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public rdoCursor  As rdoResultset
Public IndiceProf As Integer

Private Sub cmdConsulta_Click()
  
  Dim Sexo          As Integer
  Dim strSql        As String
  Dim strWhere      As String
  Dim i             As Integer
  Dim j             As Integer
  Dim strAND        As String
  Dim blnUno        As Boolean
  Dim strComprc(5)  As String
  Dim strField(4)   As String
  Dim strValue      As String
  Dim rdoConsulta   As rdoQuery
  Dim Parametro(10) As Variant
  Dim NumReg        As Long
  Dim Annus1        As String
  Dim Annus2        As String
  Dim Hoy           As Date
  Dim blnConEdad    As Boolean

  On Error GoTo cwIntError
  
  Call CambiaStatus

  Sexo = IIf(Option1 = True, 0, 1)
        
  If Check1.Value = 1 Then
    strField(0) = "CI22CODFONNOM"
    strField(1) = "CI22CODFONAP1"
    strField(2) = "CI22CODFONAP2"
    For i = 0 To 2
        strComprc(i) = "=?"
    Next i
    
    Select Case cboSearch.Text
      Case "Comienzo de campo"
        strComprc(5) = " LIKE CONCAT(?,'%')"
      Case "Cualquier parte del campo"
        strComprc(5) = " LIKE CONCAT(CONCAT('%',?),'%')"
      Case "Hacer coincidir todo el campo"
        strComprc(5) = "=?"
    End Select

  Else
    strField(0) = "CI22NOMBREALF"
    strField(1) = "CI22PRIAPELALF"
    strField(2) = "CI22SEGAPELALF"
    
    Select Case cboSearch.Text
      Case "Comienzo de campo"
        For i = 0 To 2
            strComprc(i) = " LIKE CONCAT(?,'%')"
        Next i
        'strComprc(5) = " LIKE CONCAT(?,'%')"
        strComprc(5) = "=?"
      Case "Cualquier parte del campo"
        For i = 0 To 2
            strComprc(i) = " LIKE CONCAT(CONCAT('%',?),'%')"
        Next i
        strComprc(5) = " LIKE CONCAT(CONCAT('%',?),'%')"
      Case "Hacer coincidir todo el campo"
        For i = 0 To 2
            strComprc(i) = "=?"
        Next i
        strComprc(5) = "=?"
    End Select
  
'    If Check2.Value = False Then
'        For i = 0 To 2
'            strField(i) = "UPPER(" & strField(i) & ")"
'        Next i
'    End If

  End If
  
  strAND = " AND "
  strSql = "SELECT CI21CODPERSONA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI22DNI,CI22NUMSEGSOC,CI22FECNACIM,CI22NUMHISTORIA"
  strWhere = " FROM CI2200 WHERE CI30CODSEXO=?"
  Parametro(j) = (Sexo + 1)
  j = j + 1
  
  strField(3) = "CI22DNI"
  strComprc(3) = "=?"
  strField(4) = "CI22NUMSEGSOC"
  strComprc(4) = "=?"
  
  For i = 0 To 4
    strValue = Text1(i).Text
    strValue = IIf(Check2.Value, strValue, UCase(strValue))
    
    If strValue <> "" And Check1.Value = 1 Then
        Select Case i
        Case 1
            strValue = Sons(strValue, 0, Sexo + 1)
        Case 2
            strValue = Sons(strValue, 0, Sexo + 1)
        Case 0
            strValue = Sons(strValue, 1, Sexo + 1)
        End Select
        strWhere = strWhere & strAND & strField(i) & strComprc(i)
        Parametro(j) = strValue
        j = j + 1
    ElseIf strValue <> "" Then
        strWhere = strWhere & strAND & strField(i) & strComprc(i)
        Parametro(j) = IIf(i > 2, strValue, Alf(strValue))
        j = j + 1
    End If
  
  Next i
  
'  If Combo1.Text <> "" Then
'    strWhere = strWhere & strAND & " CI25CODPROFESI=? "
'    Parametro(j) = Combo1.ListIndex + 1
'    j = j + 1
'  End If
  
  If Text1(5).Text <> "" Then
    Hoy = objGen.GetDBDateTime
    Annus1 = Year(DateAdd("yyyy", 0 - (Val(Text1(5).Text) - 5), Hoy))
    Annus2 = Year(DateAdd("yyyy", 0 - (Val(Text1(5).Text) + 5), Hoy))
    strWhere = strWhere & strAND & " CI22FECNACIM BETWEEN TO_DATE('0101" & Annus2 & "','ddmmyyyy') AND TO_DATE('3112" & Annus1 & "','ddmmyyyy')"
    blnConEdad = True
  End If
  
  If Combo2.ListIndex <> -1 Then
'    strWhere = strWhere & strAND & " ("
    Annus1 = Check(Combo2.Text, True, strComprc(5), Check2.Value, False)
'    If Annus1 = "mal" Then
'        MsgBox ("Demasiados registros cumplen las restricciones...")
'        Call CambiaStatus
'        Exit Sub
'    End If
'    If Annus1 <> "NADA" Then
'        strWhere = strWhere & "CI26CODPROVI IN " & (Annus1)
'    End If
    
    'strWhere = strWhere & " OR " & "CI22DESPROVI " & strComprc(5) & ")"
    strWhere = strWhere & " AND (CI21CODPERSONA,CI22NUMDIRPRINC) IN " _
    & "(SELECT CI21CODPERSONA,CI10NUMDIRECCI FROM CI1000 WHERE CI26CODPROVI " & strComprc(5) & ")"
    Parametro(j) = Annus1 'IIf(Check2.Value, Combo2.Text, UCase(Combo2.Text))
    j = j + 1
   
  End If
  
'  If Combo3.ListIndex <> -1 Then
  If Combo3.ListIndex <> -1 And Combo3.ListIndex <> 49 Then
'    strWhere = strWhere & strAND & " ("
    Annus1 = Check(Combo3.Text, True, strComprc(5), Check2.Value, True)
'    If Annus1 = "mal" Then
'        MsgBox ("Demasiados registros cumplen las restricciones...")
'        Call CambiaStatus
'        Exit Sub
'    End If
'    If Annus1 <> "NADA" Then
'        strWhere = strWhere & "CI26CODPROVI IN " & (Annus1)
'    End If
    
    'strWhere = strWhere & " OR " & "CI22DESPROVI " & strComprc(5) & ")"
    
    Rem comentarios /* */ de LAS 6.4.1999
'    strWhere = strWhere & " AND (CI21CODPERSONA /*,CI22NUMDIRPRINC*/) IN " _
'    & "(SELECT CI21CODPERSONA /*,CI10NUMDIRECCI*/ FROM CI1000 WHERE CI19CODPAIS " & strComprc(5) & ")"
    strWhere = strWhere & " AND (CI21CODPERSONA) IN " _
    & "(SELECT CI21CODPERSONA FROM CI1000 WHERE CI19CODPAIS " & strComprc(5) & ")"
    Parametro(j) = Annus1 'IIf(Check2.Value, Combo3.Text, UCase(Combo3.Text))
    j = j + 1
   
  End If
  
  If Text1(7).Text <> "" Then
    Annus2 = ""
    strWhere = strWhere & strAND & " ("
    Annus1 = Check(Text1(7).Text, False, strComprc(5), Check2.Value, False)
    If Annus1 = "mal" Then
        MsgBox ("Demasiados registros cumplen las restricciones...")
        Call CambiaStatus
        Exit Sub
    End If
    
    If Annus1 <> "NADA" Then
        strWhere = strWhere & "CI16CODLOCALID IN " & (Annus1)
        Annus2 = " OR "
    End If
    
    strWhere = strWhere & Annus2 & "CI22DESLOCALID " & strComprc(5) & ")"
    Parametro(j) = IIf(Check2.Value, strValue, UCase(strValue))
    j = j + 1
    
  End If
  
  If Text1(8).Text <> "" Then
    strWhere = strWhere & strAND & CheckTfno(Text1(8).Text)
  End If
  
  If Text1(9).Text <> "" Then
    strWhere = strWhere & strAND & " CI22NUMHISTORIA = ?"
    Parametro(j) = Text1(9)
    j = j + 1
  End If
  
  strWhere = strWhere & " AND ROWNUM<101"
  strSql = strSql & strWhere & " ORDER BY CI22PRIAPEL ASC"
  
  If j = 1 And blnConEdad Then
    MsgBox ("Por favor, indique m�s restricciones para la b�squeda")
    Call CambiaStatus
    Exit Sub
  End If
  
  NumReg = CompResult(strWhere, Parametro, j)
  If NumReg >= 100 Then
    MsgBox ("El n�mero de registros es excesivo (>100)" & Chr$(13) & "Por favor, indique m�s restricciones para la b�squeda")
    Call CambiaStatus
    Exit Sub
  End If
    
  Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
  For i = 0 To j - 1
    rdoConsulta.rdoParameters(i) = Parametro(i)
  Next i
  
  Set rdoCursor = rdoConsulta.OpenResultset(rdOpenStatic, rdConcurReadOnly)
  If rdoCursor.RowCount <> 0 Then
    rdoCursor.MoveFirst
  Else
    MsgBox ("No se ha encontrado ningun paciente con esas caracter�sticas" & Chr$(13) & "Por favor, repase las restricciones")
  End If
  Set MSRDC1.Resultset = rdoCursor
  ssGrid.Columns(0).Caption = "C�d. Persona"
  ssGrid.Columns(1).Caption = "Nombre"
  ssGrid.Columns(2).Caption = "1er Apellido"
  ssGrid.Columns(3).Caption = "2� Apellido"
  ssGrid.Columns(4).Caption = "DNI"
  ssGrid.Columns(5).Caption = "N�Seg.Social"
  ssGrid.Columns(6).Caption = "Fecha Nac."
  ssGrid.Columns(7).Caption = "Historia"
  
    ssGrid.Columns(0).Width = 800
    ssGrid.Columns(1).Width = 1800
    ssGrid.Columns(2).Width = 2050
    ssGrid.Columns(3).Width = 2050
    ssGrid.Columns(4).Width = 900
    ssGrid.Columns(5).Width = 1100
    ssGrid.Columns(6).Width = 1000
    ssGrid.Columns(7).Width = 800
    
  ssGrid.Caption = "Resultado: " & rdoCursor.RowCount & " registros"
  
  ssGrid.Refresh
  Call CambiaStatus
  

  
cwResError:
  Exit Sub
  
cwIntError:
  MsgBox ("Repase las restricciones...")
  Resume cwResError
  
End Sub



Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Command2_Click()
  CodPersona = ssGrid.Columns(0).Value
  
  rdoCursor.Close
  Set rdoCursor = Nothing
  Unload Me
  Set frmBusqueda = Nothing

End Sub

Private Sub Form_Load()
    Dim rdoCursor    As rdoResultset
    Dim rdoCursora    As rdoResultset
    Dim strSql       As String
    
    Set imgX = objApp.imlImageList.ListImages("i45")
    Command1.Picture = imgX.Picture
    Set imgX = objApp.imlImageList.ListImages("i46")
    Command2.Picture = imgX.Picture
    'Command2.Picture.Height = 350
  
    Command2.Enabled = False
'    strSql = "SELECT CI25CODPROFESI,CI25DESPROFESI FROM CI2500"
'    Set rdoCursora = objApp.rdoConnect.OpenResultset(strSql, rdOpenStatic, rdConcurReadOnly)
'    rdoCursora.MoveFirst

'    With rdoCursora
'    IndiceProf = .rdoColumns(0)
'    Do While .EOF = False
'        Call Combo1.AddItem(.rdoColumns(1), .rdoColumns(0) - IndiceProf)
'        .MoveNext
'    Loop
'    rdoCursora.Close
'
'    End With
    
'    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", "Select ci25codprofesi,ci25desprofesi from ci2500")
    'Set rdoCursor = objApp.rdoConnect.OpenResultset("Select ci26codprovi,ci26desprovi from ci2600", rdOpenStatic, rdConcurReadOnly)
    Set rdoCursor = objApp.rdoConnect.OpenResultset("Select ci26desprovi from ci2600 order by ci26desprovi", rdOpenStatic, rdConcurReadOnly)

    Set MSRDC2.Resultset = rdoCursor
'    Combo2.DataMode = ssDataModeAddItem
'    Combo2.DataMode = ssDataModeUnbound

    For i = 1 To MSRDC2.Resultset.RowCount
    Combo2.AddItem MSRDC2.Resultset.rdoColumns(0)
    MSRDC2.Resultset.MoveNext
    Next i
    'SSDBCombo1.Align = 0
    
    'SSDBCombo1.DataFieldList = "ci26desprovi"
    'SSDBCombo1.DataFieldToDisplay = "ci26desprovi"
    'SSDBCombo1.Columns(0).Width = 800
    'SSDBCombo1.Columns(0).Width = 2500
    'SSDBCombo1.Columns(0).Caption = "C�digo"
    'SSDBCombo1.Columns(0).Caption = "Descripci�n"
    'SSDBCombo1.Refresh
  
   Set rdoCursor = objApp.rdoConnect.OpenResultset("Select ci19despais from ci1900 order by ci19despais", rdOpenStatic, rdConcurReadOnly)

    Set MSRDC3.Resultset = rdoCursor
'    Combo2.DataMode = ssDataModeAddItem
'    Combo2.DataMode = ssDataModeUnbound

    For i = 1 To MSRDC3.Resultset.RowCount
    Combo3.AddItem MSRDC3.Resultset.rdoColumns(0)
    MSRDC3.Resultset.MoveNext
    Next i
    cboSearch.ListIndex = 0
    Option1.Value = 1
    Check1.Value = 1
    Set rdoCursora = Nothing
    
    CodPersona = 0
End Sub

Private Sub SSDBCombo1_Change()
If SSDBCombo1.Text <> "" Then
    Text1(6).Text = SSDBCombo1.Columns(0).Text
Else
    Text1(6).Text = ""
End If
End Sub

Private Sub SSDBCombo1_Click()
If SSDBCombo1.Text <> "" Then
    Text1(6).Text = SSDBCombo1.Columns(0).Text
Else
    Text1(6).Text = ""
End If
End Sub

Private Sub ssGrid_Click()
Command2.Enabled = True
End Sub

Private Sub ssGrid_DblClick()

  CodPersona = ssGrid.Columns(0).Value
  
  rdoCursor.Close
  Set rdoCursor = Nothing
  Unload Me
  Set frmBusqueda = Nothing

End Sub

Private Function Sons(Nombre As String, Nap As Integer, Sexo As Integer) As Integer

    Dim rdoSons     As rdoQuery
    Dim rdoCursor   As rdoResultset
    Dim strSql      As String
    
    strSql = "SELECT GCFN04(?,?,?) FROM DUAL"
    Set rdoSons = objApp.rdoConnect.CreateQuery("", strSql)
    rdoSons.rdoParameters(0) = Nombre
    rdoSons.rdoParameters(1) = Nap
    rdoSons.rdoParameters(2) = Sexo
    
    Set rdoCursor = rdoSons.OpenResultset(rdOpenStatic, rdConcurReadOnly)
    Sons = rdoCursor.rdoColumns(0)
    
    Call rdoCursor.Close
    Call rdoSons.Close
    
    Set rdoCursor = Nothing
    Set rdoSons = Nothing
    
End Function
Private Function Alf(Nombre As String) As String

    Dim rdoSons     As rdoQuery
    Dim rdoCursor   As rdoResultset
    Dim strSql      As String
    
    strSql = "SELECT GCFN05(?) FROM DUAL"
    Set rdoSons = objApp.rdoConnect.CreateQuery("", strSql)
    rdoSons.rdoParameters(0) = Nombre
    
    Set rdoCursor = rdoSons.OpenResultset(rdOpenStatic, rdConcurReadOnly)
    Alf = rdoCursor.rdoColumns(0)
    
    Call rdoCursor.Close
    Call rdoSons.Close
    
    Set rdoCursor = Nothing
    Set rdoSons = Nothing
    
End Function

Private Function CompResult(strWhere As String, Parametro() As Variant, j As Integer) As Long

    Dim rdoComp As rdoResultset
    Dim rdoCons As rdoQuery
    Dim i       As Integer
        
    Set rdoCons = objApp.rdoConnect.CreateQuery("", "SELECT COUNT(*) " & strWhere)
    For i = 0 To j - 1
        rdoCons.rdoParameters(i) = Parametro(i)
    Next i
    
    Set rdoComp = rdoCons.OpenResultset(rdOpenStatic, rdConcurReadOnly)
    CompResult = rdoComp.rdoColumns(0)
    
    Call rdoComp.Close
    Call rdoCons.Close
    
    Set rdoComp = Nothing
    Set rdoCons = Nothing

End Function

Private Function Check(strValue As String, Sino As Boolean, strCom As String, May As Boolean, maria As Boolean) As String

  Dim rdoConsulta As rdoResultset
  Dim rdoBeam     As rdoQuery
  Dim strSql      As String
  Dim strField(2) As String
  Dim i           As Integer
  Dim strComa     As String
  
  strField(0) = "CI26DESPROVI"
  strField(1) = "CI16DESLOCALID"
  strField(2) = "CI19despais"

  strValue = IIf(May, strValue, UCase(strValue))
 
  For i = 0 To 2
      strField(i) = IIf(May, strField(i), "UPPER(" & strField(i) & ")")
    Next i
   If Not maria Then
  Select Case Sino
  Case True
    strSql = "SELECT CI26CODPROVI FROM CI2600 WHERE " & strField(0) & strCom
  Case False
    strSql = "SELECT CI16CODLOCALID FROM CI1600 WHERE " & strField(1) & strCom
  End Select
  Else
     strSql = "SELECT CI19codpais FROM CI1900 WHERE " & strField(2) & strCom

  End If
  
  Set rdoBeam = objApp.rdoConnect.CreateQuery("", strSql)
  rdoBeam.rdoParameters(0) = strValue
  Set rdoConsulta = rdoBeam.OpenResultset(rdOpenStatic, rdConcurReadOnly)
  'strValue = "("
  With rdoConsulta
    If .RowCount > 250 Then
        Check = "mal"
        Exit Function
    End If
    If .RowCount <> 0 Then
        .MoveFirst
        Do While .EOF = False
            'strValue = strValue & strComa & .rdoColumns(0)
            strValue = .rdoColumns(0)
            strComa = ","
            .MoveNext
        Loop
        'strValue = strValue & ")"
    Else
        strValue = "NADA"
    End If
  End With

  Call rdoConsulta.Close
  Call rdoBeam.Close
  Set rdoConsulta = Nothing
  Set rdoBeam = Nothing

  Check = strValue

End Function

Private Sub CambiaStatus()
    
    With StatusBar1
    If .Panels(1).Tag <> 0 Then
       .Panels(1).Tag = 0
       .Panels(1).Text = "Preparado"
       Screen.MousePointer = vbDefault
    Else
        .Panels(1).Tag = 1
        .Panels(1).Text = "Buscando..."
        Screen.MousePointer = vbHourglass
    End If
    End With

End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call cmdConsulta_Click
    End If
End Sub

Private Function CheckTfno(strTfno) As String

    Dim rdoCursorB   As rdoResultset
    Dim rdoConsultaB As rdoQuery
    Dim strSql       As String
    Dim Valores      As String
    Dim strComa      As String
    
    strSql = "SELECT CI21CODPERSONA FROM CI1000 WHERE CI10TELEFONO=?"
    Set rdoConsultaB = objApp.rdoConnect.CreateQuery("", strSql)
    rdoConsultaB.rdoParameters(0) = strTfno
    Set rdoCursorB = rdoConsultaB.OpenResultset(rdOpenStatic, rdConcurReadOnly)
    Valores = "CI21CODPERSONA IN ("
    Do While rdoCursorB.EOF = False
        Valores = Valores & rdoCursorB.rdoColumns(0) & strComa
        strComa = ","
        rdoCursorB.MoveNext
    Loop
    If rdoCursorB.RowCount <= 0 Then
        Valores = Valores & "-1"
    End If
    Valores = Valores & ")"
    
    CheckTfno = Valores
    
End Function
