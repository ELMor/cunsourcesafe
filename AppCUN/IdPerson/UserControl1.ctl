VERSION 5.00
Begin VB.UserControl IdPersona 
   AutoRedraw      =   -1  'True
   ClientHeight    =   1335
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10020
   FillColor       =   &H8000000F&
   FillStyle       =   0  'Solid
   FontTransparent =   0   'False
   ScaleHeight     =   1335
   ScaleWidth      =   10020
   ToolboxBitmap   =   "UserControl1.ctx":0000
   Begin VB.CommandButton cmdBusqueda 
      Caption         =   "B�squeda"
      Height          =   375
      Left            =   7680
      TabIndex        =   12
      Top             =   240
      Width           =   1215
   End
   Begin VB.CommandButton cmdAvisos 
      Enabled         =   0   'False
      Height          =   330
      Left            =   6120
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Ver avisos sobre persona"
      Top             =   240
      Width           =   495
   End
   Begin VB.TextBox txtIdenti 
      BackColor       =   &H00C0C0C0&
      Height          =   330
      HelpContextID   =   1
      Index           =   5
      Left            =   6720
      Locked          =   -1  'True
      MaxLength       =   25
      TabIndex        =   5
      TabStop         =   0   'False
      Tag             =   "Segundo Apellido del Paciente"
      ToolTipText     =   "Nombre y Apellidos del Paciente"
      Top             =   960
      Width           =   3255
   End
   Begin VB.TextBox txtIdenti 
      BackColor       =   &H00C0C0C0&
      Height          =   330
      HelpContextID   =   1
      Index           =   4
      Left            =   3360
      Locked          =   -1  'True
      MaxLength       =   25
      TabIndex        =   4
      TabStop         =   0   'False
      Tag             =   "Primer Apellido del Paciente"
      ToolTipText     =   "Nombre y Apellidos del Paciente"
      Top             =   960
      Width           =   3255
   End
   Begin VB.TextBox txtIdenti 
      BackColor       =   &H00C0FFC0&
      DataField       =   "CI21CodPersona"
      Height          =   330
      HelpContextID   =   1
      Index           =   0
      Left            =   0
      MaxLength       =   7
      TabIndex        =   13
      Tag             =   "C�digo de Persona del Paciente"
      ToolTipText     =   "C�digo Persona"
      Top             =   240
      Width           =   1575
   End
   Begin VB.TextBox txtIdenti 
      BackColor       =   &H00C0C0C0&
      Height          =   330
      HelpContextID   =   1
      Index           =   1
      Left            =   1680
      Locked          =   -1  'True
      MaxLength       =   7
      TabIndex        =   1
      TabStop         =   0   'False
      Tag             =   "Historia del paciente"
      ToolTipText     =   "Historia Cl�nica"
      Top             =   240
      Width           =   1575
   End
   Begin VB.TextBox txtIdenti 
      BackColor       =   &H00C0C0C0&
      Height          =   330
      HelpContextID   =   1
      Index           =   3
      Left            =   0
      Locked          =   -1  'True
      MaxLength       =   25
      TabIndex        =   3
      TabStop         =   0   'False
      Tag             =   "Nombre del Paciente"
      ToolTipText     =   "Nombre y Apellidos del Paciente"
      Top             =   960
      Width           =   3255
   End
   Begin VB.TextBox txtIdenti 
      BackColor       =   &H00C0C0C0&
      Height          =   330
      HelpContextID   =   1
      Index           =   2
      Left            =   3360
      Locked          =   -1  'True
      MaxLength       =   12
      TabIndex        =   2
      TabStop         =   0   'False
      Tag             =   "D.N.I. del Paciente"
      ToolTipText     =   "D.N.I. Persona F�sica"
      Top             =   240
      Width           =   2415
   End
   Begin VB.Label lblIdenti 
      AutoSize        =   -1  'True
      Caption         =   "Apellido 2�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   6720
      TabIndex        =   11
      Top             =   720
      Width           =   975
   End
   Begin VB.Label lblIdenti 
      AutoSize        =   -1  'True
      Caption         =   "Apellido 1�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   3360
      TabIndex        =   10
      Top             =   720
      Width           =   975
   End
   Begin VB.Label lblIdenti 
      AutoSize        =   -1  'True
      Caption         =   "Persona"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   735
   End
   Begin VB.Label lblIdenti 
      AutoSize        =   -1  'True
      Caption         =   "Historia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   1680
      TabIndex        =   8
      Top             =   0
      Width           =   735
   End
   Begin VB.Label lblIdenti 
      AutoSize        =   -1  'True
      Caption         =   "D.N.I"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   3360
      TabIndex        =   7
      Top             =   0
      Width           =   495
   End
   Begin VB.Label lblIdenti 
      AutoSize        =   -1  'True
      Caption         =   "Nombre "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   0
      TabIndex        =   6
      Top             =   720
      Width           =   735
   End
End
Attribute VB_Name = "IdPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
 
'Dim objApp As clsCWApp

'Default Property Values:
Const m_def_BackColor = 0
Const m_def_ForeColor = 0
Const m_def_Enabled = 0
Const m_def_blnSearchButton = -1
Const m_def_BackStyle = 0
Const m_def_BorderStyle = 0
Const m_def_Connect = ""
Const m_def_SQL = ""
Const m_def_blnBus = 0 'maria1 he definido la propiedad blnBus para saber
                        'si busco una persona o un responsable
                        'maria2

'Property Variables:
Dim m_BackColor As Long
Dim m_ForeColor As Long
Dim m_Enabled As Boolean
Dim m_blnSearchButton As Boolean
Dim m_Font As Font
Dim m_BackStyle As Integer
Dim m_BorderStyle As Integer
Dim m_Connect As String
Dim m_SQL As String
Dim m_MaxLength As Integer
Dim m_blnBus As Boolean

'Event Declarations:
Event Click()
Attribute Click.VB_Description = "Occurs when the user presses and then releases a mouse button over an object."
Event Change()
Event DblClick()
Attribute DblClick.VB_Description = "Occurs when the user presses and releases a mouse button and then presses and releases it again over an object."
Event KeyDown(KeyCode As Integer, Shift As Integer)
Attribute KeyDown.VB_Description = "Occurs when the user presses a key while an object has the focus."
Event KeyPress(KeyAscii As Integer)
Attribute KeyPress.VB_Description = "Occurs when the user presses and releases an ANSI key."
Event KeyUp(KeyCode As Integer, Shift As Integer)
Attribute KeyUp.VB_Description = "Occurs when the user releases a key while an object has the focus."
Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Attribute MouseDown.VB_Description = "Occurs when the user presses the mouse button while an object has the focus."
Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Attribute MouseMove.VB_Description = "Occurs when the user moves the mouse."
Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Attribute MouseUp.VB_Description = "Occurs when the user releases the mouse button while an object has the focus."
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
'controlar si el foco esta en el c�digo de persona
Dim blnCodigoFocus As Boolean

Public Sub Buscar()
     Busqueda
End Sub


Public Property Get blnBus() As Boolean
    blnBus = m_blnBus
End Property



'
Public Property Let blnBus(ByVal NewblnBus As Boolean)
     m_blnBus = NewblnBus
    PropertyChanged "blnBus"
End Property



Public Property Get blnSearchButton() As Boolean
    blnSearchButton = m_blnSearchButton
End Property

Public Property Let blnSearchButton(ByVal NewBlnSearchButton As Boolean)
     m_blnSearchButton = NewBlnSearchButton
     cmdBusqueda.Visible = m_blnSearchButton
     PropertyChanged "BlnSearchButton"
End Property

Public Property Get Enabled() As Boolean
Attribute Enabled.VB_Description = "Returns/sets a value that determines whether an object can respond to user-generated events."
    Enabled = m_Enabled
End Property

Public Property Let Enabled(ByVal New_Enabled As Boolean)
    m_Enabled = New_Enabled
    PropertyChanged "Enabled"
End Property

Public Property Get Font() As Font
Attribute Font.VB_Description = "Returns a Font object."
Attribute Font.VB_UserMemId = -512
    Set Font = m_Font
End Property

Public Property Set Font(ByVal New_Font As Font)
    Set m_Font = New_Font
    PropertyChanged "Font"
End Property

Public Property Get BackStyle() As Integer
Attribute BackStyle.VB_Description = "Indicates whether a Label or the background of a Shape is transparent or opaque."
    BackStyle = m_BackStyle
End Property

Public Property Let BackStyle(ByVal New_BackStyle As Integer)
    m_BackStyle = New_BackStyle
    PropertyChanged "BackStyle"
End Property

Public Property Get BorderStyle() As Integer
Attribute BorderStyle.VB_Description = "Returns/sets the border style for an object."
    BorderStyle = m_BorderStyle
End Property

Public Property Let BorderStyle(ByVal New_BorderStyle As Integer)
    m_BorderStyle = New_BorderStyle
    PropertyChanged "BorderStyle"
End Property

Public Sub Refresh()
Attribute Refresh.VB_Description = "Forces a complete repaint of a object."
     Me.Refresh
End Sub
'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MappingInfo=txtIdenti(0),txtIdenti,0,Text
Public Property Get Text() As String
Attribute Text.VB_Description = "Returns/sets the text contained in the control."
Attribute Text.VB_ProcData.VB_Invoke_Property = ";Text"
Attribute Text.VB_UserMemId = 0
    Text = txtIdenti(0).Text
End Property
'
Public Property Let Text(ByVal New_Text As String)
    txtIdenti(0).Text() = New_Text
    PropertyChanged "Text"
End Property
'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MappingInfo=txtIdenti(3),txtIdenti,3,Text
Public Property Get Nombre() As String
Attribute Nombre.VB_Description = "Returns/sets the text contained in the control."
    Nombre = txtIdenti(3).Text
End Property
'
Public Property Let Nombre(ByVal New_Nombre As String)
    txtIdenti(3).Text() = New_Nombre
    PropertyChanged "Nombre"
End Property
'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MappingInfo=txtIdenti(4),txtIdenti,4,Text
Public Property Get Apellido1() As String
    Apellido1 = txtIdenti(4).Text
End Property
'
Public Property Let Apellido1(ByVal New_Apellido1 As String)
    txtIdenti(4).Text() = New_Apellido1
    PropertyChanged "Apellido1"
End Property
'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MappingInfo=txtIdenti(5),txtIdenti,5,Text
Public Property Get Apellido2() As String
    Apellido2 = txtIdenti(5).Text
End Property
'
Public Property Let Apellido2(ByVal New_Apellido2 As String)
    txtIdenti(5).Text() = New_Apellido2
    PropertyChanged "Apellido2"
End Property
'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MappingInfo=txtIdenti(5),txtIdenti,5,Text
Public Property Get blnAvisos() As Boolean
    blnAvisos = cmdAvisos.Enabled
End Property
'
Public Property Let blnAvisos(ByVal New_blnAvisos As Boolean)
    cmdAvisos.Enabled() = New_blnAvisos
    'cmdAvisos.Visible() = New_blnAvisos
    PropertyChanged "blnAvisos"
End Property


'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MappingInfo=txtIdenti(1),txtIdenti,1,Text
Public Property Get Historia() As String
Attribute Historia.VB_Description = "Returns/sets the text contained in the control."
    Historia = txtIdenti(1).Text
End Property
'
Public Property Let Historia(ByVal New_Historia As String)
    txtIdenti(1).Text() = New_Historia
    PropertyChanged "Historia"
End Property
'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MappingInfo=txtIdenti(2),txtIdenti,2,Text
Public Property Get DNI() As String
Attribute DNI.VB_Description = "Returns/sets the text contained in the control."
    DNI = txtIdenti(2).Text
End Property
Public Property Get MaxLength() As String
    MaxLength = txtIdenti(0).MaxLength
End Property
'
Public Property Let Locked(ByVal New_Locked As Boolean)
    txtIdenti(0).Locked() = New_Locked
    PropertyChanged "Locked"
End Property
Public Property Get Locked() As Boolean
    Locked = txtIdenti(0).Locked
End Property
'
Public Property Let MaxLength(ByVal New_Text As String)
    txtIdenti(0).MaxLength() = New_Text
    PropertyChanged "MaxLength"
End Property

'
Public Property Let DNI(ByVal New_Dni As String)
    txtIdenti(3).Text() = New_Dni
    PropertyChanged "Dni"
End Property
Public Property Get Connect() As String
    Connect = m_Connect
End Property
Public Property Let Connect(ByVal New_Connect As String)
    m_Connect = New_Connect
    PropertyChanged "Connect"
End Property
Public Property Get SQL() As String
    SQL = m_SQL
End Property
Public Property Let SQL(ByVal New_SQL As String)
    m_SQL = New_SQL
    PropertyChanged "SQL"
End Property

Public Function ReadPersona() As Variant
 Dim rs As rdoResultset
 Dim rsMens As rdoResultset
 Dim imgX As ListImage

 m_SQL = "SELECT CI21CodPersona, CI22Nombre, CI22PriApel, CI22SegApel, CI22NumHistoria, CI22DNI FROM CI2200 WHERE CI21CodPersona = " & Val(txtIdenti(0).Text)
 Set rs = objApp.rdoConnect.OpenResultset(m_SQL, rdOpenKeyset, rdConcurReadOnly)
 With rs
   If .RowCount > 0 Then
     .MoveFirst
    
     txtIdenti(1) = ""
     txtIdenti(2) = ""
     txtIdenti(3) = ""
     txtIdenti(4) = ""
     txtIdenti(5) = ""
    'UserControl11.Codigo = .rdocolumns("CodPerso")
     If Not IsNull(.rdoColumns("CI22NumHistoria")) Then
       txtIdenti(1).Text = .rdoColumns("CI22NumHistoria")
     End If
     If Not IsNull(.rdoColumns("CI22DNI")) Then
       txtIdenti(2).Text = .rdoColumns("CI22DNI")
     End If
     If Not IsNull(.rdoColumns("CI22Nombre")) Then
       txtIdenti(3).Text = .rdoColumns("CI22Nombre")
     End If
     If Not IsNull(.rdoColumns("CI22PriApel")) Then
       txtIdenti(4).Text = .rdoColumns("CI22PriApel")
     End If
     If Not IsNull(.rdoColumns("CI22SegAPel")) Then
       txtIdenti(5).Text = .rdoColumns("CI22SegAPel")
     End If
     m_SQL = "SELECT ci21CODPERSONA from  CI0400 WHERE CI21CodPersona =" & Val(txtIdenti(0).Text) & " and (ci04FecCadMens >= " & "{ts '" _
& Format(Date, "yyyy-mm-dd hh:nn:ss") & "'}" _
     & " Or ci04FecCadMens Is Null)"
     Set rsMens = objApp.rdoConnect.OpenResultset(m_SQL, rdOpenKeyset, rdConcurReadOnly)
     If rsMens.RowCount > 0 Then
       Set imgX = objApp.imlImageList.ListImages("i44")
       cmdAvisos.Picture = imgX.Picture
     Else
       Set imgX = objApp.imlImageList.ListImages("i40")
       cmdAvisos.Picture = imgX.Picture
     End If
     rsMens.Close
     cmdAvisos.Enabled = True
   Else
    'UserControl11.Codigo = ""
     txtIdenti(1).Text = ""
     txtIdenti(2).Text = ""
     txtIdenti(3).Text = ""
     txtIdenti(4).Text = ""
     txtIdenti(5).Text = ""
     cmdAvisos.Enabled = False
   End If
   .Close
  End With
End Function

Public Function BeginControl(mobjApp As clsCWApp, mobjGen As clsCWGen)
  Set objApp = mobjApp
  Set objGen = mobjGen
End Function

Public Function EndControl()
'  Set objApp = Nothing
'  Set objGen = Nothing
End Function


Public Function SearchPersona()

   Dim objField As clsCWFieldSearch
   Set objSearch = New clsCWSearch
   
   With objSearch
      .strTable = "CI2200"
      .strWhere = ""
      .strOrder = ""
      Set objField = .AddField("CI21CODPERSONA")
      Set objField = .AddField("CI22NOMBRE")
      Set objField = .AddField("CI22PRIAPEL")
      Set objField = .AddField("CI22SEGAPEL")
      Set objField = .AddField("CI22DNI")
      Set objField = .AddField("CI22NUMHISTORIA")
      Set objField = .AddField("CI22NUMSEGSOC")
      Set objField = .AddField("CI22FECNACIM")
      'Call .AddField("CI22FECNACIM")
      .LoadDictionary
      If .Search Then
         txtIdenti(0).Text = .cllValues("CI21CODPERSONA")
      End If
   End With
   Set objField = Nothing
   Set objSearch = Nothing

End Function

Private Sub cmdAvisos_Click()
'Call frmAvisos.BeginControl(objApp)
  vntPersonaAviso = txtIdenti(0).Text
  strHistoria = txtIdenti(1).Text
  strDni = txtIdenti(2).Text
  strNombre = txtIdenti(3).Text
  strApellido1 = txtIdenti(4).Text
  strApellido2 = txtIdenti(5).Text
  Load frmAvisos
  frmAvisos.Show vbModal
  Unload frmAvisos
  Call ReadPersona
  Set frmAvisos = Nothing
End Sub


Private Sub cmdBusqueda_Click()

    Call Busqueda
    
End Sub

Private Sub txtIdenti_Change(intIndex As Integer)
  Dim intText As Integer
'comprobar si los colores de los campos readonly han sido cambiados
  If objApp.objUserColor.lngReadOnly <> txtIdenti(1).BackColor Then
    For intText = 1 To 5
      txtIdenti(intText).BackColor = objApp.objUserColor.lngReadOnly
    Next
  End If
  If intIndex = 0 Then
    'If cmdAvisos.Enabled = False And blnCodigoFocus = True Then
      'nada
    'Else
      RaiseEvent Change
      Call ReadPersona
    'End If
  End If
End Sub

Private Sub txtIdenti_GotFocus(intIndex As Integer)
  blnCodigoFocus = IIf(intIndex = 0, True, False)
  UserControl.Tag = txtIdenti(intIndex)
End Sub

Private Sub txtIdenti_KeyPress(intIndex As Integer, intKeyAscii As Integer)
  If intIndex = 0 Then
    Select Case intKeyAscii
      Case 32, 45, vbKeyBack
      Case vbKey0 To vbKey9
      Case Else
        intKeyAscii = 0
    End Select
  End If
End Sub

Private Sub txtIdenti_LostFocus(intIndex As Integer)
  If intIndex = 0 Then
    Call ReadPersona
  End If
  blnCodigoFocus = False
End Sub

'Initialize Properties for User Control
Private Sub UserControl_InitProperties()
    m_BackColor = m_def_BackColor
    m_ForeColor = m_def_ForeColor
    m_Enabled = m_def_Enabled
    m_blnSearchButton = m_def_blnSearchButton
    m_blnBus = m_def_blnBus
    Set m_Font = Ambient.Font
    m_BackStyle = m_def_BackStyle
    m_BorderStyle = m_def_BorderStyle
    m_Connect = m_def_Connect
    m_SQL = m_def_SQL
End Sub

'Load property values from storage
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

    'm_BackColor = PropBag.ReadProperty("BackColor", m_def_BackColor)
    txtIdenti(0).BackColor = PropBag.ReadProperty("BackColor", &HFFFFFF)
    m_ForeColor = PropBag.ReadProperty("ForeColor", m_def_ForeColor)
    m_Enabled = PropBag.ReadProperty("Enabled", m_def_Enabled)
    m_blnSearchButton = PropBag.ReadProperty("blnSearchButton", m_def_blnSearchButton)
    m_blnBus = PropBag.ReadProperty("blnBus", m_def_blnBus)
    Set m_Font = PropBag.ReadProperty("Font", Ambient.Font)
    m_BackStyle = PropBag.ReadProperty("BackStyle", m_def_BackStyle)
    m_BorderStyle = PropBag.ReadProperty("BorderStyle", m_def_BorderStyle)
    txtIdenti(0).Datafield = PropBag.ReadProperty("Datafield", "")
    txtIdenti(0).Text = PropBag.ReadProperty("Text", "")
    txtIdenti(1).Text = PropBag.ReadProperty("Historia", "")
    txtIdenti(2).Text = PropBag.ReadProperty("Dni", "")
    txtIdenti(3).Text = PropBag.ReadProperty("Nombre", "")
    txtIdenti(4).Text = PropBag.ReadProperty("Apellido1", "")
    txtIdenti(5).Text = PropBag.ReadProperty("Apellido2", "")
    txtIdenti(0).MaxLength = PropBag.ReadProperty("MaxLength", 0)
    cmdAvisos.Enabled = PropBag.ReadProperty("blnAvisos", True)
    m_Connect = PropBag.ReadProperty("Connect", m_def_Connect)
    m_SQL = PropBag.ReadProperty("SQL", m_def_SQL)

End Sub

'Write property values to storage
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    'Call PropBag.WriteProperty("BackColor", m_BackColor, m_def_BackColor)
    Call PropBag.WriteProperty("BackColor", txtIdenti(0).BackColor, "&H00FFFFFF&")
    Call PropBag.WriteProperty("ForeColor", m_ForeColor, m_def_ForeColor)
    Call PropBag.WriteProperty("Enabled", m_Enabled, m_def_Enabled)
    Call PropBag.WriteProperty("blnSearchButton", m_blnSearchButton, m_def_blnSearchButton)
    Call PropBag.WriteProperty("blnBus", m_blnBus, m_def_blnBus)
    Call PropBag.WriteProperty("Font", m_Font, Ambient.Font)
    Call PropBag.WriteProperty("BackStyle", m_BackStyle, m_def_BackStyle)
    Call PropBag.WriteProperty("BorderStyle", m_BorderStyle, m_def_BorderStyle)
    Call PropBag.WriteProperty("Datafield", txtIdenti(0).Datafield, "")
    Call PropBag.WriteProperty("Text", txtIdenti(0).Text, "")
    Call PropBag.WriteProperty("Historia", txtIdenti(1).Text, "")
    Call PropBag.WriteProperty("Dni", txtIdenti(2).Text, "")
    Call PropBag.WriteProperty("Nombre", txtIdenti(3).Text, "")
    Call PropBag.WriteProperty("Apellido1", txtIdenti(4).Text, "")
    Call PropBag.WriteProperty("Apellido2", txtIdenti(5).Text, "")
    Call PropBag.WriteProperty("MaxLength", txtIdenti(0).MaxLength, 0)
    Call PropBag.WriteProperty("blnAvisos", cmdAvisos.Enabled, True)
    Call PropBag.WriteProperty("Connect", m_Connect, m_def_Connect)
    Call PropBag.WriteProperty("SQL", m_SQL, m_def_SQL)
End Sub
'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtIdenti(0),txtIdenti,0,Text
Public Property Get Datafield() As String
    Datafield = txtIdenti(0).Datafield
End Property

Public Property Let Datafield(ByVal New_Datafield As String)
    txtIdenti(0).Datafield() = New_Datafield
    PropertyChanged "Datafield"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtIdenti(0),txtIdenti,0,Text
Public Property Get BackColor() As Long
Attribute BackColor.VB_Description = "Returns/sets the background color used to display text and graphics in an object."
    BackColor = txtIdenti(0).BackColor
End Property

Public Property Let BackColor(ByVal New_BackColor As Long)
    txtIdenti(0).BackColor() = New_BackColor
    PropertyChanged "BackColor"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtIdenti(0),txtIdenti,0,Text
Public Property Get ForeColor() As Long
Attribute ForeColor.VB_Description = "Returns/sets the foreground color used to display text and graphics in an object."
    ForeColor = txtIdenti(0).ForeColor
End Property

Public Property Let ForeColor(ByVal New_ForeColor As Long)
    txtIdenti(0).ForeColor() = New_ForeColor
    PropertyChanged "ForeColor"
End Property

Public Sub Busqueda()
    frmBusqueda.Show vbModal
    'If CodPersona <> 0 Then
    'maria1 le he colocado el if pero no lo de dentro.
    'para saber si esta buscando a la persona principal de un formulario
    'o al resoonsable familiar o economico
    
    If blnBus = False Then
    Call objApp.objActiveWin.DataOpen
    End If
    'maria2
    txtIdenti(0).Text = CodPersona
    Call objApp.objActiveWin.CtrlLostFocus
    'End If
    blnBus = False
End Sub

Public Sub SearchButton(Activar As Boolean)
    cmdBusqueda.Visible = Activar
End Sub
