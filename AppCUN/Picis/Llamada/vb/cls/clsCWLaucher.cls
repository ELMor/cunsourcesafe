VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Const PIWinAdmision As String = "PI0101"
Const PIWinUsuarios As String = "PI0102"
Const PIWinCamas As String = "PI0103"
Const PIWinFarmacia As String = "PI0104"
Const PIWinOrdenMedica As String = "PI0105"
Const PIWinPasoAutomatico As String = "PI0106"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
  Set objCW = mobjCW
End Sub
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case PIWinAdmision
        frmAdmision.Show vbModal
        Set frmAdmision = Nothing
    Case PIWinUsuarios
        frmUsuarios.Show vbModal
        Set frmUsuarios = Nothing
    Case PIWinCamas
        frmCamas.Show vbModal
        Set frmCamas = Nothing
    Case PIWinFarmacia
        frmFarmacia.Show vbModal
        Set frmFarmacia = Nothing
    Case PIWinOrdenMedica
        frmOrdenMedica.Show vbModal
        Set frmOrdenMedica = Nothing
    Case PIWinPasoAutomatico
        frmPasoAutomatico.Show vbModal
        Set frmOrdenMedica = Nothing
    Case Else
        LaunchProcess = False
    End Select
  End Function
  Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a procesos
  ReDim aProcess(1 To 6, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = PIWinAdmision
  aProcess(1, 2) = "Admision de Picis"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PIWinUsuarios
  aProcess(2, 2) = "Usuarios de Picis"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PIWinCamas
  aProcess(3, 2) = "Transacciones de camas de Picis"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PIWinFarmacia
  aProcess(4, 2) = "Mantenimiento de Medicamentos de Picis"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PIWinOrdenMedica
  aProcess(5, 2) = "Orden medica de Picis"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow

  aProcess(5, 1) = PIWinPasoAutomatico
  aProcess(5, 2) = "Paso Autom�tico"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow

End Sub





