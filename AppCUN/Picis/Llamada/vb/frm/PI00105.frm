VERSION 5.00
Begin VB.Form frmOrdenMedica 
   Caption         =   "Orden M�dica"
   ClientHeight    =   3465
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4125
   LinkTopic       =   "Form1"
   ScaleHeight     =   3465
   ScaleWidth      =   4125
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Anular Orden M�dica"
      Default         =   -1  'True
      Height          =   615
      Left            =   960
      TabIndex        =   5
      Top             =   2640
      Width           =   1935
   End
   Begin VB.TextBox txtCodPeticion 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   2040
      TabIndex        =   1
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton cmdOrdenMedica 
      Caption         =   " Pasar Orden M�dica"
      Height          =   615
      Left            =   960
      TabIndex        =   3
      Top             =   1680
      Width           =   1935
   End
   Begin VB.TextBox txtHistoria 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   2040
      TabIndex        =   2
      Text            =   "250"
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo de Petici�n"
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   960
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "N�mero de Historia"
      Height          =   255
      Left            =   480
      TabIndex        =   0
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "frmOrdenMedica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdOrdenMedica_Click()
Dim objOrdenMedica As New PI00100.clsFarmacia
Me.MousePointer = vbHourglass
    Set objOrdenMedica.objClsCW = objCW
    objOrdenMedica.strUser = objSecurity.strUser
    Call objOrdenMedica.ordenmedica(txtHistoria.Text, txtCodPeticion.Text)
Me.MousePointer = vbDefault
End Sub

Private Sub Command1_Click()
Dim objOrdenMedica As New PI00100.clsFarmacia
Me.MousePointer = vbHourglass
    Set objOrdenMedica.objClsCW = objCW
    objOrdenMedica.strUser = objSecurity.strUser
    Call objOrdenMedica.anularom(txtHistoria.Text, txtCodPeticion.Text)
Me.MousePointer = vbDefault
End Sub

Private Sub Form_Load()
'txtHistoria.SetFocus
End Sub
