VERSION 5.00
Begin VB.Form frmPasoAutomatico 
   Caption         =   "Paso de datos inicial"
   ClientHeight    =   2265
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   2265
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdMedicamentos 
      Caption         =   "Medicacion: CUN -> Picis"
      Height          =   495
      Left            =   1200
      TabIndex        =   1
      Top             =   1200
      Width           =   2055
   End
   Begin VB.CommandButton cmdFamilias 
      Caption         =   "Familias: CUN -> Picis"
      Height          =   495
      Left            =   1200
      TabIndex        =   0
      Top             =   480
      Width           =   2055
   End
End
Attribute VB_Name = "frmPasoAutomatico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Private Sub cmdFamilias_Click()
Me.MousePointer = vbHourglass
Dim objFarmacia As New PI00100.clsFarmacia
Set objFarmacia.objClsCW = objCW
objFarmacia.strUser = objSecurity.strUser
Call objFarmacia.pasopicisfamilias
Me.MousePointer = vbDefault
End Sub

Private Sub cmdMedicamentos_Click()
Me.MousePointer = vbHourglass
Dim objFarmacia As New PI00100.clsFarmacia
Set objFarmacia.objClsCW = objCW
objFarmacia.strUser = objSecurity.strUser
Call objFarmacia.PASOPICISMEDICAMENTOS(objCW)
Me.MousePointer = vbDefault
End Sub

