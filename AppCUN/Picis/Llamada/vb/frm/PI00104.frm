VERSION 5.00
Begin VB.Form frmFarmacia 
   Caption         =   "Farmacia"
   ClientHeight    =   5475
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4320
   LinkTopic       =   "Form1"
   ScaleHeight     =   5475
   ScaleWidth      =   4320
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "Medicamentos"
      Height          =   2415
      Left            =   240
      TabIndex        =   4
      Top             =   2160
      Width           =   3615
      Begin VB.CommandButton cmdMedicamento 
         Caption         =   "Crear Medicamento"
         Default         =   -1  'True
         Height          =   735
         Left            =   600
         TabIndex        =   7
         Top             =   1320
         Width           =   2055
      End
      Begin VB.TextBox txtMedicamento 
         Height          =   375
         Left            =   1920
         TabIndex        =   5
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Cod. Medicamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Familia"
      Height          =   1575
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   3615
      Begin VB.TextBox txtFamilia 
         Height          =   375
         Left            =   1920
         TabIndex        =   2
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cmdFamilia 
         Caption         =   "Insertar Familia"
         Height          =   735
         Left            =   720
         TabIndex        =   1
         Top             =   720
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "Cod. Familia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmFarmacia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdFamilia_Click()
    Dim objFarmacia As New PI00100.clsFarmacia
    Set objFarmacia.objClsCW = objCW
    objFarmacia.strUser = objSecurity.strUser
    Call objFarmacia.Familia(txtFamilia.Text)
End Sub


Private Sub cmdMedicamento_Click()
    Dim objFarmacia As New PI00100.clsFarmacia
    Set objFarmacia.objClsCW = objCW
    objFarmacia.strUser = objSecurity.strUser
    Call objFarmacia.Medicamentos(txtMedicamento.Text)
End Sub


