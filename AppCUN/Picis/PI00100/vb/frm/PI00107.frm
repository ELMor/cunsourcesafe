VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmEntorno 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selección del entorno de Quirófano"
   ClientHeight    =   4065
   ClientLeft      =   3345
   ClientTop       =   2940
   ClientWidth     =   5535
   FillColor       =   &H00800000&
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   -1  'True
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000080&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4065
   ScaleWidth      =   5535
   Begin VsOcxLib.VideoSoftAwk Awk 
      Left            =   720
      Top             =   3600
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   " ;"
   End
   Begin VB.Frame FrameEntorno 
      Caption         =   "Entorno"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   3375
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   4815
      Begin VB.OptionButton Option1 
         Caption         =   "Circulación Extracorpórea"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   2
         Left            =   840
         TabIndex        =   4
         Top             =   2040
         Width           =   3615
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Monitorización No Invasiva"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   1
         Left            =   840
         TabIndex        =   3
         Top             =   1260
         Width           =   3375
      End
      Begin VB.CommandButton CmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2760
         TabIndex        =   2
         Top             =   2640
         Width           =   1575
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Monitorización Invasiva"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   0
         Left            =   840
         TabIndex        =   1
         Top             =   480
         Width           =   3495
      End
   End
End
Attribute VB_Name = "frmEntorno"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdAceptar_Click()

Dim TipoEntorno As String
Dim EntornoDboid As String

    If Option1(0) = True Then
        TipoEntorno = 1     'Monitorización Invasiva
    End If
    
    If Option1(1) = True Then
        TipoEntorno = 2     'Monitorización No Invasiva
    End If
    
    If Option1(2) = True Then
        TipoEntorno = 3     'Circulación extracorpórea
    End If

Unload frmEntorno

Select Case TipoEntorno
    Case 1
        EntornoDboid = "041000000000001000511"
    Case 2
        EntornoDboid = "041000000000000000511"
    Case 3
        EntornoDboid = "041000000000007000000"
    Case Else
        EntornoDboid = "041000000000002000511"
End Select

Call Admision(objPipe.PipeGet("PI_CodAsistencia"), objPipe.PipeGet("PI_blnVisualcare"), EntornoDboid)

End Sub
