Attribute VB_Name = "FuncionesCamas"
Option Explicit
Public Function TransferirCama(codAsistencia As Long, PatientDboid As String) As Boolean
Dim condition As String
Dim Asistencia As String
Dim cama As String
Dim Systime As Long
Dim codPersona As Long
Dim numHistoria As Long
Dim Historia As String
Dim sNull As String

TransferirCama = True

Call BuscarPacienteDesdeAsistencia(codAsistencia, numHistoria)
    ierrcode = PcsListObjectsAlloc(hList)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
        Exit Function
    End If
Historia = CStr(numHistoria)
    'Build the condition string
    condition = "$='" + Historia + "'"
    'The only condition is the Patient name, so the other fields are empty strings
    ierrcode = PcsPatientFindWithMembers(hLog, hList, condition, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
        TransferirCama = False
        Exit Function
    End If
  ierrcode = PcsListObjectsGotoHeadPosition(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "ERROR"
    TransferirCama = False
    Exit Function
End If
ierrcode = PcsPatientAlloc(hPatient)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
    TransferirCama = False
    Exit Function
End If
ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hPatient)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
    TransferirCama = False
    Exit Function
End If
ierrcode = PcsDbObjectGetIdentifier(hPatient, PatientDboid, 21)
If ierrcode <> 0 Then
            MsgBox "Error al obtener el PatientDboid", vbExclamation
    TransferirCama = False
    Exit Function
End If
ierrcode = PcsAdtObjectAlloc(hTransfer, hLog)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
    TransferirCama = False
    Exit Function
End If
Systime = 0
ierrcode = PcsAdtObjectTransfer(hTransfer, hPatient, hLog, ByVal Systime)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
    TransferirCama = False
    Exit Function
End If
TransferirCama = True
ierrcode = PcsListObjectsFree(hList)
ierrcode = PcsPatientFree(hPatient)
ierrcode = PcsAdtObjectFree(hTransfer)
End Function

Public Function InsertarCama(codCama As String, dDepartamento As String, codDepartam As String) As Boolean

Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim condition As String
Dim ExisteDepartamento As Boolean
Dim DepartDboid As String * 21
Dim Computer As String
Dim Iniciales As String

'strSql = "SELECT AD1500.AD15CODCAMA, AD0200.AD02DESDPTO FROM AD1500, AD0200 WHERE AD1500.AD02CODDPTO=AD0200.AD02CODDPTO AND AD1500.AD15CODCAMA=?"
'Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
'Qy(0) = codCama
'Set Rs = Qy.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)

ierrcode = PcsLocationAlloc(hLocation)
If ierrcode <> 0 Then
        MsgBox "Error allocating Location", vbExclamation
End If
    
ierrcode = PcsDepartmentAlloc(hDepartamento)
If ierrcode <> 0 Then
        MsgBox "Error allocating Departament", vbExclamation
End If
    
ierrcode = PcsListObjectsAlloc(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If

    ierrcode = PcsDbObjectGenerateIdentifier(hLocation)
    If ierrcode <> 0 Then
            MsgBox "Error generate identifier", vbExclamation
            InsertarCama = False
            Exit Function
    End If
    ierrcode = PcsLocationSetDescription(hLocation, codCama)
    If ierrcode <> 0 Then
            MsgBox "Error de cama", vbExclamation
            InsertarCama = False
            Exit Function
    End If
 
'Comprueba si existe el Departamento
    condition = "$='" + dDepartamento + "'"
    ierrcode = PcsDepartmentFindWithMembers(hLog, hList, condition)
    If ierrcode <> 37 And ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
    End If
    If ierrcode = 0 Then ExisteDepartamento = True
    If ierrcode = 37 Then ExisteDepartamento = False

If ExisteDepartamento Then

        ierrcode = PcsListObjectsGotoHeadPosition(hList)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "ERROR"
        End If
        
        ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hDepartamento)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
    
Else

        ierrcode = PcsDbObjectGenerateIdentifier(hDepartamento)
        If ierrcode <> 0 Then
                MsgBox "Error generate identifier", vbExclamation
                InsertarCama = False
                Exit Function
        End If
        
        ierrcode = PcsDbObjectGetIdentifier(hDepartamento, DepartDboid, 21)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        
        ierrcode = PcsDbObjectAllocFromIdentifier(hDepartamento, DepartDboid)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        
        ierrcode = PcsDepartmentSetDescription(hDepartamento, dDepartamento)
        If ierrcode <> 0 Then
                    MsgBox "Error Departamento", vbExclamation
                    InsertarCama = False
                    Exit Function
        End If
         
            ierrcode = PcsDbObjectInsert(hDepartamento)
        If ierrcode <> 0 Then
            MsgBox "Error al ESCRIBIR", vbExclamation
            InsertarCama = False
            Exit Function
        End If
 
End If
    
    ierrcode = PcsLocationSetDepartment(hLocation, hDepartamento)
    If ierrcode <> 0 Then
                MsgBox "Error Departamento", vbExclamation
                InsertarCama = False
                Exit Function
    End If

''    ierrcode = PcsLocationSetComputerName(hLocation, "Desconocido")
''    If ierrcode <> 0 Then
''                MsgBox "Error Departamento", vbExclamation
''                InsertarCama = False
''                Exit Function
''    End If

    Computer = "CPU" + codDepartam + "00"
    ierrcode = PcsLocationSetComputerName(hLocation, Computer)
    If ierrcode <> 0 Then
            MsgBox "Error de cama", vbExclamation
            InsertarCama = False
            Exit Function
    End If
    
    Iniciales = Mid(codCama, 2, 1) & Mid(codCama, 4, 2)
    ierrcode = PcsLocationSetInitials(hLocation, Iniciales)
    If ierrcode <> 0 Then
                MsgBox "Error Departamento", vbExclamation
    End If
    
    ierrcode = PcsDbObjectInsert(hLocation)
    If ierrcode <> 0 Then
        MsgBox "Error al ESCRIBIR", vbExclamation
        InsertarCama = False
        Exit Function
    End If

InsertarCama = True

''Rs.Close
''Qy.Close

ierrcode = PcsListObjectsFree(hList)
ierrcode = PcsDepartmentFree(hDepartamento)
ierrcode = PcsLocationFree(hLocation)

End Function

Public Function ExisteCama(codCama As String) As Boolean

Dim condition As String
Dim sNull As String

ierrcode = PcsListObjectsAlloc(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If

condition = "$='" + codCama + "'"
ierrcode = PcsLocationFindWithMembers(hLog, hList, sNull, condition, sNull, sNull, sNull)
    If ierrcode <> 37 And ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
    End If
    If ierrcode = 0 Then ExisteCama = True
    If ierrcode = 37 Then ExisteCama = False

ierrcode = PcsListObjectsFree(hList)
End Function
