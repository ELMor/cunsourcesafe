Attribute VB_Name = "ModADT"
' ADT functions

#If DebugVersion Then

Public Declare Function PcsAdtObjectAlloc Lib "PCSADT500D.DLL" (ByRef out_phAdtObject As Long, ByVal in_hlog As Long) As Long
Public Declare Function PcsAdtObjectFree Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long) As Long
Public Declare Function PcsAdtObjectSetLocation Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByVal in_hlocation As Long, ByVal bBool As Long) As Long
Public Declare Function PcsAdtObjectSetPatient Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_bConnect As Long) As Long
Public Declare Function PcsAdtObjectGetLocation Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByRef out_phlocation As Long) As Long
Public Declare Function PcsAdtObjectGetLocationStatus Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByRef out_pdwStatus As Long) As Long
Public Declare Function PcsAdtObjectGetPatientStatus Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByRef out_pdwStatus As Long) As Long
Public Declare Function PcsAdtObjectGetPatient Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByRef out_phPatient As Long) As Long
Public Declare Function PcsAdtObjectGetAdmission Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByRef out_phadmission As Long) As Long
Public Declare Function PcsAdtObjectGetPicisData Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByRef out_phpicisdata As Long) As Long
Public Declare Function PcsAdtObjectGetCurrentEnvironment Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByRef out_phenvironment As Long) As Long
Public Declare Function PcsAdtObjectGetCurrentEnvironmentType Lib "PCSADT500D.DLL" (ByVal in_hAdtObject As Long, ByRef out_phEnvType As Long) As Long

Public Declare Function PcsAdtObjectAdmit Lib "PCSADT500D.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlocation As Long, ByVal in_hEnvType As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectExtendedAdmit Lib "PCSADT500D.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlocation As Long, ByVal in_hEnvType As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectPreAdmit Lib "PCSADT500D.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlocation As Long, ByVal in_hEnvType As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectTransfer Lib "PCSADT500D.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectWaitForAdmit Lib "PCSADT500D.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectDischarge Lib "PCSADT500D.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hDischarge As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectConnect Lib "PCSADT500D.DLL" (ByRef out_phAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlocation As Long, ByVal in_hlog As Long) As Long
Public Declare Function PcsAdtObjectGeneralChannelConnect Lib "PCSADT500D.DLL" (ByRef out_phAdtObject As Long) As Long

Public Declare Function PcsUiAdtAdmission Lib "PCSADT500D.DLL" (ByRef out_hAdtObject As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtPreAdmission Lib "PCSADT500D.DLL" (ByRef out_hAdtObject As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtFixedAdmission Lib "PCSADT500D.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtFixedPreAdmission Lib "PCSADT500D.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtDischarge Lib "PCSADT500D.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtTransfer Lib "PCSADT500D.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtReopenAdmission Lib "PCSADT500D.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtReopenPreAdmission Lib "PCSADT500D.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtObjectShowCensusList Lib "PCSADT500D.DLL" (ByRef out_hpat As Long, ByVal in_world As String, ByVal in_file As String, ByVal hLog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtConfigCensusList Lib "PCSADT500D.DLL" (ByVal in_world As String, ByVal in_file As String, ByVal hLog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtConnect Lib "PCSADT500D.DLL" (ByRef out_phAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtDisconnect Lib "PCSADT500D.DLL" (ByVal out_hAdtObject As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtObjectLocalConnect Lib "PCSADT500D.DLL" (ByRef out_phAdtObject As Long, ByVal in_hparent As Long) As Long

#Else

Public Declare Function PcsAdtObjectAlloc Lib "PCSADT500.DLL" (ByRef out_phAdtObject As Long, ByVal in_hlog As Long) As Long
Public Declare Function PcsAdtObjectFree Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long) As Long
Public Declare Function PcsAdtObjectSetLocation Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByVal in_hlocation As Long, ByVal bBool As Long) As Long
Public Declare Function PcsAdtObjectSetPatient Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_bConnect As Long) As Long
Public Declare Function PcsAdtObjectGetLocation Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByRef out_phlocation As Long) As Long
Public Declare Function PcsAdtObjectGetLocationStatus Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByRef out_pdwStatus As Long) As Long
Public Declare Function PcsAdtObjectGetPatientStatus Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByRef out_pdwStatus As Long) As Long
Public Declare Function PcsAdtObjectGetPatient Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByRef out_phPatient As Long) As Long
Public Declare Function PcsAdtObjectGetAdmission Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByRef out_phadmission As Long) As Long
Public Declare Function PcsAdtObjectGetPicisData Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByRef out_phpicisdata As Long) As Long
Public Declare Function PcsAdtObjectGetCurrentEnvironment Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByRef out_phenvironment As Long) As Long
Public Declare Function PcsAdtObjectGetCurrentEnvironmentType Lib "PCSADT500.DLL" (ByVal in_hAdtObject As Long, ByRef out_phEnvType As Long) As Long

Public Declare Function PcsAdtObjectAdmit Lib "PCSADT500.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlocation As Long, ByVal in_hEnvType As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectExtendedAdmit Lib "PCSADT500.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlocation As Long, ByVal in_hEnvType As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectPreAdmit Lib "PCSADT500.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlocation As Long, ByVal in_hEnvType As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectTransfer Lib "PCSADT500.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectWaitForAdmit Lib "PCSADT500.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectDischarge Lib "PCSADT500.DLL" (ByRef in_hAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hDischarge As Long, ByVal in_hlog As Long, ByVal in_SystemTime As Any) As Long
Public Declare Function PcsAdtObjectConnect Lib "PCSADT500.DLL" (ByRef out_phAdtObject As Long, ByVal in_hpatient As Long, ByVal in_hlocation As Long, ByVal in_hlog As Long) As Long
Public Declare Function PcsAdtObjectGeneralChannelConnect Lib "PCSADT500.DLL" (ByRef out_phAdtObject As Long) As Long

Public Declare Function PcsUiAdtAdmission Lib "PCSADT500.DLL" (ByRef out_hAdtObject As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtPreAdmission Lib "PCSADT500.DLL" (ByRef out_hAdtObject As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtFixedAdmission Lib "PCSADT500.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtFixedPreAdmission Lib "PCSADT500.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtDischarge Lib "PCSADT500.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtTransfer Lib "PCSADT500.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtReopenAdmission Lib "PCSADT500.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtReopenPreAdmission Lib "PCSADT500.DLL" (ByRef out_hAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtObjectShowCensusList Lib "PCSADT500.DLL" (ByRef out_hpat As Long, ByVal in_world As String, ByVal in_file As String, ByVal hLog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtConfigCensusList Lib "PCSADT500.DLL" (ByVal in_world As String, ByVal in_file As String, ByVal hLog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtConnect Lib "PCSADT500.DLL" (ByRef out_phAdtObject As Long, ByVal in_hpat As Long, ByVal in_hlog As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtDisconnect Lib "PCSADT500.DLL" (ByVal out_hAdtObject As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsUiAdtObjectLocalConnect Lib "PCSADT500.DLL" (ByRef out_phAdtObject As Long, ByVal in_hparent As Long) As Long
Public Declare Function PcsVbPimsSendAllOrders Lib "PCSVBPIMS500.DLL" (ByVal i_PatientDboid As String, ByVal i_Module As Long) As Long
Public Declare Function PcsVbPimsFree Lib "PCSVBPIMS500.DLL" () As Long
#End If

Global Const ERRCODE_CANCELLED = 607
Global Const ERRCODE_PATIENT_ADMITTED = 612
Global Const ERRCODE_PATIENT_PREADMITTED = 613
Global Const ERRCODE_CENSUSLIST_GOT_PATIENT = 632
