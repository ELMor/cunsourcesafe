Attribute VB_Name = "Module8"
#If DebugVersion Then

Public Declare Function PcsTaskAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsTaskDataAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsTaskDataFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIndex_cond As String, ByVal in_pData_cond As String, _
ByVal in_pTask_cond As String) As Integer
Public Declare Function PcsTaskDataFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsTaskDataGetData Lib "PCSDB500D.DLL" (ByVal in_hHTASKDATA As Long, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskDataGetIndex Lib "PCSDB500D.DLL" (ByVal in_hHTASKDATA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsTaskDataGetTask Lib "PCSDB500D.DLL" (ByVal in_hHTASKDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskDataGetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTASKDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskDataListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsTaskDataSetData Lib "PCSDB500D.DLL" (ByVal in_hHTASKDATA As Long, ByVal in_pData As String) As Integer
Public Declare Function PcsTaskDataSetIndex Lib "PCSDB500D.DLL" (ByVal in_hHTASKDATA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsTaskDataSetTask Lib "PCSDB500D.DLL" (ByVal in_hHTASKDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskDataSetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTASKDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pCreationDate_cond As String, ByVal in_pToDoDate_cond As String, _
ByVal in_pDoneDate_cond As String, ByVal in_pValidatedDate_cond As String, ByVal in_pDose_cond As String, ByVal in_pHasAMemo_cond As String, ByVal in_pValidator_cond As String, _
ByVal in_pOrder_cond As String, ByVal in_pStatus_cond As String, ByVal in_pValidatorGroup_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsTaskFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer

Public Declare Function PcsTaskGetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTaskGetDoneDate Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTaskGetDose Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsTaskGetHasAMemo Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_pHasAMemo As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetOrder Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetStatus Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetToDoDate Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTaskGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetValidatedDate Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTaskGetValidator Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetValidatorGroup Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetValidatorGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetValidatorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsTaskSetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTaskSetDoneDate Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, _
ByVal in_second As Long) As Integer
Public Declare Function PcsTaskSetDose Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsTaskSetHasAMemo Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_pHasAMemo As String) As Integer
Public Declare Function PcsTaskSetOrder Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskSetStatus Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskSetToDoDate Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, _
ByVal in_second As Long) As Integer
Public Declare Function PcsTaskSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskSetValidatedDate Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTaskSetValidator Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetValidatorGroup Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetValidatorGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskSetValidatorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironmentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsThEnvironmentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, _
ByVal in_pEnvironmentType_cond As String, ByVal in_pEnvironment_cond As String) As Integer
Public Declare Function PcsThEnvironmentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnded Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnvironmentType Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnvironmentTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironmentGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsThEnvironmentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsThEnvironmentSetEnded Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsThEnvironmentSetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironmentSetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironmentSetEnvironmentType Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironmentSetEnvironmentTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironmentSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pThEnvironment_cond As String, _
ByVal in_pStaff_cond As String, ByVal in_pLocation_cond As String) As Integer
Public Declare Function PcsThEnvironStaffLocationFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetLocation Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetLocationIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetThEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetThEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationSetLocation Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationSetLocationIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironStaffLocationSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironStaffLocationSetThEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationSetThEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTimestampAlloc Lib "PCSDB500D.DLL" (ByRef out_phtimestamp As Long) As Integer
Public Declare Function PcsTimestampFree Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long) As Integer
Public Declare Function PcsTimestampGetAll Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTimestampGetDay Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByRef out_pday As Long) As Integer
Public Declare Function PcsTimestampGetHour Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByRef out_phour As Long) As Integer
Public Declare Function PcsTimestampGetMinute Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByRef out_pminute As Long) As Integer
Public Declare Function PcsTimestampGetMonth Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByRef out_pmonth As Long) As Integer
Public Declare Function PcsTimestampGetSecond Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTimestampGetYear Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByRef out_pyear As Long) As Integer
Public Declare Function PcsTimestampSetAll Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTimestampSetDay Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByVal in_day As Long) As Integer
Public Declare Function PcsTimestampSetHour Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByVal in_hour As Long) As Integer
Public Declare Function PcsTimestampSetMinute Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByVal in_minute As Long) As Integer
Public Declare Function PcsTimestampSetMonth Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByVal in_month As Long) As Integer
Public Declare Function PcsTimestampSetSecond Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTimestampSetYear Lib "PCSDB500D.DLL" (ByVal in_htimestamp As Long, ByVal in_year As Long) As Integer
Public Declare Function PcsTreatmentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsTreatmentDataAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsTreatmentDataFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pInstant_cond As String, ByVal in_pDose_cond As String, _
ByVal in_pRate_cond As String, ByVal in_pTreatment_cond As String, ByVal in_pPicisData_cond As String, ByVal in_pStatus_cond As String, ByVal in_pAdditive_cond As String, ByVal in_pDoseUnit_cond As String, _
ByVal in_pRateUnit_cond As String, ByVal in_pOrder_cond As String) As Integer
Public Declare Function PcsTreatmentDataFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsTreatmentDataGetAdditive Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetAdditiveIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetDose Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsTreatmentDataGetDoseUnit Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetDoseUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetInstant Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTreatmentDataGetOrder Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetRate Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsTreatmentDataGetRateUnit Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetRateUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetStatus Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsTreatmentDataSetAdditive Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetAdditiveIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetDose Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsTreatmentDataSetDoseUnit Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetDoseUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetInstant Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTreatmentDataSetOrder Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetRate Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsTreatmentDataSetRateUnit Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetRateUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetStatus Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pBrandName_cond As String, ByVal in_pGenericName_cond As String, _
ByVal in_pIsDeleted_cond As String, ByVal in_pFamily_cond As String, ByVal in_pGroup_cond As String) As Integer
Public Declare Function PcsTreatmentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsTreatmentGetBrandName Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pBrandName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentGetFamily Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentGetFamilyIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentGetGenericName Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pGenericName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentGetGroup Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentGetGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsTreatmentSetBrandName Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pBrandName As String) As Integer
Public Declare Function PcsTreatmentSetFamily Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentSetFamilyIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentSetGenericName Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pGenericName As String) As Integer
Public Declare Function PcsTreatmentSetGroup Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentSetGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsUiAdmissionSelect Lib "PCSDB500D.DLL" (ByVal in_hadmlist As Long, ByRef out_phadmission As Long, ByVal in_hwnd As Long) As Integer
Public Declare Function PcsUiDbObjectCreateNew Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pData As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiDbObjectCreateShowProperties Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pData As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiDbObjectLocate Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hparent As Long, ByVal in_plocatedata As Long) As Integer
Public Declare Function PcsUiDbObjectShowMenu Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hwnd As Long, ByVal in_menuflags As Long, ByVal in_x As Long, ByVal in_y As Long) As Integer
Public Declare Function PcsUiEditPrecautionsList Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal begin_isolation_list As Long, ByVal result_isolation_list As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiErrorHandlerPromptText Lib "PCSERR500D.DLL" (ByVal in_handler As Long, ByVal in_hparent As Long, ByVal in_ptext As String, ByVal in_type As Long) As Integer
Public Declare Function PcsUiLogGetData Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_hparent As Long, ByVal in_flags As Long) As Integer
Public Declare Function PcsUiPatientLocate Lib "PCSDB500D.DLL" (ByVal in_hpat As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiPatientShowDemographics Lib "PCSDB500D.DLL" (ByVal in_hpat As Long) As Integer
Public Declare Function PcsUiPrecautionsMaintenance Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiPromptForText Lib "PCSDB500D.DLL" (ByVal in_ptitle As String, ByVal in_pquestion As String, ByVal in_pdefault As String, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsUnitConvert Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_hFromUnit As Long, ByVal in_FromValue As Double, ByVal in_hToUnit As Long, _
ByRef out_pToValue As Double) As Integer
Public Declare Function PcsUnitConvertDboid Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_FromDboid As String, ByVal in_FromValue As Double, ByVal in_ToDboid As String, _
ByRef out_pToValue As Double) As Integer
Public Declare Function PcsUnitConvertToReferenceDboid Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_FromDboid As String, ByVal in_FromValue As Double, ByRef out_pToValue As Double) As Integer
Public Declare Function PcsUnitFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pSymbol_cond As String, _
ByVal in_pIsDeleted_cond As String, ByVal in_pConversionFactor_cond As String, ByVal in_pUnitType_cond As String) As Integer
Public Declare Function PcsUnitFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsUnitGetConversionFactor Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsUnitGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitGetSymbol Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal out_pSymbol As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitGetUnitType Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsUnitGetUnitTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsUnitSetConversionFactor Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsUnitSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsUnitSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsUnitSetSymbol Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal in_pSymbol As String) As Integer
Public Declare Function PcsUnitSetUnitType Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsUnitSetUnitTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHUNIT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsUnitTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsUnitTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsUnitTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsUnitTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHUNITTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsUnitTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHUNITTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsUtilAppGetReleaseStatus Lib "PCSDB500D.DLL" (ByVal in_AppId As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsUtilAppIsInstalled Lib "PCSDB500D.DLL" (ByVal in_AppId As Long, ByRef out_pIsInstalled As Long) As Integer
Public Declare Function PcsUtilBin2Text Lib "PCSDB500D.DLL" (ByVal in_pData As String, ByVal in_num_data_code As Long, ByVal out_pData As String, ByVal in_num_data As Long) As Integer
Public Declare Function PcsUtilConstructAgeString Lib "PCSDB500D.DLL" (ByVal out_pstring As String, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilConstructDateString Lib "PCSDB500D.DLL" (ByVal out_pstring As String, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilConstructDateTimeString Lib "PCSDB500D.DLL" (ByVal out_pstring As String, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilCrypt Lib "PCSDB500D.DLL" (ByVal in_pData As String, ByVal in_num_in As Long, ByVal in_pkey As String, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilCryptGetLength Lib "PCSDB500D.DLL" (ByVal in_num_in As Long, ByRef out_pnum_chars As Long) As Integer
Public Declare Function PcsUtilDateCompare Lib "PCSDB500D.DLL" (ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long, _
ByVal in_year2 As Long, ByVal in_month2 As Long, ByVal in_day2 As Long, ByVal in_hour2 As Long, ByVal in_minute2 As Long, ByVal in_second2 As Long, ByRef out_plCompRes As Long) As Integer
Public Declare Function PcsUtilDateStringConstruct Lib "PCSDB500D.DLL" (ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_min As Long, _
ByVal in_sec As Long, ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilDateStringDiscompose Lib "PCSDB500D.DLL" (ByVal in_pstring As String, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pmin As Long, ByRef out_psec As Long) As Integer
Public Declare Function PcsUtilDateToString Lib "PCSDB500D.DLL" (ByVal in_format As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_min As Long, _
ByVal in_second As Long, ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilDrawBitmapInRect Lib "PCSDB500D.DLL" (ByVal in_hdc As Long, ByRef in_prect As Long, ByVal in_hbitmap As Long) As Integer
Public Declare Function PcsUtilDrawBitmapInRectWithROP Lib "PCSDB500D.DLL" (ByVal in_hdc As Long, ByRef in_prect As Long, ByVal in_hbitmap As Long, ByVal in_rop As Long) As Integer
Public Declare Function PcsUtilGetBitmapSizes Lib "PCSDB500D.DLL" (ByVal in_bitmap As Long, ByRef out_size_x As Long, ByRef out_size_y As Long) As Integer
Public Declare Function PcsUtilGetCharacterSize Lib "PCSDB500D.DLL" (ByVal in_hdc As Long, ByVal in_hfont As Long, ByRef out_pwidth As Long, ByRef out_pheight As Long) As Integer
Public Declare Function PcsUtilGetTextLineSize Lib "PCSDB500D.DLL" (ByVal in_hdc As Long, ByVal in_hfont As Long, ByVal in_ptext As String, ByRef out_pwidth As Long, ByRef out_pheight As Long) As Integer
Public Declare Function PcsUtilGetTextMultiLineSize Lib "PCSDB500D.DLL" (ByVal in_hdc As Long, ByVal in_hfont As Long, ByVal in_ptext As String, ByRef out_pwidth As Long, ByRef out_pheight As Long, _
ByVal width_line As Long) As Integer
Public Declare Function PcsUtilGetTimeStamp Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByRef in_year As Long, ByRef in_month As Long, ByRef in_day As Long, ByRef in_hour As Long, ByRef in_min As Long, _
ByRef in_sec As Long) As Integer
Public Declare Function PcsUtilIsRectangleSeen Lib "PCSDB500D.DLL" (ByVal in_hdc As Long, ByRef in_prect As Long) As Integer
Public Declare Function PcsUtilStringGetLength Lib "PCSDB500D.DLL" (ByVal in_pstring As String, ByRef out_plength As Long) As Integer
Public Declare Function PcsUtilStringToDate Lib "PCSDB500D.DLL" (ByVal in_pstring As String, ByVal in_format As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pmin As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsUtilStringTrimRight Lib "PCSDB500D.DLL" (ByVal inout_pstring As String) As Integer
Public Declare Function PcsUtilSubstituteInString Lib "PCSDB500D.DLL" (ByVal in_pstring As String, ByVal in_pformula As String, ByVal in_pnew_value As String, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringAddItem Lib "PCSDB500D.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_pitem As String, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringElliminateDifferentItems Lib "PCSDB500D.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringEraseItems Lib "PCSDB500D.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_item_index As Long, ByVal in_number_of_items As Long, _
ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringGetItem Lib "PCSDB500D.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_num As Long, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringGetNumItems Lib "PCSDB500D.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByRef out_pnum As Long) As Integer
Public Declare Function PcsUtilTabbedStringInsertItem Lib "PCSDB500D.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_pitem As String, ByVal in_index As Long, _
ByVal in_number_of_times As Long, ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringSetItem Lib "PCSDB500D.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_num As Long, ByVal in_pnew_value As String, _
ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilText2Bin Lib "PCSDB500D.DLL" (ByVal in_pbuffer As String, ByVal out_pData As String, ByVal num_data As Long) As Integer
Public Declare Function PcsUtilUncrypt Lib "PCSDB500D.DLL" (ByVal in_pbuffer As String, ByVal in_pkey As String, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilUncryptGetLength Lib "PCSDB500D.DLL" (ByVal in_pbuffer As String, ByRef out_pnum_chars As Long) As Integer
Public Declare Function PcsVersionGet Lib "PCSDB500D.DLL" (ByRef out_pbig_version As Long, ByRef out_pminor_version As Long) As Integer

Public Declare Function PcsUtilDateToTime_t Lib "PCSDB500D.DLL" (ByVal in_date As Date, ByRef out_pTime As Long) As Integer
Public Declare Function PcsUtilTime_tToDate Lib "PCSDB500D.DLL" (ByVal in_Time As Long, ByRef out_date As Date) As Integer

#Else

Public Declare Function PcsTaskAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsTaskDataAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsTaskDataFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIndex_cond As String, ByVal in_pData_cond As String, _
ByVal in_pTask_cond As String) As Integer
Public Declare Function PcsTaskDataFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsTaskDataGetData Lib "PCSDB500.DLL" (ByVal in_hHTASKDATA As Long, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskDataGetIndex Lib "PCSDB500.DLL" (ByVal in_hHTASKDATA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsTaskDataGetTask Lib "PCSDB500.DLL" (ByVal in_hHTASKDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskDataGetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTASKDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskDataListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsTaskDataSetData Lib "PCSDB500.DLL" (ByVal in_hHTASKDATA As Long, ByVal in_pData As String) As Integer
Public Declare Function PcsTaskDataSetIndex Lib "PCSDB500.DLL" (ByVal in_hHTASKDATA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsTaskDataSetTask Lib "PCSDB500.DLL" (ByVal in_hHTASKDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskDataSetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTASKDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pCreationDate_cond As String, ByVal in_pToDoDate_cond As String, _
ByVal in_pDoneDate_cond As String, ByVal in_pValidatedDate_cond As String, ByVal in_pDose_cond As String, ByVal in_pHasAMemo_cond As String, ByVal in_pValidator_cond As String, _
ByVal in_pOrder_cond As String, ByVal in_pStatus_cond As String, ByVal in_pValidatorGroup_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsTaskFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsTaskGetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTaskGetDoneDate Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTaskGetDose Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsTaskGetHasAMemo Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_pHasAMemo As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetOrder Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetStatus Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetToDoDate Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTaskGetUnit Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetValidatedDate Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTaskGetValidator Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetValidatorGroup Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTaskGetValidatorGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskGetValidatorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTaskListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsTaskSetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTaskSetDoneDate Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, _
ByVal in_second As Long) As Integer
Public Declare Function PcsTaskSetDose Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsTaskSetHasAMemo Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_pHasAMemo As String) As Integer
Public Declare Function PcsTaskSetOrder Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskSetStatus Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskSetToDoDate Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, _
ByVal in_second As Long) As Integer
Public Declare Function PcsTaskSetUnit Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskSetValidatedDate Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTaskSetValidator Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetValidatorGroup Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTaskSetValidatorGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTaskSetValidatorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTSK As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironmentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsThEnvironmentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, _
ByVal in_pEnvironmentType_cond As String, ByVal in_pEnvironment_cond As String) As Integer
Public Declare Function PcsThEnvironmentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnded Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnvironmentType Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironmentGetEnvironmentTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironmentGetStarted Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsThEnvironmentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsThEnvironmentSetEnded Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsThEnvironmentSetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironmentSetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironmentSetEnvironmentType Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironmentSetEnvironmentTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironmentSetStarted Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pThEnvironment_cond As String, _
ByVal in_pStaff_cond As String, ByVal in_pLocation_cond As String) As Integer
Public Declare Function PcsThEnvironStaffLocationFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetLocation Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetLocationIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetStaff Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetThEnvironment Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationGetThEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationSetLocation Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationSetLocationIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironStaffLocationSetStaff Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsThEnvironStaffLocationSetThEnvironment Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsThEnvironStaffLocationSetThEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTHENVIRONSTAFFLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTimestampAlloc Lib "PCSDB500.DLL" (ByRef out_phtimestamp As Long) As Integer
Public Declare Function PcsTimestampFree Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long) As Integer
Public Declare Function PcsTimestampGetAll Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTimestampGetDay Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByRef out_pday As Long) As Integer
Public Declare Function PcsTimestampGetHour Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByRef out_phour As Long) As Integer
Public Declare Function PcsTimestampGetMinute Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByRef out_pminute As Long) As Integer
Public Declare Function PcsTimestampGetMonth Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByRef out_pmonth As Long) As Integer
Public Declare Function PcsTimestampGetSecond Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTimestampGetYear Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByRef out_pyear As Long) As Integer
Public Declare Function PcsTimestampSetAll Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTimestampSetDay Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByVal in_day As Long) As Integer
Public Declare Function PcsTimestampSetHour Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByVal in_hour As Long) As Integer
Public Declare Function PcsTimestampSetMinute Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByVal in_minute As Long) As Integer
Public Declare Function PcsTimestampSetMonth Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByVal in_month As Long) As Integer
Public Declare Function PcsTimestampSetSecond Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTimestampSetYear Lib "PCSDB500.DLL" (ByVal in_htimestamp As Long, ByVal in_year As Long) As Integer
Public Declare Function PcsTreatmentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsTreatmentDataAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsTreatmentDataFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pInstant_cond As String, ByVal in_pDose_cond As String, _
ByVal in_pRate_cond As String, ByVal in_pTreatment_cond As String, ByVal in_pPicisData_cond As String, ByVal in_pStatus_cond As String, ByVal in_pAdditive_cond As String, ByVal in_pDoseUnit_cond As String, _
ByVal in_pRateUnit_cond As String, ByVal in_pOrder_cond As String) As Integer
Public Declare Function PcsTreatmentDataFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsTreatmentDataGetAdditive Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetAdditiveIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetDose Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsTreatmentDataGetDoseUnit Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetDoseUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetInstant Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsTreatmentDataGetOrder Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetPicisData Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetRate Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsTreatmentDataGetRateUnit Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetRateUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetStatus Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentDataListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsTreatmentDataSetAdditive Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetAdditiveIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetDose Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsTreatmentDataSetDoseUnit Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetDoseUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetInstant Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsTreatmentDataSetOrder Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetPicisData Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetRate Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsTreatmentDataSetRateUnit Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetRateUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetStatus Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentDataSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentDataSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pBrandName_cond As String, ByVal in_pGenericName_cond As String, _
ByVal in_pIsDeleted_cond As String, ByVal in_pFamily_cond As String, ByVal in_pGroup_cond As String) As Integer
Public Declare Function PcsTreatmentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsTreatmentGetBrandName Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pBrandName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentGetFamily Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentGetFamilyIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentGetGenericName Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pGenericName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentGetGroup Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsTreatmentGetGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsTreatmentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsTreatmentSetBrandName Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pBrandName As String) As Integer
Public Declare Function PcsTreatmentSetFamily Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentSetFamilyIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentSetGenericName Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pGenericName As String) As Integer
Public Declare Function PcsTreatmentSetGroup Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsTreatmentSetGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsTreatmentSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHTREATMENT As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsUiAdmissionSelect Lib "PCSDB500.DLL" (ByVal in_hadmlist As Long, ByRef out_phadmission As Long, ByVal in_hwnd As Long) As Integer
Public Declare Function PcsUiDbObjectCreateNew Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pData As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiDbObjectCreateShowProperties Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pData As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiDbObjectLocate Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hparent As Long, ByVal in_plocatedata As Long) As Integer
Public Declare Function PcsUiDbObjectShowMenu Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hwnd As Long, ByVal in_menuflags As Long, ByVal in_x As Long, ByVal in_y As Long) As Integer
Public Declare Function PcsUiEditPrecautionsList Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal begin_isolation_list As Long, ByVal result_isolation_list As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiErrorHandlerPromptText Lib "PCSERR500.DLL" (ByVal in_handler As Long, ByVal in_hparent As Long, ByVal in_ptext As String, ByVal in_type As Long) As Integer
Public Declare Function PcsUiLogGetData Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_hparent As Long, ByVal in_flags As Long) As Integer
Public Declare Function PcsUiPatientLocate Lib "PCSDB500.DLL" (ByVal in_hpat As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiPatientShowDemographics Lib "PCSDB500.DLL" (ByVal in_hpat As Long) As Integer
Public Declare Function PcsUiPrecautionsMaintenance Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_hparent As Long) As Integer
Public Declare Function PcsUiPromptForText Lib "PCSDB500.DLL" (ByVal in_ptitle As String, ByVal in_pquestion As String, ByVal in_pdefault As String, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsUnitConvert Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_hFromUnit As Long, ByVal in_FromValue As Double, ByVal in_hToUnit As Long, _
ByRef out_pToValue As Double) As Integer
Public Declare Function PcsUnitConvertDboid Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_FromDboid As String, ByVal in_FromValue As Double, ByVal in_ToDboid As String, _
ByRef out_pToValue As Double) As Integer
Public Declare Function PcsUnitConvertToReferenceDboid Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_FromDboid As String, ByVal in_FromValue As Double, ByRef out_pToValue As Double) As Integer
Public Declare Function PcsUnitFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pSymbol_cond As String, _
ByVal in_pIsDeleted_cond As String, ByVal in_pConversionFactor_cond As String, ByVal in_pUnitType_cond As String) As Integer
Public Declare Function PcsUnitFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsUnitGetConversionFactor Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsUnitGetDescription Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitGetSymbol Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal out_pSymbol As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitGetUnitType Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsUnitGetUnitTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsUnitSetConversionFactor Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsUnitSetDescription Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsUnitSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsUnitSetSymbol Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal in_pSymbol As String) As Integer
Public Declare Function PcsUnitSetUnitType Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsUnitSetUnitTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHUNIT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsUnitTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsUnitTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsUnitTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsUnitTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHUNITTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUnitTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsUnitTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHUNITTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsUtilAppGetReleaseStatus Lib "PCSDB500.DLL" (ByVal in_AppId As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsUtilAppIsInstalled Lib "PCSDB500.DLL" (ByVal in_AppId As Long, ByRef out_pIsInstalled As Long) As Integer
Public Declare Function PcsUtilBin2Text Lib "PCSDB500.DLL" (ByVal in_pData As String, ByVal in_num_data_code As Long, ByVal out_pData As String, ByVal in_num_data As Long) As Integer
Public Declare Function PcsUtilConstructAgeString Lib "PCSDB500.DLL" (ByVal out_pstring As String, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilConstructDateString Lib "PCSDB500.DLL" (ByVal out_pstring As String, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilConstructDateTimeString Lib "PCSDB500.DLL" (ByVal out_pstring As String, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilCrypt Lib "PCSDB500.DLL" (ByVal in_pData As String, ByVal in_num_in As Long, ByVal in_pkey As String, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilCryptGetLength Lib "PCSDB500.DLL" (ByVal in_num_in As Long, ByRef out_pnum_chars As Long) As Integer
Public Declare Function PcsUtilDateCompare Lib "PCSDB500.DLL" (ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long, _
ByVal in_year2 As Long, ByVal in_month2 As Long, ByVal in_day2 As Long, ByVal in_hour2 As Long, ByVal in_minute2 As Long, ByVal in_second2 As Long, ByRef out_plCompRes As Long) As Integer
Public Declare Function PcsUtilDateStringConstruct Lib "PCSDB500.DLL" (ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_min As Long, _
ByVal in_sec As Long, ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilDateStringDiscompose Lib "PCSDB500.DLL" (ByVal in_pstring As String, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pmin As Long, ByRef out_psec As Long) As Integer
Public Declare Function PcsUtilDateToString Lib "PCSDB500.DLL" (ByVal in_format As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_min As Long, _
ByVal in_second As Long, ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilDrawBitmapInRect Lib "PCSDB500.DLL" (ByVal in_hdc As Long, ByRef in_prect As Long, ByVal in_hbitmap As Long) As Integer
Public Declare Function PcsUtilDrawBitmapInRectWithROP Lib "PCSDB500.DLL" (ByVal in_hdc As Long, ByRef in_prect As Long, ByVal in_hbitmap As Long, ByVal in_rop As Long) As Integer
Public Declare Function PcsUtilGetBitmapSizes Lib "PCSDB500.DLL" (ByVal in_bitmap As Long, ByRef out_size_x As Long, ByRef out_size_y As Long) As Integer
Public Declare Function PcsUtilGetCharacterSize Lib "PCSDB500.DLL" (ByVal in_hdc As Long, ByVal in_hfont As Long, ByRef out_pwidth As Long, ByRef out_pheight As Long) As Integer
Public Declare Function PcsUtilGetTextLineSize Lib "PCSDB500.DLL" (ByVal in_hdc As Long, ByVal in_hfont As Long, ByVal in_ptext As String, ByRef out_pwidth As Long, ByRef out_pheight As Long) As Integer
Public Declare Function PcsUtilGetTextMultiLineSize Lib "PCSDB500.DLL" (ByVal in_hdc As Long, ByVal in_hfont As Long, ByVal in_ptext As String, ByRef out_pwidth As Long, ByRef out_pheight As Long, _
ByVal width_line As Long) As Integer
Public Declare Function PcsUtilGetTimeStamp Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByRef in_year As Long, ByRef in_month As Long, ByRef in_day As Long, ByRef in_hour As Long, ByRef in_min As Long, _
ByRef in_sec As Long) As Integer
Public Declare Function PcsUtilIsRectangleSeen Lib "PCSDB500.DLL" (ByVal in_hdc As Long, ByRef in_prect As Long) As Integer
Public Declare Function PcsUtilStringGetLength Lib "PCSDB500.DLL" (ByVal in_pstring As String, ByRef out_plength As Long) As Integer
Public Declare Function PcsUtilStringToDate Lib "PCSDB500.DLL" (ByVal in_pstring As String, ByVal in_format As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pmin As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsUtilStringTrimRight Lib "PCSDB500.DLL" (ByVal inout_pstring As String) As Integer
Public Declare Function PcsUtilSubstituteInString Lib "PCSDB500.DLL" (ByVal in_pstring As String, ByVal in_pformula As String, ByVal in_pnew_value As String, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringAddItem Lib "PCSDB500.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_pitem As String, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringElliminateDifferentItems Lib "PCSDB500.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringEraseItems Lib "PCSDB500.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_item_index As Long, ByVal in_number_of_items As Long, _
ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringGetItem Lib "PCSDB500.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_num As Long, ByVal out_pstring As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringGetNumItems Lib "PCSDB500.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByRef out_pnum As Long) As Integer
Public Declare Function PcsUtilTabbedStringInsertItem Lib "PCSDB500.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_pitem As String, ByVal in_index As Long, _
ByVal in_number_of_times As Long, ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilTabbedStringSetItem Lib "PCSDB500.DLL" (ByVal in_separator As Byte, ByVal in_pstring As String, ByVal in_num As Long, ByVal in_pnew_value As String, _
ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilText2Bin Lib "PCSDB500.DLL" (ByVal in_pbuffer As String, ByVal out_pData As String, ByVal num_data As Long) As Integer
Public Declare Function PcsUtilUncrypt Lib "PCSDB500.DLL" (ByVal in_pbuffer As String, ByVal in_pkey As String, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsUtilUncryptGetLength Lib "PCSDB500.DLL" (ByVal in_pbuffer As String, ByRef out_pnum_chars As Long) As Integer
Public Declare Function PcsVersionGet Lib "PCSDB500.DLL" (ByRef out_pbig_version As Long, ByRef out_pminor_version As Long) As Integer

Public Declare Function PcsUtilDateToTime_t Lib "PCSDB500.DLL" (ByVal in_date As Date, ByRef out_pTime As Long) As Integer
Public Declare Function PcsUtilTime_tToDate Lib "PCSDB500.DLL" (ByVal in_Time As Long, ByRef out_date As Date) As Integer


#End If
