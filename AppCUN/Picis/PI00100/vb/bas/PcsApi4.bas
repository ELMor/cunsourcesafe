Attribute VB_Name = "Module9"
#If DebugVersion Then

Public Declare Function PcsMemoAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsMemoFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pStarted_cond As String, _
ByVal in_pScheduleTime_cond As String, ByVal in_pStaff_cond As String, ByVal in_pTableRef_cond As String) As Integer
Public Declare Function PcsMemoFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsMemoGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMemoGetScheduleTime Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsMemoGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsMemoGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMemoGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsMemoGetTableRefIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMemoListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsMemoSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsMemoSetScheduleTime Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsMemoSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsMemoSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsMemoSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, _
ByVal in_second As Long) As Integer
Public Declare Function PcsMemoSetTableRefIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHMEMO As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsModifyScreenBitmap Lib "PCSDB500D.DLL" (ByVal in_hbitmap As Long, ByVal in_light As Long, ByVal in_dark As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pNeedle_cond As String, ByVal in_pLocation_cond As String, _
ByVal in_pAttempts_cond As String, ByVal in_pMultiple_cond As String, ByVal in_pInjectionTime_cond As String, ByVal in_pComments_cond As String, ByVal in_pAmount_cond As String, _
ByVal in_pAnesthetic_cond As String, ByVal in_pEpipherine_cond As String, ByVal in_pBlock_cond As String, ByVal in_pAmountUnit_cond As String, ByVal in_pAdmission_cond As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAmount Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAmountUnit Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAmountUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAnesthetic Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pAnesthetic As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAttempts Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetBlock Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pBlock As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetComments Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pComments As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetEpipherine Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pEpipherine As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetInjectionTime Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetLocation Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pLocation As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetMultiple Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pMultiple As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetNeedle Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pNeedle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAmount Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAmountUnit Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAmountUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAnesthetic Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pAnesthetic As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAttempts Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetBlock Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pBlock As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetComments Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pComments As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetEpipherine Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pEpipherine As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetInjectionTime Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetLocation Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pLocation As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetMultiple Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pMultiple As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetNeedle Lib "PCSDB500D.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pNeedle As String) As Integer
Public Declare Function PcsObjectAlloc Lib "PCSDB500D.DLL" (ByRef out_phobject As Long, ByVal in_pclassname As String) As Integer
Public Declare Function PcsObjectFree Lib "PCSDB500D.DLL" (ByVal in_hobject As Long) As Integer
Public Declare Function PcsObjectGetClassName Lib "PCSDB500D.DLL" (ByVal in_hobject As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsObjectGetUserData Lib "PCSDB500D.DLL" (ByVal in_hobject As Long, ByRef out_puserdata As Long) As Integer
Public Declare Function PcsObjectGetUserText Lib "PCSDB500D.DLL" (ByVal in_hobject As Long, ByVal out_ptext As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsObjectIsValid Lib "PCSDB500D.DLL" (ByVal in_hobject As Long, ByRef out_pyes As Long) As Integer
Public Declare Function PcsObjectSetUserData Lib "PCSDB500D.DLL" (ByVal in_hobject As Long, ByVal in_userdata As Long) As Integer
Public Declare Function PcsObjectSetUserText Lib "PCSDB500D.DLL" (ByVal in_hobject As Long, ByVal in_ptext As String) As Integer
Public Declare Function PcsOrderAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderDataAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderDataFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIndex_cond As String, ByVal in_pData_cond As String, _
ByVal in_pOrder_cond As String) As Integer
Public Declare Function PcsOrderDataFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderDataGetData Lib "PCSDB500D.DLL" (ByVal in_hHORDERDATA As Long, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderDataGetIndex Lib "PCSDB500D.DLL" (ByVal in_hHORDERDATA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsOrderDataGetOrder Lib "PCSDB500D.DLL" (ByVal in_hHORDERDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderDataGetOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDERDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderDataListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderDataSetData Lib "PCSDB500D.DLL" (ByVal in_hHORDERDATA As Long, ByVal in_pData As String) As Integer
Public Declare Function PcsOrderDataSetIndex Lib "PCSDB500D.DLL" (ByVal in_hHORDERDATA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsOrderDataSetOrder Lib "PCSDB500D.DLL" (ByVal in_hHORDERDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderDataSetOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDERDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pCreationDate_cond As String, _
ByVal in_pCondition_cond As String, ByVal in_pInitDate_cond As String, ByVal in_pLastDate_cond As String, ByVal in_pHasAMemo_cond As String, ByVal in_pHigherDose_cond As String, _
ByVal in_pLowerDose_cond As String, ByVal in_pIsFromReSchedule_cond As String, ByVal in_pExtraInfo_cond As String, ByVal in_pPicisData_cond As String, ByVal in_pCreator_cond As String, _
ByVal in_pOrderType_cond As String, ByVal in_pStatus_cond As String, ByVal in_pTreatment_cond As String, ByVal in_pOrderSet_cond As String, ByVal in_pValidatorGroup_cond As String, _
ByVal in_pUnit_cond As String, ByVal in_pRoute_cond As String, ByVal in_pForm_cond As String, ByVal in_pTaskGroup_cond As String, ByVal in_pStdOrder_cond As String) As Integer
Public Declare Function PcsOrderFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderGetCondition Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pCondition As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsOrderGetCreator Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetCreatorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetExtraInfo Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsOrderGetForm Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetFormIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetHasAMemo Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pHasAMemo As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsOrderGetInitDate Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsOrderGetIsFromReSchedule Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pIsFromReSchedule As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetLastDate Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsOrderGetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsOrderGetOrderSet Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetOrderSetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetOrderType Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetOrderTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetRoute Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetRouteIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetStatus Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetStdOrder Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetStdOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetTaskGroup Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetTaskGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetValidatorGroup Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetValidatorGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderSetAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderSetCondition Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pCondition As String) As Integer
Public Declare Function PcsOrderSetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsOrderSetCreator Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetCreatorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsOrderSetExtraInfo Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsOrderSetFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pCreationDate_cond As String, _
ByVal in_pIndex_cond As String, ByVal in_pIsDeleted_cond As String, ByVal in_pCategoryType_cond As String, ByVal in_pCreator_cond As String) As Integer
Public Declare Function PcsOrderSetForm Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetFormIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderSetGetCategoryType Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderSetGetCategoryTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderSetGetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsOrderSetGetCreator Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderSetGetCreatorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderSetGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderSetGetIndex Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsOrderSetGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderSetHasAMemo Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pHasAMemo As String) As Integer
Public Declare Function PcsOrderSetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsOrderSetInitDate Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsOrderSetIsFromReSchedule Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pIsFromReSchedule As String) As Integer
Public Declare Function PcsOrderSetLastDate Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsOrderSetListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderSetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsOrderSetOrderSet Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetOrderSetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetOrderType Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetOrderTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetRoute Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetRouteIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetSetCategoryType Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetSetCategoryTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetSetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsOrderSetSetCreator Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetSetCreatorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsOrderSetSetIndex Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsOrderSetSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHORDERSET As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsOrderSetStatus Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetStdOrder Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetStdOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetTaskGroup Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetTaskGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetValidatorGroup Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetValidatorGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderTaskStatusAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderTaskStatusFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsOrderTaskStatusFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderTaskStatusGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHORDERTASKSTATUS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderTaskStatusListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderTaskStatusSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHORDERTASKSTATUS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsOrderTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsOrderTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHORDERTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHORDERTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsPartAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPartFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pAnalysis_cond As String) As Integer
Public Declare Function PcsPartFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPartGetAnalysis Lib "PCSDB500D.DLL" (ByVal in_hHPART As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPartGetAnalysisIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPART As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPartGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHPART As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPartListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPartSetAnalysis Lib "PCSDB500D.DLL" (ByVal in_hHPART As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPartSetAnalysisIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPART As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPartSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHPART As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsPatientAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPatientFindWithIds Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_pId1 As String, ByVal in_pId2 As String, ByVal in_pId3 As String, ByRef out_hpat As Long) As Integer
Public Declare Function PcsPatientFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pFirstIdentifier_cond As String, ByVal in_pSecondIdentifier_cond As String, _
ByVal in_pThirdIdentifier_cond As String, ByVal in_pLastName_cond As String, ByVal in_pName_cond As String, ByVal in_pMiddleName_cond As String, ByVal in_pMotherMaidenName_cond As String, _
ByVal in_pPatTitle_cond As String, ByVal in_pBirthDate_cond As String, ByVal in_pEthnic_cond As String, ByVal in_pBloodGroup_cond As String, ByVal in_pSex_cond As String, ByVal in_pMarital_cond As String, _
ByVal in_pReligion_cond As String) As Integer
Public Declare Function PcsPatientFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPatientGetAllAdmissions Lib "PCSDB500D.DLL" (ByVal in_hpat As Long, ByVal in_hadmlist As Long) As Integer
Public Declare Function PcsPatientGetBirthDate Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsPatientGetBloodGroup Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetBloodGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetEthnic Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetEthnicIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetFirstIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pFirstIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetLastAdmission Lib "PCSDB500D.DLL" (ByVal in_hpat As Long, ByRef out_phadmission As Long) As Integer
Public Declare Function PcsPatientGetLastLocation Lib "PCSDB500D.DLL" (ByVal in_hpat As Long, ByRef out_phlocation As Long) As Integer
Public Declare Function PcsPatientGetLastName Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pLastName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetListFromSelect Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_PatId As String, ByVal in_PatLastName As String, _
ByVal in_PatName As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPatientGetLocation Lib "PCSDB500D.DLL" (ByVal in_hpat As Long, ByRef out_phlocation As Long) As Integer
Public Declare Function PcsPatientGetMarital Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetMaritalIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetMiddleName Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pMiddleName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetMotherMaidenName Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pMotherMaidenName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetName Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetNameFormat Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_Format As String) As Integer
Public Declare Function PcsPatientGetPatTitle Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pPatTitle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetReligion Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetReligionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetSecondIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pSecondIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetSex Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetSexIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetStatus Lib "PCSDB500D.DLL" (ByVal in_hpat As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsPatientGetThirdIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal out_pThirdIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPatientSetBirthDate Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsPatientSetBloodGroup Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetBloodGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetEthnic Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetEthnicIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetFirstIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pFirstIdentifier As String) As Integer
Public Declare Function PcsPatientSetLastName Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pLastName As String) As Integer
Public Declare Function PcsPatientSetMarital Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetMaritalIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetMiddleName Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pMiddleName As String) As Integer
Public Declare Function PcsPatientSetMotherMaidenName Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pMotherMaidenName As String) As Integer
Public Declare Function PcsPatientSetName Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pName As String) As Integer
Public Declare Function PcsPatientSetPatTitle Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pPatTitle As String) As Integer
Public Declare Function PcsPatientSetReligion Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetReligionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetSecondIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pSecondIdentifier As String) As Integer
Public Declare Function PcsPatientSetSex Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetSexIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetThirdIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPAT As Long, ByVal in_pThirdIdentifier As String) As Integer
Public Declare Function PcsPatientsGetUsingFilter Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hpatients_list As Long, ByVal in_pfilter_name As String, ByVal in_pfilter_lastname As String, _
ByVal in_pfilter_id As String) As Integer
Public Declare Function PcsPicisDataAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPicisDataFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pCPFileName_cond As String, ByVal in_pAdmission_cond As String) As Integer
Public Declare Function PcsPicisDataFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPicisDataGetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHPICISDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPicisDataGetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPICISDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPicisDataGetCPFileName Lib "PCSDB500D.DLL" (ByVal in_hHPICISDATA As Long, ByVal out_pCPFileName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPicisDataGetFirstEnvironment Lib "PCSDB500D.DLL" (ByVal in_hpicisdata As Long, ByRef out_phenvironment As Long) As Integer
Public Declare Function PcsPicisDataGetOpenEnvironment Lib "PCSDB500D.DLL" (ByVal in_hpicisdata As Long, ByRef out_phenvironment As Long) As Integer
Public Declare Function PcsPicisDataListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPicisDataSetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHPICISDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPicisDataSetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPICISDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPicisDataSetCPFileName Lib "PCSDB500D.DLL" (ByVal in_hHPICISDATA As Long, ByVal in_pCPFileName As String) As Integer
Public Declare Function PcsPrecautionAddToPatient Lib "PCSDB500D.DLL" (ByVal in_hpatient As Long, ByVal in_hisolation As Long) As Integer
Public Declare Function PcsPrecautionAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPrecautionDeleteAllFromPatient Lib "PCSDB500D.DLL" (ByVal in_hpatient As Long) As Integer
Public Declare Function PcsPrecautionDeleteFromPatient Lib "PCSDB500D.DLL" (ByVal in_hpatient As Long, ByVal in_hisolation As Long) As Integer
Public Declare Function PcsPrecautionFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pPrecautionType_cond As String) As Integer
Public Declare Function PcsPrecautionFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPrecautionGetAll Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hisolation_list As Long) As Integer
Public Declare Function PcsPrecautionGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHPRECAUTION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPrecautionGetFromPatient Lib "PCSDB500D.DLL" (ByVal in_hpatient As Long, ByVal out_hisolation_list As Long) As Integer
Public Declare Function PcsPrecautionGetPrecautionType Lib "PCSDB500D.DLL" (ByVal in_hHPRECAUTION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPrecautionGetPrecautionTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPRECAUTION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPrecautionListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPrecautionSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHPRECAUTION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsPrecautionSetPrecautionType Lib "PCSDB500D.DLL" (ByVal in_hHPRECAUTION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPrecautionSetPrecautionTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHPRECAUTION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPrecautionSetToPatient Lib "PCSDB500D.DLL" (ByVal in_hpatient As Long, ByVal out_hisolation_list As Long) As Integer
Public Declare Function PcsPrecautionTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPrecautionTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsPrecautionTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPrecautionTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHPRECAUTIONTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPrecautionTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsPrecautionTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHPRECAUTIONTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pAdmission_cond As String, _
ByVal in_pAmbulatoryStatus_cond As String) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusGetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusGetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusGetAmbulatoryStatus Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusGetAmbulatoryStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusSetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusSetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusSetAmbulatoryStatus Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusSetAmbulatoryStatusIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionChiefComplaintAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pAdmission_cond As String, _
ByVal in_pChiefComplaint_cond As String) As Integer
Public Declare Function PcsRAdmissionChiefComplaintFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintGetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintGetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintGetChiefComplaint Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintGetChiefComplaintIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintSetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintSetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionChiefComplaintSetChiefComplaint Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintSetChiefComplaintIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDate_cond As String, ByVal in_pSeverity_cond As String, _
ByVal in_pComments_cond As String, ByVal in_pAdmission_cond As String, ByVal in_pDiagnosis_cond As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetComments Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_pComments As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetDate Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetDiagnosis Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetDiagnosisIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetSeverity Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetComments Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_pComments As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetDate Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetDiagnosis Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetDiagnosisIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetSeverity Lib "PCSDB500D.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsReligionAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsReligionFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsReligionFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsReligionGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHRELIGION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsReligionListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsReligionSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHRELIGION As Long, ByVal in_pDescription As String) As Integer

#Else

Public Declare Function PcsMemoAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsMemoFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pStarted_cond As String, _
ByVal in_pScheduleTime_cond As String, ByVal in_pStaff_cond As String, ByVal in_pTableRef_cond As String) As Integer
Public Declare Function PcsMemoFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsMemoGetDescription Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMemoGetScheduleTime Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsMemoGetStaff Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsMemoGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMemoGetStarted Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsMemoGetTableRefIdentifier Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsMemoListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsMemoSetDescription Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsMemoSetScheduleTime Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsMemoSetStaff Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsMemoSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsMemoSetStarted Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, _
ByVal in_second As Long) As Integer
Public Declare Function PcsMemoSetTableRefIdentifier Lib "PCSDB500.DLL" (ByVal in_hHMEMO As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsModifyScreenBitmap Lib "PCSDB500.DLL" (ByVal in_hbitmap As Long, ByVal in_light As Long, ByVal in_dark As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pNeedle_cond As String, ByVal in_pLocation_cond As String, _
ByVal in_pAttempts_cond As String, ByVal in_pMultiple_cond As String, ByVal in_pInjectionTime_cond As String, ByVal in_pComments_cond As String, ByVal in_pAmount_cond As String, _
ByVal in_pAnesthetic_cond As String, ByVal in_pEpipherine_cond As String, ByVal in_pBlock_cond As String, ByVal in_pAmountUnit_cond As String, ByVal in_pAdmission_cond As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAdmission Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAmount Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAmountUnit Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAmountUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAnesthetic Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pAnesthetic As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetAttempts Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetBlock Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pBlock As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetComments Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pComments As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetEpipherine Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pEpipherine As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetInjectionTime Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetLocation Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pLocation As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetMultiple Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pMultiple As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaGetNeedle Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal out_pNeedle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAdmission Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAmount Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAmountUnit Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAmountUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAnesthetic Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pAnesthetic As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetAttempts Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetBlock Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pBlock As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetComments Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pComments As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetEpipherine Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pEpipherine As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetInjectionTime Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetLocation Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pLocation As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetMultiple Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pMultiple As String) As Integer
Public Declare Function PcsNerveBlockAnesthesiaSetNeedle Lib "PCSDB500.DLL" (ByVal in_hHNERVEBLOCKANESTHESIA As Long, ByVal in_pNeedle As String) As Integer
Public Declare Function PcsObjectAlloc Lib "PCSDB500.DLL" (ByRef out_phobject As Long, ByVal in_pclassname As String) As Integer
Public Declare Function PcsObjectFree Lib "PCSDB500.DLL" (ByVal in_hobject As Long) As Integer
Public Declare Function PcsObjectGetClassName Lib "PCSDB500.DLL" (ByVal in_hobject As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsObjectGetUserData Lib "PCSDB500.DLL" (ByVal in_hobject As Long, ByRef out_puserdata As Long) As Integer
Public Declare Function PcsObjectGetUserText Lib "PCSDB500.DLL" (ByVal in_hobject As Long, ByVal out_ptext As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsObjectIsValid Lib "PCSDB500.DLL" (ByVal in_hobject As Long, ByRef out_pyes As Long) As Integer
Public Declare Function PcsObjectSetUserData Lib "PCSDB500.DLL" (ByVal in_hobject As Long, ByVal in_userdata As Long) As Integer
Public Declare Function PcsObjectSetUserText Lib "PCSDB500.DLL" (ByVal in_hobject As Long, ByVal in_ptext As String) As Integer
Public Declare Function PcsOrderAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderDataAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderDataFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIndex_cond As String, ByVal in_pData_cond As String, _
ByVal in_pOrder_cond As String) As Integer
Public Declare Function PcsOrderDataFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderDataGetData Lib "PCSDB500.DLL" (ByVal in_hHORDERDATA As Long, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderDataGetIndex Lib "PCSDB500.DLL" (ByVal in_hHORDERDATA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsOrderDataGetOrder Lib "PCSDB500.DLL" (ByVal in_hHORDERDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderDataGetOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDERDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderDataListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderDataSetData Lib "PCSDB500.DLL" (ByVal in_hHORDERDATA As Long, ByVal in_pData As String) As Integer
Public Declare Function PcsOrderDataSetIndex Lib "PCSDB500.DLL" (ByVal in_hHORDERDATA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsOrderDataSetOrder Lib "PCSDB500.DLL" (ByVal in_hHORDERDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderDataSetOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDERDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pCreationDate_cond As String, _
ByVal in_pCondition_cond As String, ByVal in_pInitDate_cond As String, ByVal in_pLastDate_cond As String, ByVal in_pHasAMemo_cond As String, ByVal in_pHigherDose_cond As String, _
ByVal in_pLowerDose_cond As String, ByVal in_pIsFromReSchedule_cond As String, ByVal in_pExtraInfo_cond As String, ByVal in_pPicisData_cond As String, ByVal in_pCreator_cond As String, _
ByVal in_pOrderType_cond As String, ByVal in_pStatus_cond As String, ByVal in_pTreatment_cond As String, ByVal in_pOrderSet_cond As String, ByVal in_pValidatorGroup_cond As String, _
ByVal in_pUnit_cond As String, ByVal in_pRoute_cond As String, ByVal in_pForm_cond As String, ByVal in_pTaskGroup_cond As String, ByVal in_pStdOrder_cond As String) As Integer
Public Declare Function PcsOrderFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderGetCondition Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pCondition As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsOrderGetCreator Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetCreatorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetDescription Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetExtraInfo Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsOrderGetForm Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetFormIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetHasAMemo Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pHasAMemo As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsOrderGetInitDate Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsOrderGetIsFromReSchedule Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pIsFromReSchedule As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetLastDate Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsOrderGetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsOrderGetOrderSet Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetOrderSetIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetOrderType Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetOrderTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetPicisData Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetRoute Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetRouteIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetStatus Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetStdOrder Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetStdOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetTaskGroup Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetTaskGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetUnit Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderGetValidatorGroup Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderGetValidatorGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderSetAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderSetCondition Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pCondition As String) As Integer
Public Declare Function PcsOrderSetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsOrderSetCreator Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetCreatorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetDescription Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsOrderSetExtraInfo Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsOrderSetFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pCreationDate_cond As String, _
ByVal in_pIndex_cond As String, ByVal in_pIsDeleted_cond As String, ByVal in_pCategoryType_cond As String, ByVal in_pCreator_cond As String) As Integer
Public Declare Function PcsOrderSetForm Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetFormIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderSetGetCategoryType Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderSetGetCategoryTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderSetGetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsOrderSetGetCreator Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsOrderSetGetCreatorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderSetGetDescription Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderSetGetIndex Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsOrderSetGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderSetHasAMemo Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pHasAMemo As String) As Integer
Public Declare Function PcsOrderSetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsOrderSetInitDate Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsOrderSetIsFromReSchedule Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pIsFromReSchedule As String) As Integer
Public Declare Function PcsOrderSetLastDate Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsOrderSetListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderSetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsOrderSetOrderSet Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetOrderSetIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetOrderType Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetOrderTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetPicisData Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetRoute Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetRouteIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetSetCategoryType Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetSetCategoryTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetSetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsOrderSetSetCreator Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetSetCreatorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetSetDescription Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsOrderSetSetIndex Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsOrderSetSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHORDERSET As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsOrderSetStatus Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetStdOrder Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetStdOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetTaskGroup Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetTaskGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetUnit Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderSetValidatorGroup Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsOrderSetValidatorGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsOrderTaskStatusAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderTaskStatusFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsOrderTaskStatusFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderTaskStatusGetDescription Lib "PCSDB500.DLL" (ByVal in_hHORDERTASKSTATUS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderTaskStatusListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderTaskStatusSetDescription Lib "PCSDB500.DLL" (ByVal in_hHORDERTASKSTATUS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsOrderTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsOrderTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsOrderTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsOrderTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHORDERTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsOrderTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsOrderTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHORDERTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsPartAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPartFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pAnalysis_cond As String) As Integer
Public Declare Function PcsPartFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPartGetAnalysis Lib "PCSDB500.DLL" (ByVal in_hHPART As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPartGetAnalysisIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPART As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPartGetDescription Lib "PCSDB500.DLL" (ByVal in_hHPART As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPartListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPartSetAnalysis Lib "PCSDB500.DLL" (ByVal in_hHPART As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPartSetAnalysisIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPART As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPartSetDescription Lib "PCSDB500.DLL" (ByVal in_hHPART As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsPatientAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPatientFindWithIds Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_pId1 As String, ByVal in_pId2 As String, ByVal in_pId3 As String, ByRef out_hpat As Long) As Integer
Public Declare Function PcsPatientFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pFirstIdentifier_cond As String, ByVal in_pSecondIdentifier_cond As String, _
ByVal in_pThirdIdentifier_cond As String, ByVal in_pLastName_cond As String, ByVal in_pName_cond As String, ByVal in_pMiddleName_cond As String, ByVal in_pMotherMaidenName_cond As String, _
ByVal in_pPatTitle_cond As String, ByVal in_pBirthDate_cond As String, ByVal in_pEthnic_cond As String, ByVal in_pBloodGroup_cond As String, ByVal in_pSex_cond As String, ByVal in_pMarital_cond As String, _
ByVal in_pReligion_cond As String) As Integer
Public Declare Function PcsPatientFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPatientGetAllAdmissions Lib "PCSDB500.DLL" (ByVal in_hpat As Long, ByVal in_hadmlist As Long) As Integer
Public Declare Function PcsPatientGetBirthDate Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsPatientGetBloodGroup Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetBloodGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetEthnic Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetEthnicIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetFirstIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pFirstIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetLastAdmission Lib "PCSDB500.DLL" (ByVal in_hpat As Long, ByRef out_phadmission As Long) As Integer
Public Declare Function PcsPatientGetLastLocation Lib "PCSDB500.DLL" (ByVal in_hpat As Long, ByRef out_phlocation As Long) As Integer
Public Declare Function PcsPatientGetLastName Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pLastName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetListFromSelect Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_PatId As String, ByVal in_PatLastName As String, _
ByVal in_PatName As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPatientGetLocation Lib "PCSDB500.DLL" (ByVal in_hpat As Long, ByRef out_phlocation As Long) As Integer
Public Declare Function PcsPatientGetMarital Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetMaritalIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetMiddleName Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pMiddleName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetMotherMaidenName Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pMotherMaidenName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetName Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetNameFormat Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_Format As String) As Integer
Public Declare Function PcsPatientGetPatTitle Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pPatTitle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetReligion Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetReligionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetSecondIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pSecondIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetSex Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPatientGetSexIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientGetStatus Lib "PCSDB500.DLL" (ByVal in_hpat As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsPatientGetThirdIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal out_pThirdIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPatientListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPatientSetBirthDate Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsPatientSetBloodGroup Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetBloodGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetEthnic Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetEthnicIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetFirstIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pFirstIdentifier As String) As Integer
Public Declare Function PcsPatientSetLastName Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pLastName As String) As Integer
Public Declare Function PcsPatientSetMarital Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetMaritalIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetMiddleName Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pMiddleName As String) As Integer
Public Declare Function PcsPatientSetMotherMaidenName Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pMotherMaidenName As String) As Integer
Public Declare Function PcsPatientSetName Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pName As String) As Integer
Public Declare Function PcsPatientSetPatTitle Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pPatTitle As String) As Integer
Public Declare Function PcsPatientSetReligion Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetReligionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetSecondIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pSecondIdentifier As String) As Integer
Public Declare Function PcsPatientSetSex Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPatientSetSexIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPatientSetThirdIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPAT As Long, ByVal in_pThirdIdentifier As String) As Integer
Public Declare Function PcsPatientsGetUsingFilter Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hpatients_list As Long, ByVal in_pfilter_name As String, ByVal in_pfilter_lastname As String, _
ByVal in_pfilter_id As String) As Integer
Public Declare Function PcsPicisDataAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPicisDataFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pCPFileName_cond As String, ByVal in_pAdmission_cond As String) As Integer
Public Declare Function PcsPicisDataFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPicisDataGetAdmission Lib "PCSDB500.DLL" (ByVal in_hHPICISDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPicisDataGetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPICISDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPicisDataGetCPFileName Lib "PCSDB500.DLL" (ByVal in_hHPICISDATA As Long, ByVal out_pCPFileName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPicisDataGetFirstEnvironment Lib "PCSDB500.DLL" (ByVal in_hpicisdata As Long, ByRef out_phenvironment As Long) As Integer
Public Declare Function PcsPicisDataGetOpenEnvironment Lib "PCSDB500.DLL" (ByVal in_hpicisdata As Long, ByRef out_phenvironment As Long) As Integer
Public Declare Function PcsPicisDataListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPicisDataSetAdmission Lib "PCSDB500.DLL" (ByVal in_hHPICISDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPicisDataSetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPICISDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPicisDataSetCPFileName Lib "PCSDB500.DLL" (ByVal in_hHPICISDATA As Long, ByVal in_pCPFileName As String) As Integer
Public Declare Function PcsPrecautionAddToPatient Lib "PCSDB500.DLL" (ByVal in_hpatient As Long, ByVal in_hisolation As Long) As Integer
Public Declare Function PcsPrecautionAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPrecautionDeleteAllFromPatient Lib "PCSDB500.DLL" (ByVal in_hpatient As Long) As Integer
Public Declare Function PcsPrecautionDeleteFromPatient Lib "PCSDB500.DLL" (ByVal in_hpatient As Long, ByVal in_hisolation As Long) As Integer
Public Declare Function PcsPrecautionFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pPrecautionType_cond As String) As Integer
Public Declare Function PcsPrecautionFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPrecautionGetAll Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hisolation_list As Long) As Integer
Public Declare Function PcsPrecautionGetDescription Lib "PCSDB500.DLL" (ByVal in_hHPRECAUTION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPrecautionGetFromPatient Lib "PCSDB500.DLL" (ByVal in_hpatient As Long, ByVal out_hisolation_list As Long) As Integer
Public Declare Function PcsPrecautionGetPrecautionType Lib "PCSDB500.DLL" (ByVal in_hHPRECAUTION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsPrecautionGetPrecautionTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPRECAUTION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPrecautionListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsPrecautionSetDescription Lib "PCSDB500.DLL" (ByVal in_hHPRECAUTION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsPrecautionSetPrecautionType Lib "PCSDB500.DLL" (ByVal in_hHPRECAUTION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsPrecautionSetPrecautionTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHPRECAUTION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsPrecautionSetToPatient Lib "PCSDB500.DLL" (ByVal in_hpatient As Long, ByVal out_hisolation_list As Long) As Integer
Public Declare Function PcsPrecautionTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsPrecautionTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsPrecautionTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsPrecautionTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHPRECAUTIONTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsPrecautionTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsPrecautionTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHPRECAUTIONTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pAdmission_cond As String, _
ByVal in_pAmbulatoryStatus_cond As String) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusGetAdmission Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusGetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusGetAmbulatoryStatus Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusGetAmbulatoryStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusSetAdmission Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusSetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusSetAmbulatoryStatus Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionAmbulatoryStatusSetAmbulatoryStatusIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONAMBULATORYSTATUS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionChiefComplaintAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pAdmission_cond As String, _
ByVal in_pChiefComplaint_cond As String) As Integer
Public Declare Function PcsRAdmissionChiefComplaintFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintGetAdmission Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintGetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintGetChiefComplaint Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintGetChiefComplaintIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintSetAdmission Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintSetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionChiefComplaintSetChiefComplaint Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionChiefComplaintSetChiefComplaintIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONCHIEFCOMPLAINT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDate_cond As String, ByVal in_pSeverity_cond As String, _
ByVal in_pComments_cond As String, ByVal in_pAdmission_cond As String, ByVal in_pDiagnosis_cond As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetAdmission Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetComments Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_pComments As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetDate Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetDiagnosis Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetDiagnosisIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisGetSeverity Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetAdmission Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetComments Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_pComments As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetDate Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetDiagnosis Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetDiagnosisIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRAdmissionDiagnosisSetSeverity Lib "PCSDB500.DLL" (ByVal in_hHRADMISSIONDIAGNOSIS As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsReligionAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsReligionFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsReligionFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsReligionGetDescription Lib "PCSDB500.DLL" (ByVal in_hHRELIGION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsReligionListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsReligionSetDescription Lib "PCSDB500.DLL" (ByVal in_hHRELIGION As Long, ByVal in_pDescription As String) As Integer


#End If
