Attribute VB_Name = "Module7"
#If DebugVersion Then

Public Declare Function PcsRTDataValidatedAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTDataValidatedFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pRTData_cond As String, _
ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsRTDataValidatedFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTDataValidatedGetRTData Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataValidatedGetRTDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataValidatedGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataValidatedGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataValidatedGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRTDataValidatedListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTDataValidatedSetRTData Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataValidatedSetRTDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataValidatedSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataValidatedSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataValidatedSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pTreatment_cond As String, _
ByVal in_pFormType_cond As String) As Integer
Public Declare Function PcsRTreatmentFormTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeGetFormType Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeGetFormTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeSetFormType Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeSetFormTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentFormTypeSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentRouteTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pTreatment_cond As String, _
ByVal in_pRouteType_cond As String) As Integer
Public Declare Function PcsRTreatmentRouteTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeGetRouteType Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeGetRouteTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeSetRouteType Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeSetRouteTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentRouteTypeSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentUnitTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pTreatment_cond As String, _
ByVal in_pUnitType_cond As String) As Integer
Public Declare Function PcsRTreatmentUnitTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeGetUnitType Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeGetUnitTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentUnitTypeSetUnitType Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeSetUnitTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsScheduleAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsScheduleFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pFrequency_cond As String, _
ByVal in_pDuration_cond As String, ByVal in_pIsDeleted_cond As String) As Integer
Public Declare Function PcsScheduleFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsScheduleGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSCHEDULE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScheduleGetDuration Lib "PCSDB500D.DLL" (ByVal in_hHSCHEDULE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsScheduleGetFrequency Lib "PCSDB500D.DLL" (ByVal in_hHSCHEDULE As Long, ByVal out_pFrequency As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScheduleGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHSCHEDULE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScheduleListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsScheduleSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSCHEDULE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsScheduleSetDuration Lib "PCSDB500D.DLL" (ByVal in_hHSCHEDULE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsScheduleSetFrequency Lib "PCSDB500D.DLL" (ByVal in_hHSCHEDULE As Long, ByVal in_pFrequency As String) As Integer
Public Declare Function PcsScheduleSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHSCHEDULE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsScoreGroupAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsScoreGroupFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIndex_cond As String, _
ByVal in_pIsExclusive_cond As String, ByVal in_pTreatment_cond As String) As Integer
Public Declare Function PcsScoreGroupFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsScoreGroupGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreGroupGetIndex Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsScoreGroupGetIsExclusive Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal out_pIsExclusive As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreGroupGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsScoreGroupGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreGroupListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsScoreGroupSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsScoreGroupSetIndex Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsScoreGroupSetIsExclusive Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_pIsExclusive As String) As Integer
Public Declare Function PcsScoreGroupSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsScoreGroupSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsScoreItemAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsScoreItemFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIndex_cond As String, _
ByVal in_pLower_cond As String, ByVal in_pHigher_cond As String, ByVal in_pScoreGroup_cond As String) As Integer
Public Declare Function PcsScoreItemFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsScoreItemGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreItemGetHigher Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsScoreItemGetIndex Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsScoreItemGetLower Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsScoreItemGetScoreGroup Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsScoreItemGetScoreGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreItemListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsScoreItemSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsScoreItemSetHigher Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsScoreItemSetIndex Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsScoreItemSetLower Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsScoreItemSetScoreGroup Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsScoreItemSetScoreGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsSetColor Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long, ByVal in_update As Long) As Integer
Public Declare Function PcsSetDouble Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Double, ByVal in_update As Long) As Integer
Public Declare Function PcsSetFont Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef in_pnew_value As Long, ByVal in_update As Long) As Integer
Public Declare Function PcsSexAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsSexFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsSexFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsSexGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSEX As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSexListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsSexSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSEX As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsShiftTimeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsShiftTimeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pCreationDate_cond As String, _
ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, ByVal in_pDepartment_cond As String) As Integer
Public Declare Function PcsShiftTimeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsShiftTimeGetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsShiftTimeGetDepartment Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsShiftTimeGetDepartmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsShiftTimeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsShiftTimeGetEnded Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsShiftTimeGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsShiftTimeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsShiftTimeSetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsShiftTimeSetDepartment Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsShiftTimeSetDepartmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsShiftTimeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsShiftTimeSetEnded Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsShiftTimeSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pPatientPosition_cond As String, ByVal in_pNeedle_cond As String, _
ByVal in_pLocation_cond As String, ByVal in_pAttempts_cond As String, ByVal in_pMultiple_cond As String, ByVal in_pInjectionTime_cond As String, ByVal in_pComments_cond As String, _
ByVal in_pSpinalAgent_cond As String, ByVal in_pAmount_cond As String, ByVal in_pAmountUnit_cond As String, ByVal in_pAdmission_cond As String) As Integer
Public Declare Function PcsSpinalAnesthesiaFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAmount Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAmountUnit Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAmountUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAttempts Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetComments Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pComments As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetInjectionTime Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetLocation Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pLocation As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetMultiple Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pMultiple As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetNeedle Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pNeedle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetPatientPosition Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pPatientPosition As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetSpinalAgent Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pSpinalAgent As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAmount Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAmountUnit Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAmountUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAttempts Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetComments Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pComments As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetInjectionTime Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetLocation Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pLocation As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetMultiple Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pMultiple As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetNeedle Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pNeedle As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetPatientPosition Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pPatientPosition As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetSpinalAgent Lib "PCSDB500D.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pSpinalAgent As String) As Integer
Public Declare Function PcsSplashDisplayStartingWindow Lib "PCSDB500D.DLL" (ByVal in_ptext As String, ByVal in_bitmap As Long, ByVal in_hpalette As Long, ByVal in_delay As Long) As Integer
Public Declare Function PcsSplashDisplayStartingWindowWithResourceBitmap Lib "PCSDB500D.DLL" (ByVal in_ptext As String, ByVal in_hInstance As Long, ByVal resource_identifier As Long, _
ByVal in_delay As Long) As Integer
Public Declare Function PcsSplashEndDisplayStartingWindow Lib "PCSDB500D.DLL" () As Integer
Public Declare Function PcsSplashPicisBegin Lib "PCSDB500D.DLL" (ByVal in_AppId As Long, ByVal in_delay As Long) As Integer
Public Declare Function PcsSplashPicisEnd Lib "PCSDB500D.DLL" () As Integer
Public Declare Function PcsStaffAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStaffFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pFirstIdentifier_cond As String, ByVal in_pSecondIdentifier_cond As String, _
ByVal in_pThirdIdentifier_cond As String, ByVal in_pName_cond As String, ByVal in_pLastName_cond As String, ByVal in_pUserName_cond As String, ByVal in_pVirtualPassword_cond As String, _
ByVal in_pVIPLastUpdate_cond As String, ByVal in_pIsDeleted_cond As String, ByVal in_pInitials_cond As String, ByVal in_pStaffType_cond As String) As Integer
Public Declare Function PcsStaffFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStaffGetAllGroups Lib "PCSDB500D.DLL" (ByVal in_hstaff As Long, ByVal out_hlist As Long, ByVal in_pappdboid As String) As Integer
Public Declare Function PcsStaffGetSystemGroups Lib "PCSDB500D.DLL" (ByVal in_hstaff As Long, ByVal out_hlist As Long, ByVal in_pappdboid As String) As Integer
Public Declare Function PcsStaffGetPrescValGroups Lib "PCSDB500D.DLL" (ByVal in_hstaff As Long, ByVal out_hlist As Long, ByVal in_pappdboid As String) As Integer
Public Declare Function PcsStaffGetUserGroups Lib "PCSDB500D.DLL" (ByVal in_hstaff As Long, ByVal out_hlist As Long, ByVal in_pappdboid As String) As Integer
Public Declare Function PcsStaffGetAllUserNames Lib "PCSDB500D.DLL" (ByVal in_log As Long, ByVal out_slist As Long) As Integer
Public Declare Function PcsStaffGetFirstIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pFirstIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetFromUserName Lib "PCSDB500D.DLL" (ByVal in_hstaff As Long, ByVal in_pUsername As String) As Integer
Public Declare Function PcsStaffGetInitials Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pInitials As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetLastName Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pLastName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetName Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetNameFormat Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_Format As String) As Integer
Public Declare Function PcsStaffGetSecondIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pSecondIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetStaffType Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStaffGetStaffTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetThirdIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pThirdIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetUserName Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pUserName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetVIPLastUpdate Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsStaffGetVirtualPassword Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pVirtualPassword As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsStaffSetFirstIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pFirstIdentifier As String) As Integer
Public Declare Function PcsStaffSetInitials Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pInitials As String) As Integer
Public Declare Function PcsStaffSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsStaffSetLastName Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pLastName As String) As Integer
Public Declare Function PcsStaffSetName Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pName As String) As Integer
Public Declare Function PcsStaffSetSecondIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pSecondIdentifier As String) As Integer
Public Declare Function PcsStaffSetStaffType Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStaffSetStaffTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStaffSetThirdIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pThirdIdentifier As String) As Integer
Public Declare Function PcsStaffSetUserName Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pUsername As String) As Integer
Public Declare Function PcsStaffSetVIPLastUpdate Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsStaffSetVirtualPassword Lib "PCSDB500D.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pVirtualPassword As String) As Integer
Public Declare Function PcsStaffTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStaffTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pPreTitle_cond As String, _
ByVal in_pPostTitle_cond As String) As Integer
Public Declare Function PcsStaffTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStaffTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffTypeGetPostTitle Lib "PCSDB500D.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal out_pPostTitle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffTypeGetPreTitle Lib "PCSDB500D.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal out_pPreTitle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsStaffTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsStaffTypeSetPostTitle Lib "PCSDB500D.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal in_pPostTitle As String) As Integer
Public Declare Function PcsStaffTypeSetPreTitle Lib "PCSDB500D.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal in_pPreTitle As String) As Integer
Public Declare Function PcsStatementAlloc Lib "PCSDB500D.DLL" (ByRef out_phstatement As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsStatementBindColumnDouble Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_pdouble As Double, _
ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementBindColumnLong Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_plong As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementBindColumnShort Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_plong As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementBindColumnString Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByVal in_pstring As String, ByVal in_num_max As Long, _
ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementBindColumnTimestamp Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByVal in_htimestamp As Long, _
ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementCancel Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long) As Integer
Public Declare Function PcsStatementDescribeCol Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_lColumnNumber As Long, ByVal in_lSizeofColumnNumber As Long, ByVal out_cColumnName As String, _
ByRef out_lColumnType As Long, ByRef out_lPrecision As Long, ByRef out_lScale As Long) As Integer
Public Declare Function PcsStatementExecute Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_pquery As String) As Integer
Public Declare Function PcsStatementFetch Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long) As Integer
Public Declare Function PcsStatementFree Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long) As Integer
Public Declare Function PcsStatementGetDouble Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_pdouble As Double, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetLong Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_plong As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetNumberOfColumns Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByRef out_pnum_columns As Long) As Integer
Public Declare Function PcsStatementGetShort Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_pshort As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetString Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByVal in_pstring As String, ByVal in_num_max As Long, _
ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetTimestamp Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByVal in_htimestamp As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetTimestampFields Lib "PCSDB500D.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, _
ByRef out_pday As Long, ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStdAdditiveAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStdAdditiveFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pHigherDose_cond As String, ByVal in_pLowerDose_cond As String, _
ByVal in_pTreatment_cond As String, ByVal in_pUnit_cond As String, ByVal in_pGroup_cond As String) As Integer
Public Declare Function PcsStdAdditiveFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStdAdditiveGetGroup Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveGetGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdAdditiveGetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsStdAdditiveGetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsStdAdditiveGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdAdditiveGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdAdditiveListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsStdAdditiveSetGroup Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveSetGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdAdditiveSetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsStdAdditiveSetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsStdAdditiveSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdAdditiveSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdConditionAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStdConditionFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsStdConditionFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStdConditionGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSTDCONDITION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdConditionListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsStdConditionSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHSTDCONDITION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsStdOrderAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStdOrderDataAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStdOrderDataFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIndex_cond As String, ByVal in_pData_cond As String, _
ByVal in_pStdOrder_cond As String) As Integer
Public Declare Function PcsStdOrderDataFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStdOrderDataGetData Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderDataGetIndex Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDERDATA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsStdOrderDataGetStdOrder Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderDataGetStdOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderDataListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsStdOrderDataSetData Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal in_pData As String) As Integer
Public Declare Function PcsStdOrderDataSetIndex Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsStdOrderDataSetStdOrder Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderDataSetStdOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsADefaultOrder_cond As String, ByVal in_pCondition_cond As String, _
ByVal in_pCreationDate_cond As String, ByVal in_pLowerDose_cond As String, ByVal in_pHigherDose_cond As String, ByVal in_pIsDeleted_cond As String, ByVal in_pHasAMemo_cond As String, _
ByVal in_pTreatment_cond As String, ByVal in_pForm_cond As String, ByVal in_pRoute_cond As String, ByVal in_pUnit_cond As String, ByVal in_pSchedule_cond As String, ByVal in_pOrderType_cond As String, _
ByVal in_pPrescriberGroup_cond As String, ByVal in_pValidatorGroup_cond As String, ByVal in_pCreator_cond As String) As Integer
Public Declare Function PcsStdOrderFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStdOrderGetCondition Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pCondition As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsStdOrderGetCreator Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetCreatorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetForm Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetFormIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetHasAMemo Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pHasAMemo As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsStdOrderGetIsADefaultOrder Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pIsADefaultOrder As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsStdOrderGetOrderType Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetOrderTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetPrescriberGroup Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetPrescriberGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetRoute Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetRouteIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetSchedule Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetScheduleIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetValidatorGroup Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetValidatorGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsStdOrderSetCondition Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pCondition As String) As Integer
Public Declare Function PcsStdOrderSetCreationDate Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsStdOrderSetCreator Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetCreatorIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetForm Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetFormIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetHasAMemo Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pHasAMemo As String) As Integer
Public Declare Function PcsStdOrderSetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsStdOrderSetIsADefaultOrder Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pIsADefaultOrder As String) As Integer
Public Declare Function PcsStdOrderSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsStdOrderSetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsStdOrderSetOrderType Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetOrderTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetPrescriberGroup Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetPrescriberGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetRoute Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetRouteIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetSchedule Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetScheduleIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetValidatorGroup Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetValidatorGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer

#Else

Public Declare Function PcsRTDataValidatedAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTDataValidatedFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pRTData_cond As String, _
ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsRTDataValidatedFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTDataValidatedGetRTData Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataValidatedGetRTDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataValidatedGetStaff Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataValidatedGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataValidatedGetStarted Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRTDataValidatedListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTDataValidatedSetRTData Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataValidatedSetRTDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataValidatedSetStaff Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataValidatedSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataValidatedSetStarted Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_VALIDATED As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pTreatment_cond As String, _
ByVal in_pFormType_cond As String) As Integer
Public Declare Function PcsRTreatmentFormTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeGetFormType Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeGetFormTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeSetFormType Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeSetFormTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentFormTypeSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentFormTypeSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTFORMTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentRouteTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pTreatment_cond As String, _
ByVal in_pRouteType_cond As String) As Integer
Public Declare Function PcsRTreatmentRouteTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeGetRouteType Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeGetRouteTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeSetRouteType Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeSetRouteTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentRouteTypeSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentRouteTypeSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTROUTETYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentUnitTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pTreatment_cond As String, _
ByVal in_pUnitType_cond As String) As Integer
Public Declare Function PcsRTreatmentUnitTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeGetUnitType Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeGetUnitTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTreatmentUnitTypeSetUnitType Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTreatmentUnitTypeSetUnitTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTREATMENTUNITTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsScheduleAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsScheduleFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pFrequency_cond As String, _
ByVal in_pDuration_cond As String, ByVal in_pIsDeleted_cond As String) As Integer
Public Declare Function PcsScheduleFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsScheduleGetDescription Lib "PCSDB500.DLL" (ByVal in_hHSCHEDULE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScheduleGetDuration Lib "PCSDB500.DLL" (ByVal in_hHSCHEDULE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsScheduleGetFrequency Lib "PCSDB500.DLL" (ByVal in_hHSCHEDULE As Long, ByVal out_pFrequency As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScheduleGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHSCHEDULE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScheduleListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsScheduleSetDescription Lib "PCSDB500.DLL" (ByVal in_hHSCHEDULE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsScheduleSetDuration Lib "PCSDB500.DLL" (ByVal in_hHSCHEDULE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsScheduleSetFrequency Lib "PCSDB500.DLL" (ByVal in_hHSCHEDULE As Long, ByVal in_pFrequency As String) As Integer
Public Declare Function PcsScheduleSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHSCHEDULE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsScoreGroupAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsScoreGroupFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIndex_cond As String, _
ByVal in_pIsExclusive_cond As String, ByVal in_pTreatment_cond As String) As Integer
Public Declare Function PcsScoreGroupFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsScoreGroupGetDescription Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreGroupGetIndex Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsScoreGroupGetIsExclusive Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal out_pIsExclusive As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreGroupGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsScoreGroupGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreGroupListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsScoreGroupSetDescription Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsScoreGroupSetIndex Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsScoreGroupSetIsExclusive Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_pIsExclusive As String) As Integer
Public Declare Function PcsScoreGroupSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsScoreGroupSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSCOREGROUP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsScoreItemAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsScoreItemFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIndex_cond As String, _
ByVal in_pLower_cond As String, ByVal in_pHigher_cond As String, ByVal in_pScoreGroup_cond As String) As Integer
Public Declare Function PcsScoreItemFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsScoreItemGetDescription Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreItemGetHigher Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsScoreItemGetIndex Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsScoreItemGetLower Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsScoreItemGetScoreGroup Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsScoreItemGetScoreGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsScoreItemListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsScoreItemSetDescription Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsScoreItemSetHigher Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsScoreItemSetIndex Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsScoreItemSetLower Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsScoreItemSetScoreGroup Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsScoreItemSetScoreGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSCOREITEM As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsSetColor Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long, ByVal in_update As Long) As Integer
Public Declare Function PcsSetDouble Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Double, ByVal in_update As Long) As Integer
Public Declare Function PcsSetFont Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef in_pnew_value As Long, ByVal in_update As Long) As Integer
Public Declare Function PcsSexAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsSexFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsSexFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsSexGetDescription Lib "PCSDB500.DLL" (ByVal in_hHSEX As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSexListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsSexSetDescription Lib "PCSDB500.DLL" (ByVal in_hHSEX As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsShiftTimeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsShiftTimeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pCreationDate_cond As String, _
ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, ByVal in_pDepartment_cond As String) As Integer
Public Declare Function PcsShiftTimeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsShiftTimeGetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsShiftTimeGetDepartment Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsShiftTimeGetDepartmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsShiftTimeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsShiftTimeGetEnded Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsShiftTimeGetStarted Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsShiftTimeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsShiftTimeSetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsShiftTimeSetDepartment Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsShiftTimeSetDepartmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsShiftTimeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsShiftTimeSetEnded Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsShiftTimeSetStarted Lib "PCSDB500.DLL" (ByVal in_hHSHIFTTIME As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pPatientPosition_cond As String, ByVal in_pNeedle_cond As String, _
ByVal in_pLocation_cond As String, ByVal in_pAttempts_cond As String, ByVal in_pMultiple_cond As String, ByVal in_pInjectionTime_cond As String, ByVal in_pComments_cond As String, _
ByVal in_pSpinalAgent_cond As String, ByVal in_pAmount_cond As String, ByVal in_pAmountUnit_cond As String, ByVal in_pAdmission_cond As String) As Integer
Public Declare Function PcsSpinalAnesthesiaFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAdmission Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAmount Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAmountUnit Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAmountUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetAttempts Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetComments Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pComments As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetInjectionTime Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetLocation Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pLocation As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetMultiple Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pMultiple As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetNeedle Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pNeedle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetPatientPosition Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pPatientPosition As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaGetSpinalAgent Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal out_pSpinalAgent As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAdmission Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAmount Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAmountUnit Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAmountUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetAttempts Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetComments Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pComments As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetInjectionTime Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsSpinalAnesthesiaSetLocation Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pLocation As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetMultiple Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pMultiple As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetNeedle Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pNeedle As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetPatientPosition Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pPatientPosition As String) As Integer
Public Declare Function PcsSpinalAnesthesiaSetSpinalAgent Lib "PCSDB500.DLL" (ByVal in_hHSPINALANESTHESIA As Long, ByVal in_pSpinalAgent As String) As Integer
Public Declare Function PcsSplashDisplayStartingWindow Lib "PCSDB500.DLL" (ByVal in_ptext As String, ByVal in_bitmap As Long, ByVal in_hpalette As Long, ByVal in_delay As Long) As Integer
Public Declare Function PcsSplashDisplayStartingWindowWithResourceBitmap Lib "PCSDB500.DLL" (ByVal in_ptext As String, ByVal in_hInstance As Long, ByVal resource_identifier As Long, _
ByVal in_delay As Long) As Integer
Public Declare Function PcsSplashEndDisplayStartingWindow Lib "PCSDB500.DLL" () As Integer
Public Declare Function PcsSplashPicisBegin Lib "PCSDB500.DLL" (ByVal in_AppId As Long, ByVal in_delay As Long) As Integer
Public Declare Function PcsSplashPicisEnd Lib "PCSDB500.DLL" () As Integer
Public Declare Function PcsStaffAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStaffEncryptPassword Lib "PCSDB500.DLL" (ByVal in_pUsername As String, ByVal in_pPassword As String, ByVal out_pEncryption As String, ByVal in_lMaxChars As Long) As Integer
Public Declare Function PcsStaffFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pFirstIdentifier_cond As String, ByVal in_pSecondIdentifier_cond As String, _
ByVal in_pThirdIdentifier_cond As String, ByVal in_pName_cond As String, ByVal in_pLastName_cond As String, ByVal in_pUserName_cond As String, ByVal in_pVirtualPassword_cond As String, _
ByVal in_pVIPLastUpdate_cond As String, ByVal in_pIsDeleted_cond As String, ByVal in_pInitials_cond As String, ByVal in_pStaffType_cond As String) As Integer
Public Declare Function PcsStaffFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStaffGetAllGroups Lib "PCSDB500.DLL" (ByVal in_hstaff As Long, ByVal out_hlist As Long, ByVal in_pappdboid As String) As Integer
Public Declare Function PcsStaffGetSystemGroups Lib "PCSDB500.DLL" (ByVal in_hstaff As Long, ByVal out_hlist As Long, ByVal in_pappdboid As String) As Integer
Public Declare Function PcsStaffGetPrescValGroups Lib "PCSDB500.DLL" (ByVal in_hstaff As Long, ByVal out_hlist As Long, ByVal in_pappdboid As String) As Integer
Public Declare Function PcsStaffGetUserGroups Lib "PCSDB500.DLL" (ByVal in_hstaff As Long, ByVal out_hlist As Long, ByVal in_pappdboid As String) As Integer
Public Declare Function PcsStaffGetAllUserNames Lib "PCSDB500.DLL" (ByVal in_log As Long, ByVal out_slist As Long) As Integer
Public Declare Function PcsStaffGetFirstIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pFirstIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetFromUserName Lib "PCSDB500.DLL" (ByVal in_hstaff As Long, ByVal in_pUsername As String) As Integer
Public Declare Function PcsStaffGetInitials Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pInitials As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetLastName Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pLastName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetName Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetNameFormat Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_Format As String) As Integer
Public Declare Function PcsStaffGetSecondIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pSecondIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetStaffType Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStaffGetStaffTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetThirdIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pThirdIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetUserName Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pUserName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffGetVIPLastUpdate Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsStaffGetVirtualPassword Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal out_pVirtualPassword As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsStaffSetFirstIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pFirstIdentifier As String) As Integer
Public Declare Function PcsStaffSetInitials Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pInitials As String) As Integer
Public Declare Function PcsStaffSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsStaffSetLastName Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pLastName As String) As Integer
Public Declare Function PcsStaffSetName Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pName As String) As Integer
Public Declare Function PcsStaffSetSecondIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pSecondIdentifier As String) As Integer
Public Declare Function PcsStaffSetStaffType Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStaffSetStaffTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStaffSetThirdIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pThirdIdentifier As String) As Integer
Public Declare Function PcsStaffSetUserName Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pUsername As String) As Integer
Public Declare Function PcsStaffSetVIPLastUpdate Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsStaffSetVirtualPassword Lib "PCSDB500.DLL" (ByVal in_hHSTAFF As Long, ByVal in_pVirtualPassword As String) As Integer
Public Declare Function PcsStaffTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStaffTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pPreTitle_cond As String, _
ByVal in_pPostTitle_cond As String) As Integer
Public Declare Function PcsStaffTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStaffTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffTypeGetPostTitle Lib "PCSDB500.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal out_pPostTitle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffTypeGetPreTitle Lib "PCSDB500.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal out_pPreTitle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStaffTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsStaffTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsStaffTypeSetPostTitle Lib "PCSDB500.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal in_pPostTitle As String) As Integer
Public Declare Function PcsStaffTypeSetPreTitle Lib "PCSDB500.DLL" (ByVal in_hHSTAFFTYPE As Long, ByVal in_pPreTitle As String) As Integer
Public Declare Function PcsStatementAlloc Lib "PCSDB500.DLL" (ByRef out_phstatement As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsStatementBindColumnDouble Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_pdouble As Double, _
ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementBindColumnLong Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_plong As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementBindColumnShort Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_plong As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementBindColumnString Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByVal in_pstring As String, ByVal in_num_max As Long, _
ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementBindColumnTimestamp Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByVal in_htimestamp As Long, _
ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementCancel Lib "PCSDB500.DLL" (ByVal in_hstatement As Long) As Integer
Public Declare Function PcsStatementDescribeCol Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_lColumnNumber As Long, ByVal in_lSizeofColumnNumber As Long, ByVal out_cColumnName As String, _
ByRef out_lColumnType As Long, ByRef out_lPrecision As Long, ByRef out_lScale As Long) As Integer
Public Declare Function PcsStatementExecute Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_pquery As String) As Integer
Public Declare Function PcsStatementFetch Lib "PCSDB500.DLL" (ByVal in_hstatement As Long) As Integer
Public Declare Function PcsStatementFree Lib "PCSDB500.DLL" (ByVal in_hstatement As Long) As Integer
Public Declare Function PcsStatementGetDouble Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_pdouble As Double, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetLong Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_plong As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetNumberOfColumns Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByRef out_pnum_columns As Long) As Integer
Public Declare Function PcsStatementGetShort Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef in_pshort As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetString Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByVal in_pstring As String, ByVal in_num_max As Long, _
ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetTimestamp Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByVal in_htimestamp As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStatementGetTimestampFields Lib "PCSDB500.DLL" (ByVal in_hstatement As Long, ByVal in_num_column As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, _
ByRef out_pday As Long, ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long, ByRef out_pnum_bytes_read As Long) As Integer
Public Declare Function PcsStdAdditiveAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStdAdditiveFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pHigherDose_cond As String, ByVal in_pLowerDose_cond As String, _
ByVal in_pTreatment_cond As String, ByVal in_pUnit_cond As String, ByVal in_pGroup_cond As String) As Integer
Public Declare Function PcsStdAdditiveFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStdAdditiveGetGroup Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveGetGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdAdditiveGetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsStdAdditiveGetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsStdAdditiveGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdAdditiveGetUnit Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdAdditiveListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsStdAdditiveSetGroup Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveSetGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdAdditiveSetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsStdAdditiveSetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsStdAdditiveSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdAdditiveSetUnit Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdAdditiveSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdConditionAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStdConditionFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsStdConditionFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStdConditionGetDescription Lib "PCSDB500.DLL" (ByVal in_hHSTDCONDITION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdConditionListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsStdConditionSetDescription Lib "PCSDB500.DLL" (ByVal in_hHSTDCONDITION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsStdOrderAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStdOrderDataAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsStdOrderDataFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIndex_cond As String, ByVal in_pData_cond As String, _
ByVal in_pStdOrder_cond As String) As Integer
Public Declare Function PcsStdOrderDataFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStdOrderDataGetData Lib "PCSDB500.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal out_pData As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderDataGetIndex Lib "PCSDB500.DLL" (ByVal in_hHSTDORDERDATA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsStdOrderDataGetStdOrder Lib "PCSDB500.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderDataGetStdOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderDataListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsStdOrderDataSetData Lib "PCSDB500.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal in_pData As String) As Integer
Public Declare Function PcsStdOrderDataSetIndex Lib "PCSDB500.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsStdOrderDataSetStdOrder Lib "PCSDB500.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderDataSetStdOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDERDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsADefaultOrder_cond As String, ByVal in_pCondition_cond As String, _
ByVal in_pCreationDate_cond As String, ByVal in_pLowerDose_cond As String, ByVal in_pHigherDose_cond As String, ByVal in_pIsDeleted_cond As String, ByVal in_pHasAMemo_cond As String, _
ByVal in_pTreatment_cond As String, ByVal in_pForm_cond As String, ByVal in_pRoute_cond As String, ByVal in_pUnit_cond As String, ByVal in_pSchedule_cond As String, ByVal in_pOrderType_cond As String, _
ByVal in_pPrescriberGroup_cond As String, ByVal in_pValidatorGroup_cond As String, ByVal in_pCreator_cond As String) As Integer
Public Declare Function PcsStdOrderFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsStdOrderGetCondition Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pCondition As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsStdOrderGetCreator Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetCreatorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetForm Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetFormIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetHasAMemo Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pHasAMemo As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsStdOrderGetIsADefaultOrder Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pIsADefaultOrder As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsStdOrderGetOrderType Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetOrderTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetPrescriberGroup Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetPrescriberGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetRoute Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetRouteIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetSchedule Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetScheduleIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetUnit Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderGetValidatorGroup Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsStdOrderGetValidatorGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsStdOrderListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsStdOrderSetCondition Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pCondition As String) As Integer
Public Declare Function PcsStdOrderSetCreationDate Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsStdOrderSetCreator Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetCreatorIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetForm Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetFormIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetHasAMemo Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pHasAMemo As String) As Integer
Public Declare Function PcsStdOrderSetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsStdOrderSetIsADefaultOrder Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pIsADefaultOrder As String) As Integer
Public Declare Function PcsStdOrderSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsStdOrderSetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsStdOrderSetOrderType Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetOrderTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetPrescriberGroup Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetPrescriberGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetRoute Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetRouteIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetSchedule Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetScheduleIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetUnit Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsStdOrderSetValidatorGroup Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsStdOrderSetValidatorGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHSTDORDER As Long, ByVal in_pdboid As String) As Integer


#End If
