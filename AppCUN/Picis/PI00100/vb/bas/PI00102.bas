Attribute VB_Name = "FuncionesStaff"
Option Explicit


Public Function NuevoStaff(codUsuario As String, staffdboid As String)
Dim strSql As String
Dim Rs As rdoResultset
Dim apel1 As String
Dim apel2 As String
Dim apels As String
Dim Qy As rdoQuery
Dim PassDes As String


'************************************************************
'* ESTA FUNCION A�ADE UN NUEVO USUARIO A PICIS              *
'************************************************************

strSql = "SELECT * FROM SG0200 WHERE SG02COD=?"
Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
Qy(0) = codUsuario
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

'Alloc Staff handle
ierrcode = PcsStaffAlloc(hStaff)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error allocating Staff", vbExclamation
End If
   
'Generar el dboid
ierrcode = PcsDbObjectGenerateIdentifier(hStaff)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al crear el DBOID de usuario", vbExclamation
End If
'Introducir el dboid
ierrcode = PcsDbObjectGetIdentifier(hStaff, staffdboid, 21)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al crear el DBOID de usurio", vbExclamation
End If

'Introducir el username y la contrasenha
ierrcode = PcsStaffSetUserName(hStaff, codUsuario)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al introducir el usario", vbExclamation
End If
PassDes = Desencriptar(Rs!SG02PASSW)
''ierrcode = PcsStaffSetVirtualPassword(hStaff, "84608C738F699C698C5B")
ierrcode = PcsStaffSetVirtualPassword(hStaff, PassDes)
        If ierrcode <> 0 Then
            MsgBox "Error al introducir la contrasenha", vbExclamation
       End If
ierrcode = PcsStaffSetInitials(hStaff, codUsuario)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al introducir el usario", vbExclamation
End If
'Introducir el tipo de DBOID
ierrcode = PcsStaffSetStaffTypeIdentifier(hStaff, "22000000000000000511") 'Masin: luego se cambiara por un staff type gen�rico
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al introducir el usario", vbExclamation
End If
'Introducir el tipo de DBOID
If IsNull(Rs!SG02FECDES) Then
    ierrcode = PcsStaffSetIsDeleted(hStaff, "F")
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error al introducir el campo is deleted", vbExclamation
    End If
Else
    ierrcode = PcsStaffSetIsDeleted(hStaff, "T")
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error al introducir el campo is deleted", vbExclamation
    End If
End If
'Introducimos el nombre
ierrcode = PcsStaffSetName(hStaff, Rs!SG02NOM)
apel1 = Trim(Rs!SG02APE1)
If Not IsNull(Rs!SG02APE2) Then
    apel2 = Trim(Rs!SG02APE2)
    apels = apel1 & " " & apel2
Else
    apels = apel1
End If
If Len(apels) > 48 Then
    apels = Mid(apels, 1, 48)
End If
ierrcode = PcsStaffSetLastName(hStaff, apels)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al introducir el campo nombre o apellidos", vbExclamation
End If
'We send all the changes to the database
ierrcode = PcsDbObjectInsert(hStaff)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al ESCRIBIR", vbExclamation
End If
ierrcode = PcsStaffFree(hStaff)

Rs.Close
Qy.Close
End Function


Public Function NuevoGroupStaff(groupdboid As String, staffdboid As String, blnExisteFechaFin As Boolean)

    ierrcode = PcsRGroupStaffAlloc(hGroupStaff)
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al alocar el GroupStaff", vbExclamation
            End If
    ierrcode = PcsDbObjectGenerateIdentifier(hGroupStaff)
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al generar el identificador de grupo-staff", vbExclamation
            End If
    ierrcode = PcsRGroupStaffSetGroupIdentifier(hGroupStaff, groupdboid)
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al introducir el dboid de grupo", vbExclamation
            End If
    ierrcode = PcsRGroupStaffSetStaffIdentifier(hGroupStaff, staffdboid)
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al introducir el dboid de staff", vbExclamation
            End If
    If blnExisteFechaFin Then
            ierrcode = PcsRGroupStaffSetIsDeleted(hGroupStaff, "T")
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al introducir el campo IsDeleted", vbExclamation
            End If
    Else
            ierrcode = PcsRGroupStaffSetIsDeleted(hGroupStaff, "F")
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al introducir el campo IsDeleted", vbExclamation
            End If
    End If
    ierrcode = PcsDbObjectInsert(hGroupStaff)
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al introducir el registro Staff", vbExclamation
            End If
    ierrcode = PcsRGroupStaffFree(hGroupStaff)
End Function

Public Function modificarStaff(codUsuario As String, staffdboid As String)
'**************************************************************
'* BUSCA EL STAFF POR SU CODIGO Y LO                          *
'* MODIFICA SI HAY ALGUNA MODIFICACION                        *
'* SI ESTA IGUAL NO INTRODUCE NADA                            *
'**************************************************************
Dim Nombre As String * 30
Dim borrado As String * 1
Dim Nombre1 As String
Dim apellido As String * 48
Dim Apellido1 As String
Dim Iniciales As String * 5
Dim Iniciales1 As String
Dim Comparacion As String
Dim blnActualizar As Boolean
Dim condition As String
Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim sNull As String
Dim Password As String * 255
Dim password1 As String * 35
Dim PassDes As String
Dim PassEnc As String * 35

strSql = "SELECT * FROM SG0200 WHERE SG02COD=?"
Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
   Qy(0) = codUsuario
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

ierrcode = PcsListObjectsAlloc(hList)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
'Buscar el usuario
condition = "$='" + codUsuario + "'"
ierrcode = PcsStaffFindWithMembers(hLog, hList, sNull, sNull, sNull, sNull, sNull, condition, sNull, sNull, sNull, sNull, sNull)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al localizar el usuario", vbExclamation
        End If
ierrcode = PcsStaffAlloc(hStaff)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error allocating Staff", vbExclamation
        End If
ierrcode = PcsListObjectsGotoHeadPosition(hList)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "ERROR"
        End If
ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hStaff)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
    'Obtener datos del usuario para comentar
    'posteriormente
ierrcode = PcsDbObjectGetIdentifier(hStaff, staffdboid, 21)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
'Ahora obtiene los campos si los hay
ierrcode = PcsStaffGetName(hStaff, Nombre, 30)
                If ierrcode <> ERRCODE_ALL_OK Then
                    MsgBox "Error"
                End If
ierrcode = PcsStaffGetLastName(hStaff, apellido, 48)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
ierrcode = PcsStaffGetInitials(hStaff, Iniciales, 5)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
ierrcode = PcsStaffGetIsDeleted(hStaff, borrado, 1)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
ierrcode = PcsStaffGetVirtualPassword(hStaff, Password, 255)
Select Case ierrcode
Case ERRCODE_NULL_DATA
    password1 = ""
Case ERRCODE_ALL_OK
    password1 = Trim(Mid(Password, 1, InStr(Password, Chr(0)) - 1))
    'password1 = Trim(Password)
Case Else
    MsgBox "Error"
End Select

'Cortar las variables para compararlas con una misma longitud
Nombre1 = Mid(Nombre, 1, InStr(Nombre, Chr(0)) - 1)
Apellido1 = Mid(apellido, 1, InStr(apellido, Chr(0)) - 1)
Iniciales1 = Mid(Iniciales, 1, InStr(Iniciales, Chr(0)) - 1)
        
'**************************************************************************
'*Se comparan las variables y si alguna cambia, entonces se introducen los*
'*nuevos dato. Si cambia, la variable blnActualizar se pone a true        *
'*para que posteriormente se mande la escritura a la base de datos        *
'**************************************************************************
    
blnActualizar = False
If Nombre1 <> Trim(Rs!SG02NOM) Then
    ierrcode = PcsStaffSetName(hStaff, Rs!SG02NOM)
    blnActualizar = True
End If
If Trim(Mid(Rs!SG02APE1 & " " & Rs!SG02APE2, 1, 48)) <> Trim(Apellido1) Then
    ierrcode = PcsStaffSetLastName(hStaff, Trim(Mid(Rs!SG02APE1 & " " & Rs!SG02APE2, 1, 48)))
    blnActualizar = True
End If
If Iniciales1 <> Trim(codUsuario) Then
    ierrcode = PcsStaffSetInitials(hStaff, codUsuario)
    blnActualizar = True
End If
If IsNull(Rs!SG02FECDES) Then Comparacion = "F" Else Comparacion = "T"
If Comparacion <> borrado Then
    blnActualizar = True
'Modificamos el campo IsDeleted para desactivar el usuario
    If Comparacion = "F" Then  'si no hay fecha fin
        ierrcode = PcsStaffSetIsDeleted(hStaff, "F")
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al introducir el campo is deleted", vbExclamation
        End If
    Else
        ierrcode = PcsStaffSetIsDeleted(hStaff, "T") 'si hay fecha fin
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al introducir el campo is deleted", vbExclamation
        End If
    End If
End If
PassDes = Desencriptar(Rs!SG02PASSW)
'''Nuevo
    ierrcode = PcsStaffEncryptPassword(codUsuario, PassDes, PassEnc, 35)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error al introducir el campo is deleted", vbExclamation
    End If
       PassEnc = Trim(Mid(PassEnc, 1, InStr(PassEnc, Chr(0)) - 1))

'''Fin
If password1 <> PassEnc Then
'MASIN: Modificar mas adelante
    ierrcode = PcsStaffSetVirtualPassword(hStaff, PassEnc)
    blnActualizar = True
End If
If blnActualizar = True Then ' Si ha cambiado algo se escribe
        ierrcode = PcsDbObjectWrite(hStaff)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al ESCRIBIR", vbExclamation
        End If
        blnActualizar = False
End If
Rs.Close
Qy.Close

ierrcode = PcsListObjectsFree(hList)
ierrcode = PcsStaffFree(hStaff)
End Function

Public Function ModificarGroupStaff(staffdboid As String, groupdboid As String, blnExisteFechaFin As Boolean)

'***************************************************************************
'* Actualizar los grupos (departamento/puesto)                             *
'***************************************************************************
Dim conditionS As String
Dim conditionG As String
Dim sNull As String
Dim borrado As String * 1
Dim blnActualizar As Boolean
ierrcode = PcsListObjectsAlloc(hList)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
'Buscar el usuario
conditionS = "$='" + staffdboid + "'"
conditionG = "$='" + groupdboid + "'"
ierrcode = PcsRGroupStaffFindWithMembers(hLog, hList, sNull, conditionG, conditionS)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al localizar el usuario", vbExclamation
        End If
ierrcode = PcsRGroupStaffAlloc(hGroupStaff)
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al alocar el GroupStaff", vbExclamation
            End If
ierrcode = PcsListObjectsGotoHeadPosition(hList)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "ERROR"
        End If
ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hGroupStaff)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
ierrcode = PcsRGroupStaffGetIsDeleted(hGroupStaff, borrado, 1)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al introducir el campo IsDeleted", vbExclamation
        End If
blnActualizar = False
If blnExisteFechaFin And borrado = "F" Then
        ierrcode = PcsRGroupStaffSetIsDeleted(hGroupStaff, "T")
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al introducir el campo IsDeleted", vbExclamation
        End If
        blnActualizar = True
ElseIf Not blnExisteFechaFin And borrado = "T" Then
        ierrcode = PcsRGroupStaffSetIsDeleted(hGroupStaff, "F")
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al introducir el campo IsDeleted", vbExclamation
        End If
        blnActualizar = True
End If
If blnActualizar Then
    ierrcode = PcsDbObjectWrite(hGroupStaff)
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error al escribir el Group/staff", vbExclamation
            End If
    blnActualizar = False
End If
ierrcode = PcsListObjectsFree(hList)
ierrcode = PcsRGroupStaffFree(hGroupStaff)
End Function
Public Function ExisteStaff(codUsuario As String) As Boolean
Dim condition As String
Dim sNull As String
'***************************************************
'* AVERIGUA SI YA EXISTE EL REGISTRO               *
'***************************************************
ierrcode = PcsListObjectsAlloc(hList)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
'Buscar el usuario
condition = "$='" + codUsuario + "'"
ierrcode = PcsStaffFindWithMembers(hLog, hList, sNull, sNull, sNull, sNull, sNull, condition, sNull, sNull, sNull, sNull, sNull)
If ierrcode <> 37 And ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
End If
    If ierrcode = 0 Then ExisteStaff = True
    If ierrcode = 37 Then ExisteStaff = False
PcsListObjectsFree (hList)
End Function

''Public Function BuscarStaffDboid(codUsuario As String) As String
''Dim staffdboid As String * 21
''Dim condition As String
''Dim sNull As String
'' ierrcode = PcsListObjectsAlloc(hList)
''        If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "Error"
''        End If
'''Buscar el usuario
''condition = "$='" + codUsuario + "'"
''ierrcode = PcsStaffFindWithMembers(hLog, hList, sNull, sNull, sNull, sNull, sNull, condition, sNull, sNull, sNull, sNull, sNull)
''        If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "Error al localizar el usuario", vbExclamation
''        End If
''ierrcode = PcsStaffAlloc(hStaff)
''        If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "Error allocating Staff", vbExclamation
''        End If
''ierrcode = PcsListObjectsGotoHeadPosition(hList)
''        If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "ERROR"
''        End If
''ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hStaff)
''        If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "Error"
''        End If
''    'Obtener datos del usuario para comentar
''    'posteriormente
''ierrcode = PcsDbObjectGetIdentifier(hStaff, staffdboid, 21)
''        If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "Error"
''        End If
''BuscarStaffDboid = staffdboid
''PcsListObjectsFree (hList)
''PcsStaffFree (hStaff)
''End Function

Public Function ExisteGroupStaff(staffdboid As String, groupdboid As String)
'***************************************************
'* AVERIGUA SI YA EXISTE EL REGISTRO               *
'***************************************************
Dim conditionS As String
Dim conditionG As String
Dim sNull As String
ierrcode = PcsListObjectsAlloc(hList)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
    End If
'Buscar el usuario
conditionS = "$='" + staffdboid + "'"
conditionG = "$='" + groupdboid + "'"
ierrcode = PcsRGroupStaffFindWithMembers(hLog, hList, sNull, conditionG, conditionS)
    If ierrcode <> 37 And ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
    End If
    If ierrcode = 0 Then ExisteGroupStaff = True
    If ierrcode = 37 Then ExisteGroupStaff = False
PcsListObjectsFree (hList)
End Function
