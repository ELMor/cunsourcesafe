Attribute VB_Name = "FuncionesAdmision"
Option Explicit
'Public Function ExisteAdmisionAbierta(Asistencia As Long) As Boolean
'Dim strSql As String
'Dim Rs As rdoResultset
'Dim Qy As rdoQuery
'Dim condition As String
'Dim snull As String
'
'If Not ExisteAdmision(Asistencia) Then
'    ierrcode = PcsListObjectsAlloc(hList)
'    If ierrcode <> ERRCODE_ALL_OK Then
'        MsgBox "Error"
'    End If
'
'    'Build the condition string
'    condition = "$!='" + "'"
'    'condition = "$!=NULL"
'    ierrcode = PcsAdmissionFindWithMembers(hLog, hList, snull, snull, snull, snull, condition, snull, snull, snull, snull, snull, snull, snull, snull, snull, snull, snull, snull, snull, snull, snull, snull, snull)
'    If ierrcode <> 37 And ierrcode <> ERRCODE_ALL_OK Then
'        MsgBox "Error"
'    End If
'
'    If ierrcode = 0 Then ExisteAdmisionAbierta = True
'    If ierrcode = 37 Then ExisteAdmisionAbierta = False
'    ierrcode = PcsListObjectsFree(hList)
'End If
'End Function

Public Function ExistePaciente(codAsistencia As Long, numHistoria As Long) As Boolean
Dim condition As String
Dim sNull As String
Dim Historia As String

Call BuscarPacienteDesdeAsistencia(codAsistencia, numHistoria)

ierrcode = PcsListObjectsAlloc(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
'Build the condition string
Historia = CStr(numHistoria)
condition = "$='" + Historia + "'"
'The only condition is the Patient name, so the other fields are empty strings
ierrcode = PcsPatientFindWithMembers(hLog, hList, condition, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull)
If ierrcode <> 37 And ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
If ierrcode = 0 Then ExistePaciente = True
If ierrcode = 37 Then ExistePaciente = False
ierrcode = PcsListObjectsFree(hList)
End Function
Public Function ModificarPaciente(numHistoria As Long, PatientDboid As String)
Dim strsql As String
Dim rs As rdoResultset
Dim qy As rdoQuery
Dim condition As String
Dim Rapellido1 As String
Dim Rapellido2 As String
Dim Comodin As String
Dim blnActualizar As Boolean
Dim sNull As String
Dim Apellido1 As String * 48
Dim Apellido2 As String * 48
Dim Nombre As String * 48
Dim Nombre1 As String
Dim Fecha As Date
Dim A�o As Variant
Dim Mes As Variant
Dim Dia As Variant
Dim Historia As String

strsql = "SELECT CI22FECNACIM, CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = numHistoria
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

    ierrcode = PcsListObjectsAlloc(hList)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
    End If
Historia = CStr(numHistoria)
    'Build the condition string
    condition = "$='" + Historia + "'"
    'The only condition is the Patient name, so the other fields are empty strings
    ierrcode = PcsPatientFindWithMembers(hLog, hList, condition, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
    End If
          ierrcode = PcsListObjectsGotoHeadPosition(hList)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "ERROR"
        End If
        ierrcode = PcsPatientAlloc(hPatient)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hPatient)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        ierrcode = PcsDbObjectGetIdentifier(hPatient, PatientDboid, 21)
        If ierrcode <> 0 Then
                    MsgBox "Error al obtener el PatientDboid", vbExclamation
        End If
        ierrcode = PcsPatientGetName(hPatient, Nombre, 25)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        Nombre1 = Mid(Nombre, 1, InStr(Nombre, Chr(0)) - 1)
        ierrcode = PcsPatientGetLastName(hPatient, Apellido1, 25)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        
        Rapellido1 = Mid(Apellido1, 1, InStr(Apellido1, Chr(0)) - 1)
        
        ierrcode = PcsPatientGetMiddleName(hPatient, Apellido2, 25)
    Select Case ierrcode
        Case ERRCODE_NULL_DATA
            Rapellido2 = "NoApellido"
        Case ERRCODE_ALL_OK
            Rapellido2 = Mid(Apellido2, 1, InStr(Apellido2, Chr(0)) - 1)
        Case Else
                MsgBox "Error al obtener el segundo apellido"
    End Select
        If IsNull(rs!CI22SEGAPEL) Then
            Comodin = "NoApellido"
        Else
            Comodin = rs!CI22SEGAPEL
        End If


        ierrcode = PcsPatientGetBirthDate(hPatient, A�o, Mes, Dia, 0, 0, 0)
    Select Case ierrcode
        Case ERRCODE_NULL_DATA
            Fecha = "0.00.00"
        Case ERRCODE_ALL_OK
            Fecha = Dia & "/" & Mes & "/" & A�o
            Fecha = CDate(Format(Fecha, "DD/MM/YY"))
        Case Else
                MsgBox "Error al obtener la fecha de nacimiento"
    End Select

        'Se comparan los registros y si hay alguno distinto se actualiza
        blnActualizar = False
        If Rapellido1 <> rs!CI22PRIAPEL Then
            ierrcode = PcsPatientSetLastName(hPatient, rs!CI22PRIAPEL)
            If ierrcode <> 0 Then
                        MsgBox "Error Apellido", vbExclamation
            End If
            blnActualizar = True
        End If
        If Rapellido2 <> Comodin Then
            ierrcode = PcsPatientSetMiddleName(hPatient, Comodin)
            If ierrcode <> 0 Then
                        MsgBox "Error Apellido", vbExclamation
            End If
            blnActualizar = True
        End If
        If Nombre1 <> rs!CI22NOMBRE Then
            ierrcode = PcsPatientSetName(hPatient, rs!CI22NOMBRE)
            If ierrcode <> 0 Then
                        MsgBox "Error nombre", vbExclamation
            End If
            blnActualizar = True
        End If
        If Not (Fecha = "0.00.00" And IsNull(rs!CI22FECNACIM)) Then
            If Fecha <> rs!CI22FECNACIM Then
                Fecha = Format(rs!CI22FECNACIM, "DD/MM/YYYY")
                A�o = Format(Fecha, "YYYY")
                Mes = Format(rs!CI22FECNACIM, "MM")
                Dia = Format(rs!CI22FECNACIM, "DD")
                ierrcode = PcsPatientSetBirthDate(hPatient, A�o, Mes, Dia, 0, 0, 0)
                If ierrcode <> 0 Then
                            MsgBox "Error Fecha de nacimiento", vbExclamation
                End If
                blnActualizar = True
            End If
        End If
        If blnActualizar Then
            ierrcode = PcsDbObjectWrite(hPatient)
            If ierrcode <> 0 Then
                MsgBox "Error al ESCRIBIR", vbExclamation
            End If
            blnActualizar = False
        End If
    ierrcode = PcsListObjectsFree(hList)
    ierrcode = PcsPatientFree(hPatient)
rs.Close
qy.Close

End Function
Public Function NuevoPaciente(numHistoria As Long, PatientDboid As String)
'*******************************************
'* Crear al paciente                       *
'*******************************************
Dim strsql As String
Dim rs As rdoResultset
Dim qy As rdoQuery
Dim condition As String
Dim sNull As String
Dim Fecha As Date
Dim A�o As Variant
Dim Mes As Variant
Dim Dia As Variant
Dim blnNoFecha As Boolean

strsql = "SELECT CI22FECNACIM, CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = numHistoria
Set rs = qy.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)

ierrcode = PcsPatientAlloc(hPatient)
If ierrcode <> 0 Then
            MsgBox "Error allocating Patient", vbExclamation
End If
ierrcode = PcsDbObjectGenerateIdentifier(hPatient)
If ierrcode <> 0 Then
            MsgBox "Error generate identifier", vbExclamation
End If
ierrcode = PcsDbObjectGetIdentifier(hPatient, PatientDboid, 21)
If ierrcode <> 0 Then
            MsgBox "Error al obtener el PatientDboid", vbExclamation
End If
ierrcode = PcsPatientSetFirstIdentifier(hPatient, numHistoria)
If ierrcode <> 0 Then
            MsgBox "Error historia", vbExclamation
End If
ierrcode = PcsPatientSetLastName(hPatient, rs!CI22PRIAPEL)
If ierrcode <> 0 Then
            MsgBox "Error Apellido", vbExclamation
End If
If Not IsNull(rs!CI22SEGAPEL) Then
    ierrcode = PcsPatientSetMiddleName(hPatient, rs!CI22SEGAPEL)
    If ierrcode <> 0 Then
                MsgBox "Error Apellido", vbExclamation
    End If
End If
ierrcode = PcsPatientSetName(hPatient, rs!CI22NOMBRE)
If ierrcode <> 0 Then
            MsgBox "Error nombre", vbExclamation
End If
If Not IsNull(rs!CI22FECNACIM) Then 'existe la fecha de nacimiento
    Fecha = Format(rs!CI22FECNACIM, "DD/MM/YYYY")
    A�o = Format(Fecha, "YYYY")
    Mes = Format(rs!CI22FECNACIM, "MM")
    Dia = Format(rs!CI22FECNACIM, "DD")
Else
    blnNoFecha = True 'no existe la fecha de nacimiento
End If
If Not blnNoFecha Then 'si existe se introduce
    ierrcode = PcsPatientSetBirthDate(hPatient, A�o, Mes, Dia, 0, 0, 0)
    If ierrcode <> 0 Then
                MsgBox "Error Fecha de nacimiento", vbExclamation
    End If
End If

ierrcode = PcsDbObjectInsert(hPatient)
If ierrcode <> 0 Then
    MsgBox "Error al ESCRIBIR", vbExclamation
End If

rs.Close
qy.Close
ierrcode = PcsPatientFree(hPatient)
End Function
Public Function ExisteAdmision(codAsistencia As Long) As Boolean
Dim strsql As String
Dim rs As rdoResultset
Dim qy As rdoQuery
Dim condition As String
Dim sNull As String
Dim Asistencia As String
ierrcode = PcsListObjectsAlloc(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
Asistencia = CStr(codAsistencia)
'Build the condition string
condition = "$='" + Asistencia + "'"
'The only condition is the Patient name, so the other fields are empty strings
ierrcode = PcsAdmissionFindWithMembers(hLog, hList, condition, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull)
If ierrcode <> 37 And ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
If ierrcode = 0 Then ExisteAdmision = True
If ierrcode = 37 Then ExisteAdmision = False
ierrcode = PcsListObjectsFree(hList)
End Function
Public Function ModificarAdmision(codAsistencia As Long, AdmisionDboid As String) As Boolean
Dim strsql As String
Dim rs As rdoResultset
Dim qy As rdoQuery
Dim condition As String
Dim sNull As String
Dim Proceso1 As String * 48
Dim Proceso2 As String * 48
Dim LProceso1 As Long
Dim LProceso2 As Long
Dim Asistencia As String
Dim blnActualizar As Boolean
Dim Peso As String
Dim Altura As String
Dim pos%
Dim pesoDec As String
Dim pesoInt As String

'-------------
strsql = "SELECT AD07CODPROCESO FROM AD0800 WHERE AD01CODASISTENCI=?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = codAsistencia
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

ierrcode = PcsListObjectsAlloc(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsAdmissionAlloc(hAdmision)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
Asistencia = CStr(codAsistencia)
'Build the condition string
condition = "$='" + Asistencia + "'"
'The only condition is the Patient name, so the other fields are empty strings
ierrcode = PcsAdmissionFindWithMembers(hLog, hList, condition, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsListObjectsGotoHeadPosition(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "ERROR"
End If

ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hAdmision)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
If rs.EOF Then
    MsgBox "Error: Asistencia sin proceso"
    Exit Function
End If
ierrcode = PcsDbObjectGetIdentifier(hAdmision, AdmisionDboid, 21)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If

ierrcode = PcsAdmissionGetSecondIdentifier(hAdmision, Proceso1, 48)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
    End If
LProceso1 = CLng(Mid(Proceso1, 1, InStr(Proceso1, Chr(0)) - 1))
ierrcode = PcsAdmissionGetThirdIdentifier(hAdmision, Proceso2, 48)
    Select Case ierrcode
    Case ERRCODE_NULL_DATA
        LProceso2 = 0
    Case ERRCODE_ALL_OK
        LProceso2 = CLng(Mid(Proceso2, 1, InStr(Proceso2, Chr(0)) - 1))
    Case Else
            MsgBox "Error al obtener el segundo proceso"
    End Select
rs.MoveFirst
'****************************************************************
'*Se comparan los procesos                                      *
'****************************************************************
blnActualizar = False
    If LProceso1 <> rs!AD07CODPROCESO Then
            ierrcode = PcsAdmissionSetSecondIdentifier(hAdmision, rs!AD07CODPROCESO)
            If ierrcode <> 0 Then
                        MsgBox "Error Proceso1", vbExclamation
            End If
            blnActualizar = True
    End If
rs.MoveNext
If Not rs.EOF Then
     If LProceso2 <> rs!AD07CODPROCESO Then
           ierrcode = PcsAdmissionSetThirdIdentifier(hAdmision, rs!AD07CODPROCESO)
           If ierrcode <> 0 Then
                       MsgBox "Error Proceso2", vbExclamation
           End If
           blnActualizar = True
    End If
End If
rs.Close
qy.Close

'***************************************************************
'Actualizar el peso y la altura
'***************************************************************
strsql = "SELECT AD38PESO,AD38ALTURA FROM AD3800 WHERE AD38FECFIN IS NULL "
strsql = strsql & " AND AD01CODASISTENCI=?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = codAsistencia
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

'Peso = Rs!AD38PESO
'Altura = Rs!AD38ALTURA
'If IsNull(Rs!AD38ALTURA) Then
'Rs.Close
'Qy.Close
'If rs.EOF Then
'    MsgBox "Error: Asistencia sin peso ni altura"
'    ModificarAdmision = False
'    Exit Function ' Si hay un fallo que salga de la funci�n
'End If
rs.MoveLast 'siempre hay un proceso
While rs.EOF = False
    If rs!AD38PESO > 0 Or rs!AD38ALTURA > 0 Then
        If IsNull(rs!AD38PESO) Then
                Altura = rs!AD38ALTURA
                strsql = "UPDATE ADMISSIONS SET HEIGHT = ? WHERE " _
                        & "ADMISSIONDBOID = ?"
                Set qy = objApp.rdoConnect.CreateQuery("", strsql)
                qy(0) = Format$(Altura, "###0.00")
                qy(1) = AdmisionDboid
                qy.Execute
        Else
            If IsNull(rs!AD38ALTURA) Then
                Peso = rs!AD38PESO
                strsql = "UPDATE ADMISSIONS SET WEIGHT = ? WHERE " _
                         & "ADMISSIONDBOID = ?"
                Set qy = objApp.rdoConnect.CreateQuery("", strsql)
                qy(0) = Format$(Peso, "###0.000")
                qy(1) = AdmisionDboid
                qy.Execute
            Else
                Peso = Trim(rs!AD38PESO)
                Altura = rs!AD38ALTURA
                    strsql = "UPDATE ADMISSIONS SET HEIGHT = ?, WEIGHT = ? WHERE " _
                            & "ADMISSIONDBOID = ?"
                    Set qy = objApp.rdoConnect.CreateQuery("", strsql)
                    qy(0) = Format$(Altura, "###0.00")
                    qy(1) = Format$(Peso, "###0.000")
                    qy(2) = AdmisionDboid
                    qy.Execute
            End If
        End If
        rs.MoveNext
    Else
        rs.MovePrevious
    End If
Wend
rs.Close
qy.Close

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
'****************************************************************
'*Se comprueba si el paciente esta de alta con esta admision    *
'****************************************************************

strsql = "SELECT DISTINCT A.ADMID1, P.PTID1 "
strsql = strsql & "FROM PICIS.ADMISSIONS A, PICIS.PICISDATA D,"
strsql = strsql & "PICIS.ENVIRONMENTS E, PICIS.ENVIRONMENTLOCATION EL,"
strsql = strsql & "PICIS.PATIENTS P "
strsql = strsql & "WHERE P.PATIENTDBOID = A.PATIENTDBOID "
strsql = strsql & "AND (A.ADMISSIONDBOID=D.ADMISSIONDBOID) "
strsql = strsql & "AND (D.PICISDATADBOID=E.PICISDATADBOID) "
strsql = strsql & "AND (E.ENVIRONMENTDBOID=EL.ENVIRONMENTDBOID) "
strsql = strsql & "AND (A.STARTED Is Not Null) AND (A.ENDED Is Null) "
strsql = strsql & "AND (EL.ENDED IS NULL) AND (E.ENDED IS NULL) "
strsql = strsql & "AND (A.ADMID1 =?)"

Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = codAsistencia
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

If rs.RowCount = 0 Then
    Call ReAdmision(AdmisionDboid)
End If


' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

'****************************************************************
'*Si se ha modificado se escribe                                *
'****************************************************************
    
    If blnActualizar = True Then
    ierrcode = PcsDbObjectWrite(hAdmision)
           If ierrcode <> 0 Then
                       MsgBox "Error al escribir", vbExclamation
            End If
    blnActualizar = False
    End If
   
ModificarAdmision = True
ierrcode = PcsListObjectsFree(hList)
ierrcode = PcsAdmissionFree(hAdmision)
End Function
Public Function Admitir(codAsistencia As Long, PatientDboid As String, LocationId As Long) As Boolean
'Dim Systime As Long
'Dim Loc As Long
'Dim Env As Long
''Dim condition As String
'Dim LocDboid As String * 21
'Dim LocId As String

Admitir = True
ierrcode = PcsDbObjectAllocFromIdentifier(hPatient, PatientDboid)
If ierrcode <> 0 Then
    Admitir = False
    MsgBox "Error al realizar la admision del paciente", vbExclamation, "PICIS"
End If

ierrcode = PcsLocationGetFromLocationId(hLog, LocationId, hLocation)
If ierrcode <> 0 Then
    Admitir = False
    MsgBox "Error al realizar la admision del paciente", vbExclamation, "PICIS"
End If

ierrcode = PcsAdtObjectExtendedAdmit(hAdt, hPatient, hLocation, 0, 0, vbNullString)
If ierrcode <> 0 And ierrcode <> 705 Then               'Error: Falla mensaje de PIMS
    Admitir = False
    MsgBox "Error al realizar la admision del paciente", vbExclamation, "PICIS"
End If
ierrcode = PcsAdtObjectFree(hAdt)
ierrcode = PcsPatientFree(hPatient)
End Function
Public Function AbrirSinoEstaAbierto(blnvisualcare As Boolean, AdmisionDboid As String, PatientDboid As String, EnvironmentDboid As String)

Dim strToExecute As String

    If blnvisualcare Then 'Si se quiere abrir Visual Care
            strToExecute = "C:\Picis\VisualCare\VisualCare / A=" & AdmisionDboid & "/ P=" & PatientDboid & "/ C=" & EnvironmentDboid
            ierrcode = Shell(strToExecute, vbMaximizedFocus)
    Else 'En el caso de ChartPlus
            ChDir "C:\Picis\ChartPlus"
            strToExecute = "C:\Picis\ChartPlus\ChartPlus.exe /A=" & AdmisionDboid & " /P=" & PatientDboid & " /C=" & EnvironmentDboid
            ierrcode = Shell(strToExecute, vbMaximizedFocus)
    End If
End Function
Public Function NuevaAdmisionOK(codAsistencia As Long, PatientDboid As String, AdmisionDboid As String, LocationId As Long) As Boolean
Dim strsql As String
Dim rs As rdoResultset
Dim qy As rdoQuery
Dim Systime As Long
Dim Loc As Long
Dim Env As Long
Dim Peso As String
Dim Altura As String

'*******************************************
'* Crear la admisi�n                       *
'*******************************************

NuevaAdmisionOK = True
''If Not Admitir(codAsistencia, PatientDboid) Then
''    NuevaAdmisionOK = False
''    ierrcode = PcsAdmissionFree(hAdmision)
''    ierrcode = PcsAdtObjectFree(hAdt)
''    Exit Function ' Si hay un fallo que salga de la funci�n
''End If
''ierrcode = PcsAdtObjectAlloc(hAdt, hLog)
''ierrcode = PcsDbObjectAllocFromIdentifier(hPatient, PatientDboid)
''ierrcode = PcsAdmissionAlloc(hAdmision)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''    NuevaAdmisionOK = False
''    ierrcode = PcsAdmissionFree(hAdmision)
''    ierrcode = PcsAdtObjectFree(hAdt)
''    Exit Function ' Si hay un fallo que salga de la funci�n
''End If
''
''Systime = Loc = Env = 0
''ierrcode = PcsAdtObjectExtendedAdmit(hAdt, hPatient, ByVal Loc, ByVal Env, hLog, ByVal Systime)
''If ierrcode <> 0 And ierrcode <> 705 Then               'Error: Falla mensaje de PIMS
''    NuevaAdmisionOK = False
''    MsgBox "Error al realizar la admision del paciente", vbExclamation, "PICIS"
''End If
ierrcode = PcsDbObjectAllocFromIdentifier(hPatient, PatientDboid)
If ierrcode <> 0 Then
    NuevaAdmisionOK = False
    MsgBox "Error al realizar la admision del paciente", vbExclamation, "PICIS"
End If

ierrcode = PcsLocationGetFromLocationId(hLog, LocationId, hLocation)
If ierrcode <> 0 Then
    NuevaAdmisionOK = False
    MsgBox "Error al realizar la admision del paciente", vbExclamation, "PICIS"
End If

ierrcode = PcsAdtObjectExtendedAdmit(hAdt, hPatient, hLocation, 0, 0, vbNullString)
If ierrcode <> 0 And ierrcode <> 705 Then               'Error: Falla mensaje de PIMS
    NuevaAdmisionOK = False
    MsgBox "Error al realizar la admision del paciente", vbExclamation, "PICIS"
End If


ierrcode = PcsAdtObjectGetAdmission(hAdt, hAdmision)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
    NuevaAdmisionOK = False
    ierrcode = PcsAdmissionFree(hAdmision)
    ierrcode = PcsAdtObjectFree(hAdt)
    Exit Function ' Si hay un fallo que salga de la funci�n
End If

ierrcode = PcsDbObjectGetIdentifier(hAdmision, AdmisionDboid, 21)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
    NuevaAdmisionOK = False
    ierrcode = PcsAdmissionFree(hAdmision)
    ierrcode = PcsAdtObjectFree(hAdt)
    Exit Function ' Si hay un fallo que salga de la funci�n
End If

ierrcode = PcsAdmissionSetFirstIdentifier(hAdmision, codAsistencia)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
    NuevaAdmisionOK = False
    ierrcode = PcsAdmissionFree(hAdmision)
    ierrcode = PcsAdtObjectFree(hAdt)
    Exit Function ' Si hay un fallo que salga de la funci�n
End If
'Se seleccionan los dos primeros procesos asociados a la asistencia
strsql = "SELECT AD07CODPROCESO FROM AD0800 WHERE AD01CODASISTENCI=?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = codAsistencia
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rs.EOF Then
    MsgBox "Error: Asistencia sin proceso"
    NuevaAdmisionOK = False
    ierrcode = PcsAdmissionFree(hAdmision)
    ierrcode = PcsAdtObjectFree(hAdt)
    Exit Function ' Si hay un fallo que salga de la funci�n
End If
rs.MoveFirst 'siempre hay un proceso
        ierrcode = PcsAdmissionSetSecondIdentifier(hAdmision, rs!AD07CODPROCESO)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
            NuevaAdmisionOK = False
            ierrcode = PcsAdmissionFree(hAdmision)
            ierrcode = PcsAdtObjectFree(hAdt)
            Exit Function ' Si hay un fallo que salga de la funci�n
        End If
rs.MoveNext
        If Not rs.EOF Then
            ierrcode = PcsAdmissionSetThirdIdentifier(hAdmision, rs!AD07CODPROCESO)
            If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error"
                NuevaAdmisionOK = False
                ierrcode = PcsAdmissionFree(hAdmision)
                ierrcode = PcsAdtObjectFree(hAdt)
                Exit Function ' Si hay un fallo que salga de la funci�n
            End If
        End If
rs.Close
qy.Close

' Si se ha modificado se escribe

ierrcode = PcsDbObjectWrite(hAdmision)
If ierrcode <> 0 Then
    MsgBox "Error al ESCRIBIR", vbExclamation
    NuevaAdmisionOK = False
    ierrcode = PcsAdmissionFree(hAdmision)
    ierrcode = PcsAdtObjectFree(hAdt)
    Exit Function ' Si hay un fallo que salga de la funci�n
End If

'Actualizar el peso y la altura

strsql = "SELECT AD38PESO,AD38ALTURA FROM AD3800 WHERE AD38FECFIN IS NULL "
strsql = strsql & " AND AD01CODASISTENCI=?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = codAsistencia
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

'Peso = Rs!AD38PESO
'Altura = Rs!AD38ALTURA
'If IsNull(Rs!AD38ALTURA) Then
'Rs.Close
'Qy.Close
If rs.EOF Then
    MsgBox "Error: Asistencia sin peso ni altura"
    Exit Function ' Si hay un fallo que salga de la funci�n
End If
rs.MoveLast 'siempre hay un proceso
While rs.EOF = False
    If rs!AD38PESO > 0 Or rs!AD38ALTURA > 0 Then
        If IsNull(rs!AD38PESO) Then
                Altura = rs!AD38ALTURA
                    strsql = "UPDATE ADMISSIONS SET HEIGHT = ? WHERE " _
                            & "ADMISSIONDBOID = ?"
                    Set qy = objApp.rdoConnect.CreateQuery("", strsql)
                    qy(0) = Format$(Altura, "###0.00")
                    qy(1) = AdmisionDboid
                    qy.Execute
        Else
            If IsNull(rs!AD38ALTURA) Then
                Peso = rs!AD38PESO
                    strsql = "UPDATE ADMISSIONS SET WEIGHT = ? WHERE " _
                             & "ADMISSIONDBOID = ?"
                    Set qy = objApp.rdoConnect.CreateQuery("", strsql)
                    qy(0) = Format$(Peso, "###0.000")
                    qy(1) = AdmisionDboid
                    qy.Execute
            Else
                Peso = rs!AD38PESO
                Altura = rs!AD38ALTURA
                    strsql = "UPDATE ADMISSIONS SET HEIGHT = ?, WEIGHT = ? WHERE " _
                            & "ADMISSIONDBOID = ?"
                    Set qy = objApp.rdoConnect.CreateQuery("", strsql)
                    qy(0) = Format$(Altura, "###0.00")
                    qy(1) = Format$(Peso, "###0.000")
                    qy(2) = AdmisionDboid
                    qy.Execute
            End If
        End If
        rs.MoveNext
    Else
        rs.MovePrevious

    End If
    
Wend
     
rs.Close
qy.Close


ierrcode = PcsAdmissionFree(hAdmision)
ierrcode = PcsAdtObjectFree(hAdt)
End Function

Public Function CambiarStationID(LocationId As Long) As Boolean
' Si hay alg�n error en la funcion devuelve una variable True
Dim vrtList(3) As Variant
vrtList(0) = CONFIG_TYPE_STRING
vrtList(1) = "StationId"
vrtList(2) = LocationId
vrtList(3) = CONFIG_TYPE_ENDPARM
ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al alocatar la configuraci�n"
End If
ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error "
End If
ierrcode = PcsCfgEntriesIntoSection(CurrentCfgHandle, "DbApi", "Station", True, vrtList())
If ierrcode <> ERRCODE_ALL_OK Then CambiarStationID = True

ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
End Function
Public Function ObtenerPicisLocationID(camaCun As String) As Integer
Dim strsql As String
Dim qy As rdoQuery
Dim rs As rdoResultset
strsql = "SELECT LOCATIONS.LOCATIONID FROM LOCATIONS "
strsql = strsql & "WHERE LOCATIONS.LOCATIONDESC=?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = camaCun
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
ObtenerPicisLocationID = rs.rdoColumns(0).Value
End Function
Public Function EstadoLocationOK(LocationId As Long, blnvisualcare As Boolean, _
AdmisionStatus As Integer, codAsistencia As Long, CpuEstado As Integer) As Boolean
'Esta funcion devuelve True si la Location esta ocupada por un
'paciente que no es el que se est� intentando admitir
'Devuelve False si la Location est� libre o esta
'ocupada por el mismo paciente que se quiere admitir
    ' Si admisionstatus=1 => En curso
    ' Si admisionstatus=2 => Transladado
    ' Si admisionstatus=3 => Nueva Admision

Dim strsql As String
Dim qy As rdoQuery
Dim rs As rdoResultset
Dim CpuName As String
Dim Respuesta As Integer

strsql = "SELECT P.PTID1, P.NAME, P.LASTNAME, A.STARTED, A.ADMID1, L.COMPUTERNAME FROM PATIENTS P, "
strsql = strsql & " ADMISSIONS A, ENVIRONMENTS E, ENVIRONMENTLOCATION EL, "
strsql = strsql & " LOCATIONS L, PICISDATA PD WHERE P.PATIENTDBOID = A.PATIENTDBOID "
strsql = strsql & " AND A.ADMISSIONDBOID = PD.ADMISSIONDBOID AND PD.PICISDATADBOID = E.PICISDATADBOID "
strsql = strsql & " AND E.ENVIRONMENTDBOID = EL.ENVIRONMENTDBOID AND EL.LOCATIONDBOID = L.LOCATIONDBOID "
strsql = strsql & " AND (EL.ENDED IS NULL) AND (E.ENDED IS NULL) "
strsql = strsql & " AND (A.ENDED IS NULL) AND L.LOCATIONID=?"

Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = LocationId
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Select Case AdmisionStatus
Case 1 'En curso
    If rs.RowCount = 0 Then
        EstadoLocationOK = True
        CpuEstado = 1
    Else
        If codAsistencia = rs!ADMID1 Then
            EstadoLocationOK = True 'Esta ocupada pero por la misma asistencia
            If Not blnvisualcare Then   'Caso ChartPlus

                If ComprobarUsuario(objPipe.PipeGet("PI_CodDepart")) Then
                    CpuEstado = 1
                    If rs!COMPUTERNAME <> Environ$("COMPUTERNAME") Then
                        CpuName = rs!COMPUTERNAME
                        Respuesta = MsgBox("La admisi�n est� en curso en " & CpuName & ". �Desea admitirlo en este ordenador?", vbYesNo + vbQuestion)
                        If Respuesta = vbYes Then
                            strsql = "UPDATE LOCATIONS SET COMPUTERNAME = ? WHERE " _
                            & "LOCATIONID = ?"
                            Set qy = objApp.rdoConnect.CreateQuery("", strsql)
                            qy(0) = Environ$("COMPUTERNAME")
                            qy(1) = LocationId
                            qy.Execute
                        Else                        'Caso VisualCare
                            CpuEstado = 2
                        End If
                    End If
                Else
                    CpuEstado = 2
                End If
            Else
                CpuEstado = 2
            End If
        Else
            EstadoLocationOK = False
        End If
    End If
Case 2, 3 'trasladado
     If rs.RowCount = 0 Then
        EstadoLocationOK = True
        If Not blnvisualcare Then   'Caso ChartPlus
            If ComprobarUsuario(objPipe.PipeGet("PI_CodDepart")) Then
                CpuEstado = 1
            Else
                CpuEstado = 2
            End If
        Else                        'Caso VisualCare
            CpuEstado = 2
        End If
    Else
        EstadoLocationOK = False
    End If
End Select
End Function
Public Function AdmisionTrasladada(codAsistencia As Long) As Boolean
'Devuelve True si la admisi�n est� transladada
'Devuelve False si la admisi�n est� en curso
Dim strsql As String
Dim qy As rdoQuery
Dim rs As rdoResultset

strsql = "SELECT DISTINCT A.ADMID1, P.PTID1 "
strsql = strsql & "FROM PICIS.ADMISSIONS A, PICIS.PICISDATA D,"
strsql = strsql & "PICIS.ENVIRONMENTS E, PICIS.ENVIRONMENTLOCATION EL,"
strsql = strsql & "PICIS.PATIENTS P "
strsql = strsql & "WHERE P.PATIENTDBOID = A.PATIENTDBOID "
'strSql = strSql & "AND (E.ENVIRONMENTTYPEDBOID=41000000000002000000) "
strsql = strsql & "AND (A.STARTED Is Not Null) AND (A.ENDED Is Null) "
strsql = strsql & "AND (EL.ENDED IS NULL) AND (E.ENDED IS NULL) "
strsql = strsql & "AND (A.ADMISSIONDBOID=D.ADMISSIONDBOID) "
strsql = strsql & "AND (D.PICISDATADBOID=E.PICISDATADBOID) "
strsql = strsql & "AND (E.ENVIRONMENTDBOID=EL.ENVIRONMENTDBOID) "
strsql = strsql & "AND (EL.LOCATIONDBOID=42000000000001000000) "
strsql = strsql & "AND (A.ADMID1 =?)"

Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = codAsistencia
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rs.RowCount > 0 Then AdmisionTrasladada = True Else AdmisionTrasladada = False
End Function

Public Sub Admision(codAsistencia As Long, blnvisualcare As Boolean, EntornoDboid As String)
Dim numHistoria As Long
Dim PatientDboid As String * 21
Dim AdmisionDboid As String * 21
Dim variable As Variant
Dim strOrdenador As String
Dim condition As String
Dim sNull As String
Dim EnvironmentDboid As String
Dim ierrcode As Long
Dim blnAbierto, blnError As Boolean
Dim LocationId As Long
Dim codDepart As String
Dim camaCun As String
Dim cama As String
Dim CpuEstado As Integer

codAsistencia = objPipe.PipeGet("PI_CodAsistencia")
blnvisualcare = objPipe.PipeGet("PI_blnVisualcare")
camaCun = objPipe.PipeGet("PI_CodCama")
codDepart = objPipe.PipeGet("PI_CodDepart")

If Not LOG Then
    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
    Exit Sub
End If

    LocationId = ObtenerPicisLocationID(camaCun) 'Cambiar el archivo DbApi.pcs

''If ExistePaciente(codAsistencia, numHistoria) Then 'Si existe se modifica
''    Call ModificarPaciente(numHistoria, PatientDboid)

    If ExisteAdmision(codAsistencia) Then
        Call BuscarPacienteDesdeAsistencia(codAsistencia, numHistoria)
        Call ModificarPaciente(numHistoria, PatientDboid)
        If Not ModificarAdmision(codAsistencia, AdmisionDboid) Then Exit Sub 'No se introduce el Patientdboid
        'porque se supone que es el mismo. Si el paciente ya estuviera admitido o trasladado
        If AdmisionTrasladada(codAsistencia) Then 'El caso de que estuviera trasladado
                If EstadoLocationOK(LocationId, blnvisualcare, 2, codAsistencia, CpuEstado) Then
                    'If CpuEstado = 1 Then
                        blnError = CambiarStationID(LocationId)
                        If blnError Then
                            MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
                            ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                            ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                            Exit Sub
                        End If
                        Call ModificarConfiguracion(codDepart, blnvisualcare, camaCun, EntornoDboid, EnvironmentDboid)
                        If Not Admitir(codAsistencia, PatientDboid, LocationId) Then Exit Sub     'Admitirlo
                    'Else
'                        blnError = CambiarStationID(LocationId)
'                        If blnError Then
'                            MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
'                            ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'                            ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'                            Exit Sub
'                        End If
'                        Call ModificarConfiguracion(codDepart, blnvisualcare, camaCun, EntornoDboid, EnvironmentDboid)
'
'                    End If
                Else
                    MsgBox "La Location est� ocupada por un paciente distinto al seleccionado.", vbExclamation
                    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                    Exit Sub
                End If
        Else ' Admision en curso
                If Not EstadoLocationOK(LocationId, blnvisualcare, 1, codAsistencia, CpuEstado) Then
                    MsgBox "La Location est� ocupada por un paciente distinto al seleccionado.", vbExclamation
                    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                    Exit Sub
                Else
                    blnError = CambiarStationID(LocationId)
                    If blnError Then
                        MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
                        ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                        ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                        Exit Sub
                    End If
                    Call ModificarConfiguracion(codDepart, blnvisualcare, camaCun, EntornoDboid, EnvironmentDboid)
                End If
        End If
    Else 'no existe la admision con este codAsistencia
        If ExistePaciente(codAsistencia, numHistoria) Then 'Si existe se modifica
            Call ModificarPaciente(numHistoria, PatientDboid)

            If Not EstadoLocationOK(LocationId, blnvisualcare, 3, codAsistencia, CpuEstado) Then
                MsgBox "La Location est� ocupada por un paciente distinto al seleccionado.", vbExclamation
                ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                Exit Sub
            Else
                blnError = CambiarStationID(LocationId)
                If blnError Then
                    MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
                    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                    Exit Sub
                End If
            End If
                    Call ModificarConfiguracion(codDepart, blnvisualcare, camaCun, EntornoDboid, EnvironmentDboid)
                    If Not NuevaAdmisionOK(codAsistencia, PatientDboid, AdmisionDboid, LocationId) Then Exit Sub
        Else
            'En el caso de que el paciente fuera nuevo
            Call NuevoPaciente(numHistoria, PatientDboid)
            blnError = CambiarStationID(LocationId)
            If blnError Then
                MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
                ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                Exit Sub
            End If
                    Call ModificarConfiguracion(codDepart, blnvisualcare, camaCun, EntornoDboid, EnvironmentDboid)
                    If Not NuevaAdmisionOK(codAsistencia, PatientDboid, AdmisionDboid, LocationId) Then Exit Sub
            'Se introduce en la
            'nueva admision el PatientDboid. Luego no se vuelve a comparar.
        End If
    End If

'''-------------------------------------------
''        'Se introduce en la
''    'nueva admision el PatientDboid. Luego no se vuelve a comparar.
''    'En el caso de que el paciente fuera nuevo
''    End If
''Else 'sino existe se crea
''    Call NuevoPaciente(numHistoria, PatientDboid)
''    blnError = CambiarStationID(LocationId)
''    If blnError Then
''        MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
''        ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
''        ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
''        Exit Sub
''    End If
''    If Not NuevaAdmisionOK(codAsistencia, PatientDboid, AdmisionDboid) Then Exit Sub
''    'Se introduce en la
''    'nueva admision el PatientDboid. Luego no se vuelve a comparar.
''End If

''''If codDepart = "213" And Not blnvisualcare Then
''''    cama = Mid(camaCun, 1, 4)
''''    If cama <> "0105" Then
''''        EnvironmentDboid = EntornoDboid
''''    Else
''''        'Escoge el environment que est� por defecto
''''        ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
''''        If ierrcode <> ERRCODE_ALL_OK Then
''''                    MsgBox "Error al alocatar la configuraci�n"
''''        End If
''''        ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
''''        If ierrcode <> ERRCODE_ALL_OK Then
''''                    MsgBox "Error "
''''        End If
''''        ierrcode = PcsCfgReadString(CurrentCfgHandle, "Adt", "General", "DefaultEnvironmentType", EnvironmentDboid)
''''        If ierrcode <> ERRCODE_ALL_OK Then
''''                    MsgBox "Error al leer de la configuraci�n"
''''        End If
''''    End If
''''Else
''''    'Escoge el environment que est� por defecto
''''    ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
''''    If ierrcode <> ERRCODE_ALL_OK Then
''''                MsgBox "Error al alocatar la configuraci�n"
''''    End If
''''    ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
''''    If ierrcode <> ERRCODE_ALL_OK Then
''''                MsgBox "Error "
''''    End If
''''    ierrcode = PcsCfgReadString(CurrentCfgHandle, "Adt", "General", "DefaultEnvironmentType", EnvironmentDboid)
''''    If ierrcode <> ERRCODE_ALL_OK Then
''''                MsgBox "Error al leer de la configuraci�n"
''''    End If
''''
''''End If


        

'Abre VC o C+ con la admision seleccionada sino est� abierto antes
'Si ya est�n abiertas esas aplicaciones simplemente realiza la admisi�n
'pero no abre el ejecutable
If Not blnvisualcare And CpuEstado = 2 Then
    Call AbrirRemoteView(AdmisionDboid, PatientDboid, EnvironmentDboid)
Else
    Call AbrirSinoEstaAbierto(blnvisualcare, AdmisionDboid, PatientDboid, EnvironmentDboid)
End If
'Libera las variables
 ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
 ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)

End Sub

Public Function AdmitirEnNuevaCama(codAsistencia As Long, PatientDboid As String, LocationId As Long) As Boolean
Dim Systime As Long
Dim Loc As Long
Dim Env As Long
AdmitirEnNuevaCama = True
ierrcode = PcsAdtObjectAlloc(hAdt, hLog)
ierrcode = PcsLocationAlloc(hLocation)
ierrcode = PcsDbObjectAllocFromIdentifier(hPatient, PatientDboid)

ierrcode = PcsLocationGetFromLocationId(hLog, LocationId, hLocation)

Systime = Env = 0
ierrcode = PcsAdtObjectAdmit(hAdt, hPatient, hLocation, ByVal Env, hLog, ByVal Systime)
If ierrcode <> 0 And ierrcode <> 705 Then               'Error: Falla mensaje de PIMS
    AdmitirEnNuevaCama = False
    MsgBox "Error al realizar la admision del paciente", vbExclamation, "PICIS"
End If
ierrcode = PcsAdtObjectFree(hAdt)
ierrcode = PcsLocationFree(hLocation)
End Function


''Public Function Alta(codAsistencia As Long)
''Dim condition As String
''Dim Asistencia As String
''Dim cama As String
''Dim Systime As Long
''Dim codPersona As Long
''Dim numHistoria As Long
''Dim Historia As String
''Dim sNull As String
''Call LOG
''Call BuscarPacienteDesdeAsistencia(codAsistencia, numHistoria)
''ierrcode = PcsListObjectsAlloc(hList)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''
''ierrcode = PcsPatientAlloc(hPatient)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''Historia = CStr(numHistoria)
'''Build the condition string
''condition = "$='" + Historia + "'"
'''The only condition is the Patient name, so the other fields are empty strings
''ierrcode = PcsPatientFindWithMembers(hLog, hList, condition, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''ierrcode = PcsListObjectsGotoHeadPosition(hList)
''ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hPatient)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''ierrcode = PcsListObjectsFree(hList)
''
'''ierrcode = PcsDischargeAlloc(hDis)
''ierrcode = PcsListObjectsAlloc(hList)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''condition = "$='" + "Inconnue" + "'" 'MASIN: Cambiar por Unknown cuando
'''se cambie la base de datos
''ierrcode = PcsDischargeFindWithMembers(hLog, hList, condition)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''ierrcode = PcsListObjectsGotoHeadPosition(hList)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hDis)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''ierrcode = PcsAdtObjectAlloc(hDischarge, hLog)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''Systime = 0
''ierrcode = PcsAdtObjectDischarge(hDischarge, hPatient, hDis, hLog, ByVal Systime)
''If ierrcode <> ERRCODE_ALL_OK Then
''    MsgBox "Error"
''End If
''ierrcode = PcsListObjectsFree(hList)
''ierrcode = PcsPatientFree(hPatient)
''ierrcode = PcsAdtObjectFree(hDischarge)
''
''ierrcode = PcsLogLogOff(hLog)
''ierrcode = PcsLogFree(hLog)
''
''End Function

''Public Function Admision(codAsistencia As Long)
''Dim numHistoria As Long
''Dim PatientDboid As String * 21
''Dim AdmisionDboid As String * 21
''Dim variable As Variant
''Dim strOrdenador As String
''Dim condition As String
''Dim sNull As String
''Dim EnvironmentDboid As String
''Dim strToExecute As String
''Dim ierrcode As Long
''Call LOG
'''************************************************
'''* PACIENTE Y ADMISI�N                          *
'''************************************************
''If ExistePaciente(codAsistencia, numHistoria) Then 'Si existe se modifica
''    Call ModificarPaciente(numHistoria, PatientDboid)
''    If ExisteAdmision(codAsistencia) Then
''        Call ModificarAdmision(codAsistencia, AdmisionDboid) 'No se introduce el Patientdboid
''    'porque se supone que es el mismo.
''    Else
''        Call NuevaAdmision(codAsistencia, PatientDboid, AdmisionDboid) 'Se introduce en la
''    'nueva admision el PatientDboid. Luego no se vuelve a comparar.
''    End If
''Else 'sino existe se crea
''    Call NuevoPaciente(numHistoria, PatientDboid)
''    Call NuevaAdmision(codAsistencia, PatientDboid, AdmisionDboid) 'Se introduce en la
''    'nueva admision el PatientDboid. Luego no se vuelve a comparar.
''End If
''
''ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
''If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "Error al alocatar la configuraci�n"
''End If
''ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
''If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "Error "
''End If
''ierrcode = PcsCfgReadString(CurrentCfgHandle, "Adt", "General", "DefaultEnvironmentType", EnvironmentDboid)
''If ierrcode <> ERRCODE_ALL_OK Then
''            MsgBox "Error al leer de la configuraci�n"
''End If
''If ConexionesVisualCare = 0 Then
''strToExecute = "C:\Picis\VisualCare\VisualCare /" & AdmisionDboid & "/" & PatientDboid & "/" & EnvironmentDboid
''ierrcode = Shell(strToExecute, vbMaximizedFocus)
''End If
'' ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'' ierrcode = PcsLogLogOff(hLog)
'' ierrcode = PcsLogFree(hLog)
''End Function

'''''Public Sub Admision(codAsistencia As Long, blnvisualcare As Boolean, EntornoDboid As String)
'''''Dim numHistoria As Long
'''''Dim PatientDboid As String * 21
'''''Dim AdmisionDboid As String * 21
'''''Dim variable As Variant
'''''Dim strOrdenador As String
'''''Dim condition As String
'''''Dim sNull As String
'''''Dim EnvironmentDboid As String
'''''Dim ierrcode As Long
'''''Dim blnAbierto, blnError As Boolean
'''''Dim LocationId As Long
'''''Dim codDepart As String
'''''Dim camaCun As String
'''''Dim cama As String
'''''
'''''codAsistencia = objPipe.PipeGet("PI_CodAsistencia")
'''''blnvisualcare = objPipe.PipeGet("PI_blnVisualcare")
'''''camaCun = objPipe.PipeGet("PI_CodCama")
'''''codDepart = objPipe.PipeGet("PI_CodDepart")
'''''
'''''If Not LOG Then
'''''    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''    Exit Sub
'''''End If
'''''
'''''    LocationId = ObtenerPicisLocationID(camaCun) 'Cambiar el archivo DbApi.pcs
'''''
'''''If ExistePaciente(codAsistencia, numHistoria) Then 'Si existe se modifica
'''''    Call ModificarPaciente(numHistoria, PatientDboid)
'''''
'''''    If ExisteAdmision(codAsistencia) Then
'''''        Call ModificarAdmision(codAsistencia, AdmisionDboid) 'No se introduce el Patientdboid
'''''        'porque se supone que es el mismo. Si el paciente ya estuviera admitido o transaladado
'''''        If AdmisionTransladada(codAsistencia) Then 'El caso de que estuviera transaldado
'''''                If EstadoLocationOK(LocationId, 2, codAsistencia) Then
'''''                    blnError = CambiarStationID(LocationId)
'''''                    If blnError Then
'''''                        MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
'''''                        ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''                        ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''                        Exit Sub
'''''                    End If
'''''                    If Not Admitir(codAsistencia, PatientDboid) Then Exit Sub      'Admitirlo
'''''                Else
'''''                    MsgBox "La Location est� ocupada por un paciente distinto al seleccionado.", vbExclamation
'''''                    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''                    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''                    Exit Sub
'''''                End If
'''''        Else ' Admision en curso
'''''                If Not EstadoLocationOK(LocationId, 1, codAsistencia) Then
'''''                    MsgBox "La Location est� ocupada por un paciente distinto al seleccionado.", vbExclamation
'''''                    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''                    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''                    Exit Sub
'''''                Else
'''''                    blnError = CambiarStationID(LocationId)
'''''                    If blnError Then
'''''                        MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
'''''                        ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''                        ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''                        Exit Sub
'''''                    End If
'''''                End If
'''''        End If
'''''    Else 'no existe la admision con este codAsistencia
'''''        If Not EstadoLocationOK(LocationId, 3, codAsistencia) Then
'''''            MsgBox "La Location est� ocupada por un paciente distinto al seleccionado.", vbExclamation
'''''            ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''            ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''            Exit Sub
'''''        Else
'''''            blnError = CambiarStationID(LocationId)
'''''            If blnError Then
'''''                MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
'''''                ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''                ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''                Exit Sub
'''''            End If
'''''        End If
'''''        If Not NuevaAdmisionOK(codAsistencia, PatientDboid, AdmisionDboid) Then Exit Sub
'''''        'Se introduce en la
'''''    'nueva admision el PatientDboid. Luego no se vuelve a comparar.
'''''    'En el caso de que el paciente fuera nuevo
'''''    End If
'''''Else 'sino existe se crea
'''''    Call NuevoPaciente(numHistoria, PatientDboid)
'''''    blnError = CambiarStationID(LocationId)
'''''    If blnError Then
'''''        MsgBox "Ha fallado el cambio de localizaci�n", vbCritical
'''''        ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''        ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''        Exit Sub
'''''    End If
'''''    If Not NuevaAdmisionOK(codAsistencia, PatientDboid, AdmisionDboid) Then Exit Sub
'''''    'Se introduce en la
'''''    'nueva admision el PatientDboid. Luego no se vuelve a comparar.
'''''End If
'''''
'''''If codDepart = "213" And Not blnvisualcare Then
'''''    cama = Mid(camaCun, 1, 4)
'''''    If cama <> "0105" Then
'''''        EnvironmentDboid = EntornoDboid
'''''    Else
'''''        'Escoge el environment que est� por defecto
'''''        ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
'''''        If ierrcode <> ERRCODE_ALL_OK Then
'''''                    MsgBox "Error al alocatar la configuraci�n"
'''''        End If
'''''        ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
'''''        If ierrcode <> ERRCODE_ALL_OK Then
'''''                    MsgBox "Error "
'''''        End If
'''''        ierrcode = PcsCfgReadString(CurrentCfgHandle, "Adt", "General", "DefaultEnvironmentType", EnvironmentDboid)
'''''        If ierrcode <> ERRCODE_ALL_OK Then
'''''                    MsgBox "Error al leer de la configuraci�n"
'''''        End If
'''''    End If
'''''
'''''End If
'''''
''''''Abre VC o C+ con la admision seleccionada sino est� abierto antes
''''''Si ya est�n abiertas esas aplicaciones simplemente realiza la admisi�n
''''''pero no abre el ejecutable
'''''Call AbrirSinoEstaAbierto(blnvisualcare, AdmisionDboid, PatientDboid, EnvironmentDboid)
''''''Libera las variables
''''' ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
''''' ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''
'''''End Sub

Public Function ComprobarUsuario(codDepart As String) As Boolean
Dim strsql As String
Dim rs As rdoResultset
Dim qy As rdoQuery

strsql = "SELECT AD0300.SG02COD, AD0300.AD02CODDPTO FROM AD0300 "
strsql = strsql & "WHERE SG02COD = ? AND AD02CODDPTO = ?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = strCodUser
qy(1) = codDepart
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rs.RowCount > 0 Then
    ComprobarUsuario = True
Else
    ComprobarUsuario = False
End If
End Function

Public Function AbrirRemoteView(AdmisionDboid As String, PatientDboid As String, EnvironmentDboid As String)
Dim strToExecute As String
Dim strsql As String
Dim qy As rdoQuery
Dim rs As rdoResultset
Dim PicisdaDboid As String * 21

strsql = "SELECT D.PICISDATADBOID FROM PICIS.PICISDATA D "
strsql = strsql & "WHERE D.ADMISSIONDBOID = ?"

Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = AdmisionDboid
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    PicisdaDboid = rs!PICISDATADBOID
        ChDir "C:\Picis\ChartPlus"
        strToExecute = "C:\Picis\ChartPlus\CPRemoteView.exe /A=" & AdmisionDboid & " /P=" & PatientDboid & " /C=" & EnvironmentDboid & " /D=0" & PicisdaDboid
        ierrcode = Shell(strToExecute, vbMaximizedFocus)

End Function

Public Function CambiarEntorno(EntornoDboid As String) As Boolean
' Si hay alg�n error en la funcion devuelve una variable True
Dim vrtList(3) As Variant
vrtList(0) = CONFIG_TYPE_STRING
vrtList(1) = "DefaultEnvironmentType"
vrtList(2) = EntornoDboid
vrtList(3) = CONFIG_TYPE_ENDPARM
ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error al alocatar la configuraci�n"
End If
ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error "
End If
ierrcode = PcsCfgEntriesIntoSection(CurrentCfgHandle, "Adt", "General", True, vrtList())
If ierrcode <> ERRCODE_ALL_OK Then CambiarEntorno = True

ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
End Function

Public Function ModificarConfiguracion(codDepart As String, blnvisualcare As Boolean, camaCun As String, EntornoDboid As String, EnvironmentDboid As String)
Dim cama As String
Dim blnError As Boolean

'If Not blnvisualcare Then
    Select Case codDepart
    Case "213" 'quirofano
        cama = Mid(camaCun, 1, 4)
        If cama <> "0105" Then
            EnvironmentDboid = EntornoDboid
            blnError = CambiarEntorno(EntornoDboid)
            If blnError Then
                MsgBox "Ha fallado el cambio de entorno", vbCritical
                ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                Exit Function
            End If
        Else
            'Escoge el environment que est� por defecto
            ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
            If ierrcode <> ERRCODE_ALL_OK Then
                        MsgBox "Error al alocatar la configuraci�n"
            End If
            ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
            If ierrcode <> ERRCODE_ALL_OK Then
                        MsgBox "Error "
            End If
            ierrcode = PcsCfgReadString(CurrentCfgHandle, "Adt", "General", "DefaultEnvironmentType", EnvironmentDboid)
            If ierrcode <> ERRCODE_ALL_OK Then
                        MsgBox "Error al leer de la configuraci�n"
            End If
        End If
    Case "318" 'Uci Adultos
            EntornoDboid = "041000000000004000511"
            EnvironmentDboid = EntornoDboid
            blnError = CambiarEntorno(EntornoDboid)
            If blnError Then
                MsgBox "Ha fallado el cambio de entorno", vbCritical
                ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                Exit Function
            End If
    Case Else 'Si el paciente es de cualquier planta
            EntornoDboid = "041000000000003000511"
            EnvironmentDboid = EntornoDboid
            blnError = CambiarEntorno(EntornoDboid)
            If blnError Then
                MsgBox "Ha fallado el cambio de entorno", vbCritical
                ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                Exit Function
            End If
'    Case Else
'            'Escoge el environment que est� por defecto
'            ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
'            If ierrcode <> ERRCODE_ALL_OK Then
'                        MsgBox "Error al alocatar la configuraci�n"
'            End If
'            ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
'            If ierrcode <> ERRCODE_ALL_OK Then
'                        MsgBox "Error "
'            End If
'            ierrcode = PcsCfgReadString(CurrentCfgHandle, "Adt", "General", "DefaultEnvironmentType", EnvironmentDboid)
'            If ierrcode <> ERRCODE_ALL_OK Then
'                        MsgBox "Error al leer de la configuraci�n"
'            End If
    End Select
'Else

'    'Escoge el environment que est� por defecto
'    ierrcode = PcsCfgHandleAlloc(CurrentCfgHandle)
'    If ierrcode <> ERRCODE_ALL_OK Then
'                MsgBox "Error al alocatar la configuraci�n"
'    End If
'    ierrcode = PcsCfgWorldSet(CurrentCfgHandle, "DBAPI")
'    If ierrcode <> ERRCODE_ALL_OK Then
'                MsgBox "Error "
'    End If
'    ierrcode = PcsCfgReadString(CurrentCfgHandle, "Adt", "General", "DefaultEnvironmentType", EnvironmentDboid)
'    If ierrcode <> ERRCODE_ALL_OK Then
'                MsgBox "Error al leer de la configuraci�n"
'    End If
'
'End If

End Function

Public Function ReAdmision(AdmisionDboid As String)
Dim strsql As String
Dim qy As rdoQuery
Dim rs As rdoResultset
Dim PicisdaDboid As String
Dim EnvironDboid As String
Dim strU As String
Dim QyU As rdoQuery
Dim strV As String
Dim QyV As rdoQuery
Dim RsV As rdoResultset

'objApp.BeginTrans

'Se pone a Null el campo ENDED de la tabla ADMISSIONS

            strsql = "UPDATE ADMISSIONS SET ENDED = NULL WHERE " _
                    & "ADMISSIONDBOID = ? AND ENDED=(SELECT MAX(ENDED) FROM ADMISSIONS WHERE ADMISSIONDBOID = ?)"
            Set qy = objApp.rdoConnect.CreateQuery("", strsql)
            qy(0) = AdmisionDboid
            qy(1) = AdmisionDboid
            qy.Execute
'                If Qy.RowsAffected = 0 Then
'                  objApp.RollbackTrans
'                  MsgBox "Error al actualizar la admisi�n"
'                  Exit Function
'                End If
            qy.Close

'Se obtiene el PicisDataDboid y el EnvironmentDboid de la admision

'Se actualiza la tabla Environments

strsql = "SELECT D.PICISDATADBOID "
strsql = strsql & "FROM PICIS.PICISDATA D "
strsql = strsql & "WHERE (D.ADMISSIONDBOID =?) "

Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = AdmisionDboid
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

    PicisdaDboid = rs!PICISDATADBOID
    
            strU = "UPDATE ENVIRONMENTS SET ENDED = NULL WHERE " _
                    & "PICISDATADBOID = ? AND ENDED=(SELECT MAX(ENDED) FROM ENVIRONMENTS WHERE PICISDATADBOID = ?)"
            Set QyU = objApp.rdoConnect.CreateQuery("", strU)
            QyU(0) = PicisdaDboid
            QyU(1) = PicisdaDboid
            QyU.Execute

'            If QyU.RowsAffected = 0 Then
'              objApp.RollbackTrans
'              MsgBox "Error al actualizar la tabla Environments"
'              Exit Function
'            End If
            QyU.Close
'Se actualiza la tabla EnvironmentLocation
                strV = "SELECT E.ENVIRONMENTDBOID ENVDBOID "
                strV = strV & "FROM PICIS.ENVIRONMENTS E "
                strV = strV & "WHERE PICISDATADBOID = ? AND (E.ENDED Is Null)"
                
                Set QyV = objApp.rdoConnect.CreateQuery("", strV)
                QyV(0) = PicisdaDboid
                Set RsV = QyV.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
                
                    EnvironDboid = RsV!ENVDBOID
                RsV.Close
                QyV.Close
                
            
                    strU = "UPDATE ENVIRONMENTLOCATION SET ENDED = NULL WHERE " _
                            & "ENVIRONMENTDBOID = ? AND ENDED=(SELECT MAX(ENDED) FROM ENVIRONMENTLOCATION WHERE ENVIRONMENTDBOID = ?)"
                    Set QyU = objApp.rdoConnect.CreateQuery("", strU)
                    QyU(0) = EnvironDboid
                    QyU(1) = EnvironDboid
                    QyU.Execute
            
'                    If QyU.RowsAffected = 0 Then
'                      objApp.RollbackTrans
'                      MsgBox "Error al actualizar la tabla EnvironmentLocation"
'                      Exit Function
'                    End If
                    QyU.Close


rs.Close
qy.Close

'objApp.CommitTrans

End Function
