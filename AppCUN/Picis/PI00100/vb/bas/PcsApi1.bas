Attribute VB_Name = "Module2"
#If DebugVersion Then

Public Declare Function PcsAddCallback Lib "PCSDB500D.DLL" (ByVal in_callback As Long, ByRef in_puserdata As Long) As Integer
Public Declare Function PcsAdmissionAddAmbulatoryStatus Lib "PCSDB500D.DLL" (ByVal in_hadmission As Long, ByVal in_hambulstatus As Long) As Integer
Public Declare Function PcsAdmissionAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAdmissionDeleteAllAmbulatoryStatus Lib "PCSDB500D.DLL" (ByVal in_hadmission As Long) As Integer
Public Declare Function PcsAdmissionDeleteAmbulatoryStatus Lib "PCSDB500D.DLL" (ByVal in_hadmission As Long, ByVal in_hambulstatus As Long) As Integer
Public Declare Function PcsAdmissionFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pFirstIdentifier_cond As String, _
ByVal in_pSecondIdentifier_cond As String, ByVal in_pThirdIdentifier_cond As String, ByVal in_pAdmissionDate_cond As String, ByVal in_pDepartureDate_cond As String, ByVal in_pPreAdmission_cond As String, _
ByVal in_pWeight_cond As String, ByVal in_pHeight_cond As String, ByVal in_pStreet_cond As String, ByVal in_pCity_cond As String, ByVal in_pState_cond As String, ByVal in_pZipCode_cond As String, _
ByVal in_pTelephone_cond As String, ByVal in_pAccountNumber_cond As String, ByVal in_pEMail_cond As String, ByVal in_pNearestKinName_cond As String, ByVal in_pNearestKinPhone_cond As String, _
ByVal in_pObservations_cond As String, ByVal in_pPatient_cond As String, ByVal in_pCountry_cond As String, ByVal in_pAdmissionType_cond As String, ByVal in_pDischarge_cond As String) As Integer
Public Declare Function PcsAdmissionFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAdmissionGetAccountNumber Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pAccountNumber As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetAdmissionDate Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsAdmissionGetAdmissionType Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAdmissionGetAdmissionTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetAllAmbulatoryStatus Lib "PCSDB500D.DLL" (ByVal in_hadmission As Long, ByVal out_hambulstatus_list As Long) As Integer
Public Declare Function PcsAdmissionGetCity Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pCity As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetCountry Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAdmissionGetCountryIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetDepartureDate Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsAdmissionGetDischarge Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAdmissionGetDischargeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetEMail Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pEMail As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetFirstIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pFirstIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetHeight Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsAdmissionGetNearestKinName Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pNearestKinName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetNearestKinPhone Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pNearestKinPhone As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetObservations Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pObservations As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetPatient Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAdmissionGetPatientIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetPicisData Lib "PCSDB500D.DLL" (ByVal in_hadmission As Long, ByRef out_phpicisdata As Long) As Integer
Public Declare Function PcsAdmissionGetPreAdmission Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsAdmissionGetSecondIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pSecondIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetState Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pState As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetStreet Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pStreet As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetTelephone Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pTelephone As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetThirdIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pThirdIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetWeight Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsAdmissionGetZipCode Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pZipCode As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsAdmissionSetAccountNumber Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pAccountNumber As String) As Integer
Public Declare Function PcsAdmissionSetAdmissionDate Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsAdmissionSetAdmissionType Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAdmissionSetAdmissionTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAdmissionSetAllAmbulatoryStatus Lib "PCSDB500D.DLL" (ByVal in_hadmission As Long, ByVal in_hambulstatus_list As Long) As Integer
Public Declare Function PcsAdmissionSetCity Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pCity As String) As Integer
Public Declare Function PcsAdmissionSetCountry Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAdmissionSetCountryIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAdmissionSetDepartureDate Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsAdmissionSetDischarge Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAdmissionSetDischargeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAdmissionSetEMail Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pEMail As String) As Integer
Public Declare Function PcsAdmissionSetFirstIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pFirstIdentifier As String) As Integer
Public Declare Function PcsAdmissionSetHeight Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsAdmissionSetNearestKinName Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pNearestKinName As String) As Integer
Public Declare Function PcsAdmissionSetNearestKinPhone Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pNearestKinPhone As String) As Integer
Public Declare Function PcsAdmissionSetObservations Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pObservations As String) As Integer
Public Declare Function PcsAdmissionSetPatient Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAdmissionSetPatientIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAdmissionSetPreAdmission Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsAdmissionSetSecondIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pSecondIdentifier As String) As Integer
Public Declare Function PcsAdmissionSetState Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pState As String) As Integer
Public Declare Function PcsAdmissionSetStreet Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pStreet As String) As Integer
Public Declare Function PcsAdmissionSetTelephone Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pTelephone As String) As Integer
Public Declare Function PcsAdmissionSetThirdIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pThirdIdentifier As String) As Integer
Public Declare Function PcsAdmissionSetWeight Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsAdmissionSetZipCode Lib "PCSDB500D.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pZipCode As String) As Integer
Public Declare Function PcsAdmissionTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAdmissionTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsAdmissionTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAdmissionTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHADMISSIONTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsAdmissionTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHADMISSIONTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsAmbulatoryStatusAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAmbulatoryStatusFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsAmbulatoryStatusFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAmbulatoryStatusGetAll Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hambulstatus_list As Long) As Integer
Public Declare Function PcsAmbulatoryStatusGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHAMBULATORYSTATUS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAmbulatoryStatusListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsAmbulatoryStatusSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHAMBULATORYSTATUS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsAnalysisAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAnalysisFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsAnalysisFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAnalysisGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHANALYSIS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAnalysisListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsAnalysisSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHANALYSIS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsApplicationAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsApplicationFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsApplicationFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsApplicationGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsApplicationGetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal io_AppExeName As String, ByVal out_pAppDboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsApplicationIsIn Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_pappdboid As String, ByRef out_pyes As Long) As Integer
Public Declare Function PcsApplicationListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsApplicationRegister Lib "PCSDB500D.DLL" (ByVal in_ptext As String, ByRef out_pwas_already_registered As Long) As Integer
Public Declare Function PcsApplicationSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsApplicationVersionAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsApplicationVersionFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pInstallDate_cond As String, ByVal in_pApplication_cond As String) As Integer
Public Declare Function PcsApplicationVersionFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsApplicationVersionGetApplication Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsApplicationVersionGetApplicationIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsApplicationVersionGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsApplicationVersionGetInstallDate Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsApplicationVersionListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsApplicationVersionSetApplication Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsApplicationVersionSetApplicationIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsApplicationVersionSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsApplicationVersionSetInstallDate Lib "PCSDB500D.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsASATypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsASATypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsASATypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsASATypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHASATYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsASATypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsASATypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHASATYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsAssessmentItemAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAssessmentItemFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIndex_cond As String, _
ByVal in_pCode_cond As String, ByVal in_pTreatment_cond As String) As Integer
Public Declare Function PcsAssessmentItemFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAssessmentItemGetCode Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal out_pCode As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAssessmentItemGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAssessmentItemGetIndex Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsAssessmentItemGetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAssessmentItemGetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAssessmentItemListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsAssessmentItemSetCode Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_pCode As String) As Integer
Public Declare Function PcsAssessmentItemSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsAssessmentItemSetIndex Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsAssessmentItemSetTreatment Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAssessmentItemSetTreatmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAttendingTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAttendingTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsAttendingTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAttendingTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHATTENDINGTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAttendingTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsAttendingTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHATTENDINGTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsBloodGroupAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsBloodGroupFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsBloodGroupFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsBloodGroupGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHBLOODGROUP As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsBloodGroupListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsBloodGroupSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHBLOODGROUP As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsCategoryAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsCategoryFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pDLLName_cond As String, _
ByVal in_pIsDeleted_cond As String, ByVal in_pCategoryType_cond As String) As Integer
Public Declare Function PcsCategoryFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCategoryGetCategoryType Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsCategoryGetCategoryTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryGetDLLName Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_pDLLName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsCategorySetCategoryType Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsCategorySetCategoryTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsCategorySetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsCategorySetDLLName Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_pDLLName As String) As Integer
Public Declare Function PcsCategorySetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsCategoryTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsCategoryTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsCategoryTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCategoryTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORYTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsCategoryTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCATEGORYTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsCfgAlloc Lib "PCSCFG500D.DLL" (ByRef out_phandle As Long) As Integer
Public Declare Function PcsCfgAllocRemote Lib "PCSCFG500D.DLL" (ByRef out_phandle As Long, ByVal in_pcomputername As String) As Integer
Public Declare Function PcsCfgFree Lib "PCSCFG500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCfgGenerateFullPath Lib "PCSCFG500D.DLL" (ByVal in_pdirectory As String, ByVal out_pdirectory As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetAllEntriesInSection Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal out_pallentries As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetAllSectionsInZone Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal out_pallsections As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetAllWorldsInComputer Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal out_pallworlds As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetAllZonesInWorld Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal out_pallzones As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetBitmap Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_pbitmap As Long, _
ByVal in_pdefaultinfo As String) As Integer
Public Declare Function PcsCfgGetBitmapFromBitmapInfo Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pinfo As String, ByRef out_phbitmap As Long) As Integer
Public Declare Function PcsCfgGetBitmapInfo Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal out_pinfo As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetBool Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef output_value As Long, _
ByVal in_default As Long) As Integer
Public Declare Function PcsCfgGetCallback Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByRef out_pcallback As Long, ByRef in_pData As Long) As Integer
Public Declare Function PcsCfgGetColor Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_pcolorref As Long, _
ByVal in_default As Long) As Integer
Public Declare Function PcsCfgGetDescription Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pdefault As String, _
ByVal out_result As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsCfgGetDouble Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_pdouble As Double, _
ByVal in_default As Double) As Integer
Public Declare Function PcsCfgGetEntryType Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_ptype As Long) As Integer
Public Declare Function PcsCfgGetFont Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_plogfont As Long, _
ByRef in_pdefault As Long) As Integer
Public Declare Function PcsCfgGetIcon Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_picon As Long, _
ByVal in_pdefaultinfo As String) As Integer
Public Declare Function PcsCfgGetIconFromIconInfo Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pinfo As String, ByRef out_phicon As Long) As Integer
Public Declare Function PcsCfgGetIconInfo Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal out_pinfo As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetInt Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef output_value As Long, _
ByVal in_default As Long) As Integer
Public Declare Function PcsCfgGetLimitedString Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pdefault As String, _
ByVal out_result As String, ByVal in_num_chars As Long, ByRef out_plimited_size As Long) As Integer
Public Declare Function PcsCfgGetLocation Lib "PCSCFG500D.DLL" (ByVal in_hconf As Long, ByVal out_pLocationID As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetPicisHomeDirectory Lib "PCSCFG500D.DLL" (ByVal out_pdirectory As String, ByVal in_num_max As Long, ByRef out_pwasdefault As Long) As Integer
Public Declare Function PcsCfgGetString Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pdefault As String, _
ByVal out_result As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsCfgGetUpdateFlag Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByRef out_pvalue As Long) As Integer
Public Declare Function PcsCfgGetWorld Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal out_pworld As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgModifyScreenBitmap Lib "PCSCFG500D.DLL" (ByVal in_hbitmap As Long, ByVal in_light As Long, ByVal in_dark As Long) As Integer
Public Declare Function PcsCfgSetBool Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long) As Integer
Public Declare Function PcsCfgSetCallback Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_callback As Long, ByRef in_pData As Long) As Integer
Public Declare Function PcsCfgSetColor Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long) As Integer
Public Declare Function PcsCfgSetDescription Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, _
ByVal in_pnew_value As String) As Integer
Public Declare Function PcsCfgSetDouble Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, _
ByVal in_new_value As Double) As Integer
Public Declare Function PcsCfgSetFont Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef in_pnew_value As Long) As Integer
Public Declare Function PcsCfgSetInt Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long) As Integer
Public Declare Function PcsCfgSetLimitedString Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pnew_value As String, _
ByVal in_num_limited_chars As Long, ByVal in_language_indepenedent As Long) As Integer
Public Declare Function PcsCfgSetLocation Lib "PCSCFG500D.DLL" (ByVal in_hconf As Long, ByVal in_LocationID As String) As Integer
Public Declare Function PcsCfgSetString Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pnew_value As String, _
ByVal in_language_indepenedent As Long) As Integer
Public Declare Function PcsCfgSetUpdateFlag Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_new_value As Long) As Integer
Public Declare Function PcsCfgSetWorld Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pworld As String) As Integer
Public Declare Function PcsCfgWriteBitmapInfo Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, _
ByVal in_pinfo As String) As Integer
Public Declare Function PcsCfgWriteIconInfo Lib "PCSCFG500D.DLL" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, _
ByVal in_pinfo As String) As Integer
Public Declare Function PcsChiefComplaintAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsChiefComplaintFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsChiefComplaintFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsChiefComplaintGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCHIEFCOMPLAINT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsChiefComplaintListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsChiefComplaintSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCHIEFCOMPLAINT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsComponentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsComponentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsComponentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsComponentGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCOMPONENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsComponentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsComponentSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCOMPONENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsConfigGetInt Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Long) As Integer
Public Declare Function PcsConfigGetString Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pdefault As String, ByVal out_result As String, _
ByVal in_num_chars As Long) As Integer
Public Declare Function PcsConfigSetInt Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long, _
ByVal in_update As Long) As Integer
Public Declare Function PcsConfigSetString Lib "PCSDB500D.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pnew_value As String, _
ByVal in_language_independent As Long, ByVal in_update As Long) As Integer
Public Declare Function PcsCostAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsCostFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDate_cond As String, ByVal in_pValue_cond As String, _
ByVal in_pTableRef_cond As String) As Integer
Public Declare Function PcsCostFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCostGetDate Lib "PCSDB500D.DLL" (ByVal in_hHCOST As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsCostGetTableRefIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHCOST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCostGetValue Lib "PCSDB500D.DLL" (ByVal in_hHCOST As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsCostListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsCostSetDate Lib "PCSDB500D.DLL" (ByVal in_hHCOST As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, _
ByVal in_second As Long) As Integer
Public Declare Function PcsCostSetTableRefIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHCOST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsCostSetValue Lib "PCSDB500D.DLL" (ByVal in_hHCOST As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsCountryAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsCountryFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsCountryFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCountryGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCOUNTRY As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCountryListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsCountrySetDescription Lib "PCSDB500D.DLL" (ByVal in_hHCOUNTRY As Long, ByVal in_pDescription As String) As Integer

#Else

Public Declare Function PcsAddCallback Lib "PCSDB500.DLL" (ByVal in_callback As Long, ByRef in_puserdata As Long) As Integer
Public Declare Function PcsAdmissionAddAmbulatoryStatus Lib "PCSDB500.DLL" (ByVal in_hadmission As Long, ByVal in_hambulstatus As Long) As Integer
Public Declare Function PcsAdmissionAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAdmissionDeleteAllAmbulatoryStatus Lib "PCSDB500.DLL" (ByVal in_hadmission As Long) As Integer
Public Declare Function PcsAdmissionDeleteAmbulatoryStatus Lib "PCSDB500.DLL" (ByVal in_hadmission As Long, ByVal in_hambulstatus As Long) As Integer
Public Declare Function PcsAdmissionFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pFirstIdentifier_cond As String, _
ByVal in_pSecondIdentifier_cond As String, ByVal in_pThirdIdentifier_cond As String, ByVal in_pAdmissionDate_cond As String, ByVal in_pDepartureDate_cond As String, ByVal in_pPreAdmission_cond As String, _
ByVal in_pWeight_cond As String, ByVal in_pHeight_cond As String, ByVal in_pStreet_cond As String, ByVal in_pCity_cond As String, ByVal in_pState_cond As String, ByVal in_pZipCode_cond As String, _
ByVal in_pTelephone_cond As String, ByVal in_pAccountNumber_cond As String, ByVal in_pEMail_cond As String, ByVal in_pNearestKinName_cond As String, ByVal in_pNearestKinPhone_cond As String, _
ByVal in_pObservations_cond As String, ByVal in_pPatient_cond As String, ByVal in_pCountry_cond As String, ByVal in_pAdmissionType_cond As String, ByVal in_pDischarge_cond As String) As Integer
Public Declare Function PcsAdmissionFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAdmissionGetAccountNumber Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pAccountNumber As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetAdmissionDate Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsAdmissionGetAdmissionType Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAdmissionGetAdmissionTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetAllAmbulatoryStatus Lib "PCSDB500.DLL" (ByVal in_hadmission As Long, ByVal out_hambulstatus_list As Long) As Integer
Public Declare Function PcsAdmissionGetCity Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pCity As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetCountry Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAdmissionGetCountryIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetDepartureDate Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsAdmissionGetDischarge Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAdmissionGetDischargeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetEMail Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pEMail As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetFirstIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pFirstIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetHeight Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsAdmissionGetNearestKinName Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pNearestKinName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetNearestKinPhone Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pNearestKinPhone As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetObservations Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pObservations As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetPatient Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAdmissionGetPatientIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetPicisData Lib "PCSDB500.DLL" (ByVal in_hadmission As Long, ByRef out_phpicisdata As Long) As Integer
Public Declare Function PcsAdmissionGetPreAdmission Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsAdmissionGetSecondIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pSecondIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetState Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pState As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetStreet Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pStreet As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetTelephone Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pTelephone As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetThirdIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pThirdIdentifier As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionGetWeight Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsAdmissionGetZipCode Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal out_pZipCode As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsAdmissionSetAccountNumber Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pAccountNumber As String) As Integer
Public Declare Function PcsAdmissionSetAdmissionDate Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsAdmissionSetAdmissionType Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAdmissionSetAdmissionTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAdmissionSetAllAmbulatoryStatus Lib "PCSDB500.DLL" (ByVal in_hadmission As Long, ByVal in_hambulstatus_list As Long) As Integer
Public Declare Function PcsAdmissionSetCity Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pCity As String) As Integer
Public Declare Function PcsAdmissionSetCountry Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAdmissionSetCountryIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAdmissionSetDepartureDate Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsAdmissionSetDischarge Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAdmissionSetDischargeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAdmissionSetEMail Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pEMail As String) As Integer
Public Declare Function PcsAdmissionSetFirstIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pFirstIdentifier As String) As Integer
Public Declare Function PcsAdmissionSetHeight Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsAdmissionSetNearestKinName Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pNearestKinName As String) As Integer
Public Declare Function PcsAdmissionSetNearestKinPhone Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pNearestKinPhone As String) As Integer
Public Declare Function PcsAdmissionSetObservations Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pObservations As String) As Integer
Public Declare Function PcsAdmissionSetPatient Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAdmissionSetPatientIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAdmissionSetPreAdmission Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsAdmissionSetSecondIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pSecondIdentifier As String) As Integer
Public Declare Function PcsAdmissionSetState Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pState As String) As Integer
Public Declare Function PcsAdmissionSetStreet Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pStreet As String) As Integer
Public Declare Function PcsAdmissionSetTelephone Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pTelephone As String) As Integer
Public Declare Function PcsAdmissionSetThirdIdentifier Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pThirdIdentifier As String) As Integer
Public Declare Function PcsAdmissionSetWeight Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsAdmissionSetZipCode Lib "PCSDB500.DLL" (ByVal in_hHADMISSION As Long, ByVal in_pZipCode As String) As Integer
Public Declare Function PcsAdmissionTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAdmissionTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsAdmissionTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAdmissionTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHADMISSIONTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAdmissionTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsAdmissionTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHADMISSIONTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsAmbulatoryStatusAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAmbulatoryStatusFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsAmbulatoryStatusFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAmbulatoryStatusGetAll Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hambulstatus_list As Long) As Integer
Public Declare Function PcsAmbulatoryStatusGetDescription Lib "PCSDB500.DLL" (ByVal in_hHAMBULATORYSTATUS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAmbulatoryStatusListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsAmbulatoryStatusSetDescription Lib "PCSDB500.DLL" (ByVal in_hHAMBULATORYSTATUS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsAnalysisAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAnalysisFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsAnalysisFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAnalysisGetDescription Lib "PCSDB500.DLL" (ByVal in_hHANALYSIS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAnalysisListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsAnalysisSetDescription Lib "PCSDB500.DLL" (ByVal in_hHANALYSIS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsApplicationAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsApplicationFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsApplicationFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsApplicationGetDescription Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsApplicationGetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal io_AppExeName As String, ByVal out_pAppDboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsApplicationIsIn Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_pappdboid As String, ByRef out_pyes As Long) As Integer
Public Declare Function PcsApplicationListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsApplicationRegister Lib "PCSDB500.DLL" (ByVal in_ptext As String, ByRef out_pwas_already_registered As Long) As Integer
Public Declare Function PcsApplicationSetDescription Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsApplicationVersionAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsApplicationVersionFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pInstallDate_cond As String, ByVal in_pApplication_cond As String) As Integer
Public Declare Function PcsApplicationVersionFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsApplicationVersionGetApplication Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsApplicationVersionGetApplicationIdentifier Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsApplicationVersionGetDescription Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsApplicationVersionGetInstallDate Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsApplicationVersionListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsApplicationVersionSetApplication Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsApplicationVersionSetApplicationIdentifier Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsApplicationVersionSetDescription Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsApplicationVersionSetInstallDate Lib "PCSDB500.DLL" (ByVal in_hHAPPLICATIONVERSION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsASATypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsASATypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsASATypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsASATypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHASATYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsASATypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsASATypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHASATYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsAssessmentItemAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAssessmentItemFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIndex_cond As String, _
ByVal in_pCode_cond As String, ByVal in_pTreatment_cond As String) As Integer
Public Declare Function PcsAssessmentItemFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAssessmentItemGetCode Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal out_pCode As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAssessmentItemGetDescription Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAssessmentItemGetIndex Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsAssessmentItemGetTreatment Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsAssessmentItemGetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAssessmentItemListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsAssessmentItemSetCode Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_pCode As String) As Integer
Public Declare Function PcsAssessmentItemSetDescription Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsAssessmentItemSetIndex Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsAssessmentItemSetTreatment Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsAssessmentItemSetTreatmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHASSESSMENTITEM As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsAttendingTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsAttendingTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsAttendingTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsAttendingTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHATTENDINGTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsAttendingTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsAttendingTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHATTENDINGTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsBloodGroupAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsBloodGroupFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsBloodGroupFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsBloodGroupGetDescription Lib "PCSDB500.DLL" (ByVal in_hHBLOODGROUP As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsBloodGroupListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsBloodGroupSetDescription Lib "PCSDB500.DLL" (ByVal in_hHBLOODGROUP As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsCategoryAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsCategoryFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pDLLName_cond As String, _
ByVal in_pIsDeleted_cond As String, ByVal in_pCategoryType_cond As String) As Integer
Public Declare Function PcsCategoryFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCategoryGetCategoryType Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsCategoryGetCategoryTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryGetDescription Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryGetDLLName Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_pDLLName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsCategorySetCategoryType Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsCategorySetCategoryTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsCategorySetDescription Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsCategorySetDLLName Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_pDLLName As String) As Integer
Public Declare Function PcsCategorySetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHCATEGORY As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsCategoryTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsCategoryTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsCategoryTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCategoryTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHCATEGORYTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCategoryTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsCategoryTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHCATEGORYTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsCfgAlloc Lib "PCSCFG500.dll" (ByRef out_phandle As Long) As Integer
Public Declare Function PcsCfgAllocRemote Lib "PCSCFG500.dll" (ByRef out_phandle As Long, ByVal in_pcomputername As String) As Integer
Public Declare Function PcsCfgFree Lib "PCSCFG500.dll" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCfgGenerateFullPath Lib "PCSCFG500.dll" (ByVal in_pdirectory As String, ByVal out_pdirectory As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetAllEntriesInSection Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal out_pallentries As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetAllSectionsInZone Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal out_pallsections As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetAllWorldsInComputer Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal out_pallworlds As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetAllZonesInWorld Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal out_pallzones As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetBitmap Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_pbitmap As Long, _
ByVal in_pdefaultinfo As String) As Integer
Public Declare Function PcsCfgGetBitmapFromBitmapInfo Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pinfo As String, ByRef out_phbitmap As Long) As Integer
Public Declare Function PcsCfgGetBitmapInfo Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal out_pinfo As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetBool Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef output_value As Long, _
ByVal in_default As Long) As Integer
Public Declare Function PcsCfgGetCallback Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByRef out_pcallback As Long, ByRef in_pData As Long) As Integer
Public Declare Function PcsCfgGetColor Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_pcolorref As Long, _
ByVal in_default As Long) As Integer
Public Declare Function PcsCfgGetDescription Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pdefault As String, _
ByVal out_result As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsCfgGetDouble Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_pdouble As Double, _
ByVal in_default As Double) As Integer
Public Declare Function PcsCfgGetEntryType Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_ptype As Long) As Integer
Public Declare Function PcsCfgGetFont Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_plogfont As Long, _
ByRef in_pdefault As Long) As Integer
Public Declare Function PcsCfgGetIcon Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef out_picon As Long, _
ByVal in_pdefaultinfo As String) As Integer
Public Declare Function PcsCfgGetIconFromIconInfo Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pinfo As String, ByRef out_phicon As Long) As Integer
Public Declare Function PcsCfgGetIconInfo Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal out_pinfo As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetInt Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef output_value As Long, _
ByVal in_default As Long) As Integer
Public Declare Function PcsCfgGetLimitedString Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pdefault As String, _
ByVal out_result As String, ByVal in_num_chars As Long, ByRef out_plimited_size As Long) As Integer
Public Declare Function PcsCfgGetLocation Lib "PCSCFG500.dll" (ByVal in_hconf As Long, ByVal out_pLocationID As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgGetPicisHomeDirectory Lib "PCSCFG500.dll" (ByVal out_pdirectory As String, ByVal in_num_max As Long, ByRef out_pwasdefault As Long) As Integer
Public Declare Function PcsCfgGetString Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pdefault As String, _
ByVal out_result As String, ByVal in_num_chars As Long) As Integer
Public Declare Function PcsCfgGetUpdateFlag Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByRef out_pvalue As Long) As Integer
Public Declare Function PcsCfgGetWorld Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal out_pworld As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCfgModifyScreenBitmap Lib "PCSCFG500.dll" (ByVal in_hbitmap As Long, ByVal in_light As Long, ByVal in_dark As Long) As Integer
Public Declare Function PcsCfgSetBool Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long) As Integer
Public Declare Function PcsCfgSetCallback Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_callback As Long, ByRef in_pData As Long) As Integer
Public Declare Function PcsCfgSetColor Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long) As Integer
Public Declare Function PcsCfgSetDescription Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, _
ByVal in_pnew_value As String) As Integer
Public Declare Function PcsCfgSetDouble Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, _
ByVal in_new_value As Double) As Integer
Public Declare Function PcsCfgSetFont Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByRef in_pnew_value As Long) As Integer
Public Declare Function PcsCfgSetInt Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long) As Integer
Public Declare Function PcsCfgSetLimitedString Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pnew_value As String, _
ByVal in_num_limited_chars As Long, ByVal in_language_indepenedent As Long) As Integer
Public Declare Function PcsCfgSetLocation Lib "PCSCFG500.dll" (ByVal in_hconf As Long, ByVal in_LocationID As String) As Integer
Public Declare Function PcsCfgSetString Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pnew_value As String, _
ByVal in_language_indepenedent As Long) As Integer
Public Declare Function PcsCfgSetUpdateFlag Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_new_value As Long) As Integer
Public Declare Function PcsCfgSetWorld Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pworld As String) As Integer
Public Declare Function PcsCfgWriteBitmapInfo Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, _
ByVal in_pinfo As String) As Integer
Public Declare Function PcsCfgWriteIconInfo Lib "PCSCFG500.dll" (ByVal in_handle As Long, ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, _
ByVal in_pinfo As String) As Integer
Public Declare Function PcsChiefComplaintAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsChiefComplaintFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsChiefComplaintFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsChiefComplaintGetDescription Lib "PCSDB500.DLL" (ByVal in_hHCHIEFCOMPLAINT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsChiefComplaintListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsChiefComplaintSetDescription Lib "PCSDB500.DLL" (ByVal in_hHCHIEFCOMPLAINT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsComponentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsComponentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsComponentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsComponentGetDescription Lib "PCSDB500.DLL" (ByVal in_hHCOMPONENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsComponentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsComponentSetDescription Lib "PCSDB500.DLL" (ByVal in_hHCOMPONENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsConfigGetInt Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_default As Long) As Integer
Public Declare Function PcsConfigGetString Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pdefault As String, ByVal out_result As String, _
ByVal in_num_chars As Long) As Integer
Public Declare Function PcsConfigSetInt Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_new_value As Long, _
ByVal in_update As Long) As Integer
Public Declare Function PcsConfigSetString Lib "PCSDB500.DLL" (ByVal in_pzone As String, ByVal in_psection As String, ByVal in_pentry As String, ByVal in_pnew_value As String, _
ByVal in_language_independent As Long, ByVal in_update As Long) As Integer
Public Declare Function PcsCostAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsCostFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDate_cond As String, ByVal in_pValue_cond As String, _
ByVal in_pTableRef_cond As String) As Integer
Public Declare Function PcsCostFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCostGetDate Lib "PCSDB500.DLL" (ByVal in_hHCOST As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsCostGetTableRefIdentifier Lib "PCSDB500.DLL" (ByVal in_hHCOST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCostGetValue Lib "PCSDB500.DLL" (ByVal in_hHCOST As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsCostListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsCostSetDate Lib "PCSDB500.DLL" (ByVal in_hHCOST As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, _
ByVal in_second As Long) As Integer
Public Declare Function PcsCostSetTableRefIdentifier Lib "PCSDB500.DLL" (ByVal in_hHCOST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsCostSetValue Lib "PCSDB500.DLL" (ByVal in_hHCOST As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsCountryAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsCountryFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsCountryFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsCountryGetDescription Lib "PCSDB500.DLL" (ByVal in_hHCOUNTRY As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsCountryListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsCountrySetDescription Lib "PCSDB500.DLL" (ByVal in_hHCOUNTRY As Long, ByVal in_pDescription As String) As Integer


#End If


Global Const IDM_STOP_THREAD = 1000
Global Const IDM_VIEW_AUTO_ALIGN = 1008
Global Const IDM_VIEW_RIGHT = 1009
Global Const IDM_VIEW_BOTTOM = 1010
Global Const IDM_SHOW_ADMISSION_SHEET = 1011
Global Const IDM_SHOW_DEMOGRAPHICS = 1012
Global Const IDM_PASS_TO = 1013
Global Const MAX_PROFILE_STRING = 256
Global Const PRIVATE_SECTION = "@"
Global Const SEP_CHAR = "#"
Global Const ITEMS_SEPARATION_CHAR = "\t"
Global Const NUM_MAX_ACCESS_WORLD = 16
Global Const SEP_ITEMS = "\n"
Global Const CONFIG_CB_PRE_CONFIGURATION = 0
Global Const CONFIG_CB_CONFIGURATION = 1
Global Const CONFIG_CB_POST_CONFIGURATION = 2
Global Const ERRCODE_CONFIG_ALL_OK = 0
Global Const ERRCODE_CONFIG_COULD_NOT_ALLOCATE = 1
Global Const ERRCODE_CONFIG_COULD_NOT_LOAD = 2
Global Const ERRCODE_CONFIG_COULD_NOT_READ = 3
Global Const CFGAPI_SEPARATOR = "\n"
Global Const MEMBER_TYPE_NONE = 0
Global Const MEMBER_TYPE_STRING = 1
Global Const MEMBER_TYPE_DATE = 2
Global Const MEMBER_TYPE_DOUBLE = 3
Global Const MEMBER_TYPE_LONG = 4
Global Const MEMBER_TYPE_REFERENCE = 5
Global Const MEMBER_TYPE_MAX = 6
Global Const DBOID_TYPE_LENGTH = 3
Global Const DBOID_COUNTER_LENGTH = 15
Global Const DBOID_HOSPITAL_LENGTH = 3
Global Const DBOID_TOTAL_LENGTH = 21
Global Const DBOID_SIZE = DBOID_TOTAL_LENGTH
Global Const DB_TYPE_DONT_KNOW = 0
Global Const DB_TYPE_ORACLE = 1
Global Const DB_TYPE_SQLSERVER = 2
Global Const DB_TYPE_WATCOM = 3
Global Const DB_TYPE_MAX = 4
Global Const PCSUILOGGETDATA_TYPE_VIRTUAL = 1
Global Const PCSUILOGGETDATA_TYPE_PHYSICAL = 2
Global Const PCSUILOGGETDATA_TYPE_VIRTUAL_COMMON = 4
Global Const PCS_ERROR_MESSAGE_TYPE_ERROR = 1
Global Const PCS_ERROR_MESSAGE_TYPE_WARNING = 2
Global Const PCS_ERROR_MESSAGE_TYPE_BOX = 3
Global Const PCS_ERROR_MESSAGE_TYPE_MAX = 4
Global Const SMESS_DECL_ESCAPE = 1001
Global Const CSCONTROL_FOCUS_DONT_STOP = 1
Global Const CSCONTROL_IGNORE_BACKGROUND = 2
Global Const CSCONTROL_DEFAULT_BACKGROUND = 4
Global Const CSCONTROL_NO_DRAG_DROP = 8
Global Const MENU_COMMAND_PROPERTIES = 1000
Global Const MENU_COMMAND_WHAT_IS_THIS = 1001
Global Const MESSAGE_CDBDATE_MODIFIED = 1
Global Const MAXIMUM_COLUMN_NUMBER = 32
Global Const SOBJECTL_AUTO_REPORT = 256
Global Const HOOK_TYPE_NONE = 0
Global Const HOOK_TYPE_CALLBACK = 1
Global Const HOOK_TYPE_DLLCALL = 2
Global Const HOOK_MESSAGE_NOTIFY = 0
Global Const HOOK_MESSAGE_QUERY = 1
Global Const HOOK_MESSAGE_FAILED = 2
Global Const HOOK_MESSAGE_POST = 3
Global Const DEMOGS_ACCESS_TYPE_TEXT = 0
Global Const DEMOGS_ACCESS_TYPE_NUMBER = 1
Global Const DEMOGS_ACCESS_TYPE_ENUM = 2
Global Const DEMOGS_ACCESS_TYPE_DATE = 3
Global Const DEMOGS_ACCESS_TYPE_LENGTH = 4
Global Const DEMOGS_ACCESS_TYPE_WEIGHT = 5
Global Const DEMOGS_ACCESS_TYPE_MAXX = 6
Global Const PATIENT_STATUS_PREADMITTED = 1
Global Const PATIENT_STATUS_ADMITTED = 2
Global Const PATIENT_STATUS_DISCHARGED = 3
Global Const PATIENT_STATUS_UNKNOWN = 4
Global Const FIRST_DATE_SOONER = 1
Global Const FIRST_DATE_IDENTICAL = 2
Global Const FIRST_DATE_LATER = 3
Global Const LOCATION_ALLOCATED = 101
Global Const LOCATION_FREE = 102
Global Const QUERY_MAX_LENGTH = 2048
Global Const QUERY_FIELDS_LENGTH = 1024
Global Const QUERY_VALUES_LENGTH = 1024
Global Const PCS_STRING_NULL = Null
Global Const PCS_DATE_NULL = -1
Global Const PCS_DATE_ODBC_TIMESTAMP = 1
Global Const PCS_DATE_ODBC_DATE = 2
Global Const PCS_DATE_SYSTEM = 3
Global Const PCS_DATE_SYSTEM_ONLYDATE = 4
Global Const PCS_DATE_SYSTEM_ONLYTIME = 5
Global Const PCS_APP_CHARTP = 0
Global Const PCS_APP_VCARE = 1
Global Const PCS_APP_CCENTRAL = 2
Global Const PCS_APP_QQUERY = 3
Global Const PCS_APP_INOTES = 4
Global Const PCS_APP_NUMBER = 5
Global Const PCS_APP_STATUS_UNKNOWN = 1
Global Const PCS_APP_STATUS_ALPHA = 2
Global Const PCS_APP_STATUS_BETA = 3
Global Const PCS_APP_STATUS_QAPASSED = 4
Global Const PCS_FORMAT_STRING_MAX_LENGTH = 256
Global Const PCS_DBOID_NOT_A_VALID_DBOID = 1
Global Const PCS_DBOID_NOT_KNOWN_TYPE = 2
Global Const PCS_DBOID_COULD_NOT_QUERY_DB = 3
Global Const PCS_DBOID_NOT_FOUND_IN_DB = 4
Global Const PCS_DBOID_FOUND_IN_DB = 5
Global Const ERRCODE_ALL_OK = 0
Global Const ERRCODE_HENV_ALREADY_ALLOCATED = 1
Global Const ERRCODE_HENV_COULD_NOT_ALLOCATE = 2
Global Const ERRCODE_HENV_TRIED_TO_FREE_BUT_NOT_ALLOC = 3
Global Const ERRCODE_HENV_ERROR_ON_FREEING = 4
Global Const ERRCODE_HENV_INFO_ON_FREEING = 5
Global Const ERRCODE_HENV_INVALID_HANDLE_ON_FREEING = 6
Global Const ERRCODE_CONF_ALREADY_INITIALIZED = 7
Global Const ERRCODE_CONF_TRIED_TO_FREE_WITHOUT_INIT = 8
Global Const ERRCODE_MESS_ERROR_CODE_DOES_NOT_EXIST = 9
Global Const ERRCODE_LOG_COULD_NOT_ALLOCATE_CONNECT = 10
Global Const ERRCODE_LOG_WITH_INFO = 11
Global Const ERRCODE_LOG_NO_MEMORY = 12
Global Const ERRCODE_LOG_COULD_NOT_CONNECT = 13
Global Const ERRCODE_LOG_LOGOFF_WITHOUT_VALID_LOGON = 14
Global Const ERRCODE_LOG_ALREADY_LOGGED = 15
Global Const ERRCODE_LOG_NOT_CURRENTLY_LOGGED = 16
Global Const ERRCODE_LOG_COULD_NOT_READ_USER_PROPS = 17
Global Const ERRCODE_WRONG_PARAMETERS = 18
Global Const ERRCODE_UI_DIALOG_CANCEL = 19
Global Const ERRCODE_SLIST_NO_MEMORY_TO_ALLOC = 20
Global Const ERRCODE_SLIST_LIST_IS_EMPTY = 21
Global Const ERRCODE_SLIST_POSITION_NOT_VALID = 22
Global Const ERRCODE_SLIST_ITEM_NOT_FOUND = 23
Global Const ERRCODE_USERS_COULD_NOT_BE_READ = 24
Global Const ERRCODE_GENERIC_ERROR = 25
Global Const ERRCODE_DEMOGS_PAGE_COULD_NOT_UPDATE = 26
Global Const ERRCODE_DEMOGS_GROUP_ALREADY_EXISTS = 27
Global Const ERRCODE_DEMOGS_ENTRY_ALREADY_EXISTS = 28
Global Const ERRCODE_INVALID_DBOID = 29
Global Const ERRCODE_DB_ACCESS_ERROR = 30
Global Const ERRCODE_INVALID_LOG = 31
Global Const ERRCODE_COULD_NOT_ALLOCATE = 32
Global Const ERRCODE_COULD_NOT_FREE = 33
Global Const ERRCODE_NOT_FOUND = 34
Global Const ERRCODE_TRANSACTIONS_OVERFLOW_DETECTED = 35
Global Const ERRCODE_COULD_NOT_UPDATE_ISOLATIONS = 36
Global Const ERRCODE_NO_DATA = 37
Global Const ERRCODE_COULD_NOT_BIND = 38
Global Const ERRCODE_COULD_NOT_EXECUTE = 39
Global Const ERRCODE_COULD_NOT_ALLOCATE_HDBST = 40
Global Const ERRCODE_COULD_NOT_LOAD_DATA = 41
Global Const ERRCODE_INVALID_PATIENT = 42
Global Const ERRCODE_UNDER_CONSTRUCTION = 43
Global Const ERRCODE_NULL_DATA = 44
Global Const ERRCODE_NOT_A_VALID_DBOBJECT = 45
Global Const ERRCODE_OBJECT_TYPE_WRONG = 46
Global Const ERRCODE_NOT_A_METHOD = 47
Global Const ERRCODE_WRONG_COLUMN_NUMBER = 48
Global Const ERRCODE_COULD_NOT_GET_DATA = 49
Global Const ERRCODE_COULD_NOT_FETCH = 50
Global Const ERRCODE_CALCULATION_IMPOSSIBLE = 51
Global Const ERRCODE_COULD_NOT_DETECT = 52
Global Const ERRCODE_CAN_NOT_CHANGE_THIS_DATA_NOW = 53
Global Const ERRCODE_DATA_NOT_AVAILABLE = 54
Global Const ERRCODE_DBOID_CAN_NOT_READ_HOSPID = 55
Global Const ERRCODE_DBOBJECT_HAS_NO_PROPERTIES_SHEET = 56
Global Const ERRCODE_SETTING_DBOID_OF_WRONG_TYPE = 57
Global Const ERRCODE_NOT_A_VALID_OBJECT = 58
Global Const ERRCODE_LIST_IS_EMPTY = 59
Global Const ERRCODE_LIST_POSITION_NOT_VALID = 60
Global Const ERRCODE_LIST_ITEM_NOT_FOUND = 61
Global Const ERRCODE_HOOK_ACTION_COULD_NOT_LOAD = 62
Global Const ERRCODE_HOOK_COULD_NOT_ATTACH_DLL = 63
Global Const ERRCODE_HOOK_COULD_NOT_FIND_DLL_FUNC = 64
Global Const ERRCODE_HOOK_NO_FORMER_CONFIG_SPECIFIED = 65
Global Const ERRCODE_HOOK_TRUE_NOT_FOUND = 66
Global Const ERRCODE_OBJECT_NO_PROPERTY_PAGES = 67
Global Const ERRCODE_HOOK_AVOIDED = 68
Global Const ERRCODE_NO_HOOKCONTROLLER = 69
Global Const ERRCODE_NO_DBOBJECT_ICON = 70
Global Const ERRCODE_MEMBER_NOT_FOUND = 71
Global Const ERRCODE_MEMBER_BAD_TYPE = 72
Global Const ERRCODE_MEMBER_UNKNOWN_WIDTH = 73
Global Const ERRCODE_MEMBER_UNKNOWN_TITLE = 74
Global Const ERRCODE_MEMBER_UNKNOWN_TILE_ICON = 75
Global Const ERRCODE_USER_NOT_FOUND = 76
Global Const ERRCODE_VIP_NOT_CORRECT = 77
Global Const ERRCODE_BAD_CRC = 78
Global Const ERRCODE_NO_DBOBJECT_DESCRIPTION_FOUND = 79
Global Const ERRCODE_DYNAMIC_CLASS_AINT_REGISTERED = 80
Global Const ERRCODE_DYNAMIC_CLASS_REGISTERED_WRONG = 81
Global Const ERRCODE_DYNAMIC_CLASS_DLL_NOT_FOUND = 82
Global Const ERRCODE_DYNAMIC_CLASS_FUNC_NOT_FOUND = 83
Global Const ERRCODE_DYNAMIC_CLASS_FROM_TYPE_NOT_FOUND = 84
Global Const ERRCODE_ALL_PAT_IDS_CAN_NOT_BE_NULL = 85
Global Const ERRCODE_INVALID_PARAMETERS = 86
Global Const ERRCODE_CONF_COULD_NOT_ALLOCATE = 87
Global Const ERRCODE_CONF_COULD_NOT_LOAD = 88
Global Const ERRCODE_CONF_COULD_NOT_READ = 89
Global Const ERRCODE_CONF_WORLD_DOES_NOT_EXIST = 90
Global Const ERRCODE_DATA_SOURCES_GENERIC_ERROR = 91
Global Const ERRCODE_STAFF_ISDELETED_NOT_CORRECT = 92
Global Const ERRCODE_DBOBJECT_ALREADY_EXISTS = 93
Global Const ERRCODE_DBOBJECT_IS_EMPTY = 94
Global Const ERRCODE_WORKSTATION_NOT_DEFINED = 95
Global Const ERRCODE_DBOID_GEN_INTERNAL_ERROR01 = 96
Global Const ERRCODE_DBOID_GEN_INTERNAL_ERROR02 = 97
Global Const ERRCODE_CONNECTION_LOST = 98
Global Const ERRCODE_RTDATA_INVALID_LOG = 100
Global Const ERRCODE_RTDATA_COULD_NOT_GET_TABLE_DEF = 101
Global Const ERRCODE_RTDATA_INVALID_CLCKLNK_CODE = 102
Global Const ERRCODE_RTDATA_CLCKLNK_CODE_NOT_IN_DB = 103
Global Const ERRCODE_RTDATA_CLCKLNK_VALUE_UNDEFINED = 104
Global Const ERRCODE_RTDATA_CLCKLNK_ERROR_LOAD_VAR = 105
Global Const ERRCODE_RTDATA_STARTED_IS_NULL = 106
Global Const ERRCODE_RTDATA_VALIDATED_NOT_CORRECT = 107
Global Const ERRCODE_RTDATA_AUDITED_NOT_CORRECT = 108
Global Const ERRCODE_RTDATA_PICISDATA_IS_NULL = 109
Global Const ERRCODE_RTDATA_IS_NUMERIC = 110
Global Const ERRCODE_RTDATA_IS_NOT_NUMERIC = 111
Global Const ERRCODE_RTDATA_VALID_STARTED_IS_NULL = 120
Global Const ERRCODE_RTDATA_VALID_RTDATA_IS_NULL = 121
Global Const ERRCODE_RTDATA_VALID_STAFF_IS_NULL = 122
Global Const ERRCODE_RTDATA_VALID_CAN_NOT_BE_UPDATED = 123
Global Const ERRCODE_RTDATA_AUD_CAN_NOT_BE_UPDATED = 140
Global Const MESS_MAXX = 900
Global Const DBOID_HOSPITAL_PICIS_STANDARD = 0
Global Const DBOID_HOSPITAL_NOT_KNOWN = 999
Global Const DBOID_TYPE_TEMPORARY = 0
Global Const DBOID_TYPE_PATIENT = 1
Global Const DBOID_TYPE_PRECAUTION = 2
Global Const DBOID_TYPE_AMBULATORYSTATUS = 3
Global Const DBOID_TYPE_ADMISSION = 4
Global Const DBOID_TYPE_ENVIRONMENT = 5
Global Const DBOID_TYPE_DIAGNOSISTYPE = 6
Global Const DBOID_TYPE_DIAGNOSIS = 7
Global Const DBOID_TYPE_CHIEFCOMPLAINT = 8
Global Const DBOID_TYPE_DISCHARGE = 9
Global Const DBOID_TYPE_ETHNIC = 10
Global Const DBOID_TYPE_SEX = 11
Global Const DBOID_TYPE_MARITAL = 12
Global Const DBOID_TYPE_RELIGION = 13
Global Const DBOID_TYPE_FIGURE = 14
Global Const DBOID_TYPE_ZONE = 15
Global Const DBOID_TYPE_HELPZONE = 16
Global Const DBOID_TYPE_ZONEQUERY = 17
Global Const DBOID_TYPE_STAFF = 18
Global Const DBOID_TYPE_APPLICATION = 19
Global Const DBOID_TYPE_BLOODGROUP = 20
Global Const DBOID_TYPE_GROUP = 21
Global Const DBOID_TYPE_STAFFTYPE = 22
Global Const DBOID_TYPE_ATTTYPE = 23
Global Const DBOID_TYPE_HIREPERIOD = 24
Global Const DBOID_TYPE_ANALYSIS = 25
Global Const DBOID_TYPE_COMPONENT = 26
Global Const DBOID_TYPE_APPLICATIONVERSION = 27
Global Const DBOID_TYPE_ADMISSIONTYPE = 28
Global Const DBOID_TYPE_RANALYSISCOMPONENT = 29
Global Const DBOID_TYPE_RLABVALUE = 30
Global Const DBOID_TYPE_RSTAFFATTENDINGTYPE = 31
Global Const DBOID_TYPE_RSTAFFATTENDINGTYPEENVIRONMENT = 32
Global Const DBOID_TYPE_RGROUPSTAFF = 33
Global Const DBOID_TYPE_RGROUPGROUP = 34
Global Const DBOID_TYPE_RPATIENTPRECAUTION = 35
Global Const DBOID_TYPE_RADMISSIONDIAGNOSIS = 36
Global Const DBOID_TYPE_RADMISSIONCHIEFCOMPLAINT = 37
Global Const DBOID_TYPE_RADMISSIONAMBULATORYSTATUS = 38
Global Const DBOID_TYPE_PRECAUTIONTYPE = 39
Global Const DBOID_TYPE_RENVIRONMENTLOCATION = 40
Global Const DBOID_TYPE_ENVIRONMENTTYPE = 41
Global Const DBOID_TYPE_LOCATION = 42
Global Const DBOID_TYPE_COUNTRY = 43
Global Const DBOID_TYPE_ASATYPE = 44
Global Const DBOID_TYPE_UNITS = 45
Global Const DBOID_TYPE_UNITTYPE = 46
Global Const DBOID_TYPE_DEPARTMENT = 47
Global Const DBOID_TYPE_COST = 48
Global Const DBOID_TYPE_MEMO = 49
Global Const DBOID_TYPE_LABRESULT = 50
Global Const DBOID_TYPE_LABDATASTATUS = 51
Global Const DBOID_TYPE_LABTEST = 52
Global Const DBOID_TYPE_PART = 53
Global Const DBOID_TYPE_RPARTCOMPONENT = 54
Global Const DBOID_TYPE_PICISDATA = 55
Global Const DBOID_TYPE_ELEMENT = 57
Global Const DBOID_TYPE_ELEMENTCOST = 58
Global Const DBOID_TYPE_MEDICHISTORY = 59
Global Const DBOID_TYPE_ENVIRONMENTBYPASS = 60
Global Const DBOID_TYPE_EVENTTYPE = 61
Global Const DBOID_TYPE_EQUIPMENT = 62
Global Const DBOID_TYPE_RTDATA = 63
Global Const DBOID_TYPE_RTDATAAUDITED = 64
Global Const DBOID_TYPE_RTDATAVALIDATED = 65
Global Const DBOID_TYPE_EVENT = 66
Global Const DBOID_TYPE_ELEMENTTYPE = 67
Global Const DBOID_TYPE_RPATIENTSEX = 68
Global Const DBOID_TYPE_RPATIENTMARITAL = 69
Global Const DBOID_TYPE_RPATIENTRELIGION = 70
Global Const DBOID_TYPE_EXTERNALLINK = 71
Global Const DBOID_TYPE_EQUIPMENTTYPE = 72
Global Const DBOID_TYPE_REQUIPMENTPICISDATA = 73
Global Const DBOID_TYPE_REVENTDATA = 74
Global Const DBOID_TYPE_FIGUREAPPSTAFF = 75
Global Const DBOID_TYPE_THEORETIC_ENV = 76
Global Const DBOID_TYPE_THEORETIC_ENV_STAFF_LOC = 77
Global Const DBOID_TYPE_MEDICAL_PROCEDURE = 78
Global Const DBOID_TYPE_ENVIRON_MEDICAL_PROCEDURE = 79
Global Const DBOID_TYPE_SHIFT_TIMES = 80
Global Const DBOID_TYPE_LABRESULTAUDITED = 81
Global Const DBOID_TYPE_QAAPP = 100
Global Const DBOID_TYPE_QAAPPVER = 101
Global Const DBOID_TYPE_QATYPE = 102
Global Const DBOID_TYPE_QASTATUS = 103
Global Const DBOID_TYPE_QAUSER = 104
Global Const DBOID_TYPE_QASEVERITY = 105
Global Const DBOID_TYPE_QAAREA = 106
Global Const DBOID_TYPE_QAEMERGENCY = 107
Global Const DBOID_TYPE_QAORIGIN = 108
Global Const DBOID_TYPE_QAPLACEFOUND = 109
Global Const DBOID_TYPE_QARUSERSTATUSBUG = 110
Global Const DBOID_TYPE_QABUG = 111
Global Const DBOID_TYPE_QARBUGENVIRONMENT = 112
Global Const DBOID_TYPE_QAENVIRONMENT = 113
Global Const DBOID_TYPE_TREATMENT = 150
Global Const DBOID_TYPE_FORM = 151
Global Const DBOID_TYPE_RTREATMENTFORMTYPE = 152
Global Const DBOID_TYPE_ROUTETYPE = 153
Global Const DBOID_TYPE_RTREATMENTROUTETYPE = 154
Global Const DBOID_TYPE_ROUTE = 155
Global Const DBOID_TYPE_FAMILY = 156
Global Const DBOID_TYPE_CATEGORY = 157
Global Const DBOID_TYPE_CATEGORYTYPE = 158
Global Const DBOID_TYPE_SCOREGROUP = 159
Global Const DBOID_TYPE_SCOREITEM = 160
Global Const DBOID_TYPE_ASSESSMENTITEM = 161
Global Const DBOID_TYPE_SCHEDULE = 162
Global Const DBOID_TYPE_STDORDER = 163
Global Const DBOID_TYPE_STDORDERDATA = 164
Global Const DBOID_TYPE_ORDERSET = 165
Global Const DBOID_TYPE_RSTDORDERORDERSET = 166
Global Const DBOID_TYPE_ORDER = 167
Global Const DBOID_TYPE_ORDERDATA = 168
Global Const DBOID_TYPE_ORDERTYPE = 169
Global Const DBOID_TYPE_ORDERTASKSTATUS = 170
Global Const DBOID_TYPE_TASK = 171
Global Const DBOID_TYPE_TASKDATA = 172
Global Const DBOID_TYPE_STDADDITIVE = 173
Global Const DBOID_TYPE_ADDITIVE = 174
Global Const DBOID_TYPE_TREATMENTDATA = 175
Global Const DBOID_TYPE_RTREATMENTUNITTYPE = 176
Global Const DBOID_TYPE_RSTDORDERADDITIVE = 177
Global Const DBOID_TYPE_STDCONDITION = 178
Global Const DBOID_TYPE_RTASKASSESSMENT = 179
Global Const DBOID_TYPE_RTASKSCORE = 180
Global Const DBOID_TYPE_RORDERADDITIVE = 181
Global Const DBOID_TYPE_RTASKADDITIVE = 182
Global Const DBOID_TYPE_FORMTYPE = 183
Global Const DBOID_TYPE_FAMILYBEHAVIOR = 184
Global Const DBOID_TYPE_BBURNEDAREA = 200
Global Const DBOID_TYPE_BTREATMENTS = 201
Global Const DBOID_TYPE_BFAMILIES = 202
Global Const DBOID_TYPE_BBANDAGES = 203
Global Const DBOID_TYPE_BSTATUS = 204
Global Const DBOID_TYPE_BBACTERIOLOGY = 205
Global Const DBOID_TYPE_BSAMPLETYPE = 206
Global Const DBOID_TYPE_BUNITS = 207
Global Const DBOID_TYPE_BGERMS = 208
Global Const DBOID_TYPE_BINFECTIONTYPE = 209
Global Const DBOID_TYPE_BBODYZONES = 210
Global Const DBOID_TYPE_BHOSPITALLOCS = 211
Global Const DBOID_TYPE_BRTREATMENTBACTERIA = 212
Global Const DBOID_TYPE_BCALCULATIONPERCENTAGES = 213
Global Const DBOID_TYPE_BIDEALWEIGHT = 214
Global Const DBOID_TYPE_BEXCISIONS = 215
Global Const DBOID_TYPE_BGRAFTS = 216
Global Const DBOID_TYPE_BSKINTYPES = 217
Global Const DBOID_TYPE_BCATEGORIES = 218
Global Const DBOID_TYPE_BGALLERY = 219
Global Const DBOID_TYPE_BKINES = 220
Global Const DBOID_TYPE_BKINERESULTS = 221
Global Const DBOID_TYPE_BMOVEMENTS = 222
Global Const DBOID_TYPE_BARTICULATIONS = 223
Global Const DBOID_ADMISSIONSUMMARY = 224
Global Const DBOID_DISCHARGESUMMARY = 225
Global Const DBOID_BPSYCHIATRIC = 226
Global Const DBOID_BCATHETER = 227
Global Const DBOID_BCATHETERTYPE = 228
Global Const DBOID_BCATHETERLOCATION = 229
Global Const DBOID_TYPE_EPIDURALANESTHESIA = 230
Global Const DBOID_TYPE_NERVEBLOCKANESTHESIA = 231
Global Const DBOID_TYPE_SPINALANESTHESIA = 232
Global Const DBOID_TYPE_MAX = 256
Global Const PCS_DBOID_APPLICATION_ALL = "019000000000000000000"
Global Const PCS_DBOID_APPLICATION_CHARTP = "019000000000001000000"
Global Const PCS_DBOID_APPLICATION_VISUALCARE = "019000000000002000000"
Global Const PCS_DBOID_APPLICATION_HOSPIAUDIT = "019000000000003000000"
Global Const PCS_DBOID_APPLICATION_CARECENTRAL = "019000000000005000000"
Global Const PCS_DBOID_APPLICATION_CP2DB = "019000000000007000000"
Global Const PCS_DBOID_APPLICATION_TOUCHUP = "019000000000008000000"
Global Const PCS_DBOID_PRECAUTION_UNKNOWN = "002000000000000000000"
Global Const PCS_DBOID_AMBULATORY_STATUS_UNKNOWN = "003000000000000000000"
Global Const PCS_DBOID_DIAGNOSIS_TYPE_UNKNOWN = "006000000000000000000"
Global Const PCS_DBOID_DIAGNOSIS_UNKNOWN = "007000000000000000000"
Global Const PCS_DBOID_CHIEF_COMPLAINT_UNKNOWN = "008000000000000000000"
Global Const PCS_DBOID_DISCHARGE_UNKNOWN = "009000000000000000000"
Global Const PCS_DBOID_ETHNIC_UNKNOWN = "010000000000000000000"
Global Const PCS_DBOID_SEX_UNKNOWN = "011000000000000000000"
Global Const PCS_DBOID_MARITAL_UNKNOWN = "012000000000000000000"
Global Const PCS_DBOID_RELIGION_UNKNOWN = "013000000000000000000"
Global Const PCS_DBOID_BLOOD_GROUP_UNKNOWN = "020000000000000000000"
Global Const PCS_DBOID_ADMISSION_TYPE_UNKNOWN = "028000000000000000000"
Global Const PCS_DBOID_ENVIRONMENT_TYPE_UNKNOWN = "041000000000000000000"
Global Const PCS_DBOID_COUNTRY_UNKNOWN = "043000000000000000000"
Global Const PCS_DBOID_ASA_TYPE_UNKNOWN = "044000000000000000000"
Global Const PCS_DBOID_MEDICAL_PROCEDURE_UNKNOWN = "078000000000000000000"
Global Const PCS_DBOID_UNITTYPE_TEMPERATURE = "046000000000001000000"
Global Const PCS_DBOID_UNITTYPE_LENGTH = "046000000000002000000"
Global Const PCS_DBOID_UNITTYPE_NON_HUMAN_MASS = "046000000000003000000"
Global Const PCS_DBOID_UNITTYPE_VOLUME = "046000000000004000000"
Global Const PCS_DBOID_UNITTYPE_TIME = "046000000000005000000"
Global Const PCS_DBOID_UNITTYPE_MASS_MASS = "046000000000006000000"
Global Const PCS_DBOID_UNITTYPE_VOLUME_MASS = "046000000000007000000"
Global Const PCS_DBOID_UNITTYPE_MASS_TIME = "046000000000008000000"
Global Const PCS_DBOID_UNITTYPE_VOLUME_TIME = "046000000000009000000"
Global Const PCS_DBOID_UNITTYPE_MASS_MASS_TIME = "046000000000010000000"
Global Const PCS_DBOID_UNITTYPE_VOLUME_MASS_TIME = "046000000000011000000"
Global Const PCS_DBOID_UNITTYPE_MASS_VOLUME = "046000000000012000000"
Global Const PCS_DBOID_UNITTYPE_HUMAN_MASS = "046000000000013000000"
Global Const PCS_DBOID_EVENTTYPE_AUDIT = "061000000000005000000"
Global Const PCS_DBOID_EVENT_LOGON = "066000000000014000000"
Global Const PCS_DBOID_EVENT_LOGOFF = "066000000000015000000"

Global Const PCS_DBOID_ROUTETYPE_INTRAVENOUS = "153000000000001000000"
Global Const PCS_DBOID_ATTENDING_TYPE_DEFAULT = "023000000000000000000"
Global Const PCS_DBOID_STAFF_TYPE_DEFAULT = "022000000000000000000"
Global Const PCS_DBOID_GROUP_DEFAULT = "021000000000000000000"
Global Const PCS_DBOID_APPLICATION_DEFAULT = "019000000000006000000"
