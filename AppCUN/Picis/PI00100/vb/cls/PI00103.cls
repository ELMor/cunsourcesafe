VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCambioCama"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Public Property Set objClsCW(new_objCW As Object)
    Set objcw = new_objCW
    Set objApp = objcw.objApp
    Set objPipe = objcw.objPipe
    Set objGen = objcw.objGen
    Set objEnv = objcw.objEnv
    Set objError = objcw.objError
    Set objSecurity = objcw.objSecurity
    Set objMRes = CreateObject("CodeWizard.clsCWMRES")
End Property
Public Property Get strUser() As String
    strUser = strCodUser
End Property
Public Property Let strUser(new_strUser As String)
    strCodUser = new_strUser
End Property



Public Function NuevasCamas()
Dim strsql As String
Dim rs As rdoResultset
Dim qy As rdoQuery
Dim codCama As String
Dim dDepartamento As String
Dim codDepartam As String


If Not LOG Then
    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
    Exit Function
End If

strsql = "SELECT AD1500.AD15CODCAMA, AD0200.AD02DESDPTO, AD0200.AD02CODDPTO" & _
" FROM AD1500, AD0200 WHERE AD1500.AD02CODDPTO=AD0200.AD02CODDPTO" & _
" ORDER BY AD1500.AD15CODCAMA"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
Set rs = qy.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    While rs.EOF = False
            codCama = rs!AD15CODCAMA
            dDepartamento = rs!AD02DESDPTO
            codDepartam = rs!AD02CODDPTO
'''''
        If Not ExisteCama(codCama) Then   'Si existe la cama no se introduce
            If Not InsertarCama(codCama, dDepartamento, codDepartam) Then Exit Function
        End If
        
    rs.MoveNext
    Wend
rs.Close
qy.Close

ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)
End Function

Public Sub CambioCama(codAsistencia As Long, CamaInicio As String, CamaFin As String)

Dim LocationIdIni As String
Dim LocationIdFin As String
Dim LocationId As Long
Dim blnError As Boolean
Dim PatientDboid As String * 21
Dim numHistoria As Long
Dim AdmisionDboid As String * 21

If Not LOGGeneral Then
    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
    Exit Sub
End If

    LocationIdIni = ObtenerPicisLocationID(CamaInicio)
'    LocationIdFin = ObtenerPicisLocationID(CamaFin)
    
    If ExisteAdmision(codAsistencia) Then
         LocationId = LocationIdIni
            blnError = CambiarStationID(LocationId)
            If blnError Then
                MsgBox "Ha fallado el cambio de localización", vbCritical
                ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
                ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
                Exit Sub
            End If
         If Not AdmisionTrasladada(codAsistencia) Then 'El caso de que estuviera transaldado
            If Not TransferirCama(codAsistencia, PatientDboid) Then Exit Sub
         End If
''         LocationId = LocationIdFin
''            blnError = CambiarStationID(LocationId)
''            If blnError Then
''                MsgBox "Ha fallado el cambio de localización", vbCritical
''                ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
''                ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
''                Exit Sub
''            End If
''
''         If Not AdmitirEnNuevaCama(codAsistencia, PatientDboid, LocationId) Then Exit Sub          'Admitirlo
    End If
        
 
 ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
 ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
    
End Sub

Public Function DejarCamaTrans(codAsistencia As Long) As Boolean
Dim condition As String
Dim Asistencia As String
Dim cama As String
Dim Systime As Long
Dim codPersona As Long
Dim numHistoria As Long
Dim Historia As String
Dim sNull As String
Dim PatientDboid As String * 21

If Not LOGGeneral Then
    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
    Exit Function
End If

Call BuscarPacienteDesdeAsistencia(codAsistencia, numHistoria)
ierrcode = PcsListObjectsAlloc(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsPatientAlloc(hPatient)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
Historia = CStr(numHistoria)
'Build the condition string
condition = "$='" + Historia + "'"
'The only condition is the Patient name, so the other fields are empty strings
ierrcode = PcsPatientFindWithMembers(hLog, hList, condition, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsListObjectsGotoHeadPosition(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hPatient)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsDbObjectGetIdentifier(hPatient, PatientDboid, 21)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsAdtObjectAlloc(hTransfer, hLog)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
Systime = 0
ierrcode = PcsAdtObjectTransfer(hTransfer, hPatient, hLog, ByVal Systime)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsListObjectsFree(hList)
ierrcode = PcsPatientFree(hPatient)
ierrcode = PcsAdtObjectFree(hTransfer)
ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)

End Function

