VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inmulite"
   ClientHeight    =   1005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2115
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1005
   ScaleWidth      =   2115
   Begin VB.Timer TimerAUTO 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   720
      Top             =   480
   End
   Begin VB.Timer TimerTransMENSAJE 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   360
      Top             =   480
   End
   Begin VB.Timer TimerEsperaREPLY 
      Enabled         =   0   'False
      Interval        =   15000
      Left            =   0
      Top             =   480
   End
   Begin VsOcxLib.VideoSoftAwk Awk3 
      Left            =   960
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   480
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Enum Operation
  cteReset = 2
  cteSuma = 1
End Enum

Const ConstRegH = "H|\^&|||||||8N1|||P|1"
Const ConstRegP = "P|1|1|||^||||||||"
Const ConstRegO = "O|1|"
Const ConstRegL = "L|1|N"

Dim MiObjMuestra As clsMuestra
Dim MiObjActuacion As clsActuacion
Dim strlog As String
Public pos As String         'la pos inicial del mask
Dim contintentoscomunic As Integer 'cuenta el n� de veces que se intenta comunicar con el autoanalizador
Dim contintentostrans As Integer 'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
Dim TipoFrame As Integer
Dim UltimoEnvio As Boolean        'True: si no hay m�s env�os de prueba a programar
                                    'False: todav�a quedan pruebas a programar

Private Sub Envio_Mensajes()
'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es env�ar el
'caracter <ENQ> y esperar a que el autoanalizador responda afirmativamente. Luego se manda
'el mensaje propiamente dicho hasta que sea recibido correctamente por el autoanalizador.
'Finalmente se cierra la comunicaci�n enviando <EOT> para dar paso a la respuesta del
'autoanalizador al requerimiento del sistema.
Dim sql As String
Dim T As Long
Dim RES As Integer

On Error GoTo AutoanError

With objAutoAn
Select Case .Estado
  Case cteESPERA
    conintentoscomunic = contintentoscomunic + 1
    If contintentoscomunic = 7 Then 'Fallo en la comunicacion despues de 7 intentos
      contintentoscomunic = 1
      sql = "No se puede establecer la comunicaci�n"
      Select Case MsgBox(sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
        Case vbRetry
          T = Timer + 5
          Do While T > Timer: RES = DoEvents(): Loop
          Sistema_Espera
          Exit Sub
        Case vbCancel
          Descargar
      End Select
    Else
      Sistema_Espera
      .PanelEstado .fLeerEstado(.Estado) & " " & contintentoscomunic
      Exit Sub
    End If
  Case cteMASTER
    contintentostrans = contintentostrans + 1
    If contintentostrans = 7 Then 'Fallo en la transmision despu�s de 7 intentos
      conintentostrans = 0
      .Estado = cteESPERA
      sql = "No se puede transmitir la informaci�n."
      Select Case MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
        Case vbRetry
          'Se reintenta mandar todo el registro actual
          T = Timer + 5
          Do While T > Timer: RES = DoEvents(): Loop
          Sistema_Espera
          contintentoscomunic = 1
          Exit Sub
        Case vbAbort
          'Se sale del programa y se queda la prueba sin programar
          MiObjActuacion.strEstado = "SP"
          Descargar
        Case vbIgnore
          'Se queda la prueba como no aceptada y se sigue la transmision con el 1er TipoFrame
          'del siguiente registro en estado "SP"
          MiObjActuacion.strEstado = "NA"
          T = Timer + 5
          Do While T > Timer: RES = DoEvents(): Loop
          Sistema_Espera
          Exit Sub
      End Select
    Else
      Sistema_Master
      Exit Sub
    End If
    
  End Select
End With
Exit Sub

AutoanError:
  Call objError.InternalError(Me, "Envio_Mensajes", "Envio_mMensajes")
End Sub

Private Sub Sistema_Espera()
On Error GoTo AutoanError
'Se envia el caracter de solicitud de comunicaion <ENQ>y se pasa al estado
'de solicitud
objAutoAn.Enviar Chr$(5)
'Se inicializa el contador del tipoFrame a transmitir y el n� linea secuencial
TipoFrame = 1
Linea cteReset
objAutoAn.Estado = cteSOLICITUD
TimerEsperaREPLY.Enabled = True 'Empieza a contar el tiempo para el timeout
If objAutoAn.blnDesign Then objAutoAn.AutoRecepcion Chr$(6)
Exit Sub

AutoanError:
  Call objError.InternalError(Me, "Sistema_Espera", "Sistema_Espera")
End Sub

Private Sub Sistema_Master()
'Se transmiten datos al autoanalizador
Dim Mensaje As String 'Contiene un string correspondiente al mensaje a enviar
On Error GoTo AutoanError
  'Se construye el mensaje para enviar
   Records Mensaje
   'y se envia el mensaje
   objAutoAn.Enviar Mensaje
   TimerEsperaREPLY.Enabled = True 'Empieza a contar el tiempo para el tTimeOut.
   If objAutoAn.blnDesign Then objAutoAn.AutoRecepcion Chr$(6)
  Exit Sub

AutoanError:
  Call objError.InternalError(Me, "Sistema_Master", "Sistema_Master")
End Sub

Private Sub Establecer_CheckFin(Mensaje As String)
'Subrutina que establece el check del mensaje y ademas los caracteres del final del TipoFrame
'<CR><LF> que ser�n enviados al autoanalizador.
Dim contador As Integer     'cuenta el n�mero de caracteres le�dos
Dim suma As Integer         'suma de los c�digos ASCII
On Error GoTo AutoanError

  suma = 0
  For contador = 1 To Len(Mensaje)
    suma = suma + Asc(Mid$(Mensaje, contador, 1))
  Next contador
  
  If Len(Hex$(suma)) = 1 Then
    Mensaje = Mensaje & "0" & Hex$(suma)
  Else
    Mensaje = Mensaje & Right$(Hex$(suma), 2)
  End If
  Mensaje = Mensaje + Chr$(13) + Chr$(10)
  Exit Sub
    
AutoanError:
    Call objError.InternalError(Me, "Establecer_CheckFin", "Establecer_CheckFin")
End Sub


Private Sub Records(Mensaje As String)
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
'En esta subrutina se define el tipo de mensaje que toca enviar seg�n la
'variable TipoFrame
On Error GoTo AutoanError
Select Case TipoFrame
  Case 1  'Mensaje de cabecera
  Mensaje = Trim$(Str$(Linea)) + ConstRegH + Chr$(13) + Chr$(3)
  Establecer_CheckFin Mensaje
  Mensaje = Chr$(2) + Mensaje
  Case 2 'Mensaje paciente
    Mensaje = Trim$(Str$(Linea)) + ConstRegP + Chr$(13) + Chr$(3)
    Establecer_CheckFin Mensaje
    Mensaje = Chr$(2) + Mensaje
  Case 3  'Mensaje prueba
     RecordOrder Mensaje
  Case 4  'Mensaje final
      Mensaje = Trim$(Str$(Linea)) + ConstRegL + Chr$(13) + Chr$(3)
      Establecer_CheckFin Mensaje
      Mensaje = Chr$(2) + Mensaje
   End Select
  
  Exit Sub
 
AutoanError:
  Call objError.InternalError(Me, "Records", "Records")
End Sub
Private Sub RecordOrder(Mensaje As String)
'Se forma el TipoFrame que corresponde al de programacion de la muestra y prueba
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim cMuestra As String     'c�digo de la muestra
Dim cPrueba As String       'c�digo de la prueba
On Error GoTo AutoanError

Mensaje = ""
For Each objMuestra In objAutoAn.ColMuestras
  For Each objActuacion In objMuestra.ColMuestrasPruebas
    If objActuacion.strEstado = cteSINPROGRAMAR Then
      objActuacion.strEstado = cteINTENTOPROGRAMAR
        Set MiObjActuacion = objActuacion
        GoTo envio
    End If
  Next
Next

envio: cMuestra = objMuestra.strCodMuestra
      cPrueba = objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strCodActAuto
      Mensaje = Trim$(Str$(Linea)) & ConstRegO & cMuestra & "||^^^" & _
      cPrueba & Chr$(13) & Chr$(3)
      Establecer_CheckFin Mensaje
      Mensaje = Chr$(2) + Mensaje


UltimoEnvio = True
  For Each objMuestra In objAutoAn.ColMuestras
    For Each objActuacion In objMuestra.ColMuestrasPruebas
      If objActuacion.strEstado = cteSINPROGRAMAR _
      Then UltimoEnvio = False: Exit For
    Next
    If Not UltimoEnvio Then Exit For
  Next
Exit Sub

AutoanError:
  Call objError.InternalError(Me, "RecordOrder", "RecordOrder")
End Sub
Private Function Linea(Optional Sumador As Operation) As Integer
Static MiLinea As Integer
  Select Case Sumador
    Case cteReset: MiLinea = 0
    Case Else: MiLinea = IIf(MiLinea < 7, MiLinea + 1, 0) 'suma
  End Select
  Linea = MiLinea
End Function

Public Sub Lectura_Datos(datos As String)
'Se leen los datos que envia el autoanalizador. Solo se tendr�n en cuenta los mensajes _
Query y de Resultado
Static codMuestra$, codActuacion$
Dim codResultadoAuto$, Resultado$
Dim sql$
Dim Qry As rdoQuery
Dim rs1 As rdoResultset

On Error GoTo AutoanError

'Se separan los distintos tipos de registro del mensaje
strlog = strlog & "AWK1=DATOS=" & datos & Chr$(13) & Chr$(10)
Awk1.FS = "|": Awk1 = datos
'El awk1.F(1) contiene la cabecera (registro tipo H)
'(registro tipo L) que no sirven para nada.
'Q (query)
'Busco que tipo de frame me ha enviado el autoanalizador
'Si es tipo O tengo que sacar el codigo de muestra
Select Case UCase$(Right$(Awk1.F(1), 1))
  Case "O"
    'Se obtiene el id de la muestra
    codMuestra = Trim$(Awk1.F(3))
    'Si el autoanalizador solo env�a los numeros se obtiene adem�s
    'las letras correspondientes a esa muestra.
    'MIENTRAS NO SE PUEDE PROGRAMAR
    If IsNumeric(codMuestra) And Len(codMuestra) <= 4 Then
     sql = "SELECT CMUESTRALETRA FROM CODIGOPRUEBA"
     Set Qry = objAutoAn.rdoConnect.CreateQuery("", sql)
     Set rs1 = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
     If Not rs1.EOF Then
      Select Case Len(codMuestra)
        Case 4
          codMuestra = rs1!CMUESTRALETRA & "-" & codMuestra
        Case 3
          codMuestra = rs1!CMUESTRALETRA & "-0" & codMuestra
        Case 2
          codMuestra = rs1!CMUESTRALETRA & "-00" & codMuestra
        Case 1
          codMuestra = rs1!CMUESTRALETRA & "-000" & codMuestra
      End Select
      End If
    End If
    
    If objAutoAn.blnExistMuestra(codMuestra) Then
      'Se obtiene el codigo de la prueba
      Awk2 = Awk1.F(5): Awk2.FS = Chr$(13)
      Awk3 = Trim$(Awk2.F(1))
      Awk3.FS = "^"
      codActuacion = Trim$(Awk3.F(4))
    End If
    
  Case "R"
    'Se obtien el codigo del resultado
    Awk2 = Awk1.F(3): Awk2.FS = "^"
    codResultadoAuto = Trim$(Awk2.F(4))
    If objAutoAn.blnExistMuestra(codMuestra) And codActuacion = codResultadoAuto Then
      If Awk1.F(4) <> "" Then
        'Cuando los valores son <0 solo envia .76 por ejemplo. Le
        'a�adimos el 0
        Awk1.F(4) = IIf(Left$(Awk1.F(4), 1) = ".", "0" & Awk1.F(4), Awk1.F(4))
        Resultado = IIf(Awk1.F(7) <> "N", Awk1.F(7) & " " & Awk1.F(4), Awk1.F(4))
        Call objAutoAn.pIntroResultConCResAuto(codMuestra, codResultadoAuto, Resultado)
        codMuestra = ""
        codActuacion = ""
      End If
    End If
  End Select
Exit Sub
  
AutoanError:
  Call objError.InternalError(Me, "Lectura_Datos", "Lectura_Datos")
  
End Sub

Private Sub Inmulite_Master(Mensaje As String, caracter As String)
'Tras haber recibido un mensaje el autoanalizador responde confirm�ndolo o rechaz�ndolo
'si ha habido errores.
'Si el mensaje es rechazado habr� que volver a enviarlo
'Si el mensaje es aceptado habr� que enviar el siguiente TipoFrame del mensaje.
' Cada mensaje esta compuesto de 4 frames:
'-Header,-Patient,-Order,-Termination cada uno de ellos precedido por un n� secuencial (0-7)
'(vble linea)
On Error GoTo AutoanError
TimerEsperaREPLY.Enabled = False 'Se desactiva el timeout, pues ya nos ha respondido algo
  'existe la posibilidad de que el autoanalizador conteste con <EOT> en vez de <ACK> para solicitar que
  'acabe la comunicaci�n. En tal caso pasaremos al estado de espera

Select Case caracter
  Case Chr$(4) '<EOT>
    'Se ha recibido respuesta positiva y no habr� que volver a intentarlo de nuevo
    objAutoAn.Estado = cteESPERA
  Case Chr$(6) '<ACK>- El autoanalizador env�a un reconocimiento positivo
    contintentostrans = 0
    'cuando hemos acabado de transmitir todo
    If UltimoEnvio = True And TipoFrame = 4 Then
      objAutoAn.Enviar Chr$(4) '<EOT> - Se cierra la comunicacion para esperar
      'la respuesta del autoanalizador. Es asi porque se ha terminado toda la
      'programaci�n.
      objAutoAn.Estado = cteESPERA
      MiObjActuacion.strEstado = cteACEPTADA
      MiObjActuacion.Muestra.strEstado = cteACEPTADA
      Exit Sub
    End If
    Select Case TipoFrame
      Case 1, 2
        TipoFrame = TipoFrame + 1
      Case 3
        If UltimoEnvio = True Then
          TipoFrame = 4
        Else
          MiObjActuacion.strEstado = cteACEPTADA
          TipoFrame = 2
        End If
      Case 4
        TipoFrame = 1
    End Select

    Envio_Mensajes  'Se enviar� el siguiente mensaje
    Mensaje = ""
  Case Chr$(21) '<NAK>- El autoanalizador no ha aceptado el ultimo TipoFrame enviado
    TimerTransMENSAJE.Enabled = True 'Se envia el timer para volver a reenviar
    'el ultimo frame enviado.
    objAutoAn.Estado = cteMASTER
    Mensaje = ""
   Case Else
    'parada de comprobacion para ver si se mandan otros caracteres
      sql = "Se ha recibido un caracter." + (caracter)
      MsgBox sql, vbExclamation, frmPrincipal.Caption
  End Select
Exit Sub
 
AutoanError:
 Call objError.InternalError(Me, "Inmulite_Master", "Inmulite_Master")
 
End Sub
Private Sub Inmulite_Esclavo(Mensaje As String, caracter As String)
' Identifica el final de un bloque pasando a su lectura.
'Tambi�n se identificca el final de la comunicaci�n y se vuelve al estado de espera
On Error GoTo AutoanError
TimerAUTO.Enabled = False 'Al llegar datos del autoanalizador se desactiva el timer
Select Case True
  Case caracter = Chr$(4) '<EOT> - Final de la transmisi�n por parte del autoanalizador
    objAutoAn.Estado = cteESPERA
    Mensaje = ""
  Case Right$(Mensaje, 1) = Chr$(10) '<LF>
    'se ha recibido el final del Tipoframe se contesta con <ACK> y se reinicia el tiempo
    'de espera
    objAutoAn.Enviar Chr$(6)
    'se activa el TimerAUTO que corresponde al tiempo de espera asignado para el proximo TipoFrame
    TimerAUTO.Enabled = True
      Lectura_Datos (Mensaje)
      Mensaje = ""

  End Select
  Exit Sub
  
AutoanError:
  Call objError.InternalError(Me, "Inmulite Esclavo", "Inmulite Esclavo")
End Sub

Private Sub Inmulite_Solicitud(Mensaje As String, caracter As String)
'Tras haber realizado el sistema una solicitud para establecer una comunicaci�n,
'el autoanalizador responde aceptando o negando dicha comunicaci�n.On Error GoTo AutoanError
On Error GoTo AutoanError

TimerEsperaREPLY.Enabled = False 'puesto que ya nos ha respondido algo, se desactiva el timeout
Select Case caracter
  Case Chr$(6) '<ACK>- El autoanalizador ha aceptado la comunicaci�n
    'inicializamos el n� de intentos de establecimiento de comunicacion pues ya se ha conseguido
    contintentoscomunic = 0
    objAutoAn.Estado = cteMASTER
    Mensaje = ""
    Envio_Mensajes 'Se envia el bloque de datos por parte del sistema al autoanalizador
  Case Chr$(21) '<NAK>- El autoanalizador no ha aceptado la comunicai�n.
    objAutoAn.Estado = cteESPERA
    Mensaje = ""
    TimerTransMENSAJE.Enabled = True 'Se esperan 10 segundos para intentar establecer una nueva comunicai�n
  Case Chr$(5)  '<ENQ>- El autoanalizador ha "solicitado linea" a la vez que el sistema
  'No se manda nada y se pasa a Espera para esperar a recibir el nuevo intento del Autoanalizador
    objAutoAn.Estado = cteESPERA
    Mensaje = ""
End Select
Exit Sub

AutoanError:
  Call objError.InternalError(Me, "Inmulite_Solicitud", "Inmulite_Solicitud")
End Sub


Private Sub Inmulite_Espera(Mensaje As String, caracter As String)
On Error GoTo AutoanError
'Identifica la llegada de los caracter de solicitud de comunicacion
'y pasa al estado de ESCLAVO

If caracter = Chr$(5) Then '<ENQ>-Solicitud de comunicacion por parte del autoanalizador
  'por si acaso le habiamos mandado un <ENQ> simultaneo desactivamos el timer de espera
  TimerEsperaREPLY.Enabled = False
  'Se controla el tiempo que tarda el autoanaliz. en volver a mandar el siguiente mensaje
  TimerAUTO.Enabled = True
  objAutoAn.Enviar Chr$(6) 'Se acepta la comunicacion. (No se conoce ninguna raz�n para rechazarla!!)
  objAutoAn.Estado = cteESCLAVO
  Mensaje = "" 'Se eliminan los posibles caracteres enviados antes de establecerse la comunicaci�n
End If
Exit Sub

AutoanError:
  Call objError.InternalError(Me, "Inmulite_Espera", "Inmulite_Espera")
  
End Sub
Public Sub Lectura_Protocolo(caracter As String)
Static Mensaje As String  'contiene el mensaje que envia el autoanlizador
Dim CopiaMensaje As String  'Contiene una copia del mensaje que se est� recogiendo y es _
la que se env�a a otras subrutinas pudiendo ser modificada en �stas. Al ser modificada _
la copia, tambi�n deber� modificarse el original. El hecho de utilizar esta copia _
se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas _
por estar declarada como Static en esta subrutina.

Mensaje = Mensaje & caracter
CopiaMensaje = Mensaje

'Se acude a la subrutina correspondiente para la lectura del mensaje seg�n el estado.
Select Case objAutoAn.Estado
  Case cteESPERA
    Inmulite_Espera CopiaMensaje, caracter
    Mensaje = CopiaMensaje
  Case cteSOLICITUD
    Inmulite_Solicitud CopiaMensaje, caracter
    Mensaje = CopiaMensaje
  Case cteESCLAVO
    Inmulite_Esclavo CopiaMensaje, caracter
    Mensaje = CopiaMensaje
  Case cteMASTER
    Inmulite_Master Mensaje, caracter
    Mensaje = CopiaMensaje
End Select
 
End Sub


Public Sub Descargar()
On Error GoTo AutoanError

  'Fin de programa
  With objAutoAn
    .EscribirLog Format(.fFechaHoraActual, "hh:mm:ss") & " Fin de Ejecuci�n"
  End With
  Set objAutoAn = Nothing
  End
  Exit Sub

AutoanError:
  Call objError.InternalError(Me, "Descargar", "Descargar")
End Sub

Public Sub Programar()
On Error GoTo AutoanError
'Para que no empieze a pedir comunicacion sin que haya nada que programar
If objAutoAn.blnExistMuestraConEstado(cteSINPROGRAMAR) Then
  Envio_Mensajes
Else
   MsgBox "�Todas las pruebas est�n ya programadas!" & vbCr & _
    "Para volver a programarlas paselas a estado 'Sin Programar'", vbExclamation, frmPrincipal.Caption
End If
Exit Sub


AutoanError:
  Call objError.InternalError(Me, "Programar", "Programar")
End Sub

