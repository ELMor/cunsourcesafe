VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Vitalab Viva"
   ClientHeight    =   465
   ClientLeft      =   3255
   ClientTop       =   1425
   ClientWidth     =   1095
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Vitalab.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   465
   ScaleWidth      =   1095
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Sub Lectura_Datos(datos As String)
Dim i%, cMuestra$, Resultado$, CodResultadoAuto$
  '<STX>{
  'R;Device_id(4);
  'Type(1);
  'Sample_nr(12);Sample_Name(20);date of birth(11);sex(1);
  'nr_of_test(2);
  'test_name(4);result(7);flags(22);units(6);
  'test_name(4);result(7);flags(22);units(6);
  '.....
  '}
'  objAutoAn.EscribirLog "datos:" & datos
  Awk1 = datos: Awk1.FS = ";"
  'Comprobacion de que es mensaje de resultados
  If Mid$(Awk1.F(1), 3, 1) = "R" Then
    With objAutoAn
      cMuestra = Trim$(Awk1.F(4))
      If IsNumeric(cMuestra) Then cMuestra = .fNumeroACodigo(cMuestra)
      If .blnExistMuestra(cMuestra) Then
        For i = 9 To Awk1.NF - 3 Step 4
          CodResultadoAuto = Trim$(Awk1.F(i))
          If CodResultadoAuto <> "MPA" Then 'TOMAMOS EL VALOR Alfanumerico
            Resultado = Trim$(Awk1.F(i + 2))
            Resultado = UCase$(Left$(Resultado, 1)) + LCase$(Mid$(Resultado, 2))
          Else 'TOMAMOS EL VALOR Numerico
            Resultado = Trim$(Awk1.F(i + 1))
          End If
          Call .pIntroResultConCResAuto(cMuestra, CodResultadoAuto, Resultado, True)
        Next i
      End If
    End With
  End If
End Sub

Public Sub Lectura_Protocolo(caracter As String)
Static mensaje$
Dim CopiaMensaje$

  mensaje = mensaje & caracter: CopiaMensaje = mensaje
 
  Select Case objAutoAn.Estado
    Case cteESPERA: Viva_Espera CopiaMensaje: mensaje = CopiaMensaje
    Case cteTRANSMISION: Viva_Transmision CopiaMensaje: mensaje = CopiaMensaje
  End Select
  
End Sub

Sub Viva_Espera(mensaje As String)
'Identifica la llegada del caracter de comienzo de transmisión
'(comienzo del mensaje) y pasa al estado de transmisión.
  If Right$(mensaje, 1) = Chr$(2) Then '<STX>
    objAutoAn.Estado = cteTRANSMISION
    mensaje = mensaje
  End If
End Sub

Sub Viva_Transmision(mensaje As String)
'Identifica el final del mensaje y procede a su lectura
  If Right$(mensaje, 2) = vbCrLf Then '<ETX>
    Lectura_Datos mensaje
    objAutoAn.Estado = cteESPERA
    mensaje = ""
  End If
End Sub

Public Sub probar()
Static nvez%
  nvez = nvez + 1
  Select Case nvez
     Case 1: objAutoAn.AutoRecepcion Chr$(2) & "{R;0611;N;0007993557  ;                    ;           ;M; 5;OPI1;0.185  ;                       negat. ;      ;COC1;0.355  ;                       negat. ;      ;cAMP;0.184  ;                       negat. ;      ;BAR1;0.201  ;                       negat. ;      ;CAN1;0.957  ;                       POSIT. ;      ;}" & vbCrLf
     
'    Case 1: objAutoAn.AutoRecepcion Chr$(2) & "{R;0611;N;ADN-7457    ;                    ;           ;M; 3;OPI1;0.338  ;                       POSIT. ;      ;COC1;0.469  ;                       negat. ;      ;cAMP;0.375  ;                       POSIT. ;      ;}" & Chr$(3)
'    Case 2: objAutoAn.AutoRecepcion Chr$(2) & "{R;0611;N;ADN-4745    ;                    ;           ;M; 1;MPA ;1.9    ;                                  ;ug/ml ;}" & Chr$(3)
'        objAutoAn.AutoRecepcion Chr$(3)  '<ETX>
'    Case 1: objAutoAn.AutoRecepcion Chr$(2) & "D1U110399000302000044000007755031A1132    01001000000000010000067300320200922029320916002880031400157002062008800677000280000100013900005900045500001900000100150004980015600121004230*00010000000000000000000000000000000000000000000000000000000" & Chr$(3)
'    Case Else: objAutoAn.AutoRecepcion Chr$(2) & "D1U110399000302000044000007755031A1132    01001000000000010000067300320200922029320916002880031400157002062008800677000280000100013900005900045500001900000100150004980015600121004230*00010000000000000000000000000000000000000000000000000000000" & Chr$(3)
  End Select
End Sub

Public Sub Descargar()
  Unload Me
  End
End Sub

