Attribute VB_Name = "modReference"
Option Explicit
Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    pCargarColumnasGrid
    .pCargaColEstados (Array(.VEst(cteSINPROGRAMAR), .VEst(cteACEPTADA), .VEst(cteEXTRAIDA)))
    If Not .fInicializar(App, cteREFERENCE, True, , , True, , True, , , True, True, , , , , True) Then End
'    pCargarNombres
    .Borrar_Logs
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarColumnasGrid()
  With objAutoAn
    'columnas del grid
'    .AgregarColumnaAGridMuestras cteMuestraPROPERTY1, "Paciente", 2700, , , Izquierda
'    .AgregarColumnaAGridMuestras cteMuestraPROPERTY2, "Sec.", 700
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraPROPERTY3, "Vol.", , , Editable
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION, , , Visible
    .AgregarColumnaAGridMuestras cteMuestraDILUCION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
'    .AgregarColumnaAGridPruebas ctepruebaNUMACTUACION
    .AgregarColumnaAGridPruebas ctePruebaNUMREPETICION
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas ctePruebaESTADO
    .AgregarColumnaAGridPruebas ctePruebaCODACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoESTADO, , , InVisible
  End With
End Sub

Private Sub pCargarNombresBak()
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql$
On Error GoTo AutoAnError
  
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      sql = "select ci22priapel||' '||ci22segapel||', '||ci22nombre"
      sql = sql & " from ci2200, pr0400 where"
      sql = sql & " ci2200.ci21codpersona = pr0400.ci21codpersona"
      sql = sql & " and pr0400.pr04numactplan = ?"
      Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
      qry(0) = CLng(.ColMuestrasPruebas(1).strNumAct)
      Debug.Print CLng(.ColMuestrasPruebas(1).strNumAct)
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not rs.EOF Then
        .strProperty1 = Trim$(Left$(rs(0), 30))
      Else
        .strProperty1 = "Nombre Paciente No Encontrado"
      End If
    End With
  Next
  Exit Sub
    
AutoAnError:
'    Call objError.InternalError(, "Cargando Nombres de Pacientes", "pCargarNombres")
End Sub

Private Sub pCargarNombres()
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql$
On Error GoTo AutoAnError
  
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      sql = "select ap1||' '||ap2||', '||nom"
      sql = sql & " from pac where"
      sql = sql & " nh = ?"
      Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
      qry(0) = .lngHistoria
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not rs.EOF Then
        .strProperty1 = Trim$(Left$(rs(0), 30))
      Else
        .strProperty1 = "Nombre Paciente No Encontrado"
      End If
    End With
  Next
  Exit Sub
    
AutoAnError:
'    Call objError.InternalError(, "Cargando Nombres de Pacientes", "pCargarNombres")
End Sub


