Attribute VB_Name = "Pentra60mod"
Option Explicit

Public objAutoAn As New clsAutoAnalizador
Public objError As New clsErrores

Public Sub Main()

Screen.MousePointer = vbHourglass
With objAutoAn
  Set .frmFormulario = frmPrincipal
  fCargarColumnasGrid
  If Not .fInicializar(App, ctePENTRA60, , , , , , , , , , , , , , True) Then End
  .Show
  .Borrar_Logs
End With
Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
'Cargo las columnas correspondientes
With objAutoAn
  .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
  .AgregarColumnaAGridMuestras cteMuestraURGENTE
  .AgregarColumnaAGridMuestras cteMuestraPROPERTY1, "Prot.Tot.", 1350
  .AgregarColumnaAGridMuestras cteMuestraESTADO
  .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
  
  .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
  .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
  .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO, , 1000
  .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
  .AgregarColumnaAGridPruebas ctePruebaURGENTE
  .AgregarColumnaAGridPruebas cteResultadoESTADO
  .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
End With
End Sub


