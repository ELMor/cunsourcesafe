VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Caption         =   "Form1"
   ClientHeight    =   480
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   1560
   Icon            =   "frmPrincipal.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   480
   ScaleWidth      =   1560
   StartUpPosition =   3  'Windows Default
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   480
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Lectura_Datos(datos As String)
Dim strCodMuestra As String
Dim contador As Integer

On Error GoTo AutoAnError

'Se leen los tramos del mensaje
Awk1.FS = Chr$(13) '<CR>
Awk1 = datos

'El c�digo de muestra est� en el awk1.F(X) cuya primer caracter es una 'v' min�scula.
For contador = 1 To Awk1.NF
  If Left$(Awk1.F(contador), 1) = "v" Then
    strCodMuestra = objAutoAn.fNumeroACodigo(Trim$(Mid$(Awk1.F(contador), 2)))
    If Not objAutoAn.blnExistMuestra(strCodMuestra) Then _
    Exit Sub Else Exit For
  End If
Next contador
'Los datos de las pruebas est�n entre el awk1.f(3) y el awk1.f(28)
For contador = 3 To 28
  Call objAutoAn.pIntroResultConCResAuto(strCodMuestra, _
  Left$(Awk1.F(contador), 1), Mid$(Awk1.F(contador), 3, 5), True)
Next contador
Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Lectura_Datos", "Lectura_Datos")

End Sub
Private Sub Pentra60_Transmision(mensaje As String)
'Identifica el final de la trnsmision. Se procede a la lectura de
'datos ya que no hay que enviar ninguna respuesta. Si un mensaje ha
'llegado incorrectamente se perdera sin la posibilidad de que sea
'retransmitido
On Error GoTo AutoAnError

Select Case Right$(mensaje, 1)
'Se identifica el final de un mensaje
  Case Chr$(3) '<ETX>
    Lectura_Datos mensaje
    objAutoAn.Estado = cteESPERA
    mensaje = ""
  End Select
  
Exit Sub

AutoAnError:
  Call objError.InternalError(Me, "Pentra60_Transmision", "Pentra60_Transmision")
End Sub
Private Sub Pentra60_Espera(mensaje As String)
'Identifica la llegada de caracteres de comienzo de transmision
'comienzo del mensaje.
'cada mensaje comienza con <STX> y termina con <ETX>
On Error GoTo AutoAnError
  If Right$(mensaje, 1) = Chr$(2) Then '<STX>
  'llegada de caracter de comienzo de mensaje
  objAutoAn.Estado = cteTRANSMISION
  mensaje = ""
  End If
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Pentra60_Espera", "Pentra60_Espera")
End Sub
Public Sub Descargar()
  objAutoAn.EscribirLog "Fin de Transmision"
  End
End Sub
Public Sub Lectura_Protocolo(caracter As String)
'El sistema (el ordenador que comunica con el autoanalizador) puede estar
'en uno de estos dos estados:
' Espera: no se ha establecido todav�a la comunicaci�n.
' Transmisi�n: el autoanalizador est� mandando datos al sistema.

Static mensaje As String 'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
    'pudendo ser modificada en �stas. Al ser modificada la copia, tambi�n deber� modificarse el original.
    'El hecho de utilizar esta copia se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas
    'por estar declarada como Static en esta subrutina.
On Error GoTo AutoAnError

  mensaje = mensaje & caracter
  CopiaMensaje = mensaje
  Select Case objAutoAn.Estado
    Case cteESPERA
      Pentra60_Espera CopiaMensaje
      mensaje = CopiaMensaje
    Case cteTRANSMISION
      Pentra60_Transmision CopiaMensaje
      mensaje = CopiaMensaje
  End Select

Exit Sub

AutoAnError:
  Call objError.InternalError(Me, "Lectura_Protocolo", "Lectura_Protocolo")
End Sub

Public Sub probar()
  Static nvez As Integer
  nvez = nvez + 1
  
  Select Case nvez
    Case 1: objAutoAn.AutoRecepcion Chr$(2) & Chr$(2)
    Case 2: objAutoAn.AutoRecepcion Chr$(2) & Chr$(2) & "A" & Chr$(13) & "1" & Chr$(13) & "1" & Chr$(13) & "B" & Chr$(13) & "adt0454" & Chr$(13) & "1" & Chr$(13) & "5" & Chr$(13) & "16/02/00E" & Chr$(13) & "6.80F" & Chr$(13) & "A" & Chr$(13) & "2.08J" & Chr$(13) & "67.6" & Chr$(13) & "3.4" & Chr$(13) & "9.1" & Chr$(13) & "10.9" & Chr$(13) & "9.0K" & Chr$(13) & "4.60" & Chr$(13) & "0.23" & Chr$(13) & "0.62" & Chr$(13) & "0.74" & Chr$(13) & "0.61" & Chr$(3)
    Case 3: objAutoAn.AutoRecepcion Chr$(2) & Chr$(2)
    Case 4: objAutoAn.AutoRecepcion Chr$(2) & Chr$(2) & "A" & Chr$(13) & "1" & Chr$(13) & "1" & Chr$(13) & "B" & Chr$(13) & "ads3776" & Chr$(13) & "1" & Chr$(13) & "5" & Chr$(13) & "16/02/00E" & Chr$(13) & "6.80F" & Chr$(13) & "A" & Chr$(13) & "2.08J" & Chr$(13) & "67.6" & Chr$(13) & "3.4" & Chr$(13) & "9.1" & Chr$(13) & "10.9" & Chr$(13) & "9.0K" & Chr$(13) & "4.60" & Chr$(13) & "0.23" & Chr$(13) & "0.62" & Chr$(13) & "0.74" & Chr$(13) & "0.61" & Chr$(3)
  End Select
 
End Sub


