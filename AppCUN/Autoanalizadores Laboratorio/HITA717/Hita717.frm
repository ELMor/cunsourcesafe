VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Hitachi 717"
   ClientHeight    =   840
   ClientLeft      =   705
   ClientTop       =   1545
   ClientWidth     =   1320
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   12
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   -1  'True
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Hita717.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   840
   ScaleWidth      =   1320
   Begin VB.Timer HITACHI 
      Enabled         =   0   'False
      Interval        =   1500
      Left            =   900
      Top             =   360
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   360
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Timer TimerNuevasPruebas 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   480
      Top             =   360
   End
   Begin VB.Label label 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "STOP"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000005&
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   1035
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "mnuMenu"
      Begin VB.Menu mnuAC 
         Caption         =   "Programar a Mano -> 'AC'"
      End
      Begin VB.Menu mnuSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSP 
         Caption         =   "Pasar a Estado 'SP'"
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Por el momento no se considera la posibilidad de que se llene el buffer de recepci�n de datos y se produzca un error RXOVER.
'Si esto pudiera llegar a ocurrir la soluci�n ser� una de las siguientes:
'   1 - Aumentar el tama�o del buffer de recepci�n
'   2 - Enviar un Xoff cuando el buffer est� proximo a llenarse
Dim FirstActivate As Integer        'Se�ala si es la primera vez que el form se vualve activo

Dim estado As Integer               'Estado del sistema
Dim matrizpruebas() As String       'Matriz que contiene las pruebas de la lista de trabajo
Dim MatrizResultados() As String    'Matriz que contiene los resultados de las pruebas
Dim matrizgrid() As String          'Matriz que controla el grid
Dim matrizmuestradoble() As String
Dim matrizpruebasRE() As String       'Matriz que contiene las pruebas de la lista de trabajo
Dim matrizgridRE() As String          'Matriz que controla el grid

Dim cListaRealizacion As Long       'N�mero de la lista de trabajo
Dim infoMuestras As String          'Informaci�n de las posiciones de las muestras en el carrusel

Dim numReenvios As Integer          'Cuenta el n�mero de veces que se intenta transmitir el mismo mensaje
Dim SEGUNDAPARTE As Integer         'Indica si se espera un segundo env�o de resultados de la misma muestra

Dim procesar As Integer     'vble que paraliza la comunicacion por motivos de seguridad
Dim prim As Integer         'vble que indica que ha comenzado la comunicacion

Dim limite As Integer       'Vble que almacena el n� de filas activas para hacer programacion.
                            'Es igual al limite superior de matrizgrid cuando se le da al start.
                            'Por si acaso se ejecutan pruebas dinamicas durante el proceso,
                            'para que las programe



'************************************************************
'
'MODIFICACIONES DE LA DRA GIL:
'
'1- QUE DESAPAREZCAN LOS QUE TIENEN RESULTADO
'2- QUE SE AUTODESCARGA CUANDO FINALIZA LA LISTA
'3- QUE CONSIDERE SOLO FUERA DE RANGO A LOS AVISOS
'
'
'************************************************************

Dim MiOpened As Boolean

Property Let blnOpened(Val As Boolean)
  If MiOpened <> Val Then
    MiOpened = Val
    With objAutoAn
      .CambiarButtonProgram Val
      .LabelVisible Not Val
    End With
  End If
End Property

Property Get blnOpened() As Boolean
  blnOpened = MiOpened
End Property

Private Sub cmdUrgencias_Click()
'Dim i As Integer
'    For i = 0 To ssdgpruebas.SelCount - 1
'        ssdgpruebas.Row = ssdgpruebas.SelRow(i)
'        If ssdgpruebas.ColText(7) <> "RE" Then
'            If ssdgpruebas.ColText(7) = "SP" Then
'                Text1 = Text1 & Chr$(13) & Chr$(10) & "pasada a urgente la fila " & ssdgpruebas.Row + 1 & Chr$(13) & Chr$(10)
'                ssdgpruebas.ColText(0) = "PROG."
'                ssdgpruebas.ColText(1) = " A"
'                ssdgpruebas.ColText(2) = "MANO"
'                ssdgpruebas.ColText(7) = "AC"
'                'Fila 1: muestra       Fila 2: estado       Fila 3: disco       Fila 4: copa
'                'Fila 5: dilucion      Fila 6: n�mero secuencia
'                matrizgrid(7, ssdgpruebas.Row + 1) = "PROG."
'                matrizgrid(3, ssdgpruebas.Row + 1) = " A"
'                matrizgrid(4, ssdgpruebas.Row + 1) = "MANO"
'                matrizgrid(2, ssdgpruebas.Row + 1) = "AC"
'            Else
'                Text1 = Text1 & Chr$(13) & Chr$(10) & "pasada a NO urgente la fila " & ssdgpruebas.Row + 1 & Chr$(13) & Chr$(10)
'                ssdgpruebas.ColText(0) = ""
'                ssdgpruebas.ColText(1) = ""
'                ssdgpruebas.ColText(2) = ""
'                ssdgpruebas.ColText(7) = "SP"
'                matrizgrid(7, ssdgpruebas.Row + 1) = ""
'                matrizgrid(3, ssdgpruebas.Row + 1) = ""
'                matrizgrid(4, ssdgpruebas.Row + 1) = ""
'                matrizgrid(2, ssdgpruebas.Row + 1) = "SP"
'            End If
'        End If
'    Next i
'
'    For i = 0 To ssdgpruebas.SelCount - 1
'        ssdgpruebas.EvalRowIsSelected = 0
'    Next i
'
End Sub

Private Function DecodificarAviso(Aviso As String) As String
  Select Case Aviso
    Case "A": DecodificarAviso = "ADC anormal"
    Case "Q": DecodificarAviso = "Blanco de cubetas anormal"
    Case "V": DecodificarAviso = "Muestra insuficiente"
    Case "T": DecodificarAviso = "Reactivo insuficiente"
    Case "Z": DecodificarAviso = "Exceso de absorbancia"
    Case "P": DecodificarAviso = "Error prozona"
    Case "I", "J", "K": DecodificarAviso = "L�mite de reacci�n superado"
    Case "W": DecodificarAviso = "Linearidad anormal (9 o m�s puntos)"
    Case "F": DecodificarAviso = "Linearidad anormal (8 o menos puntos)"
    Case "U": DecodificarAviso = "Error de duplicaci�n"
    Case "S": DecodificarAviso = "Error est�ndar"
    Case "Y": DecodificarAviso = "Error de sensibilidad"
    Case "B": DecodificarAviso = "Error de calibraci�n"
    Case "G": DecodificarAviso = "Error de conversi�n"
    Case Else: DecodificarAviso = ""
  End Select
End Function

Private Sub IniciarLista()
Dim objMuestra As clsMuestra
  For Each objMuestra In objAutoAn.ColMuestras
    If objMuestra.strestado = cteSINPROGRAMAR Then
      objMuestra.strestado = ctePREPROGRAMACION
    End If
  Next
End Sub

Private Sub Envio_Mensajes()
Dim cPruebaAuto As Integer      'C�digo de la prueba establecido en la tabla pruebasAutoAnalizador
Dim cmuestra As String            'C�digo de la muestra le�do por el autoanalizador
Dim Canal As String * 35        'Canal
Dim mensaje As String
Dim contador1 As Integer
Dim contador2 As Integer
Dim contador3 As Integer
Dim contador4 As Integer
Dim objMuestra As clsMuestra
Dim SQL As String
Dim i%, preprocesar%
Static posic$ 'indica en que posicion queda la programacion para la siguiente vez en caso dinamico
  
  If blnOpened Then 'Sistema No cerrado
    For contador1 = 1 To 4
      If Mid$(infoMuestras, 1 + (contador1 - 1) * 22, 1) = "K" Then
        If Mid$(infoMuestras, 14 + (contador1 - 1) * 22, 8) = "        " Then
          mensaje = mensaje & Mid$(infoMuestras, 1 + (contador1 - 1) * 22, 10) & Space$(53) & "0000000000000000000000000000000000 "
          For Each objMuestra In objAutoAn.ColMuestras
            With objMuestra
              If .strestado = ctePREPROGRAMACION Then  'Or matrizgrid(2, contador2) = "AC") Then 'CodigoANumero(matrizgrid(1, contador2)) = cMuestra And
                mensaje = Left$(mensaje, Len(mensaje) - 85) & Left$(.strCodMuestra, 8) & " " & Right$(mensaje, 76)
                .strestado = cteACEPTADA
                .strDisco = Mid$(infoMuestras, 7 + (contador1 - 1) * 22, 1)
                .strCopa = Trim$(Mid$(infoMuestras, 8 + (contador1 - 1) * 22, 2))
                .strProperty2 = Trim$(Mid$(infoMuestras, 2 + (contador1 - 1) * 22, 4))
                mensaje = Left$(mensaje, Len(mensaje) - 35) & .strProperty1 'canal
                'si ya se han acabado las muestras de la lista
                If Not objAutoAn.blnExistMuestraConEstado(ctePREPROGRAMACION) Then blnOpened = False
                Exit For
              End If
            End With
          Next
        Else
          'estos son los mensajes de respuesta a posiciones ya ocupadas por alguna muestra
          mensaje = mensaje & Mid$(infoMuestras, 1 + (contador1 - 1) * 22, 10) & Space$(53) & "0000000000000000000000000000000000 "
          For Each objMuestra In objAutoAn.ColMuestras
            With objMuestra
              If .strCodMuestra = Mid$(infoMuestras, 14 + (contador1 - 1) * 22, 8) Then  'Or matrizgrid(2, contador2) = "AC") Then 'CodigoANumero(matrizgrid(1, contador2)) = cMuestra And
                mensaje = Left$(mensaje, Len(mensaje) - 85) & Left$(.strCodMuestra, 8) & " " & Right$(mensaje, 76)
                .strestado = cteACEPTADA
                .strDisco = Mid$(infoMuestras, 7 + (contador1 - 1) * 22, 1)
                .strCopa = Trim$(Mid$(infoMuestras, 8 + (contador1 - 1) * 22, 2))
                .strProperty2 = Trim$(Mid$(infoMuestras, 2 + (contador1 - 1) * 22, 4))
                mensaje = Left$(mensaje, Len(mensaje) - 35) & .strProperty1
                Exit For
              End If
            End With
          Next
        End If
      Else
        'cuando nos manda el ultimo mensaje de peticion del carro y este no es completo (nos manda
        '3 posiciones en vez de 4)
        'p.ej   "k 118    k 119    k120      xxxxxxxx"
        mensaje = mensaje & Space$(98)
      End If
    Next contador1
    'Se coloca el protocolo en el mensaje
    SQL = Chr$(2) & "01" & Mid$(mensaje, 1, 196) & Chr$(13) & Chr$(10) & Chr$(23) & Chr$(2)
    SQL = SQL & "01" & Mid$(mensaje, 197) & Chr$(13) & Chr$(10) & Chr$(3)
    'Se env�a el mensaje de programaci�n de pruebas
    objAutoAn.Enviar SQL
  End If
  objAutoAn.estado = cteESPERA
End Sub

'Private Sub Envio_Mensajes2()
'Dim cPruebaAuto As Integer      'C�digo de la prueba establecido en la tabla pruebasAutoAnalizador
'Dim cmuestra As String            'C�digo de la muestra le�do por el autoanalizador
'Dim Canal As String * 35        'Canal
'Dim mensaje As String
'Dim contador1 As Integer
'Dim contador2 As Integer
'Dim contador3 As Integer
'Dim contador4 As Integer
'Dim SQL As String
'Dim i%, preprocesar%
'Static posic$ 'indica en que posicion queda la programacion para la siguiente vez en caso dinamico
''
''***** DESARROLLO *****
'
'    '''''on error Resume Next
'    If posic = "" Then posic = "0"
'    'cuando se inicia la comunicacion deshabilitamos la posibilidad de cambiar ya ninguna fila
'    'tambien cuando se recibe una peticion de la priemra posicion esperada para la siguiente prog
'    'ya que es posible que sin haber acabado el proceso de preguntas anterior (i-60) se haya podido
'    'habilitar el boton de start para la siguiente vez
'    If prim = 0 Or CInt(posic) + 1 = CInt(Mid$(infoMuestras, 3, 3)) Then
'        prim = 1
'        limite = UBound(matrizgrid, 2)
'        cmdUrgencias.Enabled = False
''        Command1.Enabled = False
'    End If
'
'    'se comprueba que se ha acabado el proceso de comunicacion cuando se recibe el mensaje de peticion de la ultima posicion
'    'Entonces es cuando se pueden ejecutar la programacion de las nuevas pruebas.
'    'reiniciamos la vble que indica que comienza una nueva comunicacion
'    For contador1 = 1 To 4
'        If Mid$(infoMuestras, 8 + (contador1 - 1) * 22, 2) = "60" Then
'
'            preprocesar = 1
'            cmdUrgencias.Enabled = True
''            Command1.Enabled = True
'            prim = 0
'        End If
'    Next contador1
'
'    For i = 1 To limite
'    'EL LIMITE se establece al principio para no programar en las posiciones aleatorias en las
'    'que el autoanaliz. pregunta cuando aparecen las muestras dinamicas
'        If matrizgrid(2, i) = "SP" Then
'            Exit For
'        End If
'    Next i
'
'    If i > limite Or procesar = 0 Then
'        'cuando las peticiones de prog superan el n�muestras que existian cuando comenzaron las
'        'peticiones se paraliza la prog. de pruebas dinamicas hasta el siguiente proceso
'        '(al pulsar de nuevo START)( y por prog. al recibir la ultima peticion)
'        procesar = 0
'        cmdUrgencias.Enabled = False
''        Command1.Enabled = False
'        objAutoAn.estado = cteESPERA
'
'        If preprocesar = 1 Then
'            procesar = 1
'            cmdUrgencias.Enabled = True
''            Command1.Enabled = True
'        End If
'        Exit Sub
'    End If
'
'    'Se procesa la variable infoMuestras para ver cuantas muestras hay que programar
'
'    For contador1 = 1 To 4
'        If Mid$(infoMuestras, 1 + (contador1 - 1) * 22, 1) = "K" Then
'            '1� programacion de una posicion
'            If Mid$(infoMuestras, 14 + (contador1 - 1) * 22, 8) = "        " Then
'                mensaje = mensaje & Mid$(infoMuestras, 1 + (contador1 - 1) * 22, 10) & Space$(53) & "0000000000000000000000000000000000 "
'                'Se obtienen los datos para la muestra
'                For contador2 = 1 To limite
'                    'Text1 = Text1 & "matrizgrid(2, " & contador2 & ")=" & matrizgrid(2, contador2) & Chr$(13) & Chr$(10)
'                    If matrizgrid(2, contador2) = "SP" Then 'Or matrizgrid(2, contador2) = "AC") Then 'CodigoANumero(matrizgrid(1, contador2)) = cMuestra And
'                        'Fila 1: muestra       Fila 2: estado       Fila 3: disco       Fila 4: copa
'                        'Fila 5: ID muestra    Fila 6: dilucion     Fila 7: n�mero secuencia
'                        'mensaje = mensaje & Mid$(infoMuestras, 1 + (contador1 - 1) * 22, 10) & matrizgrid(1, contador2) & Space$(11 - Len(matrizgrid(1, contador2))) & Space$(42) & matrizgrid(9, contador2)
'                        mensaje = Left$(mensaje, Len(mensaje) - 85) & Left$(matrizgrid(1, contador2), 8) & " " & Right$(mensaje, 76)
'                        matrizgrid(2, contador2) = "AC"
'                        matrizgrid(3, contador2) = Mid$(infoMuestras, 7 + (contador1 - 1) * 22, 1)
'                        matrizgrid(4, contador2) = Trim$(Mid$(infoMuestras, 8 + (contador1 - 1) * 22, 2))
'                        matrizgrid(7, contador2) = Trim$(Mid$(infoMuestras, 2 + (contador1 - 1) * 22, 4))
'                        mensaje = Left$(mensaje, Len(mensaje) - 35) & matrizgrid(9, contador2)
'                        If contador2 = limite Then
'                            'almacenamos la posicion en que queda para la proxima programacion dinamica
'                            posic = Mid$(infoMuestras, 3 + (contador1 - 1) * 22, 3)
'                            'Text1 = Text1 & "posic: " & posic & Chr$(13) & Chr$(10)
'                        End If
'                        Exit For
'                    End If
'                Next contador2
'            Else
'
'                'estos son los mensajes de respuesta a posiciones ya ocupadas por alguna muestra
'                mensaje = mensaje & Mid$(infoMuestras, 1 + (contador1 - 1) * 22, 10) & Space$(53) & "0000000000000000000000000000000000 "
'                'Se obtienen los datos para la muestra
'                For contador2 = 1 To limite
'                    'Text1 = Text1 & "matrizgrid(2, " & contador2 & ")=" & matrizgrid(2, contador2) & Chr$(13) & Chr$(10)
'                    If matrizgrid(1, contador2) = Mid$(infoMuestras, 14 + (contador1 - 1) * 22, 8) Then 'Or matrizgrid(2, contador2) = "AC") Then 'CodigoANumero(matrizgrid(1, contador2)) = cMuestra And
'                        'Fila 1: muestra       Fila 2: estado       Fila 3: disco       Fila 4: copa
'                        'Fila 5: ID muestra    Fila 6: dilucion     Fila 7: n�mero secuencia
'                        mensaje = Left$(mensaje, Len(mensaje) - 85) & Left$(matrizgrid(1, contador2), 8) & " " & Right$(mensaje, 76)
'                        matrizgrid(2, contador2) = "AC"
'                        matrizgrid(3, contador2) = Mid$(infoMuestras, 7 + (contador1 - 1) * 22, 1)
'                        matrizgrid(4, contador2) = Trim$(Mid$(infoMuestras, 8 + (contador1 - 1) * 22, 2))
'                        matrizgrid(7, contador2) = Trim$(Mid$(infoMuestras, 2 + (contador1 - 1) * 22, 4))
'
'                        'Se sigue formando el mensaje
'                        mensaje = Left$(mensaje, Len(mensaje) - 35) & matrizgrid(9, contador2)
'                        If contador2 = limite Then
'                            'almacenamos la posicion en que queda para la proxima programacion dinamica
'                            posic = Mid$(infoMuestras, 3 + (contador1 - 1) * 22, 3)
'                            'Text1 = Text1 & "posic: " & posic & Chr$(13) & Chr$(10)
'                        End If
'                        Exit For
'                    End If
'                Next contador2
'            End If
'        Else
'            'cuando nos manda el ultimo mensaje de peticion del carro y este no es completo (nos manda
'            '3 posiciones en vez de 4)
'            'p.ej   "k 118    k 119    k120      xxxxxxxx"
'
'            mensaje = mensaje & Space$(98)
'        End If
'    Next contador1
'
'    'Se coloca el protocolo en el mensaje
'    SQL = Chr$(2) & "01" & Mid$(mensaje, 1, 196) & Chr$(13) & Chr$(10) & Chr$(23) & Chr$(2)
'    SQL = SQL & "01" & Mid$(mensaje, 197) & Chr$(13) & Chr$(10) & Chr$(3)
'
'    'Se env�a el mensaje de programaci�n de pruebas
'    objAutoAn.Enviar SQL
'    objAutoAn.estado = cteESPERA
'
'    'esto significa que esta listo para una nueva programacion cuando termina el proceso
'    If preprocesar = 1 Then procesar = 1
'
'End Sub

Private Sub Form_Activate()
'Dim tiempo As Long, tiempo2 As Long, tiempo3 As Long, tim As Long
'
'  'tiempo = gettickcount()
'  If FirstActivate = True Then
'    'Frame1.Visible = True
'    DoEvents
'    FirstActivate = False
'    Screen.MousePointer = Default
'    'Frame1.Visible = False
'  End If
'  ReDim matrizmuestradoble(0 To 1)
'  procesar = 1
End Sub

Private Sub Form_Load()
'  FirstActivate = True
'  'Se inicializa la variable segundaParte
'  SEGUNDAPARTE = False
  blnOpened = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
  End
End Sub

Private Sub Hitachi_Esclavo(mensaje As String)
'Identifica el final del mensaje que se est� recibiendo y procede a su lectura si es el caso.
  If Right$(mensaje, 3) = Chr$(13) & Chr$(10) & Chr$(3) Or _
  Right$(mensaje, 3) = Chr$(13) & Chr$(10) & Chr$(23) Or _
  Right$(mensaje, 1) = Chr$(3) Or Right$(mensaje, 1) = Chr$(23) Then
  '<CR><LF><ETX> o <CR><LF><ETB> - Fin del mensaje.
    Lectura_Datos mensaje
    mensaje = ""
    If objAutoAn.estado = cteMASTER Then
      Envio_Mensajes
    Else
    '    Comm1.Output = Chr$(6) '<ACK> - Respuesta afirmativa
      objAutoAn.estado = cteESPERA
    End If
  End If
End Sub

Private Sub Hitachi_Espera(mensaje As String)
'Identifica la llegada de los caracteres de comienzo o final de la transmisi�n.
    If Right$(mensaje, 1) = Chr$(2) Then '<STX> - LLegada de los caracteres de comienzo de la transmisi�n
        objAutoAn.estado = cteESCLAVO
        mensaje = ""
        'PasarMuestrasAPreProg 'para marcar las muestras que se programaran
    'ElseIf Right$(Mensaje, 1) = Chr$(4) Then '<EOT> - Fin de la transmisi�n
    '    Form_Unload (False)
    End If
End Sub

Private Sub PasarMuestrasAPreProg()
Dim objMuestra As clsMuestra
  For Each objMuestra In objAutoAn.ColMuestras
    If objMuestra.strestado = cteSINPROGRAMAR Then
      objMuestra.strestado = ctePREPROGRAMACION
    End If
  Next
End Sub

Private Sub Lectura_Datos(Datos As String)
'Los datos que llegan pueden ser de requerimientos de programaci�n de pruebas (funci�n 01)
'o de resultados (funciones 02).
Dim codMuestra As String          'C�digo de la muestra analizada
Dim cResultadoAuto As String    'C�digo del resultado definido en resultadosAutoanalizador
Dim Aviso As String             'C�digo de error del resultado
Dim Resultado As String         'Valor del resultado
Dim contador1 As Integer
Dim NumResultados As Integer
Dim SQL As String

  Select Case CInt(Mid$(Datos, 1, 2))
    Case 1, 51 'Mensaje con datos de la muestra para la programaci�n de pruebas
      infoMuestras = Mid$(Datos, 3, Len(Datos) - 5)
      objAutoAn.estado = cteMASTER
    Case 2, 3, 52, 53 'Mensaje de resultados
      'Se inicializa la matriz que contendr� los resultados. Esta matriz almacenar� en (1,1) el c�digo de la muestra, en
      '(2,1) la fecha y la hora separadas por una coma, en (1,X) el cResultadoAuto de la prueba y en (2,X) el resultado.
      'NOTA: el cResultadoAuto de una prueba coincide con el c�digo de la prueba.
      'Se obtiene el c�digo de la muestra y la fecha y hora
      If Val(Trim$(Mid$(Datos, 13, 11))) = 0 Then
        codMuestra = Trim$(Mid$(Datos, 13, 11))
      Else
        On Error Resume Next
        codMuestra = objAutoAn.fNumeroACodigo(CLng(Trim$(Mid$(Datos, 13, 11))))
      End If
      'Se elimina la parte de los datos ya le�da
      Datos = Mid$(Datos, 25)
      'Se anotan los resultados en la matriz
      NumResultados = CInt((Len(Datos) - 1) / 9)
      If NumResultados > 0 Then
        For contador1 = 1 To NumResultados
          cResultadoAuto = Trim$(Mid$(Datos, (contador1 - 1) * 9 + 1, 2))
          'Recogida de avisos. Hay ocasiones en que las muestras se programan como urgencias
          'de forma manual con c�digos de hasta 3 n�meros que al ser le�das se transforman
          'en 3 letras, gui�n y hasta 3 n�meros. S�lo leeremos los avisos de las muestras
          'que respondan al formato adecuado
          If Len(codMuestra) = 8 Then
            Aviso = Trim$(Mid$(Datos, contador1 * 9, 1))
            If Aviso <> "" Then
              Aviso = DecodificarAviso(Aviso)
              If Aviso <> "" Then
                With frmAvisos.txtAvisos
                  'If .Text = "" Then
                    .Text = .Text & "Muestra: " & codMuestra & "     Test: " & cResultadoAuto & Chr$(13) & Chr$(10)
                  'Else
                  '  .Text = .Text & "Muestra: " & MatrizResultados(1, 1) & "     Test: " & cResultadoAuto & Chr$(13) & Chr$(10)
                  'End If
                  .Text = .Text & Aviso & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
                  frmAvisos.Tag = "Visible"
                  frmAvisos.Show
                End With
              End If
            End If
          End If
          Resultado = Trim$(Mid$(Datos, 3 + (contador1 - 1) * 9, 6))
          Call objAutoAn.pIntroResultConCResAuto(codMuestra, cResultadoAuto, Resultado)
'          With objAutoAn.ColMuestras(codMuestra)
'            If .strestado = cteREALIZACIONCOMPLETA Or _
'            .strestado = cteAREPETIRFIN Or _
'            .strestado = cteREALIZADA Or _
'            .strestado = cteFUERADERANGOFIN Or _
'            .strestado = cteFALLODEAUTOANALIZADORFIN Or _
'            .strestado = cteREALIZACIONCOMPLETA Then .quitarmuestra
'          End With
        Next contador1
      End If
  End Select
End Sub

Private Sub Lectura_Datos2(Datos As String)
'Los datos que llegan pueden ser de requerimientos de programaci�n de pruebas (funci�n 01)
'o de resultados (funciones 02).
Dim cmuestra As String          'C�digo de la muestra analizada
Dim fecha As String             'Fecha en que se realiz� el an�lisis
Dim hora As String              'Hora en que se realiz� el an�lisis
Dim cResultadoAuto As String    'C�digo del resultado definido en resultadosAutoanalizador
Dim Aviso As String             'C�digo de error del resultado
Dim Resultado As String         'Valor del resultado
Dim contador1 As Integer
Dim NumResultados As Integer
Dim SQL As String

    Select Case CInt(Mid$(Datos, 1, 2))
      Case 1, 51 'Mensaje con datos de la muestra para la programaci�n de pruebas
        infoMuestras = Mid$(Datos, 3, Len(Datos) - 5)
        objAutoAn.estado = cteMASTER
      Case 2, 3, 52, 53 'Mensaje de resultados
        'Se inicializa la matriz que contendr� los resultados. Esta matriz almacenar� en (1,1) el c�digo de la muestra, en
        '(2,1) la fecha y la hora separadas por una coma, en (1,X) el cResultadoAuto de la prueba y en (2,X) el resultado.
        'NOTA: el cResultadoAuto de una prueba coincide con el c�digo de la prueba.
        'If SEGUNDAPARTE = False Then
        ReDim MatrizResultados(1 To 2, 1 To 1)
        'Se obtiene el c�digo de la muestra y la fecha y hora
        If Val(Trim$(Mid$(Datos, 13, 11))) = 0 Then
          cmuestra = Trim$(Mid$(Datos, 13, 11))
        Else
          On Error Resume Next
          cmuestra = objAutoAn.fNumeroACodigo(CLng(Trim$(Mid$(Datos, 13, 11))))
        End If
        MatrizResultados(1, 1) = cmuestra
        fecha = Format(Now, "dd/mm/yyyy")
        hora = Format(Now, "hh:mm:ss")
        MatrizResultados(2, 1) = fecha & "," & hora
        'Se elimina la parte de los datos ya le�da
        Datos = Mid$(Datos, 25)
        'Se anotan los resultados en la matriz
        NumResultados = CInt((Len(Datos) - 1) / 9)
        If NumResultados > 0 Then
          For contador1 = 1 To NumResultados
            ReDim Preserve MatrizResultados(1 To 2, 1 To UBound(MatrizResultados, 2) + 1)
            cResultadoAuto = Trim$(Mid$(Datos, (contador1 - 1) * 9 + 1, 2))
            MatrizResultados(1, UBound(MatrizResultados, 2)) = cResultadoAuto
            'SEGUNDAPARTE = True
            'text1 = text1 & Chr$(13) & Chr$(10) & "LECTURA_DATOS 2:SEGUNDAPARTE=TRUE" & Chr$(13) & Chr$(10)
            'Recogida de avisos. Hay ocasiones en que las muestras se programan como urgencias
            'de forma manual con c�digos de hasta 3 n�meros que al ser le�das se transforman
            'en 3 letras, gui�n y hasta 3 n�meros. S�lo leeremos los avisos de las muestras
            'que respondan al fromato adecuado
            If Len(MatrizResultados(1, 1)) = 8 Then
              Aviso = Trim$(Mid$(Datos, contador1 * 9, 1))
              If Aviso <> "" Then
                Aviso = DecodificarAviso(Aviso)
                If Aviso <> "" Then
                  If frmAvisos.txtAvisos.Text = "" Then
                    frmAvisos.txtAvisos.Text = "Muestra: " & MatrizResultados(1, 1) & "     Test: " & cResultadoAuto & Chr$(13) & Chr$(10)
                  Else
                    frmAvisos.txtAvisos.Text = frmAvisos.txtAvisos.Text & "Muestra: " & MatrizResultados(1, 1) & "     Test: " & cResultadoAuto & Chr$(13) & Chr$(10)
                  End If
                  frmAvisos.txtAvisos.Text = frmAvisos.txtAvisos.Text & Aviso & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
                  ''''on error Resume Next
                  frmAvisos.Tag = "Visible"
                  frmAvisos.Show vbModeless
                End If
              End If
            End If
            Resultado = Trim$(Mid$(Datos, 3 + (contador1 - 1) * 9, 6))
            MatrizResultados(2, UBound(MatrizResultados, 2)) = Resultado
          Next contador1
        End If
'        Actualizar_BD
    End Select
    
End Sub

Public Sub Lectura_Protocolo(Caracter As String)
'El sistema (ordenador que comunica con el autoanalizador) puede estar en uno de estos estados:
'   Espera: a la espera de recibir mensajes procedente del autoanalizador
'   Esclavo: recibiendo mensajes procedentes del autoanalizador
'   Master: enviando informaci�n al autoanalizador
Static mensaje As String   'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas

  mensaje = mensaje & Caracter 'Se va componiendo el mensaje
  CopiaMensaje = mensaje
  Select Case objAutoAn.estado
    Case cteESPERA
      Hitachi_Espera CopiaMensaje
      mensaje = CopiaMensaje
    Case cteESCLAVO
      Hitachi_Esclavo CopiaMensaje
      mensaje = CopiaMensaje
  End Select
End Sub

Private Sub HITACHI_Timer()
Static NVez As Integer
NVez = NVez + 1
'objAutoAn.AutoRecepcion Chr$(2) & "01K  12 012             K  13 013             K  14 014             K  15 015             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  16 016             K  17 017             K  18 018             K  19 019             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  20 020             K  21 021             K  22 022             K  23 023             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  24 024             K  25 025             K  26 026             K  27 027             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  28 028             K  29 029             K  30 030             K  31 031             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  32 032             K  33 033             K  34 034             K  35 035             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  36 036             K  37 037             K  38 038             K  39 039             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  40 040             K  41 041             K  42 042             K  43 043             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  44 044             K  45 045             K  46 046             K  47 047             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  48 048             K  49 049             K  50 050             K  51 051             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  52 052             K  53 053             K  54 054             K  55 055             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  56 056             K  57 057             K  58 058             K  59 059             " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "01K  60 060                                                                               " & vbCrLf & Chr$(3)
'objAutoAn.AutoRecepcion Chr$(2) & "03N  12 012    ACV-2595  1    54  2   309  3    12  4     7  5     5  6   120  7   462 16  0.04 17  0.25 " & vbCrLf & Chr$(3)


'  Case 1: objAutoAn.AutoRecepcion Chr$(2) & "01K   3 0 3             K   4 0 4             K   5 0 5             K   6 0 6             " & vbCrLf & Chr$(3)
'  Case 2: objAutoAn.AutoRecepcion Chr$(2) & "01K   7 0 7             K   8 0 8             K   9 0 9             K  10 010             " & vbCrLf & Chr$(3)
'  Case 3: objAutoAn.AutoRecepcion Chr$(2) & "01K  11 011             K  12 012             K  13 013             K  14 014             " & vbCrLf & Chr$(3)
'  Case 4: objAutoAn.AutoRecepcion Chr$(2) & "01K  15 015             K  16 016             K  17 017             K  18 018             " & vbCrLf & Chr$(3)
'  Case 5: objAutoAn.AutoRecepcion Chr$(2) & "01K  19 019             K  20 020             K  21 021             K  22 022             " & vbCrLf & Chr$(3)
'  Case 6: objAutoAn.AutoRecepcion Chr$(2) & "01K  23 023             K  24 024             K  25 025             K  26 026             " & vbCrLf & Chr$(3)
'  Case 7: objAutoAn.AutoRecepcion Chr$(2) & "01K  27 027             K  28 028             K  29 029             K  30 030             " & vbCrLf & Chr$(3)
'  Case 8: objAutoAn.AutoRecepcion Chr$(2) & "01K  31 031             K  32 032             K  33 033             K  34 034             " & vbCrLf & Chr$(3)
'  Case 9: objAutoAn.AutoRecepcion Chr$(2) & "01K  35 035             K  36 036             K  37 037             K  38 038             " & vbCrLf & Chr$(3)
'  Case 10: objAutoAn.AutoRecepcion Chr$(2) & "01K  39 039             K  40 040             K  41 041             K  42 042             " & vbCrLf & Chr$(3)
'  Case 11: objAutoAn.AutoRecepcion Chr$(2) & "01K  43 043             K  44 044             K  45 045             K  46 046             " & vbCrLf & Chr$(3)
'  Case 12: objAutoAn.AutoRecepcion Chr$(2) & "01K  47 047             K  48 048             K  49 049             K  50 050             " & vbCrLf & Chr$(3)
'  Case 13: objAutoAn.AutoRecepcion Chr$(2) & "01K  51 051             K  52 052             K  53 053             K  54 054             " & vbCrLf & Chr$(3)
'  Case 14: objAutoAn.AutoRecepcion Chr$(2) & "01K  55 055             K  56 056             K  57 057             K  58 058             " & vbCrLf & Chr$(3): HITACHI.Interval = 5000
'  Case 15: objAutoAn.AutoRecepcion Chr$(2) & "03N   3 0 3    ACV-2616  2   174  3     9  4    14  5    15  6    96 15  5.62 16  0.17 17  0.94 " & vbCrLf & Chr$(3)
'  Case 16: objAutoAn.AutoRecepcion Chr$(2) & "03N   4 0 4    ACV-2620  1    57  3    18  4    17  6   137  7    53 14  8.03 15  5.87 16  0.12 17  0.38 19   1.8$" & vbCrLf & Chr$(3)
'  Case 17: objAutoAn.AutoRecepcion Chr$(2) & "03N   5 0 5    ACV-2635  2  1395Q 3   264  4   245  5   376$ 6  1775K12   116 16  2.45 17  3.39 " & vbCrLf & Chr$(3)
'  Case 18: objAutoAn.AutoRecepcion Chr$(2) & "03N   6 0 6    ACV-2639  2   262  3    14  4    57  5    60  6   100 14  8.13 15  6.02 16  0.63 17  1.66 " & vbCrLf & Chr$(3)
'  Case 19: objAutoAn.AutoRecepcion Chr$(2) & "03N   7 0 7    ACV-2645  2   297  3    31  4    36  5   122  6   285  8   145  9   154 10  6.30 12    90 14  7.46 15  5.59 16  0.58 17  1.07 " & vbCrLf & Chr$(3)
'  Case 20: HITACHI.Enabled = False
  
  
  

  Select Case NVez
    Case 1: objAutoAn.AutoRecepcion Chr$(2) & "01K  47 047             K  48 048             K  49 049             K  50 050             " & vbCrLf & Chr$(3)
    Case 2: objAutoAn.AutoRecepcion Chr$(2) & "01K  51 051             K  52 052             K  53 053             K  54 054             " & vbCrLf & Chr$(3)
    Case 3: objAutoAn.AutoRecepcion Chr$(2) & "01K  55 055             K  56 056             K  57 057             K  58 058             " & vbCrLf & Chr$(3)
    Case 4: objAutoAn.AutoRecepcion Chr$(2) & "01K  59 059             K  60 060                                                         " & vbCrLf & Chr$(3)
    Case 5: objAutoAn.AutoRecepcion Chr$(2) & "02N  47 047    ACX-8261 15  4.13 " & vbCrLf & Chr$(3)
    Case 6: objAutoAn.AutoRecepcion Chr$(2) & "02N  48 048    ACX-8270 14  7.63 " & vbCrLf & Chr$(3)
    Case 7: objAutoAn.AutoRecepcion Chr$(2) & "03N  49 049    ACX-8325  5   182 " & vbCrLf & Chr$(3)
    Case 8: objAutoAn.AutoRecepcion Chr$(2) & "02N  50 050    ACX-8455 12   138 15  6.54 " & vbCrLf & Chr$(3)
    Case 9: objAutoAn.AutoRecepcion Chr$(2) & "03N  51 051    ACX-8457  9   100 12    35 " & vbCrLf & Chr$(3)
    Case 10: objAutoAn.AutoRecepcion Chr$(2) & "03N  52 052    ACX-8475  3     8  4     9  5    12  6    67  8    50  9   243 12    68 15  6.57 19   0.3$" & vbCrLf & Chr$(3)
    Case 11: objAutoAn.AutoRecepcion Chr$(2) & "03N  53 053    ACX-8487  3     9  4     7  5     8  6    93  8    47  9   218 10  4.10 14  9.99 16  0.24 17  0.54 " & vbCrLf & Chr$(3)
    Case 12: objAutoAn.AutoRecepcion Chr$(2) & "03N  54 054    ACX-8502  2   172  3    19  4    21  5    36  6   284 16  0.28 17  0.52 18     5F" & vbCrLf & Chr$(3)
    Case 13: objAutoAn.AutoRecepcion Chr$(2) & "03N  55 055    ACX-8507  3     8  4     6  5     8  6    92  8    61  9   179 10  3.67 12    92 14  9.01 15  6.49 16  0.24 17  0.09 " & vbCrLf & Chr$(3)
    Case 14: objAutoAn.AutoRecepcion Chr$(2) & "03N  56 056    ACX-8515  3    12  4    18  5    19 " & vbCrLf & Chr$(3)
    Case 15: objAutoAn.AutoRecepcion Chr$(2) & "02N  57 057    ACX-8519  2   250  3    11  4     4  5    11  6   102 14  8.60 15  5.78 16  0.22 17  0.55 " & vbCrLf & Chr$(3)
    Case 16: objAutoAn.AutoRecepcion Chr$(2) & "03N  58 058    ACX-8526 14 10.67 " & vbCrLf & Chr$(3)
    Case 17: objAutoAn.AutoRecepcion Chr$(2) & "03N  59 059    ACX-8534  3    12  4     8  8    87  9   138 10  8.00 12    87 16  1.06 17  2.85 " & vbCrLf & Chr$(3)
    Case 18: objAutoAn.AutoRecepcion Chr$(2) & "03N  60 060    ACX-8540  2   162  3     9  4     6  5    28  6   186 16  0.22 17  0.35 " & vbCrLf & Chr$(3)
    Case 19: objAutoAn.AutoRecepcion Chr$(2) & "01K  61 1 1             K  62 1 2             K  63 1 3             K  64 1 4             " & vbCrLf & Chr$(3)
    Case 20: objAutoAn.AutoRecepcion Chr$(2) & "01K  65 1 5             K  66 1 6             K  67 1 7             K  68 1 8             " & vbCrLf & Chr$(3)
    Case 21: objAutoAn.AutoRecepcion Chr$(2) & "01K  69 1 9             K  70 110             K  71 111             K  72 112             " & vbCrLf & Chr$(3)
    Case 22: objAutoAn.AutoRecepcion Chr$(2) & "01K  73 113             K  74 114             K  75 115             K  76 116             " & vbCrLf & Chr$(3)
    Case 23: objAutoAn.AutoRecepcion Chr$(2) & "01K  77 117             K  78 118             K  79 119             K  80 120             " & vbCrLf & Chr$(3)
    Case 24: objAutoAn.AutoRecepcion Chr$(2) & "01K  81 121             K  82 122             K  83 123             K  84 124             " & vbCrLf & Chr$(3)
    Case 25: objAutoAn.AutoRecepcion Chr$(2) & "01K  85 125             K  86 126             K  87 127             K  88 128             " & vbCrLf & Chr$(3)
    Case 26: objAutoAn.AutoRecepcion Chr$(2) & "01K  89 129             K  90 130             K  91 131             K  92 132             " & vbCrLf & Chr$(3)
    Case 27: objAutoAn.AutoRecepcion Chr$(2) & "01K  93 133             K  94 134             K  95 135             K  96 136             " & vbCrLf & Chr$(3)
    Case 28: objAutoAn.AutoRecepcion Chr$(2) & "01K  97 137             K  98 138             K  99 139             K 100 140             " & vbCrLf & Chr$(3)
    Case 29: objAutoAn.AutoRecepcion Chr$(2) & "01K 101 141             K 102 142             K 103 143             K 104 144             " & vbCrLf & Chr$(3)
    Case 30: objAutoAn.AutoRecepcion Chr$(2) & "01K 105 145             K 106 146             K 107 147             K 108 148             " & vbCrLf & Chr$(3)
    Case 31: objAutoAn.AutoRecepcion Chr$(2) & "01K 109 149             K 110 150             K 111 151             K 112 152             " & vbCrLf & Chr$(3)
    Case 32: objAutoAn.AutoRecepcion Chr$(2) & "01K 113 153             K 114 154             K 115 155             K 116 156             " & vbCrLf & Chr$(3)
    Case 33: objAutoAn.AutoRecepcion Chr$(2) & "01K 117 157             K 118 158             K 119 159             K 120 160             " & vbCrLf & Chr$(3)
    Case 34: objAutoAn.AutoRecepcion Chr$(2) & "03E   1 0 1        8432 16  1.15 17 11.70 " & vbCrLf & Chr$(3)
    Case 35: objAutoAn.AutoRecepcion Chr$(2) & "03N  61 1 1    ACX-8543  2   162  3    14  4    31  5    14  6   120 16  0.18 17  0.24 18     0 " & vbCrLf & Chr$(3)
    Case 36: objAutoAn.AutoRecepcion Chr$(2) & "03N  62 1 2    ACX-8548  2   238  3    12  4    11  5     7  6   121  9   246 16  0.30 17  0.80 18     0 " & vbCrLf & Chr$(3)
    Case 37: objAutoAn.AutoRecepcion Chr$(2) & "03N  63 1 3    ACX-8553  3    14  4    20  5    13  6    92  8    88  9   190 10  6.82 12    90 15  7.20 16  0.46 17  1.45 " & vbCrLf & Chr$(3)
    Case 38: objAutoAn.AutoRecepcion Chr$(2) & "03N  64 1 4    ACX-8566  3    78  4    41  5   135  6   385  8   131  9   128 10  6.13 12   119 15  6.62 16  1.13 17  1.45 " & vbCrLf & Chr$(3)
  End Select

End Sub

Private Sub mnuAC_Click()
Dim i%, strCodMuestra$
Dim bkmrk As Variant ' Bookmarks are always defined as variants

  With objAutoAn.GridMuestras
    For i = 0 To .SelBookmarks.Count - 1
      bkmrk = .SelBookmarks(i)
      strCodMuestra = .Columns("Muestra").CellValue(bkmrk)
      objAutoAn.ColMuestras(strCodMuestra).strestado = cteACEPTADA
      '.Row = .SelRow(i)
'      If .ColText(7) <> "RE" Then
'        If .ColText(7) = "SP" Then
          
'            ssdgpruebas.ColText(0) = "PROG."
'            ssdgpruebas.ColText(1) = " A"
'            ssdgpruebas.ColText(2) = "MANO"
'            ssdgpruebas.ColText(7) = "AC"
'            'Fila 1: muestra       Fila 2: estado       Fila 3: disco       Fila 4: copa
'            'Fila 5: dilucion      Fila 6: n�mero secuencia
'            matrizgrid(7, ssdgpruebas.Row + 1) = "PROG."
'            matrizgrid(3, ssdgpruebas.Row + 1) = " A"
'            matrizgrid(4, ssdgpruebas.Row + 1) = "MANO"
'            matrizgrid(2, ssdgpruebas.Row + 1) = "AC"
'          Else
'            ssdgpruebas.ColText(0) = ""
'            ssdgpruebas.ColText(1) = ""
'            ssdgpruebas.ColText(2) = ""
'            ssdgpruebas.ColText(7) = "SP"
'            matrizgrid(7, ssdgpruebas.Row + 1) = ""
'            matrizgrid(3, ssdgpruebas.Row + 1) = ""
'            matrizgrid(4, ssdgpruebas.Row + 1) = ""
'            matrizgrid(2, ssdgpruebas.Row + 1) = "SP"
'        End If
'      End If
    Next i
'    For i = 0 To .SelCount - 1
'        .EvalRowIsSelected = 0
'    Next i
  End With
  
End Sub

Public Function Salir() As Boolean
  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion, Me.Caption) = vbCancel)
  objAutoAn.EscribirLog " Pulsado Salir"
  If Not Salir Then Descargar
End Function

Public Sub Descargar()
  End
End Sub

Public Sub Programar()
  blnOpened = Not blnOpened
  If blnOpened Then IniciarLista
'  With objAutoAn
'    blnOpened = .PortOpen
'    .pAbrirCerrarPuerto Not blnOpened
'    .CambiarButtonProgram Not blnOpened
'    .LabelVisible blnOpened
'  End With
End Sub

Public Sub Probar()
  HITACHI.Enabled = True
  objAutoAn.ColMuestras("ADD-1001").strestado = cteFUERADERANGOFIN
End Sub

Public Sub pCargaMuestra(objMuestra As clsMuestra)
Dim Canal$, i%, cPruebaAuto%
Dim objActuacion As clsActuacion
On Error GoTo AutoAnError
  
  Awk1.FS = ","
  Canal = "0000000000000000000000000000000000 "
  For Each objActuacion In objMuestra.ColMuestrasPruebas
    Awk1 = objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strcodactauto
    For i = 1 To Awk1.NF
      cPruebaAuto = CInt(Awk1.F(i))
      If cPruebaAuto < 33 Then
        Canal = Left$(Canal, cPruebaAuto - 1) & "1" & Mid$(Canal, cPruebaAuto + 1)
      End If
    Next i
  Next
  objMuestra.strProperty1 = Canal
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "Cargando Canal de Muestra" & objMuestra.strCodMuestra, "pCargarCanales")
End Sub

Public Sub pModificaMuestra(objMuestra As clsMuestra)
Dim Rs As rdoResultset
Dim SQL$, Qry As rdoQuery
Dim Canal$, i%, cPruebaAuto%
Dim objActuacion As clsActuacion
On Error GoTo AutoAnError
  
  Awk1.FS = ","
  With objMuestra
    SQL = "SELECT ComentariosExtraccion FROM MuestraAsistencia"
    SQL = SQL & " WHERE cMuestra =? "
    Set Qry = objAutoAn.rdoConnect.CreateQuery("", SQL)
    Qry(0) = .strCodMuestra
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not Rs.EOF Then
      If Not IsNull(Rs(0)) Then
        .strProperty3 = Rs(0)
      End If
    End If
    Canal = "0000000000000000000000000000000000 "
    For Each objActuacion In .ColMuestrasPruebas
      Awk1 = objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strcodactauto
      For i = 1 To Awk1.NF
        cPruebaAuto = CInt(Awk1.F(i))
        If cPruebaAuto < 33 Then
          Canal = Left$(Canal, cPruebaAuto - 1) & "1" & Mid$(Canal, cPruebaAuto + 1)
        End If
      Next i
    Next
    .strProperty1 = Canal
    If blnOpened Then .strestado = ctePREPROGRAMACION
  End With
  Exit Sub
    
AutoAnError:
  Call objError.InternalError(Me, "Cargando Canales de Pruebas", "pCargarCanales")
End Sub
