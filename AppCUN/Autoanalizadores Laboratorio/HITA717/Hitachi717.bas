Attribute VB_Name = "Mod717"
Option Explicit
Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    fCargarColumnasGrid
    If Not .fInicializar(App, cteHITACHI_717, , , , , True, , , , True, , , , , , , , , True) Then End
    .Show
    .Borrar_Logs
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  'columnas del grid
  With objAutoAn
    .AgregarColumnaAGridMuestras cteMuestraDISCO
    .AgregarColumnaAGridMuestras cteMuestraCOPA
    .AgregarColumnaAGridMuestras cteMuestraPROPERTY2, "Sec.", 700
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    '.AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
    '.AgregarColumnaAGridMuestras cteMuestraPROPERTY1, "CanalMuestra"
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
'    .AgregarColumnaAGridMuestras cteMuestraDILUCION, , , , Editable
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    .AgregarColumnaAGridMuestras cteMuestraPROPERTY3, "Indicaciones", 3000
    
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctePruebaNUMREPETICION
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
End Sub
