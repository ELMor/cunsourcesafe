VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pentra 120"
   ClientHeight    =   465
   ClientLeft      =   2865
   ClientTop       =   1515
   ClientWidth     =   1095
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Pentra120.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   465
   ScaleWidth      =   1095
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Lectura_Datos(datos As String)
Dim cMuestra As String
Dim contador As Integer
Dim res As String
  'Se sustituyen los caracteres con c�digo ASCII = 0,
  'que hacen que no funcione el awk1 por,espacios en blanco.
  For contador = 1 To Len(datos)
    If Asc(Mid$(datos, contador, 1)) = 0 Then
      datos = Left$(datos, contador - 1) & " " & Mid$(datos, contador + 1)
    End If
  Next contador
  
  'Se leen los tramos del mensaje
  Awk1 = datos
  Awk1.FS = Chr$(13)
  
  'El c�digo de muestra est� en el awk1.F(X) cuya primer caracter es una 'u' min�scula.
  For contador = 1 To Awk1.NF
    If Left$(Awk1.F(contador), 1) = "u" Then
      cMuestra = objAutoAn.fNumeroACodigo(Trim$(Mid$(Awk1.F(contador), 2)))
      If Not objAutoAn.blnExistMuestra(cMuestra) Then _
      Exit Sub Else Exit For
    End If
  Next contador
  
  'Los datos de resultados que nos interesan est�n entre el awk1.F(3) y el awk1.F(28)
  For contador = 3 To 28
    Call objAutoAn.pIntroResultConCResAuto(cMuestra, _
    Left$(Awk1.F(contador), 1), Mid$(Awk1.F(contador), 3, 5), True)
  Next contador
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Datos", "Datos :" & datos)
End Sub

Public Sub Lectura_Protocolo(Caracter As String)
Static Mensaje As String 'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
    'pudendo ser modificada en �stas. Al ser modificada la copia, tambi�n deber� modificarse el original.
    'El hecho de utilizar esta copia se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas
    'por estar declarada como Static en esta subrutina
  Mensaje = Mensaje & Caracter
  CopiaMensaje = Mensaje
  Select Case objAutoAn.Estado
    Case cteESPERA
      Vega_Espera CopiaMensaje
      Mensaje = CopiaMensaje
    Case cteTRANSMISION
      Vega_Transmision CopiaMensaje
      Mensaje = CopiaMensaje
  End Select
End Sub

Private Sub Vega_Espera(Mensaje As String)
'Identifica la llegada del caracter de comienzo de transmisi�n (comienzo del mensaje) y pasa
'al estado de transmisi�n.
  If Right$(Mensaje, 1) = Chr$(2) Then '<STX> - Llegada del car�cter de comienzo del mensaje
    objAutoAn.Estado = cteTRANSMISION
    Mensaje = Right$(Mensaje, 1)
  End If
End Sub

Private Sub Vega_Transmision(Mensaje As String)
'Identifica el final del mensaje y se vuelve al estado de espera.
  If Right$(Mensaje, 1) = Chr$(3) Then '<ETX> - Se ha identifcado el final de una transmisi�n
    Lectura_Datos Mensaje
    objAutoAn.Estado = cteESPERA
    Mensaje = ""
  End If
End Sub

Public Sub Descargar()
  End
End Sub

Public Sub probar()
'Static NVez%
'  NVez = NVez + 1
'  Select Case NVez
'    Case 1: objAutoAn.AutoRecepcion Chr$(2) & "00648" & vbCr & "� RESULT  " & vbCr & "! --.--  " & vbCr & _
'      Chr$(34) & " --.--  " & vbCr & "# --.--  " & vbCr & "$ --.--  " & vbCr & "% --.--  " & vbCr & "( --.--  " & vbCr & ") --.--  " & vbCr & "* --.--  " & vbCr & "+ --.--  " & vbCr & ", --.--  " & vbCr & "- --.--  " & vbCr & ". --.--  " & vbCr & "/ --.--  " & vbCr & "0 --.--  " & vbCr & "1 --.--  " & vbCr & "2 --.--  " & vbCr & "3 --.--  " & vbCr & "4 --.--  " & vbCr & "5 --.--  " & vbCr & "6 --.--  " & vbCr & "7 --.--  " & vbCr & "8 --.--  " & vbCr & "@ --.--  " & vbCr & "A --.--  " & vbCr & "B --.--  " & vbCr & "C --.--  " & vbCr & "P             " & vbCr & "Q                          " & vbCr & "R     " & vbCr & "S       " & vbCr & "T" & vbCr & "U" & vbCr & "V" & vbCr & "f               " & vbCr & "g       " & vbCr & "p 01" & vbCr & "q 04/10/99 10h56mn41s" & vbCr & "r 104DIF01        " & vbCr & "s 0067" & vbCr & "t R" & vbCr & "u 0007845944      " & vbCr & "v                               " & vbCr & "w         " & vbCr & "x    " & vbCr & "y" _
'      & vbCr & "z  " & vbCr & "{                " & vbCr & "|           " & vbCr & "}               " & vbCr & "~                                 " & vbCr & " est ndar        " & vbCr & "� B" & vbCr & "� 0" & vbCr & "� 1" & vbCr & "� msm" & vbCr & "� VEGA    " & vbCr & "� V2.5 " & vbCr & "� 69de" & vbCr & Chr$(3)
'    Case 2: objAutoAn.AutoRecepcion Chr$(2) & "00648" & vbCr & "� RESULT  " & vbCr & "! 07.99  " & _
'      vbCr & Chr$(34) & " 02.52  " & vbCr & "# 31.60  " & vbCr & "$ 00.37  " & vbCr & "% 04.60  " & vbCr & "( 04.88  " & vbCr & ") 61.10  " & vbCr & "* 00.13  " & vbCr & "+ 01.60  " & vbCr & ", 00.09  " & vbCr & "- 01.10  " & vbCr & ". 00.11  " & vbCr & "/ 01.36  " & vbCr & "0 00.03  " & vbCr & "1 00.43  " & vbCr & "2 04.43  " & vbCr & "3 12.71  " & vbCr & "4 40.06  " & vbCr & "5 90.47  " & vbCr & "6 28.70  " & vbCr & "7 31.72  " & vbCr & "8 10.88  " & vbCr & "@ 00252  " & vbCr & "A 08.96  " & vbCr & "B 0.226  " & vbCr & "C 16.00  " & vbCr & "P             " & vbCr & "Q                          " & vbCr & "R     " & vbCr & "S       " & vbCr & "T" & vbCr & "U" & vbCr & "V" & vbCr & "f               " & vbCr & "g       " & vbCr & "p 01" & vbCr & "q 04/10/99 10h57mn11s" & vbCr & "r 104DIF02        " & vbCr & "s 0068" & vbCr & "t R" & vbCr & "u 0007845965      " & vbCr & "v                               " & vbCr & "w         " & vbCr & "x    " & vbCr & "y" _
'      & vbCr & "z  " & vbCr & "{                " & vbCr & "|           " & vbCr & "}               " & vbCr & "~                                 " & vbCr & " est ndar        " & vbCr & "� B" & vbCr & "� 0" & vbCr & "� 1" & vbCr & "� msm" & vbCr & "� VEGA    " & vbCr & "� V2.5 " & vbCr & "� 6c37" & vbCr & Chr$(3)
'    Case 3: objAutoAn.AutoRecepcion Chr$(2) & "00653" & vbCr & "� RESULT  " & vbCr & "! 05.38  " & _
'      vbCr & Chr$(34) & " 01.12  " & vbCr & "# 20.90 l" & vbCr & "$ 00.34  " & vbCr & "% 06.40  " & vbCr & "( 03.73  " & vbCr & ") 69.30  " & vbCr & "* 00.13  " & vbCr & "+ 02.40  " & vbCr & ", 00.05  " & vbCr & "- 01.00  " & vbCr & ". 00.04  " & vbCr & "/ 00.83  " & vbCr & "0 00.03  " & vbCr & "1 00.48  " & vbCr & "2 04.26  " & vbCr & "3 12.38  " & vbCr & "4 38.93  " & vbCr & "5 91.34  " & vbCr & "6 29.06  " & vbCr & "7 31.81  " & vbCr & "8 11.30  " & vbCr & "@ 00218  " & vbCr & "A 07.81  " & vbCr & "B 0.170 l" & vbCr & "C 14.25  " & vbCr & "P             " & vbCr & "Q                          " & vbCr & "R     " & vbCr & "S       " & vbCr & "T LYM-" & vbCr & "U" & vbCr & "V" & vbCr & "f               " & vbCr & "g       " & vbCr & "p 01" & vbCr & "q 04/10/99 10h57mn41s" & vbCr & "r 104DIF01        " & vbCr & "s 0069" & vbCr & "t R" & vbCr & "u 0007845944      " & vbCr & "v                               " & vbCr & "w         " & vbCr & "x    " & vbCr _
'      & "y  " & vbCr & "z  " & vbCr & "{                " & vbCr & "|           " & vbCr & "}               " & vbCr & "~                                 " & vbCr & " est ndar        " & vbCr & "� B" & vbCr & "� 0" & vbCr & "� 2" & vbCr & "� msm" & vbCr & "� VEGA    " & vbCr & "� V2.5 " & vbCr & "� 6e00" & vbCr & Chr$(3)
'    Case 4: objAutoAn.AutoRecepcion Chr$(2) & "00653" & vbCr & "� RESULT  " & vbCr & "! 04.37  " & _
'      vbCr & Chr$(34) & " 01.69  " & vbCr & "# 38.60  " & vbCr & "$ 00.32  " & vbCr & "% 07.40  " & vbCr & "( 02.12  " & vbCr & ") 48.50 l" & vbCr & "* 00.18  " & vbCr & "+ 04.20  " & vbCr & ", 00.06  " & vbCr & "- 01.30  " & vbCr & ". 00.08  " & vbCr & "/ 01.75  " & vbCr & "0 00.03  " & vbCr & "1 00.71  " & vbCr & "2 05.21  " & vbCr & "3 14.46  " & vbCr & "4 43.91  " & vbCr & "5 84.24  " & vbCr & "6 27.75  " & vbCr & "7 32.94  " & vbCr & "8 11.41  " & vbCr & "@ 00207  " & vbCr & "A 08.06  " & vbCr & "B 0.167 l" & vbCr & "C 13.25  " & vbCr & "P             " & vbCr & "Q                          " & vbCr & "R     " & vbCr & "S       " & vbCr & "T NEU-" & vbCr & "U" & vbCr & "V" & vbCr & "f               " & vbCr & "g       " & vbCr & "p 01" & vbCr & "q 04/10/99 10h58mn11s" & vbCr & "r 104DIF03        " & vbCr & "s 0070" & vbCr & "t R" & vbCr & "u 0007845979      " & vbCr & "v                               " & vbCr & "w         " & vbCr & "x    " & vbCr _
'      & "y  " & vbCr & "z  " & vbCr & "{                " & vbCr & "|           " & vbCr & "}               " & vbCr & "~                                 " & vbCr & " est ndar        " & vbCr & "� B" & vbCr & "� 0" & vbCr & "� 1" & vbCr & "� msm" & vbCr & "� VEGA    " & vbCr & "� V2.5 " & vbCr & "� 6e0a" & vbCr & Chr$(3)
'  End Select
End Sub

