Attribute VB_Name = "modPentra120"
Option Explicit
Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    frmPrincipal.Caption = "Pentra 120"
    fCargarColumnasGrid 'Definicion de las columnas que componen los 2 Grid
    If Not .fInicializar(App, cteCOBAS_VEGA, , , , , , , , , , , , , , True) Then End
    .Borrar_Logs
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  With objAutoAn
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoESTADO
    .AgregarColumnaAGridPruebas cteResultadoREFMIN
    .AgregarColumnaAGridPruebas cteResultadoREFMAX
  End With
End Sub
