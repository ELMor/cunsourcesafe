Attribute VB_Name = "Hyrysmod"
Option Explicit

Public objAutoAn As New clsAutoAnalizador
Public objError As New clsErrores

Public Sub Main()

Screen.MousePointer = vbHourglass
With objAutoAn
  Set .frmFormulario = frmPrincipal
  fCargarColumnasGrid
  If Not .fInicializar(App, cteHYRYS) Then End
  fCargaPT
  .Borrar_Logs
  .Show
End With
Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
'Cargo las columnas correspondientes
With objAutoAn
  .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
  .AgregarColumnaAGridMuestras cteMuestraURGENTE
  .AgregarColumnaAGridMuestras cteMuestraPROPERTY1, "Prot.Tot.", 1350
  .AgregarColumnaAGridMuestras cteMuestraESTADO
  .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
  
  .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
  .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
  .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO, , 1000
  .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
  .AgregarColumnaAGridPruebas ctePruebaURGENTE
  .AgregarColumnaAGridPruebas cteResultadoESTADO
  .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
End With
End Sub

Private Sub fCargaPT()
'Carga las proteinas totales de la Base de Datos
Dim Sql As String
Dim Qry As rdoQuery
Dim Rs As rdoResultset
Dim objMuestra As clsMuestra

For Each objMuestra In objAutoAn.ColMuestras
   Sql = "SELECT" _
    & " RESULTADOASISTENCIA.RESULTADOALFANUMERICO" _
    & " FROM" _
    & " MUESTRAPRUEBA MUESTRAPRUEBA," _
    & " PRUEBAASISTENCIA PRUEBAASISTENCIA," _
    & " PRUEBAASISTENCIA PRUEBAASISTENCIA2," _
    & " RESULTADOASISTENCIA RESULTADOASISTENCIA" _
    & " WHERE" _
    & " MUESTRAPRUEBA.HISTORIA = PRUEBAASISTENCIA.HISTORIA AND" _
    & " MUESTRAPRUEBA.CASO = PRUEBAASISTENCIA.CASO AND" _
    & " MUESTRAPRUEBA.SECUENCIA = PRUEBAASISTENCIA.SECUENCIA AND" _
    & " PRUEBAASISTENCIA.HISTORIA = PRUEBAASISTENCIA2.HISTORIA AND" _
    & " PRUEBAASISTENCIA.CASO = PRUEBAASISTENCIA2.CASO AND" _
    & " PRUEBAASISTENCIA.NPETICION = PRUEBAASISTENCIA2.NPETICION AND" _
    & " PRUEBAASISTENCIA2.HISTORIA = RESULTADOASISTENCIA.HISTORIA AND" _
    & " PRUEBAASISTENCIA2.CASO = RESULTADOASISTENCIA.CASO AND" _
    & " PRUEBAASISTENCIA2.SECUENCIA = RESULTADOASISTENCIA.SECUENCIA AND" _
    & " PRUEBAASISTENCIA2.NREPETICION = RESULTADOASISTENCIA.NREPETICION AND" _
    & " PRUEBAASISTENCIA.CTIPOPRUEBA = 31 AND" _
    & " PRUEBAASISTENCIA2.CTIPOPRUEBA = 527 AND" _
    & " PRUEBAASISTENCIA2.ESTADO = 9 AND" _
    & " MUESTRAPRUEBA.CMUESTRA = ?" _
    & " ORDER BY" _
    & " MUESTRAPRUEBA.CMUESTRA ASC"
    Set Qry = objAutoAn.rdoConnect.CreateQuery("", Sql)
    Qry(0) = objMuestra.strCodMuestra
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not Rs.EOF Then objMuestra.strProperty1 = Rs("resultadoalfanumerico")
    Rs.Close
    Qry.Close
  Next
End Sub
