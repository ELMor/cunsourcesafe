Attribute VB_Name = "modACL7000"
Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
  With objAutoAn
    Set .frmFormulario = frmPrincipal: fCargarColumnasGrid
    If Not .fInicializar(App, cteACL7000, , , True) Then End
    .Borrar_Logs
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  'columnas del grid
  With objAutoAn
    .AgregarColumnaAGridMuestras cteMuestraCOPA
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION, , 1200
    .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO, , 1200
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO, , 1200
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
End Sub

