VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Electra 1600C"
   ClientHeight    =   6375
   ClientLeft      =   1050
   ClientTop       =   1455
   ClientWidth     =   6510
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Electra.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6375
   ScaleWidth      =   6510
   Begin VB.Timer TimerNuevasPruebas 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   2520
      Top             =   0
   End
   Begin VB.Timer TimerAUTO 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   2880
      Top             =   0
   End
   Begin VB.Timer TimerTrasMENSAJE 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   3300
      Top             =   0
   End
   Begin VB.Timer TimerEsperaREPLY 
      Enabled         =   0   'False
      Interval        =   15000
      Left            =   3720
      Top             =   0
   End
   Begin VB.CommandButton cmdSalir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2460
      TabIndex        =   1
      Top             =   5520
      Width           =   1335
   End
   Begin MSCommLib.MSComm Comm1 
      Left            =   5940
      Top             =   -60
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      DTREnable       =   -1  'True
      Handshaking     =   1
      InBufferSize    =   10240
      InputLen        =   1
      ParityReplace   =   0
      RThreshold      =   1
      RTSEnable       =   -1  'True
   End
   Begin SSDataWidgets_B.SSDBGrid ssdgPruebas 
      Height          =   4875
      Left            =   240
      TabIndex        =   2
      Top             =   540
      Width           =   6015
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   6
      AllowUpdate     =   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   1085
      Columns(0).Caption=   "Copa"
      Columns(0).Name =   "Copa"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1720
      Columns(1).Caption=   "Muestra"
      Columns(1).Name =   "Muestra"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2196
      Columns(2).Caption=   "ID Muestra"
      Columns(2).Name =   "ID Muestra"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Prueba"
      Columns(3).Name =   "Prueba"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1005
      Columns(4).Caption=   "Urg."
      Columns(4).Name =   "Urg."
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   1349
      Columns(5).Caption=   "Estado"
      Columns(5).Name =   "Estado"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   10610
      _ExtentY        =   8599
      _StockProps     =   79
      Caption         =   "Orden de an�lisis de las pruebas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSPanel SSPanel3 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   6000
      Width           =   6510
      _Version        =   65536
      _ExtentX        =   11483
      _ExtentY        =   661
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   3
      BevelOuter      =   1
      Begin Threed.SSPanel panEstado 
         Height          =   375
         Left            =   0
         TabIndex        =   4
         Top             =   0
         Width           =   3150
         _Version        =   65536
         _ExtentX        =   5556
         _ExtentY        =   661
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelWidth      =   3
         BevelOuter      =   1
      End
      Begin Threed.SSPanel panPuerto 
         Height          =   375
         Left            =   3120
         TabIndex        =   5
         Top             =   0
         Width           =   3390
         _Version        =   65536
         _ExtentX        =   5980
         _ExtentY        =   661
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelWidth      =   3
         BevelOuter      =   1
      End
   End
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   5040
      Top             =   120
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk3 
      Left            =   5520
      Top             =   120
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   4560
      Top             =   120
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Label lblListaTrabajo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Lista de trabajo n�:  "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   4335
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
'                           ***** NOTAS DE INTER�S GENERAL PARA EL FUNCIONAMIENTO *****

'Por el momento no se considera la posibilidad de que se llene el buffer de recepci�n _
    de datos y se produzca un error RXOVER.
'Si esto pudiera llegar a ocurrir la soluci�n ser� la siguiente:
'   1 - Aumentar el tama�o del buffer de recepci�n
'   2 - Enviar Xoff cuando el buffer est� pr�ximo a llenarse
Dim objAutoAn As clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Dim arActuaciones() As typeActuacion    'Array de estructuras con datos de la Actuaci�n
Dim arResultados() As typeResultados    'Array de estructuras con datos de los Resultados
Dim arResultAuto() As typeResultAuto   'Array de estructuras con datos de los Resultados _
                                        definidos en el autoanalizador
Dim estado As Integer                   'Estado del sistema
Dim res As Boolean
Dim strLog As String
Dim strArchivoLog As String

Dim blnAutoEsperaRespuesta As Integer   'Indica si el autoanalizador espera un mensaje de respuesta
Dim blnUltimoRepetido As Integer      'Indica si es el �ltimo frame a enviar
Dim intNumFrameTrans As Integer    'Indica el n�mero de frame que hay que trasmitir
Dim blnFrameRepetido As Integer    'Indica si el frame es repetido
Dim arFrames() As String          'Contiene los frames que forman el mensaje a transmitir completo
Dim strFrameTotal As String        'Recoge los distintos frames libres de caracteres de protocolo
Dim strCodMuestraQuery As String     'C�digo de la muestra enviado por el Electra en un Query
Dim strRackQuery As String         'Posicion de la muestra enviada por el Electra en un Query

'Constantes de urgencias
Const constURGENCIA = 1
Const constRUTINA = 0


Private Sub pActualizarDB(strIDMuestra As String, strCodActAuto As String)
'Se actualiza la base de datos con los resultados almacenados en la matriz de resultados.
'La base de datos se actualiza por cada muestra-prueba que se analiza.
    Dim contador1 As Integer
    Dim contador2 As Integer
    Dim contador3 As Integer

    'Se establece la posici�n de la matriz de pruebas a la que corresponden los resultados.
    For contador3 = 1 To UBound(arActuaciones)
        If arActuaciones(contador3).strIDMuestra = strIDMuestra And arActuaciones(contador3).strCodActAuto = strCodActAuto Then
            Exit For
        End If
    Next contador3
    If contador3 > UBound(arActuaciones) Then Exit Sub
    
    'Se recorre la matriz que contiene los resultados.
    For contador1 = 1 To UBound(arResultados)
        'Se localizan los resultados que tiene definidos la actuaci�n.
        For contador2 = 1 To UBound(arResultAuto)
            If strCodActAuto = arResultAuto(contador2).strCodActAuto _
            And arResultados(contador1).strCodResultAuto = arResultAuto(contador2).strCodResultAuto Then
                Exit For
            End If
        Next contador2
        
        'Se inserta el resultado
        If contador2 <= UBound(arResultAuto) Then
            'Se actualiza la tabla resultadoAsistencia.
            res = objAutoAn.fActualizarResultadoAsistencia(arActuaciones(contador3).strNumAct, _
                arActuaciones(contador3).intNumRepet, arResultAuto(contador2).intCodResult, _
                arResultados(contador1).strResultado, arResultAuto(contador2).intCodUnidad)
        End If
    Next contador1
    
    'Se actualiza la tabla seguimientoPrueba.
    res = objAutoAn.fActualizarSeguimientoPrueba(arActuaciones(contador3).strNumAct, _
        arActuaciones(contador3).intNumRepet)
        
    'Se cambia el estado de la prueba en la tabla pruebaAsistencia.
    res = objAutoAn.fActualizarPruebaAsistencia(arActuaciones(contador3).strNumAct, _
        arActuaciones(contador3).intNumRepet)
    
    'Se actualiza el grid.
    arActuaciones(contador3).strEstado = objAutoAn.constESTADOPRAUTOREALIZADA
    ssdgPruebas.MoveFirst
    ssdgPruebas.MoveRecords contador3 - 1
    ssdgPruebas.Columns("Estado").Text = objAutoAn.constESTADOPRAUTOREALIZADA

    'Se cambia el estado de la lista de trabajo en la tabla listaRealizacion. _
    (Solo har�a falta cambiarlo una �nica vez al recibir alg�n resultado)
    res = objAutoAn.fActualizarListaRealizacion()
    
    'Se mira si es el �ltimo resultado esperado
    For contador1 = 1 To UBound(arActuaciones)
        If arActuaciones(contador1).strEstado <> objAutoAn.constESTADOPRAUTOREALIZADA Then
            Exit For
        End If
    Next contador1
    If contador1 > UBound(arActuaciones) Then
        estado = objAutoAn.constESTADOSISTESPERA
        panEstado.Caption = objAutoAn.fLeerEstado(estado)
        cmdSalir_Click
    End If

End Sub

Private Sub cmdSalir_Click()
    Dim contador As Integer
    Dim resp As Integer
    
    strLog = strLog & "Pulsado salir" & Chr$(13) & Chr$(10)

    'Se comprueba si existe alguna prueba que no tiene resultados y en ese caso se _
    cambia su estado a Prueba Extra�da.
    For contador = 1 To UBound(arActuaciones)
        If arActuaciones(contador).strEstado <> objAutoAn.constESTADOPRAUTOREALIZADA Then
            resp = objAutoAn.fPreguntarVolverPruebasAExtraidas()
            If resp = vbCancel Then Exit Sub
            Exit For
        End If
    Next contador
    
    'El form_unload s�lo se ejecuta si no se ha cancelado
    Form_Unload (False)

End Sub

Private Sub Comm1_OnComm()
    Dim ERMsg$
    Dim caracter As String * 1
    Dim strCharLog As String 'para el Log

    'Se selecciona el evento que ocurre en el puerto
    Select Case Comm1.CommEvent
        'Si se produce una entrada se procede a su lectura
        'Se lee cada caracter por separado
        Case comEvReceive
            Comm1.RThreshold = 0
            Do While Comm1.InBufferCount
                DoEvents
                caracter = Comm1.Input
                strCharLog = caracter
                Select Case Asc(strCharLog)
                Case 1
                    strCharLog = "<SOH>"
                Case 2
                    strCharLog = "<STX>"
                Case 3
                    strCharLog = "<ETX>"
                Case 4
                    strCharLog = "<EOT>"
                Case 5
                    strCharLog = "<ENQ>"
                Case 6
                    strCharLog = "<ACK>"
                Case 10
                    strCharLog = "<LF>"
                Case 13
                    strCharLog = "<CR>"
                Case 16
                    strCharLog = "<DLE>"
                Case 17
                    strCharLog = "<XON>"
                Case 19
                    strCharLog = "<XOFF>"
                Case 21
                    strCharLog = "<NAK>"
                Case 23
                    strCharLog = "<ETB>"
                End Select
                strLog = strLog & strCharLog
                Call pLecturaProtocolo(caracter)
            Loop
            Comm1.RThreshold = 1
        Case comEventRxOver
            ERMsg$ = "Buffer de entada de datos lleno."
        Case comEventTxFull
            ERMsg$ = "Buffer de salida de datos lleno."
    End Select
    
    If Len(ERMsg$) Then
        MsgBox ERMsg$, vbExclamation, Me.Caption
        ERMsg$ = ""
    End If

End Sub

Private Sub Electra_Confirmacion(Mensaje As String)
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo _
    (si le ha llegado con errores).Si el mensaje es rechazado habr� que volver a enviarlo.
    Dim contador1 As Integer


    On Error Resume Next
        
    TimerEsperaREPLY.Enabled = False

    If Right$(Mensaje, 1) = Chr$(6) Or Right$(Mensaje, 1) = Chr$(4) Then '<ACK> o <EOT> - El autoanalizador ha aceptado el bloque
        '***** HABR�A QUE ESTUDIAR M�S EN PROFUNDIDAD QUE OCURRE EN EL CASO DE QUE EL   *****
        '***** AUTOANALIZADOR RESPONDA CON UN <EOT>                                     *****
        blnFrameRepetido = False
        Mensaje = ""
        If blnUltimoRepetido = True Then
            Comm1.Output = Chr$(4) '<EOT> - Se cierra la comunicaci�n
            blnAutoEsperaRespuesta = False
            estado = objAutoAn.constESTADOSISTESPERA
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
            panEstado.Caption = objAutoAn.fLeerEstado(estado)
            'Se actualiza el grid
            For contador1 = 1 To UBound(arActuaciones)
                If arActuaciones(contador1).strEstado = objAutoAn.constESTADOPRAUTOINTENTOPROGRAMAR Then
                    arActuaciones(contador1).strEstado = objAutoAn.constESTADOPRAUTOACEPTADA
                    ssdgPruebas.MoveFirst
                    ssdgPruebas.MoveRecords contador1 - 1
                    ssdgPruebas.Columns("Estado").Text = arActuaciones(contador1).strEstado
                End If
            Next contador1
        Else
            estado = objAutoAn.constESTADOSISTMASTER
            panEstado.Caption = objAutoAn.fLeerEstado(estado)
            strLog = strLog & " intNumFrameTrans= " & intNumFrameTrans & Chr$(13) & Chr$(10)
            intNumFrameTrans = intNumFrameTrans + 1
            Call pTransmisionFrames
        End If
        Exit Sub
    End If
    
    If Right$(Mensaje, 1) = Chr$(21) Then  '<NAK> - El autoanalizador no ha aceptado el bloque
        blnFrameRepetido = True
        estado = objAutoAn.constESTADOSISTMASTER
        panEstado.Caption = objAutoAn.fLeerEstado(estado)
        Mensaje = ""
        Call pEnvioMensajes  'Se llama a esta subrutina y no directamene a pTransmisionFrames para
                        'poder controlar el n�mero de repeticiones que se hacen
    End If

End Sub

Private Sub Electra_Esclavo(Mensaje As String)
'Identifica el final del mensaje que se est� recibiendo y procede a su lectura si es le caso.
'El mensaje puede ser enviado en uno o varios frames dependiendo de su longitud
    Static intNumFrame As Integer 'Guarda el anterior n�mero de frame
    Dim intActualNumFrame As Integer 'Recoge el actal n�mero de frame
    Dim strFrame As String 'variable de paso de Mensaje a strFrameTotal
    Dim filenum As Integer
    
    TimerAUTO.Enabled = False 'Al llegar datos del autoanalizador, se desactiva el Timer

    If Right$(Mensaje, 1) = Chr$(4) Then '<EOT> - Final de la transmisi�n por parte del autoanalizador
        Mensaje = ""
        estado = objAutoAn.constESTADOSISTESPERA
        panEstado.Caption = objAutoAn.fLeerEstado(estado)
        strLog = strLog & "ESTADO ESPERA" & Chr$(13) & Chr$(10)
        strLog = strLog & "Fin de Transmision" & Chr$(13) & Chr$(10)
        'Si el autoanalizador est� esperando un respuesta se contesta al Query
        If blnAutoEsperaRespuesta = True Then
            Call pEnvioMensajes 'Se env�a una nueva prueba a programar
        End If
        Exit Sub
    End If

    If Right$(Mensaje, 2) = Chr$(13) & Chr$(10) Then '<CR><LF> - Fin del frame.
        'Se elimina el comienzo del mensaje hasta el <STX> inclusive
        strLog = strLog & "Fin de Frame" & Chr$(13) & Chr$(10)
        strFrame = Mid$(Mensaje, InStr(1, Mensaje, Chr$(2)) + 1)
        Mensaje = ""
        'Se lee el n�mero de Frame
        intActualNumFrame = Val(Left$(strFrame, 1))
        'Se comprueba el n�mero frame
        Select Case intActualNumFrame
        Case intNumFrame      'Frame repetido
        Case intNumFrame + 1  'Frame correcto
            Comm1.Output = Chr$(6) '<ACK> - Respuesta afirmativa
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <ACK>" & Chr$(13) & Chr$(10)
            'Se comprueba si es el �ltimo frame
            If InStr(1, strFrame, Chr$(3)) Then
                'Se actualiza el n�mero del frame
                intNumFrame = 0
                'Se elimina el n�mero de frame y el final del mensaje desde <ETX>
                strFrame = Mid$(strFrame, 2, InStr(1, strFrame, Chr$(3)) - 2)
                strFrameTotal = strFrameTotal & strFrame
                Call pLecturaDatos(strFrameTotal)
                strFrameTotal = ""
            ElseIf InStr(1, strFrame, Chr$(23)) Then
                'Se actualiza el n�mero del frame
                Select Case intActualNumFrame
                Case 7
                    intNumFrame = -1
                Case Else
                    intNumFrame = intActualNumFrame
                End Select
                'Se elimina el n�mero de frame y el final del mensaje desde <ETB>
                strFrame = Mid$(strFrame, 2, InStr(1, strFrame, Chr$(23)) - 2)
                strFrameTotal = strFrameTotal & strFrame
            End If
        Case Else          'Frame incorrecto
            Comm1.Output = Chr$(21) '<NAK> - Respuesta negativa
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <NAK>" & Chr$(13) & Chr$(10)
        End Select
        TimerAUTO.Enabled = True

        filenum = FreeFile
        Open strArchivoLog For Append As #filenum
        Print #filenum, strLog
        Close #filenum
        strLog = ""

        Exit Sub
    End If

End Sub

Private Sub Electra_Espera(Mensaje As String)
'Identifica la llegada de los caracteres de solicitud de comunicaci�n, la acepta y pasa _
    al estado de Esclavo.
    
    If Right$(Mensaje, 1) = Chr$(5) Then '<ENQ> - Solicitud de comunicaci�n por parte del autoanalizador
        Comm1.Output = Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
        estado = objAutoAn.constESTADOSISTESCLAVO
        strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <ACK> ESTADO ESCLAVO" & Chr$(13) & Chr$(10)
        panEstado.Caption = objAutoAn.fLeerEstado(estado)
        Me.Refresh
        Mensaje = ""
        TimerAUTO.Enabled = True
    End If

End Sub

Private Sub Electra_Solicitud(Mensaje As String)
'Respuesta del autoanalizador a la solicitud para establecer una comunicaci�n.
    TimerEsperaREPLY.Enabled = False

    If Right$(Mensaje, 1) = Chr$(6) Then '<ACK> - El autoanalizador ha aceptado la comunicaci�n
        estado = objAutoAn.constESTADOSISTMASTER 'El sistema se convierte en master
        panEstado.Caption = objAutoAn.fLeerEstado(estado)
        strLog = strLog & "ESTADO MASTER"
        Mensaje = ""
        Call pEnvioMensajes 'Se env�a el bloque de datos por parte del sistema al autoanalizador
        Exit Sub
    End If
    
    If Right$(Mensaje, 1) = Chr$(21) Then '<NAK> - El autoanalizador no ha aceptado la comunicaci�n
        estado = objAutoAn.constESTADOSISTESPERA 'El sistema vuelve a estado de espera
        panEstado.Caption = objAutoAn.fLeerEstado(estado)
        strLog = strLog & "ESTADO ESPERA"
        Mensaje = ""
        TimerTrasMENSAJE.Enabled = True 'Se espera 10 segundos para intentar una nueva comunicaci�n
        Exit Sub
    End If
    
    If Right$(Mensaje, 1) = Chr$(5) Then '<ENQ> - El autoanalizador ha "solicitado l�nea" a la vez que el sistema
        estado = objAutoAn.constESTADOSISTESPERA 'El sistema da paso a la preferencia del autoanalizador
        panEstado.Caption = objAutoAn.fLeerEstado(estado) & "  (P)"
        strLog = strLog & "ESTADO ESPERA"
        Mensaje = ""
    End If

End Sub

Private Sub pEnvioMensajes()
'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es env�ar _
    el caracter <ENQ> y esperar a que el autoanalizador responda afirmativamente. Luego _
    se manda el mensaje propiamente dicho hasta que sea recibido correctamente por el _
    autoanalizador. Finalmente se cierra la comunicaci�n enviando <EOT>.
    Static intContIntentosComunic As Integer 'cuenta el n� de veces que se intenta comunicar con el autoanalizador
    Static intContIntentosTrans As Integer 'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
    Dim T As Long
    Dim res As Integer
    Dim msg As String
    Dim contador1 As Integer
    
    On Error Resume Next

    Select Case estado
    Case objAutoAn.constESTADOSISTESPERA 'Se intenta establecer la comunicaci�n
        intContIntentosTrans = 0 'Se inicializa el intContIntentosTrans por si se realiza una nueva comunicaci�n
        intContIntentosComunic = intContIntentosComunic + 1
        If intContIntentosComunic = 7 Then 'Fallo en la comunicaci�n despu�s de 6 intentos
            intContIntentosComunic = 0
            msg = "No se puede establecer comunicaci�n."
            Select Case MsgBox(msg, vbExclamation + vbRetryCancel, Me.Caption)
            Case vbRetry
                T = Timer + 5
                Do While T > Timer
                    res = DoEvents()
                Loop
                Sistema_Espera
                intContIntentosComunic = intContIntentosComunic + 1
                panEstado.Caption = objAutoAn.fLeerEstado(estado) & "  " & intContIntentosComunic
                Exit Sub
            Case vbCancel
                Comm1.Output = Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
                estado = objAutoAn.constESTADOSISTESPERA 'Se pasa al estado de espera
                strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
                panEstado.Caption = objAutoAn.fLeerEstado(estado)
                Exit Sub
            End Select
        Else
            Sistema_Espera
            panEstado.Caption = objAutoAn.fLeerEstado(estado) & "  " & intContIntentosComunic
            Exit Sub
        End If

    Case objAutoAn.constESTADOSISTMASTER 'Se intenta transmitir los datos
        intContIntentosComunic = 0 'Se inicializa el intContIntentosComunic por si se realiza una nueva comunicaci�n
        intContIntentosTrans = intContIntentosTrans + 1
        If intContIntentosTrans = 7 Then 'Fallo en la transmisi�n despu�s de 6 intentos
            intContIntentosTrans = 0
            estado = objAutoAn.constESTADOSISTESPERA
            panEstado.Caption = objAutoAn.fLeerEstado(estado)
            strLog = strLog & "ESTADO ESPERA"
            msg = "No se puede transmitir informaci�n."
            Select Case MsgBox(msg, vbExclamation + vbRetryCancel, Me.Caption)
            Case vbRetry
                T = Timer + 5
                Do While T > Timer
                    res = DoEvents()
                Loop
                Sistema_Espera
                intContIntentosComunic = intContIntentosComunic + 1
                panEstado.Caption = objAutoAn.fLeerEstado(estado) & "  " & intContIntentosComunic
                Exit Sub
            Case vbCancel
                Comm1.Output = Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
                strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> " & Chr$(13) & Chr$(10)
                'Se actualiza el grid
                For contador1 = 1 To UBound(arActuaciones)
                    If arActuaciones(contador1).strEstado = objAutoAn.constESTADOPRAUTOINTENTOPROGRAMAR Then
                        arActuaciones(contador1).strEstado = objAutoAn.constESTADOPRAUTOSINPROGRAMAR
                        arActuaciones(contador1).strPosicion = ""
                        ssdgPruebas.MoveFirst
                        ssdgPruebas.MoveRecords contador1 - 1
                        ssdgPruebas.Columns("Copa").Text = ""
                        ssdgPruebas.Columns("Estado").Text = arActuaciones(contador1).strEstado
                    End If
                Next contador1
                
                estado = objAutoAn.constESTADOSISTESPERA 'Se pasa al estado de espera
                panEstado.Caption = objAutoAn.fLeerEstado(estado)
                strLog = strLog & "ESTADO ESPERA"
                Exit Sub
            End Select
        Else
            If blnFrameRepetido = False Then
                Sistema_Master
            Else
                Call pTransmisionFrames
            End If
            panEstado.Caption = "CONFIRMACI�N " & intContIntentosTrans
            Exit Sub
        End If
    End Select

End Sub

Private Sub pEstablecerCheck(Mensaje As String)
'Subrutina que establece el check del mensaje que ser� enviado al autoanalizador.
    Dim contador As Integer     'cuenta el n�mero de caracteres le�dos
    Dim suma As Integer         'suma de los c�digoas ASCII, m�dulo 256, de los caracteres del bloque en decimal

    suma = 0
    For contador = 1 To Len(Mensaje)
        suma = suma + Asc(Mid$(Mensaje, contador, 1))
        If suma >= 256 Then
            suma = suma - 256
        End If
    Next contador
    If Len(Hex$(suma)) = 1 Then
        Mensaje = Mensaje & "0" & Hex$(suma)
    Else
        Mensaje = Mensaje & Hex$(suma)
    End If

End Sub



Private Sub Form_Load()
    Dim filenum As Integer
    
    'Si hay una instancia previa abierta, se finaliza la ejecuci�n
    If App.PrevInstance Then End

    Screen.MousePointer = vbHourglass
    
    'Posicionamiento de pantalla
    Call Move(0, (Screen.Height - Me.Height) / 2)
    
    App.HelpFile = "f:\laborat\ayuda\" & App.EXEName & ".hlp"
    
    'Se define una instancia del objeto clsAutoAnalizador
    Set objAutoAn = New clsAutoAnalizador
    objAutoAn.intCodAutoAn = objAutoAn.constAUTOELECTRA
    If objAutoAn.fInicializar(Comm1) = False Then End
    panPuerto.Caption = Comm1.Tag
    
    'LOG
    strArchivoLog = "c:\log\electra" & Format(Now, "yymmdd") & ".log"
    filenum = FreeFile
    Open strArchivoLog For Append As #filenum
    Print #filenum, Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10) & Format(Now, "dd/mm/yy hh:mm:ss") & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
    Close #filenum

    'Se carga la lista de trabajo
    Call pListaTrabajo

    'Se activa el Timer que busca las nuevas pruebas a a�dir a la lista
    TimerNuevasPruebas.Enabled = True

    Screen.MousePointer = vbDefault

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Dim filenum As Integer

    If Comm1.PortOpen = True Then
        Comm1.PortOpen = False
    End If
    DoEvents
    
    filenum = FreeFile
    Open strArchivoLog For Append As #filenum
    Print #filenum, strLog & Chr$(13) & Chr$(10) & "Fin de Ejecucion"
    Close #filenum

    End

End Sub

Private Sub pLecturaDatos(datos As String)
'Se leen los mensajes que env�a el autoanalizador. Solo se tendr�n en cuenta los mensajes _
    Query y los de Resultados.
    Dim contador1 As Integer
    Dim resultado As String
    Dim i As Integer
    Dim filenum As Integer
    Dim strIDMuestra As String
    Dim strCodActAuto As String
    Dim dimension As Integer
    
    'Se separan los distintos tipos de registros del mensaje
    Awk1 = datos
    Awk1.Fs = Chr$(13)
    strLog = strLog & "Awk1=Datos=" & datos & Chr$(13) & Chr$(10)
    'El awk1.F(1) contiene la cabecera (registro tipo H) y el awk1.F(awk1.NF) el terminador
    '(registro tipo L) que no sirven para nada.
    'El awk1.F(2) nos indica que tipo de mensaje es: registro tipo P (resultados) o registro tipo
    'Q (query)
    Select Case UCase$(Left$(Awk1.F(2), 1))
    Case "P"
        blnAutoEsperaRespuesta = False
        Awk2.Fs = "|"
        
        'Se obtiene la ID de la muestra
        Awk2 = Awk1.F(2)
        strIDMuestra = Awk2.F(3)
        If Len(strIDMuestra) = 8 And Val(strIDMuestra) = 0 Then
            strIDMuestra = objAutoAn.fCodigoANumero(strIDMuestra)
        End If
        
        'Se obtiene el c�digo de la prueba
        Awk2 = Awk1.F(3)
        Awk3.Fs = "^"
        Awk3 = Awk2.F(5)
        strCodActAuto = Awk3.F(4)
        
        'Se recogen los resultados. Los comentarios no se tienen en cuenta por ahora
        Erase arResultados
        dimension = 0
        For contador1 = 4 To Awk1.Nf
            If UCase$(Left$((Awk1.F(contador1)), 1)) = "R" Then
                Awk2 = Awk1.F(contador1)
                If Awk2.F(4) <> "" Then 'si el resultado no es null
                    Awk3.Fs = "^"
                    Awk3 = Awk2.F(3)
                    dimension = dimension + 1
                    ReDim Preserve arResultados(1 To dimension)
                    arResultados(dimension).strCodResultAuto = Awk3.F(5) 'awk2.F(2)   'codigo
                    resultado = Awk2.F(4)
                    i = objAutoAn.fFormatearNumero(resultado, ".", ",")
                    arResultados(dimension).strResultado = resultado   'resultado
                End If
            End If
        Next contador1
        
        'Se actualiza la base de datos
        Call pActualizarDB(strIDMuestra, strCodActAuto)
        Erase arResultados
        
    Case "Q"
        Awk2.Fs = "|"
        Awk2 = Awk1.F(2)
        
        'Solo nos interesa el c�digo de la muestra y la posicion de esta en el rack
        Awk3.Fs = "^"
        Awk3 = Awk2.F(3)
        
        'Se guarda el c�digo de la muestra en formato enviado por el autoanalizador
        strCodMuestraQuery = Awk3.F(1)
        strRackQuery = Awk3.F(2)
        
        'El autoanalizador estar� esperando una respuesta a su Query
        blnAutoEsperaRespuesta = True
    End Select

    filenum = FreeFile
    Open strArchivoLog For Append As #filenum
    Print #filenum, strLog
    Close #filenum
    strLog = ""

End Sub

Private Sub pLecturaProtocolo(caracter As String)
    Static Mensaje As String   'Contiene el mensaje que env�a el autoanalizador
    Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es _
    la que se env�a a otras subrutinas pudiendo ser modificada en �stas. Al ser modificada _
    la copia, tambi�n deber� modificarse el original. El hecho de utilizar esta copia _
    se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas _
    por estar declarada como Static en esta subrutina
    
    'Se va componiendo el mensaje
    Mensaje = Mensaje & caracter
    CopiaMensaje = Mensaje
    
    'Se acude a la subrutina correspondiente para la lectura del mensaje seg�n el estado.
    Select Case estado
    Case objAutoAn.constESTADOSISTESPERA
        Electra_Espera CopiaMensaje
        Mensaje = CopiaMensaje
    Case objAutoAn.constESTADOSISTSOLICITUD
        Electra_Solicitud CopiaMensaje
        Mensaje = CopiaMensaje
    Case objAutoAn.constESTADOSISTESCLAVO
        Electra_Esclavo CopiaMensaje
        Mensaje = CopiaMensaje
    Case objAutoAn.constESTADOSISTCONFIRMACION
        Electra_Confirmacion CopiaMensaje
        Mensaje = CopiaMensaje
    End Select

End Sub

Private Sub pListaTrabajo()
'Se llena una matriz que contiene una especie de copia de la lista de trabajo que ha _
    sido entregada a la enfermera para poder programar las pruebas a realizar en el _
    autoanalizador a partir de ella.
    Dim rs As rdoResultset, qry As rdoQuery
    Dim rs1 As rdoResultset, qry1 As rdoQuery
    Dim SQL As String, strItem As String
    Dim dimension As Integer
    Dim strIDMuestra As String
    
    'Se establecen los registros de la matriz de pruebas.
    SQL = "SELECT mP.cMuestra, mP.PR04numActPlan, pA.nRepeticion, PR0400.PR01codActuacion,"
    SQL = SQL & " PR0100.PR01desCorta, pAuto.cPruebaAuto, nvl(pA.urgenciaRealizacion,'0') urgencia"
    SQL = SQL & " FROM muestraPrueba mP, pruebaAsistencia pA, muestraAsistencia mA,"
    SQL = SQL & " PR0400, PR0100, pruebasAutoanalizador pAuto"
    SQL = SQL & " WHERE pA.PR04numActPlan = mP.PR04numActPlan"
    SQL = SQL & " AND pA.cListaRealizacion = ?"
    SQL = SQL & " AND pA.estado = ?"
    SQL = SQL & " AND (pA.demorada = 0 OR pA.demorada  IS NULL)"
    SQL = SQL & " AND mA.cMuestra = mP.cMuestra"
    SQL = SQL & " AND mA.estado = ?"
    SQL = SQL & " AND PR0400.PR04numActPlan = pA.PR04numActPlan"
    SQL = SQL & " AND PR0100.PR01codActuacion = PR0400.PR01codActuacion"
    SQL = SQL & " AND pAuto.PR01codActuacion = PR0400.PR01codActuacion"
    SQL = SQL & " AND pAuto.cAutoAnalizador = ?"
    SQL = SQL & " ORDER BY mP.cMuestra"
    Set qry = objAutoAn.rdoConnect.CreateQuery("", SQL)
    qry(0) = objAutoAn.strNumLista
    qry(1) = objAutoAn.constPRUEBASOLICITUDREALIZ
    qry(2) = objAutoAn.constMUESTRAEXTRAIDA
    qry(3) = objAutoAn.intCodAutoAn
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        'se anotan los datos en el array de actuaciones
        dimension = 0
        Do While Not rs.EOF
            dimension = dimension + 1
            ReDim Preserve arActuaciones(1 To dimension)
            arActuaciones(dimension).strCodMuestra = rs!cMuestra
            strIDMuestra = objAutoAn.fCodigoANumero(arActuaciones(dimension).strCodMuestra)
            strIDMuestra = String(10 - Len(strIDMuestra), "0") & strIDMuestra
            arActuaciones(dimension).strIDMuestra = strIDMuestra
            arActuaciones(dimension).strNumAct = rs!PR04numActPlan
            arActuaciones(dimension).intNumRepet = rs!nRepeticion
            arActuaciones(dimension).strCodAct = rs!PR01codActuacion
            arActuaciones(dimension).strCodActAuto = rs!cPruebaAuto
            arActuaciones(dimension).strEstado = objAutoAn.constESTADOPRAUTOSINPROGRAMAR
            If rs!urgencia = constURGENCIA Then
                arActuaciones(dimension).blnUrgente = True
            Else
                arActuaciones(dimension).blnUrgente = False
            End If
            'se muestran los datos en el grid
            strItem = arActuaciones(dimension).strPosicion & Chr$(9) 'posicion
            strItem = strItem & arActuaciones(dimension).strCodMuestra & Chr$(9) 'muestra
            strItem = strItem & arActuaciones(dimension).strIDMuestra & Chr$(9) 'ID muestra
            strItem = strItem & rs!PR01desCorta & Chr$(9) 'designacion de la prueba
            If arActuaciones(dimension).blnUrgente = True Then 'urgente
                strItem = strItem & "SI" & Chr$(9)
            Else
                strItem = strItem & "NO" & Chr$(9)
            End If
            strItem = strItem & arActuaciones(dimension).strEstado 'estado
            ssdgPruebas.AddItem strItem
            rs.MoveNext
        Loop
        
        'se llena el array que contiene los resultados definidos para el autoanalizador
        SQL = "SELECT rA.PR01codActuacion, rA.cResultado, rA.cResultadoAuto,"
        SQL = SQL & " pA.cPruebaAuto, pR.cUnidadConvencional"
        SQL = SQL & " FROM resultadosAutoanalizador rA, pruebasAutoanalizador pA, pruebasResultados pR"
        SQL = SQL & " WHERE rA.cAutoAnalizador = ?"
        SQL = SQL & " AND pA.cAutoAnalizador = rA.cAutoAnalizador"
        SQL = SQL & " AND pA.PR01codActuacion = rA.PR01codActuacion"
        SQL = SQL & " AND pR.PR01codActuacion = rA.PR01codActuacion"
        SQL = SQL & " AND pR.cResultado = rA.cResultado"
        Set qry1 = objAutoAn.rdoConnect.CreateQuery("", SQL)
        qry1(0) = objAutoAn.intCodAutoAn
        Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        dimension = 0
        Do While Not rs1.EOF
            dimension = dimension + 1
            ReDim Preserve arResultAuto(1 To dimension)
            arResultAuto(dimension).strCodAct = rs1!PR01codActuacion
            arResultAuto(dimension).strCodActAuto = rs1!cPruebaAuto
            arResultAuto(dimension).intCodResult = rs1!cResultado
            arResultAuto(dimension).strCodResultAuto = rs1!cResultadoAuto
            arResultAuto(dimension).intCodUnidad = rs1!cUnidadConvencional
            rs1.MoveNext
        Loop
        rs1.Close
        qry1.Close
        
        'se actualizan otros datos en pantalla
        lblListaTrabajo.Caption = "Lista de trabajo n�:  " & objAutoAn.strNumLista
        estado = objAutoAn.constESTADOSISTESPERA
        panEstado.Caption = objAutoAn.fLeerEstado(estado)
    Else
        Screen.MousePointer = vbDefault
        SQL = "No se ha encontrado ninguna muestra para realizar la lista de trabajo pendiente."
        MsgBox SQL, vbExclamation, Me.Caption
        Form_Unload (False)
    End If
    rs.Close
    qry.Close
  
End Sub

Private Sub Sistema_Espera()
'Se env�a el caracter de solicitud de comunicaci�n y se pasa a estado de Solicitud
    Comm1.Output = Chr$(5) 'Env�o de <ENQ>, se "solicita l�nea"
    estado = objAutoAn.constESTADOSISTSOLICITUD
    'panEstado.Caption = objAutoAn.fLeerEstado(estado)  'Especificado en pEnvioMensajes
    TimerEsperaREPLY.Enabled = True
    strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <ENQ> ESTADO SOLICITUD" & Chr$(13) & Chr$(10)
End Sub

Private Sub Sistema_Master()
'Se forma el mensaje de respuesta al Query.
    Dim strRegistro_H As String
    Dim strRegistro_P As String
    Dim arRegistro_O() As String
    Dim dimension As Integer
    Dim strRegistro_L As String
    Dim intNumeroDeFrames As Integer
    Dim contador1 As Integer
    Dim contador2 As Integer
    Dim msg As String

    'Registro cabecera tipo H
    strRegistro_H = "H|\^&" & Chr$(13)

    'Registro de informaci�n del paciente tipo P
    strRegistro_P = "P|1|" & strCodMuestraQuery & Chr$(13)

    'Registros de ordenes de pruebas tipo O
    'Se crear�n tantos registros de orden tipo O como pruebas existan para la muestra
    dimension = 0
    For contador1 = 1 To UBound(arActuaciones)
        If arActuaciones(contador1).strIDMuestra = strCodMuestraQuery Then
            dimension = dimension + 1
            ReDim Preserve arRegistro_O(1 To dimension)
            msg = "O|" & dimension & "||" & strRackQuery & "|^^^" & arActuaciones(contador1).strCodActAuto & "|"
            If arActuaciones(contador1).blnUrgente = True Then
                msg = msg & "S|"
            Else
                msg = msg & "|"
            End If
            msg = msg & "|||||P||||||||||||||Q" & Chr$(13)
            arRegistro_O(dimension) = msg
            'se actualiza el arActuaciones y el grid
            arActuaciones(contador1).strPosicion = strRackQuery
            arActuaciones(contador1).strEstado = objAutoAn.constESTADOPRAUTOINTENTOPROGRAMAR
            ssdgPruebas.MoveFirst
            ssdgPruebas.MoveRecords contador1 - 1
            ssdgPruebas.Columns("Copa").Text = arActuaciones(contador1).strPosicion
            ssdgPruebas.Columns("Estado").Text = arActuaciones(contador1).strEstado
        End If
    Next contador1
    If dimension = 0 Then 'no existe nada pedido para la muestra
        dimension = dimension + 1
        ReDim Preserve arRegistro_O(1 To dimension)
        arRegistro_O(dimension) = "O|1||" & strRackQuery & "|^^^000|||||||P||||||||||||||Z" & Chr$(13)
    End If

    'Registro terminador tipo L
    strRegistro_L = "L|1|I" & Chr$(13)

    'Se juntan todos los registros en uno solo (se emplea la variable strRegistro_H)
    strRegistro_H = strRegistro_H & strRegistro_P
    For contador1 = 1 To UBound(arRegistro_O)
        strRegistro_H = strRegistro_H & arRegistro_O(contador1)
    Next contador1
    strRegistro_H = strRegistro_H & strRegistro_L

    'Se descompone el mensaje en frames
    If Len(strRegistro_H) / 240 = Int(Len(strRegistro_H) / 240) Then
        intNumeroDeFrames = Int(Len(strRegistro_H) / 240)
    Else
        intNumeroDeFrames = Int(Len(strRegistro_H) / 240) + 1
    End If
    ReDim arFrames(1 To intNumeroDeFrames)
    contador2 = 0
    For contador1 = 1 To intNumeroDeFrames
        arFrames(contador1) = Mid$(strRegistro_H, 1 + (contador1 - 1) * 240, 240)
        'Se completan los frames con los caracteres de transmision
        contador2 = contador2 + 1
        If contador2 = 8 Then
            contador2 = 0
        End If
        If contador1 = UBound(arFrames) Then
            arFrames(contador1) = contador2 & arFrames(contador1) & Chr$(3)
        Else
            arFrames(contador1) = contador2 & arFrames(contador1) & Chr$(23)
        End If
        Call pEstablecerCheck(arFrames(contador1))
        arFrames(contador1) = Chr$(2) & arFrames(contador1) & Chr$(13) & Chr$(10)
    Next contador1

    'Se transmiten los frames
    intNumFrameTrans = 1
    strLog = strLog & " intNumFrameTrans= 1" & Chr$(13) & Chr$(10)
    Call pTransmisionFrames

End Sub

Private Sub TimerAUTO_Timer()
'Controla si se ha producido un TimeOut por parte del autoanalizador. Esto ocurre cuando _
    estando el ordenador en estado de esclavo pasan 30 seg. sin recibir ning�n dato.
    Dim msg As String
    Dim resp As Integer

    TimerAUTO.Enabled = False
    If estado = objAutoAn.constESTADOSISTESCLAVO Then
        msg = "Se ha producido un Timeout durante la comunicaci�n por parte del autoanalizador."
        msg = msg & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
        resp = MsgBox(msg, vbExclamation + vbYesNo, Me.Caption)
        Select Case resp
        Case vbYes
            TimerAUTO.Enabled = True
        Case vbNo
            strFrameTotal = ""
            Comm1.Output = Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
            estado = objAutoAn.constESTADOSISTESPERA 'Se pasa al estado de espera
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
            panEstado.Caption = objAutoAn.fLeerEstado(estado)
            Exit Sub
        End Select
    End If

End Sub

Private Sub TimerEsperaREPLY_Timer()
'Controla los TimeOut que produce el autoanalizador cuando el ordenador est� a la espera _
    de una respuesta
        
    Dim msg As String
    Dim resp As Integer
    
    TimerEsperaREPLY.Enabled = False
    Select Case estado
    Case objAutoAn.constESTADOSISTSOLICITUD, objAutoAn.constESTADOSISTCONFIRMACION
        msg = "Se ha producido un Timeout durante la comunicaci�n por parte del autoanalizador."
        msg = msg & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
        resp = MsgBox(msg, vbExclamation + vbYesNo, Me.Caption)
        Select Case resp
        Case vbYes
            TimerEsperaREPLY.Enabled = True
        Case vbNo
            Comm1.Output = Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
            estado = objAutoAn.constESTADOSISTESPERA 'Se pasa al estado de espera
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
            panEstado.Caption = objAutoAn.fLeerEstado(estado)
            Exit Sub
        End Select
    Case Else
        msg = "Error de programaci�n. Revisar el c�digo. El TimeoOut que se ha producido solo "
        msg = msg & "deb�a producirse en el estado de SOLICITUD o CONFIRMACION"
        MsgBox msg
    End Select

End Sub

Private Sub TimerNuevasPruebas_Timer()
'Se busca si existe alguna urgencia para realizar en el autoanalziador
    Dim strIDMuestra As String
    Dim dimension As Integer
    Dim SQL As String
    Dim rs As rdoResultset, qry As rdoQuery
    Dim strItem As String
    
    'Se pone el cListaRealizacion de las nuevas urgencias activas a -constELECTRA para poder
    'reconocerlas posteriormente.
    SQL = "UPDATE pruebaAsistencia"
    SQL = SQL & " SET cListaRealizacion = ?"
    SQL = SQL & " WHERE estado = ?"
    SQL = SQL & " AND (demorada = 0 OR demorada IS NULL)"
    SQL = SQL & " AND cListaRealizacion <> ?"
    SQL = SQL & " AND cCarpeta IN"
    SQL = SQL & "      (SELECT cCarpeta FROM carpetas"
    SQL = SQL & "       WHERE cAutoAnalizador = ?"
    SQL = SQL & "      )"
    Set qry = objAutoAn.rdoConnect.CreateQuery("", SQL)
    qry(0) = -objAutoAn.intCodAutoAn
    qry(1) = objAutoAn.constPRUEBASOLICITUDREALIZ
    qry(2) = objAutoAn.strNumLista
    qry(3) = objAutoAn.intCodAutoAn
    qry.Execute

    'Se establecen los registros de la matriz de pruebas.
    SQL = "SELECT mP.cMuestra, mP.PR04numActPlan, pA.nRepeticion, PR0400.PR01codActuacion,"
    SQL = SQL & " PR0100.PR01desCorta, pAuto.cPruebaAuto, nvl(pA.urgenciaRealizacion,'0') urgencia"
    SQL = SQL & " FROM muestraPrueba mP, pruebaAsistencia pA, muestraAsistencia mA,"
    SQL = SQL & " PR0400, PR0100, pruebasAutoanalizador pAuto"
    SQL = SQL & " WHERE pA.PR04numActPlan = mP.PR04numActPlan"
    SQL = SQL & " AND pA.cListaRealizacion = ?"
    SQL = SQL & " AND pA.estado = ?"
    SQL = SQL & " AND (pA.demorada = 0 OR pA.demorada  IS NULL)"
    SQL = SQL & " AND mA.cMuestra = mP.cMuestra"
    SQL = SQL & " AND mA.estado = ?"
    SQL = SQL & " AND PR0400.PR04numActPlan = pA.PR04numActPlan"
    SQL = SQL & " AND PR0100.PR01codActuacion = PR0400.PR01codActuacion"
    SQL = SQL & " AND pAuto.PR01codActuacion = PR0400.PR01codActuacion"
    SQL = SQL & " AND pAuto.cAutoAnalizador = ?"
    SQL = SQL & " ORDER BY mP.cMuestra"
    Set qry = objAutoAn.rdoConnect.CreateQuery("", SQL)
    qry(0) = -objAutoAn.intCodAutoAn
    qry(1) = objAutoAn.constPRUEBASOLICITUDREALIZ
    qry(2) = objAutoAn.constMUESTRAEXTRAIDA
    qry(3) = objAutoAn.intCodAutoAn
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        'se anotan los datos en el array de actuaciones
        dimension = UBound(arActuaciones)
        Do While Not rs.EOF
            dimension = dimension + 1
            ReDim Preserve arActuaciones(1 To dimension)
            arActuaciones(dimension).strCodMuestra = rs!cMuestra
            strIDMuestra = objAutoAn.fCodigoANumero(arActuaciones(dimension).strCodMuestra)
            strIDMuestra = String(10 - Len(strIDMuestra), "0") & strIDMuestra
            arActuaciones(dimension).strIDMuestra = strIDMuestra
            arActuaciones(dimension).strNumAct = rs!PR04numActPlan
            arActuaciones(dimension).intNumRepet = rs!nRepeticion
            arActuaciones(dimension).strCodAct = rs!PR01codActuacion
            arActuaciones(dimension).strCodActAuto = rs!cPruebaAuto
            arActuaciones(dimension).strEstado = objAutoAn.constESTADOPRAUTOSINPROGRAMAR
            If rs!urgencia = constURGENCIA Then
                arActuaciones(dimension).blnUrgente = True
            Else
                arActuaciones(dimension).blnUrgente = False
            End If
            'se muestran los datos en el grid
            strItem = arActuaciones(dimension).strPosicion & Chr$(9) 'posicion
            strItem = strItem & arActuaciones(dimension).strCodMuestra & Chr$(9) 'muestra
            strItem = strItem & arActuaciones(dimension).strIDMuestra & Chr$(9) 'ID muestra
            strItem = strItem & rs!PR01desCorta & Chr$(9) 'designacion de la prueba
            If arActuaciones(dimension).blnUrgente = True Then 'urgente
                strItem = strItem & "SI" & Chr$(9)
            Else
                strItem = strItem & "NO" & Chr$(9)
            End If
            strItem = strItem & arActuaciones(dimension).strEstado 'estado
            ssdgPruebas.AddItem strItem
            rs.MoveNext
        Loop
    End If

    'Se actualiza el n� de la lista -constELECTRA al n� de la lista actual
    SQL = "UPDATE pruebaAsistencia"
    SQL = SQL & " SET cListaRealizacion = ?"
    SQL = SQL & " WHERE cListaRealizacion = ?"
    Set qry = objAutoAn.rdoConnect.CreateQuery("", SQL)
    qry(0) = objAutoAn.strNumLista
    qry(1) = -objAutoAn.intCodAutoAn
    qry.Execute
    
End Sub

Private Sub TimerTrasMENSAJE_Timer()
'Controla los tiempos entre env�os de solicitudes
    TimerTrasMENSAJE.Enabled = False
    Call pEnvioMensajes

End Sub

Private Sub pTransmisionFrames()
'Se transmiten los frames teniendo en cuenta las respuestas del autoanalizador.
    Dim contador1 As Integer
    Dim SQL As String

    If intNumFrameTrans = UBound(arFrames) Then
        blnUltimoRepetido = True
    Else
        blnUltimoRepetido = False
    End If
    Comm1.Output = arFrames(intNumFrameTrans)
    estado = objAutoAn.constESTADOSISTCONFIRMACION
    strLog = strLog & Chr$(13) & Chr$(10) & " ESTADO CONFIRMACION" & Chr$(13) & Chr$(10)
    strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: " & arFrames(intNumFrameTrans) & Chr$(13) & Chr$(10)
    TimerEsperaREPLY.Enabled = True

End Sub

