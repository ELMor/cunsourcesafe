Attribute VB_Name = "ModSynchron"
Option Explicit

Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
Declare Function WritePrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lplFileName As String) As Integer

Global txtrecibir As String

'Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador          'Objeto con todas las ctes
'Public objError As New clsErrores
Global Const constORINA = "OR"

Public Sub Main0()
  Screen.MousePointer = vbHourglass
'  App.HelpFile = "f:\laborat\ayuda\" & App.EXEName & ".hlp"
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    Protocolo = cteBECKMAN_CX3 'cteSYNCHRON
    frmPrincipal.Caption = "Synchron LX20"
    fCargarColumnasGrid 'Definicion de las columnas que componen los 2 Grid
'    .pCargaColEstados (Array(10, 4, 5))
    If Not .fInicializar(App, cteSYNCHRON, True, True, , , , , , , True, , True, True, , , , , , True) Then End
'    fCargarEspecOrinaSexoEdadRefs
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  With objAutoAn
    'Columnas del Grid Muestras
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESPECIMEN
    .AgregarColumnaAGridMuestras cteMuestraCARPETA
    '.AgregarColumnaAGridMuestras cteMuestraPROPERTY1, "Especie", , InVisible  'tipo especie
    '.AgregarColumnaAGridMuestras cteMuestraPROPERTY2, "Orina", , InVisible  'si es orina se pone 3
    'estos 2 habra que quitar mas tarde...
    '.AgregarColumnaAGridMuestras cteMuestraPROPERTY3, "Edad"   'Edad (aprovechando la columna Bandeja inutilizada)
    '.AgregarColumnaAGridMuestras cteMuestraCOPA, "Sexo"   'Sexo(aprovechando la columna Copa inutilizada)
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    'Columnas del Grid Pruebas-Resultados
    .AgregarColumnaAGridPruebas ctePruebaFILA
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctePruebaCODACTUACION, , , InVisible
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    '.AgregarColumnaAGridPruebas ctePruebaESTADO
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    '.AgregarColumnaAGridPruebas cteResultadoREFMIN
    '.AgregarColumnaAGridPruebas cteResultadoREFMAX
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
End Sub

Private Sub fCargarEspecOrinaSexoEdadRefs()
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim objResultado As clsResultado
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql$
On Error GoTo Error

  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      'Especimen
      sql = "SELECT cTipoEspecieAuto FROM TiposEspeciesAutoanalizador" _
      & " WHERE cAutoanalizador =" & cteSYNCHRON & " and (cTipoEspecie) IN" _
      & " (SELECT cTipoEspecie FROM tiposmuestras where cTipoMuestra " _
      & " In (select cTipoMuestra from MuestraAsistencia WHERE cMuestra = ?))"
      Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
      qry(0) = .strCodMuestra
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not rs.EOF Then   'Si no se encuentra el especimen quedar� vac�a su posicion en la matriz
        .strProperty1 = rs(0)
        'Las orinas solo se hacen una vez
        If rs("ctipoespecie") = "2" Then .strProperty2 = constORINA
      Else
        .strProperty1 = "  "
      End If
      qry.Close: rs.Close
      'Sexo y Edad
      sql = "Select CI30CODSEXO, CI22FECNACIM From CI2200 Where"
      sql = sql & " CI21CODPERSONA in (Select CI21codpersona from"
      sql = sql & " PR0400 where PR04NUMACTPLAN = ?"
      Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
      qry(0) = CLng(.ColMuestrasPruebas(1).strNumAct)
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not rs.EOF Then
        If rs("CI22FECNACIM") = Null Then
          .strBandeja = "1"
        Else
          .strBandeja = CStr(DateDiff("yyyy", rs("CI22FECNACIM"), Now))
        End If
        .strCopa = IIf(rs("CI30CODSEXO") = 1, "M", "F") 'Hombre o Mujer
      End If
      qry.Close: rs.Close
'      For Each objActuacion In .ColMuestrasPruebas
'        With objActuacion
'          For Each objResultado In .ColPruebasResultados
'            sql = "SELECT referenciaMin, referenciaMax FROM pruebasValidacion"
'            sql = sql & " WHERE PR01CODACTUACION = ?  AND cResultado = ?"
'            sql = sql & " AND (sexo = ? OR sexo IS NULL)"
'            sql = sql & " AND (EdadInferior IS NULL OR EdadInferior <= ?)"
'            sql = sql & " AND (EdadSuperior IS NULL OR EdadSuperior >= ?)"
'            sql = sql & " AND (referenciaMin IS NOT NULL OR referenciaMax IS NOT NULL)"
'            Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
'            qry(0) = .strCodAct
'            qry(1) = objResultado.codResultado
'            qry(2) = objMuestra.strCopa
'            qry(3) = CInt(objMuestra.strBandeja)
'            Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'            If Not rs.EOF Then
'              With objResultado
'                .strProperty1 = IIf(rs("referenciaMin") = "", "0", rs("referenciaMin"))
'                .strProperty2 = IIf(rs("referenciaMax") = "", "0", rs("referenciaMax"))
'              End With
'            End If
'            qry.Close: rs.Close
'          Next
'        End With
'      Next
    End With
  Next
  
Error:

End Sub

