VERSION 5.00
Begin VB.Form frmAvisos 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Avisos CX3"
   ClientHeight    =   2700
   ClientLeft      =   2625
   ClientTop       =   2550
   ClientWidth     =   4320
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2700
   ScaleWidth      =   4320
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   180
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   300
      Width           =   3975
   End
   Begin VB.CommandButton cmdCerrar 
      Appearance      =   0  'Flat
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   1500
      TabIndex        =   0
      Top             =   2220
      Width           =   1395
   End
End
Attribute VB_Name = "frmAvisos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCerrar_Click()
    frmAvisos.Tag = ""
    Unload frmAvisos
End Sub

Private Sub Form_Load()
  'ubicación de la pantalla de avisos
  Move (Screen.Width - frmAvisos.Width), (frmPrincipal.Top + frmPrincipal.Height - frmAvisos.Height)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmAvisos.Tag = "Oculta"
    DoEvents
End Sub

