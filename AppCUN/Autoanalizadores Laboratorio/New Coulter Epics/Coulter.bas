Attribute VB_Name = "modCoulter"
Option Explicit
Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores
Declare Function GetTickCount Lib "kernel32" () As Long

Public Sub Main()
  If App.PrevInstance Then End 'Si hay una instancia previa abierta, se finaliza la ejecución
  Screen.MousePointer = vbHourglass
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    fCargarColumnasGrid 'Definicion de las columnas que componen los 2 Grid
    If Not .fInicializar(App, cteEPICS_XL, , , True, , , True, , , False, True, , , True, True, True) Then End
    pCargaPanelProtocoloRegion
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  'Columnas del Grid Muestras
  With objAutoAn
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    'Columnas del Grid Pruebas-Resultados
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctePruebaNUMREPETICION
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    '.AgregarColumnaAGridPruebas ctePruebaCODACTUACION
    '.AgregarColumnaAGridPruebas ctePruebaNUMACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoPROPERTY1, "Panel", 2000
    .AgregarColumnaAGridPruebas cteResultadoPROPERTY2, "Protocolo", 2400
    .AgregarColumnaAGridPruebas cteResultadoPROPERTY3, "Reg.", 550
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
End Sub

Private Sub pCargaPanelProtocoloRegion()
Dim Sql$, codResAuto$
Dim Qry As rdoQuery
Dim Rs As rdoResultset
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim objResultado As clsResultado
Dim objPruebaAuto As clsPruebaAuto
Dim objResultAuto As clsResultAuto
  
  For Each objPruebaAuto In objAutoAn.ColPruebaAuto
    For Each objResultAuto In objPruebaAuto.ColResultAuto
      With objResultAuto
        Sql = "SELECT A.LB21DESPROTOCOLO, B.LB22CODREGION, C.LB23DESPANEL " & _
        "FROM " & _
        "LB2100 A, LB2200 B, LB2300 C " & _
        "WHERE " & _
        "B.LB23CODPANEL = C.LB23CODPANEL AND " & _
        "B.LB21CODPROTOCOLO = A.LB21CODPROTOCOLO AND " & _
        "B.LB23CODPANEL = A.LB23CODPANEL AND " & _
        "B.LB22CODRESULTADO = ?"
        Set Qry = objAutoAn.rdoConnect.CreateQuery("", Sql)
        Qry(0) = .strCodResultAuto
        Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If Not Rs.EOF Then
          .strProperty1 = Rs("LB23DesPanel")
          .strProperty2 = Rs("LB21DesProtocolo")
          .strProperty3 = Rs("LB22CodRegion")
        End If
      End With
    Next
  Next
  For Each objMuestra In objAutoAn.ColMuestras
    For Each objActuacion In objMuestra.ColMuestrasPruebas
      For Each objResultado In objActuacion.ColPruebasResultados
        With objAutoAn.ColPruebaAuto(objActuacion.strCodAct).ColResultAuto(objResultado.codResultado)
          objResultado.strProperty1 = .strProperty1
          objResultado.strProperty2 = .strProperty2
          objResultado.strProperty3 = .strProperty3
        End With
      Next
    Next
  Next
'  Sql = "SELECT A.LB21DESPROTOCOLO, B.lb22codRESULTADO, B.lb22codregion " & _
'  "FROM LB2100 A,lb2200 B,LB2300 C " & _
'  "WHERE " & _
'  "A.lb21codPROTOCOLO = B.lb21codPROTOCOLO AND " & _
'  "A.LB23CODPANEL = C.LB23CODPANEL AND " & _
'  "A.lb23codpanel = B.lb23codpanel and " & _
'  "C.LB23DESPANEL=? order by a.lb21DESPROTOCOLO"
'  Set qry = objAutoAn.rdoConnect.CreateQuery("", Sql)
'  qry(0) = objAutoAn.fQuitarStringsDeString((Panel)
'  Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'  If rs.EOF Then MsgBox ("Panel No Definido en GDL")
'  Do While Not rs.EOF
'    Set objProtocolo = New clsProtocolo
'    With objProtocolo
'      .ProtocolName = rs!lb21DESPROTOCOLO
'      Do
'        Set objResultProtocolo = New clsResultProtocolo
'        Sql = "SELECT a.cprueba, a.cresultado, b.cunidadconvencional, b.designacion FROM "
'        Sql = Sql & "resultadosautoanalizador a,pruebasresultados b "
'        Sql = Sql & "WHERE a.cprueba = b.cprueba and "
'        Sql = Sql & "a.cresultado = b.cresultado and A.cresultadoauto = ?"
'        Set qry1 = objAutoAn.rdoConnect.CreateQuery("", Sql)
'        qry1(0) = rs!lb22codResultado
'        Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'        If Not rs1.EOF Then
'          With objResultProtocolo
'            .Region = rs!lb22codregion
'            .cPrueba = rs1!cPrueba
'            .cResultado = rs1!cResultado
'            .cUnidad = rs1!cUnidadconvencional
'            .Descripcion = rs1!designacion
'          End With
'          .ColResultados.Add objResultProtocolo, CStr(objResultProtocolo.Region)
'          Set objResultProtocolo = Nothing
'        End If
'        rs1.Close
'        qry1.Close
'        rs.MoveNext
'        If rs.EOF Then Exit Do
'      Loop While .ProtocolName = rs!lb21DESPROTOCOLO
'      colProtocolos.Add objProtocolo, CStr(.ProtocolName)
'    End With
'    Set objProtocolo = Nothing
'  Loop
'  rs.Close
'  qry.Close
End Sub
