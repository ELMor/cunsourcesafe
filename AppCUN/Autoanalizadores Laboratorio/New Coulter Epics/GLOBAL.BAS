Attribute VB_Name = "GLOBAL"
Option Explicit

'                               ----- (�ltima revisi�n 5-10-95) -----

Global dbCun As Database            ' Base de datos de laboratorio.

'Constantes de acceso a Oracle
Global Const constPropietario = "CUNLab"        ' Propietario de las tablas
Global Const constPwd = "ESII"                  ' Password de las tablas
Global Const constDatabase = "Q+E ML Oracle7"   ' Nombre del driver que se utiliza
Global Const constServerName = "central1.cun"    ' Nombre del servidor

'Constante de t�tulo del programa general que llama al resto de programas
Global Const constProgGeneral = "Controlador de autoanalizadores del SILAB"

'Constantes de estado del sistema
Global Const constESPERA = 0
Global Const constSOLICITUD = 1
Global Const constESCLAVO = 2
Global Const constCONFIRMACION = 3
Global Const constMASTER = 4
Global Const constTRANSMISION = 5

'Constantes de los procesos de las pruebas
Global Const constPROCESOSOLICITUD = 1
Global Const constPROCESOREALIZACION = 5
Global Const constPROCESOVALIDACION = 9

'Constantes de los estados de las pruebas
Global Const constPRUEBASOLICITADA = 1
Global Const constPRUEBAIMPRESA = 2
Global Const constPRUEBAEXTRAIDA = 3
Global Const constPRUEBASOLICITUDREALIZ = 4
Global Const constPRUEBAREALIZANDO = 5
Global Const constPRUEBARESULTADO = 6
Global Const constPRUEBARESULTANALIZ = 7
Global Const constPRUEBAVALIDADA = 8
Global Const constPRUEBAINSERTADA = 9
Global Const constPRUEBAENVIOPROV = 10
Global Const constPRUEBAENVIADA = 11
Global Const constPRUEBAREPETIDA = 90
Global Const constPRUEBAANULADA = 99
    
'Constantes del estado de las muestras
Global Const constMUESTRAPENDIENTE = 1
Global Const constMUESTRAEXTRAIDA = 2
Global Const constMUESTRAFINALIZADA = 3
Global Const constMUESTRAANULADAMUESTRA = 4
Global Const constMUESTRAANULADAPRUEBA = 5
Global Const constMUESTRAGUARDADA = 6

'Constantes de los estados de los resultados
Global Const constRESULTADOINTRODUCIDO = 1
Global Const constRESULTADOVALIDADO = 2
Global Const constRESULTADOREPETIDO = 3
Global Const constRESULTADOANULADO = 4

'Variables de configuraci�n de los par�metros I/O
Global Puerto As Integer            'Puerto serie utilizado por el ordenador.
Global BaudRate As Integer          'Velocidad de comunicaci�n.
Global DataBits As Integer          'N�mero de bits por caracter.
Global StopBits As Integer          'N�mero de bits de stop.
Global Parity As String             'Paridad

'C�digos de los autoanalizadores
Global Const constELECTRA = 1
Global Const constKOAGULAB = 2
Global Const constKOAG_A_MATE = 3
Global Const constAGGRECORDER = 4
Global Const constCPA_COULTER = 5
Global Const constCOULTER_T540 = 6
Global Const constCOULTER_STKS = 7
Global Const constTECHNICON = 8
Global Const constCOBAS_VEGA = 9
Global Const constVES_MATIC = 10
Global Const constBECKMAN_CX3 = 11
Global Const constHITACHI_704 = 12
Global Const constHITACHI_717 = 13
Global Const constCOBAS_FARA = 14
Global Const constCLINITEK = 15
Global Const constLABORATORIO_REF = 16
Global Const constCOBAS_CORE_MARC = 17
Global Const constAXSYM = 18
Global Const constBECKMAN_APPRAISE = 19
Global Const constIMX_PRO = 20
Global Const constBECKMAN_ARRAY = 21
Global Const constVITEK = 22
Global Const constBACK_ALERT = 23
Global Const constCOBAS_CORE_SER = 24
Global Const constIMX_SER = 25
Global Const constCERES_900 = 26
Global Const constEPICS_XL = 27
Global Const constCAP = 28
Global Const constTDX = 29
Global Const constTDX_FLX = 30
Global Const constIMX_FARM = 31
Global Const constBNA_BEHRING = 32
Global Const constREFERENCE = 227

'Constantes del estado de las listas de trabajo
Global Const constLISTAPREALIZANDO = 1
Global Const constLISTAPRESULTANAL = 2
Global Const constLISTAPRESULTADO = 3
Global Const constLISTAPVALIDTECNI = 4

Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
Declare Function WritePrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lplFileName As String) As Integer
Declare Function GetTickCount Lib "kernel32" () As Long
Declare Function SetWindowPos Lib "user32" _
    (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, _
    ByVal x As Long, ByVal y As Long, ByVal cx As Long, _
    ByVal cy As Long, ByVal wFlags As Long) As Long
    
Global Text1 As String
Global archivolog As String
Global rdocon As rdoConnection
Global rdoEnv As rdoEnvironment

Public Const SWP_NOMOVE = &H2
Public Const SWP_NOSIZE = &H1
Public Const SWP_SHOWWINDOW = &H40
Public Const SWP_NOACTIVATE = &H10
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2


'Global wrkODBC As Workspace
'Global wrkAlberto As Workspace
'Global wrkJet As Workspace
'Global conODBC As Connection
'Global conAlberto As Connection
'Global conJet As Connection
'Global rdocon As rdoConnection
'Global rdoEnv As rdoEnvironment
'Option Explicit



Function CodigoANumero&(Cod$)

    Dim num&
    If (Left$(Cod, 1) >= "A" And Left$(Cod, 1) <= "Z") Or (Left$(Cod, 1) >= "a" And Left$(Cod, 1) <= "z") Then
        Dim Izq$, Der$, idx%
        If Len(Cod) <> 8 And Len(Cod) <> 7 Then
            CodigoANumero = 0
            Exit Function
        End If
        Izq = UCase(Left$(Cod, 3))
        idx = Len(Cod)
        While IsNumeric(Mid$(Cod, idx, 1))
            Der = Mid$(Cod, idx, 1) & Der
            idx = idx - 1
        Wend
        For idx = 0 To 2
            num = 26 * (num + Asc(Mid$(Izq, idx + 1, 1)) - 64)
        Next idx
        num = num / 26
        num = num * 10000 + Val(Der)
        CodigoANumero = num
    Else
        If Not IsNumeric(Cod) Then
            CodigoANumero = 0
            Exit Function
        End If
        If Val(Cod) > 9999 Then
            CodigoANumero = Val(Cod)
        Else
            Dim SQL$, rdorec As rdoResultset
            SQL = "Select cMuestraLetra from CodigoPrueba"
            Set rdorec = rdocon.OpenResultset(UCase(SQL), DATA_SQLPASSTHROUGH)
            CodigoANumero = CodigoANumero(rdorec(0) & "-" & Format(Val(Cod), "0000"))
        End If
    End If

End Function

Sub Config_IO(cAutoAnalizador As Integer)

'                                   ----- (�ltima revisi�n 5-03-96) -----
    
'Establece los par�metros de Entrada/Salida de los puertos serie del ordenador para que est�n
'conformes a los par�metros de los autoanalizadores.

'                                   ***** DECLARACI�N DE VARIABLES *****

    Dim PARAMETROS As String * 80
    Dim SQL As String
    Dim rdorec As rdoResultset
    Dim res%, a%
    Dim DBTIPO As Integer
    Dim RDOTIPO As Integer
    
    
'                                      ***** DESARROLLO *****
    'DBTIPO = dbOpenSnapshot
    'rdtipo = rdOpenStatic
    'On Error GoTo label
   
   
    'Se accede a la base de datos
    'PARAMETROS = "DSN=" & constDatabase & ";SERVERNAME=" & constServerName & ";UID=" & constPropietario & ";PWD=" & constPwd
    'res = GetPrivateProfileString("DATABASE", "PARAMET", "", PARAMETROS, 80, "F:\LABORAT\AUTOAN\AUTOAN.INI")
    'Set dbCun = OpenDatabase("ORACLE", 0, 0, PARAMETROS)
    Set rdoEnv = rdoEnvironments(0)
    Set rdocon = rdoEnv.OpenConnection(dsName:="", Prompt:=dbDriverNoPrompt, _
        Connect:="UID=CUNLAB;PWD=ESII;DSN=ORACLE73 Autoan")
    'a% = MsgBox("conexion realizada", vbOKOnly, "depuracion")
    
    'Se leen las caracter�sticas de la configuraci�n I/O del autoanalizador y se establecen para el Comm.
'    sql = "SELECT puertoSerie, baudRate, parity, dataBits, stopBits, designacion FROM " & constPropietario & ".autoAnalizadores"
'    sql = sql & " WHERE cAutoAnalizador = " & cAutoAnalizador
'    Set rdorec = rdocon.OpenResultset(sql, 2) ', rdtipo, rdbloqueo, rdAsyncEnable)
'    'set rdorec=openresultset(sql)
'    If rdorec.RowCount > 0 Then
'        Puerto = rdorec("puertoserie")
'        BaudRate = rdorec("baudRate")
'        Parity = rdorec("parity")
'        DataBits = rdorec("dataBits")
'        StopBits = rdorec("stopBits")
'    End If
'    frmPrincipal.Comm1.CommPort = Puerto
'    'frmPrincipal.Comm1.Notification = 1  'Modo Event Driven; 0 - Modo Polling   Ref:Q101944
'    frmPrincipal.Comm1.Settings = BaudRate & "," & Parity & "," & DataBits & "," & StopBits
'    'frmPrincipal.Comm1.PortOpen = True
'    frmPrincipal.panPuerto.Caption = "Puerto Serie " & frmPrincipal.Comm1.CommPort & ": Abierto"
'    rdorec.Close

End Sub

Function fFormatearNumero(numero As String, sustituir As String, sustituirPor As String) As Integer

'                                   ----- (�ltima revisi�n 10-11-95) -----
    
'Formatea un n�mero decimal intercambiando puntos por comas y viceversa

'                                   ***** DECLARACI�N DE VARIABLES *****
    Dim res As Integer
'                                      ***** DESARROLLO *****
    res = InStr(numero, sustituir)
    If res > 0 Then
        numero = Left$(numero, res - 1) & sustituirPor & Right$(numero, Len(numero) - res)
    End If

End Function

Function LeerEstado(status As Integer) As String

'Lee los estados del sistema y los escribe como un string
    
    Select Case status
    Case constESPERA
        LeerEstado = "ESPERA"
    Case constCONFIRMACION
        LeerEstado = "CONFIRMACION"
    Case constSOLICITUD
        LeerEstado = "SOLICITUD"
    Case constESCLAVO
        LeerEstado = "ESCLAVO"
    Case constMASTER
        LeerEstado = "MASTER"
    Case constTRANSMISION
        LeerEstado = "TRANSMISION"
    Case Else
        LeerEstado = "no definido"
    End Select

End Function

Function NumeroACodigo$(num&)
 
    If num < 10000 Then
        Dim SQL$, rdorec As rdoResultset
        SQL = "Select cMuestraLetra from CodigoPrueba"
        Set rdorec = rdocon.OpenResultset(SQL)
        NumeroACodigo = rdorec(0) & "-" & CStr(num)
    Else
        Dim a%, b%, c%, d%
        d = num Mod 10000
        num = (num - d) / 10000
        c = num Mod 26
        num = (num - c) / 26
        b = num Mod 26
        num = (num - b) / 26
        a = num
        If (a < 1 Or a > 26) Or (b < 1 Or b > 26) Or (c < 1 Or c > 26) Then
            NumeroACodigo = ""
        Else
            NumeroACodigo = Chr$(64 + a) & Chr$(64 + b) & Chr$(64 + c) & "-" & Format(CStr(d), "0000")
        End If
    End If

End Function

Sub Seleccion_Lista_Trabajo(cAutoAnalizador As Integer, cListaRealizacion As Long)

'                                       ----- (�ltima revisi�n 17-10-95) -----

'Subrutina que localiza, si existe, la lista de trabajo de c�digo m�nimo que todav�a tiene
'pruebas pendientes de realizar en un autoanalizador.

'Devuelve (en la variable cListaRealizacion) el c�digo de la lista de trabajo si existe o cero si
'no existe ninguna lista con pruebas pendientes de realizar.

'                                       ***** DECALRACI�N DE VARIABLES *****

    Dim MatCarpetas()  As Integer       'c�digos de las carpetas asociadas al autoanalizador
    Dim NumCarpetas As Integer
    Dim minCListaRealizacion As Long
    
    Dim rdorec As rdoResultset
    Dim numRegistros As Integer
    Dim SQL As String
    Dim contador As Integer, a%
'
''                                              ***** DESARROLLO *****
'    'Se determina la lista de realizaci�n de menor orden que tienen pruebas pendientes de realizar
'label:
'    SQL = "SELECT distinct(cListaRealizacion) FROM " & constPropietario & ".pruebaAsistencia WHERE "
'    SQL = SQL & " (estado = " & constPRUEBASOLICITUDREALIZ
'    SQL = SQL & " OR estado = " & constPRUEBAREALIZANDO & ")"
'    SQL = SQL & " AND (demorada = 0 OR demorada IS NULL) AND "
'    SQL = SQL & "cCarpeta = " & constEPICS_XL
'    Set rdorec = rdocon.OpenResultset(SQL, 0)
'    'Existen pruebas pendientes de realizar en el autoanalziador
'    cListaRealizacion = 0
'    If rdorec.EOF = False Then
'        minCListaRealizacion = rdorec("cListaRealizacion")
'        While rdorec.EOF = False
'            cListaRealizacion = rdorec("cListaRealizacion")
'            If cListaRealizacion < minCListaRealizacion Then
'                minCListaRealizacion = cListaRealizacion
'            End If
'            rdorec.MoveNext
'        Wend
'        If minCListaRealizacion = -1 * cAutoAnalizador Then
'            SQL = "UPDATE " & constPropietario & ".pruebaAsistencia SET cListaRealizacion = NULL "
'            SQL = SQL & ", estado = " & constPRUEBAEXTRAIDA & " WHERE cListaRealizacion = -" & cAutoAnalizador
'            SQL = SQL & " AND (demorada = 0 OR demorada IS NULL) AND "
'            SQL = SQL & "cCarpeta = " & constREFERENCE
'            rdocon.Execute UCase(SQL)
'            GoTo label
'        End If
'        cListaRealizacion = minCListaRealizacion
'    'No existen pruebas pendientes de realizar en el autoanalizador
'    Else
'        cListaRealizacion = 0
'    End If
'    rdorec.Close
'
'



'                                       ----- (�ltima revisi�n 17-10-95) -----

'Subrutina que localiza, si existe, la lista de trabajo de c�digo m�nimo que todav�a tiene
'pruebas pendientes de realizar en un autoanalizador.

'Devuelve (en la variable cListaRealizacion) el c�digo de la lista de trabajo si existe o cero si
'no existe ninguna lista con pruebas pendientes de realizar.

'                                       ***** DECALRACI�N DE VARIABLES *****

  
    
    
    

    
    
    

'                                              ***** DESARROLLO *****

    'Se determina las carpetas del autoanalizador
    ReDim MatCarpetas(0 To 0)
    SQL = "SELECT cCarpeta FROM " & constPropietario & ".carpetas WHERE cAutoAnalizador = " & cAutoAnalizador
    Set rdorec = rdocon.OpenResultset(SQL, 0)
    If Not rdorec.EOF Then
        While Not rdorec.EOF
        NumCarpetas = rdorec.RowCount
        'rdorec.MoveFirst
'        For contador = 1 To NumCarpetas
            ReDim Preserve MatCarpetas(0 To UBound(MatCarpetas) + 1)
            MatCarpetas(UBound(MatCarpetas)) = rdorec("cCarpeta")
            rdorec.MoveNext
'        Next contador
        Wend
    Else
        cListaRealizacion = -1000 'No existen carpetas asociadas al autoanalizador
        Exit Sub
    End If
    rdorec.Close

    'Se determina la lista de realizaci�n de menor orden que tienen pruebas pendientes de realizar
label:
    SQL = "SELECT cListaRealizacion FROM " & constPropietario & ".pruebaAsistencia WHERE estado = " & constPRUEBASOLICITUDREALIZ
    SQL = SQL & " AND (demorada = 0 OR demorada IS NULL) AND ("
    For contador = 1 To UBound(MatCarpetas)
        SQL = SQL & "cCarpeta = " & MatCarpetas(contador) & " OR "
    Next contador
    SQL = Left$(SQL, Len(SQL) - 4) & ")"
    Set rdorec = rdocon.OpenResultset(SQL, 0)
    'Existen pruebas pendientes de realizar en el autoanalziador
    If Not rdorec.EOF Then
        'rdorec.MoveLast
        'numRegistros = rdorec.RowCount
        'rdorec.MoveFirst
         minCListaRealizacion = rdorec("cListaRealizacion")
        While Not rdorec.EOF
       
'        For contador = 1 To numRegistros
            cListaRealizacion = rdorec("cListaRealizacion")
            If cListaRealizacion < minCListaRealizacion Then
                minCListaRealizacion = cListaRealizacion
            End If
            rdorec.MoveNext
'        Next contador
        Wend
        If minCListaRealizacion = -1 * cAutoAnalizador Then
            SQL = "UPDATE " & constPropietario & ".pruebaAsistencia SET cListaRealizacion = NULL "
            SQL = SQL & ", estado = " & constPRUEBAEXTRAIDA & " WHERE cListaRealizacion = -" & cAutoAnalizador
            SQL = SQL & " AND (demorada = 0 OR demorada IS NULL) AND ("
            For contador = 1 To UBound(MatCarpetas)
                SQL = SQL & "cCarpeta = " & MatCarpetas(contador) & " OR "
            Next contador
            SQL = Left$(SQL, Len(SQL) - 4) & ")"
            rdocon.Execute UCase(SQL)
            GoTo label
        End If
        cListaRealizacion = minCListaRealizacion
    'No existen pruebas pendientes de realizar en el autoanalizador
    Else
        cListaRealizacion = 0
    End If
    rdorec.Close


End Sub

