VERSION 5.00
Begin VB.Form Resultados 
   Caption         =   "Resultados Obtenidos"
   ClientHeight    =   6495
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7110
   LinkTopic       =   "Form1"
   ScaleHeight     =   6495
   ScaleWidth      =   7110
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancelar"
      Height          =   405
      Left            =   5100
      TabIndex        =   8
      Top             =   6030
      Width           =   1515
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Aceptar"
      Height          =   405
      Left            =   480
      TabIndex        =   7
      Top             =   6030
      Width           =   1515
   End
   Begin VB.TextBox Text4 
      Height          =   4965
      Left            =   90
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   990
      Width           =   6915
   End
   Begin VB.TextBox Text3 
      Height          =   345
      Left            =   5070
      TabIndex        =   5
      Top             =   540
      Width           =   1335
   End
   Begin VB.TextBox Text2 
      Height          =   345
      Left            =   5070
      TabIndex        =   4
      Top             =   180
      Width           =   1335
   End
   Begin VB.TextBox Text1 
      Height          =   345
      Left            =   1740
      TabIndex        =   3
      Top             =   210
      Width           =   1335
   End
   Begin VB.Label Label3 
      Caption         =   "Muestra:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   690
      TabIndex        =   2
      Top             =   270
      Width           =   1005
   End
   Begin VB.Label Label2 
      Caption         =   "Caso:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4020
      TabIndex        =   1
      Top             =   600
      Width           =   1005
   End
   Begin VB.Label Label1 
      Caption         =   "Historia:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4020
      TabIndex        =   0
      Top             =   210
      Width           =   1005
   End
End
Attribute VB_Name = "Resultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public blnCancelar As Boolean

Private Sub Command1_Click()
blnCancelar = False
Me.Hide
End Sub

Private Sub Command2_Click()
blnCancelar = True
Me.Hide
End Sub

Public Sub Muestra(m As String)
Text1 = m
End Sub

Public Sub Historia(m As String)
Text2 = m
End Sub

Public Sub Caso(m As String)
Text3 = m
End Sub

Public Sub Resultados(m As String)
Text4 = m
End Sub

Private Sub Form_Resize()
Text4.Width = Me.Width - 300
End Sub
