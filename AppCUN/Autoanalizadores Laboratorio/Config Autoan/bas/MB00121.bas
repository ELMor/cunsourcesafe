Attribute VB_Name = "modLaborat"
Option Explicit

Public Sub pComentarioSeguimientoPrueba(historia As Long, caso%, secuencia%, nrepeticion%)
'se anota como Comentario en seguimientoPrueba que los resultados son definitivos
    Dim SQL As String
    Dim qrySegPrueba As rdoQuery
    
    On Error Resume Next
    SQL = "UPDATE seguimientoPrueba"
    SQL = SQL & " SET comentarios = ?"
    SQL = SQL & " WHERE historia = ?"
    SQL = SQL & " AND caso = ?"
    SQL = SQL & " AND secuencia = ?"
    SQL = SQL & " AND nRepeticion = ?"
    SQL = SQL & " AND proceso = ?"
    Set qrySegPrueba = objApp.rdoConnect.CreateQuery("", SQL)
    qrySegPrueba(0) = constINFORMEDEFINITIVO
    qrySegPrueba(1) = historia
    qrySegPrueba(2) = caso
    qrySegPrueba(3) = secuencia
    qrySegPrueba(4) = nrepeticion
    qrySegPrueba(5) = constPROCESOVALIDACION
    qrySegPrueba.Execute
    If Err <> 0 Then
        MsgBox "Se ha producido un error al informar la finalizaci�n de la prueba", vbError
    End If
    qrySegPrueba.Close
    
End Sub

Public Function fCodigoResultadoLaboratorio(strCSeccion$, blnTecnica As Boolean) As String
    Dim SQL As String
    Dim qryResultado As rdoQuery
    Dim rsResultado As rdoResultset
    
    SQL = "SELECT "
    SQL = SQL & "   MAX(cResultado)"
    SQL = SQL & " FROM"
    SQL = SQL & "   pruebasResultados"
    SQL = SQL & " WHERE "
    SQL = SQL & "   cPrueba IN"
    SQL = SQL & "       (SELECT p.cPrueba "
    SQL = SQL & "        FROM pruebas p, pruebasCarpetas pC, carpetas c"
    SQL = SQL & "        WHERE pC.cCarpeta = c.cCarpeta"
    SQL = SQL & "        AND p.cPrueba = pC.cPrueba"
    SQL = SQL & "        AND c.cDptoSecc = ?"
    SQL = SQL & "       )"
    If blnTecnica = False Then
        SQL = SQL & " AND cResultado > " & constcResultadoTecnica
    Else
        SQL = SQL & " AND cResultado <= " & constcResultadoTecnica
    End If
    Set qryResultado = objApp.rdoConnect.CreateQuery("Resultado", SQL)
    qryResultado(0) = strCSeccion
    Set rsResultado = qryResultado.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsResultado.EOF Then
        If blnTecnica = True Then
            fCodigoResultadoLaboratorio = 1
        Else
            fCodigoResultadoLaboratorio = constcResultadoTecnica + 1
        End If
    Else
        If Not IsNull(rsResultado(0)) Then
            fCodigoResultadoLaboratorio = 1 + rsResultado(0)
        Else
            fCodigoResultadoLaboratorio = 1
        End If
    End If
    rsResultado.Close
    qryResultado.Close
    
End Function

Public Function fInsertarResultadoLaboratorio(strCodigo$, strDesignacion$, strCSeccion$) As Boolean
    Dim SQL As String
    Dim qryPruebas As rdoQuery
    Dim rsPruebas As rdoResultset
    Dim qryResult As rdoQuery
    Dim rsResult As rdoResultset
    
    On Error Resume Next
    ' Statment para pruebas de las secci�n
    SQL = " SELECT DISTINCT p.cPrueba "
    SQL = SQL & " FROM pruebas p, pruebasCarpetas pC, carpetas c"
    SQL = SQL & " WHERE pC.cCarpeta = c.cCarpeta"
    SQL = SQL & " AND p.cPrueba = pC.cPrueba"
    SQL = SQL & " AND c.cDptoSecc = ?"
    Set qryPruebas = objApp.rdoConnect.CreateQuery("Pruebas", SQL)
    
    ' Statment para inserci�n de resultado en las pruebas de las secci�n
    SQL = "INSERT INTO pruebasResultados"
    SQL = SQL & " (cPrueba, cResultado,designacion,cUnidadConvencional, presentacion, tipoResultado, imprimir) "
    SQL = SQL & " VALUES "
    SQL = SQL & " (?,?,?,?,?,?,?)"
    Set qryResult = objApp.rdoConnect.CreateQuery("PruebaResultado", SQL)

    ' Se obtienen las pruebas
    qryPruebas(0) = strCSeccion
    Set rsPruebas = qryPruebas.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsPruebas.EOF
        qryResult(0) = rsPruebas!cPrueba
        qryResult(1) = strCodigo
        qryResult(2) = strDesignacion
        qryResult(3) = constUnidadDefecto
        qryResult(4) = constPRESENTACIONTABULAR
        qryResult(5) = constRESULTADOALFANUMERICO
        qryResult(6) = 1
        Err = 0
        Set rsResult = qryResult.OpenResultset(rdOpenKeyset)
        
        rsPruebas.MoveNext
    Loop
    
    rsPruebas.Close
    qryResult.Close
    qryPruebas.Close
    

'    If Err <> 0 Then
'        fInsertarResultadoLaboratorio = True
'    Else
'        fInsertarResultadoLaboratorio = False
'    End If
    
End Function

Public Function fRegenerarResultadosPruebasLaboratorio(strCSeccion$) As Boolean
    Dim SQL As String
    Dim qryMicro As rdoQuery
    Dim rsMicro As rdoResultset
    Dim res As Boolean

    ' Se recogen los resultados de TECNICAS
    SQL = "SELECT cResultado, MB32_DESCRIP"
    SQL = SQL & " FROM MB3200"
    SQL = SQL & " WHERE MB09_CODTEC IN"
    SQL = SQL & "   (SELECT MB09_CODTEC FROM MB0900 WHERE cDptoSecc = ?)"
    SQL = SQL & " AND cResultado IS NOT NULL "
    
    Set qryMicro = objApp.rdoConnect.CreateQuery("Resultado", SQL)

    ' Se obtienen las pruebas
    qryMicro(0) = strCSeccion
    Set rsMicro = qryMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsMicro.EOF
        res = fInsertarResultadoLaboratorio(rsMicro!cResultado, rsMicro!MB32_DESCRIP, strCSeccion)
        rsMicro.MoveNext
    Loop
    rsMicro.Close
    qryMicro.Close
    
    
    ' Se recogen los resultados de g�nero de microorganismos
    SQL = "SELECT cResultado, MB11_DESCRIP"
    SQL = SQL & " FROM MB1100"
    SQL = SQL & " WHERE MB08_CODTIMIC IN"
    SQL = SQL & "   (SELECT MB08_CODTIMIC FROM MB0800 WHERE cDptoSecc = ?)"
    SQL = SQL & " AND cResultado IS NOT NULL "
    
    Set qryMicro = objApp.rdoConnect.CreateQuery("Resultado", SQL)

    ' Se obtienen las pruebas
    qryMicro(0) = strCSeccion
    Set rsMicro = qryMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsMicro.EOF
        res = fInsertarResultadoLaboratorio(rsMicro!cResultado, rsMicro!MB11_DESCRIP, strCSeccion)
        rsMicro.MoveNext
    Loop
    rsMicro.Close
    qryMicro.Close
    
    
    ' Se recogen los resultados de morfolog�as
    SQL = "SELECT cResultado, MB12_DESCRIP"
    SQL = SQL & " FROM MB1200"
    SQL = SQL & " WHERE MB08_CODTIMIC IN"
    SQL = SQL & "   (SELECT MB08_CODTIMIC FROM MB0800 WHERE cDptoSecc = ?)"
    SQL = SQL & " AND cResultado IS NOT NULL "
    
    Set qryMicro = objApp.rdoConnect.CreateQuery("Resultado", SQL)

    ' Se obtienen las pruebas
    qryMicro(0) = strCSeccion
    Set rsMicro = qryMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsMicro.EOF
        res = fInsertarResultadoLaboratorio(rsMicro!cResultado, rsMicro!MB12_DESCRIP, strCSeccion)
        rsMicro.MoveNext
    Loop
    rsMicro.Close
    qryMicro.Close
    
    
    ' Se recogen los resultados de microorganismos
    SQL = "SELECT cResultado, MB18_DESCRIP"
    SQL = SQL & " FROM MB1800"
    SQL = SQL & " WHERE MB11_CODGEMICRO IN"
    SQL = SQL & "   (SELECT MB11_CODGEMICRO FROM MB1100, MB0800 WHERE MB1100.MB08_CODTIMIC=MB0800.MB08_CODTIMIC AND cDptoSecc = ?)"
    SQL = SQL & " AND cResultado IS NOT NULL "
    
    Set qryMicro = objApp.rdoConnect.CreateQuery("Resultado", SQL)

    ' Se obtienen las pruebas
    qryMicro(0) = strCSeccion
    Set rsMicro = qryMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsMicro.EOF
        res = fInsertarResultadoLaboratorio(rsMicro!cResultado, rsMicro!MB18_DESCRIP, strCSeccion)
        rsMicro.MoveNext
    Loop
    rsMicro.Close
    qryMicro.Close

End Function

Public Sub pRegenerarResultadosLaboratorio()
    ' Pregunta al usuario si desea actualizar la base de datos de laboratorio
    
    Dim res As Integer
    
    res = MsgBox("�Desea regenerar los resultados de la base de datos de Laboratorio?", vbYesNo + vbQuestion, "Regenerar base de datos de Laboratorio")
    If res = vbYes Then
        Screen.MousePointer = Hourglass
        res = fRegenerarResultadosPruebasLaboratorio(departamento)
        MsgBox "Actualizaci�n Finalizada", vbInformation, "Regenerar base de datos de Laboratorio"
        Screen.MousePointer = Default
    End If
End Sub



Public Function fInformarResultadoLabor(nRef$, cTecAsist%, historia As Long, caso%, secuencia%, nrepeticion%, responsable%, cResult%) As Integer
'se anota el resultado informado en la base de datos de Laboratorio
    Dim qryRes As rdoQuery
    Dim rsSelec As rdoResultset, qrySelec As rdoQuery
    Dim SQL$
    Dim fecha$, hora$
    Dim cResultado%, result$, cUnidad%, desigResult$
    
    fecha = fFechaHoraActual
    hora = Format(fecha, "HH:MM:SS")
    
    On Error Resume Next
    SQL = "SELECT NVL(cResultado,0), MB33_result, MB3200.cUnidad, MB32_desig"
    SQL = SQL & " FROM MB3300, MB3200"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    'no hace falta el codTec
    'SQL = SQL & " AND MB3200.MB09_codTec = ?"
    SQL = SQL & " AND MB3200.MB32_codResult = ?"
    SQL = SQL & " AND MB3300.MB09_codTec = MB3200.MB09_codTec"
    SQL = SQL & " AND MB3300.MB32_codResult = MB3200.MB32_codResult"
    Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
    qrySelec(0) = nRef
    qrySelec(1) = cTecAsist
    qrySelec(2) = cResult
    Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsSelec.EOF = False Then
        cResultado = rsSelec(0)
        result = rsSelec(1)
        cUnidad = rsSelec(2)
        desigResult = rsSelec(3)
    End If
    rsSelec.Close
    qrySelec.Close
    If cResultado = 0 Then 'el resultado no est� definido en Laboratorio: se puede poner un mensaje
        fInformarResultadoLabor = constCORRECTO
        Exit Function
    End If
    
    'se actualiza la tabla RESULTADOASISTENCIA
    SQL = "INSERT INTO resultadoAsistencia (historia, caso, secuencia, nRepeticion, fecha,"
    SQL = SQL & " hora, responsable, cResultado, resultadoAlfanumerico, cUnidad, estado)"
    SQL = SQL & " VALUES (?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?)"
    Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
    qryRes(0) = historia
    qryRes(1) = caso
    qryRes(2) = secuencia
    qryRes(3) = nrepeticion
    qryRes(4) = fecha
    qryRes(5) = hora
    qryRes(6) = responsable
    qryRes(7) = cResultado
    qryRes(8) = result
    qryRes(9) = cUnidad
    qryRes(10) = constRESULTADOVALIDADO
    qryRes.Execute
    If Err = 40002 Then
        Err = 0
        MsgBox "El resultado '" & desigResult & "' ya fue informado previamente y no se puede modificar su valor.", vbInformation, "Informar resultados"
        fInformarResultadoLabor = constOTRO
        Exit Function
    End If
    
    'se actualiza la tabla PRUEBAASISTENCIA
    SQL = "UPDATE pruebaAsistencia SET estado = ? WHERE nRef = ?"
    Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
    qryRes(0) = constPRUEBAVALIDADA
    qryRes(1) = nRef
    qryRes.Execute
    
    'se actualiza la tabla SEGUMIENTOPRUEBA
    SQL = "SELECT count(*) FROM seguimientoPrueba"
    SQL = SQL & " WHERE historia = ?"
    SQL = SQL & " AND caso = ?"
    SQL = SQL & " AND secuencia = ?"
    SQL = SQL & " AND nRepeticion = ?"
    SQL = SQL & " AND proceso = ?"
    Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
    qrySelec(0) = historia
    qrySelec(1) = caso
    qrySelec(2) = secuencia
    qrySelec(3) = nrepeticion
    qrySelec(4) = constPROCESOVALIDACION
    Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsSelec(0) = 0 Then
        SQL = "INSERT INTO seguimientoPrueba (historia, caso, secuencia, nRepeticion, proceso,"
        SQL = SQL & "fecha, hora, responsable,comentarios) VALUES (?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?)"
        Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
        qryRes(0) = historia
        qryRes(1) = caso
        qryRes(2) = secuencia
        qryRes(3) = nrepeticion
        qryRes(4) = constPROCESOVALIDACION
        qryRes(5) = fecha
        qryRes(6) = hora
        qryRes(7) = responsable
        qryRes(8) = constINFORMEPROVISIONAL
        qryRes.Execute
    Else
        SQL = "UPDATE seguimientoPrueba"
        SQL = SQL & " SET fecha = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
        SQL = SQL & " , hora = ?"
        SQL = SQL & " , responsable = ?"
        SQL = SQL & " WHERE historia = ?"
        SQL = SQL & " AND caso = ?"
        SQL = SQL & " AND secuencia = ?"
        SQL = SQL & " AND nRepeticion = ?"
        SQL = SQL & " AND proceso = ?"
        Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
        qryRes(0) = fecha
        qryRes(1) = hora
        qryRes(2) = responsable
        qryRes(3) = historia
        qryRes(4) = caso
        qryRes(5) = secuencia
        qryRes(6) = nrepeticion
        qryRes(7) = constPROCESOVALIDACION
        qryRes.Execute
    End If
    
    rsSelec.Close
    qrySelec.Close
    qryRes.Close
    
    If Err = 0 Then
        fInformarResultadoLabor = constCORRECTO
    Else
        fInformarResultadoLabor = constINCORRECTO
    End If

End Function

Public Function fInformarColoniaLabor(nRef$, cCol%, cMicro%, cGeMicro%, cMorf%, historia As Long, caso%, secuencia%, nrepeticion%, responsable%, colExiste As Boolean) As Integer
'Se anota la colonia informada en la base de datos de Laboratorio
'Se llama a esta funci�n antes de actualizar la base de datos de Micro para poder conocer _
cual era la anterior identificaci�n de la colonia que se hab�a informado. Hay que quitar _
de la BDLaboratorio la identificacion anterior y a�adir la nueva.
'Si la colonia ya existe (colExiste = True), s�lo habr� que eliminar la identificaci�n _
anterior (si hubiera) y no hace falta insertar la nueva identificaci�n ya que si estaba _
informada ya existir� y si s�lo estaba validada, seguir� estando validada, pero no informada.
'Los par�metros cMicro, cGeMicro y cMorf pasados por ventana corresponden a la identificaci�n _
nueva de la colonia.
    Dim qryRes As rdoQuery
    Dim rsSelec As rdoResultset, qrySelec As rdoQuery
    Dim SQL$, i%
    Dim fecha$, hora$
    Dim cResultadoAnterior%, cResultadoNuevo%
    
    fecha = fFechaHoraActual
    hora = Format(fecha, "HH:MM:SS")
    
    On Error Resume Next
    'se busca la anterior identificaci�n de la colonia que todav�a permanece en la base de _
    datos de Micro.
    cResultadoAnterior = 0
    SQL = "SELECT MB1800.cResultado, MB1100.cResultado, MB1200.cResultado"
    SQL = SQL & " FROM MB2700, MB1800, MB1100, MB1200"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    SQL = SQL & " AND MB36_codEstCol = ?"
    SQL = SQL & " AND MB1800.MB18_codMicro (+)= MB2700.MB18_codMicro"
    SQL = SQL & " AND MB1100.MB11_codGeMicro (+)= MB2700.MB11_codGeMicro"
    SQL = SQL & " AND MB1200.MB12_codMorf (+)= MB2700.MB12_codMorf"
    Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
    qrySelec(0) = nRef
    qrySelec(1) = cCol
    qrySelec(2) = constCOLINFORMADA
    Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsSelec.EOF = False Then
        If Not IsNull(rsSelec(0)) Then
            cResultadoAnterior = rsSelec(0)
        ElseIf Not IsNull(rsSelec(1)) Then
            cResultadoAnterior = rsSelec(1)
        ElseIf Not IsNull(rsSelec(2)) Then
            cResultadoAnterior = rsSelec(2)
        End If
    End If
    rsSelec.Close
    qrySelec.Close
    
    'se obtiene la identificaci�n actual de la colonia
    cResultadoNuevo = 0
    If colExiste = False Then
         If cMicro <> -1 Then
             SQL = "SELECT NVL(cResultado,0) FROM MB1800 WHERE MB18_codMicro = ?"
             Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
             qrySelec(0) = cMicro
             Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
             cResultadoNuevo = rsSelec(0)
             rsSelec.Close
             qrySelec.Close
         ElseIf cGeMicro <> -1 Then
             SQL = "SELECT NVL(cResultado,0) FROM MB1100 WHERE MB11_codGeMicro = ?"
             Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
             qrySelec(0) = cGeMicro
             Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
             cResultadoNuevo = rsSelec(0)
             rsSelec.Close
             qrySelec.Close
        ElseIf cMorf <> -1 Then
             SQL = "SELECT NVL(cResultado,0) FROM MB1200 WHERE MB12_codMorf = ?"
             Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
             qrySelec(0) = cMorf
             Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
             cResultadoNuevo = rsSelec(0)
             rsSelec.Close
             qrySelec.Close
         End If
    End If
    
    'se realizan las operaciones en la base de datos si ha cambiado la identificaci�n de la colonia
    If cResultadoAnterior <> cResultadoNuevo Then
        'BORRAR EL ANTERIOR
        If cResultadoAnterior > 0 Then
            SQL = "SELECT resultadoAlfanumerico FROM resultadoAsistencia"
            SQL = SQL & " WHERE historia = ?"
            SQL = SQL & " AND caso = ?"
            SQL = SQL & " AND secuencia = ?"
            SQL = SQL & " AND nRepeticion = ?"
            SQL = SQL & " AND cResultado = ?"
            Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
            qrySelec(0) = historia
            qrySelec(1) = caso
            qrySelec(2) = secuencia
            qrySelec(3) = nrepeticion
            qrySelec(4) = cResultadoAnterior
            Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If rsSelec.EOF = False Then
                If CInt(rsSelec(0)) > 1 Then
                    SQL = "UPDATE resultadoAsistencia"
                    SQL = SQL & " SET fecha = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
                    SQL = SQL & " , hora = ?"
                    SQL = SQL & " , responsable = ?"
                    SQL = SQL & " , resultadoAlfanumerico = ?"
                Else
                    SQL = "DELETE FROM resultadoAsistencia"
                End If
                SQL = SQL & " WHERE historia = ?"
                SQL = SQL & " AND caso = ?"
                SQL = SQL & " AND secuencia = ?"
                SQL = SQL & " AND nRepeticion = ?"
                SQL = SQL & " AND cResultado = ?"
                Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
                If CInt(rsSelec(0)) > 1 Then
                    qryRes(0) = fecha
                    qryRes(1) = hora
                    qryRes(2) = responsable
                    qryRes(3) = CInt(rsSelec(0)) - 1
                    i = 4
                Else
                    i = 0
                End If
                qryRes(0 + i) = historia
                qryRes(1 + i) = caso
                qryRes(2 + i) = secuencia
                qryRes(3 + i) = nrepeticion
                qryRes(4 + i) = cResultadoAnterior
                qryRes.Execute
                rsSelec.Close
                qrySelec.Close
                qryRes.Close
            End If
        End If
        
        If colExiste = False Then
            'A�ADIR EL NUEVO
            If cResultadoNuevo > 0 Then
                SQL = "SELECT resultadoAlfanumerico FROM resultadoAsistencia"
                SQL = SQL & " WHERE historia = ?"
                SQL = SQL & " AND caso = ?"
                SQL = SQL & " AND secuencia = ?"
                SQL = SQL & " AND nRepeticion = ?"
                SQL = SQL & " AND cResultado = ?"
                Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
                qrySelec(0) = historia
                qrySelec(1) = caso
                qrySelec(2) = secuencia
                qrySelec(3) = nrepeticion
                qrySelec(4) = cResultadoNuevo
                Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                If rsSelec.EOF = False Then
                    i = CInt(rsSelec(0))
                    SQL = "UPDATE resultadoAsistencia"
                    SQL = SQL & " SET fecha = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
                    SQL = SQL & " , hora = ?"
                    SQL = SQL & " , responsable = ?"
                    SQL = SQL & " , resultadoAlfanumerico = ?"
                    SQL = SQL & " , cUnidad = ?"
                    SQL = SQL & " WHERE historia = ?"
                    SQL = SQL & " AND caso = ?"
                    SQL = SQL & " AND secuencia = ?"
                    SQL = SQL & " AND nRepeticion = ?"
                    SQL = SQL & " AND cResultado = ?"
                    SQL = SQL & " AND estado = ?"
                Else
                    i = 0
                    SQL = "INSERT INTO resultadoAsistencia (fecha, hora, responsable, resultadoAlfanumerico,"
                    SQL = SQL & " cUnidad, historia, caso, secuencia, nRepeticion, cResultado, estado)"
                    SQL = SQL & " VALUES (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?,?,?,?,?)"
                End If
                Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
                qryRes(0) = fecha
                qryRes(1) = hora
                qryRes(2) = responsable
                qryRes(3) = i + 1
                qryRes(4) = constUnidadDefecto
                qryRes(5) = historia
                qryRes(6) = caso
                qryRes(7) = secuencia
                qryRes(8) = nrepeticion
                qryRes(9) = cResultadoNuevo
                qryRes(10) = constRESULTADOVALIDADO
                qryRes.Execute
                rsSelec.Close
                qrySelec.Close
                qryRes.Close
            End If
        End If
        
        'se actualiza la tabla PRUEBAASISTENCIA
        SQL = "UPDATE pruebaAsistencia SET estado = ? WHERE nRef = ?"
        Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
        qryRes(0) = constPRUEBAVALIDADA
        qryRes(1) = nRef
        qryRes.Execute
        
        'se actualiza la tabla SEGUMIENTOPRUEBA
        SQL = "SELECT count(*) FROM seguimientoPrueba"
        SQL = SQL & " WHERE historia = ?"
        SQL = SQL & " AND caso = ?"
        SQL = SQL & " AND secuencia = ?"
        SQL = SQL & " AND nRepeticion = ?"
        SQL = SQL & " AND proceso = ?"
        Set qrySelec = objApp.rdoConnect.CreateQuery("", SQL)
        qrySelec(0) = historia
        qrySelec(1) = caso
        qrySelec(2) = secuencia
        qrySelec(3) = nrepeticion
        qrySelec(4) = constPROCESOVALIDACION
        Set rsSelec = qrySelec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If rsSelec(0) = 0 Then
            SQL = "INSERT INTO seguimientoPrueba (historia, caso, secuencia, nRepeticion, proceso,"
            SQL = SQL & "fecha, hora, responsable, comentarios) VALUES (?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?)"
            Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
            qryRes(0) = historia
            qryRes(1) = caso
            qryRes(2) = secuencia
            qryRes(3) = nrepeticion
            qryRes(4) = constPROCESOVALIDACION
            qryRes(5) = fecha
            qryRes(6) = hora
            qryRes(7) = responsable
            qryRes(8) = constINFORMEPROVISIONAL
            qryRes.Execute
        Else
            SQL = "UPDATE seguimientoPrueba"
            SQL = SQL & " SET fecha = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
            SQL = SQL & " , hora = ?"
            SQL = SQL & " , responsable = ?"
            SQL = SQL & " WHERE historia = ?"
            SQL = SQL & " AND caso = ?"
            SQL = SQL & " AND secuencia = ?"
            SQL = SQL & " AND nRepeticion = ?"
            SQL = SQL & " AND proceso = ?"
            Set qryRes = objApp.rdoConnect.CreateQuery("", SQL)
            qryRes(0) = fecha
            qryRes(1) = hora
            qryRes(2) = responsable
            qryRes(3) = historia
            qryRes(4) = caso
            qryRes(5) = secuencia
            qryRes(6) = nrepeticion
            qryRes(7) = constPROCESOVALIDACION
            qryRes.Execute
        End If
        rsSelec.Close
        qrySelec.Close
        qryRes.Close
    End If
    
    If Err = 0 Then
        fInformarColoniaLabor = constCORRECTO
    Else
        fInformarColoniaLabor = constINCORRECTO
    End If
    
End Function
