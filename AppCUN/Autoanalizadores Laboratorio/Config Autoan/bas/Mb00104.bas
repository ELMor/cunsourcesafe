Attribute VB_Name = "modFunciones_01"

Public Function fDesignacionTecnica(cTecnica$) As String
    ' Recoge el c�digo de la t�cnica y devuelve la designaci�n
    Dim qryTec As rdoQuery
    Dim rsTec As rdoResultset
    Dim SQL As String
    
    SQL = "SELECT MB09_Desig FROM MB0900 WHERE MB09_CODTec =?"
    Set qryTec = objApp.rdoConnect.CreateQuery("DesigTecnica", SQL)
    qryTec.RowsetSize = 1
    qryTec(0) = cTecnica
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsPrueba.EOF Then
        If Not IsNull(rsTec!MB09_Desig) Then
            fDesignacionTecnica = rsTec!MB09_Desig
        End If
    End If
    rsTec.Close
    qryTec.Close
End Function

Public Function fEstadoPrueba(estado As Integer) As String
' Recibe un n�mero y devuelve la descripci�n del estado de la prueba que le corresponde

    Select Case estado
        Case constPRUEBASOLICITADA
            fEstadoPrueba = "Solicitada"
        Case constPRUEBAIMPRESA
            fEstadoPrueba = "Impresa"
        Case constPRUEBAEXTRAIDA
            fEstadoPrueba = "Extra�da"
        Case constPRUEBASOLICITUDREALIZ
            fEstadoPrueba = "Solic. Realizaci�n"
        Case constPRUEBAREALIZANDO
            fEstadoPrueba = "Realiz�ndose"
        Case constPRUEBARESULTADO
            fEstadoPrueba = "Resultado"
        Case constPRUEBARESULTANALIZ
            fEstadoPrueba = "Resultado"
        Case constPRUEBAVALIDADA
            fEstadoPrueba = "Firmada"
        Case constPRUEBAINSERTADA
            fEstadoPrueba = "Firmada"
        Case constPRUEBAENVIOPROV
            fEstadoPrueba = "Env�o Provisional"
        Case constPRUEBAENVIADA
            fEstadoPrueba = "Enviada"
        Case constPRUEBAANULADA
            fEstadoPrueba = "Eliminada"
        Case constPRUEBAVALIDTEC
            fEstadoPrueba = "Revisada"
        Case constPRUEBAMANUAL
            fEstadoPrueba = "Contestaci�n Manual"
    End Select
End Function

Public Function fDesignacionPrueba(cPrueba$) As String
    ' Recoge el c�digo de la prueba y devuelve la designaci�n
    Dim qryPrueba As rdoQuery
    Dim rsPrueba As rdoResultset
    Dim SQL As String
    
    SQL = "SELECT designacion FROM pruebas WHERE cPrueba =?"
    Set qryPrueba = objApp.rdoConnect.CreateQuery("Prueba", SQL)
    qryPrueba.RowsetSize = 1
    qryPrueba(0) = cPrueba
    Set rsPrueba = qryPrueba.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsPrueba.EOF Then
        If Not IsNull(rsPrueba!Designacion) Then
            fDesignacionPrueba = rsPrueba!Designacion
        End If
    End If
    rsPrueba.Close
    qryPrueba.Close
End Function

Public Function fLocalizarPlaca(nRef$, codOrig$) As String
    Dim SQL As String
    Dim qryPlaca As rdoQuery
    Dim rsPlaca As rdoResultset
    
    SQL = "SELECT MB20_CodPlaca FROM MB2000"
    SQL = SQL & " WHERE nRef=?"
    SQL = SQL & " AND MB20_CODTecAsist=?"
    Set qryPlaca = objApp.rdoConnect.CreateQuery("Placa", SQL)
    qryPlaca.RowsetSize = 1
    qryPlaca(0) = nRef
    qryPlaca(1) = codOrig
    
    Set rsPlaca = qryPlaca.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsPlaca.EOF Then
        If Not IsNull(rsPlaca!MB20_CODPlaca) Then
            fLocalizarPlaca = rsPlaca!MB20_CODPlaca
        End If
    End If
    rsPlaca.Close
    
End Function

Public Function fLocalizarPlacaOrigen(nRef$, codOrig$) As String
    Dim SQL As String
    Dim qryPlaca As rdoQuery
    Dim rsPlaca As rdoResultset
    
    SQL = "SELECT MB20_CodPlaca FROM MB2000"
    SQL = SQL & " WHERE "
    SQL = SQL & "   (nRef,MB20_CODTecAsist) IN "
    SQL = SQL & "   (SELECT nRef, MB20_CODTecAsist_Orig"
    SQL = SQL & "       FROM MB2000"
    SQL = SQL & "       WHERE nRef=?"
    SQL = SQL & "       AND MB20_CODTecAsist=?)"
    Set qryPlaca = objApp.rdoConnect.CreateQuery("Placa", SQL)
    qryPlaca.RowsetSize = 1
    qryPlaca(0) = nRef
    qryPlaca(1) = codOrig
    
    Set rsPlaca = qryPlaca.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsPlaca.EOF Then
        If Not IsNull(rsPlaca!MB20_CODPlaca) Then
            fLocalizarPlacaOrigen = rsPlaca!MB20_CODPlaca
        End If
    End If
    rsPlaca.Close

End Function

Public Function fLetraNumRef(cDptoSecc$, strDesigAntib$) As String
    ' Se obtiene la letra de la numeraci�n del n� de referencia correspondiente
    ' a la secci�n
    
    Dim SQL As String
    Dim qryLetra As rdoQuery
    Dim rsLetra As rdoResultset
    
    strDesigAntib = "Antibiograma"
    
    SQL = "SELECT MB41_CODNumRef,MB41_DESIGANTIB FROM MB4100 WHERE cDptoSecc =?"
    Set qryLetra = objApp.rdoConnect.CreateQuery("Letra", SQL)
    qryLetra.RowsetSize = 1
    qryLetra(0) = cDptoSecc
    Set rsLetra = qryLetra.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsLetra.EOF Then
        If Not IsNull(rsLetra!MB41_CODNumRef) Then
            fLetraNumRef = rsLetra!MB41_CODNumRef
        End If
        If Not IsNull(rsLetra!MB41_desigAntib) Then
            strDesigAntib = rsLetra!MB41_desigAntib
        End If
    End If
    rsLetra.Close
    qryLetra.Close

End Function
Public Function fIniciales(strTexto$) As String
    Dim intPosicion As Integer
    Dim strIniciales As String
    
    strIniciales = Mid(strTexto, 1, 1) & "."
    intPosicion = InStr(1, strTexto, " ")
    Do While intPosicion > 0
        strIniciales = strIniciales & Mid(strTexto, intPosicion + 1, 1) & "."
        intPosicion = InStr(intPosicion + 1, strTexto, " ")
    Loop
    fIniciales = strIniciales
End Function
Public Function fNumeroMicro(nRef$) As String
    Const constLongitudNRef = 7
    If nRef <> "" Then
        If Len(nRef) >= constLongitudNRef Then
            fNumeroMicro = Str$(Val(Right(nRef, constLongitudNRef)))
        Else
            fNumeroMicro = nRef
        End If
    Else
        fNumeroMicro = ""
    End If
End Function

Public Function fOrigenTecnica(tiOrigen%) As String

    Select Case tiOrigen
        Case constORIGENMUESTRA
            fOrigenTecnica = "Muestra"
        Case constORIGENPLACA
            fOrigenTecnica = "Cultivo"
        Case constORIGENTECNICA
            fOrigenTecnica = "T�cnica"
        Case constORIGENCOLONIA
            fOrigenTecnica = "Colonia"
    End Select
    
End Function

Public Function fUserDefectoCarpeta(cCarpeta$, nombreUser$) As String
    Dim SQL As String
    Dim qryCarpeta As rdoQuery
    Dim rsCarpeta As rdoResultset
    
    SQL = "SELECT MB1600.cUser, u.nombre||' '||u.Apellido1||' '||u.Apellido2 as nomUser"
    SQL = SQL & " FROM MB1600, users u"
    SQL = SQL & " WHERE u.cUser=MB1600.cUser"
    SQL = SQL & " AND MB1600.cCarpeta=?"
    
    Set qryCarpeta = objApp.rdoConnect.CreateQuery("Carpeta", SQL)
    qryCarpeta(0) = cCarpeta
    Set rsCarpeta = qryCarpeta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsCarpeta.EOF = False Then
        fUserDefectoCarpeta = rsCarpeta!cUser
        nombreUser = rsCarpeta!nomUser
    End If
    rsCarpeta.Close
    qryCarpeta.Close
    
End Function


Public Function fLocalizarTecnicaOrigen(nRef$, codOrig$) As String
    ' Recoge los c�digos origen y devuelve el nombre de la t�cnica
    
    Dim qryTec As rdoQuery
    Dim rsTec As rdoResultset
    Dim SQL As String
    
    SQL = "SELECT MB09_Desig FROM MB0900 WHERE MB09_CODTec ="
    SQL = SQL & "   (SELECT MB09_CODTec FROM MB2000"
    SQL = SQL & "    WHERE nRef=? AND MB20_CODTecAsist=?)"
    Set qryTec = objApp.rdoConnect.CreateQuery("DesigTecnica", SQL)
    qryTec.RowsetSize = 1
    qryTec(0) = nRef
    qryTec(1) = codOrig
    
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsPrueba.EOF Then
        If Not IsNull(rsTec!MB09_Desig) Then
            fLocalizarTecnicaOrigen = rsTec!MB09_Desig
        End If
    End If
    rsTec.Close
    qryTec.Close
    
End Function

Public Function fRazonProtocolo(cRazon%) As String
    ' Devuelve la descripci�n de por qu� se aplica el protocolo
    Select Case cRazon
        Case constAplicProtP     ' Por prueba
            fRazonProtocolo = "Prueba"
        Case constAplicProtM     ' Por muestra
            fRazonProtocolo = "Muestra"
        Case constAplicProtDpt   ' Por solicitante
            fRazonProtocolo = "Solic."
        Case constAplicProtP + constAplicProtM   ' Por prueba y muestra
            fRazonProtocolo = "Pr. + M."
        Case constAplicProtP + constAplicProtDpt   ' Por prueba y solicitante
            fRazonProtocolo = "Pr. + Solic."
        Case constAplicProtM + constAplicProtDpt  ' Por muestra y solicitante
            fRazonProtocolo = "M. + Solic."
        Case constAplicProtP + constAplicProtM + constAplicProtDpt ' Por prueba, muestra y solicitante
            fRazonProtocolo = "Pr. + M. + Solic."
        Case Else
            fRazonProtocolo = ""
    End Select
End Function
Public Function fBuscarProtocolo(protocolos() As typeProtocolo, cTipoPrueba$, cTipoMuestra$, cDpt$) As Integer
    ' Recoge el c�digo de la prueba, muestra y solicitante, y devuelve los protocolos aplicables
    Dim qryProtocolo As rdoQuery
    Dim rsProtocolo As rdoResultset
    Dim SQL As String
    Dim dimension As Integer
    Dim aplic As Integer
    
    On Error Resume Next
    Erase protocolos
    dimension = 0
'    SQL = "SELECT MB0500.MB05_CODProt, MB05_Desig, cPrueba,cTipoMuestra, cDpt"
'    SQL = SQL & " FROM MB0500,MB1000 "
'    SQL = SQL & " WHERE MB0500.MB05_CODProt = MB1000.MB05_CODProt"
'    SQL = SQL & " AND (cPrueba =? OR cTipoMuestra =? OR cDpt=?)"
'    SQL = SQL & " AND MB0500.cDptoSecc = ?"
'    SQL = SQL & " AND MB05_INDActiva=-1"
    
    SQL = "SELECT MB0500.MB05_CODProt, MB05_Desig, cPrueba,cTipoMuestra, cDpt"
    SQL = SQL & " FROM MB0500,MB1000 "
    SQL = SQL & " WHERE MB0500.MB05_CODProt = MB1000.MB05_CODProt"
    SQL = SQL & " AND ("
    'SQL = SQL & "   (cPrueba IS NULL AND cTipoMuestra IS NULL AND cDpt IS NULL)OR "
    SQL = SQL & "   (cPrueba IS NULL AND cTipoMuestra IS NULL AND cDpt = ? ) OR"
    SQL = SQL & "   (cPrueba IS NULL AND cTipoMuestra =? AND cDpt = ? ) OR"
    SQL = SQL & "   (cPrueba =? AND cTipoMuestra =? AND cDpt = ? ) OR"
    SQL = SQL & "   (cPrueba IS NULL AND cTipoMuestra =? AND cDpt IS NULL ) OR"
    SQL = SQL & "   (cPrueba =? AND cTipoMuestra =? AND cDpt IS NULL)OR "
    SQL = SQL & "   (cPrueba =? AND cTipoMuestra IS NULL AND cDpt IS NULL)OR "
    SQL = SQL & "   (cPrueba =? AND cTipoMuestra IS NULL AND cDpt =?) "
    SQL = SQL & " )"
    SQL = SQL & " AND MB0500.cDptoSecc = ?"
    SQL = SQL & " AND MB05_INDActiva=-1"
    
    Set qryProtocolo = objApp.rdoConnect.CreateQuery("Protocolo", SQL)
    qryProtocolo(0) = cDpt
    qryProtocolo(1) = cTipoMuestra
    qryProtocolo(2) = cDpt
    qryProtocolo(3) = cTipoPrueba
    qryProtocolo(4) = cTipoMuestra
    qryProtocolo(5) = cDpt
    qryProtocolo(6) = cTipoMuestra
    qryProtocolo(7) = cTipoPrueba
    qryProtocolo(8) = cTipoMuestra
    qryProtocolo(9) = cTipoPrueba
    qryProtocolo(10) = cTipoPrueba
    qryProtocolo(11) = cDpt
    qryProtocolo(12) = departamento
    Set rsProtocolo = qryProtocolo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsProtocolo.EOF
        aplic = 0
        ' Condici�n de prueba
        If Not IsNull(rsProtocolo!cPrueba) Then
            If rsProtocolo!cPrueba = cTipoPrueba Then
                aplic = aplic + constAplicProtP
            End If
        End If
        ' Condici�n de muestra
        If Not IsNull(rsProtocolo!cTipoMuestra) Then
            If rsProtocolo!cTipoMuestra = cTipoMuestra Then
                aplic = aplic + constAplicProtM
            End If
        End If
        ' Condici�n de departamento
        If Not IsNull(rsProtocolo!cDpt) Then
            If rsProtocolo!cDpt = cDpt Then
                aplic = aplic + constAplicProtDpt
            End If
        End If
                
        dimension = UBound(protocolos)
        dimension = dimension + 1
        ReDim Preserve protocolos(1 To dimension)
        protocolos(dimension).cProtocolo = rsProtocolo!MB05_CODProt
        protocolos(dimension).desig = rsProtocolo!MB05_Desig
        protocolos(dimension).aplicProt = aplic
        rsProtocolo.MoveNext
    Loop
    fBuscarProtocolo = dimension
    rsProtocolo.Close
    qryProtocolo.Close
End Function
Public Function fNumeroReferencia(cDptoSecc$) As String
    ' Recoge el c�digo de la secci�n y devuelve el n� de referencia subsiguiente
    Dim rsNRef As rdoResultset
    Dim SQL As String
 
    On Error Resume Next
    SQL = "SELECT NRef_" & letraNumRef & "_SEQUENCE.nextVal FROM DUAL"
    Set rsNRef = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
    If Err Then
        pRegenerarSequences False
        Set rsNRef = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
        fNumeroReferencia = letraNumRef & rsNRef(0)
        rsNRef.Close
    Else
        fNumeroReferencia = letraNumRef & rsNRef(0)
        rsNRef.Close
    End If

    
End Function

Public Function fAceptarPrueba(historia$, caso$, secuencia$, nrepeticion$, nRef$) As Integer
    ' Actualiza la prueba a realiz�ndose y asigna el n� de referencia
    Dim qryPrueba As rdoQuery
    Dim rsPrueba As rdoResultset
    Dim SQL As String
    
    On Error Resume Next
    SQL = "UPDATE pruebaAsistencia"
    SQL = SQL & " SET estado = " & constPRUEBAREALIZANDO
    SQL = SQL & " , NRef = ?"
    SQL = SQL & " WHERE historia = ?"
    SQL = SQL & " AND caso = ?"
    SQL = SQL & " AND secuencia = ?"
    SQL = SQL & " AND nRepeticion = ?"
    
    Set qryPrueba = objApp.rdoConnect.CreateQuery("Prueba", SQL)
    qryPrueba(0) = nRef
    qryPrueba(1) = historia
    qryPrueba(2) = caso
    qryPrueba(3) = secuencia
    qryPrueba(4) = nrepeticion
    Set rsPrueba = qryPrueba.OpenResultset(rdOpenKeyset)
    If Err = False Then
        fAceptarPrueba = True
    End If
    rsPrueba.Close
    qryPrueba.Close
    
End Function
Public Function fMedioActivo(nRef$, cTecAsist$) As String
    Dim SQL As String
    Dim qrySegTecnica As rdoQuery
    Dim rsSegTecnica As rdoResultset
        
    ' Se recoge el ambiente en el que se encuentra la t�cnica
    
    SQL = "SELECT MB02_CODCond FROM MB2600"
    SQL = SQL & " WHERE NRef = ?"
    SQL = SQL & " AND MB20_CODTecAsist = ?"
    SQL = SQL & " AND MB35_CODESTSegui = ?"
    
    Set qrySegTecnica = objApp.rdoConnect.CreateQuery("SegTecnica", SQL)
   
    qrySegTecnica(0) = nRef
    qrySegTecnica(1) = cTecAsist
    qrySegTecnica(2) = constSEGUITECVALIDA
    
    Set rsSegTecnica = qrySegTecnica.OpenResultset(rdOpenKeyset)
    If Not rsSegTecnica.EOF Then
        fMedioActivo = rsSegTecnica(0)
    End If
    rsSegTecnica.Close
    qrySegTecnica.Close
End Function

Public Sub pChequeoSequence()
    ' Chequea si los sequence corresponden al a�o actual. Si no es as� lo actualiza
    
    Dim SQL As String
    Dim qrySequence As rdoQuery
    Dim rsSequence As rdoResultset
    Dim strAnyo As String
    Dim strAnyoAnterior As String
    
    SQL = "SELECT MAX(NREF) "
    SQL = SQL & " FROM pruebaAsistencia"
    SQL = SQL & " WHERE cCarpeta IN (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = ?)"
    Set qrySequence = objApp.rdoConnect.CreateQuery("Sequence", SQL)
    qrySequence(0) = departamento
    Set rsSequence = qrySequence.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not IsNull(rsSequence(0)) Then
        strAnyoAnterior = rsSequence(0)
        strAnyoAnterior = Mid(strAnyoAnterior, 2, 4)
    Else
        strAnyoAnterior = "1997"
    End If
    qrySequence.Close
    
    strAnyo = Format$(fFechaActual, "yyyy")
    
    If Val(strAnyo) > Val(strAnyoAnterior) Then
        On Error Resume Next
        objApp.rdoConnect.BeginTrans
        SQL = "DROP SEQUENCE NRef_" & letraNumRef & "_SEQUENCE"
        Set qrySequence = objApp.rdoConnect.CreateQuery("Sequence", SQL)
        qrySequence.Execute
        If Err <> 0 Then
            objApp.rdoConnect.RollbackTrans
            qrySequence.Close
            Exit Sub
        End If
        qrySequence.Close
    
        SQL = "CREATE SEQUENCE NRef_" & letraNumRef & "_SEQUENCE"
        SQL = SQL & " INCREMENT BY 1"
        SQL = SQL & " START WITH " & strAnyo & "0000001"
        SQL = SQL & " MAXVALUE " & strAnyo & "9999999"
        SQL = SQL & " NOCACHE"
        
        Set qrySequence = objApp.rdoConnect.CreateQuery("Sequence", SQL)
        qrySequence.Execute
        If Err <> 0 Then
            objApp.rdoConnect.RollbackTrans
            qrySequence.Close
            Exit Sub
        End If
        qrySequence.Close
        objApp.rdoConnect.CommitTrans
    End If
End Sub

Public Sub pRegenerarSequences(blnPreguntar As Boolean)
    ' Reinicializa los sequence al a�o actual
    
    Dim SQL As String
    Dim qrySequence As rdoQuery
    Dim rsSequence As rdoResultset
    Dim strAnyo As String
    Dim strAnyoAnterior As String
    
    Screen.MousePointer = Hourglass

    SQL = "SELECT MAX(NREF) "
    SQL = SQL & " FROM pruebaAsistencia"
    SQL = SQL & " WHERE NREF LIKE  ? "
    Set qrySequence = objApp.rdoConnect.CreateQuery("Sequence", SQL)
    qrySequence(0) = letraNumRef & "%"
    Set rsSequence = qrySequence.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not IsNull(rsSequence(0)) Then
        strAnyoAnterior = rsSequence(0)
        strAnyoAnterior = Mid(strAnyoAnterior, 2, 4)
    Else
        strAnyoAnterior = "1997"
    End If
    qrySequence.Close
    
    strAnyo = Format$(fFechaActual, "yyyy")
    
    If Val(strAnyo) > Val(strAnyoAnterior) Then
        If blnPreguntar = False Then
            intRes = vbYes
        Else
            intRes = MsgBox("�Desea asignar los nuevos n�meros de Laboratorio para el a�o " & strAnyo & " en lugar de " & strAnyoAnterior & "?", vbQuestion + vbYesNo, "Actualizaci�n de n�meros de laboratorio")
        End If
        If intRes = vbYes Then
            On Error Resume Next
            objApp.rdoConnect.BeginTrans
            SQL = "DROP SEQUENCE NRef_" & letraNumRef & "_SEQUENCE"
            Set qrySequence = objApp.rdoConnect.CreateQuery("Sequence", SQL)
            qrySequence.Execute
            If Err <> 0 And blnPreguntar Then
                MsgBox "Actualizaci�n no realizada. Se ha producido un error en la eliminaci�n de " & "NRef_" & letraNumRef & "_SEQUENCE,", vbExclamation, "Actualizaci�n de n�meros de laboratorio"
                objApp.rdoConnect.RollbackTrans
                qrySequence.Close
                Screen.MousePointer = Default
                Exit Sub
            End If
            qrySequence.Close
        
            SQL = "CREATE SEQUENCE NRef_" & letraNumRef & "_SEQUENCE"
            SQL = SQL & " INCREMENT BY 1"
            SQL = SQL & " START WITH " & strAnyo & "0000001"
            SQL = SQL & " MAXVALUE " & strAnyo & "9999999"
            SQL = SQL & " NOCACHE"
            
            Set qrySequence = objApp.rdoConnect.CreateQuery("Sequence", SQL)
            Err = 0
            qrySequence.Execute
            If Err <> 0 Then
                MsgBox "Actualizaci�n no realizada. Se ha producido un error en la creaci�n de " & "NRef_" & letraNumRef & "_SEQUENCE. Contacte con el supervisor del sistema.", vbExclamation, "Actualizaci�n de n�meros de laboratorio"
                objApp.rdoConnect.RollbackTrans
                qrySequence.Close
                Screen.MousePointer = Default
                Exit Sub
            End If
            qrySequence.Close
            objApp.rdoConnect.CommitTrans
        End If
    Else
        MsgBox "No se puede realizar la Actualizaci�n. Se est� intentando pasar de un a�o (" & strAnyoAnterior & ") igual o posterior al nuevo (" & strAnyo & "). Contacte con el supervisor del sistema.", vbExclamation, "Actualizaci�n de n�meros de laboratorio"
    End If
    Screen.MousePointer = Default
End Sub

