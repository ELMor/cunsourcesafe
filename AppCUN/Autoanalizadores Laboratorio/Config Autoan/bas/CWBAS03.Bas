Attribute VB_Name = "Resources"
Option Explicit

' constante para el icono de codewizard
Public Const cwMsgNoIcon As Long = 101

Public Const cwImageNoCopy As Long = 102
Public Const cwImageNoCut As Long = 103
Public Const cwImageNoDelete As Long = 104
Public Const cwImageNoEdit As Long = 105
Public Const cwImageNoExecute As Long = 106
Public Const cwImageNoExit As Long = 107
Public Const cwImageNoFilterOff As Long = 108
Public Const cwImageNoFilterOn As Long = 109
Public Const cwImageNoFind As Long = 110
Public Const cwImageNoFirst As Long = 111
Public Const cwImageNoForeign As Long = 112
Public Const cwImageNoLast As Long = 113
Public Const cwImageNoMaint As Long = 114
Public Const cwImageNoNew As Long = 115
Public Const cwImageNoNext As Long = 116
Public Const cwImageNoOpen As Long = 117
Public Const cwImageNoOrder As Long = 118
Public Const cwImageNoFileClose As Long = 119
Public Const cwImageNoFileOpen As Long = 120
Public Const cwImageNoPaste As Long = 121
Public Const cwImageNoPrevious As Long = 122
Public Const cwImageNoPrint As Long = 123
Public Const cwImageNoQuery As Long = 124
Public Const cwImageNoRefresh As Long = 125
Public Const cwImageNoSave As Long = 126
Public Const cwImageNoUndo As Long = 127
Public Const cwImageNoValueOff As Long = 128
Public Const cwImageNoValueOn As Long = 129
Public Const cwImageNoViewSQL As Long = 130


' constantes para mensajes internos de CW
Public Const cwMsgUsrAccess As Long = 10000
Public Const cwMsgUsrCreateCursor As Long = 10001
Public Const cwMsgUsrExist As Long = 10002
Public Const cwMsgUsrNotExist As Long = 10003
Public Const cwMsgUsrWrite As Long = 10004
Public Const cwMsgUsrDelete As Long = 10005
Public Const cwMsgUsrValidate As Long = 10006
Public Const cwMsgUsrDeleteQuery As Long = 10007
Public Const cwMsgUsrRestoreQuery As Long = 10008
Public Const cwMsgUsrSaveQuery As Long = 10009
Public Const cwMsgUsrSearchNotInfo As Long = 10010
Public Const cwMsgUsrSearchNotFound As Long = 10011
Public Const cwMsgUsrFilterQueryDelete As Long = 10012
Public Const cwMsgUsrFilterNotFound As Long = 10013
Public Const cwMsgUsrFilterError As Long = 10014
Public Const cwMsgUsrFilterNotRegs As Long = 10015
Public Const cwMsgUsrSysInfoNotFound As Long = 10016
Public Const cwMsgUsrInstallDetected As Long = 10017
Public Const cwMsgUsrInstallError As Long = 10018
Public Const cwMsgUsrFilterMaxSize As Long = 10019

Public Const cwMsgInternalError As Long = 10020


' constantes para los mensajes de usuario
Public Const cwMsgUserMessage As Long = 11000
Public Const cwMsgUserQuery As Long = 11001
Public Const cwMsgUserCrystal As Long = 11002


' constantes del c�digo
Public Const cwMsgViewCaption As Long = 15000
Public Const cwMsgRegistros As Long = 15001
Public Const cwMsgNewSession As Long = 15002
Public Const cwMsgReportPath As Long = 15003


' strings de la columna de estado del multilinea
Public Const cwMsgCaptionLoaded As Long = 15100
Public Const cwMsgCaptionAdded As Long = 15101
Public Const cwMsgCaptionDeleted As Long = 15102
Public Const cwMsgCaptionModified As Long = 15103


' constantes para filtros
Public Const cwMsgKeyAscending As Long = 15200
Public Const cwMsgKeyDescending As Long = 15201


' deliminator para las descripciones corta | larga
Public Const cwMsgCharDelimiter As Long = 15300


' constantes para la StatusBar del formulario
Public Const cwMsgStbPanDescription As Long = 15400
Public Const cwMsgStbPanForeign As Long = 15401
Public Const cwMsgStbPanStatus As Long = 15402
Public Const cwMsgStbPanDate As Long = 15403
Public Const cwMsgStbPanTime As Long = 15404
Public Const cwMsgStbPanForeignMsg As Long = 15405
Public Const cwMsgStbPanStatusMsgEdit As Long = 15406
Public Const cwMsgStbPanStatusMsgNewCode As Long = 15407
Public Const cwMsgStbPanStatusMsgNewData As Long = 15408
Public Const cwMsgStbPanStatusMsgOpen As Long = 15409


' constantes para los Tool Tip
Public Const cwMsgToolTipNew As Long = 15500
Public Const cwMsgToolTipOpen As Long = 15501
Public Const cwMsgToolTipSave As Long = 15502
Public Const cwMsgToolTipPrint As Long = 15503
Public Const cwMsgToolTipDelete As Long = 15504
Public Const cwMsgToolTipCut As Long = 15505
Public Const cwMsgToolTipCopy As Long = 15506
Public Const cwMsgToolTipPaste As Long = 15507
Public Const cwMsgToolTipUndo As Long = 15508
Public Const cwMsgToolTipSearch As Long = 15509
Public Const cwMsgToolTipFilterOn As Long = 15510
Public Const cwMsgToolTipFilterOff As Long = 15511
Public Const cwMsgToolTipFirst As Long = 15512
Public Const cwMsgToolTipPrevious As Long = 15513
Public Const cwMsgToolTipNext As Long = 15514
Public Const cwMsgToolTipLast As Long = 15515
Public Const cwMsgToolTipRefresh As Long = 15516
Public Const cwMsgToolTipMaint As Long = 15517
Public Const cwMsgToolTipExit As Long = 15518

Public Const cwMsgToolTipNewFilter As Long = 15519
Public Const cwMsgToolTipOpenFilter As Long = 15520
Public Const cwMsgToolTipSaveFilter As Long = 15521
Public Const cwMsgToolTipOrderFilter As Long = 15522
Public Const cwMsgToolTipViewSQLFilter As Long = 15523
Public Const cwMsgToolTipHideSQLFilter As Long = 15524
Public Const cwMsgToolTipOpenViewFilter As Long = 15525
Public Const cwMsgToolTipExecuteFilter As Long = 15526
Public Const cwMsgToolTipExitFilter As Long = 15527


' caption de las columnas internas del multilinea
Public Const cwMsgColumnStatus As Long = 15600
Public Const cwMsgColumnBookmark As Long = 15601
Public Const cwMsgColumnDescription As Long = 15602


' valores del combo de filtros
Public Const cwMsgFilterComienza As Long = 15700
Public Const cwMsgFilterContiene As Long = 15701
Public Const cwMsgFilterIgual As Long = 15702
Public Const cwMsgFilterNoContiene As Long = 15703
Public Const cwMsgFilterDistinto As Long = 15704


' constante que identifica el valor de los errores internos de c�digo
Public Const cwFatalError As Long = 20000


' errores internos de c�digo
Public Const cwMsgClsFormAddOrderField As Long = 20100
Public Const cwMsgClsFormAddOrderFunction As Long = 20101
Public Const cwMsgClsFormAddRelation As Long = 20102
Public Const cwMsgClsFormGetCaps As Long = 21103

Public Const cwMsgClsPrinterNotExist As Long = 20200
Public Const cwMsgClsPrinterAdd As Long = 20201
Public Const cwMsgClsPrinterRemove As Long = 20202
Public Const cwMsgClsPrinterGetDesc As Long = 20203
Public Const cwMsgClsPrinterGetFileName As Long = 20204
Public Const cwMsgClsPrinterGetCopies As Long = 20205
Public Const cwMsgClsPrinterGetDestination As Long = 20206
Public Const cwMsgClsPrinterGetEnabled As Long = 20207
Public Const cwMsgClsPrinterGetName As Long = 20208
Public Const cwMsgClsPrinterSetCopies As Long = 20209
Public Const cwMsgClsPrinterSetDestination As Long = 20210
Public Const cwMsgClsPrinterSetReport As Long = 20211
Public Const cwMsgClsPrinterShow As Long = 20212
Public Const cwMsgClsPrinterSetValues As Long = 20213

Public Const cwMsgClsAppCreateInfo As Long = 20300
Public Const cwMsgClsAppRemoveInfo As Long = 20301
Public Const cwMsgClsAppCreateImageList As Long = 20302
Public Const cwMsgClsAppNewVersion As Long = 20303
Public Const cwMsgClsAppRegister As Long = 20304
Public Const cwMsgClsAppChangeColors As Long = 20305
Public Const cwMsgClsAppCheckInstance As Long = 20306
Public Const cwMsgClsAppGetCWVersion As Long = 20307
Public Const cwMsgClsAppGetClientVersion As Long = 20308
Public Const cwMsgClsAppHelpContext As Long = 20309
Public Const cwMsgClsAppAbout As Long = 20310

Public Const cwMsgClsGenLoadCombo As Long = 20400
Public Const cwMsgClsGenViewList As Long = 20401
Public Const cwMsgClsGenChangeIcon As Long = 20402

Public Const cwMsgClsPipeSet As Long = 20500
Public Const cwMsgClsPipeGet As Long = 20501
Public Const cwMsgClsPipeRemove As Long = 20502

Public Const cwMsgClsLogWrite As Long = 20600

Public Const cwMsgClsDataBaseAddEnv As Long = 20700
Public Const cwMsgClsDataBaseAddValue As Long = 20701
Public Const cwMsgClsDataBaseGetValue As Long = 20702

Public Const cwMsgClsConfigLoad As Long = 20800
Public Const cwMsgClsConfigSave As Long = 20801

Public Const cwMsgClsCtrlSet As Long = 20900
Public Const cwMsgClsCtrlGet As Long = 20901
Public Const cwMsgClsCtrlChangeColor As Long = 20902
Public Const cwMsgClsCtrlGotFocus As Long = 20903
Public Const cwMsgClsCtrlLostFocus As Long = 20904
Public Const cwMsgClsCtrlDataChange As Long = 20905
Public Const cwMsgClsCtrlPrepare As Long = 20906
Public Const cwMsgClsCtrlCreateInfo As Long = 20907
Public Const cwRESERVADO1 As Long = 20908
Public Const cwRESERVADO2 As Long = 20909
Public Const cwMsgClsCtrlUseClipboard As Long = 20910
Public Const cwMsgClsCtrlEnabled As Long = 20911
Public Const cwMsgClsCtrlStabilize As Long = 20912
Public Const cwMsgClsCtrlSetMask As Long = 20913
Public Const cwMsgClsCtrlIsData As Long = 20914
Public Const cwMsgClsCtrlIsLinked As Long = 20915
Public Const cwMsgClsCtrlIsNegotiated As Long = 20916
Public Const cwMsgClsCtrlIsContainer As Long = 20917
Public Const cwMsgClsCtrlGetKey As Long = 20918
Public Const cwMsgClsCtrlGetCtrl As Long = 20919
Public Const cwMsgClsCtrlNotInfo As Long = 20920
Public Const cwMsgClsCtrlSetFocus As Long = 20921
Public Const cwMsgClsCtrlBlock As Long = 20922
Public Const cwMsgClsCtrlRestore As Long = 20923
Public Const cwRESERVADO3 As Long = 20924
Public Const cwMsgClsCtrlAddLinked As Long = 20925
Public Const cwMsgClsCtrlCreateLinked As Long = 20926
Public Const cwMsgClsCtrlSearchLinked As Long = 20927

Public Const cwMsgClsWinCreateInfo As Long = 21000
Public Const cwMsgClsWinRemoveInfo As Long = 21001
Public Const cwMsgClsWinRegister As Long = 21002
Public Const cwMsgClsWinDeRegister As Long = 21003
Public Const cwMsgClsWinCreateToolBar As Long = 21004
Public Const cwMsgClsWinCreateStatusBar As Long = 21005
Public Const cwMsgClsWinExit As Long = 21006
Public Const cwMsgClsWinPrepareScr As Long = 21007
Public Const cwMsgClsWinProcess As Long = 21008
Public Const cwMsgClsWinStabilize As Long = 21009
Public Const cwMsgClsWinGetCaps As Long = 21010
Public Const cwMsgClsWinIsReady As Long = 21011

Public Const cwMsgClsDataSetColumn As Long = 21100
Public Const cwMsgClsDataGetColumn As Long = 21101
Public Const cwMsgClsDataMoveFirst As Long = 21102
Public Const cwMsgClsDataMoveLast As Long = 21103
Public Const cwMsgClsDataMovePrevious As Long = 21104
Public Const cwMsgClsDataMoveNext As Long = 21105
Public Const cwMsgClsDataLocate As Long = 21106
Public Const cwMsgClsDataOpen As Long = 21107
Public Const cwMsgClsDataNew As Long = 21108
Public Const cwMsgClsDataRefresh As Long = 21109
Public Const cwMsgClsDataSearch As Long = 21110
Public Const cwMsgClsDataSave As Long = 21111
Public Const cwMsgClsDataWrite As Long = 21112
Public Const cwMsgClsDataRestore As Long = 21113
Public Const cwMsgClsDataRestoreLine As Long = 21114
Public Const cwMsgClsDataDelete As Long = 21115
Public Const cwMsgClsDataDeleteLine As Long = 21116
Public Const cwMsgClsDataRead As Long = 21117
Public Const cwMsgClsDataValidate As Long = 21118
Public Const cwMsgClsDataGetWhere As Long = 21119
Public Const cwMsgClsDataGetOrder As Long = 21120
Public Const cwMsgClsDataCreateKeys As Long = 21121
Public Const cwMsgClsDataCreateCursor As Long = 21122
Public Const cwMsgClsDataRemoveCursor As Long = 21123
Public Const cwMsgClsDataCreateStatement As Long = 21124
Public Const cwMsgClsDataRemoveStatement As Long = 21125
Public Const cwMsgClsDataSearchKeys As Long = 21126
Public Const cwMsgClsDataCompareColumns As Long = 21127
Public Const cwMsgClsDataCenterCursor As Long = 21128
Public Const cwMsgClsDataGetValue As Long = 21129
Public Const cwMsgClsDataCreateFunc As Long = 21130
Public Const cwMsgClsDataCreateWhere As Long = 21131

Public Const cwMsgClsFormCreateInfo As Long = 21200
Public Const cwMsgClsFormRemoveInfo As Long = 21201
Public Const cwMsgClsFormEmptyValue As Long = 21202
Public Const cwMsgClsFormDefaultValue As Long = 21203
Public Const cwMsgClsFormChangeColor As Long = 21204
Public Const cwMsgClsFormNextPrev As Long = 21205
Public Const cwMsgClsFormShowFilter As Long = 21206
Public Const cwMsgClsFormRemoveFilter As Long = 21207
Public Const cwMsgClsFormChangeActive As Long = 21208
Public Const cwMsgClsFormAddInfo As Long = 21209
Public Const cwMsgClsFormRefreshNextLevels As Long = 21210
Public Const cwMsgClsFormChangeTab As Long = 21211
Public Const cwMsgClsFormCreateFilterOrder As Long = 21212
Public Const cwMsgClsFormCreateFilterWhere As Long = 21213
Public Const cwMsgClsFormChangeStatus As Long = 21214
Public Const cwMsgClsFormIsCancel As Long = 21215
Public Const cwMsgClsFormCreateChildList As Long = 21216
Public Const cwMsgClsFormCreateContainerList As Long = 21217
Public Const cwMsgClsFormPrinterDialog As Long = 21218

Public Const cwMsgClsGridLoad As Long = 21300
Public Const cwMsgClsGridError As Long = 21301
Public Const cwMsgClsGridUnboundRead As Long = 21302
Public Const cwMsgClsGridChangeRowCol As Long = 21303
Public Const cwMsgClsGridAddColumn As Long = 21304
Public Const cwMsgClsGridCreate As Long = 21305
Public Const cwMsgClsGridDblClick As Long = 21306
Public Const cwMsgClsGridRefresh As Long = 21307

Public Const cmMsgFrmUserFormLoad As Long = 21400
Public Const cmMsgFrmUserAcceptClick As Long = 21401
Public Const cmMsgFrmUserAdvancedClick As Long = 21402
Public Const cmMsgFrmUserCancelClick As Long = 21403

Public Const cmMsgFrmSearchFormLoad As Long = 21500
Public Const cmMsgFrmSearchFormUnload As Long = 21501
Public Const cmMsgFrmSearchButtonClick As Long = 21502
Public Const cmMsgFrmSearchFindCursor As Long = 21503
Public Const cmMsgFrmSearchFindSQL As Long = 21504
Public Const cmMsgFrmSearchFindGrid As Long = 21505
Public Const cmMsgFrmSearchViewSQL As Long = 21506
Public Const cmMsgFrmSearchCheck As Long = 21507

Public Const cwRESERVADO4 As Long = 21600
Public Const cmMsgFrmViewFormActivate As Long = 21601
Public Const cmMsgFrmViewAcceptClick As Long = 21602

Public Const cmMsgFrmColorsFormLoad As Long = 21700
Public Const cmMsgFrmColorsColorClick As Long = 21701
Public Const cmMsgFrmColorsAcceptClick As Long = 21702
Public Const cmMsgFrmColorsCancelClick As Long = 21703
Public Const cmMsgFrmColorsRestoreClick As Long = 21704

Public Const cmMsgFrmAboutFormLoad As Long = 21800

Public Const cmMsgFrmSaveFilterFormLoad As Long = 21900

Public Const cmRESERVADO4 As Long = 22000
Public Const cmMsgFrmOrderFormActivate As Long = 22001
Public Const cmMsgFrmOrderDownClick As Long = 22002
Public Const cmMsgFrmOrderUpClick As Long = 22003
Public Const cmMsgFrmOrderMakeStable As Long = 22004

Public Const cmMsgFrmPrinterFormLoad As Long = 22100
Public Const cmMsgFrmPrinterLoadReports As Long = 22101
Public Const cmMsgFrmPrinterReportsClick As Long = 22102
Public Const cmMsgFrmPrinterAcceptClick As Long = 22103
Public Const cmMsgFrmPrinterSetupClick As Long = 22104
Public Const cmMsgFrmPrinterCancelClick As Long = 22105

Public Const cmMsgFrmFilterFormLoad As Long = 22200
Public Const cmMsgFrmFilterFormActivate As Long = 22201
Public Const cmMsgFrmFilterFormUnload As Long = 22202
Public Const cmMsgFrmFilterGridBtnClick As Long = 22203
Public Const cmMsgFrmFilterRedraw As Long = 22204
Public Const cmMsgFrmFilterNew As Long = 22205
Public Const cmMsgFrmFilterSave As Long = 22206
Public Const cmMsgFrmFilterShowSQL As Long = 22207
Public Const cmMsgFrmFilterOrder As Long = 22208
Public Const cmMsgFrmFilterView As Long = 22209
Public Const cmMsgFrmFilterExecute As Long = 22210
Public Const cmMsgFrmFilterEmpty As Long = 22211
Public Const cmMsgFrmFilterCreateWhere As Long = 22212
Public Const cmMsgFrmFilterCreateOrder As Long = 22213
Public Const cmMsgFrmFilterLoadOrder As Long = 22214
Public Const cmMsgFrmFilterLoadValues As Long = 22215
Public Const cmMsgFrmFilterChangeCollection As Long = 22216
Public Const cmMsgFrmFilterCount As Long = 22217

