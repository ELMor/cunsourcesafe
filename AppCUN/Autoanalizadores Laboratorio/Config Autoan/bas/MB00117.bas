Attribute VB_Name = "modGrid"
Option Explicit

Public Sub pFormatearGrid(grdSSGrid As SSDBGrid)
    Dim i As Integer
    Dim localizado As Boolean
    
    ' Formatea el grid que se pasa por ventana
    
    For i = 0 To grdSSGrid.StyleSets.Count - 1
        If grdSSGrid.StyleSets(i).Name = "Selected" Then
            localizado = True
            Exit For
        End If
    Next i
    If localizado = False Then grdSSGrid.StyleSets.Add "Selected"
    
    For i = 0 To grdSSGrid.StyleSets.Count - 1
        If grdSSGrid.StyleSets(i).Name = "Selected" Then
            grdSSGrid.StyleSets(i).BackColor = vbHighlight
            grdSSGrid.StyleSets(i).ForeColor = vbHighlightText
        End If
    Next i
    grdSSGrid.ActiveRowStyleSet = "Selected"
    
    grdSSGrid.AllowAddNew = False
    grdSSGrid.AllowColumnMoving = False
    grdSSGrid.AllowColumnShrinking = False
    grdSSGrid.AllowColumnSizing = True
    grdSSGrid.AllowColumnSwapping = False
    grdSSGrid.AllowDelete = False
'    grdSSGrid.AllowDragDrop=false
    grdSSGrid.AllowGroupMoving = False
    grdSSGrid.AllowGroupShrinking = False
    grdSSGrid.AllowGroupSizing = False
    grdSSGrid.AllowGroupSwapping = False
    grdSSGrid.AllowRowSizing = False
    grdSSGrid.AllowUpdate = False
    grdSSGrid.BackColor = vbApplicationWorkspace
    grdSSGrid.BackColorEven = vbApplicationWorkspace
    grdSSGrid.BackColorOdd = vbApplicationWorkspace
    grdSSGrid.DividerStyle = ssDividerStyleRaised
    grdSSGrid.DividerType = ssDividerTypeNone
    grdSSGrid.ForeColorEven = vbWindowText
    grdSSGrid.RecordSelectors = False
    grdSSGrid.SelectByCell = False
    grdSSGrid.SelectTypeCol = ssSelectionTypeNone
    grdSSGrid.SelectTypeRow = ssSelectionTypeSingleSelect
End Sub
