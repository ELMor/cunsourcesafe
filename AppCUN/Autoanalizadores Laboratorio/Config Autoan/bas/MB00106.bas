Attribute VB_Name = "modEstructuras"
Option Explicit
Public Type typeProtocolo   ' Estructura donde se guardan los datos de los protocolos
    cProtocolo As Integer       'C�digo del Protocolo
    desig As String             ' Designaci�n corta del protocolo
    aplicProt As Integer        ' Raz�n por la que se aplica el protocolo:  Muestra, Prueba , solicitante, o combinaci�n de ambas
End Type

Public Type typeTecProt   ' Estructura donde se guardan los datos de las t�cnicas de los protocolos
    codTecAsist As Integer       'C�digo de la t�cnica asistencia
    codTec As Integer            'C�digo de la t�cnica
    codPlaca As Integer          'C�digo de la placa si se trata de un cultivo
    tiOrig As Integer            'Tipo de origen de la t�cnica
    codOrig As String            'C�digo de origen de la t�cnica
End Type


Public Type typeTecMesa    'Estructura utilizada en el monatje de la mesa
    nRef As String              ' N� de referencia
    codTecAsist As Integer      ' C�digo de la t�cnica asistencia
    codPlaca As Integer         ' N� de placa, si se trata de un cultivo
    desigTecnica As String      ' Designaci�n de la t�cnica
    desigCondicion As String    ' Designaci�n de la condici�n ambiental en la que se encuentra
    colocado As Boolean         ' Indica si la placa ya se ha colocado o no
End Type

Public Type typeMesa        ' Estructura utilizada para guardar el mapa de la mesa en su montaje
    nRef As String              ' N� de referencia
    nTec As Integer             ' N� de t�cnicas que le corresponden
    nTecCol As Integer          ' N� de t�cnicas colocadas
    estado As Integer           ' Estado:
End Type

Public Type typeInfAntibElecCol ' Estructura utilizada en la impresi�n del antibiograma en los informes finales, se utiliza dentro de la estructura anterior, para enumerar los resultados de colonias-antibi�ticos
    cCol As Integer             ' C�digo de la colonia
    result As String            ' Reacci�n
    conc As String              ' Concentraci�n
    columna As Integer          ' Columna en la que se incorpora el resultado
End Type

Public Type typeInfAntibElec ' Estructura utilizada en la impresi�n del antibiograma en los informes finales, se utiliza dentro de la estructura anterior, para enumerar los antibi�ticos de cada elecci�n
    cAntib As Integer           ' C�digo de antibi�tico
    antib As String             ' Nombre del Antibi�tico
    infConc As Boolean          ' Indica si hay que informar concentraciones
    arCol() As typeInfAntibElecCol ' Colonias
End Type

Public Type typeInfAntib    ' Estructura utilizada en la impresi�n del antibiograma en los informes finales
    cElec As Integer            ' C�digo de elecci�n de Antibi�ticos
    elec As String              ' Nombre de la elecci�n
    nCol As Integer             ' N� de colonias con antibi�ticos de la elecci�n
    posicion As Integer         ' Posici�n en eje x donde comienza a escribirse la columna en la impresora
    arAntib() As typeInfAntibElec ' Antibi�ticos
End Type
    
    
Public Type typeMorfologias   ' Estructura donde se guardan los datos de las morfolog�as
    cMorf As Integer            ' C�digo de la morfolog�a
    desig As String             ' Designaci�n corta de la morfolog�a
    numLoc As Integer           ' n� de veces que se ha localizado la morfolog�a por observaci�n directa
    numCol As Integer           ' n� de colonias encontradas de la morfolog�a
End Type

Public Type typeColonias   ' Estructura donde se guardan los datos de las colonias
    cCol As Integer             ' C�digo de la colonia
    cMorf As Integer            ' C�digo de la morfolog�a
    asig As Boolean             ' si la colonia ha sido asignada a una morfolog�a encontradas por observaci�n directa
End Type

Public Type typeAntibioticos   ' Estructura donde se guardan los datos de los antibioticos
    cAnti As Integer            ' C�digo del antibiotico
    desig As String             ' designacion del antibiotico
End Type

Public Type typeReaccionAntibiotico 'Estructura donde se guardan datos de la respuesta de microorganismos frente a antibi�ticos
    cAnti As Integer                    'c�digo del antibi�tico
    cReac As Integer                    'c�digo de la reacci�n o sensibilidad
    desigReac As String                 'designaci�n de la sensibilidad
    conc As String                      'concentraci�n del antibi�tico
    recom As Integer                    'recomendaci�n
    infConc As Integer                  'informar la concentraci�n
    cTecAsist As Integer                'c�digo Tecnica-Asistencia de la t�cnica de antibiograma
    accion As String
End Type

Public Type typeResultados 'Estructura donde se guardan los resultados de t�cnicas
    cTec As Integer             'c�digo de la t�cnica
    cResult As Integer          'c�digo del resultado
    result As String            'valor del resultado
End Type

Public Type typeEtiquetas 'Estructura donde se guardan las etiquetas a imprimir
    strNRef As String           ' N� de Referencia
    intCodPlaca As Integer      ' C�digo del cultivo
    strFecha As String          ' Fecha de Siembra
    strHistoria As String       ' Historia
    strPaciente As String       ' Nombre del paciente
    strTecnica As String        ' Nombre de la t�cnica
    strCondicion As String      ' Condici�n Ambiental de la t�cnica
    strMuestra As String        ' Tipo de Muestra
    strcCol As String           ' C�digo de colonia
    strcTecAsist As String      ' C�digo de t�cnica-asistencia
    blnAutoanalizador As Boolean ' Indica si se realiza en autoanalizador
End Type

Public Type typeVitekAntib 'Estructura donde se guardan los datos de los antibi�ticos del Vitek
    cAnti As String         'c�digo del antibi�tico
    conc As String          'concentraci�n del antibi�tico
    sensib As String        'sensibilidad del antibi�tico
End Type

Public Type typeVitekTec        'Estructura donde se guardan los resultados del Vitek
    cTec As String                'c�digo de la t�cnica
    cGrupoTest As Integer         'c�digo del grupo de test devuelto por Vitek (Vitek ID, Vitek Susc,...)
    cCol As Integer               'c�digo de la colonia
    cMicro As String              'c�digo del microorganismo
    bionum As String              'bion�mero
    antib() As typeVitekAntib     'matriz que contiene los antibi�ticos
End Type

