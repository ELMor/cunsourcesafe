Attribute VB_Name = "modAutoAnalizador"
Option Explicit

Public Function fConfig_IO(cAutoAnalizador As Integer, comm As MSComm) As Boolean
'Establece los parámetros de Entrada/Salida de los puertos serie del ordenador para que estén
'conformes a los parámetros de los autoanalizadores.
    Dim SQL As String
    Dim rsAuto As rdoResultset, qryAuto As rdoQuery
   
    fConfig_IO = True
    
    'Se leen las características de la configuración I/O del autoanalizador y se establecen para el Comm.
    SQL = "SELECT puertoSerie, baudRate, parity, dataBits, stopBits" ', designacion"
    SQL = SQL & " FROM autoanalizadores"
    SQL = SQL & " WHERE cAutoAnalizador = ?"
    Set qryAuto = objApp.rdoConnect.CreateQuery("", SQL)
    qryAuto(0) = cAutoAnalizador
    Set rsAuto = qryAuto.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsAuto.EOF = False Then
        comm.CommPort = rsAuto("puertoserie")
        'frmPrincipal.Comm1.Notification = 1  'Modo Event Driven; 0 - Modo Polling   Ref:Q101944
        comm.Settings = rsAuto("baudRate") & "," & rsAuto("parity") & "," & rsAuto("dataBits") & "," & rsAuto("stopBits")
        'frmPrincipal.Comm1.PortOpen = True
        'frmPrincipal.panPuerto.Caption = "Puerto Serie " & frmPrincipal.Comm1.CommPort & ": Abierto"
    Else
        fConfig_IO = False
    End If
    rsAuto.Close
    qryAuto.Close

End Function

