Attribute VB_Name = "modImprimirEtiqueta"
Option Explicit

Private Function fCambiarCodigoImpresora(ByRef strTexto$) As String
    ' Recoge eltexto a imprimir, y cambia los caracteres
    ' especiales castellanos a la codificaci�n de la impresora.
    ' Se utiliza el character set 850 de 8 bits.
    
    ' Se cambian:
    '               � -> chr$(160)
    '               � -> chr$(161)
    '               � -> chr$(162)
    '               � -> chr$(163)
    '               � -> chr$(164)
    '               � -> chr$(165)
    '               � -> chr$(166)
    '               � -> chr$(167)
    '               � -> chr$(168)
    '               � -> chr$(169)
    '               � -> chr$(173)
    '               � -> chr$(181)
    '               � -> chr$(210)
    '               � -> chr$(214)
    '               � -> chr$(224)
    '               � -> chr$(233)
    
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(160), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(130), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(161), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(162), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(163), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(164), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(165), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(166), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(167), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(168), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(172), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(181), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(210), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(214), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(224), 0)
    strTexto = objGen.ReplaceStr(strTexto, "�", Chr$(233), 0)
    
    fCambiarCodigoImpresora = strTexto
End Function

Sub pImprimirEtiqueta(strNRef$, intcPlaca%)
    Dim crlf$, texto$
    Dim strcPlaca$ 'n� de placa formateado con 3 d�gitos
    Dim intNRef% 'n� de referencia formateado al valor
    
    crlf = Chr$(13) & Chr$(10)
    strcPlaca = Left$("000", 3 - Len(CStr(intcPlaca))) & CStr(intcPlaca)
    intNRef = Val(Mid$(strNRef, 6))
    
    'Texto a enviar a la impresora
    'configuracion
    texto = crlf
    texto = texto & "N" & crlf 'vaciar el buffer
    texto = texto & "I8,1,034" & crlf 'configuraci�n lenguaje
    texto = texto & "Q60,24" & crlf  'largo etiqueta = 60; separaci�n entre etiquetas = 24
    texto = texto & "R280,0" & crlf 'punto de referencia
    texto = texto & "S2" & crlf 'nivel de velocidad
    texto = texto & "D8" & crlf 'nivel de color
    texto = texto & "ZB" & crlf 'sentido de impresion de la etiqueta
    'texto
    texto = texto & "B20,0,0,1,2,3,50,N," & Chr$(34) & strNRef & strcPlaca & Chr$(34) & crlf
    texto = texto & "A20,60,0,4,1,1,N," & Chr$(34) & "N" & Chr$(167) & " Ref.: " & intNRef & Chr$(34) & crlf
    texto = texto & "A20,90,0,4,1,1,N," & Chr$(34) & "N" & Chr$(167) & " Placa: " & intcPlaca & Chr$(34) & crlf
    'orden de impresi�n
    texto = texto & "P1" & crlf 'N� de etiquetas; debe ir al final
    texto = texto & " " & crlf
        
    Call pImpresoraDefecto("Etiquetas")
    Printer.Print texto
    Printer.EndDoc
    Call pImpresoraDefecto("Recepcion")

End Sub

Sub pImprimirEtiquetas(arEtiquetas() As typeEtiquetas)
    Dim crlf$, texto$
    Dim strcPlaca$ 'n� de placa formateado con 3 d�gitos
    Dim intNRef% 'n� de referencia formateado al valor
    Dim intDimension As Integer
    Dim i As Integer
    Dim strID As Integer
    Dim strCodigoBarras As String
    Dim strcTecAsist As String
    
    
    On Error Resume Next
    intDimension = UBound(arEtiquetas)
    If intDimension = 0 Then
        Exit Sub
    End If
    Call pImpresoraDefecto("Etiquetas")
    crlf = Chr$(13) & Chr$(10)
    
    
        'texto
    'Texto a enviar a la impresora
    'configuracion

    
    For i = 1 To intDimension
        strcTecAsist = arEtiquetas(i).strcTecAsist
        If arEtiquetas(i).blnAutoanalizador Then
            strcTecAsist = Trim(strcTecAsist)
            Do While Len(strcTecAsist) < 3
                strcTecAsist = "0" & strcTecAsist
            Loop
            strCodigoBarras = Right(arEtiquetas(i).strNRef, Len(arEtiquetas(i).strNRef) - 3) & strcTecAsist & Chr$(10) & arEtiquetas(i).strcCol
            strID = arEtiquetas(i).strcCol
            intNRef = Val(Mid$(arEtiquetas(i).strNRef, 6))
        Else
            strcPlaca = Left$("000", 3 - Len(CStr(arEtiquetas(i).intCodPlaca))) & CStr(arEtiquetas(i).intCodPlaca)
            intNRef = Val(Mid$(arEtiquetas(i).strNRef, 6))
            strCodigoBarras = arEtiquetas(i).strNRef & strcPlaca
            strID = arEtiquetas(i).intCodPlaca
        End If

        'Printer.Print texto
        
        texto = crlf
        texto = texto & "N" & crlf 'vaciar el buffer
        texto = texto & "I8,1,034" & crlf 'configuraci�n lenguaje
        texto = texto & "Q60,24" & crlf  'largo etiqueta = 60; separaci�n entre etiquetas = 24
        texto = texto & "R250,0" & crlf 'punto de referencia
        texto = texto & "S2" & crlf 'nivel de velocidad
        texto = texto & "D8" & crlf 'nivel de color
        texto = texto & "ZB" & crlf 'sentido de impresion de la etiqueta
        
        texto = texto & "B10,0,0,1,2,3,25,N," & Chr$(34) & strCodigoBarras & Chr$(34) & crlf
        texto = texto & "A10,30,0,4,1,1,N," & Chr$(34) & intNRef & "/" & strID & Chr$(34) & crlf
        texto = texto & "A10,60,0,2,1,1,N," & Chr$(34) & "(" & arEtiquetas(i).strFecha & ")" & Chr$(34) & crlf
        texto = texto & "A10,75,0,2,1,1,N," & Chr$(34) & fCambiarCodigoImpresora(Left(arEtiquetas(i).strTecnica, 15)) & " " & fCambiarCodigoImpresora(Left(arEtiquetas(i).strCondicion, 10)) & Chr$(34) & crlf
        texto = texto & "A10,93,0,2,1,1,N," & Chr$(34) & fCambiarCodigoImpresora(Left(arEtiquetas(i).strMuestra, 15)) & Chr$(34) & crlf
        texto = texto & "A10,110,0,2,1,1,N," & Chr$(34) & arEtiquetas(i).strHistoria & ": " & fCambiarCodigoImpresora(arEtiquetas(i).strPaciente) & Chr$(34) & crlf
        
        'orden de impresi�n
        texto = texto & "P1" & crlf 'N� de etiquetas; debe ir al final
        texto = texto & " " & crlf
        
        Printer.Print texto
        'If i Mod 10    = 0 Then
            Printer.EndDoc
            'pPausa 2
        'End If
    
    Next i

    'Printer.EndDoc
    
    Call pImpresoraDefecto("Recepcion")

End Sub

Sub pImpresoraDefecto(nombre As String)
    Dim X As Printer
    
    For Each X In Printers
      If InStr(UCase(X.DeviceName), UCase(nombre)) > 0 Then
        ' La define como predeterminada del sistema.
        Set Printer = X
        ' Sale del bucle.
        Exit For
      End If
    Next
    
End Sub

Private Sub pPausa(intSegundos As Integer)
    Dim varInicio As Variant

    varInicio = Now
    Do While Val(Format$((Now - varInicio), "s")) < 1
        DoEvents
    Loop
End Sub
