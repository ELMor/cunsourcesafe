Attribute VB_Name = "modPeticionTecnica"
Option Explicit

    Public Function fBorrarSeguimientoTecnica(nRef$, codTecAsist$, Optional codPaso$) As Boolean
    ' Funci�n que elimina los registros de la tabla MB2600.
    ' Los registros que se eliminen depender� de los par�metros
    ' que se suministren

    Dim SQL As String
    Dim qrySegTec As rdoQuery
    Dim rsSegTec As rdoResultset
    
    SQL = "DELETE FROM MB2600"
    SQL = SQL & " WHERE nRef =?"
    SQL = SQL & " AND MB20_codTecAsist =?"
    If codPaso <> "" Then
        SQL = SQL & " AND MB26_codPaso=?"
    End If
    
    On Error Resume Next
    
    Set qrySegTec = objApp.rdoConnect.CreateQuery("SegTec", SQL)
    qrySegTec(0) = nRef
    qrySegTec(1) = codTecAsist
    If codPaso <> "" Then
        qrySegTec(2) = codPaso
    End If
    
    Set rsSegTec = qrySegTec.OpenResultset(rdOpenKeyset)
    If Err = 0 Then
        fBorrarSeguimientoTecnica = True
    End If
    rsSegTec.Close
    qrySegTec.Close
    
End Function
Public Function fGenerarTecnica(nRef$, cTec$, cCond$, nVeces%, tipoOrigen$, origen$, cMuestra$, cultivo As Boolean, arTecProt() As typeTecProt, Optional codTecOrig$, Optional siembra As Boolean) As Boolean
    Dim qryTecAsist As rdoQuery
    Dim rsTecAsist As rdoResultset
    Dim qryTecResult As rdoQuery
    Dim rsTecResult As rdoResultset
    Dim qryTecResultAsist As rdoQuery
    Dim rsTecResultAsist As rdoResultset
    
    Dim SQL As String
    Dim res As Boolean
    Dim i As Integer
    Dim nTec As Integer
    Dim qrySeg As rdoQuery
    Dim rsSeg As rdoResultset
    Dim nPlaca As Integer
    Dim dimension As Integer
    
    On Error Resume Next
    'Se recoge el correlativo de t�cnica dentro del n� de referencia
    nTec = fNumTecAsist(nRef, nPlaca)
    
    ' Se define el 'statement' de inserci�n de las t�cnicas
    SQL = "INSERT INTO MB2000"
    SQL = SQL & "(nRef, cMuestra,MB20_CODTecAsist, MB20_TiOrig, MB20_CODOrig"
    SQL = SQL & ", MB09_CodTec, MB34_CODEstTecAsist,MB20_codPlaca,MB20_INDSiembra"
    If codTecOrig <> "" Then
        SQL = SQL & " ,MB20_CODTecAsist_Orig"
    End If
    SQL = SQL & ")"
    SQL = SQL & " VALUES "
    SQL = SQL & "(?,?,?,?,?,?,?,?,?"
    If codTecOrig <> "" Then
        SQL = SQL & ",?"
    End If
    SQL = SQL & ")"
    Set qryTecAsist = objApp.rdoConnect.CreateQuery("TecAsist", SQL)

    ' Se define el 'statement' de inserci�n de las condiciones de las t�cnicas
    SQL = "INSERT INTO MB2600"
    SQL = SQL & "(nRef, MB20_CODTecAsist, MB26_CODPaso, MB02_CODCond"
    SQL = SQL & ", MB35_CODEstSegui)"
    SQL = SQL & " VALUES "
    SQL = SQL & "(?,?,?,?,?)"
    Set qrySeg = objApp.rdoConnect.CreateQuery("SegTec", SQL)
        
        
    ' Se define el 'statement' de lectura de los resultados de las t�cnicas
    SQL = "SELECT "
    SQL = SQL & "       MB32_CODRESULT, cUnidad "
    SQL = SQL & " FROM "
    SQL = SQL & "       MB3200"
    SQL = SQL & " WHERE "
    SQL = SQL & "       MB09_CODTEC = ?"
    SQL = SQL & "       AND MB32_INDACTIVA=-1"
    Set qryTecResult = objApp.rdoConnect.CreateQuery("TecResult", SQL)
    
    ' Se define el 'statement' de inserci�n de los resultados de las t�cnicas en asistencia
    SQL = "INSERT INTO MB3300"
    SQL = SQL & " (nRef,MB20_CODTECAsist,MB09_CODTec,MB32_CODResult,cUnidad,MB38_CODEstResult)"
    SQL = SQL & " VALUES "
    SQL = SQL & " (?,?,?,?,?,?)"
    Set qryTecResultAsist = objApp.rdoConnect.CreateQuery("TecResultAsist", SQL)
    
    For i = 1 To nVeces
        dimension = UBound(arTecProt)
        dimension = dimension + 1
        ReDim Preserve arTecProt(1 To dimension)
        
        ' Se genera el registro en t�cnica asistencia en estado=1
        nTec = nTec + 1
        qryTecAsist(0) = nRef
        qryTecAsist(1) = cMuestra
        qryTecAsist(2) = nTec
        qryTecAsist(3) = tipoOrigen
        qryTecAsist(4) = origen
        qryTecAsist(5) = cTec
        qryTecAsist(6) = constTECSOLICITADA
        If cultivo = True Then
            nPlaca = nPlaca + 1
            qryTecAsist(7) = nPlaca
        Else
            qryTecAsist(7) = 0
        End If
        If siembra = True Then
            qryTecAsist(8) = -1
        Else
            qryTecAsist(8) = 0
        End If
        If codTecOrig <> "" Then
            qryTecAsist(9) = codTecOrig
        End If
        Err = 0
        Set rsTecAsist = qryTecAsist.OpenResultset(rdOpenKeyset)
        If Err = 0 Then
            ' Se genera el registro de seguimiento de t�cnica en estado=1 y paso=1
            ' ya que es el primer ambiente por el que pasa
            qrySeg(0) = nRef
            qrySeg(1) = nTec
            qrySeg(2) = 1
            qrySeg(3) = cCond
            qrySeg(4) = constSEGUITECVALIDA
            Err = 0

            Set rsSeg = qrySeg.OpenResultset(rdOpenKeyset)
            If Err <> 0 Then
                Exit For
            End If
        Else
            Exit For
        End If
        arTecProt(dimension).codTecAsist = nTec
        arTecProt(dimension).codTec = cTec
        If cultivo = True Then
            arTecProt(dimension).codPlaca = nPlaca
        End If
        arTecProt(dimension).tiOrig = tipoOrigen
        arTecProt(dimension).codOrig = origen
        
        ' Se generan los registros de resultados
        qryTecResult(0) = cTec
        Set rsTecResult = qryTecResult.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        Do While Not rsTecResult.EOF
            qryTecResultAsist(0) = nRef
            qryTecResultAsist(1) = nTec
            qryTecResultAsist(2) = cTec
            qryTecResultAsist(3) = rsTecResult!MB32_CODResult
            qryTecResultAsist(4) = rsTecResult!cUnidad
            qryTecResultAsist(5) = constRESULTSOLICITADO
            Set rsTecResultAsist = qryTecResultAsist.OpenResultset(rdOpenKeyset)
            rsTecResult.MoveNext
        Loop
        rsTecResult.Close
    Next i
    If Err = 0 Then
        fGenerarTecnica = True
    End If
    rsTecAsist.Close
    qryTecAsist.Close
    rsSeg.Close
    qrySeg.Close
End Function
Public Function fGenerarTecProt(nRef$, protocolos$, tipoOrigen$, origen$, cMuestra$, arTecProt() As typeTecProt, Optional codTecOrig$, Optional siembra As Boolean)
    ' Se recogen el n� de referencia y los c�digos de los protocolos a aplicar
    ' Se obtienen las t�cnicas correspondientes a los protocolos
    
    Dim qryProt As rdoQuery
    Dim rsProt As rdoResultset
    Dim SQL As String
    Dim res As Boolean
    Dim cultivo As Boolean
    Dim intAplicar As Boolean
    
    res = True
    If protocolos <> "" Then
        Erase arTecProt
        ' Se recogen las t�cnicas
        SQL = "SELECT MB1400.MB09_CODTec,MB02_CODCond,MB14_NUMVeces, MB04_CODTITEC FROM MB1400,MB0900 "
        SQL = SQL & " WHERE MB0900.MB09_CODTEC=MB1400.MB09_CODTEC"
        SQL = SQL & " AND MB05_CODProt IN (" & protocolos & ")"
        SQL = SQL & " AND MB14_INDActiva=-1"
        Set qryProt = objApp.rdoConnect.CreateQuery("Protocolo", SQL)
        'qryProt(0).Type = rdTypeVARCHAR
        'qryProt(0) = protocolos
        Err = 0
        Set rsProt = qryProt.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If Err = False Then
            Do While Not rsProt.EOF
                ' Se mira si se puede aplicar el tipo de t�cnica en el origen correspondiente
                intAplicar = fPosibleAplicarTecnica(tipoOrigen, rsProt!MB04_CODTITEC)
                If intAplicar = True Then
                    cultivo = False
                    If Val(rsProt!MB04_CODTITEC) = constCULTIVO Then cultivo = True
                    res = fGenerarTecnica(nRef, rsProt!MB09_CODTec, rsProt!MB02_CODCond, rsProt!MB14_NUMVeces, tipoOrigen, origen, cMuestra, cultivo, arTecProt(), codTecOrig, siembra)
                    If res = False Then
                        Exit Do
                    End If
                End If
                rsProt.MoveNext
            Loop
        Else
            res = False
        End If
        fGenerarTecProt = res
        rsProt.Close
        qryProt.Close
    End If
End Function

Public Function fGenerarTecCult(nRef$, cTecAsist$, tipoOrigen$, origen$, cMuestra$, arTecProt() As typeTecProt)
    ' Se recogen el n� de referencia y los c�digos de los cultivos (ctecasist) a los que se aplican t�cnicas
    ' Se obtienen las t�cnicas correspondientes a los cultivos
    
    Dim qryProt As rdoQuery
    Dim rsProt As rdoResultset
    Dim SQL As String
    Dim res As Boolean
    Dim cultivo As Boolean
    Dim intAplicar As Boolean
    
    Dim numVeces As Integer
    Dim cCond As Integer
    
    numVeces = 1
    cCond = 1
    res = True
    If cTecAsist <> "" Then
        Erase arTecProt
        ' Se recogen las t�cnicas
'        SQL = "SELECT MB4300.MB09_CODTec_T, MB04_CODTITEC FROM MB4300,MB0900 "
'        SQL = SQL & " WHERE MB0900.MB09_CODTEC=MB4300.MB09_CODTEC_T"
'        SQL = SQL & " AND MB09_CODTEC_C =? "
'        SQL = SQL & " AND MB09_INDActiva=-1"
        SQL = "SELECT MB4300.MB09_CODTec_T, MB04_CODTITEC FROM MB4300,MB0900,MB2000"
        SQL = SQL & " WHERE nRef = ?"
        SQL = SQL & " AND MB20_codTecAsist = ?"
        SQL = SQL & " AND MB4300.MB09_codTec_C = MB2000.MB09_codTec"
        SQL = SQL & " AND MB0900.MB09_codTec = MB4300.MB09_codTec_T"
        SQL = SQL & " AND MB09_INDActiva = -1"
        Set qryProt = objApp.rdoConnect.CreateQuery("Protocolo", SQL)
        qryProt(0) = nRef
        qryProt(1) = cTecAsist
        Err = 0
        Set rsProt = qryProt.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If Err = False Then
            Do While Not rsProt.EOF
                ' Se mira si se puede aplicar el tipo de t�cnica en el origen correspondiente
                intAplicar = fPosibleAplicarTecnica(tipoOrigen, rsProt!MB04_CODTITEC)
                If intAplicar = True Then
                    cultivo = False
                    If Val(rsProt!MB04_CODTITEC) = constCULTIVO Then cultivo = True
                    res = fGenerarTecnica(nRef, rsProt!MB09_CODTec_T, CStr(cCond), numVeces, tipoOrigen, origen, cMuestra, cultivo, arTecProt(), cTecAsist)
                    If res = False Then
                        Exit Do
                    End If
                End If
                rsProt.MoveNext
            Loop
        Else
            res = False
        End If
        fGenerarTecCult = res
        rsProt.Close
        qryProt.Close
    End If
End Function

Public Function fPosibleAplicarTecnica(tipoOrigen$, tiTec$) As Boolean
    ' Comprueba si es posible aplicar un tipo de t�cnica sobre un origen
    
    Select Case Val(tipoOrigen)
        Case constORIGENMUESTRA     ' Si el origen es una muestra
            ' Se permiten: cultivo, observaci�n directa, bioqu�micas
            Select Case Val(tiTec)
                Case constIDENTIFICACION, constANTIBIOGRAMA
                    fPosibleAplicarTecnica = False
                Case Else
                    fPosibleAplicarTecnica = True
            End Select
        Case constORIGENPLACA       ' Si el origen es un cultivo
            ' Se permiten pedir: cultivos
            Select Case Val(tiTec)
                Case constCULTIVO
                    fPosibleAplicarTecnica = True
                Case Else
                    fPosibleAplicarTecnica = False
            End Select
        Case constORIGENTECNICA     ' Si el origen es una t�cnica
            ' No se permite nada
            fPosibleAplicarTecnica = False
        Case constORIGENCOLONIA     ' Si el origen es una colonia
            ' Se permite: cultivos, identificaci�n, antibiograma, bioqu�micas
            Select Case Val(tiTec)
                Case constOBSERVDIRECTA
                    fPosibleAplicarTecnica = False
                Case Else
                    fPosibleAplicarTecnica = True
            End Select
    End Select
            
End Function
Public Function fNumTecAsist(nRef$, nPlaca%) As Integer
    Dim qryTecAsist As rdoQuery
    Dim rsTecAsist As rdoResultset
    Dim SQL As String

    'Se obtiene el correlativo de t�cnica dentro del n� de referencia y
    ' el correlativo de cultivo dentro del n� de referencia
    SQL = "SELECT MAX(MB20_CODTecAsist),MAX(MB20_codPlaca) FROM MB2000"
    SQL = SQL & " WHERE nRef = ?"
    Set qryTecAsist = objApp.rdoConnect.CreateQuery("TecAsist", SQL)
    qryTecAsist(0) = nRef
    Set rsTecAsist = qryTecAsist.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsTecAsist.RowCount <> 0 Then
        If Not IsNull(rsTecAsist(0)) Then
            fNumTecAsist = rsTecAsist(0)
        Else
            fNumTecAsist = 0
        End If
        If Not IsNull(rsTecAsist(1)) Then
            nPlaca = rsTecAsist(1)
        Else
            nPlaca = 0
        End If
    Else
        fNumTecAsist = 0
    End If
    rsTecAsist.Close
    qryTecAsist.Close

End Function

