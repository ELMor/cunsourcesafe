Attribute VB_Name = "modPrincipal"
Option Explicit
Public tipoAccesoDoctor As Boolean  ' Indica si se accede a la pantalla de petciiones con deerechos de Doctor
Public departamento As String       ' Indica el departamento/Secci�n para la cual se est� trabajando

Sub pAccesoPeticion()

    frmG_Identificacion.Show vbModal
    If frmG_Identificacion.LoginSucceeded = True Then
        tipoAccesoDoctor = True
    Else
        tipoAccesoDoctor = False
    End If
    Unload frmG_Identificacion
    frmP_Peticion.Show vbModal

End Sub
