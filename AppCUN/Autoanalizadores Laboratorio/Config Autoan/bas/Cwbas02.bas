Attribute VB_Name = "PrivateConstants"
Option Explicit


' posibles valores de retorno en una validaci�n
Public Const cwValidationOk As Byte = 0
Public Const cwValidationNullError As Byte = 1
Public Const cwValidationDataError As Byte = 2
Public Const cwValidationRangeError As Byte = 3


' modos de la columna de estado en un multilinea (operaciones booleanas)
Public Const cwMultiLineLoaded As Byte = 1
Public Const cwMultiLineAdded As Byte = 2
Public Const cwMultiLineModified As Byte = 4
Public Const cwMultiLineDeleted As Byte = 8


' caption de las columnas internas del multilinea
Public Const cwColumnStatus As String = "<Modo>"
Public Const cwColumnBookmark As String = "<BookMark>"
Public Const cwColumnDescription As String = "<Estado>"


' strings de la columna de estado del multilinea
Public Const cwCaptionLoaded As String = "Le�do"
Public Const cwCaptionAdded As String = "A�adido"
Public Const cwCaptionDeleted As String = "Borrado"
Public Const cwCaptionModified As String = "Modificado"


' constantes para filtros
Public Const cwKeyAscending As String = " (ASC)  "
Public Const cwKeyDescending As String = " (DESC) "


' deliminator para las descripciones corta | larga
Public Const cwCharDelimiter As String = "|"


' ancho de un caracter en twips
Public Const cwCharWidth As Byte = 120


' constantes para mensajes internos de CW
Public Const cwMsgAccess As Byte = 0
Public Const cwMsgCreateCursor As Byte = 1
Public Const cwMsgExist As Byte = 2
Public Const cwMsgNotExist As Byte = 3
Public Const cwMsgWrite As Byte = 4
Public Const cwMsgDelete As Byte = 5
Public Const cwMsgValidate As Byte = 6
Public Const cwMsgDeleteNotRegs As Byte = 7
Public Const cwMsgDeleteQuery As Byte = 8
Public Const cwMsgRestoreNotRegs As Byte = 9
Public Const cwMsgRestoreQuery As Byte = 10
Public Const cwMsgSaveQuery As Byte = 11
Public Const cwMsgSearchNotInfo As Byte = 12
Public Const cwMsgSearchNotFound As Byte = 13
Public Const cwMsgFilterQueryDelete As Byte = 14
Public Const cwMsgFilterNotFound As Byte = 15
Public Const cwMsgFilterError As Byte = 16
Public Const cwMsgFilterNotRegs As Byte = 17
Public Const cwMsgSysInfoNotFound As Byte = 18
Public Const cwMsgInstallDetected As Byte = 19
Public Const cwMsgInstallError As Byte = 20


' constantes para el uso del registro de windows
Public Const cwWinRegStringZ As Long = 1
Public Const cwWinRegDWORD As Long = 4
Public Const cwWinRegHKEYCLASSESROOT As Long = &H80000000
Public Const cwWinRegHKEYCURRENTUSER As Long = &H80000001
Public Const cwWinRegHKEYLOCALMACHINE As Long = &H80000002
Public Const cwWinRegHKEYUSERS As Long = &H80000003
Public Const cwWinRegERRORNONE As Integer = 0
Public Const cwWinRegERRORBADDB As Integer = 1
Public Const cwWinRegERRORBADKEY As Integer = 2
Public Const cwWinRegERRORCANTOPEN As Integer = 3
Public Const cwWinRegERRORCANTREAD As Integer = 4
Public Const cwWinRegERRORCANTWRITE As Integer = 5
Public Const cwWinRegERROROUTOFMEMORY As Integer = 6
Public Const cwWinRegERRORINVALIDPARAMETER As Integer = 7
Public Const cwWinRegERRORACCESSDENIED As Integer = 8
Public Const cwWinRegERRORINVALIDPARAMETERS As Integer = 87
Public Const cwWinRegERRORNOMOREITEMS As Integer = 259
Public Const cwWinRegKEYALLACCESS As Integer = &H3F
Public Const cwWinRegOPTIONNONVOLATILE As Integer = 0


' constantes para la StatusBar del formulario
Public Const cwStatusBarPanelDescription As String = "Description"
Public Const cwStatusBarPanelForeign As String = "Foreign"
Public Const cwStatusBarPanelStatus As String = "Status"
Public Const cwStatusBarPanelDate As String = "Date"
Public Const cwStatusBarPanelTime As String = "Time"


' constantes para las im�genes
Public Const cwImageNew As String = "i1"
Public Const cwImageOpen As String = "i2"
Public Const cwImageSave As String = "i3"
Public Const cwImagePrint As String = "i4"
Public Const cwImageDelete As String = "i5"
Public Const cwImageCut As String = "i6"
Public Const cwImageCopy As String = "i7"
Public Const cwImagePaste As String = "i8"
Public Const cwImageUndo As String = "i9"
Public Const cwImageFind As String = "i10"
Public Const cwImageFilterOn As String = "i11"
Public Const cwImageFilterOff As String = "i12"
Public Const cwImageExit As String = "i13"
Public Const cwImageFirst As String = "i14"
Public Const cwImagePrevious As String = "i15"
Public Const cwImageNext As String = "i16"
Public Const cwImageLast As String = "i17"
Public Const cwImageRefresh As String = "i18"
Public Const cwImageEdit As String = "i19"
Public Const cwImageOrder As String = "i20"
Public Const cwImageExecute As String = "i21"
Public Const cwImageQuery As String = "i22"
Public Const cwImageViewSQL As String = "i23"
Public Const cwImageForeign As String = "i24"
Public Const cwImageMaint As String = "i25"
Public Const cwImageValueOff As String = "i26"
Public Const cwImageValueOn As String = "i27"
Public Const cwImageFileClose As String = "i28"
Public Const cwImageFileOpen As String = "i29"


' constante para el tama�o de bloque de I/O en columnas LONG
Public Const cwLongDataBlockSize As Integer = 16384


' constantes para el manejo del clipboard
Public Const cwClipboardDelete As Byte = 0
Public Const cwClipboardCut As Byte = 1
Public Const cwClipboardCopy As Byte = 2
Public Const cwClipboardPaste As Byte = 3
Public Const cwClipboardUndo As Byte = 4


' constantes para el tama�o del cursor
Public Const cwCursorSizeDef As Integer = 50
Public Const cwCursorSizeMax As Integer = 1000


' constantes para los valores de retorno de la localizaci�n
' din�mica del cursor
Public Const cwSearchOk As Byte = 0
Public Const cwSearchNotFound As Byte = 1
Public Const cwSearchMore As Byte = 2

