Attribute VB_Name = "modHojasTrabajo"
Option Explicit

Const margenIzquierdo = 1000
Const margenDerecho = 1000
Const constSaltoLinea = 200
Const constSaltoParrafo = 600

Const constSaltoPagina = 15500

Public Function fImprimirHojaRepeticion(cCarpeta$, cUser$, fechaImpresion$, blnSiembras As Boolean) As Integer
    ' Se recogen la carpeta a imprimir, el usuario que imprimi� y la fecha de impresi�n anterior
    
    Dim SQL As String
    Dim qryTec As rdoQuery
    Dim rsTec As rdoResultset
    Dim nRef As String
    Dim res As Boolean
    Dim reali As String
    Dim reali1 As String
    Dim cTecOrig As String
    Dim arEtiquetas() As typeEtiquetas
    Dim cAutoanalizador As String

    Screen.MousePointer = vbHourglass
    ' Para las t�cnicas se obtiene lo siguiente:
    '       - N� Referencia
    '       - C�digo interno de la t�cnica
    '       - N�mero de Placa (si se trata de un cultivo)
    '       - C�digo de la t�cnica
    '       - Nombre de la t�cnica
    '       - C�digo de la condici�n ambiental
    '       - Nombre de la condici�n
    '       - Sobre qu� se realiza la t�cnica
    
    SQL = "SELECT MB2000.nRef, MB2000.MB20_CODTecAsist, MB2000.MB20_CODPlaca,MB2000.MB20_Reali"
    SQL = SQL & "   , MB0900.MB09_CodTec, MB0900.MB09_Desig"
    SQL = SQL & "   , MB0200.MB02_CodCond, MB0200.MB02_Desig"
    SQL = SQL & "   , MB2000.MB20_tiOrig, MB2000.MB20_CodOrig"
    SQL = SQL & "   , MB34_CODEstTecAsist, MB2000.MB20_CODTecAsist_Orig"
    SQL = SQL & "   , MB04_CODTITEC, MB0300.MB03_Desig"
    SQL = SQL & "   , MB0900.cAutoanalizador"
    SQL = SQL & " FROM MB2000, MB0900,MB0200,MB2600,MB0300"
    SQL = SQL & " WHERE MB2600.nRef=MB2000.nRef"
    SQL = SQL & "   AND MB2600.MB20_CodTecAsist=MB2000.MB20_CodTecAsist"
    SQL = SQL & "   AND MB34_CODEstTecAsist IN (" & constTECSOLICITADA & "," & constTECREALIZADA & "," & constTECREINCUBADA & ")"
    SQL = SQL & "   AND MB20_FecReali = TO_DATE(?,'dd/mm/yyyy hh24:mi:ss')"
    SQL = SQL & "   AND MB2600.MB26_FecEntra<=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & "   AND (   MB2600.MB26_FecSali>TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & "        OR MB2600.MB26_FecSali IS NULL )"
    SQL = SQL & "   AND MB0900.MB09_CodTec=MB2000.MB09_CodTec"
    SQL = SQL & "   AND MB0200.MB02_CodCond=MB2600.MB02_CodCond"
    SQL = SQL & "   AND MB0300.MB03_codRecip=MB0900.MB03_codRecip"
    SQL = SQL & "   AND MB20_INDSiembra = ?"
    SQL = SQL & "   AND MB2000.cUser =?"
    SQL = SQL & "   AND MB2000.nRef IN"
    SQL = SQL & "       (SELECT nRef FROM pruebaAsistencia"
    'SQL = SQL & "        WHERE estado = ? "
    SQL = SQL & "        WHERE estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    If Trim$(cCarpeta) <> "" Then
        SQL = SQL & "        AND cCarpeta = ?"
    Else
        SQL = SQL & "       AND cCarpeta IN (SELECT cCarpeta FROM carpetas WHERE cDptoSecc=?)"
    End If
    SQL = SQL & "       )"
    SQL = SQL & "   AND MB04_CODTITEC NOT IN (" & constRECUENTO & "," & constRESULTADOS & ")"
    SQL = SQL & "   ORDER BY MB2000.nRef, MB2000.MB20_CODPlaca ,MB2000.MB20_CODTecAsist"
                                                                               
    Set qryTec = objApp.rdoConnect.CreateQuery("Tecnica", SQL)
    qryTec(0) = fechaImpresion
    qryTec(1) = fechaImpresion
    qryTec(2) = fechaImpresion
    qryTec(3) = blnSiembras
    qryTec(4) = cUser
    'qryTec(5) = constPRUEBAREALIZANDO
    If Trim$(cCarpeta) <> "" Then
        qryTec(5) = cCarpeta
    Else
        qryTec(5) = departamento
    End If
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Call pComenzarImpresion     ' Se prepara la impresora para imprimir
    Do While Not rsTec.EOF
        ' En el primer registro se carga el valor de nRef, para que no genere
        ' nueva p�gina
        'If nRef = "" Then nRef = rsTec!nRef
        
        reali = ""
        If Not IsNull(rsTec!MB20_Reali) Then
            reali = rsTec!MB20_Reali
        End If
        ' Si el origen es una colonia -> se obtienen los comentarios a la colonia/placa
        If rsTec!MB20_tiOrig = constORIGENCOLONIA Then
            reali1 = fObtenerObservPlacaColonia(rsTec!nRef, rsTec!MB20_codOrig, rsTec!MB20_CODTecAsist_Orig)
            If reali1 <> "" Then
                If reali <> "" Then
                    reali = reali & Chr$(13) & Chr$(10)
                End If
                reali = reali & reali1
            End If
        End If
        
        ' Se imprime la t�cnica
        If Not IsNull(rsTec!MB20_CODTecAsist_Orig) Then
            cTecOrig = rsTec!MB20_CODTecAsist_Orig
        End If
        If Not IsNull(rsTec!cAutoanalizador) Then
            cAutoanalizador = rsTec!cAutoanalizador
        Else
            cAutoanalizador = ""
        End If
        res = fImprimirTecnicaEnHoja(rsTec!nRef, rsTec!MB20_CODPlaca, rsTec!MB09_Desig, rsTec!MB02_Desig, rsTec!MB20_tiOrig, rsTec!MB20_codOrig, nRef, reali, rsTec!MB34_codEstTecAsist, cTecOrig, rsTec!MB04_CODTITEC, rsTec!MB20_CODTECASIST, rsTec!MB03_desig, cAutoanalizador, arEtiquetas(), fechaImpresion)
        
        rsTec.MoveNext
        ' Si es la �ltima o se cambia de paciente, se salta de p�gina
        'If Not rsTec.EOF Then
        '    If nRef <> rsTec!nRef Then
        '        nRef = rsTec!nRef
            '    Call pSaltoPagina
        '    End If
        'End If
    Loop
    Call pFinalizarImpresion
    
    ' Se imprimen las etiquetas
    Call pImprimirEtiquetas(arEtiquetas())
    fImprimirHojaRepeticion = True
    rsTec.Close
    qryTec.Close
    Screen.MousePointer = vbDefault

End Function


Public Function fObtenerObservPlacaColonia(nRef, codOrig, CodTecAsistOrig) As String
    Dim SQL As String
    Dim observ As String
    Dim qryObserv As rdoQuery
    Dim rsObserv As rdoResultset
    
    
    On Error Resume Next
    SQL = "SELECT nRef,MB20_CODTECASIST,MB27_CODCOL,MB30_Observ FROM MB3000"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_CODTECASIST = ?"
    SQL = SQL & " AND MB27_CODCOL =? "
    
    Set qryObserv = objApp.rdoConnect.CreateQuery("Observaciones", SQL)
    'qryObserv.RowsetSize = 1
    qryObserv(0) = nRef
    qryObserv(1) = CodTecAsistOrig
    qryObserv(2) = codOrig
    
    Set rsObserv = qryObserv.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If Not rsObserv.EOF Then
        observ = rsObserv!mb30_observ
        If observ <> "" Then
            fObtenerObservPlacaColonia = observ
        End If
    End If
    rsObserv.Close
    qryObserv.Close
End Function
Public Function fImprimirHoja(cCarpeta As String, cUser As String, blnSiembras As Boolean) As Integer
    ' Se recogen las t�cnicas a preparar, se imprimen y se
    ' cambian de estado. El cambio de estado lleva consigo la
    ' actualizaci�n de la fecha en la que hay que volver a
    ' ver las t�cnicas
    
    Dim SQL As String
    Dim qryTec As rdoQuery
    Dim rsTec As rdoResultset
    Dim nRef As String
    Dim res As Boolean
    Dim reali As String
    Dim reali1 As String
    Dim cTecOrig As String
    Dim fechaCreacion As String
    Dim estado As Integer
    Dim arEtiquetas() As typeEtiquetas
    Dim cAutoanalizador As String
    
    Screen.MousePointer = vbHourglass
    fechaCreacion = fFechaHoraActual()
    ' Para las t�cnicas se obtiene lo siguiente:
    '       - N� Referencia
    '       - C�digo interno de la t�cnica
    '       - N�mero de Placa (si se trata de un cultivo)
    '       - C�digo de la t�cnica
    '       - Nombre de la t�cnica
    '       - C�digo de la condici�n ambiental
    '       - Nombre de la condici�n
    '       - Sobre qu� se realiza la t�cnica
    
    SQL = "SELECT MB2000.nRef, MB2000.MB20_CODTecAsist, MB2000.MB20_CODPlaca,MB2000.MB20_Reali"
    SQL = SQL & "   , MB0900.MB09_CodTec, MB0900.MB09_Desig"
    SQL = SQL & "   , MB0200.MB02_CodCond, MB0200.MB02_Desig"
    SQL = SQL & "   , MB2000.MB20_tiOrig, MB2000.MB20_CodOrig"
    SQL = SQL & "   , MB34_CODEstTecAsist, MB2000.MB20_CODTecAsist_Orig"
    SQL = SQL & "   , MB04_CODTITEC, MB0900.cAutoanalizador, MB0300.MB03_Desig"
    SQL = SQL & " FROM MB2000, MB0900,MB0200,MB2600,MB0300"
    SQL = SQL & " WHERE MB2600.nRef=MB2000.nRef"
    SQL = SQL & "   AND MB2600.MB20_CodTecAsist=MB2000.MB20_CodTecAsist"
    SQL = SQL & "   AND MB2600.MB35_CODEstSegui=" & constSEGUITECVALIDA
    SQL = SQL & "   AND MB0900.MB09_CodTec=MB2000.MB09_CodTec"
    SQL = SQL & "   AND MB0200.MB02_CodCond=MB2600.MB02_CodCond"
    SQL = SQL & "   AND MB0300.MB03_codRecip=MB0900.MB03_codRecip"
    SQL = SQL & "   AND MB20_INDSiembra = ?"
    SQL = SQL & "   AND MB2000.nRef IN"
    SQL = SQL & "       (SELECT nRef FROM pruebaAsistencia"
'    SQL = SQL & "        WHERE estado = ? "
    SQL = SQL & "        WHERE estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    If Trim$(cCarpeta) <> "" Then
        SQL = SQL & "       AND cCarpeta = ?"
    Else
        SQL = SQL & "       AND cCarpeta IN (SELECT cCarpeta FROM carpetas WHERE cDptoSecc=?)"
    End If
    SQL = SQL & "       )"
    SQL = SQL & "   AND MB34_CODEstTecAsist IN (" & constTECSOLICITADA & "," & constTECREINCUBADA & ")"
    SQL = SQL & "   AND MB04_CODTITEC NOT IN (" & constRECUENTO & "," & constRESULTADOS & ")"
    SQL = SQL & "   ORDER BY MB2000.nRef, MB2000.MB20_CODPlaca ,MB2000.MB20_CODTecAsist"
                                                                               
    Set qryTec = objApp.rdoConnect.CreateQuery("Tecnica", SQL)
    qryTec(0) = blnSiembras
    'qryTec(1) = constPRUEBAREALIZANDO
    If Trim$(cCarpeta) <> "" Then
        qryTec(1) = cCarpeta
    Else
        qryTec(1) = departamento
    End If
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Call pComenzarImpresion     ' Se prepara la impresora para imprimir
    Do While Not rsTec.EOF
        ' En el primer registro se carga el valor de nRef, para que no genere
        ' nueva p�gina
        'If nRef = "" Then nRef = rsTec!nRef
                
        reali = ""
        If Not IsNull(rsTec!MB20_Reali) Then
            reali = rsTec!MB20_Reali
        End If
        ' Si el origen es una colonia -> se obtienen los comentarios a la colonia/placa
        If rsTec!MB20_tiOrig = constORIGENCOLONIA Then
            reali1 = fObtenerObservPlacaColonia(rsTec!nRef, rsTec!MB20_codOrig, rsTec!MB20_CODTecAsist_Orig)
            If reali1 <> "" Then
                If reali <> "" Then
                    reali = reali & Chr$(13) & Chr$(10)
                End If
                reali = reali & reali1
            End If
        End If
        
        ' Se imprime la t�cnica
        If Not IsNull(rsTec!MB20_CODTecAsist_Orig) Then
            cTecOrig = rsTec!MB20_CODTecAsist_Orig
        End If
        If Not IsNull(rsTec!cAutoanalizador) Then
            cAutoanalizador = rsTec!cAutoanalizador
        Else
            cAutoanalizador = ""
        End If
        res = fImprimirTecnicaEnHoja(rsTec!nRef, rsTec!MB20_CODPlaca, rsTec!MB09_Desig, rsTec!MB02_Desig, rsTec!MB20_tiOrig, rsTec!MB20_codOrig, nRef, reali, rsTec!MB34_codEstTecAsist, cTecOrig, rsTec!MB04_CODTITEC, rsTec!MB20_CODTECASIST, rsTec!MB03_desig, cAutoanalizador, arEtiquetas(), fechaCreacion)
        
        ' Se obtiene el estado en el que ha de quedar la t�cnica
        '       . Realizada: si se hace de forma manual
        '       . Solicitada: si se hace en autoanalizador
        If Not IsNull(rsTec!cAutoanalizador) Then
            estado = constTECSOLICITAREALI
        Else
            estado = constTECREALIZADA
        End If
        ' Se da la t�cnica como realizada
        res = fRealizarTecnica(rsTec!nRef, rsTec!MB20_CODTECASIST, rsTec!MB09_codTec, rsTec!MB02_CODCond, cUser, estado, fechaCreacion, blnSiembras)
        rsTec.MoveNext
        ' Si es la �ltima o se cambia de paciente, se salta de p�gina
        'If Not rsTec.EOF Then
        '    If nRef <> rsTec!nRef Then
        '        nRef = rsTec!nRef
            '    Call pSaltoPagina
        '    End If
        'End If
    Loop
    Call pFinalizarImpresion
    ' Se imprimen las etiquetas
    Call pImprimirEtiquetas(arEtiquetas())
    
    rsTec.Close
    qryTec.Close
    Screen.MousePointer = vbDefault
End Function

Public Function fSaltarPagina(posicion%) As Boolean
    If posicion >= constSaltoPagina Then
        fSaltarPagina = True
    End If
End Function

Public Sub pComenzarImpresion()
    On Error Resume Next
    Printer.DrawWidth = 3
    
    Printer.FontName = "Arial"
    Printer.FontSize = 12
    
    Printer.Orientation = vbPRORPortrait
    Printer.PaperSize = vbPRPSA4 'Se establece como p�gina el A4
    
    '=============================================
    ' Para imprimir en A5 activar lo siguiente
            'Printer.Orientation = vbPRORLandscape
            'Printer.PaperSize = vbPRPSA5
    '=============================================
    
    
End Sub

Public Function fRealizarTecnica(nRef$, cTecAsist$, cTec$, cCond$, cUser$, estado%, fechaCreacion$, blnSiembras As Boolean) As Boolean
    ' Recoge la identificaci�n de la t�cnica y la pone como realizada  si no es una t�cnica de autoabalizador.
    '   En este �ltimo caso, se mantiene el estado de la t�cnica en solicitada.
    ' Adem�s se actualizan los siguientes datos:
    '       - Fecha de realizaci�n: se supone que es el momento actual
    '       - Fecha de siguiente lectura: la fecha actual m�s el tiempo que se haya
    '                                    definido en la tabla de tiempo t�cnica (MB1300)
    '       - Usuario que realiza las t�cnicas
    
    Dim SQL As String
    Dim qryTec As rdoQuery
    Dim rsTec As rdoResultset
    Dim fLect As String
    
    On Error Resume Next
    ' Se recoge cu�ndo se tiene que mostrar en la mesa la t�cnica
    fLect = fDiaSiguienteLectura(cTec, cCond, blnSiembras)
    
    ' Se genera la orden de actualizaci�n de datos
    SQL = "UPDATE MB2000"
    SQL = SQL & " SET MB34_CODEstTecAsist=?"
    SQL = SQL & ", MB20_fecReali=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & ", MB20_fecLect=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & ", cUser = ?"
    SQL = SQL & " WHERE nRef =?"
    SQL = SQL & " AND  MB20_CODTecAsist= ?"
    Set qryTec = objApp.rdoConnect.CreateQuery("", SQL)
    qryTec(0) = constTECREALIZADA
    qryTec(1) = fechaCreacion
    qryTec(2) = Format$(DateAdd("h", Val(fLect), fFechaHoraActual()), "dd/mm/yyyy hh:nn:ss")
    qryTec(3) = cUser
    qryTec(4) = nRef
    qryTec(5) = cTecAsist
    Err = 0
    Set rsTec = qryTec.OpenResultset(rdOpenKeyset)
    If Err = 0 Then
        fRealizarTecnica = True
    Else
        fRealizarTecnica = False
    End If
    rsTec.Close
    qryTec.Close

    ' Se actualiza la fecha de entrada en la tabla de seguimiento
    SQL = "UPDATE MB2600"
    SQL = SQL & " SET MB26_FECEntra=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & " WHERE nRef =?"
    SQL = SQL & " AND  MB20_CODTecAsist= ?"
    SQL = SQL & " AND  MB35_CODEstSegui= ?"
    
    Set qryTec = objApp.rdoConnect.CreateQuery("", SQL)
    qryTec(0) = fechaCreacion
    qryTec(1) = nRef
    qryTec(2) = cTecAsist
    qryTec(3) = constSEGUITECVALIDA
    Err = 0
    Set rsTec = qryTec.OpenResultset(rdOpenKeyset)
    If Err = 0 Then
        fRealizarTecnica = True
    Else
        fRealizarTecnica = False
    End If
    rsTec.Close
    qryTec.Close
    
End Function

Public Sub pFinalizarImpresion()
    Printer.EndDoc
End Sub


Public Function fSaltoPagina(posicion%, Optional saltar%) As Integer
    Dim Y As Long
    Dim blnBold As Boolean
    Dim blnItalic As Boolean
    Dim strFontName As String
    Dim lngFontSize As Long
    
    If saltar = True Or posicion >= constSaltoPagina Then
        blnBold = Printer.FontBold
        blnItalic = Printer.FontItalic
        strFontName = Printer.FontName
        lngFontSize = Printer.FontSize
        
        Printer.NewPage
        Printer.FontBold = True
        Printer.FontSize = 10
        
        Printer.CurrentX = 1000
        Printer.CurrentY = 200
        Printer.Print fFechaHoraActual ' Imprime la fecha de impresi�n.
        
        Printer.CurrentX = 8000
        Printer.CurrentY = 200
        Printer.Print "P�g. " & Printer.page  ' Imprime el n�mero de p�gina.
        
        ' Dibuja una l�nea atravesando la p�gina.
        Y = Printer.CurrentY
        Printer.Line (0, Y)-(Printer.ScaleWidth, Y)     ' Dibuja una l�nea.
    
        posicion = Printer.CurrentY + constSaltoParrafo
        
        Printer.FontBold = blnBold
        Printer.FontItalic = blnItalic
        Printer.FontName = strFontName
        Printer.FontSize = lngFontSize
        
        Printer.CurrentY = posicion
        fSaltoPagina = True
    End If
End Function

Public Function fImprimirTecnicaEnHoja(nRef$, codPlaca$, tecnica$, condicion$, tiOrig%, codOrig$, nRefAnterior$, indicaciones$, estado%, cTecOrig$, cTiTec%, cTecAsist$, strRecip$, cAutoanalizador$, arEtiquetas() As typeEtiquetas, strFecha$) As Boolean

    ' Se imprime la t�cnica en la hoja de trabajo
    ' Comparando nRef y nRefAnterior se sabe si se ha pasaodo a un nuevo n�mero de referencia
    
    Dim texto As String
    Dim posicionY As Integer
    Dim historia As String
    Dim paciente As String
    Dim fecha As String
    Dim res As Integer
    Dim intDimension As Integer
    Dim strTipoMuestra As String
    Dim strInicialesPac As String
    
    On Error Resume Next
    posicionY = Printer.CurrentY
    If Trim$(nRef) <> Trim$(nRefAnterior) Then
        ' Cambio de nRef
        posicionY = posicionY + constSaltoParrafo
        res = fSaltoPagina(posicionY)
        Printer.CurrentY = posicionY
        Printer.FontBold = True
        Printer.FontSize = 10
        Printer.CurrentX = margenIzquierdo
        ' Se obtiene n� historia y nombre del paciente
        res = fDatosNReferencia(nRef, historia, paciente, fecha, strTipoMuestra, strInicialesPac)
        
        texto = "N� Referencia " & Str$(Val(Right(nRef, 7)))
        Printer.Print texto
        Printer.CurrentY = posicionY
        
        If res = True Then
            Printer.FontSize = 8
            Printer.CurrentX = margenIzquierdo + 2500
            texto = fecha
            Printer.Print texto
            Printer.CurrentY = posicionY
            
            Printer.CurrentX = margenIzquierdo + 4500
            texto = "(" & historia & ": " & paciente & ")"
            Printer.Print texto
            Printer.CurrentY = posicionY
        End If
        nRefAnterior = nRef
    Else
        On Error Resume Next
        intDimension = 0
        intDimension = UBound(arEtiquetas)
        If intDimension > 0 Then
            historia = arEtiquetas(intDimension).strHistoria
            strTipoMuestra = arEtiquetas(intDimension).strMuestra
            strInicialesPac = arEtiquetas(intDimension).strPaciente
        Else
            res = fDatosNReferencia(nRef, historia, paciente, fecha, strTipoMuestra, strInicialesPac)
        End If
    End If
    posicionY = posicionY + constSaltoLinea
    res = fSaltoPagina(posicionY)
    Printer.CurrentY = posicionY
    Printer.FontBold = False
    Printer.FontSize = 10

    If estado = constTECREINCUBADA Then
        'Printer.FontStrikethru = True
        Printer.FontBold = False
        Printer.FontItalic = True
        texto = "Reinc."
        Printer.CurrentX = margenIzquierdo + 500
        Printer.Print texto
        Printer.CurrentY = posicionY
    Else
        Printer.FontItalic = False
        Printer.FontBold = True
    End If
    
    If Val(codPlaca) <> 0 Then
        Printer.CurrentX = margenIzquierdo + 1300
        'texto = "Placa " & Trim$(codPlaca)
        texto = Left(strRecip, 5) & " " & Trim$(codPlaca)
        Printer.Print texto
        Printer.CurrentY = posicionY
    End If
    texto = Trim$(tecnica)
    Printer.CurrentX = margenIzquierdo + 2500
    Printer.Print texto
    
    Printer.CurrentY = posicionY
    texto = Trim$(condicion)
    Printer.CurrentX = margenIzquierdo + 4500
    Printer.Print texto
    
    Printer.CurrentY = posicionY
    texto = "Sobre " & fOrigenTecnica(tiOrig) & Space(1)
    Select Case CInt(tiOrig)
        Case constORIGENTECNICA
            ' Se localiza la t�cnica
            texto = texto & fLocalizarTecnicaOrigen(nRef, codOrig)
        Case constORIGENPLACA
            ' Se localiza el n� de placa
            'TEXTO = TEXTO & fLocalizarPlaca(nRef, codOrig)
            texto = texto & codOrig
        Case constORIGENMUESTRA
            ' Se incluyen los c�digo sin m�s
            texto = texto & codOrig
        Case constORIGENCOLONIA
            ' Se incluye el c�digo de la colonia y la placa de la que hay que hacerla
            'texto = texto & codOrig & " en placa " & fLocalizarPlacaOrigen(nRef, cTecOrig)
            texto = texto & codOrig & " en placa " & fLocalizarPlaca(nRef, cTecOrig)
    End Select
    
    Printer.CurrentX = margenIzquierdo + 6500
    Printer.Print texto
    'Printer.FontStrikethru = False
    
    Printer.FontBold = False
    
    ' Si es un antibiograma se imprimen los antibi�ticos solicitados
    If cTiTec = constANTIBIOGRAMA Then
        texto = fImprimirAntibiotico(nRef, cTecAsist)
        If texto <> "" Then
            posicionY = posicionY + constSaltoLinea
            Printer.CurrentY = posicionY
            posicionY = fImprimirTexto(texto, 3000, 5)
        End If
    Else
        Printer.CurrentY = posicionY
    End If
    
    If indicaciones <> "" Then
        posicionY = posicionY + constSaltoLinea
        Printer.CurrentY = posicionY
        posicionY = fImprimirTexto(indicaciones, 3000, 5)
    End If
    Printer.CurrentY = posicionY
    
    ' Si se trata de un cultivo -> se imprime la etiqueta
    If (Val(codPlaca) > 0 And estado <> constTECREINCUBADA) Or cAutoanalizador <> "" Then
        On Error Resume Next
        intDimension = UBound(arEtiquetas)
        intDimension = intDimension + 1
        ReDim Preserve arEtiquetas(1 To intDimension)
        arEtiquetas(intDimension).strNRef = nRef
        arEtiquetas(intDimension).intCodPlaca = codPlaca
        arEtiquetas(intDimension).strcCol = codOrig
        arEtiquetas(intDimension).strcTecAsist = cTecAsist

        arEtiquetas(intDimension).strHistoria = historia
        arEtiquetas(intDimension).strFecha = Format$(strFecha, "d/m/yy hh:mm")
        arEtiquetas(intDimension).strMuestra = strTipoMuestra
        arEtiquetas(intDimension).strTecnica = tecnica
        arEtiquetas(intDimension).strCondicion = condicion
        arEtiquetas(intDimension).strPaciente = strInicialesPac
        If cAutoanalizador = "" Then
            arEtiquetas(intDimension).blnAutoanalizador = False
        Else
            arEtiquetas(intDimension).blnAutoanalizador = True
        End If
    End If
End Function


Public Function fImprimirAntibiotico(nRef$, cTecAsist$) As String
    ' Devuelve en un string los nomnres de los antibi�ticos
    ' pertenecientes a una prueba de antibiograma
    
    Dim SQL As String
    Dim qryAntib As rdoQuery
    Dim rsAntib As rdoResultset
    Dim antib As String
    
    SQL = "SELECT DISTINCT NVL(MB31_Desig,MB07_Desig) AS ANTI"
    SQL = SQL & " FROM MB2900,MB0700,MB3100"
    SQL = SQL & " WHERE MB0700.MB07_CODANTI = MB2900.MB07_CODANTI"
    SQL = SQL & " AND MB3100.MB31_CODGRANTI (+) = MB2900.MB31_CODGRANTI"
    SQL = SQL & " AND MB2900.NREF=?"
    SQL = SQL & " AND MB2900.MB20_CODTECASIST =?"
    SQL = SQL & " ORDER BY ANTI"
    
    Set qryAntib = objApp.rdoConnect.CreateQuery("Tiempo", SQL)
    qryAntib(0) = nRef
    qryAntib(1) = cTecAsist
    Set rsAntib = qryAntib.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsAntib.EOF
        antib = antib & rsAntib!ANTI & ", "
        rsAntib.MoveNext
    Loop
    If antib <> "" Then
        If Right(antib, 2) = ", " Then antib = Left(antib, Len(antib) - 2)
        fImprimirAntibiotico = "(" & antib & ")"
    End If
    rsAntib.Close
    qryAntib.Close
End Function
Public Function fImprimirTexto(indicaciones$, Optional XprimeraLinea%, Optional tipoSangrado%) As Long
Dim donde As Integer, linea As String, linea1 As String, linea2 As String, linea3 As String
Dim limite As Long
Dim primeraLinea As Boolean
Dim comienzolinea As Long
Dim texto As String
   
  texto = Space(tipoSangrado) & indicaciones
  texto = texto & Chr$(13) & " "
  donde = InStr(texto, Chr$(13))
  comienzolinea = XprimeraLinea
  limite = Printer.ScaleWidth - margenIzquierdo - margenDerecho - comienzolinea
  primeraLinea = True
  
  While donde > 0
    linea1 = Left(texto, donde - 1)
    Do While Printer.TextWidth(linea1) > limite
      linea = ""
      Do While Printer.TextWidth(linea) < limite
        If linea2 = linea And linea <> "" Then Exit Do
        linea2 = linea
        linea = linea & Left(linea1, InStr(linea1, Chr$(32)))
        linea3 = linea1
        linea1 = Right(linea1, Len(linea1) - InStr(linea1, Chr$(32)))
        If linea = "" Then Exit Do
      Loop
      Printer.CurrentX = margenIzquierdo + comienzolinea
      Printer.Print linea2
      linea1 = linea3
      If linea2 = "" Then Exit Do
    Loop
    Printer.CurrentX = margenIzquierdo + comienzolinea
    Printer.Print linea1
    If Len(texto) > InStr(texto, Chr$(13)) Then
      texto = Right(texto, Len(texto) - InStr(texto, Chr$(13)))
    Else
      texto = Left(texto, Len(texto) - 1)
    End If
    If Left(texto, 1) = Chr$(10) Then
        texto = Right(texto, Len(texto) - 1)
    End If
    texto = Space(tipoSangrado) & texto
    donde = InStr(1, texto, Chr$(13))
  Wend
  fImprimirTexto = Printer.CurrentY - constSaltoLinea
    
End Function
Public Function fDatosNReferencia(nRef$, historia$, paciente$, fecha$, strTipoMuestra$, strInicialesPac$) As Integer
    ' Localiza los datos del paciente y muestra dado un n� de referencia
    
    Dim SQL As String
    Dim qryDatos As rdoQuery
    Dim rsDatos As rdoResultset
    
    On Error Resume Next
    
    SQL = "SELECT "
    SQL = SQL & "   pA.historia,"
    SQL = SQL & "   (pac.nom||' '||pac.ap1||' '||pac.ap2) as nombre, "
    SQL = SQL & "   mA.fechaExtraccion, tm.designacion, pac.nom, pac.ap1"
    SQL = SQL & " FROM "
    SQL = SQL & "   pruebaAsistencia pA, pac, muestraPrueba mP,"
    SQL = SQL & "   muestraAsistencia mA, tiposMuestras tm"
    SQL = SQL & " WHERE "
    SQL = SQL & "   pac.nh (+) = pA.historia"
    SQL = SQL & "   AND mP.historia = pA.historia"
    SQL = SQL & "   AND mP.caso = pA.caso"
    SQL = SQL & "   AND mP.secuencia= pA.secuencia"
    SQL = SQL & "   AND mA.cMuestra = mP.cMuestra"
    SQL = SQL & "   AND tm.cTipoMuestra = ma.cTipoMuestra"
    SQL = SQL & "   AND pA.nRef = ?"
    SQL = SQL & " ORDER BY "
    SQL = SQL & "   mA.fechaExtraccion"
    
    Set qryDatos = objApp.rdoConnect.CreateQuery("Datos Ref", SQL)
    qryDatos.RowsetSize = 1
    qryDatos(0) = nRef
    
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        historia = rsDatos!historia
        fecha = rsDatos!fechaExtraccion
        If Not IsNull(rsDatos!nombre) Then
            paciente = rsDatos!nombre
        Else
            paciente = ""
        End If
        If Not IsNull(rsDatos!ap1) Then
            If Len(rsDatos!ap1) < 20 Then
                strInicialesPac = rsDatos!ap1
            Else
                strInicialesPac = Left(rsDatos!ap1, 20) & "."
            End If
        End If
        If Not IsNull(rsDatos!nom) Then
            strInicialesPac = fIniciales(rsDatos!nom) & " " & strInicialesPac
        End If
        strTipoMuestra = rsDatos!Designacion
        fDatosNReferencia = True
    Else
        fDatosNReferencia = False
    End If
    rsDatos.Close
    qryDatos.Close
End Function
Public Function fDiaSiguienteLectura(cTec$, cCond$, blnSiembras As Boolean) As String
    ' Se recoge la t�cnica y el ambiente, y se dice cu�ndo es la siguiente lectura
    
    
    Dim SQL As String
    Dim qryTiempo As rdoQuery
    Dim rsTiempo As rdoResultset
    
    SQL = "SELECT MB13_Tiempo,MB13_tResiembra FROM MB1300 "
    SQL = SQL & " WHERE MB09_CODTec=?"
    SQL = SQL & " AND MB02_CODCond =?"
    
    Set qryTiempo = objApp.rdoConnect.CreateQuery("Tiempo", SQL)
    qryTiempo.RowsetSize = 1
    qryTiempo(0) = cTec
    qryTiempo(1) = cCond
    
    Set rsTiempo = qryTiempo.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsTiempo.EOF Then
        If blnSiembras Then
            If Not IsNull(rsTiempo!MB13_Tiempo) Then
                fDiaSiguienteLectura = rsTiempo!MB13_Tiempo
            Else
                fDiaSiguienteLectura = 24
            End If
        Else
            If Not IsNull(rsTiempo!MB13_tResiembra) Then
                fDiaSiguienteLectura = rsTiempo!MB13_tResiembra
            Else
                fDiaSiguienteLectura = 24
            End If
        End If
    Else
        fDiaSiguienteLectura = 24
    End If
    rsTiempo.Close
    qryTiempo.Close
End Function

