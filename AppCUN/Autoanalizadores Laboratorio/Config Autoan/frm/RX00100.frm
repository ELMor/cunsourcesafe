VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "Crystl32.ocx"
Begin VB.Form frmPrincipal 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Archivo de Radio Diagnostico "
   ClientHeight    =   5955
   ClientLeft      =   285
   ClientTop       =   1785
   ClientWidth     =   8430
   ClipControls    =   0   'False
   HelpContextID   =   101
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5955
   ScaleWidth      =   8430
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin Crystal.CrystalReport crCrystalReport1 
      Left            =   6000
      Top             =   2460
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      WindowBorderStyle=   3
      WindowControlBox=   -1  'True
      WindowMaxButton =   0   'False
      WindowMinButton =   0   'False
      DiscardSavedData=   -1  'True
      WindowControls  =   -1  'True
      WindowState     =   2
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog1 
      Left            =   6255
      Top             =   270
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
      HelpFile        =   "csc.hlp"
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   240
      Index           =   1
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   423
      _StockProps     =   15
      Caption         =   "&Archivo"
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   240
      Index           =   2
      Left            =   420
      TabIndex        =   1
      Top             =   1860
      Width           =   915
      _Version        =   65536
      _ExtentX        =   1614
      _ExtentY        =   423
      _StockProps     =   15
      Caption         =   "&Areas"
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   240
      Index           =   3
      Left            =   180
      TabIndex        =   2
      Top             =   2880
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   423
      _StockProps     =   15
      Caption         =   "T�cnicas"
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   240
      Index           =   4
      Left            =   180
      TabIndex        =   3
      Top             =   3960
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   423
      _StockProps     =   15
      Caption         =   "Diagn�sticos"
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   4
      Left            =   600
      Picture         =   "RX00100.frx":0000
      Tag             =   "Ayuda"
      Top             =   3420
      Width           =   480
   End
   Begin ComctlLib.ImageList imlImageList2 
      Left            =   6960
      Top             =   1680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   14
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":030A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":0624
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":093E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":0C58
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":0F72
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":128C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":15A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":18C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":1BDA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":1EF4
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":220E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":2528
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":27B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "RX00100.frx":2A10
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   1
      Left            =   600
      Picture         =   "RX00100.frx":2D2A
      Tag             =   "Contactos"
      Top             =   300
      WhatsThisHelpID =   10101
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   2
      Left            =   600
      Picture         =   "RX00100.frx":3034
      Tag             =   "Contactos"
      Top             =   1320
      WhatsThisHelpID =   10101
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   3
      Left            =   600
      Picture         =   "RX00100.frx":333E
      Tag             =   "Ayuda"
      Top             =   2340
      Width           =   480
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   6300
      Top             =   1680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mnuarchivo 
      Caption         =   "&Archivo"
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Archivo"
         Index           =   190
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "-"
         Index           =   191
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&T�cnicas"
         Index           =   195
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Areas"
         Index           =   200
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Diagn�sticos"
         Index           =   220
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "-"
         Index           =   225
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Salir"
         Index           =   230
         Shortcut        =   ^S
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Private entrada As Boolean
'Dim blnShowFrmMain As Boolean

Private Sub Form_Activate()
    Dim entrada As Boolean
    entrada = objPipe.PipeGet("entrada")
    If entrada = True Then
        Call objPipe.PipeSet("entrada", False)
'        frmG_Seccion.Show vbModal
'        Set frmG_Seccion = Nothing
'        'If frmG_Seccion.SelectSession = False Then
'        If objPipe.PipeGet("SelectSession") = False Then
'            Unload Me
'        End If
    End If
   
End Sub

Private Sub Form_Load()
Dim strEntorno1 As String
Dim strEntorno2 As String

  Call InitApp
  With objApp
    Call .Register(App, Screen)
    Set .frmFormMain = Me
    Set .imlImageList = imlImageList1
    Set .crCrystal = crCrystalReport1
    Set .dlgCommon = dlgCommonDialog1
'    .strUserName = "MICRO"
'    .strPassword = "MICRO"
'    .strDataSource = "Oracle7_Micro"
    '.blnCursorGoFirst = True 'Con las vistas y con el nuevo DataRefresh ya no hay que tenerlo en cuenta
'    .strUserName = "cun"
'    .strPassword = "tuy"
    .strUserName = "cun"
    .strPassword = "tnxafwq"
    .strDataSource = "ORACLE7_RX"
    .intUserEnv = 1
    .blnAskPassword = False
    .objUserColor.lngReadOnly = &HC0C0C0
    .objUserColor.lngMandatory = &HFFFF00
    .blnUseRegistry = True
  End With
  
  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotaci�n"
  
  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "Main", "MICRO.")
  
    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "Main", "")
  
  End With
    
  If Not objApp.CreateInfo Then
    Call ExitApp
  End If
  
  rptPath = App.Path
  If Right(rptPath, 1) <> "\" Then rptPath = rptPath & "\"
  'rptPath = rptPath & "..\..\inf"
  rptPath = rptPath & "rpt"
  
  'entrada = True
  Call objPipe.PipeSet("entrada", True)
  
  Call objPipe.PipeSet("nfotosh", 4)
  Call objPipe.PipeSet("nfotosv", 2)
  Call objPipe.PipeSet("seph", 4)
  Call objPipe.PipeSet("sepv", 5)
  
  App.HelpFile = App.Path & "\mb.hlp"
  
  objSecurity.strUser = "PRUEBA"
  
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
  Call ExitApp
End Sub

Private Sub Image1_DblClick(intIndex As Integer)
  Call Icon_Execute(intIndex)
End Sub

Private Sub mnuArchivoOpcion_Click(Index As Integer)
    Select Case Index
        Case 10
'            frmA_Paciente.Show (vbModal)
'            Set frmA_Paciente = Nothing
        Case 20
'            Call pVentanaPruebas("", "", "")
        Case 30
            ' Secci�n
        Case 40
'            frmA_Microorganismo.Show (vbModal)
'            Set frmA_Microorganismo = Nothing
        Case 50
'            frmA_FamiliaMicroorganismo.Show (vbModal)
'            Set frmA_FamiliaMicroorganismo = Nothing
        Case 60
'            frmA_TipoMicroorganismo.Show (vbModal)
'            Set frmA_TipoMicroorganismo = Nothing
        Case 65
'            frmA_Reaccion.Show (vbModal)
'            Set frmA_Reaccion = Nothing
        Case 70
'            frmA_Morfologia.Show (vbModal)
'            Set frmA_Morfologia = Nothing
        Case 80
'            frmA_Antibiotico.Show (vbModal)
'            Set frmA_Antibiotico = Nothing
        Case 85
'            frmA_GrAntibiotico.Show (vbModal)
'            Set frmA_GrAntibiotico = Nothing
        Case 90
            'Secci�n
        Case 100
'            frmA_Tecnica.Show (vbModal)
'            Set frmA_Tecnica = Nothing
        Case 110
'            frmA_TipoTecnica.Show (vbModal)
'            Set frmA_TipoTecnica = Nothing
        Case 120
'            frmA_Protocolo.Show (vbModal)
'            Set frmA_Protocolo = Nothing
        Case 125
'            frmA_TipoProtocolo.Show (vbModal)
'            Set frmA_TipoProtocolo = Nothing
        Case 130
'            frmA_Condicion.Show (vbModal)
'            Set frmA_Condicion = Nothing
        Case 140
'            frmA_Carpetas.Show (vbModal)
'            Set frmA_Carpetas = Nothing
        Case 150
        '    'Secci�n
        Case 160
'            frmA_Eleccion.Show (vbModal)
'            Set frmA_Eleccion = Nothing
        Case 170
'            frmA_Recipiente.Show (vbModal)
'            Set frmA_Recipiente = Nothing
        Case 180
'            frmA_MotivoAlmacenamiento.Show (vbModal)
'            Set frmA_MotivoAlmacenamiento = Nothing
        Case 190
            frmA_Archivo.Show (vbModal)
            Set frmA_Archivo = Nothing
            Set frmImpresion = Nothing
        Case 195
            frmA_Tecnica.Show (vbModal)
            Set frmA_Tecnica = Nothing
        Case 200
            frmA_Area.Show (vbModal)
            Set frmA_Area = Nothing
        Case 220
            frmA_Diagnostico.Show (vbModal)
            Set frmA_Diagnostico = Nothing
        Case 230
            End
    End Select
        
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      'Call objCW.stdAppHelpContext
    Case 20
      'Call objCW.stdAppAbout
  End Select
End Sub

Private Sub Icon_Execute(intIndex As Integer)
    Select Case intIndex
        Case 0
'            frmP_Recepcion.Show (vbModal)
'            Set frmP_Recepcion = Nothing
        Case 1
            
            frmA_Archivo.Show (vbModal)
            Set frmA_Archivo = Nothing
            Set frmImpresion = Nothing
        Case 2
            frmA_Area.Show (vbModal)
            Set frmA_Area = Nothing
        Case 3
            frmA_Tecnica.Show (vbModal)
            Set frmA_Tecnica = Nothing
        Case 4
            frmA_Diagnostico.Show (vbModal)
            Set frmA_Diagnostico = Nothing
    End Select
End Sub

'Private Sub mnuHerramientasOpcion_Click(Index As Integer)
'
'    Select Case Index
'        Case 10
'            .Show vbModal
'            Set frmH_EtiqfrmH_Etiquetasuetas = Nothing
'        Case 20
'            ' Secci�n
'        Case 30
'            frmH_FechasLectura.Show vbModal
'            Set frmH_FechasLectura = Nothing
'        Case 40
'            ' Secci�n
'        Case 50
'            Call pRegenerarResultadosLaboratorio
'        Case 60
'            Call pRegenerarSequences(True)
'    End Select
'
'End Sub

'Private Sub mnuInformesOpcion_Click(Index As Integer)
'    Select Case Index
'        Case 10
'            frmI_HojaTrabajo.Show (vbModal)
'            Set frmI_HojaTrabajo = Nothing
'        Case 20
'            ' Secci�n
'        Case 30
'            Call objPipe.PipeSet("informe", constINFProblemas)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 40
'            Call objPipe.PipeSet("informe", constINFColonias)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 50
'            Call objPipe.PipeSet("informe", constINFMuestras)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 60
'            Call objPipe.PipeSet("informe", constINFAislamientos)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 70
'            Call objPipe.PipeSet("informe", constINFAntibiogramas)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 80
'            Call objPipe.PipeSet("informe", constINFTecnicas)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 90
'            Call objPipe.PipeSet("informe", constINFBionumeros)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 100
'            Call objPipe.PipeSet("informe", constINFCrecimientos)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 110
'            Call objPipe.PipeSet("informe", constINFEdades)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'        Case 120
'            Call objPipe.PipeSet("informe", constINFDesglose)
'            frmI_Informes.Show (vbModal)
'            Set frmI_Informes = Nothing
'    End Select
'End Sub

'Private Sub mnuProcesoOpcion_Click(Index As Integer)
'    Select Case Index
'        Case 10
'            frmP_Recepcion.Show (vbModal)
'            Set frmP_Recepcion = Nothing
'        Case 20
'            frmP_MontajeMesa.Show (vbModal)
'            Set frmP_MontajeMesa = Nothing
'        Case 30
'            frmI_HojaRealizacion.Show (vbModal)
'            Set frmI_HojaRealizacion = Nothing
'        Case 40
'            ' Secci�n
'        Case 50
'            frmG_Seccion.Show vbModal
'            Set frmG_Seccion = Nothing
'    End Select
'End Sub
