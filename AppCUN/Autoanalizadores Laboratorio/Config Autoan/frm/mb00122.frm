VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "comdlg32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmA_Problema 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tabla de Medicamentos"
   ClientHeight    =   8400
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11970
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8400
   ScaleWidth      =   11970
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11970
      _ExtentX        =   21114
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Fotografias"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7575
      Index           =   0
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   11820
      Begin TabDlg.SSTab tabTab1 
         Height          =   7200
         Index           =   0
         Left            =   120
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   300
         Width           =   11610
         _ExtentX        =   20479
         _ExtentY        =   12700
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tab             =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00122.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "CmDialog1"
         Tab(0).Control(1)=   "SSDateCombo1(0)"
         Tab(0).Control(2)=   "Command1(0)"
         Tab(0).Control(3)=   "txtText1(3)"
         Tab(0).Control(4)=   "txtText1(2)"
         Tab(0).Control(5)=   "txtText1(1)"
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(7)=   "txtText1(4)"
         Tab(0).Control(8)=   "txtText1(6)"
         Tab(0).Control(9)=   "txtText1(7)"
         Tab(0).Control(10)=   "txtText1(8)"
         Tab(0).Control(11)=   "txtText1(5)"
         Tab(0).Control(12)=   "cboCombo1(0)"
         Tab(0).Control(13)=   "cboCombo1(1)"
         Tab(0).Control(14)=   "cbossResult"
         Tab(0).Control(15)=   "Label1(3)"
         Tab(0).Control(16)=   "Image1"
         Tab(0).Control(17)=   "Label1(1)"
         Tab(0).Control(18)=   "Label1(0)"
         Tab(0).Control(19)=   "Label1(4)"
         Tab(0).Control(20)=   "Label1(5)"
         Tab(0).Control(21)=   "Label1(6)"
         Tab(0).Control(22)=   "Label1(7)"
         Tab(0).Control(23)=   "Label1(8)"
         Tab(0).Control(24)=   "Label1(9)"
         Tab(0).ControlCount=   25
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "mb00122.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Fotos"
         TabPicture(2)   =   "mb00122.frx":0038
         Tab(2).ControlEnabled=   -1  'True
         Tab(2).Control(0)=   "SSPanel1"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).ControlCount=   1
         Begin Threed.SSPanel SSPanel1 
            Height          =   7095
            Left            =   60
            TabIndex        =   27
            Top             =   60
            Visible         =   0   'False
            Width           =   11175
            _Version        =   65536
            _ExtentX        =   19711
            _ExtentY        =   12515
            _StockProps     =   15
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelOuter      =   0
            Begin VB.TextBox Text1 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0C0C0&
               BorderStyle     =   0  'None
               Height          =   300
               Index           =   0
               Left            =   180
               TabIndex        =   28
               Top             =   4560
               Visible         =   0   'False
               Width           =   3315
            End
            Begin VB.Image FotoGrafia 
               BorderStyle     =   1  'Fixed Single
               Height          =   4305
               Index           =   0
               Left            =   3120
               Stretch         =   -1  'True
               Top             =   780
               Visible         =   0   'False
               Width           =   3315
            End
         End
         Begin MSComDlg.CommonDialog CmDialog1 
            Left            =   -70080
            Top             =   2640
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   327681
         End
         Begin VB.PictureBox SSDateCombo1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_FECHAALTA"
            Height          =   330
            Index           =   0
            Left            =   -70080
            ScaleHeight     =   270
            ScaleWidth      =   1800
            TabIndex        =   17
            Tag             =   "Fecha Alta"
            Top             =   7770
            Width           =   1860
         End
         Begin VB.CommandButton Command1 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   -70080
            TabIndex        =   16
            Top             =   3180
            Width           =   345
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_NOMBREFOTO"
            Height          =   285
            HelpContextID   =   30101
            Index           =   3
            Left            =   -74745
            MaxLength       =   40
            TabIndex        =   15
            Tag             =   "Archivo Foto | Foto del Medicamento"
            Top             =   3210
            Width           =   4605
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_DESCRIPCION"
            Height          =   1155
            HelpContextID   =   30101
            Index           =   2
            Left            =   -74745
            MaxLength       =   300
            MultiLine       =   -1  'True
            TabIndex        =   14
            Tag             =   "Descripcion | Descripcion Medicamento"
            Top             =   1710
            Width           =   5010
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_REFERENCIA"
            Height          =   285
            HelpContextID   =   30101
            Index           =   1
            Left            =   -74745
            MaxLength       =   15
            TabIndex        =   13
            Tag             =   "Referencia|Referencia del Medicamento"
            Top             =   1110
            Width           =   2175
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_CODIGO"
            Height          =   285
            HelpContextID   =   30101
            Index           =   0
            Left            =   -74760
            MaxLength       =   12
            TabIndex        =   12
            Tag             =   "C�digo|C�digo Medicamento"
            Top             =   540
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_UBICACION"
            Height          =   285
            HelpContextID   =   30101
            Index           =   4
            Left            =   -74745
            MaxLength       =   8
            TabIndex        =   11
            Tag             =   "Ubicaci�n|Ubicaci�n del Medicamento"
            Top             =   5490
            Width           =   1245
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_VVENT"
            Height          =   285
            HelpContextID   =   30101
            Index           =   6
            Left            =   -72705
            MaxLength       =   15
            TabIndex        =   10
            Tag             =   "Vvent | Vvent del Medicamento"
            Top             =   7830
            Width           =   2235
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_PROVEEDOR"
            Height          =   285
            HelpContextID   =   30101
            Index           =   7
            Left            =   -72900
            MaxLength       =   36
            TabIndex        =   9
            Tag             =   "Proveedor | Proveedor del Medicamento"
            Top             =   3840
            Width           =   3135
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_GRUPO"
            Height          =   285
            HelpContextID   =   30101
            Index           =   8
            Left            =   -72900
            MaxLength       =   36
            TabIndex        =   8
            Tag             =   "Grupo | Grupo del Medicamento"
            Top             =   4530
            Width           =   3135
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_ENVASE"
            Height          =   285
            HelpContextID   =   30101
            Index           =   5
            Left            =   -72420
            MaxLength       =   5
            TabIndex        =   7
            Tag             =   "Envase | Envase del Medicamento"
            Top             =   5460
            Width           =   1245
         End
         Begin VB.ComboBox cboCombo1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_PROVEEDOR"
            Height          =   315
            Index           =   0
            Left            =   -74760
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   3840
            Width           =   1770
         End
         Begin VB.ComboBox cboCombo1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01_GRUPO"
            Height          =   315
            Index           =   1
            Left            =   -74760
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   4530
            Width           =   1770
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   0
            Left            =   -75210
            TabIndex        =   1
            TabStop         =   0   'False
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   15266
            _ExtentY        =   5054
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cbossResult 
            DataField       =   "CUNIDAD"
            Height          =   315
            Left            =   -72480
            TabIndex        =   29
            Tag             =   "Unidad|Unidad"
            Top             =   540
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label Label1 
            Caption         =   "Foto"
            DataField       =   "GC50_FOTO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74760
            TabIndex        =   26
            Tag             =   "Foto | Path de la Foto"
            Top             =   3000
            Width           =   1455
         End
         Begin VB.Image Image1 
            BorderStyle     =   1  'Fixed Single
            Height          =   6945
            Left            =   -69600
            Stretch         =   -1  'True
            Top             =   120
            Width           =   5775
         End
         Begin VB.Label Label1 
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74760
            TabIndex        =   25
            Top             =   870
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74760
            TabIndex        =   24
            Top             =   300
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Ubicaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -74760
            TabIndex        =   23
            Top             =   5250
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Envase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   -72420
            TabIndex        =   22
            Top             =   5190
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "VVent"
            Height          =   255
            Index           =   6
            Left            =   -72720
            TabIndex        =   21
            Top             =   7590
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha de Alta"
            DataField       =   "FR01_FALTA"
            Height          =   255
            Index           =   7
            Left            =   -70080
            TabIndex        =   20
            Top             =   7530
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   -74760
            TabIndex        =   19
            Top             =   3600
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   -74760
            TabIndex        =   18
            Top             =   4260
            Width           =   1455
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   3
      Top             =   8055
      Width           =   11970
      _ExtentX        =   21114
      _ExtentY        =   609
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Image Image3 
      BorderStyle     =   1  'Fixed Single
      Height          =   8325
      Index           =   0
      Left            =   0
      Stretch         =   -1  'True
      Top             =   0
      Visible         =   0   'False
      Width           =   8115
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
      Begin VB.Menu cmanual 
         Caption         =   "C&arga manual"
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Problema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objDetailInfo As New clsCWForm
Dim nreg As Integer, numpag As Integer
Dim pagina() As Variant
Dim nver As Integer
Dim nhor As Integer
Dim anch As Double
Dim alt As Double
Dim sepve As Double
Dim sepho As Double
Dim nfotospagina As Byte
Dim paginactual As Integer
Dim primera As Integer
Dim posic As Integer 'posicion del cursor :inicio 0 o final de pagina 1
Dim navegacion As Boolean 'indica si se esta navegando
'desde la 1� pag (true) o desde la ultima (false)
Dim nfot As Integer
Dim EofBof As Boolean
Dim Ref As Variant
Dim aviso As Boolean, aviso2 As Boolean

Sub Mostrar(Inicio As Boolean, NumFotos As Byte)

Dim q As Byte

    For q = nfotospagina * Abs(Not Inicio) + 1 * Abs(Inicio) To NumFotos
        FotoGrafia(q).Visible = True
        Text1(q).Visible = True
    Next q
    SSPanel1.Visible = True
    
End Sub
Sub Ocultar()

Dim q As Byte

    SSPanel1.Visible = False
    For q = 1 To nfotospagina
        FotoGrafia(q).Visible = False
        Text1(q).Visible = False
        FotoGrafia(q).Picture = LoadPicture()
    Next q
    
End Sub
Sub MoverRegistro(HaciaDelante As Boolean)

'    stbStatusBar1.Panels(2).Text = "Cursor en: " & objDetailInfo.rdoCursor("fr01_CODIGO") & " nfot: " & nfot   '& " Aviso: " & aviso & " Aviso2: " & aviso2
    
    With objDetailInfo
        If HaciaDelante Eqv objDetailInfo.blnDirection Then
            .rdoCursor.MoveNext
        Else
            .rdoCursor.MovePrevious
        End If
        If .rdoCursor.EOF Or .rdoCursor.BOF Then
            If HaciaDelante Then
                If .blnDirection Then .rdoCursor.MovePrevious Else .rdoCursor.MoveNext
                objWinInfo.DataMoveNext
            Else
                If .blnDirection Then .rdoCursor.MoveNext Else .rdoCursor.MovePrevious
                objWinInfo.DataMovePrevious
            End If
            EofBof = True
        End If
    End With
    
    stbStatusBar1.Panels(2).Text = objDetailInfo.rdoCursor("fr01_CODIGO") & " nfot: " & nfot  '& " Aviso: " & aviso & " Aviso2: " & aviso2
    
End Sub
Sub Siguiente()
    If objDetailInfo.rdoCursor.EOF Then
        EofBof = True
        MoverRegistro False
        objWinInfo.DataMoveNext
        nfot = 4
    Else
        EofBof = False
        MoverRegistro True
    End If
End Sub
Sub Anterior()
    If objDetailInfo.rdoCursor.BOF Then
        EofBof = True
        MoverRegistro True
        objWinInfo.DataMovePrevious
        nfot = 4
    Else
        EofBof = False
        MoverRegistro False
    End If
End Sub
Sub CreatePage()

    Dim i%, j%, q%, X%, txt$
    
    
    
     'frmA_Problema.objDetailInfo.rdoCursor.rdoColumns ("FR01_CODIGO")
    'Text1(1).Text = objDetailInfo.blnDirection & ", " & objDetailInfo.rdoCursor.rdoColumns(0)
    
    posic = 1
    aviso = False
    If objDetailInfo.rdoCursor("fr01_codigo") = code2 Then
        aviso2 = True
    End If
    
    'Al cambiarnos de pagina borramos lo que habia
    For q = 1 To nfotospagina
        FotoGrafia(q).Visible = False
        Text1(q).Visible = False
        FotoGrafia(q).Picture = LoadPicture()
    Next q
    
    q = 1
    
    SSPanel1.Visible = False
    For i = 1 To nver
        For j = 1 To nhor
            'If q = frmFotos.objDetailInfo.rdoCursor.RowCount Then Exit Function
             If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                 On Error Resume Next
                 FotoGrafia(q).Picture = LoadPicture(Trim$(objDetailInfo.rdoCursor("fr01_nombrefoto")))
                 'frmFotos.Image2(q).Picture = LoadPicture(Trim$(grdSSDBGrid1.Columns(5).Text))
                 Err = 0
             End If
             
             txt = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
'                 txtMedicamento(q).Text = grdSSDBGrid1.Columns(0).Text & " - " & grdSSDBGrid1.Columns(1).Text
             q = q + 1
             'frmFotos.objDetailInfo.rdoCursor.MoveNext
             If objDetailInfo.blnDirection = False Then
'                Call objWinInfo.stdWinProcess(cwProcessToolBar, 23, 0)
                Call objWinInfo.DataMoveNext
             Else
'                Call objWinInfo.stdWinProcess(cwProcessToolBar, 22, 0)
                Call objWinInfo.DataMoveNext
             End If
             
            Text1(q - 1) = txt
            'Text1(0).Text = frmFotos.objDetailInfo.rdoStatement.SQL
            
            If aviso2 Then
                nfot = q + 1
                'SSPanel1.visible = True
                For X = 1 To q + 1
                    FotoGrafia(X).Visible = True
                    Text1(X).Visible = True
                Next X
                SSPanel1.Visible = True
                'Text1(1).Text = Text1(1).Text & "nfot" & nfot
                'Text1(1).Text = Text1(1).Text & "fin:" & objDetailInfo.blnDirection & ", " & frmFotos.objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
                Screen.MousePointer = 0
                Exit Sub
            End If
            
            If objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code2 Then
                aviso2 = True
            End If
            
            'If frmFotos.objDetailInfo.rdoCursor.EOF = True Then
'                frmfotos.frame1.Caption = "Registros N� " & nfotospagina * (page - 1) + 1 & " a " & nfotospagina * (page - 1) + q
'                frmfotos.Frame1.Caption = "P�gina " & page & " de " & numpag
                'frmFotos.objDetailInfo.rdoCursor.Bookmark = pagina(page)
            '    Exit Sub
            'End If
        Next j
    Next i
    
    'Text1(1).Text = Text1(1).Text & "fin:" & objDetailInfo.blnDirection & ", " & objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    
    For q = 1 To nfotospagina
        FotoGrafia(q).Visible = True
        Text1(q).Visible = True
    Next q
    
    SSPanel1.Visible = True
    nfot = nfotospagina + 1
'    Text1(1).Text = Text1(1).Text & "nfot" & NFot
    Screen.MousePointer = 0

End Sub
Sub PosicionaFotos()
Dim i As Byte, j As Byte, n As Integer
'segun el numero de fotos por pagina creamos los images y los ponemos
'para la 3� pesta�a
    LeerDimPagina
    For i = 1 To nver
        For j = 1 To nhor
            n = n + 1
            Load FotoGrafia(n)
            FotoGrafia(n).Width = (SSPanel1.Width - (nhor + 1) * sepho * 567) / nhor
            FotoGrafia(n).Height = (SSPanel1.Height - (nver + 1) * sepve * 567) / nver - 325 'se resta 325 para que entre el texto
            FotoGrafia(n).Left = j * sepho * 567 + FotoGrafia(n).Width * (j - 1)
            FotoGrafia(n).Top = i * sepve * 567 + (FotoGrafia(n).Height + 325) * (i - 1)
            FotoGrafia(n).Visible = True
            Load Text1(n)
            Text1(n).Left = FotoGrafia(n).Left
            Text1(n).Width = FotoGrafia(n).Width
            Text1(n).Top = FotoGrafia(n).Top + FotoGrafia(n).Height + 25
            Text1(n).Height = 300
            Text1(n).BackColor = &HC0C0C0
            Text1(n).Visible = True
        Next j
    Next i
End Sub

Sub PagePrevious()
    Dim i%, j%, q%, X%, txt$
      
    If aviso Then Exit Sub
    If EofBof Then
        objDetailInfo.rdoCursor.Bookmark = Ref
        EofBof = False
    End If
    If posic = 1 Then
        For i = 1 To nfot: MoverRegistro False: Next i
        posic = 0
    End If
    aviso2 = False
    If objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code1 Then aviso = True
    SSPanel1.Visible = False
    'Al cambiarnos de pagina borramos lo que habia
    Ocultar
    q = nfotospagina
    For i = nver To 1 Step -1
        For j = nhor To 1 Step -1
            If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                On Error Resume Next
                FotoGrafia(q).Picture = LoadPicture(Trim$(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                Err = 0
            End If
            txt = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
            q = q - 1
            If Not aviso Then MoverRegistro False
            Text1(q + 1).Text = txt
            If aviso Then
                nfot = nfotospagina - q
                Mostrar False, q + 1
                Screen.MousePointer = 0
                Exit Sub
            End If
            If objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") = code1 Then aviso = True
            Text1(0).Text = objDetailInfo.rdoStatement.SQL
        Next j
    Next i
    If EofBof Then
        Ref = objDetailInfo.rdoCursor.Bookmark
    End If
    Mostrar True, nfotospagina
    nfot = nfotospagina + 1
    Screen.MousePointer = 0
End Sub

Sub PageLast()
    
    Dim i%, j%, q%, X%, txt
    
    navegacion = False
    q = 0
    If aviso2 Then Exit Sub
    aviso2 = True
    aviso = False
    'Al cambiarnos de pagina borramos lo que habia
    Ocultar
    posic = 0
    objWinInfo.DataMoveLast
    code2 = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    q = nfotospagina
    SSPanel1.Visible = False
    For i = nver To 1 Step -1
        For j = nhor To 1 Step -1
            'If q = frmFotos.objDetailInfo.rdoCursor.RowCount Then Exit Function
            If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                On Error Resume Next
                FotoGrafia(q).Picture = LoadPicture(Trim$(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                Err = 0
            End If
            Text1(q).Text = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
            q = q - 1
            MoverRegistro False
            Text1(q).Text = txt
            Text1(0).Text = objDetailInfo.rdoStatement.SQL
        Next j
    Next i
    Mostrar True, nfotospagina
    nfot = nfotospagina + 1
    'Text1(1).Text = Text1(1).Text & "nfot" & NFot
    Screen.MousePointer = 0
     
End Sub
Sub LeerDimPagina()

    Dim res%, i%, j%
    Dim alto As String * 5
    Dim ancho As String * 5
    Dim sepver As String * 5
    Dim sephor As String * 5
    
   ' res = GetPrivateProfileString("config", "anchura", "6", ancho, 5, "C:\FARMACIA\DIMFOTOS.INI")
   ' res = GetPrivateProfileString("config", "altura", "6", alto, 5, "C:\FARMACIA\DIMFOTOS.INI")
   ' res = GetPrivateProfileString("config", "sephoriz", "1", sephor, 5, "C:\FARMACIA\DIMFOTOS.INI")
   ' res = GetPrivateProfileString("config", "sepverti", "1", sepver, 5, "C:\FARMACIA\DIMFOTOS.INI")
   '
   ' anch = CDbl(Left(ancho, InStr(ancho, Chr(0)) - 1))
   ' alt = CDbl(Left(alto, InStr(alto, Chr(0)) - 1))
   ' sepve = CDbl(Left(sephor, InStr(sephor, Chr(0)) - 1))
   ' sepho = CDbl(Left(sepver, InStr(sepver, Chr(0)) - 1))
    anch = 4
    alt = 5
    sepve = 0.5
    sepho = 0.5
   
    nhor = Int((SSPanel1.Width - 567 * sepho) / (anch * 567 + sepho * 567))
    nver = Int((SSPanel1.Height - 465 - 567 * sepve) / (alt * 567 + sepve * 567))
    
    nfotospagina = nhor * nver
    
End Sub
Function Movepage(page As Integer)

    Dim i%, j%, q%, X%
    Dim bkmrk As Variant ' Bookmarks are always defined as variants
    
    q = 0
   
    'Al cambiarnos de pagina borramos lo que habia
    For q = 1 To nfotospagina
        FotoGrafia(q).Visible = False
        Text1(q).Visible = False
        FotoGrafia(q).Picture = LoadPicture()
    Next q
        
    If objDetailInfo.rdoCursor.RowCount > 0 Then
        objDetailInfo.rdoCursor.Bookmark = pagina(page)
        paginactual = page
        q = 1
        For i = 1 To nver
            For j = 1 To nhor
                If q = objDetailInfo.rdoCursor.RowCount Then Exit Function
                 If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                     On Error Resume Next
                     FotoGrafia(q).Picture = LoadPicture(Trim$(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                     Err = 0
                 End If
                 FotoGrafia(q).Visible = True
                 Text1(q).Text = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
                 Text1(q).Visible = True
                 q = q + 1
                 objDetailInfo.rdoCursor.MoveNext
    
                If objDetailInfo.rdoCursor.EOF Then
                    objDetailInfo.rdoCursor.Bookmark = pagina(page)
                    Exit Function
                End If
            Next j
        Next i
        objDetailInfo.rdoCursor.Bookmark = pagina(page)
    End If
    
End Function
Sub CalculoNumPaginas()

Dim i As Integer, j%

nreg = objDetailInfo.rdoCursor.RowCount

If nreg / nfotospagina > Int(nreg / nfotospagina) Then
    numpag = Int(nreg / nfotospagina) + 1
Else
    numpag = Int(nreg / nfotospagina)
End If

ReDim Preserve pagina(1 To numpag)

Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
pagina(1) = objDetailInfo.rdoCursor.Bookmark

End Sub

Sub PageFirst()

    Dim i%, j%, q%, X%, txt$
    navegacion = True
    q = 0
    
    If aviso Then Exit Sub
    aviso = True
    aviso2 = False
    'Al cambiarnos de pagina borramos lo que habia
    SSPanel1.Visible = False
    Ocultar
    
    'Call objWinInfo.stdWinProcess(cwProcessToolBar, 21, 0)
    objWinInfo.DataMoveFirst
    code1 = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    
    posic = 1
    q = 1
       
    For i = 1 To nver
        For j = 1 To nhor
            'If q = frmFotos.objDetailInfo.rdoCursor.RowCount Then Exit Function
             If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                 On Error Resume Next
                 FotoGrafia(q).Picture = LoadPicture(Trim$(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")))
                 Err = 0
             End If
             txt = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
             q = q + 1
             MoverRegistro True
             Text1(q - 1).Text = txt
        Next j
    Next i
    Mostrar True, nfotospagina
    nfot = nfotospagina - 1
    Screen.MousePointer = 0
End Sub

Sub PageNext()

    Dim i%, j%, q%, X%, txt$
    
    aviso = False
    If aviso2 Then Exit Sub
    If EofBof Then
        objDetailInfo.rdoCursor.Bookmark = Ref
        EofBof = False
    End If
    If posic = 0 Then
        For i = 1 To nfot: MoverRegistro True: Next i
        posic = 1
    End If
    If objDetailInfo.rdoCursor("fr01_codigo") = code2 Then aviso2 = True
    'Al cambiarnos de pagina borramos lo que habia
    Ocultar
    q = 1
    For i = 1 To nver
        For j = 1 To nhor
            If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto")) Then
                 On Error Resume Next
                 FotoGrafia(q).Picture = LoadPicture(Trim$(objDetailInfo.rdoCursor("fr01_nombrefoto")))
                 Err = 0
            End If
            txt = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
            q = q + 1
            If Not aviso2 Then MoverRegistro True
            Text1(q - 1) = txt
            If aviso2 Then
                nfot = q - 1
                Mostrar True, q - 1
                SSPanel1.Visible = True
                Screen.MousePointer = 0
                Exit Sub
            End If
            If (objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")) = code2 Then aviso2 = True
        Next j
    Next i
    If EofBof Then
        Ref = objDetailInfo.rdoCursor.Bookmark
    End If
    Mostrar True, nfotospagina
    If Not EofBof Then nfot = nfotospagina + 1
    Screen.MousePointer = 0
    
End Sub

Private Sub cbossResult_Click()
    Call objWinInfo.CtrlDataChange
    Call objWinInfo.CtrlLostFocus
    
End Sub

Private Sub cmanual_Click()
Dim i%, pt$

pt = "d:\farmacia\fotos\"
For i = 1 To nfotospagina
    FotoGrafia(i).Picture = LoadPicture()
Next i
With objDetailInfo.rdoCursor
    For i = 1 To nfotospagina
        If objDetailInfo.rdoCursor.EOF Then .MovePrevious: objWinInfo.DataMoveNext
        If Len(objDetailInfo.rdoCursor("fr01_nombrefoto")) > 0 Then
            FotoGrafia(i).Picture = LoadPicture(objDetailInfo.rdoCursor("fr01_nombrefoto"))
        End If
        Text1(i) = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01_referencia")
        If Not objDetailInfo.rdoCursor.EOF Then objDetailInfo.rdoCursor.MoveNext 'Else objDetailInfo.rdoCursor.MovePrevious: objWinInfo.DataMoveNext  'Else objWinInfo.DataMoveNext
    Next i
End With

End Sub

Private Sub Command1_Click(Index As Integer)
'?frmfotos.objdetailinfo.rdocursor.RDOCOLUMNS("GC50_CODIGO")
    Dim q$, extension$
    Dim X, fd
    CmDialog1.DialogTitle = "Insertar el Archivo Foto"
    CmDialog1.Action = 1
    q = CmDialog1.filename
    If q <> "" Then
        txtText1(3).SetFocus
        txtText1(3).Text = q
        On Error Resume Next
        fd = FileDateTime(q)
        If Err <> 0 Then  'no existe el archivo bmp
            Err = 0
        Else
            Image1.Picture = LoadPicture(q)
            'Exit Sub
        End If
        
'        extension = Right$(q, Len(q) - InStr(q, "."))
'        If LCase(extension) = "bmp" Then
'            X = Shell("C:\Archivos de programa\Archivos comunes\Microsoft Shared\PhotoEd\PHOTOED.EXE " & q, 1)
'        End If
    End If
End Sub

Private Sub Form_Load()
  Dim strKey As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Fotos"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "FR0100"
    Call .FormAddOrderField("FR01_CODIGO", cwAscending)
    .blnHasMaint = False
    .blnAskPrimary = False
      
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Problemas")
    Call .FormAddFilterWhere(strKey, "FR01_CODIGO", "C�digo", cwString)
    Call .FormAddFilterWhere(strKey, "FR01_REFERENCIA", "Referencia", cwString)
    Call .FormAddFilterWhere(strKey, "FR01_DESCRIPCION", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR01_GRUPO", "Grupo", cwString)
    Call .FormAddFilterWhere(strKey, "FR01_PROVEEDOR", "Proveedor", cwString)
    Call .FormAddFilterWhere(strKey, "FR01_UBICACION", "Ubicaci�n", cwString)
    'Call .FormAddFilterWhere(strKey, "FR01_INDACTIVA", "�Activa?", cwBoolean)

    Call .FormAddFilterOrder(strKey, "FR01_CODIGO", "C�digo")
    Call .FormAddFilterOrder(strKey, "FR01_REFERENCIA", "Designaci�n")
    'Call .FormAddFilterOrder(strKey, "MB22_DESCRIP", "Descripci�n")
    
    Call .objPrinter.Add("MB00140", "Fotos")
    
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    '.CtrlGetInfo(chkProbl(0)).blnInFind = True
    '.CtrlGetInfo(txtText1(0)).blnValidate = False
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
  
'    SQL = "SELECT cgrupo, designacion FROM unidades"
'    SQL = SQL & " ORDER BY designacion"
'    .CtrlGetInfo(cbossResult).strSQL = SQL
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  PosicionaFotos
  CalculoNumPaginas
  nfot = nfotospagina - 2
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub FotoGrafia_DblClick(Index As Integer)
    Image3(0).Picture = FotoGrafia(Index).Picture
    fraFrame1(0).Visible = False
    frmA_Problema.Caption = Text1(Index).Text
    stbStatusBar1.Visible = False
    tlbToolbar1.Visible = False
    Image3(0).Visible = True
End Sub

Private Sub Image3_DblClick(Index As Integer)
    Image3(0).Visible = False
    fraFrame1(0).Visible = True
    frmA_Problema.Caption = "Tabla de Medicamentos"
    stbStatusBar1.Visible = True
    tlbToolbar1.Visible = True
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    If strFormName = objDetailInfo.strName Then
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtText1(0).Text = fNextClave("fr01_codigo", "MB2200")
            objDetailInfo.rdoCursor("fr01_codigo") = txtText1(0).Text
        End If
    End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objDetailInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
  End Select

End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub Text1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub Text1_GotFocus(Index As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub Text1_LostFocus(Index As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

Dim res
    ' analiza el bot�n seleccionado y ejecuta la acci�n asociada
    If tabTab1.Item(0).Tab = 2 And (btnButton.Index = 21 Or btnButton.Index = 22 Or btnButton.Index = 23 Or btnButton.Index = 24) Then
        Select Case btnButton.Index
            Case 21
                If aviso Then
                    Beep
                Else
                    PageFirst
                End If
            Case 22
                If aviso Then
                    Beep
                Else
                    PagePrevious
                End If
            Case 23
                If aviso2 Then
                    Beep
                Else
                    PageNext
                End If
            Case 24
                If aviso2 Then
                    Beep
                Else
                   PageLast
                End If
        End Select
    Else
        
        If btnButton.Index = 18 Then
            For res = 1 To nfotospagina
                FotoGrafia(res).Visible = False
                txtText1(res).Visible = False
                FotoGrafia(res).Picture = LoadPicture()
            Next res
            tabTab1.Item(0).TabIndex = 0
        End If
        
        'Select Case btnButton.Key
        '    Case cwToolBarButtonOpen
        '        Call objWinInfo.WinProcess(cwProcessRegister, 10, 0)
        '    Case Else
'                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.index, 0)
        'End Select
        If btnButton.Index = 19 Then 'Or btnButton.Index = 19 Or btnButton.Index = 1 Then
            'calculonumpag
            'Movepage (1)
            PageFirst
        End If
    End If
    
    If btnButton.Index = 8 Then
        If Not objDetailInfo.rdoCursor.EOF And Not objDetailInfo.rdoCursor.BOF Then
            stbStatusBar1.Panels(1).Text = "Cursor en: " & objDetailInfo.rdoCursor("fr01_CODIGO") & " nfot: " & nfot '& " Aviso: " & aviso & " Aviso2: " & aviso2
        Else
            stbStatusBar1.Panels(1).Text = "EOF BOF"
        End If
        Exit Sub
    End If
        
    If tabTab1.Item(0).Tab <> 2 Then Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    
    If tabTab1.Item(0).Tab = 0 Then
        On Error Resume Next
        Image1.Picture = LoadPicture(objDetailInfo.rdoCursor("fr01_nombrefoto"))
    End If
    
    If Not objDetailInfo.rdoCursor.EOF And Not objDetailInfo.rdoCursor.BOF Then
        stbStatusBar1.Panels(1).Text = "Cursor en: " & objDetailInfo.rdoCursor("fr01_CODIGO") & " nfot: " & nfot '& " Aviso: " & aviso & " Aviso2: " & aviso2
    Else
        stbStatusBar1.Panels(1).Text = "EOF BOF"
    End If
    
    DoEvents
    
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
    
    If tabTab1.Item(0).Tab <> 2 Then
        SSPanel1.Visible = False
        Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
        'If tabTab1.Item(0).Tab = 0 Then Image1.Picture = LoadPicture(objDetailInfo.rdoCursor.rdoColumns("fr01_nombrefoto"))
        
    Else
        CreatePage
        SSPanel1.Visible = True
    End If
    
End Sub

Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub chkProbl_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkProbl_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkProbl_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub txtProbl_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtProbl_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtProbl_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


