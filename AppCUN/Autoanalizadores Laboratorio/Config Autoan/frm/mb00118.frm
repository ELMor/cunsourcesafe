VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_Microorganismo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Microorganismos"
   ClientHeight    =   7095
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9855
   HelpContextID   =   30001
   Icon            =   "mb00118.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7095
   ScaleWidth      =   9855
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   420
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3585
      Left            =   240
      TabIndex        =   24
      Top             =   3120
      Width           =   9465
      Begin TabDlg.SSTab tabDatos 
         Height          =   3135
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   360
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   5530
         _Version        =   327681
         Style           =   1
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         TabCaption(0)   =   "Resultado"
         TabPicture(0)   =   "mb00118.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraResult"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Sensibilidad"
         TabPicture(1)   =   "mb00118.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraResist"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Relaci�n Antibi�ticos"
         TabPicture(2)   =   "mb00118.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "fraRelac"
         Tab(2).ControlCount=   1
         Begin VB.Frame fraResult 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2715
            Left            =   60
            TabIndex        =   30
            Top             =   360
            Width           =   9075
            Begin TabDlg.SSTab tabResult 
               Height          =   2595
               Left            =   60
               TabIndex        =   31
               TabStop         =   0   'False
               Top             =   60
               Width           =   8955
               _ExtentX        =   15796
               _ExtentY        =   4577
               _Version        =   327681
               TabOrientation  =   3
               Style           =   1
               Tabs            =   2
               TabsPerRow      =   2
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Detalle"
               TabPicture(0)   =   "mb00118.frx":0060
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(11)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(0)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(8)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(4)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(6)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "cbossResult(2)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "cbossResult(1)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "cbossResult(0)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "txtResult(1)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "chkResult"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtResult(0)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "txtResult(2)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "txtResult(3)"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).ControlCount=   13
               TabCaption(1)   =   "Tabla"
               TabPicture(1)   =   "mb00118.frx":007C
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "grdssResult(0)"
               Tab(1).ControlCount=   1
               Begin VB.TextBox txtResult 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H0000FFFF&
                  DataField       =   "MB25_CODRESMICRO"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   3
                  Left            =   3780
                  Locked          =   -1  'True
                  MaxLength       =   4
                  TabIndex        =   33
                  Top             =   420
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtResult 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFF00&
                  DataField       =   "MB18_CODMICRO"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   2
                  Left            =   3780
                  Locked          =   -1  'True
                  MaxLength       =   3
                  TabIndex        =   32
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "MB25_RESULT"
                  Height          =   555
                  HelpContextID   =   40101
                  Index           =   0
                  Left            =   1200
                  Locked          =   -1  'True
                  MaxLength       =   1000
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   10
                  Tag             =   "Descripci�n|Descripci�n del resultado"
                  Top             =   1020
                  Width           =   7140
               End
               Begin VB.CheckBox chkResult 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Activo"
                  DataField       =   "MB25_INDACTIVA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Left            =   5100
                  TabIndex        =   12
                  Tag             =   "Activa|Activa"
                  Top             =   600
                  Width           =   945
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "MB25_OBSERV"
                  Height          =   765
                  HelpContextID   =   40101
                  Index           =   1
                  Left            =   1560
                  Locked          =   -1  'True
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   11
                  Tag             =   "Observaciones|Observaciones"
                  Top             =   1680
                  Width           =   6780
               End
               Begin SSDataWidgets_B.SSDBGrid grdssResult 
                  Height          =   2310
                  Index           =   0
                  Left            =   -74850
                  TabIndex        =   34
                  TabStop         =   0   'False
                  Top             =   105
                  Width           =   8205
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  RecordSelectors =   0   'False
                  Col.Count       =   0
                  BevelColorFrame =   0
                  BevelColorHighlight=   16777215
                  AllowUpdate     =   0   'False
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   2
                  AllowGroupSwapping=   0   'False
                  AllowGroupShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  MaxSelectedRows =   0
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterVisible =   -1  'True
                  Columns(0).Width=   3200
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   4096
                  UseDefaults     =   -1  'True
                  _ExtentX        =   14473
                  _ExtentY        =   4075
                  _StockProps     =   79
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "MB09_CODTEC"
                  Height          =   315
                  Index           =   0
                  Left            =   1200
                  TabIndex        =   7
                  Tag             =   "T�cnica|T�cnica"
                  Top             =   180
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16776960
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "MB32_CODRESULT"
                  Height          =   315
                  Index           =   1
                  Left            =   5820
                  TabIndex        =   8
                  Tag             =   "Resultado|Resultado"
                  Top             =   180
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  AllowNull       =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16776960
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "CUNIDAD"
                  Height          =   315
                  Index           =   2
                  Left            =   1200
                  TabIndex        =   9
                  Tag             =   "Unidad|Unidad"
                  Top             =   600
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Valor:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   180
                  TabIndex        =   39
                  Top             =   1020
                  Width           =   900
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "T�cnica:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   180
                  TabIndex        =   38
                  Top             =   180
                  Width           =   900
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Observaciones:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   60
                  TabIndex        =   37
                  Top             =   1680
                  Width           =   1425
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Unidad:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   180
                  TabIndex        =   36
                  Top             =   600
                  Width           =   900
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Resultado:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   4380
                  TabIndex        =   35
                  Top             =   240
                  Width           =   1305
               End
            End
         End
         Begin VB.Frame fraResist 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2550
            Left            =   -74940
            TabIndex        =   28
            Top             =   360
            Width           =   9060
            Begin SSDataWidgets_B.SSDBGrid grdssResist 
               Height          =   2430
               Index           =   0
               Left            =   120
               TabIndex        =   29
               Top             =   120
               Width           =   8820
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15557
               _ExtentY        =   4286
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraRelac 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2670
            Left            =   -74940
            TabIndex        =   26
            Top             =   360
            Width           =   9060
            Begin SSDataWidgets_B.SSDBGrid grdssRelac 
               Height          =   2430
               Index           =   0
               Left            =   120
               TabIndex        =   27
               Top             =   120
               Width           =   8820
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15557
               _ExtentY        =   4286
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraMicro 
      Caption         =   "Microorganismos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2565
      Left            =   240
      TabIndex        =   13
      Top             =   480
      Width           =   9465
      Begin TabDlg.SSTab tabMicro 
         Height          =   2070
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   360
         Width           =   9075
         _ExtentX        =   16007
         _ExtentY        =   3651
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00118.frx":0098
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(9)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(10)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "cbossMicro(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cbossMicro(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtMicro(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtMicro(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtMicro(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "chkMicro(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtMicro(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtMicro(4)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "fraLaboratorio"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "mb00118.frx":00B4
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssMicro"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraLaboratorio 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   375
            Left            =   5280
            TabIndex        =   41
            Top             =   1260
            Width           =   2415
            Begin VB.CheckBox chkMicro 
               Alignment       =   1  'Right Justify
               Caption         =   "Laboratorio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   1
               Left            =   0
               TabIndex        =   42
               Tag             =   "Activa|Activa"
               Top             =   60
               Width           =   1650
            End
         End
         Begin VB.TextBox txtMicro 
            BackColor       =   &H00FFFFFF&
            DataField       =   "cResultado"
            Height          =   285
            Index           =   4
            Left            =   5280
            MaxLength       =   20
            TabIndex        =   40
            Tag             =   "CResultado|cResultado"
            Top             =   1740
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.TextBox txtMicro 
            BackColor       =   &H00FFFFFF&
            DataField       =   "MB18_TCRECI"
            Height          =   285
            Index           =   3
            Left            =   2460
            MaxLength       =   3
            TabIndex        =   5
            Tag             =   "T. crecimiento|Tiempo de crecimiento"
            Top             =   1290
            Width           =   2475
         End
         Begin VB.CheckBox chkMicro 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "MB18_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   1680
            TabIndex        =   6
            Tag             =   "Activa|Activa"
            Top             =   1650
            Width           =   990
         End
         Begin VB.TextBox txtMicro 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB18_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   2460
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del microorganismo"
            Top             =   540
            Width           =   5895
         End
         Begin VB.TextBox txtMicro 
            BackColor       =   &H0000FFFF&
            DataField       =   "MB18_CODMICRO"
            Height          =   285
            Index           =   0
            Left            =   2460
            MaxLength       =   3
            TabIndex        =   0
            Tag             =   "C�digo|C�digo del microorganismo"
            Top             =   150
            Width           =   495
         End
         Begin VB.TextBox txtMicro 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB18_DESIG"
            Height          =   285
            Index           =   1
            Left            =   4980
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n del microorganismo"
            Top             =   150
            Width           =   3375
         End
         Begin SSDataWidgets_B.SSDBCombo cbossMicro 
            DataField       =   "MB12_CODMORF"
            Height          =   315
            Index           =   0
            Left            =   2460
            TabIndex        =   3
            Tag             =   "Morfolog�a|Morfolog�a del microorganismo"
            Top             =   900
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cbossMicro 
            DataField       =   "MB11_CODGEMICRO"
            Height          =   315
            Index           =   1
            Left            =   5880
            TabIndex        =   4
            Tag             =   "G�nero|G�nero del microorganismo"
            Top             =   900
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBGrid grdssMicro 
            Height          =   1770
            Left            =   -74880
            TabIndex        =   23
            Top             =   180
            Width           =   8460
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   14922
            _ExtentY        =   3122
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tiempo Crecimiento (h):"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   10
            Left            =   240
            TabIndex        =   22
            Top             =   1320
            Width           =   2145
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Morfolog�a:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   1320
            TabIndex        =   21
            Top             =   960
            Width           =   1050
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   1320
            TabIndex        =   20
            Top             =   570
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   1680
            TabIndex        =   19
            Top             =   150
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   3420
            TabIndex        =   18
            Top             =   210
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            Caption         =   "G�nero:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   5100
            TabIndex        =   17
            Top             =   960
            Width           =   810
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   6810
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Microorganismo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMicro As New clsCWForm
Dim objResult As New clsCWForm
Dim objResist As New clsCWForm
Dim objRelac As New clsCWForm

Dim blnPostRead As Boolean
Private Sub cbossMicro_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
    Call cbossMicro_LostFocus(intIndex)
End Sub

Private Sub cbossMicro_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossMicro_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossMicro_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossResult_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossResult_CloseUp(Index As Integer)
    'Se llena el combo de los resultados seg�n la t�cnica seleccionada
    If Index = 0 Then
        If cbossResult(0).Columns(0).Text <> "" Then
            cbossResult(0).Text = cbossResult(0).Columns(1).Text
            Call pComboResultCargar(objWinInfo, cbossResult(1), cbossResult(0).Columns(0).Text)
            cbossResult(1).Text = cbossResult(1).Columns(1).Text
        End If
    End If
    
End Sub

Private Sub cbossResult_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossResult_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossResult_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkMicro_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

  Select Case intIndex
    Case 0
        'Call objWinInfo.CtrlDataChange
    Case 1  ' Laboratorio
        If blnPostRead = False Then
            If chkMicro(1).Value = 1 Then
                txtMicro(4).Text = fCodigoResultadoLaboratorio(departamento, False)
            Else
                txtMicro(4).Text = ""
            End If
        End If
End Select
  
End Sub

Private Sub chkMicro_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkMicro_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkResult_Click()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkResult_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkResult_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub


Private Sub Form_Load()
  Dim strKey As String, SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMicro
    .strName = "Micro"
    Set .objFormContainer = fraMicro
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabMicro
    Set .grdGrid = grdssMicro
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1800"
    SQL = "MB1800.MB11_codGeMicro IN"
    SQL = SQL & "      (SELECT MB1100.MB11_codGeMicro FROM MB1100,MB0800"
    SQL = SQL & "       WHERE MB1100.MB08_codTiMic = MB0800.MB08_codTiMic"
    SQL = SQL & "       AND cDptoSecc = " & departamento & ")"
    .strWhere = SQL 'nombre tabla para report
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("MB18_CODMICRO", cwAscending)
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Microorganismos")
    Call .FormAddFilterWhere(strKey, "MB1800.MB18_CODMICRO", "C�digo microorganismo", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB1800.MB11_CODGEMICRO", "C�digo g�nero", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB1800.MB12_CODMORF", "C�digo morfolog�a", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB18_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB18_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB18_TCRECI", "Tiempo crecimiento", cwNumeric)
    Call .FormAddFilterWhere(strKey, "MB18_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "MB1800.MB18_CODMICRO", "C�digo microorganismo") 'nombre tabla para report
    Call .FormAddFilterOrder(strKey, "MB1800.MB11_CODGEMICRO", "C�digo g�nero") 'nombre tabla para report
    Call .FormAddFilterOrder(strKey, "MB1800.MB12_CODMORF", "C�digo morfolog�a") 'nombre tabla para report
    Call .FormAddFilterOrder(strKey, "MB18_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "MB18_DESCRIP", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "MB18_TCRECI", "Tiempo crecimiento")
    
    Call .objPrinter.Add("MB00152", "Microorganismos agrupados por G�nero")
    Call .objPrinter.Add("MB00153", "Microorganismos agrupados por Morfolog�a")
    Call .objPrinter.Add("MB00154", "Microorganismos ordenados alfab�ticamente")
    Call .objPrinter.Add("MB00155", "Microorganismos frente a T�cnicas")
    Call .objPrinter.Add("MB00156", "Microorganismos frente a Antibi�ticos")
    Call .objPrinter.Add("MB00157", "Relaci�n entre Antibi�ticos")
  End With
  
  With objResult
    .strName = "Result"
    Set .objFormContainer = fraResult
    Set .objFatherContainer = fraMicro
    Set .tabMainTab = tabResult
    Set .grdGrid = grdssResult(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2500"
    .blnAskPrimary = False
    Call .FormAddOrderField("MB09_CODTEC", cwAscending)
    Call .FormAddOrderField("MB32_CODRESULT", cwAscending)
    Call .FormAddRelation("MB18_CODMICRO", txtMicro(0))
    
    'Call .objPrinter.Add("MB00155", "Microorganismo frente a t�cnicas") 'columna ambigua
  End With
    
  With objResist
    .strName = "Resist"
    Set .objFormContainer = fraResist
    Set .objFatherContainer = fraMicro
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssResist(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2400"
    .intAllowance = cwAllowAll

    Call .FormAddOrderField("MB07_CODANTI", cwAscending)
    Call .FormAddOrderField("MB40_CODREAC", cwAscending)
    Call .FormAddRelation("MB18_CODMICRO", txtMicro(0))
    
    'Call .objPrinter.Add("MB00156", "Microorganismos frente a antibi�ticos") 'columna ambigua

  End With
  
  With objRelac
    .strName = "Relac"
    Set .objFormContainer = fraRelac
    Set .objFatherContainer = fraMicro
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssRelac(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2300"
    .intAllowance = cwAllowAll

    Call .FormAddOrderField("MB07_CODANTI_1", cwAscending)
    Call .FormAddOrderField("MB07_CODANTI_2", cwAscending)
    Call .FormAddOrderField("MB40_CODREAC_1", cwAscending)
    Call .FormAddOrderField("MB40_CODREAC_2", cwAscending)
    Call .FormAddRelation("MB18_CODMICRO", txtMicro(0))
    
    'Call .objPrinter.Add("MB00157", "Microorganismos y la relaci�n entre antibi�ticos") 'columna ambigua
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMicro, cwFormDetail)
    Call .FormAddInfo(objResult, cwFormDetail)
    Call .FormAddInfo(objResist, cwFormMultiLine)
    
    Call .GridAddColumn(objResist, "Microorganismo", "MB18_CODMICRO", cwNumeric, 3)
    Call .GridAddColumn(objResist, "Antibi�tico", "MB07_CODANTI", cwNumeric, 2)
    Call .GridAddColumn(objResist, "Sensibilidad", "MB40_CODREAC", cwNumeric, 1)
    Call .GridAddColumn(objResist, "Activa", "MB24_INDACTIVA", cwBoolean, 1)
    
    Call .FormAddInfo(objRelac, cwFormMultiLine)
    Call .GridAddColumn(objRelac, "Microorganismo ", "MB18_CODMICRO", cwNumeric, 3)
    Call .GridAddColumn(objRelac, "Antibi�tico 1", "MB07_CODANTI_1", cwNumeric, 2)
    Call .GridAddColumn(objRelac, "Sensib. Antib. 1", "MB40_CODREAC_1", cwNumeric, 1)
    Call .GridAddColumn(objRelac, "Antibi�tico 2", "MB07_CODANTI_2", cwNumeric, 2)
    Call .GridAddColumn(objRelac, "Sensib. Antib. 2", "MB40_CODREAC_2", cwNumeric, 1)
    Call .GridAddColumn(objRelac, "Activa", "MB23_INDACTIVA", cwBoolean, 1)
    
    Call .FormCreateInfo(objMicro)
    
    .CtrlGetInfo(chkMicro(0)).blnInFind = True
    .CtrlGetInfo(txtMicro(0)).blnValidate = False
    .CtrlGetInfo(txtMicro(0)).blnInFind = True
    .CtrlGetInfo(txtMicro(1)).blnInFind = True
    .CtrlGetInfo(txtMicro(2)).blnInFind = True
    .CtrlGetInfo(txtMicro(3)).blnInFind = True
    .CtrlGetInfo(cbossMicro(0)).blnInFind = True
    .CtrlGetInfo(cbossMicro(1)).blnInFind = True
    .CtrlGetInfo(chkMicro(1)).blnMandatory = False
    .CtrlGetInfo(txtMicro(4)).blnInGrid = False
    
    ' se llenan los combos del formulario superior
    SQL = "SELECT MB12_CodMorf, MB12_Desig FROM MB1200"
    SQL = SQL & " WHERE MB12_indActiva = -1"
    SQL = SQL & " AND MB08_codTiMic IN"
    SQL = SQL & "        (SELECT MB08_codTiMic FROM MB0800"
    SQL = SQL & "         WHERE cDptoSecc = " & departamento & ")"
    SQL = SQL & " ORDER BY MB12_Desig"
    .CtrlGetInfo(cbossMicro(0)).strSQL = SQL
    .CtrlGetInfo(cbossMicro(0)).blnForeign = True
    
    SQL = "SELECT MB11_CodGeMicro, MB11_Desig FROM MB1100"
    SQL = SQL & " WHERE MB11_indActiva = -1"
    SQL = SQL & " AND MB08_codTiMic IN"
    SQL = SQL & "        (SELECT MB08_codTiMic FROM MB0800"
    SQL = SQL & "         WHERE cDptoSecc = " & departamento & ")"
    SQL = SQL & " ORDER BY MB11_Desig"
    .CtrlGetInfo(cbossMicro(1)).strSQL = SQL
    .CtrlGetInfo(cbossMicro(1)).blnForeign = True
      
    Call .FormChangeColor(objResult)
    .CtrlGetInfo(txtResult(2)).blnInGrid = False
    .CtrlGetInfo(txtResult(3)).blnValidate = False
    .CtrlGetInfo(txtResult(3)).blnInGrid = False
    Call .FormChangeColor(objResist)
    grdssResist(0).Columns(3).Visible = False
    Call .FormChangeColor(objRelac)
    grdssRelac(0).Columns(3).Visible = False
    
    ' se llenan DropDowns y combos de los formularios inferiores
    SQL = "SELECT MB09_codTec, MB09_desig FROM MB0900"
    SQL = SQL & " WHERE cDptoSecc=" & departamento
    SQL = SQL & " AND MB09_indActiva =-1"
    SQL = SQL & " AND (MB04_codTiTec <> " & constCULTIVO & " AND MB04_codTiTec <> " & constANTIBIOGRAMA & ")"
    SQL = SQL & " AND MB09_codTec  IN"
    SQL = SQL & "       (SELECT DISTINCT MB09_codTec FROM MB3200"
    SQL = SQL & "        WHERE MB32_indActiva = -1)"
    SQL = SQL & " ORDER BY MB09_desig"
    .CtrlGetInfo(cbossResult(0)).strSQL = SQL
    .CtrlGetInfo(cbossResult(0)).blnForeign = True
    
    SQL = "SELECT MB32_codResult,MB32_desig FROM MB3200"
    SQL = SQL & " WHERE MB09_CODTEC = 0" 'No traer ning�n resultado hasta que no se seleccione la t�cnica
    SQL = SQL & " ORDER BY MB32_desig"
    .CtrlGetInfo(cbossResult(1)).strSQL = SQL
'    .CtrlGetInfo(cbossResult(1)).blnForeign = True
    
    SQL = "SELECT cUnidad, designacion FROM unidades"
    SQL = SQL & " ORDER BY designacion"
    .CtrlGetInfo(cbossResult(2)).strSQL = SQL
    
    SQL = "SELECT MB07_CodAnti, MB07_Desig FROM MB0700 "
    SQL = SQL & " WHERE MB07_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB07_Desig"
    .CtrlGetInfo(grdssResist(0).Columns(4)).strSQL = SQL
    .CtrlGetInfo(grdssRelac(0).Columns(4)).strSQL = SQL
    .CtrlGetInfo(grdssRelac(0).Columns(6)).strSQL = SQL
    .CtrlGetInfo(grdssResist(0).Columns(4)).blnForeign = True
    .CtrlGetInfo(grdssRelac(0).Columns(4)).blnForeign = True
    .CtrlGetInfo(grdssRelac(0).Columns(6)).blnForeign = True
    
    SQL = "SELECT MB40_CodReac, MB40_Desig FROM MB4000"
    SQL = SQL & " WHERE MB40_indActiva = -1"
    SQL = SQL & " ORDER BY MB40_Desig"
    .CtrlGetInfo(grdssResist(0).Columns(5)).strSQL = SQL
    .CtrlGetInfo(grdssRelac(0).Columns(5)).strSQL = SQL
    .CtrlGetInfo(grdssRelac(0).Columns(7)).strSQL = SQL
    .CtrlGetInfo(grdssResist(0).Columns(5)).blnForeign = True
    .CtrlGetInfo(grdssRelac(0).Columns(5)).blnForeign = True
    .CtrlGetInfo(grdssRelac(0).Columns(7)).blnForeign = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub fraDatos_Click()
    Select Case tabDatos.Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraResult, True, True)
    Case 1
      Call objWinInfo.FormChangeActive(fraResist, True, True)
    Case 2
      Call objWinInfo.FormChangeActive(fraRelac, True, True)
    End Select
End Sub

Private Sub fraMicro_Click()
  Call objWinInfo.FormChangeActive(fraMicro, False, True)
End Sub

Private Sub fraResist_Click()
    Call objWinInfo.FormChangeActive(fraResist, False, True)
End Sub

Private Sub fraResult_Click()
    Call objWinInfo.FormChangeActive(fraResult, False, True)
End Sub

Private Sub grdssResist_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssResist_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssResist_GotFocus(intIndex As Integer)
Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssResist_RowColChange(intIndex As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
  Call objWinInfo.GridChangeRowCol(LastRow, LastCol)
End Sub
Private Sub grdssRelac_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssRelac_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssRelac_GotFocus(intIndex As Integer)
Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssRelac_RowColChange(intIndex As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
  Call objWinInfo.GridChangeRowCol(LastRow, LastCol)
End Sub

Private Sub grdssResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssResult_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssResult_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssResult_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    
    On Error GoTo label
    
    Select Case strFormName
        Case objMicro.strName
            Select Case strCtrl
                Case objWinInfo.CtrlGetInfo(cbossMicro(0)).strName
                    frmA_Morfologia.Show vbModal
                    Set frmA_Morfologia = Nothing
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossMicro(0)))
                Case objWinInfo.CtrlGetInfo(cbossMicro(1)).strName
                    frmA_FamiliaMicroorganismo.Show vbModal
                    Set frmA_FamiliaMicroorganismo = Nothing
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossMicro(1)))
            End Select
        Case objResult.strName
            Select Case strCtrl
                Case objWinInfo.CtrlGetInfo(cbossResult(0)).strName
                    frmA_Tecnica.Show vbModal
                    Set frmA_Tecnica = Nothing
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossResult(0)))
            End Select
        Case objResist.strName, objRelac.strName
            Select Case strCtrl
                Case objWinInfo.CtrlGetInfo(grdssResist(0).Columns(4)).strName, objWinInfo.CtrlGetInfo(grdssRelac(0).Columns(4)).strName, objWinInfo.CtrlGetInfo(grdssRelac(0).Columns(6)).strName
                    frmA_Antibiotico.Show vbModal
                    Set frmA_Antibiotico = Nothing
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssResist(0).Columns(4)))
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssRelac(0).Columns(4)))
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssRelac(0).Columns(6)))
                    
                    grdssResist(0).Refresh
                    grdssResult(0).Refresh
                Case objWinInfo.CtrlGetInfo(grdssResist(0).Columns(5)).strName, objWinInfo.CtrlGetInfo(grdssRelac(0).Columns(5)).strName, objWinInfo.CtrlGetInfo(grdssRelac(0).Columns(7)).strName
                    frmA_Reaccion.Show vbModal
                    Set frmA_Reaccion = Nothing
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssResist(0).Columns(5)))
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssRelac(0).Columns(5)))
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssRelac(0).Columns(7)))
                        grdssResist(0).Refresh
                    grdssResult(0).Refresh
                
            End Select
    End Select
     
label:
    If Err = 400 Then
        MsgBox "La Ventana a la que se quiere acceder ya est� activa.", vbInformation, "Acceso a Ventana"
    ElseIf Err <> 0 Then
        MsgBox Error
    End If
    
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: microorganismos, g�neros y morfolog�as
    
'Si se realiza alguna operaci�n en el formulario de microorganismos(objMicro) habr� que anotar _
el mantenimiento para los microorganismos y tambi�n para los g�neros y las morfolog�as ya que _
un microorganismo ha podido cambiar su g�nero o morfolog�a
    
    If strFormName = objMicro.strName Then
        Call pAnotarMantenimiento(constMANTGENEROS)
        Call pAnotarMantenimiento(constMANTMORFOLOGIAS)
        Call pAnotarMantenimiento(constMANTMICROORGANISMOS)
    End If
    
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
     'blnPostRead = True
    If strFormName = objResult.strName Then
        If grdssResult(0).Columns(1).Text <> "" Then
            Call pComboResultCargar(objWinInfo, cbossResult(1), objResult.rdoCursor("MB09_codTec"))
            If grdssResult(0).Columns(2).Text <> "" Then
                Call pComboResultTexto(cbossResult(1), objResult.rdoCursor("MB09_codTec"), objResult.rdoCursor("MB32_codResult"))
            End If
        End If
    ElseIf strFormName = objMicro.strName Then
        If Not IsNull(objMicro.rdoCursor!cResultado) Then
            chkMicro(1).Enabled = False
            fraLaboratorio.Enabled = False
            chkMicro(1).Value = 1
        Else
            chkMicro(1).Enabled = True
            fraLaboratorio.Enabled = True
            chkMicro(1).Value = 0
        End If
    End If
    blnPostRead = False
   
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: microorganismos, g�neros y morfolog�as
    
'Si se realiza alguna operaci�n en el formulario de microorganismos(objMicro) habr� que anotar _
el mantenimiento para los microorganismos y tambi�n para los g�neros y las morfolog�as ya que _
un microorganismo ha podido cambiar su g�nero o morfolog�a
    
    If strFormName = objMicro.strName Then
        Call pAnotarMantenimiento(constMANTGENEROS)
        Call pAnotarMantenimiento(constMANTMORFOLOGIAS)
        Call pAnotarMantenimiento(constMANTMICROORGANISMOS)
        ' Si se ha introducido la relaci�n con laboratorio -> se genera el resultado en laboratorio
        If fraLaboratorio.Enabled = True And chkMicro(1).Value = 1 Then
            blnError = fInsertarResultadoLaboratorio(txtMicro(4).Text, txtMicro(2).Text, departamento)
        End If
    End If
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
    blnPostRead = True
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
 
     Select Case strFormName
     Case objMicro.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtMicro(0).Text = fNextClave("MB18_codMicro", "MB1800")
            objMicro.rdoCursor("MB18_codMicro") = txtMicro(0).Text
        End If
     Case objResult.strName
         If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtResult(3).Text = fNextClave("MB25_codResMicro", "MB2500")
            objResult.rdoCursor("MB25_codResMicro") = txtResult(3).Text
         End If
     End Select
     
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objMicro.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
    Case objResult.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
          End If
        Set objPrinter = Nothing
    Case objResist.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
            Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                        objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
    Case objRelac.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
          End If
        Set objPrinter = Nothing
  End Select
End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Select Case tabDatos.Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraResult, True, True)
    Case 1
      Call objWinInfo.FormChangeActive(fraResist, True, True)
    Case 2
      Call objWinInfo.FormChangeActive(fraRelac, True, True)
    End Select
End Sub

Private Sub tabMicro_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabMicro, False, True)
End Sub

Private Sub tabResult_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabResult, False, True)
End Sub

Private Sub Timer1_Timer()
    Select Case tabDatos.Tab
    Case 0
        fraDatos.ForeColor = fraResult.ForeColor
    Case 1
        fraDatos.ForeColor = fraResist.ForeColor
    Case 2
        fraDatos.ForeColor = fraRelac.ForeColor
    End Select
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'
'    Dim rdo As rdoResultset, SQL As String
'    Select Case btnButton.Index
'      Case 2
'          Select Case objWinInfo.objWinActiveForm.strName
'          Case objResult.strName
'              SQL = "SELECT MB25_CODRESMICRO_SEQUENCE.NEXTVAL FROM DUAL"
'              Set rdo = objApp.rdoConnect.OpenResultset(SQL)
'              txtResult(2).Text = txtMicro(0).Text
'              txtResult(3).Text = rdo(0)
'              rdo.Close
'          Case objResist.strName
'              grdssResist(0).Columns(3).Text = txtMicro(0).Text
'          Case objRelac.strName
'              grdssRelac(0).Columns(3).Text = txtMicro(0).Text
'          End Select
'    End Select
    
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssMicro_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssMicro_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssMicro_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssMicro_Change()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtMicro_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtMicro_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtMicro_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  Select Case intIndex
    Case 3
        If txtMicro(4).Text = "" Then
            chkMicro(1).Enabled = True
            fraLaboratorio.Enabled = True
            chkMicro(1).Value = 0
        End If
  End Select
  
End Sub

Private Sub txtResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtResult_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    pAutotexto KeyCode
End Sub

Private Sub txtResult_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtResult_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

