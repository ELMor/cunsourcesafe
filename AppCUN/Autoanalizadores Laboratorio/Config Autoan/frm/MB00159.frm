VERSION 5.00
Begin VB.Form frmG_Identificacion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Identificaci�n"
   ClientHeight    =   1545
   ClientLeft      =   2835
   ClientTop       =   3480
   ClientWidth     =   3750
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   912.837
   ScaleMode       =   0  'Usuario
   ScaleWidth      =   3521.047
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtUserName 
      Height          =   345
      Left            =   1080
      TabIndex        =   1
      Top             =   135
      Width           =   2445
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      Height          =   390
      Left            =   495
      TabIndex        =   4
      Top             =   1020
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   390
      Left            =   2100
      TabIndex        =   5
      Top             =   1020
      Width           =   1140
   End
   Begin VB.TextBox txtPassword 
      Height          =   345
      IMEMode         =   3  'DISABLE
      Left            =   1080
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   540
      Width           =   2445
   End
   Begin VB.Label lblLabels 
      Caption         =   "&C. Usuario:"
      Height          =   270
      Index           =   0
      Left            =   105
      TabIndex        =   0
      Top             =   180
      Width           =   960
   End
   Begin VB.Label lblLabels 
      Caption         =   "&Contrase�a:"
      Height          =   270
      Index           =   1
      Left            =   105
      TabIndex        =   2
      Top             =   600
      Width           =   1080
   End
End
Attribute VB_Name = "frmG_Identificacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public LoginSucceeded As Boolean
Private Sub cmdCancel_Click()
    LoginSucceeded = False
    Me.Hide
End Sub

Private Sub cmdOK_Click()
    Dim SQL As String
    Dim rsAcceso As rdoResultset, qryAcceso As rdoQuery
    Dim clave As String
    Dim cUser As Integer
    
    If txtUserName.Text <> "" Then
        SQL = "SELECT cTitulacion, clave, u.cUser"
        SQL = SQL & " FROM USERS u,USERSPERFIL up"
        SQL = SQL & " WHERE u.cUser=uP.cUser"
        SQL = SQL & " AND UPPER(login) = ?"
        SQL = SQL & " AND up.cDptoSecc = ?"
        Set qryAcceso = objApp.rdoConnect.CreateQuery("Acceso", SQL)
            
        qryAcceso(0) = UCase$(txtUserName.Text)
        qryAcceso(1) = departamento
        
        Set rsAcceso = qryAcceso.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        
        If rsAcceso.EOF Then
            MsgBox "El nombre de usuario es incorrecto.", , "Inicio de sesi�n"
            txtUserName.SetFocus
            SendKeys "{Home}+{End}"
        Else
            If IsNull(rsAcceso("clave")) Then
                clave = ""
            Else
                clave = rsAcceso("clave")
            End If
            
            If UCase$(clave) = UCase$(txtPassword.Text) Then
                cUser = rsAcceso("cuser")
                Call objPipe.PipeSet("MB_cUser", cUser)
                LoginSucceeded = True
                If fAcceso(constENVIARINFORMES) Then
                    tipoAccesoDoctor = True
                    Me.Hide
                ElseIf fAcceso(constVALIDARINFORMES) Then
                    tipoAccesoDoctor = False
                    Me.Hide
                Else
                    MsgBox "No dispone de acceso al formulario de petici�n de t�cnicas.", , "Inicio de sesi�n"
                    txtUserName.SetFocus
                    SendKeys "{Home}+{End}"
                End If
            Else
                MsgBox "La contrase�a es incorrecta.", , "Inicio de sesi�n"
                txtPassword.SetFocus
                SendKeys "{Home}+{End}"
            End If
        End If
        
        rsAcceso.Close
        qryAcceso.Close
    End If
    
End Sub

