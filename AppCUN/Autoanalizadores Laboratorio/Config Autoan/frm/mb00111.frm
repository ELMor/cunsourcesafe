VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_FamiliaMicroorganismo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de G�neros de Microorganismos"
   ClientHeight    =   7095
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9780
   HelpContextID   =   30001
   Icon            =   "mb00111.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7095
   ScaleWidth      =   9780
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   9780
      _ExtentX        =   17251
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   420
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3585
      Left            =   120
      TabIndex        =   22
      Top             =   3180
      Width           =   9465
      Begin TabDlg.SSTab tabDatos 
         Height          =   3150
         HelpContextID   =   90001
         Left            =   150
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   315
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   5556
         _Version        =   327681
         Style           =   1
         Tab             =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         TabCaption(0)   =   "Microorganismos"
         TabPicture(0)   =   "mb00111.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "fraMicro"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Antibi�ticos"
         TabPicture(1)   =   "mb00111.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraAnti"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Resultados"
         TabPicture(2)   =   "mb00111.frx":0044
         Tab(2).ControlEnabled=   -1  'True
         Tab(2).Control(0)=   "fraResult"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).ControlCount=   1
         Begin VB.Frame fraResult 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2745
            Left            =   60
            TabIndex        =   28
            Top             =   360
            Width           =   9075
            Begin TabDlg.SSTab tabResult 
               Height          =   2595
               Left            =   60
               TabIndex        =   29
               TabStop         =   0   'False
               Top             =   60
               Width           =   8955
               _ExtentX        =   15796
               _ExtentY        =   4577
               _Version        =   327681
               TabOrientation  =   3
               Style           =   1
               Tabs            =   2
               TabsPerRow      =   2
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Detalle"
               TabPicture(0)   =   "mb00111.frx":0060
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(11)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(10)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(8)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(6)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(4)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "cbossResult(2)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "cbossResult(1)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "cbossResult(0)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "txtResult(1)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "chkResult"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtResult(0)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "txtResult(2)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "txtResult(3)"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).ControlCount=   13
               TabCaption(1)   =   "Tabla"
               TabPicture(1)   =   "mb00111.frx":007C
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "grdssResult(0)"
               Tab(1).ControlCount=   1
               Begin VB.TextBox txtResult 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H0000FFFF&
                  DataField       =   "MB25_CODRESMICRO"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   3
                  Left            =   3780
                  Locked          =   -1  'True
                  MaxLength       =   4
                  TabIndex        =   6
                  Top             =   420
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtResult 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFF00&
                  DataField       =   "MB11_CODGEMICRO"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   2
                  Left            =   3780
                  Locked          =   -1  'True
                  MaxLength       =   2
                  TabIndex        =   5
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "MB25_RESULT"
                  Height          =   495
                  HelpContextID   =   40101
                  Index           =   0
                  Left            =   1200
                  Locked          =   -1  'True
                  MaxLength       =   1000
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   10
                  Tag             =   "Descripci�n|Descripci�n del resultado"
                  Top             =   1020
                  Width           =   7140
               End
               Begin VB.CheckBox chkResult 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Activo"
                  DataField       =   "MB25_INDACTIVA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Left            =   5100
                  TabIndex        =   12
                  Tag             =   "Activa|Activa"
                  Top             =   600
                  Width           =   945
               End
               Begin VB.TextBox txtResult 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "MB25_OBSERV"
                  Height          =   825
                  HelpContextID   =   40101
                  Index           =   1
                  Left            =   1560
                  Locked          =   -1  'True
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   11
                  Tag             =   "Observaciones|Observaciones"
                  Top             =   1620
                  Width           =   6780
               End
               Begin SSDataWidgets_B.SSDBGrid grdssResult 
                  Height          =   2310
                  Index           =   0
                  Left            =   -74850
                  TabIndex        =   30
                  TabStop         =   0   'False
                  Top             =   105
                  Width           =   8325
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  RecordSelectors =   0   'False
                  Col.Count       =   0
                  BevelColorFrame =   0
                  BevelColorHighlight=   16777215
                  AllowUpdate     =   0   'False
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   2
                  AllowGroupSwapping=   0   'False
                  AllowGroupShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  MaxSelectedRows =   0
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterVisible =   -1  'True
                  Columns(0).Width=   3200
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   4096
                  UseDefaults     =   0   'False
                  _ExtentX        =   14684
                  _ExtentY        =   4075
                  _StockProps     =   79
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "MB09_CODTEC"
                  Height          =   315
                  Index           =   0
                  Left            =   1200
                  TabIndex        =   7
                  Tag             =   "T�cnica|T�cnica"
                  Top             =   180
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorEven   =   16776960
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16776960
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "MB32_CODRESULT"
                  Height          =   315
                  Index           =   1
                  Left            =   5820
                  TabIndex        =   8
                  Tag             =   "Resultado|Resultado"
                  Top             =   180
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  AllowNull       =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorEven   =   16776960
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16776960
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin SSDataWidgets_B.SSDBCombo cbossResult 
                  DataField       =   "CUNIDAD"
                  Height          =   315
                  Index           =   2
                  Left            =   1200
                  TabIndex        =   9
                  Tag             =   "Unidad|Unidad"
                  Top             =   600
                  Width           =   2490
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4392
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Valor:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   180
                  TabIndex        =   35
                  Top             =   1020
                  Width           =   900
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "T�cnica:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   180
                  TabIndex        =   34
                  Top             =   180
                  Width           =   900
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Observaciones:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   60
                  TabIndex        =   33
                  Top             =   1620
                  Width           =   1425
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Unidad:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   180
                  TabIndex        =   32
                  Top             =   600
                  Width           =   900
               End
               Begin VB.Label lblLabel1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Resultado:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   4380
                  TabIndex        =   31
                  Top             =   240
                  Width           =   1305
               End
            End
         End
         Begin VB.Frame fraMicro 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2805
            Left            =   -74970
            TabIndex        =   25
            Top             =   315
            Width           =   9135
            Begin SSDataWidgets_B.SSDBGrid grdssMicro 
               Height          =   2610
               Index           =   0
               Left            =   120
               TabIndex        =   26
               Tag             =   "Microorganismos|Microorganismos"
               Top             =   120
               Width           =   8865
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   -1  'True
               _ExtentX        =   15637
               _ExtentY        =   4604
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraAnti 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2730
            Left            =   -74940
            TabIndex        =   24
            Top             =   360
            Width           =   9060
            Begin SSDataWidgets_B.SSDBGrid grdssAnti 
               Height          =   2610
               Index           =   0
               Left            =   60
               TabIndex        =   27
               Tag             =   "Antibioticos|Antibioticos"
               Top             =   60
               Width           =   8940
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15769
               _ExtentY        =   4604
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraGeMicro 
      Caption         =   "G�nero de Microorganismos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Left            =   120
      TabIndex        =   13
      Top             =   480
      Width           =   9465
      Begin TabDlg.SSTab tabGeMicro 
         Height          =   2130
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   360
         Width           =   9195
         _ExtentX        =   16219
         _ExtentY        =   3757
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00111.frx":0098
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(7)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cbossTipoMicro"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtFamiliaMicroorganismo(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkGeMicro(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtFamiliaMicroorganismo(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtFamiliaMicroorganismo(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtFamiliaMicroorganismo(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "fraLaboratorio"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).ControlCount=   11
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "mb00111.frx":00B4
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssGeMicro"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraLaboratorio 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   435
            Left            =   4080
            TabIndex        =   37
            Top             =   1260
            Width           =   2415
            Begin VB.CheckBox chkGeMicro 
               Alignment       =   1  'Right Justify
               Caption         =   "Laboratorio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   1
               Left            =   780
               TabIndex        =   38
               Tag             =   "Activa|Activa"
               Top             =   60
               Width           =   1470
            End
         End
         Begin VB.TextBox txtFamiliaMicroorganismo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "cResultado"
            Height          =   285
            Index           =   3
            Left            =   2460
            MaxLength       =   20
            TabIndex        =   36
            Tag             =   "CResultado|cResultado"
            Top             =   1680
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.TextBox txtFamiliaMicroorganismo 
            BackColor       =   &H0000FFFF&
            DataField       =   "MB11_CODGEMICRO"
            Height          =   285
            Index           =   0
            Left            =   1500
            MaxLength       =   3
            TabIndex        =   0
            Tag             =   "C�digo|C�digo de la familia de microorganismos"
            Top             =   150
            Width           =   495
         End
         Begin VB.TextBox txtFamiliaMicroorganismo 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB11_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1500
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n de la familia de microorganismos"
            Top             =   540
            Width           =   3375
         End
         Begin VB.CheckBox chkGeMicro 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "MB11_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   780
            TabIndex        =   4
            Tag             =   "Activa|Activa"
            Top             =   1680
            Width           =   930
         End
         Begin VB.TextBox txtFamiliaMicroorganismo 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB11_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1500
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n de la familia de microorganismos"
            Top             =   930
            Width           =   6555
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTipoMicro 
            DataField       =   "MB08_CODTIMIC"
            Height          =   315
            Left            =   1500
            TabIndex        =   3
            Tag             =   "Tipo Microorganismo|Tipo de microorganismo"
            Top             =   1290
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBGrid grdssGeMicro 
            Height          =   1830
            Left            =   -74880
            TabIndex        =   21
            Top             =   150
            Width           =   8640
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15240
            _ExtentY        =   3228
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   300
            TabIndex        =   20
            Top             =   180
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   19
            Top             =   600
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tipo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   300
            TabIndex        =   18
            Top             =   1320
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   300
            TabIndex        =   17
            Top             =   960
            Width           =   1095
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   6810
      Width           =   9780
      _ExtentX        =   17251
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_FamiliaMicroorganismo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objGeMicro As New clsCWForm
Dim objMicro As New clsCWForm
Dim objAnti As New clsCWForm
Dim objResult As New clsCWForm

Dim blnPostRead As Boolean  ' Indica que se est� en el evento PostRead del registro
Private Sub cbossResult_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossResult_CloseUp(Index As Integer)
    'Se llena el combo de los resultados seg�n la t�cnica seleccionada
    If Index = 0 Then
        If cbossResult(0).Columns(0).Text <> "" Then
            cbossResult(0).Text = cbossResult(0).Columns(1).Text
            Call pComboResultCargar(objWinInfo, cbossResult(1), cbossResult(0).Columns(0).Text)
            cbossResult(1).Text = cbossResult(1).Columns(1).Text
        End If
    End If
    
End Sub

Private Sub cbossResult_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossResult_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossResult_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossTipoMicro_Click()
    Call objWinInfo.CtrlDataChange
    Call cbossTipoMicro_LostFocus
End Sub

Private Sub cbossTipoMicro_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossTipoMicro_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossTipoMicro_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkResult_Click()
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkResult_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkResult_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkGeMicro_Click(intIndex As Integer)
      Call objWinInfo.CtrlDataChange
      Select Case intIndex
        Case 0
            'Call objWinInfo.CtrlDataChange
        Case 1  ' Laboratorio
            If blnPostRead = False Then
                If chkGeMicro(1).Value = 1 Then
                    txtFamiliaMicroorganismo(3).Text = fCodigoResultadoLaboratorio(departamento, False)
                Else
                    txtFamiliaMicroorganismo(3).Text = ""
                End If
            End If
    End Select
End Sub

Private Sub chkGeMicro_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkGeMicro_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub Form_Load()
  
  Dim strKey As String, SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objGeMicro
    .strName = "GeMicro"
    Set .objFormContainer = fraGeMicro
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabGeMicro
    Set .grdGrid = grdssGeMicro
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1100"
    .strWhere = "MB08_codTiMic IN (SELECT MB08_codTiMic FROM MB0800 WHERE cDptoSecc = " & departamento & ")"
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("MB11_CODGEMICRO", cwAscending)
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Familias de Microorganismos")
    Call .FormAddFilterWhere(strKey, "MB11_CODGEMICRO", "C�digo familia", cwNumeric, 2)
    Call .FormAddFilterWhere(strKey, "MB11_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB11_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB08_CODTIMIC", "Tipo microorgnismo", cwNumeric, 2)
    Call .FormAddFilterWhere(strKey, "MB11_INDACTIVA", "�Activa?", cwNumeric, 1)
    
    Call .FormAddFilterOrder(strKey, "MB11_CODGEMICRO", "C�digo familia")
    Call .FormAddFilterOrder(strKey, "MB11_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "MB11_DESCRIP", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "MB08_CODTIMIC", "Tipo microorgnismo")
  End With
  
  With objMicro
    .strName = "Micro"
    Set .objFormContainer = fraMicro
    Set .objFatherContainer = fraGeMicro
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssMicro(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1800"
    .strWhere = "MB18_indActiva = -1"
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("MB18_CODMICRO", cwAscending)
    Call .FormAddRelation("MB11_CODGEMICRO", txtFamiliaMicroorganismo(0))
  End With
  
  With objAnti
    .strName = "Anti"
    Set .objFormContainer = fraAnti
    Set .objFatherContainer = fraGeMicro
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssAnti(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB3900"
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("MB31_CODGRANTI", cwAscending)
    Call .FormAddRelation("MB11_CODGEMICRO", txtFamiliaMicroorganismo(0))
  End With
  
  With objResult
    .strName = "Result"
    Set .objFormContainer = fraResult
    Set .objFatherContainer = fraGeMicro
    Set .tabMainTab = tabResult
    Set .grdGrid = grdssResult(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2500"
    .blnAskPrimary = False
    Call .FormAddOrderField("MB09_CODTEC", cwAscending)
    Call .FormAddOrderField("MB32_CODRESULT", cwAscending)
    Call .FormAddRelation("MB11_CODGEMICRO", txtFamiliaMicroorganismo(0))
  End With
    
  With objWinInfo
    Call .FormAddInfo(objGeMicro, cwFormDetail)
    
    Call .FormAddInfo(objMicro, cwFormMultiLine)
    'Call .GridAddColumn(objMicro, "C�digo", "MB18_CODMICRO", cwNumeric, 3)
    Call .GridAddColumn(objMicro, "Designaci�n", "MB18_DESIG", cwString, 20)
    Call .GridAddColumn(objMicro, "Tiempo Crecimiento (h)", "MB18_TCRECI", cwNumeric, 3)
    'Call .GridAddColumn(objMicro, "Descripci�n", "MB18_DESCRIP", cwString, 50)
    'Call .GridAddColumn(objMicro, "C. Morfolog�a", "MB12_CODMORF", cwNumeric, 3)
    'Call .GridAddColumn(objMicro, "Morfolog�a", "", cwString, 20)
    'Call .GridAddColumn(objMicro, "Activa", "MB18_INDACTIVA", cwBoolean, 1)
    
    Call .FormAddInfo(objAnti, cwFormMultiLine)
    Call .GridAddColumn(objAnti, "Familia microorganismos", "MB11_CODGEMICRO", cwNumeric, 2)
    Call .GridAddColumn(objAnti, "Grupo antibi�ticos", "MB31_CODGRANTI", cwNumeric, 2)
    
    Call .FormAddInfo(objResult, cwFormDetail)
    
    Call .FormCreateInfo(objGeMicro)
    
    .CtrlGetInfo(chkGeMicro(0)).blnInFind = True
    '.CtrlGetInfo(chkGeMicro(1)).blnNegotiated = False
    .CtrlGetInfo(txtFamiliaMicroorganismo(3)).blnMandatory = False
    
    .CtrlGetInfo(txtFamiliaMicroorganismo(0)).blnValidate = False
    .CtrlGetInfo(txtFamiliaMicroorganismo(0)).blnInFind = True
    .CtrlGetInfo(txtFamiliaMicroorganismo(1)).blnInFind = True
    .CtrlGetInfo(txtFamiliaMicroorganismo(2)).blnInFind = True
    .CtrlGetInfo(cbossTipoMicro).blnInFind = True
    .CtrlGetInfo(txtFamiliaMicroorganismo(3)).blnInGrid = False

  
    SQL = "SELECT MB08_CodTiMic, MB08_Desig FROM MB0800"
    SQL = SQL & " WHERE MB08_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB08_Desig"
    .CtrlGetInfo(cbossTipoMicro).strSQL = SQL
    .CtrlGetInfo(cbossTipoMicro).blnForeign = True
    
    Call .FormChangeColor(objMicro)
    .CtrlGetInfo(grdssMicro(0).Columns(3)).blnReadOnly = True
    .CtrlGetInfo(grdssMicro(0).Columns(4)).blnReadOnly = True
    '.CtrlGetInfo(grdssMicro(0).Columns(5)).blnReadOnly = True
       
    Call .FormChangeColor(objAnti)
    Call .FormChangeColor(objResult)
    .CtrlGetInfo(txtResult(2)).blnInGrid = False
    .CtrlGetInfo(txtResult(3)).blnValidate = False
    .CtrlGetInfo(txtResult(3)).blnInGrid = False
    
    ' se a�aden los links
'    SQL = "SELECT MB12_desig FROM MB1200 WHERE MB12_codMorf = ?"
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdssMicro(0).Columns(6)), "MB12_CODMORF", SQL)
'    Call .CtrlAddLinked(.CtrlGetInfo(grdssMicro(0).Columns(6)), grdssMicro(0).Columns(7), "MB12_DESIG")
'    grdssMicro(0).Columns(6).Visible = False
    
    ' se llenan los DropDown
    grdssAnti(0).Columns(3).Visible = False
    SQL = "SELECT MB31_CodGrAnti, MB31_Desig FROM MB3100"
    SQL = SQL & " WHERE MB31_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB31_Desig"
    .CtrlGetInfo(grdssAnti(0).Columns(4)).strSQL = SQL
    .CtrlGetInfo(grdssAnti(0).Columns(4)).blnForeign = True
    
    SQL = "SELECT MB09_codTec,MB09_desig FROM MB0900"
    SQL = SQL & " WHERE cDptoSecc=" & departamento
    SQL = SQL & " AND MB09_indActiva =-1"
    SQL = SQL & " AND (MB04_codTiTec <> " & constCULTIVO & " AND MB04_codTiTec <> " & constANTIBIOGRAMA & ")"
    SQL = SQL & " AND MB09_codTec  IN"
    SQL = SQL & "       (SELECT DISTINCT MB09_codTec FROM MB3200"
    SQL = SQL & "        WHERE MB32_indActiva = -1)"
    SQL = SQL & " ORDER BY MB09_desig"
    .CtrlGetInfo(cbossResult(0)).strSQL = SQL
    .CtrlGetInfo(cbossResult(0)).blnForeign = True
    
    SQL = "SELECT MB32_codResult,MB32_desig FROM MB3200"
    SQL = SQL & " WHERE MB09_CODTEC = 0" 'No traer ning�n resultado hasta que no se seleccione la t�cnica
    SQL = SQL & " ORDER BY MB32_desig"
    .CtrlGetInfo(cbossResult(1)).strSQL = SQL
'    .CtrlGetInfo(cbossResult(1)).blnForeign = True
    
    SQL = "SELECT cUnidad, designacion FROM unidades"
    SQL = SQL & " ORDER BY designacion"
    .CtrlGetInfo(cbossResult(2)).strSQL = SQL
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraAnti_Click()
  Call objWinInfo.FormChangeActive(fraAnti, False, True)
End Sub

Private Sub fraDatos_Click()
    Select Case tabDatos.Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraMicro, True, True)
    Case 1
      Call objWinInfo.FormChangeActive(fraAnti, True, True)
    Case 2
      Call objWinInfo.FormChangeActive(fraResult, True, True)
    End Select
End Sub

Private Sub fraGeMicro_Click()
  Call objWinInfo.FormChangeActive(fraGeMicro, False, True)
End Sub

Private Sub fraMicro_Click()
  Call objWinInfo.FormChangeActive(fraMicro, False, True)
End Sub

Private Sub fraResult_Click()
  Call objWinInfo.FormChangeActive(fraResult, False, True)
End Sub


Private Sub grdssAnti_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssAnti_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssAnti_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssAnti_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssMicro_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssMicro_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssMicro_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssMicro_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssResult_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssResult_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssResult_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    
    On Error GoTo label
    
    Select Case strFormName
    Case objGeMicro.strName
        Select Case strCtrl
        Case objWinInfo.CtrlGetInfo(cbossTipoMicro).strName
            frmA_TipoMicroorganismo.Show vbModal
            Set frmA_TipoMicroorganismo = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossTipoMicro))
        End Select
    Case objAnti.strName
        Select Case strCtrl
        Case objWinInfo.CtrlGetInfo(grdssAnti(0).Columns(4)).strName
            frmA_GrAntibiotico.Show vbModal
            Set frmA_GrAntibiotico = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssAnti(0).Columns(4)))
        End Select
    Case objResult.strName
        Select Case strCtrl
        Case objWinInfo.CtrlGetInfo(cbossResult(0)).strName
            frmA_Tecnica.Show vbModal
            Set frmA_Tecnica = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossResult(0)))
        End Select
    End Select
    
label:
    If Err = 400 Then
        MsgBox "La Ventana a la que se quiere acceder ya est� activa.", vbInformation, "Acceso a Ventana"
    ElseIf Err <> 0 Then
        MsgBox Error
    End If
        
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: g�neros
    
'Si se realiza alguna operaci�n en el formulario de g�neros(objGeMicro) habr� que anotar _
el mantenimiento para los g�neros
    
    If strFormName = objGeMicro.strName Then
        Call pAnotarMantenimiento(constMANTGENEROS)
    End If
    
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    'blnPostRead = True
    If strFormName = objResult.strName Then
        If grdssResult(0).Columns(1).Text <> "" Then
            Call pComboResultCargar(objWinInfo, cbossResult(1), objResult.rdoCursor("MB09_codTec"))
            If grdssResult(0).Columns(2).Text <> "" Then
                Call pComboResultTexto(cbossResult(1), objResult.rdoCursor("MB09_codTec"), objResult.rdoCursor("MB32_codResult"))
            End If
        End If
    ElseIf strFormName = objGeMicro.strName Then
        If Not IsNull(objGeMicro.rdoCursor!cResultado) Then
            chkGeMicro(1).Enabled = False
            fraLaboratorio.Enabled = False
            chkGeMicro(1).Value = 1
        Else
            chkGeMicro(1).Enabled = True
            fraLaboratorio.Enabled = True
            chkGeMicro(1).Value = 0
        End If
    End If
    blnPostRead = False
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: g�neros
    
'Si se realiza alguna operaci�n en el formulario de g�neros(objGeMicro) habr� que anotar _
el mantenimiento para los g�neros
    
    If strFormName = objGeMicro.strName Then
        Call pAnotarMantenimiento(constMANTGENEROS)
        ' Si se ha introducido la relaci�n con laboratorio -> se genera el resultado en laboratorio
        If fraLaboratorio.Enabled = True And chkGeMicro(1).Value = 1 Then
            blnError = fInsertarResultadoLaboratorio(txtFamiliaMicroorganismo(3).Text, txtFamiliaMicroorganismo(2).Text, departamento)
        End If
    End If
    
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
    blnPostRead = True
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
    Select Case strFormName
    Case objGeMicro.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtFamiliaMicroorganismo(0).Text = fNextClave("MB11_codGeMicro", "MB1100")
            objGeMicro.rdoCursor("MB11_codGeMicro") = txtFamiliaMicroorganismo(0).Text
        End If
    Case objResult.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtResult(3).Text = fNextClave("MB25_codResMicro", "MB2500")
            objResult.rdoCursor("MB25_codResMicro") = txtResult(3).Text
        End If
    End Select
    
End Sub


Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
    Select Case tabDatos.Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraMicro, True, True)
    Case 1
      Call objWinInfo.FormChangeActive(fraAnti, True, True)
    Case 2
      Call objWinInfo.FormChangeActive(fraResult, True, True)
    End Select
End Sub

Private Sub tabGeMicro_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabGeMicro, False, True)
End Sub

Private Sub tabResult_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabResult, False, True)
End Sub

Private Sub Timer1_Timer()
    Select Case tabDatos.Tab
    Case 0
        fraDatos.ForeColor = fraMicro.ForeColor
    Case 1
        fraDatos.ForeColor = fraAnti.ForeColor
    Case 2
        fraDatos.ForeColor = fraResult.ForeColor
    End Select
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'
'  Dim rdo As rdoResultset, SQL As String
'  Select Case btnButton.Index
'    Case 2
'        Select Case objWinInfo.objWinActiveForm.strName
'        Case objMicro.strName
'            grdssMicro(0).Columns(3).Text = txtFamiliaMicroorganismo(0).Text
'        Case objAnti.strName
'            grdssAnti(0).Columns(3).Text = txtFamiliaMicroorganismo(0).Text
'        Case objResult.strName
'            SQL = "SELECT MB25_CODRESMICRO_SEQUENCE.NEXTVAL FROM DUAL"
'            Set rdo = objApp.rdoConnect.OpenResultset(SQL)
'            txtResult(2).Text = txtFamiliaMicroorganismo(0).Text
'            txtResult(3).Text = rdo(0)
'            rdo.Close
'        End Select
'  End Select

End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssGeMicro_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssGeMicro_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssGeMicro_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssGeMicro_Change()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtFamiliaMicroorganismo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtFamiliaMicroorganismo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtFamiliaMicroorganismo_Change(intIndex As Integer)
  Select Case intIndex
    Case 3
        If txtFamiliaMicroorganismo(3).Text = "" Then
            chkGeMicro(1).Enabled = True
            fraLaboratorio.Enabled = True
            chkGeMicro(1).Value = 0
        End If
  End Select
  Call objWinInfo.CtrlDataChange

End Sub

Private Sub txtResult_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtResult_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
pAutotexto KeyCode
End Sub

Private Sub txtResult_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtResult_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

