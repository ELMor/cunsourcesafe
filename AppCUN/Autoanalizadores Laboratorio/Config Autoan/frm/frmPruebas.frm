VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmPruebas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGURIDAD. Mantenimiento de Procesos"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   31
   Icon            =   "frmPruebas.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Procesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3480
      Index           =   0
      Left            =   90
      TabIndex        =   1
      Top             =   495
      Width           =   9510
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2985
         Index           =   0
         Left            =   135
         TabIndex        =   2
         Top             =   360
         Width           =   8925
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   15743
         _ExtentY        =   5265
         _StockProps     =   79
      End
      Begin VB.Image imgIcon 
         Height          =   240
         Left            =   9135
         Stretch         =   -1  'True
         Top             =   360
         Width           =   240
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPruebas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWProcess
' Coded by SYSECA Bilbao
' **********************************************************************************


Const cwProcessMsgDateError As String = "La fecha de Activaci�n no puede ser superior a la fecha de Desactivaci�n"


Dim WithEvents objWin As clsCWWin
Attribute objWin.VB_VarHelpID = -1


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objProcess As New clsCWForm
Dim strKey     As String
On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objWin = New clsCWWin
  
  Call objWin.WinCreateInfo(cwModeMultiLineEdit, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  
  With objWin.objDoc
    .cwPRJ = "CWSpy"
    .cwMOD = "M�dulo Process"
    .cwAUT = "SYSECA Bilbao"
    .cwDAT = "08-09-97"
    .cwDES = "Este formulario mantiene los procesos"
    .cwUPD = "08-09-97 - SYSECA Bilbao - Creaci�n del m�dulo"
    .cwEVT = ""
  End With

  With objProcess
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
    .strDataBase = objEnv.GetValue("DataBase")
    .strTable = "PruebasAutoanalizador"

    Call .FormAddOrderField("SG00ID", cwAscending)
    Call .FormAddOrderField("SG04COD", cwAscending)
    Call .FormAddOrderField("SG17COD", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Procesos")
    Call .FormAddFilterWhere(strKey, "SG04COD", "C�digo de Proceso", cwString, objGen.ReplaceStr("SELECT SG04COD #C�digo de Proceso#, SG04DES #Descripci�n del Proceso# FROM SG0400 ORDER BY SG04COD", "#", Chr(34), 0))
    Call .FormAddFilterWhere(strKey, "SG00ID", "C�digo de Aplicaci�n", cwString, objGen.ReplaceStr("SELECT SG00ID #C�digo de Aplicaci�n#, SG00DES #Descripci�n de la Aplicaci�n# FROM SG0000 ORDER BY SG00ID", "#", Chr(34), 0))
    Call .FormAddFilterWhere(strKey, "SG04DES", "Descripci�n del Proceso", cwString)
    Call .FormAddFilterWhere(strKey, "SG17COD", "Tipo de Proceso", cwNumeric, objGen.ReplaceStr("SELECT SG17COD #C�digo de Tipo#, SG17DES #Descripci�n del Tipo# FROM SG1700 ORDER BY SG17COD", "#", Chr(34), 0))
     
    Call .FormAddFilterOrder(strKey, "SG04COD", "C�digo de Proceso")
    Call .FormAddFilterOrder(strKey, "SG00ID", "C�digo de Aplicaci�n")
    Call .FormAddFilterOrder(strKey, "SG04DES", "Descripci�n del Proceso")
    Call .FormAddFilterOrder(strKey, "SG04NOM", "Tipo de Proceso")
    
    Call .objPrinter.Add("sg0009", "Relaci�n de Procesos por Usuario")
    Call .objPrinter.Add("sg0010", "Relaci�n de Procesos por Aplicaci�n")
    
    .intCursorSize = -1
    .strName = "Process"
  End With
  
  With objWin
    Call .FormAddInfo(objProcess, cwFormMultiLine)
    Call .GridAddColumn(objProcess, "C�digo", "SG04COD")
    Call .GridAddColumn(objProcess, "Aplicaci�n", "SG00ID")
    Call .GridAddColumn(objProcess, "Componente", "SG21CODCOMPON")
    Call .GridAddColumn(objProcess, "Descripci�n", "SG04DES")
    Call .GridAddColumn(objProcess, "Tipo", "SG17COD")
    Call .GridAddColumn(objProcess, "Ruta", "SG04PATH")
    Call .GridAddColumn(objProcess, "Ruta del Icono", "SG04PATHICO")
    Call .GridAddColumn(objProcess, "Fecha de Activaci�n", "SG04FECACT")
    Call .GridAddColumn(objProcess, "Fecha de Desactivaci�n", "SG04FECDES")
    Call .GridAddColumn(objProcess, "Indicador de Men�", "SG04INDMENU", cwBoolean)

    Call .FormCreateInfo(objProcess)
  
    With .CtrlGetInfo(grdDBGrid1(0).Columns(3))
      .intMask = cwMaskUpper
      .blnInFind = True
    End With
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
  
    With .CtrlGetInfo(grdDBGrid1(0).Columns(8))
      .strName = "Path"
      .blnForeign = True
      .blnInFind = True
    End With
    
    With .CtrlGetInfo(grdDBGrid1(0).Columns(9))
      .blnForeign = True
      .strName = "PathIco"
      .blnInFind = True
    End With
    
    With .CtrlGetInfo(grdDBGrid1(0).Columns(10))
      .blnForeign = True
      .strName = "FecAct"
    End With
    
    With .CtrlGetInfo(grdDBGrid1(0).Columns(11))
      .blnForeign = True
      .strName = "FecDes"
    End With
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).strSQL = "SELECT SG00ID, SG00DES FROM " & objEnv.GetValue("DataBase") & "SG0000 ORDER BY SG00DES"
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).strSQL = "SELECT SG21CODCOMPON, SG21DESCOMPON FROM " & objEnv.GetValue("DataBase") & "SG2100 ORDER BY SG21DESCOMPON"
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).strSQL = "SELECT SG17COD, SG17DES FROM " & objEnv.GetValue("DataBase") & "SG1700 ORDER BY SG17DES"
  
    Call .WinRegister
    Call .WinStabilize
  End With
    
cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWin.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWin.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWin.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWin.WinDeRegister
  Call objWin.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWin_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objFile    As New clsCWFileOpen
  Dim objGetDate As New clsCWGetDate
  Dim objControl As Object
  Dim intType    As Integer
  
  If strFormName = "Process" Then
    If strCtrl = "Path" Then
      intType = objWin.CtrlGet(grdDBGrid1(0).Columns(7))
      Set objControl = grdDBGrid1(0).Columns(8)
      If Not objWin.CtrlIsReadOnly(objControl) Then
        Select Case intType
          Case cwTypeApplication
            Call objFile.AddFilter("Aplicaciones (*.exe)", "*.exe")
            Call objFile.AddFilter("Todos", "*.*")
          Case cwTypeReport, cwTypeExternalReport
            Call objFile.AddFilter("Listados (*.rpt)", "*.rpt")
            Call objFile.AddFilter("Todos", "*.*")
          Case Else
            Call objFile.AddFilter("Todos", "*.*")
        End Select
        If objFile.GetFile(objWin.CtrlGet(objControl), "Localizar Aplicaci�n") Then
          Call objWin.CtrlSet(objControl, objFile.strFile)
        End If
      End If
      Set objControl = Nothing
    ElseIf strCtrl = "PathIco" Then
      Set objControl = grdDBGrid1(0).Columns(9)
      If Not objWin.CtrlIsReadOnly(objControl) Then
        Call objFile.AddFilter("Iconos (*.ico)", "*.ico")
        Call objFile.AddFilter("Todos", "*.*")
        If objFile.GetFile(objWin.CtrlGet(objControl), "Localizar Icono") Then
          Call objWin.CtrlSet(objControl, objFile.strFile)
        End If
      End If
      Set objControl = Nothing
    ElseIf strCtrl = "FecAct" Then
      Set objControl = grdDBGrid1(0).Columns(10)
      If Not objWin.CtrlIsReadOnly(objControl) Then
        If objGetDate.GetDate(objWin.CtrlGet(objControl)) Then
          Call objWin.CtrlSet(objControl, objGetDate.strDate)
        End If
      End If
      Set objControl = Nothing
    ElseIf strCtrl = "FecDes" Then
      Set objControl = grdDBGrid1(0).Columns(11)
      If Not objWin.CtrlIsReadOnly(objControl) Then
        If objGetDate.GetDate(objWin.CtrlGet(objControl)) Then
          Call objWin.CtrlSet(objControl, objGetDate.strDate)
        End If
      End If
      Set objControl = Nothing
    End If
  End If
End Sub


Private Sub objWin_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim vntDate1 As Variant
  Dim vntDate2 As Variant
  
  If strFormName = "Process" Then
    vntDate1 = objWin.CtrlGet(grdDBGrid1(0).Columns(10))
    vntDate2 = objWin.CtrlGet(grdDBGrid1(0).Columns(11))
        
    If IsDate(vntDate1) And IsDate(vntDate2) Then
      If CDate(vntDate1) > CDate(vntDate2) Then
        Call objError.SetError(cwCodeMsg, cwProcessMsgDateError)
        Call objError.Raise
        blnCancel = True
      End If
    End If
  End If
End Sub

Private Sub objWin_cwPrint(ByVal strFormName As String)
  Dim strWhere  As String
  Dim strOrder  As String
  
  If strFormName = "Process" Then
    With objWin.FormPrinterDialog(True, "")
      If .Selected > 0 Then
        strWhere = objWin.DataGetWhere(False)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
          strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), "WHERE ", " AND ")
          strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = "ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWin.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWin.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWin.CtrlGotFocus
  Call EnableTypeProcess
  Call IconPreview
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWin.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWin.GridChangeRowCol(vntLastRow, intLastCol)
  Call EnableTypeProcess
  Call IconPreview
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWin.CtrlDataChange
  Call EnableTypeProcess
  Call IconPreview
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWin.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' procedimientos privados
' -----------------------------------------------
Private Sub IconPreview()
  On Error Resume Next
  Set imgIcon.Picture = LoadPicture
  Set imgIcon.Picture = LoadPicture(grdDBGrid1(0).Columns(8).Text)
End Sub

Private Sub EnableTypeProcess()
  Dim intRegType As cwMultilineStatus
  
  On Error Resume Next
  intRegType = grdDBGrid1(0).Columns(1).Value
  If objGen.IsAnd(intRegType, cwMultiLineLoaded) Then
    grdDBGrid1(0).Columns(7).Locked = True
    grdDBGrid1(0).Columns(7).Style = ssStyleEdit
  Else
    grdDBGrid1(0).Columns(7).Locked = False
    grdDBGrid1(0).Columns(7).Style = ssStyleComboBox
  End If
End Sub
