VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_Protocolo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Protocolo"
   ClientHeight    =   7020
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9810
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7020
   ScaleWidth      =   9810
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9810
      _ExtentX        =   17304
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   420
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3345
      Left            =   120
      TabIndex        =   13
      Top             =   3360
      Width           =   9525
      Begin TabDlg.SSTab tabDatos 
         Height          =   2835
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   360
         Width           =   9330
         _ExtentX        =   16457
         _ExtentY        =   5001
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         TabCaption(0)   =   "T�cnicas"
         TabPicture(0)   =   "MB00105.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraTec"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Pruebas"
         TabPicture(1)   =   "MB00105.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraPruebas"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraPruebas 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2250
            Left            =   -74940
            TabIndex        =   17
            Top             =   360
            Width           =   9120
            Begin SSDataWidgets_B.SSDBGrid grdssPruebas 
               Height          =   2130
               Index           =   0
               Left            =   120
               TabIndex        =   18
               Top             =   120
               Width           =   8940
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   -1  'True
               _ExtentX        =   15769
               _ExtentY        =   3757
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraTec 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2265
            Left            =   60
            TabIndex        =   15
            Top             =   420
            Width           =   9135
            Begin SSDataWidgets_B.SSDBGrid grdssTec 
               Height          =   2190
               Index           =   0
               Left            =   120
               TabIndex        =   16
               Top             =   60
               Width           =   8940
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               AllowColumnMoving=   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15769
               _ExtentY        =   3863
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraProt 
      Caption         =   "Protocolos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2745
      Left            =   90
      TabIndex        =   7
      Top             =   450
      Width           =   9525
      Begin TabDlg.SSTab tabProt 
         Height          =   2265
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   9240
         _ExtentX        =   16298
         _ExtentY        =   3995
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00105.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cbossTipoProt"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkProt"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtProt(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtProt(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtProt(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtProt(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssProt"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtProt 
            BackColor       =   &H00FFFF00&
            DataField       =   "CDPTOSECC"
            Height          =   285
            Index           =   3
            Left            =   3960
            MaxLength       =   50
            TabIndex        =   19
            Tag             =   "C. Secci�n|C�digo de la Secci�n"
            Top             =   120
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.TextBox txtProt 
            BackColor       =   &H0000FFFF&
            DataField       =   "MB05_CODPROT"
            Height          =   285
            Index           =   0
            Left            =   1860
            MaxLength       =   2
            TabIndex        =   0
            Tag             =   "C�digo|C�digo del Protocolo"
            Top             =   120
            Width           =   495
         End
         Begin VB.TextBox txtProt 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB05_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1860
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Nombre abreviado del Protocolo"
            Top             =   600
            Width           =   4275
         End
         Begin VB.TextBox txtProt 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB05_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1860
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del Protocolo"
            Top             =   1080
            Width           =   6915
         End
         Begin VB.CheckBox chkProt 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "MB05_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1140
            TabIndex        =   4
            Tag             =   "Activa|Activa"
            Top             =   1920
            Width           =   945
         End
         Begin SSDataWidgets_B.SSDBGrid grdssProt 
            Height          =   2070
            Left            =   -74910
            TabIndex        =   6
            Top             =   90
            Width           =   8700
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   15346
            _ExtentY        =   3651
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTipoProt 
            DataField       =   "MB42_CODTIPROT"
            Height          =   315
            Left            =   1860
            TabIndex        =   3
            Tag             =   "Tipo Protocolo|Tipo de protocolos"
            Top             =   1500
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tipo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   660
            TabIndex        =   20
            Top             =   1500
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   500
            TabIndex        =   12
            Top             =   120
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   495
            TabIndex        =   11
            Top             =   600
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   495
            TabIndex        =   10
            Top             =   1080
            Width           =   1200
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   6735
      Width           =   9810
      _ExtentX        =   17304
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Protocolo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objProtocolos As New clsCWForm
Dim objTecnicas As New clsCWForm
Dim objPruebas As New clsCWForm
  
Dim desigPrueba As String
Dim desigTiTec As String

Private Sub cbossTipoProt_Click()
    Call objWinInfo.CtrlDataChange
    Call cbossTipoProt_LostFocus
End Sub

Private Sub cbossTipoProt_Change()
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub cbossTipoProt_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossTipoProt_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub Form_Load()
  Dim strKey As String, SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objProtocolos
    .strName = "Protocolos"
    Set .objFormContainer = fraProt
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabProt
    Set .grdGrid = grdssProt
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB0500"
    .strWhere = "MB0500.cDptoSecc = " & departamento 'nombre tabla para report
    Call .FormAddOrderField("MB0500.MB05_CODPROT", cwAscending) 'nombre tabla para report
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Protocolo")
    Call .FormAddFilterWhere(strKey, "MB0500.MB05_CODPROT", "C�digo", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB05_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB05_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB0500.MB42_CODTIPROT", "Tipo de protocolo", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB05_INDACTIVA", "�Activa?", cwBoolean)

    Call .FormAddFilterOrder(strKey, "MB0500.MB05_CODPROT", "C�digo") 'nombre tabla para report
    Call .FormAddFilterOrder(strKey, "MB05_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "MB05_DESCRIP", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "MB0500.MB42_CODTIPROT", "Tipo de protocolo") 'nombre tabla para report
    
    Call .objPrinter.Add("MB00134", "Protocolos ordenados alfab�ticamente")
    Call .objPrinter.Add("MB00129", "Tipos de Protocolos")
    Call .objPrinter.Add("MB00147", "Aplicaci�n de Protocolos")

  End With
  
  With objTecnicas
    .strName = "Tecnicas"
    Set .objFormContainer = fraTec
    Set .objFatherContainer = fraProt
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssTec(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1400"
    Call .FormAddOrderField("MB1400.MB09_CODTEC", cwAscending) 'nombre tabla para report
    Call .FormAddRelation(".MB05_CODPROT", txtProt(0))
  
    'Call .objPrinter.Add("MB00134", "Protocolos") 'columna ambigua
    
  End With

  With objPruebas
    .strName = "Pruebas"
    Set .objFormContainer = fraPruebas
    Set .objFatherContainer = fraProt
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssPruebas(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys

    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1000"

    Call .FormAddOrderField("MB1000.CPRUEBA", cwAscending) 'nombre tabla para report
    Call .FormAddOrderField("MB1000.CTIPOMUESTRA", cwAscending) 'nombre tabla para report
    Call .FormAddOrderField("MB1000.CDPT", cwAscending) 'nombre tabla para report
    Call .FormAddRelation("MB05_CODPROT", txtProt(0))

    'Call .objPrinter.Add("MB00147", "Aplicaci�n de Protocolos") 'columna ambigua

  End With
  
  With objWinInfo
    Call .FormAddInfo(objProtocolos, cwFormDetail)
    
    Call .FormAddInfo(objTecnicas, cwFormMultiLine)
    Call .GridAddColumn(objTecnicas, "Protocolo ", "MB05_CODPROT", cwNumeric, 2)
    Call .GridAddColumn(objTecnicas, "Tipo T�cnica", "", cwNumeric, 3)
    Call .GridAddColumn(objTecnicas, "T�cnica", "MB09_CODTEC", cwNumeric, 2)
    Call .GridAddColumn(objTecnicas, "Cond. Ambientales", "MB02_CODCOND", cwNumeric, 2)
    Call .GridAddColumn(objTecnicas, "Num. Veces", "MB14_NUMVECES", cwNumeric, 1)
    Call .GridAddColumn(objTecnicas, "Activa", "MB14_INDACTIVA", cwBoolean, 1)
    
    Call .FormAddInfo(objPruebas, cwFormMultiLine)
    Call .GridAddColumn(objPruebas, "Protocolo", "MB05_CODPROT", cwNumeric, 3)
    Call .GridAddColumn(objPruebas, "Prueba           ", "CPRUEBA", cwNumeric, 4)
    Call .GridAddColumn(objPruebas, "Tipo Muestra     ", "CTIPOMUESTRA", cwNumeric, 3)
    Call .GridAddColumn(objPruebas, "Dpto.            ", "CDPT", cwNumeric, 3)
    Call .GridAddColumn(objPruebas, "contador protocolo", "MB10_CODPROTPR", cwNumeric, 3)
     
    Call .FormCreateInfo(objProtocolos)
       
    .CtrlGetInfo(chkProt).blnInFind = True
    .CtrlGetInfo(txtProt(0)).blnValidate = False
    .CtrlGetInfo(txtProt(0)).blnInFind = True
    .CtrlGetInfo(txtProt(1)).blnInFind = True
    .CtrlGetInfo(txtProt(2)).blnInFind = True
    .CtrlGetInfo(txtProt(3)).blnValidate = False
    .CtrlGetInfo(txtProt(3)).blnInGrid = False
    .CtrlGetInfo(cbossTipoProt).blnInFind = True
        
    'se llena el combo de tipos de protocolos
    SQL = "SELECT MB42_codTiProt, MB42_Desig FROM MB4200"
    SQL = SQL & " WHERE MB42_indActiva = -1"
    SQL = SQL & " ORDER BY MB42_Desig"
    .CtrlGetInfo(cbossTipoProt).strSQL = SQL
    .CtrlGetInfo(cbossTipoProt).blnForeign = True
    
    Call .FormChangeColor(objTecnicas)
    grdssTec(0).Columns(3).Visible = False
    Call .FormChangeColor(objPruebas)
    grdssPruebas(0).Columns(3).Visible = False
    grdssPruebas(0).Columns(7).Visible = False
    .CtrlGetInfo(grdssPruebas(0).Columns(7)).blnValidate = False
  
    ' se llenan los DropDown
    SQL = "SELECT MB04_codTiTec, MB04_desig FROM MB0400"
    SQL = SQL & " WHERE MB04_indActiva = -1"
    SQL = SQL & " ORDER BY MB04_desig"
    .CtrlGetInfo(grdssTec(0).Columns(4)).strSQL = SQL
    
    SQL = "SELECT MB09_codTec,MB09_desig FROM MB0900"
    SQL = SQL & " WHERE cDptoSecc = " & departamento
    SQL = SQL & " AND MB09_indActiva =-1"
    SQL = SQL & " ORDER BY MB09_desig"
    .CtrlGetInfo(grdssTec(0).Columns(5)).strSQL = SQL
    .CtrlGetInfo(grdssTec(0).Columns(5)).blnForeign = True
    
    SQL = "SELECT MB02_codCond, MB02_desig FROM MB0200 "
    SQL = SQL & " WHERE MB02_indActiva = -1"
    SQL = SQL & " ORDER BY MB02_desig"
    .CtrlGetInfo(grdssTec(0).Columns(6)).strSQL = SQL
    .CtrlGetInfo(grdssTec(0).Columns(6)).blnForeign = True

    SQL = "SELECT cPrueba,Designacion FROM Pruebas"
    SQL = SQL & " WHERE cPrueba IN "
    SQL = SQL & "      (SELECT pE.cPrueba FROM PruebasExtracciones pE, pruebasCarpetas pC"
    SQL = SQL & "        WHERE pE.cPrueba = pC.cPrueba"
    SQL = SQL & "        AND pC.cCarpeta IN"
    SQL = SQL & "           (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    SQL = SQL & "        )"
    SQL = SQL & "    ORDER BY designacion"
    .CtrlGetInfo(grdssPruebas(0).Columns(4)).strSQL = SQL
    grdssPruebas(0).Columns(4).Width = 2000

    SQL = "SELECT cTipoMuestra,designacion FROM tiposMuestras"
    SQL = SQL & " WHERE cTipoMuestra IN "
    SQL = SQL & "       (SELECT cTipoMuestra FROM pruebasExtracciones pE, pruebasCarpetas pC"
    SQL = SQL & "        WHERE pE.cPrueba = pC.cPrueba"
    SQL = SQL & "        AND pC.cCarpeta IN"
    SQL = SQL & "           (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    SQL = SQL & "        )"
    SQL = SQL & " ORDER BY designacion"
    .CtrlGetInfo(grdssPruebas(0).Columns(5)).strSQL = SQL
    grdssPruebas(0).Columns(5).Width = 2000

    SQL = "SELECT cDpt,Desig FROM DPT"
    SQL = SQL & " WHERE cTiDpt = 1"
    SQL = SQL & " ORDER BY Desig"
    .CtrlGetInfo(grdssPruebas(0).Columns(6)).strSQL = SQL
    grdssPruebas(0).Columns(6).Width = 1800

    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraDatos_Click()
    Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraTec, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraPruebas, True, True)
    End Select
End Sub

Private Sub fraPruebas_Click()
    Call objWinInfo.FormChangeActive(fraPruebas, False, True)
End Sub



Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    
    On Error GoTo label
    
    Select Case strFormName
    Case objTecnicas.strName
        Select Case strCtrl
        Case objWinInfo.CtrlGetInfo(grdssTec(0).Columns(6)).strName
            frmA_Condicion.Show vbModal
            Set frmA_Condicion = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssTec(0).Columns(6)))
            'objWinInfo.WinPrepareScr
        Case objWinInfo.CtrlGetInfo(grdssTec(0).Columns(5)).strName
            frmA_Tecnica.Show vbModal
            Set frmA_Tecnica = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssTec(0).Columns(5)))
            'objWinInfo.WinPrepareScr
        End Select
    Case objProtocolos.strName
        Select Case strCtrl
        Case objWinInfo.CtrlGetInfo(cbossTipoProt).strName
            frmA_TipoProtocolo.Show vbModal
            Set frmA_TipoProtocolo = Nothing
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossTipoProt))
        End Select
    End Select
    
label:
    If Err = 400 Then
        MsgBox "La Ventana a la que se quiere acceder ya est� activa.", vbInformation, "Acceso a Ventana"
    ElseIf Err <> 0 Then
        MsgBox Error
    End If
        
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: protocolos
    
'Si se realiza alguna operaci�n en el formulario de protocolos(objProtocolos) o de t�cnicas _
(objTecnias)habr� que anotar el mantenimiento para protocolos
    
    If strFormName = objProtocolos.strName Or strFormName = objTecnicas.strName Then
        Call pAnotarMantenimiento(constMANTPROTOCOLOS)
    End If
    
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: protocolos
    
'Si se realiza alguna operaci�n en el formulario de protocolos(objProtocolos) o de t�cnicas _
(objTecnias)habr� que anotar el mantenimiento para protocolos
    
    If strFormName = objProtocolos.strName Or strFormName = objTecnicas.strName Then
        Call pAnotarMantenimiento(constMANTPROTOCOLOS)
    End If
    
End Sub


Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
    
    Select Case strFormName
    Case objTecnicas.strName
        If desigTiTec <> "" Then
            desigTiTec = ""
            Call pCargarDropDownTecnicas
        End If

    Case objPruebas.strName
        If desigPrueba <> "" Then
            desigPrueba = ""
            Call pCargarDropDownTiposMuestras
        End If
    End Select
    
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Dim rsprotoc As rdoResultset, qryProtoc As rdoQuery
    Dim SQL As String
    
    Select Case strFormName
    Case objProtocolos.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtProt(0).Text = fNextClave("MB05_codProt", "MB0500")
            txtProt(3).Text = departamento
            objProtocolos.rdoCursor("MB05_codProt") = txtProt(0).Text
            objProtocolos.rdoCursor("cDptoSecc") = txtProt(3).Text
        End If
        
    Case objPruebas.strName
        If grdssPruebas(0).Columns(7).Text = "" Then
            SQL = "SELECT MAX(MB10_CODPROTPR)"
            SQL = SQL & " FROM MB1000"
            SQL = SQL & " WHERE MB05_CODPROT = ?"
            Set qryProtoc = objApp.rdoConnect.CreateQuery("Protoc", SQL)
            qryProtoc(0) = txtProt(0).Text
            Set rsprotoc = qryProtoc.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If IsNull(rsprotoc(0)) Then
                grdssPruebas(0).Columns(7).Text = 1
            Else
                grdssPruebas(0).Columns(7).Text = rsprotoc(0) + 1
            End If
            objPruebas.rdoCursor("MB10_CODPROTPR") = grdssPruebas(0).Columns(7).Text
            
            rsprotoc.Close
            qryProtoc.Close
        End If
        
    End Select
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objProtocolos.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
            If intReport > 0 Then
                blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
                Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                         objWinInfo.DataGetOrder(blnHasFilter, True))
            End If
            Set objPrinter = Nothing
            
    Case objTecnicas.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
            If intReport > 0 Then
                blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
                Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                         objWinInfo.DataGetOrder(blnHasFilter, True))
            End If
            Set objPrinter = Nothing
            
    Case objPruebas.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
            If intReport > 0 Then
                blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
                Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                         objWinInfo.DataGetOrder(blnHasFilter, True))
            End If
            Set objPrinter = Nothing
  End Select
End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraTec, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraPruebas, True, True)
    End Select
End Sub

Private Sub Timer1_Timer()
    Select Case tabDatos.Tab
    Case 0
        fraDatos.ForeColor = fraTec.ForeColor
    Case 1
        fraDatos.ForeColor = fraPruebas.ForeColor
    End Select
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub
Sub pCargarDropDownTecnicas()
    Dim SQL As String
    
    SQL = "SELECT MB09_codTec,MB09_desig FROM MB0900"
    SQL = SQL & " WHERE cDptoSecc=" & departamento
    SQL = SQL & " AND MB09_indActiva =-1"
    If desigTiTec <> "" Then
        SQL = SQL & " AND MB04_codTiTec IN "
        SQL = SQL & "    (SELECT MB04_codTiTec FROM MB0400"
        SQL = SQL & "     WHERE MB04_desig = '" & desigTiTec & "')"
    End If
    SQL = SQL & " ORDER BY MB09_desig"
    objWinInfo.CtrlGetInfo(grdssTec(0).Columns(5)).strSQL = SQL
    'grdssTec(0).Columns(5).RemoveAll
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssTec(0).Columns(5)))
    objWinInfo.WinPrepareScr
    
End Sub
Sub pCargarDropDownTiposMuestras()
    Dim SQL As String
    
    SQL = "SELECT cTipoMuestra, Designacion FROM TiposMuestras"
    SQL = SQL & " WHERE cTipoMuestra IN "
    SQL = SQL & "       (SELECT cTipoMuestra FROM pruebasExtracciones pE, pruebasCarpetas pC"
    SQL = SQL & "        WHERE pE.cPrueba = pC.cPrueba"
    SQL = SQL & "        AND pC.cCarpeta IN"
    SQL = SQL & "           (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    If desigPrueba <> "" Then
        SQL = SQL & "        AND pC.cPrueba IN"
        SQL = SQL & "           (SELECT cPrueba FROM Pruebas WHERE Designacion = '" & desigPrueba & "')"
    End If
    SQL = SQL & "        )"
    SQL = SQL & " ORDER BY Designacion"
    'grdssPruebas(0).Columns(5).RemoveAll
    objWinInfo.CtrlGetInfo(grdssPruebas(0).Columns(5)).strSQL = SQL
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssPruebas(0).Columns(5)))
    objWinInfo.WinPrepareScr
    
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssProt_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssProt_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssProt_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssProt_Change()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssTec_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTec_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTec_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If grdssTec(0).Col = 5 Then
        If grdssTec(0).Columns(0).Text = "A�adido" Then
            desigTiTec = grdssTec(0).Columns(4).Text
            Call pCargarDropDownTecnicas
        End If
    End If
    
End Sub

Private Sub grdssTec_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssPruebas_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssPruebas_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPruebas_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  
    If grdssPruebas(0).Col = 5 Then
        If grdssPruebas(0).Columns(0).Text = "A�adido" Then
            desigPrueba = grdssPruebas(0).Columns(4).Text
            Call pCargarDropDownTiposMuestras
        End If
    End If
    
End Sub


Private Sub grdssPruebas_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub tabProt_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabProt, False, True)
End Sub



Private Sub fraProt_Click()
  Call objWinInfo.FormChangeActive(fraProt, False, True)
End Sub
Private Sub fraTec_Click()
  Call objWinInfo.FormChangeActive(fraTec, False, True)
End Sub


Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub chkProt_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkProt_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkProt_Click()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub txtProt_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtProt_LostFocus(intIndex As Integer)
     Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtProt_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
