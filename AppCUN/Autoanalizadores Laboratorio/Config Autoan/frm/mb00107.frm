VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_Antibiotico 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Antibi�ticos"
   ClientHeight    =   7020
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9780
   HelpContextID   =   30001
   Icon            =   "mb00107.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7020
   ScaleWidth      =   9780
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   9780
      _ExtentX        =   17251
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   420
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3345
      Left            =   120
      TabIndex        =   14
      Top             =   3360
      Width           =   9465
      Begin TabDlg.SSTab tabDatos 
         Height          =   2835
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Tag             =   "Sensibilidad|Sensibilidad"
         Top             =   360
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   5001
         _Version        =   327681
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         TabCaption(0)   =   "Utilizaci�n"
         TabPicture(0)   =   "mb00107.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraGrAnti"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Sensibilidad"
         TabPicture(1)   =   "mb00107.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraResist"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Relaci�n"
         TabPicture(2)   =   "mb00107.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "fraRelac"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Referencias"
         TabPicture(3)   =   "mb00107.frx":0060
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "fraReferencias"
         Tab(3).ControlCount=   1
         Begin VB.Frame fraReferencias 
            BorderStyle     =   0  'None
            Caption         =   "Referencias"
            Height          =   2370
            Left            =   -74940
            TabIndex        =   23
            Top             =   360
            Width           =   9060
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2310
               Index           =   3
               Left            =   60
               TabIndex        =   24
               Tag             =   "Relaci�n|Relaci�n"
               Top             =   60
               Width           =   8940
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15769
               _ExtentY        =   4075
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraGrAnti 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2385
            Left            =   60
            TabIndex        =   21
            Top             =   360
            Width           =   9135
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2310
               Index           =   0
               Left            =   60
               TabIndex        =   22
               Tag             =   "Utilizaci�n|Utilizaci�n"
               Top             =   60
               Width           =   8940
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   -1  'True
               _ExtentX        =   15769
               _ExtentY        =   4075
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraResist 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2370
            Left            =   -74940
            TabIndex        =   19
            Top             =   360
            Width           =   9060
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2310
               Index           =   1
               Left            =   60
               TabIndex        =   20
               Top             =   60
               Width           =   8940
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   -1  'True
               _ExtentX        =   15769
               _ExtentY        =   4075
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraRelac 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2370
            Left            =   -74940
            TabIndex        =   17
            Top             =   360
            Width           =   9060
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2310
               Index           =   2
               Left            =   60
               TabIndex        =   18
               Tag             =   "Relaci�n|Relaci�n"
               Top             =   60
               Width           =   8940
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15769
               _ExtentY        =   4075
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraAntibiotico 
      Caption         =   "Antibi�ticos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2805
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   9465
      Begin TabDlg.SSTab tabAntibiotico 
         Height          =   2310
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   360
         Width           =   9195
         _ExtentX        =   16219
         _ExtentY        =   4075
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00107.frx":007C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(7)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cbossAntibiotico"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtAntibiotico(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkAntibiotico"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtAntibiotico(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtAntibiotico(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtAntibiotico(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "mb00107.frx":0098
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssAntibiotico"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtAntibiotico 
            BackColor       =   &H00FFFF00&
            DataField       =   "CDPTOSECC"
            Height          =   285
            Index           =   3
            Left            =   3960
            MaxLength       =   50
            TabIndex        =   15
            Tag             =   "C. Secci�n|C�digo de la Secci�n"
            Top             =   180
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.TextBox txtAntibiotico 
            BackColor       =   &H0000FFFF&
            DataField       =   "MB07_CODANTI"
            Height          =   285
            Index           =   0
            Left            =   1560
            MaxLength       =   3
            TabIndex        =   0
            Tag             =   "C�digo|C�digo del antibi�tico"
            Top             =   210
            Width           =   495
         End
         Begin VB.TextBox txtAntibiotico 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB07_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1560
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n del antibi�tico"
            Top             =   660
            Width           =   3375
         End
         Begin VB.CheckBox chkAntibiotico 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "MB07_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   780
            TabIndex        =   4
            Tag             =   "Activa|Activa"
            Top             =   1860
            Width           =   990
         End
         Begin VB.TextBox txtAntibiotico 
            BackColor       =   &H00FFFF00&
            DataField       =   "MB07_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1560
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del antibi�tico"
            Top             =   1050
            Width           =   6555
         End
         Begin SSDataWidgets_B.SSDBCombo cbossAntibiotico 
            DataField       =   "MB01_CODELEC"
            Height          =   315
            Left            =   1560
            TabIndex        =   3
            Tag             =   "Elecci�n|Elecci�n"
            Top             =   1440
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBGrid grdssAntibiotico 
            Height          =   2010
            Left            =   -74880
            TabIndex        =   13
            Top             =   150
            Width           =   8640
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   15240
            _ExtentY        =   3545
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   250
            TabIndex        =   12
            Top             =   210
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   11
            Top             =   660
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Elecci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   250
            TabIndex        =   10
            Top             =   1500
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   255
            TabIndex        =   9
            Top             =   1080
            Width           =   1200
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   6735
      Width           =   9780
      _ExtentX        =   17251
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Antibiotico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objAntibiotico As New clsCWForm
Dim objMultiGrAnti As New clsCWForm
Dim objMultiResist As New clsCWForm
Dim objMultiRelac As New clsCWForm
Dim objMultiReferencias As New clsCWForm
  
Dim desigGeMicro1 As String
Dim desigGeMicro2 As String

Sub pCargarDropDownMicroorg(intIndex As Integer, desigGeMicro$)
    Dim SQL As String
    
    SQL = "SELECT MB18_CodMicro, MB18_Desig FROM MB1800"
    SQL = SQL & " WHERE MB18_indActiva = -1"
    If desigGeMicro = "" Then
        SQL = SQL & " AND MB11_codGeMicro IN"
        SQL = SQL & "       (SELECT MB11_codGeMicro FROM MB1100"
        SQL = SQL & "        WHERE MB08_codTiMic IN"
        SQL = SQL & "           (SELECT MB08_codTiMic FROM MB0800"
        SQL = SQL & "            WHERE cDptoSecc = " & departamento & "))"
    Else
        SQL = SQL & " AND MB11_codGeMicro IN"
        SQL = SQL & "    (SELECT MB11_codGeMicro FROM MB1100"
        SQL = SQL & "     WHERE MB11_DESIG = '" & desigGeMicro & "')"
    End If
    SQL = SQL & " ORDER BY MB18_Desig"
'    grdssDatos(intIndex).Columns(5).RemoveAll
    objWinInfo.CtrlGetInfo(grdssDatos(intIndex).Columns(5)).strSQL = SQL
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(intIndex).Columns(5)))
    
End Sub

Private Sub cbossAntibiotico_Click()
    Call objWinInfo.CtrlDataChange
    Call cbossAntibiotico_LostFocus
End Sub

Private Sub cbossAntibiotico_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossAntibiotico_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossAntibiotico_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkAntibiotico_Click()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkAntibiotico_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkAntibiotico_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub Form_Load()
  Dim strKey As String, SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objAntibiotico
    Set .objFormContainer = fraAntibiotico
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabAntibiotico
    Set .grdGrid = grdssAntibiotico
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB0700"
    .strWhere = "cDptoSecc = " & departamento
    Call .FormAddOrderField("MB0700.MB07_CODANTI", cwAscending)
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Antibi�ticos")
    Call .FormAddFilterWhere(strKey, "MB07_CODANTI", "C�digo antibi�tico", cwNumeric)
    Call .FormAddFilterWhere(strKey, "MB07_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB07_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "MB0700.MB01_CODELEC", "Elecci�n", cwNumeric) 'nombre tabla para report
    Call .FormAddFilterWhere(strKey, "MB07_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "MB07_CODANTI", "C�digo antibi�tico")
    Call .FormAddFilterOrder(strKey, "MB07_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "MB07_DESCRIP", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "MB0700.MB01_CODELEC", "Elecci�n") 'nombre tabla para report
    
    Call .objPrinter.Add("MB00148", "Antibi�ticos ordenados alfab�ticamente")
    Call .objPrinter.Add("MB00149", "Antibi�ticos por Nivel de Elecci�n")
    Call .objPrinter.Add("MB00143", "Referencias de Sensibilidad")
  End With
  
  With objMultiGrAnti
    .strName = "GrupoAnti"
    Set .objFormContainer = fraGrAnti
    Set .objFatherContainer = fraAntibiotico
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1700"
    Call .FormAddOrderField("MB31_CODGRANTI", cwAscending)
    Call .FormAddRelation("MB07_CODANTI", txtAntibiotico(0))
  End With
  
  With objMultiResist
    .strName = "Resist"
    Set .objFormContainer = fraResist
    Set .objFatherContainer = fraAntibiotico
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2400"
    SQL = "MB18_codMicro IN"
    SQL = SQL & "(SELECT MB18_CodMicro FROM MB1800"
    SQL = SQL & " WHERE MB11_codGeMicro IN"
    SQL = SQL & "       (SELECT MB11_codGeMicro FROM MB1100"
    SQL = SQL & "        WHERE MB08_codTiMic IN"
    SQL = SQL & "           (SELECT MB08_codTiMic FROM MB0800"
    SQL = SQL & "            WHERE cDptoSecc = " & departamento & ")))"
    .strWhere = SQL
    Call .FormAddOrderField("MB18_CODMICRO", cwAscending)
    Call .FormAddOrderField("MB40_CODREAC", cwAscending)
    Call .FormAddRelation("MB07_CODANTI", txtAntibiotico(0))
  End With
  
  With objMultiRelac
    .strName = "Relac"
    Set .objFormContainer = fraRelac
    Set .objFatherContainer = fraAntibiotico
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2300"
    SQL = "(MB18_codMicro IN"
    SQL = SQL & "(SELECT MB18_CodMicro FROM MB1800"
    SQL = SQL & " WHERE MB11_codGeMicro IN"
    SQL = SQL & "       (SELECT MB11_codGeMicro FROM MB1100"
    SQL = SQL & "        WHERE MB08_codTiMic IN"
    SQL = SQL & "           (SELECT MB08_codTiMic FROM MB0800"
    SQL = SQL & "            WHERE cDptoSecc = " & departamento & ")))"
    SQL = SQL & " OR MB18_codMicro IS NULL)"
    .strWhere = SQL
    Call .FormAddOrderField("MB18_CODMICRO", cwAscending)
    Call .FormAddOrderField("MB07_CODANTI_2", cwAscending)
    Call .FormAddRelation("MB07_CODANTI_1", txtAntibiotico(0))
  End With
  
  With objMultiReferencias
    .strName = "Referencias"
    Set .objFormContainer = fraReferencias
    Set .objFatherContainer = fraAntibiotico
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB4400"
    Call .FormAddOrderField("MB4400.MB40_CODREAC", cwAscending) 'nombre tabla para report
    Call .FormAddRelation("MB07_CODANTI", txtAntibiotico(0))
    
    'Call .objPrinter.Add("MB00143", "Referencias de Sensibilidad") 'columna ambigua

  End With
  
  With objWinInfo
    Call .FormAddInfo(objAntibiotico, cwFormDetail)
    
    Call .FormAddInfo(objMultiGrAnti, cwFormMultiLine)
    Call .GridAddColumn(objMultiGrAnti, "Antibi�tico", "MB07_CODANTI", cwNumeric, 2)
    Call .GridAddColumn(objMultiGrAnti, "Grupo antibi�ticos", "MB31_CODGRANTI", cwNumeric, 2)
    
    Call .FormAddInfo(objMultiResist, cwFormMultiLine)
    Call .GridAddColumn(objMultiResist, "Antibi�tico", "MB07_CODANTI", cwNumeric, 2)
    Call .GridAddColumn(objMultiResist, "G�nero microorganismo", "", cwNumeric, 2)
    Call .GridAddColumn(objMultiResist, "Microorganismo", "MB18_CODMICRO", cwNumeric, 3)
    Call .GridAddColumn(objMultiResist, "Sensibilidad", "MB40_CODREAC", cwNumeric, 1)
    Call .GridAddColumn(objMultiResist, "Activa", "MB24_INDACTIVA", cwBoolean, 1)
    
    Call .FormAddInfo(objMultiRelac, cwFormMultiLine)
    Call .GridAddColumn(objMultiRelac, "Antibi�tico1", "MB07_CODANTI_1", cwNumeric, 2)
    Call .GridAddColumn(objMultiRelac, "G�nero microorganismo ", "", cwNumeric, 2)
    Call .GridAddColumn(objMultiRelac, "Microorganismo ", "MB18_CODMICRO", cwNumeric, 3)
    Call .GridAddColumn(objMultiRelac, "Sensib. Antib. 1", "MB40_CODREAC_1", cwNumeric, 1)
    Call .GridAddColumn(objMultiRelac, "Antibi�tico 2", "MB07_CODANTI_2", cwNumeric, 2)
    Call .GridAddColumn(objMultiRelac, "Sensib. Antib. 2", "MB40_CODREAC_2", cwNumeric, 1)
    Call .GridAddColumn(objMultiRelac, "Activa", "MB23_INDACTIVA", cwBoolean, 1)
    Call .GridAddColumn(objMultiRelac, "contador", "MB23_CODREL", cwNumeric, 2)
  
    Call .FormAddInfo(objMultiReferencias, cwFormMultiLine)
    Call .GridAddColumn(objMultiReferencias, "Antibi�tico", "MB07_CODANTI", cwNumeric, 2)
    Call .GridAddColumn(objMultiReferencias, "Sensibilidad", "MB40_CODREAC", cwNumeric, 1)
    Call .GridAddColumn(objMultiReferencias, "Di�m. m�nimo", "MB44_DIAMINI", cwNumeric, 3)
    Call .GridAddColumn(objMultiReferencias, "Di�m. m�ximo", "MB44_DIAMFIN", cwNumeric, 3)
    Call .GridAddColumn(objMultiReferencias, "Conc. m�nima", "MB44_CONCINI", cwDecimal, 4)
    Call .GridAddColumn(objMultiReferencias, "Conc. m�xima", "MB44_CONCFIN", cwDecimal, 4)
    
    Call .FormCreateInfo(objAntibiotico)
        
    .CtrlGetInfo(chkAntibiotico).blnInFind = True
    .CtrlGetInfo(txtAntibiotico(0)).blnValidate = False
    .CtrlGetInfo(txtAntibiotico(0)).blnInFind = True
    .CtrlGetInfo(txtAntibiotico(1)).blnInFind = True
    .CtrlGetInfo(txtAntibiotico(2)).blnInFind = True
    .CtrlGetInfo(txtAntibiotico(3)).blnValidate = False
    .CtrlGetInfo(txtAntibiotico(3)).blnInGrid = False
    
    SQL = "SELECT MB01_CodElec, MB01_Desig FROM MB0100"
    SQL = SQL & " WHERE MB01_indActiva = -1"
    SQL = SQL & " ORDER BY MB01_CodElec"
    .CtrlGetInfo(cbossAntibiotico).strSQL = SQL
    .CtrlGetInfo(cbossAntibiotico).blnForeign = True
    
    Call .FormChangeColor(objMultiGrAnti)
    grdssDatos(0).Columns(3).Visible = False
    Call .FormChangeColor(objMultiResist)
    grdssDatos(1).Columns(3).Visible = False
    Call .FormChangeColor(objMultiRelac)
    grdssDatos(2).Columns(3).Visible = False
    grdssDatos(2).Columns(10).Visible = False
    .CtrlGetInfo(grdssDatos(2).Columns(10)).blnValidate = False
    Call .FormChangeColor(objMultiReferencias)
    grdssDatos(3).Columns(3).Visible = False
    grdssDatos(3).Columns(5).Width = 1400
    grdssDatos(3).Columns(6).Width = 1400
    grdssDatos(3).Columns(7).Width = 1400
    grdssDatos(3).Columns(8).Width = 1400
    
    ' se llenan los DropDown
    SQL = "SELECT MB31_CodGrAnti, MB31_Desig FROM MB3100"
    SQL = SQL & " WHERE MB31_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB31_CodGrAnti"
    .CtrlGetInfo(grdssDatos(0).Columns(4)).strSQL = SQL
    .CtrlGetInfo(grdssDatos(0).Columns(4)).blnForeign = True
    
    SQL = "SELECT MB11_codGeMicro, MB11_Desig FROM MB1100"
    SQL = SQL & " WHERE MB11_indActiva = -1"
    SQL = SQL & " AND MB08_codTiMic IN"
    SQL = SQL & "       (SELECT MB08_codTiMic FROM MB0800"
    SQL = SQL & "        WHERE cDptoSecc = " & departamento & ")"
    SQL = SQL & " ORDER BY MB11_Desig"
    .CtrlGetInfo(grdssDatos(1).Columns(4)).strSQL = SQL
'    .CtrlGetInfo(grdssDatos(2).Columns(4)).strSQL = SQL 'ERROR FUNCIONES LINK NULL

    SQL = "SELECT MB18_CodMicro, MB18_Desig FROM MB1800"
    SQL = SQL & " WHERE MB18_indActiva = -1"
    SQL = SQL & " AND MB11_codGeMicro IN"
    SQL = SQL & "       (SELECT MB11_codGeMicro FROM MB1100"
    SQL = SQL & "        WHERE MB08_codTiMic IN"
    SQL = SQL & "           (SELECT MB08_codTiMic FROM MB0800"
    SQL = SQL & "            WHERE cDptoSecc = " & departamento & "))"
    SQL = SQL & " ORDER BY MB18_Desig"
    .CtrlGetInfo(grdssDatos(1).Columns(5)).strSQL = SQL
    .CtrlGetInfo(grdssDatos(2).Columns(5)).strSQL = SQL
    .CtrlGetInfo(grdssDatos(1).Columns(5)).blnForeign = True
    .CtrlGetInfo(grdssDatos(2).Columns(5)).blnForeign = True
    
    SQL = "SELECT MB40_CodReac, MB40_Desig FROM MB4000"
    SQL = SQL & " WHERE MB40_indActiva = -1"
    SQL = SQL & " ORDER BY MB40_Desig"
    .CtrlGetInfo(grdssDatos(1).Columns(6)).strSQL = SQL
    .CtrlGetInfo(grdssDatos(2).Columns(6)).strSQL = SQL
    .CtrlGetInfo(grdssDatos(2).Columns(8)).strSQL = SQL
    .CtrlGetInfo(grdssDatos(3).Columns(4)).strSQL = SQL
    .CtrlGetInfo(grdssDatos(1).Columns(6)).blnForeign = True
    .CtrlGetInfo(grdssDatos(2).Columns(6)).blnForeign = True
    .CtrlGetInfo(grdssDatos(2).Columns(8)).blnForeign = True
    .CtrlGetInfo(grdssDatos(3).Columns(4)).blnForeign = True
    
    SQL = "SELECT MB07_CodAnti, MB07_Desig FROM MB0700 "
    SQL = SQL & " WHERE MB07_indActiva = -1"
    SQL = SQL & " AND cDptoSecc = " & departamento
    SQL = SQL & " ORDER BY MB07_Desig"
    .CtrlGetInfo(grdssDatos(2).Columns(7)).strSQL = SQL
    '.CtrlGetInfo(grdssDatos(2).Columns(7)).blnForeign = True
        
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraReferencias_Click()
    Call objWinInfo.FormChangeActive(fraReferencias, False, True)
End Sub

Private Sub fraRelac_Click()
    Call objWinInfo.FormChangeActive(fraRelac, False, True)
End Sub

Private Sub fraResist_Click()
    Call objWinInfo.FormChangeActive(fraResist, False, True)
End Sub
Private Sub fraAntibiotico_Click()
    Call objWinInfo.FormChangeActive(fraAntibiotico, False, True)
End Sub

Private Sub fraDatos_Click()
    Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraGrAnti, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraResist, True, True)
    Case 2
        Call objWinInfo.FormChangeActive(fraRelac, True, True)
    Case 3
        Call objWinInfo.FormChangeActive(fraReferencias, True, True)
    End Select
    'grdssDatos(tabDatos.Tab).SetFocus
End Sub


Private Sub fraGrAnti_Click()
    Call objWinInfo.FormChangeActive(fraGrAnti, False, True)
End Sub

Private Sub grdssDatos_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssDatos_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssDatos_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub
Private Sub grdssDatos_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  
    Select Case intIndex
    Case 1
        If grdssDatos(intIndex).Col = 5 Then
            If grdssDatos(intIndex).Columns(0).Text = "A�adido" Then
                desigGeMicro1 = grdssDatos(intIndex).Columns(4).Text
                Call pCargarDropDownMicroorg(intIndex, desigGeMicro1)
            End If
        End If
    Case 2
        If grdssDatos(intIndex).Col = 5 Then
            If grdssDatos(intIndex).Columns(0).Text = "A�adido" Then
                desigGeMicro2 = grdssDatos(intIndex).Columns(4).Text
                Call pCargarDropDownMicroorg(intIndex, desigGeMicro2)
            End If
        End If
    End Select
      
End Sub



Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    
    On Error GoTo label
    
    Select Case strFormName
        Case objAntibiotico.strName
            If strCtrl = objWinInfo.CtrlGetInfo(cbossAntibiotico).strName Then
                frmA_Eleccion.Show vbModal
                Set frmA_Eleccion = Nothing
                Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossAntibiotico))
            End If
        Case objMultiGrAnti.strName
            If strCtrl = objWinInfo.CtrlGetInfo(grdssDatos(0).Columns(4)).strName Then
                frmA_GrAntibiotico.Show vbModal
                Set frmA_GrAntibiotico = Nothing
                Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(0).Columns(4)))
            End If
        Case objMultiResist.strName, objMultiRelac.strName
            Select Case strCtrl
                Case objWinInfo.CtrlGetInfo(grdssDatos(1).Columns(5)).strName, objWinInfo.CtrlGetInfo(grdssDatos(2).Columns(5)).strName
                    frmA_Microorganismo.Show vbModal
                    Set frmA_Microorganismo = Nothing
                    'no se llena el DropDown del objMultiRelac ya que se llena en el RowColChange
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(1).Columns(5)))
                Case objWinInfo.CtrlGetInfo(grdssDatos(1).Columns(6)).strName, objWinInfo.CtrlGetInfo(grdssDatos(2).Columns(6)).strName, objWinInfo.CtrlGetInfo(grdssDatos(2).Columns(8)).strName
                    frmA_Reaccion.Show vbModal
                    Set frmA_Reaccion = Nothing
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(1).Columns(6)))
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(2).Columns(6)))
                    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(2).Columns(8)))
            End Select
        Case objMultiReferencias.strName
            If strCtrl = objWinInfo.CtrlGetInfo(grdssDatos(3).Columns(4)).strName Then
                frmA_Reaccion.Show vbModal
                Set frmA_Reaccion = Nothing
                Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(3).Columns(4)))
            End If
    End Select
    
label:
    If Err = 400 Then
        MsgBox "La Ventana a la que se quiere acceder ya est� activa.", vbInformation, "Acceso a Ventana"
    ElseIf Err <> 0 Then
        MsgBox Error
    End If
    
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: antibi�ticos
    
'Si se realiza alguna operaci�n en el formulario de t�cnicas(objAntibiotico) habr� que anotar _
el mantenimiento para los antibi�ticos
        
    If strFormName = objAntibiotico.strName Or strFormName = objMultiGrAnti.strName Then
        Call pAnotarMantenimiento(constMANTANTIBIOTICOS)
    End If
    
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'se anotan los mantenimientos que se han realizado y que afectan a la pantalla de solicitud _
de t�cnicas: antibi�ticos
    
'Si se realiza alguna operaci�n en el formulario de antibi�ticos(objAntibiotico) o en el _
de grupos de antibi�ticos(objMultiGrAnti) habr� que anotar el mantenimiento para los _
antibi�ticos
    
    If strFormName = objAntibiotico.strName Or strFormName = objMultiGrAnti.strName Then
        Call pAnotarMantenimiento(constMANTANTIBIOTICOS)
    End If
    
End Sub


Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)

    Select Case strFormName
'    Case objAntibiotico.strName
'        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
'            txtAntibiotico(0).Text = fNextClave("MB07_codAnti", "MB0700")
'            objAntibiotico.rdoCursor("MB07_codAnti") = txtAntibiotico(0).Text
'        End If
        Case objMultiResist.strName
            If desigGeMicro1 <> "" Then
                desigGeMicro1 = ""
                Call pCargarDropDownMicroorg(1, desigGeMicro1)
            End If
        Case objMultiRelac.strName
            If desigGeMicro2 <> "" Then
                desigGeMicro2 = ""
                Call pCargarDropDownMicroorg(2, desigGeMicro2)
            End If
        Case objMultiReferencias.strName
            If Val(grdssDatos(3).Columns(5).Text) > Val(grdssDatos(3).Columns(6).Text) And grdssDatos(3).Columns(6).Text <> "" Then
                MsgBox "El di�metro m�nimo no puede ser mayor que el m�ximo.", vbExclamation, Me.Caption
                blnCancel = True
                Exit Sub
            End If
            If Val(grdssDatos(3).Columns(7).Text) > Val(grdssDatos(3).Columns(8).Text) And grdssDatos(3).Columns(8).Text <> "" Then
                MsgBox "La concentraci�n m�nima no puede ser mayor que la m�xima.", vbExclamation, Me.Caption
                blnCancel = True
            End If
    End Select
    
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Dim rsRelAntib As rdoResultset, qryRelAntib As rdoQuery
    Dim SQL As String
    
    Select Case strFormName
    Case objAntibiotico.strName
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtAntibiotico(0).Text = fNextClave("MB07_codAnti", "MB0700")
            txtAntibiotico(3).Text = departamento
            objAntibiotico.rdoCursor("MB07_codAnti") = txtAntibiotico(0).Text
            objAntibiotico.rdoCursor("cDptoSecc") = txtAntibiotico(3).Text
        End If
    Case objMultiRelac.strName
        If grdssDatos(2).Columns(10).Text = "" Then
              SQL = "SELECT MAX(MB23_codRel)"
              SQL = SQL & " FROM MB2300"
              SQL = SQL & " WHERE MB07_codAnti_1 = ?"
              SQL = SQL & " AND MB07_codAnti_2 = ?"
              Set qryRelAntib = objApp.rdoConnect.CreateQuery("SelMaxCodRelAntib", SQL)
              qryRelAntib(0) = objMultiRelac.rdoCursor(0)
              qryRelAntib(1) = objMultiRelac.rdoCursor(3)
              Set rsRelAntib = qryRelAntib.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
              If IsNull(rsRelAntib(0)) Then
                  grdssDatos(2).Columns(10).Text = 1
              Else
                  grdssDatos(2).Columns(10).Text = rsRelAntib(0) + 1
              End If
              objMultiRelac.rdoCursor("MB23_codRel") = grdssDatos(2).Columns(10).Text
              
              rsRelAntib.Close
              qryRelAntib.Close
          End If
'      Case objMultiReferencias.strName
'          If Val(grdssDatos(3).Columns(5).Text) > Val(grdssDatos(3).Columns(6).Text) And grdssDatos(3).Columns(6).Text <> "" Then
'            MsgBox "El di�metro m�nimo no puede ser mayor que el m�ximo.", vbExclamation, Me.Caption
'            objMultiReferencias.rdoCursor("MB44_diamIni").Value = Null
'            objMultiReferencias.rdoCursor("MB44_diamFin").Value = Null
'          End If
'          If Val(grdssDatos(3).Columns(7).Text) > Val(grdssDatos(3).Columns(8).Text) And grdssDatos(3).Columns(8).Text <> "" Then
'            MsgBox "La concentraci�n m�nima no puede ser mayor que la m�xima.", vbExclamation, Me.Caption
'            objMultiReferencias.rdoCursor("MB44_concIni").Value = Null
'            objMultiReferencias.rdoCursor("MB44_concFin").Value = Null
'          End If
    End Select
    
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)

  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objAntibiotico.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
        
    Case objMultiReferencias.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
  
  End Select
End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraGrAnti, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraResist, True, True)
    Case 2
        Call objWinInfo.FormChangeActive(fraRelac, True, True)
    Case 3
        Call objWinInfo.FormChangeActive(fraReferencias, True, True)
    End Select
    'grdssDatos(tabDatos.Tab).SetFocus
End Sub
Private Sub tabAntibiotico_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabAntibiotico, False, True)
End Sub

Private Sub Timer1_Timer()
    Select Case tabDatos.Tab
    Case 0
        fraDatos.ForeColor = fraGrAnti.ForeColor
    Case 1
        fraDatos.ForeColor = fraResist.ForeColor
    Case 2
        fraDatos.ForeColor = fraRelac.ForeColor
    Case 3
        fraDatos.ForeColor = fraReferencias.ForeColor
    End Select

End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssAntibiotico_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssAntibiotico_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssAntibiotico_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssAntibiotico_Change()
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub txtAntibiotico_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtAntibiotico_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtAntibiotico_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

