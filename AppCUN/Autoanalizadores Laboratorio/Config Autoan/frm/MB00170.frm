VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_Paciente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Pacientes"
   ClientHeight    =   7260
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   11565
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   11565
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   11565
      _ExtentX        =   20399
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraPruebaAsistencia 
      Caption         =   "Pruebas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Index           =   0
      Left            =   60
      TabIndex        =   39
      Top             =   4380
      Width           =   11415
      Begin SSDataWidgets_B.SSDBGrid grdSSPruebaAsistencia 
         Height          =   2205
         Index           =   0
         Left            =   60
         TabIndex        =   40
         TabStop         =   0   'False
         Tag             =   "Pruebas|Pruebas"
         Top             =   240
         Width           =   11295
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   19923
         _ExtentY        =   3889
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraPac 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Index           =   0
      Left            =   60
      TabIndex        =   17
      Top             =   480
      Width           =   11415
      Begin TabDlg.SSTab tabPac 
         Height          =   3390
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   360
         Width           =   11130
         _ExtentX        =   19632
         _ExtentY        =   5980
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00170.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(9)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(25)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(20)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(21)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(22)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(10)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(5)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(27)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(28)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(29)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtAsist(9)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtAsist(5)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtAsist(6)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtAsist(7)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtAsist(8)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtAsist(4)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtAsist(3)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtAsist(2)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtAsist(0)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtAsist(1)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtAsist(12)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtAsist(14)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtAsist(10)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtAsist(11)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtAsist(13)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtAsist(17)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtAsist(16)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtAsist(15)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "SSPanel1"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).ControlCount=   35
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "MB00170.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdSSPac(0)"
         Tab(1).ControlCount=   1
         Begin Threed.SSPanel SSPanel1 
            Height          =   1035
            Left            =   8460
            TabIndex        =   41
            Top             =   900
            Width           =   1755
            _Version        =   65536
            _ExtentX        =   3096
            _ExtentY        =   1826
            _StockProps     =   15
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelOuter      =   0
            Enabled         =   0   'False
            Begin VB.Frame Frame3 
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000007&
               Height          =   900
               Index           =   0
               Left            =   60
               TabIndex        =   42
               Top             =   60
               Width           =   1590
               Begin VB.OptionButton optPaciente 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Hombre"
                  Height          =   240
                  Index           =   0
                  Left            =   240
                  TabIndex        =   44
                  Tag             =   "Sexo|Sexo"
                  Top             =   240
                  Value           =   -1  'True
                  Width           =   990
               End
               Begin VB.OptionButton optPaciente 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Mujer"
                  Height          =   240
                  Index           =   1
                  Left            =   240
                  TabIndex        =   43
                  Tag             =   "Sexo|Sexo"
                  Top             =   600
                  Width           =   990
               End
            End
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "tfno"
            Height          =   285
            Index           =   15
            Left            =   9150
            MaxLength       =   50
            TabIndex        =   14
            Tag             =   "Tfno.|N� de Tel�fono"
            Top             =   2100
            Width           =   1530
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fax"
            Height          =   285
            Index           =   16
            Left            =   9150
            MaxLength       =   50
            TabIndex        =   15
            Tag             =   "Fax|Fax"
            Top             =   2400
            Width           =   1530
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   17
            Left            =   9900
            MaxLength       =   50
            TabIndex        =   16
            Tag             =   "e-mail|e-mail"
            Top             =   3000
            Visible         =   0   'False
            Width           =   330
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "pcia"
            Height          =   285
            Index           =   13
            Left            =   1305
            MaxLength       =   50
            TabIndex        =   12
            Tag             =   "Provincia|Provincia"
            Top             =   2700
            Width           =   2730
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "pobl"
            Height          =   285
            Index           =   11
            Left            =   1305
            MaxLength       =   50
            TabIndex        =   10
            Tag             =   "Poblaci�n|Poblaci�n del docmicilio"
            Top             =   2400
            Width           =   4155
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "calle"
            Height          =   285
            Index           =   10
            Left            =   1305
            MaxLength       =   50
            TabIndex        =   9
            Tag             =   "Direcci�n|Direcci�n"
            Top             =   2100
            Width           =   6105
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "pais"
            Height          =   285
            Index           =   14
            Left            =   4905
            MaxLength       =   50
            TabIndex        =   13
            Tag             =   "Pa�s|Pa�s"
            Top             =   2700
            Width           =   2505
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "cp"
            Height          =   285
            Index           =   12
            Left            =   6330
            MaxLength       =   50
            TabIndex        =   11
            Tag             =   "C.P.|C�digo Postal"
            Top             =   2400
            Width           =   1080
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "sexo"
            Height          =   285
            Index           =   1
            Left            =   10200
            TabIndex        =   4
            Tag             =   "Sexo|Sexo"
            Top             =   1320
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H0000FFFF&
            DataField       =   "NH"
            Height          =   285
            Index           =   0
            Left            =   1320
            TabIndex        =   0
            Tag             =   "N� Historia|N� Historia"
            Top             =   540
            Width           =   870
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "nom"
            Height          =   285
            Index           =   2
            Left            =   1320
            MaxLength       =   50
            TabIndex        =   1
            Tag             =   "Nombre|Nombre del Paciente"
            Top             =   915
            Width           =   4230
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "ap1"
            Height          =   285
            Index           =   3
            Left            =   1320
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Apellido 1|Primer Apellido del Paciente"
            Top             =   1215
            Width           =   4230
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "ap2"
            Height          =   285
            Index           =   4
            Left            =   1320
            MaxLength       =   50
            TabIndex        =   3
            Tag             =   "Apellido 2|Segundo Apellido del Paciente"
            Top             =   1515
            Width           =   4230
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fnac"
            Height          =   285
            Index           =   8
            Left            =   6945
            MaxLength       =   50
            TabIndex        =   8
            Tag             =   "F. Nacimiento|Fecha de Nacimiento"
            Top             =   1515
            Width           =   1350
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "dni"
            Height          =   285
            Index           =   7
            Left            =   6945
            MaxLength       =   50
            TabIndex        =   7
            Tag             =   "DNI|Documento Nacional de Identidad"
            Top             =   1215
            Width           =   1350
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "ss"
            Height          =   285
            Index           =   6
            Left            =   6945
            MaxLength       =   50
            TabIndex        =   6
            Tag             =   "N� S.S.|N�mero de la Seguridad Social"
            Top             =   915
            Width           =   1350
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "cPais"
            Height          =   285
            Index           =   5
            Left            =   7440
            MaxLength       =   50
            TabIndex        =   21
            Tag             =   "cPais|cPais"
            Top             =   2700
            Visible         =   0   'False
            Width           =   330
         End
         Begin VB.TextBox txtAsist 
            BackColor       =   &H00FFFFFF&
            DataField       =   "cama"
            Height          =   285
            Index           =   9
            Left            =   9180
            MaxLength       =   50
            TabIndex        =   5
            Tag             =   "Cama|Cama de Ingreso"
            Top             =   540
            Width           =   690
         End
         Begin SSDataWidgets_B.SSDBGrid grdSSPac 
            Height          =   3165
            Index           =   0
            Left            =   -74940
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   120
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   5583
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tfno:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   29
            Left            =   8475
            TabIndex        =   37
            Tag             =   "Tel�fono|Tel�fono"
            Top             =   2100
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fax:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   8550
            TabIndex        =   36
            Top             =   2400
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            Caption         =   "e-mail:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   27
            Left            =   9300
            TabIndex        =   35
            Top             =   3060
            Visible         =   0   'False
            Width           =   630
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Provincia:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   255
            TabIndex        =   34
            Top             =   2700
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Poblaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   180
            TabIndex        =   33
            Top             =   2400
            Width           =   930
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Calle:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   630
            TabIndex        =   32
            Top             =   2100
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Pais:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   4305
            TabIndex        =   31
            Top             =   2775
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "CP:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   5880
            TabIndex        =   30
            Top             =   2460
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   420
            TabIndex        =   29
            Top             =   540
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   420
            TabIndex        =   28
            Top             =   915
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            Caption         =   "1� Apellido:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   21
            Left            =   120
            TabIndex        =   27
            Top             =   1215
            Width           =   1005
         End
         Begin VB.Label lblLabel1 
            Caption         =   "2� Apellido:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   120
            TabIndex        =   26
            Top             =   1515
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "F. Nacimiento:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   5640
            TabIndex        =   25
            Top             =   1515
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� SS:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   6300
            TabIndex        =   24
            Top             =   960
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            Caption         =   "DNI:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   25
            Left            =   6480
            TabIndex        =   23
            Top             =   1260
            Width           =   465
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cama:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   8520
            TabIndex        =   22
            Top             =   600
            Width           =   915
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   20
      Top             =   6975
      Width           =   11565
      _ExtentX        =   20399
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.ImageList imlPruebaAsistencia 
      Index           =   0
      Left            =   0
      Top             =   420
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   7
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00170.frx":0038
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00170.frx":0096
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00170.frx":00F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00170.frx":0152
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00170.frx":01B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00170.frx":020E
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00170.frx":026C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Nuevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener"
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        Ctrl+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior       RePag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     AvPag"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          Ctrl+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Paciente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objAsistInfo As New clsCWForm
Dim objPruebaInfo As New clsCWForm

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)

End Sub


Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  Dim i As Integer
  Dim historia As String
      
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  'se busca la historia, si se ha accedido con una historia particular
  If objPipe.PipeExist("BuscHistoria") Then
    historia = objPipe.PipeGet("BuscHistoria")
  Else
    historia = ""
  End If
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objAsistInfo
    .strName = "ASIST"
    Set .objFormContainer = fraPac(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPac(0)
    Set .grdGrid = grdSSPac(0)
    .intAllowance = cwAllowReadOnly
    '.strDataBase = objEnv.GetValue("Main")
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    .blnHasMaint = False
    
    .strTable = "PAC_1"
    SQL = "(nh) IN "
    SQL = SQL & "   (SELECT historia FROM pruebaAsistencia pA, carpetas c"
    SQL = SQL & "   WHERE c.cCarpeta=pA.cCarpeta"
    SQL = SQL & "   AND c.cDptoSecc = " & departamento
    SQL = SQL & "   )"
    If historia <> "" Then
        SQL = SQL & " AND nh = " & historia
    End If
    .strWhere = SQL
    Call .FormAddOrderField("NH", cwAscending)
        
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "ASIST")
    Call .FormAddFilterWhere(strKey, "NH", "Historia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "Ap1", "Apellido 1", cwString)
    Call .FormAddFilterWhere(strKey, "Ap2", "Apellido 2", cwString)
    Call .FormAddFilterWhere(strKey, "Nom", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "fNac", "Nacimiento", cwDate)
    Call .FormAddFilterWhere(strKey, "cama", "Cama", cwNumeric)
    Call .FormAddFilterWhere(strKey, "pobl", "Poblaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "pcia", "Provincia", cwString)
    
    Call .FormAddFilterOrder(strKey, "NH", "Historia")
    Call .FormAddFilterOrder(strKey, "Ap1", "Apellido 1")
    Call .FormAddFilterOrder(strKey, "Ap2", "Apellido 2")
    Call .FormAddFilterOrder(strKey, "Nom", "Nombre")
    Call .FormAddFilterOrder(strKey, "fNac", "Nacimiento")
    Call .FormAddFilterOrder(strKey, "cama", "Cama")
    Call .FormAddFilterOrder(strKey, "pobl", "Poblaci�n")
    Call .FormAddFilterOrder(strKey, "pcia", "Provincia")
  End With
   
  With objPruebaInfo
    .strName = "PRUEBAASISTENCIA"
    Set .objFormContainer = fraPruebaAsistencia(0)
    Set .objFatherContainer = fraPac(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssPruebaAsistencia(0)
    .intAllowance = cwAllowReadOnly
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    '.strDataBase = objEnv.GetValue("Main")
    
    .strTable = "PRUEBAASISTENCIA_3"
    SQL = "proceso  = " & constPROCESOSOLICITUD
    SQL = SQL & " AND cCarpeta IN "
    SQL = SQL & "       (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    .strWhere = SQL
    Call .FormAddOrderField("fechaIngreso", cwDescending)
    Call .FormAddOrderField("secuencia", cwDescending)
    Call .FormAddRelation("historia", txtAsist(0))

    'strKey = .strDataBase & .strTable
  End With
  
  With objWinInfo
    Call .FormAddInfo(objAsistInfo, cwFormDetail)
    Call .FormAddInfo(objPruebaInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objPruebaInfo, "N� Ref.", "nRef", cwNumeric, 12)
    Call .GridAddColumn(objPruebaInfo, "Caso", "caso", cwNumeric, 6)
    Call .GridAddColumn(objPruebaInfo, "Secuencia", "secuencia", cwNumeric, 4)
    Call .GridAddColumn(objPruebaInfo, "F. Ingreso", "fechaIngreso", cwDate, 22)
    Call .GridAddColumn(objPruebaInfo, "Prueba", "desigPrueba", cwString, 20)
    Call .GridAddColumn(objPruebaInfo, "F. Petici�n", "fechaPeticion", cwDate, 22)
    Call .GridAddColumn(objPruebaInfo, "Muestra", "cMuestra", cwString, 10)
    Call .GridAddColumn(objPruebaInfo, "F. Extracci�n", "fechaExtraccion", cwDate, 22)
    Call .GridAddColumn(objPruebaInfo, "Dpto.", "dptSolicitante", cwString, 20)
    Call .GridAddColumn(objPruebaInfo, "Doctor", "drSolicitante", cwString, 20)
    Call .GridAddColumn(objPruebaInfo, "cEstado", "estado", cwNumeric, 2)
    Call .GridAddColumn(objPruebaInfo, "Estado", "", cwString, 20)
    Call .GridAddColumn(objPruebaInfo, "Cama", "cama", cwString, 6)
    
    Call .FormCreateInfo(objAsistInfo)
    'campos no visibles en la tabla del objAsistInfo
    '.CtrlGetInfo(txtAsist(1)).blnInGrid = False
    .CtrlGetInfo(txtAsist(5)).blnInGrid = False
    'campos inclu�dos en la b�squeda del objAsistInfo
    .CtrlGetInfo(txtAsist(0)).blnInFind = True
    .CtrlGetInfo(txtAsist(3)).blnInFind = True
    .CtrlGetInfo(txtAsist(4)).blnInFind = True
    .CtrlGetInfo(txtAsist(2)).blnInFind = True
    '.CtrlGetInfo(txtAsist(8)).blnInFind = True 'CWCun.dll no soporta b�squedas por campos tipo fecha
    .CtrlGetInfo(txtAsist(9)).blnInFind = True
    .CtrlGetInfo(txtAsist(11)).blnInFind = True
    .CtrlGetInfo(txtAsist(13)).blnInFind = True
    'campos ReadOnly del objAsistInfo
    For i = 0 To txtAsist.Count - 1
        .CtrlGetInfo(txtAsist(i)).blnReadOnly = True
    Next i
    'campos ReadOnly del objPruebaInfo
    For i = 3 To grdssPruebaAsistencia(0).Cols - 1
        .CtrlGetInfo(grdssPruebaAsistencia(0).Columns(i)).blnReadOnly = True
    Next i
    'campos no visibles en la tabla del objPruebaInfo
    grdssPruebaAsistencia(0).Columns(13).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
  End With

  Screen.MousePointer = vbDefault 'Call objApp.SplashOff

End Sub
Private Sub Form_QueryUnload(intCancel As Integer, UnloadMode As Integer)
  intCancel = objWinInfo.WinExit

End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo

End Sub


Private Sub fraPac_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraPac(intIndex), False, True)

End Sub

Private Sub fraPruebaAsistencia_Click(intIndex As Integer)
Call objWinInfo.FormChangeActive(fraPruebaAsistencia(intIndex), False, True)
End Sub

Private Sub grdssPac_Change(Index As Integer)
Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssPac_DblClick(Index As Integer)
Call objWinInfo.GridDblClick
End Sub


Private Sub grdssPac_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub


Private Sub grdssPac_RowColChange(Index As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)

End Sub

Private Sub grdSSPruebaAsistencia_Change(Index As Integer)
Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdSSPruebaAsistencia_DblClick(Index As Integer)
    'Call objWinInfo.GridDblClick
    If grdssPruebaAsistencia(0).Columns(3).Text <> "" Then
        pVentanaPruebas grdssPruebaAsistencia(0).Columns(3).Text
    End If
End Sub


Private Sub grdSSPruebaAsistencia_GotFocus(Index As Integer)
Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdSSPruebaAsistencia_RowColChange(Index As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdSSPruebaAsistencia_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    ' Se carga la descripci�n del estado de la prueba
    If grdssPruebaAsistencia(0).Columns(13).Text <> "" Then
        grdssPruebaAsistencia(0).Columns(14).Text = fEstadoPrueba(grdssPruebaAsistencia(0).Columns(13).Text)
    End If
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    
    Select Case strFormName
        Case objAsistInfo.strName
            Select Case objAsistInfo.rdoCursor("sexo")
                Case 1
                    optPaciente(0).Value = True
                    optPaciente(1).Value = False
                Case 2
                    optPaciente(0).Value = False
                    optPaciente(1).Value = True
                Case Else
                    optPaciente(0).Value = False
                    optPaciente(1).Value = False
            End Select
    End Select

End Sub
Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub


Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub


Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub


Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub


Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub


Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As ComctlLib.Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabPac_MouseDown(intIndex As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabPac(intIndex), False, True)

End Sub


Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As ComctlLib.Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


Private Sub txtAsist_Change(Index As Integer)
Call objWinInfo.CtrlDataChange
End Sub


Private Sub txtAsist_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub


Private Sub txtAsist_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus

End Sub


