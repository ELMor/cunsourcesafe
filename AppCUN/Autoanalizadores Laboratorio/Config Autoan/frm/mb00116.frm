VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_Carpetas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Carpetas"
   ClientHeight    =   6855
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9750
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "mb00116.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6855
   ScaleWidth      =   9750
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9750
      _ExtentX        =   17198
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraUsers 
      Caption         =   "Usuarios"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3345
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   3120
      Width           =   9465
      Begin SSDataWidgets_B.SSDBGrid grdssUsers 
         Height          =   2940
         Index           =   0
         Left            =   180
         TabIndex        =   12
         Tag             =   "usuarios|Usuarios"
         Top             =   300
         Width           =   9195
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   16219
         _ExtentY        =   5186
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraCarp 
      Caption         =   "Carpetas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2565
      Index           =   0
      Left            =   90
      TabIndex        =   5
      Top             =   450
      Width           =   9465
      Begin TabDlg.SSTab tabCarp 
         Height          =   2025
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   3572
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00116.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtCarpetas(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtCarpetas(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtCarpetas(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "mb00116.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssCarp(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFF00&
            DataField       =   "CCARPETA"
            Height          =   285
            Index           =   0
            Left            =   1860
            MaxLength       =   3
            TabIndex        =   0
            Tag             =   "C�digo|C�digo de la carpeta"
            Top             =   240
            Width           =   495
         End
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFF00&
            DataField       =   "DESIGNACION"
            Height          =   285
            Index           =   1
            Left            =   1860
            MaxLength       =   50
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n de la carpeta"
            Top             =   720
            Width           =   3375
         End
         Begin VB.TextBox txtCarpetas 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CDPTOSECC"
            Height          =   285
            Index           =   2
            Left            =   1860
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Secci�n|C�digo de la secci�n"
            Top             =   1200
            Width           =   915
         End
         Begin SSDataWidgets_B.SSDBGrid grdssCarp 
            Height          =   1770
            Index           =   0
            Left            =   -74910
            TabIndex        =   4
            Top             =   90
            Width           =   8700
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15346
            _ExtentY        =   3122
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   400
            TabIndex        =   11
            Top             =   240
            Width           =   1300
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   400
            TabIndex        =   10
            Top             =   720
            Width           =   1300
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Secci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   400
            TabIndex        =   9
            Top             =   1200
            Width           =   1300
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   6570
      Width           =   9750
      _ExtentX        =   17198
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Carpetas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm

Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    Set .objFormContainer = fraCarp(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabCarp(0)
    Set .grdGrid = grdssCarp(0)
    .intAllowance = cwAllowReadOnly
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "CARPETAS"
    .strWhere = "CARPETAS.cDptoSecc = " & departamento 'nombre de tabla para reports
    Call .FormAddOrderField("CCARPETA", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Carpetas")
    Call .FormAddFilterWhere(strKey, "CARPETAS.CCARPETA", "C�digo", cwNumeric, 3) 'nombre de tabla para reports
    Call .FormAddFilterWhere(strKey, "DESIGNACION", "Designaci�n", cwString)
    
    Call .FormAddFilterOrder(strKey, "CARPETAS.CCARPETA", "C�digo") 'nombre de tabla para reports
    Call .FormAddFilterOrder(strKey, "DESIGNACION", "Designaci�n")
    
    Call .objPrinter.Add("MB00142", "Carpetas")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraUsers(0)
    Set .objFatherContainer = fraCarp(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssUsers(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB1600"

    Call .FormAddOrderField("MB1600.CUSER", cwAscending) 'nombre tabla para report
    Call .FormAddRelation("CCARPETA", txtCarpetas(0))
    
    'Call .objPrinter.Add("MB00142", "Carpetas") 'Columna ambigua
  
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo, "Carpeta", "CCARPETA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Usuario", "CUSER", cwNumeric, 4)
    
    Call .FormCreateInfo(objMasterInfo)
    .CtrlGetInfo(txtCarpetas(0)).blnReadOnly = True
    .CtrlGetInfo(txtCarpetas(1)).blnReadOnly = True
    .CtrlGetInfo(txtCarpetas(2)).blnReadOnly = True
    .CtrlGetInfo(txtCarpetas(0)).blnInFind = True
    .CtrlGetInfo(txtCarpetas(1)).blnInFind = True
    
    Call .FormChangeColor(objMultiInfo)
    grdssUsers(0).Columns(3).Visible = False
    
    SQL = "SELECT cUser,nombre||' '||Apellido1||' '||Apellido2 as nomUser"
    SQL = SQL & " FROM users "
    SQL = SQL & " WHERE cUser IN "
    SQL = SQL & "       (SELECT cUser FROM usersPerfil"
    SQL = SQL & "       WHERE cDptoSecc = " & departamento
    SQL = SQL & "       )"
    SQL = SQL & " ORDER BY nomUser"
    .CtrlGetInfo(grdssUsers(0).Columns(4)).strSQL = SQL
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdssUsers(0).Columns(4).Width = 4000
    
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraCarp_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraCarp(intIndex), False, True)
End Sub

Private Sub fraUsers_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraUsers(intIndex), False, True)
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objMasterInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
        
 Case objMultiInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
        
  End Select
End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabCarp_MouseDown(intIndex As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabCarp(intIndex), False, True)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub grdssCarp_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssCarp_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssCarp_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssCarp_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssUsers_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssUsers_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssUsers_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssUsers_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtCarpetas_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtCarpetas_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtCarpetas_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
