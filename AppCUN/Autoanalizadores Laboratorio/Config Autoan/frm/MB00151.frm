VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmG_Seccion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Selecci�n de Secci�n"
   ClientHeight    =   4200
   ClientLeft      =   75
   ClientTop       =   360
   ClientWidth     =   4905
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   4905
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraSeccion 
      Caption         =   "Departamentos/Secciones"
      Height          =   3495
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   4695
      Begin SSDataWidgets_B.SSDBGrid grdssSeccion 
         Height          =   3195
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Tag             =   "Secciones|Secciones de Microbiolog�a"
         Top             =   240
         Width           =   4515
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   4
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "CODIGO"
         Columns(0).Name =   "CODIGO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4419
         Columns(1).Caption=   "Designacion"
         Columns(1).Name =   "Designacion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3519
         Columns(2).Caption=   "Perfil"
         Columns(2).Name =   "designacionPerfil"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "codigoPerfil"
         Columns(3).Name =   "codigoPerfil"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         UseDefaults     =   0   'False
         _ExtentX        =   7964
         _ExtentY        =   5636
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   390
      Left            =   3600
      TabIndex        =   1
      Top             =   3720
      Width           =   1140
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      Height          =   390
      Left            =   2280
      TabIndex        =   0
      Top             =   3720
      Width           =   1140
   End
End
Attribute VB_Name = "frmG_Seccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private entrada As Boolean
Private Function fUsuarioAccesoSecciones()
    Dim SQL As String
    Dim qryUser As rdoQuery
    Dim rsUser As rdoResultset
    Dim UName As String
    
    ' Lectura del Usuario del ordenador.
    UName = Trim(Environ$("UserName"))
    If UName = "" Then MsgBox "Necesita Vd. estar conectado a la Red.": End
    ' Lectura del c�digo del usuario.
    SQL = "SELECT cUser FROM Users WHERE UPPER(Login)= ?"
    Set qryUser = objApp.rdoConnect.CreateQuery("codUser", SQL)
    qryUser.RowsetSize = 1
    qryUser(0) = UCase(UName)
    Set rsUser = qryUser.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsUser.EOF = True Then
        MsgBox "No esta Vd. dado de alta en el sistema." + Chr$(13) + "Consulte con el Administrador.", vbExclamation, "Laboratorio de Microbiolog�a"
        End
    End If
    strUsuario = rsUser(0)
    rsUser.Close
    qryUser.Close
    fUsuarioAccesoSecciones = strUsuario

End Function

Private Sub cmdCancel_Click()
    'SelectSession = False
    'Me.Hide
    Call objPipe.PipeSet("SelectSession", False)
    Unload Me
End Sub

Private Sub cmdOK_Click()
'    SelectSession = True
    If grdssSeccion(0).Rows > 0 Then
        Call objPipe.PipeSet("SelectSession", True)
        departamento = grdssSeccion(0).Columns(0).Text
        letraNumRef = fLetraNumRef(departamento, strDesignacionAntibiograma)
        strPerfil = grdssSeccion(0).Columns(3).Text
        frmPrincipal.Caption = "Gesti�n de Laboratorio de Microbiolog�a (" & grdssSeccion(0).Columns(1).Text & ")"
        Call pAccesoMantenimientoSeccion
        Call pAccesoConsultaResultados
        Call pAccesoExplotacion
        'Me.Hide
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String
    Dim qrySec As rdoQuery
    Dim rsSec As rdoResultset
    Dim SQL As String
    Dim strUsuario As String
  
    Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
    pFormatearGrid grdssSeccion(0)
  
'    sql = "SELECT cDptoSecc, designacion FROM dptoSecc"
'    sql = sql & " WHERE cDptoDependiente = ?"
'    sql = sql & " ORDER BY designacion"
    ' Seguridad
    strUsuario = fUsuarioAccesoSecciones
    
    SQL = "SELECT ds.cDptoSecc, ds.designacion, p.designacion as designacionPerfil, p.cPerfil"
    SQL = SQL & " FROM DptoSecc ds, UsersPerfil up, Perfiles p, Users u "
    SQL = SQL & " Where  "
    SQL = SQL & "   up.cDptoSecc=ds.cDptoSecc and "
    SQL = SQL & "   p.cPerfil=up.cPerfil and "
    SQL = SQL & "   u.cUser=up.cUser AND "
    SQL = SQL & "   up.cUser= ?"
    SQL = SQL & " ORDER BY DS.CDPTODEPENDIENTE "
            
    Set qrySec = objApp.rdoConnect.CreateQuery("Secci�n", SQL)
    qrySec(0) = strUsuario
    
    Set rsSec = qrySec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsSec.EOF
        grdssSeccion(0).AddItem rsSec!cDptoSecc & Chr$(9) & rsSec!Designacion & Chr$(9) & rsSec!DesignacionPerfil & Chr$(9) & rsSec!cPerfil
        rsSec.MoveNext
    Loop
    rsSec.Close
    qrySec.Close

  
    Screen.MousePointer = vbDefault 'Call objApp.SplashOff

End Sub

Private Sub grdssSeccion_DblClick(Index As Integer)
'    cmdOK_Click
End Sub

