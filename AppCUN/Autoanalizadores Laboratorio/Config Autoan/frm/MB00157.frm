VERSION 5.00
Begin VB.Form frmG_BuscRef 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar N� Referencia"
   ClientHeight    =   1455
   ClientLeft      =   75
   ClientTop       =   360
   ClientWidth     =   4500
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1455
   ScaleWidth      =   4500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtAnyo 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1320
      MaxLength       =   4
      TabIndex        =   4
      Top             =   780
      Width           =   570
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2700
      TabIndex        =   3
      Top             =   780
      Width           =   1215
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   2700
      TabIndex        =   2
      Top             =   240
      Width           =   1215
   End
   Begin VB.TextBox txtNRef 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1320
      MaxLength       =   7
      TabIndex        =   0
      Top             =   300
      Width           =   870
   End
   Begin VB.Label lblLabel1 
      Alignment       =   1  'Right Justify
      Caption         =   "A�o:"
      Height          =   255
      Index           =   0
      Left            =   600
      TabIndex        =   5
      Top             =   780
      Width           =   600
   End
   Begin VB.Label lblLabel1 
      Alignment       =   1  'Right Justify
      Caption         =   "N� Ref.:"
      Height          =   255
      Index           =   1
      Left            =   300
      TabIndex        =   1
      Top             =   300
      Width           =   900
   End
End
Attribute VB_Name = "frmG_BuscRef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

    
Private Sub cmdBuscar_Click()
    Dim Anyo As String
    Dim nRefBusc As String

    ' chequeos previos
    If Val(txtNRef.Text) = 0 Then
        MsgBox "No se ha introducido el N� de Ref.", vbExclamation, "Buscar N� de Ref."
        txtNRef.Text = ""
        txtNRef.SetFocus
        Exit Sub
    End If
    
    If Val(txtAnyo.Text) = 0 Then
        Anyo = Format(fFechaActual, "yyyy")
    Else
        If Len(CStr(Val(txtAnyo.Text))) < 4 Then
            MsgBox "El a�o introducido es incorrecto.", vbExclamation, "Buscar N� de Ref."
            txtAnyo.Text = ""
            txtAnyo.SetFocus
            Exit Sub
        Else
            Anyo = txtAnyo.Text
        End If
    End If
    
    ' n� de referencia buscado
    nRefBusc = letraNumRef & Anyo & Left$("0000000", 7 - Len(txtNRef.Text)) & txtNRef.Text
    
    Call objPipe.PipeSet("BuscNRef", nRefBusc)
    Unload Me
    
End Sub
Private Sub cmdCancelar_Click()
    Call objPipe.PipeSet("BuscNRef", "")
    Unload Me
End Sub

Private Sub Form_Load()

    txtNRef.BackColor = objApp.objUserColor.lngMandatory
    
End Sub

Private Sub txtAnyo_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdBuscar_Click
    Case 27
      cmdCancelar_Click
    Case Is < 47, Is > 58
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
  End Select
End Sub


Private Sub txtNRef_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdBuscar_Click
    Case 27
      cmdCancelar_Click
    Case Is < 47, Is > 58
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
  End Select
End Sub

