VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmH_Calendar 
   Caption         =   "Calendario"
   ClientHeight    =   2985
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5055
   LinkTopic       =   "Form1"
   ScaleHeight     =   2985
   ScaleWidth      =   5055
   StartUpPosition =   2  'CenterScreen
   Begin SSCalendarWidgets_A.SSMonth ssmCalendar 
      Height          =   2835
      Left            =   120
      TabIndex        =   2
      Top             =   60
      Width           =   3435
      _Version        =   65537
      _ExtentX        =   6059
      _ExtentY        =   5001
      _StockProps     =   76
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      StartofWeek     =   2
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Cancelar"
      Height          =   435
      Index           =   1
      Left            =   3660
      TabIndex        =   1
      Top             =   2460
      Width           =   1275
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Aceptar"
      Height          =   435
      Index           =   0
      Left            =   3660
      TabIndex        =   0
      Top             =   1860
      Width           =   1275
   End
End
Attribute VB_Name = "frmH_Calendar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click(Index As Integer)

    If Index = 0 Then
        Call objPipe.PipeSet("MB_FechaActual", ssmCalendar.Date)
    End If
    Unload Me
    
End Sub


Private Sub Form_Load()
    
    If objPipe.PipeExist("MB_FechaActual") Then
        ssmCalendar.Date = objPipe.PipeGet("MB_FechaActual")
    Else
        ssmCalendar.Date = fFechaActual
    End If
    If objPipe.PipeExist("MB_FechaMinima") Then
        ssmCalendar.MinDate = objPipe.PipeGet("MB_FechaMinima")
    End If
    If objPipe.PipeExist("MB_FechaMaxima") Then
        ssmCalendar.MaxDate = objPipe.PipeGet("MB_FechaMaxima")
    End If
        
End Sub


