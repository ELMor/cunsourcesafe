Attribute VB_Name = "modClinitek"
Option Explicit
Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
  App.HelpFile = "f:\laborat\ayuda\" & App.EXEName & ".hlp"
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    fCargarColumnasGrid
    If Not .fInicializar(App, cteCLINITEK, , , , , , , , , , , , , True, True) Then End
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  'columnas del grid
  With objAutoAn
    .AgregarColumnaAGridMuestras cteMuestraFILA
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
End Sub

Public Sub MaskChange()
  objAutoAn.MaskChange
End Sub
