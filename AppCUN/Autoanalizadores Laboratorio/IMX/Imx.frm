VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "IMX"
   ClientHeight    =   480
   ClientLeft      =   2385
   ClientTop       =   2730
   ClientWidth     =   1410
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Imx.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   480
   ScaleWidth      =   1410
   Begin VsOcxLib.VideoSoftAwk VideoSoftAwk2 
      Left            =   480
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk VideoSoftAwk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables que guardan la informaci�n sobre la prueba y sus resultados para actualizar la base de datos
Dim cprueba As String               'c�digo de la prueba
Dim res1 As String                  'c�digo del primer resultado utilizado por el autoanalizador
Dim res2 As String                  'c�digo del segundo resultado utilizado por el autoanalizador

Private Sub IMX_Espera(mensaje As String)
'Identifica la llegada del caracter de comienzo de transmisi�n (comienzo del mensaje) y pasa al OBJAUTOAN.ESTADO de transmisi�n.

    On Error Resume Next

    If Right$(mensaje, 1) = "[" Then 'Llegada del car�cter de comienzo del mensaje
        objAutoAn.estado = constTRANSMISION
        panOBJAUTOAN.estado.Caption = LeerOBJAUTOAN.estado(objAutoAn.estado)
        frmPrincipal.Refresh
        mensaje = Right$(mensaje, 1)
    End If

End Sub

Private Sub IMX_Transmision(mensaje As String)

'                               ----- (�ltima revisi�n 08-03-96) -----
        

'Identifica el final de un archivo y procede a su lectura.
'Si no es el �ltimo archivo del mensaje, el sistema queda en disposici�n de aceptar nuevos archivos.
'Identifica tambi�n el final del mensaje o transmisi�n y vuelve a OBJAUTOAN.ESTADO de espera.

'                           **** DECLARACI�N DE VARIABLES ****

    Dim contador As Integer
    Dim filenum

    'On Error Resume Next

    If Right$(mensaje, 1) = "[" Then 'Todo lo que se transmita antes del comienzo de un archivo se elimina
        mensaje = Right$(mensaje, 1)
        Exit Sub
    End If

    If Len(mensaje) > 5 Then
        If Mid$(mensaje, Len(mensaje) - 5, 3) = "]" & Chr$(13) & Chr$(10) Then 'Se ha identifcado el final de un archivo
            Lectura_Datos mensaje 'Se procede a la lectura de los datos
            mensaje = ""
            Exit Sub
        End If
    End If
    
    If Right$(mensaje, 4) = "!!" & Chr$(13) & Chr$(10) Then 'Se ha identifcado el final del mensaje
        objAutoAn.estado = constESPERA
        panOBJAUTOAN.estado.Caption = LeerOBJAUTOAN.estado(objAutoAn.estado)
        frmPrincipal.Refresh
        mensaje = ""
        'Si todas las pruebas tienen resultados, se sale del programa.
        For contador = 1 To UBound(matrizpruebas, 2)
            If matrizpruebas(7, contador) <> "RE" Then
                Exit For
            End If
        Next contador
        If contador > UBound(matrizpruebas, 2) Then
            cmdSalir_Click
        End If
    End If

End Sub

Private Sub Lectura_Datos(Datos As String)
 
'                               ----- (�ltima revisi�n 08-03-96) -----


'                           ***** DECLARACI�N DE VARIABLES *****

    Dim snp1 As snapshot
    Dim contador1 As Integer
    Dim contador2 As Integer
    Dim sql As String
    Dim res As String
    Dim i As Integer

'                           ***** DESARROLLO *****

    awk1 = Datos
'awk1 = "[" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "DATE: 10/02/97" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "TIME: 12:52:17" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "TECH ID : _________" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "RGNT LOT: _________" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "SERIAL #:  3747" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "CRSL ID:" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "ASSAY 112 TACRO II    3C10 Revision 1" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "LOC CALIB   RATE" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "----------------" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "1    A 445.7" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "2    A 406.6" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "3    B 344.9" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "4    B 335.6" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "5    C 311.1" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "6    C 290.7" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "7    D 217.8" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "8    D 222#" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "9    E 177.7" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "10    E 159.6" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "11    F 132.5" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "12    F 122#" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "CALIB      ng/mL    AVGR    FITR    RERR" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "----------------------------------------" & Chr$(13) & Chr$(10)
'  awk1 = awk1 & "A           0.   426.2   426.2     0.0" & Chr$(13) & Chr$(10)
'  awk1 = awk1 & "B           3.   340.3   344.7    -4.4" & Chr$(13) & Chr$(10)
'  awk1 = awk1 & "C           6.   300.9   292.6     8.3" & Chr$(13) & Chr$(10)
'  awk1 = awk1 & "D          12.   219.9   224.3    -4.4" & Chr$(13) & Chr$(10)
'  awk1 = awk1 & "E          20.   168.7   169.0    -0.3" & Chr$(13) & Chr$(10)
'  awk1 = awk1 & "F          30.   127.3   126.2     1.1" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "RMSE = 3.693" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "CALIBRATION ACCEPTED" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "LOC    ID            ng/mL     RATE NOTE" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "----------------------------------------" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "13  CONTROL L          5.7    297.5" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "14  CONTROL M         11.6    227.8" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "15  CONTROL H         24.3    148.1" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "16  000000001         15.4    197.6" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "17  000000000          0.9    395.3" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "TESTS USED TO DATE :  33" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "]" & Chr$(13) & Chr$(10)
'awk1 = awk1 & "_" & Chr$(13) & Chr$(10)
'

    awk1.FS = Chr$(10)
    'En la matriz de resultados se almacena en (1,X) el c�digo de la muestra, en (2,X) el primer
    'resultado y en (3,X) el segundo resultado
    ReDim matrizresultados(1 To 3, 0 To 0)

    'Se procesan los datos
    For contador1 = 1 To awk1.NF
        'Se busca la prueba
        If Left$(awk1.F(contador1), 5) = "ASSAY" Then
            awk2 = awk1.F(contador1)
            awk2.FS = " "
            sql = "SELECT cPrueba FROM " & constPropietario & ".pruebasAutoanalizador WHERE "
            sql = sql & "cAutoanalizador = " & constIMX_FARM & " AND cPruebaAuto = '" & Trim$(awk2.F(2)) & "'"
            Set snp1 = dbCUN.CreateSnapshot(UCase(sql), DB_SQLPASSTHROUGH)
            If snp1.RecordCount > 0 Then
                cprueba = snp1("cPrueba")
            Else
                Exit Sub
            End If
            Exit For
        End If
    Next contador1

    'Se busca la posici�n de los resultados en el string de datos. La forma de hacerlo es un poco
    'por las bravas pero no se conoce otra forma mejor.
    For contador1 = awk1.NF To 1 Step -1
        If Left$(awk1.F(contador1), 10) = "----------" Then
            awk2 = awk1.F(contador1 - 1)
            res1 = awk2.F(awk2.NF - 2)
            res2 = awk2.F(awk2.NF - 1)
            For contador2 = contador1 + 1 To awk1.NF
                If awk1.F(contador2) = Chr$(13) Then
                    Exit For
                End If
                awk2 = awk1.F(contador2)
                ReDim Preserve matrizresultados(1 To 3, 0 To UBound(matrizresultados, 2) + 1)
                matrizresultados(1, UBound(matrizresultados, 2)) = Right$(awk2.F(2), 4)
                res = awk2.F(awk2.NF - 2)
                i = fFormatearNumero(res, ".", ",")
                matrizresultados(2, UBound(matrizresultados, 2)) = res
                res = Left$(awk2.F(awk2.NF - 1), Len(awk2.F(awk2.NF - 1)))
                If Asc(Right$(res, 1)) = 13 Then  'El awk devuelve tambi�n el <CR> que lo eliminamos
                    res = Left$(res, Len(res) - 1)
                End If
                i = fFormatearNumero(res, ".", ",")
                matrizresultados(3, UBound(matrizresultados, 2)) = res
            Next contador2
            Actualizar_BD
            Exit For
        End If
    Next contador1
    
End Sub

Private Sub Lectura_Protocolo(caracter As String)

'                               ----- (�ltima revisi�n 08-03-96) -----

'El sistema (ordenador que comunica con el autoanalizador) puede estar en uno de estos OBJAUTOAN.ESTADOs:
'   Espera: no se ha establecido todav�a la comunicaci�n.
'   Transmisi�n: el autoanalizador est� mandando datos al sistema


'                               ***** DECLARACI�N DE VARIABLES *****
    
    Static mensaje As String 'Contiene el mensaje que env�a el autoanalizador
    Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
    'pudendo ser modificada en �stas. Al ser modificada la copia, tambi�n deber� modificarse el original.
    'El hecho de utilizar esta copia se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas
    'por estar declarada como Static en esta subrutina


'                               ***** DESARROLLO *****

    'On Error Resume Next

    mensaje = mensaje & caracter
    CopiaMensaje = mensaje

    Select Case objAutoAn.estado
    Case constESPERA
        IMX_Espera CopiaMensaje
        mensaje = CopiaMensaje
    Case constTRANSMISION
        IMX_Transmision CopiaMensaje
        mensaje = CopiaMensaje
    End Select

End Sub

