VERSION 5.00
Begin VB.Form frmAvisos 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Avisos Hitachi 717"
   ClientHeight    =   2700
   ClientLeft      =   4335
   ClientTop       =   3045
   ClientWidth     =   4380
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2700
   ScaleWidth      =   4380
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   2835
      Left            =   0
      TabIndex        =   4
      Top             =   -60
      Visible         =   0   'False
      Width           =   4395
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   1500
         TabIndex        =   6
         Top             =   2340
         Width           =   1395
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         Height          =   2115
         Left            =   0
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   5
         Top             =   60
         Width           =   4395
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   2835
      Left            =   0
      TabIndex        =   0
      Top             =   -60
      Width           =   4395
      Begin VB.TextBox txtAvisos 
         Appearance      =   0  'Flat
         Height          =   2115
         Left            =   0
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   3
         Top             =   60
         Width           =   4395
      End
      Begin VB.CommandButton cmdCerrar 
         Appearance      =   0  'Flat
         Caption         =   "&Cerrar"
         Height          =   375
         Left            =   2640
         TabIndex        =   2
         Top             =   2340
         Width           =   1395
      End
      Begin VB.CommandButton cmdBorrrar 
         Appearance      =   0  'Flat
         Caption         =   "&Borrar"
         Height          =   375
         Left            =   360
         TabIndex        =   1
         Top             =   2340
         Width           =   1395
      End
   End
End
Attribute VB_Name = "frmAvisos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBorrrar_Click()
  txtAvisos.Text = ""
End Sub

Private Sub cmdCerrar_Click()
  frmAvisos.Tag = ""
  Unload frmAvisos
End Sub

Private Sub Command1_Click()
  If txtAvisos.Text <> "" Then
    Frame2.Visible = False
    Frame1.Visible = True
  Else
    Unload Me
  End If
End Sub

Private Sub Form_Load()
    'ubicación de la pantalla de avisos
    Move (Screen.Width - frmAvisos.Width), (frmPrincipal.Top + frmPrincipal.Height - frmAvisos.Height)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmAvisos.Tag = "Oculta"
    DoEvents
End Sub

