VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsConst"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'Constantes de estado del sistema
Public constESTADOSISTESPERA As Integer
Public constESTADOSISTSOLICITUD As Integer
Public constESTADOSISTESCLAVO As Integer
Public constESTADOSISTCONFIRMACION As Integer
Public constESTADOSISTMASTER As Integer
Public constESTADOSISTTRANSMISION As Integer
'Constantes de los procesos de las pruebas
Public constPROCESOSOLICITUD As Integer
Public constPROCESOREALIZACION As Integer
Public constPROCESOVALIDACION As Integer
'Constantes de los estados de las pruebas
Public constPRUEBASOLICITADA As Integer
Public constPRUEBAIMPRESA As Integer
Public constPRUEBAEXTRAIDA As Integer
Public constPRUEBASOLICITUDREALIZ As Integer
Public constPRUEBAREALIZANDO As Integer
Public constPRUEBARESULTADO As Integer
Public constPRUEBARESULTANALIZ As Integer
Public constPRUEBAVALIDADA As Integer
Public constPRUEBAINSERTADA As Integer
Public constPRUEBAENVIOPROV As Integer
Public constPRUEBAENVIADA As Integer
Public constPRUEBAREPETIDA As Integer
Public constPRUEBAANULADA As Integer
'Constantes del estado de las muestras
Public constMUESTRAPENDIENTE As Integer
Public constMUESTRAEXTRAIDA As Integer
Public constMUESTRAFINALIZADA As Integer
Public constMUESTRAANULADAMUESTRA As Integer
Public constMUESTRAANULADAPRUEBA As Integer
Public constMUESTRAGUARDADA As Integer
'Constantes de los estados de los resultados
Public constRESULTADOINTRODUCIDO As Integer
Public constRESULTADOVALIDADO As Integer
Public constRESULTADOREPETIDO As Integer
Public constRESULTADOANULADO As Integer
'Constantes del estado de las listas de trabajo
Public constLISTAPREALIZANDO As Integer
Public constLISTAPRESULTADO As Integer
Public constLISTAPVALIDTECNI As Integer
Public constLISTAPRESULTANAL As Integer
'C�digos de los autoanalizadores
Public constAUTOELECTRA As Integer
Public constAUTOKOAGULAB As Integer
Public constAUTOAGGRECORDER As Integer
Public constAUTOCPA_COULTER As Integer
Public constAUTOCOULTER_T540 As Integer
Public constAUTOCOULTER_STKS As Integer
Public constAUTOTECHNICON As Integer
Public constAUTOCOBAS_VEGA As Integer
Public constAUTOVES_MATIC As Integer
Public constAUTOBECKMAN_CX3 As Integer
Public constAUTOHITACHI_704 As Integer
Public constAUTOHITACHI_717 As Integer
Public constAUTOCOBAS_FARA As Integer
Public constAUTOCLINITEK As Integer
Public constAUTOLABORATORIO_REF As Integer
Public constAUTOCOBAS_CORE_MARC As Integer
Public constAUTOAXSYM As Integer
Public constAUTOBECKMAN_APPRAISE As Integer
Public constAUTOIMX_PRO As Integer
Public constAUTOBECKMAN_ARRAY As Integer
Public constAUTOVITEK As Integer
Public constAUTOBACK_ALERT As Integer
Public constAUTOCOBAS_CORE_SER As Integer
Public constAUTOIMX_SER As Integer
Public constAUTOCERES_900 As Integer
Public constAUTOEPICS_XL As Integer
Public constAUTOCAP As Integer
Public constAUTOTDX As Integer
Public constAUTOTDX_FLX As Integer
Public constAUTOIMX_FARM As Integer
Public constAUTOBNA_BEHRING As Integer
Public constAUTOSTA_COMPACT As Integer
Public constAUTOTDX3 As Integer
Public constAUTOTDX4 As Integer
Public constAUTOREFERENCE As Integer
'Constantes de estado de las pruebas en el autoanalizador
Public constESTADOPRAUTOSINPROGRAMAR As String
Public constESTADOPRAUTOINTENTOPROGRAMAR As String
Public constESTADOPRAUTOACEPTADA As String
Public constESTADOPRAUTONOACEPTADA As String
Public constESTADOPRAUTOREALIZADA As String
Public constESTADOMUAUTOAREPETIR As String 'Estos 4 estados son para el CX3
Public constESTADOMUAUTOINTENTOBORRADO As String
Public constESTADOMUAUTOBORRADA As String
Public constESTADOMUAUTONOBORRADA As String
Public constESTADOMUAUTORECIBIENDORESULTADOS As String
Public constESTADOMUAUTOLEIDA As String
Public constESTADOMUPREPROGRAMACION As String

Public ConstCABIDMUESTRA As Integer
Public ConstCABCODIGOACTUACION As Integer
Public ConstCABDESCRIACTUACION As Integer
Public ConstCABNUMEROACTUACION As Integer
Public ConstCABNUMREPETICION As Integer
Public ConstCABCODACTAUTO As Integer

Public ConstCABPOSICION As Integer
Public ConstCABSECCION As Integer
Public ConstCABDISCO As Integer
Public ConstCABCOPA As Integer
Public ConstCABBANDEJA As Integer
Public ConstCABDILUCION As Integer
Public ConstCABDESCRIRESULTADO As Integer
Public ConstCABVALRESULTADO As Integer

Public ConstCABMuestraFILA As Integer
Public ConstCABMuestraCODMUESTRA As String
Public ConstCABMuestraREPETICION As Integer
Public ConstCABMuestraURGENTE As Integer
Public ConstCABMuestraESTADO As String
Public ConstCABMuestraPROPERTY1 As String
Public ConstCABMuestraPROPERTY2 As String
Public ConstCABMuestraPROPERTY3 As String
Public ConstCABPruebaCODMUESTRA As String
Public ConstCABPruebaREPETICION As Integer
Public ConstCABPruebaURGENTE As Integer
Public ConstCABPruebaESTADO As String
Public ConstCABPruebaPROPERTY1 As String
Public ConstCABPruebaPROPERTY2 As String
Public ConstCABResultadoESTADO As String
Public ConstCABResultadoPROPERTY1 As String
Public ConstCABResultadoPROPERTY2 As String

Private Sub Class_Initialize()
  constLISTAPREALIZANDO = cteLISTAPREALIZANDO
  constLISTAPRESULTANAL = cteLISTAPRESULTANAL
  constLISTAPRESULTADO = cteLISTAPRESULTADO
  constLISTAPVALIDTECNI = cteLISTAPVALIDTECNI
  
  constESTADOSISTESPERA = cteESPERA
  constESTADOSISTSOLICITUD = cteSOLICITUD
  constESTADOSISTESCLAVO = cteESCLAVO
  constESTADOSISTCONFIRMACION = cteCONFIRMACION
  constESTADOSISTMASTER = cteMASTER
  constESTADOSISTTRANSMISION = cteTRANSMISION
  constESTADOPRAUTOSINPROGRAMAR = cteSINPROGRAMAR
  constESTADOPRAUTOINTENTOPROGRAMAR = cteINTENTOPROGRAMAR
  constESTADOPRAUTOACEPTADA = cteACEPTADA
  constESTADOPRAUTONOACEPTADA = cteNOACEPTADA
  constESTADOPRAUTOREALIZADA = cteREALIZADA
  constESTADOMUAUTOAREPETIR = cteAREPETIR 'Estos 4 estados son para el CX3
  constESTADOMUAUTOINTENTOBORRADO = cteINTENTOBORRADO
  constESTADOMUAUTOBORRADA = cteBORRADA
  constESTADOMUAUTONOBORRADA = cteNOBORRADA
  constESTADOMUAUTORECIBIENDORESULTADOS = cteRECIBIENDORESULTADOS
  constESTADOMUAUTOLEIDA = cteLEIDA
  constESTADOMUPREPROGRAMACION = ctePREPROGRAMACION
  
  constPROCESOSOLICITUD = ctePROCESOSOLICITUD
  constPROCESOREALIZACION = ctePROCESOREALIZACION
  constPROCESOVALIDACION = ctePROCESOVALIDACION
  
  constPRUEBASOLICITADA = ctePRUEBASOLICITADA
  constPRUEBAIMPRESA = ctePRUEBAIMPRESA
  constPRUEBAEXTRAIDA = ctePRUEBAEXTRAIDA
  constPRUEBASOLICITUDREALIZ = ctePRUEBASOLICITUDREALIZ
  constPRUEBAREALIZANDO = ctePRUEBAREALIZANDO
  constPRUEBARESULTADO = ctePRUEBARESULTADO
  constPRUEBARESULTANALIZ = ctePRUEBARESULTANALIZ
  constPRUEBAVALIDADA = ctePRUEBAVALIDADA
  constPRUEBAINSERTADA = ctePRUEBAINSERTADA
  constPRUEBAENVIOPROV = ctePRUEBAENVIOPROV
  constPRUEBAENVIADA = ctePRUEBAENVIADA
  constPRUEBAREPETIDA = ctePRUEBAREPETIDA
  constPRUEBAANULADA = ctePRUEBAANULADA
  
  constMUESTRAPENDIENTE = cteMUESTRAPENDIENTE
  constMUESTRAEXTRAIDA = cteMUESTRAEXTRAIDA
  constMUESTRAFINALIZADA = cteMUESTRAFINALIZADA
  constMUESTRAANULADAMUESTRA = cteMUESTRAANULADAMUESTRA
  constMUESTRAANULADAPRUEBA = cteMUESTRAANULADAPRUEBA
  constMUESTRAGUARDADA = cteMUESTRAGUARDADA
  
  constRESULTADOINTRODUCIDO = cteRESULTADOINTRODUCIDO
  constRESULTADOVALIDADO = cteRESULTADOVALIDADO
  constRESULTADOREPETIDO = cteRESULTADOREPETIDO
  constRESULTADOANULADO = cteRESULTADOANULADO
  
  constAUTOELECTRA = cteELECTRA
  constAUTOKOAGULAB = cteKOAGULAB
  constAUTOAGGRECORDER = cteAGGRECORDER
  constAUTOCPA_COULTER = cteCPA_COULTER
  constAUTOCOULTER_T540 = cteCOULTER_T540
  constAUTOCOULTER_STKS = cteCOULTER_STKS
  constAUTOTECHNICON = cteTECHNICON
  constAUTOCOBAS_VEGA = cteCOBAS_VEGA
  constAUTOVES_MATIC = cteVES_MATIC
  constAUTOBECKMAN_CX3 = cteBECKMAN_CX3
  constAUTOHITACHI_704 = cteHITACHI_704
  constAUTOHITACHI_717 = cteHITACHI_717
  constAUTOCOBAS_FARA = cteCOBAS_FARA
  constAUTOCLINITEK = cteCLINITEK
  constAUTOLABORATORIO_REF = cteLABORATORIO_REF
  constAUTOCOBAS_CORE_MARC = cteCOBAS_CORE_MARC
  constAUTOAXSYM = cteAXSYM
  constAUTOBECKMAN_APPRAISE = cteBECKMAN_APPRAISE
  constAUTOIMX_PRO = cteIMX_PRO
  constAUTOBECKMAN_ARRAY = cteBECKMAN_ARRAY
  constAUTOVITEK = cteVITEK
  constAUTOBACK_ALERT = cteBACK_ALERT
  constAUTOCOBAS_CORE_SER = cteCOBAS_CORE_SER
  constAUTOIMX_SER = cteIMX_SER
  constAUTOCERES_900 = cteCERES_900
  constAUTOEPICS_XL = cteEPICS_XL
  constAUTOCAP = cteCAP
  constAUTOTDX = cteTDX
  constAUTOTDX_FLX = cteTDX_FLX
  constAUTOIMX_FARM = cteIMX_FARM
  constAUTOBNA_BEHRING = cteBNA_BEHRING
  constAUTOSTA_COMPACT = cteSTA_COMPACT
  constAUTOTDX3 = cteTDX3
  constAUTOTDX4 = cteTDX4
  constAUTOREFERENCE = cteREFERENCE
  
  
  ConstCABIDMUESTRA = cteIDMUESTRA
  ConstCABCODIGOACTUACION = cteCODIGOACTUACION
  ConstCABDESCRIACTUACION = cteDESCRIACTUACION
  ConstCABNUMEROACTUACION = cteNUMEROACTUACION
  ConstCABNUMREPETICION = cteNUMREPETICION
  ConstCABCODACTAUTO = cteCODACTAUTO
  ConstCABPOSICION = ctePOSICION
  ConstCABSECCION = cteSECCION
  ConstCABDISCO = cteDISCO
  ConstCABCOPA = cteCOPA
  ConstCABBANDEJA = cteBANDEJA
  ConstCABDILUCION = cteDILUCION
  ConstCABDESCRIRESULTADO = cteDESCRIRESULTADO
  ConstCABVALRESULTADO = cteVALRESULTADO
  
  ConstCABMuestraFILA = cteMuestraFILA
  ConstCABMuestraCODMUESTRA = cteMuestraCODMUESTRA
  ConstCABMuestraREPETICION = cteMuestraREPETICION
  ConstCABMuestraURGENTE = cteMuestraURGENTE
  ConstCABMuestraESTADO = cteMuestraESTADO
  ConstCABMuestraPROPERTY1 = cteMuestraPROPERTY1
  ConstCABMuestraPROPERTY2 = cteMuestraPROPERTY2
  ConstCABMuestraPROPERTY3 = cteMuestraPROPERTY3
  
  ConstCABPruebaFILA = ctePruebaFILA
  ConstCABPruebaCODMUESTRA = ctePruebaCODMUESTRA
  ConstCABPruebaREPETICION = ctePruebaREPETICION
  ConstCABPruebaPROPERTY1 = ctePruebaPROPERTY1
  ConstCABPruebaPROPERTY2 = ctePruebaPROPERTY2
  ConstCABPruebaURGENTE = ctePruebaURGENTE
  
  ConstCABResultadoESTADO = cteResultadoESTADO
  ConstCABResultadoPROPERTY1 = cteResultadoPROPERTY1
  ConstCABResultadoPROPERTY2 = cteResultadoPROPERTY2
  
End Sub
