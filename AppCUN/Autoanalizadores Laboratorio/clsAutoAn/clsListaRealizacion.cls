VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsListaRealizacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
'variables locales que contienen valores de propiedad
Private mvarEstado As constEstadoLista 'copia local
Public strResponsableLista As String
Public colActuaciones As Collection
Private MiLngNumero

Public Property Let lngNumero(val As Long)
'Obtiene el responsable de la emisión de una lista de trabajo
Dim Sql As String, Rs As rdoResultset, Qry As rdoQuery
On Error GoTo AutoAnError
    
  MiLngNumero = val
  Sql = "SELECT responsable FROM listaRealizacion WHERE cListaRealizacion = ?"
  Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
  Qry(0) = val
  Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
  If Not Rs.EOF Then
    strResponsableLista = IIf(IsNull(Rs(0)), 0, Rs(0))
  End If
  Rs.Close
  Qry.Close
  Exit Property
  
AutoAnError:
    Call objError.InternalError(Me, "fResponsableListaTrabajo", "lngListaTrabajo = " & val)
End Property

Public Property Get lngNumero() As Long
  lngNumero = MiLngNumero
End Property

Public Property Let Estado(ByVal vData As constEstadoLista)
'se usa cuando se asigna un valor a una propiedad, en el lado izquierdo de la asignación.
'Syntax: X.Estado = 5
  If mvarEstado <> vData Then
    mvarEstado = vData
    fListaRealizacionAEstado
  End If
End Property

Public Property Get Estado() As constEstadoLista
'se usa cuando se asigna un valor a una propiedad, en el lado derecho de la asignación.
'Syntax: Debug.Print X.Estado
    Estado = mvarEstado
End Property

Public Sub fListaRealizacionAEstado()
'Se cambia el estado de la lista de trabajo en la tabla listaRealizacion.
Dim Sql As String
Dim Qry As rdoQuery
On Error GoTo AutoAnError

  Sql = "UPDATE listaRealizacion " _
  & " SET estado = " & Estado _
  & " WHERE cListaRealizacion = ?"
  Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
  Qry(0) = MiLngNumero
  Qry.Execute
  Qry.Close
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "fListaRealizacionAEstado", "Sql= " & Sql)
End Sub

Private Sub Class_Initialize()
  Set colActuaciones = New Collection
End Sub
