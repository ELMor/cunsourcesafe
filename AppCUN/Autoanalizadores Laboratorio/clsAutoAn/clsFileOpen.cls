VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsFileOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Dim mcllDesc    As New Collection
Dim mcllPattern As New Collection

Public strFile  As String
Public strTitle As String


Public Sub AddFilter(ByVal strDesc As String, _
                     ByVal strPattern As String)
  Call mcllDesc.Add(strDesc)
  Call mcllPattern.Add(strPattern)
End Sub

Public Function GetFile(Optional ByVal mstrFile As String = "", _
                        Optional ByVal mstrTitle As String = "") As Boolean
  Dim strFilter As String
  Dim lngInd    As Long
  
  With objAutoan.dlgCommon
    .Flags = cdlOFNFileMustExist + _
             cdlOFNHideReadOnly + _
             cdlOFNPathMustExist
    
    If Not IsStrEmpty(mstrFile) Then
      strFile = mstrFile
    End If
    .filename = strFile
    
    If Not IsStrEmpty(mstrTitle) Then
      strTitle = mstrTitle
    End If
    .DialogTitle = strTitle
    
    For lngInd = 1 To mcllDesc.Count
      strFilter = strFilter & mcllDesc(lngInd) & "|" & mcllPattern(lngInd) & "|"
    Next
    .Filter = strFilter
    
    .FilterIndex = 1
    
    .CancelError = True
    
    On Error Resume Next
    
    Call .ShowOpen
    If Err.Number = 20477 Then
      .filename = ""
      Call .ShowOpen
    End If
    If Err.Number = 0 And Not IsStrEmpty(.filename) Then
      strFile = .filename
      GetFile = True
    End If
  End With
  
  ' dar tiempo a que se oculte la ventana
  DoEvents
End Function


