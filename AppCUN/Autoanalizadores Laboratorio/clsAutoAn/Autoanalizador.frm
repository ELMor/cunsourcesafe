VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "Ssdatb32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAutoan 
   Appearance      =   0  'Flat
   BackColor       =   &H00404000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   7620
   ClientLeft      =   1050
   ClientTop       =   1740
   ClientWidth     =   6105
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Autoanalizador.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7620
   ScaleWidth      =   6105
   Visible         =   0   'False
   Begin ComctlLib.Toolbar Toolbar1 
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8730
      _ExtentX        =   15399
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   10
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "b1"
            Object.ToolTipText     =   "Programar Lista en Autoanalizador"
            Object.Tag             =   "Programar Lista"
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "b2"
            Object.ToolTipText     =   "Pedir Resultados al Autoanalizador"
            Object.Tag             =   "Resultados"
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "b6"
            Object.ToolTipText     =   "Comprobaci�n de Resultados"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "b5"
            Object.ToolTipText     =   "Refrescar"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Enabled         =   0   'False
            Key             =   "b3"
            Object.ToolTipText     =   "Imprimir Lista Actual"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "b4"
            Object.ToolTipText     =   "Salir del Programa"
            Object.Tag             =   "Salir"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   435
         Left            =   2940
         TabIndex        =   3
         Top             =   0
         Width           =   3735
         Begin MSMask.MaskEdBox Mascara 
            Height          =   315
            Left            =   1560
            TabIndex        =   4
            Top             =   60
            Width           =   795
            _ExtentX        =   1402
            _ExtentY        =   556
            _Version        =   327681
            PromptChar      =   " "
         End
         Begin VB.Label Etiqueta 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0FF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000005&
            Height          =   330
            Left            =   2340
            TabIndex        =   6
            Top             =   30
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label TextoMascara 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "textomask"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   390
            TabIndex        =   5
            Top             =   60
            Visible         =   0   'False
            Width           =   1065
         End
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3240
      Top             =   6600
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5835
      Left            =   150
      TabIndex        =   2
      Top             =   540
      Width           =   5835
      _ExtentX        =   10292
      _ExtentY        =   10292
      _Version        =   327681
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      BackColor       =   4210688
      TabCaption(0)   =   "Muestras"
      TabPicture(0)   =   "Autoanalizador.frx":030A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "ssdgMuestras"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Pruebas-Resultados"
      TabPicture(1)   =   "Autoanalizador.frx":0326
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "ssdgPruebas"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin SSDataWidgets_B.SSDBGrid ssdgPruebas 
         Height          =   5295
         Left            =   120
         TabIndex        =   11
         Top             =   420
         Width           =   5595
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowColumnSwapping=   2
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   9869
         _ExtentY        =   9340
         _StockProps     =   79
         ForeColor       =   12582912
         BackColor       =   12632256
      End
      Begin SSDataWidgets_B.SSDBGrid ssdgMuestras 
         Height          =   5145
         Left            =   -74790
         TabIndex        =   10
         Top             =   510
         Width           =   5415
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   9551
         _ExtentY        =   9075
         _StockProps     =   79
         ForeColor       =   12582912
         BackColor       =   12632256
      End
   End
   Begin VB.Timer ParpadeoTrans 
      Left            =   2160
      Top             =   6690
   End
   Begin VB.Frame Frame10 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame10"
      Height          =   435
      Left            =   5550
      TabIndex        =   7
      Top             =   7170
      Width           =   645
      Begin VB.Shape Shape2 
         BackColor       =   &H00004000&
         BackStyle       =   1  'Opaque
         BorderStyle     =   6  'Inside Solid
         Height          =   315
         Left            =   360
         Shape           =   3  'Circle
         Top             =   0
         Width           =   195
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Trans."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   300
         TabIndex        =   9
         Top             =   240
         Width           =   435
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rec."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   0
         TabIndex        =   8
         Top             =   240
         Width           =   435
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00000040&
         BackStyle       =   1  'Opaque
         BorderStyle     =   6  'Inside Solid
         Height          =   315
         Left            =   30
         Shape           =   3  'Circle
         Top             =   0
         Width           =   195
      End
   End
   Begin VB.Timer Parpadeo 
      Enabled         =   0   'False
      Interval        =   350
      Left            =   1740
      Top             =   6690
   End
   Begin VB.Timer TimerNuevasPruebas 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   2580
      Top             =   6690
   End
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   7125
      Width           =   6105
      _ExtentX        =   10769
      _ExtentY        =   873
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   3
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   4472
            MinWidth        =   2646
            Text            =   "TRANSMISION"
            TextSave        =   "TRANSMISION"
            Key             =   "Comunicacion"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   5001
            MinWidth        =   3175
            Text            =   "Puerto 1 Abierto"
            TextSave        =   "Puerto 1 Abierto"
            Key             =   "Puerto"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel3 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Bevel           =   0
            Object.Width           =   1190
            MinWidth        =   1190
            Key             =   "Bombillas"
            Object.Tag             =   ""
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSCommLib.MSComm Comm1 
      Left            =   600
      Top             =   6540
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      DTREnable       =   -1  'True
      Handshaking     =   1
      InBufferSize    =   10240
      InputLen        =   1
      ParityReplace   =   0
      RThreshold      =   1
      RTSEnable       =   -1  'True
   End
   Begin MSCommLib.MSComm Comm2 
      Left            =   1170
      Top             =   6540
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      CommPort        =   2
      DTREnable       =   -1  'True
      Handshaking     =   1
      InBufferSize    =   10240
      InputLen        =   1
      ParityReplace   =   0
      RThreshold      =   1
      RTSEnable       =   -1  'True
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   30
      Top             =   6540
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   7
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Autoanalizador.frx":0342
            Key             =   "i1"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Autoanalizador.frx":065C
            Key             =   "i2"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Autoanalizador.frx":0976
            Key             =   "i3"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Autoanalizador.frx":0C90
            Key             =   "i4"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Autoanalizador.frx":0E6A
            Key             =   "i5"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Autoanalizador.frx":1184
            Key             =   "i6"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Autoanalizador.frx":149E
            Key             =   "i7"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArchivo 
      Caption         =   "&Archivo"
      Begin VB.Menu mnuImprimir 
         Caption         =   "&Imprimir"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSalir 
         Caption         =   "&Salir"
      End
   End
   Begin VB.Menu mnuAutoanalizador 
      Caption         =   "&Autoanalizador"
      Begin VB.Menu mnuProgramar 
         Caption         =   "&Programar"
      End
      Begin VB.Menu mnuResultados 
         Caption         =   "&Resultados"
      End
   End
   Begin VB.Menu mnuCambioPrueba 
      Caption         =   "&Prueba"
      Visible         =   0   'False
      Begin VB.Menu mnuCambioEstadoPrueba 
         Caption         =   "Cambio de &Estado"
         Begin VB.Menu mnuItemEstadoPrueba 
            Caption         =   "-"
            Index           =   1
         End
      End
      Begin VB.Menu mnusep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCambioCarpetaPrueba 
         Caption         =   "Cambio de &Carpeta"
         Enabled         =   0   'False
         Begin VB.Menu mnuItemCarpetaPrueba 
            Caption         =   "-"
            Index           =   1
         End
      End
   End
   Begin VB.Menu mnuCambioMuestra 
      Caption         =   "&Muestra"
      Visible         =   0   'False
      Begin VB.Menu mnuCambioEstadoMuestra 
         Caption         =   "Cambio de &Estado"
         Begin VB.Menu mnuItemEstadoMuestra 
            Caption         =   "-"
            Index           =   1
         End
      End
      Begin VB.Menu mnusep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCambioCarpetaMuestra 
         Caption         =   "Cambio de &Carpeta"
         Enabled         =   0   'False
         Begin VB.Menu mnuItemCarpetaMuestra 
            Caption         =   "-"
            Index           =   1
         End
      End
   End
   Begin VB.Menu mnuConfiguracion 
      Caption         =   "&Configuraci�n"
      Begin VB.Menu mnuAutoDescarga 
         Caption         =   "&Autodescarga de Resultados"
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&Ayuda"
      Begin VB.Menu mnuContenido 
         Caption         =   "&Contenido"
      End
      Begin VB.Menu mnusep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAcerca 
         Caption         =   "&Acerca de..."
      End
   End
End
Attribute VB_Name = "frmAutoan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Public ColMuestras As Collection
'Public ColCabeceraMuestras As Collection
Public Enum Estilos
  Normal = 1
  FRango = 7
  fallo = 30
  FalloB = 31
  NormalB = 9
  Urgente = 2
  FRangoB = 15
  Repetida = 3
  UrgenteB = 10
  Resultado = 5
  RepetidaB = 11
  ResultadoB = 13
  SinProgramar = 20
  UrgenteFRango = 8
  SinProgramarB = 21
  UrgenteFRangoB = 16
  UrgenteRepetida = 4
  UrgenteResultado = 6
  UrgenteRepetidaB = 12
  UrgenteResultadoB = 14
End Enum
Public Enum CtesEstilos
  EstiloRepetida = 1
  EstiloFRango = 2
  EstiloAceptada = 4
  EstiloSinProgramar = 8
  EstiloARepetir = 16
  EstiloResultado = 32
  EstiloUrgente = 64
End Enum
Dim UltimaFila As Integer

Private Sub Comm1_OnComm()
Dim ERMsg$
Dim caracter As String
Static strTextoLog As String
On Error GoTo Autoanerror

  'Se selecciona el evento que ocurre en el puerto
  With Comm1
    Select Case .CommEvent
      'Si se produce una entrada se procede a su lectura
      'Se lee cada caracter por separado
      Case comEvReceive
        .RThreshold = 0
        'para iluminar la bombillita de recepci�n
        With Parpadeo
          If .Enabled Then .Enabled = False _
          Else Shape1.BackColor = &HFF& 'ya esta iluminada y si no rojo claro
        End With
        Do While .InBufferCount
          DoEvents
          caracter = .Input
          TextoLog = TextoLog & TransfAsciiString(caracter)
          If Asc(caracter) = 128 Then Exit Do   'Para salir del bucle cuando se cierra la comunicaci�n
          Formulario.Lectura_Protocolo caracter
        Loop
        Parpadeo.Enabled = True
'        If TextoLog <> "" Then objAutoan.EscribirLogComm ""
        .RThreshold = 1
      Case comEventRxOver
        ERMsg$ = "Buffer de Entrada de Datos Lleno."
      Case comEventTxFull
        ERMsg$ = "Buffer de Salida de Datos Lleno."
    End Select
  End With
  If Len(ERMsg$) Then
    MsgBox ERMsg$, vbExclamation, Me.Caption
    ERMsg$ = ""
  End If
  Exit Sub
  
Autoanerror:
  Call objError.InternalError(Me, "Comm1_OnComm", "Caracter" & TransfAsciiString(caracter))
End Sub

Private Sub Form_Activate()
'  Me.Refresh
End Sub

Private Sub Form_DblClick()
On Error Resume Next
  If objAutoan.blnDesign Then
    objAutoan.frmFormulario.probar
  End If
End Sub

Private Sub Form_Load()
  'Posicionamiento de pantalla
  Call Move(0, (Screen.Height - Me.Height) / 2)
  AgregarEstilos ssdgMuestras
  AgregarEstilos ssdgPruebas
  With Toolbar1
    Set .ImageList = ImageList1
    .Buttons(ButtonProgram).Image = IconProgram
    .Buttons(ButtonResults).Image = IconResults
    .Buttons(ButtonPrint).Image = IconPrint
    .Buttons(ButtonExit).Image = IconExit
    .Buttons(ButtonRefresh).Image = IconRefresh
    .Buttons(ButtonComprobar).Image = IconComprobar
  End With
End Sub

Private Sub AgregarEstilosbak(Grid As SSDBGrid)
On Error GoTo Autoanerror
  
  AddStyle Grid, Normal, vbBlue
  AddStyle Grid, SinProgramar, vbBlack
  AddStyle Grid, Urgente, vbRed
  AddStyle Grid, Repetida, vbBlue, vbYellow
  AddStyle Grid, UrgenteRepetida, vbRed, vbYellow
  AddStyle Grid, Resultado, vbBlue, vbCyan
  AddStyle Grid, UrgenteResultado, vbRed, vbCyan
  AddStyle Grid, FRango, vbWhite, vbRed
  AddStyle Grid, UrgenteFRango, vbWhite, vbRed
  AddStyle Grid, fallo, vbBlue, vbCyan
  
  AddStyle Grid, NormalB, vbBlue, , True
  AddStyle Grid, SinProgramarB, vbBlack, , True
  AddStyle Grid, UrgenteB, vbRed, , True
  AddStyle Grid, RepetidaB, vbBlue, vbYellow, True
  AddStyle Grid, UrgenteRepetidaB, vbRed, vbYellow, True
  AddStyle Grid, ResultadoB, vbBlue, vbGreen, True
  AddStyle Grid, UrgenteResultadoB, vbRed, vbCyan, True
  AddStyle Grid, FRangoB, vbWhite, vbRed, True
  AddStyle Grid, UrgenteFRangoB, vbWhite, vbRed, True
  AddStyle Grid, FalloB, vbBlue, vbCyan, True
  Exit Sub
  
Autoanerror:
    Call objError.InternalError(Me, "Asignaci�n de Control", "prueba") 'cwMsgWrite & strFileName
End Sub

Public Sub AgregarEstilos(Grid As SSDBGrid)
Dim i%, iNum As CtesEstilos 'repetida
Dim j%, jNum As CtesEstilos 'estado
Dim k%, kNum As CtesEstilos 'urgente
Dim Num%, X%
Dim ForeCol As ColorConstants
Dim BackCol As ColorConstants

  For i = 0 To 1
    iNum = i
    BackCol = IIf(iNum = EstiloRepetida, vbYellow, vbWhite)
    ForeCol = vbBlack
    Debug.Print IIf(iNum = EstiloRepetida, "Repetida", "Norepetida")
    For j = 0 To 5
      jNum = IIf(j = 0, 0, 2 ^ j)
      Select Case jNum
        Case EstiloAceptada: ForeCol = vbBlue: Debug.Print "  Aceptada"
        Case EstiloARepetir: BackCol = vbRed: ForeCol = vbWhite: Debug.Print "  ARepetir"
        Case EstiloFRango: BackCol = vbRed: ForeCol = vbBlack: Debug.Print "  FueraRango"
        Case EstiloResultado: BackCol = vbCyan: ForeCol = vbBlue: Debug.Print "  Resultado"
        Case EstiloSinProgramar: ForeCol = vbBlack: Debug.Print "  SinProgramar"
      End Select
      
      For k = 0 To 1
        kNum = IIf(k = 0, 0, 2 ^ (k + 5))
        Select Case kNum
          Case EstiloUrgente: ForeCol = IIf(BackCol = vbRed, vbWhite, vbRed): Debug.Print "    Urgente"
          Case Else:: Debug.Print "    NoUrgente" ' ForeCol = vbBlack
        End Select
        Num = iNum + jNum + kNum
'        Select Case ForeCol
'          Case vbBlue: If BackCol = vbRed Then ForeCol = vbCyan
'        End Select
        AddStyle Grid, "N" & CStr(Num), ForeCol, BackCol, False
        AddStyle Grid, "NB" & CStr(Num), ForeCol, BackCol, True
        Select Case ForeCol
          Case vbWhite: Debug.Print "     Forecolor Blanco"
          Case vbBlack: Debug.Print "     Forecolor Negro"
          Case vbBlue: Debug.Print "     Forecolor Azul"
          Case vbRed: Debug.Print "     Forecolor Rojo"
          Case vbCyan: Debug.Print "     Forecolor Cyan"
          Case vbYellow: Debug.Print "     Forecolor Yellow"
          Case Else: Debug.Print "    Forecolor " & ForeCol
        End Select
        Select Case BackCol
          Case vbWhite: Debug.Print "     BackCol Blanco"
          Case vbBlack: Debug.Print "     BackCol Negro"
          Case vbBlue: Debug.Print "     BackCol Azul"
          Case vbRed: Debug.Print "     BackCol Rojo"
          Case vbCyan: Debug.Print "     BackCol Cyan"
          Case vbYellow: Debug.Print "     BackCol Yellow"
          Case Else: Debug.Print "     BackCol " & BackCol
        End Select

      Next k
      BackCol = IIf(iNum = EstiloRepetida, vbYellow, vbWhite)
      ForeCol = vbBlack
    Next j
  Next i
End Sub

Function strEstilo(stl As Estilos) As String
  Select Case stl
    Case fallo: strEstilo = "Fallo"
    Case FalloB: strEstilo = "FalloB"
    Case Normal: strEstilo = "Normal"
    Case FRango: strEstilo = "FRango"
    Case NormalB: strEstilo = "NormalB"
    Case FRangoB: strEstilo = "FRangoB"
    Case Urgente: strEstilo = "Urgente"
    Case UrgenteB: strEstilo = "UrgenteB"
    Case Repetida: strEstilo = "Repetida"
    Case RepetidaB: strEstilo = "RepetidaB"
    Case Resultado: strEstilo = "Resultado"
    Case ResultadoB: strEstilo = "ResultadoB"
    Case SinProgramar: strEstilo = "SinProgramar"
    Case SinProgramarB: strEstilo = "SinProgramarB"
    Case UrgenteFRango: strEstilo = "UrgenteFRango"
    Case UrgenteFRangoB: strEstilo = "UrgenteFRangoB"
    Case UrgenteRepetida: strEstilo = "UrgenteRepetida"
    Case UrgenteRepetidaB: strEstilo = "UrgenteRepetidaB"
    Case UrgenteResultado: strEstilo = "UrgenteResultado"
    Case UrgenteResultadoB: strEstilo = "UrgenteResultadoB"
  End Select
End Function

Private Sub AddStyle(Grid As SSDBGrid, strNombre As String, lngForeColor As ColorConstants, Optional lngBackColor As ColorConstants, Optional blnBold As Boolean)
Dim strNombreEstilo$
'  strNombreEstilo = strEstilo(strNombre)
  With Grid
    .StyleSets.Add strNombre 'Estilo
    With .StyleSets(strNombre) 'Estilo)
      .ForeColor = lngForeColor
      .Font.Bold = blnBold
      .BackColor = IIf(lngBackColor = 0, vbWhite, lngBackColor)
    End With
  End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
On Error Resume Next
    objAutoan.pSalir
End Sub

Private Sub Mascara_Change()
On Error Resume Next
  If Formulario.Mask.Text <> Mascara.Text Then
    Formulario.Mask.Text = Mascara.Text
  End If
End Sub

Private Sub Mascara_GotFocus()
  With Mascara
    .SelStart = 0
    .SelLength = .MaxLength
  End With
End Sub

Private Sub Mascara_KeyPress(KeyAscii As Integer)
On Error Resume Next
  Call Formulario.Mask_KeyPress(KeyAscii)
End Sub

Private Sub Mascara_LostFocus()
On Error Resume Next
  Call Formulario.Mask_LostFocus
End Sub

Private Sub mnuc_Click()

End Sub

Private Sub mnuAc_Click()
Dim i%, BkMrk
Dim strCodMuestra$
Dim strCodActuacion$
On Error GoTo Autoanerror

  With ssdgPruebas
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodActuacion = .Columns("CodActuInv").CellValue(BkMrk)
      strCodMuestra = .Columns("CodMuesInv").CellValue(BkMrk)
      objAutoan.ColMuestras(strCodMuestra).ColMuestrasPruebas _
      (strCodActuacion).strEstado = cteACEPTADA
    Next i
  End With
  Exit Sub
  
Autoanerror:
    Call objError.InternalError(Me, "No existe MnuMenu en el Formulario", "ssdgMuestras_MouseDown") 'cwMsgWrite & strFileName)
End Sub

Private Sub mnuAcerca_Click()
  frmAbout.Show
End Sub

Private Sub mnuSP_Click()
Dim i%, BkMrk
Dim strCodMuestra$
Dim strCodActuacion$
On Error GoTo Autoanerror

  With ssdgPruebas
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodActuacion = .Columns("CodActuInv").CellValue(BkMrk)
      strCodMuestra = .Columns("CodMuesInv").CellValue(BkMrk)
      With objAutoan.ColMuestras(strCodMuestra).ColMuestrasPruebas(strCodActuacion)
        .strEstado = cteSINPROGRAMAR
      End With
    Next i
  End With
  Exit Sub
  
Autoanerror:
    Call objError.InternalError(Me, "No existe MnuMenu en el Formulario", "ssdgMuestras_MouseDown") 'cwMsgWrite & strFileName)
End Sub

Public Function blnExist(key As String) As Boolean
End Function

Private Sub mnuAutoDescarga_Click()
  objAutoan.blnAutoDescarga = Not objAutoan.blnAutoDescarga
End Sub

Private Sub mnuItemCarpetaMuestra_Click(Index As Integer)
Dim i%, BkMrk
Dim j As Boolean
Dim strCodMuestra$
Dim strCodActuacion$
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim ColMuestras As New Collection

  With ssdgMuestras
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodMuestra = .Columns("Muestra").CellValue(BkMrk)
      Err = 0: On Error Resume Next
      j = (ColMuestras(strCodMuestra).strCodMuestra <> "")
      If Err <> 0 Then
        ColMuestras.Add objAutoan.ColMuestras(strCodMuestra), strCodMuestra
      End If
    Next i
  End With
  For Each objMuestra In ColMuestras
    For Each objActuacion In objMuestra.ColMuestrasPruebas
      With objActuacion
        If .strEstado = cteSINPROGRAMAR Or _
        .strEstado = cteAREPETIRFIN Or _
        .strEstado = cteNOACEPTADA Or _
        .strEstado = cteNOBORRADA Then
          .intCarpeta = mnuItemCarpetaMuestra(Index).Tag
        End If
      End With
    Next
  Next
  If i > 0 Then objAutoan.pRefreshGrids
End Sub

Private Sub mnuItemCarpetaPrueba_Click(Index As Integer)
Dim i%, BkMrk
Dim j As Boolean
Dim strCodMuestra$
Dim strCodActuacion$
Dim objActuacion As clsActuacion
Dim colActuaciones As New Collection

  With ssdgPruebas
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodActuacion = .Columns("CodActuInv").CellValue(BkMrk)
      strCodMuestra = .Columns("CodMuesInv").CellValue(BkMrk)
      Err = 0: On Error Resume Next
      j = (colActuaciones(strCodMuestra & "/" & strCodActuacion).strCodActuacion <> "")
      If Err <> 0 Then
        colActuaciones.Add objAutoan.ColMuestras(strCodMuestra).ColMuestrasPruebas(strCodActuacion), strCodMuestra & "/" & strCodActuacion
      End If
    Next i
  End With
  For Each objActuacion In colActuaciones
    With objActuacion
      If .strEstado = cteSINPROGRAMAR Or _
      .strEstado = cteAREPETIRFIN Or _
      .strEstado = cteNOACEPTADA Or _
      .strEstado = cteNOBORRADA Then
        objActuacion.intCarpeta = mnuItemCarpetaPrueba(Index).Tag
      End If
    End With
  Next
  If i > 0 Then objAutoan.pRefreshGrids
End Sub

Private Sub mnuItemEstadoMuestra_Click(Index As Integer)
Dim i%, BkMrk
Dim j As Boolean
Dim strCodMuestra$
Dim strCodActuacion$
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim ColMuestras As New Collection
Dim Nuevoestado As constEstado
  
  Nuevoestado = mnuItemEstadoMuestra(Index).Tag
  With ssdgMuestras
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodMuestra = .Columns("Muestra").CellValue(BkMrk)
      Err = 0: On Error Resume Next
      j = (ColMuestras(strCodMuestra).strCodMuestra <> "")
      If Err <> 0 Then
        ColMuestras.Add objAutoan.ColMuestras(strCodMuestra), strCodMuestra
      End If
    Next i
  End With
  For Each objMuestra In ColMuestras
    For Each objActuacion In objMuestra.ColMuestrasPruebas
      With objActuacion
        Select Case .strEstado
          Case cteAREPETIRFIN, cteNOACEPTADA, cteNOBORRADA
            If Nuevoestado = cteAREPETIR Then
              .intRepeticion = .intRepeticion + 1
            Else
              .strEstado = Nuevoestado
            End If
          Case cteSINPROGRAMAR
            If Nuevoestado = cteACEPTADA Or _
            Nuevoestado = cteEXTRAIDA Then
              .strEstado = Nuevoestado
            End If
          Case cteACEPTADA
            If Nuevoestado = cteSINPROGRAMAR Or _
            Nuevoestado = cteEXTRAIDA Then
              .strEstado = Nuevoestado
            End If
          Case cteREALIZACIONCOMPLETA, cteFUERADERANGOFIN
            If Nuevoestado = cteAREPETIR Then
              .intRepeticion = .intRepeticion + 1
            End If
        End Select
      End With
    Next
    With objMuestra
      If .strEstado <> cteREALIZACIONCOMPLETA And _
      .strEstado <> cteRECIBIENDORESULTADOS And _
      .strEstado <> cteREALIZADA Then
        .strEstado = mnuItemEstadoMuestra(Index).Tag
      End If
    End With
  Next
  If i > 0 Then objAutoan.pRefreshGrids
End Sub

Private Sub mnuItemEstadoPrueba_Click(Index As Integer)
Dim i%, BkMrk
Dim j As Boolean
Dim strCodMuestra$
Dim strCodActuacion$
Dim Nuevoestado As constEstado
Dim objActuacion As clsActuacion
Dim colActuaciones As New Collection

  Nuevoestado = mnuItemEstadoPrueba(Index).Tag
  With ssdgPruebas
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodActuacion = .Columns("CodActuInv").CellValue(BkMrk)
      strCodMuestra = .Columns("CodMuesInv").CellValue(BkMrk)
      Err = 0: On Error Resume Next
      j = (colActuaciones(strCodMuestra & "/" & strCodActuacion).strCodActuacion <> "")
      If Err <> 0 Then
        colActuaciones.Add objAutoan.ColMuestras(strCodMuestra).ColMuestrasPruebas(strCodActuacion), strCodMuestra & "/" & strCodActuacion
      End If
    Next i
  End With
  For Each objActuacion In colActuaciones
    With objActuacion
      Select Case .strEstado
        Case cteAREPETIRFIN, cteNOACEPTADA, cteNOBORRADA
          If Nuevoestado = cteAREPETIR Then
            .intRepeticion = .intRepeticion + 1
          Else
            .strEstado = Nuevoestado
          End If
        Case cteSINPROGRAMAR
          If Nuevoestado = cteACEPTADA Or _
          Nuevoestado = cteEXTRAIDA Then _
          .strEstado = Nuevoestado
        Case cteACEPTADA
          If Nuevoestado = cteSINPROGRAMAR Or _
          Nuevoestado = cteEXTRAIDA Then _
          .strEstado = Nuevoestado
        Case cteREALIZACIONCOMPLETA, cteFUERADERANGOFIN
          If Nuevoestado = cteAREPETIR Then _
          .intRepeticion = .intRepeticion + 1
      End Select
    End With
  Next
  If i > 0 Then objAutoan.pRefreshGrids
End Sub

Private Sub mnuSalir_Click()
  With objAutoan
    If .blnSalirSinPreguntar Then _
    Formulario.descargar Else .pSalir
  End With
End Sub

Public Sub Parpadeo_Timer()
  'Apagamos Bombillita
  Parpadeo.Enabled = False
  Shape1.BackColor = &H40& 'rojo oscuro
End Sub

Private Sub ssdgMuestras_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
On Error GoTo Autoanerror

  If Button = vbRightButton Then
    If objAutoan.blnMenuPropio Then
      Formulario.PopupMenu Formulario.mnumenu
    Else
      pCargaCarpetasMuestras
      PopupMenu mnuCambioMuestra
    End If
  End If
  Exit Sub
    
Autoanerror:
    Call objError.InternalError(Me, "No existe MnuMenu en el Formulario", "ssdgMuestras_MouseDown") 'cwMsgWrite & strFileName)
End Sub

Private Sub ssdgMuestras_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim i%, EstiloCelda$
Dim strModificacion$
Dim strMuestra$
Dim NewRow As Variant
Dim NewCol As Variant
Dim objCol As clsCabeceraGrid
On Error GoTo Autoanerror

'  If LastCol > -1 And LastCol < ssdgMuestras.Cols And Not IsNull(LastRow) Then
    With ssdgMuestras.Columns(LastCol)
      If Not IsNull(LastRow) Then
        If IsNumeric(val(LastRow)) And LastCol <> -1 Then
          If Not .Locked And Not blnBloqueoChangeRow Then
            NewRow = ssdgMuestras.Row
            NewCol = ssdgMuestras.Col
           
            'strMuestra = fValGridMuestrasEnLinea(cteMuestraCODMUESTRA, LastRow)
            'strMuestra = objAutoan.ColMuestras(val(LastRow) + 1).strCodMuestra
            strMuestra = ssdgMuestras.Columns("muestra").CellValue(LastRow)
            strModificacion = .CellValue(LastRow)
            For Each objCol In objAutoan.ColCabeceraMuestras
              If .TagVariant = objCol.intCampo Then
                With objAutoan.ColMuestras(strMuestra)
                  Select Case ssdgMuestras.Columns(LastCol).TagVariant
                    Case cteMuestraCODMUESTRA
                      .strCodMuestra = strModificacion
    '                  Dim objMuestra As clsMuestra
    '                  Set objMuestra = objAutoan.ColMuestras(strMuestra)
    '                  objMuestra.strCodMuestra = strModificacion
    '                  objAutoan.ColMuestras.Remove strMuestra
    '                  objAutoan.ColMuestras.Add objMuestra, strModificacion
                    Case cteMuestraESTADO: .strEstado = strModificacion
                    Case cteMuestraIDMUESTRA: .strIDMuestra = strModificacion
                    Case cteMuestraPOSICION: .strPosicion = strModificacion
                    Case ctePruebaURGENTE: .strUrgente = strModificacion
                    Case cteMuestraCOPA: .strCopa = strModificacion
                    Case cteMuestraURGENTE: .strUrgente = strModificacion
                    Case cteMuestraNUMREPETICION: .blnRepetida = strModificacion
                    Case cteMuestraPROPERTY1: .strProperty1 = strModificacion
                    Case cteMuestraPROPERTY2: .strProperty2 = strModificacion
                    Case cteMuestraPROPERTY3: .strProperty3 = strModificacion
                    Case cteMuestraNewSample: .strNuevaMuestra = strModificacion
                    Case cteMuestraDILUCION
                      If IsNumeric(strModificacion) Then
                        .intDilucion = strModificacion
                      Else
                        .intDilucion = 1
                      End If
                  End Select
                End With
                Exit For
              End If
            Next
            With ssdgMuestras
              .Row = NewRow
              .Col = NewCol
            End With
          End If
        End If
      End If
    End With
'  End If
  UltimaFila = ssdgMuestras.Row
  Exit Sub

Autoanerror:
    Call objError.InternalError(Me, "Cambio de Fila", "ssdgMuestras_RowColChange") 'cwMsgWrite & strFileName)
End Sub

Private Sub ssdgMuestras_RowLoadedBAK(ByVal Bookmark As Variant)
Dim blnUrgente As Boolean
Dim blnRepetida As Boolean
Dim i%, EstiloCelda As Estilos
Dim EstiloCeldaResultado As Estilos
On Error GoTo Autoanerror
  
  With ssdgMuestras
    blnUrgente = .Columns("Urg.").Text = "SI"
    blnRepetida = .Columns("Nrep.").Text = "SI"
    If blnUrgente Then
      EstiloCelda = IIf(val(.Columns("N�Rep.").Text) > 1, UrgenteRepetida, Urgente)
    Else
      EstiloCelda = IIf(val(.Columns("N�Rep.").Text) > 1, Repetida, Normal)
    End If
    Select Case strEstadoDeDesc(.Columns("Estado").Text)
      Case cteSINPROGRAMAR: EstiloCeldaResultado = IIf(blnUrgente, Urgente, SinProgramar)
      Case cteLEIDA: EstiloCeldaResultado = IIf(blnUrgente, Urgente, Normal)
      Case cteBORRADA: EstiloCeldaResultado = IIf(blnUrgente, Urgente, Normal)
      Case cteACEPTADA: EstiloCeldaResultado = IIf(blnUrgente, Urgente, Normal)
      Case cteEXTRAIDA: EstiloCeldaResultado = IIf(blnUrgente, Urgente, Normal)
      Case cteAREPETIR: EstiloCeldaResultado = IIf(blnUrgente, UrgenteRepetida, Repetida)
      Case cteAREPETIRFIN: EstiloCeldaResultado = IIf(blnUrgente, UrgenteRepetida, Repetida)
      Case cteNOBORRADA: EstiloCeldaResultado = IIf(blnUrgente, Urgente, fallo)
      Case cteNOACEPTADA: EstiloCeldaResultado = IIf(blnUrgente, Urgente, fallo)
      Case cteREALIZADA: EstiloCeldaResultado = IIf(blnUrgente, UrgenteResultado, Resultado)
      Case cteFUERADERANGOFIN: EstiloCeldaResultado = IIf(blnUrgente, UrgenteFRango, FRango)
      Case ctePREPROGRAMACION: EstiloCeldaResultado = IIf(blnUrgente, Urgente, Normal)
      Case cteFUERADERANGO: EstiloCeldaResultado = IIf(blnUrgente, UrgenteFRango, FRango)
      Case cteINTENTOBORRADO: EstiloCeldaResultado = IIf(blnUrgente, Urgente, Normal)
      Case cteINTENTOPROGRAMAR: EstiloCeldaResultado = IIf(blnUrgente, Urgente, Normal)
      Case cteREALIZACIONCOMPLETA: EstiloCeldaResultado = IIf(blnUrgente, UrgenteResultado, Resultado)
      Case cteRECIBIENDORESULTADOS: EstiloCeldaResultado = IIf(blnUrgente, Urgente, fallo)
      Case cteFALLODEAUTOANALIZADOR: EstiloCeldaResultado = IIf(blnUrgente, Urgente, fallo)
      Case cteFALLODEAUTOANALIZADORFIN: EstiloCeldaResultado = IIf(blnUrgente, Urgente, fallo)
    End Select
'    For i = 0 To .Cols - 1
'      Select Case .Columns(i).TagVariant
    For i = 0 To .Cols - 1
      .Columns(i).CellStyleSet strEstilo(EstiloCeldaResultado) & "B" 'strEstilo(EstiloCelda) & "B"
    Next i
  End With
  Exit Sub
        
Autoanerror:
  Call objError.InternalError(Me, "ssdgMuestras_RowLoaded", "prueba") 'cwMsgWrite & strFileName)
End Sub

Private Sub ssdgMuestras_RowLoaded(ByVal Bookmark As Variant)
Dim blnUrgente As Boolean
Dim blnRepetida As Boolean
Dim i%, EstiloCelda As Estilos
Dim EstiloCeldaResultado As Estilos
Dim NEstilo As CtesEstilos
On Error GoTo Autoanerror
  
  With ssdgMuestras
    If .Columns("Urg.").Text = "SI" Then NEstilo = EstiloUrgente
    If .Columns("N�Rep.").Text = "SI" Then NEstilo = NEstilo + EstiloRepetida
    Select Case strEstadoDeDesc(.Columns("Estado").Text)
      Case cteSINPROGRAMAR: NEstilo = NEstilo + EstiloSinProgramar
      Case cteLEIDA: NEstilo = NEstilo + EstiloSinProgramar
      Case cteBORRADA: NEstilo = NEstilo + EstiloAceptada
      Case cteACEPTADA: NEstilo = NEstilo + EstiloAceptada
      Case cteEXTRAIDA: NEstilo = NEstilo + EstiloSinProgramar
      Case cteAREPETIR: NEstilo = NEstilo + EstiloARepetir
      Case cteAREPETIRFIN: NEstilo = NEstilo + EstiloARepetir
      Case cteNOBORRADA: NEstilo = NEstilo + EstiloAceptada
      Case cteNOACEPTADA: NEstilo = NEstilo + EstiloSinProgramar
      Case cteREALIZADA: NEstilo = NEstilo + EstiloResultado
      Case cteFUERADERANGOFIN: NEstilo = NEstilo + EstiloFRango
      Case ctePREPROGRAMACION: NEstilo = NEstilo + EstiloAceptada
      Case cteFUERADERANGO: NEstilo = NEstilo + EstiloFRango
      Case cteINTENTOBORRADO: NEstilo = NEstilo + EstiloAceptada
      Case cteINTENTOPROGRAMAR: NEstilo = NEstilo + EstiloAceptada
      Case cteREALIZACIONCOMPLETA: NEstilo = NEstilo + EstiloResultado
      Case Else: NEstilo = NEstilo + EstiloSinProgramar
    End Select
    For i = 0 To .Cols - 1
      .Columns(i).CellStyleSet "NB" & CStr(NEstilo)
    Next i
  End With
  Exit Sub
        
Autoanerror:
  Call objError.InternalError(Me, "ssdgMuestras_RowLoaded", "prueba") 'cwMsgWrite & strFileName)
End Sub

Private Sub ssdgPruebas_DblClick()
'objAutoan.frmFormulario.probar
End Sub

Private Sub ssdgPruebas_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
On Error GoTo Autoanerror

  If Button = vbRightButton Then
    If objAutoan.blnMenuPropio Then
      Formulario.PopupMenu Formulario.mnumenu
    Else
      pCargaCarpetasPruebas
      PopupMenu mnuCambioPrueba
    End If
  End If
  Exit Sub
    
Autoanerror:
  Call objError.InternalError(Me, "No existe MnuMenu en el Formulario", "ssdgPruebas_MouseDown") 'cwMsgWrite & strFileName)
End Sub

Private Sub ssdgPruebas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim i%, EstiloCelda$
Dim strModificacion$
Dim strActuacion$
Dim strMuestra$
Dim NewRow As Variant
Dim NewCol As Variant
Dim objCol As clsCabeceraGrid
On Error GoTo Autoanerror

  If LastCol > -1 And LastCol < ssdgPruebas.Cols And Not IsNull(LastRow) Then
    With ssdgPruebas.Columns(LastCol)
      If Not .Locked And Not blnBloqueoChangeRow Then
        NewRow = ssdgPruebas.Row
        NewCol = ssdgPruebas.Col
        strMuestra = fValGridPruebasEnLinea(cteMuestraCODMUESTRA, LastRow)
        strActuacion = fValGridPruebasEnLinea(ctePruebaCODACTUACION, LastRow)
        strModificacion = .CellValue(LastRow)
        For Each objCol In objAutoan.ColCabeceraMuestras
          If .TagVariant = objCol.intCampo Then
            With objAutoan.ColMuestras(strMuestra).ColMuestrasPruebas(strActuacion)
              Select Case ssdgPruebas.Columns(LastCol).TagVariant
                Case ctePruebaCODACTUACION: .strCodAct = strModificacion
                Case ctePruebaDESCACTUACION: .strDesAct = strModificacion
                Case ctepruebaNUMACTUACION: .strNumAct = strModificacion
                Case ctepruebaNUMREPETICION: .intRepeticion = strModificacion
                Case ctePruebaCODACTAUTO: .strCodActAuto = strModificacion
                Case ctePruebaESTADO: .strEstado = strModificacion
                Case ctePruebaURGENTE: .blnUrgente = strModificacion
                Case ctePruebaPROPERTY1: .strProperty1 = strModificacion
                Case ctePruebaPROPERTY2: .strProperty2 = strModificacion
                Case ctePruebaREPETICION: .intRepeticion = strModificacion
                Case ctePruebaLISTAREALIZACION: .lngNumLista = strModificacion
                Case ctePruebaDILUCION
                  If IsNumeric(strModificacion) Then
                    .intDilucion = strModificacion
                  Else
                    .intDilucion = 1
                  End If
              End Select
            End With
            Exit For
          End If
        Next
        With ssdgPruebas
          .Row = NewRow
          .Col = NewCol
        End With
      End If
    End With
  End If
  Exit Sub

Autoanerror:
    Call objError.InternalError(Me, "Cambio de Fila", "ssdgPruebas_RowColChange") 'cwMsgWrite & strFileName)
End Sub

Private Sub pCargaEstados()
Dim i%
  'pCargaEstado
  mnuItemCarpetaPrueba(1).Visible = False
  mnuItemCarpetaMuestra(1).Visible = False
End Sub

Public Sub pCargaEstado(Estado As constEstado)
Dim i%
  i = mnuItemEstadoPrueba.Count + 1
  Load mnuItemEstadoPrueba(i)
  With mnuItemEstadoPrueba(i)
    .Caption = strDescEstado(Estado)
    .Visible = True
    .Tag = Estado
  End With
  i = mnuItemEstadoMuestra.Count + 1
  Load mnuItemEstadoMuestra(i)
  With mnuItemEstadoMuestra(i)
    .Caption = strDescEstado(Estado)
    .Visible = True
    .Tag = Estado
  End With
End Sub

Private Sub pCargaCarpetasPruebas()
Dim Sql$, i%
Dim Qry As rdoQuery
Dim BkMrk As Variant
Dim Rs As rdoResultset
Dim ArrayCods As String
Dim strCodActuacion As String
On Error GoTo Autoanerror

  With ssdgPruebas
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodActuacion = .Columns("CodActuInv").CellValue(BkMrk)
      If Trim$(strCodActuacion) <> "" Then
        ArrayCods = ArrayCods & strCodActuacion & ","
      End If
    Next i
  End With
  mnuItemCarpetaPrueba(1).Visible = True
  For i = 2 To mnuItemCarpetaPrueba.Count '- 1
    Unload mnuItemCarpetaPrueba(i)
  Next i
  If ArrayCods <> "" Then
    Sql = "SELECT DISTINCT c.designacion, c.ccarpeta " _
    & " FROM carpetas c, pruebascarpetas pc" _
    & " WHERE c.ccarpeta = pc.ccarpeta" _
    & " AND c.cAutoanalizador <> ?" _
    & " AND pc.cPrueba in " _
    & "(" & Left$(ArrayCods, Len(ArrayCods) - 1) & ")" _
    & " ORDER BY c.designacion"
    Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
    Qry(0) = objAutoan.intCodAutoAn
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    i = 1
    If Not Rs.EOF Then
      Do
        i = i + 1
        Load mnuItemCarpetaPrueba(i)
        With mnuItemCarpetaPrueba(i)
          .Caption = Rs!designacion
          .Visible = True
          .Tag = Rs!cCarpeta
        End With
        Rs.MoveNext
      Loop While Not Rs.EOF
      mnuItemCarpetaPrueba(1).Visible = False
    End If
  End If
  Exit Sub

Autoanerror:
    Call objError.InternalError(Me, "pCargaCarpetas", "pCargaCarpetas")
End Sub

Private Sub pCargaCarpetasMuestras()
Dim Sql$, i%
Dim j As Boolean
Dim Qry As rdoQuery
Dim BkMrk As Variant
Dim Rs As rdoResultset
Dim ArrayCods As String
Dim strCodMuestra As String
Dim strCodActuacion As String
Dim objActuacion As clsActuacion
Dim colCodActuaciones As New Collection
On Error GoTo Autoanerror

  With ssdgMuestras
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodMuestra = .Columns("muestra").CellValue(BkMrk)
      For Each objActuacion In objAutoan.ColMuestras(strCodMuestra).ColMuestrasPruebas
        Err = 0
        On Error Resume Next
        j = (colCodActuaciones(objActuacion.strCodAct).strCodAct <> "")
        If Err <> 0 Then
          colCodActuaciones.Add objActuacion, objActuacion.strCodAct
        End If
      Next
    Next i
    For Each objActuacion In colCodActuaciones
      ArrayCods = ArrayCods & objActuacion.strCodAct & ","
    Next
  End With
  mnuItemCarpetaMuestra(1).Visible = True
  For i = 2 To mnuItemCarpetaMuestra.Count '- 1
    Unload mnuItemCarpetaMuestra(i)
  Next i
  If ArrayCods <> "" Then
    Sql = "SELECT DISTINCT c.designacion, c.ccarpeta " _
    & " FROM carpetas c, pruebascarpetas pc" _
    & " WHERE c.ccarpeta = pc.ccarpeta" _
    & " AND c.cAutoanalizador <> ?" _
    & " AND pc.cPrueba in " _
    & "(" & Left$(ArrayCods, Len(ArrayCods) - 1) & ")" _
    & " ORDER BY c.designacion"
    Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
    Qry(0) = objAutoan.intCodAutoAn
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    i = 1
    If Not Rs.EOF Then
      Do
        i = i + 1
        Load mnuItemCarpetaMuestra(i)
        With mnuItemCarpetaMuestra(i)
          .Caption = Rs!designacion
          .Visible = True
          .Tag = Rs!cCarpeta
        End With
        Rs.MoveNext
      Loop While Not Rs.EOF
      mnuItemCarpetaMuestra(1).Visible = False
    End If
  End If
  Exit Sub

Autoanerror:
    Call objError.InternalError(Me, "pCargaCarpetas", "pCargaCarpetas")
End Sub

Private Function fValGridMuestrasEnLinea(intColumna As Integer, varLine As Variant) As String
Dim objColumna As clsCabeceraGrid
On Error GoTo Autoanerror

  For Each objColumna In objAutoan.ColCabeceraMuestras
    If objColumna.intCampo = intColumna Then
      fValGridMuestrasEnLinea = ssdgMuestras.Columns(objColumna.strNombre).CellValue(varLine)
      Exit For
    End If
  Next
  Exit Function
  
Autoanerror:
    Call objError.InternalError(Me, "fValGridMuestrasEnLinea", "fValGridMuestrasEnLinea")
End Function

Private Function fValGridPruebasEnLinea(intColumna As Integer, varLine As Variant) As String
Dim objColumna As clsCabeceraGrid
On Error GoTo Autoanerror

  For Each objColumna In objAutoan.ColCabeceraPruebas
    If objColumna.intCampo = intColumna Then
      fValGridPruebasEnLinea = ssdgPruebas.Columns(objColumna.strNombre).CellValue(varLine)
      Exit For
    End If
  Next
  Exit Function
  
Autoanerror:
    Call objError.InternalError(Me, "fValGridPruebasEnLinea", "fValGridPruebasEnLinea") 'cwMsgWrite & strFileName)
End Function

Private Sub ssdgPruebas_RowLoaded(ByVal Bookmark As Variant)
Dim i%, NEstilo As CtesEstilos
Dim EstiloCelda As Estilos
Dim EstiloCeldaResultado As Estilos
On Error GoTo Autoanerror

  With ssdgPruebas
    If .Columns("Urg.").Text = "SI" Then NEstilo = EstiloUrgente
    If .Columns("N�Rep.").Text = "SI" Then NEstilo = NEstilo + EstiloRepetida
    Select Case strEstadoDeDesc(.Columns("Estado Res").Text)
      Case cteSINPROGRAMAR: NEstilo = NEstilo + EstiloSinProgramar
      Case cteLEIDA: NEstilo = NEstilo + EstiloAceptada
      Case cteBORRADA: NEstilo = NEstilo + EstiloAceptada
      Case cteACEPTADA: NEstilo = NEstilo + EstiloAceptada
      Case cteEXTRAIDA: NEstilo = NEstilo + EstiloSinProgramar
      Case cteAREPETIR: NEstilo = NEstilo + EstiloARepetir
      Case cteNOBORRADA: NEstilo = NEstilo + EstiloAceptada
      Case cteNOACEPTADA: NEstilo = NEstilo + EstiloAceptada
      Case cteREALIZADA: NEstilo = NEstilo + EstiloResultado
      Case cteFUERADERANGOFIN: NEstilo = NEstilo + EstiloFRango
      Case ctePREPROGRAMACION: NEstilo = NEstilo + EstiloAceptada
      Case cteFUERADERANGO: NEstilo = NEstilo + EstiloFRango
      Case cteINTENTOBORRADO: NEstilo = NEstilo + EstiloAceptada
      Case cteINTENTOPROGRAMAR: NEstilo = NEstilo + EstiloAceptada
      Case cteREALIZACIONCOMPLETA: NEstilo = NEstilo + EstiloResultado
      Case cteRECIBIENDORESULTADOS: NEstilo = NEstilo + EstiloResultado
      Case Else: NEstilo = NEstilo + EstiloSinProgramar
    End Select
    For i = 0 To .Cols - 1
      Select Case .Columns(i).TagVariant
        Case ctePruebaFILA, ctePruebaCODMUESTRA, ctePruebaREPETICION, _
        ctePruebaURGENTE, ctePruebaESTADO, ctePruebaCODACTUACION, _
        ctePruebaLISTAREALIZACION, ctePruebaDESCACTUACION, _
        ctepruebaNUMACTUACION, ctepruebaNUMREPETICION, _
        ctePruebaCODACTAUTO, ctePruebaPROPERTY1, _
        ctePruebaPROPERTY2, ctePruebaPROPERTY3
          .Columns(i).CellStyleSet "NB" & NEstilo
        Case cteResultadoVALRESULTADO, cteResultadoDESCRESULTADO
          .Columns(i).CellStyleSet "N" & NEstilo
        Case Else: .Columns(i).CellStyleSet "N" & NEstilo
      End Select
    Next i
  End With
  Exit Sub
    
Autoanerror:
  Call objError.InternalError(Me, "ssdgPruebas_RowLoaded", "ssdgPruebas_RowLoaded")
End Sub

Private Sub ssdgPruebas_RowLoadedBACK(ByVal Bookmark As Variant)
Dim i%
Dim EstiloCelda As Estilos
Dim EstiloCeldaResultado As Estilos
On Error GoTo Autoanerror

  With ssdgPruebas
    If .Columns("Urg.").Text = "SI" Then
      EstiloCelda = IIf(val(.Columns("N�Rep.").Text) > 1, UrgenteRepetida, Urgente)
    Else
      EstiloCelda = IIf(val(.Columns("N�Rep.").Text) > 1, Repetida, Normal)
    End If
    Select Case strEstadoDeDesc(.Columns("Estado Res").Text)
      Case cteSINPROGRAMAR: EstiloCeldaResultado = SinProgramar
      Case cteLEIDA: EstiloCeldaResultado = Normal
      Case cteBORRADA: EstiloCeldaResultado = Normal
      Case cteACEPTADA: EstiloCeldaResultado = Normal
      Case cteEXTRAIDA: EstiloCeldaResultado = Normal
      Case cteAREPETIR: EstiloCeldaResultado = Normal
      Case cteNOBORRADA: EstiloCeldaResultado = fallo
      Case cteNOACEPTADA: EstiloCeldaResultado = fallo
      Case cteREALIZADA: EstiloCeldaResultado = Resultado
      Case cteFUERADERANGOFIN: EstiloCeldaResultado = FRango
      Case ctePREPROGRAMACION: EstiloCeldaResultado = Normal
      Case cteFUERADERANGO: EstiloCeldaResultado = FRango
      Case cteINTENTOBORRADO: EstiloCeldaResultado = Normal
      Case cteINTENTOPROGRAMAR: EstiloCeldaResultado = Normal
      Case cteREALIZACIONCOMPLETA: EstiloCeldaResultado = Resultado
      Case cteRECIBIENDORESULTADOS: EstiloCeldaResultado = Normal
      Case cteFALLODEAUTOANALIZADOR: EstiloCeldaResultado = fallo
      Case cteFALLODEAUTOANALIZADORFIN: EstiloCeldaResultado = fallo
    End Select
    'If Trim$(.Columns("Muestra").Text) <> "" Then
    '  EstiloCelda = EstiloCelda & "1" '"Normal"
    'End If
    For i = 0 To .Cols - 1
      Select Case .Columns(i).TagVariant
        Case ctePruebaFILA, ctePruebaCODMUESTRA, ctePruebaREPETICION, _
        ctePruebaURGENTE, ctePruebaESTADO, ctePruebaCODACTUACION, _
        ctePruebaDESCACTUACION, ctepruebaNUMACTUACION, ctepruebaNUMREPETICION, _
        ctePruebaCODACTAUTO, ctePruebaPROPERTY1, ctePruebaPROPERTY2, ctePruebaPROPERTY3
          .Columns(i).CellStyleSet strEstilo(EstiloCelda) & "B"
        Case cteResultadoVALRESULTADO, cteResultadoDESCRESULTADO
          .Columns(i).CellStyleSet strEstilo(EstiloCeldaResultado)
        Case Else: .Columns(i).CellStyleSet strEstilo(EstiloCelda)
      End Select
    Next i
  End With
  Exit Sub
    
Autoanerror:
  Call objError.InternalError(Me, "ssdgPruebas_RowLoaded", "ssdgPruebas_RowLoaded")
End Sub

Private Sub ssdgPruebas_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
Dim i
Dim cMuestra$
Dim cActuacion$
'  For i = 0 To .SelBookmarks.Count - 1
'      bkmrk = .SelBookmarks(i)
'      strCodActuacion = .Columns("Cod.Act.").CellValue(bkmrk)
'  Next i
  cActuacion = ssdgPruebas.Columns("codactuinv").Text
  cMuestra = ssdgPruebas.Columns("codmuesinv").Text
  objAutoan.ColMuestras(cMuestra).ColMuestrasPruebas(cActuacion).ElegirFila
  'Debug.Print ssdgPruebas.Row
  
End Sub





Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
Dim objMuestra As clsMuestra
On Error GoTo Autoanerror

  With objAutoan
    .EscribirLog "Toolbar1_ButtonClick: " & Button.ToolTipText
    Select Case Button.key
      Case ButtonExit
        If .blnSalirSinPreguntar Then _
        Formulario.descargar Else .pSalir
      Case ButtonProgram: Formulario.Programar
      Case ButtonRefresh
        .pDescargarResultados
        .TimerCargaNuevasPruebas_Timer
        .pRefreshGrids
      Case ButtonResults: Formulario.resultados
      Case ButtonComprobar: .pComprobacion
    End Select
  End With
  Exit Sub
    
Autoanerror:
  Call objError.InternalError(Me, "No se encontro el metodo '" & Button.Tag & "' en el formulario del " & Formulario.Caption, "Toolbar1_ButtonClick")
End Sub
