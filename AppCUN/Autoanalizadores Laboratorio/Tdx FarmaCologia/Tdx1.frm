VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TDX FLX 1"
   ClientHeight    =   465
   ClientLeft      =   2385
   ClientTop       =   1980
   ClientWidth     =   960
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Tdx1.frx":0000
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   465
   ScaleWidth      =   960
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   510
      Top             =   30
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MiNumeroListas As Integer
Dim MiLngListaTrabajo As Long

Public Sub Programar()
  frmSeleccionLista.Show vbModal
End Sub

Public Property Let lngListaTrabajo(Lista As Long)
  If MiLngListaTrabajo <> Lista Then
    MiLngListaTrabajo = Lista: CargarLista
  End If
End Property

Public Property Get lngListaTrabajo() As Long
  lngListaTrabajo = MiLngListaTrabajo
End Property

Public Sub pComprobarLista()
Dim objActuacion As clsActuacion
Dim NLista As Long, objMuestra As clsMuestra
  If MiNumeroListas > 1 Then
    If Not objAutoAn.blnExistMuestraConEstado(cteACEPTADA) Then
      frmSeleccionLista.Show vbModal
    Else 'comprobamos que todas las muestras
    'programadas son de la misma lista
      For Each objMuestra In objAutoAn.ColMuestras
        If objMuestra.strEstado = cteACEPTADA Then
          For Each objActuacion In objMuestra.ColMuestrasPruebas
            If objActuacion.strEstado = cteACEPTADA Then
              If objActuacion.lngNumLista <> NLista And NLista <> 0 Then
                NLista = objActuacion.lngNumLista
              End If
            End If
          Next
        End If
      Next
    End If
  Else
    lngListaTrabajo = objAutoAn.ColMuestras(1). _
    ColMuestrasPruebas(1).lngNumLista: CargarLista
  End If
End Sub

Sub CargarLista()
Dim EstadoMuestra As constEstado, EstadoPrueba As constEstado
Dim objMuestra As clsMuestra, objActuacion As clsActuacion
'Pone en estado para recibir los resultados
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      EstadoMuestra = .strEstado
      If EstadoMuestra <> cteAREPETIR And EstadoMuestra <> cteAREPETIRFIN And _
      EstadoMuestra <> cteFUERADERANGO And EstadoMuestra <> cteFUERADERANGOFIN And _
      EstadoMuestra <> cteREALIZADA And EstadoMuestra <> cteREALIZACIONCOMPLETA Then _
      .strEstado = cteSINPROGRAMAR
      For Each objActuacion In .ColMuestrasPruebas
        With objActuacion
          If .lngNumLista = lngListaTrabajo Then
            .strEstado = cteACEPTADA
'            .Muestra.strestado = cteACEPTADA
'            EstadoPrueba = .strestado
'            If EstadoPrueba <> cteAREPETIR And EstadoPrueba <> cteAREPETIRFIN And _
'            EstadoPrueba <> cteFUERADERANGO And EstadoPrueba <> cteFUERADERANGOFIN And _
'            EstadoPrueba <> cteREALIZADA And EstadoPrueba <> cteREALIZACIONCOMPLETA Then _
'            .strestado = cteSINPROGRAMAR
          End If
        End With
      Next
    End With
  Next
End Sub

Property Let NumeroListas(numListas As Integer)
  If MiNumeroListas <> numListas Then
    MiNumeroListas = numListas
  End If
End Property

Property Get NumeroListas() As Integer
  NumeroListas = MiNumeroListas
End Property

Public Sub pModificaActuacion(objActuacion As clsActuacion)
 NumeroListas = objAutoAn.ColListasDeTrabajo.Count
 objActuacion.strProperty1 = objActuacion.lngNumLista
End Sub

Private Sub Lectura_Datos(Datos As String)
'Se leen los datos seg�n lo explicado en el manual. La 1� l�nea contiene la fecha (que no se leer� por que no suele coincidir
'con la fecha real). Las 10 l�neas restantes contienen los resultados de la prueba, que siempre aparecen en el mismo orden.
Dim CodPrueba As String, blnListaOK As Boolean
Dim intcodResultado As Integer, x%
Dim PosNeg As Integer, ResultadoAuto As String
Dim Resultado As String, Resul$, RefMin%, RefMax%
Dim i As Integer, Res%, Numero%, Valor As Double
Dim Campo$, objMuestra As clsMuestra
Dim Sql$
Dim Qry As rdoQuery
Dim Rs1 As rdoResultset
Dim objActuacion As clsActuacion

  Awk1 = Datos: Awk1.FS = vbCrLf
  PosNeg = 0:  intcodResultado = 1
'no puedo hacer esto porque el autoanalizador no devuelve el test.
'Solo lo da cuando se hace un reprint
'  ResultadoAuto = Trim$(Left$(Awk1.F(1), 15))
'buscamos el codigo de prueba-resultado en GDL
'  Sql = "SELECT cprueba , cresultado FROM ResultadosAutoanalizador " _
'  & "WHERE cAutoAnalizador = ? AND cResultadoAuto= ?"
'  Set Qry = objAutoAn.rdoConnect.CreateQuery("", Sql)
'  Qry(0) = objAutoAn.intCodAutoAn
'  Qry(1) = ResultadoAuto
'  Set Rs1 = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'  If Not Rs1.EOF Then
'    CodPrueba = Rs1!cprueba
'    intcodresultado = Rs1!cresultado
'  End If
     
  'Verificamos que la lista preseleccionada tiene la prueba recibida
'  Do While Not blnListaOK
'    If Not objAutoAn.blnExistPruebaInCollection(objAutoAn. _
'    ColListasDeTrabajo(CStr(lngListaTrabajo)).colActuaciones, CodPrueba) Then
'      If MsgBox("No se ha encontrado la prueba " & ResultadoAuto & _
'      "en la lista seleccionada " & lngListaTrabajo & "." & vbCrLf & _
'      "Desea seleccionar otra lista?", vbQuestion, "Prueba No Encontrada") = vbOK Then
'        frmSeleccionLista.Show vbModal
'      Else
'        Exit Sub
'      End If
'    Else
'      blnListaOK = True
'    End If
'  Loop
'
'  If ResultadoAuto = "COCAINE METAB U" Or ResultadoAuto = "BARBITURATES U" Or _
'  ResultadoAuto = "CANNABINOIDS U" Or ResultadoAuto = "AMPHET/METH U" Or _
'  ResultadoAuto = "BENZODIAZEPINE" Or ResultadoAuto = "OPIATES U" Then PosNeg = 1
      
  'Buscamos la posicion de los resultados
'  For x = 1 To Awk1.NF - 3
'    If Awk1.F(x) = Chr$(13) Then Exit For
'    'numero de posici�n
'    If Right$(Awk1.F(x), 2) = " 1" Then Exit For
'  Next x

'  If x <= Awk1.NF - 3 Then
'    For i = x To Awk1.NF - 3
For i = 1 To Awk1.NF - 3
  If IsNumeric(Trim$(Mid$(Awk1.F(i), 1, 2))) Then
    Numero = CInt(Trim$(Mid$(Awk1.F(i), 1, 2)))
      If Numero <= objAutoAn.ColMuestras.Count Then
        Set objMuestra = objAutoAn.ColMuestras(Numero) 'van en orden
          If objMuestra.strEstado = cteACEPTADA Then
            For Each objActuacion In objMuestra.ColMuestrasPruebas
              With objActuacion
               If objAutoAn.blnExistPruebaInMuestra(objMuestra.strCodMuestra, .strCodAct) Then
                  If .strCodAct = "298" Or .strCodAct = "302" Or .strCodAct = "297" Or _
                      .strCodAct = "299" Or .strCodAct = "294" Or _
                      .strCodAct = "304" Then
                        PosNeg = 1
                  End If
                  
                  With .ColPruebasResultados(CStr(intcodResultado))
'                 With objMuestra.ColMuestrasPruebas(CodPrueba).ColPruebasResultados(CStr(intcodResultado))
                RefMin = .DblRefMin: RefMax = .DblRefMax
                Campo = Trim$(Mid$(Awk1.F(i), 5, 9)): Res = InStr(Awk1.F(i), ">=T")
                If Res > 0 And PosNeg = 1 Then Campo = "Positivo"
                If IsNumeric(Campo) = True Then
                  'valores numericos
                  Valor = objAutoAn.fFormatearNumero(Campo, ".", ",")
                  If PosNeg = 1 And (RefMin <> 0 Or RefMax <> 0) Then
                    Select Case Valor
                      Case Is >= RefMax: Resul = "Positivo"
                      Case Is < RefMin: Resul = "Negativo"
                      Case Else: Resul = Campo
                    End Select
                  Else
                    Resul = CStr(Valor)
                  End If
                Else
                  If Campo = "LOW" Then
                    If PosNeg <> 0 Then Campo = "Negativo" Else Campo = "Indetectable"
                  End If: Resul = Campo
                End If: .strResultado = Resul
              End With  'clopruebasres
            End If  'existe prueba
          End With  'colpruebas
          Next
          End If 'muestra aceptada
        End If 'numero<=
      End If  'isnumeric
    Next i
'  End If
End Sub

Public Sub Lectura_Protocolo(caracter As String)
Static Mensaje As String 'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
  Mensaje = Mensaje & caracter 'Se va componiendo el mensaje
  CopiaMensaje = Mensaje
  Select Case objAutoAn.Estado
    Case cteESPERA
      Tdx_Espera CopiaMensaje
      Mensaje = CopiaMensaje
    Case cteTRANSMISION
      Tdx_Transmision CopiaMensaje
      Mensaje = CopiaMensaje
  End Select
End Sub

Private Sub Tdx_Espera(Mensaje As String)
  If Right$(Mensaje, 3) = " 1 " Then
    objAutoAn.Estado = cteTRANSMISION
    Mensaje = " 1 "
  End If
'objAutoAn.EscribirLog Mensaje & "-"
'If Right$(Mensaje, 6) = "ASSAY:" Then
'    objAutoAn.Estado = cteTRANSMISION
'    Mensaje = ""
'  End If
End Sub

Private Sub Tdx_Transmision(Mensaje As String)
  If Right$(Mensaje, 2) = "!!" Or _
  Right$(Mensaje, 4) = "USED" Or _
  Right$(Mensaje, 4) = "TEST" Then
    Lectura_Datos Mensaje
    objAutoAn.Estado = cteESPERA
    Mensaje = ""
  End If
End Sub

Public Sub Descargar()
  End
End Sub

Public Sub Probar()
Dim Datos$

'  Datos = vbCrLf & _
'  "DATE: 16/10/96" & vbCrLf & _
'  "TIME: 09:30:41" & vbCrLf & _
'  "SERIAL #: 90872" & vbCrLf & _
'  "LOCK= 1" & vbCrLf & _
'  "ASSAY: BENZODIAZEPINE U" & vbCrLf &
'  Datos = "CY A (WB)" & vbCrLf & _
'  "CAROUSEL:  1" & vbCrLf & _
'  vbCrLf & _
'  "SPLVOL=   5.00" & vbCrLf & _
'  "REPS= 1" & vbCrLf & _
'  "GAIN= 20" & vbCrLf & _
'  "CALIB.DATE: 06/10/95" & vbCrLf & _
'  "CALIB.TIME: 11:30:58" & vbCrLf & _
'  vbCrLf & _
'  "CONC= NG/ML" & vbCrLf & _
'  vbCrLf

'Datos = " 1     7.93      167.95       391.59" & vbCrLf & _
'        vbCrLf & _
'      "TESTS USED= 16" & vbCrLf & _
'      vbCrLf & _
'      vbCrLf & _
'      vbCrLf & _
'      "!!"
'
  
  
'  Datos = Datos & "        STORED THRESHOLD =   " & vbCrLf & _
'  " 1     138.69      218.43        63.63" & vbCrLf & _
'  " 2       4.12      218.43        63.63" & vbCrLf & _
'  " 3    3211.70 >=T  100.12        45.34" & vbCrLf & _
  vbCrLf & _
  "TESTS USED= 16" & vbCrLf & _
  vbCrLf & _
  vbCrLf & _
  vbCrLf & _
  "!!" '& vbCrLf
  
'  objAutoAn.AutoRecepcion Datos
'Tdx_Transmision Datos
End Sub

Public Function Salir() As Boolean
On Error GoTo AutoAnError
'  SubRutina = "Salir"
  objAutoAn.EscribirLog "Salir"
  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion, Me.Caption) = vbCancel)
  objAutoAn.EscribirLog " Pulsado Salir"
  If Not Salir Then Descargar
  Exit Function
  
AutoAnError:
    Call objError.InternalError(Me, "Salir", "Salir")
End Function
