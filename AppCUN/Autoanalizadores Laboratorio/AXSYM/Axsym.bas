Attribute VB_Name = "modAxsym"
Option Explicit
Public objAutoAn As clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
'  App.HelpFile = "f:\laborat\ayuda\" & App.EXEName & ".hlp"
  Set objAutoAn = New clsAutoAnalizador 'Se define una instancia del objeto clsAutoAnalizador
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    fCargarColumnasGrid 'Definicion de las columnas que componen los 2 Grid
    If Not .fInicializar(App, cteAXSYM, True, , True, , , , , , True) Then End
    'Cargar_ResultadosAnteriores_Tiempos
    .Borrar_Logs
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  With objAutoAn
    'Columnas del Grid Muestras
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraDILUCION, , , , Editable, , 4
    .AgregarColumnaAGridMuestras cteMuestraPROPERTY1, "Tiempo"
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    'Columnas del Grid Pruebas-Resultados
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION, , 1000
'    .AgregarColumnaAGridPruebas ctePruebaDILUCION, , , , Editable
    '.AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    '.AgregarColumnaAGridPruebas ctePruebaPROPERTY1, "Anterior"
    .AgregarColumnaAGridPruebas ctePruebaPROPERTY1, "Anterior", , , Izquierda
'    .AgregarColumnaAGridPruebas ctePruebaPROPERTY2, "Anterior2", , , Izquierda
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
End Sub

Private Sub Cargar_ResultadosAnteriores_Tiempos()
Dim qry1 As rdoQuery
Dim qry2 As rdoQuery
Dim rs1 As rdoResultset
Dim rs2 As rdoResultset
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim sql$, NumHistoria As Long, i%
On Error GoTo AutoAnError

  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      frmPrincipal.pModificaMuestra objMuestra
'      sql = "SELECT tiempo FROM muestraAsistencia WHERE "
'      sql = sql & "cMuestra = ?"
'      Set qry1 = objAutoAn.rdoConnect.CreateQuery("", sql)
'      qry1(0) = .strcodmuestra
'      Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'      .strProperty1 = IIf(IsNull(rs1(0)), "", rs1(0)) 'tiempo
'      rs1.Close: qry1.Close
'      'se saca el n� Historia
''      sql = "select ci22numhistoria from ci2200, pr0400 where"
''      sql = sql & " ci2200.ci21codpersona = pr0400.ci21codpersona"
''      sql = sql & " and pr0400.pr04numactplan = ?"
''      Set qry1 = objAutoAn.rdoConnect.CreateQuery("", sql)
''      qry1(0) = CLng(.colmuestraspruebas(1).strnumact)
''      Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
''      If Not rs1.EOF Then NumHistoria = rs1(0) Else GoTo NextMuestra
''      .strProperty2 = NumHistoria
''      rs1.Close: qry1.Close
      For Each objActuacion In objMuestra.ColMuestrasPruebas
        frmPrincipal.pModificaActuacion objActuacion
'        With objActuacion
'          sql = "select resultadoalfanumerico from" _
'          & " resultadoasistencia ra, pruebaasistencia pa where" _
'          & " ra.historia = pa.historia and ra.caso = pa.caso and" _
'          & " ra.secuencia = pa.secuencia and" _
'          & " ra.nrepeticion = pa.nrepeticion and" _
'          & " pa.historia = ? and pa.ctipoprueba = ?" _
'          & " order by ra.fecha desc"
'          Set qry2 = objAutoAn.rdoConnect.CreateQuery("", sql)
'          qry2(0) = objMuestra.lngHistoria 'NumHistoria
'          qry2(1) = .strCodAct
'          Set rs2 = qry2.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'          i = 0
'          Do While Not rs2.EOF And i < 2
'            i = i + 1
'            Select Case i
'              Case 1: .strProperty1 = rs2("resultadoalfanumerico")
'              Case 2: .strProperty2 = rs2("resultadoalfanumerico")
'            End Select
'            rs2.MoveNext
'          Loop
'          rs2.Close: qry2.Close
'        End With
      Next
NextMuestra:
    End With
  Next
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(App, "Cargar_ResultadosAnteriores_Tiempos")
End Sub
