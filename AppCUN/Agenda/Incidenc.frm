VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmIncidencias 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Incidencias"
   ClientHeight    =   5625
   ClientLeft      =   2595
   ClientTop       =   1890
   ClientWidth     =   7335
   ClipControls    =   0   'False
   Icon            =   "Incidenc.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5625
   ScaleWidth      =   7335
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Cancelar"
      Height          =   495
      Index           =   1
      Left            =   5475
      TabIndex        =   3
      Top             =   4950
      Width           =   1575
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Aceptar"
      Height          =   495
      Index           =   0
      Left            =   3555
      TabIndex        =   2
      Top             =   4950
      Width           =   1575
   End
   Begin VB.TextBox txtText1 
      Height          =   1695
      Index           =   2
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   2880
      Width           =   6855
   End
   Begin VB.TextBox txtText1 
      Height          =   1095
      Index           =   1
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   1200
      Width           =   6855
   End
   Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
      Height          =   330
      Index           =   0
      Left            =   270
      TabIndex        =   7
      Tag             =   "Descripci�n"
      Top             =   375
      Width           =   6825
      DataFieldList   =   "Column 0"
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ","
      DefColWidth     =   4471
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   4471
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   12039
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasForeColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   12039
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "M�todo de soluci�n propuesto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   240
      TabIndex        =   6
      Top             =   2640
      Width           =   2580
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Descripci�n del alcance"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   240
      TabIndex        =   5
      Top             =   960
      Width           =   2070
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Descripci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   1020
   End
End
Attribute VB_Name = "frmIncidencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Procedimiento que creara un nuevo registro de incidencia en la
'tabla de incidencias.
'***************************************************
'No se si al final lo tendra que generar la libreria
'***************************************************

Sub A�adir_Incidencia()
  Dim strSql As String
  Dim rdoQueryInc As rdoQuery
  Dim rdoIncidencias As rdoResultset

' Abrimos cursor
  strSql = "select AG05NUMINCIDEN,AG06CODMOTINCI, " _
         & "AG05DESINCIDEN, AG05DESALCANCE,AG05DESSOLUCIO," _
         & "AG05SITINCIDEN from AG0500 order by AG05NUMINCIDEN desc"
   
  Set rdoQueryInc = objApp.rdoConnect.CreateQuery("", strSql)

 'Establezco el n� de registros a recuperar a 1
  rdoQueryInc.MaxRows = 1
  Set rdoIncidencias = rdoQueryInc.OpenResultset(rdOpenKeyset, rdConcurRowVer)
 
   
' Obtenemos un n�mero de incidencia
  If rdoIncidencias.RowCount = 0 Then
    vntNuevoCod = 1
  Else
    vntNuevoCod = rdoIncidencias("AG05NUMINCIDEN") + 1
  End If
 
' Damos de alta la incidencia
'� Sera una funci�n de la libreria ?
  With frmIncidencias
    rdoIncidencias.AddNew
    rdoIncidencias("AG05NUMINCIDEN") = vntNuevoCod
    rdoIncidencias("AG06CODMOTINCI") = intCodMotInci
    rdoIncidencias("AG05DESINCIDEN") = .cboSSDBCombo1(0).Text
    rdoIncidencias("AG05DESALCANCE") = .txtText1(1).Text & " "
    rdoIncidencias("AG05DESSOLUCIO") = .txtText1(2).Text & " "
    rdoIncidencias("AG05SITINCIDEN") = "P"  'No se que c�digo va aqui
    rdoIncidencias.Update
    rdoIncidencias.Close
  End With
 'Vuelvo a inicializar el c�digo de incidencia
  intCodMotInci = 0

End Sub

'La ventana de Incidencias se meustra cuando se ha producido un
'cambio de disponibilidad en el sistema y una vez rellenado los datos
'devuelve el c�digo de incidencia en la variable global del proyecto
'vntNuevoCod.

'Esta ventana hay que abrirla con el metodo Load y no con el metodo
'Show

'Puede darse el caso que el c�digo de motivo de incidencia pasado
'a traves de la variable global intCodMotInci no exista en la
'tabla de C�digos de motivo de incidencia en cuyo caso
'sale un mensaje adviertindolo y  se pone la
'variable global blnRollBack a True y no se graba la incidencia

'Tambien puede darse el caso de que el c�digo de incidencia no
'este activo (Fecha Inicio > Fecha Hoy ) en cuyo caso la variable
'global blnActive se pone a False y la modificaci�n del registro
'se lleva a cabo pero no se crea un regsitro de incidencia.

'Si el usuario cancela la incidencia pulsando el boton de Cancelar
'la variable Global blnRollback se pone a True y no se graba el registro
'de incidencia


'Por lo tanto para que se genere un regsitro de incidencia y devuelva
'el c�digo de incidencia en vntNuevoCod la variable blnActive tiene
'que valer True y la  variable blnRollback tiene que valer False.

Private Sub Form_Load2()
' Variable resultset y query para la descripci�n del motivo
  Dim rdoMotivoInci As rdoResultset
  Dim rdoQueryMotivo As rdoQuery
  
  Dim rdoTiposIncidencia As rdoResultset
' Variable para la sentencia Sql
  Dim strSql As String
' Inicializamos blnRollback a false
  blnRollback = False
  blnActive = True
' Abrimos el cursor para obtener la descripci�n del motivo
  
  strSql = "select AG06DESMOTINCI,AG06INDINCDISM, AG06FECACTIVA from AG0600 " _
         & "where AG06CODMOTINCI = " & intCodMotInci
  
  Set rdoQueryMotivo = objApp.rdoConnect.CreateQuery("", strSql)
 'Establecemos el tama�o del cursor a 1
  rdoQueryMotivo.MaxRows = 1
  Set rdoMotivoInci = rdoQueryMotivo.OpenResultset(rdOpenKeyset, _
                                                rdConcurReadOnly)
  
' Miramos si existe el c�digo del motivo de incidencia
  If rdoMotivoInci.RowCount = 0 Then
    'No existe el codigo de motivo incidencia
     Call objError.SetError(cwCodeMsg, "No existe el c�digo de motivo incidencia N�" & intCodMotInci)
     Call objError.Raise
     
    ' MsgBox "No existe el c�digo de motivo incidencia N�" & intCodMotInci, vbExclamation, "AGENDA. Incidencias"
     blnRollback = True
     Exit Sub
  Else
    'Si la fecha activa del c�digo de incidencia es
    'menor que la actual visualizamos el form
    If CDate(rdoMotivoInci("AG06FECACTIVA")) < CDate(Format(objGen.GetDBDateTime, "dd/mm/yyyy")) Then
      If blnHideFrmInciden Then
        Call A�adir_Incidencia
        blnHideFrmInciden = False
      Else
        'Asignamos la descripci�n del motivo de incidencia
        If Val(rdoMotivoInci("AG06INDINCDISM")) = 1 Then
          cboSSDBCombo1(0).Text = rdoMotivoInci("AG06DESMOTINCI")
          cboSSDBCombo1(0).Enabled = False
          Me.Height = 6000
          cmdCommand1(0).Top = 4900
          cmdCommand1(1).Top = 4900
          txtText1(2).Visible = True
          lblLabel1(2).Visible = True
          lblLabel1(1).Caption = "Descripci�n del alcance"
        Else
          Me.Height = 3700
          cmdCommand1(0).Top = 2700
          cmdCommand1(1).Top = 2700
          txtText1(2).Visible = False
          lblLabel1(1).Caption = "Descripci�n"
          lblLabel1(2).Visible = False
          strSql = "SELECT AG06CODMOTINCI, AG06DESMOTINCI,AG06INDINCDISM FROM AG0600 WHERE AG06INDINCDISM=" & Val(rdoMotivoInci("AG06INDINCDISM")) & " AND AG06FECACTIVA < " & objGen.ValueToSQL(Date, cwDate)
          Set rdoTiposIncidencia = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly, rdConcurReadOnly)
          Do While Not rdoTiposIncidencia.EOF
            Call cboSSDBCombo1(0).AddItem(rdoTiposIncidencia(0) & "," & rdoTiposIncidencia(1))
            rdoTiposIncidencia.MoveNext
          Loop
          cboSSDBCombo1(0).Value = intCodMotInci
          rdoTiposIncidencia.Close
        End If
      
        Set rdoTiposIncidencia = Nothing
        Me.Show (vbModal)
      
        Me.MousePointer = vbDefault
      End If
    Else
     'Si la fecha es mayor cancelamos la incidencia
     'pero no realizamos rollback
      blnActive = False
      Exit Sub
    End If
  End If

End Sub
Private Sub cmdCommand1_Click(intIndex As Integer)
 'Variable que guarda la respuesta al messagebox
  Dim intRespuesta As Integer
  
  If intIndex = 0 Then
  ' Bot�n Aceptar
    blnRollback = False
  'A�adimos un nuevo registro de incidencia
    Call A�adir_Incidencia
    Me.Hide
  Else
  'Bot�n Cancelar
     Call objError.SetError(cwCodeQuery, "Va a cancelar la grabaci�n de la incidencia. Los cambios realizados no se almacenaran �desea continuar?")
     intRespuesta = objError.Raise

    'intRespuesta = MsgBox("Va a cancelar la grabaci�n de la incidencia. Los cambios realizados no se almacenaran �desea continuar?", vbYesNo + vbExclamation, "AGENDA. Incidencias")
    'Si la respuesta es "si" entonces pongo la variable
    'blnActive a true para cancelar la incidenica
    If intRespuesta = vbYes Then
      blnRollback = True
     'Vuelvo a inicializar el c�digo de incidencia
      intCodMotInci = 0
      Me.Hide
    End If
  End If
End Sub
Public Sub ConfigureWindow(intTypeWin As Integer)
      
  If intTypeWin = 1 Then
    cboSSDBCombo1(0).Enabled = False
    Height = 6000
    cmdCommand1(0).Top = 4900
    cmdCommand1(1).Top = 4900
    txtText1(2).Visible = True
    lblLabel1(2).Visible = True
    lblLabel1(1).Caption = "Descripci�n del alcance"
  Else
    Height = 3700
    cmdCommand1(0).Top = 2700
    cmdCommand1(1).Top = 2700
    txtText1(2).Visible = False
    lblLabel1(1).Caption = "Descripci�n"
    lblLabel1(2).Visible = False
   End If
      

End Sub

