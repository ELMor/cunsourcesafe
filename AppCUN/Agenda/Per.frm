VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Begin VB.Form frmPerfiles 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Calendarios"
   ClientHeight    =   7860
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11625
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7860
   ScaleWidth      =   11625
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraFrame1 
      Caption         =   "Frame2"
      Height          =   2295
      Index           =   2
      Left            =   240
      TabIndex        =   13
      Top             =   5280
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         Index           =   2
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   3201
         _Version        =   327680
         Style           =   1
         Tabs            =   2
         TabHeight       =   520
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "Per.frx":0000
         Tab(0).ControlCount=   1
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "txtText1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "Per.frx":001C
         Tab(1).ControlCount=   1
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG02CODCALENDA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   4
            Left            =   240
            TabIndex        =   25
            Tag             =   "C�digo Calendario|C�digo"
            Top             =   840
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1395
            Index           =   2
            Left            =   -74880
            TabIndex        =   24
            Top             =   360
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   2461
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327680
      BorderStyle     =   1
      MouseIcon       =   "Per.frx":0038
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Frame1"
      Height          =   2295
      Index           =   1
      Left            =   120
      TabIndex        =   12
      Top             =   2880
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   1935
         Index           =   1
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   3413
         _Version        =   327680
         Style           =   1
         Tabs            =   2
         TabHeight       =   520
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "Per.frx":0054
         Tab(0).ControlCount=   7
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chkCheck1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkCheck1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Command1"
         Tab(0).Control(6).Enabled=   0   'False
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "Per.frx":0070
         Tab(1).ControlCount=   1
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).Control(0).Enabled=   0   'False
         Begin VB.CommandButton Command1 
            Caption         =   "Franjas"
            Height          =   375
            Left            =   9000
            TabIndex        =   22
            Top             =   1440
            Width           =   1455
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Activo"
            DataField       =   "AG02INDCALPRIN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   8400
            TabIndex        =   21
            Tag             =   "Indicador Calendario Principal|Indicador"
            Top             =   840
            Width           =   885
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Perfil Gen�rico"
            DataField       =   "AG02INDCALPRIN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   6480
            TabIndex        =   20
            Tag             =   "Indicador Calendario Principal|Indicador"
            Top             =   840
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG02DESCALENDA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   2040
            TabIndex        =   17
            Tag             =   "Descripci�n Calendario|Descripci�n"
            Top             =   840
            Width           =   4230
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG02CODCALENDA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   2
            Left            =   480
            TabIndex        =   16
            Tag             =   "C�digo Calendario|C�digo"
            Top             =   840
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1395
            Index           =   1
            Left            =   -74880
            TabIndex        =   23
            Top             =   360
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   2461
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   2040
            TabIndex        =   19
            Top             =   600
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   480
            TabIndex        =   18
            Top             =   600
            Width           =   600
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Calendarios"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2325
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   3201
         _Version        =   327680
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "Per.frx":008C
         Tab(0).ControlCount=   7
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(4)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtcDateCombo1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkCheck1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "Per.frx":00A8
         Tab(1).ControlCount=   1
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Calendario principal"
            DataField       =   "AG02INDCALPRIN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   6480
            TabIndex        =   2
            Tag             =   "Indicador Calendario Principal|Indicador"
            Top             =   840
            Width           =   2085
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG02DESCALENDA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   1890
            TabIndex        =   1
            Tag             =   "Descripci�n Calendario|Descripci�n"
            Top             =   870
            Width           =   4230
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG02CODCALENDA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   480
            TabIndex        =   0
            Tag             =   "C�digo Calendario|C�digo"
            Top             =   870
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1395
            Index           =   0
            Left            =   -74880
            TabIndex        =   5
            Top             =   360
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   2461
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG02FECBAJA"
            Height          =   330
            Index           =   3
            Left            =   8760
            TabIndex        =   3
            Tag             =   "Fecha Baja Calendario|Fecha Baja"
            Top             =   840
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Baja"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   8760
            TabIndex        =   11
            Top             =   600
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1890
            TabIndex        =   9
            Top             =   645
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   450
            TabIndex        =   8
            Top             =   645
            Width           =   600
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   7575
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327680
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
      MouseIcon       =   "Per.frx":00C4
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPerfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


' Procedimiento que devuelve el c�digo de motivo de incidencia
' en la variable global intCodMotInci si hay disminuci�n de
' disponibilidad, si no hay devuelve 0 en intCodMotInci

'Posibles valores en la tabla de motivos de incidencia (AG0600)

'AG06CODMOTINCI AG06DESMOTINCI                           AG06INDINCDISM
'-------------- ---------------------------------------- --------------
'            10 Cambio en periodo de vigencia                         1
'            20 Cambio a festivo de un d�a especial                   1                                   2
'            30 Nuevo per�odo de vigencia                             1
'            40 Nuevo d�a especial                                    1
'            50 Borrado de d�a especial no festivo                       1
'            60 Borrado de per�odo de vigencia                        1
'            70 Disminuci�n de Recursos                               1
'            80 Borrado de Recurso                                    1

Sub Disminucion_Periodos()
' Inicializo intCodMotInci
  intCodMotInci = 0
 
  With objWinInfo.objWinActiveForm
    If objGen.GetRowCount(.rdoCursor) = 0 Then
        intCodMotInci = 30
        Exit Sub
    End If
  ' Comprobaci�n Fecha Inicio
    If CDate(.rdoCursor("AG08FECINIPERI")) <> CDate(dtcDateCombo1(0).Text) Then
      intCodMotInci = 10 ' Para Probar
    End If
  ' Comprobaci�n Fecha Fin
    If CDate(.rdoCursor("AG08FECFINPERI")) <> CDate(dtcDateCombo1(1).Text) Then
      intCodMotInci = 10 ' Para Probar
    End If
  ' Comprobaci�n cambio dias festivos
  ' Lunes
    If chkCheck1(2).Value <> .rdoCursor("AG08INDLUNFEST") Then
      intCodMotInci = 10 ' Para Probar
    End If
  End With
End Sub

' Procedimiento que devuelve el c�digo de motivo de incidencia
' en la variable global intCodMotInci si hay disminuci�n de
' disponibilidad, si no hay devuelve 0 en intCodMotInci

Sub Disminucion_DiasEspeciales()
' Inicializo intCodMotInci
  intCodMotInci = 0
  With objWinInfo.objWinActiveForm
  ' Si se a�ade un nuevo registro
    If objGen.GetRowCount(.rdoCursor) = 0 Then
        intCodMotInci = 40
        Exit Sub
    End If
  ' Comprobaci�n cambio dias festivos
    If chkCheck1(9).Value <> .rdoCursor("AG03INDFESTIVO") Then
      intCodMotInci = 20 ' Para Probar
    End If
  End With
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form maestro
  Dim objMasterInfo As New clsCWForm
' Form detalle 1
  Dim objDetailInfo1 As New clsCWForm
' Form detalle 2
  Dim objDetailInfo2 As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  
' **********************************
' Fin declaraci�n de variables
' **********************************
  blnRollback = False
' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Calendarios"
    .cwDAT = "11-07-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite mantener los diferentes calendarios de la CUN"
    
    .cwUPD = "11-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form padre
  With objMasterInfo
   ' Asigno nombre al frame
    .strName = "Calendario"

  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0200"

  
  ' Asignaci�n del nivel de acceso del usuario al formulario
  '  .intAllowance = cwAllowReadOnly
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG02CODCALENDA", cwAscending)
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Calendarios")
    Call .FormAddFilterWhere(strKey, "AG02DESCALENDA", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AG02INDCALPRIN", "Indicador Calendario Principal", cwString)
    Call .FormAddFilterWhere(strKey, "AG02FECBAJA", "Fecha Baja", cwString)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder("AG02DESCALENDA", "Descripci�n")
    Call .FormAddFilterOrder("AG02INDCALPRIN", "Indicador Calendario Principal")
    Call .FormAddFilterOrder("AG02FECBAJA", "Fecha Baja")
  
  End With
  
' Declaraci�n de las caracter�sticas del form detalle1
  With objDetailInfo1
    ' Asigno nombre al frame
    .strName = "Per�odo_Vigencia"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
  ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(3)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0800"
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG08CODPERVIGE", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("AG02CODCALENDA", txtText1(0))
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Periodo Vigencia Calendario")
    Call .FormAddFilterWhere(strKey, "AG08DESPERVIGE", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AG08FECINIPERI", "Fecha Desde", cwString)
    Call .FormAddFilterWhere(strKey, "AG08FECFINPERI", "Fecha Hasta", cwString)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder("AG08DESPERVIGE", "Descripci�n")
    Call .FormAddFilterOrder("AG08FECINIPERI", "Fecha Desde")
    Call .FormAddFilterOrder("AG08FECFINPERI", "Fecha Hasta")

  End With
   
' Declaraci�n de las caracter�sticas del form detalle2
  With objDetailInfo2
   ' Asigno nombre al frame
    .strName = "D�a_Especial"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(2)
  ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(2)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(4)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0300"
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG03FECDIAESPE", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("AG02CODCALENDA", txtText1(0))
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de D�as Especiales")
    Call .FormAddFilterWhere(strKey, "AG03FECDIAESPE", "Fecha D�a Especial", cwString)
    Call .FormAddFilterWhere(strKey, "AG03DESDIAESPE", "Fecha", cwString)
   
  ' Creaci�n de los criterios de ordenaci�n
    
    Call .FormAddFilterOrder("AG03FECDIAESPE", "Fecha D�a Especial")
    Call .FormAddFilterOrder("AG03DESPDIAESPE", "Descripci�n")

  End With
  
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
  ' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo1, cwFormDetail)
    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
  ' Se obtiene informaci�n de las caracter�sticas de
  ' los controles del formulario
    Call .FormCreateInfo(objMasterInfo)
        
  ' Campos que intervienen en busquedas
  '  .CtrlGetInfo(chkCheck1(0)).blnInFind = True
  '  .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
  '  .CtrlGetInfo(txtText1(2)).blnInFind = True
  '  .CtrlGetInfo(txtText1(4)).blnInFind = True
    
  
   ' Eliminamos campos del grid
     .CtrlGetInfo(txtText1(2)).blnInGrid = False
     .CtrlGetInfo(txtText1(10)).blnInGrid = False
     .CtrlGetInfo(txtText1(11)).blnInGrid = False
     .CtrlGetInfo(txtText1(20)).blnInGrid = False
     .CtrlGetInfo(txtText1(21)).blnInGrid = False
     .CtrlGetInfo(txtText1(22)).blnInGrid = False
  
  ' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
  ' Se estabiliza la ventana configurando las propiedades
  ' de los controles
    Call .WinStabilize
  
  End With
  
' Se oculta el formulario de splash
  Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
' Si se a�ade un nuevo registro de per�odo de vigencia
' o d�a especial le paso el c�digo del calendario
 If strFormName = "Per�odo_Vigencia" Then
   txtText1(10).Text = txtText1(0).Text
 End If
 If strFormName = "D�a_Especial" Then
   txtText1(11).Text = txtText1(0).Text
 End If
End Sub
' Validaciones del programa
Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)

' Validaciones del form Per�odo de Vigencia
  If strFormName = "Per�odo_Vigencia" Then
   
   ' Validaci�n de Fecha Fin mayor que Fecha Inicio
    If IsDate(dtcDateCombo1(1).Date) Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
         Call objError.SetError(cwCodeMsg, "La Fecha Fin es  menor que Fecha Inicial")
         Call objError.Raise
         blnCancel = True
      End If
    End If
  ' Comprobaci�n disminuci�n de disponibilidad
    Call Disminucion_Periodos
  End If
  
   

' Validaciones del form D�as Especiales
  If strFormName = "D�a_Especial" Then

  ' Validaci�n de Fecha Baja D�a Especial mayor que Fecha D�a Especial
    If IsDate(dtcDateCombo1(4).Date) Then
      If DateDiff("d", dtcDateCombo1(4).Date, dtcDateCombo1(2).Date) > 0 Then
         Call objError.SetError(cwCodeMsg, "La Fecha de Baja es menor que la Fecha del D�a Especial")
         Call objError.Raise
         blnCancel = True
      End If
    End If
  
  ' Comprobaci�n disminuci�n de disponibilidad
    Call Disminucion_DiasEspeciales
  
  End If

End Sub


Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Compruebo si el usuarario ha cancelado o no la incidencia para
' validar los cambios o anularlos
'  If Not blnCancelSave Then
    If blnRollback = True Then
      Call objApp.rdoEnv.RollbackTrans
      blnRollback = False
      
    Else
      Call objApp.rdoEnv.CommitTrans
    End If
'  End If
End Sub

Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
  intCodMotInci = 0
  If strFormName = "Per�odo_Vigencia" Then
    intCodMotInci = 60
  Else
    If objWinInfo.objWinActiveForm.rdoCursor("AG03INDFESTIVO").Value = 0 Then
      intCodMotInci = 50
    End If
  End If
' Carga de la ventana Incidencias
  Load frmIncidencias
      
  blnCancel = blnCancelIncidencia
' A�adimos un nuevo registro de incidencia
  If blnCancel = False Then
    Call A�adir_Incidencia
' Descargamos el form de incidencias de memoria
  End If
  Unload frmIncidencias
  
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Variable para las sentencia Sql que abrira el cursor
' para dar de alta la incidencia
  Dim strSql As String
' Variable que guardara el campo n� de incidencia del form activo
  Dim strCampoNumIncidencia As String
  
' Asignamos el campo n� de incidencia
  If strFormName = "Per�odo_Vigencia" Then
    strCampoNumIncidencia = "AG08NUMINCIDEN"
  Else
    strCampoNumIncidencia = "AG03NUMINCIDEN"
  End If
  ' Miramos si hay disminuci�n
    If intCodMotInci <> 0 Then
      
    ' Iniciamos la transacci�n
      Call objApp.rdoEnv.BeginTrans
    ' Carga de la ventana Incidencias
      Load frmIncidencias
      
    ' A�adimos un nuevo registro de incidencia
      If blnRollback = False Then
        Call A�adir_Incidencia
      ' Asignamos el n�mero de incidencia al campo del form activo
        objWinInfo.objWinActiveForm.rdoCursor(strCampoNumIncidencia) = lngNuevoCod
    ' Descargamos el form de incidencias de memoria
      End If
      Unload frmIncidencias
    End If
    
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
