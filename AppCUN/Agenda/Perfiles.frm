VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmPerfiles 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Perfiles"
   ClientHeight    =   7860
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11505
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "Perfiles.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7860
   ScaleWidth      =   11505
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Width           =   11505
      _ExtentX        =   20294
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Periodos de Vigencia"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Index           =   2
      Left            =   120
      TabIndex        =   20
      Tag             =   "Mantenimiento de Periodos de Vigencia por Perfil"
      Top             =   5280
      Width           =   11280
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         Index           =   2
         Left            =   120
         TabIndex        =   22
         Top             =   360
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   3201
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabHeight       =   520
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "Perfiles.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(4)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(5)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(20)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(22)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(21)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).ControlCount=   11
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "Perfiles.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG07CODPERFIL"
            Height          =   330
            HelpContextID   =   40101
            Index           =   21
            Left            =   1680
            TabIndex        =   37
            TabStop         =   0   'False
            Tag             =   "C�digo Calendario|C�digo"
            Text            =   "C�d Perfil"
            Top             =   1320
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG05NUMINCIDEN"
            Height          =   330
            HelpContextID   =   40101
            Index           =   22
            Left            =   3120
            TabIndex        =   36
            TabStop         =   0   'False
            Tag             =   "C�digo Calendario|C�digo"
            Text            =   "N� Incidencia"
            Top             =   1320
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   20
            Left            =   360
            TabIndex        =   35
            TabStop         =   0   'False
            Tag             =   "C�digo Calendario|C�digo"
            Text            =   "C�d Recurso"
            Top             =   1320
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG09MOTIVOPERF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   6600
            TabIndex        =   7
            Tag             =   "Motivo"
            Top             =   840
            Width           =   3975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG09NUMPVIGPER"
            Height          =   330
            HelpContextID   =   40101
            Index           =   4
            Left            =   480
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "C�digo Periodo|C�digo"
            Top             =   840
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1395
            Index           =   2
            Left            =   -74880
            TabIndex        =   26
            Top             =   360
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   2461
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG09FECINVIPER"
            Height          =   330
            Index           =   0
            Left            =   2025
            TabIndex        =   5
            Tag             =   "Fecha Inicio Periodo|Fecha Inicio"
            Top             =   840
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG09FECFIVIPER"
            Height          =   330
            Index           =   1
            Left            =   4275
            TabIndex        =   6
            Tag             =   "Fecha Fin Periodo|Fecha Fin"
            Top             =   840
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Motivo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   6600
            TabIndex        =   30
            Top             =   600
            Width           =   585
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Hasta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   4320
            TabIndex        =   29
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Desde"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   2040
            TabIndex        =   28
            Top             =   600
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   480
            TabIndex        =   27
            Top             =   600
            Width           =   600
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Perfiles"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Index           =   1
      Left            =   120
      TabIndex        =   19
      Tag             =   "Mantenimiento de Perfiles de Recursos"
      Top             =   2880
      Width           =   11280
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         Index           =   1
         Left            =   120
         TabIndex        =   21
         Top             =   345
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   3201
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabHeight       =   520
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "Perfiles.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(9)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtcDateCombo1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(3)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkCheck1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cmdCommand1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(10)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(11)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "Perfiles.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG05NUMINCIDEN"
            Height          =   330
            HelpContextID   =   40101
            Index           =   11
            Left            =   2040
            TabIndex        =   34
            TabStop         =   0   'False
            Tag             =   "C�digo Calendario|C�digo"
            Text            =   "N� Incidencia"
            Top             =   1320
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   10
            Left            =   480
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   "C�digo Calendario|C�digo"
            Text            =   "C�d Recurso"
            Top             =   1320
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.CommandButton cmdCommand1 
            Caption         =   "Franjas"
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   9120
            TabIndex        =   3
            Top             =   1320
            Width           =   1530
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Perfil Gen�rico"
            DataField       =   "AG07INDPERGENE"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   6615
            TabIndex        =   1
            Tag             =   "Indicador Perfil Gen�rico"
            Top             =   705
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG07DESPERFIL"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   2040
            TabIndex        =   0
            Tag             =   "Descripci�n Perfil|Descripci�n"
            Top             =   735
            Width           =   4230
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG07CODPERFIL"
            Height          =   330
            HelpContextID   =   40101
            Index           =   2
            Left            =   465
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "C�digo Perfil|C�digo Perfil"
            Top             =   720
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1395
            Index           =   1
            Left            =   -74880
            TabIndex        =   25
            Top             =   360
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   2461
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG07FECBAJA"
            Height          =   330
            Index           =   2
            Left            =   8775
            TabIndex        =   2
            Tag             =   "Fecha Baja Perfl|Fecha Baja"
            Top             =   720
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Baja"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   8760
            TabIndex        =   32
            Top             =   480
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   2040
            TabIndex        =   24
            Top             =   480
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   480
            TabIndex        =   23
            Top             =   480
            Width           =   600
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Recursos"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2325
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   480
      Width           =   11280
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   360
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   3201
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "Perfiles.frx":007C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtcDateCombo1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cboSSDBCombo1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "Perfiles.frx":0098
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG11DESRECURSO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   2130
            TabIndex        =   9
            Tag             =   "Descripci�n Recurso|Descripci�n"
            Top             =   885
            Width           =   5430
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   480
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "C�digo Recurso|C�digo"
            Top             =   870
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1395
            Index           =   0
            Left            =   -74880
            TabIndex        =   13
            Top             =   360
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   2461
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "AG14CODTIPRECU"
            Height          =   330
            Index           =   0
            Left            =   8175
            TabIndex        =   10
            Tag             =   "Tipo Recurso"
            Top             =   870
            Width           =   2415
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   4260
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   4260
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG11FECFINVREC"
            Height          =   330
            Index           =   3
            Left            =   3300
            TabIndex        =   38
            Tag             =   "Fecha Fin Vigencia (DD/MM/YYYY)"
            Top             =   1365
            Visible         =   0   'False
            Width           =   1935
            _Version        =   65537
            _ExtentX        =   3413
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1997/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BevelColorFace  =   12632256
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   8160
            TabIndex        =   31
            Top             =   645
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   2115
            TabIndex        =   17
            Top             =   645
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   480
            TabIndex        =   16
            Top             =   645
            Width           =   600
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   7575
      Width           =   11505
      _ExtentX        =   20294
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPerfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

'******************************************************************
' Procedimiento que devuelve el c�digo de motivo de incidencia
' en la variable global intCodMotInci si hay disminuci�n de
' disponibilidad, si no hay devuelve 0 en intCodMotInci
'******************************************************************

Sub Disminucion_Perfiles()
' Inicializo intCodMotInci
'  intCodMotInci = 0
  
'  With objWinInfo.objWinActiveForm
  ' Comprobaci�n cambio perfil gen�rico
'    If -chkCheck1(0).Value <> .rdoCursor("AG07INDPERGENE") Then
'      intCodMotInci = 90 ' Para Probar
'    End If
  ' Comprobaci�n cambio Fecha Baja
'    If CDate(.rdoCursor("AG07FECBAJA")) <> dtcDateCombo1(2).Date Then
'        intCodMotInci = 100 ' Para Probar
'      End If
'   End With
End Sub


Private Sub cmdCommand1_Click(intIndex As Integer)
'******************************************************************
' Si pulsan el bot�n de franjas miro si ha habido modificaciones
' en perfiles
'******************************************************************
  If objWinInfo.objWinActiveForm.blnChanged = True Then
    objWinInfo.DataRefresh
  End If
'******************************************************************
' Obtengo el c�digo de recurso y c�digo de perfil para pasarselos a
' la ventana de franjas
'******************************************************************
  With objWinInfo.objWinActiveForm
    lngCodRecurso = .rdoCursor("AG11CODRECURSO")
    lngCodPerfil = .rdoCursor("AG07CODPERFIL")
  End With
' Muestro la ventana de Franjas
  Load frmFranjas
  Call frmFranjas.Show(vbModal)
  Unload frmFranjas
  Set frmFranjas = Nothing
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form maestro
  Dim objMasterInfo As New clsCWForm
' Form detalle 1
  Dim objDetailInfo1 As New clsCWForm
' Form detalle 2 ( detalle de detale 1 )
  Dim objDetailInfo2 As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  
' **********************************
' Fin declaraci�n de variables
' **********************************
  blnRollback = False
' Se visualiza el formulario de splash
  'Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Perfiles"
    .cwDAT = "23-07-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite mantener los diferentes perfirles y periodos de vigencia por perfil de la CUN"
    
    .cwUPD = "23-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form maestro
  With objMasterInfo
   ' Asigno nombre al frame
    .strName = "Recurso"

  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1100"
    .strInitialWhere = strWherePerfInicial
    .strWhere = strWherePerfiles
  ' Asignaci�n del nivel de acceso del usuario al formulario
    .intAllowance = cwAllowReadOnly

  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG11CODRECURSO", cwAscending)
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Recursos")
    Call .FormAddFilterWhere(strKey, "AG11CODRECURSO", "C�digo Recurso", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG11DESRECURSO", "Descripci�n Recurso", cwString)
    Call .FormAddFilterWhere(strKey, "AG14CODTIPRECU", "Tipo Recurso", cwNumeric)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder(strKey, "AG11CODRECURSO", "C�digo Recurso")
    Call .FormAddFilterOrder(strKey, "AG11DESRECURSO", "Descripci�n Recurso")
    Call .FormAddFilterOrder(strKey, "AG14CODTIPRECU", "Tipo Recurso")
 
  End With
  
' Declaraci�n de las caracter�sticas del form detalle1
  With objDetailInfo1
    ' Asigno nombre al frame
    .strName = "Perfil"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
  ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0700"
    .blnAskPrimary = False
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG07CODPERFIL", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("AG11CODRECURSO", txtText1(0))
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Perfiles")
    Call .FormAddFilterWhere(strKey, "AG11CODRECURSO", "C�digo Recurso", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG07DESPERFIL", "Descripci�n Perfil", cwString)
    Call .FormAddFilterWhere(strKey, "AG07INDPERGENE", "Perfil Gen�rico", cwBoolean)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder(strKey, "AG11CODRECURSO", "C�digo Recurso")
    Call .FormAddFilterOrder(strKey, "AG07DESPERFIL", "Descripci�n Perfil")
    Call .FormAddFilterOrder(strKey, "AG07INDPERGENE", "Perfil Gen�rico")
  ' Definici�n del impreso
    Call .objPrinter.Add("AG0004", "Listado 1 de Perfiles de un Recurso")

  End With
   
' Declaraci�n de las caracter�sticas del form detalle2
  With objDetailInfo2
   ' Asigno nombre al frame
    .strName = "Periodo_Vigencia"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(2)
  ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(1)
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(2)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(2)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0900"
    .blnAskPrimary = False
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG09NUMPVIGPER", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("AG11CODRECURSO", txtText1(0))
    Call .FormAddRelation("AG07CODPERFIL", txtText1(2))
 ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Per�odos de Vigencia")
    Call .FormAddFilterWhere(strKey, "AG09FECINVIPER", "Fecha Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "AG09FECFIVIPER", "Fecha Fin Vigencia", cwDate)
   
  ' Creaci�n de los criterios de ordenaci�n
    
    Call .FormAddFilterOrder(strKey, "AG09FECINVIPER", "Fecha Inicio Vigencia")
    Call .FormAddFilterOrder(strKey, "AG09FECFIVIPER", "Fecha Fin Vigencia")

  End With
  
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
  ' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo1, cwFormDetail)
    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
  ' Se obtiene informaci�n de las caracter�sticas de
  ' los controles del formulario
    Call .FormCreateInfo(objMasterInfo)
        
  ' Campos que intervienen en busquedas
    
    'Form Recursos
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(0)).blnInFind = True
    
    'Form Perfiles
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    
  
   ' Eliminamos campos del grid
     .CtrlGetInfo(txtText1(10)).blnInGrid = False
     .CtrlGetInfo(txtText1(11)).blnInGrid = False
     .CtrlGetInfo(txtText1(20)).blnInGrid = False
     .CtrlGetInfo(txtText1(21)).blnInGrid = False
     .CtrlGetInfo(txtText1(22)).blnInGrid = False
   
   ' Propiedades del campo clave perfil para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(2)).blnValidate = False
    .CtrlGetInfo(txtText1(2)).blnInGrid = False
    
    ' Propiedades del campo clave periodo para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(4)).blnValidate = False
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
    
   
   ' Cargamos las combo
     .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AG14CODTIPRECU, AG14DESTIPRECU FROM " & objEnv.GetValue("Database") & "AG1400 ORDER BY AG14DESTIPRECU"


     'Campos del Form Recursos de solo lectura
     .CtrlGetInfo(txtText1(1)).blnReadOnly = True
     .CtrlGetInfo(cboSSDBCombo1(0)).blnReadOnly = True
     
  ' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
  ' Se estabiliza la ventana configurando las propiedades
  ' de los controles
    Call .WinStabilize
  End With
  
' Se oculta el formulario de splash
  'Call objApp.SplashOff
' Me posiciono en el primer registro
  Call objWinInfo.DataMoveFirst
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  
  Dim blnPerfilGen As Boolean
  Dim rdoPerfil As rdoResultset
  Dim rdoQueryPerfil As rdoQuery
  Dim strSql As String
  
  
'******************************************************************
' Compruebo si hay alg�n perfil gen�rico, si no lo hay saco
' un mensaje y vuelvo al form.
'******************************************************************

' Creo un nuevo cursor con los indicadores de perfil gen�rico
  strSql = "SELECT AG07INDPERGENE FROM " & objEnv.GetValue("Database") & "AG0700 " _
         & " WHERE AG11CODRECURSO=" & Val(txtText1(0))
  Set rdoQueryPerfil = objApp.rdoConnect.CreateQuery("", strSql)
  rdoQueryPerfil.MaxRows = 1
  Set rdoPerfil = rdoQueryPerfil.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
  
 'Si no he encontrado ning�n perfil gen�rico saco mensaje de error
  If objGen.GetRowCount(rdoPerfil) > 0 Then
     Call objError.SetError(cwCodeMsg, "No existe ning�n perfil gen�rico")
     Call objError.Raise
     'Cancelo el Query_Unload
     intCancel = 1
  End If
  rdoPerfil.Close
  
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'******************************************************************
' Si el form que se activa es Perfiles habilito el bot�n
' de franjas, si no lo desabilito
'******************************************************************
  If strFormName = "Perfil" Then
   'Miro primero si hay registros en el cursor
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
      cmdCommand1(0).Enabled = True
    End If
  Else
    cmdCommand1(0).Enabled = False
  End If
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
'******************************************************************
' Si el form que se activa es Perfiles miro si se esta a�adiendo
' un nuevo registro para deshabilitar el bot�n de franjas
'******************************************************************
  If strFormName = "Perfil" Then
    If intNewStatus = cwModeSingleAddRest Then
      cmdCommand1(0).Enabled = False
      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = False
      objWinInfo.CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = False
      
    End If
  ' Si pasamos a modo edici�n habilito el bot�n de franjas
    If intNewStatus = cwModeSingleEdit Then
      cmdCommand1(0).Enabled = True
    End If
  End If
  

End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
'******************************************************************
' Si se a�ade un nuevo registro de perfil o periodo de vigencia
' le paso el c�digo del recurso
'******************************************************************
 If strFormName = "Perfil" Then
   txtText1(10).Text = txtText1(0).Text
 End If
 
 If strFormName = "Periodo_Vigencia" Then
 ' C�d. Recurso
   txtText1(20).Text = txtText1(0).Text
 ' C�d. Perfil
   txtText1(21).Text = txtText1(2).Text
 End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
   
   If strFormName = "Recurso" Then
     If IsDate(dtcDateCombo1(3)) Then
       If DateDiff("d", dtcDateCombo1(3).Date, CDate(Format(objGen.GetDBDateTime, "dd/mm/yyyy"))) > 0 Then
         objWinInfo.cllWinForms("fraFrame1(1)").intAllowance = cwAllowReadOnly
         objWinInfo.cllWinForms("fraFrame1(2)").intAllowance = cwAllowReadOnly
       Else
         objWinInfo.cllWinForms("fraFrame1(1)").intAllowance = cwAllowAll
         objWinInfo.cllWinForms("fraFrame1(2)").intAllowance = cwAllowAll
       End If
    End If
   End If
 
 'Compruebo si estoy en el Perfil Gen�rico para desactivar el
 'indicador de perfil gen�rico y la fecha de baja
  If strFormName = "Perfil" Then
    If chkCheck1(0).Value = 1 And objWinInfo.intWinStatus = cwModeSingleEdit Then
      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = True
      objWinInfo.CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
      fraFrame1(2).Enabled = False
    
    Else
      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = False
      objWinInfo.CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = False
      fraFrame1(2).Enabled = True
   
   End If
   'Actualizo la pantallla para los cambios
   objWinInfo.WinPrepareScr
  End If
End Sub

' Validaciones del programa
Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)

  Dim rdoPeriodos As rdoResultset
  Dim strSql As String
  Dim vntCodPeriodo As Variant
  Dim rdoPerfil As rdoResultset
  Dim vntCodPerfil As Variant
  Dim dteFechaFinCursor As Date
  Dim dteFechaFinActual As Date

'******************************************************************
'******************************************************************
' Validaciones del form Per�odo de Vigencia
'******************************************************************
'******************************************************************
  If strFormName = "Periodo_Vigencia" Then
   
'******************************************************************
   'Validaci�n de Perfil Gen�rico solo un periodo vigencia
'******************************************************************
    If objWinInfo.cllWinForms("fraFrame1(1)").rdoCursor("AG07INDPERGENE") = -1 Then
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        Call objError.SetError(cwCodeMsg, "El Perfil Gen�rico solo puede tener un Per�odo de Vigencia")
        Call objError.Raise
        blnCancel = True
        Exit Sub
      End If
    End If
   
'******************************************************************
   ' Validaci�n de Fecha Fin mayor que Fecha Inicio
'******************************************************************
    If IsDate(dtcDateCombo1(1).Date) Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        Call objError.SetError(cwCodeMsg, "La Fecha Fin es  menor que Fecha Inicial")
        Call objError.Raise
        blnCancel = True
        Exit Sub
      End If
    End If
    
    
'******************************************************************
    'Comprobaci�n de que no hay solapamiento de Periodos de
    'Vigencia.(A no ser que sea el Perfil Gen�rico)
'******************************************************************
    
    'Si estamos modificando un registro ya existente guardo su
    'c�digo para poder saltarlo.
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      vntCodPeriodo = objWinInfo.objWinActiveForm.rdoCursor("AG09NUMPVIGPER")
    End If
    
    'Abro el cursor
    strSql = "SELECT AG09NUMPVIGPER, AG09FECINVIPER, AG09FECFIVIPER FROM " _
        & objEnv.GetValue("Database") & "AG0900 " _
        & " WHERE AG11CODRECURSO=" & Val(txtText1(0)) _
        & " AND AG07CODPERFIL=" & Val(txtText1(2))
    Set rdoPeriodos = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly, rdConcurReadOnly)
    'Miro si el registro en el que estoy es el que estoy comparando para
    'no compararlo consigo mismo
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      If Val(rdoPeriodos("AG09NUMPVIGPER")) = vntCodPeriodo Then
        rdoPeriodos.MoveNext
      End If
    End If
    If dtcDateCombo1(1).Date = "" Then
      dteFechaFinActual = dtcDateCombo1(1).MaxDate
    Else
      dteFechaFinActual = dtcDateCombo1(1).Date
    End If
    'Recorro el cursor hasta el final o hasta que encuentre
    'un perfil gen�rico
    While Not rdoPeriodos.EOF And Not blnCancel
     'Si el campo fecha fin vigencia es null entonces le asigno
     'la fecha maxima (30/12/2100)
      If IsNull(rdoPeriodos("AG09FECFIVIPER")) Then
        dteFechaFinCursor = dtcDateCombo1(1).MaxDate
      Else
        dteFechaFinCursor = CDate(rdoPeriodos("AG09FECFIVIPER"))
      End If
     
     'Si es el perfil gen�rico no es error
      If objWinInfo.cllWinForms("fraFrame1(1)").rdoCursor("AG07INDPERGENE") = -1 Then
        rdoPeriodos.MoveNext
      Else
       'Compruebo que no hay solapamiento de fechas
        If dtcDateCombo1(0).Date <= dteFechaFinCursor Then
          If dteFechaFinActual >= CDate(rdoPeriodos("AG09FECINVIPER")) Then
            'Hay solapamiento
            blnCancel = True
            Call objError.SetError(cwCodeMsg, "Alguna fecha se solapa con el periodo con c�digo " & rdoPeriodos("AG09NUMPVIGPER"))
            Call objError.Raise
            Exit Sub
          End If
        End If
        rdoPeriodos.MoveNext
      End If
     'Miro si el primer registro del recordset coincide con el
     'registro que acabo de modificar. Si es as�, paso al siguiente
     'registro
      If objWinInfo.intWinStatus = cwModeSingleEdit And Not rdoPeriodos.EOF Then
        If Val(rdoPeriodos("AG09NUMPVIGPER")) = vntCodPeriodo Then
          rdoPeriodos.MoveNext
        End If
      End If
    
   Wend
 
  End If
  
'******************************************************************
'******************************************************************
' Validaciones del form Perfiles
'******************************************************************
'******************************************************************
  
  If strFormName = "Perfil" Then

'******************************************************************
  ' Comprobaci�n de Perfil Unico �nico
'******************************************************************
'    If chkCheck1(0).Value = 1 Then
      'Abrimos cursor
'      strSql = "SELECT AG07CODPERFIL, AG07INDPERGENE FROM " _
'        & objEnv.GetValue("Database") & "AG0700 WHERE AG11CODRECURSO=" _
'        & Val(txtText1(0))
'      Set rdoPerfil = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly, rdConcurReadOnly)
      'Si estamos en modo edici�n guardo el c�digo de calendario
'      If objWinInfo.intWinStatus = cwModeSingleEdit Then
'        vntCodPerfil = objWinInfo.objWinActiveForm.rdoCursor("AG07CODPERFIL")
'      End If
      'Si el que estoy comparando es el primero me lo salto
'      If rdoPerfil("AG07CODPERFIL") = vntCodPerfil Then
'        rdoPerfil.MoveNext
'      End If
      'Recorro el cursor
'      While Not rdoPerfil.EOF And Not blnCancel
        'Si hay algun otro calendario principal saco mensaje de error
'        If rdoPerfil("AG07INDPERGENE") = -1 Then
'          blnCancel = True
'          Call objError.SetError(cwCodeMsg, "Solo puede haber un perfil gen�rico !")
'          Call objError.Raise
 '         Exit Sub
 '       Else
'          rdoPerfil.MoveNext
'        End If
        'Salto si es el registro con el que estoy comparando
'        If objWinInfo.intWinStatus = cwModeSingleEdit And Not rdoPerfil.EOF Then
'          If rdoPerfil("AG07CODPERFIL") = vntCodPerfil Then
 '           rdoPerfil.MoveNext
 '         End If
 '       End If
 '     Wend
'******************************************************************
      'Comprobaci�n de que el perfil gen�rico no tiene m�s de un
      'periodo de vigencia
'******************************************************************
 '     If objWinInfo.cllWinForms("fraFrame1(2)").rdoCursor.RowCount > 1 Then
 '       blnCancel = True
 '       Call objError.SetError(cwCodeMsg, "El Perfil Gen�rico solo puede tener un Periodo de Vigencia")
 '       Call objError.Raise
 '       Exit Sub
 '     End If
 '   End If
  
'******************************************************************
  ' Comprobaci�n disminuci�n de disponibilidad solo si estamos en edici�n
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      Call Disminucion_Perfiles
    End If
  
  End If
  
 
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'******************************************************************
' Compruebo si el usuarario ha cancelado o no la incidencia para
' validar los cambios o anularlos
'******************************************************************
  If blnRollback = True Or blnError = True Then
    Call objApp.rdoConnect.RollbackTrans
    blnRollback = False
      
  Else
    Call objApp.rdoConnect.CommitTrans
  ' Si el form es Perfiles habilito el bot�n de franjas
    If strFormName = "Perfil" Then
      cmdCommand1(0).Enabled = True
    End If
  End If
End Sub

Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
'******************************************************************
 'Comprobaci�n de que un Perfil no tenga ning�n periodo de
 'vigencia asociado
'******************************************************************
  If strFormName = "Perfil" Then
    
    If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(2)").rdoCursor) > 0 Then
      Call objError.SetError(cwCodeMsg, "No se puede borrar un Perfil que tiene asociado alg�n Periodo de Vigencia")
      Call objError.Raise
      blnCancel = True
      Exit Sub
    End If
  
  End If
End Sub


Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Variable para las sentencia Sql que abrira el cursor
' para dar de alta la incidencia
  Dim strSql As String
' Variable que guardara el campo n� de incidencia del form activo
  Dim strCampoNumIncidencia As String
  Dim rdoPerfil As rdoResultset
  
'******************************************************************
'******************************************************************
' Actuaciones en el form Perfiles antes de la actualizaci�n
'******************************************************************
'******************************************************************
  
  If strFormName = "Perfil" Then
    
    'Asigno a strCampoNumIncidencia el campo de n� incidencia del
    'form Perfil
     strCampoNumIncidencia = "AG05NUMINCIDEN"
        
'******************************************************************
   'Si estamos a�adiendo un nuevo registro obtenemos su c�digo
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      'Creo la Sql para obtener el nuevo c�digo
      strSql = "SELECT AG07CODPERFIL FROM " & objEnv.GetValue("Database") & "AG0700 " _
             & " WHERE AG11CODRECURSO=" & Val(txtText1(0)) & " ORDER BY AG07CODPERFIL DESC"
     'Empieza transacci�n
      Call objApp.rdoConnect.BeginTrans
      vntNuevoCod = GetNewCode(strSql)
      Call objWinInfo.CtrlStabilize(txtText1(2), vntNuevoCod)
    End If
  
 
'******************************************************************
   'Si estamos a�adiendo el Perfil General actualizamos los demas
'******************************************************************
 
    If chkCheck1(0).Value = 1 And dtcDateCombo1(2) = "" Then
      strSql = "SELECT AG11CODRECURSO, AG07CODPERFIL, AG07INDPERGENE " _
             & "FROM AG0700 WHERE AG11CODRECURSO=" & Val(txtText1(0)) _
             & " AND AG07INDPERGENE=-1"
      Set rdoPerfil = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
     
      If objGen.GetRowCount(rdoPerfil) > 0 Then
        While Not rdoPerfil.EOF
          rdoPerfil.Edit
          rdoPerfil("AG07INDPERGENE") = 0
          rdoPerfil.Update
          rdoPerfil.MoveNext
        Wend
      End If
    End If
  
   Exit Sub
 End If
  
  
'******************************************************************
'******************************************************************
' Actuaciones en el form Periodo_Vigencia antes de la actualizaci�n
'******************************************************************
'******************************************************************
  If strFormName = "Periodo_Vigencia" Then
   'Asigno a strCampoNumIncidencia el campo de n� incidencia del
   'form Perfil
    strCampoNumIncidencia = "AG05NUMINCIDEN"
    
'******************************************************************
 'Si estamos a�adiendo un nuevo registro obtenemos su c�digo
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
     
     'Creo la Sql para obtener el nuevo c�digo
      strSql = "SELECT AG09NUMPVIGPER FROM " & objEnv.GetValue("Database") & "AG0900 " _
             & " WHERE AG11CODRECURSO=" & Val(txtText1(0)) _
             & " AND AG07CODPERFIL=" & Val(txtText1(2)) _
             & " ORDER BY AG09NUMPVIGPER DESC"
     ' Empieza transacci�n
      Call objApp.rdoConnect.BeginTrans
      'Obtengo el nuevo c�digo
      vntNuevoCod = GetNewCode(strSql)
      'Paso el nuevo c�digo al campo del cursor y a la textbox
      Call objWinInfo.CtrlStabilize(txtText1(4), vntNuevoCod)
      intCodMotInci = 90
    ' Carga de la ventana Incidencias
    '  Load frmIncidencias
      Call AddInciden(intCodMotInci, False)
      If blnActive And Not blnRollback Then
      ' Asignamos el n�mero de incidencia al campo del form activo
        objWinInfo.objWinActiveForm.rdoCursor(strCampoNumIncidencia) = vntNuevoCod
      End If
    ' Descargamos el form de incidencias de memoria
    '  Unload frmIncidencias
    '  Set frmIncidencias = Nothing

    End If
  End If

'******************************************************************
' Si estamos en modo edici�n puede que haya alguna incidencia al
' grabar los cambios
'******************************************************************
  If objWinInfo.intWinStatus = cwModeSingleEdit Then
  ' Miramos si hay disminuci�n
    If intCodMotInci <> 0 Then
      
    ' Iniciamos la transacci�n
      Call objApp.rdoConnect.BeginTrans
    ' Carga de la ventana Incidencias
    '  Load frmIncidencias
      Call AddInciden(intCodMotInci, False)
      If blnActive And Not blnRollback Then
      ' Asignamos el n�mero de incidencia al campo del form activo
        objWinInfo.objWinActiveForm.rdoCursor(strCampoNumIncidencia) = vntNuevoCod
      End If
    ' Descargamos el form de incidencias de memoria
    '  Unload frmIncidencias
    '  Set frmIncidencias = Nothing
    End If
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "Perfil" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(False)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
 'Si es el perfil gen�rico desactivo la fecha de baja
  If intIndex = 0 And chkCheck1(0).Value = 1 Then
   
    Call objWinInfo.CtrlSet(dtcDateCombo1(2), "")
    dtcDateCombo1(2).Enabled = False
  Else
    dtcDateCombo1(2).Enabled = True
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
