Attribute VB_Name = "SimpleAgenda"
Option Explicit

Dim datFechasD(20) As Date
Dim datFechasH(20) As Date
Dim datFecEspe As Date
Dim datFecEspeHas As Date
Dim cllWDias As New Collection
Dim cllWLabFes As New Collection
Dim cllWDiaSemana As New Collection
Public cllPubDias As New Collection
Public cllPubLabFes As New Collection
Public cllPubDiaSemana As New Collection
Public cllPubPerfil As New Collection
Public cllPubFecDes As New Collection
Public cllPubFecHas As New Collection


Public Sub TiposDatosSimple()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  'Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "AGENDA. Mantenimiento de Tipos de Datos"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "AGENDA"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "I�aki Gabiola"
      .cwDAT = "10-07-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes tipos de datos que se pueden seleccionar en el mantenimiento de tipos de restricci�n"
      .cwUPD = "10-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "TipDatos"
      .objFormContainer.Caption = "Tipos de Datos"
      ' Definici�n del impreso
      Call .objPrinter.Add("AG0004", "Listado de Tipos de Datos")
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "AG1800"

      Call .FormAddOrderField("AG18CODTIPDATO", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de Tipos de Datos")
      Call .FormAddFilterWhere(strKey, "AG18DESTIPDATO", "Descripci�n", cwString)

      Call .FormAddFilterOrder(strKey, "AG18DESTIPDATO", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "AG18CODTIPDATO")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "AG18DESTIPDATO")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With

    'Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

Public Sub CodigosIncidencia()
' Declaraci�n de variables

' Form simple
  Dim objSimple As New clsCWSimpleMaint
' Form multilinea
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim grdGrid As Object
  
' Visualizaci�n de la ventana splash
  'Call objApp.SplashOn
  
  
  With objSimple
' Creaci�n de la ventana
    Set .frmSimple.objWin = New clsCWWin
  
    Call .frmSimple.objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                             .frmSimple, _
                                             .frmSimple.tlbToolbar1, _
                                             .frmSimple.stbStatusBar1, _
                                             cwWithAll)
    
' T�tulo de ventana
    .frmSimple.Caption = "AGENDA. Mantenimiento de C�digos de Incidencia"
    
'Documentaci�n
    With .frmSimple.objWin.objDoc
      .cwPRJ = "AGENDA"
      .cwMOD = "M�dulo Multil�nea"
      .cwAUT = "I�aki Gabiola"
      .cwDAT = "8-09-97"
      .cwDES = "Ventana que realiza el mantenimiento de los diferentes c�digos de incidencia"
      .cwUPD = "8-09-97 - I�aki Gabiola - Creaci�n del m�dulo"
      .cwEVT = " "
    End With

' Caracter�sticas del form multil�nea
    With objMultiInfo
      Set .objFormContainer = objSimple.frmSimple.fraFrame1(0)
      
      .strName = "CodIncidencias"
      .objFormContainer.Caption = "C�digos de Incidencia"
      ' Definici�n del impreso
      Call .objPrinter.Add("AG0002", "Listado de C�digos de Motivos de Incidencia")
      Set .objFatherContainer = Nothing
      Set .tabMainTab = Nothing
      Set .grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

      .strDataBase = objEnv.GetValue("DataBase")
      .strTable = "AG0600"
      .intAllowance = cwAllowAdd + cwAllowModify

      Call .FormAddOrderField("AG06CODMOTINCI", cwAscending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Tabla de C�digos de Motivos de Incidencia")
      Call .FormAddFilterWhere(strKey, "AG06DESMOTINCI", "Descripci�n", cwString)

      Call .FormAddFilterOrder(strKey, "AG06DESMOTINCI", "Descripci�n")
    End With

    With .frmSimple.objWin
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

      Call .GridAddColumn(objMultiInfo, "C�digo", "AG06CODMOTINCI")
      Call .GridAddColumn(objMultiInfo, "Descripci�n", "AG06DESMOTINCI")
      Call .GridAddColumn(objMultiInfo, "Indicador Disminuci�n", "AG06INDINCDISM")
      Call .GridAddColumn(objMultiInfo, "Fecha Inicio", "AG06FECACTIVA")

      Call .FormCreateInfo(objMultiInfo)

      Set grdGrid = objSimple.frmSimple.grdDBGrid1(0)
      .CtrlGetInfo(grdGrid.Columns(3)).blnInFind = True
      .CtrlGetInfo(grdGrid.Columns(4)).blnInFind = True
      '.CtrlGetInfo(grdGrid.Columns(5)).blnInFind = True

      '.CtrlGetInfo(grdGrid.Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("DataBase") & "dept ORDER BY deptno"

      'Call .CtrlCreateLinked(.CtrlGetInfo(grdGrid.Columns(8)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(9), "dname")
      'Call .CtrlAddLinked(.CtrlGetInfo(grdGrid.Columns(8)), grdGrid.Columns(10), "loc")
      Set grdGrid = Nothing
      
      Call .WinRegister
      Call .WinStabilize
    End With
    'Call objApp.SplashOff
  End With
    
  Call objSimple.Show
  Set objSimple = Nothing
End Sub

'Public Sub DiasFestivos()
Public Sub DiasFestivos(intCalen As Integer, datFecdes As Date, datFechas As Date)
  Dim strWhere02 As String
  Dim rdo02 As rdoResultset
  Dim rdo02cl As rdoColumn
  Dim strSql As String
  Dim intCalPrin As Integer
  Dim intI As Integer
  Dim cllDias As New Collection
  Dim cllLabFes As New Collection
  Dim cllDiaSemana As New Collection
  Dim strAux1 As String
  
  'Dim intCalen As Integer
  'Dim datFecdes As Date
  'Dim datFechas As Date
  
 ' datos de prueba
  '  intCalen = 10
  '  datFecdes = #10/13/97#
  '  datFechas = #10/31/97#
 
 ' inicializamos datos
  intI = 1
  Do While intI < 20
        datFechasD(intI) = 0
        datFechasH(intI) = 0
        intI = intI + 1
  Loop
      
  Call objGen.RemoveCollection(cllDias)
  Call objGen.RemoveCollection(cllLabFes)
  Call objGen.RemoveCollection(cllDiaSemana)
  Call objGen.RemoveCollection(cllWDias)
  Call objGen.RemoveCollection(cllWLabFes)
  Call objGen.RemoveCollection(cllWDiaSemana)
  
  If Not IsNull(intCalen) And Not IsNull(datFecdes) And Not IsNull(datFechas) Then
   
  'Comprobamos tipo calendario en 02
    strWhere02 = "WHERE AG02INDCALPRIN = -1"
         
    strSql = "SELECT AG02CODCALENDA FROM AG0200 " & strWhere02
    Set rdo02 = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
    
    If rdo02.RowCount > 0 Then
        intCalPrin = 0
        Call DiasFestivosB(intCalPrin, intCalen, datFecdes, datFechas)
        For Each rdo02cl In rdo02.rdoColumns
            If rdo02cl.Value <> intCalen Then
                intCalPrin = 1
                Call DiasFestivosB(intCalPrin, rdo02cl.Value, datFecdes, datFechas)
            End If
        Next
    End If
  
  ' formamos las collecciones de salida
    datFecEspe = datFecdes
    Do Until datFecEspe > datFechas
        cllDias.Add (datFecEspe), CStr(datFecEspe)
        On Error Resume Next
        
        strAux1 = cllWLabFes(CStr(datFecEspe))
        
        If Err.Number = 0 Then
            cllLabFes.Add cllWLabFes(CStr(datFecEspe)), CStr(datFecEspe)
            cllDiaSemana.Add cllWDiaSemana(CStr(datFecEspe)), CStr(datFecEspe)
        Else
            cllLabFes.Add ("L"), CStr(datFecEspe)
            cllDiaSemana.Add WeekDay(datFecEspe), CStr(datFecEspe)
        End If
       
        datFecEspe = DateAdd("d", 1, datFecEspe)
    Loop

  
   End If

Set cllPubDias = cllDias
Set cllPubLabFes = cllLabFes
Set cllPubDiaSemana = cllDiaSemana

End Sub

Public Sub DiasFestivosB(intCalPrin As Integer, intCalen As Integer, datFecdes As Date, datFechas As Date)
  Dim strWhere03 As String
  Dim strWhere08 As String
  Dim rdo03 As rdoResultset
  Dim rdo08 As rdoResultset
  Dim rdo03cl As rdoColumn
  Dim rdo08cl As rdoColumn
  Dim strSql As String
  Dim intDiaEspe As Integer
  Dim intLun As Integer
  Dim intMar As Integer
  Dim intMie As Integer
  Dim intJue As Integer
  Dim intVie As Integer
  Dim intSab As Integer
  Dim intDom As Integer
  Dim intI As Integer
  Dim intOK As Integer
  
 'Comprobamos dias especiales en 03
    strWhere03 = "WHERE " _
         & " AG02CODCALENDA =" & intCalen & " AND " _
         & " AG03FECDIAESPE >=" & objGen.ValueToSQL(datFecdes, cwDate) & " AND " _
         & " AG03FECDIAESPE <=" & objGen.ValueToSQL(datFechas, cwDate)

    strSql = "SELECT AG03FECDIAESPE, AG03INDFESTIVO FROM AG0300 " & strWhere03
    Set rdo03 = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
    
    Do While Not rdo03.EOF
        For Each rdo03cl In rdo03.rdoColumns
            If rdo03cl.Name = "AG03FECDIAESPE" Then
                datFecEspe = rdo03cl.Value
                intDiaEspe = WeekDay(datFecEspe)
            ElseIf rdo03cl.Name = "AG03INDFESTIVO" Then
               On Error Resume Next
               If rdo03cl.Value = -1 Then
                    cllWDias.Add (datFecEspe), CStr(datFecEspe)
                    cllWLabFes.Add ("F"), CStr(datFecEspe)
                    cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                Else
                    cllWDias.Add (datFecEspe), CStr(datFecEspe)
                    cllWLabFes.Add ("L"), CStr(datFecEspe)
                    cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                End If
            End If
        Next
        rdo03.MoveNext
    Loop
   
  'Comprobamos dias festivos en 08
    strWhere08 = "WHERE " _
         & " AG02CODCALENDA =" & intCalen & " AND " _
         & " AG08FECINIPERI <=" & objGen.ValueToSQL(datFechas, cwDate) & " AND " _
         & " AG08FECFINPERI >=" & objGen.ValueToSQL(datFecdes, cwDate)

    strSql = "SELECT AG08FECINIPERI, AG08FECFINPERI, AG08INDLUNFEST, AG08INDMARFEST, AG08INDMIEFEST, AG08INDJUEFEST, AG08INDVIEFEST, AG08INDSABFEST, AG08INDDOMFEST FROM AG0800 " & strWhere08
    Set rdo08 = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
    
    Do While Not rdo08.EOF
        For Each rdo08cl In rdo08.rdoColumns
            If rdo08cl.Name = "AG08FECINIPERI" Then
                datFecEspe = rdo08cl.Value
            ElseIf rdo08cl.Name = "AG08FECFINPERI" Then
                datFecEspeHas = rdo08cl.Value
            ElseIf rdo08cl.Name = "AG08INDLUNFEST" Then
                    intLun = rdo08cl.Value
            ElseIf rdo08cl.Name = "AG08INDMARFEST" Then
                    intMar = rdo08cl.Value
            ElseIf rdo08cl.Name = "AG08INDMIEFEST" Then
                    intMie = rdo08cl.Value
            ElseIf rdo08cl.Name = "AG08INDJUEFEST" Then
                    intJue = rdo08cl.Value
            ElseIf rdo08cl.Name = "AG08INDVIEFEST" Then
                    intVie = rdo08cl.Value
            ElseIf rdo08cl.Name = "AG08INDSABFEST" Then
                    intSab = rdo08cl.Value
            ElseIf rdo08cl.Name = "AG08INDDOMFEST" Then
                    intDom = rdo08cl.Value
            End If
        Next
        
        If intCalPrin = 0 Then
            intI = intI + 1
            datFechasD(intI) = datFecEspe
            datFechasH(intI) = datFecEspeHas
        End If
            
        Do Until datFecEspe > datFecEspeHas
            intOK = 0
            If intCalPrin = 1 Then
                intI = 1
                Do While intI < 20 Or datFechasD(intI) <> 0
                    If datFecEspe >= datFechasD(intI) And datFecEspe <= datFechasH(intI) Then
                       intOK = 1
                       intI = 19
                    End If
                    intI = intI + 1
                Loop
            End If
           
            If intOK = 0 Then
                intDiaEspe = WeekDay(datFecEspe)
                  On Error Resume Next
  
                  Select Case intDiaEspe
                    Case 2 And intLun = -1
                        cllWDias.Add (datFecEspe), CStr(datFecEspe)
                        cllWLabFes.Add ("F"), CStr(datFecEspe)
                        cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                    Case 3 And intMar = -1
                        cllWDias.Add (datFecEspe), CStr(datFecEspe)
                        cllWLabFes.Add ("F"), CStr(datFecEspe)
                        cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                    Case 4 And intMie = -1
                        cllWDias.Add (datFecEspe), CStr(datFecEspe)
                        cllWLabFes.Add ("F"), CStr(datFecEspe)
                        cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                    Case 5 And intJue = -1
                        cllWDias.Add (datFecEspe), CStr(datFecEspe)
                        cllWLabFes.Add ("F"), CStr(datFecEspe)
                        cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                    Case 6 And intVie = -1
                        cllWDias.Add (datFecEspe), CStr(datFecEspe)
                        cllWLabFes.Add ("F"), CStr(datFecEspe)
                        cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                    Case 7 And intSab = -1
                        cllWDias.Add (datFecEspe), CStr(datFecEspe)
                        cllWLabFes.Add ("F"), CStr(datFecEspe)
                        cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                    Case 1 And intDom = -1
                        cllWDias.Add (datFecEspe), CStr(datFecEspe)
                        cllWLabFes.Add ("F"), CStr(datFecEspe)
                        cllWDiaSemana.Add (intDiaEspe), CStr(datFecEspe)
                End Select
            End If
            datFecEspe = DateAdd("d", 1, datFecEspe)
       Loop
        
    rdo08.MoveNext
    Loop
    
End Sub

Public Sub PerfilVigente(intRecur As Integer, datFecdes As Date, datFechas As Date)
  Dim strWhere07 As String
  Dim strWhere09 As String
  Dim rdo07 As rdoResultset
  Dim rdo09 As rdoResultset
  Dim rdo07cl As rdoColumn
  Dim rdo09cl As rdoColumn
  Dim strSql As String
  Dim intPerGene As Integer
  Dim intI As Integer
  Dim intL As Integer
  Dim cllWPerfil As New Collection
  Dim cllWFecDes As New Collection
  Dim cllWFecHas As New Collection
  Dim cllPerfil As New Collection
  Dim cllFecDes As New Collection
  Dim cllFecHas As New Collection
  Dim datFec As Date
  
 ' inicializamos datos
  Call objGen.RemoveCollection(cllWPerfil)
  Call objGen.RemoveCollection(cllWFecDes)
  Call objGen.RemoveCollection(cllWFecHas)
  Call objGen.RemoveCollection(cllPerfil)
  Call objGen.RemoveCollection(cllFecDes)
  Call objGen.RemoveCollection(cllFecHas)
  
  If Not IsNull(intRecur) And Not IsNull(datFecdes) And Not IsNull(datFechas) Then
   
  ' Accedemos a 07
    strWhere07 = "WHERE AG11CODRECURSO = " & intRecur & " AND AG07INDPERGENE = -1"
         
    strSql = "SELECT AG07CODPERFIL, AG07INDPERGENE FROM AG0700 " & strWhere07
    Set rdo07 = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
    intI = 0

    Do While Not rdo07.EOF
        intI = intI + 1
        For Each rdo07cl In rdo07.rdoColumns
            If rdo07cl.Name = "AG07CODPERFIL" Then
                intPerGene = rdo07cl.Value
            End If
        Next
        rdo07.MoveNext
    Loop
 
 ' Accedemos a 09
    strWhere09 = "WHERE " _
    & " AG11CODRECURSO =" & intRecur & " AND " _
    & " AG07CODPERFIL <>" & intPerGene & " AND " _
    & " AG09FECINVIPER <=" & objGen.ValueToSQL(datFechas, cwDate) & " AND " _
    & " AG09FECFIVIPER >=" & objGen.ValueToSQL(datFecdes, cwDate)
         
    strSql = "SELECT AG07CODPERFIL, AG09FECINVIPER, AG09FECFIVIPER FROM AG0900 " & strWhere09 & " ORDER BY AG09FECINVIPER"
    Set rdo09 = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)

    Do While Not rdo09.EOF
        intL = intL + 1
        For Each rdo09cl In rdo09.rdoColumns
            If rdo09cl.Name = "AG07CODPERFIL" Then
                cllWPerfil.Add (rdo09cl.Value)
            ElseIf rdo09cl.Name = "AG09FECINVIPER" Then
                cllWFecDes.Add (rdo09cl.Value)
            ElseIf rdo09cl.Name = "AG09FECFIVIPER" Then
                cllWFecHas.Add (rdo09cl.Value)
            End If
        Next
        rdo09.MoveNext
    Loop
 
 ' formamos las collecciones de salida
    intI = 1
    datFec = datFecdes
    
    Do Until datFec > datFechas
        If intI <= intL Then
            If cllWFecDes(intI) > datFec Then
                cllPerfil.Add (intPerGene)
                cllFecDes.Add (datFec)
                cllFecHas.Add (DateAdd("d", -1, cllWFecDes(intI)))
            End If
            
            cllPerfil.Add (cllWPerfil(intI))
            
            If cllWFecDes(intI) < datFec Then
                cllFecDes.Add (datFec)
            Else
                cllFecDes.Add (cllWFecDes(intI))
            End If
            If cllWFecHas(intI) < datFechas Then
                cllFecHas.Add (cllWFecHas(intI))
                datFec = DateAdd("d", 1, cllWFecHas(intI))
            Else
                cllFecHas.Add (datFechas)
                datFec = DateAdd("d", 1, datFechas)
            End If
            intI = intI + 1
        Else
            cllPerfil.Add (intPerGene)
            cllFecDes.Add (datFec)
            cllFecHas.Add (datFechas)
            datFec = DateAdd("d", 1, datFechas)
        End If
    Loop
 
 
    Set cllPubPerfil = cllPerfil
    Set cllPubFecDes = cllFecDes
    Set cllPubFecHas = cllFecHas

End If

End Sub
