VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmTipRest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Mantenimiento de Tipos de Restricci�n"
   ClientHeight    =   6795
   ClientLeft      =   690
   ClientTop       =   1470
   ClientWidth     =   10935
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "TipRest.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   6795
   ScaleWidth      =   10935
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   20
      Top             =   0
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipos de Restricci�n"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2900
      Index           =   0
      Left            =   105
      TabIndex        =   24
      Tag             =   "Mantenimiento de Tipos de Restricci�n"
      Top             =   570
      Width           =   10725
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   420
         Width           =   10455
         _ExtentX        =   18441
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "TipRest.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label2"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(12)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(11)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtcDateCombo1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cboSSDBCombo1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(4)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(3)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "TipRest.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG16CODTIPREST"
            Height          =   330
            HelpContextID   =   40101
            Index           =   1
            Left            =   360
            MaxLength       =   2
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "C�digo tipo restricci�n|C�digo"
            Top             =   540
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG16DESTIPREST"
            Height          =   330
            HelpContextID   =   40102
            Index           =   2
            Left            =   2490
            TabIndex        =   1
            Tag             =   "Descripci�n tipo restricci�n|Descripci�n"
            Top             =   540
            Width           =   3270
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AG16TABLAVALOR"
            Height          =   330
            Index           =   3
            Left            =   360
            TabIndex        =   4
            Tag             =   "C�digo tabla o vista valores|C�digo tabla"
            Top             =   1320
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AG16COLCODVALO"
            Height          =   690
            Index           =   4
            Left            =   2490
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            Tag             =   "Columnas c�digo tabla valores|Columna"
            Top             =   1320
            Width           =   3480
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AG16COLDESVALO"
            Height          =   330
            Index           =   5
            Left            =   6105
            TabIndex        =   6
            Tag             =   "Columna descripci�n tabla valores|Columna descripci�n"
            Top             =   1320
            Width           =   3480
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "AG18CODTIPDATO"
            Height          =   330
            Index           =   0
            Left            =   6105
            TabIndex        =   2
            Tag             =   "Tipo de dato|Tipo Dato"
            Top             =   540
            Width           =   1575
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   2778
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   2778
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG16FECBAJA"
            Height          =   330
            Index           =   0
            Left            =   7920
            TabIndex        =   3
            Tag             =   "Fecha baja tipo restricci�n|Fecha baja"
            Top             =   540
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2990
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            NullDateLabel   =   "__/__/____"
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2010
            Index           =   0
            Left            =   -74880
            TabIndex        =   26
            Top             =   90
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   3545
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   360
            TabIndex        =   33
            Top             =   315
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   2490
            TabIndex        =   32
            Top             =   315
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Dato"
            DataField       =   "AG18TIPDATREST"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   6105
            TabIndex        =   31
            Top             =   300
            Width           =   1125
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Tabla Valores"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   360
            TabIndex        =   30
            Top             =   1080
            Width           =   1830
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Columnas C�digo Tabla Valores"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   2490
            TabIndex        =   29
            Top             =   1080
            Width           =   2700
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Columna Descripci�n "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   6105
            TabIndex        =   28
            Top             =   1080
            Width           =   1860
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Baja"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   7920
            TabIndex        =   27
            Top             =   315
            Width           =   975
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Valores"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2835
      Index           =   1
      Left            =   120
      TabIndex        =   14
      Tag             =   "Mantenimiento de Valores de Tipos de Restricci�n"
      Top             =   3585
      Width           =   10725
      Begin TabDlg.SSTab tabTab1 
         Height          =   2265
         Index           =   1
         Left            =   120
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   420
         Width           =   10455
         _ExtentX        =   18441
         _ExtentY        =   3995
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "TipRest.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(7)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(9)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(7)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(10)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(9)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "TipRest.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG16CODTIPREST"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   2280
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "C�digo tipo restricci�n|C�digo"
            Top             =   480
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AG17VALHASTATRE"
            Height          =   330
            Index           =   9
            Left            =   2280
            TabIndex        =   9
            Tag             =   "Valor hasta tipo restricci�n|Valor hasta"
            Top             =   1440
            Width           =   1575
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG17DESVALTIRE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   4200
            TabIndex        =   10
            Tag             =   "Descripci�n asociada valor tipo restricci�n|Descripci�n asociada"
            Top             =   1440
            Width           =   4260
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG17VALTIRES"
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   360
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "C�digo valor tipo restricci�n|C�digo valor"
            Top             =   540
            Width           =   1410
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG17VALDESDETRE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   8
            Left            =   345
            TabIndex        =   8
            Tag             =   "Valor desde tipo restricci�n|Valor desde"
            Top             =   1440
            Width           =   1545
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1965
            Index           =   1
            Left            =   -74880
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   120
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17383
            _ExtentY        =   3466
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Valor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   4200
            TabIndex        =   19
            Top             =   1200
            Width           =   1515
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Valor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   360
            TabIndex        =   18
            Top             =   315
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Desde"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   360
            TabIndex        =   17
            Top             =   1200
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hasta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   2280
            TabIndex        =   16
            Top             =   1200
            Width           =   510
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   6510
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Columas c�digo tabla valores"
      Height          =   195
      Index           =   13
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   2070
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Descipci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   10
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   960
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Descipci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   8
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   960
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmTipRest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Private Function BuscarEnRestricciones() As Boolean
  Dim rdoRestricciones As rdoResultset
  Dim strSql As String
  
  BuscarEnRestricciones = False
  
  strSql = "SELECT AG11CODRECURSO FROM AG1300 WHERE AG16CODTIPREST=" & Val(txtText1(1))
  Set rdoRestricciones = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
  If objGen.GetRowCount(rdoRestricciones) > 0 Then
    BuscarEnRestricciones = True
    rdoRestricciones.Close

    GoTo Salir
  End If
  strSql = "SELECT AG07CODPERFIL FROM AG1200 WHERE AG16CODTIPREST=" & Val(txtText1(1))
  Set rdoRestricciones = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
  If objGen.GetRowCount(rdoRestricciones) > 0 Then
    BuscarEnRestricciones = True
    rdoRestricciones.Close
  End If
Salir:
  Set rdoRestricciones = Nothing
End Function

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form maestro
  Dim objMasterInfo As New clsCWForm
' Form hijo
  Dim objDetailInfo As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  
' **********************************
' Fin declaraci�n de variables
' **********************************
  
' Se visualiza el formulario de splash
  'Call objApp.SplashOn
    
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Introducir la documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Tipos de Restricci�n"
    .cwDAT = "09-07-97"
    .cwAUT = "I�aki Gabiola"
    .cwDES = "Esta ventana permite mantener los diferentes tipos de restricci�n de la CUN"
    .cwUPD = "09-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form maestro
  With objMasterInfo
  ' Nombre del form
    .strName = "TipoRestricci�n"
  ' Definici�n del impreso
    Call .objPrinter.Add("AG0006", "Listado 1 de Tipos de Restricci�n")
  
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1600"
    .blnAskPrimary = False
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG16CODTIPREST", cwAscending)
    
  ' Creaci�n de los filtros de busqueda
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Tabla de Tipos Restricci�n")
    Call .FormAddFilterWhere(strKey, "AG18CODTIPDATO", "C�digo Tipo Restricci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG16DESTIPREST", "Descripci�n Tipo Restricci�n", cwString)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder(strKey, "AG18CODTIPDATO", "Tipo Dato Restricci�n")
    Call .FormAddFilterOrder(strKey, "AG16DESTIPREST", "Descripci�n Tipo Restricci�n")
  
  End With
  
' Declaraci�n de las caracter�sticas del form detalle
  With objDetailInfo
   ' Asigno nombre al frame
    .strName = "ValorRestricci�n"
    
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(0)
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1700"
    .blnAskPrimary = False
    
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG17VALTIRES", cwAscending)
    Call .FormAddRelation("AG16CODTIPREST", txtText1(1))
  
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
   

  End With
   
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
  
  ' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
  ' Se obtiene informaci�n de las caracter�sticas de
  ' los controles del formulario
    Call .FormCreateInfo(objMasterInfo)
        
' Campos de busqueda form maestro
    .CtrlGetInfo(txtText1(2)).blnInFind = True
   
    
' Campos busqueda form hijo
   .CtrlGetInfo(txtText1(10)).blnInFind = True
   
   .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AG18CODTIPDATO, AG18DESTIPDATO FROM " & objEnv.GetValue("Database") & "AG1800 ORDER BY AG18CODTIPDATO"
    
  ' Propiedades del campo clave Tipo Restricci�n para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(1)).blnValidate = False
    .CtrlGetInfo(txtText1(1)).blnInGrid = False
    
   ' Propiedades del campo clave Valor Restricci�n para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(7)).blnValidate = False
    .CtrlGetInfo(txtText1(7)).blnInGrid = False

 ' Eliminamos campos del grid
   .CtrlGetInfo(txtText1(0)).blnInGrid = False
  ' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
  ' Se estabiliza la ventana configurando las propiedades
  ' de los controles
    Call .WinStabilize
    Call .DataMoveFirst
  End With
  
' Se oculta el formulario de splash
  'Call objApp.SplashOff
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  If strFormName = "ValorRestricci�n" And Not objWinInfo.objWinMainForm.rdoCursor.EOF Then
    Select Case objWinInfo.objWinMainForm.rdoCursor("AG18CODTIPDATO").Value
      Case 1
        objWinInfo.CtrlGetInfo(txtText1(8)).intMask = cwMaskInteger
        objWinInfo.CtrlGetInfo(txtText1(9)).intMask = cwMaskInteger
      Case 2
        objWinInfo.CtrlGetInfo(txtText1(8)).intMask = cwMaskString
        objWinInfo.CtrlGetInfo(txtText1(9)).intMask = cwMaskString
      Case 3
        objWinInfo.CtrlGetInfo(txtText1(8)).intMask = cwMaskString
        objWinInfo.CtrlGetInfo(txtText1(9)).intMask = cwMaskString
    End Select
  End If
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  If strFormName = "TipoRestricci�n" Then
    With objWinInfo
    If intNewStatus = cwModeSingleAddRest Then
      .CtrlGetInfo(cboSSDBCombo1(0)).blnReadOnly = False
      .CtrlGetInfo(txtText1(3)).blnReadOnly = False
      .CtrlGetInfo(txtText1(4)).blnReadOnly = False
      .CtrlGetInfo(txtText1(5)).blnReadOnly = False
      Call .FormChangeColor(.objWinActiveForm)
      Call .WinPrepareScr
    End If
   End With
  End If
End Sub

Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
  
  If strFormName = "TipoRestricci�n" And dtcDateCombo1(0).Text <> "" Then
    If BuscarEnRestricciones Then
      Call objError.SetError(cwCodeMsg, "Este Tipo de Restricci�n no se puede dar de baja al ser parte de alguna restricci�n")
      Call objError.Raise
      blnCancel = True
    End If
  End If
  
  If strFormName = "ValorRestricci�n" Then
    If IsNumeric(txtText1(8)) And IsNumeric(txtText1(9)) Then
       If Val(txtText1(8)) > Val(txtText1(9)) Then
         Call objError.SetError(cwCodeMsg, "El valor Desde es mayor que el valor Hasta")
         Call objError.Raise
         blnCancel = True
       End If
     End If
   End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'******************************************************************
' Compruebo si ha habido error para
' validar los cambios o anularlos
'******************************************************************
  If blnError = True Then
    Call objApp.rdoConnect.RollbackTrans
  Else
    Call objApp.rdoConnect.CommitTrans
  End If

End Sub

Private Sub objWinInfo_cwPreChangeForm(ByVal strFormName As String)
'******************************************************************
' Se proteje el campo Tipo de Dato, Tabla de valores, Columnas c�digo
' y Columnas descripci�n del form maestro si este tiene alg�n registro
' detalle asociado
'******************************************************************
  If strFormName = "ValorRestricci�n" Then
    With objWinInfo
      ' Vemos si el maestro tiene asociado algun registro
      If objGen.GetRowCount(.cllWinForms("fraFrame1(1)").rdoCursor) > 0 Then
        .CtrlGetInfo(cboSSDBCombo1(0)).blnReadOnly = True
        .CtrlGetInfo(txtText1(3)).blnReadOnly = True
        .CtrlGetInfo(txtText1(4)).blnReadOnly = True
        .CtrlGetInfo(txtText1(5)).blnReadOnly = True
      Else
        .CtrlGetInfo(cboSSDBCombo1(0)).blnReadOnly = False
        .CtrlGetInfo(txtText1(3)).blnReadOnly = False
        .CtrlGetInfo(txtText1(4)).blnReadOnly = False
        .CtrlGetInfo(txtText1(5)).blnReadOnly = False
      End If
      Call .FormChangeColor(.objWinActiveForm)
      Call .WinPrepareScr

    End With
  End If

End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim strSql As String
  If strFormName = "TipoRestricci�n" Then
'******************************************************************
    'Si estamos a�adiendo un nuevo registro obtenemos el nuevo
    'c�digo
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      strSql = "SELECT AG16CODTIPREST FROM " & objEnv.GetValue("Database") & "AG1600 " _
             & " ORDER BY 1 DESC"
     ' Empieza transacci�n
      Call objApp.rdoConnect.BeginTrans
     'Obtengo el nuevo c�digo
      vntNuevoCod = GetNewCode(strSql)
     'Paso el nuevo c�digo al campo del cursor y a la textbox
      Call objWinInfo.CtrlStabilize(txtText1(1), vntNuevoCod)
    End If
  End If

  If strFormName = "ValorRestricci�n" Then
'******************************************************************
    'Si estamos a�adiendo un nuevo registro obtenemos el nuevo
    'c�digo
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      strSql = "SELECT AG17VALTIRES FROM " & objEnv.GetValue("Database") & "AG1700 " _
             & "WHERE AG16CODTIPREST=" & Val(txtText1(1)) _
             & " ORDER BY 1 DESC"
     ' Empieza transacci�n
      Call objApp.rdoConnect.BeginTrans
     'Obtengo el nuevo c�digo
      vntNuevoCod = GetNewCode(strSql)
     'Paso el nuevo c�digo al campo del cursor y a la textbox
      Call objWinInfo.CtrlStabilize(txtText1(7), vntNuevoCod)
    End If
  End If

End Sub

' Introducci�n de los impresos
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "TipoRestricci�n" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(False)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
 
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
' Si se a�ade un nuevo valor de tipo de restrici�n le paso
' el c�difo del tipo de restricci�n
 If strFormName = "ValorRestricci�n" Then
   txtText1(0).Text = txtText1(1).Text
 End If
End Sub



Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'******************************************************************
' Se proteje el campo Tipo de Dato, Tabla de valores, Columnas c�digo
' y Columnas descripci�n del form maestro si este tiene alg�n registro
' detalle asociado
'******************************************************************
  If strFormName = "TipoRestricci�n" And objWinInfo.intWinStatus = cwModeSingleEdit Then
    With objWinInfo
      ' Vemos si el maestro tiene asociado algun registro
      If objGen.GetRowCount(.cllWinForms("fraFrame1(1)").rdoCursor) > 0 Then
        .CtrlGetInfo(cboSSDBCombo1(0)).blnReadOnly = True
        .CtrlGetInfo(txtText1(3)).blnReadOnly = True
        .CtrlGetInfo(txtText1(4)).blnReadOnly = True
        .CtrlGetInfo(txtText1(5)).blnReadOnly = True
    
      Else
        .CtrlGetInfo(cboSSDBCombo1(0)).blnReadOnly = False
        .CtrlGetInfo(txtText1(3)).blnReadOnly = False
        .CtrlGetInfo(txtText1(4)).blnReadOnly = False
        .CtrlGetInfo(txtText1(5)).blnReadOnly = False
      End If
      Call .FormChangeColor(.objWinActiveForm)
      Call .WinPrepareScr

    End With
  End If
End Sub

Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
'******************************************************************
 'Compruebo antes de borrar que el tipo de restricci�n
 'no tiene ningun valor de restricci�n
'******************************************************************
  If strFormName = "TipoRestricci�n" Then
   If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(1)").rdoCursor) > 0 Then
     Call objError.SetError(cwCodeMsg, "No se puede borrar un Tipo de Restricci�n que tiene asociado un valor")
     Call objError.Raise
     blnCancel = True
     Exit Sub
   End If
 End If
'******************************************************************
'compruebo que no se borre si es el �nico valor que queda
'******************************************************************
' If strFormName = "ValorRestricci�n" Then
'   If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(1)").rdoCursor) = 1 Then
'     Call objError.SetError(cwCodeMsg, "No se puede borrar porque es el �nico valor que queda para este Tipo de Restricci�n")
'     Call objError.Raise
'     blnCancel = True
'     Exit Sub
'   End If
' End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
'Private Sub chkCheck1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
'End Sub

'Private Sub chkCheck1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
'End Sub

'Private Sub chkCheck1_Click(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub
Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
      
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
'******************************************************************
' Miramos si el campo de c�digo tabla tiene algun valor en
' cuyo caso establecemos como obligatorios los campos columna
' y columna descripci�n
'******************************************************************
  
  If intIndex = 3 Then
    If txtText1(intIndex).Text <> "" Then
      With objWinInfo
        .CtrlGetInfo(txtText1(4)).blnMandatory = True
        .CtrlGetInfo(txtText1(5)).blnMandatory = True
        ' Form detalle solo lectura
        fraFrame1(1).Enabled = False
             
      End With
    Else
      With objWinInfo
        .CtrlGetInfo(txtText1(4)).blnMandatory = False
        .CtrlGetInfo(txtText1(5)).blnMandatory = False
        ' Form detalle con todas las opciones
        fraFrame1(1).Enabled = True
      End With
     ' Borrado del contenido de los campos columna
     ' y columna descripci�n
       
       Call objWinInfo.CtrlSet(txtText1(4), "")
       
       Call objWinInfo.CtrlSet(txtText1(5), "")
    End If
    Call objWinInfo.FormChangeColor(objWinInfo.objWinActiveForm)
   End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del SSDBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

