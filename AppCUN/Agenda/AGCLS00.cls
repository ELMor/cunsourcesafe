VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWLauncher
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
Const AGRepTipoRest             As String = "AG0006"
Const AGRepTipoRecursos         As String = "AG0005"
Const AGRepCalendarios          As String = "AG0001"
Const AGRepRecursos             As String = "AG0003"
Const AGRepPerfiles             As String = "AG0004"
Const AGRepFranjas              As String = "AG0007"
Const AGRepAgendaRecurso        As String = "AG0008"
Const AGRepDietarioRecurso      As String = "AG0009"
Const AGRepMotivosIncidencia    As String = "AG0002"
Const AGRepActuacionesRecursos  As String = "AG0010"
Const AGRepGradoOcupacionRec    As String = "AG0017"

' Ahora las ventanas. Continuan la numeraci�n a partir del AG1000
Const AGWinTipoRest             As String = "AG1001"
Const AGWinTipoRecursos         As String = "AG1002"
Const AGWinCalendarios          As String = "AG1003"
Const AGWinRecursos             As String = "AG1004"
Const AGWinRestricciones        As String = "AG1005"
Const AGWinPerfiles             As String = "AG1006"
Const AGWinFranjas              As String = "AG1007"
Const AGWinIncidencias          As String = "AG1011"
Const AGWinAgendaRecurso        As String = "AG1008"
Const AGWinDietarioRecurso      As String = "AG1009"
Const AGWinMotivosIncidencia    As String = "AG1010"
Const AGWinParametros           As String = "AG1012"

'Constantes para Historicos y Estad�sticas
Const AGRepHisRecursos          As String = "AG0013"
Const AGRepHisPerVigCal         As String = "AG0011"
Const AGRepHisDiaEsp            As String = "AG0012"
Const AGRepHisPerVigFra         As String = "AG0015"
Const AGRepHisPerfiles          As String = "AG0014"
Const AGRepHisFranjas           As String = "AG0016"


' Constantes pata los HelpContextID de las ventanas
Const AGHelpIDTipoRest             As Integer = 26
Const AGHelpIDTipoRecursos         As Integer = 1
Const AGHelpIDCalendarios          As Integer = 3
Const AGHelpIDRecursos             As Integer = 6
Const AGHelpIDRestriccionesR       As Integer = 15
Const AGHelpIDRestriccionesF       As Integer = 20
Const AGHelpIDPerfiles             As Integer = 9
Const AGHelpIDFranjas              As Integer = 18
'Const AGHelpIDIncidencias          As Integer = 0
Const AGHelpIDAgendaRecurso        As Integer = 24
Const AGHelpIDDietarioRecurso      As Integer = 25
Const AGHelpIDMotivosIncidencia    As Integer = 29


' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
     Case AGWinTipoRecursos
      Load frmTipoRecu
      Call objSecurity.AddHelpContext(AGHelpIDTipoRecursos)
      Call frmTipoRecu.Show(vbModal)
      'Unload frmTipoRecu
      Call objSecurity.RemoveHelpContext
      Set frmTipoRecu = Nothing
     Case AGWinTipoRest
      Load frmTipRest
      Call objSecurity.AddHelpContext(AGHelpIDTipoRest)
      Call frmTipRest.Show(vbModal)
      'Unload frmTipRest
      Call objSecurity.RemoveHelpContext
      Set frmTipRest = Nothing
     Case AGWinCalendarios
      Load frmCalendarios
      Call objSecurity.AddHelpContext(AGHelpIDCalendarios)
      Call frmCalendarios.Show(vbModal)
      'Unload frmCalendarios
      Call objSecurity.RemoveHelpContext
      Set frmCalendarios = Nothing
     Case AGWinRecursos
      Load frmRecurso
      Call objSecurity.AddHelpContext(AGHelpIDRecursos)
      Call frmRecurso.Show(vbModal)
      'Unload frmRecurso
      Call objSecurity.RemoveHelpContext
      Set frmRecurso = Nothing
      
     Case AGWinAgendaRecurso
        Load frmAgendaRecurso
      Call objSecurity.AddHelpContext(AGHelpIDAgendaRecurso)
      Call frmAgendaRecurso.Show(vbModal)
      'Unload frmAgendaRecurso
      Call objSecurity.RemoveHelpContext
      Set frmAgendaRecurso = Nothing
      
     Case AGWinDietarioRecurso
      If vntData(1) <> "" Then
         vntWhereRecursos(1) = vntData(1)
         vntWhereRecursos(2) = vntData(2)
      End If
      Load frmDietario
      Call objSecurity.AddHelpContext(AGHelpIDDietarioRecurso)
      Call frmDietario.Show(vbModal)
      'Unload frmDietario
      Call objSecurity.RemoveHelpContext
      Set frmDietario = Nothing
     Case AGWinPerfiles
      frmRecurso.MousePointer = vbHourglass
      Load frmPerfiles
      frmRecurso.MousePointer = vbDefault
      Call objSecurity.AddHelpContext(AGHelpIDPerfiles)
      Call frmPerfiles.Show(vbModal)
      'Unload frmPerfiles
      Call objSecurity.RemoveHelpContext
      Set frmPerfiles = Nothing
     Case AGWinFranjas
      frmRecurso.MousePointer = vbHourglass
      Load frmFranjas
      frmRecurso.MousePointer = vbDefault
      Call objSecurity.AddHelpContext(AGHelpIDFranjas)
      Call frmFranjas.Show(vbModal)
      'Unload frmFranjas
      Call objSecurity.RemoveHelpContext
      Set frmFranjas = Nothing
     Case AGWinRestricciones
      If intTipoRest = agRestPorRecurso Then
        frmRecurso.MousePointer = vbHourglass
      Else
        frmFranjas.MousePointer = vbHourglass
      End If
      Load frmRestricciones
      If intTipoRest = agRestPorRecurso Then
        frmRecurso.MousePointer = vbDefault
        Call objSecurity.AddHelpContext(AGHelpIDRestriccionesR)
      Else
        frmFranjas.MousePointer = vbDefault
        Call objSecurity.AddHelpContext(AGHelpIDRestriccionesF)
      End If
      Call frmRestricciones.Show(vbModal)
      'Unload frmRestricciones
      Call objSecurity.RemoveHelpContext
      Set frmRestricciones = Nothing
     Case AGWinMotivosIncidencia
      Call objSecurity.AddHelpContext(AGHelpIDMotivosIncidencia)
      Call CodigosIncidencia
      Call objSecurity.RemoveHelpContext
     Case AGRepHisRecursos
       Call ReportHistoric(strProcess)
     Case AGRepHisPerVigCal
       Call ReportHistoric(strProcess)
     Case AGRepHisDiaEsp
       Call ReportHistoric(strProcess)
     Case AGRepHisPerVigFra
       Call ReportHistoric(strProcess)
     Case AGRepHisPerfiles
       Call ReportHistoric(strProcess)
     Case AGRepHisFranjas
       Call ReportHistoric(strProcess)
     Case AGWinParametros
       Call Parametros
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 28 procesos
  ReDim aProcess(1 To 29, 1 To 4) As Variant
      
      
  ' VENTANAS
  aProcess(1, 1) = AGWinTipoRest
  aProcess(1, 2) = "Mantenimiento de Tipos de Restricci�n"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = AGWinTipoRecursos
  aProcess(2, 2) = "Mantenimiento de Tipos de Recursos"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = AGWinCalendarios
  aProcess(3, 2) = "Mantenimiento de Calendarios"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = AGWinRecursos
  aProcess(4, 2) = "Mantenimiento de Recursos"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = AGWinRestricciones
  aProcess(5, 2) = "Relaci�n de Restriciones"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = AGWinPerfiles
  aProcess(6, 2) = "Mantenimiento de Perfiles"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = AGWinFranjas
  aProcess(7, 2) = "Mantenimiento de Franjas"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = AGWinIncidencias
  aProcess(8, 2) = "Incidencias"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
      
  aProcess(9, 1) = AGWinAgendaRecurso
  aProcess(9, 2) = "Agenda del Recurso"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = AGWinDietarioRecurso
  aProcess(10, 2) = "Dietario del Recurso"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
      
  aProcess(11, 1) = AGWinMotivosIncidencia
  aProcess(11, 2) = "Mantenimiento de C�digos de Incidencia"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow
      
     
  ' LISTADOS
  aProcess(12, 1) = AGRepTipoRest
  aProcess(12, 2) = "Relaci�n de Tipos de Restricci�n"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeReport
      
  aProcess(13, 1) = AGRepTipoRecursos
  aProcess(13, 2) = "Relaci�n de Tipos de Recursos"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeReport
  
  aProcess(14, 1) = AGRepCalendarios
  aProcess(14, 2) = "Relaci�n de Calendarios"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeReport
  
  aProcess(15, 1) = AGRepRecursos
  aProcess(15, 2) = "Relaci�n de Recursos"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeReport

  aProcess(16, 1) = AGRepPerfiles
  aProcess(16, 2) = "Relaci�n de Perfiles de Recurso"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeReport
  
  aProcess(17, 1) = AGRepFranjas
  aProcess(17, 2) = "Relaci�n de Franjas por Perfil"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeReport

  aProcess(18, 1) = AGRepAgendaRecurso
  aProcess(18, 2) = "Relaci�n de la Agenda del Recurso "
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeReport

  aProcess(19, 1) = AGRepDietarioRecurso
  aProcess(19, 2) = "Relaci�n del Dietario del Recurso"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeReport

  aProcess(20, 1) = AGRepMotivosIncidencia
  aProcess(20, 2) = "Relaci�n de C�digos de Incidencia"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeReport
  
  aProcess(21, 1) = AGRepActuacionesRecursos
  aProcess(21, 2) = "Relaci�n de Actuaciones por Recurso"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeReport

  aProcess(22, 1) = AGRepHisRecursos
  aProcess(22, 2) = "Hist�rico de Recursos"
  aProcess(22, 3) = True
  aProcess(22, 4) = cwTypeReport

  aProcess(23, 1) = AGRepHisPerVigCal
  aProcess(23, 2) = "Hist�rico de Per�odos de Vigencia de Calendarios"
  aProcess(23, 3) = True
  aProcess(23, 4) = cwTypeReport

  aProcess(24, 1) = AGRepHisDiaEsp
  aProcess(24, 2) = "Hist�rico de D�as Especiales"
  aProcess(24, 3) = True
  aProcess(24, 4) = cwTypeReport

  aProcess(25, 1) = AGRepHisPerVigFra
  aProcess(25, 2) = "Hist�rico de Per�odos de Vigencia de Franjas"
  aProcess(25, 3) = True
  aProcess(25, 4) = cwTypeReport

  aProcess(26, 1) = AGRepHisPerfiles
  aProcess(26, 2) = "Hist�rico de Perfiles"
  aProcess(26, 3) = True
  aProcess(26, 4) = cwTypeReport

  aProcess(27, 1) = AGRepHisFranjas
  aProcess(27, 2) = "Hist�rico de Franjas por Perfil"
  aProcess(27, 3) = True
  aProcess(27, 4) = cwTypeReport
  
  aProcess(28, 1) = AGWinParametros
  aProcess(28, 2) = "Mantenimiento de Par�metros"
  aProcess(28, 3) = True
  aProcess(28, 4) = cwTypeWindow

  aProcess(29, 1) = AGRepGradoOcupacionRec
  aProcess(29, 2) = "Relaci�n de Grados Ocupaci�n de Recursos"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeReport

End Sub
