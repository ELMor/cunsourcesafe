VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form frmRecurso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Recursos"
   ClientHeight    =   6720
   ClientLeft      =   900
   ClientTop       =   1680
   ClientWidth     =   11025
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "Recurso.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   11025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   11025
      _ExtentX        =   19447
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Recursos"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5865
      Index           =   0
      Left            =   90
      TabIndex        =   13
      Tag             =   "Mantenimiento de Recursos"
      Top             =   450
      Width           =   10770
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Restricciones"
         Enabled         =   0   'False
         Height          =   375
         Index           =   1
         Left            =   9135
         TabIndex        =   37
         Top             =   5280
         Width           =   1455
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Perfiles"
         Enabled         =   0   'False
         Height          =   375
         Index           =   0
         Left            =   7260
         TabIndex        =   36
         Top             =   5280
         Width           =   1455
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   3990
         Index           =   0
         Left            =   120
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   930
         Width           =   10515
         _ExtentX        =   18547
         _ExtentY        =   7038
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "Recurso.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(7)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(8)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtcDateCombo1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboSSDBCombo1(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboSSDBCombo1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "cboSSDBCombo1(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "chkCheck1(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "UpDown1"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(4)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(3)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "fraFrame2"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "Recurso.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraFrame2 
            Caption         =   "Persona Asociada"
            Height          =   1020
            Left            =   315
            TabIndex        =   27
            Top             =   1185
            Width           =   9990
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30101
               Index           =   8
               Left            =   7140
               TabIndex        =   33
               TabStop         =   0   'False
               Tag             =   "Segundo Apellido"
               Top             =   510
               Width           =   2625
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30101
               Index           =   7
               Left            =   4230
               TabIndex        =   32
               TabStop         =   0   'False
               Tag             =   "Primer Apellido"
               Top             =   510
               Width           =   2625
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30101
               Index           =   6
               Left            =   1365
               TabIndex        =   30
               TabStop         =   0   'False
               Tag             =   "Nombre Persona"
               Top             =   510
               Width           =   2625
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD06CODPERSONA"
               Height          =   330
               HelpContextID   =   30101
               Index           =   5
               Left            =   195
               TabIndex        =   28
               TabStop         =   0   'False
               Tag             =   "C�digo Persona"
               Top             =   510
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Segundo Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   13
               Left            =   7140
               TabIndex        =   35
               Top             =   285
               Width           =   1500
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Primer Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   4230
               TabIndex        =   34
               Top             =   285
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   1365
               TabIndex        =   31
               Top             =   285
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   210
               TabIndex        =   29
               Top             =   285
               Width           =   600
            End
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   3
            Left            =   6060
            TabIndex        =   25
            Tag             =   "C�digo Departamento"
            Text            =   "C�d Dep."
            Top             =   3330
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG05NUMINCIDEN"
            Height          =   330
            HelpContextID   =   30101
            Index           =   4
            Left            =   7920
            TabIndex        =   24
            Tag             =   "N� Incidencia"
            Text            =   "N�Incidencia"
            Top             =   3405
            Visible         =   0   'False
            Width           =   1095
         End
         Begin ComCtl2.UpDown UpDown1 
            Height          =   330
            Left            =   4860
            TabIndex        =   7
            Top             =   2655
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   582
            _Version        =   327681
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtText1(2)"
            BuddyDispid     =   196612
            BuddyIndex      =   2
            OrigLeft        =   4440
            OrigTop         =   1440
            OrigRight       =   4680
            OrigBottom      =   1770
            Max             =   32700
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   0   'False
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG11NUMUNIDREC"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   3780
            MaxLength       =   10
            TabIndex        =   6
            Tag             =   "N� Unidades"
            Top             =   2655
            Width           =   1050
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Planificable"
            DataField       =   "AG11INDPLANIFI"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   8925
            TabIndex        =   4
            Tag             =   "Indicador Planificable"
            Top             =   735
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   345
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "C�digo Recurso"
            Top             =   690
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG11DESRECURSO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   1560
            TabIndex        =   2
            Tag             =   "Descripci�n"
            Top             =   690
            Width           =   3540
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3435
            Index           =   0
            Left            =   -74925
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   420
            Width           =   10290
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18150
            _ExtentY        =   6059
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "AG02CODCALENDA"
            Height          =   330
            Index           =   2
            Left            =   420
            TabIndex        =   5
            Tag             =   "Calendario"
            Top             =   2655
            Width           =   3015
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefColWidth     =   4471
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   4471
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   5318
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            Columns(2).Width=   2566
            Columns(2).Caption=   "Fecha Baja"
            Columns(2).Name =   "Fecha Baja"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "AG14CODTIPRECU"
            Height          =   330
            Index           =   1
            Left            =   5430
            TabIndex        =   3
            Tag             =   "Tipo Recurso"
            Top             =   690
            Width           =   3240
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   4260
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Planificable"
            Columns(2).Name =   "Planificable"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "Fecha Baja"
            Columns(3).Name =   "Fecha Baja"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   7
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "TablaAosc"
            Columns(4).Name =   "TablaAsoc"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   5715
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "AG11MODASIGCITA"
            Height          =   330
            Index           =   3
            Left            =   5460
            TabIndex        =   8
            Tag             =   "Modo de Asignaci�n"
            Top             =   2655
            Width           =   3255
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            FieldDelimiter  =   "'"
            FieldSeparator  =   ","
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   5741
            Columns(1).Caption=   "Modo de Asignaci�n"
            Columns(1).Name =   "Modo de Asignaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5741
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG11FECINIVREC"
            Height          =   330
            Index           =   0
            Left            =   420
            TabIndex        =   9
            Tag             =   "Fecha Inicio Vigencia (DD/MM/YYYY)"
            Top             =   3375
            Width           =   1935
            _Version        =   65537
            _ExtentX        =   3413
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1997/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BevelColorFace  =   12632256
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG11FECFINVREC"
            Height          =   330
            Index           =   1
            Left            =   2940
            TabIndex        =   10
            Tag             =   "Fecha Fin Vigencia (DD/MM/YYYY)"
            Top             =   3375
            Width           =   1935
            _Version        =   65537
            _ExtentX        =   3413
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1997/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BevelColorFace  =   12632256
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� de Unidades"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   3780
            TabIndex        =   23
            Top             =   2415
            Width           =   1350
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   2940
            TabIndex        =   22
            Top             =   3135
            Width           =   1650
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Calendario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   405
            TabIndex        =   14
            Top             =   2415
            Width           =   915
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   420
            TabIndex        =   20
            Top             =   3135
            Width           =   1860
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   5400
            TabIndex        =   19
            Top             =   450
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Modo de Asignaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   5460
            TabIndex        =   18
            Top             =   2415
            Width           =   1740
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   360
            TabIndex        =   17
            Top             =   450
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1560
            TabIndex        =   16
            Top             =   450
            Width           =   1020
         End
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   6660
         TabIndex        =   0
         Tag             =   "Departamento"
         Top             =   585
         Width           =   3975
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AutoRestore     =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1667
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   5609
         Columns(1).Caption=   "Departamento"
         Columns(1).Name =   "Departamento"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7011
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   6645
         TabIndex        =   26
         Top             =   330
         Width           =   1200
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   6435
      Width           =   11025
      _ExtentX        =   19447
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Declaraci�n del objeto ventana
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

'******************************************************************
' Procedimiento que nos devuelve en intCodMotInci el c�digo
' del motivo de incidencia si lo ha habido, si no devuelve 0
' en intCodMotInci
'******************************************************************

Sub Disminucion_Disponibilidad()
' Inicializo intCodMotInci
  intCodMotInci = 0
 
  With objWinInfo.objWinActiveForm
  ' Si no se esta a�adiendo un recurso nuevo
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
    ' Comprobaci�n disminuci�n en n� de unidades de recursos
   '   If .rdoCursor("AG11NUMUNIDREC") > val(txtText1(2).Text) Then
   '     intCodMotInci = 70 ' Disminuir el n� de unidades
   '   End If
   ' Comprobaci�n cambio en calendario
   '   If cboSSDBCombo1(0).Columns(0).Value <> Val(.rdoCursor("AG02CODCALENDA")) Then
   '     intCodMotInci = 130 ' Cambio Calendario
   '   End If
     If dtcDateCombo1(1).Text <> "" Then
       intCodMotInci = 80
     End If
    End If
  End With
End Sub

Private Sub cmdCommand1_Click(intIndex As Integer)
'******************************************************************
'Cuando pulsan el bot�n de perfiles le paso la Where a la ventana
'de Perfiles
'******************************************************************
  If objWinInfo.objWinActiveForm.blnChanged = True Then
    objWinInfo.DataRefresh
  End If
 
 If intIndex = 0 Then
   With objWinInfo.objWinActiveForm
     strWherePerfInicial = "AG11CODRECURSO=" & Val(.rdoCursor("AG11CODRECURSO"))
    'Si hay un filtro activado le paso la Where del filtro
     If .blnFilterOn = True Then
       strWherePerfiles = .objFilter.strWhere
     Else
      'Si no hay filtro le paso la Where del form
       strWherePerfiles = .strWhere
     End If
   End With
    'Cargamos el form de perfiles
    Load frmPerfiles
    Call frmPerfiles.Show(vbModal)
    Unload frmPerfiles
    Set frmPerfiles = Nothing
  End If
 
  If intIndex = 1 Then
    'Cargamos el form de perfiles
    intTipoRest = agRestPorRecurso
    Load frmRestricciones
    Call frmRestricciones.Show(vbModal)
    Unload frmRestricciones
    Set frmRestricciones = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form detalle
  Dim objDetailInfo As New clsCWForm
' Base de datos y tabla
  Dim strKey As String
  
' **********************************
' Fin declaraci�n de variables
' **********************************

' Se visualiza el formulario de splash
  'Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Introducir la documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Recursos"
    .cwDAT = "17-07-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite mantener los recursos de la CUN"
    
    .cwUPD = "17-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form detalle
  With objDetailInfo
 
  ' Asignaci�n del nombre del form
    .strName = "Recursos"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la base de datos y tabla del form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1100"
    .blnAskPrimary = False
    .strWhere = "AG1100.AG11CODRECURSO=0"

' M�todo de ordenacion del form
    Call .FormAddOrderField("AG11CODRECURSO", cwAscending)
    
' Definici�n del impreso
    Call .objPrinter.Add("AG0003", "Listado de Recursos")
    Call .objPrinter.Add("AG0010", "Listado Actuaciones por Recursos")
    
'    .blnHasMaint = True
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Recursos")
    Call .FormAddFilterWhere(strKey, "AG11CODRECURSO", "C�digo Recurso", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG11DESRECURSO", "Descripci�n Recurso", cwString)
    Call .FormAddFilterWhere(strKey, "AG14CODTIPRECU", "Tipo Recurso", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG11INDPLANIFI", "Indicador Planificable", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG02CODCALENDA", "Calendario", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG11MODASIGCITA", "Modo Asignaci�n Cita", cwString)
    Call .FormAddFilterWhere(strKey, "AG11FECINIVREC", "Fecha Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "AG11FECFINVREC", "Fecha Fin Vigencia", cwDate)
    
    Call .FormAddFilterOrder(strKey, "AG11CODRECURSO", "C�digo Recurso")
    Call .FormAddFilterOrder(strKey, "AG11DESRECURSO", "Descripci�n Recurso")
    Call .FormAddFilterOrder(strKey, "AG14CODTIPRECU", "Tipo Recurso")
  End With
   
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
 
  ' Se a�ade el formulario a la ventana
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
  ' Se obtiene informaci�n de las caracter�sticas de
  ' los controles del formulario
    Call .FormCreateInfo(objDetailInfo)
        
' Campos que intervienen en busquedas
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(2)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
  
'Valores por defecto
    .CtrlGetInfo(txtText1(2)).vntDefaultValue = 1
  
'Campos con lista de valores
    .CtrlGetInfo(txtText1(5)).blnForeign = True
  
  
 'Campos relcacionados entre si
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "AD06CODPERSONA", "SELECT AD06CODPERSONA, AD06NOMBRE, AD06PRIAPEL, AD06SEGAPEL FROM " & objEnv.GetValue("Database") & "AD0600 WHERE AD06CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "AD06NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(7), "AD06PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(8), "AD06SEGAPEL")
  

'    .CtrlGetInfo(txtText1(1)).blnForeign = True
  
  ' Valores con los que se cargara las DbCombo
    .CtrlGetInfo(cboSSDBCombo1(0)).blnNegotiated = False
    .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02INDRESPONPROC=-1 ORDER BY AD02DESDPTO"
    .CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT AG14CODTIPRECU, AG14DESTIPRECU, AG14INDPLANIFI, AG14FECFIVGTR, AG14TABLAASOC FROM " & objEnv.GetValue("Database") & "AG1400 ORDER BY AG14DESTIPRECU"
    .CtrlGetInfo(cboSSDBCombo1(2)).strSql = "SELECT AG02CODCALENDA, AG02DESCALENDA, AG02FECBAJA FROM " & objEnv.GetValue("Database") & "AG0200 WHERE AG02INDCALPRIN=0 ORDER BY AG02INDCALPRIN, AG02DESCALENDA"
    cboSSDBCombo1(3).AddItem ("'1','POR CANTIDAD'")
    cboSSDBCombo1(3).AddItem ("'2','SECUENCIAL'")
    cboSSDBCombo1(3).AddItem ("'3','INTERVALOS PREDETERMINADOS'")

 
 ' Propiedades del campo clave para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(0)).blnValidate = False
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
  
   
  ' Eliminamos campos del grid
    .CtrlGetInfo(txtText1(3)).blnInGrid = False
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
  ' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
  ' Se estabiliza la ventana configurando las propiedades
  ' de los controles
    Call .WinStabilize
   'Cargo el calendario principal como valor por defecto
   ' cboSSDBCombo1(2).MoveFirst
   ' .CtrlGetInfo(cboSSDBCombo1(2)).vntDefaultValue = cboSSDBCombo1(2).Columns(0).Value
    .CtrlGetInfo(dtcDateCombo1(0)).vntDefaultValue = Format(objGen.GetDBDateTime, "DD/MM/YYYY")
  End With
  If cboSSDBCombo1(0).Rows = 0 Then
    cboSSDBCombo1(0).Enabled = False
  End If
' Se oculta el formulario de splash
  'Call objApp.SplashOff
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
' Si me crean un nuevo recurso desabilito los botones de perfiles y
' restricciones.
  If intNewStatus = cwModeSingleAddRest Then
    cmdCommand1(0).Enabled = False
    cmdCommand1(1).Enabled = False
  End If
  If intNewStatus = cwModeSingleEmpty Then
    UpDown1.Enabled = False
  Else
    UpDown1.Enabled = True
  End If
End Sub


Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
'******************************************************************
' Validaciones del form Recursos
'******************************************************************
   
'******************************************************************
   ' Validaci�n de Fecha Fin mayor que Fecha Inicio
'******************************************************************
    If IsDate(dtcDateCombo1(1).Date) Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
         Call objError.SetError(cwCodeMsg, "La Fecha Fin es  menor que Fecha Inicial")
         Call objError.Raise
         blnCancel = True
         Exit Sub
      End If
    End If
    
  'Validaci�n de Tipo de Recurso no dado de  baja
   If cboSSDBCombo1(1).Columns(3).Text <> "" Then
     Call objError.SetError(cwCodeMsg, "El Tipo de Recurso elegido no est� vigente")
     Call objError.Raise
     blnCancel = True
     Exit Sub
   End If
  
  'Validaci�n de Calendario elegido no dado de  baja
   'cboSSDBCombo1(2).DoClick
   If cboSSDBCombo1(2).Columns(2).Value <> "" Then
     Call objError.SetError(cwCodeMsg, "El Calendario elegido no est� vigente")
     Call objError.Raise
     blnCancel = True
     Exit Sub
   End If




'******************************************************************
  ' Comprobaci�n disminuci�n de disponibilidad
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      Call Disminucion_Disponibilidad
    End If

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'******************************************************************
' Compruebo si el usuarario ha cancelado o no la incidencia para
' validar los cambios o anularlos
'******************************************************************

' Variable para el resultset para dar de alta el perfil gen�rico
  Dim rdoPerfil As rdoResultset
' Variable para el resultset para dar de alta el periodo de vigencia
  Dim rdoPeriodo As rdoResultset
  Dim strSql As String
    
  If blnRollback = True Or blnError = True Then
    Call objApp.rdoConnect.RollbackTrans
    blnRollback = False
  Else
    '****************************************************
    '  NO SE SI TENGO QUE HACERLO YO
    '****************************************************
    
'******************************************************************
    'Si estamos a�adiendo un nuevo recurso creamos su perfil gen�rico
    'y el periodo de vigencia del perfil gen�rico
 '******************************************************************
   
'    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
    ' Abrimos cursor para crear el perfil gen�rico
      
      'No se si hay que hacerlo pero si se hace falta
      'crear query con maxrow=1
'      strSql = "select AG11CODRECURSO, AG07CODPERFIL, AG07DESPERFIL, AG07INDPERGENE " _
'               & " from AG0700 WHERE AG11CODRECURSO=" & Val(txtText1(0)) _
               & " order by  AG11CODRECURSO desc"
'      Set rdoPerfil = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
      
      
'      rdoPerfil.AddNew
'      rdoPerfil("AG11CODRECURSO") = txtText1(0)
'      rdoPerfil("AG07CODPERFIL") = 1 'Es el primero
'      rdoPerfil("AG07DESPERFIL") = "PERFIL GENERICO"
'      rdoPerfil("AG07INDPERGENE") = -1 ' ACTIVO
'      rdoPerfil.Update
   
  
    ' Abrimos cursor para crear el periodo de vigencia
'      strSql = "select AG11CODRECURSO, AG07CODPERFIL, AG09NUMPVIGPER, " _
              & "AG09FECINVIPER, AG09FECFIVIPER from AG0900 "
           
'      Set rdoPeriodo = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
    
'      rdoPeriodo.AddNew
'      rdoPeriodo("AG11CODRECURSO") = txtText1(0)
'      rdoPeriodo("AG07CODPERFIL") = 1 'Es el primero
'      rdoPeriodo("AG09NUMPVIGPER") = 1 'Es el primero
'      rdoPeriodo("AG09FECINVIPER") = "1/1/1990"
'      rdoPeriodo("AG09FECFIVIPER") = "31/12/2100"
'      rdoPeriodo.Update
'      rdoPeriodo.Close
'      rdoPerfil.Close
  
'    End If
    
    'Valido el alta y activo los botones de Perfiles y Restricciones
    Call objApp.rdoConnect.CommitTrans
    
    cmdCommand1(0).Enabled = True
    cmdCommand1(1).Enabled = True
  
  End If

End Sub

Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
'******************************************************************
' Si se borra un recurso hay que craer un registro de incidencia
'******************************************************************
'  intCodMotInci = 80 ' Borrado de recurso
 
' Carga de la ventana Incidencias
'  Load frmIncidencias
      
'  blnCancel = blnRollback
' Descargamos el form de incidencias de memoria
'  Unload frmIncidencias
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, ByRef blnError As Boolean)
' Paso el valor del c�digo de departamento
  
  Call objWinInfo.CtrlSet(txtText1(3), cboSSDBCombo1(0).Value)
End Sub


Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Variable para las sentencia Sql que abrira el cursor
' para dar de alta la incidencia
  Dim strSql As String
' Variable para el resultset para obtener el nuevo c�digo de recurso
  Dim rdoRecursos As rdoResultset
    
'******************************************************************
  'Si estamos en modo editar miramos si ha habido disminuci�n
  'de disponibilidad
'******************************************************************
  If objWinInfo.intWinStatus = cwModeSingleEdit Then
  
  ' Miramos si hay disminuci�n
    If intCodMotInci <> 0 Then
      
    ' Iniciamos la transacci�n
      Call objApp.rdoConnect.BeginTrans
    ' Carga de la ventana Incidencias
    '  Load frmIncidencias
      Call AddInciden(intCodMotInci, False)
      If blnActive And Not blnRollback Then
      ' Asignamos el n�mero de incidencia al campo n� de incidencia
        Call objWinInfo.CtrlStabilize(txtText1(4), vntNuevoCod)
      End If
    ' Descargamos el form de incidencias de memoria
    '  Unload frmIncidencias
    End If
  End If
  
'******************************************************************
  'Si estamos a�adiendo un nuevo registro obtenenemos su nuevo
  'c�digo de recurso y creamos su perfil gen�rico
'******************************************************************
     
  If objWinInfo.intWinStatus = cwModeSingleAddRest Then
  ' Abrimos cursor
    strSql = "select AG11CODRECURSO from AG1100 order by  AG11CODRECURSO desc"
  ' Empieza transacci�n
    Call objApp.rdoConnect.BeginTrans
    'Obtengo el nuevo c�digo
    vntNuevoCod = GetNewCode(strSql)
    'Paso el nuevo c�digo al campo del cursor y a la textbox
    Call objWinInfo.CtrlStabilize(txtText1(0), vntNuevoCod)
    
  End If
   
 
   
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "Recursos" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(False)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If

End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Recursos" Then
    
    If strCtrl = "txtText1(5)" Then
      Set objSearch = New clsCWSearch
      With objSearch
       .strTable = "AD0600"
       .strWhere = ""
       .strOrder = ""
      
       Set objField = .AddField("AD06CODPERSONA")
       objField.strSmallDesc = "C�digo Persona"
       Set objField = .AddField("AD06NOMBRE")
       objField.strSmallDesc = "Nombre"
       Set objField = .AddField("AD06PRIAPEL")
       objField.strSmallDesc = "Primer Apellido "
       Set objField = .AddField("AD06SEGAPEL")
       objField.strSmallDesc = "Segundo Apellido"
       
       If .Search Then
        Call objWinInfo.CtrlSet(txtText1(5), .cllValues("AD06CODPERSONA"))
       End If
      End With
     End If
     Set objSearch = Nothing
   End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)

  Call objWinInfo.CtrlDataChange
 'Paso el valor de planificable del Tipo de Recurso
  If intIndex = 1 And objWinInfo.intWinStatus <> cwModeSingleEmpty Then
    chkCheck1(0).Value = -Val(cboSSDBCombo1(1).Columns(2).Value)
  End If

End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 1 Then
    If cboSSDBCombo1(1).Columns("TablaAsoc").Value <> "" Then
      fraFrame2.Enabled = True
    Else
      
      Call objWinInfo.CtrlSet(txtText1(5), "")
      
      Call objWinInfo.CtrlSet(txtText1(6), "")
      
      Call objWinInfo.CtrlSet(txtText1(7), "")
      
      Call objWinInfo.CtrlSet(txtText1(8), "")
      fraFrame2.Enabled = False
    End If
  End If
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 0 Then
'******************************************************************
  ' Cambio de departamento
'******************************************************************
  ' Modifico la strWhere para obtener los nuevos registro del
  ' departamento elegido y refresco el cursor
    With objWinInfo
      .objWinMainForm.strInitialWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value
      .objWinMainForm.strWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value
      '.DataMoveFirst
      '.DataRead
       .DataRefresh
    End With
   'Si hay alg�n recurso en ese departamento habilito los botones
   'de perfiles y restricciones
    If objGen.GetRowCount(objWinInfo.objWinMainForm.rdoCursor) > 0 Then
      cmdCommand1(0).Enabled = True
      cmdCommand1(1).Enabled = True
    Else
      cmdCommand1(0).Enabled = False
      cmdCommand1(1).Enabled = False
    End If
    Exit Sub
  End If
  
  If intIndex = 1 Then
    If cboSSDBCombo1(1).Columns("TablaAsoc").Value <> "" Then
      fraFrame2.Enabled = True
    Else
      
      Call objWinInfo.CtrlSet(txtText1(5), "")
     
      Call objWinInfo.CtrlSet(txtText1(6), "")
      
      Call objWinInfo.CtrlSet(txtText1(7), "")
      
      Call objWinInfo.CtrlSet(txtText1(8), "")
      fraFrame2.Enabled = False
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


