VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{35ED254B-6E76-11D2-A484-00C04F7D9E25}#2.0#0"; "Horario.ocx"
Begin VB.Form frmAgendaRecurso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Consulta de la Agenda del Recurso"
   ClientHeight    =   8340
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   24
   Icon            =   "AG1008.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   240
      Left            =   0
      TabIndex        =   0
      Top             =   8100
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   423
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab TabSoporte 
      Height          =   7650
      Left            =   0
      TabIndex        =   2
      Top             =   435
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   13494
      _Version        =   327681
      Tabs            =   2
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Datos"
      TabPicture(0)   =   "AG1008.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraFrame1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Horario"
      TabPicture(1)   =   "AG1008.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "DiaRecurso1"
      Tab(1).ControlCount=   1
      Begin Control_Agenda.DiaRecurso DiaRecurso1 
         Height          =   7095
         Left            =   -75000
         TabIndex        =   32
         Top             =   360
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   12515
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Agenda"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000006&
         Height          =   4635
         Index           =   1
         Left            =   120
         TabIndex        =   14
         Top             =   2880
         Width           =   11655
         Begin VB.CommandButton cmdCommand1 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   1980
            TabIndex        =   18
            Top             =   1260
            Width           =   420
         End
         Begin VB.CommandButton cmdCommand1 
            Caption         =   "&Consultar"
            Enabled         =   0   'False
            Height          =   420
            Index           =   0
            Left            =   9480
            TabIndex        =   17
            Top             =   585
            Width           =   1830
         End
         Begin VB.TextBox txtText2 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   2700
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n"
            ToolTipText     =   "Descripci�n Actuaci�n"
            Top             =   1260
            Width           =   4185
         End
         Begin VB.TextBox txtText2 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   180
            TabIndex        =   15
            Tag             =   "Actuaci�n"
            ToolTipText     =   "Tipo Actuaci�n"
            Top             =   1260
            Width           =   1545
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   2790
            Left            =   5880
            TabIndex        =   19
            Top             =   1755
            Width           =   5670
            _ExtentX        =   10001
            _ExtentY        =   4921
            _Version        =   327681
            Style           =   1
            TabHeight       =   520
            TabCaption(0)   =   "&Actuaciones"
            TabPicture(0)   =   "AG1008.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "grdDBGrid2(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Re&stricciones"
            TabPicture(1)   =   "AG1008.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "TreeView1(0)"
            Tab(1).ControlCount=   1
            TabCaption(2)   =   "Restricciones del Rec&urso"
            TabPicture(2)   =   "AG1008.frx":007C
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "TreeView1(1)"
            Tab(2).ControlCount=   1
            Begin ComctlLib.TreeView TreeView1 
               Height          =   2730
               Index           =   0
               Left            =   -74880
               TabIndex        =   20
               Top             =   435
               Width           =   5415
               _ExtentX        =   9551
               _ExtentY        =   4815
               _Version        =   327682
               Style           =   7
               Appearance      =   1
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid2 
               Height          =   2205
               Index           =   2
               Left            =   150
               TabIndex        =   21
               Top             =   450
               Width           =   5325
               _Version        =   131078
               DataMode        =   2
               FieldSeparator  =   ";"
               AllowUpdate     =   0   'False
               AllowRowSizing  =   0   'False
               SelectTypeRow   =   1
               SelectByCell    =   -1  'True
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               ExtraHeight     =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   8705
               Columns(0).Caption=   "Actuaci�n"
               Columns(0).Name =   "Actuaci�n"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               UseDefaults     =   0   'False
               _ExtentX        =   9393
               _ExtentY        =   3889
               _StockProps     =   79
               ForeColor       =   0
            End
            Begin ComctlLib.TreeView TreeView1 
               Height          =   2730
               Index           =   1
               Left            =   -74880
               TabIndex        =   22
               Top             =   435
               Width           =   5415
               _ExtentX        =   9551
               _ExtentY        =   4815
               _Version        =   327682
               Style           =   7
               Appearance      =   1
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2760
            Index           =   1
            Left            =   120
            TabIndex        =   23
            Top             =   1800
            Width           =   5715
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            SelectByCell    =   -1  'True
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   10081
            _ExtentY        =   4868
            _StockProps     =   79
            Caption         =   "FRANJAS"
            ForeColor       =   0
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo2 
            Height          =   330
            Index           =   1
            Left            =   180
            TabIndex        =   24
            Tag             =   "Actividad"
            ToolTipText     =   "Actividad"
            Top             =   615
            Width           =   3255
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            FieldSeparator  =   ","
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   5741
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   5741
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   0
            Left            =   3930
            TabIndex        =   25
            Tag             =   "Fecha Desde"
            ToolTipText     =   "Fecha Desde"
            Top             =   615
            Width           =   1935
            _Version        =   65537
            _ExtentX        =   3413
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1997/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   8388608
            BevelColorFace  =   12632256
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   1
            Left            =   6150
            TabIndex        =   26
            Tag             =   "Fecha Hasta"
            ToolTipText     =   "Fecha Hasta"
            Top             =   615
            Width           =   1935
            _Version        =   65537
            _ExtentX        =   3413
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1997/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   8388608
            BevelColorFace  =   12632256
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Desde"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   3915
            TabIndex        =   31
            Top             =   405
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Hasta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   6165
            TabIndex        =   30
            Top             =   405
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actividad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   180
            TabIndex        =   29
            Top             =   405
            Width           =   810
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   2700
            TabIndex        =   28
            Top             =   1035
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   180
            TabIndex        =   27
            Top             =   1035
            Width           =   870
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Recurso"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2475
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   420
         Width           =   11625
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            Height          =   330
            Index           =   0
            Left            =   6960
            TabIndex        =   4
            Tag             =   "Departamento"
            Top             =   405
            Width           =   4305
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   7620
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   7594
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            DataFieldToDisplay=   "Column 1"
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   1830
            HelpContextID   =   90001
            Index           =   0
            Left            =   120
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   495
            Width           =   11355
            _ExtentX        =   20029
            _ExtentY        =   3228
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "AG1008.frx":0098
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(6)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(5)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "txtText1(0)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "txtText1(1)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "txtText1(2)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(3)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).ControlCount=   6
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "AG1008.frx":00B4
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   3
               Left            =   9840
               TabIndex        =   9
               Tag             =   "C�digo Depto"
               Top             =   840
               Visible         =   0   'False
               Width           =   1150
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AG02CODCALENDA"
               Height          =   330
               Index           =   2
               Left            =   7920
               TabIndex        =   8
               Tag             =   "C�digo Calendario"
               Text            =   "CodCalendario"
               Top             =   735
               Visible         =   0   'False
               Width           =   1150
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AG11DESRECURSO"
               Height          =   330
               Index           =   1
               Left            =   1830
               TabIndex        =   7
               Tag             =   "Descripci�n Recurso"
               Top             =   720
               Width           =   5340
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H0000FFFF&
               DataField       =   "AG11CODRECURSO"
               Height          =   330
               Index           =   0
               Left            =   360
               TabIndex        =   6
               Tag             =   "C�digo Recurso|C�digo"
               Top             =   735
               Width           =   1150
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1230
               Index           =   0
               Left            =   -74880
               TabIndex        =   10
               Top             =   405
               Width           =   10875
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowRowSizing  =   0   'False
               SelectTypeRow   =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   19182
               _ExtentY        =   2170
               _StockProps     =   79
               ForeColor       =   0
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   1830
               TabIndex        =   12
               Top             =   495
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   390
               TabIndex        =   11
               Top             =   495
               Width           =   600
            End
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   6960
            TabIndex        =   13
            Top             =   210
            Width           =   1200
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAgendaRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Rem bind repasar
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim strUsuario As String
Dim dteFechaConsulta As Date
Dim lngNumSec As Long
Dim strFuncion As String
Dim strProceso As String
Dim rdoConsultaFranjas As rdoResultset
Dim strSql As String
Public Sub A�adirFranja(lngRecursoActual As Long, lngPerfilActual As Long, lngFranjaActual As Long, dteFechaActual, intHorIni As Integer, intMinIni As Integer, intHorFin As Integer, intMinFin As Integer, strActividadActual As String, intIndPlanific As Integer, intDia As Integer)
  Dim strDia As String
  
  rdoConsultaFranjas.AddNew
  rdoConsultaFranjas("AG26CODUSUARIO") = strUsuario
  rdoConsultaFranjas("AG26FECCONSULTA") = dteFechaConsulta
  lngNumSec = lngNumSec + 1
  rdoConsultaFranjas("AG26SECUENCIA") = lngNumSec
  rdoConsultaFranjas("AG26CODFUNCION") = strFuncion 'SISTEMA
  rdoConsultaFranjas("AG26CODPROCESO") = strProceso
  rdoConsultaFranjas("AG26CODRECURSO") = lngRecursoActual
  rdoConsultaFranjas("AG26CODPERFIL") = lngPerfilActual
  rdoConsultaFranjas("AG26CODFRANJA") = lngFranjaActual
  rdoConsultaFranjas("AG26FECFRANJA") = dteFechaActual
  rdoConsultaFranjas("AG26HORINFRHH") = intHorIni
  rdoConsultaFranjas("AG26HORINFRMM") = intMinIni
  rdoConsultaFranjas("AG26HORFIFRHH") = intHorFin
  rdoConsultaFranjas("AG26HORFIFRMM") = intMinFin
  rdoConsultaFranjas("AG26ACTIVIDAD") = strActividadActual
  rdoConsultaFranjas("AG26INDPLANIFIC") = intIndPlanific
     
      
  If intDia > 1 Then
    strDia = DiaSemana(intDia - 2)
  Else
    strDia = DiaSemana(6)
  End If
  
  If intDia = 4 Then
    rdoConsultaFranjas("AG26DIASEMANA") = "X"
  Else
    rdoConsultaFranjas("AG26DIASEMANA") = Left(strDia, Len(strDia) - 1)
  End If
  rdoConsultaFranjas.Update

End Sub
Sub AgendaActiva()
  If objWinInfo.objWinActiveForm.strName = "Recurso" Then
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  End If
End Sub
Public Function ObtenerPerfil(dteFecha As Date) As Long
  Dim intPos As Integer
  
  For intPos = 1 To cllPubPerfil.Count
    If CDate(cllPubFecDes(intPos)) <= dteFecha And CDate(cllPubFecHas(intPos)) >= dteFecha Then
      ObtenerPerfil = cllPubPerfil(intPos)
      Exit For
    End If
  Next intPos
End Function
Public Sub RecuperarRestricciones(lngRecurso As Long, lngPerfil As Long, lngFranja As Long)
  intTipoRest = agRestPorFranja
  Call CargarTreeView(intTipoRest, Me, 0, lngRecurso, lngPerfil, lngFranja)

End Sub

Public Sub RecuperarActuaciones(lngCodRecurso As Long, lngPerfil As Long, lngFranja As Long)
  Dim rdoActuaciones As rdoResultset
  Dim rdoDesAct As rdoResultset
  Dim rdoDepartamento As rdoResultset
  Dim rdoConsulta1 As rdoQuery
  Dim rdoConsulta2 As rdoQuery
  Dim strSql As String
  Dim strDesDepart As String
  
'  strSql = "SELECT PR01CODACTUACION, AD02CODDPTO FROM AG0100 " _
'          & " WHERE AG11CODRECURSO=" & lngCodRecurso _
'          & " AND AG07CODPERFIL=" & lngPerfil _
'          & " AND AG04CODFRANJA=" & lngFranja _
'          & " AND (AG01FECFIVGACF > SYSDATE OR AG01FECFIVGACF IS NULL)"
  strSql = "SELECT PR01CODACTUACION, AD02CODDPTO FROM AG0100 " _
          & " WHERE AG11CODRECURSO= ? " _
          & " AND AG07CODPERFIL= ? " _
          & " AND AG04CODFRANJA= ? " _
          & " AND (AG01FECFIVGACF > SYSDATE OR AG01FECFIVGACF IS NULL)"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Set rdoConsulta1 = objApp.rdoConnect.CreateQuery("", strSql)
  rdoConsulta1(0) = lngCodRecurso
  rdoConsulta1(1) = lngPerfil
  rdoConsulta1(2) = lngFranja
  Set rdoActuaciones = rdoConsulta1.OpenResultset()
'  Set rdoActuaciones = objApp.rdoConnect.OpenResultset(strSql)
  If Not rdoActuaciones.EOF Then
    Do While Not rdoActuaciones.EOF
'      strSql = "SELECT AD02DESDPTO FROM AD0200 " _
'            & " WHERE AD02CODDPTO=" & rdoActuaciones(1) _
'            & " AND (AD02FECFIN > SYSDATE OR AD02FECFIN IS NULL)"
      strSql = "SELECT AD02DESDPTO FROM AD0200 " _
            & " WHERE AD02CODDPTO= ? " _
            & " AND (AD02FECFIN > SYSDATE OR AD02FECFIN IS NULL)"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Set rdoConsulta2 = objApp.rdoConnect.CreateQuery("", strSql)
  rdoConsulta2(0) = rdoActuaciones(1)
  Set rdoDepartamento = rdoConsulta2.OpenResultset()
'      Set rdoDepartamento = objApp.rdoConnect.OpenResultset(strSql)
      If Not rdoDepartamento.EOF Then
         strDesDepart = rdoDepartamento(0)
      Else
        strDesDepart = ""
      End If
  rdoDepartamento.Close
  rdoConsulta2.Close
  
'      strSql = "SELECT PR01DESCORTA FROM PR0100 " _
'            & " WHERE PR01CODACTUACION=" & rdoActuaciones(0) _
'            & " AND (PR01FECFIN > SYSDATE OR PR01FECFIN IS NULL)"
      strSql = "SELECT PR01DESCORTA FROM PR0100 " _
            & " WHERE PR01CODACTUACION= ? " _
            & " AND (PR01FECFIN > SYSDATE OR PR01FECFIN IS NULL)"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Set rdoConsulta2 = objApp.rdoConnect.CreateQuery("", strSql)
  rdoConsulta2(0) = rdoActuaciones(0)
  Set rdoDesAct = rdoConsulta2.OpenResultset()
'      Set rdoDesAct = objApp.rdoConnect.OpenResultset(strSql)
      If Not rdoDesAct.EOF Then
        grdDBGrid2(2).AddItem rdoDesAct(0) & Chr$(10) & vbCr & "Dept:" & strDesDepart '& ";" _
                   '& rdoActuaciones(1)
        rdoDesAct.Close
      End If
      rdoActuaciones.MoveNext
    Loop
  End If
  rdoActuaciones.Close
  rdoConsulta2.Close
  rdoConsulta1.Close
End Sub


Public Sub RecuperarFranjas(lngRecurso As Long, dteFecha As Date, intDia As Integer)
  Dim rdoFranjas As rdoResultset
  Dim rdoActividad As rdoResultset
  Dim rdoConsulta As rdoQuery 'LAS 14.4.99
  Dim rdoConsulta1 As rdoQuery 'LAS 14.4.99
  Dim strSql As String
  Dim strDesActividad As String
  Dim intPlanificable As Integer
  Dim lngPerfil As Long
  Dim strWhereActividad As String
  Dim strSelect As String
  Dim strFrom As String
  Dim strWhere As String
  Dim bindCount As Byte 'LAS 14.4.99
  
  lngPerfil = ObtenerPerfil(dteFecha)
  strSelect = "SELECT AG0400.AG04CODFRANJA, AG0400.AG04HORINFRJHH, AG0400.AG04HORINFRJMM, " _
       & " AG0400.AG04HORFIFRJHH, AG0400.AG04HORFIFRJMM, AG0400.PR12CODACTIVIDAD "
  
''|| || || || || || || || || || || || || || || || || || || || || || ||
''\/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/
'  If txtText2(0).Text <> "" Then
'    strFrom = " FROM AG0400, AG0100 "
'    strWhere = " WHERE AG0400.AG11CODRECURSO=AG0100.AG11CODRECURSO " _
'             & " AND AG0400.AG07CODPERFIL=AG0100.AG07CODPERFIL " _
'             & " AND AG0400.AG04CODFRANJA=AG0100.AG04CODFRANJA " _
'             & " AND AG0100.PR01CODACTUACION=" & Val(txtText2(0)) _
'             & " AND AG0400.AG11CODRECURSO=" & lngRecurso _
'             & " AND AG0400.AG07CODPERFIL=" & lngPerfil _
'             & " AND (TO_DATE(TO_CHAR(AG0400.AG04FECBAJA,'DD-MM-YYYY')," _
'             & "'DD-MM-YYYY') > TO_DATE('" & Format(dteFecha, "DD-MM-YYYY") _
'             & "','DD-MM-YYYY') or AG0400.AG04FECBAJA is null)" _
'             & " AND " & DiaAplicable(intDia) & "=-1"
'
'  Else
'    strFrom = " FROM AG0400 "
'    strWhere = "WHERE AG11CODRECURSO=" & lngRecurso _
'       & " AND AG07CODPERFIL=" & lngPerfil _
'       & " AND (TO_DATE(TO_CHAR(AG0400.AG04FECBAJA,'DD-MM-YYYY')," _
'       & "'DD-MM-YYYY') > TO_DATE('" & Format(dteFecha, "DD-MM-YYYY") _
'       & "','DD-MM-YYYY') or AG0400.AG04FECBAJA is null)" _
'       & " AND " & DiaAplicable(intDia) & "=-1"
'  End If
'  If cboSSDBCombo2(1).Text <> "" Then
'    strWhereActividad = " AND AG0400.PR12CODACTIVIDAD=" & cboSSDBCombo2(1).Value _
'                        & " AND (TO_DATE(TO_CHAR(AG0400.AG04FECBAJA,'DD-MM-YYYY')," _
'                         & "'DD-MM-YYYY') > TO_DATE('" & Format(dteFecha, "DD-MM-YYYY") _
'                         & "','DD-MM-YYYY') or AG0400.AG04FECBAJA is null)" _
'
'  Else
'    strWhereActividad = ""
'  End If
''/\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\
''|| || || || || || || || || || || || || || || || || || || || || || ||
'|| || || || || || || || || || || || || || || || || || || || || || ||
'\/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/ \/
  If txtText2(0).Text <> "" Then
    strFrom = " FROM AG0400, AG0100 "
    strWhere = " WHERE AG0400.AG11CODRECURSO=AG0100.AG11CODRECURSO " _
             & " AND AG0400.AG07CODPERFIL=AG0100.AG07CODPERFIL " _
             & " AND AG0400.AG04CODFRANJA=AG0100.AG04CODFRANJA " _
             & " AND AG0100.PR01CODACTUACION= ? " _
             & " AND AG0400.AG11CODRECURSO= ? " _
             & " AND AG0400.AG07CODPERFIL= ? " _
             & " AND (TO_DATE(TO_CHAR(AG0400.AG04FECBAJA,'DD-MM-YYYY')," _
             & "'DD-MM-YYYY') > TO_DATE( ? ,'DD-MM-YYYY') or AG0400.AG04FECBAJA is null)" _
             & " AND " & DiaAplicable(intDia) & "=-1"
  Else
    strFrom = " FROM AG0400 "
    strWhere = "WHERE AG11CODRECURSO= ? " _
       & " AND AG07CODPERFIL= ? " _
       & " AND (TO_DATE(TO_CHAR(AG0400.AG04FECBAJA,'DD-MM-YYYY')," _
       & "'DD-MM-YYYY') > TO_DATE( ? ,'DD-MM-YYYY') or AG0400.AG04FECBAJA is null)" _
       & " AND " & DiaAplicable(intDia) & "=-1"
  End If
  If cboSSDBCombo2(1).Text <> "" Then
    strWhereActividad = " AND AG0400.PR12CODACTIVIDAD= ? " _
                        & " AND (TO_DATE(TO_CHAR(AG0400.AG04FECBAJA,'DD-MM-YYYY')," _
                         & "'DD-MM-YYYY') > TO_DATE( ? ,'DD-MM-YYYY') or AG0400.AG04FECBAJA is null)" _

  Else
    strWhereActividad = ""
  End If
'/\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\ /\
'|| || || || || || || || || || || || || || || || || || || || || || ||
  
  strSql = strSelect & strFrom & strWhere & strWhereActividad
  Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
  
  If txtText2(0).Text <> "" Then
    rdoConsulta(0) = Val(txtText2(0))
    rdoConsulta(1) = lngRecurso
    rdoConsulta(2) = lngPerfil
    rdoConsulta(3) = Format(dteFecha, "DD-MM-YYYY")
    bindCount = 4
  Else
    rdoConsulta(0) = lngRecurso
    rdoConsulta(1) = lngPerfil
    rdoConsulta(2) = Format(dteFecha, "DD-MM-YYYY")
    bindCount = 3
  End If
  If cboSSDBCombo2(1).Text <> "" Then
    rdoConsulta(0 + bindCount) = cboSSDBCombo2(1).Value
    rdoConsulta(1 + bindCount) = Format(dteFecha, "DD-MM-YYYY")
  End If

  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Set rdoFranjas = rdoConsulta.OpenResultset()
'  Set rdoFranjas = objApp.rdoConnect.OpenResultset(strSql)
  If rdoFranjas.EOF Then
    If strWhereActividad = "" And txtText2(0).Text = "" Then
      Call A�adirFranja(Val(txtText1(0)), lngPerfil, 0, CDate(dteFecha), 0, 0, 23, 59, "SIN ACTIVIDAD", 0, intDia)

    End If
  Else
    Do While Not rdoFranjas.EOF
      If Not IsNull(rdoFranjas("PR12CODACTIVIDAD")) Then
'        strSql = "SELECT PR12DESACTIVIDAD, PR12INDPLANIFIC FROM PR1200 WHERE PR12CODACTIVIDAD=" & rdoFranjas("PR12CODACTIVIDAD")
        strSql = "SELECT PR12DESACTIVIDAD, PR12INDPLANIFIC FROM PR1200 WHERE PR12CODACTIVIDAD= ? "
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Set rdoConsulta1 = objApp.rdoConnect.CreateQuery("", strSql)
  rdoConsulta1(0) = rdoFranjas("PR12CODACTIVIDAD")
  Set rdoActividad = rdoConsulta1.OpenResultset()
'        Set rdoActividad = objApp.rdoConnect.OpenResultset(strSql)
        If Not rdoActividad.EOF Then
          strDesActividad = rdoActividad(0)
          intPlanificable = rdoActividad(1)
        Else
          strDesActividad = ""
          intPlanificable = 0
        End If
      Else
        strDesActividad = "CONSULTA/PRUEBAS"
        intPlanificable = -1
      End If
      Call A�adirFranja(Val(txtText1(0)), lngPerfil, rdoFranjas(0), CDate(dteFecha), rdoFranjas("AG04HORINFRJHH"), rdoFranjas("AG04HORINFRJMM"), rdoFranjas("AG04HORFIFRJHH"), rdoFranjas("AG04HORFIFRJMM"), strDesActividad, intPlanificable, intDia)
     
      rdoFranjas.MoveNext
    Loop
  End If
'rdoActividad.Close
'rdoConsulta1.Close
'rdoFranjas.Close
'rdoConsulta.Close
End Sub

Private Function DiaAplicable(intDia As Integer) As String
  If intDia = 1 Then
    DiaAplicable = "AG04INDDOMFRJA"
  End If
  If intDia = 2 Then
    DiaAplicable = "AG04INDLUNFRJA"
  End If
  If intDia = 3 Then
    DiaAplicable = "AG04INDMARFRJA"
  End If
  If intDia = 4 Then
    DiaAplicable = "AG04INDMIEFRJA"
  End If
  If intDia = 5 Then
    DiaAplicable = "AG04INDJUEFRJA"
  End If
  If intDia = 6 Then
    DiaAplicable = "AG04INDVIEFRJA"
  End If
  If intDia = 7 Then
    DiaAplicable = "AG04INDSABFRJA"
  End If
  DiaAplicable = "AG0400." & DiaAplicable
End Function

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 And KeyCode = 46 Then
    cboSSDBCombo1(intIndex).Text = ""
    objWinInfo.objWinActiveForm.strWhere = ""
    objWinInfo.DataRefresh
  End If

End Sub

Private Sub cboSSDBCombo1_KeyPress(Index As Integer, KeyAscii As Integer)
'Cuando se pulsaba la tecla 'A' se posicionaba en el segundo
'departamento que empezaba por 'A'
If (Index = 0) And (KeyAscii = 65 Or KeyAscii = 97) Then

    If cboSSDBCombo1(Index).Text = "" Then
        KeyAscii = Asc("W")
    End If
    
End If
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
  If intIndex = 0 And cboSSDBCombo1(0).Text <> "" Then
'******************************************************************
  ' Cambio de departamento
'******************************************************************
  ' Modifico la strWhere para obtener los nuevos registro del
  ' departamento elegido y refresco el cursor
    With objWinInfo
      .objWinActiveForm.strWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value & " AND (AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
      .DataRefresh
      
    End With
  End If

End Sub


Private Sub cboSSDBCombo2_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 1 And KeyCode = 46 Then
    cboSSDBCombo2(intIndex).Text = ""
  End If

End Sub

Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim objField As clsCWFieldSearch
  Dim lngPosicion As Long
'  Dim rdoDelete As rdoQuery
  Dim rdoConsulta As rdoQuery
  
  If intIndex = 0 Then
    
    Call AgendaActiva
 
    grdDBGrid2(2).RemoveAll
    TreeView1(0).Nodes.Clear
    If dtcDateCombo1(0) = "" Or dtcDateCombo1(1) = "" Then
      Call objError.SetError(cwCodeMsg, "La Fecha Desde y la Fecha Hasta son obligatorios")
      Call objError.Raise
      Exit Sub
    Else
    
       If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
         If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
           Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
           Call objError.Raise
           Exit Sub
         End If
       End If

      Me.MousePointer = vbHourglass
      'strUsuario = objSecurity.strUser
      strUsuario = objApp.strUserName
      dteFechaConsulta = objGen.GetDBDateTime
'      strSql = "DELETE  AG2600 WHERE  AG26CODUSUARIO =" & objGen.ValueToSQL(strUsuario, cwString) _
'           & " AND AG26FECCONSULTA <>" & objGen.ValueToSQL(dteFechaConsulta, cwDate) _
'           & " AND AG26CODFUNCION =" & objGen.ValueToSQL(strFuncion, cwString) _
'           & " AND AG26CODPROCESO =" & objGen.ValueToSQL(strProceso, cwString)
      strSql = "DELETE  AG2600 WHERE  AG26CODUSUARIO =?" _
           & " AND AG26FECCONSULTA <>?" _
           & " AND AG26CODFUNCION =?" _
           & " AND AG26CODPROCESO =?"
  Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
  rdoConsulta(0) = objGen.ValueToSQL(strUsuario, cwString)
  rdoConsulta(0) = Format$(dteFechaConsulta, "dd/mm/yyyy")
  rdoConsulta(0) = objGen.ValueToSQL(strFuncion, cwString)
  rdoConsulta(0) = objGen.ValueToSQL(strProceso, cwString)
  rdoConsulta.Execute
           
           
'      strSql = "DELETE  AG2600 WHERE  AG26CODUSUARIO = ? " _
'           & " AND AG26FECCONSULTA <> ? " _
'           & " AND AG26CODFUNCION = ? " _
'           & " AND AG26CODPROCESO = ? "
'
'  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
'  Set rdoDelete = objApp.rdoConnect.CreateQuery("", strSql)
'  rdoDelete(0) = objGen.ValueToSQL(strUsuario, cwString)
'  rdoDelete(1) = objGen.ValueToSQL(dteFechaConsulta, cwDate)
'  rdoDelete(2) = objGen.ValueToSQL(strFuncion, cwString)
'  rdoDelete(3) = objGen.ValueToSQL(strProceso, cwString)
'  rdoDelete.Execute
'  objApp.rdoConnect.Execute strSql
      
      lngNumSec = 0
      Call DiasFestivos(Val(txtText1(2)), dtcDateCombo1(0).Date, dtcDateCombo1(1).Date)
      Call PerfilVigente(Val(txtText1(0)), dtcDateCombo1(0).Date, dtcDateCombo1(1).Date)
      strSql = "SELECT * FROM AG2600 WHERE AG26CODRECURSO=0"
      Set rdoConsultaFranjas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)

      For lngPosicion = 1 To cllPubDias.Count
        If UCase(cllPubLabFes(lngPosicion)) = "F" Then
          If cboSSDBCombo2(1).Text = "" And txtText2(0) = "" Then
            
            Call A�adirFranja(Val(txtText1(0)), 0, 0, CDate(cllPubDias(lngPosicion)), 0, 0, 23, 59, "FESTIVO", 0, cllPubDiaSemana(lngPosicion))
   
          End If
        Else
          Call RecuperarFranjas(Val(txtText1(0)), cllPubDias(lngPosicion), _
                             cllPubDiaSemana(lngPosicion))
        End If
        If lngPosicion Mod 25 = 0 Then
          rdoConsultaFranjas.Close
          strSql = "SELECT * FROM AG2600 WHERE AG26CODRECURSO=0"
          Set rdoConsultaFranjas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
        End If
      Next lngPosicion
      objWinInfo.objWinActiveForm.strWhere = "AG2600.AG26CODUSUARIO =" & objGen.ValueToSQL(strUsuario, cwString) _
                         & " AND AG2600.AG26FECCONSULTA = TO_DATE('" & dteFechaConsulta & "','DD/MM/YYYY HH24:MI:SS')" _
                         & " AND AG2600.AG26CODFUNCION =" & objGen.ValueToSQL(strFuncion, cwString) _
                         & " AND AG2600.AG26CODPROCESO =" & objGen.ValueToSQL(strProceso, cwString)
      objWinInfo.DataRefresh
      Me.MousePointer = vbDefault
      If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
        Call objError.SetError(cwCodeMsg, "No se ha encontrado ninguna franja en el per�odo seleccionado")
        Call objError.Raise
      End If

    End If
  
  End If
  
  If intIndex = 1 Then
    Set objSearch = New clsCWSearch
    With objSearch
      If cboSSDBCombo2(1).Text <> "" Then
        .strTable = "AG0501J"
        .strWhere = "WHERE PR12CODACTIVIDAD=" & cboSSDBCombo2(1).Value
      Else
        .strTable = "PR0100"
        .strWhere = ""
      End If
      .strOrder = ""
      
      Set objField = .AddField("PR01CODACTUACION")
      objField.strSmallDesc = "C�digo Actuaci�n"
      Set objField = .AddField("PR01DESCORTA")
      objField.strSmallDesc = "Descripci�n Actuaci�n                              "
      
      If .Search Then
        txtText2(0) = .cllValues("PR01CODACTUACION")
        txtText2(1) = .cllValues("PR01DESCORTA")
      End If
    End With
     
    Set objSearch = Nothing
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form maestro
  Dim objMasterInfo As New clsCWForm
' Form detalle 1
  Dim objMultiInfo As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  
  Dim rdoTiposAct As rdoResultset
  Dim strSql As String
' **********************************
' Fin declaraci�n de variables
' **********************************
  blnRollback = False
' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  If objPipe.PipeExist("COD_REC") Then
    Call objWinInfo.WinCreateInfo(cwModeSingleOpen, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  Else
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  End If
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Agenda del Recurso"
    .cwDAT = "13-08-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite la consulta de la agenda de los recursos de la CUN"
    
    .cwUPD = "13-08-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form padre
  With objMasterInfo
   ' Asigno nombre al frame
    .strName = "Recurso"

  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1100"
    'Si la llamada al proceso es desde Citas le paso
    'los p�rametros strWhere y strInitialWhere
    
    
'    If vntWhereRecursos(1) = "" Then
      '.strWhere = "AG11CODRECURSO=0"
      .strWhere = " (AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
      If objPipe.PipeExist("COD_REC") Then
        .strWhere = .strWhere & " AND AG1100.AG11CODRECURSO=" & objPipe.PipeGet("COD_REC")
      Else
        .strWhere = .strWhere & " AND AG1100.AG11CODRECURSO=0"
      End If
'    'Else
'      .strWhere = vntWhereRecursos(1) & " AND (AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
'      If vntWhereRecursos(2) <> "" Then
'        .strInitialWhere = vntWhereRecursos(2) & " AND (AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
'      Else
'        .strInitialWhere = "(AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
'      End If
'      vntWhereRecursos(1) = ""
'      vntWhereRecursos(2) = ""
'    End If

  
  ' Asignaci�n del nivel de acceso del usuario al formulario
    .intAllowance = cwAllowReadOnly
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("NVL(AG11ORDEN,100)", cwAscending) '28/7/2000 - A petcici�n de Dra. Pastor
    Call .FormAddOrderField("AG11CODRECURSO", cwAscending)
  
  End With
  
  With objMultiInfo
    .strName = "FranjasConsulta"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
' Definici�n del tipo (modelo) de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .intCursorSize = -1
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG2600"
    .intAllowance = cwAllowReadOnly
    .strWhere = "AG2600.AG26CODRECURSO=0"

' M�todo de ordenacion del form
    Call .FormAddOrderField("AG26SECUENCIA", cwAscending)
  ' Definici�n del impreso
    Call .objPrinter.Add("AG0008", "Listado 1 de Agenda del Recurso")
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
  End With

  
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
  ' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "CodRecurso", "AG26CODRECURSO") '3
    Call .GridAddColumn(objMultiInfo, "CodPerfil", "AG26CODPERFIL") '4
    Call .GridAddColumn(objMultiInfo, "CodFranja", "AG26CODFRANJA") '5
    Call .GridAddColumn(objMultiInfo, "Fecha", "AG26FECFRANJA") '6
    Call .GridAddColumn(objMultiInfo, "Dia", "AG26DIASEMANA") '7
    Call .GridAddColumn(objMultiInfo, "HorIni", "AG26HORINFRHH") '8
    Call .GridAddColumn(objMultiInfo, "MinIni", "AG26HORINFRMM") '9'
    Call .GridAddColumn(objMultiInfo, "HorFin", "AG26HORFIFRHH") '10
    Call .GridAddColumn(objMultiInfo, "MinFin", "AG26HORFIFRMM") '11
    Call .GridAddColumn(objMultiInfo, "Hora Inicio", "") '12
    Call .GridAddColumn(objMultiInfo, "Hora Final", "") '13
    Call .GridAddColumn(objMultiInfo, "Actividad", "AG26ACTIVIDAD") '14
    Call .GridAddColumn(objMultiInfo, "Planificable", "AG26INDPLANIFIC") '15
 
  
  ' Se obtiene informaci�n de las caracter�sticas de
  ' los controles del formulario
    Call .FormCreateInfo(objMultiInfo)
    Call .FormCreateInfo(objMasterInfo)
     
    .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & _
                                            "AD0200 WHERE (AD02FECFIN > SYSDATE  OR AD02FECFIN IS NULL) AND AD02INDRESPONPROC=-1 ORDER BY AD02DESDPTO"
 
     .CtrlGetInfo(txtText1(1)).blnReadOnly = True
     .CtrlGetInfo(txtText1(1)).blnInFind = True
     
     .CtrlGetInfo(txtText1(2)).blnInGrid = False
     .CtrlGetInfo(txtText1(3)).blnInGrid = False
     
     .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnInGrid = False
 
    .CtrlGetInfo(cboSSDBCombo1(0)).blnNegotiated = False

    
  ' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
  ' Se estabiliza la ventana configurando las propiedades
  ' de los controles
    Call .WinStabilize
    If objMasterInfo.strWhere <> "AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL" Then
      .DataMoveFirst
    End If
  End With
 
  dtcDateCombo1(0).Date = CDate(objGen.GetDBDateTime)
  dtcDateCombo1(1).Date = CDate(objGen.GetDBDateTime)
  strSql = "SELECT PR12CODACTIVIDAD, PR12DESACTIVIDAD FROM " & objEnv.GetValue("Database") & "PR1200 ORDER BY PR12DESACTIVIDAD"
  Set rdoTiposAct = objApp.rdoConnect.OpenResultset(strSql)
  Do While Not rdoTiposAct.EOF
    cboSSDBCombo2(1).AddItem (rdoTiposAct(0) & "," & rdoTiposAct(1))
    rdoTiposAct.MoveNext
  Loop
  Set rdoTiposAct = Nothing

  grdDBGrid1(1).Columns("Fecha").Width = 930
  grdDBGrid1(1).Columns("Hora Inicio").Width = 915
  grdDBGrid1(1).Columns("Hora Final").Width = 915
  grdDBGrid1(1).Columns("Hora Inicio").Alignment = ssCaptionAlignmentCenter
  grdDBGrid1(1).Columns("Hora Final").Alignment = ssCaptionAlignmentCenter
  grdDBGrid1(1).Columns(0).Width = 4005
  grdDBGrid1(1).Columns(1).Alignment = ssCaptionAlignmentCenter
' Se oculta el formulario de splash
  Call objApp.SplashOff

'++++++++++++++++++++++++++++
  Randomize
  strFuncion = Str(CInt(Rnd * 1000))
  strProceso = Str(CInt(Rnd * 1000))
  'strUsuario = objSecurity.strUser
  strUsuario = objApp.strUserName
  dteFechaConsulta = objGen.GetDBDateTime
  
  grdDBGrid1(1).Columns(3).Visible = False
  grdDBGrid1(1).Columns(4).Visible = False
  grdDBGrid1(1).Columns(5).Visible = False
  grdDBGrid1(1).Columns(8).Visible = False
  grdDBGrid1(1).Columns(9).Visible = False
  grdDBGrid1(1).Columns(10).Visible = False
  grdDBGrid1(1).Columns(11).Visible = False
  grdDBGrid1(1).Columns(15).Visible = False
''  cboSSDBCombo1(0).Text = ""
''  txtText1(0).Text = ""
''  txtText1(1).Text = ""
    If objPipe.PipeExist("DPTO") And objPipe.PipeExist("COD_REC") And objPipe.PipeExist("REC") Then
        cboSSDBCombo1(0).Text = objPipe.PipeGet("DPTO")
        Call cboSSDBCombo1_Click(0)
        txtText1(0).Text = objPipe.PipeGet("COD_REC")
        txtText1(1).Text = objPipe.PipeGet("REC")
        
'        grdDBGrid1(0).MoveRecords (objPipe.PipeGet("COD_REC"))
        objPipe.PipeRemove ("COD_REC")
        objPipe.PipeRemove ("REC")
        objPipe.PipeRemove ("DPTO")
        
        
        
        
        
    End If
Screen.MousePointer = vbDefault
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)

  intCancel = objWinInfo.WinExit

End Sub

Private Sub Form_Unload(intCancel As Integer)
  Dim strSql As String
  Dim rdoDelete As rdoQuery
'
  If strUsuario <> "" Then
    strSql = "DELETE  AG2600 WHERE  AG26CODUSUARIO =" & objGen.ValueToSQL(strUsuario, cwString) _
         & " AND AG26FECCONSULTA = TO_DATE('" & dteFechaConsulta & "','DD/MM/YYYY HH24:MI:SS')" _
         & " AND AG26CODFUNCION =" & objGen.ValueToSQL(strFuncion, cwString) _
         & " AND AG26CODPROCESO =" & objGen.ValueToSQL(strProceso, cwString)
'    strSql = "DELETE  AG2600 WHERE  AG26CODUSUARIO =? " _
'         & " AND AG26FECCONSULTA =? " _
'         & " AND AG26CODFUNCION =? " _
'         & " AND AG26CODPROCESO =? "

  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
'  Set rdoDelete = objApp.rdoConnect.CreateQuery("", strSql)
'  rdoDelete(0) = objGen.ValueToSQL(strUsuario, cwString)
'  rdoDelete(1) = objGen.ValueToSQL(dteFechaConsulta, cwDate)
'  rdoDelete(2) = objGen.ValueToSQL(strFuncion, cwString)
'  rdoDelete(3) = objGen.ValueToSQL(strProceso, cwString)
'  rdoDelete.Execute
    objApp.rdoConnect.Execute strSql
  End If

  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  If intIndex = 1 Then
    grdDBGrid1(1).Columns("Hora Inicio").Value = Format(grdDBGrid1(1).Columns("HorIni").Value, "00") _
                  & ":" & Format(grdDBGrid1(1).Columns("MinIni").Value, "00")
    
    grdDBGrid1(1).Columns("Hora Final").Value = Format(grdDBGrid1(1).Columns("HorFin").Value, "00") _
                  & ":" & Format(grdDBGrid1(1).Columns("MinFin").Value, "00")
    
  End If
End Sub

Private Sub grdDBGrid1_Click(intIndex As Integer)
  If intIndex = 1 Then
    If grdDBGrid1(1).SelBookmarks.Count > 0 Then
      grdDBGrid2(2).RemoveAll
      TreeView1(0).Nodes.Clear
      
      If grdDBGrid1(1).Columns("Planificable").Value = True Then
        Call RecuperarActuaciones(Val(txtText1(0)), _
                         grdDBGrid1(1).Columns("CodPerfil").Value, _
                         grdDBGrid1(1).Columns("CodFranja").Value)

        Call RecuperarRestricciones(Val(txtText1(0)), _
                         grdDBGrid1(1).Columns("CodPerfil").Value, _
                         grdDBGrid1(1).Columns("CodFranja").Value)

      End If
    End If
  End If
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "PR0100"
      .strWhere = ""
      .strOrder = ""
      
      Set objField = .AddField("PR01CODACTUACION")
      objField.strSmallDesc = "C�digo Actuaci�n"
      Set objField = .AddField("PR01DESCORTA")
      objField.strSmallDesc = "Descripci�n Actuaci�n                              "
      
      If .Search Then
        txtText2(0) = .cllValues("PR01CODACTUACION")
        txtText2(1) = .cllValues("PR01DESCORTA")
      End If
    End With
     
    Set objSearch = Nothing

End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  If strFormName = "FranjasConsulta" Then
    objWinInfo.DataRefresh
    If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(0)").rdoCursor) > 0 And txtText1(1) <> "" Then
      cmdCommand1(0).Enabled = True
    Else
      cmdCommand1(0).Enabled = False
    End If
  Else
    cmdCommand1(0).Enabled = False
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
DiaRecurso1.pRepresentar Val(txtText1(0).Text), objApp.rdoConnect, , objSecurity.strUser
End Sub
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "FranjasConsulta" Then
    
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
  Call objWinInfo.GridDblClick
  If intIndex = 1 Then
    If grdDBGrid1(1).SelBookmarks.Count > 0 Then
      grdDBGrid2(2).RemoveAll
      TreeView1(0).Nodes.Clear
      If grdDBGrid1(1).Columns("Planificable").Value = -1 Then
        Call RecuperarActuaciones(Val(txtText1(0)), _
                         grdDBGrid1(1).Columns("CodPerfil").Value, _
                         grdDBGrid1(1).Columns("CodFranja").Value)
    
        Call RecuperarRestricciones(Val(txtText1(0)), _
                         grdDBGrid1(1).Columns("CodPerfil").Value, _
                         grdDBGrid1(1).Columns("CodFranja").Value)
 
      End If
    End If
  End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
 ' Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
 ' Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
 ' Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
 ' Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
'  Call AgendaActiva
 ' Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
 ' Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
 ' Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
'  Dim rdoDelete As rdoQuery
  
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 Then
    grdDBGrid1(1).RemoveAll
    grdDBGrid2(2).RemoveAll
    TreeView1(0).Nodes.Clear
    TreeView1(1).Nodes.Clear
    strUsuario = objApp.strUserName
    dteFechaConsulta = objGen.GetDBDateTime
    strSql = "DELETE  AG2600 WHERE  AG26CODUSUARIO =" & objGen.ValueToSQL(strUsuario, cwString) _
         & " AND AG26FECCONSULTA <>" & objGen.ValueToSQL(dteFechaConsulta, cwDate) _
         & " AND AG26CODFUNCION =" & objGen.ValueToSQL(strFuncion, cwString) _
         & " AND AG26CODPROCESO =" & objGen.ValueToSQL(strProceso, cwString)
'    strSql = "DELETE  AG2600 WHERE  AG26CODUSUARIO = ? " _
'         & " AND AG26FECCONSULTA <> ? " _
'         & " AND AG26CODFUNCION = ? " _
'         & " AND AG26CODPROCESO = ? "
'  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
'  Set rdoDelete = objApp.rdoConnect.CreateQuery("", strSql)
'  rdoDelete(0) = objGen.ValueToSQL(strUsuario, cwString)
'  rdoDelete(1) = objGen.ValueToSQL(dteFechaConsulta, cwDate)
'  rdoDelete(2) = objGen.ValueToSQL(strFuncion, cwString)
'  rdoDelete(3) = objGen.ValueToSQL(strProceso, cwString)
'  rdoDelete.Execute
    objApp.rdoConnect.Execute strSql
    Call CargarTreeView(agRestPorRecurso, Me, 1, Val(txtText1(0)))

    
  End If
  If intIndex = 3 Then
    If Val(cboSSDBCombo1(0).Value) <> Val(txtText1(3)) Then
      cboSSDBCombo1(0).Value = Val(txtText1(3))
    End If
  End If

End Sub

Private Sub txtText2_Change(Index As Integer)
  If txtText2(0).Text = "" Then
    txtText2(1).Text = ""
  End If
End Sub

