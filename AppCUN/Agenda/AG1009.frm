VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmDietario 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dietario de Recursos"
   ClientHeight    =   8340
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   25
   Icon            =   "AG1009.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Dietario"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000006&
      Height          =   5220
      Index           =   1
      Left            =   120
      TabIndex        =   36
      Top             =   2760
      Width           =   11745
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "&Consultar"
         Enabled         =   0   'False
         Height          =   420
         Index           =   0
         Left            =   9405
         TabIndex        =   5
         Top             =   420
         Width           =   1830
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   0
         Left            =   3360
         TabIndex        =   3
         Tag             =   "Fecha Desde"
         ToolTipText     =   "Fecha Desde"
         Top             =   480
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   1
         Left            =   5715
         TabIndex        =   4
         Tag             =   "Fecha Hasta"
         ToolTipText     =   "Fecha Hasta"
         Top             =   480
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   4125
         HelpContextID   =   90001
         Index           =   1
         Left            =   120
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   960
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   7276
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AG1009.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "tabTab1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AG1009.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin TabDlg.SSTab tabTab1 
            Height          =   3930
            Index           =   2
            Left            =   120
            TabIndex        =   41
            Top             =   120
            Width           =   11025
            _ExtentX        =   19447
            _ExtentY        =   6932
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            TabsPerRow      =   4
            TabHeight       =   520
            BackColor       =   12632256
            TabCaption(0)   =   "&Cita"
            TabPicture(0)   =   "AG1009.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(0)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(2)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(3)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(4)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(8)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(10)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(11)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(12)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(13)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(14)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(15)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(18)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(19)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "fraFrame1(2)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txttext1(3)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txttext1(4)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txttext1(5)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txttext1(6)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txttext1(7)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txttext1(8)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txttext1(9)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txttext1(10)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txttext1(11)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txttext1(12)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txttext1(13)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txttext1(18)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txttext1(23)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txttext1(24)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "txttext1(25)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).ControlCount=   29
            TabCaption(1)   =   "&Indicaciones"
            TabPicture(1)   =   "AG1009.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txttext1(16)"
            Tab(1).ControlCount=   1
            TabCaption(2)   =   "O&bservaciones"
            TabPicture(2)   =   "AG1009.frx":007C
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txttext1(17)"
            Tab(2).ControlCount=   1
            TabCaption(3)   =   "Otros Datos"
            TabPicture(3)   =   "AG1009.frx":0098
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "fraFrame1(6)"
            Tab(3).Control(0).Enabled=   0   'False
            Tab(3).Control(1)=   "fraFrame1(5)"
            Tab(3).Control(1).Enabled=   0   'False
            Tab(3).Control(2)=   "fraFrame1(4)"
            Tab(3).Control(2).Enabled=   0   'False
            Tab(3).Control(3)=   "fraFrame1(3)"
            Tab(3).Control(3).Enabled=   0   'False
            Tab(3).ControlCount=   4
            Begin VB.Frame fraFrame1 
               Caption         =   "Unidades Utilizadas en la realizacion "
               Height          =   975
               Index           =   6
               Left            =   -74880
               TabIndex        =   79
               Top             =   2760
               Width           =   3375
               Begin VB.TextBox txttext1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR04CANTIDAD"
                  Height          =   330
                  Index           =   35
                  Left            =   1800
                  TabIndex        =   80
                  Tag             =   "C�digo Persona que cita"
                  Top             =   360
                  Width           =   555
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Unidades"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   31
                  Left            =   840
                  TabIndex        =   81
                  Top             =   360
                  Width           =   810
               End
            End
            Begin VB.Frame fraFrame1 
               Caption         =   "Anestesia"
               Height          =   975
               Index           =   5
               Left            =   -74880
               TabIndex        =   72
               Top             =   1560
               Width           =   9735
               Begin VB.TextBox txttext1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR04OBSERV"
                  Height          =   570
                  Index           =   32
                  Left            =   4560
                  TabIndex        =   78
                  Tag             =   "C�digo Persona que cita"
                  Top             =   240
                  Width           =   4995
               End
               Begin VB.TextBox txttext1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR04ASA"
                  Height          =   330
                  Index           =   31
                  Left            =   3000
                  TabIndex        =   76
                  Tag             =   "C�digo Persona que cita"
                  Top             =   240
                  Width           =   315
               End
               Begin VB.TextBox txttext1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR63CODTIPOANEST"
                  Height          =   330
                  Index           =   30
                  Left            =   1800
                  TabIndex        =   74
                  Tag             =   "C�digo Persona que cita"
                  Top             =   240
                  Width           =   555
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Incidencias"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   28
                  Left            =   3480
                  TabIndex        =   77
                  Top             =   240
                  Width           =   990
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Asa"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   27
                  Left            =   2520
                  TabIndex        =   75
                  Top             =   240
                  Width           =   330
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tipo de Anestesia "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   26
                  Left            =   120
                  TabIndex        =   73
                  Top             =   240
                  Width           =   1605
               End
            End
            Begin VB.Frame fraFrame1 
               Caption         =   "Persona �ltima Actualizaci�n"
               Height          =   975
               Index           =   4
               Left            =   -69960
               TabIndex        =   67
               Top             =   360
               Width           =   4815
               Begin VB.TextBox txttext1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "SG02COD_UPD"
                  Height          =   330
                  Index           =   28
                  Left            =   120
                  TabIndex        =   69
                  Tag             =   "C�digo Persona Ultima Actualizaci�n"
                  Top             =   480
                  Width           =   915
               End
               Begin VB.TextBox txttext1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   29
                  Left            =   1440
                  TabIndex        =   68
                  Tag             =   "Nombre"
                  Top             =   480
                  Width           =   3180
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�d. Persona"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   17
                  Left            =   120
                  TabIndex        =   71
                  Top             =   240
                  Width           =   1155
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre Completo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   23
                  Left            =   1440
                  TabIndex        =   70
                  Top             =   240
                  Width           =   1500
               End
            End
            Begin VB.Frame fraFrame1 
               Caption         =   "Persona que cita"
               Height          =   975
               Index           =   3
               Left            =   -74880
               TabIndex        =   62
               Top             =   360
               Width           =   4815
               Begin VB.TextBox txttext1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   27
                  Left            =   1440
                  TabIndex        =   64
                  Tag             =   "Nombre"
                  Top             =   480
                  Width           =   3180
               End
               Begin VB.TextBox txttext1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "SG02COD_ADD"
                  Height          =   330
                  Index           =   26
                  Left            =   120
                  TabIndex        =   63
                  Tag             =   "C�digo Persona que cita"
                  Top             =   480
                  Width           =   915
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre Completo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   24
                  Left            =   1440
                  TabIndex        =   66
                  Top             =   240
                  Width           =   1500
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�d. Persona"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   25
                  Left            =   120
                  TabIndex        =   65
                  Top             =   240
                  Width           =   1155
               End
            End
            Begin VB.TextBox txttext1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR37CODESTADO"
               Height          =   285
               Index           =   25
               Left            =   5040
               TabIndex        =   60
               Tag             =   "Estado Actuaci�n"
               Top             =   1395
               Visible         =   0   'False
               Width           =   315
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI27FECFIOCUREC"
               Height          =   330
               Index           =   24
               Left            =   1935
               TabIndex        =   7
               Tag             =   "Fecha Fin Ocupaci�n"
               Top             =   990
               Width           =   1560
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI27FECINOCUREC"
               Height          =   330
               Index           =   23
               Left            =   195
               TabIndex        =   6
               Tag             =   "Fecha Inicio Ocupaci�n"
               Top             =   1005
               Width           =   1560
            End
            Begin VB.TextBox txttext1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO_ACT"
               Height          =   285
               HelpContextID   =   40101
               Index           =   18
               Left            =   6375
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "C�digo Departamento"
               Top             =   1350
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR08DESOBSERV"
               Height          =   2955
               HelpContextID   =   40102
               Index           =   17
               Left            =   -74790
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   27
               Tag             =   "Obervaciones"
               Top             =   825
               Width           =   10545
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR08DESINDICAC"
               Height          =   2955
               HelpContextID   =   40102
               Index           =   16
               Left            =   -74775
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   26
               Tag             =   "Indicaciones"
               Top             =   825
               Width           =   10545
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NUMHISTORIA"
               Height          =   330
               Index           =   13
               Left            =   9630
               TabIndex        =   19
               Tag             =   "N�mero de Historia"
               Top             =   2370
               Width           =   1150
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22SEGAPEL"
               Height          =   330
               Index           =   12
               Left            =   6870
               TabIndex        =   18
               Tag             =   "Segundo Apellido"
               Top             =   2370
               Width           =   2610
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22PRIAPEL"
               Height          =   330
               Index           =   11
               Left            =   4125
               TabIndex        =   17
               Tag             =   "Primer Apellido"
               Top             =   2370
               Width           =   2610
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NOMBRE"
               Height          =   330
               Index           =   10
               Left            =   1590
               TabIndex        =   16
               Tag             =   "Nombre"
               Top             =   2370
               Width           =   2355
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   9
               Left            =   6915
               TabIndex        =   14
               Tag             =   "Departamento"
               Top             =   1695
               Width           =   3915
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   8
               Left            =   1800
               TabIndex        =   12
               Tag             =   "Descripci�n Actuaci�n"
               Top             =   1695
               Width           =   4890
            End
            Begin VB.TextBox txttext1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   7
               Left            =   195
               TabIndex        =   15
               Tag             =   "C�digo Persona"
               Top             =   2370
               Width           =   1150
            End
            Begin VB.TextBox txttext1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR01CODACTUACION"
               Height          =   330
               Index           =   6
               Left            =   195
               TabIndex        =   11
               Tag             =   "C�digo Actuaci�n"
               Top             =   1695
               Width           =   1305
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   5
               Left            =   9750
               TabIndex        =   10
               Tag             =   "C�digo Asistencia"
               Top             =   990
               Width           =   1080
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   4
               Left            =   3645
               Locked          =   -1  'True
               TabIndex        =   8
               Tag             =   "Estado Actuaci�n"
               Top             =   990
               Width           =   1785
            End
            Begin VB.TextBox txttext1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI15DESFASECITA"
               Height          =   330
               Index           =   3
               Left            =   5610
               TabIndex        =   9
               Tag             =   "Descripci�n Fase"
               Top             =   990
               Width           =   4005
            End
            Begin VB.Frame fraFrame1 
               Caption         =   "Solicitante"
               Height          =   1005
               Index           =   2
               Left            =   120
               TabIndex        =   53
               Top             =   2820
               Width           =   10665
               Begin VB.TextBox txttext1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "AD06CODPERSONA_SOL"
                  Height          =   285
                  Index           =   22
                  Left            =   2385
                  TabIndex        =   22
                  TabStop         =   0   'False
                  Tag             =   "C�digo Persona Sol."
                  Top             =   135
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.TextBox txttext1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   21
                  Left            =   8130
                  TabIndex        =   25
                  Tag             =   "Segundo Apellido Solicitante"
                  Top             =   465
                  Width           =   2370
               End
               Begin VB.TextBox txttext1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   20
                  Left            =   5595
                  TabIndex        =   24
                  Tag             =   "Primer Apellido Solicitante"
                  Top             =   465
                  Width           =   2370
               End
               Begin VB.TextBox txttext1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   15
                  Left            =   3240
                  TabIndex        =   23
                  Tag             =   "Nombre Solicitante"
                  Top             =   465
                  Width           =   2220
               End
               Begin VB.TextBox txttext1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "AD02CODDPTO_SOL"
                  Height          =   270
                  Index           =   19
                  Left            =   1935
                  TabIndex        =   21
                  TabStop         =   0   'False
                  Tag             =   "C�digo Departamento Sol"
                  Top             =   135
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.TextBox txttext1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   14
                  Left            =   135
                  TabIndex        =   20
                  Tag             =   "Departamento Solicitante"
                  Top             =   465
                  Width           =   2955
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Segundo Apellido"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   22
                  Left            =   8130
                  TabIndex        =   59
                  Top             =   240
                  Width           =   1500
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Primer Apellido"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   21
                  Left            =   5595
                  TabIndex        =   58
                  Top             =   240
                  Width           =   1275
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   20
                  Left            =   3240
                  TabIndex        =   57
                  Top             =   240
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Departamento"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   16
                  Left            =   135
                  TabIndex        =   54
                  Top             =   240
                  Width           =   1200
               End
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Fin Ocup."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   1935
               TabIndex        =   56
               Top             =   780
               Width           =   1425
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Inicio Ocup."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   18
               Left            =   195
               TabIndex        =   55
               Top             =   765
               Width           =   1635
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "N�m.Historia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   15
               Left            =   9645
               TabIndex        =   52
               Top             =   2145
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Segundo Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   14
               Left            =   6870
               TabIndex        =   51
               Top             =   2145
               Width           =   1500
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Primer Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   13
               Left            =   4125
               TabIndex        =   50
               Top             =   2145
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   1590
               TabIndex        =   49
               Top             =   2145
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d. Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   195
               TabIndex        =   48
               Top             =   2145
               Width           =   1155
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   6915
               TabIndex        =   47
               Top             =   1470
               Width           =   1200
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Actuaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   1800
               TabIndex        =   46
               Top             =   1470
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d. Actuaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   195
               TabIndex        =   45
               Top             =   1470
               Width           =   1320
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d. Asist."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   9735
               TabIndex        =   44
               Top             =   765
               Width           =   930
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Fase"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   5610
               TabIndex        =   43
               Top             =   765
               Width           =   1485
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Estado Actuaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   3645
               TabIndex        =   42
               Top             =   780
               Width           =   1515
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3675
            Index           =   1
            Left            =   -74880
            TabIndex        =   40
            Top             =   195
            Width           =   11010
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19420
            _ExtentY        =   6482
            _StockProps     =   79
            ForeColor       =   0
         End
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Hasta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   5715
         TabIndex        =   38
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Desde"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   3360
         TabIndex        =   37
         Top             =   240
         Width           =   1140
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Recurso"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2190
      Index           =   0
      Left            =   120
      TabIndex        =   30
      Top             =   480
      Width           =   11715
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   6960
         TabIndex        =   0
         Tag             =   "Departamento"
         Top             =   420
         Width           =   4290
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AutoRestore     =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   7620
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   7567
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         DataFieldToDisplay=   "Column 1"
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   1455
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   600
         Width           =   11355
         _ExtentX        =   20029
         _ExtentY        =   2566
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AG1009.frx":00B4
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(6)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txttext1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txttext1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txttext1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AG1009.frx":00D0
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   2
            Left            =   8280
            TabIndex        =   61
            Tag             =   "C�digo Depto"
            Top             =   840
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            Index           =   0
            Left            =   390
            TabIndex        =   1
            Tag             =   "C�digo Recurso|C�digo"
            Top             =   735
            Width           =   1150
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG11DESRECURSO"
            Height          =   330
            Index           =   1
            Left            =   1800
            TabIndex        =   2
            Tag             =   "Descripci�n Recurso"
            Top             =   750
            Width           =   5340
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   975
            Index           =   0
            Left            =   -74760
            TabIndex        =   32
            Top             =   360
            Width           =   10515
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18547
            _ExtentY        =   1720
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   390
            TabIndex        =   34
            Top             =   495
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1800
            TabIndex        =   33
            Top             =   495
            Width           =   1020
         End
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   7395
         TabIndex        =   35
         Top             =   210
         Width           =   1200
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   0
      Left            =   0
      TabIndex        =   28
      Top             =   8340
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   0
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDietario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Rem bind repasar
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 And KeyCode = 46 Then
    cboSSDBCombo1(intIndex).Text = ""
    objWinInfo.objWinActiveForm.strWhere = ""
    objWinInfo.DataRefresh
  End If

End Sub

Private Sub cboSSDBCombo1_KeyPress(Index As Integer, KeyAscii As Integer)
'Cuando se pulsaba la tecla 'A' se posicionaba en el segundo
'departamento que empezaba por 'A'
If (Index = 0) And (KeyAscii = 65 Or KeyAscii = 97) Then

    If cboSSDBCombo1(Index).Text = "" Then
        KeyAscii = Asc("W")
    End If
    
End If

End Sub

Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim dtcFechaHasta As Date
  
  If intIndex = 0 And cboSSDBCombo1(0).Text <> "" Then
    If dtcDateCombo1(0) = "" Or dtcDateCombo1(1) = "" Then
      Call objError.SetError(cwCodeMsg, "La Fecha Desde y la Fecha Hasta son obligatorios")
      Call objError.Raise
      Exit Sub
    Else
      If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
        If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
          Call objError.Raise
          Exit Sub
        End If
      End If
 
      Me.MousePointer = vbHourglass

      dtcFechaHasta = dtcDateCombo1(1).DateValue + 1
'''      objWinInfo.objWinActiveForm.strWhere = "(CI2702J.AG11CODRECURSO=" & Val(txtText1(0)) _
'''                & " AND  CI2702J.CI27FECINOCUREC >= " & objGen.ValueToSQL(dtcDateCombo1(0), cwDate) _
'''                            & " AND CI2702J.CI27FECINOCUREC <= " & objGen.ValueToSQL(dtcFechaHasta, cwDate) _
'''                            & " AND ((CI2702J.PR37CODESTADO IN (2,3,4,5) AND (CI01SITCITA='1' OR CI01SITCITA='3')) OR CI01SITCITA = '4')" _
'''                 & " ) OR  (CI2702J.AG11CODRECURSO=" & Val(txtText1(0)) & " AND  CI2702J.CI27FECFIOCUREC >= " & objGen.ValueToSQL(dtcDateCombo1(0), cwDate) _
'''                         & " AND CI2702J.CI27FECFIOCUREC <= " & objGen.ValueToSQL(dtcFechaHasta, cwDate) _
'''                         & " AND ((CI2702J.PR37CODESTADO IN (2,3,4,5) AND (CI01SITCITA='1' OR CI01SITCITA='3')) OR CI01SITCITA = '4')" _
'''                         & " )"
'EFS: Cambio para que no muestre las citas pendientes de recitar (estado 3)
 objWinInfo.objWinActiveForm.strWhere = "(CI2702J.AG11CODRECURSO=" & Val(txtText1(0)) _
                & " AND  CI2702J.CI27FECINOCUREC >= " & objGen.ValueToSQL(dtcDateCombo1(0), cwDate) _
                            & " AND CI2702J.CI27FECINOCUREC <= " & objGen.ValueToSQL(dtcFechaHasta, cwDate) _
                            & " AND ((CI2702J.PR37CODESTADO IN (2,3,4,5) AND (CI01SITCITA='1')) OR CI01SITCITA = '4')" _
                & " ) OR  (CI2702J.AG11CODRECURSO=" & Val(txtText1(0)) & " AND  CI2702J.CI27FECFIOCUREC >= " & objGen.ValueToSQL(dtcDateCombo1(0), cwDate) _
                         & " AND CI2702J.CI27FECFIOCUREC <= " & objGen.ValueToSQL(dtcFechaHasta, cwDate) _
                         & " AND ((CI2702J.PR37CODESTADO IN (2,3,4,5) AND (CI01SITCITA='1')) OR CI01SITCITA = '4')" _
                         & " )"
    
      

    
      objWinInfo.DataRefresh
      Me.MousePointer = vbDefault
      If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
        Call objError.SetError(cwCodeMsg, "No se ha encontrado ninguna cita en el per�odo seleccionado")
        Call objError.Raise
      End If
    End If
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form padre
  Dim objMasterInfo As New clsCWForm
' Form hijo
  Dim objDetailInfo As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  
' **********************************
' Fin declaraci�n de variables
' **********************************
  
' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Dietario de Recursos"
    .cwDAT = "22-10-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite consultar el dietario de un recurso en un rango de fechas"
    
    .cwUPD = "22-10-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form padre
  With objMasterInfo
   ' Asigno nombre al frame
    .strName = "Recurso"

  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1100"
    
    'Si la llamada al proceso es desde Citas le paso
    'los p�rametros strWhere y strInitialWhere
    If vntWhereRecursos(1) = "" Then
      '.strWhere = "AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL"
      '.strWhere = "AG11CODRECURSO=0"
    Else
      .strWhere = vntWhereRecursos(1) & " AND (AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
      If vntWhereRecursos(2) <> "" Then
        .strInitialWhere = vntWhereRecursos(2) & " AND (AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
      End If
      vntWhereRecursos(1) = ""
      vntWhereRecursos(2) = ""
    End If
  ' Asignaci�n del nivel de acceso del usuario al formulario
    .intAllowance = cwAllowReadOnly
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG11CODRECURSO", cwAscending)
  
  End With
' Declaraci�n de las caracter�sticas del form detalle
  With objDetailInfo
    ' Asigno nombre al frame
    .strName = "Dietario"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
  ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2702J"
    
  ' Asignaci�n del nivel de acceso del usuario al formulario
    .intAllowance = cwAllowReadOnly
    .intCursorSize = -1
    
    Call .objPrinter.Add("AG0009", "Listado del Dietario del Recurso")
    Call .objPrinter.Add("AG0017", "Listado del Grado de Ocupaci�n del Recurso")

  
  ' M�todo de ordenacion del form
    
    Call .FormAddOrderField("CI27FECINOCUREC", cwAscending)
    Call .FormAddOrderField("CI27FECFIOCUREC", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
  '  Call .FormAddRelation("AG11CODRECURSO", txtText1(0))
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable

  End With
   
  
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
' Se obtiene informaci�n de las caracter�sticas de
' los controles del formulario
  

    Call .FormCreateInfo(objDetailInfo)
    Call .FormCreateInfo(objMasterInfo)
        
' Campos que intervienen en busquedas
  .CtrlGetInfo(txtText1(1)).blnInFind = True
  .CtrlGetInfo(txtText1(2)).blnInGrid = False

  .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02INDRESPONPROC=-1 AND (AD02FECFIN > SYSDATE OR AD02FECFIN IS NULL) ORDER BY AD02DESDPTO"
  'Campos relcacionados entre si
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(18)), "AD02CODDPTO_ACT", "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02CODDPTO = ? AND (AD02FECFIN > SYSDATE OR AD02FECFIN IS NULL)")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(18)), txtText1(9), "AD02DESDPTO")
  
 Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(26)), "SG02COD_ADD", "SELECT SG02NOM||' '||SG02APE1||' '||SG02APE2 USUARIO FROM " & objEnv.GetValue("Database") & "SG0200 WHERE SG02COD = ? AND (SG02FECDES > SYSDATE OR SG02FECDES IS NULL)")
Call .CtrlAddLinked(.CtrlGetInfo(txtText1(26)), txtText1(27), "USUARIO")
  
Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(28)), "SG02COD_UPD", "SELECT SG02NOM||' '||SG02APE1||' '||SG02APE2 USUARIO1 FROM " & objEnv.GetValue("Database") & "SG0200 WHERE SG02COD = ? AND (SG02FECDES > SYSDATE OR SG02FECDES IS NULL)")
Call .CtrlAddLinked(.CtrlGetInfo(txtText1(28)), txtText1(29), "USUARIO1")
   
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(19)), "AD02CODDPTO_SOL", "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02CODDPTO = ? AND (AD02FECFIN > SYSDATE OR AD02FECFIN IS NULL)")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(14), "AD02DESDPTO")
  
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "PR01CODACTUACION", "SELECT PR01CODACTUACION, PR01DESCORTA FROM " & objEnv.GetValue("Database") & "PR0100 WHERE PR01CODACTUACION = ?")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(8), "PR01DESCORTA")
   
  Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "AD06CODPERSONA_SOL", "SELECT SG02NOM, SG02APE1,SG02APE2 FROM " & objEnv.GetValue("Database") & "SG0200 WHERE SG02COD = ? AND (SG02FECDES > SYSDATE OR SG02FECDES IS NULL)")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(15), "SG02NOM")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(20), "SG02APE1")
  Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(21), "SG02APE2")
   
  'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "AD06CODPERSONA", "SELECT CI21CODPERSONA, CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL FROM CI2200 WHERE CI21CODPERSONA = ?")
  'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(15), "CI22NOMBRE")
  'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(20), "CI22PRIAPEL")
  'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(21), "CI22SEGAPEL")

' Valores con los que se cargara la DbCombo
 '   .CtrlGetInfo(cboCombo1(0)).strSql = "SELECT deptno, dname FROM " & objEnv.GetValue("Database") & "dept ORDER BY deptno"
    .CtrlGetInfo(cboSSDBCombo1(0)).blnNegotiated = False
    .CtrlGetInfo(dtcDateCombo1(0)).blnNegotiated = False
    .CtrlGetInfo(dtcDateCombo1(1)).blnNegotiated = False
  
' Eliminamos campo del grid

    .CtrlGetInfo(txtText1(18)).blnInGrid = False
    .CtrlGetInfo(txtText1(19)).blnInGrid = False
    .CtrlGetInfo(txtText1(22)).blnInGrid = False
    .CtrlGetInfo(txtText1(16)).blnInGrid = False
    .CtrlGetInfo(txtText1(17)).blnInGrid = False
    
   ' .CtrlGetInfo(txtText1(6)).blnInGrid = False
    .CtrlGetInfo(txtText1(7)).blnInGrid = True
    
    
'    .CtrlGetInfo(txtText1(24)).blnInGrid = True
'    .CtrlGetInfo(txtText1(13)).blnInGrid = True
'    .CtrlGetInfo(txtText1(6)).blnInGrid = False
'    .CtrlGetInfo(txtText1(7)).blnInGrid = False
'   Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
' Se estabiliza la ventana configurando las propiedades
' de los controles
    Call .WinStabilize
    If objMasterInfo.strWhere <> "" Then
      .DataMoveFirst
    End If
  End With
  
' Se oculta el formulario de splash
  Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)

  If Index = 1 Then
    Select Case grdDBGrid1(1).Columns(10).Text
      Case "1"
        grdDBGrid1(1).Columns(10).Text = "PENDIENTE RECITAR" 'Pendiente Recitar
     Case "2"
        grdDBGrid1(1).Columns(10).Text = "CITADA" 'Citada
      Case "3"
        grdDBGrid1(1).Columns(10).Text = "REALIZANDOSE" 'Realiz�ndose
      Case "4"
        grdDBGrid1(1).Columns(10).Text = "REALIZADA" 'Realizada
      Case "5"
        grdDBGrid1(1).Columns(10).Text = "INFORMADA" 'Informada
      Case "6"
        grdDBGrid1(1).Columns(10).Text = "ANULADA" 'Anulada
    End Select
  End If
    
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  If strFormName = "Dietario" Then
    If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(0)").rdoCursor) > 0 And txtText1(1) <> "" Then
      cmdCommand1(0).Enabled = True
    Else
      cmdCommand1(0).Enabled = False
    End If
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
      objWinInfo.DataRefresh
    End If
  Else
    cmdCommand1(0).Enabled = False
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
   
  
  If strFormName = "Dietario" Then
    'If objWinInfo.CtrlGet(txtText1(25)) = agCitada Then
    '  Call objWinInfo.CtrlSet(txtText1(4), "CITADA")
    'End If
    'If objWinInfo.CtrlGet(txtText1(25)) = agPdteRecitar Then
    '  Call objWinInfo.CtrlSet(txtText1(4), "PENDIENTE RECITAR")
    'End If
    If objWinInfo.CtrlGet(txtText1(25)) = "1" Then
      Call objWinInfo.CtrlSet(txtText1(4), "PENDIENTE RECITAR")
    End If
    If objWinInfo.CtrlGet(txtText1(25)) = "2" Then
      Call objWinInfo.CtrlSet(txtText1(4), "CITADA")
    End If
    If objWinInfo.CtrlGet(txtText1(25)) = "3" Then
      Call objWinInfo.CtrlSet(txtText1(4), "REALIZANDOSE")
    End If
    If objWinInfo.CtrlGet(txtText1(25)) = "4" Then
      Call objWinInfo.CtrlSet(txtText1(4), "REALIZADA")
    End If
    If objWinInfo.CtrlGet(txtText1(25)) = "5" Then
      Call objWinInfo.CtrlSet(txtText1(4), "INFORMADA")
    End If
    
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "Dietario" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
If intIndex = 1 And tabTab1(1).Tab = 1 Then
    grdDBGrid1(0).Columns(1).Width = 1850
    grdDBGrid1(1).Columns(2).Width = 1850
    grdDBGrid1(1).Columns(3).Width = 1800
    grdDBGrid1(1).Columns(4).Width = 0
    grdDBGrid1(1).Columns(5).Width = 1800
    grdDBGrid1(1).Columns(6).Width = 1800
    grdDBGrid1(1).Columns(7).Width = 1800
    grdDBGrid1(1).Columns(8).Width = 1800
    grdDBGrid1(1).Columns(9).Width = 1600
    grdDBGrid1(1).Columns(13).Caption = "Segundo Apellido"
    
End If
Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 Then
    If DateDiff("d", Format(dtcDateCombo1(1).Date, "DD/MM/YYYY"), Format(dtcDateCombo1(0).Date, "DD/MM/YYYY")) > 0 Then
      dtcDateCombo1(1).Date = dtcDateCombo1(0).Date
    End If
  End If

End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 Then
    If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
      If DateDiff("d", Format(dtcDateCombo1(1).Date, "DD/MM/YYYY"), Format(dtcDateCombo1(0).Date, "DD/MM/YYYY")) > 0 Then
        dtcDateCombo1(1).Date = dtcDateCombo1(0).Date
      End If
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim intPos As Integer
  
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 Then
    For intPos = 3 To 24
      Call objWinInfo.CtrlSet(txtText1(intPos), "")
    Next
    grdDBGrid1(1).RemoveAll
    objWinInfo.cllWinForms("fraFrame1(1)").strWhere = "CI2702J.AG11CODRECURSO=0 AND CI2702J.CI27FECINOCUREC >=" & _
    "TO_DATE('01-01-1901','DD-MM-YYYY') AND CI2702J.CI27FECINOCUREC<=" & _
    "TO_DATE('01-01-1901','DD-MM-YYYY') AND ((CI2702J.PR37CODESTADO IN (2,3,4,5)" & _
    " AND (CI01SITCITA='1' OR CI01SITCITA='3')) OR CI01SITCITA ='4')" & _
    " AND -1 = -1"
    
    objWinInfo.cllWinForms("fraFrame1(1)").strInitialWhere = objWinInfo.cllWinForms("fraFrame1(1)").strWhere
  End If
  If intIndex = 2 Then
    If Val(cboSSDBCombo1(0).Value) <> Val(txtText1(2)) Then
      cboSSDBCombo1(0).Value = Val(txtText1(2))
    End If
  End If

End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  If intIndex = 0 And cboSSDBCombo1(0).Text <> "" Then
'******************************************************************
  ' Cambio de departamento
'******************************************************************
  ' Modifico la strWhere para obtener los nuevos registro del
  ' departamento elegido y refresco el cursor
    With objWinInfo
      .objWinActiveForm.strInitialWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value & " AND (AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
      .objWinActiveForm.strWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value & " AND (AG11FECFINVREC > SYSDATE OR AG11FECFINVREC IS NULL)"
      .DataRefresh
    End With
   'Si hay alg�n recurso en ese departamento habilito los botones
   
  '  If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
  '    fraFrame1(1).Enabled = True
  '  Else
  '    fraFrame1(1).Enabled = False
  '  End If
  End If

End Sub

