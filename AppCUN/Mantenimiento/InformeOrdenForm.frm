VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "Crystl32.ocx"
Begin VB.Form InformeOrdenForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   4005
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6840
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   6840
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton ImpBoton 
      Caption         =   "&Imprimir"
      Height          =   540
      Left            =   2835
      TabIndex        =   9
      Top             =   3150
      Width           =   1485
   End
   Begin SSCalendarWidgets_A.SSDateCombo Fecha2 
      Height          =   330
      Left            =   4515
      TabIndex        =   6
      Top             =   1680
      Width           =   2010
      _Version        =   65537
      _ExtentX        =   3545
      _ExtentY        =   582
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo Fecha1 
      Height          =   330
      Left            =   4515
      TabIndex        =   5
      Top             =   630
      Width           =   2010
      _Version        =   65537
      _ExtentX        =   3545
      _ExtentY        =   582
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   315
      Top             =   3150
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipo de Informe"
      Height          =   2745
      Left            =   210
      TabIndex        =   0
      Top             =   105
      Width           =   3900
      Begin VB.OptionButton Option1 
         Caption         =   "Informes Resumen de Órdenes"
         Height          =   435
         Index           =   3
         Left            =   315
         TabIndex        =   4
         Top             =   1890
         Width           =   3480
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Informe Órdenes de Reparación"
         Height          =   540
         Index           =   1
         Left            =   315
         TabIndex        =   3
         Top             =   840
         Width           =   3480
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Informe Instalaciones"
         Height          =   435
         Index           =   2
         Left            =   315
         TabIndex        =   2
         Top             =   1365
         Width           =   3480
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Informe Mantenimiento Preventivo"
         Height          =   435
         Index           =   0
         Left            =   315
         TabIndex        =   1
         Top             =   420
         Width           =   3480
      End
   End
   Begin VB.Label Label2 
      Caption         =   "hasta :"
      Height          =   330
      Left            =   4515
      TabIndex        =   8
      Top             =   1260
      Width           =   1380
   End
   Begin VB.Label Label1 
      Caption         =   "Desde :"
      Height          =   330
      Left            =   4515
      TabIndex        =   7
      Top             =   210
      Width           =   1380
   End
End
Attribute VB_Name = "InformeOrdenForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ImpBoton_Click()
  
  Dim FechaAr1 As String
  Dim FechaAr2 As String
  Dim Cont     As Integer
  Dim NombreReport As String
  
  For Cont = 0 To Option1.Count
    If Option1(Cont).Value = True Then Exit For
  Next
    
  Select Case Cont
  Case 0
    NombreReport = "OrdenesManPrev.rpt"
  Case 1
    NombreReport = "OrdenesRep.rpt"
  Case 2
    NombreReport = "OrdenesRep.rpt"
  Case 3
    NombreReport = "OrdenesResumen.rpt"
  Case Else
    MsgBox ("Por favor, seleccione alguna de las opciones...")
    Exit Sub
  End Select
  
  With CrystalReport1
      
      FechaAr1 = Fecha1.Text
      FechaAr2 = Fecha2.Text
      .ReportFileName = "C:\archivos de programa\almacen\doc\" & NombreReport
      .Formulas(0) = "Fecha1= Date(" & FechaAr1 & ")"
      .Formulas(1) = "Fecha2= Date(" & FechaAr2 & ")"
      If Cont = 1 Then
        .Formulas(2) = "TipoOrden='Reparación'"
        .Formulas(3) = "EscTiOrden='(Reparaciones)'"
      ElseIf Cont = 2 Then
        .Formulas(2) = "TipoOrden='Instalación'"
        .Formulas(3) = "EscTiOrden='(Instalaciones)'"
      End If
      
      .Action = 1
      
  
  End With

End Sub


