VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form MPForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento Preventivo"
   ClientHeight    =   6705
   ClientLeft      =   1215
   ClientTop       =   1440
   ClientWidth     =   10050
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6705
   ScaleWidth      =   10050
   ShowInTaskbar   =   0   'False
   Begin VB.Data OpData 
      Caption         =   "Operarios"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   600
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT CodigoOperario,Nombre  FROM Operarios WHERE Activo=True ORDER BY Nombre"
      Top             =   120
      Visible         =   0   'False
      Width           =   1815
   End
   Begin SSDataWidgets_B.SSDBDropDown SSDBDropDown2 
      Bindings        =   "MPForm.frx":0000
      Height          =   255
      Left            =   6960
      TabIndex        =   7
      Top             =   6240
      Width           =   2175
      DataFieldList   =   "CodigoOperario"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1270
      Columns(0).Caption=   "C�d.Op."
      Columns(0).Name =   "CodigoOperario"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoOperario"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   4815
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Nombre"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1640
      Columns(2).Caption=   "C�d.Espec"
      Columns(2).Name =   "CodigoEspecialidad"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "CodigoEspecialidad"
      Columns(2).DataType=   3
      Columns(2).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   450
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Nombre"
   End
   Begin VB.Data Period 
      Caption         =   "Period"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   3720
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT CodigoPeriodicidad,Periodicidad FROM Periodicidad"
      Top             =   120
      Visible         =   0   'False
      Width           =   1935
   End
   Begin SSDataWidgets_B.SSDBDropDown SSDBDropDown1 
      Bindings        =   "MPForm.frx":0011
      Height          =   255
      Left            =   6960
      TabIndex        =   6
      Top             =   6000
      Width           =   2175
      DataFieldList   =   "CodigoPeriodicidad"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1852
      Columns(0).Caption=   "C�dd.Period"
      Columns(0).Name =   "CodigoPeriodicidad"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoPeriodicidad"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3969
      Columns(1).Caption=   "Periodicidad"
      Columns(1).Name =   "Periodicidad"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Periodicidad"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   450
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Periodicidad"
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "MPForm.frx":0022
      Height          =   4335
      Left            =   120
      TabIndex        =   5
      Top             =   1440
      Width           =   9735
      ScrollBars      =   2
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowAddNew     =   -1  'True
      RowHeight       =   423
      ExtraHeight     =   1984
      Columns.Count   =   5
      Columns(0).Width=   3043
      Columns(0).Caption=   "C�d.Per."
      Columns(0).Name =   "CodigoPeriodicidad"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoPeriodicidad"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   5689
      Columns(1).Caption=   "Acciones"
      Columns(1).Name =   "Accion"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Accion"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).VertScrollBar=   -1  'True
      Columns(2).Width=   1746
      Columns(2).Caption=   "Durac.Est."
      Columns(2).Name =   "DuracionEstimada"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "DuracionEstimada"
      Columns(2).DataType=   3
      Columns(2).FieldLen=   256
      Columns(3).Width=   1984
      Columns(3).Caption=   "Pr�xima orden"
      Columns(3).Name =   "NextOrden"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "NextOrden"
      Columns(3).DataType=   7
      Columns(3).FieldLen=   256
      Columns(4).Width=   3704
      Columns(4).Caption=   "C�d.Op."
      Columns(4).Name =   "CodigoOperario"
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "CodigoOperario"
      Columns(4).DataType=   3
      Columns(4).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   17171
      _ExtentY        =   7646
      _StockProps     =   79
      Caption         =   "DESCRIPCI�N MANTENIMIENTO PREVENTIVO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton SalirBoton 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   6000
      Width           =   1215
   End
   Begin VB.Data MPData 
      Caption         =   "MP"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      EOFAction       =   2  'Add New
      Exclusive       =   0   'False
      Height          =   375
      Left            =   6960
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "select * from MantPreventivo where CodigoMaquina=0"
      Top             =   0
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label DescLabel 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label3"
      Height          =   255
      Left            =   5400
      TabIndex        =   4
      Top             =   840
      Width           =   4215
   End
   Begin VB.Label Label2 
      Caption         =   "Descripci�n:"
      Height          =   255
      Left            =   4320
      TabIndex        =   3
      Top             =   840
      Width           =   975
   End
   Begin VB.Label CMLabel 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      Height          =   255
      Left            =   1920
      TabIndex        =   1
      Top             =   840
      Width           =   1455
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   9720
      Y1              =   600
      Y2              =   600
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo de M�quina:"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   840
      Width           =   1455
   End
End
Attribute VB_Name = "MPForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub SalirBoton_Click()
    Unload MPForm
    
End Sub

Private Sub SSDBGrid1_BeforeUpdate(Cancel As Integer)
On Error GoTo msgError
MPData.Recordset.Fields("CodigoMaquina").Value = Val(CMLabel.Caption)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub SSDBGrid1_InitColumnProps()
        
    With SSDBGrid1
        .Columns(0).DropDownHwnd = SSDBDropDown1.hWnd
        .Columns(4).DropDownHwnd = SSDBDropDown2.hWnd
        .Columns(1).FieldLen = 1000
    End With
    
End Sub
