VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form InfMPForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe sobre Mant.Preventivo"
   ClientHeight    =   4020
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   9405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4020
   ScaleWidth      =   9405
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "&Calcular"
      Height          =   375
      Left            =   7200
      TabIndex        =   10
      Top             =   480
      Width           =   855
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   255
      Left            =   2520
      OleObjectBlob   =   "InfMPForm.frx":0000
      TabIndex        =   6
      Top             =   600
      Width           =   1575
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo2 
      Height          =   255
      Left            =   4800
      OleObjectBlob   =   "InfMPForm.frx":0131
      TabIndex        =   7
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "al "
      Height          =   255
      Left            =   4320
      TabIndex        =   9
      Top             =   600
      Width           =   375
   End
   Begin VB.Label Label1 
      Caption         =   "Desde el :"
      Height          =   255
      Left            =   1560
      TabIndex        =   8
      Top             =   600
      Width           =   855
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   9240
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Label MPRBox 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   5
      Top             =   3000
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "Mantenimiento Preventivo realizado (%):"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   3000
      Width           =   3855
   End
   Begin VB.Label NHMPBox 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   3
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Label4 
      Caption         =   "N�Horas destinadas a Mant.Preventivo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Top             =   2520
      Width           =   3855
   End
   Begin VB.Label NOMPBox 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4560
      TabIndex        =   1
      Top             =   2040
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "N��rdenes de Mantenimiento Preventivo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   0
      Top             =   2040
      Width           =   3855
   End
End
Attribute VB_Name = "InfMPForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

End Sub
