Attribute VB_Name = "Informes2"
Public Function CalidadEsp1(Codigo As Long, Fecha1 As Date, Fecha2 As Date)

    Dim Busq As String, TiRe As Double, TiPa As Double
    Dim Graf1(1 To 2, 1 To 4) As Variant, Graf2(1 To 2, 1 To 4) As Variant, Graf3(1 To 2, 1 To 4) As Variant, Graf4(1 To 2, 1 To 4) As Variant
    Dim OT As Long, Dif As Long, Con As Long

On Error GoTo msgError
    With InformeCalForm
        .Show modal
        .UniBox.Caption = "Secci�n " & Codigo
        .Fecha1Box.Text = CStr(Fecha1)
        .Fecha2Box.Text = CStr(Fecha2)
        .CodigoBox.Text = Codigo
    End With
    
    Graf1(2, 1) = "Dist.Tiempos Reparaci�n"
    Graf3(2, 1) = "Dist.Tiempos Reparaci�n"
    Graf2(2, 1) = "Dist.Tiempos Paro"
    Graf4(2, 1) = "Dist.Tiempos Paro"
    Graf1(1, 2) = "<2 horas"
    Graf2(1, 2) = "<2 horas"
    Graf3(1, 2) = "<2 horas"
    Graf4(1, 2) = "<2 horas"
    Graf1(1, 3) = "2-4 horas"
    Graf2(1, 3) = "2-4 horas"
    Graf3(1, 3) = "2-4 horas"
    Graf4(1, 3) = "2-4 horas"
    Graf1(1, 4) = ">4 horas"
    Graf2(1, 4) = ">4 horas"
    Graf3(1, 4) = ">4 horas"
    Graf4(1, 4) = ">4 horas"
    
    'Gr�ficos para tiempo de reparaci�n y paro de las OT del intervalo de tiempo
    
    With InformeCalForm.Data1
        Busq = "SELECT O.TiempoReparacion, O.TiempoParo FROM Orden O, InventMaquinas M WHERE FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Busq = Busq & " AND O.CodigoEspecialidad=" & Codigo
        Busq = Busq & " AND O.CodigoMaquina=M.CodigoMaquina"
        Busq = Busq & " AND O.Finalizada=True"
        Busq = Busq & " AND M.Criticidad=1"
        .RecordSource = Busq
        .Refresh
        
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveFirst
            
            While .Recordset.EOF = False
                TiRe = TiRe + .Recordset.Fields("TiempoReparacion").Value
                TiPa = TiPa + .Recordset.Fields("TiempoParo").Value
                Select Case .Recordset.Fields("TiempoReparacion").Value
                Case Is < 2
                    Graf1(2, 2) = Graf1(2, 2) + 1
                Case Is >= 2
                    If .Recordset.Fields("TiempoReparacion").Value < 4 Then
                        Graf1(2, 3) = Graf1(2, 3) + 1
                    Else
                        Graf1(2, 4) = Graf1(2, 4) + 1
                    End If
                End Select
                
                Select Case .Recordset.Fields("TiempoParo").Value
                Case Is < 2
                    Graf2(2, 2) = Graf2(2, 2) + 1
                Case Is >= 2
                    If .Recordset.Fields("TiempoParo").Value < 4 Then
                        Graf2(2, 3) = Graf2(2, 3) + 1
                    Else
                        Graf2(2, 4) = Graf2(2, 4) + 1
                    End If
                End Select
                .Recordset.MoveNext
            Wend
        
            InformeCalForm.TiMeReBox.Caption = TiRe / .Recordset.RecordCount
            InformeCalForm.TiMePaBox.Caption = TiPa / .Recordset.RecordCount
            InformeCalForm.MSChart1 = Graf1
            InformeCalForm.MSChart2 = Graf2
        Else
            InformeCalForm.MSChart1.Visible = False
            InformeCalForm.MSChart2.Visible = False
        End If
        Busq = "SELECT O.TiempoReparacion, O.TiempoParo FROM Orden O, InventMaquinas M WHERE FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Busq = Busq & " AND O.CodigoEspecialidad=" & Codigo
        Busq = Busq & " AND O.CodigoMaquina=M.CodigoMaquina"
        Busq = Busq & " AND O.Finalizada=True"
        Busq = Busq & " AND M.Criticidad=2"
        .RecordSource = Busq
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveFirst
            
            While .Recordset.EOF = False
                TiRe = TiRe + .Recordset.Fields("TiempoReparacion").Value
                TiPa = TiPa + .Recordset.Fields("TiempoParo").Value
                Select Case .Recordset.Fields("TiempoReparacion").Value
                Case Is < 2
                    Graf3(2, 2) = Graf3(2, 2) + 1
                Case Is >= 2
                    If .Recordset.Fields("TiempoReparacion").Value < 4 Then
                        Graf3(2, 3) = Graf3(2, 3) + 1
                    Else
                        Graf3(2, 4) = Graf3(2, 4) + 1
                    End If
                End Select
                
                Select Case .Recordset.Fields("TiempoParo").Value
                Case Is < 2
                    Graf4(2, 2) = Graf4(2, 2) + 1
                Case Is >= 2
                    If .Recordset.Fields("TiempoParo").Value < 4 Then
                        Graf4(2, 3) = Graf4(2, 3) + 1
                    Else
                        Graf4(2, 4) = Graf4(2, 4) + 1
                    End If
                End Select
                .Recordset.MoveNext
            Wend
        
            InformeCalForm.TiMeRe2Box.Caption = TiRe / .Recordset.RecordCount
            InformeCalForm.TiMePa2Box.Caption = TiPa / .Recordset.RecordCount
            InformeCalForm.MSChart3 = Graf3
            InformeCalForm.MSChart4 = Graf4
        Else
            InformeCalForm.MSChart3.Visible = False
            InformeCalForm.MSChart4.Visible = False
        
        End If
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Public Function CalidadEsp2(Codigo As Long, Fecha1 As Date, Fecha2 As Date)


    Dim Busq As String, TiRe As Double, TiPa As Double
    Dim Graf1(1 To 2, 1 To 4) As Variant, Graf2(1 To 2, 1 To 4) As Variant, Graf3(1 To 2, 1 To 4) As Variant
    Dim OT As Long, Dif As Long, Con As Long
On Error GoTo msgError
    Graf1(2, 1) = "O.T.Pendientes"
    Graf3(2, 1) = "Dist.Tiempo Reparaci�n por Departamentos"
    Graf2(2, 1) = "Dist.Tiempo Reparaci�n por �reas de Trabajo"
    Graf1(1, 2) = "1 d�a"
    Graf1(1, 3) = "1-7 d�as"
    Graf1(1, 4) = ">7 d�as"
    
    'Generar gr�ficos de O.T.Pendientes separ�ndolos en grupos de 1, 1-7 y >7 d�as
    
    With InformeCal2Form
        .Show modal
        .UniBox.Caption = "Secci�n " & Codigo & " desde el " & Fecha1 & " hasta " & Fecha2
        With .Data1
            .RecordSource = "SELECT FechaOrden FROM Orden WHERE Finalizada=False AND CodigoEspecialidad=" & Codigo & " AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
            .Refresh
            While .Recordset.EOF = False
                OT = OT + 1
                Dif = DateDiff("d", .Recordset.Fields("FechaOrden").Value, Date)
                Select Case Dif
                Case Is = 1
                    Graf1(2, 2) = Graf1(2, 2) + 1
                Case Is > 1
                    If Dif < 7 Then
                        Graf1(2, 3) = Graf1(2, 3) + 1
                    Else
                        Graf1(2, 4) = Graf1(2, 4) + 1
                    End If
                End Select
                .Recordset.MoveNext
            Wend
        End With
        .MSChart1 = Graf1
        .OTPendBox.Caption = OT
        
        With .Data1
            .RecordSource = "SELECT CodigoLugar FROM Orden WHERE Finalizada=True AND CodigoEspecialidad=" & Codigo & " AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#" & " GROUP BY CodigoLugar ORDER BY CodigoLugar"
            .Refresh
            InformeCal2Form.MSChart2.RowCount = .Recordset.RecordCount
            InformeCal2Form.MSChart2.RowLabelCount = .Recordset.RecordCount
            InformeCal2Form.MSChart2.ColumnCount = 1
            If .Recordset.RecordCount <> 0 Then
                .Recordset.MoveFirst
                Con = 0
                While .Recordset.EOF = False
                    Con = Con + 1
                    InformeCal2Form.Data2.RecordSource = "SELECT TiempoReparacion FROM Orden WHERE Finalizada=True AND CodigoLugar=" & .Recordset.Fields("CodigoLugar").Value & " AND CodigoEspecialidad=" & Codigo & " AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
                    InformeCal2Form.Data2.Refresh
                    If InformeCal2Form.Data2.Recordset.RecordCount <> 0 Then
                        While InformeCal2Form.Data2.Recordset.EOF = False
                            Dif = Dif + InformeCal2Form.Data2.Recordset.Fields("TiempoReparacion").Value
                            InformeCal2Form.Data2.Recordset.MoveNext
                        Wend
                    End If
                    InformeCal2Form.MSChart2.Row = Con
                    InformeCal2Form.MSChart2.RowLabel = .Recordset.Fields("CodigoLugar").Value
                    InformeCal2Form.MSChart2.Column = 1
                    InformeCal2Form.MSChart2.Data = Dif
                    .Recordset.MoveNext
                Wend
            End If
        End With
        
        With .Data1
            .RecordSource = "SELECT CodigoMaquina FROM Orden WHERE Finalizada=True AND CodigoEspecialidad=" & Codigo & " AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#" & " GROUP BY CodigoMaquina"
            .Refresh
            InformeCal2Form.MSChart3.RowCount = .Recordset.RecordCount
            InformeCal2Form.MSChart3.RowLabelCount = .Recordset.RecordCount
            InformeCal2Form.MSChart3.ColumnCount = 1
            If .Recordset.RecordCount <> 0 Then
                .Recordset.MoveFirst
                Con = 0
                While .Recordset.EOF = False
                    Con = Con + 1
                    InformeCal2Form.Data2.RecordSource = "SELECT TiempoReparacion FROM Orden WHERE Finalizada=True AND CodigoMaquina=" & .Recordset.Fields("CodigoMaquina").Value & " AND CodigoEspecialidad=" & Codigo & " AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
                    InformeCal2Form.Data2.Refresh
                    If InformeCal2Form.Data2.Recordset.RecordCount <> 0 Then
                        While InformeCal2Form.Data2.Recordset.EOF = False
                            Dif = Dif + InformeCal2Form.Data2.Recordset.Fields("TiempoReparacion").Value
                            InformeCal2Form.Data2.Recordset.MoveNext
                        Wend
                    End If
                    InformeCal2Form.MSChart3.Row = Con
                    InformeCal2Form.MSChart3.RowLabel = .Recordset.Fields("CodigoMaquina").Value
                    InformeCal2Form.MSChart3.Column = 1
                    InformeCal2Form.MSChart3.Data = Dif
                    .Recordset.MoveNext
                Wend
            End If
        End With

    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Public Function CalidadSAT1(Codigo As String, Fecha1 As Date, Fecha2 As Date)

    Dim Busq As String, TiRe As Double, TiPa As Double
    Dim Graf1(1 To 2, 1 To 4) As Variant, Graf2(1 To 2, 1 To 4) As Variant, Graf3(1 To 2, 1 To 4) As Variant, Graf4(1 To 2, 1 To 4) As Variant
    Dim OT As Long, Dif As Long, Con As Long

On Error GoTo msgError
    With InformeCalForm
        .Show modal
        .UniBox.Caption = "SAT " & Codigo
        .Fecha1Box.Text = CStr(Fecha1)
        .Fecha2Box.Text = CStr(Fecha2)
        .CodigoBox.Text = Codigo
    End With
    
    Graf1(2, 1) = "Dist.Tiempos Reparaci�n"
    Graf3(2, 1) = "Dist.Tiempos Reparaci�n"
    Graf2(2, 1) = "Dist.Tiempos Paro"
    Graf4(2, 1) = "Dist.Tiempos Paro"
    Graf1(1, 2) = "<2 horas"
    Graf2(1, 2) = "<2 horas"
    Graf3(1, 2) = "<2 horas"
    Graf4(1, 2) = "<2 horas"
    Graf1(1, 3) = "2-4 horas"
    Graf2(1, 3) = "2-4 horas"
    Graf3(1, 3) = "2-4 horas"
    Graf4(1, 3) = "2-4 horas"
    Graf1(1, 4) = ">4 horas"
    Graf2(1, 4) = ">4 horas"
    Graf3(1, 4) = ">4 horas"
    Graf4(1, 4) = ">4 horas"
    
    'Gr�ficos para tiempo de reparaci�n y paro de las OT del intervalo de tiempo
    
    With InformeCalForm.Data1
        Busq = "SELECT O.TiempoReparacion, O.TiempoParo FROM Orden O, InventMaquinas M, PedidoSAT P WHERE FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Busq = Busq & " AND O.CodigoMaquina=M.CodigoMaquina"
        Busq = Busq & " AND O.Finalizada=True"
        Busq = Busq & " AND M.Criticidad=1"
        Busq = Busq & " AND P.NOrden=SOME (SELECT O.NOrden FROM Orden o, InventMaquinas M WHERE O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#" & " AND O.CodigoMaquina=M.CodigoMaquina AND O.Finalizada=True AND M.Criticidad=1)"
        Busq = Busq & " AND P.SAT='" & Codigo & "'"
        .RecordSource = Busq
        .Refresh
        
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveFirst
            
            While .Recordset.EOF = False
                TiRe = TiRe + .Recordset.Fields("TiempoReparacion").Value
                TiPa = TiPa + .Recordset.Fields("TiempoParo").Value
                Select Case .Recordset.Fields("TiempoReparacion").Value
                Case Is < 2
                    Graf1(2, 2) = Graf1(2, 2) + 1
                Case Is >= 2
                    If .Recordset.Fields("TiempoReparacion").Value < 4 Then
                        Graf1(2, 3) = Graf1(2, 3) + 1
                    Else
                        Graf1(2, 4) = Graf1(2, 4) + 1
                    End If
                End Select
                
                Select Case .Recordset.Fields("TiempoParo").Value
                Case Is < 2
                    Graf2(2, 2) = Graf2(2, 2) + 1
                Case Is >= 2
                    If .Recordset.Fields("TiempoParo").Value < 4 Then
                        Graf2(2, 3) = Graf2(2, 3) + 1
                    Else
                        Graf2(2, 4) = Graf2(2, 4) + 1
                    End If
                End Select
                .Recordset.MoveNext
            Wend
        
            InformeCalForm.TiMeReBox.Caption = TiRe / .Recordset.RecordCount
            InformeCalForm.TiMePaBox.Caption = TiPa / .Recordset.RecordCount
        End If
        MSChart1 = Graf1
        MSChart2 = Graf2
    
        Busq = "SELECT O.TiempoReparacion, O.TiempoParo FROM Orden O, InventMaquinas M, PedidoSAT P WHERE FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Busq = Busq & " AND O.CodigoMaquina=M.CodigoMaquina"
        Busq = Busq & " AND O.Finalizada=True"
        Busq = Busq & " AND M.Criticidad=2"
        Busq = Busq & " AND P.NOrden=SOME (SELECT O.NOrden FROM Orden o, InventMaquinas M WHERE O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#" & " AND O.CodigoMaquina=M.CodigoMaquina AND O.Finalizada=True AND M.Criticidad=1)"
        Busq = Busq & " AND P.SAT='" & Codigo & "'"
        .RecordSource = Busq
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveFirst
            
            While .Recordset.EOF = False
                TiRe = TiRe + .Recordset.Fields("TiempoReparacion").Value
                TiPa = TiPa + .Recordset.Fields("TiempoParo").Value
                Select Case .Recordset.Fields("TiempoReparacion").Value
                Case Is < 2
                    Graf3(2, 2) = Graf3(2, 2) + 1
                Case Is >= 2
                    If .Recordset.Fields("TiempoReparacion").Value < 4 Then
                        Graf3(2, 3) = Graf3(2, 3) + 1
                    Else
                        Graf3(2, 4) = Graf3(2, 4) + 1
                    End If
                End Select
                
                Select Case .Recordset.Fields("TiempoParo").Value
                Case Is < 2
                    Graf4(2, 2) = Graf4(2, 2) + 1
                Case Is >= 2
                    If .Recordset.Fields("TiempoParo").Value < 4 Then
                        Graf4(2, 3) = Graf4(2, 3) + 1
                    Else
                        Graf4(2, 4) = Graf4(2, 4) + 1
                    End If
                End Select
                .Recordset.MoveNext
            Wend
        
            InformeCalForm.TiMeRe2Box.Caption = TiRe / .Recordset.RecordCount
            InformeCalForm.TiMePa2Box.Caption = TiPa / .Recordset.RecordCount
            InformeCalForm.MSChart3 = Graf3
            InformeCalForm.MSChart4 = Graf4
        End If
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Public Function CalidadSAT2(Codigo As String, Fecha1 As Date, Fecha2 As Date)


    Dim Busq As String, TiRe As Double, TiPa As Double
    Dim Graf1(1 To 2, 1 To 4) As Variant, Graf2(1 To 2, 1 To 4) As Variant, Graf3(1 To 2, 1 To 4) As Variant, Graf4(1 To 2, 1 To 4) As Variant
    Dim OT As Long, Dif As Long, Con As Long
On Error GoTo msgError
    Graf1(2, 1) = "O.T.Pendientes"
    Graf3(2, 1) = "Dist.Tiempo Reparaci�n por Departamentos"
    Graf2(2, 1) = "Dist.Tiempo Reparaci�n por �reas de Trabajo"
    Graf1(1, 2) = "1 d�a"
    Graf1(1, 3) = "1-7 d�as"
    Graf1(1, 4) = ">7 d�as"
    
    'Generar gr�ficos de O.T.Pendientes separ�ndolos en grupos de 1, 1-7 y >7 d�as
    
    With InformeCal2Form
        .Show modal
        .UniBox.Caption = "SAT " & Codigo & " desde el " & Fecha1 & " hasta " & Fecha2
        With .Data1
            .RecordSource = "SELECT O.FechaOrden FROM Orden O, PedidoSAT P WHERE O.Finalizada=False AND O.NOrden=P.NOrden AND P.SAT='" & Codigo & "' AND O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#" & " GROUP BY O.FechaOrden ORDER BY O.FechaOrden"
            .Refresh
            While .Recordset.EOF = False
                OT = OT + 1
                Dif = DateDiff("d", .Recordset.Fields("FechaOrden").Value, Date)
                Select Case Dif
                Case Is = 1
                    Graf1(2, 2) = Graf1(2, 2) + 1
                Case Is > 1
                    If Dif < 7 Then
                        Graf1(2, 3) = Graf1(2, 3) + 1
                    Else
                        Graf1(2, 4) = Graf1(2, 4) + 1
                    End If
                End Select
                .Recordset.MoveNext
            Wend
        End With
        
        .MSChart1 = Graf1
        .OTPendBox.Caption = OT
        
        With .Data1
            Busq = "SELECT O.FechaOrden FROM Orden O, PedidoSAT P WHERE O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
            Busq = Busq & " AND O.Finalizada=False"
            Busq = Busq & " AND P.NOrden=SOME (SELECT O.NOrden FROM Orden o, InventMaquinas M WHERE O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#" & " AND O.CodigoMaquina=M.CodigoMaquina AND O.Finalizada=True AND M.Criticidad=1)"
            Busq = Busq & " AND P.SAT='" & Codigo & "'"
            .RecordSource = Busq
            .Refresh
            InformeCal2Form.MSChart2.ColumnCount = .Recordset.RecordCount
            InformeCal2Form.MSChart2.RowCount = 1
            If .Recordset.RecordCount <> 0 Then
                .Recordset.MoveFirst
                Con = 0
                While .Recordset.EOF = False
                    Con = Con + 1
                    InformeCal2Form.Data2.RecordSource = "SELECT O.TiempoParo FROM Orden O, PedidoSAT P WHERE O.Finalizada=True AND O.CodigoLocalizacion=" & .Recordset.Fields("CodigoLugar").Value & " AND O.NOrden=P.NOrden AND P.SAT='" & Codigo & "' AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
                    InformeCal2Form.Data2.Refresh
                    If InformeCal2Form.Data2.Recordset.RecordCount <> 0 Then
                        While InformeCal2Form.Data2.Recordset.EOF = False
                            Dif = Dif + InformeCal2Form.Data2.Recordset.Fields("TiempoParo").Value
                            InformeCal2Form.Data2.Recordset.MoveNext
                        Wend
                    End If
                    InformeCal2Form.MSChart2.Column = co
                    InformeCal2Form.MSChart2.ColumnLabel = .Recordset.Fields("CodigoLugar").Value
                    InformeCal2Form.MSChart2.Row = 1
                    InformeCal2Form.MSChart2.Data = Dif
                    .Recordset.MoveNext
                Wend
            End If
        End With
        
        With .Data1
            .RecordSource = "SELECT O.CodigoMaquina FROM Orden O, PedidoSAT P WHERE Finalizada=True AND P.NOrden=O.NOrden AND P.SAT='" & Codigo & "' AND O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#" & " GROUP BY O.CodigoMaquina"
            .Refresh
            InformeCal2Form.MSChart3.ColumnCount = .Recordset.RecordCount
            InformeCal2Form.MSChart3.RowCount = 1
            If .Recordset.RecordCount <> 0 Then
                .Recordset.MoveFirst
                Con = 0
                While .Recordset.EOF = False
                    Con = Con + 1
                    InformeCal2Form.Data2.RecordSource = "SELECT O.TiempoParo FROM Orden O, PedidoSAT P WHERE Finalizada=True AND P.NOrden=O.NOrden AND O.CodigoMaquina=" & .Recordset.Fields("CodigoMaquina").Value & " AND P.SAT='" & Codigo & "' AND O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
                    InformeCal2Form.Data2.Refresh
                    If InformeCal2Form.Data2.Recordset.RecordCount <> 0 Then
                        While InformeCal2Form.Data2.Recordset.EOF = False
                            Dif = Dif + InformeCal2Form.Data2.Recordset.Fields("TiempoParo").Value
                            InformeCal2Form.Data2.Recordset.MoveNext
                        Wend
                    End If
                    InformeCal2Form.MSChart3.Column = Con
                    InformeCal2Form.MSChart3.ColumnLabel = .Recordset.Fields("CodigoMaquina").Value
                    InformeCal2Form.MSChart3.Row = 1
                    InformeCal2Form.MSChart3.Data = Dif
                    .Recordset.MoveNext
                Wend
            End If
        End With

    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function


