Attribute VB_Name = "Module1"
Public IMPRESORA As Integer
Public MAXMES As Integer
Public slct As Long
Public Selct As Boolean
Public Selct2 As Integer
Public Flag As Boolean
Public Orden(3) As Long
Public campos(7) As String
Public Display(7) As String
Public TypeCampos(7) As Integer
Public Lists(4, 2) As String
Public SELE As String
Public FRO As String
Public WHER As String
Public ORD As String
Public Objeto As Data
Public Objeto2 As Variant
Public DataCarac As String
Public DataCarac2 As String
Public rstResultados As Recordset
Public Const cteCajaTexto = 1
Public Const cteDate = 2
Public Const cteCombo = 3
Public Const cteTextLike = 4


  Public wrkJet As Workspace
  Public dbsMantenimiento As Database
  Public rstLugar As Recordset


  Public Const ctemnuIra = 2
  Public Const ctemnuPrimero = 3
  Public Const ctemnuAnterior = 4
  Public Const ctemnuSiguiente = 5
  Public Const ctemnuUltimo = 6
  Public Const ctemnuAñadir = 7
  Public Const ctemnuBorrar = 8









Public Function UltimoPedido() As Integer
    Dim num As Integer
        
    Load DBase
    With DBase
        .PedidoData.RecordSource = "SELECT NPedido FROM Pedido ORDER BY NPedido"
        .PedidoData.Refresh
        If .PedidoData.Recordset.EOF = False Then
            .PedidoData.Recordset.MoveLast
            num = .PedidoData.Recordset.Fields("NPedido").Value
        Else
            num = 0
        End If
        
    End With
    
    num = num + 1
    
    UltimoPedido = num
        
    Unload DBase
    
End Function


Public Function Listado() As Integer
  'Muestra los diferentes campos por loa que se realiza la busqueda
  'Display(numero de frame): si contiene algo hace visible el frame y lo utiliza como titulo del frame
  'TypeCampos(numero): muestra un tipo de control determinado y inicializa sus parametros
  'List (i,0): campos de la select
  'List (i,2): base de datos de los campos de la select
  'List (i,1): campos al que se adjunta el control
  
  Dim Indice As Integer
  Dim Seleccion As String
  With BusqForm
    .Show modal
     For i = 0 To 6
      If TypeCampos(i) <> 0 Then
        .Frame(i).Caption = Display(i)
        .Frame(i).Visible = True
        Indice = i * 10 + 1
        Select Case TypeCampos(i)
          Case cteCajaTexto
            .Text(Indice).Visible = True
          Case cteDate
            .sDate(Indice).Visible = True
          Case cteCombo
            If i <= 3 Then
              Seleccion = "SELECT " & Lists(i, 0) & " FROM " & Lists(i, 2) '& IIf(Lists(i, 0), "", " ORDER BY " & Lists(i, 0))
              .Data(i).RecordSource = Seleccion
              .Data(i).Refresh
            End If
            .Combo(Indice).DataFieldList = Lists(i, 1)
            .Combo(Indice).DataFieldToDisplay = Lists(i, 1)
            .Combo(Indice).Visible = True
            Indice = Indice + 1
            .Combo(Indice).DataFieldList = Lists(i, 1)
            .Combo(Indice).DataFieldToDisplay = Lists(i, 1)
        End Select
      End If
    Next i
  End With
End Function


Public Function CaTotalAlb(na As Long) As Long

    Dim Total As Long
    
    Load DBase
    
    With DBase.PedidoData
        .RecordSource = "SELECT Cantidad, PrecioUnit, Descuento FROM AAlbaran WHERE NAlbaran=" & na
        .Refresh
        
        If .Recordset.RecordCount = 0 Then
        
            CaTotalAlb = 0
            Exit Function
        
        Else
            .Recordset.MoveFirst
        
            While .Recordset.EOF = False
                If (.Recordset.Fields("PrecioUnit").Value) <> 0 And (.Recordset.Fields("Cantidad").Value) <> 0 Then
                    Total = Total + ((.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value) * (1 - ((.Recordset.Fields("Descuento").Value) / 100)))
                End If
                .Recordset.MoveNext
            Wend
        
        End If
    
    End With
    
    CaTotalAlb = Total

    Unload DBase
    
End Function

Public Function CaTotalFac(nf As Long) As Long

    Dim Total As Long
    
    Load DBase
    
    With DBase.PedidoData
        .RecordSource = "SELECT Cantidad, PrecioUnit, Descuento FROM FFactura WHERE NFactura=" & nf
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveFirst
        
            While .Recordset.EOF = False
                If (.Recordset.Fields("PrecioUnit").Value) <> 0 And (.Recordset.Fields("Cantidad").Value) <> 0 Then
                    Total = Total + ((.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value) * (1 - (Val(.Recordset.Fields("Descuento").Value) / 100)))
                End If
                .Recordset.MoveNext
            Wend
        Else
            Total = 0
        End If
        
    End With
    
    CaTotalFac = Total

    Unload DBase
    
End Function

Public Function CaTotalSATFac(nf As Long) As Long

    Dim Total As Long
    
        Load DBase
        
        With DBase.PedidoData
            .RecordSource = "SELECT Cantidad, PrecioUnit, Descuento FROM SATFFactura WHERE NFacturaSAT=" & nf
            .Refresh
            If .Recordset.EOF = False Then
               
               .Recordset.MoveFirst
            
                While .Recordset.EOF = False
                    If (.Recordset.Fields("PrecioUnit").Value) <> 0 And (.Recordset.Fields("Cantidad").Value) <> 0 Then
                        Total = Total + ((.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value) * (1 - (Val(.Recordset.Fields("Descuento").Value) / 100)))
                    End If
                    .Recordset.MoveNext
                Wend
            End If
        End With
        
    CaTotalSATFac = Total
    
    Unload DBase
    
End Function

Public Function UltFactura() As Long

    Dim last As Long, sat As Long, fa As Long
    
    Load DBase
    
    With DBase.PedidoData
        .RecordSource = "SELECT NFactura FROM Factura ORDER BY NFactura"
        .Refresh
        .Recordset.MoveLast
        fa = .Recordset.Fields("NFactura").Value
    End With
    
    With DBase.Data2
        .RecordSource = "SELECT NFacturaSAT FROM SATFact ORDER BY NFacturaSAT"
        .Refresh
        If .Recordset.EOF = True Then
            sat = 0
        Else
            .Recordset.MoveLast
            sat = .Recordset.Fields("NFacturaSAT").Value
        End If
        
    End With
    
    Select Case fa > sat
        Case True
            UltFactura = fa
        Case False
            UltFactura = sat
    End Select
    
End Function


Public Function CaTotalSATAlb(nf As Long) As Long

    Dim Total As Long
    
        Load DBase
        
        With DBase.PedidoData
            .RecordSource = "SELECT Cantidad, PrecioUnit, Descuento FROM SATAAlbaran WHERE NAlbaranSAT=" & nf
            .Refresh
            If .Recordset.EOF = False Then
               
               .Recordset.MoveFirst
            
                While .Recordset.EOF = False
                    If (.Recordset.Fields("PrecioUnit").Value) <> 0 And (.Recordset.Fields("Cantidad").Value) <> 0 Then
                        Total = Total + ((.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value) * (1 - (Val(.Recordset.Fields("Descuento").Value) / 100)))
                    End If
                    .Recordset.MoveNext
                Wend
            End If
        End With
        
    CaTotalSATAlb = Total
    
    Unload DBase
    
End Function

Public Function UltAlbaran() As Long

    Dim last As Long, sat As Long, fa As Long
    
    Load DBase
    
    With DBase.PedidoData
        .RecordSource = "SELECT NAlbaran FROM Albaran ORDER BY NAlbaran"
        .Refresh
        .Recordset.MoveLast
        fa = .Recordset.Fields("NAlbaran").Value
    End With
    
    With DBase.Data2
        .RecordSource = "SELECT NAlbaranSAT FROM SATAlbaran ORDER BY NAlbaranSAT"
        .Refresh
        If .Recordset.EOF = True Then
            sat = 0
        Else
            .Recordset.MoveLast
            sat = .Recordset.Fields("NAlbaranSAT").Value
        End If
        
    End With
    
    Select Case fa > sat
        Case True
            UltAlbaran = fa
        Case False
            UltAlbaran = sat
    End Select
    
End Function

Public Function UltimoPedidoA() As Integer
  'utiliza un data control que hay el grid DBase para hacer una select y devolver el siguiente pedido al ultimo
  Load DBase
  With DBase.PedidoData
    .RecordSource = "SELECT NPedidoA FROM PedidoAl ORDER BY NPedidoA"
    .Refresh
    With .Recordset
      If .EOF = False Then
        .MoveLast
        UltimoPedidoA = .Fields!NPedidoA.Value + 1
      Else
        UltimoPedidoA = 1
      End If
    End With
  End With
  Unload DBase
End Function

Public Function UltimoProveedor() As Integer
  'utiliza un data control que hay el grid DBase para hacer una select y devolver el siguiente numero de proveedor al ultimo
  Load DBase
  With DBase.PedidoData
    .RecordSource = "SELECT CodigoProveedor FROM Proveedores ORDER BY CodigoProveedor"
    .Refresh
    With .Recordset
      If .EOF = False Then
        .MoveLast
         UltimoProveedor = .Fields!CodigoProveedor.Value + 1
      Else
        UltimoProveedor = 1
      End If
    End With
  End With
  Unload DBase
End Function

Public Function UltimoDepartamento() As Integer
  'devolver el siguiente numero de departamento
  
  Dim Sql As String
  Dim rstLugar As Recordset
  Set rstLugar = dbsMantenimiento.OpenRecordset("SELECT CodigoLugar FROM Lugar ORDER BY CodigoLugar", dbOpenDynaset)

  With rstLugar
    If .EOF = False Then
      .MoveLast
      UltimoDepartamento = .Fields!CodigoLugar + 1
    Else
      UltimoDepartamento = 1
    End If
  End With
End Function

