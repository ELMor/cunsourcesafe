VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form MostraLForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Muestra"
   ClientHeight    =   7185
   ClientLeft      =   1140
   ClientTop       =   960
   ClientWidth     =   10125
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   10125
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtOpcion 
      Height          =   495
      Left            =   120
      TabIndex        =   6
      Text            =   "MP"
      Top             =   120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Data Data2 
      Caption         =   "Data2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   8160
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   8160
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data PerioData 
      Caption         =   "Periodicidad"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   600
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "Periodicidad"
      Top             =   240
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Data OrdenData 
      Caption         =   "Ordenes"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   360
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM Orden ORDER BY NOrden"
      Top             =   6600
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton GenerarBoton 
      Caption         =   "&Generar"
      Height          =   495
      Left            =   3720
      TabIndex        =   5
      Top             =   6360
      Width           =   1455
   End
   Begin VB.CommandButton btnCancelar 
      Caption         =   "&Cancelar"
      Height          =   495
      Left            =   7920
      TabIndex        =   1
      Top             =   6360
      Width           =   1455
   End
   Begin VB.Data MPData 
      Caption         =   "MantPrev"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   360
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   6240
      Visible         =   0   'False
      Width           =   2175
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "MostraLForm.frx":0000
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   1800
      Width           =   9855
      _Version        =   131078
      RowHeight       =   423
      Columns(0).Width=   3200
      UseDefaults     =   0   'False
      _ExtentX        =   17383
      _ExtentY        =   7646
      _StockProps     =   79
      Enabled         =   0   'False
   End
   Begin VB.Label Label2 
      Caption         =   "Fecha de Hoy:"
      Height          =   255
      Left            =   3960
      TabIndex        =   4
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Se van a generar Ordenes de trabajo pertenecientes a las siguientes tareas de Mantenimiento Preventivo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   1080
      Width           =   9855
   End
   Begin VB.Label DateLabel 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   5160
      TabIndex        =   2
      Top             =   240
      Width           =   1815
   End
End
Attribute VB_Name = "MostraLForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub btnCancelar_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  Screen.MousePointer = vbNormal
  DateLabel.Caption = CStr(Date)
End Sub

Private Sub GenerarBoton_Click()
  Dim j As Integer, NumOrd As Long
  Dim Fecha As Date, inter As String, num As Integer, Maq As Long, x As Integer
  Dim Wr As String, Cod As Boolean
  On Error GoTo msgError
  
  Select Case txtOpcion
    Case "MP"
      MPData.Recordset.MoveFirst
      OrdenData.Recordset.MoveLast
      NumOrd = OrdenData.Recordset.Fields!NOrden
      For j = 1 To MPData.Recordset.RecordCount
        Fecha = MPData.Recordset.Fields!NextOrden
        inter = MPData.Recordset.Fields!CodigoPeriodicidad
        PerioData.Recordset.FindFirst ("CodigoPeriodicidad=" & inter)
        inter = PerioData.Recordset.Fields!Unidad
        num = PerioData.Recordset.Fields!Valor
        Data1.RecordSource = "SELECT CodigoEspecialidad, Nombre FROM Operarios WHERE CodigoOperario=" & MPData.Recordset.Fields!CodigoOperario
        Data1.Refresh
        
        With OrdenData.Recordset
          .AddNew
          .Fields!NOrden = NumOrd + j
          .Fields!FechaOrden = Date
          .Fields!HoraOrden = Time
          .Fields!TipoOrden = "Mant.Preventivo"
          .Fields!CodigoMaquina = MPData.Recordset.Fields!CodigoMaquina
          .Fields!CodigoEspecialidad = Data1.Recordset.Fields!CodigoEspecialidad
          .Fields!CodigoOperario = MPData.Recordset.Fields!CodigoOperario
          If MPData.Recordset.Fields!CodigoPeriodicidad <> 2 Then
            .Fields!Definicion = PerioData.Recordset.Fields!Periodicidad & " : " & MPData.Recordset.Fields!Accion
          Else
            .Fields!Definicion = "*" & PerioData.Recordset.Fields!Periodicidad & " : " & MPData.Recordset.Fields!Accion
          End If
          .Fields!TiempoEstimadoMP = MPData.Recordset.Fields!DuracionEstimada
          .Fields!CodigoPeriodicidad = MPData.Recordset.Fields!CodigoPeriodicidad
          .Update
        End With
        
        Maq = MPData.Recordset.Fields!CodigoMaquina
        If MPData.Recordset.Fields!CodigoPeriodicidad <> 2 Then
          With Data1.Recordset
            Data1.RecordSource = "SELECT CodigoMaquina FROM InventMaquinas " _
                               & "WHERE CodigoMaquina LIKE '" & Maq & "*' AND CodigoMaquina<>" & Maq
            Data1.Refresh
            If .RecordCount <> 0 Then
              .MoveLast
              .MoveFirst
              Data2.RecordSource = "SELECT * FROM ListaMP"
              Data2.Refresh
              For x = 1 To .RecordCount
                Data2.Recordset.AddNew
                Data2.Recordset.Fields!NOrden = NumOrd + j
                Data2.Recordset.Fields!CodigoMaquina = .Fields!CodigoMaquina
                Data2.Recordset.Fields!Hecho = True
                Data2.Recordset.Update
                .MoveNext
              Next x
            End If
          End With
        Else
          With Data1
            .RecordSource = "SELECT CodigoMaquina FROM InventMaquinas WHERE CodigoMaquina LIKE '" & Maq & "*' AND CodigoMaquina<>" & Maq
            .Refresh
            If .Recordset.RecordCount <> 0 Then
              .Recordset.MoveLast
              .Recordset.MoveFirst
              Data2.RecordSource = "SELECT * FROM ListaMP2"
              Data2.Refresh
              For x = 1 To .Recordset.RecordCount
                Data2.Recordset.AddNew
                Data2.Recordset.Fields!NOrden = NumOrd + j
                Data2.Recordset.Fields!CodigoMaquina = .Recordset.Fields("CodigoMaquina").Value
                Data2.Recordset.Fields!HechoS1 = True
                Data2.Recordset.Fields!HechoS2 = True
                Data2.Recordset.Fields!HechoS3 = True
                Data2.Recordset.Fields!HechoS4 = True
                Data2.Recordset.Fields!HechoM = True
                Data2.Recordset.Update
                .Recordset.MoveNext
              Next x
            End If
          End With
        End If
        MPData.Recordset.Edit
        MPData.Recordset.Fields!NextOrden = DateAdd(inter, num, Fecha)
        MPData.Recordset.Update
        MPData.Recordset.MoveNext
      Next
      OrdenForm.Show modal
      OrdenForm.OrdenData.RecordSource = "SELECT * FROM Orden WHERE NOrden > " & NumOrd
      OrdenForm.OrdenData.Refresh
      Unload MostraLForm
    Case "O"
      With MPData.Recordset
        .MoveFirst
        Do While .EOF = False
          .Edit
          .Fields!Finalizada = True
          .Fields!CodigoEstado = 999
          .Update
          .MoveNext
        Loop
      End With
      Unload Me
  End Select
  Exit Sub
msgError:
  MsgBox Err.Description, vbCritical
End Sub
