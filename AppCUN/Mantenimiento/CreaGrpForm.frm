VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form CreaGrpForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento Grupos de Art�culos"
   ClientHeight    =   3330
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3330
   ScaleWidth      =   7470
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2640
      TabIndex        =   0
      Top             =   2760
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "CreaGrpForm.frx":0000
      Height          =   2535
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7215
      ScrollBars      =   2
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2461
      Columns(0).Caption=   "C�digo Grupo Art."
      Columns(0).Name =   "CodigoGrArt"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoGrArt"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   9234
      Columns(1).Caption=   "Grupo"
      Columns(1).Name =   "Grupo"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Grupo"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      UseDefaults     =   -1  'True
      _ExtentX        =   12726
      _ExtentY        =   4471
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Data GruposDAta 
      Caption         =   "Grupos"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   3480
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "GrArticulos"
      Top             =   480
      Visible         =   0   'False
      Width           =   1935
   End
End
Attribute VB_Name = "CreaGrpForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    MantArt.GruposDAta.Refresh
    Unload Me
End Sub
