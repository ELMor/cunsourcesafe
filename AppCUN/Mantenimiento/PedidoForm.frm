VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form PedidoForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Pedidos Exteriores"
   ClientHeight    =   8310
   ClientLeft      =   1755
   ClientTop       =   675
   ClientWidth     =   11115
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   11115
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu ira 
         Caption         =   "Ir a "
         Shortcut        =   {F1}
      End
      Begin VB.Menu first 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu confi 
      Caption         =   "&Configuraciones"
      Begin VB.Menu prove 
         Caption         =   "Proveedores"
      End
      Begin VB.Menu artia 
         Caption         =   "Art�culos"
      End
   End
   Begin VB.Menu listdfg 
      Caption         =   "&Listados"
      Begin VB.Menu listf 
         Caption         =   "Listado"
      End
   End
   Begin VB.Menu imp0 
      Caption         =   "&Imprimir"
      Begin VB.Menu imp1 
         Caption         =   "Imprimir Pedido WORD"
      End
   End
End
Attribute VB_Name = "PedidoForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function Anterior()
  'se mueve al pedido anterior
  Dim NP As Integer
  On Error GoTo MsgError
  
  If PedidoData.Recordset.RecordCount = 0 Then Exit Function       'si no hay registros sale
  With PedidoData
    .Recordset.MovePrevious
    If .Recordset.BOF = True Then .Recordset.MoveNext
    NP = .Recordset.Fields("NPedido").Value
  End With
  PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad, PP.Linea, PP.SuCodigo, AR.PrecioUnit " _
                           & "FROM PPedido PP, Articulos AR " _
                           & "WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & NP & " ORDER BY PP.Linea"
  PPedidoData.Refresh
  Exit Function
  
MsgError:
   MsgBox Err.Description, vbCritical
End Function

Private Function Siguiente()
  'se mueve al pedido siguiente
  Dim NP As Integer
  On Error GoTo MsgError
  
  If PedidoData.Recordset.RecordCount = 0 Then Exit Function 'si no hay registros sale
  With PedidoData
    .Recordset.MoveNext
    If .Recordset.EOF = True Then .Recordset.MovePrevious
    NP = .Recordset.Fields("NPedido").Value
  End With
  
  PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit " _
                           & "FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & NP & " ORDER BY PP.Linea"
  PPedidoData.Refresh
  Exit Function
  
MsgError:
  MsgBox Err.Description, vbCritical
End Function

Private Function Primero()
  'se mueve al primer pedido
  Dim NP As Long
  On Error GoTo MsgError
  
  PedidoData.Recordset.MoveFirst
  Nada = PedidoData.Recordset.Fields("NPedido").Value
  PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & NP & " ORDER BY PP.Linea"
  PPedidoData.Refresh
  Exit Function
MsgError:
  MsgBox Err.Description, vbCritical
End Function

Private Function Ultimo()
  'se mueve al ultimo registro
  Dim NP As Long
  On Error GoTo MsgError
  
  PedidoData.Recordset.MoveLast
  NP = PedidoData.Recordset.Fields("NPedido").Value
  PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & NP & " ORDER BY PP.Linea"
  PPedidoData.Refresh
  Exit Function
MsgError:
  MsgBox Err.Description, vbCritical
End Function


Private Sub add_Click()
    Dim Nada As Integer
    Nada = OtroPedido(Orden(0))
End Sub

Private Sub Ante_Click()
    Dim Nada As Integer
    Nada = Anterior()
End Sub

Private Sub A�aBoton_Click()
   
  Dim id As Integer, NP As Long
On Error GoTo MsgError
  If CABox.Caption = "" Or CantBox.Text = "" Then
    
    MsgBox ("Necesita rellenar todos los campos")
  
  Else
     
    NP = NPedidoBox.Text
    
    With PPedido2Data
        If PPedidoData.Recordset.EOF = False Then
            PPedidoData.Recordset.MoveLast
            id = PPedidoData.Recordset.Fields("Linea").Value
        Else
            id = 0
        End If
        
        id = id + 1
        
        .Recordset.AddNew
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NPedido").Value = NPedidoBox.Text
        .Recordset.Fields("CodigoArticulo").Value = CABox.Caption
        If SuCodigoBox.Text = "" Then
            .Recordset.Fields("SuCodigo").Value = 0
        Else
            .Recordset.Fields("SuCodigo").Value = SuCodigoBox.Text
        End If
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .UpdateRecord
        
    End With
    
    If SuCodigoBox.Text <> "" Then
        ArtData.Recordset.Edit
        ArtData.Recordset.Fields("SuCodigo").Value = SuCodigoBox.Text
        ArtData.Recordset.Update
    End If
        
    PPedidoData.UpdateRecord
    PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & NP & " ORDER BY PP.Linea"
    PPedidoData.Refresh
    
    PPedido2Data.Refresh

  End If
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub artia_Click()
    MantArt.Show modal
    MantArt.OptionBox.Text = "1"
End Sub




Private Sub CATBox_Change()
On Error GoTo MsgError
    If CATBox.Text <> "" Then
        ATData.Recordset.FindFirst ("CodigoMaquina=" & CATBox.Text)
        ATBox.Caption = ATData.Recordset.Fields("Descripcion").Value
    Else
        CATBox.Text = "0"
    End If
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CATBox_CloseUp()
On Error GoTo MsgError
    ATData.Recordset.FindFirst ("CodigoMaquina=" & CATBox.Text)
    ATBox.Caption = ATData.Recordset.Fields("Descripcion").Value
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub CDptoBox_Change()
On Error GoTo MsgError
    If CDptoBox.Text <> "" Then
        DptoData.Recordset.FindFirst ("CodigoLugar=" & CDptoBox.Text)
        DptoBox.Caption = DptoData.Recordset.Fields("Localizacion").Value
    Else
        CDptoBox.Text = "0"
    End If
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CDptoBox_CloseUp()
On Error GoTo MsgError
    DptoData.Recordset.FindFirst ("CodigoLugar=" & CDptoBox.Text)
    DptoBox.Caption = DptoData.Recordset.Fields("Localizacion").Value
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CodEspBox_Change()
On Error GoTo MsgError
    If CodEspBox.Text <> "" Then
        SeccionData.Recordset.FindFirst ("CodigoEspecialidad=" & CodEspBox.Text)
        EspeBox.Caption = SeccionData.Recordset.Fields("Especialidad").Value
    Else
        CodEspBox.Text = "0"
    
    End If
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CodEspBox_CloseUp()
On Error GoTo MsgError
    SeccionData.Recordset.FindFirst ("CodigoEspecialidad=" & CodEspBox.Text)
    EspeBox.Caption = SeccionData.Recordset.Fields("Especialidad").Value
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub



Private Sub CPBox_Change()
On Error GoTo MsgError
    If CPBox.Text <> "" Then
        ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)
        ProvBox.Caption = ProvData.Recordset.Fields("Nombre").Value
        provtel.Caption = ProvData.Recordset.Fields("Telefono").Value
        If ProvData.Recordset.NoMatch = True Then
            ProvBox.Caption = "No Encontrado"
            provtel.Caption = "Sin Tel�fono"
        End If
    
    End If
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CPBox_CloseUp()
On Error GoTo MsgError
    If CPBox.Text <> "" Then
        ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)
        ProvBox.Caption = ProvData.Recordset.Fields("Nombre").Value
        If ProvData.Recordset.NoMatch = True Then
            ProvBox.Caption = "No Encontrado"
        End If
    
    End If
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Sub DescBox_Change()
On Error GoTo MsgError
    If DescBox.Text <> "" Then
        ArtData.Recordset.FindFirst ("Descripcion='" & DescBox.Text & "'")
        CABox.Caption = ArtData.Recordset.Fields("CodigoArticulo").Value
        If IsNull(ArtData.Recordset.Fields("SuCodigo").Value) = False Then
            SuCodigoBox.Text = ArtData.Recordset.Fields("SuCodigo").Value
        Else
            SuCodigoBox.Text = ""
        End If
    Else
        CABox.Caption = ""
        SuCodigoBox.Text = ""
    End If
    
    If ArtData.Recordset.NoMatch = True Then
        CABox.Caption = ""
        SuCodigoBox.Text = ""
    End If
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub DescBox_CloseUp()
On Error GoTo MsgError
    If DescBox.Text <> "" Then
        ArtData.Recordset.FindFirst ("Descripcion='" & DescBox.Text & "'")
        CABox.Caption = ArtData.Recordset.Fields("CodigoArticulo").Value
        If IsNull(ArtData.Recordset.Fields("SuCodigo").Value) = False Then
            SuCodigoBox.Text = ArtData.Recordset.Fields("SuCodigo").Value
        Else
            SuCodigoBox.Text = ""
        End If
    Else
        CABox.Caption = ""
        SuCodigoBox.Text = ""
    End If
    
    If ArtData.Recordset.NoMatch = True Then
        CABox.Caption = ""
        SuCodigoBox.Text = ""
    End If
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub eraser_Click()
    Dim Nada As Integer
    Nada = MsgBox("�Est� usted seguro de querer borrar el pedido?", vbYesNo)
    If Nada = vbYes Then
        PedidoData.Recordset.Delete
        Nada = Anterior()
    End If
End Sub

Private Sub exit_Click()
  Unload Me
End Sub

Private Sub first_Click()
    Dim Nada As Integer
    Nada = Primero()
End Sub

Private Sub imp1_Click()
    Dim Nada As Integer
    Nada = ImPedido()
End Sub

Private Sub ira_Click()
    Set Objeto = PedidoForm.PedidoData
    Set Objeto2 = PedidoForm.PPedidoData
    
    Selct = True
    Display(0) = "NPedido"
    DataCarac = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido="
    DataCarac2 = " ORDER BY PP.Linea"
    
    IraForm.Show modal
End Sub

Private Sub last_Click()
    Dim Nada As Integer
    Nada = Ultimo()
End Sub

Private Sub listf_Click()
    Dim Nada As Integer
    Nada = ListaPed()
End Sub

Private Sub prove_Click()
    MantProv.Show modal
End Sub

Private Sub sigui_Click()
    Dim Nada As Integer
    Nada = Siguiente()
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
  'barra de herramientas de pedidos
  Dim Nada As Variant
          
  Select Case Button.Key
    Case "Ant":       Nada = Anterior()
    Case "Sig":       Nada = Siguiente()
    Case "Salir":     Orden(0) = 0: Unload Me
    Case "Add":       Nada = OtroPedido(Orden(0))
    Case "AArticulo"
                      MantArt.Show modal
                      MantArt.OptionBox.Text = "1"
    Case "Lista":     Nada = ListaPed()
    Case "Borrar"
                      If MsgBox("�Est� usted seguro de querer borrar el pedido?", vbYesNo) = vbYes Then
                        PedidoData.Recordset.Delete
                        Nada = Anterior()
                      End If
    Case "Impr":      Nada = ImPedido()
    Case "Prov":      MantProv.Show modal
    Case "prim":      Nada = Primero()
    Case "ulti":      Nada = Ultimo()
  End Select
End Sub

Public Function OtroPedido(Orden As Long) As Integer

    Dim num As Integer
On Error GoTo MsgError
    num = UltimoPedido()
    
    With PedidoData
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveLast
        End If
        .Recordset.AddNew
        .Recordset.Fields("NPedido").Value = num
    End With

    NPedidoBox.Text = num
    CPBox.Text = 0
    NOrdenBox.Text = Orden
    FechaBox.Text = CStr(Format(Date, "dd/mm/yyyy"))
    PedidoData.Recordset.Fields("FechaPedido").Value = Date
    PedidoData.Recordset.Update
    PedidoData.Recordset.MoveLast
    PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & num & " ORDER BY PP.Linea"
    PPedidoData.Refresh
     
    Exit Function
MsgError:
    MsgBox Err.Description, vbCritical

End Function




Private Sub ActualizarBoton_Click()
    
    Dim id As Integer, NP As Integer
On Error GoTo MsgError
    id = IdBox.Text
    NP = NPedidoBox.Text
    
     With PPedido2Data
        .Recordset.FindFirst ("NPedido=" & NP & " AND Linea=" & id)
        .Recordset.Edit
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NPedido").Value = NPedidoBox.Text
        .Recordset.Fields("CodigoArticulo").Value = CABox.Caption
        If SuCodigoBox.Text <> "" Then
            .Recordset.Fields("SuCodigo").Value = SuCodigoBox.Text
        Else
            .Recordset.Fields("SuCodigo").Value = Null
        End If
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .UpdateRecord
    
    End With

    With PPedidoData
        .RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & NP & " ORDER BY PP.Linea"
        .Refresh
    End With
   
    
    A�aBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True

    id = ClearFields()
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Sub BorrarBoton_Click()
    Dim id As Integer, NP As Long
On Error GoTo MsgError
    id = IdBox.Text
    NP = NPedidoBox.Text
     
    With PPedido2Data
        .Recordset.FindFirst ("NPedido=" & NP & " AND Linea=" & id)
        .Recordset.Delete
        .Refresh
    End With
    
    With PPedidoData
        .RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & NP & " ORDER BY PP.Linea"
        .Refresh
    End With
    
        
    A�aBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    Exit Sub
MsgError:
    MsgBox Err.Description, vbCritical
    
End Sub

Private Sub SSDBGrid1_DblClick()
    
    Dim id As Integer
    
    With PPedidoData
        id = .Recordset.Fields("Linea").Value
        IdBox.Text = id
        CABox.Caption = .Recordset.Fields("CodigoArticulo").Value
        DescBox.Text = .Recordset.Fields("Descripcion").Value
        CantBox.Text = .Recordset.Fields("Cantidad").Value
        SuCodigoBox.Text = .Recordset.Fields("SuCodigo").Value
 
    End With
    
    A�aBoton.Visible = False
    BorrarBoton.Visible = True
    ActualizarBoton.Visible = True
    SSDBGrid1.Enabled = False
End Sub

Private Function ClearFields()

    CABox.Caption = ""
    CantBox.Text = ""
    SuCodigoBox.Text = ""
    
End Function

Private Function ImPedido()
    'imprime el pedido
    Dim wdApp As Word.Application
    Dim wdDoc As Word.Document
    Dim encargado As String, nempresa As String, direccion As String
    Dim nfax As String, npedido As String
    Dim Fil As Integer, Comentario As String
    Dim Opcion As Integer
On Error GoTo MsgError
    Set wdApp = New Word.Application
    Set wdDoc = wdApp.Documents.add("c:\archivos de programa\almacen\doc\pedidos.dot")
    wdDoc.Activate
        
    ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)
    
    nempresa = ProvData.Recordset.Fields("Nombre").Value
    If IsNull(ProvData.Recordset.Fields("Provincia").Value) = True Then
        direccion = "______"
    Else
        direccion = ProvData.Recordset.Fields("Provincia").Value
    End If
    
    If IsNull(ProvData.Recordset.Fields("Fax").Value) = True Then
        nfax = "No Especificado"
    Else
        nfax = ProvData.Recordset.Fields("Fax").Value
    End If
    npedido = NPedidoBox.Text
    Comentario = ComentBox.Text
    
    wdDoc.Range(wdDoc.Bookmarks("nempresa").Start, wdDoc.Bookmarks("nempresa").End).Text = nempresa
    wdDoc.Range(wdDoc.Bookmarks("direccion").Start, wdDoc.Bookmarks("direccion").End).Text = direccion
    wdDoc.Range(wdDoc.Bookmarks("nfax").Start, wdDoc.Bookmarks("nfax").End).Text = nfax
    wdDoc.Range(wdDoc.Bookmarks("npedido").Start, wdDoc.Bookmarks("npedido").End).Text = npedido
    wdDoc.Range(wdDoc.Bookmarks("comentario").Start, wdDoc.Bookmarks("comentario").End).Text = Comentario
    PPedidoData.Recordset.MoveFirst
    Fil = 2
        
    While PPedidoData.Recordset.EOF = False
        wdDoc.Tables(2).Rows.add
        wdDoc.Tables(2).Cell(Fil, 1).Select
        wdApp.Selection.Font.Bold = False
        wdApp.Selection.Font.Underline = wdUnderlineNone
        wdApp.Selection.Text = PPedidoData.Recordset.Fields("CodigoArticulo").Value
        wdDoc.Tables(2).Cell(Fil, 2).Select
        wdApp.Selection.Font.Bold = False
        wdApp.Selection.Font.Underline = wdUnderlineNone
        wdApp.Selection.Text = PPedidoData.Recordset.Fields("SuCodigo").Value
        wdDoc.Tables(2).Cell(Fil, 3).Select
        wdApp.Selection.Font.Bold = False
        wdApp.Selection.Font.Underline = wdUnderlineNone
        wdApp.Selection.Text = PPedidoData.Recordset.Fields("Descripcion").Value
        wdDoc.Tables(2).Cell(Fil, 4).Select
        wdApp.Selection.Font.Bold = False
        wdApp.Selection.Font.Underline = wdUnderlineNone
        wdApp.Selection.Text = PPedidoData.Recordset.Fields("Cantidad").Value
        
        Fil = Fil + 1
        PPedidoData.Recordset.MoveNext
        
    Wend
    
    'Opcion = MsgBox("� Se va a realizar alguna modificaci�n en el documento ?", vbYesNo)
    'If Opcion = vbYes Then
    '    With CommonDialog1
    '        .Filter = "All Files (*.*)|*.*|Documents (*.DOC)|*.DOC"
    '        .FilterIndex = 2
    '        .ShowSave
    '        wdDoc.SaveAs (.filename)
    '    End With
    'Else
    '    wdApp.Application.Visible = True
    '    wdApp.Dialogs(wdDialogFilePrint).Show
    'End If
    '
    'wdDoc.Close savechanges:=wdDoNotSaveChanges
    'wdApp.Application.Quit
    'Set wdApp = Nothing
        
    wdApp.Application.Visible = True
    
    Set wdApp = Nothing
    Set wdDoc = Nothing
    Exit Function
MsgError:
    MsgBox Err.Description, vbCritical
        
End Function

Private Function ListaPed()

  Dim Nada As Integer
  On Error GoTo MsgError
  'Inicializa todas las variables del listado, porque son globales
  Nada = NuevaBusq()
  'Campo a utilizar en la selecion
  Display(0) = "NPedido"
  Display(1) = "NOrden"
  Display(2) = "CodigoProveedor"
  Display(3) = "FechaPedido"
  Display(4) = "CodigoMaquina"
  'seleciona el tipo de control utlizado segun el frame
  TypeCampos(0) = cteCajaTexto
  TypeCampos(1) = cteCajaTexto
  TypeCampos(2) = cteCombo          'se utiliza un combo y los distintos campos y tablas que utiliza
    Lists(2, 0) = "CodigoProveedor, Nombre"
    Lists(2, 1) = "CodigoProveedor"
    Lists(2, 2) = "Proveedores ORDER BY Nombre"
  TypeCampos(3) = cteDateCombo
  TypeCampos(4) = cteCombo          'se utiliza un combo y los distintos campos y tablas que utiliza
    Lists(4, 0) = "CodigoMaquina, Descripcion"
    Lists(4, 1) = "CodigoMaquina"
    Lists(4, 2) = "InventMaquinas"

  'preforma la SELECT
  FRO = "Pedido P, Proveedores PR"
  For Nada = 0 To 4
      campos(Nada) = "P." & Display(Nada)
      SELE = SELE & campos(Nada) & ", "
  Next
  SELE = SELE & "PR.Nombre, P.Comentario"
  WHER = "P.CodigoProveedor=PR.CodigoProveedor "
  ORD = "P.NPedido"
  'determian los campos a utilizar en los frames cuando el control es un combo
'  Lists(2, 0) = "CodigoProveedor, Nombre"
'  Lists(2, 1) = "CodigoProveedor"
'  Lists(2, 2) = "Proveedores ORDER BY Nombre"
'  Lists(4, 0) = "CodigoMaquina, Descripcion"
'  Lists(4, 1) = "CodigoMaquina"
'  Lists(4, 2) = "InventMaquinas"
      
  
'  SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3) & "," & campos(4) & ",PR.Nombre,P.Comentario"
      
  Set Objeto = PedidoForm.PedidoData
  Set Objeto2 = PedidoForm.PPedidoData
  DataCarac = "SELECT PP.Linea, PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,AR.PrecioUnit " _
            & "FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido="
  DataCarac2 = " ORDER BY PP.Linea"
      
  Selct = True
      
  Nada = Listado()
  Exit Function
MsgError:
  MsgBox Err.Description, vbCritical

End Function
