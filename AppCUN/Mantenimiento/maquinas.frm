VERSION 5.00
Object = "{FAEEE763-117E-101B-8933-08002B2F4F5A}#1.1#0"; "DBLIST32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form MantInvForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de �reas de Trabajo"
   ClientHeight    =   7140
   ClientLeft      =   2865
   ClientTop       =   840
   ClientWidth     =   10095
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   10095
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   44
      Top             =   0
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   1535
      ButtonWidth     =   1482
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   12
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "Prim"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   11
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AnteriorBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Mant.Prev"
            Key             =   "MPrev"
            Description     =   "MPrevBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "SAT"
            Key             =   "SAT"
            Description     =   "SATBoton"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Baja"
            Key             =   "Baja"
            Description     =   "BajaBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Alta"
            Key             =   "Alta"
            Description     =   "AltaBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button12 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "Ulti"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   12
         EndProperty
      EndProperty
   End
   Begin VB.Data PerData 
      Caption         =   "Periodicidad"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      EOFAction       =   1  'EOF
      Exclusive       =   0   'False
      Height          =   345
      Left            =   6840
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "Periodicidad"
      Top             =   6840
      Visible         =   0   'False
      Width           =   1815
   End
   Begin SSDataWidgets_B.SSDBCombo CPeriBox 
      Bindings        =   "maquinas.frx":0000
      DataField       =   "CodFrecuenciaMP"
      DataSource      =   "InventMaq"
      Height          =   255
      Left            =   6840
      TabIndex        =   42
      Top             =   3720
      Width           =   855
      DataFieldList   =   "CodigoPeriodicidad"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2672
      Columns(0).Caption=   "CodigoPeriodicidad"
      Columns(0).Name =   "CodigoPeriodicidad"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "CodigoPeriodicidad"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   4789
      Columns(1).Caption=   "Periodicidad"
      Columns(1).Name =   "Periodicidad"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Periodicidad"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   1508
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "CodigoPeriodicidad"
   End
   Begin SSDataWidgets_B.SSDBCombo CDptoBox 
      Bindings        =   "maquinas.frx":0012
      DataField       =   "CodigoLocalizacion"
      DataSource      =   "InventMaq"
      Height          =   255
      Left            =   2280
      TabIndex        =   39
      Top             =   3240
      Width           =   1335
      DataFieldList   =   "CodigoLugar"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Columns(0).Width=   3200
      _ExtentX        =   2355
      _ExtentY        =   450
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "CodigoLugar"
   End
   Begin VB.ComboBox ContratoList 
      DataField       =   "ContratoMantenimiento"
      DataSource      =   "InventMaq"
      Height          =   315
      Left            =   6600
      TabIndex        =   38
      Top             =   4080
      Width           =   2895
   End
   Begin VB.TextBox Text1 
      DataField       =   "Criticidad"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   4560
      TabIndex        =   37
      Top             =   5400
      Width           =   855
   End
   Begin VB.CheckBox Check1 
      Alignment       =   1  'Right Justify
      Caption         =   "Baja"
      DataField       =   "Baja"
      DataSource      =   "InventMaq"
      Enabled         =   0   'False
      Height          =   255
      Left            =   6720
      TabIndex        =   35
      Top             =   5400
      Width           =   735
   End
   Begin SSCalendarWidgets_A.SSDateCombo FechCompBox 
      DataField       =   "FechaCompra"
      DataSource      =   "InventMaq"
      Height          =   255
      Left            =   7560
      TabIndex        =   34
      Top             =   1800
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDBCtls.DBCombo SATList 
      Bindings        =   "maquinas.frx":0026
      DataField       =   "SAT"
      DataSource      =   "InventMaq"
      Height          =   315
      Left            =   6720
      TabIndex        =   33
      Top             =   4800
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   556
      _Version        =   327681
      Style           =   2
      ListField       =   "Nombre"
      Text            =   ""
   End
   Begin VB.Data LugarData 
      Caption         =   "Lugar"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   4800
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Lugar"
      Top             =   6840
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data SAT 
      Caption         =   "SAT"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      EOFAction       =   1  'EOF
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2760
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SAT"
      Top             =   6840
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data InventMaq 
      Caption         =   "Maquina"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      EOFAction       =   1  'EOF
      Exclusive       =   0   'False
      Height          =   345
      Left            =   960
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM InventMaquinas ORDER BY CodigoMaquina"
      Top             =   6840
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox InstBox 
      DataField       =   "Instalacion"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   2280
      TabIndex        =   30
      Top             =   2880
      Width           =   7575
   End
   Begin VB.TextBox cygbox 
      DataField       =   "ClaseYGrado"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   7560
      TabIndex        =   29
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox A�GaraBox 
      DataField       =   "A�osGarantia"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   7560
      TabIndex        =   27
      Top             =   2160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox ObservBox 
      DataField       =   "Observaciones"
      DataSource      =   "InventMaq"
      Height          =   855
      Left            =   1920
      TabIndex        =   25
      Top             =   6000
      Width           =   7935
   End
   Begin VB.TextBox ModeloBox 
      DataField       =   "Modelo"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   2280
      TabIndex        =   23
      Top             =   1440
      Width           =   1935
   End
   Begin VB.TextBox MarcaBox 
      DataField       =   "Fabricante"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   2280
      TabIndex        =   22
      Top             =   2160
      Width           =   1935
   End
   Begin VB.TextBox CosteBox 
      DataField       =   "CosteDelContrato"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   6720
      TabIndex        =   19
      Top             =   4440
      Width           =   2055
   End
   Begin VB.Frame DocumenFrame 
      Caption         =   "Documentaci�n"
      Height          =   1935
      Left            =   600
      TabIndex        =   10
      Top             =   3600
      Width           =   2655
      Begin VB.CheckBox CertCheck 
         Caption         =   "Check3"
         DataField       =   "CertificadoCE"
         DataSource      =   "InventMaq"
         Height          =   255
         Left            =   2040
         TabIndex        =   13
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox ManMantCheck 
         Caption         =   "Check2"
         DataField       =   "ManualMantenimiento"
         DataSource      =   "InventMaq"
         Height          =   255
         Left            =   2040
         TabIndex        =   12
         Top             =   840
         Width           =   255
      End
      Begin VB.CheckBox ManUsCheck 
         Caption         =   "Check1"
         DataField       =   "ManualUsuario"
         DataSource      =   "InventMaq"
         Height          =   255
         Left            =   2040
         TabIndex        =   11
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label9 
         Caption         =   "Certificado CE"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label Label8 
         Caption         =   "Manual Mantenimiento"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "Manual Usuario"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.TextBox PrecioBox 
      DataField       =   "PrecioCompra"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   7560
      TabIndex        =   9
      Top             =   1440
      Width           =   1935
   End
   Begin VB.TextBox NSerieBox 
      DataField       =   "NSerie"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   2280
      TabIndex        =   6
      Top             =   1800
      Width           =   1935
   End
   Begin VB.TextBox DescrBox 
      DataField       =   "Descripcion"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   2280
      TabIndex        =   3
      Top             =   2520
      Width           =   7575
   End
   Begin VB.TextBox CMBox 
      DataField       =   "CodigoMaquina"
      DataSource      =   "InventMaq"
      Height          =   285
      Left            =   2280
      TabIndex        =   0
      Top             =   1080
      Width           =   1935
   End
   Begin VB.Label PeriBox 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   7920
      TabIndex        =   43
      Top             =   3720
      Width           =   1935
   End
   Begin VB.Line Line2 
      X1              =   7680
      X2              =   7920
      Y1              =   3840
      Y2              =   3840
   End
   Begin VB.Label Label18 
      Caption         =   "Periodicidad Mant.Preventivo:"
      Height          =   255
      Left            =   4440
      TabIndex        =   41
      Top             =   3720
      Width           =   2295
   End
   Begin VB.Label DptoBox 
      BorderStyle     =   1  'Fixed Single
      DataField       =   "Localizacion"
      DataSource      =   "LugarData"
      Height          =   255
      Left            =   3960
      TabIndex        =   40
      Top             =   3240
      Width           =   5895
   End
   Begin VB.Line Line1 
      X1              =   3600
      X2              =   3960
      Y1              =   3360
      Y2              =   3360
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   8640
      Top             =   5160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   12
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":0034
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":034E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":0668
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":0982
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":0C9C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":0FB6
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":12D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":15EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":1904
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":1C1E
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":1F38
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "maquinas.frx":2252
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label19 
      Caption         =   "Criticidad:"
      Height          =   255
      Left            =   3720
      TabIndex        =   36
      Top             =   5400
      Width           =   735
   End
   Begin VB.Label Label17 
      Caption         =   "Servicio Asistencia Tecnica"
      Height          =   255
      Left            =   4440
      TabIndex        =   32
      Top             =   4800
      Width           =   2055
   End
   Begin VB.Label Label16 
      Caption         =   "Instalaci�n"
      Height          =   255
      Left            =   600
      TabIndex        =   31
      Top             =   2880
      Width           =   1335
   End
   Begin VB.Label Label15 
      Caption         =   "Clase y Grado"
      Height          =   255
      Left            =   6120
      TabIndex        =   28
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label A�GarLabel 
      Caption         =   "A�os Garantia"
      Height          =   255
      Left            =   6120
      TabIndex        =   26
      Top             =   2160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label14 
      Caption         =   "Observaciones"
      Height          =   255
      Left            =   480
      TabIndex        =   24
      Top             =   5880
      Width           =   1335
   End
   Begin VB.Label Label13 
      Caption         =   "Modelo"
      Height          =   255
      Left            =   600
      TabIndex        =   21
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label Label12 
      Caption         =   "Fabricante"
      Height          =   255
      Left            =   600
      TabIndex        =   20
      Top             =   2160
      Width           =   855
   End
   Begin VB.Label Label11 
      Caption         =   "Coste del contrato"
      Height          =   255
      Left            =   4440
      TabIndex        =   18
      Top             =   4440
      Width           =   2175
   End
   Begin VB.Label Label10 
      Caption         =   "Contrato de mantenimiento"
      Height          =   255
      Left            =   4440
      TabIndex        =   17
      Top             =   4080
      Width           =   2055
   End
   Begin VB.Label Label6 
      Caption         =   "Precio de Compra"
      Height          =   255
      Left            =   6120
      TabIndex        =   8
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label Label5 
      Caption         =   "Fecha de Compra"
      Height          =   255
      Left            =   6120
      TabIndex        =   7
      Top             =   1800
      Width           =   1335
   End
   Begin VB.Label Label4 
      Caption         =   "N� Serie"
      Height          =   255
      Left            =   600
      TabIndex        =   5
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo Departamento:"
      Height          =   255
      Left            =   600
      TabIndex        =   4
      Top             =   3240
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Descripci�n"
      Height          =   255
      Left            =   600
      TabIndex        =   2
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "�rea de Trabajo:"
      Height          =   255
      Left            =   600
      TabIndex        =   1
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu Exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu ira 
         Caption         =   "Ir a "
         Shortcut        =   {F1}
      End
      Begin VB.Menu first 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu statr 
      Caption         =   "&Estado"
      Begin VB.Menu alta 
         Caption         =   "Alta"
      End
      Begin VB.Menu baja 
         Caption         =   "Baja"
      End
   End
   Begin VB.Menu confi 
      Caption         =   "&Configuraciones"
      Begin VB.Menu MPr 
         Caption         =   "Mantenimiento Preventivo"
      End
      Begin VB.Menu sated 
         Caption         =   "S.A.T."
      End
   End
   Begin VB.Menu lstd 
      Caption         =   "&Listados"
      Begin VB.Menu lista 
         Caption         =   "Listado"
      End
   End
End
Attribute VB_Name = "MantInvForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function Anterior()
    
    InventMaq.Recordset.MovePrevious
    
    If InventMaq.Recordset.BOF = True Then
        InventMaq.Recordset.MoveNext
    End If
        
End Function

Private Sub add_Click()
    Dim nada As Integer
    nada = OtraMaquina()
End Sub

Private Sub alta_Click()
On Error GoTo msgError
    With InventMaq
        .Recordset.Edit
        .Recordset.Fields("Baja").Value = False
        .Recordset.Fields("Observaciones").Value = .Recordset.Fields("Observaciones").Value & " Maquina dada de alta el " & Date
        .Recordset.Update
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
End Sub

Private Sub baja_Click()
On Error GoTo msgError
    With InventMaq
        .Recordset.Edit
        .Recordset.Fields("Baja").Value = True
        .Recordset.Fields("FechaBaja").Value = Date
        .Recordset.Fields("Observaciones").Value = .Recordset.Fields("Observaciones").Value & "Maquina dada de baja el " & InventMaq.Recordset.Fields("FechaBaja").Value
        .Recordset.Update
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CDptoBox_Change()
On Error GoTo msgError
    If CDptoBox <> "" Then
        With LugarData.Recordset
            .FindFirst ("CodigoLugar=" & CDptoBox.Text)
            If .NoMatch = True Then
                DptoBox.Caption = "No Encontrado"
            End If
        
        End With
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CDptoBox_CloseUp()
On Error GoTo msgError
    With LugarData.Recordset
        .FindFirst ("CodigoLugar=" & CDptoBox.Text)
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Sub CPeriBox_Change()
On Error GoTo msgError
    If CPeriBox <> "" Then
        PerData.Recordset.FindFirst ("CodigoPeriodicidad=" & CPeriBox.Text)
        PeriBox.Caption = PerData.Recordset.Fields("Periodicidad").Value
    Else
        CPeriBox.Text = "0"
    
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CPeriBox_CloseUp()
On Error GoTo msgError
    If CPeriBox <> "" Then
        PerData.Recordset.FindFirst ("CodigoPeriodicidad=" & CPeriBox.Text)
        PeriBox.Caption = PerData.Recordset.Fields("Periodicidad").Value
    Else
        CPeriBox.Text = "0"
    
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar el �rea de trabajo?", vbYesNo)
    If nada = vbYes Then
        InventMaq.Recordset.Delete
        nada = Anterior()
    End If
End Sub

Private Sub exit_Click()
    InventMaq.UpdateRecord
    Unload Me
    
End Sub

Private Sub first_Click()
    InventMaq.Recordset.MoveFirst
End Sub

Private Sub ira_Click()
    Set Objeto = MantInvForm.InventMaq
    Display(0) = "CodigoMaquina"
    Selct = False
    IraForm.Show modal

End Sub

Private Sub last_Click()
    InventMaq.Recordset.MoveLast
    
End Sub

Private Sub lista_Click()
    Dim nada As Integer
    nada = ListaMaq()
End Sub


Private Function Siguiente()
    
    InventMaq.Recordset.MoveNext
    
    If InventMaq.Recordset.EOF = True Then
        InventMaq.Recordset.MovePrevious
    End If
                
                
End Function



Private Sub ContratoList_Click()
    If ContratoList.Text = "Garantia" Then
        A�GaraBox.Visible = True
        A�GarLabel.Visible = True
    Else
        A�GaraBox.Visible = False
        A�GarLabel.Visible = False
    End If

End Sub

Private Sub Form_Load()
    ContratoList.AddItem "No Especificado"
    ContratoList.AddItem "Completo"
    ContratoList.AddItem "Mano de Obra"
    ContratoList.AddItem "Piezas"
    ContratoList.AddItem "Preventivo"
    ContratoList.AddItem "Garantia"
    ContratoList.AddItem "Ninguno"
End Sub

Private Function MPreventivo()
    CM = InventMaq.Recordset.Fields("CodigoMaquina").Value
    If InventMaq.Recordset.Fields("Descripcion").Value <> "" Then
            Descripcion = InventMaq.Recordset.Fields("Descripcion").Value
    Else
        Descripcion = ""
    End If
    
    MPForm.Show modal
        
End Function

Private Sub MPr_Click()
On Error GoTo msgError
    Load MPForm
    MPForm.MPData.RecordSource = "SELECT * FROM MantPreventivo WHERE CodigoMaquina=" & CMBox.Text & " ORDER BY CodigoPeriodicidad"
    MPForm.MPData.Refresh
    MPForm.CMLabel.Caption = CMBox.Text
    MPForm.DescLabel.Caption = DescrBox.Text
    MPForm.Show
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub sated_Click()
    SATForm.Show modal
End Sub

Private Sub sigui_Click()
    Dim nada As Integer
    nada = Siguiente()
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
On Error GoTo msgError
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        InventMaq.UpdateRecord
        Unload Me
    Case "Add"
        nada = OtraMaquina()
    Case "SAT"
        SATForm.Show modal

    Case "Lista"
        nada = ListaMaq()
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar el �rea de trabajo?", vbYesNo)
        If nada = vbYes Then
            InventMaq.Recordset.Delete
            nada = Anterior()
        End If
    Case "Baja"
        With InventMaq
            .Recordset.Edit
            .Recordset.Fields("Baja").Value = True
            .Recordset.Fields("FechaBaja").Value = Date
            .Recordset.Fields("Observaciones").Value = .Recordset.Fields("Observaciones").Value & "Maquina dada de baja el " & InventMaq.Recordset.Fields("FechaBaja").Value
            .Recordset.Update
        End With
    Case "Alta"
        With InventMaq
            .Recordset.Edit
            .Recordset.Fields("Baja").Value = False
            .Recordset.Fields("Observaciones").Value = .Recordset.Fields("Observaciones").Value & "Maquina dada de alta el " & Date
            .Recordset.Update
        End With
    
    Case "MPrev"
        Load MPForm
        MPForm.MPData.RecordSource = "SELECT * FROM MantPreventivo WHERE CodigoMaquina=" & CMBox.Text & " ORDER BY CodigoPeriodicidad"
        MPForm.MPData.Refresh
        MPForm.CMLabel.Caption = CMBox.Text
        MPForm.DescLabel.Caption = DescrBox.Text
        MPForm.Show
    Case "Prim"
        InventMaq.Recordset.MoveFirst
    Case "Ulti"
        InventMaq.Recordset.MoveLast
    
    End Select
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Function OtraMaquina()

Dim last As Long

With InventMaq
    .Recordset.MoveLast
    last = 1 + .Recordset.Fields("CodigoMaquina").Value
    .Recordset.AddNew
    .Recordset.Fields("CodigoMaquina").Value = last
    .Recordset.Fields("CodFrecuenciaMP").Value = 0
    .Recordset.Update
    .Recordset.MoveLast
End With

End Function

Private Function ListaMaq()
On Error GoTo msgError
    Dim nada As Integer
    
    nada = NuevaBusq()
    Display(0) = "CodigoMaquina"
    Display(1) = "Descripcion"
    Display(2) = "CodigoLocalizacion"
    Display(3) = "Instalacion"
    Display(4) = "CodigoMaquina (SECUENCIAL)"
    
    TypeCampos(0) = 1
    TypeCampos(1) = 4
    TypeCampos(2) = 3
    TypeCampos(3) = 4
    TypeCampos(4) = 4
    TypeCampos(5) = 0
    TypeCampos(6) = 0
    TypeCampos(7) = 0
    
    For nada = 0 To 3
        campos(nada) = "M." & Display(nada)
    Next
    campos(4) = "CodigoMaquina"
        
    Lists(2, 0) = "CodigoLugar, Localizacion"
    Lists(2, 1) = "CodigoLugar"
    Lists(2, 2) = "Lugar"
        
    FRO = "InventMaquinas M, Lugar L"
    SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3) & ",L.Localizacion"
    WHER = "M.CodigoLocalizacion=L.CodigoLugar"
        
    Set Objeto = MantInvForm.InventMaq
    
    Selct = False
        
    nada = Listado()
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function
