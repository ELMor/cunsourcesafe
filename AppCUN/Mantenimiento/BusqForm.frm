VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form BusqForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Par�metros de B�squeda"
   ClientHeight    =   7095
   ClientLeft      =   2640
   ClientTop       =   1215
   ClientWidth     =   7740
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   7740
   ShowInTaskbar   =   0   'False
   Begin VB.Data Data 
      Caption         =   "Data4"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Index           =   4
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6720
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton ListadoBoton 
      Caption         =   "Listado"
      Height          =   375
      Left            =   3960
      TabIndex        =   63
      Top             =   6360
      Width           =   1215
   End
   Begin VB.Frame Frame 
      Caption         =   "Frame1"
      Height          =   735
      Index           =   1
      Left            =   360
      TabIndex        =   2
      Top             =   960
      Visible         =   0   'False
      Width           =   6975
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   12
         Left            =   4200
         TabIndex        =   68
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   11
         Left            =   360
         TabIndex        =   67
         Top             =   240
         Visible         =   0   'False
         Width           =   2275
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   12
         Left            =   4200
         TabIndex        =   47
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   255
         Index           =   1
         Left            =   3240
         TabIndex        =   37
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   11
         Left            =   360
         TabIndex        =   21
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0000
         Height          =   375
         Index           =   11
         Left            =   360
         TabIndex        =   19
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   11
         Left            =   360
         TabIndex        =   20
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0012
         Height          =   375
         Index           =   12
         Left            =   4200
         TabIndex        =   45
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BorderStyle     =   0
         DefColWidth     =   7056
         RowHeight       =   423
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   12
         Left            =   4200
         TabIndex        =   46
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   9
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Frame4"
      Height          =   735
      Index           =   4
      Left            =   360
      TabIndex        =   5
      Top             =   3480
      Visible         =   0   'False
      Width           =   6975
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   42
         Left            =   4200
         TabIndex        =   74
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   41
         Left            =   360
         TabIndex        =   73
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   42
         Left            =   4200
         TabIndex        =   56
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   255
         Index           =   4
         Left            =   3360
         TabIndex        =   40
         Top             =   480
         Width           =   255
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   41
         Left            =   360
         TabIndex        =   30
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0024
         Height          =   375
         Index           =   4
         Left            =   360
         TabIndex        =   28
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   41
         Left            =   360
         TabIndex        =   29
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0036
         Height          =   375
         Index           =   42
         Left            =   4200
         TabIndex        =   54
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   42
         Left            =   4200
         TabIndex        =   55
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label5 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   12
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Data Data 
      Caption         =   "Data3"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Index           =   3
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data Data 
      Caption         =   "Data2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Index           =   2
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6000
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data Data 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Index           =   1
      Left            =   5520
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame Frame 
      Caption         =   "Frame6"
      Height          =   735
      Index           =   6
      Left            =   360
      TabIndex        =   7
      Top             =   5160
      Visible         =   0   'False
      Width           =   6975
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   62
         Left            =   4200
         TabIndex        =   78
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   61
         Left            =   360
         TabIndex        =   77
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   62
         Left            =   4200
         TabIndex        =   62
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   255
         Index           =   6
         Left            =   3240
         TabIndex        =   42
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   61
         Left            =   360
         TabIndex        =   36
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0048
         Height          =   375
         Index           =   61
         Left            =   360
         TabIndex        =   34
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   61
         Left            =   360
         TabIndex        =   35
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0058
         Height          =   375
         Index           =   62
         Left            =   4200
         TabIndex        =   60
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   62
         Left            =   4200
         TabIndex        =   61
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label7 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   14
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Frame5"
      Height          =   735
      Index           =   5
      Left            =   360
      TabIndex        =   6
      Top             =   4320
      Visible         =   0   'False
      Width           =   6975
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   52
         Left            =   4200
         TabIndex        =   76
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   51
         Left            =   360
         TabIndex        =   75
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   52
         Left            =   4200
         TabIndex        =   59
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   255
         Index           =   5
         Left            =   3240
         TabIndex        =   41
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   51
         Left            =   360
         TabIndex        =   33
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0068
         Height          =   375
         Index           =   51
         Left            =   360
         TabIndex        =   31
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   51
         Left            =   360
         TabIndex        =   32
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0078
         Height          =   375
         Index           =   52
         Left            =   4200
         TabIndex        =   57
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   52
         Left            =   4200
         TabIndex        =   58
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label6 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   13
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Frame3"
      Height          =   735
      Index           =   3
      Left            =   360
      TabIndex        =   4
      Top             =   2640
      Visible         =   0   'False
      Width           =   6975
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   32
         Left            =   4200
         TabIndex        =   72
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   31
         Left            =   360
         TabIndex        =   71
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   32
         Left            =   4200
         TabIndex        =   53
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   255
         Index           =   3
         Left            =   3240
         TabIndex        =   39
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   31
         Left            =   360
         TabIndex        =   27
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":0088
         Height          =   375
         Index           =   31
         Left            =   360
         TabIndex        =   25
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   31
         Left            =   360
         TabIndex        =   26
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":009A
         Height          =   375
         Index           =   32
         Left            =   4200
         TabIndex        =   51
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   32
         Left            =   4200
         TabIndex        =   52
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label4 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   11
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Frame2"
      Height          =   735
      Index           =   2
      Left            =   360
      TabIndex        =   3
      Top             =   1800
      Visible         =   0   'False
      Width           =   6975
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   22
         Left            =   4200
         TabIndex        =   70
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   21
         Left            =   360
         TabIndex        =   69
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox Text 
         DataSource      =   "Data(2)"
         Height          =   375
         Index           =   22
         Left            =   4200
         TabIndex        =   50
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   255
         Index           =   2
         Left            =   3360
         TabIndex        =   38
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   21
         Left            =   360
         TabIndex        =   24
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":00AC
         Height          =   375
         Index           =   21
         Left            =   360
         TabIndex        =   22
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   21
         Left            =   480
         TabIndex        =   23
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":00BE
         Height          =   375
         Index           =   22
         Left            =   4200
         TabIndex        =   48
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   22
         Left            =   4200
         TabIndex        =   49
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
         _Version        =   65537
         _ExtentX        =   4048
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label3 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   10
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame 
      Caption         =   "Frame0"
      Height          =   735
      Index           =   0
      Left            =   360
      TabIndex        =   1
      Top             =   120
      Width           =   6975
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   2
         Left            =   4200
         TabIndex        =   66
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox TextLike 
         Height          =   375
         Index           =   1
         Left            =   360
         TabIndex        =   65
         Top             =   240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   2
         Left            =   4200
         TabIndex        =   64
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":00D0
         Height          =   375
         Index           =   1
         Left            =   360
         TabIndex        =   16
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.TextBox Text 
         Height          =   375
         Index           =   1
         Left            =   720
         TabIndex        =   18
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.CheckBox Check 
         Caption         =   "Check1"
         Height          =   255
         Index           =   0
         Left            =   3360
         TabIndex        =   15
         Top             =   360
         Width           =   255
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   1
         Left            =   360
         TabIndex        =   17
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Combo 
         Bindings        =   "BusqForm.frx":00E2
         Height          =   375
         Index           =   2
         Left            =   4200
         TabIndex        =   43
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo sDate 
         Height          =   375
         Index           =   2
         Left            =   4200
         TabIndex        =   44
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   8
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Data Data 
      Caption         =   "Data0"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Index           =   0
      Left            =   5520
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6000
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CancelarBoton 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2280
      TabIndex        =   0
      Top             =   6360
      Width           =   1095
   End
End
Attribute VB_Name = "BusqForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CancelarBoton_Click()
  Unload Me
End Sub

Private Sub Check_Click(Index As Integer)
  'hace visible o invisible el segundo control en funcion del check
  Dim blnActivado As Boolean
  Dim Indice As Integer
  
  If Check(Index).Value = 1 Then blnActivado = True Else blnActivado = False
  Indice = 10 * Index + 2
  Select Case TypeCampos(Index)
   Case cteCajaTexto: Text(Indice).Visible = blnActivado
   Case cteDate:      sDate(Indice).Visible = blnActivado
   Case cteCombo:     Combo(Indice).Visible = blnActivado
   Case cteTextLike:  TextLike(Indice).Visible = blnActivado
  End Select
End Sub

Private Sub ListadoBoton_Click()
  'crea el WHERE de la  SELECT dependiendo de:
  ' los controles utilizados (uno o dos por frame seg�n si se utiliza el intervalo)
  ' el tipo de control utilizado (Caja de Texto, Control data, Combo o Control de Fecha)
  ' los valores de los controles
  Dim Indice As Integer
  Dim Pross As Integer
  
  Pross = 0
  If WHER <> "" Then Pross = Pross + 1
  For i = 0 To 6
    Indice = i * 10 + 1
    Select Case TypeCampos(i)
      Case cteCajaTexto
        If Text(Indice).Text <> "" Then
          If Pross > 0 Then WHER = WHER & " AND "
          Pross = Pross + 1
          If Check(i).Value = 1 Then
            WHER = WHER & campos(i) & " BETWEEN " & Text(Indice).Text & " AND " & Text(Indice + 1).Text
          Else
            WHER = campos(i) & "=" & Text(Indice).Text
          End If
        End If
      Case cteDate
        If sDate(Indice).Text <> "" Then
          If Pross > 0 Then WHER = WHER & " AND "
          Pross = Pross + 1
          If Check(i).Value = 1 Then
            WHER = WHER & campos(i) & " BETWEEN #" & Format(CDate(sDate(Indice).Text), "m/d/yyyy") & "# AND #" & Format(CDate(sDate(Indice + 1).Text), "m/d/yyyy") & "#"
          Else
            WHER = WHER & campos(i) & "=#" & Format(CDate(sDate(Indice).Text), "m/d/yyyy") & "#"
          End If
        End If
      Case cteCombo
        If Combo(Indice).Text <> "" Then
          If Pross > 0 Then WHER = WHER & " AND "
          Pross = Pross + 1
          If Check(i).Value = 1 Then
            WHER = WHER & campos(i) & " BETWEEN " & Combo(Indice).Text & " AND " & Combo(Indice + 1).Text
          Else
            WHER = WHER & campos(1) & "=" & Combo(Indice).Text
          End If
        End If
      Case cteTextLike
        If TextLike(Indice).Text <> "" Then
          If Pross > 0 Then WHER = WHER & " AND "
          Pross = Pross + 1
          WHER = WHER & campos(i) & " LIKE '" & TextLike(Indice) & "*'"
        End If
    End Select
  Next i
  MosBusForm.Show modal
  Unload Me
End Sub
