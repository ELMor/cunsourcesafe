VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form MantPerio 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Intervalos de Tiempo"
   ClientHeight    =   3030
   ClientLeft      =   5505
   ClientTop       =   3495
   ClientWidth     =   6195
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   6195
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   8
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "first"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "last"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBCombo2 
      Bindings        =   "MantPerio.frx":0000
      DataField       =   "Unidad"
      DataSource      =   "PerData"
      Height          =   255
      Left            =   2160
      TabIndex        =   6
      Top             =   1800
      Width           =   975
      DataFieldList   =   "PerUni"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Columns(0).Width=   3200
      _ExtentX        =   1720
      _ExtentY        =   450
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "PerUni"
   End
   Begin VB.TextBox NombreBox 
      DataField       =   "Periodicidad"
      DataSource      =   "PerData"
      Height          =   285
      Left            =   3360
      TabIndex        =   5
      Top             =   1200
      Width           =   1455
   End
   Begin SSDataWidgets_B.SSDBCombo CPebox 
      Bindings        =   "MantPerio.frx":0013
      DataField       =   "CodigoPeriodicidad"
      DataSource      =   "PerData"
      Height          =   255
      Left            =   2160
      TabIndex        =   4
      Top             =   1200
      Width           =   975
      DataFieldList   =   "CodigoPeriodicidad"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Caption=   "CodigoPeriodicidad"
      Columns(0).Name =   "CodigoPeriodicidad"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "CodigoPeriodicidad"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Periodicidad"
      Columns(1).Name =   "Periodicidad"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Periodicidad"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   1720
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "CodigoPeriodicidad"
   End
   Begin VB.TextBox ValorBox 
      DataField       =   "Valor"
      DataSource      =   "PerData"
      Height          =   285
      Left            =   2160
      TabIndex        =   1
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Data PerData 
      Caption         =   "Periodos"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   1320
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Periodicidad"
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data UnisData 
      Caption         =   "Unidades"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   2880
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "UniPeriodicidad"
      Top             =   0
      Visible         =   0   'False
      Width           =   1695
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantPerio.frx":0025
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantPerio.frx":033F
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantPerio.frx":0659
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantPerio.frx":0973
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantPerio.frx":0C8D
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantPerio.frx":0FA7
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantPerio.frx":12C1
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantPerio.frx":15DB
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label4 
      BorderStyle     =   1  'Fixed Single
      DataField       =   "Nombre"
      DataSource      =   "UnisData"
      Height          =   255
      Left            =   3360
      TabIndex        =   7
      Top             =   1800
      Width           =   1455
   End
   Begin VB.Line Line2 
      X1              =   3120
      X2              =   3360
      Y1              =   1920
      Y2              =   1920
   End
   Begin VB.Line Line1 
      X1              =   3120
      X2              =   3360
      Y1              =   1320
      Y2              =   1320
   End
   Begin VB.Label Label3 
      Caption         =   "Valor:"
      Height          =   255
      Left            =   1440
      TabIndex        =   3
      Top             =   2400
      Width           =   495
   End
   Begin VB.Label Label2 
      Caption         =   "Unidad:"
      Height          =   255
      Left            =   1320
      TabIndex        =   2
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Nombre Periodicidad:"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu prim 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu ultim 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu listad 
      Caption         =   "&Listados"
      Begin VB.Menu all 
         Caption         =   "Todos"
      End
   End
End
Attribute VB_Name = "MantPerio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function Anterior()
    PerData.Recordset.MovePrevious
 
    If PerData.Recordset.BOF = True Then
        PerData.Recordset.MoveNext
    End If
    
End Function

Private Function OtroPer()
    
    Dim i As Long
    
    PerData.Recordset.MoveLast
    i = PerData.Recordset.Fields("CodigoPeriodicidad").Value
    PerData.Recordset.AddNew
    PerData.Recordset.Fields("CodigoPeriodicidad").Value = i + 1
    PerData.Recordset.Update
    PerData.Recordset.MoveLast
    
End Function

Private Function Siguiente()
    
    PerData.Recordset.MoveNext
    
    If PerData.Recordset.EOF = True Then
        PerData.Recordset.MovePrevious
    End If

End Function


Private Sub add_Click()
    Dim nada As Integer
    nada = OtroPer()
End Sub

Private Sub all_Click()
    Dim nada As Integer
    nada = ListaPer()
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
End Sub

Private Sub CPebox_CloseUp()
On Error GoTo msgError
    PerData.Recordset.FindFirst ("CodigoPeriodicidad=" & CPebox.Text)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar el per�odo?", vbYesNo)
    If nada = vbYes Then
        PerData.Recordset.Delete
        nada = Anterior()
    End If

End Sub

Private Sub exit_Click()
    Unload Me
End Sub


Private Sub prim_Click()
    PerData.Recordset.MoveFirst
    
End Sub

Private Sub sigui_Click()
    Dim nada As Integer
    nada = Siguiente()
End Sub

Private Sub SSDBCombo2_Change()
On Error GoTo msgError
    If SSDBCombo2.Text <> "" Then
        UnisData.Recordset.FindFirst ("PerUni='" & SSDBCombo2.Text & "'")
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub SSDBCombo2_CloseUp()
On Error GoTo msgError
    UnisData.Recordset.FindFirst ("PerUni='" & SSDBCombo2.Text & "'")
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtroPer()
    
    Case "Lista"
        nada = ListaPer()
            
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar el per�odo?", vbYesNo)
        If nada = vbYes Then
            PerData.Recordset.Delete
            nada = Anterior()
        End If
    Case "first"
        PerData.Recordset.MoveFirst
    Case "last"
        PerData.Recordset.MoveLast

    End Select
    

End Sub



Private Sub ultim_Click()
    PerData.Recordset.MoveLast
End Sub

Private Function ListaPer()
    Dim nada As Integer
    nada = NuevaBusq()
    campos(0) = "CodigoPeriodicidad"
    campos(1) = "Periodicidad"
    campos(2) = "Unidad"
    campos(3) = "Valor"
    For nada = 0 To 3
        Display(nada) = campos(nada)
    Next
        
    SELE = "CodigoPeriodicidad, Periodicidad, Unidad, Valor"
    FRO = "Periodicidad"
        
    Set Objeto = MantPerio.PerData
    
    Selct = False

    MosBusForm.Show modal

End Function
