VERSION 5.00
Object = "{FAEEE763-117E-101B-8933-08002B2F4F5A}#1.1#0"; "DBLIST32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form SATAlbForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parte de Trabajo S.A.T."
   ClientHeight    =   6645
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   11850
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6645
   ScaleWidth      =   11850
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   41
      Top             =   0
      Width           =   11850
      _ExtentX        =   20902
      _ExtentY        =   1535
      ButtonWidth     =   1693
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   10
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "prim"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntrBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Mant.S.A.T."
            Key             =   "ManSAT"
            Description     =   "ManSATBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "C.M./A.T."
            Key             =   "Maq"
            Description     =   "CMATBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SiguieBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "ulti"
            Description     =   "UltimoBoton"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Facturado"
      DataField       =   "Facturado"
      DataSource      =   "SATAlbData"
      Height          =   330
      Left            =   8820
      TabIndex        =   40
      Top             =   5985
      Width           =   1170
   End
   Begin VB.TextBox ProveedorBox 
      Height          =   285
      Left            =   840
      TabIndex        =   39
      Text            =   "No Especificado"
      Top             =   5880
      Visible         =   0   'False
      Width           =   1335
   End
   Begin SSDataWidgets_B.SSDBCombo CMBox 
      Bindings        =   "SATAlbForm.frx":0000
      DataField       =   "CodigoMaquina"
      DataSource      =   "SATAlbData"
      Height          =   255
      Left            =   6240
      TabIndex        =   38
      Top             =   1560
      Width           =   1335
      DataFieldList   =   "CodigoMaquina"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2328
      Columns(0).Caption=   "C�digo M�quina"
      Columns(0).Name =   "CodigoMaquina"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoMaquina"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   7488
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripcion"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Descripcion"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2355
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "CodigoMaquina"
   End
   Begin VB.Data SATAAlbData 
      Caption         =   "AAlbaran"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2160
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SATAAlbaran"
      Top             =   240
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.TextBox NOrdenBox 
      DataField       =   "NPetSAT"
      DataSource      =   "SATAlbData"
      Height          =   285
      Left            =   10560
      TabIndex        =   36
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Data SATAAlb2Data 
      Caption         =   "AAlbaran2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   4680
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data SATAlbData 
      Caption         =   "Albar�n"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2160
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM SATAlbaran WHERE NAlbaranSAT<>0"
      Top             =   0
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.TextBox NASATBox 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      DataField       =   "NAlbaranSAT"
      DataSource      =   "SATAlbData"
      Enabled         =   0   'False
      Height          =   285
      Left            =   1200
      TabIndex        =   5
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "TOTAL"
      Height          =   735
      Left            =   4560
      TabIndex        =   1
      Top             =   5760
      Width           =   2775
      Begin VB.TextBox TotalBox 
         DataField       =   "Total"
         DataSource      =   "SATAlbData"
         Enabled         =   0   'False
         Height          =   285
         Left            =   720
         TabIndex        =   2
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.TextBox SuNASAT 
      DataField       =   "NAlbaranP"
      DataSource      =   "SATAlbData"
      Height          =   285
      Left            =   4320
      TabIndex        =   0
      Top             =   1080
      Width           =   1575
   End
   Begin MSDBCtls.DBCombo SATBox 
      Bindings        =   "SATAlbForm.frx":0016
      DataField       =   "SAT"
      DataSource      =   "SATAlbData"
      Height          =   315
      Left            =   1080
      TabIndex        =   3
      Top             =   1560
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   556
      _Version        =   327681
      Style           =   2
      ListField       =   "Nombre"
      Text            =   ""
   End
   Begin SSCalendarWidgets_A.SSDateCombo FAlbaranBox 
      DataField       =   "FechaAlbaran"
      DataSource      =   "SATAlbData"
      Height          =   255
      Left            =   7560
      TabIndex        =   4
      Top             =   1080
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3615
      Left            =   120
      TabIndex        =   6
      Top             =   2040
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   6376
      _Version        =   327681
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Material Utilizado"
      TabPicture(0)   =   "SATAlbForm.frx":0028
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label14"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "label90"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label13"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label12"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label9"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "SSDBGrid1"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "ActualizarBoton"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "BorrarBoton"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "DescuentoBox"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "PUnitBox"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "CantBox"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "MaterialBox"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "STotalCBox"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "A�adirBoton"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "IdBox"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).ControlCount=   15
      TabCaption(1)   =   "Desplazamiento y Dietas"
      TabPicture(1)   =   "SATAlbForm.frx":0044
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label7"
      Tab(1).Control(1)=   "Label6"
      Tab(1).Control(2)=   "Label5"
      Tab(1).Control(3)=   "Line2"
      Tab(1).Control(4)=   "Line3"
      Tab(1).Control(5)=   "Label8"
      Tab(1).Control(6)=   "STotalHTDBox"
      Tab(1).Control(7)=   "HTBox"
      Tab(1).Control(8)=   "PUniHTBox"
      Tab(1).Control(9)=   "DietasBox"
      Tab(1).ControlCount=   10
      Begin VB.TextBox IdBox 
         Height          =   285
         Left            =   2040
         TabIndex        =   19
         Top             =   2640
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton A�adirBoton 
         Caption         =   "&A�adir"
         Height          =   375
         Left            =   9120
         TabIndex        =   17
         Top             =   3000
         Width           =   1095
      End
      Begin VB.TextBox STotalCBox 
         DataField       =   "SubTotalC"
         DataSource      =   "SATAlbData"
         Enabled         =   0   'False
         Height          =   285
         Left            =   9600
         TabIndex        =   18
         Top             =   2400
         Width           =   1215
      End
      Begin VB.TextBox MaterialBox 
         Height          =   285
         Left            =   120
         TabIndex        =   13
         Top             =   3000
         Width           =   4695
      End
      Begin VB.TextBox CantBox 
         Height          =   285
         Left            =   5040
         TabIndex        =   14
         Text            =   "0"
         Top             =   3000
         Width           =   855
      End
      Begin VB.TextBox PUnitBox 
         Height          =   285
         Left            =   6120
         TabIndex        =   15
         Text            =   "0"
         Top             =   3000
         Width           =   1215
      End
      Begin VB.TextBox DescuentoBox 
         Height          =   285
         Left            =   7560
         TabIndex        =   16
         Text            =   "0"
         Top             =   3000
         Width           =   495
      End
      Begin VB.CommandButton BorrarBoton 
         Caption         =   "&Borrar"
         Height          =   375
         Left            =   8400
         TabIndex        =   12
         Top             =   3000
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton ActualizarBoton 
         Caption         =   "A&ctualizar"
         Height          =   375
         Left            =   9840
         TabIndex        =   11
         Top             =   3000
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.TextBox DietasBox 
         DataField       =   "GastosDesp"
         DataSource      =   "SATAlbData"
         Height          =   285
         Left            =   -71400
         TabIndex        =   10
         Top             =   2040
         Width           =   1095
      End
      Begin VB.TextBox PUniHTBox 
         DataField       =   "PrecioUnitarioHT"
         DataSource      =   "SATAlbData"
         Height          =   285
         Left            =   -71400
         TabIndex        =   9
         Top             =   1560
         Width           =   1095
      End
      Begin VB.TextBox HTBox 
         DataField       =   "HorasTecnico"
         DataSource      =   "SATAlbData"
         Height          =   285
         Left            =   -71400
         TabIndex        =   8
         Top             =   1080
         Width           =   735
      End
      Begin VB.TextBox STotalHTDBox 
         DataField       =   "SubTotalHTD"
         DataSource      =   "SATAlbData"
         Height          =   285
         Left            =   -68040
         TabIndex        =   7
         Top             =   1800
         Width           =   1455
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Bindings        =   "SATAlbForm.frx":0060
         Height          =   1815
         Left            =   120
         TabIndex        =   20
         Top             =   600
         Width           =   10935
         ScrollBars      =   2
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   12383
         Columns(0).Caption=   "Art�culo"
         Columns(0).Name =   "Articulo"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "Articulo"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1376
         Columns(1).Caption=   "Cantidad"
         Columns(1).Name =   "Cantidad"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Cantidad"
         Columns(1).DataType=   3
         Columns(1).FieldLen=   256
         Columns(2).Width=   1720
         Columns(2).Caption=   "Pr.Unitario"
         Columns(2).Name =   "PrecioUnit"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "PrecioUnit"
         Columns(2).DataType=   6
         Columns(2).NumberFormat=   "CURRENCY"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1032
         Columns(3).Caption=   "Dto"
         Columns(3).Name =   "Descuento"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "Descuento"
         Columns(3).DataType=   3
         Columns(3).FieldLen=   256
         Columns(4).Width=   1693
         Columns(4).Caption=   "Parcial"
         Columns(4).Name =   "PParcial"
         Columns(4).CaptionAlignment=   0
         Columns(4).DataField=   "PParcial"
         Columns(4).DataType=   6
         Columns(4).NumberFormat=   "CURRENCY"
         Columns(4).FieldLen=   256
         UseDefaults     =   -1  'True
         _ExtentX        =   19288
         _ExtentY        =   3201
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label8 
         Caption         =   "SubTotal (Horas T�c. y Dietas):"
         Height          =   255
         Left            =   -69480
         TabIndex        =   29
         Top             =   1560
         Width           =   2295
      End
      Begin VB.Line Line3 
         X1              =   -69720
         X2              =   -66600
         Y1              =   1800
         Y2              =   1800
      End
      Begin VB.Line Line2 
         BorderWidth     =   3
         X1              =   -69720
         X2              =   -69720
         Y1              =   1080
         Y2              =   2400
      End
      Begin VB.Label Label9 
         Caption         =   "SubTotal (material):"
         Height          =   255
         Left            =   8160
         TabIndex        =   28
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label Label12 
         Caption         =   "Material"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   2760
         Width           =   735
      End
      Begin VB.Label Label13 
         Caption         =   "Cantidad"
         Height          =   255
         Left            =   5040
         TabIndex        =   26
         Top             =   2760
         Width           =   735
      End
      Begin VB.Label label90 
         Caption         =   "P.Unitario"
         Height          =   255
         Left            =   6120
         TabIndex        =   25
         Top             =   2760
         Width           =   855
      End
      Begin VB.Label Label14 
         Caption         =   "Dto"
         Height          =   255
         Left            =   7560
         TabIndex        =   24
         Top             =   2760
         Width           =   375
      End
      Begin VB.Label Label5 
         Caption         =   "Horas T�cnico:"
         Height          =   255
         Left            =   -72480
         TabIndex        =   23
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "Precio Unitario Hora T�c.:"
         Height          =   255
         Left            =   -73320
         TabIndex        =   22
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label Label7 
         Caption         =   "Gastos desplazamiento y dietas:"
         Height          =   255
         Left            =   -73800
         TabIndex        =   21
         Top             =   2040
         Width           =   2295
      End
   End
   Begin VB.Data MaquinaData 
      Caption         =   "Maquinas"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   7080
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT CodigoMaquina, Descripcion, SAT FROM InventMaquinas"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data SATData 
      Caption         =   "SAT"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT Nombre FROM SAT ORDER BY Nombre"
      Top             =   240
      Width           =   2055
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   5880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   10
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":0077
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":0391
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":06AB
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":09C5
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":0CDF
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":0FF9
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":1313
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":162D
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":1947
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATAlbForm.frx":1C61
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label10 
      Caption         =   "N�Pedido SAT:"
      Height          =   255
      Left            =   9360
      TabIndex        =   37
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "N� Parte:"
      Height          =   255
      Left            =   240
      TabIndex        =   35
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Fecha Parte:"
      Height          =   255
      Left            =   6360
      TabIndex        =   34
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "S.A.T."
      Height          =   255
      Left            =   360
      TabIndex        =   33
      Top             =   1560
      Width           =   615
   End
   Begin VB.Label Label4 
      Caption         =   "C�digo M�quina / �rea Trabajo:"
      Height          =   255
      Left            =   3840
      TabIndex        =   32
      Top             =   1560
      Width           =   2295
   End
   Begin VB.Label ModeloBox 
      BorderStyle     =   1  'Fixed Single
      DataField       =   "Descripcion"
      DataSource      =   "MaquinaData"
      Height          =   255
      Left            =   8040
      TabIndex        =   31
      Top             =   1560
      Width           =   3615
   End
   Begin VB.Line Line1 
      X1              =   7560
      X2              =   8040
      Y1              =   1680
      Y2              =   1680
   End
   Begin VB.Label Label15 
      Caption         =   "N�Parte SAT:"
      Height          =   255
      Left            =   3000
      TabIndex        =   30
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu ira 
         Caption         =   "Ir a"
         Shortcut        =   {F1}
      End
      Begin VB.Menu first 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sig 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu confi 
      Caption         =   "&Configuraci�n"
      Begin VB.Menu sate 
         Caption         =   "S.A.T."
      End
      Begin VB.Menu at 
         Caption         =   "�reas de Trabajo"
      End
   End
   Begin VB.Menu listardf 
      Caption         =   "&Listados"
      Begin VB.Menu listad 
         Caption         =   "Listado"
      End
   End
End
Attribute VB_Name = "SATAlbForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub ActualizarBoton_Click()
    
    Dim id As Integer, nf As Long, na As Integer, Ant As Long
On Error GoTo msgError
    id = IdBox.Text
    nf = NASATBox.Text
        
     With SATAAlbData
        .Recordset.FindFirst ("NAlbaranSAT=" & nf & " AND Linea=" & id)
        .Recordset.Edit
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("Articulo").Value = MaterialBox.Text
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .Recordset.Fields("PrecioUnit").Value = PUnitBox.Text
        .Recordset.Fields("Descuento").Value = DescuentoBox.Text
        .Recordset.Fields("PParcial").Value = (Val(PUnitBox.Text) * Val(CantBox.Text)) * (1 - (Val(DescuentoBox.Text) / 100))
        .UpdateRecord
    
    End With
        
    SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & NASATBox.Text & " ORDER BY Linea"
    SATAAlb2Data.Refresh
        
    With SATAlbData
        .Recordset.Edit
        .Recordset.Fields("SubTotalC").Value = CaTotalSATAlb(nf)
        .Recordset.Fields("Total").Value = ((.Recordset.Fields("SubTotalHTD").Value) + (.Recordset.Fields("SubTotalC").Value))
        .Recordset.Update
    End With
    
    A�adirBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    
    id = ClearFields()
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub add_Click()
    Dim nada As Integer
    nada = OtroSATAlbaran(Orden(0), Orden(1), Orden(2))

End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()

End Sub

Private Sub A�adirBoton_Click()
   
    Dim id As Integer, Total As Integer, Cantidad As Integer, PrecU As Integer, np As Integer, na As Integer, Ant As Integer
    Dim sub1 As Double, sub2 As Double
On Error GoTo msgError
    With SATAAlb2Data
    
        If .Recordset.BOF = False And .Recordset.EOF = False Then
            .Recordset.MoveLast
            id = .Recordset.Fields("Linea").Value
        Else
            id = 0
        End If
    
    End With
    
    
    With SATAAlbData
               
        id = id + 1
        
        .Recordset.AddNew
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NAlbaranSAT").Value = NASATBox.Text
        .Recordset.Fields("Articulo").Value = MaterialBox.Text
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .Recordset.Fields("PrecioUnit").Value = PUnitBox.Text
        .Recordset.Fields("Descuento").Value = DescuentoBox.Text
        .Recordset.Fields("PParcial").Value = (Val(PUnitBox.Text) * Val(CantBox.Text)) * (1 - (Val(DescuentoBox.Text) / 100))
        .UpdateRecord
        
    End With
    
    If STotalHTDBox.Text = "" Then
        STotalHTDBox.Text = "0"
    End If
    
    With SATAlbData
        .Recordset.Edit
        .Recordset.Fields("SubTotalC").Value = CaTotalSATAlb(Val(NASATBox.Text))
        sub1 = .Recordset.Fields("SubTotalC").Value
        sub2 = CDbl(STotalHTDBox.Text)
        TotalBox.Text = (sub1 + sub2)
        .Recordset.Update
    End With
    
    SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & NASATBox.Text & " ORDER BY Linea"
    SATAAlb2Data.Refresh
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub at_Click()
    MantInvForm.Show modal
    
End Sub

Private Sub BorrarBoton_Click()
    Dim id As Integer, nf As Long, na As Long, np As Long, ca As Long
On Error GoTo msgError
    id = IdBox.Text
    nf = NASATBox.Text
     
    With SATAAlbData
        .Recordset.FindFirst ("NAlbaranSAT=" & nf & " AND Linea=" & id)
        .Recordset.Delete
        .Refresh
    End With
    
    With SATAlbData
        .Recordset.Edit
        .Recordset.Fields("SubTotalC").Value = CaTotalSATAlb(Val(NASATBox.Text))
        .Recordset.Fields("Total").Value = ((.Recordset.Fields("SubTotalHTD").Value) + (.Recordset.Fields("SubTotalC").Value))
        .Recordset.Update
    End With
    
    A�adirBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    
    SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & NASATBox.Text & " ORDER BY Linea"
    SATAAlb2Data.Refresh
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Function Anterior()
    
    Dim nf As Integer
On Error GoTo msgError
    If SATAlbData.Recordset.RecordCount = 0 Then
        Exit Function
    End If
    
    Flag = True
    
    With SATAlbData
        .Recordset.MovePrevious
        If .Recordset.BOF = True Then
            .Recordset.MoveNext
        End If
        nf = .Recordset.Fields("NAlbaranSAT").Value
    End With
                
    SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & nf & " ORDER BY Linea"
    SATAAlb2Data.Refresh
    
    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Siguiente()
   
    Dim nf As Integer
On Error GoTo msgError
    If SATAlbData.Recordset.RecordCount = 0 Then
        Exit Function
    End If
    
    Flag = True
    
    With SATAlbData
        .Recordset.MoveNext
        If .Recordset.EOF = True Then
            .Recordset.MovePrevious
        End If
        nf = .Recordset.Fields("NAlbaranSAT").Value
    End With
    
    SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & nf & " ORDER BY Linea"
    SATAAlb2Data.Refresh
    
    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Sub CMBox_Change()
On Error GoTo msgError
    If CMBox.Text <> "" Then
        MaquinaData.Recordset.FindFirst ("CodigoMaquina=" & CMBox.Text)
        If MaquinaData.Recordset.NoMatch = True Then
            ModeloBox.Caption = "No existente"
        Else
        If MaquinaData.Recordset.Fields("SAT").Value <> "" Then
            SATBox.Text = MaquinaData.Recordset.Fields("SAT").Value
        End If
        End If
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CMBox_CloseUp()
On Error GoTo msgError
     MaquinaData.Recordset.FindFirst ("CodigoMaquina=" & CMBox.Text)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub DietasBox_Change()

    Dim res As Long

    res = ActualSub()

End Sub

Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar el parte de trabajo?", vbYesNo)
    If nada = vbYes Then
        SATAlbData.Recordset.Delete
        nada = Anterior()
    End If
    
End Sub

Private Sub exit_Click()
    Unload Me
    
End Sub

Private Sub first_Click()
    Dim nada As Integer
    nada = Primero()
End Sub

Private Sub Form_Load()
    
    
    Flag = True
    
    
End Sub

Private Sub HTBox_Change()
    
    Dim res As Long

    res = ActualSub()

End Sub

Private Sub ira_Click()

    Dim nada As Integer
    
    Set Objeto = SATAlbForm.SATAlbData
    Set Objeto2 = SATAlbForm.SATAAlb2Data
    
    Selct = True
    
    Display(0) = "NAlbaranSAT"
    DataCarac = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT="
    DataCarac2 = " ORDER BY Linea"
    
    IraForm.Show modal
    
End Sub

Private Sub last_Click()
    Dim nada As Integer
    nada = Ultimo()

End Sub

Private Sub listad_Click()
    Dim nada As Integer
    nada = ListaSATA()
End Sub

Private Sub PUniHTBox_Change()
    
    Dim res As Long

    res = ActualSub()

End Sub

Private Sub SATAAlbData_Error(DataErr As Integer, Response As Integer)

    MsgBox ("Falta alg�n dato o alguno de los existentes es err�neo")

End Sub


Private Sub sate_Click()
    SATForm.Show modal
    
End Sub

Private Sub sig_Click()
    Dim nada As Integer
    nada = Siguiente()

End Sub

Private Sub SSDBGrid1_DblClick()
    
    Dim id As Integer
    
    With SATAAlb2Data
        id = .Recordset.Fields("Linea").Value
        IdBox.Text = id
        MaterialBox.Text = .Recordset.Fields("Articulo").Value
        CantBox.Text = .Recordset.Fields("Cantidad").Value
        If .Recordset.Fields("PrecioUnit").Value <> 0 Then
            PUnitBox.Text = .Recordset.Fields("PrecioUnit").Value
        Else
            PUnitBox.Text = "0"
        End If
        
        If .Recordset.Fields("Descuento").Value <> 0 Then
            DescuentoBox.Text = .Recordset.Fields("Descuento").Value
        Else
            DescuentoBox.Text = "0"
        End If
        
    End With
    
    A�adirBoton.Visible = False
    BorrarBoton.Visible = True
    ActualizarBoton.Visible = True
    SSDBGrid1.Enabled = False
        
End Sub


Private Sub STotalCBox_Change()
    
    Dim res As Integer
    
    res = ActualSub()
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        If Orden(1) = 0 And Orden(2) <> 0 Then
          Load DBase
          With DBase.Data2
            .RecordSource = "SELECT Orden.CodigoMaquina FROM PedidoSAT,Orden WHERE Orden.NOrden=PedidoSAT.NOrden and PedidoSAT.NPetSAT=" & Orden(2)
            .Refresh
            Orden(1) = .Recordset.Fields(0)
          End With
          Unload DBase
          Set DBase = Nothing
        End If
        nada = OtroSATAlbaran(Orden(0), Orden(1), Orden(2))
        Orden(1) = 0
    Case "Lista"
        nada = ListaSATA()
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar el parte de trabajo?", vbYesNo)
        If nada = vbYes Then
            SATAlbData.Recordset.Delete
            nada = Anterior()
        End If
    Case "ManSAT"
        SATForm.Show modal
        SATForm.IndiceBox.Text = "1"
    Case "Maq"
        MantInvForm.Show modal
    Case "prim"
        nada = Primero()
    Case "ulti"
        nada = Ultimo()
        
    End Select
    

End Sub

Public Function OtroSATAlbaran(na As Long, CM As Long, npet As Long) As Integer

    Dim num As Integer
On Error GoTo msgError
    Flag = True
    
    num = 1 + UltAlbaran()
    
    
    With SATAlbData
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveLast
        End If
        .Recordset.AddNew
        .Recordset.Fields("NAlbaranSAT").Value = num
        .Recordset.Fields("FechaAlbaran") = Date
    End With
    
    NOrdenBox.Text = npet
    SATBox.Text = ProveedorBox.Text
    CMBox.Text = CM
    TotalBox.Text = 0
    
    With SATAlbData
        .Recordset.Fields("NPetSAT").Value = npet
        .Recordset.Fields("FechaAlbaran") = Date
        .Recordset.Update
        .Recordset.MoveLast
    End With
        
    SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & num & " ORDER BY Linea"
    SATAAlb2Data.Refresh

    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function


Private Function ClearFields()

    MaterialBox.Text = ""
    CantBox.Text = ""
    PUnitBox.Text = ""
    DescuentoBox.Text = ""
    
End Function


Private Function ActualSub()
    
    Dim a1 As Double, a2 As Double, a3 As Double, Ant As Double, ant2 As Double
    
    If Flag = True Then
        Exit Function
    End If
    
    If SATAlbData.Recordset.EOF = True Or SATAlbData.Recordset.BOF = True Then
        Exit Function
    End If
    
    If HTBox.Text <> "" Then
        a1 = CDbl(HTBox.Text)
    End If
    
    If PUniHTBox.Text <> "" Then
        a2 = CDbl(PUniHTBox.Text)
    End If
    
    If DietasBox.Text <> "" Then
        a3 = CDbl(DietasBox.Text)
    End If
    
    If STotalHTDBox.Text = "" Then
        STotalHTDBox.Text = "0"
    End If
    
    If STotalCBox.Text = "" Then
        STotalCBox.Text = "0"
    End If
    
    Ant = CDbl(STotalHTDBox.Text)
    ant2 = CDbl(STotalCBox.Text)
    STotalHTDBox.Text = (a1 * a2) + a3

    TotalBox.Text = CDbl(TotalBox.Text) - (Ant + ant2) + (CDbl(STotalHTDBox.Text) + CDbl(STotalCBox.Text))
        
    
End Function



Private Function ListaSATA()
    Dim nada As Integer
    
    nada = NuevaBusq()
    Display(0) = "NAlbaranSAT"
    Display(1) = "CodigoMaquina"
    Display(2) = "FechaAlbaran"
    Display(3) = "SAT"
    Display(4) = "NAlbaranP"
    Display(5) = "NPetSAT"
        
    TypeCampos(0) = 1
    TypeCampos(1) = 3
    TypeCampos(2) = 2
    TypeCampos(3) = 3
    TypeCampos(4) = 1
    TypeCampos(5) = 1
    TypeCampos(6) = 0
    TypeCampos(7) = 0
        
    For nada = 0 To 5
        campos(nada) = Display(nada)
    Next
        
    Lists(1, 0) = "CodigoMaquina, Descripcion"
    Lists(1, 1) = "CodigoMaquina"
    Lists(1, 2) = "InventMaquinas"
    Lists(3, 0) = "Nombre"
    Lists(3, 1) = "Nombre"
    Lists(3, 2) = "SAT"
        
    FRO = "SATAlbaran"
    SELE = "* "
    'SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3) & "," & campos(4) & "," & campos(5)
    WHER = ""
    ORD = "NAlbaranSAT"
        
    Set Objeto = SATAlbForm.SATAlbData
    Set Objeto2 = SATAlbForm.SATAAlb2Data
    
    Selct = True
    
    DataCarac = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT="
    DataCarac2 = " ORDER BY Linea"
        
    nada = Listado()
                   
End Function

Private Function Ultimo()
        
    Dim nada As Long
On Error GoTo msgError
    Flag = True
    
    SATAlbData.Recordset.MoveLast
        
    nada = SATAlbData.Recordset.Fields("NAlbaranSAT").Value
       
    SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & nada & " ORDER BY Linea"
    SATAAlb2Data.Refresh
    
    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Primero()
        
    Dim nada As Long
On Error GoTo msgError
    Flag = True
    
    SATAlbData.Recordset.MoveFirst
        
    nada = SATAlbData.Recordset.Fields("NAlbaranSAT").Value
       
    SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & nada & " ORDER BY Linea"
    SATAAlb2Data.Refresh
    
    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

