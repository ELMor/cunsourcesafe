VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher DLL Mantenimientos
' Coded by SIC Donosti
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
Const PRRepListaResp     As String = "PR1351" 'Lista de respuestas (FrmListaDeRespuestas)
Const PRRepEstAct        As String = "PR2111" 'Estados de una actuaci�n (FrmEstadoActuacion)
Const PRRepInter         As String = "PR2101" 'Interacciones act-act (FrmInteraccion)
Const PRRepTipoAct       As String = "PR2081" 'Tipos de actividad (FrmTipoActividad)
Const PRRepTipoResp      As String = "PR2071" 'Tipos de respuesta (FrmTiposDeRespuestas)
Const PRRepTipoMuest     As String = "PR2061" 'Tipos de muestra (FrmTiposDeMuestra)
Const PRRepTipoInter     As String = "PR2051" 'Tipos de interacci�n (FrmTiposIntraccion)
Const PRRepTipoGrupo     As String = "PR2021" 'Tipos de grupo (FrmTiposDeGrupo)
Const PRRepPregunta      As String = "PR1311" 'Preguntas(FrmPreguntas)
Const PRRepUrgencia      As String = "PR1511" 'Tipos de grupo (FrmDefUrgencias)


'Mantenimientos
Const PRWinPreguntas               As String = "PR0131"
Const PRWinListadeRespuestas       As String = "PR0135"
Const PRWinUrgencias               As String = "PR0151"
Const PRWinMenuManten              As String = "PR0201"
Const PRWinTipodeGrupo             As String = "PR0202"
Const PRWinTiposInteraccion        As String = "PR0205"
Const PRWinTiposdeMuestra          As String = "PR0206"
Const PRWinTiposdeRespuesta        As String = "PR0207"
Const PRWinTipoActividad           As String = "PR0208"
Const PRWinInteraccion             As String = "PR0210"
Const PRWinEstadoActuacion         As String = "PR0211"
Const PRWinEstadoMuestra           As String = "PR0233"
Const PRWincuestpropiomuestraM     As String = "PR0235"

' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  'MANTENIMIENTOS
  Select Case strProcess
    
    Case PRWinPreguntas
      Load frmPreguntas
      Call objsecurity.AddHelpContext(510)
      Call frmPreguntas.Show(vbModal)
      Unload frmPreguntas
      Call objsecurity.RemoveHelpContext
      Set frmPreguntas = Nothing
    Case PRWinListadeRespuestas
      Load frmListadeRespuestas
      Call objsecurity.AddHelpContext(508)
      Call frmListadeRespuestas.Show(vbModal)
      Unload frmListadeRespuestas
      Call objsecurity.RemoveHelpContext
      Set frmListadeRespuestas = Nothing
    Case PRWinUrgencias
      Load frmDefUrgencias
      Call objsecurity.AddHelpContext(5)
      Call frmDefUrgencias.Show(vbModal)
      Unload frmDefUrgencias
      Call objsecurity.RemoveHelpContext
      Set frmDefUrgencias = Nothing
    Case PRWinMenuManten
      Load frmmenumanten
      Call objsecurity.AddHelpContext(501)
      Call frmmenumanten.Show(vbModal)
      Unload frmmenumanten
      Call objsecurity.RemoveHelpContext
      Set frmmenumanten = Nothing
    Case PRWinTipodeGrupo
      Load frmtipodegrupo
      Call objsecurity.AddHelpContext(504)
      Call frmtipodegrupo.Show(vbModal)
      Unload frmtipodegrupo
      Call objsecurity.RemoveHelpContext
      Set frmtipodegrupo = Nothing
    Case PRWinTiposInteraccion
      Load frmtiposinteraccion
      Call objsecurity.AddHelpContext(506)
      Call frmtiposinteraccion.Show(vbModal)
      Unload frmtiposinteraccion
      Call objsecurity.RemoveHelpContext
      Set frmtiposinteraccion = Nothing
    Case PRWinTiposdeMuestra
      Load frmtiposdemuestra
      Call objsecurity.AddHelpContext(503)
      Call frmtiposdemuestra.Show(vbModal)
      Unload frmtiposdemuestra
      Call objsecurity.RemoveHelpContext
      Set frmtiposdemuestra = Nothing
    Case PRWinTiposdeRespuesta
      Load frmtiposderespuesta
      Call objsecurity.AddHelpContext(505)
      Call frmtiposderespuesta.Show(vbModal)
      Unload frmtiposderespuesta
      Call objsecurity.RemoveHelpContext
      Set frmtiposderespuesta = Nothing
    Case PRWinTipoActividad
      Load frmtipoactividad
      Call objsecurity.AddHelpContext(502)
      Call frmtipoactividad.Show(vbModal)
      Unload frmtipoactividad
      Call objsecurity.RemoveHelpContext
      Set frmtipoactividad = Nothing
    Case PRWinInteraccion
      Load frminteraccion
      Call objsecurity.AddHelpContext(506)
      Call frminteraccion.Show(vbModal)
      Unload frminteraccion
      Call objsecurity.RemoveHelpContext
      Set frminteraccion = Nothing
    Case PRWinEstadoActuacion
      Load frmestadoactuacion
      Call objsecurity.AddHelpContext(509)
      Call frmestadoactuacion.Show(vbModal)
      Unload frmestadoactuacion
      Call objsecurity.RemoveHelpContext
      Set frmestadoactuacion = Nothing
    Case PRWinEstadoMuestra
      Load frmestadomuestra
      'Call objsecurity.AddHelpContext(509)
      Call frmestadomuestra.Show(vbModal)
      Unload frmestadomuestra
      'Call objsecurity.RemoveHelpContext
      Set frmestadomuestra = Nothing
    Case PRWincuestpropiomuestraM
      Load frmcuestpropiomuestraM
      'Call objsecurity.AddHelpContext(538)
      Call frmcuestpropiomuestraM.Show(vbModal)
      Unload frmcuestpropiomuestraM
      'Call objsecurity.RemoveHelpContext
      Set frmcuestpropiomuestraM = Nothing
  End Select
  Call Err.Clear

End Function

Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 19 procesos
  ReDim aProcess(1 To 22, 1 To 4) As Variant
      
  ' VENTANAS
  ' MANTENIMIENTOS
  aProcess(1, 1) = PRWinPreguntas
  aProcess(1, 2) = "Definici�n de Preguntas"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow
      
  aProcess(2, 1) = PRWinListadeRespuestas
  aProcess(2, 2) = "Lista de Respuestas"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
      
  aProcess(3, 1) = PRWinUrgencias
  aProcess(3, 2) = "Urgencias"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = PRWinMenuManten
  aProcess(4, 2) = "Mantenimiento de Tablas"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = PRWinTipodeGrupo
  aProcess(5, 2) = "Tipos de Grupo"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = PRWinTiposInteraccion
  aProcess(6, 2) = "Tipos de Interacci�n"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = PRWinTiposdeMuestra
  aProcess(7, 2) = "Tipos de Muestra"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = PRWinTiposdeRespuesta
  aProcess(8, 2) = "Tipos de Respuesta"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinTipoActividad
  aProcess(9, 2) = "Tipos de Actividad"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinInteraccion
  aProcess(10, 2) = "Interacciones"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow
    
  aProcess(11, 1) = PRWinEstadoActuacion
  aProcess(11, 2) = "Estados de la Actuaci�n"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = PRWinEstadoMuestra
  aProcess(12, 2) = "Estados Actuaci�n con Muestra"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWincuestpropiomuestraM
  aProcess(13, 2) = "Cuestionario propio muestra"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = PRRepEstAct
  aProcess(14, 2) = "Estados de una actuaci�n"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeReport
  
  aProcess(15, 1) = PRRepInter
  aProcess(15, 2) = "Interacciones actuaci�n-actuaci�n"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeReport
  
  aProcess(16, 1) = PRRepTipoAct
  aProcess(16, 2) = "Tipos de actividad"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeReport
  
  aProcess(17, 1) = PRRepTipoResp
  aProcess(17, 2) = "Tipos de respuesta"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeReport
  
  aProcess(18, 1) = PRRepTipoMuest
  aProcess(18, 2) = "Tipos de muestra"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeReport
  
  aProcess(19, 1) = PRRepTipoInter
  aProcess(19, 2) = "Tipos de interacci�n"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeReport
  
  aProcess(20, 1) = PRRepTipoGrupo
  aProcess(20, 2) = "Tipos de grupo"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeReport
  
  aProcess(21, 1) = PRRepListaResp
  aProcess(21, 2) = "Lista de respuestas"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeReport

  aProcess(22, 1) = PRRepPregunta
  aProcess(22, 2) = "Preguntas"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeReport
  
  aProcess(23, 1) = PRRepUrgencia
  aProcess(23, 2) = "Urgencias"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeReport

End Sub

