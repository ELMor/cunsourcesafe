Attribute VB_Name = "REA003"
'************************************************************************
'* PROYECTO: PRUEBAS  (PETICI�N DE ACTUACIONES)                         *
'* NOMBRE:                                                              *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 20 DE OCTUBRE DE 1997                                         *
'* DESCRIPCI�N: Funciones y Procedimientos para obtener las fechas      *
'*               de planificaci�n                                       *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Public Sub insertar(planificada As Date)
Dim strmuestra As String
Dim rstmuestra As rdoResultset
Dim strsql As String
Dim rsta As rdoResultset
Dim strsql1 As String
Dim rstA1 As rdoResultset
'Dim strsql2 As String
'Dim rstA2 As rdoResultset
Dim strsql3 As String
Dim rstA3 As rdoResultset
Dim actplanif As Long 'c�digo de actuaci�n planificada
'para insertar en PR0400
Dim strinsert As String
Dim dpto As Integer
Dim act As Integer
Dim persona As Integer
Dim entidad As Integer
Dim tipo As Integer
'para insertar en PR0700
'Dim strinsert07 As String
Dim numfase As Long
Dim desfase As String
Dim numminocupac As Integer
Dim fasepre As Long
Dim min As Integer
Dim max As Integer
Dim hab As Integer
Dim ini As Integer
'para insertar en PR1000
'Dim strinsert1000 As String
'Dim rstrec As rdoResultset
'Dim strRec As String

Dim quedia As Integer
Dim quemes As Integer
Dim queano As Integer
Dim texto As String

Dim strasis As String
Dim rstasis As rdoResultset
Dim rstdoc As rdoResultset
Dim strdoc As String


quedia = Day(planificada)
quemes = Month(planificada)
queano = Year(planificada)
texto = quedia & "/" & quemes & "/" & queano



' generaci�n autom�tica del c�digo
strsql = "SELECT PR04NUMACTPLAN_SEQUENCE.nextval FROM dual"
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
actplanif = rsta.rdoColumns(0).Value
'se busca en PR0300 (para la actuaci�n pedida):departamento,persona,actuaci�n,
'tipo econ�mico,entidad
strsql1 = "SELECT ad02coddpto,pr01codactuacion,ci21codpersona,ci32codtipecon," & _
         "ci13codentidad,pr48codurgencia,pr03indintcientif,ad02coddpto_crg " & _
         "FROM PR0300 WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0)
Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)

strasis = "SELECT AD07CODPROCESO,AD01CODASISTENCI FROM PR0800 WHERE " & _
          "PR03NUMACTPEDI=" & frmdatosactpedida.txtText1(0) & " AND " & _
          "PR08NUMSECUENCIA=1"
Set rstasis = objApp.rdoConnect.OpenResultset(strasis)

strdoc = "SELECT pr01indreqdoc FROM PR0100 WHERE PR01CODACTUACION=" & rstA1.rdoColumns("PR01CODACTUACION").Value
Set rstdoc = objApp.rdoConnect.OpenResultset(strdoc)

'INSERTAR EN PR0400 ACTUACION_PLANIFICADA
'se mira si la actuaci�n tiene muestras o no. Si tiene muestras el estado(pr37codestado)
'ser� igual a 9. Si no tiene muestras ser� 1.
strmuestra = "SELECT COUNT(*) FROM PR2500 WHERE PR01CODACTUACION=" & rstA1.rdoColumns(1).Value
Set rstmuestra = objApp.rdoConnect.OpenResultset(strmuestra)
If rstmuestra.rdoColumns(0).Value = 0 Then 'la actuaci�n no tiene muestras
strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme,im01numdoc,pr04indpadre," & _
            "pr48codurgencia,pr04indintcientif,ad02coddpto_crg,ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "to_date('" & texto & "','DD/MM/YYYY')" & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null" & ","
Else 'la actuaci�n tiene muestras
strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado,pr56codestmues," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme,im01numdoc,pr04indpadre," & _
            "pr48codurgencia,pr04indintcientif,ad02coddpto_crg,ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1,1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "to_date('" & texto & "','DD/MM/YYYY')" & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null" & ","
End If
rstmuestra.Close
Set rstmuestra = Nothing

If IsNull(rstA1.rdoColumns("pr48codurgencia").Value) Then
  strinsert = strinsert & "null" & ","
Else
  strinsert = strinsert & rstA1.rdoColumns("pr48codurgencia").Value & ","
End If
If IsNull(rstA1.rdoColumns("pr03indintcientif").Value) Then
  strinsert = strinsert & "null" & ","
Else
  strinsert = strinsert & rstA1.rdoColumns("pr03indintcientif").Value & ","
End If
If IsNull(rstA1.rdoColumns("ad02coddpto_crg").Value) Then
  strinsert = strinsert & "null" & ","
Else
  strinsert = strinsert & rstA1.rdoColumns("ad02coddpto_crg").Value & ","
End If
If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
End If
If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
End If
If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
End If
On Error GoTo Err_Ejecutar
objApp.rdoConnect.Execute strinsert, 64
'**********************************************************************************
' Se inserta en la PR0700 y en la PR1000 al Realizar                        jcr 6/8/98
'**********************************************************************************
''insertar en pr0700 FASE_REALIZADA
'strsql2 = "SELECT * FROM PR0600 WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0)
'Set rstA2 = objApp.rdoConnect.OpenResultset(strsql2)
'
'While Not rstA2.EOF
'strinsert07 = "INSERT INTO PR0700 " & _
'                 "(pr04numactplan,pr07numfase,pr07desfase,pr07numminocupac," & _
'                "pr07numfase_pre,pr07numminfpre,pr07nummaxfpre," & _
'                "pr07indhabnatu,pr07indinifin) " & _
'                " VALUES(" & actplanif & "," & rstA2.rdoColumns("pr06numfase").Value & "," & "'" & rstA2.rdoColumns("pr06desfase").Value & "'" & ","
'
'If IsNull(rstA2.rdoColumns("pr06numminocupac").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numminocupac").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06numfase_pre").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numfase_pre").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06numminfpre").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numminfpre").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06nummaxfpre").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06nummaxfpre").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06indhabnatu").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06indhabnatu").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06indinifin").Value) Then
'  strinsert07 = strinsert07 & "null" & ")"
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06indinifin").Value & ")"
'End If

'  On Error GoTo Err_Ejecutar
'  objApp.rdoConnect.Execute strinsert07, 64
'  rstA2.MoveNext
'Wend
''insertar en pr1000 RECURSO CONSUMIDO
'strRec = "SELECT * FROM PR1400 WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0)
'Set rstrec = objApp.rdoConnect.OpenResultset(strRec)
'While Not rstrec.EOF
'strinsert1000 = "INSERT INTO PR1000 " & _
'              "(pr04numactplan,pr07numfase,pr10numnecesid,ag11codrecurso," & _
'              "pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
'              "VALUES (" & actplanif & "," & _
'              rstrec.rdoColumns("pr06numfase").Value & "," & _
'              rstrec.rdoColumns("pr14numnecesid").Value & ","
'If IsNull(rstrec.rdoColumns("AG11CODRECURSO").Value) Then
'  strinsert1000 = strinsert1000 & "null" & ","
'Else
' strinsert1000 = strinsert1000 & rstrec.rdoColumns("AG11CODRECURSO").Value & ","
'End If
'If IsNull(rstrec.rdoColumns("pr14numunirec").Value) Then
'  strinsert1000 = strinsert1000 & "null" & ","
'Else
' strinsert1000 = strinsert1000 & rstrec.rdoColumns("pr14numunirec").Value & ","
'End If
'If IsNull(rstrec.rdoColumns("pr14numminocu").Value) Then
'  strinsert1000 = strinsert1000 & "null" & ","
'Else
' strinsert1000 = strinsert1000 & rstrec.rdoColumns("pr14numminocu").Value & ","
'End If
'If IsNull(rstrec.rdoColumns("AG11CODRECURSO").Value) Then
'  strinsert1000 = strinsert1000 & "null" & ")"
'Else
' strinsert1000 = strinsert1000 & rstrec.rdoColumns("AG11CODRECURSO").Value & ")"
'End If
'
'On Error GoTo Err_Ejecutar
'objApp.rdoConnect.Execute strinsert1000, 64
'rstrec.MoveNext
'Wend


rsta.Close
Set rsta = Nothing
rstA1.Close
Set rstA1 = Nothing
'rstA2.Close
'Set rstA2 = Nothing
'rstrec.Close
'Set rstrec = Nothing
rstasis.Close
Set rstasis = Nothing
rstdoc.Close
Set rstdoc = Nothing

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub



Public Sub borrar(actpedida As Long)

Dim strsql1 As String
Dim rstA1 As rdoResultset
Dim strinsert As String
Dim strdelete As String
Dim strupdate As String
Dim strFecha As String
Dim rs As rdoResultset




On Error GoTo Err_Ejecutar

'se coge la fecha y hora del sistema
Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY hh24:mi:ss')from dual")
strFecha = rs.rdoColumns(0)

'se mira si la actuacion pedida ya habia generado actuaciones planificadas
strsql1 = "select count(*) from PR0400 where pr03numactpedi=" & actpedida & _
          " and pr37codestado=1"
Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
If (rstA1.rdoColumns(0).Value <> 0) Then
'se recorren las actuaciones planificadas'

   '*********************************************************************************
   '  Se inserta esta estructura al Realizar                              jcr 6/8/98
   '*********************************************************************************
   
  ''se vuelca el contenido de PR1000 en PR4500
  ' strsql1 = "select PR04NUMACTPLAN from PR0400 where pr03numactpedi=" & actpedida & _
  '           " and pr37codestado=1"
  ' Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
  '
  '   Call volcado(actpedida, rstA1.rdoColumns(0).Value)
     
   
   'se borra de pr1000
   'strsql1 = "select PR04NUMACTPLAN from PR0400 where pr03numactpedi=" & actpedida & _
   '          " and pr37codestado=1"
   'Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
   'While Not rstA1.EOF
   '  strdelete = "DELETE FROM PR1000 where pr04numactplan=" & rstA1.rdoColumns(0).Value
   '  objApp.rdoConnect.Execute strdelete, 64
   '  objApp.rdoConnect.Execute "Commit", 64
   '  rstA1.MoveNext
   'Wend
   ''se borra de pr0700
   'strsql1 = "select PR04NUMACTPLAN from PR0400 where pr03numactpedi=" & actpedida & _
   '          " and pr37codestado=1"
   'Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
   'While Not rstA1.EOF
   '  strdelete = "DELETE FROM PR0700 where pr04numactplan=" & rstA1.rdoColumns(0).Value
   '  objApp.rdoConnect.Execute strdelete, 64
   '  objApp.rdoConnect.Execute "Commit", 64
   '  rstA1.MoveNext
   'Wend
''   'se modifica PR0400 poniendo el estado=cancelada
''   strupdate = "UPDATE PR0400 " & _
''           "SET pr37codestado=6" & _
''           " where pr03numactpedi=" & actpedida
   strupdate = "delete PR0400" & _
           " where pr03numactpedi=" & actpedida
  objApp.rdoConnect.Execute strupdate, 64
   objApp.rdoConnect.Execute "Commit", 64
End If

rstA1.Close
Set rstA1 = Nothing
rs.Close
Set rs = Nothing

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

   '*********************************************************************************
   '  Se inserta PR1000 al Realizar                                       jcr 6/8/98
   '*********************************************************************************

'Public Sub volcado(actpedida As Long, actplan As Long)
''procedimiento para volcar la informaci�n guardada en PR1000 referente a los recursos
''seleccionados para cada actuaci�n.
''en caso de variar la planificaci�n se generar�an nuevos c�digos de actuaciones
''planificadas por lo que hay que cambiar lo que hay en PR1000, por eso antes se
''vuelca en una tabla auxiliar para no perder la informaci�n que servir� despu�s
''para crear las nuevas tuplas.
'
'Dim strsql As String
'Dim rsta As rdoResultset
'Dim fase As Integer
'Dim necesidad As Integer
'Dim recurso As Integer
'Dim unirec As Integer
'Dim ocu As Integer
'Dim recpre As Integer
'Dim strinsercion As String
'
'
'On Error GoTo Err_Ejecutar
'strsql = "SELECT count(*) FROM PR1000 where pr04numactplan=" & actplan
'Set rsta = objApp.rdoConnect.OpenResultset(strsql)
'If (rsta.rdoColumns(0).Value > 0) Then
'strsql = "SELECT distinct pr07numfase,pr10numnecesid,ag11codrecurso," & _
'                         "pr10numunirec,pr10numminocurec,ag11codrecurso_pre " & _
'          "FROM PR1000 where pr04numactplan=" & actplan
'Set rsta = objApp.rdoConnect.OpenResultset(strsql)
'
'While Not rsta.EOF
'  strinsercion = "INSERT INTO PR4500 " & _
'             "(pr03numactpedi,pr04numactplan,pr07numfase,pr10numnecesid," & _
'             "ag11codrecurso,pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
'             " VALUES(" & actpedida & "," & actplan & "," & _
'             rsta.rdoColumns(0).Value & ","
'
'  If IsNull(rsta.rdoColumns(1).Value) Then
'    strinsercion = strinsercion & "null" & ","
'  Else
'    strinsercion = strinsercion & rsta.rdoColumns(1).Value & ","
'  End If
'  If IsNull(rsta.rdoColumns(2).Value) Then
'    strinsercion = strinsercion & "null" & ","
'  Else
'    strinsercion = strinsercion & rsta.rdoColumns(2).Value & ","
'  End If
'  If IsNull(rsta.rdoColumns(3).Value) Then
'    strinsercion = strinsercion & "null" & ","
'  Else
'    strinsercion = strinsercion & rsta.rdoColumns(3).Value & ","
'  End If
'  If IsNull(rsta.rdoColumns(4).Value) Then
'    strinsercion = strinsercion & "null" & ","
'  Else
'    strinsercion = strinsercion & rsta.rdoColumns(4).Value & ","
'  End If
'  If IsNull(rsta.rdoColumns(5).Value) Then
'    strinsercion = strinsercion & "null" & ")"
'  Else
'    strinsercion = strinsercion & rsta.rdoColumns(5).Value & ")"
'  End If
'
'
'  objApp.rdoConnect.Execute strinsercion, 64
'  objApp.rdoConnect.Execute "Commit", 64
'  rsta.MoveNext
'Wend
'rsta.Close
'Set rsta = Nothing
'End If
'Exit Sub
'Err_Ejecutar:
'  MsgBox "Error: " & Err.Number & " " & Err.Description
'  Exit Sub
'End Sub

'Public Sub sacar_volcado(actpedida As Long)
''procedimiento que sirve para recoger la informaci�n de la tabla auxiliar donde se ha
''guardado la informaci�n de PR1000 referente a los recursos seleccionados para una
''actuaci�n.

'Dim rsta As rdoResultset
'Dim strsql As String
'Dim rstA1 As rdoResultset
'Dim strsql1 As String
'Dim strinsert As String
'Dim strdelete As String

'On Error GoTo Err_Ejecutar
'strsql = "SELECT * FROM PR4500 WHERE pr03numactpedi=" & actpedida
'Set rsta = objApp.rdoConnect.OpenResultset(strsql)
'While Not rsta.EOF 'mientras haya tuplas en la tabla auxiliar
'  'se recorren las nuevas actuaciones planificadas
'  strsql1 = "SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & actpedida & _
'             " and pr37codestado=1"
'  Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
'  While Not rstA1.EOF
'
'        strinsert = "INSERT INTO PR1000 " & _
'                   "(pr04numactplan,pr07numfase,pr10numnecesid,ag11codrecurso," & _
'                   "pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
'                   " VALUES(" & rstA1.rdoColumns(0).Value & "," & _
'                   rsta.rdoColumns("pr07numfase").Value & "," & _
'                   rsta.rdoColumns("pr10numnecesid").Value & ","
'        If IsNull(rsta.rdoColumns("ag11codrecurso").Value) Then
'          strinsert = strinsert & "null" & ","
'        Else
'          strinsert = strinsert & rsta.rdoColumns("ag11codrecurso").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("pr10numunirec").Value) Then
'          strinsert = strinsert & "null" & ","
'        Else
'          strinsert = strinsert & rsta.rdoColumns("pr10numunirec").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("pr10numminocurec").Value) Then
'          strinsert = strinsert & "null" & ","
'        Else
'          strinsert = strinsert & rsta.rdoColumns("pr10numminocurec").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("ag11codrecurso_pre").Value) Then
'          strinsert = strinsert & "null" & ")"
'        Else
'          strinsert = strinsert & rsta.rdoColumns("ag11codrecurso_pre").Value & ")"
'        End If
'
'        objApp.rdoConnect.Execute strinsert, 64
'        objApp.rdoConnect.Execute "Commit", 64
'        rstA1.MoveNext
'  Wend
'  rstA1.Close
'  Set rstA1 = Nothing
'  rsta.MoveNext
'Wend
''una vez recuperada la informaci�n de la tabla auxiliar y volcada en PR1000 se
''borran las tuplas de la tabla auxiliar
'strdelete = "DELETE FROM PR4500 where pr03numactpedi=" & actpedida
'   objApp.rdoConnect.Execute strdelete, 64
'   objApp.rdoConnect.Execute "Commit", 64
'
'rsta.Close
'Set rsta = Nothing
'
'Exit Sub
'Err_Ejecutar:
'  MsgBox "Error: " & Err.Number & " " & Err.Description
'  Exit Sub
'End Sub


Public Sub insertarpreferencia(planificada As Date)
Dim rstmuestra As rdoResultset
Dim strmuestra As String
Dim strsql As String
Dim rsta As rdoResultset
Dim strsql1 As String
Dim rstA1 As rdoResultset
'Dim strsql2 As String
'Dim rstA2 As rdoResultset
Dim strsql3 As String
Dim rstA3 As rdoResultset

Dim actplanif As Long 'c�digo de actuaci�n planificada
''para insertar en PR0400
Dim strinsert As String
Dim dpto As Integer
Dim act As Integer
Dim persona As Integer
Dim entidad As Integer
Dim tipo As Integer
''para insertar en PR0700
'Dim strinsert07 As String
Dim numfase As Long
Dim desfase As String
Dim numminocupac As Integer
Dim fasepre As Long
Dim min As Integer
Dim max As Integer
Dim hab As Integer
Dim ini As Integer
'insertar en PR1000
'Dim strinsert1000 As String
'Dim rstrec As rdoResultset
'Dim strRec As String

Dim quedia As Integer
Dim quemes As Integer
Dim queano As Integer
Dim texto As String

Dim rstasis As rdoResultset
Dim strasis As String
Dim rstdoc As rdoResultset
Dim strdoc As String

  quedia = Day(planificada)
  quemes = Month(planificada)
  queano = Year(planificada)
  texto = quedia & "/" & quemes & "/" & queano


' generaci�n autom�tica del c�digo
strsql = "SELECT PR04NUMACTPLAN_SEQUENCE.nextval FROM dual"
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
actplanif = rsta.rdoColumns(0).Value
'se busca en PR0300 (para la actuaci�n pedida):departamento,persona,actuaci�n,
'tipo econ�mico,entidad
strsql1 = "SELECT ad02coddpto,pr01codactuacion,ci21codpersona,ci32codtipecon," & _
         "ci13codentidad,pr48codurgencia,pr03indintcientif,ad02coddpto_crg " & _
         "FROM PR0300 WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0)
Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)

strasis = "SELECT AD07CODPROCESO,AD01CODASISTENCI FROM PR0800 WHERE " & _
          "PR03NUMACTPEDI=" & frmdatosactpedida.txtText1(0) & " AND " & _
          "PR08NUMSECUENCIA=1"
Set rstasis = objApp.rdoConnect.OpenResultset(strasis)

strdoc = "SELECT pr01indreqdoc FROM PR0100 WHERE PR01CODACTUACION=" & _
       rstA1.rdoColumns("pr01codactuacion").Value
Set rstdoc = objApp.rdoConnect.OpenResultset(strdoc)
  

'INSERTAR EN PR0400 ACTUACION_PLANIFICADA
'If frmdatosactpedida.dtcfecha = "" Then
 If frmdatosactpedida.txtText1(23).Text = "" Then


 'se mira si la actuaci�n tiene muestras o no. Si tiene muestras el estado(pr37codestado)
 'ser� igual a 9. Si no tiene muestras ser� 1.
 strmuestra = "SELECT COUNT(*) FROM PR2500 WHERE PR01CODACTUACION=" & rstA1.rdoColumns(1).Value
 Set rstmuestra = objApp.rdoConnect.OpenResultset(strmuestra)
 If rstmuestra.rdoColumns(0).Value = 0 Then 'la actuaci�n no tiene muestras

  strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre," & _
            "pr48codurgencia,pr04indintcientif,ad02coddpto_crg," & _
            "ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null,null" & ","
 Else 'la actuaci�n tiene muestras
  strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado,pr56codestmues," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre," & _
            "pr48codurgencia,pr04indintcientif,ad02coddpto_crg," & _
            "ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1,1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null,null" & ","
  End If
  rstmuestra.Close
  Set rstmuestra = Nothing
  
    If IsNull(rstA1.rdoColumns("pr48codurgencia").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("pr48codurgencia").Value & ","
    End If
    If IsNull(rstA1.rdoColumns("pr03indintcientif").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("pr03indintcientif").Value & ","
    End If
    If IsNull(rstA1.rdoColumns("ad02coddpto_crg").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("ad02coddpto_crg").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
    End If
    If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
    Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
    End If
            
Else
  If (frmdatosactpedida.txthora.Text <> "" And frmdatosactpedida.txtminuto.Text <> "") Then
   texto = texto & " " & frmdatosactpedida.txthora.Text & ":" & frmdatosactpedida.txtminuto.Text
   'se mira si la actuaci�n tiene muestras o no. Si tiene muestras el estado(pr37codestado)
   'ser� igual a 9. Si no tiene muestras ser� 1.
   strmuestra = "SELECT COUNT(*) FROM PR2500 WHERE PR01CODACTUACION=" & rstA1.rdoColumns(1).Value
   Set rstmuestra = objApp.rdoConnect.OpenResultset(strmuestra)
   If rstmuestra.rdoColumns(0).Value = 0 Then 'la actuaci�n no tiene muestras
      strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre," & _
            "pr48codurgencia,pr04indintcientif,ad02coddpto_crg," & _
            "ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1," & _
            frmdatosactpedida.txtText1(0) & "," & _
             "to_date('" & texto & "','DD/MM/YYYY hh24:mi')" & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null" & ","
    Else 'la actuaci�n tiene muestras
      strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado,pr56codestmues," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre," & _
            "pr48codurgencia,pr04indintcientif,ad02coddpto_crg," & _
            "ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1,1," & _
            frmdatosactpedida.txtText1(0) & "," & _
             "to_date('" & texto & "','DD/MM/YYYY hh24:mi')" & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null" & ","
      End If
      rstmuestra.Close
      Set rstmuestra = Nothing
      
    
    If IsNull(rstA1.rdoColumns("pr48codurgencia").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("pr48codurgencia").Value & ","
    End If
    If IsNull(rstA1.rdoColumns("pr03indintcientif").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("pr03indintcientif").Value & ","
    End If
    If IsNull(rstA1.rdoColumns("ad02coddpto_crg").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("ad02coddpto_crg").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
    End If
    If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
    Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
    End If
            
  Else
   'se mira si la actuaci�n tiene muestras o no. Si tiene muestras el estado(pr37codestado)
   'ser� igual a 9. Si no tiene muestras ser� 1.
   strmuestra = "SELECT COUNT(*) FROM PR2500 WHERE PR01CODACTUACION=" & rstA1.rdoColumns(1).Value
   Set rstmuestra = objApp.rdoConnect.OpenResultset(strmuestra)
   If rstmuestra.rdoColumns(0).Value = 0 Then 'la actuaci�n no tiene muestras
    strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre," & _
            "pr48codurgencia,pr04indintcientif,ad02coddpto_crg," & _
            "ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "to_date('" & texto & "','DD/MM/YYYY')" & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null" & ","
   Else 'la actuaci�n tiene muestras
    strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado,pr56codestmues," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre," & _
            "pr48codurgencia,pr04indintcientif,ad02coddpto_crg," & _
            "ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1,1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "to_date('" & texto & "','DD/MM/YYYY')" & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null" & ","
    End If
    rstmuestra.Close
    Set rstmuestra = Nothing
    
    If IsNull(rstA1.rdoColumns("pr48codurgencia").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("pr48codurgencia").Value & ","
    End If
    If IsNull(rstA1.rdoColumns("pr03indintcientif").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("pr03indintcientif").Value & ","
    End If
    If IsNull(rstA1.rdoColumns("ad02coddpto_crg").Value) Then
      strinsert = strinsert & "null" & ","
    Else
      strinsert = strinsert & rstA1.rdoColumns("ad02coddpto_crg").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
    End If
    If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
    Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
    End If
    
    
  End If
End If
On Error GoTo Err_Ejecutar
objApp.rdoConnect.Execute strinsert, 64
'**********************************************************************************
' Se inserta en la PR0700 y en la PR1000 al Realizar                        jcr 6/8/98
'**********************************************************************************

''insertar en pr0700 FASE_REALIZADA
'strsql2 = "SELECT * FROM PR0600 WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0)
'Set rstA2 = objApp.rdoConnect.OpenResultset(strsql2)
'
'While Not rstA2.EOF
'strinsert07 = "INSERT INTO PR0700 " & _
'                 "(pr04numactplan,pr07numfase,pr07desfase,pr07numminocupac," & _
'                "pr07numfase_pre,pr07numminfpre,pr07nummaxfpre," & _
'                "pr07indhabnatu,pr07indinifin) " & _
'                " VALUES(" & actplanif & "," & rstA2.rdoColumns("pr06numfase").Value & "," & "'" & rstA2.rdoColumns("pr06desfase").Value & "'" & ","
'
'If IsNull(rstA2.rdoColumns("pr06numminocupac").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numminocupac").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06numfase_pre").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numfase_pre").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06numminfpre").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numminfpre").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06nummaxfpre").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06nummaxfpre").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06indhabnatu").Value) Then
'  strinsert07 = strinsert07 & "null" & ","
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06indhabnatu").Value & ","
'End If
'If IsNull(rstA2.rdoColumns("pr06indinifin").Value) Then
'  strinsert07 = strinsert07 & "null" & ")"
'Else
'  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06indinifin").Value & ")"
'End If
'
'  On Error GoTo Err_Ejecutar
'  objApp.rdoConnect.Execute strinsert07, 64
'  rstA2.MoveNext
'Wend
''insertar en pr1000 RECURSO CONSUMIDO
'strRec = "SELECT * FROM PR1400 WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0)
'Set rstrec = objApp.rdoConnect.OpenResultset(strRec)
'While Not rstrec.EOF
'strinsert1000 = "INSERT INTO PR1000 " & _
'              "(pr04numactplan,pr07numfase,pr10numnecesid,ag11codrecurso," & _
'              "pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
'              "VALUES (" & actplanif & "," & _
'              rstrec.rdoColumns("pr06numfase").Value & "," & _
'              rstrec.rdoColumns("pr14numnecesid").Value & ","
'If IsNull(rstrec.rdoColumns("AG11CODRECURSO").Value) Then
'  strinsert1000 = strinsert1000 & "null" & ","
'Else
' strinsert1000 = strinsert1000 & rstrec.rdoColumns("AG11CODRECURSO").Value & ","
'End If
'If IsNull(rstrec.rdoColumns("pr14numunirec").Value) Then
'  strinsert1000 = strinsert1000 & "null" & ","
'Else
' strinsert1000 = strinsert1000 & rstrec.rdoColumns("pr14numunirec").Value & ","
'End If
'If IsNull(rstrec.rdoColumns("pr14numminocu").Value) Then
'  strinsert1000 = strinsert1000 & "null" & ","
'Else
' strinsert1000 = strinsert1000 & rstrec.rdoColumns("pr14numminocu").Value & ","
'End If
'If IsNull(rstrec.rdoColumns("AG11CODRECURSO").Value) Then
'  strinsert1000 = strinsert1000 & "null" & ")"
'Else
' strinsert1000 = strinsert1000 & rstrec.rdoColumns("AG11CODRECURSO").Value & ")"
'End If
'
'On Error GoTo Err_Ejecutar
'objApp.rdoConnect.Execute strinsert1000, 64
'rstrec.MoveNext
'Wend


rsta.Close
Set rsta = Nothing
rstA1.Close
Set rstA1 = Nothing
'rstA2.Close
'Set rstA2 = Nothing
'rstrec.Close
'Set rstrec = Nothing

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub



'Public Sub diariamente(inicio As Date, fin As Date, dias As Integer)
''gplan guarda la fecha planificada
'Dim gfechaplan As Date
'Call insertar(inicio)
'gfechaplan = inicio
'gfechaplan = DateAdd("d", dias, gfechaplan)
'While (gfechaplan <= fin)
' Call insertar(gfechaplan)
' gfechaplan = DateAdd("d", dias, gfechaplan)
'Wend
'End Sub


'Public Sub semanalmente(inicio As Date, fin As Date, semanas As Integer, lunes As Integer, _
'                         martes As Integer, miercoles As Integer, jueves As Integer, _
'                         viernes As Integer, sabado As Integer, domingo As Integer)
'
'Dim gfechaplan As Date
'Dim cuantosdias As Integer'
'
'cuantosdias = (7 * semanas)'
'
'gfechaplan = inicio
'If (lunes = 0 And martes = 0 And miercoles = 0 And jueves = 0 And _
'    viernes = 0 And sabado = 0 And domingo = 0) Then
'    Call insertar(inicio)
'    gfechaplan = DateAdd("ww", semanas, gfechaplan)
'    While (gfechaplan <= fin)
'     Call insertar(gfechaplan)
'     gfechaplan = DateAdd("ww", semanas, gfechaplan)
'    Wend
'End If
'gfechaplan = inicio
'If (lunes = 1) Then
' 'se busca el primer lunes a partir de la fecha de inicio
' Do While (gfechaplan <= fin)
'  If WeekDay(gfechaplan) <> 2 Then
'     gfechaplan = DateAdd("d", 1, gfechaplan)
'   Else 'ha encontrado el primer lunes
'   Call insertar(gfechaplan)
'     Exit Do
'   End If
' Loop
' 'se planifican los pr�ximos lunes
' gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Do While (gfechaplan <= fin)
'  Call insertar(gfechaplan)
'  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Loop
'End If
'gfechaplan = inicio
'If (martes = 1) Then
' 'se busca el primer martes a partir de la fecha de inicio
' Do While (gfechaplan <= fin)
'  If WeekDay(gfechaplan) <> 3 Then
'     gfechaplan = DateAdd("d", 1, gfechaplan)
'   Else 'ha encontrado el primer martes
'   Call insertar(gfechaplan)
'     Exit Do
'   End If
' Loop
' 'se planifican los pr�ximos martes
' gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Do While (gfechaplan <= fin)
'  Call insertar(gfechaplan)
'  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Loop
'End If
'gfechaplan = inicio
'If (miercoles = 1) Then
' 'se busca el primer miercoles a partir de la fecha de inicio
' Do While (gfechaplan <= fin)
'  If WeekDay(gfechaplan) <> 4 Then
'     gfechaplan = DateAdd("d", 1, gfechaplan)
'   Else 'ha encontrado el primer miercoles
'   Call insertar(gfechaplan)
'     Exit Do
'   End If
' Loop
' 'se planifican los pr�ximos miercoles
' gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Do While (gfechaplan <= fin)
'  Call insertar(gfechaplan)
'  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Loop
'End If
'gfechaplan = inicio
'If (jueves = 1) Then
' 'se busca el primer jueves a partir de la fecha de inicio
' Do While (gfechaplan <= fin)
'  If WeekDay(gfechaplan) <> 5 Then
'    gfechaplan = DateAdd("d", 1, gfechaplan)
'   Else 'ha encontrado el primer jueves
'   Call insertar(gfechaplan)
'     Exit Do
'   End If
' Loop
' 'se planifican los pr�ximos jueves
' gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Do While (gfechaplan <= fin)
'  Call insertar(gfechaplan)
'  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Loop
'End If
'gfechaplan = inicio
'If (viernes = 1) Then
' 'se busca el primer viernes a partir de la fecha de inicio
' Do While (gfechaplan <= fin)
'  If WeekDay(gfechaplan) <> 6 Then
'     gfechaplan = DateAdd("d", 1, gfechaplan)
'   Else 'ha encontrado el primer viernes
'   Call insertar(gfechaplan)
'     Exit Do
'   End If
' Loop
' 'se planifican los pr�ximos viernes
' gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Do While (gfechaplan <= fin)
'  Call insertar(gfechaplan)
'  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Loop
'End If
'gfechaplan = inicio
'If (sabado = 1) Then
' 'se busca el primer sabado a partir de la fecha de inicio
' Do While (gfechaplan <= fin)
'  If WeekDay(gfechaplan) <> 7 Then
'     gfechaplan = DateAdd("d", 1, gfechaplan)
'   Else 'ha encontrado el primer sabado
'  Call insertar(gfechaplan)
'     Exit Do
'   End If
' Loop
' 'se planifican los pr�ximos sabado
' gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Do While (gfechaplan <= fin)
'  Call insertar(gfechaplan)
'  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Loop
'End If
'gfechaplan = inicio
'If (domingo = 1) Then
' 'se busca el primer domingo a partir de la fecha de inicio
' Do While (gfechaplan <= fin)
'  If WeekDay(gfechaplan) <> 1 Then
'     gfechaplan = DateAdd("d", 1, gfechaplan)
'   Else 'ha encontrado el primer domingo
'   Call insertar(gfechaplan)
'     Exit Do
'   End If
' Loop
' 'se planifican los pr�ximos domingos
' gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Do While (gfechaplan <= fin)
'  Call insertar(gfechaplan)
'  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
' Loop
'End If
'
'End Sub



'Public Sub mensualmente_1(inicio As Date, fin As Date, dia As Integer, mes As Integer)
'Dim gfechaplan As Date
''dia contiene el d�a del mes de la actuacion
''mes contiene cada cu�ntos meses se hace la actuaci�n
'gfechaplan = inicio
''si el d�a de inicio es igual al d�a en que debe hacerse la actuaci�n
'Do While (gfechaplan <= fin)
' If Day(gfechaplan) = dia Then
'    insertar (gfechaplan)
'    Exit Do
' Else
'    gfechaplan = DateAdd("d", 1, gfechaplan)
' End If
' Loop
' 'ya se ha encontrado la primera fecha planificada, se suman los meses
' gfechaplan = DateAdd("m", mes, gfechaplan)
' Do While (gfechaplan <= fin)
'  insertar (gfechaplan)
'  gfechaplan = DateAdd("m", mes, gfechaplan)
' Loop
'End Sub


'Public Sub anualmente_1(inicio As Date, fin As Date, dia As Integer, mes As Integer, _
'                        ano As Integer)
''dia contiene el n�mero de d�a del mes
''mes contiene el n�mero de mes
''ano contiene cada cu�ntos a�os
'Dim gfechaplan As Date'
'
'gfechaplan = inicio
''se mira si la fecha de inicio coincide con la pedida
'If (Day(gfechaplan) = dia And Month(gfechaplan) = mes) Then
'   insertar (gfechaplan)
'Else
' 'la fecha no coincide ,hay que buscar la primera fecha que la cumpla
'   Do While (gfechaplan <= fin)
'     gfechaplan = DateAdd("d", 1, gfechaplan)
'     If (Day(gfechaplan) = dia And Month(gfechaplan) = mes) Then
'       insertar (gfechaplan)
'       Exit Do
'     End If
'   Loop
'End If
'
''se crean todas las fechas planificadas,sumando el intervalo en a�os
'gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'Do While (gfechaplan <= fin)
'   insertar (gfechaplan)
'   gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'Loop
'End Sub

Public Function dia1atras(fecha As Date) As Date
While Day(fecha) <> 1
    fecha = DateAdd("d", -1, fecha)
Wend
dia1atras = fecha
End Function
Public Function dia1adelante(diabuscado As Integer, fecha As Date) As Date
While Day(fecha) <> diabuscado
    fecha = DateAdd("d", 1, fecha)
Wend
dia1adelante = fecha
End Function
Public Function buscardia(fecha As Date, diasem As Integer) As Date
'funci�n que coge el d�a 1 del mes y busca el primer diasem
'diasem=lunes,...,domingo
If diasem < 7 Then
  While WeekDay(fecha) <> (diasem + 1)
    fecha = DateAdd("d", 1, fecha)
  Wend
End If
If diasem = 7 Then
  While WeekDay(fecha) <> 1
    fecha = DateAdd("d", 1, fecha)
  Wend
End If
buscardia = fecha
End Function
Public Function ultimodiadelmes(mes As Integer, ano As Integer) As Integer
'dado un mes y un a�o devuelve el n� de d�as del mes
Select Case mes
     Case 1, 3, 5, 7, 8, 10, 12
          ultimodiadelmes = 31
     Case 4, 6, 9, 11
          ultimodiadelmes = 30
     Case 2
          If (ano Mod 4) = 0 Then
            ultimodiadelmes = 29
          Else
            ultimodiadelmes = 28
          End If
End Select
End Function


'Public Sub mensualmente_2(inicio As Date, fin As Date, dia As Integer, _
'                          semana As Integer, mes As Integer)
'Dim gfechaplan As Date
'Dim ultimo As Integer
'Dim fechaaux As Date
'Dim proxfecha As Date
'Dim guardarinicio As Date
'Dim guardarfechaaux As Date
''dia indica el lugar que ocupa el d�a(primero,segundo,...,�ltimo)
''semana indica el d�a de la semana
''mes indica el intervalo de meses
'guardarinicio = inicio
'If dia <> 9 Then  'dia<>"�ltimo"
'If semana <> 0 Then  'semana<>"D�a",es decir, semana=lunes,....,domingo
'   gfechaplan = dia1atras(guardarinicio)
'   gfechaplan = buscardia(gfechaplan, semana)
'   gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
'   If (gfechaplan < inicio) Then
'     guardarinicio = inicio
'     gfechaplan = dia1adelante(1, guardarinicio)
'     gfechaplan = buscardia(gfechaplan, semana)
'     'se suma dia-1,es decir,si dia=primero=1 no suma ninguna semana
'     'pero si es dia=segundo=2 suma 1 semana
'     gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
'   End If
'   If (gfechaplan <= fin) Then
'     'inserto si la fecha obtenida no supera a la de fin
'     insertar (gfechaplan)
'   End If
'
'   'calculo la fecha dentro de X meses pero puede que no coincida el d�a
'   gfechaplan = DateAdd("m", mes, gfechaplan)
'   proxfecha = gfechaplan
'   While (gfechaplan <= fin)
'    gfechaplan = dia1atras(proxfecha)
'    gfechaplan = buscardia(gfechaplan, semana)
'    gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
'    If (gfechaplan < proxfecha) Then
'     gfechaplan = dia1adelante(1, proxfecha)
'     gfechaplan = buscardia(gfechaplan, semana)
'     gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
'     End If
'    insertar (gfechaplan)
'    gfechaplan = DateAdd("m", mes, gfechaplan)
'    proxfecha = gfechaplan
'   Wend
'End If
'If semana = 0 Then
'   guardarinicio = inicio
'   gfechaplan = dia1atras(guardarinicio)
'   gfechaplan = DateAdd("d", (dia - 1), gfechaplan)
'   If (gfechaplan < inicio) Then
'     guardarinicio = inicio
'     gfechaplan = dia1adelante(1, guardarinicio)
'     gfechaplan = DateAdd("d", (dia - 1), gfechaplan)
'   End If
'   insertar (gfechaplan)
'   gfechaplan = DateAdd("m", mes, gfechaplan)
'   While (gfechaplan <= fin)
'    insertar (gfechaplan)
'    gfechaplan = DateAdd("m", mes, gfechaplan)
'   Wend
' End If
'End If
'
'If dia = 9 Then '�ltimo
'If semana = 0 Then '�ltimo d�a del mes
'  guardarinicio = inicio
'  ultimo = ultimodiadelmes(Month(guardarinicio), Year(guardarinicio))
'  guardarinicio = inicio
'  gfechaplan = dia1adelante(ultimo, guardarinicio)
'  insertar (gfechaplan)
'  'ya tenemos el �ltimo d�a del mes
'  gfechaplan = DateAdd("m", mes, gfechaplan)
'  Do While (gfechaplan <= fin)
'    guardarinicio = gfechaplan
'    ultimo = ultimodiadelmes(Month(guardarinicio), Year(guardarinicio))
'    guardarinicio = gfechaplan
'    gfechaplan = dia1adelante(ultimo, guardarinicio)
'    insertar (gfechaplan)
'    gfechaplan = DateAdd("m", mes, gfechaplan)
'  Loop
' End If
'
' If semana <> 0 Then '�ltimo lunes,�ltimo martes,...,�ltimo domingo
'   'primero calculo el cuarto lunes,...,domingo
'   guardarinicio = inicio
'   gfechaplan = dia1atras(guardarinicio)
'   guardarinicio = gfechaplan
'   gfechaplan = buscardia(guardarinicio, semana)
'   gfechaplan = DateAdd("ww", 3, gfechaplan) 'sumo 3 semanas
'   If (gfechaplan < inicio) Then
'     gfechaplan = dia1adelante(1, inicio)
'     gfechaplan = buscardia(gfechaplan, semana)
'     gfechaplan = DateAdd("ww", 3, gfechaplan)  'se suma 3 semanas
'   End If
'   'ya tengo el cuarto (lunes por ejemplo),ahora miro si hay un quinto
'   Do While gfechaplan <= fin
'      fechaaux = gfechaplan
'      Do While (fechaaux <= fin) And (Month(fechaaux) = Month(gfechaplan))
'          If (semana < 7) Then
'            If (WeekDay(fechaaux) = (semana + 1)) And (fechaaux <> gfechaplan) Then
'             gfechaplan = fechaaux
'             Exit Do
'            End If
'          End If
'          If (semana = 7) Then
'            If (WeekDay(fechaaux) = 1) And (fechaaux <> gfechaplan) Then
'             gfechaplan = fechaaux
'             Exit Do
'            End If
'          End If
'          fechaaux = DateAdd("d", 1, fechaaux)
'      Loop
'      insertar (gfechaplan)
'
'      gfechaplan = DateAdd("m", mes, gfechaplan)
'      guardarinicio = gfechaplan
'      gfechaplan = dia1atras(guardarinicio)
'      guardarinicio = gfechaplan
'      gfechaplan = buscardia(gfechaplan, semana)
'      gfechaplan = DateAdd("ww", 3, gfechaplan) 'sumo 3 semanas
'      If (gfechaplan < inicio) Then
'       gfechaplan = dia1adelante(1, inicio)
'       gfechaplan = buscardia(gfechaplan, semana)
'       gfechaplan = DateAdd("ww", 3, gfechaplan)  'se suma 3 semanas
'      End If
'    Loop
'  End If
'End If
'End Sub

'++++++++++++++++++++++++++++++++++++++++++++++++

'Public Sub anualmente_2(inicio As Date, fin As Date, dia As Integer, _
'                          semana As Integer, mes As Integer, ano As Integer)
'Dim gfechaplan As Date
'Dim ultimo As Integer
'Dim fechaaux As Date
'Dim proxfecha As Date
'Dim guardarinicio As Date
'Dim guardarfechaaux As Date
'Dim guardargfechaplan As Date
''dia indica el lugar que ocupa el d�a(primero,segundo,...,�ltimo)
''semana indica el d�a de la semana
''mes indica el mes
''ano indica el intervalo en a�os
'
'gfechaplan = inicio
''primero se busca la fecha de inicio del mes indicado
'Do While (gfechaplan <= fin)
'   If (Month(gfechaplan) = mes) Then
'     Exit Do
'   Else
'     gfechaplan = DateAdd("m", 1, gfechaplan)
'   End If
'Loop
''ya tenemos una fecha del mes indicado
'
'guardargfechaplan = gfechaplan
'guardarinicio = gfechaplan
'If dia <> 9 Then  'dia<>"�ltimo"
'If semana <> 0 Then  'semana<>"D�a",es decir, semana=lunes,....,domingo
'   gfechaplan = dia1atras(guardarinicio)
'   gfechaplan = buscardia(gfechaplan, semana)
'   gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
'   If (gfechaplan < inicio) Then
'     guardarinicio = guardargfechaplan
'     gfechaplan = dia1adelante(1, guardarinicio)
'     gfechaplan = buscardia(gfechaplan, semana)
'     'se suma dia-1,es decir,si dia=primero=1 no suma ninguna semana
'     'pero si es dia=segundo=2 suma 1 semana
'     gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
'   End If
'   If (gfechaplan <= fin) Then
'     'inserto si la fecha obtenida no supera a la de fin
'     insertar (gfechaplan)
'   End If
'
'   'calculo la fecha dentro de X meses pero puede que no coincida el d�a
'   gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'   proxfecha = gfechaplan
'   While (gfechaplan <= fin)
'    gfechaplan = dia1atras(proxfecha)
'    gfechaplan = buscardia(gfechaplan, semana)
'    gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
'    If (gfechaplan < proxfecha) Then
'     gfechaplan = dia1adelante(1, proxfecha)
'     gfechaplan = buscardia(gfechaplan, semana)
'     gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
'     End If
'    insertar (gfechaplan)
'    gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'    proxfecha = gfechaplan
'   Wend
'End If
'If semana = 0 Then
'   guardargfechaplan = gfechaplan
'   guardarinicio = gfechaplan
'   gfechaplan = dia1atras(guardarinicio)
'   gfechaplan = DateAdd("d", (dia - 1), gfechaplan)
'   If (gfechaplan < inicio) Then
'     guardargfechaplan = gfechaplan
'     guardarinicio = gfechaplan
'     gfechaplan = dia1adelante(1, guardarinicio)
'     gfechaplan = DateAdd("d", (dia - 1), gfechaplan)
'   End If
'   insertar (gfechaplan)
'   gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'   While (gfechaplan <= fin)
'    insertar (gfechaplan)
'    gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'   Wend
' End If
'End If
'
'If dia = 9 Then '�ltimo
'If semana = 0 Then '�ltimo d�a del mes
'  guardargfechaplan = gfechaplan
'  guardarinicio = gfechaplan
'  ultimo = ultimodiadelmes(Month(guardarinicio), Year(guardarinicio))
'  guardargfechaplan = gfechaplan
'  guardarinicio = gfechaplan
'  gfechaplan = dia1adelante(ultimo, guardarinicio)
'  insertar (gfechaplan)
' 'ya tenemos el �ltimo d�a del mes
'  gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'  Do While (gfechaplan <= fin)
'    guardarinicio = gfechaplan
'    ultimo = ultimodiadelmes(Month(guardarinicio), Year(guardarinicio))
'    guardarinicio = gfechaplan
'    gfechaplan = dia1adelante(ultimo, guardarinicio)
'    insertar (gfechaplan)
'    gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'  Loop
' End If
'
' If semana <> 0 Then '�ltimo lunes,�ltimo martes,...,�ltimo domingo
'   'primero calculo el cuarto lunes,...,domingo
'   guardargfechaplan = gfechaplan
'   guardarinicio = gfechaplan
'   gfechaplan = dia1atras(guardarinicio)
'   guardarinicio = gfechaplan
'   gfechaplan = buscardia(guardarinicio, semana)
'   gfechaplan = DateAdd("ww", 3, gfechaplan) 'sumo 3 semanas
'   If (gfechaplan < inicio) Then
'     gfechaplan = dia1adelante(1, inicio)
'     gfechaplan = buscardia(gfechaplan, semana)
'     gfechaplan = DateAdd("ww", 3, gfechaplan)  'se suma 3 semanas
'   End If
'   'ya tengo el cuarto (lunes por ejemplo),ahora miro si hay un quinto
'   Do While gfechaplan <= fin
'      fechaaux = gfechaplan
'      Do While (fechaaux <= fin) And (Month(fechaaux) = Month(gfechaplan))
'          If (semana < 7) Then
'            If (WeekDay(fechaaux) = (semana + 1)) And (fechaaux <> gfechaplan) Then
'             gfechaplan = fechaaux
'             Exit Do
'            End If
'          End If
'          If (semana = 7) Then
'            If (WeekDay(fechaaux) = 1) And (fechaaux <> gfechaplan) Then
'             gfechaplan = fechaaux
'             Exit Do
'            End If
'          End If
'          fechaaux = DateAdd("d", 1, fechaaux)
'      Loop
'      insertar (gfechaplan)
'
'      gfechaplan = DateAdd("yyyy", ano, gfechaplan)
'      guardarinicio = gfechaplan
'      gfechaplan = dia1atras(guardarinicio)
'      guardarinicio = gfechaplan
'      gfechaplan = buscardia(gfechaplan, semana)
'      gfechaplan = DateAdd("ww", 3, gfechaplan) 'sumo 3 semanas
'      If (gfechaplan < inicio) Then
'       gfechaplan = dia1adelante(1, inicio)
'       gfechaplan = buscardia(gfechaplan, semana)
'       gfechaplan = DateAdd("ww", 3, gfechaplan)  'se suma 3 semanas
'      End If
'    Loop
'  End If
'End If
'End Sub




