VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmAsigProceso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Asignaci�n de Proceso/Asistencia."
   ClientHeight    =   8340
   ClientLeft      =   330
   ClientTop       =   1785
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0234.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Procesos de la Asistencia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Index           =   1
      Left            =   240
      TabIndex        =   18
      Top             =   5400
      Width           =   11415
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1695
         Index           =   0
         Left            =   120
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   360
         Width           =   11010
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   19420
         _ExtentY        =   2990
         _StockProps     =   79
         Caption         =   "PROCESOS DE LA ASISTENCIA"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones de la Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Index           =   0
      Left            =   240
      TabIndex        =   16
      Top             =   2400
      Width           =   11415
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2415
         Index           =   1
         Left            =   120
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   360
         Width           =   11010
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   19420
         _ExtentY        =   4260
         _StockProps     =   79
         Caption         =   "ACTUACIONES DE LA PETICI�N"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frame1 
      Caption         =   "Paciente/Asistencia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Index           =   2
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   11460
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   0
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Tag             =   "Primer Apellido del Paciente"
         Top             =   1320
         Width           =   1515
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataField       =   "AD01CODASISTENCI"
         Height          =   315
         HelpContextID   =   40101
         Index           =   11
         Left            =   360
         MaxLength       =   10
         TabIndex        =   12
         Tag             =   "Asistencia|C�digo de Asistencia"
         Top             =   1320
         Width           =   1275
      End
      Begin VB.TextBox txtHora 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   0
         Left            =   5640
         MaxLength       =   2
         TabIndex        =   11
         Tag             =   "Hora"
         Top             =   1320
         Width           =   390
      End
      Begin VB.TextBox txtMinuto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   3
         Index           =   0
         Left            =   6240
         MaxLength       =   2
         TabIndex        =   10
         Tag             =   "Minutos"
         Top             =   1320
         Width           =   390
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   10
         Left            =   7080
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "Segundo Apellido del Paciente"
         Top             =   600
         Width           =   3200
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   9
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "Primer Apellido del Paciente"
         Top             =   600
         Width           =   3200
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   8
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Tag             =   "Nombre del Paciente"
         Top             =   600
         Width           =   3200
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   3750
         TabIndex        =   15
         Top             =   1080
         Width           =   1065
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Asistencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   74
         Left            =   390
         TabIndex        =   14
         Top             =   1080
         Width           =   885
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hora de Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   5640
         TabIndex        =   13
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Apellido 2�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   7080
         TabIndex        =   9
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Apellido 1�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   3720
         TabIndex        =   8
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   360
         TabIndex        =   7
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdAsignar 
      Caption         =   "Asociar"
      Height          =   375
      Left            =   5280
      TabIndex        =   2
      Top             =   7680
      Width           =   1515
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAsigProceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00244.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 4 DE NOVIEMBRE DE 1998                                        *
'* DESCRIPCION:                                                         *
'*                                                                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1





Private Sub Form_Activate()
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    objWinInfo.DataRefresh
    grdDBGrid1(1).Columns(3).Width = 1200
    grdDBGrid1(1).Columns(4).Width = 1200
    grdDBGrid1(1).Columns(5).Width = 3000
    grdDBGrid1(1).Columns(6).Width = 2800
    grdDBGrid1(1).Columns(7).Width = 1200
    grdDBGrid1(1).Columns(8).Width = 1200
    
    grdDBGrid1(1).Columns(9).Visible = False
    
    grdDBGrid1(0).Columns(3).Width = 1300
    grdDBGrid1(0).Columns(4).Width = 1600
    grdDBGrid1(0).Columns(5).Width = 1800
    grdDBGrid1(0).Columns(6).Width = 2000
    grdDBGrid1(0).Columns(7).Width = 2000
    grdDBGrid1(0).Columns(8).Width = 2000
    
    grdDBGrid1(0).Columns(9).Visible = False
    grdDBGrid1(0).Columns(10).Visible = False
    grdDBGrid1(0).Columns(11).Visible = False
    grdDBGrid1(0).Columns(12).Visible = False
    
    Text1(8).Text = frmSeleccionarAct.txtText1(3).Text
    Text1(9).Text = frmSeleccionarAct.txtText1(4).Text
    Text1(10).Text = frmSeleccionarAct.txtText1(5).Text
    Text1(11).Text = grdDBGrid1(0).Columns(9).Value
    Text1(0).Text = grdDBGrid1(0).Columns(10).Value
    txtHora(0).Text = grdDBGrid1(0).Columns(11).Value
    txtMinuto(0).Text = grdDBGrid1(0).Columns(12).Value
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo1 As New clsCWForm
    Dim objMultiInfo2 As New clsCWForm

    Dim strKey As String
  
    
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
  
    With objMultiInfo1
        .strName = "Actuaciones"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR0801J"
        strKey = .strDataBase & .strTable
        .intAllowance = cwAllowReadOnly
        .intCursorSize = 0
        .strWhere = "PR09NUMPETICION=" & frmpedir.txtText1(0).Text
        'Call .objPrinter.Add("PR1061", "Listado de grupos y actuaciones")
        'Call .objPrinter.Add("PR1062", "Listado de cuestionario de un grupo")
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormAddOrderField("PR03NUMACTPEDI", cwAscending)
        '.blnHasMaint = True
        Call .FormCreateFilterWhere(strKey, "Actuaci�n")
        Call .FormAddFilterWhere(strKey, "PR03NUMACTPEDI", "N�m Act Pedi", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�d Actuaci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Desc Actuaci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�d Dpto", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD07CODPROCESO", "Proceso", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD01CODASISTENCI", "Asistencia", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR03NUMACTPEDI", "N�m Act Pedi")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�d Actuaci�n")
        Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Desc Actuaci�n")
        Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�d Dpto")
        Call .FormAddFilterOrder(strKey, "AD07CODPROCESO", "Proceso")
        Call .FormAddFilterOrder(strKey, "AD01CODASISTENCI", "Asistencia")
    End With
  
  
    With objMultiInfo2
        .strName = "Procesos de la Asistencia"
        Set .objFormContainer = fraframe1(1)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
  
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "AD0701J"
        .intCursorSize = 0
         .intAllowance = cwAllowReadOnly
         .strWhere = "CI21CODPERSONA=" & frmSeleccionarAct.txtText1(0).Text
         
        Call .FormAddOrderField("AD07CODPROCESO", cwAscending)
 
        strKey = .strDataBase & .strTable
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Procesos de la Asistencia")
        Call .FormAddFilterWhere(strKey, "AD07CODPROCESO", "C�digo Proceso", cwNumeric)
        Call .FormAddFilterWhere(strKey, "SG02COD", "M�dico Reponsable", cwString)
        Call .FormAddFilterWhere(strKey, "SG02NOM", "Nombre", cwString)
        Call .FormAddFilterWhere(strKey, "SG02APE1", "Primer Apellido", cwString)
        Call .FormAddFilterWhere(strKey, "SG02APE2", "Segundo Apellido", cwString)
        Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Departamento", cwString)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "AD07CODPROCESO", "C�digo Proceso")
        Call .FormAddFilterOrder(strKey, "SG02COD", "M�dico Reponsable")
        Call .FormAddFilterOrder(strKey, "SG02NOM", "Nombre")
        Call .FormAddFilterOrder(strKey, "SG02APE1", "Primer Apellido")
        Call .FormAddFilterOrder(strKey, "SG02APE2", "Segundo Apellido")
        Call .FormAddFilterOrder(strKey, "AD02DESDPTO", "Departamento")
    End With

    With objWinInfo

        

        Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
        Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
        
        Call .GridAddColumn(objMultiInfo1, "N�m Act Pedi", "PR03NUMACTPEDI", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo1, "C�d Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo1, "Desc Actuaci�n", "PR01DESCORTA", cwString, 30)
        Call .GridAddColumn(objMultiInfo1, "Departamento", "AD02DESDPTO", cwString, 30)
        Call .GridAddColumn(objMultiInfo1, "Proceso", "AD07CODPROCESO", cwString, 10)
        Call .GridAddColumn(objMultiInfo1, "Asistencia", "AD01CODASISTENCI", cwString, 10)
        Call .GridAddColumn(objMultiInfo1, "Secuencia", "PR08NUMSECUENCIA", cwNumeric, 5)
        
        Call .FormCreateInfo(objMultiInfo1)
        
        'Call .GridAddColumn(objMultiInfo2, "C�digo Proceso", "AD07CODPROCESO", cwNumeric, 10)
        'Call .GridAddColumn(objMultiInfo2, "M�dico Reponsable", "SG02COD", cwString, 6)
        'Call .GridAddColumn(objMultiInfo2, "Nombre", "SG02NOM", cwString, 30)
        'Call .GridAddColumn(objMultiInfo2, "Primer Apellido", "SG02APE1", cwString, 30)
        'Call .GridAddColumn(objMultiInfo2, "Segundo Apellido", "SG02APE2", cwString, 30)
        'Call .GridAddColumn(objMultiInfo2, "Departamento", "AD02DESDPTO", cwString, 30)



        'Call .FormCreateInfo(objMultiInfo2)
        
       
        Call .FormChangeColor(objMultiInfo1)
        'Call .FormChangeColor(objMultiInfo2)
        'Call .FormChangeColor(objMultiInfo1)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        'Call .FormChangeColor(objMultiInfo2)
        
        '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
'''''''''''''''''''''''''''''''''''''''''''''''''''''
        Call .GridAddColumn(objMultiInfo2, "C�digo Proceso", "AD07CODPROCESO", cwNumeric, 10)
        Call .GridAddColumn(objMultiInfo2, "M�dico Reponsable", "SG02COD", cwString, 6)
        Call .GridAddColumn(objMultiInfo2, "Nombre", "SG02NOM", cwString, 30)
        Call .GridAddColumn(objMultiInfo2, "Primer Apellido", "SG02APE1", cwString, 30)
        Call .GridAddColumn(objMultiInfo2, "Segundo Apellido", "SG02APE2", cwString, 30)
        Call .GridAddColumn(objMultiInfo2, "Departamento", "AD02DESDPTO", cwString, 30)
        Call .GridAddColumn(objMultiInfo2, "Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
        Call .GridAddColumn(objMultiInfo2, "Fecha", "FECHA", cwString, 10)
        Call .GridAddColumn(objMultiInfo2, "Hora", "HORA", cwString, 2)
        Call .GridAddColumn(objMultiInfo2, "Minutos", "MINUTOS", cwString, 2)

        Call .FormCreateInfo(objMultiInfo2)
        
        Call .FormChangeColor(objMultiInfo2)
        
        .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
        
        Call .WinRegister
        Call .WinStabilize
    End With
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub






' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------



'Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
'  If (strFormName = "Grupos") And (strCtrl = "cboSSDBCombo1(0)") Then
'    frmtipodegrupo.cmdaceptar.Visible = True
'    'Load frmtipodegrupo
'    'Call frmtipodegrupo.Show(vbModal)
'    Call objsecurity.LaunchProcess("PR0202")
'    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)), guCodigo)
'    Call objWinInfo.CtrlSet(cboSSDBCombo1(0), guCodigo)
'    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtAct(2)), gstrdescripcion)
'    'Unload frmtipodegrupo
'    'Set frmtipodegrupo = Nothing
'    Call objCW.objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
'  End If
' guCodigo = ""
' gstrdescripcion = ""
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)

    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
      
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)

  Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


Private Sub cmdAsignar_Click()
    Dim strUpdate As String
    Dim mintisel As Integer
    Dim mintNTotalSelRows As Integer
    Dim mvarBkmrk As Variant
    
  cmdAsignar.Enabled = False
    If grdDBGrid1(1).SelBookmarks.Count = 0 Then
        Call MsgBox("No ha Seleccionado las Actuaciones", vbInformation)
        cmdAsignar.Enabled = True
        Exit Sub
    End If
    If grdDBGrid1(0).SelBookmarks.Count = 0 Then
        Call MsgBox("No ha Seleccionado el Proceso", vbInformation)
        cmdAsignar.Enabled = True
        Exit Sub
    End If
    'Guardamos el n�mero de filas seleccionadas
    mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
    'minti = 1
    For mintisel = 0 To mintNTotalSelRows - 1
        'Guardamos el n�mero de fila que est� seleccionada
        mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintisel)
        grdDBGrid1(1).Columns(1).CellValue (mvarBkmrk)
        strUpdate = "UPDATE PR0800 set ad01codasistenci=" & grdDBGrid1(0).Columns(9).Value & _
                    ",ad07codproceso=" & grdDBGrid1(0).Columns(3).Value & _
                    " where pr09numpeticion=" & frmpedir.txtText1(0).Text & _
                    " and pr03numactpedi=" & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk) & _
                    " and pr08numsecuencia=" & grdDBGrid1(1).Columns(9).CellValue(mvarBkmrk)
        'On Error GoTo Err_Ejecutar
        objApp.rdoConnect.Execute strUpdate, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        strUpdate = "UPDATE PR0400 set ad01codasistenci=" & grdDBGrid1(0).Columns(9).Value & _
                    ",ad07codproceso=" & grdDBGrid1(0).Columns(3).Value & _
                    " where pr03numactpedi=" & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk)
        'On Error GoTo Err_Ejecutar
        objApp.rdoConnect.Execute strUpdate, 64
        objApp.rdoConnect.Execute "Commit", 64
    Next mintisel
    
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    objWinInfo.DataRefresh
  cmdAsignar.Enabled = True
    
End Sub





