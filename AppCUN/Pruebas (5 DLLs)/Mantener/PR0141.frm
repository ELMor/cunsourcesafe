VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmlanzarconsentimiento 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Lanzar Consentimiento"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11640
   ControlBox      =   0   'False
   Icon            =   "PR0141.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11640
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11640
      _ExtentX        =   20532
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraCabecera 
      Caption         =   "Petici�n y Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1845
      Index           =   0
      Left            =   360
      TabIndex        =   5
      Top             =   480
      Width           =   10905
      Begin VB.TextBox txtCabecera 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   4
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "Primer apellido|Primer apellido del paciente"
         Top             =   1200
         Width           =   4500
      End
      Begin VB.TextBox txtCabecera 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   3
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Tag             =   "Nombre|Nombre del paciente"
         Top             =   1200
         Width           =   4500
      End
      Begin VB.TextBox txtCabecera 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   2
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "D.N.I."
         Top             =   1200
         Width           =   1312
      End
      Begin VB.TextBox txtCabecera 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   0
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "Grupo"
         Top             =   600
         Width           =   1092
      End
      Begin VB.TextBox txtCabecera 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   1
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "C�digo Petici�n"
         Top             =   600
         Width           =   1092
      End
      Begin VB.Label lblCabecera 
         Caption         =   "Apellido 1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   4
         Left            =   6240
         TabIndex        =   14
         Top             =   960
         Width           =   975
      End
      Begin VB.Label lblCabecera 
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   3
         Left            =   1680
         TabIndex        =   12
         Top             =   960
         Width           =   975
      End
      Begin VB.Label lblCabecera 
         Caption         =   "D.N.I."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   2
         Top             =   960
         Width           =   975
      End
      Begin VB.Label lblCabecera 
         Caption         =   "C�d.Petici�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   1680
         TabIndex        =   8
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblCabecera 
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones Pedidas para las que es Necesario el Consentimiento del Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5400
      Index           =   0
      Left            =   360
      TabIndex        =   1
      Top             =   2520
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4905
         Index           =   0
         Left            =   240
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   10440
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   18415
         _ExtentY        =   8652
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11640
      _ExtentX        =   20532
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmlanzarconsentimiento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00141.FRM                                                  *
'* AUTOR: JESUS MARIA RODILLA LARA                                      *
'* FECHA: 17 DE OCTUBRE DE 1997                                         *
'* DESCRIPCION: Lanza los consentimientos para que sean firmados        *
'*                                                                      *
'* ARGUMENTOS:  GRUPO, N� DE PETICION, DNI, NOMBRE Y PRIMER APELLIDO    *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub Form_Activate()
  'Cargamos los campos de la cabecera
  'txtCabecera(0).Text = 1 'N�mero de grupo
  'txtCabecera(1).Text = 1 'N�mero de petici�n
  'txtCabecera(2).Text = 99999999 'D.N.I. del paciente
  'txtCabecera(3).Text = "JUAN" 'Nombre del paciente
  'txtCabecera(4).Text = "SIN MIEDO" 'Apellido del paciente
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMultiInfo
    .strName = "Consentimiento Firmado"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .intAllowance = cwAllowModify
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0300"
    .strWhere = " (PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR0100 WHERE (PR01INDCONSFDO = 1) OR (PR01INDCONSFDO = -1))) " _
                & " AND PR03NUMACTPEDI IN (SELECT PR03NUMACTPEDI " _
                & " FROM PR0800 " _
                & " WHERE PR09NUMPETICION = " & frmpedir.txtText1(0) & ")" 'DEBE PONERSE EL N� DE PETICION DE LA CABECERA
                
     .intCursorSize = 0
    Call .FormAddOrderField("PR03NUMACTPEDI", cwAscending)
    
    Call .objPrinter.Add("PR1411", "Paciente Procedimientos no Quir�rgicos")
    Call .objPrinter.Add("PR1412", "Familiar Procedimientos Quir�rgicos")
    Call .objPrinter.Add("PR1413", "Paciente Procedimientos Quir�rgicos")
    Call .objPrinter.Add("PR1414", "Cateterismo Cardiaco")
    Call .objPrinter.Add("PR1415", "Ecocardiograma Transesof�gico")
    Call .objPrinter.Add("PR1416", "Familiar Departamento Oncolog�a")
    Call .objPrinter.Add("PR1417", "Paciente Departamento Oncolog�a")
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Tabla de Actuaciones Pedias")
    Call .FormAddFilterWhere(strKey, "PR03INDCONSFIRM", "Hay que Firmar", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03NUMACTPEDI", "N�Actuaci�n", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "PR03DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Departamento", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "PR03NUMACTPEDI", "N�Actuaci�n")
    'Call .FormAddFilterOrder(strKey, "PR03DESCORTA", "Actuaci�n")
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "Firmado", "PR03INDCONSFIRM", cwBoolean, 5)
    Call .GridAddColumn(objMultiInfo, "N�Actuacion", "PR03NUMACTPEDI", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "Actuaci�n", "PR03DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "C�d.Dpto.", "AD02CODDPTO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Departamento", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "pr01codactuacion", "PR01CODACTUACION", cwNumeric, 9)
      
    Call .FormCreateInfo(objMultiInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnMandatory = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnMandatory = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnMandatory = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnMandatory = True
    
    Call .FormChangeColor(objMultiInfo)
  
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = False
    
    
  
    'Sacamos el nombre del departamento que realiza esa actuaci�n
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(7), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(5), "PR01DESCORTA")
        
    Call .WinRegister
    Call .WinStabilize
  End With
  
  grdDBGrid1(0).Columns(8).Visible = False
  
  'Call objApp.SplashOff
End Sub
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strFiltro As String
  
  If strFormName = "Consentimiento Firmado" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
     'If blnHasFilter = False Then
     '    strFiltro = objWinInfo.DataGetWhere(blnHasFilter)
     'Else
     '    strFiltro = CrearFiltroInforme(objWinInfo.DataGetWhere(blnHasFilter), "PR3301J.", "PR33CODESTANDARD")
     '     strFiltro = CrearFiltroInforme(strFiltro, "PR3301J.", "AD02CODDPTO")
     ' End If
      strFiltro = objWinInfo.DataGetWhere(blnHasFilter)
      Call objPrinter.ShowReport(strFiltro, objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub


Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


