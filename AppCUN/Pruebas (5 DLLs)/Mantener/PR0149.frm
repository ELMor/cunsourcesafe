VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form frmdatosactpedida 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Planificaci�n de Actuaciones"
   ClientHeight    =   6870
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11670
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0149.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6870
   ScaleWidth      =   11670
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   53
      Top             =   0
      Width           =   11670
      _ExtentX        =   20585
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaci�n Pedida"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7575
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   11625
      Begin TabDlg.SSTab tabTab1 
         Height          =   7140
         Index           =   0
         Left            =   120
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   12594
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0149.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(24)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(10)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(23)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cboSSDBCombo1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "tabTab1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cmdcuestionario"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(12)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(13)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(19)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "fraFrame1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "chkCheck1(8)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "cmdrecursos"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(4)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "cmdinterplani"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtDesUrgencia"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "chkCheck1(9)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(2)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtDesDptoCargo"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(6)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).ControlCount=   25
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0149.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   6
            Left            =   5520
            TabIndex        =   2
            Tag             =   "N� Actuaci�n Pedida|N�mero de Actuaci�n Pedida"
            Top             =   840
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.TextBox txtDesDptoCargo 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Left            =   7800
            TabIndex        =   111
            TabStop         =   0   'False
            Tag             =   "Departamento al que se le carga la actuaci�n de inter�s cient�fico"
            Top             =   1200
            Visible         =   0   'False
            Width           =   3120
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO_CRG"
            Height          =   330
            HelpContextID   =   30101
            Index           =   2
            Left            =   7320
            TabIndex        =   110
            Tag             =   "C�digo Departamento|Departamento"
            Top             =   1200
            Visible         =   0   'False
            Width           =   372
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�Es de inter�s cientifico?"
            DataField       =   "PR03INDINTCIENTIF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   7920
            TabIndex        =   7
            Tag             =   "�Es de inter�s cient�fico?"
            Top             =   600
            Width           =   2535
         End
         Begin VB.TextBox txtDesUrgencia 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Left            =   2880
            Locked          =   -1  'True
            TabIndex        =   107
            TabStop         =   0   'False
            Tag             =   "Descripci�n de la Urgencia"
            Top             =   960
            Width           =   1920
         End
         Begin VB.CommandButton cmdinterplani 
            Caption         =   "Interacciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   9240
            TabIndex        =   26
            Top             =   3240
            Width           =   1575
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   2040
            TabIndex        =   3
            Tag             =   "Descripci�n Corta|Descripci�n Corta de la Actuaci�n"
            Top             =   360
            Width           =   5400
         End
         Begin VB.CommandButton cmdrecursos 
            Caption         =   "Recursos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   9240
            TabIndex        =   25
            Top             =   2640
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�Citable?"
            DataField       =   "PR03INDCITAANT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   7920
            TabIndex        =   6
            Tag             =   "�La actuaci�n es Citable?"
            Top             =   360
            Width           =   1455
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "La actuaci�n ocurre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2775
            Index           =   1
            Left            =   360
            TabIndex        =   23
            Top             =   4200
            Width           =   10335
            Begin TabDlg.SSTab tabTab1 
               Height          =   2340
               Index           =   2
               Left            =   240
               TabIndex        =   30
               TabStop         =   0   'False
               Top             =   360
               Width           =   7695
               _ExtentX        =   13573
               _ExtentY        =   4128
               _Version        =   327681
               Style           =   1
               Tabs            =   5
               TabsPerRow      =   5
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "1 vez"
               TabPicture(0)   =   "PR0149.frx":0044
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(40)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(41)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "dtcfecha"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "txthora"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "txtminuto"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "UpDownhora"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "UpDownminuto"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "txtText1(23)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).ControlCount=   8
               TabCaption(1)   =   "Diariamente"
               TabPicture(1)   =   "PR0149.frx":0060
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "txtText1(10)"
               Tab(1).Control(1)=   "UpDowndias"
               Tab(1).Control(2)=   "Label1"
               Tab(1).Control(3)=   "lblLabel1(29)"
               Tab(1).Control(4)=   "lblLabel1(3)"
               Tab(1).ControlCount=   5
               TabCaption(2)   =   "Semanalmente"
               TabPicture(2)   =   "PR0149.frx":007C
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "txtText1(20)"
               Tab(2).Control(1)=   "UpDownsemanas"
               Tab(2).Control(2)=   "chkCheck1(7)"
               Tab(2).Control(3)=   "chkCheck1(6)"
               Tab(2).Control(4)=   "chkCheck1(5)"
               Tab(2).Control(5)=   "chkCheck1(4)"
               Tab(2).Control(6)=   "chkCheck1(3)"
               Tab(2).Control(7)=   "chkCheck1(1)"
               Tab(2).Control(8)=   "chkCheck1(2)"
               Tab(2).Control(9)=   "lblLabel1(26)"
               Tab(2).Control(10)=   "lblLabel1(31)"
               Tab(2).Control(11)=   "lblLabel1(30)"
               Tab(2).ControlCount=   12
               TabCaption(3)   =   "Mensualmente"
               TabPicture(3)   =   "PR0149.frx":0098
               Tab(3).ControlEnabled=   0   'False
               Tab(3).Control(0)=   "fraFrame1(2)"
               Tab(3).Control(1)=   "txtText1(16)"
               Tab(3).Control(2)=   "txtText1(17)"
               Tab(3).Control(3)=   "txtText1(15)"
               Tab(3).Control(4)=   "txtText1(14)"
               Tab(3).ControlCount=   5
               TabCaption(4)   =   "Anualmente"
               TabPicture(4)   =   "PR0149.frx":00B4
               Tab(4).ControlEnabled=   0   'False
               Tab(4).Control(0)=   "fraFrame1(3)"
               Tab(4).Control(1)=   "txtText1(3)"
               Tab(4).Control(2)=   "txtText1(18)"
               Tab(4).ControlCount=   3
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR03NUMDIASENOCU"
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   10
                  Left            =   -74160
                  TabIndex        =   12
                  Tag             =   "N�mero de D�as entre ocurrencias de la Actuaci�n|N� d�as entre ocurrencias"
                  Top             =   960
                  Width           =   480
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR03NUMSEMENOCU"
                  Height          =   330
                  HelpContextID   =   30101
                  Index           =   20
                  Left            =   -73560
                  TabIndex        =   13
                  Tag             =   "N� de Semanas entre Ocurrencias de la Actuaci�n|N� Semanas entre Ocurrencias"
                  Top             =   840
                  Width           =   372
               End
               Begin ComCtl2.UpDown UpDownsemanas 
                  Height          =   330
                  Left            =   -73200
                  TabIndex        =   97
                  Top             =   840
                  Width           =   240
                  _ExtentX        =   423
                  _ExtentY        =   582
                  _Version        =   327681
                  Value           =   1
                  OrigLeft        =   -73200
                  OrigTop         =   840
                  OrigRight       =   -72960
                  OrigBottom      =   1170
                  Max             =   20
                  Min             =   1
                  Enabled         =   -1  'True
               End
               Begin ComCtl2.UpDown UpDowndias 
                  Height          =   330
                  Left            =   -73680
                  TabIndex        =   96
                  Top             =   960
                  Width           =   240
                  _ExtentX        =   423
                  _ExtentY        =   582
                  _Version        =   327681
                  Value           =   1
                  OrigLeft        =   -73680
                  OrigTop         =   960
                  OrigRight       =   -73440
                  OrigBottom      =   1290
                  Max             =   45
                  Min             =   1
                  Enabled         =   -1  'True
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR03fecpreferen"
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   23
                  Left            =   840
                  Locked          =   -1  'True
                  TabIndex        =   11
                  TabStop         =   0   'False
                  Tag             =   "Fecha y Hora de Preferencia"
                  Top             =   1680
                  Visible         =   0   'False
                  Width           =   3975
               End
               Begin ComCtl2.UpDown UpDownminuto 
                  Height          =   375
                  Left            =   4440
                  TabIndex        =   91
                  Top             =   1080
                  Width           =   240
                  _ExtentX        =   423
                  _ExtentY        =   661
                  _Version        =   327681
                  OrigLeft        =   4440
                  OrigTop         =   1080
                  OrigRight       =   4680
                  OrigBottom      =   1455
                  Increment       =   5
                  Max             =   55
                  Enabled         =   -1  'True
               End
               Begin ComCtl2.UpDown UpDownhora 
                  Height          =   375
                  Left            =   3840
                  TabIndex        =   90
                  Top             =   1080
                  Width           =   240
                  _ExtentX        =   423
                  _ExtentY        =   661
                  _Version        =   327681
                  OrigLeft        =   3840
                  OrigTop         =   1080
                  OrigRight       =   4080
                  OrigBottom      =   1455
                  Max             =   23
                  Enabled         =   -1  'True
               End
               Begin VB.TextBox txtminuto 
                  Alignment       =   1  'Right Justify
                  Height          =   375
                  Left            =   4080
                  MaxLength       =   2
                  TabIndex        =   10
                  Tag             =   "Minutos de la Preferencia"
                  Top             =   1080
                  Width           =   375
               End
               Begin VB.TextBox txthora 
                  Alignment       =   1  'Right Justify
                  Height          =   375
                  Left            =   3480
                  MaxLength       =   2
                  TabIndex        =   9
                  Tag             =   "Hora de Preferencia"
                  Top             =   1080
                  Width           =   375
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Domingo"
                  DataField       =   "PR03INDDOMINGO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   7
                  Left            =   -70560
                  TabIndex        =   20
                  Tag             =   "Domingo|La Actuaci�n se realizar� los Domingos"
                  Top             =   1680
                  Width           =   1155
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "S�bado"
                  DataField       =   "PR03INDSABADO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   6
                  Left            =   -71760
                  TabIndex        =   19
                  Tag             =   "S�bado|La Actuaci�n se realizar� los S�bados"
                  Top             =   1680
                  Width           =   1035
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Viernes"
                  DataField       =   "PR03INDVIERNES"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   5
                  Left            =   -71760
                  TabIndex        =   18
                  Tag             =   "Viernes|La Actuaci�n se realizar� los Viernes"
                  Top             =   1440
                  Width           =   1035
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Jueves"
                  DataField       =   "PR03INDJUEVES"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   4
                  Left            =   -73080
                  TabIndex        =   17
                  Tag             =   "Jueves|La Actuaci�n se realizar� los Jueves"
                  Top             =   1680
                  Width           =   1035
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Mi�rcoles"
                  DataField       =   "PR03INDMIERCOLES"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   3
                  Left            =   -73080
                  TabIndex        =   16
                  Tag             =   "Mi�rcoles|La Actuaci�n se realizar� los Mi�rcoles"
                  Top             =   1440
                  Width           =   1155
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Lunes"
                  DataField       =   "PR03INDLUNES"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   1
                  Left            =   -74160
                  TabIndex        =   14
                  Tag             =   "Lunes|La Actuaci�n se realizar� los Lunes"
                  Top             =   1440
                  Width           =   915
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Martes"
                  DataField       =   "PR03INDMARTES"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   2
                  Left            =   -74160
                  TabIndex        =   15
                  Tag             =   "Martes|La Actuaci�n se realizar� los Martes"
                  Top             =   1680
                  Width           =   975
               End
               Begin VB.Frame fraFrame1 
                  Height          =   1575
                  Index           =   2
                  Left            =   -74880
                  TabIndex        =   71
                  Top             =   480
                  Width           =   6615
                  Begin VB.TextBox txtcombo 
                     Height          =   315
                     Index           =   1
                     Left            =   2400
                     Locked          =   -1  'True
                     TabIndex        =   100
                     Tag             =   "D�a de la semana"
                     Top             =   840
                     Width           =   1300
                  End
                  Begin VB.TextBox txtcombo 
                     Height          =   315
                     Index           =   0
                     Left            =   1200
                     Locked          =   -1  'True
                     TabIndex        =   99
                     Tag             =   "Situaci�n del d�a de la semana"
                     Top             =   840
                     Width           =   840
                  End
                  Begin VB.ComboBox cboCombo1 
                     Height          =   315
                     Index           =   0
                     Left            =   1200
                     TabIndex        =   33
                     Tag             =   "Posici�n del D�a de la Semana"
                     Top             =   840
                     Width           =   1095
                  End
                  Begin VB.OptionButton optOption1 
                     Caption         =   " El"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   1
                     Left            =   360
                     TabIndex        =   76
                     Top             =   840
                     Width           =   615
                  End
                  Begin VB.OptionButton optOption1 
                     Caption         =   " D�a"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   0
                     Left            =   360
                     TabIndex        =   75
                     Top             =   360
                     Width           =   735
                  End
                  Begin VB.ComboBox cboCombo1 
                     Height          =   315
                     Index           =   1
                     Left            =   2400
                     TabIndex        =   34
                     Tag             =   "D�a de la Semana"
                     Top             =   840
                     Width           =   1575
                  End
                  Begin VB.TextBox Text1 
                     Alignment       =   1  'Right Justify
                     Height          =   330
                     Index           =   0
                     Left            =   2760
                     MaxLength       =   2
                     TabIndex        =   32
                     Tag             =   "N�mero de Meses entre Ocurrencias"
                     Top             =   360
                     Width           =   400
                  End
                  Begin VB.TextBox Text1 
                     Alignment       =   1  'Right Justify
                     Height          =   330
                     Index           =   1
                     Left            =   4800
                     MaxLength       =   2
                     TabIndex        =   35
                     Tag             =   "N�mero de Meses entre Ocurrencias"
                     Top             =   840
                     Width           =   400
                  End
                  Begin VB.TextBox Text1 
                     Alignment       =   1  'Right Justify
                     Height          =   330
                     Index           =   2
                     Left            =   1200
                     MaxLength       =   2
                     TabIndex        =   31
                     Tag             =   "D�a del Mes en que se har� la Actuaci�n"
                     Top             =   360
                     Width           =   400
                  End
                  Begin ComCtl2.UpDown UpDownmes2 
                     Height          =   330
                     Left            =   5160
                     TabIndex        =   72
                     Top             =   840
                     Width           =   240
                     _ExtentX        =   423
                     _ExtentY        =   582
                     _Version        =   327681
                     Value           =   1
                     OrigLeft        =   5160
                     OrigTop         =   840
                     OrigRight       =   5400
                     OrigBottom      =   1170
                     Max             =   20
                     Min             =   1
                     Enabled         =   -1  'True
                  End
                  Begin ComCtl2.UpDown UpDownmes 
                     Height          =   330
                     Left            =   3120
                     TabIndex        =   73
                     Top             =   360
                     Width           =   240
                     _ExtentX        =   423
                     _ExtentY        =   582
                     _Version        =   327681
                     Value           =   1
                     OrigLeft        =   3120
                     OrigTop         =   360
                     OrigRight       =   3360
                     OrigBottom      =   690
                     Max             =   20
                     Min             =   1
                     Enabled         =   -1  'True
                  End
                  Begin ComCtl2.UpDown UpDowndiames 
                     Height          =   330
                     Left            =   1560
                     TabIndex        =   74
                     Top             =   360
                     Width           =   240
                     _ExtentX        =   423
                     _ExtentY        =   582
                     _Version        =   327681
                     Value           =   1
                     OrigLeft        =   1560
                     OrigTop         =   360
                     OrigRight       =   1800
                     OrigBottom      =   690
                     Max             =   31
                     Min             =   1
                     Enabled         =   -1  'True
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "mes(es)"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   32
                     Left            =   5520
                     TabIndex        =   83
                     Top             =   840
                     Width           =   735
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "de cada"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   33
                     Left            =   4080
                     TabIndex        =   82
                     Top             =   840
                     Width           =   735
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "mes(es)"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   34
                     Left            =   3480
                     TabIndex        =   81
                     Top             =   360
                     Width           =   735
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "de cada"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   35
                     Left            =   1920
                     TabIndex        =   80
                     Top             =   360
                     Width           =   735
                  End
                  Begin VB.Label Label2 
                     Caption         =   "arriba(NUMMESENOCU)"
                     Height          =   255
                     Left            =   4080
                     TabIndex        =   79
                     Top             =   1200
                     Visible         =   0   'False
                     Width           =   1935
                  End
                  Begin VB.Label Label3 
                     Caption         =   "numdiasemocu"
                     Height          =   255
                     Left            =   2640
                     TabIndex        =   78
                     Top             =   1200
                     Visible         =   0   'False
                     Width           =   1095
                  End
                  Begin VB.Label Label4 
                     Caption         =   "numsitdiaemes"
                     Height          =   255
                     Left            =   1200
                     TabIndex        =   77
                     Top             =   1200
                     Visible         =   0   'False
                     Width           =   1095
                  End
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "PR03NUMDIASEMOCU"
                  Height          =   300
                  Index           =   16
                  Left            =   -68160
                  TabIndex        =   47
                  Tag             =   "D�a de la Semana"
                  Top             =   1680
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "PR03NUMSITDIAEMES"
                  Height          =   300
                  Index           =   17
                  Left            =   -68160
                  TabIndex        =   46
                  Tag             =   "Posici�n del D�a de la Semana"
                  Top             =   1320
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "PR03NUMDIAMESOCU"
                  Height          =   300
                  Index           =   15
                  Left            =   -68160
                  TabIndex        =   44
                  Tag             =   "N� del D�a de la Semana"
                  Top             =   600
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "PR03NUMMESENOCU"
                  Height          =   300
                  Index           =   14
                  Left            =   -68160
                  TabIndex        =   45
                  Tag             =   "N�mero de Meses entre Ocurrencias"
                  Top             =   960
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.Frame fraFrame1 
                  Height          =   1815
                  Index           =   3
                  Left            =   -74760
                  TabIndex        =   61
                  Top             =   360
                  Width           =   6615
                  Begin VB.TextBox txtcombo 
                     Height          =   315
                     Index           =   5
                     Left            =   4200
                     Locked          =   -1  'True
                     TabIndex        =   104
                     Tag             =   "Mes"
                     Top             =   840
                     Width           =   1800
                  End
                  Begin VB.TextBox txtcombo 
                     Height          =   315
                     Index           =   4
                     Left            =   2160
                     Locked          =   -1  'True
                     TabIndex        =   103
                     Tag             =   "D�a de la semana"
                     Top             =   840
                     Width           =   1200
                  End
                  Begin VB.TextBox txtcombo 
                     Height          =   315
                     Index           =   3
                     Left            =   960
                     Locked          =   -1  'True
                     TabIndex        =   102
                     Tag             =   "Situaci�n del D�a de la Semana"
                     Top             =   840
                     Width           =   840
                  End
                  Begin VB.TextBox txtcombo 
                     Height          =   315
                     Index           =   2
                     Left            =   2160
                     Locked          =   -1  'True
                     TabIndex        =   101
                     Tag             =   "Mes"
                     Top             =   360
                     Width           =   1200
                  End
                  Begin ComCtl2.UpDown UpDownano2 
                     Height          =   330
                     Left            =   2280
                     TabIndex        =   94
                     Top             =   1320
                     Width           =   240
                     _ExtentX        =   423
                     _ExtentY        =   582
                     _Version        =   327681
                     Value           =   1
                     OrigLeft        =   2280
                     OrigTop         =   1320
                     OrigRight       =   2520
                     OrigBottom      =   1650
                     Min             =   1
                     Enabled         =   -1  'True
                  End
                  Begin VB.ComboBox cboCombo1 
                     Height          =   315
                     Index           =   5
                     Left            =   4200
                     TabIndex        =   41
                     Tag             =   "Mes de la Ocurrencia"
                     Top             =   840
                     Width           =   2055
                  End
                  Begin VB.ComboBox cboCombo1 
                     Height          =   315
                     Index           =   4
                     Left            =   2160
                     TabIndex        =   40
                     Tag             =   "D�a de la Semana"
                     Top             =   840
                     Width           =   1455
                  End
                  Begin VB.ComboBox cboCombo1 
                     Height          =   315
                     Index           =   3
                     Left            =   960
                     TabIndex        =   39
                     Tag             =   "Posici�n del D�a de la Semana"
                     Top             =   840
                     Width           =   1095
                  End
                  Begin VB.OptionButton optOption2 
                     Caption         =   " El"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   1
                     Left            =   240
                     TabIndex        =   65
                     Top             =   840
                     Width           =   615
                  End
                  Begin VB.OptionButton optOption2 
                     Caption         =   " D�a"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   0
                     Left            =   240
                     TabIndex        =   64
                     Top             =   360
                     Width           =   735
                  End
                  Begin VB.TextBox Text1 
                     Alignment       =   1  'Right Justify
                     Height          =   330
                     Index           =   3
                     Left            =   1080
                     MaxLength       =   2
                     TabIndex        =   36
                     Tag             =   "D�a del Mes en que se har� la Actuaci�n"
                     Top             =   360
                     Width           =   400
                  End
                  Begin VB.ComboBox cboCombo1 
                     Height          =   315
                     Index           =   2
                     Left            =   2160
                     TabIndex        =   37
                     Tag             =   "Mes de la Ocurrencia"
                     Top             =   360
                     Width           =   1455
                  End
                  Begin VB.TextBox Text1 
                     Alignment       =   1  'Right Justify
                     Height          =   330
                     Index           =   4
                     Left            =   4560
                     MaxLength       =   2
                     TabIndex        =   38
                     Tag             =   "N�mero de a�os entre Ocurrencias"
                     Top             =   360
                     Width           =   400
                  End
                  Begin VB.TextBox Text1 
                     Alignment       =   1  'Right Justify
                     Height          =   330
                     Index           =   5
                     Left            =   1920
                     MaxLength       =   2
                     TabIndex        =   42
                     Tag             =   "N�mero de a�os entre Ocurrencias"
                     Top             =   1320
                     Width           =   375
                  End
                  Begin ComCtl2.UpDown UpDowndiaano 
                     Height          =   330
                     Left            =   1440
                     TabIndex        =   62
                     Top             =   360
                     Width           =   240
                     _ExtentX        =   423
                     _ExtentY        =   582
                     _Version        =   327681
                     Value           =   1
                     OrigLeft        =   1440
                     OrigTop         =   360
                     OrigRight       =   1680
                     OrigBottom      =   690
                     Max             =   31
                     Min             =   1
                     Enabled         =   -1  'True
                  End
                  Begin ComCtl2.UpDown UpDownano 
                     Height          =   330
                     Left            =   4920
                     TabIndex        =   63
                     Top             =   360
                     Width           =   240
                     _ExtentX        =   423
                     _ExtentY        =   582
                     _Version        =   327681
                     Value           =   1
                     OrigLeft        =   4920
                     OrigTop         =   360
                     OrigRight       =   5160
                     OrigBottom      =   690
                     Min             =   1
                     Enabled         =   -1  'True
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "a�o(s)"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   7
                     Left            =   2640
                     TabIndex        =   98
                     Top             =   1320
                     Width           =   615
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "de "
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   36
                     Left            =   3720
                     TabIndex        =   70
                     Top             =   840
                     Width           =   495
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "de"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   37
                     Left            =   1800
                     TabIndex        =   69
                     Top             =   360
                     Width           =   255
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "de cada"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   11
                     Left            =   3720
                     TabIndex        =   68
                     Top             =   360
                     Width           =   735
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "a�o(s)"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   38
                     Left            =   5280
                     TabIndex        =   67
                     Top             =   360
                     Width           =   615
                  End
                  Begin VB.Label lblLabel1 
                     Caption         =   "de cada"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   25
                     Left            =   1080
                     TabIndex        =   66
                     Top             =   1320
                     Width           =   855
                  End
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "PR03NUMMESANOOCU"
                  Height          =   300
                  Index           =   3
                  Left            =   -68040
                  TabIndex        =   49
                  Tag             =   "N� del Mes de la Ocurrencia"
                  Top             =   960
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR03NUMANOENOCU"
                  Height          =   300
                  HelpContextID   =   30104
                  Index           =   18
                  Left            =   -68040
                  TabIndex        =   48
                  Tag             =   "N�mero de a�os entre Ocurrencias"
                  Top             =   600
                  Visible         =   0   'False
                  Width           =   420
               End
               Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
                  Height          =   2865
                  Index           =   2
                  Left            =   -74910
                  TabIndex        =   58
                  TabStop         =   0   'False
                  Top             =   90
                  Width           =   8655
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Col.Count       =   0
                  BevelColorFrame =   0
                  BevelColorHighlight=   16777215
                  AllowUpdate     =   0   'False
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   2
                  AllowGroupSwapping=   0   'False
                  AllowGroupShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  MaxSelectedRows =   0
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterVisible =   -1  'True
                  Columns(0).Width=   3200
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   4096
                  UseDefaults     =   0   'False
                  _ExtentX        =   15266
                  _ExtentY        =   5054
                  _StockProps     =   79
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcfecha 
                  Height          =   330
                  Left            =   840
                  TabIndex        =   8
                  Tag             =   "Fecha de Preferencia"
                  Top             =   1080
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  AutoSelect      =   0   'False
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora de Preferencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   41
                  Left            =   3480
                  TabIndex        =   93
                  Top             =   840
                  Width           =   2175
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha de Preferencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   40
                  Left            =   840
                  TabIndex        =   92
                  Top             =   840
                  Width           =   2055
               End
               Begin VB.Label Label1 
                  Caption         =   "PR03NUMDIASENOCU"
                  Height          =   255
                  Left            =   -74040
                  TabIndex        =   89
                  Top             =   1320
                  Visible         =   0   'False
                  Width           =   1935
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cada"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   29
                  Left            =   -74040
                  TabIndex        =   88
                  Top             =   720
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "d�a(s)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   -73320
                  TabIndex        =   87
                  Top             =   1080
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "PR03NUMSEMENOCU"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   26
                  Left            =   -73440
                  TabIndex        =   86
                  Top             =   600
                  Visible         =   0   'False
                  Width           =   2055
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cada"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   31
                  Left            =   -74160
                  TabIndex        =   85
                  Top             =   840
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "semana(s)    el"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   30
                  Left            =   -72840
                  TabIndex        =   84
                  Top             =   960
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Indicaciones"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   27
                  Left            =   -74640
                  TabIndex        =   60
                  Top             =   480
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Observaciones"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   28
                  Left            =   -74760
                  TabIndex        =   59
                  Top             =   480
                  Width           =   1455
               End
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "PR03FECFINAL"
               Height          =   330
               Index           =   2
               Left            =   8160
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "Fecha de Fin de la Actuaci�n"
               Top             =   1920
               Visible         =   0   'False
               Width           =   1800
               _Version        =   65537
               _ExtentX        =   3175
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "PR03FECINICIAL"
               Height          =   330
               Index           =   1
               Left            =   8160
               TabIndex        =   21
               TabStop         =   0   'False
               Tag             =   "Fecha de Inicio de la Actuaci�n"
               Top             =   1320
               Visible         =   0   'False
               Width           =   1800
               _Version        =   65537
               _ExtentX        =   3175
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Final"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   8160
               TabIndex        =   106
               Top             =   1680
               Visible         =   0   'False
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Inicial"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   8160
               TabIndex        =   105
               Top             =   1080
               Visible         =   0   'False
               Width           =   1215
            End
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR03NUMACPEDI_PRI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   19
            Left            =   9960
            TabIndex        =   43
            TabStop         =   0   'False
            Tag             =   "Actuaci�n Asociada"
            Top             =   1680
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR03CODTIPITERAC"
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   9240
            Locked          =   -1  'True
            TabIndex        =   27
            TabStop         =   0   'False
            Tag             =   "Tipo de Interacci�n de la Ocurrencia|Tipo de Interacci�n"
            Top             =   1680
            Visible         =   0   'False
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR03NUMACTPEDI"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   1
            Tag             =   "N� Actuaci�n Pedida|N�mero de Actuaci�n Pedida"
            Top             =   360
            Width           =   1455
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR09NUMPETICION"
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   360
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "N�mero de Petici�n"
            Top             =   960
            Width           =   852
         End
         Begin VB.CommandButton cmdcuestionario 
            Caption         =   "Cuestionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   9240
            TabIndex        =   24
            Top             =   2040
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�Consentimiento Firmado?"
            DataField       =   "PR03INDCONSFIRM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   7920
            TabIndex        =   5
            Tag             =   "�Necesita Consentimiento Firmado?"
            Top             =   120
            Width           =   2595
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6705
            Index           =   0
            Left            =   -74760
            TabIndex        =   50
            TabStop         =   0   'False
            Top             =   240
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   11827
            _StockProps     =   79
            Caption         =   "ACTUACIONES PEDIDAS"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   2625
            Index           =   1
            Left            =   360
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "Indicaciones"
            Top             =   1440
            Width           =   8655
            _ExtentX        =   15266
            _ExtentY        =   4630
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "PR0149.frx":00D0
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(1)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(14)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(13)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(17)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(18)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(19)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(20)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(25)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(31)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(24)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(30)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(5)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(26)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(27)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(28)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(29)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).ControlCount=   16
            TabCaption(1)   =   "Departamento"
            TabPicture(1)   =   "PR0149.frx":00EC
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(11)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "txtText1(1)"
            Tab(1).Control(2)=   "lblLabel1(9)"
            Tab(1).Control(3)=   "lblLabel1(8)"
            Tab(1).ControlCount=   4
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   29
               Left            =   1800
               TabIndex        =   125
               TabStop         =   0   'False
               Tag             =   "N� Historia"
               Top             =   840
               Width           =   852
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   28
               Left            =   6720
               TabIndex        =   124
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   840
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   27
               Left            =   4800
               TabIndex        =   123
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   840
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   26
               Left            =   2880
               TabIndex        =   122
               TabStop         =   0   'False
               Tag             =   "Nombre del Paciente"
               Top             =   840
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               HelpContextID   =   30101
               Index           =   5
               Left            =   240
               TabIndex        =   121
               Tag             =   "C�digo Persona|Persona"
               Top             =   840
               Width           =   852
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30101
               Index           =   30
               Left            =   720
               TabIndex        =   120
               TabStop         =   0   'False
               Tag             =   "Descripci�n Entidad Financiera"
               Top             =   1440
               Width           =   6000
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI13CODENTIDAD"
               Height          =   330
               HelpContextID   =   30104
               Index           =   24
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   119
               TabStop         =   0   'False
               Tag             =   "C�digo de la Entidad|Entidad"
               Top             =   1440
               Width           =   400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30101
               Index           =   31
               Left            =   720
               TabIndex        =   118
               TabStop         =   0   'False
               Tag             =   "Descripci�n Tipo Econ�mico"
               Top             =   2040
               Width           =   6000
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI32CODTIPECON"
               Height          =   330
               HelpContextID   =   30104
               Index           =   25
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   117
               TabStop         =   0   'False
               Tag             =   "Tipo Econ�mico"
               Top             =   2040
               Width           =   400
            End
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   11
               Left            =   -72600
               TabIndex        =   114
               TabStop         =   0   'False
               Tag             =   "Descripci�n de Departamento"
               Top             =   1080
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               HelpContextID   =   30101
               Index           =   1
               Left            =   -74640
               TabIndex        =   113
               Tag             =   "C�digo Departamento|Departamento"
               Top             =   1080
               Width           =   372
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   2865
               Index           =   1
               Left            =   -74910
               TabIndex        =   55
               TabStop         =   0   'False
               Top             =   90
               Width           =   8655
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15266
               _ExtentY        =   5054
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   1800
               TabIndex        =   132
               Top             =   600
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   6720
               TabIndex        =   131
               Top             =   600
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   4800
               TabIndex        =   130
               Top             =   600
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   2880
               TabIndex        =   129
               Top             =   600
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   240
               TabIndex        =   128
               Top             =   600
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Entidad Financiera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   240
               TabIndex        =   127
               Top             =   1200
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tipo Econ�mico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   126
               Top             =   1800
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   -72600
               TabIndex        =   116
               Top             =   840
               Width           =   2295
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d.Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   -74640
               TabIndex        =   115
               Top             =   840
               Width           =   1590
            End
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR48CODURGENCIA"
            Height          =   330
            Index           =   0
            Left            =   2040
            TabIndex        =   112
            Tag             =   "C�digo de la Urgencia"
            Top             =   960
            Width           =   735
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   873
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3175
            Columns(1).Caption=   "URGENCIA"
            Columns(1).Name =   "URGENCIA"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1296
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dpto de Cargo (Inter�s Cient�fico)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   7320
            TabIndex        =   109
            Top             =   960
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo de Urgencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   2040
            TabIndex        =   108
            Top             =   720
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   2040
            TabIndex        =   95
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Primera Act"
            Height          =   255
            Index           =   23
            Left            =   9960
            TabIndex        =   57
            Top             =   1560
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo Inter"
            Height          =   255
            Index           =   10
            Left            =   9120
            TabIndex        =   56
            Top             =   1440
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�m.Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   24
            Left            =   360
            TabIndex        =   54
            Top             =   720
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N�m.Act.Pedida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   360
            TabIndex        =   52
            Top             =   120
            Width           =   1380
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   51
      Top             =   6585
      Width           =   11670
      _ExtentX        =   20585
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmdatosactpedida"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00149.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 20 DE OCTUBRE DE 1997                                         *
'* DESCRIPCI�N: Pantalla para modificar las actuaciones pedidas de      *
'*              una petici�n,responder a sus cuestionarios y elegir     *
'*              los recursos espec�ficos                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
'strfecahaux guarda la fecha
Dim strfechaaux As String
'intaux controla la fecha y hora
Dim intaux As Integer
'intleer sirve para diferenciar si las cajas de texto cambian al entrar en
'un registro (intleer=1) o porque las cambia el usuario (intleer=0)
Dim intleer As Integer
'intactcambiada sirve para detectar si ha cambiado alguna de las cajas de texto
'o datecombo que hace que se vuelvan a lanzar las actuaciones planificadas
Dim intactcambiada As Integer
'intdatocorrecto controla si los datos metidos son correctos o no
Dim intdatocorrecto As Integer
'intquitargrid controla que el grid de Tabla est� desactivado si los datos
'para lanzar la planificaci�n no est�n metidos correctamente
Dim intquitargrid As Integer
'intopt sirve para distinguir cuando los optionbutton cambian porque se pinchan
'o cuando se ejecuta PostRead
Dim intopt As Integer
'intsalir controla si para salir se pulsa el bot�n o men� o se cierra la pantalla
Dim intsalir As Integer
'mblnFechaModif sirve para saver si el usuario ha modificado la fecha de preferencia con el spin o con el teclado
Dim mblnFechaModif As Boolean
'intactivoguardar controla que al volver del cuestionario si el bot�n Guardar estaba
'activado que se active
Dim intactivoguardar As Integer
'pinchargrid detecta si se hace clic o doble clic en el grid
Dim pinchargrid As Integer
'fila recoge la fila del grid que corresponde con el registro del detalle
Dim fila As Variant
'intcambioalgo detecta si se cambia la fecha,hora o minutos
Dim intcambioalgo As Integer
'intminuto detecta si los minutos se meten tecleando directamente sobre txtminuto para
'que si se mete 1 minuto no pase a la oculta 01 y de la oculta otra vez a txtminuto 01
Dim intminuto As Integer






Private Sub cmdcuestionario_Click()
  Dim rsta As rdoResultset
  Dim strsqlA As String
  Dim rstM As rdoResultset
  Dim strsqlM As String
  Dim intResp As Integer
  Dim strSelect As String
  
  Dim strtiporec As String
  Dim rsttiporec As rdoResultset
  Dim strrest As String
  Dim rstrest As rdoResultset
  Dim numrestricciones As Integer
  
  
  
  
  'antes de contestar el cuestionario se guarda en una variable global si
  'el bt�n Guardar est� activado o no para dejarlo en el mismo estado al
  'volver de contestar el cuestionario.
  
 'If objWinInfo.objWinActiveForm.blnChanged = True Then
   intactivoguardar = 1
  'Else
   'intactivoguardar = 0
  'End If
  
    gblncuestionario2 = 0
    cmdcuestionario.Enabled = False

    ' se mira si la actuaci�n tiene cuestionario
    strsqlA = "select count(*) from PR4100 where pr03numactpedi=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)

    'se mira si la actuaci�n tiene muestras con cuestionario
    'strsqlM = "select count(*) from PR4300 where pr03numactpedi=" & txtText1(0).Text
    'Set rstM = objApp.rdoConnect.OpenResultset(strsqlM)
    
    'se miran los tipos de recurso asociados a la actuaci�n pedida
    numrestricciones = 0
    strtiporec = "select ag14codtiprecu from PR1400 where pr03numactpedi=" & txtText1(0).Text
    Set rsttiporec = objApp.rdoConnect.OpenResultset(strtiporec)
    While Not rsttiporec.EOF
          'se mira para cada tipo de recurso si tiene alguna restricci�n
          strrest = "select count(*) from AG1500 " & _
                  "where ag14codtiprecu=" & rsttiporec.rdoColumns(0).Value
          Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
          numrestricciones = numrestricciones + rstrest.rdoColumns(0).Value
    rsttiporec.MoveNext
    rstrest.Close
    Set rstrest = Nothing
    Wend
    rsttiporec.Close
    Set rsttiporec = Nothing
          
    'la actuaci�n tiene cuestionario y no tiene ning�n tipo de recurso con restricci�n
    If (rsta.rdoColumns(0).Value > 0) And (numrestricciones = 0) Then
          Call objsecurity.LaunchProcess("PR0145")
    End If
    
    'la actuaci�n no tiene cuestionario pero tiene tipos de recurso con restricciones
     If (rsta.rdoColumns(0).Value = 0) And (numrestricciones > 0) Then
          Call objsecurity.LaunchProcess("PR0198")
    End If
    
    'la actuaci�n tiene cuestionario y tiene tipos de recurso con restricciones
     If (rsta.rdoColumns(0).Value > 0) And (numrestricciones > 0) Then
          Call objsecurity.LaunchProcess("PR0212")
    End If
    numrestricciones = 0
    '*************************************************************************
    'la actuaci�n tiene cuestionario y no tiene muestras con cuestionario
    'If (rsta.rdoColumns(0).Value > 0 And rstM.rdoColumns(0).Value = 0) Then
    '  Call objsecurity.LaunchProcess("PR0145")
    'End If
    'la actuaci�n tiene cuestionario y muestras con cuestionario
    'If (rsta.rdoColumns(0).Value > 0 And rstM.rdoColumns(0).Value > 0) Then
    '  gblncuestionario2 = 0
    '  Call objsecurity.LaunchProcess("PR0146")
    'End If

    'la actuaci�n no tiene cuestionario y tiene muestras con cuestionario
    'If (rsta.rdoColumns(0).Value = 0 And rstM.rdoColumns(0).Value > 0) Then
    '  Call objsecurity.LaunchProcess("PR0147")
    'End If
    '**************************************************************************
  

  rsta.Close
  Set rsta = Nothing
  'rstM.Close
  'Set rstM = Nothing
  cmdcuestionario.Enabled = True
  Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub



Private Sub cmdinterplani_Click()
  Dim intResp As Integer
  Dim rsta As rdoResultset
  Dim strSelect As String
   
   'JRC 30-6-98
   intactivoguardar = 1
  
  
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
  
  'If Fecha_Mayor(dtcfecha.Text, dtcfecha.Text, rstA.rdoColumns(0).Value, _
                 txthora.Text, txtminuto.Text, True, True) Then
    cmdinterplani.Enabled = False
    'la actuaci�n pedida ser� la actuaci�n destino en la pantalla de interacciones
    frminterplani.txtText1(2) = txtText1(0)
    'Load frminterplani
    'Call frminterplani.Show(vbModal)
    Call objsecurity.LaunchProcess("PR0161")
    'Unload frminterplani
    'Set frminterplani = Nothing
    cmdinterplani.Enabled = True
  'Else
    'intResp = MsgBox("La FECHA DE PREFERENCIA ha pasado", vbInformation, "Importante")
  'End If
rsta.Close
Set rsta = Nothing
End Sub

Private Sub cmdrecursos_Click()
  Dim intResp As Integer
  Dim rsta As rdoResultset
  Dim strSelect As String
  
   'JRC 30-6-98
   intactivoguardar = 1
  
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
  
  'If Fecha_Mayor(dtcfecha.Text, dtcfecha.Text, rstA.rdoColumns(0).Value, _
                 txthora.Text, txtminuto.Text, True, True) Then
    cmdrecursos.Enabled = False
    'objWinInfo.DataSave
    'Load frmrecursos
    'Call frmrecursos.Show(vbModal)
    Call objsecurity.LaunchProcess("PR0150")
    'Unload frmrecursos
    'Set frmrecursos = Nothing
    cmdrecursos.Enabled = True
  'Else
    'intResp = MsgBox("La FECHA DE PREFERENCIA ha pasado", vbInformation, "Importante")
  'End If
rsta.Close
Set rsta = Nothing
End Sub
Private Function cuestionario_no_respondido() As Integer
  Dim rsta As rdoResultset
  Dim strsqlA As String
  Dim intoblig As Integer
  Dim intcodres As Integer
  Dim strrespuesta As String
  Dim intnosalir As Integer
  Dim actpedi As Long
  Dim cont As Integer
  '---------------
  Dim numrestricciones As Integer
  Dim strtiporec As String
  Dim rsttiporec As rdoResultset
  Dim strrest As String
  Dim rstrest As rdoResultset
  Dim strresp As String
  Dim rstresp As rdoResultset
  
  
   On Error GoTo Err_Ejecutar
   intnosalir = 0
   actpedi = txtText1(0)
   ' se mira si la actuaci�n tiene cuestionario
   strsqlA = "select count(*) from PR4100 where pr03numactpedi=" & actpedi
   Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
   cont = rsta.rdoColumns(0).Value
   rsta.Close
   Set rsta = Nothing
   'si la actuaci�n tiene cuestionario
   If (cont > 0) Then
    strsqlA = "select PR27CODTIPRESPU,PR41INDROBLIG,PR41RESPUESTA from PR4100 " & _
              "where pr03numactpedi=" & actpedi
    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
    Do While Not rsta.EOF
     intcodres = rsta.rdoColumns(0).Value
     intoblig = rsta.rdoColumns(1).Value
     If IsNull(rsta.rdoColumns(2).Value) Then
       strrespuesta = ""
     Else
       strrespuesta = rsta.rdoColumns(2).Value
     End If
     If (intoblig = -1 And strrespuesta = "") Then
       intnosalir = 1
       Exit Do
     End If
     rsta.MoveNext
    Loop
   rsta.Close
   Set rsta = Nothing
   End If
   
   '--------------------------------------------------------------------
   'se miran los tipos de recurso asociados a la actuaci�n pedida
    numrestricciones = 0
    strtiporec = "select ag14codtiprecu from PR1400 where pr03numactpedi=" & actpedi
    Set rsttiporec = objApp.rdoConnect.OpenResultset(strtiporec)
    Do While Not rsttiporec.EOF
          'se mira para cada tipo de recurso si tiene alguna restricci�n
          strrest = "select count(*) from AG1500 " & _
                  "where ag14codtiprecu=" & rsttiporec.rdoColumns(0).Value
          Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
          numrestricciones = rstrest.rdoColumns(0).Value
          If numrestricciones > 0 Then  'el tipo de recurso tiene restricciones
             strrest = "select AG16CODTIPREST from AG1500 " & _
                  "where ag14codtiprecu=" & rsttiporec.rdoColumns(0).Value
             Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
             Do While Not rstrest.EOF
                   strresp = "SELECT pr47valor FROM PR4700 WHERE " & _
                            "pr09numpeticion=" & txtText1(12).Text & " AND " & _
                            "ag16codtiprest=" & rstrest.rdoColumns("ag16codtiprest").Value
                   Set rstresp = objApp.rdoConnect.OpenResultset(strresp)
                   If Trim(rstresp.rdoColumns("pr47valor").Value) = "" Then
                      intnosalir = 1
                      rstresp.Close
                      Set rstresp = Nothing
                      Exit Do
                   End If
             rstrest.MoveNext
             rstresp.Close
             Set rstresp = Nothing
             Loop
          End If
       rstrest.Close
       Set rstrest = Nothing
       rsttiporec.MoveNext
       Loop
       rsttiporec.Close
       Set rsttiporec = Nothing
 
   
   '--------------------------------------------------------------------
   
  '********************SIN MUESTRAS ESTO NO DEBE EJECUTARSE***************
   ''se mira si la actuaci�n tiene muestras con cuestionario
   'strsqlA = "select count(*) from PR4300 where pr03numactpedi=" & actpedi
   'Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
   'cont = rsta.rdoColumns(0).Value
   'rsta.Close
   'Set rsta = Nothing
   ''si la actuaci�n tiene muestras con cuestionario
   'If (cont > 0) Then
   ' strsqlA = "select PR27CODTIPRESPU,PR43INDOBLIG,PR43RESPUESTA from PR4300 " & _
              "where pr03numactpedi=" & actpedi
    'Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
    'Do While Not rsta.EOF
    ' intcodres = rsta.rdoColumns(0).Value
    ' intoblig = rsta.rdoColumns(1).Value
    ' If IsNull(rsta.rdoColumns(2).Value) Then
    '   strrespuesta = ""
    ' Else
     '  strrespuesta = rsta.rdoColumns(2).Value
    ' End If
    ' If (intoblig = -1 And strrespuesta = "") Then
    '   intnosalir = 1
    '   Exit Do
    ' End If
    ' rsta.MoveNext
    'Loop
    'rsta.Close
   ' Set rsta = Nothing
   'End If
   '*************************************************************************
   
   
   cuestionario_no_respondido = intnosalir

Exit Function

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Function
End Function
Private Function rellenar_datos() As Integer
Dim strmensaje As String
'funci�n que controla que para cada actuaci�n pedida se metan los campos
'necesarios para planificar
rellenar_datos = 0
Select Case txtText1(13).Text
 Case ""
   If txtText1(23).Text = "" And chkCheck1(8).Value = True Then        'jcr 27/2/98
      If intquitargrid = 0 Then
      strmensaje = MsgBox("Para lanzar la planificaci�n debe insertar una Fecha de Preferencia.", _
              vbCritical, "Datos Incompletos")
      End If
      intactcambiada = 1
      rellenar_datos = 1
   End If
 Case "D"
   If txtText1(10).Text = "" Then
      If intquitargrid = 0 Then
      strmensaje = MsgBox("Para lanzar la planificaci�n Diaria debe especificar " & _
                     "el n�mero de d�as entre actuaciones.", _
                      vbCritical, "Datos Incompletos")
      End If
      intactcambiada = 1
      rellenar_datos = 1
   End If
Case "S"
   If txtText1(20).Text = "" Then
      If intquitargrid = 0 Then
      strmensaje = MsgBox("Para lanzar la planificaci�n Semanal debe especificar " & _
                     "el n�mero de semanas entre actuaciones.", _
                      vbCritical, "Datos Incompletos")
      End If
      intactcambiada = 1
      rellenar_datos = 1
   End If
Case "M"
   If optOption1(0).Value = True Then
     If (Text1(2).Text = "" Or Text1(0).Text = "") Then
      If intquitargrid = 0 Then
      strmensaje = MsgBox("Para lanzar la planificaci�n Mensual debe especificar " & _
                     "el d�a y mes de las ocurrencias ", _
                      vbCritical, "Datos Incompletos")
      End If
      Text1(2).Text = ""
      Text1(0).Text = ""
      intactcambiada = 1
      rellenar_datos = 1
     End If
   End If
   If optOption1(1).Value = True Then
      If (txtcombo(0).Text = "" Or txtcombo(1).Text = "" Or Text1(1).Text = "") Then
      If intquitargrid = 0 Then
      strmensaje = MsgBox("Para lanzar la planificaci�n Mensual debe especificar " & _
                     "el d�a y mes de las ocurrencias ", _
                      vbCritical, "Datos Incompletos")
       End If
       txtcombo(0).Text = ""
        txtcombo(1).Text = ""
         Text1(1).Text = ""
         intactcambiada = 1
      rellenar_datos = 1
     End If
   End If
   If (optOption1(1).Value = False And optOption1(0).Value = False) Then
      If intquitargrid = 0 Then
      strmensaje = MsgBox("Para lanzar la planificaci�n Mensual debe especificar " & _
                     "el d�a y mes de las ocurrencias ", _
                      vbCritical, "Datos Incompletos")
      End If
      intactcambiada = 1
      rellenar_datos = 1
   End If
Case "A"
   If optOption2(0).Value = True Then
     If (Text1(3).Text = "" Or txtcombo(2).Text = "" Or Text1(4).Text = "") Then
      If intquitargrid = 0 Then
      strmensaje = MsgBox("Para lanzar la planificaci�n Anual debe especificar " & _
                     "el d�a, mes y a�o de las ocurrencias ", _
                      vbCritical, "Datos Incompletos")
      End If
      Text1(3).Text = ""
      txtcombo(2).Text = ""
      Text1(4).Text = ""
      intactcambiada = 1
      rellenar_datos = 1
     End If
   End If
   If optOption2(1).Value = True Then
      If (txtcombo(3).Text = "" Or txtcombo(4).Text = "" Or txtcombo(5).Text = "" _
           Or Text1(5).Text = "") Then
      If intquitargrid = 0 Then
      strmensaje = MsgBox("Para lanzar la planificaci�n Anual debe especificar " & _
                     "el d�a, mes y a�o de las ocurrencias ", _
                      vbCritical, "Datos Incompletos")
      End If
      txtcombo(3).Text = ""
      txtcombo(4).Text = ""
      txtcombo(5).Text = ""
      Text1(5).Text = ""
      intactcambiada = 1
      rellenar_datos = 1
     End If
   End If
   If (optOption2(0).Value = False And optOption2(1).Value = False) Then
    If intquitargrid = 0 Then
    strmensaje = MsgBox("Para lanzar la planificaci�n Anual debe especificar " & _
                     "el d�a, mes y a�o de las ocurrencias ", _
                      vbCritical, "Datos Incompletos")
     End If
      intactcambiada = 1
      rellenar_datos = 1
   End If
End Select
End Function

Private Sub Lanzar_Planificacion()
'procedimiento que por cada actuaci�n pedida lanza las correspondientes
'actuaciones planificadas
Dim iniciodia As Date
Dim findia As Date
Dim rs As rdoResultset
Dim strHoraSistema As String



'se coge la fecha del sistema
Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
strHoraSistema = rs.rdoColumns(0)
rs.Close
Set rs = Nothing

If dtcDateCombo1(1).Text = "" Or IsNull(dtcDateCombo1(1).Text) Then
   iniciodia = strHoraSistema
Else
  iniciodia = dtcDateCombo1(1).Text
End If

If dtcDateCombo1(2).Text = "" Or IsNull(dtcDateCombo1(2).Text) Then
   findia = DateAdd("yyyy", 1, iniciodia)
Else
  findia = dtcDateCombo1(2).Text
End If

'GENERAR PLANIFICADAS
'se llama a procedimientos que est�n en un m�dulo
 If rellenar_datos = 1 Then
  Exit Sub
 End If
 
'se controla que se cancele la actuaci�n o no
'Call borrar_del_todo(txtText1(0).Text)
Call borrar(txtText1(0).Text)

Select Case txtText1(13).Text
 Case ""
    If (txtText1(23).Text = "") Then
     Call insertarpreferencia(0)
    Else
     Call insertarpreferencia(txtText1(23).Text)
    End If
 Case "D"
    Call diariamente(iniciodia, findia, txtText1(10))
 Case "S"
    Call semanalmente(iniciodia, findia, txtText1(20), chkCheck1(1).Value, _
                       chkCheck1(2).Value, chkCheck1(3).Value, chkCheck1(4).Value, _
                       chkCheck1(5).Value, chkCheck1(6).Value, chkCheck1(7).Value)
 Case "M"
    If (optOption1(0).Value = True) Then
      Call mensualmente_1(iniciodia, findia, txtText1(15), txtText1(14))
    End If
    If (optOption1(1).Value = True) Then
      Call mensualmente_2(iniciodia, findia, txtText1(17), _
                          txtText1(16), txtText1(14))
    End If
Case "A"
    If (optOption2(0).Value = True) Then
      Call anualmente_1(iniciodia, findia, txtText1(15), _
                         txtText1(3), txtText1(18))
    End If
    If (optOption2(1).Value = True) Then
      Call anualmente_2(iniciodia, findia, txtText1(17), _
                         txtText1(16), txtText1(3), txtText1(18))
    End If

End Select
End Sub



Private Sub dtcDateCombo1_KeyPress(Index As Integer, KeyAscii As Integer)
If (Index = 1 Or Index = 2) Then
     grdDBGrid1(0).Enabled = False
End If
End Sub

Private Sub dtcfecha_Change()
 Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  grdDBGrid1(0).Enabled = False
  
      intcambioalgo = 1
      intaux = 0
      If dtcfecha.Text <> "" Then
        If (txtHora.Text <> "" And txtMinuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txtHora.Text & ":" & txtMinuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
End Sub

Private Sub dtcfecha_CloseUp()
  Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  grdDBGrid1(0).Enabled = False
  intcambioalgo = 1
  intaux = 0
  
  blnFechaValida = True
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
 
  If dtcfecha.Text <> "" Then
    'Obtenemos el a�o seleccionado y lo ponemos con 4 digitos si procede
    strAnyoSelAux = Format(dtcfecha.Text, "yyyy")
    If Len(strAnyoSelAux) = 2 Then
      strAnyoSelAux = "19" & strAnyoSelAux
    End If
    intAnyoSelAux = strAnyoSelAux
    
    'Obtenemos el a�o del systema y lo ponemos con 4 d�gitos si procede
    strAnyoSysAux = Format(rsta.rdoColumns(0).Value, "yyyy")
    If Len(strAnyoSysAux) = 2 Then
      strAnyoSysAux = "19" & strAnyoSysAux
    End If
    intAnyoSysAux = strAnyoSysAux
    
    'Obtenemos el mes seleccionado
    strMesSelAux = Format(dtcfecha.Text, "mm")
    intMesSelAux = strMesSelAux
    
    'Obtenemos el mes sel systema
    strMesSysAux = Format(rsta.rdoColumns(0).Value, "mm")
    intMesSysAux = strMesSysAux
    
    'Obtenemos el d�a seleccionado
    strDiaSelAux = Format(dtcfecha.Text, "dd")
    intDiaSelAux = strDiaSelAux
    
    'Obtenemos el d�a del systema
    strDiaSysAux = Format(rsta.rdoColumns(0).Value, "dd")
    intDiaSysAux = strDiaSysAux
        
    If intAnyoSelAux < intAnyoSysAux Then
      blnFechaValida = False
    Else
      If intAnyoSelAux = intAnyoSysAux Then
        If intMesSelAux < intMesSysAux Then
          blnFechaValida = False
        Else
          If intMesSelAux = intMesSysAux Then
            If intDiaSelAux < intDiaSysAux Then
              blnFechaValida = False
            Else
              If (intDiaSelAux = intDiaSysAux) And (txtHora.Text <> "") Then
                If CDbl(txtHora.Text) < CDbl(Format(rsta.rdoColumns(0).Value, "hh")) Then
                  blnFechaValida = False
                Else
                  If CDbl(txtHora.Text) = CDbl(Format(rsta.rdoColumns(0).Value, "hh")) _
                     And (txtMinuto.Text <> "") Then
                    If CDbl(txtMinuto.Text) <= CDbl(Format(rsta.rdoColumns(0).Value, "nn")) Then
                      blnFechaValida = False
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    
    
    If blnFechaValida = False Then
      intResp = MsgBox("La FECHA DE PREFERENCIA ha pasado", vbInformation, "Importante")
      dtcfecha.Text = ""
    Else
      If dtcfecha.Text <> "" Then
        If (txtHora.Text <> "" And txtMinuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txtHora.Text & ":" & txtMinuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If
  rsta.Close
  Set rsta = Nothing

End Sub


Private Sub dtcfecha_KeyPress(KeyAscii As Integer)
  mblnFechaModif = True
  intaux = 0
End Sub

Private Sub dtcfecha_Lostfocus()
  Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  
grdDBGrid1(0).Enabled = False

If mblnFechaModif = True Then
  blnFechaValida = True
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
 
  If dtcfecha.Text <> "" Then
    'Obtenemos el a�o seleccionado y lo ponemos con 4 digitos si procede
    strAnyoSelAux = Format(dtcfecha.Text, "yyyy")
    If Len(strAnyoSelAux) = 2 Then
      strAnyoSelAux = "19" & strAnyoSelAux
    End If
    intAnyoSelAux = strAnyoSelAux
    
    'Obtenemos el a�o del systema y lo ponemos con 4 d�gitos si procede
    strAnyoSysAux = Format(rsta.rdoColumns(0).Value, "yyyy")
    If Len(strAnyoSysAux) = 2 Then
      strAnyoSysAux = "19" & strAnyoSysAux
    End If
    intAnyoSysAux = strAnyoSysAux
    
    'Obtenemos el mes seleccionado
    strMesSelAux = Format(dtcfecha.Text, "mm")
    intMesSelAux = strMesSelAux
    
    'Obtenemos el mes sel systema
    strMesSysAux = Format(rsta.rdoColumns(0).Value, "mm")
    intMesSysAux = strMesSysAux
    
    'Obtenemos el d�a seleccionado
    strDiaSelAux = Format(dtcfecha.Text, "dd")
    intDiaSelAux = strDiaSelAux
    
    'Obtenemos el d�a del systema
    strDiaSysAux = Format(rsta.rdoColumns(0).Value, "dd")
    intDiaSysAux = strDiaSysAux
        
    If intAnyoSelAux < intAnyoSysAux Then
      blnFechaValida = False
    Else
      If intAnyoSelAux = intAnyoSysAux Then
        If intMesSelAux < intMesSysAux Then
          blnFechaValida = False
        Else
          If intMesSelAux = intMesSysAux Then
            If intDiaSelAux < intDiaSysAux Then
              blnFechaValida = False
            Else
              If (intDiaSelAux = intDiaSysAux) And (txtHora.Text <> "") Then
                If CDbl(txtHora.Text) < CDbl(Format(rsta.rdoColumns(0).Value, "hh")) Then
                  blnFechaValida = False
                Else
                  If CDbl(txtHora.Text) = CDbl(Format(rsta.rdoColumns(0).Value, "hh")) _
                     And (txtMinuto.Text <> "") Then
                    If CDbl(txtMinuto.Text) <= CDbl(Format(rsta.rdoColumns(0).Value, "nn")) Then
                      blnFechaValida = False
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    
    
    If blnFechaValida = False Or blnFechaValida = True Then
      'intResp = MsgBox("La FECHA DE PREFERENCIA ha pasado", vbInformation, "Importante")
       'Else
      If dtcfecha.Text <> "" Then
        If (txtHora.Text <> "" And txtMinuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txtHora.Text & ":" & txtMinuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          If intcambioalgo = 1 Then
              objWinInfo.objWinActiveForm.blnChanged = True
          End If
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          fecha = dtcfecha.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          If intcambioalgo = 1 Then
            objWinInfo.objWinActiveForm.blnChanged = True
          End If
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  
End If
mblnFechaModif = False
End Sub

Private Sub dtcfecha_Spin(OldDate As String, NewDate As String)
  mblnFechaModif = True
End Sub

Private Sub Form_Activate()
tlbToolbar1.Buttons(4).Enabled = True
objWinInfo.objWinActiveForm.blnChanged = False
'se chequea la variable intactivoguardar que nos dice si antes de pasar a contestar
'el cuestionario el bot�n Guardar estaba activo o no.
If intactivoguardar = 1 Then
  intactivoguardar = 0
  tlbToolbar1.Buttons(4).Enabled = True
  objWinInfo.objWinActiveForm.blnChanged = True
End If
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
  pinchargrid = 1
End Sub

Private Sub grdDBGrid1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
fila = grdDBGrid1(0).Bookmark
pinchargrid = 1
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Dim strconsen As String
Dim rstconsen As rdoResultset



  'si la actuaci�n no necesita consentimiento firmado que no visualice la chec donde
  'se indica si el paciente lo ha firmado o no.
    If txtText1(6).Text <> "" Then
      strconsen = "SELECT pr01indconsfdo FROM PR0100 WHERE pr01codactuacion=" & txtText1(6).Text
      Set rstconsen = objApp.rdoConnect.OpenResultset(strconsen)
      If rstconsen.rdoColumns(0).Value = 0 Then
        chkCheck1(0).Visible = False
      Else
        chkCheck1(0).Visible = True
      End If
      rstconsen.Close
      Set rstconsen = Nothing
    End If
  

  grdDBGrid1(0).Enabled = True
  If (chkCheck1(9).Value = 0) Then
    objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
    txtText1(2).BackColor = &HFFFFFF
  End If
        
  
  intleer = 0
  'para que se active el Optionbutton correspondiente
  If (Text1(2) <> "" And Text1(0) <> "") Then
    intopt = 1
    optOption1(0).Value = True
  End If
  If (cboCombo1(0) <> "" And cboCombo1(1) <> "" And Text1(1) <> "") Then
    intopt = 1
    optOption1(1).Value = True
  End If
  If (Text1(3) <> "" And cboCombo1(2) <> "" And Text1(4) <> "") Then
    intopt = 1
    optOption2(0).Value = True
  End If
  If (cboCombo1(3) <> "" And cboCombo1(4) <> "" And cboCombo1(5) <> "" And _
    Text1(5) <> "") Then
    intopt = 1
    optOption2(1).Value = True
  End If
  Select Case txtText1(13).Text
     Case ""
     tabTab1(2).Tab = 0
     fraframe1(1).Caption = "La actuaci�n ocurre 1 vez"
     objWinInfo.objWinActiveForm.blnChanged = False
     dtcDateCombo1(1).Enabled = False
     dtcDateCombo1(1).Visible = False
     dtcDateCombo1(2).Enabled = False
     dtcDateCombo1(2).Visible = False
     lblLabel1(12).Enabled = False
     lblLabel1(16).Enabled = False
     lblLabel1(12).Visible = False
     lblLabel1(16).Visible = False
    Case "D"
     tabTab1(2).Tab = 1
     fraframe1(1).Caption = "La actuaci�n ocurre Diariamente"
     objWinInfo.objWinActiveForm.blnChanged = False
     dtcDateCombo1(1).Enabled = True
     dtcDateCombo1(1).Visible = True
     dtcDateCombo1(2).Enabled = True
     dtcDateCombo1(2).Visible = True
     lblLabel1(12).Enabled = True
     lblLabel1(16).Enabled = True
     lblLabel1(12).Visible = True
     lblLabel1(16).Visible = True
    Case "S"
     tabTab1(2).Tab = 2
     fraframe1(1).Caption = "La actuaci�n ocurre Semanalmente"
     objWinInfo.objWinActiveForm.blnChanged = False
     dtcDateCombo1(1).Enabled = True
     dtcDateCombo1(1).Visible = True
     dtcDateCombo1(2).Enabled = True
     dtcDateCombo1(2).Visible = True
     lblLabel1(12).Enabled = True
     lblLabel1(16).Enabled = True
     lblLabel1(12).Visible = True
     lblLabel1(16).Visible = True
    Case "M"
     tabTab1(2).Tab = 3
     fraframe1(1).Caption = "La actuaci�n ocurre Mensualmente"
     objWinInfo.objWinActiveForm.blnChanged = False
     dtcDateCombo1(1).Enabled = True
     dtcDateCombo1(1).Visible = True
     dtcDateCombo1(2).Enabled = True
     dtcDateCombo1(2).Visible = True
     lblLabel1(12).Enabled = True
     lblLabel1(16).Enabled = True
     lblLabel1(12).Visible = True
     lblLabel1(16).Visible = True
    Case "A"
     tabTab1(2).Tab = 4
     fraframe1(1).Caption = "La actuaci�n ocurre Anualmente"
     objWinInfo.objWinActiveForm.blnChanged = False
     dtcDateCombo1(1).Enabled = True
     dtcDateCombo1(1).Visible = True
     dtcDateCombo1(2).Enabled = True
     dtcDateCombo1(2).Visible = True
     lblLabel1(12).Enabled = True
     lblLabel1(16).Enabled = True
     lblLabel1(12).Visible = True
     lblLabel1(16).Visible = True
  End Select
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim strmodif As String
Dim strmensaje As String

Dim strpref As String
Dim rstpref As rdoResultset
Dim strmod As String
Dim strupd As String

Dim sqlInstrReal As String
Dim rstInstrReal As rdoResultset

grdDBGrid1(0).Enabled = True
intcambioalgo = 0
Call funcion

'se guarda en PR0800 el motivo,indicaciones y observaciones
'strmodif = "UPDATE PR0800 set pr08desindicac=" & "'" & txtText1(8).Text & "'" & "," & _
'           "pr08desobserv=" & "'" & txtText1(9).Text & "'" & "," & _
'           "pr08desmotpet=" & "'" & txtText1(7).Text & "'" & _
'           " where pr09numpeticion=" & txtText1(12).Text & _
'           " and pr03numactpedi=" & txtText1(0).Text & _
'           " AND pr08numsecuencia>=0"
'On Error GoTo Err_Ejecutar
'objApp.rdoConnect.Execute strmodif, 64
'objApp.rdoConnect.Execute "Commit", 64

'se mira si la actuaci�n es iterativa para poner la actuaci�n como no citable
'ya que Citas no trata las iterativas
If txtText1(13).Text = "D" Or txtText1(13).Text = "S" Or _
   txtText1(13).Text = "M" Or txtText1(13).Text = "A" Then
      strupd = "UPDATE PR0300 SET pr03indcitable=0,pr03indcitaant=0 where pr03numactpedi=" & txtText1(0).Text
      objApp.rdoConnect.Execute strupd, 64
      objApp.rdoConnect.Execute "Commit", 64
End If
'****************************************************************jcr4/8/98
  If chkCheck1(8).Value = 1 Then
      sqlInstrReal = "SELECT * FROM PR0100 WHERE PR01CODACTUACION=" & txtText1(6).Text
      Set rstInstrReal = objApp.rdoConnect.OpenResultset(sqlInstrReal)
      If rstInstrReal("pr01indinstrrea").Value = -1 Then
        sqlInstrReal = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & txtText1(0).Text & _
                       " AND PR04DESINSTREA IS NULL"
        Set rstInstrReal = objApp.rdoConnect.OpenResultset(sqlInstrReal)
        If rstInstrReal.EOF = True Then
          strupd = "UPDATE PR0300 SET pr03indcitable=-1 where pr03numactpedi=" & txtText1(0).Text
          objApp.rdoConnect.Execute strupd, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      End If
      rstInstrReal.Close
      Set rstInstrReal = Nothing
  Else
    strupd = "UPDATE PR0300 SET pr03indcitable=0 where pr03numactpedi=" & txtText1(0).Text
    objApp.rdoConnect.Execute strupd, 64
    objApp.rdoConnect.Execute "Commit", 64
  End If
'****************************************************************
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
  intleer = 1
  
End Sub

Private Sub txtcombo_Change(Index As Integer)
grdDBGrid1(0).Enabled = False
End Sub

Private Sub txthora_Change()
Dim fecha As String

grdDBGrid1(0).Enabled = False
  
  
If IsNumeric(txtHora.Text) = True Then
 If txtHora.Text > 23 Then
    Beep
    txtHora.Text = ""
 End If
  If dtcfecha.Text <> "" Then
     If (txtHora.Text <> "" And txtMinuto <> "") Then
       fecha = dtcfecha.Text & " " & txtHora.Text & ":" & txtMinuto.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
        objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     Else
       fecha = dtcfecha.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
        objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     End If
 End If
Else
 Beep
 txtHora.Text = ""
End If

'If txtminuto = "" Then
'   txtminuto = 0
'End If

End Sub

Private Sub txthora_KeyPress(KeyAscii As Integer)
intcambioalgo = 1
End Sub

Private Sub txtminuto_Change()
Dim fecha As String

grdDBGrid1(0).Enabled = False
  
If IsNumeric(txtMinuto.Text) = True Then
  If txtMinuto.Text > 59 Then
    Beep
    txtMinuto.Text = ""
  End If
  If dtcfecha.Text <> "" Then
     If (txtHora.Text <> "" And txtMinuto <> "") Then
       fecha = dtcfecha.Text & " " & txtHora.Text & ":" & txtMinuto.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
         objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     Else
       fecha = dtcfecha.Text
       Call objWinInfo.CtrlSet(txtText1(23), fecha)
       If intcambioalgo = 1 Then
          objWinInfo.objWinActiveForm.blnChanged = True
       End If
       tlbToolbar1.Buttons(4).Enabled = True
       intaux = 0
     End If
  End If
Else
  Beep
  txtMinuto.Text = ""
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Actuacion Pedida"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0300"
    'no debe dejar planificar aquellas actuaciones de PR0300 cuyo estado
    'sea distinto de 1=PEDIDA
    .strWhere = "((PR09NUMPETICION=" & frmpedir.txtText1(0).Text & _
                 " AND pr03numactpedi in (select pr03numactpedi from PR0400 " & _
                                         " where pr37codestado=1)" & _
                 " AND pr03numactpedi not in (select pr03numactpedi from PR0400 " & _
                                         " where pr37codestado<>1))" & _
                 " OR " & _
                 "(PR09NUMPETICION=" & frmpedir.txtText1(0).Text & _
                 " AND (pr03numactpedi not in (select pr03numactpedi from PR0400)))) and " & _
                 " (pr03numactpedi in " & gstrplanif & ")"
                 
                 
    .intAllowance = cwAllowModify
    
    Call .FormAddOrderField("PR03NUMACTPEDI", cwAscending)
    

    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Actuaciones Pedidas")
    Call .FormAddFilterWhere(strKey, "PR03INDCONSFIRM", "�Necesita Consentimiento Firmado?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03INDCITAANT", "�La actuaci�n es citable?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03FECPREFEREN", "Fecha de Preferencia", cwDate)
    Call .FormAddFilterWhere(strKey, "PR03CODTIPITERAC", "Tipo de Iteraci�n", cwString)
    Call .FormAddFilterWhere(strKey, "PR03NUMDIASENOCU", "N� de d�as entre ocurrencias", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR03FECINICIAL", "Fecha de Inicio de la Actuaci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "PR03FECFINAL", "Fecha de Fin de la Actuaci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "PRO3NUMSEMENOCU", "N� de semanas entre ocurrencias", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR03INDLUNES", "Iteraci�n semanal los Lunes", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03INDMARTES", "Iteraci�n semanal los Martes", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03INDMIERCOLES", "Iteraci�n semanal los Mi�rcoles", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03INDJUEVES", "Iteraci�n semanal los Jueves", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03INDVIERNES", "Iteraci�n semanal los Viernes", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03INDSABADO", "Iteraci�n semanal los S�bados", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03INDDOMINGO", "Iteraci�n semanal los Domingos", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR03NUMMESENOCU", "N� de meses entre ocurrencias", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR03NUMDIAMESOCU", "D�a del mes de la ocurrencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR03NUMDIASEMOCU", "D�a de la semana de la ocurrencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR03NUMANOENOCU", "N� de a�os entre ocurrencias", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR03NUMMESANOOCU", "N� del mes en el a�o", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR48CODURGENCIA", "C�digo de Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR03INDINTCIENTIF", "Indicador de Inter�s Cientifico", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO_CRG", "C�d. Dpto. de Cargo de Inter�s Cient�fico", cwNumeric)

 
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(txtText1(12)).blnReadOnly = True 'n�m.petici�n
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True  'dpto.
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True  'persona
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True 'entidad
    .CtrlGetInfo(txtText1(25)).blnReadOnly = True 'tipo econ�mico
    .CtrlGetInfo(chkCheck1(0)).blnReadOnly = True 'consentimiento firmado
        
        
    '.CtrlGetInfo(txtText1(4)).blnInFind = True 'desc.corta
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(8)).blnInFind = True
    
      
    .CtrlGetInfo(dtcfecha).blnNegotiated = False
    .CtrlGetInfo(txtHora).blnNegotiated = False
    .CtrlGetInfo(txtMinuto).blnNegotiated = False
    .CtrlGetInfo(Text1(0)).blnNegotiated = False
    .CtrlGetInfo(Text1(1)).blnNegotiated = False
    .CtrlGetInfo(Text1(3)).blnNegotiated = False
    .CtrlGetInfo(Text1(4)).blnNegotiated = False '*********
    .CtrlGetInfo(Text1(2)).blnNegotiated = False  '*********
    .CtrlGetInfo(Text1(5)).blnNegotiated = False
    .CtrlGetInfo(cboCombo1(0)).blnNegotiated = False
    .CtrlGetInfo(cboCombo1(1)).blnNegotiated = False
    .CtrlGetInfo(cboCombo1(2)).blnNegotiated = False
    .CtrlGetInfo(cboCombo1(3)).blnNegotiated = False
    .CtrlGetInfo(cboCombo1(4)).blnNegotiated = False
    .CtrlGetInfo(cboCombo1(5)).blnNegotiated = False
    .CtrlGetInfo(txtcombo(0)).blnNegotiated = False
    .CtrlGetInfo(txtcombo(1)).blnNegotiated = False
    .CtrlGetInfo(txtcombo(2)).blnNegotiated = False
    .CtrlGetInfo(txtcombo(3)).blnNegotiated = False
    .CtrlGetInfo(txtcombo(4)).blnNegotiated = False
    .CtrlGetInfo(txtcombo(5)).blnNegotiated = False
    
    '.CtrlGetInfo(txtText1(8)).blnInGrid = False
    '.CtrlGetInfo(txtText1(9)).blnInGrid = False
    .CtrlGetInfo(txtText1(19)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
   .CtrlGetInfo(txtText1(1)).blnInGrid = False
    .CtrlGetInfo(txtText1(5)).blnInGrid = False
    '.CtrlGetInfo(txtText1(7)).blnInGrid = False
    .CtrlGetInfo(txtText1(12)).blnInGrid = False
    .CtrlGetInfo(txtText1(13)).blnInGrid = False
    .CtrlGetInfo(txtText1(24)).blnInGrid = False
    .CtrlGetInfo(txtText1(25)).blnInGrid = False
    .CtrlGetInfo(txtText1(30)).blnInGrid = False
    .CtrlGetInfo(txtText1(31)).blnInGrid = False
    .CtrlGetInfo(chkCheck1(0)).blnInGrid = False
    
    '.CtrlGetInfo(txtText1(7)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(8)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(9)).blnReadOnly = True
    
    
    'Se activa el flag en txtText1(2) para que aparezca la lista en la barra de estado
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    
    'Creamos las ligaduras entre los campos txtText1(2), c�d. del dpto. al que se le carga la actuaci�n de inter�s cient�fico,
    ' y txtDesDptoCargo, descripci�n del dpto al que se le carga la actuaci�n
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO_CRG", "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtDesDptoCargo, "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "ad02coddpto", "SELECT ad02coddpto,ad02desdpto FROM AD0200 WHERE ad02coddpto = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(11), "ad02desdpto")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "pr01codactuacion", "SELECT pr01codactuacion,pr01descorta FROM PR0100 WHERE pr01codactuacion = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(4), "pr01descorta")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "ci21codpersona", "SELECT ci21codpersona,ci22nombre,ci22priapel,ci22segapel,ci22numhistoria FROM CI2200 WHERE ci21codpersona = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(26), "ci22nombre")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(27), "ci22priapel")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(28), "ci22segapel")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(29), "ci22numhistoria")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "ci13codentidad", "SELECT ci13codentidad,ci13desentidad FROM CI1300 WHERE ci13codentidad = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(30), "ci13desentidad")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "ci32codtipecon", "SELECT ci32codtipecon,ci32destipecon FROM  CI3200 WHERE ci32codtipecon = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(31), "ci32destipecon")
    
    'Llenamos el combo cboSSDBCombo1(0) con la lista de urgencias
    .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR48CODURGENCIA,PR48DESURGENCIA FROM PR4800 ORDER BY PR48CODURGENCIA ASC"
    
    'Creamos las ligaduras entre los campos cboCombo1(6), c�d. de urgencia, el txtDesUrgencia,
    'Descripci�n de la urgencia
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR48CODURGENCIA", "SELECT PR48CODURGENCIA,PR48DESURGENCIA FROM PR4800 WHERE PR48CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtDesUrgencia, "PR48DESURGENCIA")
    
    
    Call .WinRegister
    Call .WinStabilize
  End With

' se rellenan las Combo que no tienen Datafield
cboCombo1(0).AddItem "Primer"
cboCombo1(0).AddItem "Segundo"
cboCombo1(0).AddItem "Tercer"
cboCombo1(0).AddItem "Cuarto"
cboCombo1(0).AddItem "�ltimo"

cboCombo1(1).AddItem "D�a"
cboCombo1(1).AddItem "Lunes"
cboCombo1(1).AddItem "Martes"
cboCombo1(1).AddItem "Mi�rcoles"
cboCombo1(1).AddItem "Jueves"
cboCombo1(1).AddItem "Viernes"
cboCombo1(1).AddItem "S�bado"
cboCombo1(1).AddItem "Domingo"

cboCombo1(2).AddItem "Enero"
cboCombo1(2).AddItem "Febrero"
cboCombo1(2).AddItem "Marzo"
cboCombo1(2).AddItem "Abril"
cboCombo1(2).AddItem "Mayo"
cboCombo1(2).AddItem "Junio"
cboCombo1(2).AddItem "Julio"
cboCombo1(2).AddItem "Agosto"
cboCombo1(2).AddItem "Septiembre"
cboCombo1(2).AddItem "Octubre"
cboCombo1(2).AddItem "Noviembre"
cboCombo1(2).AddItem "Diciembre"

cboCombo1(3).AddItem "Primer"
cboCombo1(3).AddItem "Segundo"
cboCombo1(3).AddItem "Tercer"
cboCombo1(3).AddItem "Cuarto"
cboCombo1(3).AddItem "�ltimo"

cboCombo1(4).AddItem "D�a"
cboCombo1(4).AddItem "Lunes"
cboCombo1(4).AddItem "Martes"
cboCombo1(4).AddItem "Mi�rcoles"
cboCombo1(4).AddItem "Jueves"
cboCombo1(4).AddItem "Viernes"
cboCombo1(4).AddItem "S�bado"
cboCombo1(4).AddItem "Domingo"

cboCombo1(5).AddItem "Enero"
cboCombo1(5).AddItem "Febrero"
cboCombo1(5).AddItem "Marzo"
cboCombo1(5).AddItem "Abril"
cboCombo1(5).AddItem "Mayo"
cboCombo1(5).AddItem "Junio"
cboCombo1(5).AddItem "Julio"
cboCombo1(5).AddItem "Agosto"
cboCombo1(5).AddItem "Septiembre"
cboCombo1(5).AddItem "Octubre"
cboCombo1(5).AddItem "Noviembre"
cboCombo1(5).AddItem "Diciembre"
   
  'blnprimprop = True

 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)

 Dim strmensaje As String
 
 If intsalir = 1 Then
    intCancel = objWinInfo.WinExit
 Else 'no se ha pulsado Salir

 If rellenar_datos = 0 Then 'datos correctos
   intdatocorrecto = 0
   If cuestionario_no_respondido = 0 Then
    objWinInfo.DataSave
    cmdrecursos.Enabled = True
    objWinInfo.objWinActiveForm.blnChanged = False
    intCancel = objWinInfo.WinExit
    
   Else
    'Cuestionario no respondido.Datos correctos
    strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
      objWinInfo.objWinActiveForm.blnChanged = False
      intCancel = objWinInfo.WinExit
   Else
      intCancel = 1
   End If
   End If
Else 'datos no correctos
   intdatocorrecto = 1
   If cuestionario_no_respondido = 0 Then
    'Cuestionario respondido.Datos no correctos
    strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
      objWinInfo.objWinActiveForm.blnChanged = False
      intCancel = objWinInfo.WinExit
   Else
      intCancel = 1
   End If
   Else
   'Cuestionario no respondido.Datos incorrectos
   strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
      objWinInfo.objWinActiveForm.blnChanged = False
      intCancel = objWinInfo.WinExit
   Else
      intCancel = 1
   End If
End If
End If
    
 End If
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Empleados" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Actuacion Pedida" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
      .strOrder = "ORDER BY AD02CODDPTO ASC"
      .strWhere = "WHERE (AD02FECFIN IS NULL) " & _
                  " OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL))"
      
      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�d.Dpto."
      
      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Departamento"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
        Call objWinInfo.CtrlSet(txtDesDptoCargo, .cllValues("AD02DESDPTO"))
      End If
      
    End With
    Set objSearch = Nothing
    
  End If

End Sub




Private Sub optOption1_Click(Index As Integer)

If (optOption1(0).Value = True) Then
        UpDowndiames.Enabled = True
        UpDownmes.Enabled = True
        UpDownmes2.Enabled = False
        Text1(2).Enabled = True
        Text1(0).Enabled = True
        cboCombo1(0).Enabled = False
        cboCombo1(1).Enabled = False
        txtcombo(0).Enabled = False
        txtcombo(1).Enabled = False
        Text1(1).Enabled = False
        If intopt = 0 Then
        If (txtText1(17) <> "" Or txtText1(16) <> "" Or txtText1(14) <> "") Then
         'si se pincha en la primera opci�n vaciar lo que haya en la segunda
         Call objWinInfo.CtrlSet(txtText1(17), "")
         Call objWinInfo.CtrlSet(txtText1(16), "")
         Call objWinInfo.CtrlSet(txtText1(14), "")
         objWinInfo.objWinActiveForm.blnChanged = True
         tlbToolbar1.Buttons(4).Enabled = True
        End If
        Else
        intopt = 1
        End If
End If
If (optOption1(1).Value = True) Then
        UpDowndiames.Enabled = False
        UpDownmes.Enabled = False
        UpDownmes2.Enabled = True
        cboCombo1(0).Enabled = True
        cboCombo1(1).Enabled = True
        txtcombo(0).Enabled = True
        txtcombo(1).Enabled = True
        Text1(1).Enabled = True
        Text1(2).Enabled = False
        Text1(0).Enabled = False
        If intopt = 0 Then
        If (txtText1(15) <> "" Or txtText1(14) <> "") Then
         Call objWinInfo.CtrlSet(txtText1(15), "")
         Call objWinInfo.CtrlSet(txtText1(14), "")
         objWinInfo.objWinActiveForm.blnChanged = True
         tlbToolbar1.Buttons(4).Enabled = True
        End If
        Else
        intopt = 1
        End If
End If

End Sub

Private Sub optOption2_Click(Index As Integer)

If (optOption2(0).Value = True) Then
    UpDowndiaano.Enabled = True
    UpDownano.Enabled = True
    UpDownano2.Enabled = False
    Text1(3).Enabled = True
    cboCombo1(2).Enabled = True
    txtcombo(2).Enabled = True
    Text1(4).Enabled = True
    Text1(5).Enabled = False
    cboCombo1(3).Enabled = False
    txtcombo(3).Enabled = False
    cboCombo1(4).Enabled = False
    txtcombo(4).Enabled = False
    cboCombo1(5).Enabled = False
    txtcombo(5).Enabled = False
    If intopt = 0 Then
    If (txtText1(17) <> "" Or txtText1(16) <> "" Or txtText1(18) <> "" Or txtText1(3) <> "") Then
        'si se pincha en la primera opci�n vaciar lo que haya en la segunda
        Call objWinInfo.CtrlSet(txtText1(17), "")
        Call objWinInfo.CtrlSet(txtText1(16), "")
        Call objWinInfo.CtrlSet(txtText1(18), "")
        Call objWinInfo.CtrlSet(txtText1(3), "")
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
    End If
    Else
    intopt = 0
    End If
End If
If (optOption2(1).Value = True) Then
    UpDowndiaano.Enabled = False
    UpDownano.Enabled = False
    UpDownano2.Enabled = True
    cboCombo1(3).Enabled = True
    txtcombo(3).Enabled = True
    cboCombo1(4).Enabled = True
    txtcombo(4).Enabled = True
    cboCombo1(5).Enabled = True
    txtcombo(5).Enabled = True
    Text1(3).Enabled = False
    cboCombo1(2).Enabled = False
    txtcombo(2).Enabled = False
    Text1(5).Enabled = True
    Text1(4).Enabled = False
    If intopt = 0 Then
    If (txtText1(15) <> "" Or txtText1(18) <> "" Or txtText1(3) <> "") Then
    'si se pincha en la primera opci�n vaciar lo que haya en la primera
        Call objWinInfo.CtrlSet(txtText1(15), "")
        Call objWinInfo.CtrlSet(txtText1(18), "")
        Call objWinInfo.CtrlSet(txtText1(3), "")
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
    End If
    Else
    intopt = 0
    End If
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
If Index = 2 Then
   Select Case tabTab1(2).Tab
   Case 0
        Call objWinInfo.CtrlSet(txtText1(13), "")
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
        fraframe1(1).Caption = "La actuaci�n ocurre 1 vez"
        dtcDateCombo1(1).Enabled = False
        dtcDateCombo1(1).Visible = False
        dtcDateCombo1(2).Enabled = False
        dtcDateCombo1(2).Visible = False
        lblLabel1(12).Enabled = False
        lblLabel1(16).Enabled = False
        lblLabel1(12).Visible = False
        lblLabel1(16).Visible = False
    Case 1
        Call objWinInfo.CtrlSet(txtText1(13), "D")
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
        fraframe1(1).Caption = "La actuaci�n ocurre Diariamente"
        dtcDateCombo1(1).Enabled = True
        dtcDateCombo1(1).Visible = True
        dtcDateCombo1(2).Enabled = True
        dtcDateCombo1(2).Visible = True
        lblLabel1(12).Enabled = True
        lblLabel1(16).Enabled = True
        lblLabel1(12).Visible = True
        lblLabel1(16).Visible = True
    Case 2
        Call objWinInfo.CtrlSet(txtText1(13), "S")
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
        fraframe1(1).Caption = "La actuaci�n ocurre Semanalmente"
        dtcDateCombo1(1).Enabled = True
        dtcDateCombo1(1).Visible = True
        dtcDateCombo1(2).Enabled = True
        dtcDateCombo1(2).Visible = True
        lblLabel1(12).Enabled = True
        lblLabel1(16).Enabled = True
        lblLabel1(12).Visible = True
        lblLabel1(16).Visible = True
    Case 3
        Call objWinInfo.CtrlSet(txtText1(13), "M")
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
        fraframe1(1).Caption = "La actuaci�n ocurre Mensualmente"
        dtcDateCombo1(1).Enabled = True
        dtcDateCombo1(1).Visible = True
        dtcDateCombo1(2).Enabled = True
        dtcDateCombo1(2).Visible = True
        lblLabel1(12).Enabled = True
        lblLabel1(16).Enabled = True
        lblLabel1(12).Visible = True
        lblLabel1(16).Visible = True
    Case 4
        Call objWinInfo.CtrlSet(txtText1(13), "A")
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
        fraframe1(1).Caption = "La actuaci�n ocurre Anualmente"
        dtcDateCombo1(1).Enabled = True
        dtcDateCombo1(1).Visible = True
        dtcDateCombo1(2).Enabled = True
        dtcDateCombo1(2).Visible = True
        lblLabel1(12).Enabled = True
        lblLabel1(16).Enabled = True
        lblLabel1(12).Visible = True
        lblLabel1(16).Visible = True
    End Select
End If

'se bloquea el Grid
If Index = 0 Then
 If tabTab1(0).Tab = 1 Then
     intquitargrid = 1
    If rellenar_datos = 0 Then 'datos correctos
        intdatocorrecto = 0
    Else 'datos no correctos
  End If
Else
  intquitargrid = 0
End If
End If
End Sub

Private Sub Text1_Change(Index As Integer)

grdDBGrid1(0).Enabled = False
Select Case Index

Case 0
 If IsNumeric(Text1(0).Text) = True Then
      Call objWinInfo.CtrlSet(txtText1(14), Text1(0).Text)
 Else
      Text1(0).Text = ""
 End If
 
Case 1
 If IsNumeric(Text1(1).Text) = True Then
    Call objWinInfo.CtrlSet(txtText1(14), Text1(1).Text)
 Else
    Text1(1).Text = ""
 End If
 
Case 2
  If IsNumeric(Text1(2).Text) = True Then
    If Text1(2).Text <> "" Then
      If Text1(2).Text > 31 Then
          Text1(2).Text = 31
      End If
    End If
    Call objWinInfo.CtrlSet(txtText1(15), Text1(2).Text)
  Else
    Text1(2).Text = ""
  End If
    
Case 3
  If IsNumeric(Text1(3).Text) = True Then
    If Text1(3).Text <> "" Then
      If Text1(3).Text > 31 Then
          Text1(3).Text = 31
      End If
    End If
    Call objWinInfo.CtrlSet(txtText1(15), Text1(3).Text)
  Else
    Text1(3).Text = ""
  End If
  
Case 4
  If IsNumeric(Text1(4).Text) = True Then
    Call objWinInfo.CtrlSet(txtText1(18), Text1(4).Text)
  Else
    Text1(4).Text = ""
  End If
  
Case 5
  If IsNumeric(Text1(5).Text) = True Then
    Call objWinInfo.CtrlSet(txtText1(18), Text1(5).Text)
  Else
    Text1(5).Text = ""
  End If
  
End Select

End Sub
Private Sub funcion()
Dim strselectfunc As String
Dim rstafunc As rdoResultset
Dim strmensaje As String

If intdatocorrecto = 0 Then
 If txtText1(0).Text <> "" Then
  strselectfunc = "SELECT count(*) from pr0400 where pr03numactpedi=" & txtText1(0).Text
  Set rstafunc = objApp.rdoConnect.OpenResultset(strselectfunc)
  If intactcambiada = 1 Or rstafunc(0).Value = 0 Then
      Call Lanzar_Planificacion
  End If
  rstafunc.Close
  Set rstafunc = Nothing
 End If
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim strmensaje As String

  Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  Dim strSelDpto As String
  Dim rstDpto As rdoResultset
  Dim blnDptoNoValido As Boolean

  Dim rs As rdoResultset
  Dim strHoraSistema As String
  Dim iniciodia As Date
  Dim findia As Date
  
  
  'objWinInfo.objWinActiveForm.blnChanged = True
  
  If txtText1(13).Text = "D" Or txtText1(13).Text = "S" Or _
     txtText1(13).Text = "M" Or txtText1(13).Text = "A" Then
  'se coge la fecha del sistema
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
  strHoraSistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
  If dtcDateCombo1(1).Text = "" Or IsNull(dtcDateCombo1(1).Text) Then
   iniciodia = strHoraSistema
   Call objWinInfo.CtrlSet(dtcDateCombo1(1), iniciodia)
  Else
  iniciodia = dtcDateCombo1(1).Text
  End If

  If dtcDateCombo1(2).Text = "" Or IsNull(dtcDateCombo1(2).Text) Then
   findia = DateAdd("yyyy", 1, iniciodia)
   Call objWinInfo.CtrlSet(dtcDateCombo1(2), findia)
  Else
  findia = dtcDateCombo1(2).Text
  End If
  End If
  
  
  If (chkCheck1(9).Value = 1) And (txtText1(2).Text <> "") Then
    strSelDpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & txtText1(2).Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strSelDpto)
    If rstDpto.rdoColumns(0).Value = 0 Then
      blnDptoNoValido = True
    Else
      blnDptoNoValido = False
    End If
    rstDpto.Close
    Set rstDpto = Nothing
  End If
  
  blnFechaValida = True
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
 
  If dtcfecha.Text <> "" Then
    'Obtenemos el a�o seleccionado y lo ponemos con 4 digitos si procede
    strAnyoSelAux = Format(dtcfecha.Text, "yyyy")
    If Len(strAnyoSelAux) = 2 Then
      strAnyoSelAux = "19" & strAnyoSelAux
    End If
    intAnyoSelAux = strAnyoSelAux
    
    'Obtenemos el a�o del systema y lo ponemos con 4 d�gitos si procede
    strAnyoSysAux = Format(rsta.rdoColumns(0).Value, "yyyy")
    If Len(strAnyoSysAux) = 2 Then
      strAnyoSysAux = "19" & strAnyoSysAux
    End If
    intAnyoSysAux = strAnyoSysAux
    
    'Obtenemos el mes seleccionado
    strMesSelAux = Format(dtcfecha.Text, "mm")
    intMesSelAux = strMesSelAux
    
    'Obtenemos el mes del systema
    strMesSysAux = Format(rsta.rdoColumns(0).Value, "mm")
    intMesSysAux = strMesSysAux
    
    'Obtenemos el d�a seleccionado
    strDiaSelAux = Format(dtcfecha.Text, "dd")
    intDiaSelAux = strDiaSelAux
    
    'Obtenemos el d�a del systema
    strDiaSysAux = Format(rsta.rdoColumns(0).Value, "dd")
    intDiaSysAux = strDiaSysAux
        
    If intAnyoSelAux < intAnyoSysAux Then
      blnFechaValida = False
    Else
      If intAnyoSelAux = intAnyoSysAux Then
        If intMesSelAux < intMesSysAux Then
          blnFechaValida = False
        Else
          If intMesSelAux = intMesSysAux Then
            If intDiaSelAux < intDiaSysAux Then
              blnFechaValida = False
            Else
              If (intDiaSelAux = intDiaSysAux) And (txtHora.Text <> "") Then
                If CDbl(txtHora.Text) < CDbl(Format(rsta.rdoColumns(0).Value, "hh")) Then
                  blnFechaValida = False
                Else
                  If CDbl(txtHora.Text) = CDbl(Format(rsta.rdoColumns(0).Value, "hh")) _
                     And (txtMinuto.Text <> "") Then
                    If CDbl(txtMinuto.Text) <= CDbl(Format(rsta.rdoColumns(0).Value, "nn")) Then
                      blnFechaValida = False
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    
    
    'If blnFechaValida = False Then
      'intResp = MsgBox("La FECHA DE PREFERENCIA ha pasado", vbInformation, "Importante")
    'Else
    If blnFechaValida = True Or blnFechaValida = False Then
      If dtcfecha.Text <> "" Then
        If (txtHora.Text <> "" And txtMinuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txtHora.Text & ":" & txtMinuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          If intcambioalgo = 1 Then
            objWinInfo.objWinActiveForm.blnChanged = True
            tlbToolbar1.Buttons(4).Enabled = True
          End If
        Else
          If (txtHora.Text = "" And txtMinuto.Text = "") Or _
             (txtHora.Text = "" And txtMinuto.Text <> "") Then
            fecha = dtcfecha.Text
            Call objWinInfo.CtrlSet(txtText1(23), fecha)
            If intcambioalgo = 1 Then
               objWinInfo.objWinActiveForm.blnChanged = True
               tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
          If (txtHora.Text <> "" And txtMinuto.Text = "") Then
            fecha = dtcfecha.Text & " " & txtHora.Text & ":00"
            Call objWinInfo.CtrlSet(txtText1(23), fecha)
            If intcambioalgo = 1 Then
                objWinInfo.objWinActiveForm.blnChanged = True
                tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
        End If
      End If
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  
  
  If btnButton.Index <> 4 Then
  If blnFechaValida = True Or blnFechaValida = False Then
    'Salir
    If btnButton.Index = 30 Then
      intsalir = 1
    Else
    intsalir = 0
    End If

    If rellenar_datos = 0 Then 'datos correctos
      intdatocorrecto = 0
      If cuestionario_no_respondido = 0 Then
        If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
        Else
          If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
            objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
            txtText1(2).BackColor = &HFFFFFF
          End If
          cmdrecursos.Enabled = True
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          If btnButton.Index = 30 Then
            Exit Sub
          End If
          'para que se active Guardar ya que la fecha de preferencia no es obligatoria
          If dtcfecha.Text = "" Then
               tlbToolbar1.Buttons(4).Enabled = True
            End If
        End If
      Else
        'Cuestionario no respondido.Datos correctos
        strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
        If strmensaje = vbYes Then
          If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
            'Tiene que haber un c�dido de departamento
            strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
          Else
            If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
              objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
              txtText1(2).BackColor = &HFFFFFF
            End If
            objWinInfo.objWinActiveForm.blnChanged = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            If btnButton.Index = 30 Then
               Exit Sub
            End If
            'para que se active Guardar ya que la fecha de preferencia no es obligatoria
            If dtcfecha.Text = "" Then
                tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
        End If
      End If
    Else 'datos no correctos
      intdatocorrecto = 1
      If cuestionario_no_respondido = 0 Then
        'Cuestionario respondido.Datos no correctos
        strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
        If strmensaje = vbYes Then
          If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
            'Tiene que haber un c�dido de departamento
            strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
          Else
            If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
             objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
             txtText1(2).BackColor = &HFFFFFF
            End If
            objWinInfo.objWinActiveForm.blnChanged = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            If btnButton.Index = 30 Then
                Exit Sub
            End If
            'para que se active Guardar ya que la fecha de preferencia no es obligatoria
            If dtcfecha.Text = "" Then
                tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
        End If
      Else
        'Cuestionario no respondido.Datos incorrectos
        strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
        If strmensaje = vbYes Then
          If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
            'Tiene que haber un c�dido de departamento
            strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
          Else
            If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
             objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
             txtText1(2).BackColor = &HFFFFFF
            End If
            objWinInfo.objWinActiveForm.blnChanged = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            If btnButton.Index = 30 Then
                Exit Sub
            End If
            'para que se active Guardar ya que la fecha de preferencia no es obligatoria
            If dtcfecha.Text = "" Then
                tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
        End If
      End If
    End If
  End If
End If


'*********************
  If btnButton.Index = 4 Then
  If blnFechaValida = True Then
    'Salir
    If btnButton.Index = 30 Then
      intsalir = 1
    Else
    intsalir = 0
    End If

    If rellenar_datos = 0 Then 'datos correctos
      intdatocorrecto = 0
      If cuestionario_no_respondido = 0 Then
        If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
        Else
          If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
            objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
            txtText1(2).BackColor = &HFFFFFF
          End If
          cmdrecursos.Enabled = True
          If Right(txtText1(23).Text, 1) = "." Then
            txtText1(23).Text = Left(txtText1(23).Text, Len(txtText1(23).Text) - 1)
          End If
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          If btnButton.Index = 30 Then
            Exit Sub
          End If
          'para que se active Guardar ya que la fecha de preferencia no es obligatoria
          If dtcfecha.Text = "" Then
               tlbToolbar1.Buttons(4).Enabled = True
            End If
        End If
      Else
        'Cuestionario no respondido.Datos correctos
        strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "La actuaci�n no se planificar�.", _
                         vbOKOnly, "Planificaci�n Incompleta")
      End If
    Else 'datos no correctos
      intdatocorrecto = 1
      If cuestionario_no_respondido = 0 Then
        'Cuestionario respondido.Datos no correctos
        strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "La actuaci�n no se planificar�.", _
                         vbOKOnly, "Planificaci�n Incompleta")
        
      Else
        'Cuestionario no respondido.Datos incorrectos
        strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "La actuaci�n no se planificar�.", _
                         vbOKOnly, "Planificaci�n Incompleta")
        
      End If
    End If
  End If
End If

'******************

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

  Dim strmensaje As String

  Dim strSelDpto As String
  Dim rstDpto As rdoResultset
  Dim blnDptoNoValido As Boolean
  
  
  
  Dim rs As rdoResultset
  Dim strHoraSistema As String
  Dim iniciodia As Date
  Dim findia As Date
  
  objWinInfo.objWinActiveForm.blnChanged = True
  
  'si el usuario no mete fecha de inicio o fecha de final se mete la fecha de hoy
  'como fecha inicial y la fecha de inicio+1 a�o como fecha final
  If txtText1(13).Text = "D" Or txtText1(13).Text = "S" Or _
     txtText1(13).Text = "M" Or txtText1(13).Text = "A" Then
  'se coge la fecha del sistema
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
  strHoraSistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
  If dtcDateCombo1(1).Text = "" Or IsNull(dtcDateCombo1(1).Text) Then
   iniciodia = strHoraSistema
   Call objWinInfo.CtrlSet(dtcDateCombo1(1), iniciodia)
  Else
  iniciodia = dtcDateCombo1(1).Text
  End If

  If dtcDateCombo1(2).Text = "" Or IsNull(dtcDateCombo1(2).Text) Then
   findia = DateAdd("yyyy", 1, iniciodia)
   Call objWinInfo.CtrlSet(dtcDateCombo1(2), findia)
  Else
  findia = dtcDateCombo1(2).Text
  End If
  End If
  
  
  If (chkCheck1(9).Value = 1) And (txtText1(2).Text <> "") Then
    strSelDpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & txtText1(2).Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strSelDpto)
    If rstDpto.rdoColumns(0).Value = 0 Then
      blnDptoNoValido = True
    Else
      blnDptoNoValido = False
    End If
    rstDpto.Close
    Set rstDpto = Nothing
  End If

'Salir
If intIndex = 100 Then
   intsalir = 1
Else
   intsalir = 0
End If


If intIndex <> 40 Then
If rellenar_datos = 0 Then 'datos correctos
   intdatocorrecto = 0
   If cuestionario_no_respondido = 0 Then
     If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
      'Tiene que haber un c�dido de departamento
      strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
     Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
        objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
        txtText1(2).BackColor = &HFFFFFF
      End If
      cmdrecursos.Enabled = True
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      If intIndex = 100 Then
            Exit Sub
      End If
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
     End If
   Else
    'Cuestionario no respondido.Datos correctos
    strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
        objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
        txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      If intIndex = 100 Then
            Exit Sub
      End If
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   End If
Else 'datos no correctos
   intdatocorrecto = 1
   If cuestionario_no_respondido = 0 Then
    'Cuestionario respondido.Datos no correctos
    strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
      If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
      Else
        If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
        End If
        objWinInfo.objWinActiveForm.blnChanged = False
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        If intIndex = 100 Then
            Exit Sub
        End If
        'para que se active Guardar ya que la fecha de preferencia no es obligatoria
        If dtcfecha.Text = "" Then
           tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
   End If
   Else
   'Cuestionario no respondido.Datos incorrectos
   strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      If intIndex = 100 Then
            Exit Sub
      End If
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
End If
End If
End If

'********************************************************************+
If intIndex = 40 Then
If rellenar_datos = 0 Then 'datos correctos
   intdatocorrecto = 0
   If cuestionario_no_respondido = 0 Then
     If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
      'Tiene que haber un c�dido de departamento
      strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
     Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
        objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
        txtText1(2).BackColor = &HFFFFFF
      End If
      cmdrecursos.Enabled = True
        If Right(txtText1(23).Text, 1) = "." Then
            txtText1(23).Text = Left(txtText1(23).Text, Len(txtText1(23).Text) - 1)
        End If
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      If intIndex = 100 Then
            Exit Sub
      End If
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
     End If
   Else
    'Cuestionario no respondido.Datos correctos
    strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbOKOnly, "Planificaci�n Incompleta")
   End If
Else 'datos no correctos
   intdatocorrecto = 1
   If cuestionario_no_respondido = 0 Then
    'Cuestionario respondido.Datos no correctos
    strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbOKOnly, "Planificaci�n Incompleta")
   Else
   'Cuestionario no respondido.Datos incorrectos
   strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbOKOnly, "Planificaci�n Incompleta")
End If
End If
End If

'*********************************************************************
    

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
 
  Dim strmensaje As String
  Dim strSelDpto As String
  Dim rstDpto As rdoResultset
  Dim blnDptoNoValido As Boolean
  
  
  Dim rs As rdoResultset
  Dim strHoraSistema As String
  Dim iniciodia As Date
  Dim findia As Date
  
  objWinInfo.objWinActiveForm.blnChanged = True
  
  'si el usuario no mete fecha de inicio o fecha de final se mete la fecha de hoy
  'como fecha inicial y la fecha de inicio+1 a�o como fecha final
  If txtText1(13).Text = "D" Or txtText1(13).Text = "S" Or _
     txtText1(13).Text = "M" Or txtText1(13).Text = "A" Then
  'se coge la fecha del sistema
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
  strHoraSistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
  If dtcDateCombo1(1).Text = "" Or IsNull(dtcDateCombo1(1).Text) Then
   iniciodia = strHoraSistema
   Call objWinInfo.CtrlSet(dtcDateCombo1(1), iniciodia)
  Else
  iniciodia = dtcDateCombo1(1).Text
  End If

  If dtcDateCombo1(2).Text = "" Or IsNull(dtcDateCombo1(2).Text) Then
   findia = DateAdd("yyyy", 1, iniciodia)
   Call objWinInfo.CtrlSet(dtcDateCombo1(2), findia)
  Else
  findia = dtcDateCombo1(2).Text
  End If
  End If
  If (chkCheck1(9).Value = 1) And (txtText1(2).Text <> "") Then
    strSelDpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & txtText1(2).Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strSelDpto)
    If rstDpto.rdoColumns(0).Value = 0 Then
      blnDptoNoValido = True
    Else
      blnDptoNoValido = False
    End If
    rstDpto.Close
    Set rstDpto = Nothing
  End If

If rellenar_datos = 0 Then 'datos correctos
   intdatocorrecto = 0
   If cuestionario_no_respondido = 0 Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
      cmdrecursos.Enabled = True
    End If
   Else
    'Cuestionario no respondido.Datos correctos
    strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   End If
Else 'datos no correctos
   intdatocorrecto = 1
   If cuestionario_no_respondido = 0 Then
    'Cuestionario respondido.Datos no correctos
    strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   Else
   'Cuestionario no respondido.Datos incorrectos
   strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
End If
End If
    

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  
  Dim strmensaje As String
  Dim strSelDpto As String
  Dim rstDpto As rdoResultset
  Dim blnDptoNoValido As Boolean
  
  Dim rs As rdoResultset
  Dim strHoraSistema As String
  Dim iniciodia As Date
  Dim findia As Date
  
  objWinInfo.objWinActiveForm.blnChanged = True
  
  'si el usuario no mete fecha de inicio o fecha de final se mete la fecha de hoy
  'como fecha inicial y la fecha de inicio+1 a�o como fecha final
  If txtText1(13).Text = "D" Or txtText1(13).Text = "S" Or _
     txtText1(13).Text = "M" Or txtText1(13).Text = "A" Then
  'se coge la fecha del sistema
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
  strHoraSistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
  If dtcDateCombo1(1).Text = "" Or IsNull(dtcDateCombo1(1).Text) Then
   iniciodia = strHoraSistema
   Call objWinInfo.CtrlSet(dtcDateCombo1(1), iniciodia)
  Else
  iniciodia = dtcDateCombo1(1).Text
  End If

  If dtcDateCombo1(2).Text = "" Or IsNull(dtcDateCombo1(2).Text) Then
   findia = DateAdd("yyyy", 1, iniciodia)
   Call objWinInfo.CtrlSet(dtcDateCombo1(2), findia)
  Else
  findia = dtcDateCombo1(2).Text
  End If
  End If
  
  If (chkCheck1(9).Value = 1) And (txtText1(2).Text <> "") Then
    strSelDpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & txtText1(2).Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strSelDpto)
    If rstDpto.rdoColumns(0).Value = 0 Then
      blnDptoNoValido = True
    Else
      blnDptoNoValido = False
    End If
    rstDpto.Close
    Set rstDpto = Nothing
  End If

If rellenar_datos = 0 Then 'datos correctos
   intdatocorrecto = 0
   If cuestionario_no_respondido = 0 Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
      cmdrecursos.Enabled = True
    End If
   Else
    'Cuestionario no respondido.Datos correctos
    strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
     If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
     End If
     objWinInfo.objWinActiveForm.blnChanged = False
     Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
     'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   End If
Else 'datos no correctos
   intdatocorrecto = 1
   If cuestionario_no_respondido = 0 Then
    'Cuestionario respondido.Datos no correctos
    strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   Else
   'Cuestionario no respondido.Datos incorrectos
   strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
End If
End If
    

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Dim strmensaje As String
  Dim strSelDpto As String
  Dim rstDpto As rdoResultset
  Dim blnDptoNoValido As Boolean
  
  Dim rs As rdoResultset
  Dim strHoraSistema As String
  Dim iniciodia As Date
  Dim findia As Date
  
  objWinInfo.objWinActiveForm.blnChanged = True
  
  'si el usuario no mete fecha de inicio o fecha de final se mete la fecha de hoy
  'como fecha inicial y la fecha de inicio+1 a�o como fecha final
  If txtText1(13).Text = "D" Or txtText1(13).Text = "S" Or _
     txtText1(13).Text = "M" Or txtText1(13).Text = "A" Then
  'se coge la fecha del sistema
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
  strHoraSistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
  If dtcDateCombo1(1).Text = "" Or IsNull(dtcDateCombo1(1).Text) Then
   iniciodia = strHoraSistema
   Call objWinInfo.CtrlSet(dtcDateCombo1(1), iniciodia)
  Else
  iniciodia = dtcDateCombo1(1).Text
  End If

  If dtcDateCombo1(2).Text = "" Or IsNull(dtcDateCombo1(2).Text) Then
   findia = DateAdd("yyyy", 1, iniciodia)
   Call objWinInfo.CtrlSet(dtcDateCombo1(2), findia)
  Else
  findia = dtcDateCombo1(2).Text
  End If
  End If
  
  If (chkCheck1(9).Value = 1) And (txtText1(2).Text <> "") Then
    strSelDpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & txtText1(2).Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strSelDpto)
    If rstDpto.rdoColumns(0).Value = 0 Then
      blnDptoNoValido = True
    Else
      blnDptoNoValido = False
    End If
    rstDpto.Close
    Set rstDpto = Nothing
  End If

If rellenar_datos = 0 Then 'datos correctos
   intdatocorrecto = 0
   If cuestionario_no_respondido = 0 Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
      cmdrecursos.Enabled = True
    End If
   Else
    'Cuestionario no respondido.Datos correctos
    strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
     If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
     End If
     objWinInfo.objWinActiveForm.blnChanged = False
     Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
     'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   End If
Else 'datos no correctos
   intdatocorrecto = 1
   If cuestionario_no_respondido = 0 Then
    'Cuestionario respondido.Datos no correctos
    strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   Else
   'Cuestionario no respondido.Datos incorrectos
   strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
End If
End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
 
  Dim strmensaje As String
  Dim strSelDpto As String
  Dim rstDpto As rdoResultset
  Dim blnDptoNoValido As Boolean
  
  Dim rs As rdoResultset
  Dim strHoraSistema As String
  Dim iniciodia As Date
  Dim findia As Date
  
  'si el usuario no mete fecha de inicio o fecha de final se mete la fecha de hoy
  'como fecha inicial y la fecha de inicio+1 a�o como fecha final
  If txtText1(13).Text = "D" Or txtText1(13).Text = "S" Or _
     txtText1(13).Text = "M" Or txtText1(13).Text = "A" Then
  'se coge la fecha del sistema
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
  strHoraSistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
  If dtcDateCombo1(1).Text = "" Or IsNull(dtcDateCombo1(1).Text) Then
   iniciodia = strHoraSistema
   Call objWinInfo.CtrlSet(dtcDateCombo1(1), iniciodia)
  Else
  iniciodia = dtcDateCombo1(1).Text
  End If

  If dtcDateCombo1(2).Text = "" Or IsNull(dtcDateCombo1(2).Text) Then
   findia = DateAdd("yyyy", 1, iniciodia)
   Call objWinInfo.CtrlSet(dtcDateCombo1(2), findia)
  Else
  findia = dtcDateCombo1(2).Text
  End If
  End If
  If (chkCheck1(9).Value = 1) And (txtText1(2).Text <> "") Then
    strSelDpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & txtText1(2).Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strSelDpto)
    If rstDpto.rdoColumns(0).Value = 0 Then
      blnDptoNoValido = True
    Else
      blnDptoNoValido = False
    End If
    rstDpto.Close
    Set rstDpto = Nothing
  End If

If rellenar_datos = 0 Then 'datos correctos
   intdatocorrecto = 0
   If cuestionario_no_respondido = 0 Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
      cmdrecursos.Enabled = True
    End If
   Else
    'Cuestionario no respondido.Datos correctos
    strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   End If
Else 'datos no correctos
   intdatocorrecto = 1
   If cuestionario_no_respondido = 0 Then
    'Cuestionario respondido.Datos no correctos
    strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   Else
   'Cuestionario no respondido.Datos incorrectos
   strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
End If
End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  
  Dim strmensaje As String
  Dim strSelDpto As String
  Dim rstDpto As rdoResultset
  Dim blnDptoNoValido As Boolean
  
  Dim rs As rdoResultset
  Dim strHoraSistema As String
  Dim iniciodia As Date
  Dim findia As Date
  
  'si el usuario no mete fecha de inicio o fecha de final se mete la fecha de hoy
  'como fecha inicial y la fecha de inicio+1 a�o como fecha final
  If txtText1(13).Text = "D" Or txtText1(13).Text = "S" Or _
     txtText1(13).Text = "M" Or txtText1(13).Text = "A" Then
  'se coge la fecha del sistema
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
  strHoraSistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
  If dtcDateCombo1(1).Text = "" Or IsNull(dtcDateCombo1(1).Text) Then
   iniciodia = strHoraSistema
   Call objWinInfo.CtrlSet(dtcDateCombo1(1), iniciodia)
  Else
  iniciodia = dtcDateCombo1(1).Text
  End If

  If dtcDateCombo1(2).Text = "" Or IsNull(dtcDateCombo1(2).Text) Then
   findia = DateAdd("yyyy", 1, iniciodia)
   Call objWinInfo.CtrlSet(dtcDateCombo1(2), findia)
  Else
  findia = dtcDateCombo1(2).Text
  End If
  End If
  
  If (chkCheck1(9).Value = 1) And (txtText1(2).Text <> "") Then
    strSelDpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & txtText1(2).Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strSelDpto)
    If rstDpto.rdoColumns(0).Value = 0 Then
      blnDptoNoValido = True
    Else
      blnDptoNoValido = False
    End If
    rstDpto.Close
    Set rstDpto = Nothing
  End If


If rellenar_datos = 0 Then 'datos correctos
   intdatocorrecto = 0
   If cuestionario_no_respondido = 0 Then
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
    cmdrecursos.Enabled = True
   Else
    'Cuestionario no respondido.Datos correctos
    strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   End If
Else 'datos no correctos
   intdatocorrecto = 1
   If cuestionario_no_respondido = 0 Then
    'Cuestionario respondido.Datos no correctos
    strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
   Else
   'Cuestionario no respondido.Datos incorrectos
   strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
   If strmensaje = vbYes Then
    If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
    Else
      If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
          objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
          txtText1(2).BackColor = &HFFFFFF
      End If
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
      'para que se active Guardar ya que la fecha de preferencia no es obligatoria
      If dtcfecha.Text = "" Then
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    End If
   End If
End If
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
  Call objWinInfo.GridDblClick
  pinchargrid = 1
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  
  Dim strmensaje As String

  Dim fecha As String
  Dim strSelect As String
  Dim rsta As rdoResultset
  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  Dim intResp As Integer
  
  Dim strSelDpto As String
  Dim rstDpto As rdoResultset
  Dim blnDptoNoValido As Boolean
  
  If pinchargrid = 1 Then
     pinchargrid = 0

  
  If (chkCheck1(9).Value = 1) And (txtText1(2).Text <> "") Then
    strSelDpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & txtText1(2).Text
    Set rstDpto = objApp.rdoConnect.OpenResultset(strSelDpto)
    If rstDpto.rdoColumns(0).Value = 0 Then
      blnDptoNoValido = True
    Else
      blnDptoNoValido = False
    End If
    rstDpto.Close
    Set rstDpto = Nothing
  End If
  
  blnFechaValida = True
  strSelect = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY hh24:mi') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
 
  If dtcfecha.Text <> "" Then
    'Obtenemos el a�o seleccionado y lo ponemos con 4 digitos si procede
    strAnyoSelAux = Format(dtcfecha.Text, "yyyy")
    If Len(strAnyoSelAux) = 2 Then
      strAnyoSelAux = "19" & strAnyoSelAux
    End If
    intAnyoSelAux = strAnyoSelAux
    
    'Obtenemos el a�o del systema y lo ponemos con 4 d�gitos si procede
    strAnyoSysAux = Format(rsta.rdoColumns(0).Value, "yyyy")
    If Len(strAnyoSysAux) = 2 Then
      strAnyoSysAux = "19" & strAnyoSysAux
    End If
    intAnyoSysAux = strAnyoSysAux
    
    'Obtenemos el mes seleccionado
    strMesSelAux = Format(dtcfecha.Text, "mm")
    intMesSelAux = strMesSelAux
    
    'Obtenemos el mes del systema
    strMesSysAux = Format(rsta.rdoColumns(0).Value, "mm")
    intMesSysAux = strMesSysAux
    
    'Obtenemos el d�a seleccionado
    strDiaSelAux = Format(dtcfecha.Text, "dd")
    intDiaSelAux = strDiaSelAux
    
    'Obtenemos el d�a del systema
    strDiaSysAux = Format(rsta.rdoColumns(0).Value, "dd")
    intDiaSysAux = strDiaSysAux
        
    If intAnyoSelAux < intAnyoSysAux Then
      blnFechaValida = False
    Else
      If intAnyoSelAux = intAnyoSysAux Then
        If intMesSelAux < intMesSysAux Then
          blnFechaValida = False
        Else
          If intMesSelAux = intMesSysAux Then
            If intDiaSelAux < intDiaSysAux Then
              blnFechaValida = False
            Else
              If (intDiaSelAux = intDiaSysAux) And (txtHora.Text <> "") Then
                If CDbl(txtHora.Text) < CDbl(Format(rsta.rdoColumns(0).Value, "hh")) Then
                  blnFechaValida = False
                Else
                  If CDbl(txtHora.Text) = CDbl(Format(rsta.rdoColumns(0).Value, "hh")) _
                     And (txtMinuto.Text <> "") Then
                    If CDbl(txtMinuto.Text) <= CDbl(Format(rsta.rdoColumns(0).Value, "nn")) Then
                      blnFechaValida = False
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    
  
  If blnFechaValida = False Or blnFechaValida = True Then
      'intResp = MsgBox("La FECHA DE PREFERENCIA ha pasado", vbInformation, "Importante")
      'Else
      If dtcfecha.Text <> "" Then
        If (txtHora.Text <> "" And txtMinuto.Text <> "") Then
          fecha = dtcfecha.Text & " " & txtHora.Text & ":" & txtMinuto.Text
          Call objWinInfo.CtrlSet(txtText1(23), fecha)
          If intcambioalgo = 1 Then
            objWinInfo.objWinActiveForm.blnChanged = True
            tlbToolbar1.Buttons(4).Enabled = True
          End If
        Else
          If (txtHora.Text = "" And txtMinuto.Text = "") Or _
             (txtHora.Text = "" And txtMinuto.Text <> "") Then
            fecha = dtcfecha.Text
            Call objWinInfo.CtrlSet(txtText1(23), fecha)
            If intcambioalgo = 1 Then
               objWinInfo.objWinActiveForm.blnChanged = True
               tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
          If (txtHora.Text <> "" And txtMinuto.Text = "") Then
            fecha = dtcfecha.Text & " " & txtHora.Text & ":00"
            Call objWinInfo.CtrlSet(txtText1(23), fecha)
            If intcambioalgo = 1 Then
                objWinInfo.objWinActiveForm.blnChanged = True
                tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
        End If
      End If
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  
  
  
  If blnFechaValida = True Or blnFechaValida = False Then
    'Salir
    If rellenar_datos = 0 Then 'datos correctos
      intdatocorrecto = 0
      If cuestionario_no_respondido = 0 Then
        If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
          'Tiene que haber un c�dido de departamento
          strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
          grdDBGrid1(0).SelBookmarks.RemoveAll
          grdDBGrid1(0).Bookmark = fila
        Else
          If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
            objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
            txtText1(2).BackColor = &HFFFFFF
          End If
          cmdrecursos.Enabled = True
          Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
          'para que se active Guardar ya que la fecha de preferencia no es obligatoria
          If dtcfecha.Text = "" Then
                tlbToolbar1.Buttons(4).Enabled = True
            End If
        End If
      Else
        'Cuestionario no respondido.Datos correctos
        strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                        "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
        If strmensaje = vbYes Then
          If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
            'Tiene que haber un c�dido de departamento
            strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
            grdDBGrid1(0).SelBookmarks.RemoveAll
            grdDBGrid1(0).Bookmark = fila
          Else
            If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
              objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
              txtText1(2).BackColor = &HFFFFFF
            End If
            objWinInfo.objWinActiveForm.blnChanged = False 'IRENE
            Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
            'para que se active Guardar ya que la fecha de preferencia no es obligatoria
            If dtcfecha.Text = "" Then
                tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
          Else
            grdDBGrid1(0).SelBookmarks.RemoveAll
            grdDBGrid1(0).Bookmark = fila
        End If
      End If
    Else 'datos no correctos
      intdatocorrecto = 1
      If cuestionario_no_respondido = 0 Then
        'Cuestionario respondido.Datos no correctos
        strmensaje = MsgBox("Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                      "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
        If strmensaje = vbYes Then
          If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
            'Tiene que haber un c�dido de departamento
            strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
            grdDBGrid1(0).SelBookmarks.RemoveAll
            grdDBGrid1(0).Bookmark = fila
          Else
            If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
             objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
             txtText1(2).BackColor = &HFFFFFF
            End If
            objWinInfo.objWinActiveForm.blnChanged = False
            Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
            'para que se active Guardar ya que la fecha de preferencia no es obligatoria
            If dtcfecha.Text = "" Then
                tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
        Else
         grdDBGrid1(0).SelBookmarks.RemoveAll
         grdDBGrid1(0).Bookmark = fila
        End If
      Else
        'Cuestionario no respondido.Datos incorrectos
        strmensaje = MsgBox("El cuestionario asociado debe ser respondido." & Chr(13) & _
                       "Los datos referentes a las fechas de planificaci�n no son correctos." & Chr(13) & _
                       "�Quiere continuar sin que la actuaci�n se planifique?", _
                         vbYesNo + vbInformation, "Planificaci�n Incompleta")
        If strmensaje = vbYes Then
          If (chkCheck1(9).Value = 1) And ((txtText1(2).Text = "") Or (blnDptoNoValido = True)) Then
            'Tiene que haber un c�dido de departamento
            strmensaje = MsgBox("El Departamento de Cargo NO es V�lido", vbCritical, "Inter�s Cient�fico")
            grdDBGrid1(0).SelBookmarks.RemoveAll
            grdDBGrid1(0).Bookmark = fila
          Else
            If (chkCheck1(9).Value = 0) Then
             'objWinInfo.CtrlGetInfo(txtDesDptoCargo).blnMandatory = False
             objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
             txtText1(2).BackColor = &HFFFFFF
            End If
            objWinInfo.objWinActiveForm.blnChanged = False
            Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
            'para que se active Guardar ya que la fecha de preferencia no es obligatoria
            If dtcfecha.Text = "" Then
                tlbToolbar1.Buttons(4).Enabled = True
            End If
          End If
          Else
            grdDBGrid1(0).SelBookmarks.RemoveAll
            grdDBGrid1(0).Bookmark = fila
        End If
      End If
    End If
  End If

Else
Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End If

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If (intIndex = 0) Then
      If cboCombo1(0).Text = "" Then
        Call objWinInfo.CtrlSet(txtText1(17), 1)
        txtcombo(0).Text = cboCombo1(0).Text
      ElseIf cboCombo1(0).Text = "Primer" Then
        Call objWinInfo.CtrlSet(txtText1(17), 1)
        txtcombo(0).Text = cboCombo1(0).Text
      ElseIf cboCombo1(0).Text = "Segundo" Then
        Call objWinInfo.CtrlSet(txtText1(17), 2)
        txtcombo(0).Text = cboCombo1(0).Text
      ElseIf cboCombo1(0).Text = "Tercer" Then
        Call objWinInfo.CtrlSet(txtText1(17), 3)
        txtcombo(0).Text = cboCombo1(0).Text
      ElseIf cboCombo1(0).Text = "Cuarto" Then
         Call objWinInfo.CtrlSet(txtText1(17), 4)
         txtcombo(0).Text = cboCombo1(0).Text
      ElseIf cboCombo1(0).Text = "�ltimo" Then
         Call objWinInfo.CtrlSet(txtText1(17), 9)
         txtcombo(0).Text = cboCombo1(0).Text
  End If
  End If
  
  If (intIndex = 1) Then
      If cboCombo1(1).Text = "" Then
         Call objWinInfo.CtrlSet(txtText1(16), 0)
         txtcombo(1).Text = cboCombo1(1).Text
      ElseIf cboCombo1(1).Text = "D�a" Then
         Call objWinInfo.CtrlSet(txtText1(16), 0)
         txtcombo(1).Text = cboCombo1(1).Text
      ElseIf cboCombo1(1).Text = "Lunes" Then
         Call objWinInfo.CtrlSet(txtText1(16), 1)
         txtcombo(1).Text = cboCombo1(1).Text
      ElseIf cboCombo1(1).Text = "Martes" Then
         Call objWinInfo.CtrlSet(txtText1(16), 2)
         txtcombo(1).Text = cboCombo1(1).Text
      ElseIf cboCombo1(1).Text = "Mi�rcoles" Then
         Call objWinInfo.CtrlSet(txtText1(16), 3)
         txtcombo(1).Text = cboCombo1(1).Text
      ElseIf cboCombo1(1).Text = "Jueves" Then
         Call objWinInfo.CtrlSet(txtText1(16), 4)
         txtcombo(1).Text = cboCombo1(1).Text
      ElseIf cboCombo1(1).Text = "Viernes" Then
         Call objWinInfo.CtrlSet(txtText1(16), 5)
         txtcombo(1).Text = cboCombo1(1).Text
      ElseIf cboCombo1(1).Text = "S�bado" Then
         Call objWinInfo.CtrlSet(txtText1(16), 6)
         txtcombo(1).Text = cboCombo1(1).Text
      ElseIf cboCombo1(1).Text = "Domingo" Then
         Call objWinInfo.CtrlSet(txtText1(16), 7)
         txtcombo(1).Text = cboCombo1(1).Text
    End If
  End If
  
    If (intIndex = 3) Then
      If cboCombo1(3).Text = "" Then
         Call objWinInfo.CtrlSet(txtText1(17), 1)
         txtcombo(3).Text = cboCombo1(3).Text
      ElseIf cboCombo1(3).Text = "Primer" Then
         Call objWinInfo.CtrlSet(txtText1(17), 1)
         txtcombo(3).Text = cboCombo1(3).Text
      ElseIf cboCombo1(3).Text = "Segundo" Then
         Call objWinInfo.CtrlSet(txtText1(17), 2)
         txtcombo(3).Text = cboCombo1(3).Text
      ElseIf cboCombo1(3).Text = "Tercer" Then
         Call objWinInfo.CtrlSet(txtText1(17), 3)
         txtcombo(3).Text = cboCombo1(3).Text
      ElseIf cboCombo1(3).Text = "Cuarto" Then
         Call objWinInfo.CtrlSet(txtText1(17), 4)
         txtcombo(3).Text = cboCombo1(3).Text
      ElseIf cboCombo1(3).Text = "�ltimo" Then
         Call objWinInfo.CtrlSet(txtText1(17), 9)
         txtcombo(3).Text = cboCombo1(3).Text
  End If
 End If
 
  If (intIndex = 4) Then
      If cboCombo1(4).Text = "" Then
         Call objWinInfo.CtrlSet(txtText1(16), 0)
         txtcombo(4).Text = cboCombo1(4).Text
      ElseIf cboCombo1(4).Text = "D�a" Then
         Call objWinInfo.CtrlSet(txtText1(16), 0)
         txtcombo(4).Text = cboCombo1(4).Text
      ElseIf cboCombo1(4).Text = "Lunes" Then
         Call objWinInfo.CtrlSet(txtText1(16), 1)
         txtcombo(4).Text = cboCombo1(4).Text
      ElseIf cboCombo1(4).Text = "Martes" Then
         Call objWinInfo.CtrlSet(txtText1(16), 2)
         txtcombo(4).Text = cboCombo1(4).Text
      ElseIf cboCombo1(4).Text = "Mi�rcoles" Then
         Call objWinInfo.CtrlSet(txtText1(16), 3)
         txtcombo(4).Text = cboCombo1(4).Text
      ElseIf cboCombo1(4).Text = "Jueves" Then
         Call objWinInfo.CtrlSet(txtText1(16), 4)
         txtcombo(4).Text = cboCombo1(4).Text
      ElseIf cboCombo1(4).Text = "Viernes" Then
         Call objWinInfo.CtrlSet(txtText1(16), 5)
         txtcombo(4).Text = cboCombo1(4).Text
      ElseIf cboCombo1(4).Text = "S�bado" Then
         Call objWinInfo.CtrlSet(txtText1(16), 6)
         txtcombo(4).Text = cboCombo1(4).Text
      ElseIf cboCombo1(4).Text = "Domingo" Then
         Call objWinInfo.CtrlSet(txtText1(16), 7)
         txtcombo(4).Text = cboCombo1(4).Text
    End If
 End If
 
    If (intIndex = 2) Then
      If (cboCombo1(2).Text = "") Then
         Call objWinInfo.CtrlSet(txtText1(3), 1)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Enero") Then
         Call objWinInfo.CtrlSet(txtText1(3), 1)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Febrero") Then
         Call objWinInfo.CtrlSet(txtText1(3), 2)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Marzo") Then
         Call objWinInfo.CtrlSet(txtText1(3), 3)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Abril") Then
         Call objWinInfo.CtrlSet(txtText1(3), 4)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Mayo") Then
         Call objWinInfo.CtrlSet(txtText1(3), 5)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Junio") Then
         Call objWinInfo.CtrlSet(txtText1(3), 6)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Julio") Then
         Call objWinInfo.CtrlSet(txtText1(3), 7)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Agosto") Then
         Call objWinInfo.CtrlSet(txtText1(3), 8)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Septiembre") Then
         Call objWinInfo.CtrlSet(txtText1(3), 9)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Octubre") Then
         Call objWinInfo.CtrlSet(txtText1(3), 10)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Noviembre") Then
         Call objWinInfo.CtrlSet(txtText1(3), 11)
         txtcombo(2).Text = cboCombo1(2).Text
      ElseIf (cboCombo1(2).Text = "Diciembre") Then
         Call objWinInfo.CtrlSet(txtText1(3), 12)
         txtcombo(2).Text = cboCombo1(2).Text
    End If
  End If
  
  If (intIndex = 5) Then
      If (cboCombo1(5).Text = "") Then
         Call objWinInfo.CtrlSet(txtText1(3), 1)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Enero") Then
         Call objWinInfo.CtrlSet(txtText1(3), 1)
         txtcombo(5).Text = cboCombo1(5).Text
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Febrero") Then
         Call objWinInfo.CtrlSet(txtText1(3), 2)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Marzo") Then
         Call objWinInfo.CtrlSet(txtText1(3), 3)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Abril") Then
         Call objWinInfo.CtrlSet(txtText1(3), 4)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Mayo") Then
         Call objWinInfo.CtrlSet(txtText1(3), 5)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Junio") Then
         Call objWinInfo.CtrlSet(txtText1(3), 6)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Julio") Then
         Call objWinInfo.CtrlSet(txtText1(3), 7)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Agosto") Then
         Call objWinInfo.CtrlSet(txtText1(3), 8)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Septiembre") Then
         Call objWinInfo.CtrlSet(txtText1(3), 9)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Octubre") Then
         Call objWinInfo.CtrlSet(txtText1(3), 10)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Noviembre") Then
         Call objWinInfo.CtrlSet(txtText1(3), 11)
         txtcombo(5).Text = cboCombo1(5).Text
      ElseIf (cboCombo1(5).Text = "Diciembre") Then
         Call objWinInfo.CtrlSet(txtText1(3), 12)
         txtcombo(5).Text = cboCombo1(5).Text
    End If
  End If
 objWinInfo.objWinActiveForm.blnChanged = True
 tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If (intIndex = 1 Or intIndex = 2) Then
  grdDBGrid1(0).Enabled = False
     If intleer = 0 Then
         intactcambiada = 1
     End If
  End If
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If (intIndex = 1 Or intIndex = 2) Then
     grdDBGrid1(0).Enabled = False
     If intleer = 0 Then
         intactcambiada = 1
     End If
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 9 Then
    If chkCheck1(9).Value = 1 Then
    'Se hace visible el campo del c�d. de dpto. al que se le carga la actuaci�n
      lblLabel1(6).Visible = True
      txtText1(2).Visible = True
      txtDesDptoCargo.Visible = True
      objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = True
      txtText1(2).BackColor = &HFFFF00
    Else
    'Se hace invisible el campo del c�d. de dpto. al que se le carga la actuaci�n
      Call objWinInfo.CtrlSet(txtText1(2), "")
      lblLabel1(6).Visible = False
      txtText1(2).Visible = False
      txtDesDptoCargo.Visible = False
      objWinInfo.CtrlGetInfo(txtText1(2)).blnMandatory = False
      txtText1(2).BackColor = &HFFFFFF
    End If
  End If
  If (intIndex = 1 Or intIndex = 2 Or intIndex = 3 Or intIndex = 4 Or _
      intIndex = 5 Or intIndex = 6 Or intIndex = 7) Then
      grdDBGrid1(0).Enabled = False
      If intleer = 0 Then
         intactcambiada = 1
      End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



Private Sub txtminuto_KeyPress(KeyAscii As Integer)
intcambioalgo = 1
intminuto = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)

  Dim strsqlA As String
  Dim rsta As rdoResultset
  Dim strsqlM As String
  Dim rstM As rdoResultset
  Dim strsqlplan As String
  Dim rstplan As rdoResultset
  Dim strfec As String
  Dim fec As String
  Dim hora As String
  Dim strinter As String
  Dim rstinter As rdoResultset
  Dim rsttiporec As rdoResultset
  Dim strtiporec As String
  Dim rstrest As rdoResultset
  Dim strrest As String
  Dim numrestricciones As Integer
  Dim rstconsen As rdoResultset
  Dim strconsen As String
  
  
  
  If intIndex = 10 Or intIndex = 20 Then
      grdDBGrid1(0).Enabled = False
  End If
  
  Call objWinInfo.CtrlDataChange
  
  
  
  'como la fecha de preferencia no es obligatoria meterla(puede ser vac�a),se
  'activa el bot�n Guardar porque si se deja la fecha vac�a es como no haber
  'ning�n cambio y no se activa el bot�n.

  If intIndex = 0 Then
    intcambioalgo = 0
  End If
  
  ' se controla cu�ndo cambia alguna columna de la base que haga que haya que
  ' volver a lanzar las actuaciones planificadas
  If (intIndex = 23 Or intIndex = 10 Or intIndex = 20 Or intIndex = 14 Or intIndex = 15 Or _
      intIndex = 16 Or intIndex = 17 Or intIndex = 18 Or intIndex = 3 Or intIndex = 13) Then
      If intleer = 0 Then
         intactcambiada = 1
      End If
  End If
  If intIndex = 0 Then
      intaux = 0
  End If
  
  If intminuto = 1 Then
    intminuto = 0
  Else
  If (intIndex = 23) Then 'fecha y hora de preferencia
     hora = txtText1(23).Text
     If hora <> "" Then
        If intaux = 0 Then
         strfechaaux = txtText1(23)
         intaux = 1
        End If
        dtcfecha.Text = Left(strfechaaux, 10)
        If Len(strfechaaux) > 10 Then
          txtHora = Right(Left(strfechaaux, 13), 2)
          txtMinuto = Right(Left(strfechaaux, 16), 2)
        Else
          txtHora = ""
          txtMinuto = ""
        End If
     Else
      dtcfecha.Text = ""
      txtHora = ""
      txtMinuto = ""
    End If
  End If
End If
  
   If (intIndex = 17) Then
    Select Case txtText1(17).Text
        Case ""
            cboCombo1(0).Text = ""
            cboCombo1(3).Text = ""
            txtcombo(0).Text = ""
            txtcombo(3).Text = ""
        Case 1
            cboCombo1(0).Text = "Primer"
            cboCombo1(3).Text = "Primer"
            txtcombo(0).Text = "Primer"
            txtcombo(3).Text = "Primer"
        Case 2
            cboCombo1(0).Text = "Segundo"
            cboCombo1(3).Text = "Segundo"
            txtcombo(0).Text = "Segundo"
            txtcombo(3).Text = "Segundo"
        Case 3
            cboCombo1(0).Text = "Tercer"
            cboCombo1(3).Text = "Tercer"
            txtcombo(0).Text = "Tercer"
            txtcombo(3).Text = "Tercer"
        Case 4
            cboCombo1(0).Text = "Cuarto"
            cboCombo1(3).Text = "Cuarto"
            txtcombo(0).Text = "Cuarto"
            txtcombo(3).Text = "Cuarto"
        Case 9
            cboCombo1(0).Text = "�ltimo"
            cboCombo1(3).Text = "�ltimo"
            txtcombo(0).Text = "�ltimo"
            txtcombo(3).Text = "�ltimo"
    End Select
    End If
    If (intIndex = 16) Then
    Select Case txtText1(16).Text
        Case ""
            cboCombo1(1).Text = ""
            cboCombo1(4).Text = ""
            txtcombo(1).Text = ""
            txtcombo(4).Text = ""
        Case 0
            cboCombo1(1).Text = "D�a"
            cboCombo1(4).Text = "D�a"
            txtcombo(1).Text = "D�a"
            txtcombo(4).Text = "D�a"
        Case 1
            cboCombo1(1).Text = "Lunes"
            cboCombo1(4).Text = "Lunes"
            txtcombo(1).Text = "Lunes"
            txtcombo(4).Text = "Lunes"
        Case 2
            cboCombo1(1).Text = "Martes"
            cboCombo1(4).Text = "Martes"
            txtcombo(1).Text = "Martes"
            txtcombo(4).Text = "Martes"
        Case 3
            cboCombo1(1).Text = "Mi�rcoles"
            cboCombo1(4).Text = "Mi�rcoles"
            txtcombo(1).Text = "Mi�rcoles"
            txtcombo(4).Text = "Mi�rcoles"
        Case 4
            cboCombo1(1).Text = "Jueves"
            cboCombo1(4).Text = "Jueves"
            txtcombo(1).Text = "Jueves"
            txtcombo(4).Text = "Jueves"
        Case 5
            cboCombo1(1).Text = "Viernes"
            cboCombo1(4).Text = "Viernes"
            txtcombo(1).Text = "Viernes"
            txtcombo(4).Text = "Viernes"
        Case 6
            cboCombo1(1).Text = "S�bado"
            cboCombo1(4).Text = "S�bado"
            txtcombo(1).Text = "S�bado"
            txtcombo(4).Text = "S�bado"
        Case 7
            cboCombo1(1).Text = "Domingo"
            cboCombo1(4).Text = "Domingo"
            txtcombo(1).Text = "Domingo"
            txtcombo(4).Text = "Domingo"
        End Select
        End If
        If (intIndex = 3) Then
        Select Case txtText1(3).Text
        Case ""
            cboCombo1(2).Text = ""
            cboCombo1(5).Text = ""
            txtcombo(2).Text = ""
            txtcombo(5).Text = ""
        Case 1
            cboCombo1(2).Text = "Enero"
            cboCombo1(5).Text = "Enero"
            txtcombo(2).Text = "Enero"
            txtcombo(5).Text = "Enero"
        Case 2
            cboCombo1(2).Text = "Febrero"
            cboCombo1(5).Text = "Febrero"
            txtcombo(2).Text = "Febrero"
            txtcombo(5).Text = "Febrero"
        Case 3
            cboCombo1(2).Text = "Marzo"
            cboCombo1(5).Text = "Marzo"
            txtcombo(2).Text = "Marzo"
            txtcombo(5).Text = "Marzo"
        Case 4
            cboCombo1(2).Text = "Abril"
            cboCombo1(5).Text = "Abril"
            txtcombo(2).Text = "Abril"
            txtcombo(5).Text = "Abril"
        Case 5
            cboCombo1(2).Text = "Mayo"
            cboCombo1(5).Text = "Mayo"
            txtcombo(2).Text = "Mayo"
            txtcombo(5).Text = "Mayo"
        Case 6
            cboCombo1(2).Text = "Junio"
            cboCombo1(5).Text = "Junio"
            txtcombo(2).Text = "Junio"
            txtcombo(5).Text = "Junio"
        Case 7
            cboCombo1(2).Text = "Julio"
            cboCombo1(5).Text = "Julio"
            txtcombo(2).Text = "Julio"
            txtcombo(5).Text = "Julio"
        Case 8
            cboCombo1(2).Text = "Agosto"
            cboCombo1(5).Text = "Agosto"
            txtcombo(2).Text = "Agosto"
            txtcombo(5).Text = "Agosto"
        Case 9
            cboCombo1(2).Text = "Septiembre"
            cboCombo1(5).Text = "Septiembre"
            txtcombo(2).Text = "Septiembre"
            txtcombo(5).Text = "Septiembre"
        Case 10
            cboCombo1(2).Text = "Octubre"
            cboCombo1(5).Text = "Octubre"
            txtcombo(2).Text = "Octubre"
            txtcombo(5).Text = "Octubre"
        Case 11
            cboCombo1(2).Text = "Noviembre"
            cboCombo1(5).Text = "Noviembre"
            txtcombo(2).Text = "Noviembre"
            txtcombo(5).Text = "Noviembre"
        Case 12
            cboCombo1(2).Text = "Diciembre"
            cboCombo1(5).Text = "Diciembre"
            txtcombo(2).Text = "Diciembre"
            txtcombo(5).Text = "Diciembre"
        End Select
        End If
        If intIndex = 14 Then
            Text1(0).Text = txtText1(14).Text
            Text1(1).Text = txtText1(14).Text
        End If
        If intIndex = 15 Then
            Text1(2).Text = txtText1(15).Text
            Text1(3).Text = txtText1(15).Text
          End If
        If intIndex = 18 Then
            Text1(4).Text = txtText1(18).Text
            Text1(5).Text = txtText1(18).Text
        End If
        
        If intIndex = 0 Then
        ' si cambia el c�digo deshabilito los controles de los Option Button
        Text1(2).Enabled = False
        Text1(0).Enabled = False
        cboCombo1(0).Enabled = False
        cboCombo1(1).Enabled = False
        txtcombo(0).Enabled = False
        txtcombo(1).Enabled = False
        txtcombo(2).Enabled = False
        txtcombo(3).Enabled = False
        txtcombo(4).Enabled = False
        txtcombo(5).Enabled = False
        Text1(1).Enabled = False
        Text1(3).Enabled = False
        Text1(4).Enabled = False
        Text1(5).Enabled = False
        cboCombo1(2).Enabled = False
        cboCombo1(3).Enabled = False
        cboCombo1(4).Enabled = False
        cboCombo1(5).Enabled = False
        txtText1(10).Locked = True
        txtText1(20).Locked = True
        'Text1(2).Locked = True
        'Text1(0).Locked = True
        'Text1(1).Locked = True
        'Text1(3).Locked = True
        'Text1(4).Locked = True
        'Text1(5).Locked = True
        txtcombo(0).Locked = True
        txtcombo(1).Locked = True
        txtcombo(2).Locked = True
        txtcombo(3).Locked = True
        txtcombo(4).Locked = True
        txtcombo(5).Locked = True
        UpDowndiames.Enabled = False
        UpDownmes.Enabled = False
        UpDownmes2.Enabled = False
        UpDowndiaano.Enabled = False
        UpDownano.Enabled = False
        UpDownano2.Enabled = False
        optOption1(0).Value = False
        optOption1(1).Value = False
        optOption2(0).Value = False
        optOption2(1).Value = False
        End If
  
If (intIndex = 0) Then
If intactcambiada = 1 Then
 intactcambiada = 0
End If

 If txtText1(0).Text <> "" Then
  'se mira si la actuaci�n tiene cuestionario
  strsqlA = "select count(*) from PR4100 where pr03numactpedi=" & txtText1(0).Text
  Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)

  'se mira si la actuaci�n tiene muestras con cuestionario
  'strsqlM = "select count(*) from PR4300 where pr03numactpedi=" & txtText1(0).Text
  'Set rstM = objApp.rdoConnect.OpenResultset(strsqlM)
  
  numrestricciones = 0
  'se miran los tipos de recurso asociados a la actuaci�n pedida
    strtiporec = "select ag14codtiprecu from PR1400 where pr03numactpedi=" & txtText1(0).Text
    Set rsttiporec = objApp.rdoConnect.OpenResultset(strtiporec)
    While Not rsttiporec.EOF
          'se mira para cada tipo de recurso si tiene alguna restricci�n
          strrest = "select count(*) from AG1500 " & _
                  "where ag14codtiprecu=" & rsttiporec.rdoColumns(0).Value
          Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
          numrestricciones = numrestricciones + rstrest.rdoColumns(0).Value
    rsttiporec.MoveNext
    rstrest.Close
    Set rstrest = Nothing
    Wend
    rsttiporec.Close
    Set rsttiporec = Nothing
  
    If (rsta.rdoColumns(0).Value > 0 Or numrestricciones > 0) Then
     cmdcuestionario.Enabled = True
    Else
     cmdcuestionario.Enabled = False
    End If
    numrestricciones = 0
  '-----------------------------------------------------------------------
  'la actuaci�n tiene cuestionario y muestras con cuestionario
  'If (rsta.rdoColumns(0).Value > 0 Or rstM.rdoColumns(0).Value > 0) Then
  '  cmdcuestionario.Enabled = True
  'Else
  '  cmdcuestionario.Enabled = False
  'End If
  '-----------------------------------------------------------------------
 rsta.Close
 Set rsta = Nothing
 'rstM.Close
 'Set rstM = Nothing
 End If
 
 
' cuando se cambia de registro se activa el bot�n de recursos si el cuestionario
' est� respondido y la actuaci�n ha generado las planificadas ya que para
' seleccionar los recursos hace falta el c�digo de actuaci�n planificada.

'se mira si la actuaci�n est� planificada,es decir, si tiene tuplas en la PR0400,
'si las tiene es que el cuestionario est� respondido
If txtText1(0).Text <> "" Then
strsqlplan = "select count(*) from PR0400 where pr03numactpedi=" & txtText1(0).Text & _
             " and pr37codestado=1"
Set rstplan = objApp.rdoConnect.OpenResultset(strsqlplan)

If (rstplan.rdoColumns(0).Value = 0) Then
   cmdrecursos.Enabled = False
Else
   cmdrecursos.Enabled = True
End If
rstplan.Close
Set rstplan = Nothing
End If

'cuando cambia el c�digo de actuaci�n pedida hay que habilitar o no el bot�n
'de interacciones si es que dicha actuaci�n interacciona con alguna otra
strinter = "select count(*) from pr3900 where pr03numactpedi_des=" & txtText1(0) & _
           " and pr03numactpedi in ( select pr03numactpedi from PR0300 " & _
           "where pr09numpeticion =" & frmpedir.txtText1(0) & ")"
Set rstinter = objApp.rdoConnect.OpenResultset(strinter)
If rstinter.rdoColumns(0).Value = 0 Then
   cmdinterplani.Enabled = False
Else
  cmdinterplani.Enabled = True
End If
rstinter.Close
Set rstinter = Nothing
'******************************************
End If
  
If (intIndex = 0) Then
    If dtcfecha.Text = "" Then
      tlbToolbar1.Buttons(4).Enabled = True
    End If
End If
  
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


Private Sub UpDownano_DownClick()
  Call objWinInfo.CtrlSet(Text1(4), UpDownano.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDownano_UpClick()
  Call objWinInfo.CtrlSet(Text1(4), UpDownano.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub



Private Sub UpDownano2_DownClick()
  Call objWinInfo.CtrlSet(Text1(5), UpDownano2.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDownano2_UpClick()
  Call objWinInfo.CtrlSet(Text1(5), UpDownano2.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDowndiaano_DownClick()
  Call objWinInfo.CtrlSet(Text1(3), UpDowndiaano.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDowndiaano_UpClick()
  Call objWinInfo.CtrlSet(Text1(3), UpDowndiaano.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDowndiames_DownClick()
  Call objWinInfo.CtrlSet(Text1(2), UpDowndiames.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDowndiames_UpClick()
  Call objWinInfo.CtrlSet(Text1(2), UpDowndiames.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDownhora_DownClick()
  txtHora.SetFocus
  txtHora = UpDownhora.Value
  intcambioalgo = 1
End Sub

Private Sub UpDownhora_UpClick()
  txtHora.SetFocus
  txtHora = UpDownhora.Value
  intcambioalgo = 1
End Sub


Private Sub UpDownmes_DownClick()
  Call objWinInfo.CtrlSet(Text1(0), UpDownmes.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDownmes_UpClick()
  Call objWinInfo.CtrlSet(Text1(0), UpDownmes.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub


Private Sub UpDownmes2_DownClick()
  Call objWinInfo.CtrlSet(Text1(1), UpDownmes2.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDownmes2_UpClick()
  Call objWinInfo.CtrlSet(Text1(1), UpDownmes2.Value)
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub UpDownminuto_DownClick()
  txtMinuto.SetFocus
  txtMinuto = UpDownminuto.Value
End Sub

Private Sub UpDownminuto_UpClick()
  txtMinuto.SetFocus
  txtMinuto = UpDownminuto.Value
End Sub

Private Sub UpDownsemanas_DownClick()
  Call objWinInfo.CtrlSet(txtText1(20), UpDownsemanas.Value)
End Sub

Private Sub UpDownsemanas_UpClick()
  Call objWinInfo.CtrlSet(txtText1(20), UpDownsemanas.Value)
End Sub

