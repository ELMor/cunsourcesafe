Attribute VB_Name = "REA004"
Option Explicit

Public gintCodDpto As Integer ' C�digo del dapartamento de la persona que va a firmar el informe
Public gstrNombrePerCUN As String 'Nombre de la persona que va a firmar el informe
Public gstrPriApelPerCUN As String 'Primer apellido de la persona que va a firmar el informe
Public gstrSegApelPerCUN As String 'Segundo apellido de la persona que va a firmar el informe
Public gintNumColegiado As Integer 'N� de colegiado de la persona que va a firmar el informe
Public gstrActPlan As String 'N� de actuaci�n pedida
