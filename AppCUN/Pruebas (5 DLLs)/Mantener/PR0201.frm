VERSION 5.00
Begin VB.Form frmmenumanten 
   Caption         =   "Men� de Mantenimiento"
   ClientHeight    =   3195
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame fraFrame1 
      Caption         =   "Men� de Mantenimiento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6855
      Left            =   840
      TabIndex        =   0
      Top             =   960
      Width           =   10695
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Estados de Actuaci�n-Muestra"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   11
         Left            =   5760
         TabIndex        =   22
         Top             =   3720
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Urgencias"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   5760
         TabIndex        =   10
         Top             =   4680
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Preguntas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   360
         TabIndex        =   9
         Top             =   4680
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Estados de una Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   10
         Left            =   5760
         TabIndex        =   8
         Top             =   2760
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   9
         Left            =   5760
         TabIndex        =   11
         Top             =   5640
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Interacciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   8
         Left            =   5760
         TabIndex        =   6
         Top             =   1800
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Listas de Respuestas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   7
         Left            =   360
         TabIndex        =   7
         Top             =   3720
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Tipos de Respuesta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   6
         Left            =   5760
         TabIndex        =   4
         Top             =   840
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Tipos de Muestra  -             Muestras"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   5
         Left            =   360
         TabIndex        =   2
         Top             =   5640
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Tipos de Interacci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   4
         Left            =   360
         TabIndex        =   5
         Top             =   2760
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Tipos de Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   360
         TabIndex        =   3
         Top             =   1800
         Width           =   1935
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Tipos de Actividad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   360
         TabIndex        =   1
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Urgencias"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   10
         Left            =   7920
         TabIndex        =   23
         Top             =   4800
         Width           =   2655
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Estados Actuaci�n con Muestra"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   3
         Left            =   7920
         TabIndex        =   21
         Top             =   3840
         Width           =   2655
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Preguntas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   2520
         TabIndex        =   20
         Top             =   4800
         Width           =   2655
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Estados de una Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   9
         Left            =   7920
         TabIndex        =   19
         Top             =   2880
         Width           =   2655
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Interacciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   8
         Left            =   7920
         TabIndex        =   18
         Top             =   1920
         Width           =   1815
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Listas de Respuestas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   7
         Left            =   2520
         TabIndex        =   17
         Top             =   3840
         Width           =   2775
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Tipos de Respuesta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   6
         Left            =   7920
         TabIndex        =   16
         Top             =   960
         Width           =   2655
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Tipos de Muestra -            Muestras"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   5
         Left            =   2520
         TabIndex        =   15
         Top             =   5760
         Width           =   2535
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Tipos de Interacci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   4
         Left            =   2520
         TabIndex        =   14
         Top             =   2880
         Width           =   2655
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Tipos de Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   2520
         TabIndex        =   13
         Top             =   1920
         Width           =   2055
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Tipos de Actividad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   2520
         TabIndex        =   12
         Top             =   960
         Width           =   2415
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipos de &Actividad"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipos de &Muestra"
         Index           =   20
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipos de &Grupo"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipos de &Respuesta"
         Index           =   40
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipos de &Interacci�n"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "I&nteracciones"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Listas de Respuestas"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Estados de una Actuaci�n"
         Index           =   80
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Preguntas"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Respuestas"
         Index           =   100
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   110
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   120
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmmenumanten"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: MANTENIMIENTOS PRUEBAS C.U.N. (PR002.VBP)                  *
'* NOMBRE: FRMMENUMANTEN (PR00201.FRM)                                  *
'* AUTOR: JAVIER OSTOLAZA LASA                                          *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: men� principal de mantenimientos                        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit



Private Sub cmdCommand1_Click(intIndex As Integer)
  cmdCommand1(intIndex).Enabled = False
  Select Case intIndex
     Case 0
        'Load frmtipoactividad
        'Call frmtipoactividad.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0208")
        'Unload frmtipoactividad
        'Set frmtipoactividad = Nothing
    Case 1
        'Load frmtipodegrupo
        'Call frmtipodegrupo.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0202")
        'Unload frmtipodegrupo
        'Set frmtipodegrupo = Nothing
    Case 2
        'Load frmPreguntas
        'Call frmPreguntas.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0131")
        'Unload frmPreguntas
        'Set frmPreguntas = Nothing
    Case 3
        'Urgencias
        Call objsecurity.LaunchProcess("PR0151")
    Case 4
        'Load frmtiposinteraccion
        'Call frmtiposinteraccion.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0205")
        'Unload frmtiposinteraccion
        'Set frmtiposinteraccion = Nothing
    Case 5
        'Load frmtiposdemuestra
        'Call frmtiposdemuestra.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0206")
        'Unload frmtiposdemuestra
        'Set frmtiposdemuestra = Nothing
    Case 6
        'Load frmtiposderespuesta
        'Call frmtiposderespuesta.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0207")
        'Unload frmtiposderespuesta
        'Set frmtiposderespuesta = Nothing
    Case 7
        'Load frmListadeRespuestas
        'Call frmListadeRespuestas.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0135")
        'Unload frmListadeRespuestas
        'Set frmListadeRespuestas = Nothing
    Case 8
        'Load frminteraccion
        'Call frminteraccion.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0210")
        'Unload frminteraccion
        'Set frminteraccion = Nothing
    Case 9
        Unload Me
    Case 10
        'Load frmestadoactuacion
        'Call frmestadoactuacion.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0211")
        'Unload frmestadoactuacion
        'Set frmestadoactuacion = Nothing
    Case 11
        'Load frmestadomuestra
        'Call frmestadomuestra.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0233")
        'Unload frmestadomuestra
        'Set frmestadomuestra = Nothing
  End Select
  cmdCommand1(intIndex).Enabled = True
End Sub




Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objApp.HelpContext
    Case 20
      Call objApp.About
  End Select
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
        'Load frmtipoactividad
        'Call frmtipoactividad.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0208")
        'Unload frmtipoactividad
        'Set frmtipoactividad = Nothing
    Case 20
        'Load frmtiposdemuestra
        'Call frmtiposdemuestra.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0206")
        'Unload frmtiposdemuestra
        'Set frmtiposdemuestra = Nothing
    Case 30
        'Load frmtipodegrupo
        'Call frmtipodegrupo.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0202")
        'Unload frmtipodegrupo
        'Set frmtipodegrupo = Nothing
    Case 40
        'Load frmtiposderespuesta
        'Call frmtiposderespuesta.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0207")
        'Unload frmtiposderespuesta
        'Set frmtiposderespuesta = Nothing
    Case 50
        'Load frmtiposinteraccion
        'Call frmtiposinteraccion.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0205")
        'Unload frmtiposinteraccion
        'Set frmtiposinteraccion = Nothing
    Case 60
        'Load frminteraccion
        'Call frminteraccion.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0210")
        'Unload frminteraccion
        'Set frminteraccion = Nothing
    Case 70
        'Load frmListadeRespuestas
        'Call frmListadeRespuestas.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0135")
        'Unload frmListadeRespuestas
        'Set frmListadeRespuestas = Nothing
    Case 80
        'Load frmestadoactuacion
        'Call frmestadoactuacion.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0211")
        'Unload frmestadoactuacion
        'Set frmestadoactuacion = Nothing
    Case 90
        'Load frmPreguntas
        'Call frmPreguntas.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0131")
        'Unload frmPreguntas
        'Set frmPreguntas = Nothing
    Case 100
        'Load frmRespuestas
        'Call frmRespuestas.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0209")
        'Unload frmRespuestas
        'Set frmRespuestas = Nothing
    
    
    Case 120
        Unload Me
  End Select
End Sub

