VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher DLL Pruebas Definir
' Coded by SIC Donosti
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco

Const PRWinCuestNoResp                As String = "PR0601"
Const PRWinCuestionarioUI             As String = "PR0602"
Const PRWinEscogerDpto                As String = "PR0603"


' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso

  Select Case strProcess
    
    Case PRWinCuestNoResp
      frmCuestPend.Show vbModal
'      Load FrmCuestNoResp
'      'Call objsecurity.AddHelpContext(528)
'      gblnPrimeraLlamada = True
'      Call FrmCuestNoResp.Show(vbModal)
'      Call objsecurity.RemoveHelpContext
'      Unload FrmCuestNoResp
'      Set FrmCuestNoResp = Nothing
    Case PRWinCuestionarioUI
'      Load frmcuestionarioUI
      'Call objsecurity.AddHelpContext(528)
'      Call frmcuestionarioUI.Show(vbModal)
'      Call objsecurity.RemoveHelpContext
'      Unload frmcuestionarioUI
'      Set frmcuestionarioUI = Nothing
    Case PRWinEscogerDpto
'      Load frmEscogerDpto
'      'Call objsecurity.AddHelpContext(528)
'      Call frmEscogerDpto.Show(vbModal)
'      Call objsecurity.RemoveHelpContext
'      Unload frmEscogerDpto
'      Set frmEscogerDpto = Nothing
  End Select
  Call Err.Clear
End Function

Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 58 procesos
  ReDim aProcess(1 To 3, 1 To 4) As Variant
      
  ' VENTANAS
  
  
  aProcess(1, 1) = PRWinCuestNoResp
  aProcess(1, 2) = "U.I. Cuestionarios Sin Responder"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinCuestionarioUI
  aProcess(2, 2) = "Cuestionario"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinEscogerDpto
  aProcess(3, 2) = "Escoger Dpto UI"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  
End Sub
