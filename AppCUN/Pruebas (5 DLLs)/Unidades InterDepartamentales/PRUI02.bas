Attribute VB_Name = "modUI"
Option Explicit

Type uiRespuestasPr
  cPreg As Long
  TipoPreg As Integer
  Modificada As Integer
End Type

Type uiPr
  TiPr As Integer
  nPr As Long
  cDpt As Long
  nPet As Long
  Prueba As String
  fReali As String
  NH As Long
  Pac As String
  fNac As String
End Type

Type uiPruebasPaquetes
  cPaq As Long
  Pr() As uiPr
End Type

Type uiResultadosLaboratorio
  pos As Integer
  cPrueba As Long
  cResultado As Long
  operacion As String
  cPrueba1 As Long
  cResultado1 As Long
End Type
