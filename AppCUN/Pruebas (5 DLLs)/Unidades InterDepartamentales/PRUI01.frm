VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmCuestPend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actuaciones con cuestionarios sin responder"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11610
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   11610
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   465
      Left            =   10350
      TabIndex        =   2
      Top             =   5250
      Width           =   1140
   End
   Begin VB.CommandButton cmdCuestionario 
      Caption         =   "&Cuestionario"
      Height          =   465
      Left            =   9075
      TabIndex        =   1
      Top             =   5250
      Width           =   1140
   End
   Begin ComctlLib.ListView lvwPr 
      Height          =   4890
      Left            =   150
      TabIndex        =   0
      Top             =   225
      Width           =   11340
      _ExtentX        =   20003
      _ExtentY        =   8625
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   9
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Paciente"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   1
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Prueba"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   2
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "F. Reali."
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(4) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   3
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Protocolo"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   4
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "N� Historia"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(6) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   5
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "F. Nacim."
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(7) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   6
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "FReali"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(8) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   7
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "NH"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(9) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   8
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "NC"
         Object.Width           =   0
      EndProperty
   End
   Begin ComctlLib.ImageList imgIcos 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   3
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PRUI01.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PRUI01.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PRUI01.frx":0634
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCuestPend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Dim PrPaq() As uiPruebasPaquetes

Private Sub cmdCuestionario_Click()
  If lvwPr.ListItems.Count > 0 Then
    If lvwPr.ListItems(lvwPr.SelectedItem.Index).Selected = False Then
      MsgBox "Debe seleccionar alguna prueba.", vbInformation, "Cuestionarios"
    Else
      frmResponder.Show vbModal
    End If
  End If
End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  Call LlenarPr
End Sub

Sub LlenarPr()
Dim sql As String
Dim rdo As rdoResultset, rdoLab As rdoResultset
Dim rdoQ As rdoQuery
Dim cDpts As String, EsLab As Integer, cPaq As Integer, nPaqs As Integer, nPrs As Integer
Dim item As ListItem
Dim i As Integer, ctrl As Integer, borrado As Integer

'  Set lvwPaquete.Icons = imgIcos
'  Set lvwPr.Icons = imgIcos
'  Set lvwPaquete.SmallIcons = imgIcos
'  Set lvwPr.SmallIcons = imgIcos
  Screen.MousePointer = 11
  nPaqs = 0
  nPrs = 0
  borrado = False
  sql = "SELECT AD0200.AD02CodDpto " _
      & "FROM AD0300, AD0200 WHERE " _
      & "AD0200.AD02CODDPTO = AD0300.AD02CODDPTO AND " _
      & "AD0300.SG02COD = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = objsecurity.strUser
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  EsLab = False
  If rdo.EOF = True Then
    Screen.MousePointer = 0
    MsgBox "El usuario no pertenece a ning�n departamento.", vbExclamation, Me.Caption
    Exit Sub
  End If
  While rdo.EOF = False
    If InStr(uiCDPTSLAB, rdo(0)) > 0 Then EsLab = True
    cDpts = cDpts & rdo(0) & ", "
    rdo.MoveNext
  Wend
  If EsLab = True Then
    cDpts = cDpts & "-1"
  Else
    cDpts = Left(cDpts, Len(cDpts) - 2)
  End If
  
' Se seleccionan las pruebas con cuestionario
' Primero los paquetes
'  sql = "SELECT PR3300.PR33CodEstandard, PR3300.PR33DesEstandard, PR0100.PR01DesCorta, " _
'      & "TO_CHAR(PR0400.PR04FecIniAct, 'DD/MM/YY HH24:MI'), PR0400.PR04NumActPlan, 0, " _
'      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
'      & "CI2200.CI22NumHistoria, TO_CHAR(CI2200.CI22FecNacim, 'DD/MM/YY'), 0, 0, " _
'      & "TO_CHAR(PR0400.PR04FecIniAct, 'DD/MM/YY') " _
'      & "FROM PR0900, PR3300, PR0300, PR0400, PR0100, CI2200 WHERE " _
'      & "PR3300.PR33CodEstandard = PR0900.PR33CodEstandard AND " _
'      & "PR0900.PR09NumPeticion = PR0300.PR09NumPeticion AND " _
'      & "PR0300.PR03NumActPedi = PR0400.PR03NumActPedi AND " _
'      & "PR0400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
'      & "PR0400.CI21CodPersona = CI2200.CI21CodPersona AND " _
'      & "PR0400.PR37CODESTADO IN (3, 4, 5) AND " _
'      & "PR0400.AD02CodDpto IN (" & cDpts & ") AND " _
'      & "PR0400.PR04NumActPlan IN (" _
'      & "  SELECT UI05Cod FROM UI0500 WHERE UI05Respuesta IS NULL AND UI05ORIGEN = 0) "
'  sql = sql & " UNION " _
'      & "SELECT DISTINCT PR3300.PR33CodEstandard, PR3300.PR33DesEstandard, " _
'      & "NVL(AD0200.AD02DesDpto, 'Laboratorio'), '', 0, 1, " _
'      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
'      & "CI2200.CI22NumHistoria, TO_CHAR(CI2200.CI22FecNacim, 'DD/MM/YY'), " _
'      & "PR0900.PR09NumPeticion, NVL(AD0200.AD02CodDpto, -1), '' " _
'      & "FROM PR0900, PR3300, CI2200, UI0500, AD0200 WHERE " _
'      & "PR3300.PR33CodEstandard = PR0900.PR33CodEstandard AND " _
'      & "PR0900.CI21CodPersona = CI2200.CI21CodPersona AND " _
'      & "UI0500.AD02CodDpto = AD0200.AD02CodDpto (+) AND " _
'      & "PR0900.PR09NumPeticion = UI0500.UI05Cod AND " _
'      & "UI0500.UI05Respuesta IS NULL AND " _
'      & "UI0500.UI05ORIGEN = 1 AND " _
'      & "UI0500.AD02CodDpto IN (" & cDpts & ") " _
'      & "ORDER BY 4"
'
'  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
'  cPaq = 0
'  While rdo.EOF = False
'    If rdo(5) = 0 Then
'      Set item = lvwPr.ListItems.Add(, "A" & rdo(4), rdo(6))
'      item.SubItems(1) = IIf(IsNull(rdo(2)), "", rdo(2))
'      item.SubItems(2) = IIf(IsNull(rdo(3)), "", rdo(3))
'      item.SubItems(6) = IIf(IsNull(rdo(11)), "99999999", Format$(rdo(11), "YYYYMMDD"))
'      item.Tag = rdo(4)
'    Else
'      Set item = lvwPr.ListItems.Add(, "D" & rdo(10) & "P" & rdo(9), rdo(6))
'      item.Tag = rdo(9)
'      If rdo(2) = "Laboratorio" Then
'        sql = "SELECT MA.FechaExtraccion, MP.Historia, MP.Caso " _
'            & "FROM MuestraAsistencia MA, MuestraPrueba MP WHERE " _
'            & "MA.cMuestra = MP.cMuestra AND " _
'            & "(MP.Historia, MP.Caso) IN " _
'            & "(SELECT TO_NUMBER(AD0700.CI22NumHistoria), TO_NUMBER(SUBSTR(AD0800.AD08NumCaso, 7, 4)) " _
'            & "FROM PR0300, PR0400, AD0700, AD0800 WHERE " _
'            & "PR0300.PR03NumActPedi = PR0400.PR03NumActPedi AND " _
'            & "PR0400.AD01CodAsistenci = AD0800.AD01CodAsistenci AND " _
'            & "PR0400.AD07CodProceso = AD0800.AD07CodProceso AND " _
'            & "AD0800.AD07CodProceso = AD0700.AD07CodProceso AND " _
'            & "PR0300.PR09NumPeticion = ?)"
'        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
'        rdoQ(0) = rdo(9)
'        Set rdoLab = rdoQ.OpenResultset(rdOpenForwardOnly)
'        If rdoLab.EOF = False Then
'          item.SubItems(1) = rdo(2)
'          item.SubItems(2) = IIf(IsNull(rdoLab(0)), "", Format$(rdoLab(0), "DD/MM/YY"))
'          item.SubItems(6) = IIf(IsNull(rdoLab(0)), "99999999", Format$(rdoLab(0), "YYYYMMDD"))
'          item.SubItems(7) = IIf(IsNull(rdoLab(1)), "", rdoLab(1))
'          item.SubItems(8) = IIf(IsNull(rdoLab(2)), "", rdoLab(2))
'        Else
'          lvwPr.ListItems.Remove item.Index
'          borrado = True
'        End If
'      End If
'    End If
'    If borrado = False Then
'      item.SubItems(3) = rdo(1)
'      item.SubItems(4) = rdo(7)
'      item.SubItems(5) = IIf(IsNull(rdo(8)), "", rdo(8))
'    Else
'      borrado = False
'    End If
'    rdo.MoveNext
'  Wend

' Luego los protocolos
  sql = "SELECT PR3500.PR35CodProtocolo, PR3500.PR35DesProtocolo, PR0100.PR01DesCorta, " _
      & "TO_CHAR(PR0400.PR04FecIniAct, 'DD/MM/YY HH24:MI'), PR0400.PR04NumActPlan, 0, " _
      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
      & "CI2200.CI22NumHistoria, TO_CHAR(CI2200.CI22FecNacim, 'DD/MM/YY'), 0, 0, " _
      & "TO_CHAR(PR0400.PR04FecIniAct, 'DD/MM/YY') " _
      & "FROM PR0900, PR3500, PR0300, PR0400, PR0100, CI2200 WHERE " _
      & "PR3500.PR35CodProtocolo = PR0900.PR35CodProtocolo AND " _
      & "PR0900.PR09NumPeticion = PR0300.PR09NumPeticion AND " _
      & "PR0300.PR03NumActPedi = PR0400.PR03NumActPedi AND " _
      & "PR0400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR0400.CI21CodPersona = CI2200.CI21CodPersona AND " _
      & "PR0400.PR37CODESTADO IN (3, 4, 5) AND " _
      & "PR0400.AD02CodDpto IN (" & cDpts & ") AND " _
      & "PR0400.PR04NumActPlan IN (" _
      & "  SELECT UI05Cod FROM UI0500 WHERE UI05Respuesta IS NULL AND UI05ORIGEN = 0) "
  sql = sql & " UNION " _
      & "SELECT DISTINCT PR3500.PR35CodProtocolo, PR3500.PR35DesProtocolo, " _
      & "NVL(AD0200.AD02DesDpto, 'Laboratorio'), '', 0, 1, " _
      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
      & "CI2200.CI22NumHistoria, TO_CHAR(CI2200.CI22FecNacim, 'DD/MM/YY'), " _
      & "PR0900.PR09NumPeticion, NVL(AD0200.AD02CodDpto, -1), '' " _
      & "FROM PR0900, PR3500, CI2200, UI0500, AD0200 WHERE " _
      & "PR3500.PR35CodProtocolo = PR0900.PR35CodProtocolo AND " _
      & "PR0900.CI21CodPersona = CI2200.CI21CodPersona AND " _
      & "UI0500.AD02CodDpto = AD0200.AD02CodDpto (+) AND " _
      & "PR0900.PR09NumPeticion = UI0500.UI05Cod AND " _
      & "UI0500.UI05Respuesta IS NULL AND " _
      & "UI0500.UI05ORIGEN = 1 AND " _
      & "UI0500.AD02CodDpto IN (" & cDpts & ") " _
      & "ORDER BY 4"

  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  cPaq = 0
  While rdo.EOF = False
    If rdo(5) = 0 Then
      Set item = lvwPr.ListItems.Add(, "A" & rdo(4), rdo(6))
      item.SubItems(1) = IIf(IsNull(rdo(2)), "", rdo(2))
      item.SubItems(2) = IIf(IsNull(rdo(3)), "", rdo(3))
      item.SubItems(6) = IIf(IsNull(rdo(11)), "99999999", Format$(rdo(11), "YYYYMMDD"))
      item.Tag = rdo(4)
    Else
      Set item = lvwPr.ListItems.Add(, "D" & rdo(10) & "P" & rdo(9), rdo(6))
      item.Tag = rdo(9)
      If rdo(2) = "Laboratorio" Then
        sql = "SELECT MA.FechaExtraccion, MP.Historia, MP.Caso " _
            & "FROM MuestraAsistencia MA, MuestraPrueba MP WHERE " _
            & "MA.cMuestra = MP.cMuestra AND " _
            & "(MP.Historia, MP.Caso) IN " _
            & "(SELECT TO_NUMBER(AD0700.CI22NumHistoria), TO_NUMBER(SUBSTR(AD0800.AD08NumCaso, 7, 4)) " _
            & "FROM PR0300, PR0400, AD0700, AD0800 WHERE " _
            & "PR0300.PR03NumActPedi = PR0400.PR03NumActPedi AND " _
            & "PR0400.AD01CodAsistenci = AD0800.AD01CodAsistenci AND " _
            & "PR0400.AD07CodProceso = AD0800.AD07CodProceso AND " _
            & "AD0800.AD07CodProceso = AD0700.AD07CodProceso AND " _
            & "PR0300.PR09NumPeticion = ?)"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = rdo(9)
        Set rdoLab = rdoQ.OpenResultset(rdOpenForwardOnly)
        If rdoLab.EOF = False Then
          item.SubItems(1) = rdo(2)
          item.SubItems(2) = IIf(IsNull(rdoLab(0)), "", Format$(rdoLab(0), "DD/MM/YY"))
          item.SubItems(6) = IIf(IsNull(rdoLab(0)), "99999999", Format$(rdoLab(0), "YYYYMMDD"))
          item.SubItems(7) = IIf(IsNull(rdoLab(1)), "", rdoLab(1))
          item.SubItems(8) = IIf(IsNull(rdoLab(2)), "", rdoLab(2))
        Else
          lvwPr.ListItems.Remove item.Index
          borrado = True
        End If
      End If
    End If
    If borrado = False Then
      item.SubItems(3) = rdo(1)
      item.SubItems(4) = rdo(7)
      item.SubItems(5) = IIf(IsNull(rdo(8)), "", rdo(8))
    Else
      borrado = False
    End If
    rdo.MoveNext
  Wend

  lvwPr.SortKey = 6
  lvwPr.Sorted = True
  Screen.MousePointer = 0
End Sub

Private Sub lvwPaquete_ItemClick(ByVal item As ComctlLib.ListItem)
  Call LlenarPrPaq(item.Index)
End Sub

Private Sub lvwPr_DblClick()
  If lvwPr.ListItems(lvwPr.SelectedItem.Index).Selected = False Then
    MsgBox "Debe seleccionar alguna prueba.", vbInformation, "Cuestionarios"
  Else
    frmResponder.Show vbModal
  End If
End Sub

Sub LlenarPrPaq(nPaq As Integer)
Dim item As ListItem, i As Integer
    
'  lvwPr.ListItems.Clear
'        Set item = lvwPr.ListItems.Add(, "A" & .nPr, .Prueba)
'        item.SubItems(1) = .fReali
'        item.SubItems(2) = .Pac
'        item.SubItems(3) = .NH
'        item.SubItems(4) = .fNac
'        item.Tag = .nPr
'
'        Set item = lvwPr.ListItems.Add(, "D" & .cDpt & "P" & .nPet, .Prueba)
'        item.SubItems(2) = .Pac
'        item.SubItems(3) = .NH
'        item.SubItems(4) = .fNac
'        item.Tag = .nPet

End Sub
