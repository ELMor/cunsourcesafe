VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmResponder 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Respuestas a las preguntas del protocolo"
   ClientHeight    =   8100
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10095
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8100
   ScaleWidth      =   10095
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   8775
      TabIndex        =   2
      Top             =   750
      Width           =   1215
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Height          =   390
      Left            =   8775
      TabIndex        =   1
      Top             =   75
      Width           =   1215
   End
   Begin Threed.SSFrame SSFrame2 
      Height          =   6765
      Left            =   150
      TabIndex        =   4
      Top             =   1200
      Width           =   9840
      _Version        =   65536
      _ExtentX        =   17357
      _ExtentY        =   11933
      _StockProps     =   14
      Caption         =   "Cuestionario"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.Frame fraRef 
         BorderStyle     =   0  'None
         Height          =   6390
         Left            =   150
         TabIndex        =   5
         Top             =   300
         Width           =   9390
         Begin VB.Frame fraResp 
            BorderStyle     =   0  'None
            Caption         =   "Frame4"
            Height          =   6315
            Left            =   150
            TabIndex        =   6
            Top             =   0
            Width           =   9165
            Begin VB.Frame fraSiNo 
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   0
               Left            =   6750
               TabIndex        =   17
               Top             =   600
               Visible         =   0   'False
               Width           =   1890
               Begin VB.CheckBox chkNo 
                  Caption         =   "No"
                  Height          =   240
                  Index           =   0
                  Left            =   600
                  TabIndex        =   21
                  Top             =   75
                  Width           =   540
               End
               Begin VB.CheckBox chkSi 
                  Caption         =   "S�"
                  Height          =   240
                  Index           =   0
                  Left            =   75
                  TabIndex        =   20
                  Top             =   75
                  Width           =   465
               End
            End
            Begin SSDataWidgets_B.SSDBCombo cboResp 
               Height          =   315
               Index           =   0
               Left            =   6675
               TabIndex        =   19
               Top             =   600
               Visible         =   0   'False
               Width           =   2415
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   131078
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   979
               Columns(0).Caption=   "N�"
               Columns(0).Name =   "N�"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4577
               Columns(1).Caption=   "Respuesta"
               Columns(1).Name =   "Respuesta"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4260
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   -2147483643
               DataFieldToDisplay=   "Column 1"
            End
            Begin VB.TextBox txtResp 
               BackColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   0
               Left            =   6750
               TabIndex        =   3
               TabStop         =   0   'False
               Top             =   600
               Visible         =   0   'False
               Width           =   2340
            End
            Begin MSMask.MaskEdBox mskResp 
               Height          =   315
               Index           =   0
               Left            =   6675
               TabIndex        =   18
               Top             =   600
               Visible         =   0   'False
               Width           =   2415
               _ExtentX        =   4260
               _ExtentY        =   556
               _Version        =   327681
               ClipMode        =   1
               PromptInclude   =   0   'False
               PromptChar      =   "_"
            End
            Begin VB.Line linSep 
               BorderWidth     =   3
               DrawMode        =   1  'Blackness
               Index           =   0
               Visible         =   0   'False
               X1              =   225
               X2              =   9075
               Y1              =   525
               Y2              =   525
            End
            Begin VB.Label lblTiPreg 
               AutoSize        =   -1  'True
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   150
               TabIndex        =   8
               Top             =   150
               Visible         =   0   'False
               Width           =   60
            End
            Begin VB.Label lblPreg 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   75
               TabIndex        =   7
               Top             =   675
               Visible         =   0   'False
               Width           =   6465
            End
         End
      End
      Begin VB.VScrollBar vsResp 
         Height          =   6585
         LargeChange     =   5
         Left            =   9600
         TabIndex        =   0
         Top             =   150
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   1140
      Left            =   150
      TabIndex        =   9
      Top             =   0
      Width           =   8265
      _Version        =   65536
      _ExtentX        =   14579
      _ExtentY        =   2011
      _StockProps     =   14
      Caption         =   "Paciente"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.TextBox txtPr 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1200
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   225
         Width           =   4665
      End
      Begin VB.TextBox txtFNac 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   6975
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   225
         Width           =   990
      End
      Begin VB.TextBox txtPac 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   2175
         Locked          =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   675
         Width           =   5790
      End
      Begin VB.TextBox txtNH 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1200
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   675
         Width           =   915
      End
      Begin VB.CommandButton cmdLab 
         Caption         =   "&Recuperar datos de laboratorio"
         Height          =   390
         Left            =   5475
         TabIndex        =   22
         Top             =   225
         Width           =   2490
      End
      Begin VB.Label lblfNac 
         Caption         =   "F. Nac.:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6075
         TabIndex        =   16
         Top             =   225
         Width           =   840
      End
      Begin VB.Label Label2 
         Caption         =   "Prueba:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   225
         TabIndex        =   15
         Top             =   225
         Width           =   840
      End
      Begin VB.Label Label1 
         Caption         =   "Paciente:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   225
         TabIndex        =   12
         Top             =   675
         Width           =   840
      End
   End
End
Attribute VB_Name = "frmResponder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim nRespTotal As Long, nTiPreg As Integer, alto As Long, nPr As Long, nPet As Long
Dim resp() As uiRespuestasPr, resLab() As uiResultadosLaboratorio
Dim PrimeraVez As Integer, Origen As Integer, NH As Long, NC As Long, nResLab As Integer
Dim ResLabValidadas() As String

Private Sub cboResp_Change(Index As Integer)
  If PrimeraVez = False Then resp(Index).Modificada = True
End Sub

Private Sub cboResp_CloseUp(Index As Integer)
  If PrimeraVez = False Then resp(Index).Modificada = True
End Sub

Private Sub cboResp_GotFocus(Index As Integer)
  Call MoverScroll(cboResp(Index))
End Sub

Private Sub chkNo_GotFocus(Index As Integer)
  Call MoverScroll(fraSiNo(Index))
End Sub

Private Sub chkSi_GotFocus(Index As Integer)
  Call MoverScroll(fraSiNo(Index))
End Sub

Private Sub cmdGuardar_Click()
  Call Guardar
End Sub

Private Sub cmdLab_Click()
Dim sql As String, nPrsValidadas As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer, j As Integer, k As Integer, contador As Integer

  nPrsValidadas = 0
  contador = 0
  sql = "SELECT PA.cTipoPrueba, RA.cResultado, RA.ResultadoAlfanumerico " _
      & "FROM ResultadoAsistencia RA, PruebaAsistencia PA WHERE " _
      & "PA.Historia = RA.Historia AND " _
      & "PA.Caso = RA.Caso AND " _
      & "PA.Secuencia = RA.Secuencia AND " _
      & "PA.nRepeticion = RA.nRepeticion AND " _
      & "PA.Estado = 9 AND " _
      & "PA.Historia = ? AND " _
      & "PA.Caso = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  rdoQ(1) = NC
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    nPrsValidadas = nPrsValidadas + 1
    ReDim Preserve ResLabValidadas(1 To 3, 1 To nPrsValidadas)
    ResLabValidadas(1, nPrsValidadas) = rdo(0)
    ResLabValidadas(2, nPrsValidadas) = rdo(1)
    If Not IsNull(rdo(2)) Then ResLabValidadas(3, nPrsValidadas) = rdo(2)
    rdo.MoveNext
  Wend
  
  For i = 1 To nResLab
    For j = 1 To nPrsValidadas
      If resLab(i).cPrueba = ResLabValidadas(1, j) And resLab(i).cResultado = ResLabValidadas(2, j) Then
        If resLab(i).operacion = "" Then
          mskResp(resLab(i).pos) = ResLabValidadas(3, j)
          contador = contador + 1
        Else
          For k = 1 To nPrsValidadas
            If resLab(i).cPrueba1 = ResLabValidadas(1, k) And resLab(i).cResultado1 = ResLabValidadas(2, k) Then
              contador = contador + 1
              Select Case resLab(i).operacion
                Case "+"
                  mskResp(resLab(i).pos) = ResLabValidadas(3, j) + ResLabValidadas(3, k)
                Case "-"
                  mskResp(resLab(i).pos) = ResLabValidadas(3, j) - ResLabValidadas(3, k)
                Case "*"
                  mskResp(resLab(i).pos) = ResLabValidadas(3, j) * ResLabValidadas(3, k)
                Case "/"
                  mskResp(resLab(i).pos) = CDbl(Format(ResLabValidadas(3, j) / ResLabValidadas(3, k), "0.00"))
              End Select
              Exit For
            End If
          Next k
        End If
        Exit For
      End If
    Next j
  Next i
  MsgBox "Se han actualizado " & contador & " resultados de laboratorio", vbInformation, "Resultados"
End Sub

Private Sub cmdSalir_Click()
Dim i As Integer, res As Integer
  
  For i = 1 To nRespTotal
    If resp(i).Modificada = True Then
      res = MsgBox("Se ha respondido a alguna pregunta y no se ha guardado. �Desea salir de todas formas sin guardar los resultados?", vbQuestion + vbYesNo, "Cuestionarios a responder")
      If res = vbYes Then Exit For Else Exit Sub
    End If
  Next i
  Unload Me
End Sub

Private Sub lvwPr_ItemClick(ByVal item As ComctlLib.ListItem)
Dim nPr As Long, cPr As Long
Dim itemPr As ListItem

  Screen.MousePointer = 11
  fraResp.Visible = False
  Call DescargarControls
  fraResp.Height = fraRef.Height
  fraResp.Top = 0
  vsResp.Value = 1
  nRespTotal = 0
  nTiPreg = 0
  alto = uiTIPREG_TOP
  If Len(item.Key) > 1 Then
    nPr = Val(Right(item.Key, Len(item.Key) - 1))
    cPr = Val(item.Tag)
    Call MostrarScroll
  Else
    Call MostrarScroll
  End If
  fraResp.Visible = True
  Screen.MousePointer = 0
End Sub

Private Sub Form_Load()
Dim clave As String

  Screen.MousePointer = 11
  With frmCuestPend.lvwPr.SelectedItem
    txtPac.Text = .SubItems(3)
    txtPr.Text = .Text
    txtNH.Text = .SubItems(4)
    txtFNac.Text = .SubItems(5)
    nPr = .Tag
    nPet = .Tag
    clave = .Key
    If .SubItems(7) <> "" Then NH = .SubItems(7)
    If .SubItems(8) <> "" Then NC = .SubItems(8)
  End With
  
  fraResp.Visible = False
  fraResp.Height = fraRef.Height
  fraResp.Top = 0
  vsResp.Value = 1
  nRespTotal = 1
  nTiPreg = 0
  nResLab = 0
  PrimeraVez = True
  alto = uiTIPREG_TOP
  If Left(clave, 1) = "A" Then ' Cuestionario asociado a la actuacion
    Origen = 0
    Call LlenarDatos(nPr, True)
    cmdLab.Visible = False
  ElseIf Left(clave, 1) = "D" Then ' Cuestionario asociado al departamento
    Origen = 1
    If Val(Right(clave, Len(clave) - 1)) = -1 Then
      cmdLab.Visible = True
      txtPr.Width = 1965
      txtPr.Visible = True
      lblfNac.Left = 3225
      txtFNac.Left = 4125
      Call LlenarDatos(nPet, False, -1)
    Else
      Call LlenarDatos(nPet, False, Val(Right(clave, Len(clave) - 1)))
    End If
  End If
  Call MostrarScroll
  fraResp.Visible = True
  vsResp.Min = 1
  PrimeraVez = False
  Screen.MousePointer = 0

End Sub

Private Sub mskResp_Change(Index As Integer)
  If PrimeraVez = False Then resp(Index).Modificada = True
End Sub

Private Sub mskResp_GotFocus(Index As Integer)
  Call MoverScroll(mskResp(Index))
End Sub

Private Sub mskResp_KeyPress(Index As Integer, KeyAscii As Integer)
  If KeyAscii < 48 And KeyAscii <> 44 And KeyAscii <> 8 Then
    KeyAscii = 0
  ElseIf KeyAscii > 57 And KeyAscii <> 44 And KeyAscii <> 8 Then
    KeyAscii = 0
  End If
End Sub

Private Sub chkNo_Click(Index As Integer)
  If PrimeraVez = False Then resp(Index).Modificada = True
  If chkNo(Index).Value = 1 Then chkSi(Index).Value = 0
End Sub

Private Sub chkSi_Click(Index As Integer)
  If PrimeraVez = False Then resp(Index).Modificada = True
  If chkSi(Index).Value = 1 Then chkNo(Index).Value = 0
End Sub

Private Sub txtResp_Change(Index As Integer)
  If PrimeraVez = False Then resp(Index).Modificada = True
End Sub

Private Sub txtResp_GotFocus(Index As Integer)
  Call MoverScroll(txtResp(Index))
End Sub

Private Sub vsResp_Change()
  fraResp.Top = fraRef.Top - 300 - uiPASO_SCROLL * (vsResp.Value - 1)
End Sub

Sub LlenarDatos(num As Long, Act As Integer, Optional cDpt As Integer)
Dim cTiPreg As Long, PosTab As Long
Dim nResp As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  Screen.MousePointer = 11
  If Act = True Then ' Se buscan las preguntas de la actuacion
    sql = "SELECT UI0500.UI02CodTiPregunta, UI0200.UI02Desig, PR4000.PR40DesPregunta, " _
        & "UI0500.UI05Respuesta, UI0500.UI05CodTipRespu, UI0500.PR46CodListResp, " _
        & "UI0500.PR40CodPregunta, UI0500.cPrueba, UI0500.cResultado, UI0500.Operacion, " _
        & "UI0500.cPrueba1, UI0500.cResultado1 " _
        & "FROM UI0200, PR4000, UI0500 WHERE " _
        & "UI0500.PR40CodPregunta = PR4000.PR40CodPregunta AND " _
        & "UI0500.UI02CodTiPregunta = UI0200.UI02CodTiPregunta AND " _
        & "UI0500.UI05Cod = ? AND " _
        & "UI0500.UI05Origen = 0 " _
        & "ORDER BY UI0500.UI05OrdenTipo, UI0500.UI05OrdenPreg"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = nPr
  Else ' Se buscan las preguntas del departamento o laboratorio
    sql = "SELECT UI0500.UI02CodTiPregunta, UI0200.UI02Desig, PR4000.PR40DesPregunta, " _
        & "UI0500.UI05Respuesta, UI0500.UI05CodTipRespu, UI0500.PR46CodListResp, " _
        & "UI0500.PR40CodPregunta, UI0500.cPrueba, UI0500.cResultado, UI0500.Operacion, " _
        & "UI0500.cPrueba1, UI0500.cResultado1 " _
        & "FROM UI0200, PR4000, UI0500 WHERE " _
        & "UI0500.PR40CodPregunta = PR4000.PR40CodPregunta AND " _
        & "UI0500.UI02CodTiPregunta = UI0200.UI02CodTiPregunta AND " _
        & "UI0500.UI05Cod = ? AND " _
        & "UI0500.UI05Origen = 1 AND " _
        & "UI0500.AD02CodDpto = ? " _
        & "ORDER BY UI0500.UI05OrdenTipo, UI0500.UI05OrdenPreg"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = nPet
    rdoQ(1) = cDpt
  End If
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  cTiPreg = 0 ' C�digo del tipo de pregunta
  nResp = 0 ' Da el n� de la pregunta en el tipo de pregunta
  PosTab = 0 ' Posici�n del TabIndex de los controles
  While rdo.EOF = False
    If cTiPreg <> rdo(0) Then ' Se inicia un nuevo grupo de respuestas
      If nTiPreg > 0 Then
        Load lblTiPreg(nTiPreg)
        Load linSep(nTiPreg)
        alto = alto + uiDIF_RESP
        nResp = 0
      End If
      lblTiPreg(nTiPreg).Visible = True
      lblTiPreg(nTiPreg).Top = alto
      lblTiPreg(nTiPreg).Caption = rdo(1)
      linSep(nTiPreg).Visible = True
      linSep(nTiPreg).Y1 = alto + uiDIF_LINEA
      linSep(nTiPreg).Y2 = alto + uiDIF_LINEA
      nTiPreg = nTiPreg + 1
      cTiPreg = rdo(0)
    End If
    If nResp = 0 Then alto = alto + uiDIF_PREG Else alto = alto + uiDIF_RESP_RESP
    Load lblPreg(nRespTotal)
    lblPreg(nRespTotal).Visible = True
    lblPreg(nRespTotal).Top = alto
    lblPreg(nRespTotal).Caption = rdo(2) & ":"
    ReDim Preserve resp(1 To nRespTotal)
    With resp(nRespTotal)
      .cPreg = rdo(6)
      .Modificada = False
    End With
    alto = alto - 75
    Select Case rdo(4)
      Case -1
        Load mskResp(nRespTotal)
        mskResp(nRespTotal).Visible = True
        mskResp(nRespTotal).Top = alto
        mskResp(nRespTotal).TabIndex = PosTab
        If Not IsNull(rdo(3)) Then
          mskResp(nRespTotal).Text = rdo(3)
        Else
          nResLab = nResLab + 1
          ReDim Preserve resLab(1 To nResLab)
          With resLab(nResLab)
            .pos = nRespTotal
            .cPrueba = rdo(7)
            .cResultado = rdo(8)
            .operacion = IIf(IsNull(rdo(9)), "", rdo(9))
            .cPrueba1 = IIf(IsNull(rdo(10)), 0, rdo(10))
            .cResultado1 = IIf(IsNull(rdo(11)), 0, rdo(11))
          End With
        End If
        resp(nRespTotal).TipoPreg = 1
      Case 1
        Load mskResp(nRespTotal)
        mskResp(nRespTotal).Visible = True
        mskResp(nRespTotal).Top = alto
        mskResp(nRespTotal).TabIndex = PosTab
        If Not IsNull(rdo(3)) Then mskResp(nRespTotal).Text = rdo(3)
        resp(nRespTotal).TipoPreg = 1
      Case 2
        Load txtResp(nRespTotal)
        txtResp(nRespTotal).Visible = True
        txtResp(nRespTotal).Top = alto
        txtResp(nRespTotal).TabIndex = PosTab
        If Not IsNull(rdo(3)) Then txtResp(nRespTotal).Text = rdo(3)
        resp(nRespTotal).TipoPreg = 2
      Case 3
        Load fraSiNo(nRespTotal)
        fraSiNo(nRespTotal).Visible = True
        fraSiNo(nRespTotal).Top = alto
        Load chkSi(nRespTotal)
        Load chkNo(nRespTotal)
        Set chkSi(nRespTotal).Container = fraSiNo(nRespTotal)
        Set chkNo(nRespTotal).Container = fraSiNo(nRespTotal)
        chkSi(nRespTotal).Visible = True
        chkSi(nRespTotal).TabIndex = PosTab
        PosTab = PosTab + 1
        chkNo(nRespTotal).Visible = True
        chkNo(nRespTotal).TabIndex = PosTab
        chkSi(nRespTotal).Top = 75
        chkNo(nRespTotal).Top = 75
        If Not IsNull(rdo(3)) Then
          If rdo(3) = 1 Then
            chkSi(nRespTotal).Value = 1
          Else
            chkNo(nRespTotal).Value = 1
          End If
        End If
        resp(nRespTotal).TipoPreg = 3
      Case 4
        Load cboResp(nRespTotal)
        cboResp(nRespTotal).Visible = True
        cboResp(nRespTotal).TabIndex = PosTab
        cboResp(nRespTotal).Top = alto
        Call LlenarCombo(cboResp(nRespTotal), rdo(5))
        If Not IsNull(rdo(3)) Then cboResp(nRespTotal).Text = rdo(3)
        resp(nRespTotal).TipoPreg = 4
    End Select
    alto = alto + 75
    
    nResp = nResp + 1
    nRespTotal = nRespTotal + 1
    PosTab = PosTab + 1
    rdo.MoveNext
  Wend
  nRespTotal = nRespTotal - 1
  Screen.MousePointer = 0

End Sub


Sub MostrarScroll()
  
  If alto + uiDIF_RESP > fraResp.Height Then
    vsResp.Max = CInt((alto + uiDIF_RESP - fraRef.Height) / uiPASO_SCROLL) + 1
    fraResp.Height = alto + uiDIF_RESP_RESP
    vsResp.Visible = True
  Else
    vsResp.Visible = False
  End If

End Sub
Sub DescargarControls()
Dim txt As TextBox, lbl As Label, lin As Line
  
  For Each txt In txtResp
    If txt.Index > 0 Then Unload txt
  Next txt
  txtResp(0).Text = ""
  txtResp(0).Visible = False

  For Each lbl In lblTiPreg
    If lbl.Index > 0 Then Unload lbl
  Next lbl
  lblTiPreg(0).Caption = ""
  lblTiPreg(0).Visible = False

  For Each lbl In lblPreg
    If lbl.Index > 0 Then Unload lbl
  Next lbl
  lblPreg(0).Caption = ""
  lblPreg(0).Visible = False
  
  For Each lin In linSep
    If lin.Index > 0 Then Unload lin
  Next lin
  
End Sub

Sub LlenarCombo(cbo As SSDBCombo, cLista As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT PR28NumRespuesta, PR28DesRespuesta FROM PR2800 WHERE PR46CodListResp = ? ORDER BY PR28NumRespuesta"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cLista
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    cbo.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend
  
End Sub

Sub Guardar()
Dim i As Integer
Dim rdoQ As rdoQuery, rdoQLista As rdoQuery
Dim sql As String, sqlLista As String

  Screen.MousePointer = 11
  sql = "UPDATE UI0500 SET UI05Respuesta = ? WHERE " _
      & "UI05Cod = ? AND " _
      & "UI05Origen = ? AND " _
      & "PR40CodPregunta = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  sqlLista = "UPDATE UI0500 SET UI05Respuesta = ?, PR28NumRespuesta = ? WHERE " _
      & "UI05Cod = ? AND " _
      & "UI05Origen = ? AND " _
      & "PR40CodPregunta = ?"
  Set rdoQLista = objApp.rdoConnect.CreateQuery("", sqlLista)
  If Origen = 0 Then rdoQ(1) = nPr Else rdoQ(1) = nPet
  rdoQ(2) = Origen
  rdoQLista(3) = Origen
  If Origen = 0 Then rdoQLista(2) = nPr Else rdoQLista(2) = nPet
  For i = 1 To nRespTotal
    If resp(i).Modificada = True Then
      Select Case resp(i).TipoPreg
        Case 1 ' N�mero
          rdoQ(0) = mskResp(i).Text
          rdoQ(3) = resp(i).cPreg
          rdoQ.Execute
        Case 2 ' Texto
          rdoQ(0) = txtResp(i).Text
          rdoQ(3) = resp(i).cPreg
          rdoQ.Execute
        Case 3 ' Si, No
          If chkSi(i).Value = 1 Then
            rdoQ(0) = 1
          ElseIf chkNo(i).Value = 1 Then
            rdoQ(0) = 0
          Else
            rdoQ(0) = ""
          End If
          rdoQ(3) = resp(i).cPreg
          rdoQ.Execute
        Case 4 ' Lista de respuestas
          rdoQLista(0) = cboResp(i).Text
          rdoQLista(1) = cboResp(i).Value
          rdoQLista(4) = resp(i).cPreg
          rdoQLista.Execute
      End Select
      resp(i).Modificada = False
    End If
  Next i
  Screen.MousePointer = 0
End Sub

Sub MoverScroll(ctrl As Object)
  If ctrl.Top + ctrl.Height > fraRef.Height + uiPASO_SCROLL * (vsResp.Value - 1) And _
        vsResp.Value < vsResp.Max Then
    vsResp.Value = vsResp.Value + 1
  End If

  If ctrl.Top < uiPASO_SCROLL * (vsResp.Value - 1) And _
        vsResp.Value > 1 Then
    If Me.ActiveControl.Index = 0 Then
      vsResp.Value = 1
    Else
      vsResp.Value = vsResp.Value - 1
    End If
  End If

End Sub


