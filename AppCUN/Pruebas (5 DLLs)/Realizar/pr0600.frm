VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmAsociarAnestesia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asociar Anestesia"
   ClientHeight    =   4200
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6630
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   6630
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdasociar 
      Caption         =   "&Asociar"
      Height          =   375
      Left            =   5040
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   3975
      Index           =   1
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   5010
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   0
      SelectTypeRow   =   3
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      SplitterPos     =   1
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      _ExtentX        =   8837
      _ExtentY        =   7011
      _StockProps     =   79
      Caption         =   "ACTUACIONES DEL DEPARTAMENTO"
   End
End
Attribute VB_Name = "frmAsociarAnestesia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
 Dim lngNumPet As Long
   Dim lngNumActPlan As Long
   Dim intTipoRecur As Long
   Dim strrecurso As String
   Dim strdesfase As String
  Dim lngnumact As Long
  Dim BKMK
   Dim codestado As Long
   
   
   
Private Sub cmdasociar_Click()
Dim fecCita As Date
'Conversi�n de tiempos
   Dim intDias As Integer
   Dim bytHoras As Byte
   Dim bytMinutos As Byte
'Campos Primery Key que se obtienen antes de insertar un registro
   Dim numPetic As Long
   Dim numActPedi As Long
   Dim numactplan As Long
   Dim numSolicit As Long
   Dim codactplan As Long
'Datos necesarios que se obtienen con funciones
   Dim codDpto As Integer 'Departamento del usuario
   'Fase
   
   Dim lngMinutFase As Long 'Minutos de ocupaci�n del paciente
   'Tipo Recurso- Fase Actuaci�n
   
'Consultas SQL
   Dim sql As String
   Dim strsqlact As String
   Dim rstact As rdoResultset
   Dim QyInsert As rdoQuery
   Dim RsRespuesta As rdoResultset
   Dim SQL1 As String
   Dim rs1 As rdoResultset
   
'valor de las restrinciones
   Dim vntRest As Variant
'Doctor solicitante
   Dim doctorSolicit As String
Dim lngcodestado As Long
Dim i As Integer
Dim numactplan1 As Long

Dim codproc As Long
Dim codasis As Long


'********************Inicializaci�n********************
On Error Resume Next
Err = 0
DoEvents

objApp.BeginTrans





  Screen.MousePointer = vbHourglass
  
If grdDBGrid1(1).SelBookmarks.Count > 0 Then
For i = 0 To grdDBGrid1(1).SelBookmarks.Count - 1
BKMK = grdDBGrid1(1).SelBookmarks(i)
'PR0300 ACTUACI�N PEDIDA
sql = "INSERT INTO PR0300 (PR03NUMACTPEDI, AD02CODDPTO, CI21CODPERSONA, PR09NUMPETICION, "
sql = sql & "PR01CODACTUACION, PR03INDCONSFIRM, PR03INDLUNES, PR03INDMARTES, "
sql = sql & "PR03INDMIERCOLES, PR03INDJUEVES, PR03INDVIERNES, PR03INDSABADO, PR03INDDOMINGO,"
sql = sql & " PR03INDCITABLE) VALUES (?, ?, ?, ?, ?, 0, 0, 0, 0, 0, 0, 0, 0, 0)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
   numActPedi = fNextClave("PR03NUMACTPEDI") 'Se usar� m�s adelante
   lngnumact = numActPedi
   QyInsert(0) = numActPedi
   QyInsert(1) = glngDptoAnes
   QyInsert(2) = objPipe.PipeGet("PR_CODPER")
   QyInsert(3) = objPipe.PipeGet("PR_NUMPETI")
   QyInsert(4) = grdDBGrid1(1).Columns("Codigo Actuacion").CellValue(BKMK)
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.RollbackTrans
   Exit Sub
End If

'PR0800 PETICI�N-ACTUACI�N PEDIDA
    sql = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA) "
    sql = sql & " VALUES (?, ?, 1)"

Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
   QyInsert(0) = objPipe.PipeGet("PR_NUMPETI")
   QyInsert(1) = numActPedi

QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.RollbackTrans
   Exit Sub
End If

'Datos previos

'PR0600 FASE PEDIDA
sql = "INSERT INTO PR0600 (PR03NUMACTPEDI, PR06NUMFASE, PR06DESFASE, PR06NUMMINOCUPAC, "
sql = sql & "PR06NUMMINFPRE, PR06NUMMAXFPRE, PR06INDHABNATU, PR06INDINIFIN) "
sql = sql & "SELECT ?,PR05NUMFASE,PR05DESFASE,PR05NUMOCUPACI,PR05NUMTMINFPRE,PR05NUMTMAXFPRE,PR05INDHABILNATU,PR05INDINICIOFIN"
sql = sql & " FROM PR0500 WHERE PR01CODACTUACION = ?"
Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
   QyInsert(0) = numActPedi
   QyInsert(1) = grdDBGrid1(1).Columns("Codigo Actuacion").CellValue(BKMK)
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.RollbackTrans
   Exit Sub
End If
'PR1400 RECURSO PEDIDO
sql = "INSERT INTO PR1400 (PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID, AG14CODTIPRECU, "
sql = sql & "AD02CODDPTO, PR14NUMUNIREC, PR14NUMMINOCU, PR14NUMMINDESREC,PR14INDRECPREFE, "
sql = sql & "PR14INDPLANIF) SELECT ?,PR05NUMFASE,PR13NUMNECESID,AG14CODTIPRECU,AD02CODDPTO,PR13NUMUNIREC,PR13NUMTIEMPREC,PR13NUMMINDESF,-1,PR13INDPLANIF"
sql = sql & " FROM PR1300 WHERE PR01CODACTUACION=?"
Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
QyInsert(0) = numActPedi
QyInsert(1) = grdDBGrid1(1).Columns("Codigo Actuacion").CellValue(BKMK)


QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.RollbackTrans
   Exit Sub
End If
'PR0400 ACTUACI�N PLANIFICADA

objPipe.PipeGet ("PR_NUMACTPLAN")
SQL1 = "SELECT AD01CODASISTENCI,AD07CODPROCESO FROM PR0400 WHERE PR04NUMACTPLAN=" & objPipe.PipeGet("PR_NUMACTPLAN")
Set rs1 = objApp.rdoConnect.OpenResultset(SQL1)
If rs1.EOF Then
  codasis = 0
  codproc = 0
  
Else
  codasis = rs1(0)
  codproc = rs1(1)
End If
rs1.Close

codactplan = objPipe.PipeGet("PR_NUMACTP") 'PR03NUMACTPEDI
SQL1 = "SELECT PR37CODESTADO FROM PR0400 WHERE PR03NUMACTPEDI=" & codactplan
Set rs1 = objApp.rdoConnect.OpenResultset(SQL1)
codestado = rs1(0)
rs1.Close
If codasis = 0 And codproc = 0 Then
    sql = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, "
    sql = sql & "CI21CODPERSONA, PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC) "
    sql = sql & "VALUES (?, ?, ?, ?, ?, ?, 0)"
Else
    sql = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, "
    sql = sql & "CI21CODPERSONA, PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC,AD01CODASISTENCI,AD07CODPROCESO) "
    sql = sql & "VALUES (?, ?, ?, ?, ?, ?, 0,?,?)"
End If


Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
   numactplan = fNextClave("PR04NUMACTPLAN")
   lngNumActPlan = numactplan
   QyInsert(0) = numactplan
   QyInsert(1) = grdDBGrid1(1).Columns("Codigo Actuacion").CellValue(BKMK)
   QyInsert(2) = glngDptoAnes  'AD02CODDPTO
   QyInsert(3) = objPipe.PipeGet("PR_CODPER")
   QyInsert(4) = codestado
   QyInsert(5) = numActPedi
   If codasis <> 0 And codproc <> 0 Then
     QyInsert(6) = codasis
     QyInsert(7) = codproc
   End If
   
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.RollbackTrans
   Exit Sub
End If
sql = "INSERT INTO PR6100 (PR03NUMACTPEDI, PR03NUMACTPEDI_ASO"

sql = sql & ") VALUES (?, ?)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", sql)
   QyInsert(0) = numActPedi 'anestesia
   QyInsert(1) = objPipe.PipeGet("PR_NUMACTP") 'PR03NUMACTPEDI
   
   
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.RollbackTrans
   Exit Sub
End If
Next i
Else
MsgBox "No hay ninguna actuacion seleccionada", vbOKOnly + vbInformation
End If


objApp.CommitTrans
Screen.MousePointer = vbDefault

Unload frmAsociarAnestesia

End Sub

Private Sub Form_Load()
Dim sql$, rs As rdoResultset
Call Formatear_grid
sql = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA"
sql = sql & " FROM PR0100,PR0200"
sql = sql & " WHERE PR0200.AD02CODDPTO=" & glngDptoAnes
sql = sql & " AND PR0100.PR01CODACTUACION=PR0200.PR01CODACTUACION "
sql = sql & " AND PR01FECFIN IS NULL"
sql = sql & " ORDER BY PR01DESCORTA"
Set rs = objApp.rdoConnect.OpenResultset(sql)
Do While Not rs.EOF
grdDBGrid1(1).AddItem rs(0) & Chr$(9) _
 & rs(1)
rs.MoveNext
Loop
rs.Close
End Sub
Private Sub Formatear_grid()
With grdDBGrid1(1)
  .Columns(0).Caption = "Codigo Actuacion"
  .Columns(0).Width = 1000
  .Columns(1).Caption = "Descripcion"
  .Columns(1).Width = 3000
End With

End Sub





