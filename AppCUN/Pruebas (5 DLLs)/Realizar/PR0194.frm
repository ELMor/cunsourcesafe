VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmEscogerDpto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Departamentos"
   ClientHeight    =   705
   ClientLeft      =   4095
   ClientTop       =   2940
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   705
   ScaleWidth      =   4680
   Begin VB.CommandButton cmdaceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   3540
      TabIndex        =   0
      Top             =   180
      Width           =   975
   End
   Begin SSDataWidgets_B.SSDBCombo cboDpto 
      Height          =   315
      Left            =   240
      TabIndex        =   1
      Top             =   180
      Width           =   3135
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 1"
   End
End
Attribute VB_Name = "frmEscogerDpto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdAceptar_Click()
    glngdptologin = cboDpto.Columns(0).Value
    Unload Me
End Sub

Private Sub Form_Load()
    Dim sqlstr As String
    Dim qry As rdoQuery
    Dim rs As rdoResultset
    '3/05/00
    sqlstr = "SELECT AD0300.AD02CODDPTO, AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD =? " & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL " & _
             " ORDER BY AD0200.AD02DESDPTO "
    Set qry = objApp.rdoConnect.CreateQuery("", sqlstr)
    qry(0) = objsecurity.strUser
    Set rs = qry.OpenResultset()
    If rs.EOF = False Then
        cboDpto.Text = rs(1)
        Do While Not rs.EOF
            cboDpto.AddItem rs(0) & Chr$(9) & rs(1)
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
End Sub

