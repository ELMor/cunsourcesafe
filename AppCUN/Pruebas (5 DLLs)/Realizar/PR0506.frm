VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmConsultaActPlanAnestesia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de actuaciones planificadas de Anestesia"
   ClientHeight    =   7890
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11415
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7890
   ScaleWidth      =   11415
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPetPendientes 
      Caption         =   "&Peticiones Pendientes"
      Height          =   375
      Left            =   7920
      TabIndex        =   28
      Top             =   7440
      Width           =   1875
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Citar"
      Height          =   375
      Left            =   9960
      TabIndex        =   27
      Top             =   7440
      Width           =   1335
   End
   Begin Crystal.CrystalReport crReport1 
      Left            =   10800
      Top             =   60
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   10320
      TabIndex        =   26
      Top             =   1140
      Width           =   975
   End
   Begin VB.CommandButton cmdImprimirPeticion 
      Caption         =   "Imprimir &Petici�n"
      Height          =   375
      Left            =   1920
      TabIndex        =   25
      Top             =   7440
      Width           =   1575
   End
   Begin VB.CommandButton cmdRealizacionAct 
      Caption         =   "&Realizaci�n Actuaciones"
      Height          =   375
      Left            =   5760
      TabIndex        =   24
      Top             =   7440
      Width           =   1995
   End
   Begin VB.CommandButton cmdMantCitasPac 
      Caption         =   "&Mant. Citas Paciente"
      Height          =   375
      Left            =   3720
      TabIndex        =   23
      Top             =   7440
      Width           =   1815
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "&Datos Actuaci�n"
      Height          =   375
      Left            =   120
      TabIndex        =   22
      Top             =   7440
      Width           =   1695
   End
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   10320
      Top             =   60
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   "|"
   End
   Begin VB.Frame Frame5 
      Caption         =   "Consulta"
      ForeColor       =   &H00C00000&
      Height          =   1995
      Left            =   60
      TabIndex        =   16
      Top             =   60
      Width           =   5895
      Begin VB.OptionButton optPruebas 
         Caption         =   "Pendiente de citar"
         Height          =   255
         Index           =   2
         Left            =   2880
         TabIndex        =   31
         Top             =   1080
         Width           =   2055
      End
      Begin VB.OptionButton optPruebas 
         Caption         =   "Sin Fecha"
         Height          =   255
         Index           =   1
         Left            =   2880
         TabIndex        =   30
         Top             =   720
         Width           =   1215
      End
      Begin VB.OptionButton optPruebas 
         Caption         =   "Con Fecha"
         Height          =   255
         Index           =   0
         Left            =   2880
         TabIndex        =   29
         Top             =   360
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.Frame Frame2 
         Caption         =   "Fechas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1395
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   2715
         Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
            Height          =   315
            Left            =   900
            TabIndex        =   18
            Top             =   300
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2990
            _ExtentY        =   556
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
            Height          =   315
            Left            =   900
            TabIndex        =   19
            Top             =   900
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2990
            _ExtentY        =   556
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Desde:"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   21
            Top             =   360
            Width           =   675
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Hasta:"
            Height          =   255
            Index           =   1
            Left            =   180
            TabIndex        =   20
            Top             =   960
            Width           =   615
         End
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   375
         Left            =   4800
         TabIndex        =   0
         Top             =   1440
         Width           =   975
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1995
      Left            =   6060
      TabIndex        =   9
      Top             =   60
      Width           =   4155
      Begin VB.CommandButton cmdB 
         Caption         =   "&Anterior"
         Height          =   375
         Index           =   1
         Left            =   3060
         TabIndex        =   15
         Top             =   1440
         Width           =   975
      End
      Begin VB.CommandButton cmdB 
         Caption         =   "Si&guiente"
         Height          =   375
         Index           =   0
         Left            =   3060
         TabIndex        =   10
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   3
         Left            =   900
         TabIndex        =   4
         Top             =   1560
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   2
         Left            =   900
         TabIndex        =   3
         Top             =   1140
         Width           =   1995
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   3060
         TabIndex        =   5
         Top             =   300
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   900
         TabIndex        =   2
         Top             =   720
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   0
         Left            =   900
         TabIndex        =   1
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Top             =   1620
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   1200
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   12
         Top             =   780
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   11
         Top             =   360
         Width           =   525
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10320
      TabIndex        =   8
      Top             =   1620
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Actuaciones planificadas"
      ForeColor       =   &H00C00000&
      Height          =   5235
      Left            =   60
      TabIndex        =   6
      Top             =   2100
      Width           =   11265
      Begin SSDataWidgets_B.SSDBGrid grdActuaciones 
         Height          =   4890
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   11025
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   19447
         _ExtentY        =   8625
         _StockProps     =   79
      End
   End
End
Attribute VB_Name = "frmConsultaActPlanAnestesia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strFecDesde$, strFecHasta$
Dim lngTipoAsist&
'variable utilizadas para b�squedas
Dim intBuscar%, blnBuscarOtro As Boolean
Dim cllBuscar As New Collection 'historia|nombre|apel1|apel2
Dim sqlOrder$
Dim sqlOrderRpt As String





Private Sub cmdB_Click(Index As Integer)
    If Index = 0 Then intBuscar = intBuscar + 1 Else intBuscar = intBuscar - 1
    If intBuscar < 0 Then intBuscar = 0
    blnBuscarOtro = True
    cmdBuscar_Click
End Sub

Private Sub cmdBuscar_Click()
    Dim intN%, i%, blnE As Boolean
    
    If fBlnBuscar Then
        cmdB(0).Enabled = True: cmdB(1).Enabled = True
        If blnBuscarOtro = True Then
            blnBuscarOtro = False
            If intBuscar = 0 Then intBuscar = 1: Exit Sub
        Else
            intBuscar = 1
        End If
        
        For i = 1 To cllBuscar.Count
            blnE = False
            awk1 = cllBuscar(i)
            If UCase(Left$(awk1.F(2), Len(txtBuscar(1).Text))) = UCase(txtBuscar(1).Text) _
            And UCase(Left$(awk1.F(3), Len(txtBuscar(2).Text))) = UCase(txtBuscar(2).Text) _
            And UCase(Left$(awk1.F(4), Len(txtBuscar(3).Text))) = UCase(txtBuscar(3).Text) Then
                If txtBuscar(0).Text = "" Then
                    blnE = True
                Else
                    If awk1.F(1) = txtBuscar(0).Text Then blnE = True
                End If
            End If
            If blnE Then
                intN = intN + 1
                If intN = intBuscar Then
                    grdActuaciones.SelBookmarks.RemoveAll
                    LockWindowUpdate Me.hWnd
                    grdActuaciones.MoveFirst
                    grdActuaciones.MoveRecords i - 1
                    LockWindowUpdate 0&
                    grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
                    cmdB(0).SetFocus
                    Exit Sub
                End If
            End If
        Next i
        
        If intBuscar = 1 Then
            MsgBox "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda.", vbInformation, Me.Caption
            Exit Sub
        Else
            intBuscar = intBuscar - 1
            Exit Sub
        End If
    Else
        blnBuscarOtro = False
    End If
End Sub

Private Sub cmdCitar_Click()
    Dim msg$

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0: msg = "No se ha seleccionado ninguna Actuaci�n."
    Case Else: Call pCitar
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption

End Sub

Private Sub cmdConsultar_Click()
    Call pActualizar(-1)
End Sub

Private Sub cmdDatosAct_Click()
    Dim msg$, lngNumActPlan&

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Actuaci�n."
    Case 1
        lngNumActPlan = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(0))
        'Acceso a los Datos de la Actuaci�n
        Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
        Call objsecurity.LaunchProcess("PR0502")
    Case Else
        msg = "Debe Ud. seleccionar una �nica Actuaci�n."
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdImprimir_Click()
Dim strWhere As String
If optPruebas(0).Value = True Then ' Con fecha de cita o fecha de planificaci�n
    strWhere = " PR0467J.AD02CODDPTO = " & constDPTO_ANESTESIA
    strWhere = strWhere & " AND PR0467J.FECHA >= TO_DATE('" & dcboDesde.Text & "','DD/MM/YYYY') "
    strWhere = strWhere & " AND PR0467J.FECHA < TO_DATE('" & CStr(DateAdd("d", 1, CDate(dcboHasta.Text))) & "','DD/MM/YYYY') "
    strWhere = strWhere & sqlOrder
    Call Imprimir_API(strWhere, "PR05062.RPT")
End If
If optPruebas(1).Value = True Then 'Sin Fecha de Planificaci�n
    strWhere = " PR0400.AD02CODDPTO = " & constDPTO_ANESTESIA
    strWhere = strWhere & " AND PR0400.PR37CODESTADO =1 AND PR0300.PR03INDCITABLE=0 "
    strWhere = strWhere & " AND PR0400.PR04FECPLANIFIC IS NULL "
    strWhere = strWhere & sqlOrderRpt
    Call Imprimir_API(strWhere, "PR05061.RPT")
End If
If optPruebas(2).Value = True Then ' Pendientes de Citar
    strWhere = " PR0400.AD02CODDPTO = " & constDPTO_ANESTESIA
    strWhere = strWhere & " AND PR0400.PR37CODESTADO =1 AND PR0300.PR03INDCITABLE=-1"
    Call Imprimir_API(strWhere, "PR0506.RPT")
End If
End Sub

Private Sub cmdImprimirPeticion_Click()
Dim msg$, strW$, i%
Dim sql As String

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox msg, vbExclamation, Me.Caption: Exit Sub
    Case 1
        crReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
    Case Else
        crReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
    End Select
    For i = 0 To grdActuaciones.SelBookmarks.Count - 1
        strW = strW & "{PR0457J.PR04NUMACTPLAN}= " & _
        grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(i)) & " OR "
    Next i
    strW = Left$(strW, Len(strW) - 4)
            
'    Dim vntdata(1) As Variant
'    For i = 0 To grdActuaciones.SelBookmarks.Count - 1
'      sql = sql & "PR0400.PR04NUMACTPLAN= " & grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(i)) & " OR "
'    Next i
'    sql = Left$(sql, Len(sql) - 4)
'    vntdata(1) = sql
'    Call objsecurity.LaunchProcess("PR2400", vntdata(1))


'    With crReport1
'        .PrinterCopies = 1
'        .Destination = crptToPrinter
'        .SelectionFormula = "(" & strW & ")"
'        .Connect = objApp.rdoConnect.Connect
'        .DiscardSavedData = True
'        Screen.MousePointer = vbHourglass
'        .Action = 1
'        Screen.MousePointer = vbDefault
'    End With
End Sub

Private Sub cmdMantCitasPac_Click()
    Dim msg$, vntDatos(1 To 1) As Variant

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ning�n Paciente."
    Case Else 'vale cualquier actuaci�n del paciente
        'Acceso al Mantenimiento de Citas del Paciente
        vntDatos(1) = grdActuaciones.Columns("Cod. Persona").CellText(grdActuaciones.SelBookmarks(0))
        Call objsecurity.LaunchProcess("CI1024", vntDatos)
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdPetPendientes_Click()
    Dim vntDatos(1 To 1) As Variant
    
    'Acceso a la Realizaci�n de Actuaciones
    vntDatos(1) = constDPTO_ANESTESIA
    Call objsecurity.LaunchProcess("PR1137", vntDatos)
End Sub
Private Sub cmdRealizacionAct_Click()
    Dim vntDatos(1 To 1) As Variant
    
    'Acceso a la Realizaci�n de Actuaciones
    vntDatos(1) = constDPTO_ANESTESIA
    Call objsecurity.LaunchProcess("PR0500", vntDatos)
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub



Private Sub dcboDesde_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboHasta.Date = dcboDesde.Date
    End If
End Sub

Private Sub dcboHasta_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboHasta.Date = dcboDesde.Date
    End If
End Sub

Private Sub Form_Load()
    Dim strHoy$
    
    Call pFormatearGrids
    strHoy = strFecha_Sistema
    dcboDesde.Date = strHoy
    dcboHasta.Date = strHoy
    dcboDesde.BackColor = objApp.objUserColor.lngMandatory
    dcboHasta.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub pActualizar(intCol%)
                                                                                                                                                                                                                        Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strFecPlan$, strHoraPlan$, strCama$, strActAsoc$
    Dim strApel1$, strApel2$
    Dim strCitable$
    Dim qry2 As rdoQuery
    Dim rs2 As rdoResultset
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then 'Opci�n por defecto: Fecha
        If sqlOrder = "" Then
            sqlOrder = " ORDER BY FECHA, PR01DESCORTA, PACIENTE"
            sqlOrderRpt = " ORDER BY PR0400.PR04FECPLANIFIC, PR0100.PR01DESCORTA ,CI2200.CI22PRIAPEL,CI2200.CI22SEGAPEL,CI2200.CI22NOMBRE"
        End If
    Else
            Select Case UCase(grdActuaciones.Columns(intCol).Caption)
            Case UCase("Fecha"), UCase("Hora")
                sqlOrder = " ORDER BY FECHA, PR01DESCORTA, PACIENTE"
                sqlOrderRpt = " ORDER BY PR0400.PR04FECPLANIFIC,PR0100.PR01DESCORTA,CI2200.CI22PRIAPEL,CI2200.CI22SEGAPEL,CI2200.CI22NOMBRE"
            Case UCase("Paciente")
                sqlOrder = " ORDER BY PACIENTE, FECHA, PR01DESCORTA"
                sqlOrderRpt = " ORDER BY CI2200.CI22PRIAPEL,CI2200.CI22SEGAPEL,CI2200.CI22NOMBRE,PR0400.PR04FECPLANIFIC,PR0100.PR01DESCORTA"
            Case UCase("N� Hist.")
                sqlOrder = " ORDER BY CI22NUMHISTORIA, FECHA, PR01DESCORTA"
                sqlOrderRpt = " ORDER BY CI22NUMHISTORIA, PR0400.PR04FECPLANIFIC, PR0100.PR01DESCORTA"
            Case UCase("Actuaci�n")
                sqlOrder = " ORDER BY PR01DESCORTA, FECHA, PACIENTE"
                sqlOrderRpt = " ORDER BY PR0100.PR01DESCORTA, PR0400.PR04FECPLANIFIC, CI2200.CI22PRIAPEL,CI2200.CI22SEGAPEL,CI2200.CI22NOMBRE"
            Case UCase("Actuaci�n asociada")
                sqlOrder = " ORDER BY DESASOCIADA,PR01DESCORTA, FECHA, PACIENTE"
                sqlOrderRpt = " ORDER BY PR0100AS.PR01DESCORTA, PR0100.PR01DESCORTA, PR0400.PR04FECPLANIFIC, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL,CI2200.CI22NOMBRE"
            Case UCase("Cama")
                sqlOrder = " ORDER BY AD15CODCAMA, FECHA, PR01DESCORTA"
                sqlOrderRpt = " ORDER BY AD15CODCAMA, PR0400.PR04FECPLANIFIC, PR0100.PR01DESCORTA"
            Case Else: Exit Sub
            End Select
       End If
    
    
    'Fechas
    strFecDesde = dcboDesde.Date
    strFecHasta = dcboHasta.Date
''    'Tipo Asistencia
''    If optTipoAsist(1).Value = True Then 'Hospitalizados
''        lngTipoAsist = constASIST_HOSP
''    ElseIf optTipoAsist(2).Value = True Then 'Ambulatorios
''        lngTipoAsist = constASIST_AMBUL
''    Else 'Todos
''        lngTipoAsist = 0
''    End If

    'vaciado previo del grid y de la colecci�n de b�sqeda
    grdActuaciones.RemoveAll
    Do While cllBuscar.Count > 0: cllBuscar.Remove 1: Loop
    
    'se cambia el scrollbar para evitar el efecto de llenado del grid dejando la �ltima _
    fila vacia durante un cierto rato
    grdActuaciones.ScrollBars = ssScrollBarsNone

    If optPruebas(0).Value = True Then 'Con fecha para realizarse
     'asociadas a una citada
        sql = "SELECT/*+ORDERED INDEX(PR6100 PR6101)*/"
        sql = sql & " PR04AN.PR04NUMACTPLAN, PR01AN.PR01DESCORTA,"
        sql = sql & " PR01AS.PR01DESCORTA DESASOCIADA, CI01AS.CI01FECCONCERT FECHA,"
        sql = sql & " CI2200.CI21CODPERSONA, CI22NUMHISTORIA, CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
        sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
        sql = sql & " GCFN06(AD1500.AD15CODCAMA) AD15CODCAMA"
        sql = sql & " From"
        sql = sql & " PR0400 PR04AN,AD1500, PR6100,PR0100 PR01AN,PR0400 PR04AS,"
        sql = sql & "  CI0100 CI01AS, PR0100 PR01AS,CI2200 "
        sql = sql & " Where"
        sql = sql & " CI01AS.CI01FECCONCERT > TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND CI01AS.CI01FECCONCERT < TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND PR04AN.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND PR04AN.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
        sql = sql & " AND PR04AN.CI21CODPERSONA = CI2200.CI21CODPERSONA"
        sql = sql & " AND PR04AN.PR03NUMACTPEDI = PR6100.PR03NUMACTPEDI"
        sql = sql & " AND PR6100.PR03NUMACTPEDI_ASO = PR04AS.PR03NUMACTPEDI"
        sql = sql & " AND PR04AS.PR04NUMACTPLAN = CI01AS.PR04NUMACTPLAN"
        sql = sql & " AND PR04AS.PR01CODACTUACION = PR01AS.PR01CODACTUACION"
        sql = sql & " AND PR04AN.PR01CODACTUACION = PR01AN.PR01CODACTUACION"
        sql = sql & " AND PR04AN.PR37CODESTADO = 1"
        sql = sql & " AND CI01AS.CI01SITCITA = '1'"
        sql = sql & " AND PR04AS.PR37CODESTADO = 2"
        sql = sql & " AND PR04AN.AD02CODDPTO = " & constDPTO_ANESTESIA
        sql = sql & " Union"
        'asociadas a una no citable con fecha de planificacion
        sql = sql & " SELECT/*+ORDERED INDEX(PR6100 PR6101)*/"
        sql = sql & " PR04AN.PR04NUMACTPLAN, PR01AN.PR01DESCORTA,"
        sql = sql & " PR01AS.PR01DESCORTA DESASOCIADA, PR04AS.PR04FECPLANIFIC FECHA,"
        sql = sql & " CI2200.CI21CODPERSONA, CI22NUMHISTORIA, CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
        sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
        sql = sql & " GCFN06(AD1500.AD15CODCAMA) AD15CODCAMA"
        sql = sql & " From"
        sql = sql & " PR0400 PR04AN,AD1500, PR6100,PR0100 PR01AN,PR0400 PR04AS, PR0300 PR03AS,"
        sql = sql & " PR0100 PR01AS,CI2200 "
        sql = sql & " Where"
        sql = sql & " PR04AS.PR04FECPLANIFIC  > TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND PR04AS.PR04FECPLANIFIC < TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND PR04AN.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND PR04AN.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
        sql = sql & " AND PR04AN.CI21CODPERSONA = CI2200.CI21CODPERSONA"
        sql = sql & " AND PR04AN.PR03NUMACTPEDI = PR6100.PR03NUMACTPEDI"
        sql = sql & " AND PR6100.PR03NUMACTPEDI_ASO = PR04AS.PR03NUMACTPEDI"
        sql = sql & " AND PR04AS.PR01CODACTUACION = PR01AS.PR01CODACTUACION"
        sql = sql & " AND PR04AS.PR03NUMACTPEDI = PR03AS.PR03NUMACTPEDI"
        sql = sql & " AND PR03AS.PR03INDCITABLE = 0"
        sql = sql & " AND PR04AN.PR01CODACTUACION = PR01AN.PR01CODACTUACION"
        sql = sql & " AND PR04AN.PR37CODESTADO = 1"
        sql = sql & " AND PR04AS.PR37CODESTADO = 1"
        sql = sql & " AND PR04AN.AD02CODDPTO = " & constDPTO_ANESTESIA
        sql = sql & " Union"
        'citadas de anestesia
        sql = sql & " SELECT PR04AN.PR04NUMACTPLAN, PR01AN.PR01DESCORTA,"
        sql = sql & "  ' ' DESASOCIADA, CI0100.CI01FECCONCERT FECHA,"
        sql = sql & " CI2200.CI21CODPERSONA, CI22NUMHISTORIA, CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
        sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
        sql = sql & " GCFN06(AD1500.AD15CODCAMA) AD15CODCAMA"
        sql = sql & " From"
        sql = sql & " PR0400 PR04AN, PR0100 PR01AN, CI0100,AD1500,CI2200"
        sql = sql & " Where"
        sql = sql & " CI0100.CI01FECCONCERT > TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND CI0100.CI01FECCONCERT < TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND CI0100.PR04NUMACTPLAN = PR04AN.PR04NUMACTPLAN"
        sql = sql & " AND PR04AN.PR37CODESTADO = 2"
        sql = sql & " AND CI01SITCITA = '1'"
        sql = sql & " AND PR04AN.PR01CODACTUACION = PR01AN.PR01CODACTUACION"
        sql = sql & " AND PR04AN.CI21CODPERSONA = CI2200.CI21CODPERSONA"
        sql = sql & " AND PR04AN.AD02CODDPTO = " & constDPTO_ANESTESIA
        sql = sql & " AND PR04AN.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND PR04AN.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
        sql = sql & " Union"
        'no citables de antesia
        sql = sql & " SELECT"
        sql = sql & " PR04AN.PR04NUMACTPLAN, PR01AN.PR01DESCORTA,' ' DESASOCIADA, PR04FECPLANIFIC FECHA,"
        sql = sql & " CI2200.CI21CODPERSONA, CI22NUMHISTORIA, CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
        sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
        sql = sql & " GCFN06(AD1500.AD15CODCAMA) AD15CODCAMA"
        sql = sql & " From"
        sql = sql & " PR0400 PR04AN, PR0100 PR01AN, PR0300,PR6100,AD1500,CI2200"
        sql = sql & " Where"
        sql = sql & " PR04AN.PR04FECPLANIFIC > TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND PR04AN.PR04FECPLANIFIC < TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND PR04AN.PR37CODESTADO = 1"
        sql = sql & " AND PR04AN.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR03INDCITABLE = 0"
        sql = sql & " AND PR0300.PR03NUMACTPEDI NOT IN (SELECT PR03NUMACTPEDI FROM PR6100)"
        sql = sql & " AND PR04AN.PR01CODACTUACION = PR01AN.PR01CODACTUACION"
        sql = sql & " AND PR04AN.CI21CODPERSONA = CI2200.CI21CODPERSONA"
        sql = sql & " AND PR04AN.AD02CODDPTO = " & constDPTO_ANESTESIA
        sql = sql & " AND PR04AN.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND PR04AN.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = Format(strFecDesde, "dd/mm/yyyy")
            qry(1) = Format(DateAdd("d", 1, strFecHasta), "dd/mm/yyyy")
            qry(2) = Format(strFecDesde, "dd/mm/yyyy")
            qry(3) = Format(DateAdd("d", 1, strFecHasta), "dd/mm/yyyy")
            qry(4) = Format(strFecDesde, "dd/mm/yyyy")
            qry(5) = Format(DateAdd("d", 1, strFecHasta), "dd/mm/yyyy")
            qry(6) = Format(strFecDesde, "dd/mm/yyyy")
            qry(7) = Format(DateAdd("d", 1, strFecHasta), "dd/mm/yyyy")
        Set rs = qry.OpenResultset()
    End If
    If optPruebas(2).Value = True Then
        sql = "SELECT"
        sql = sql & " PR04AN.PR04NUMACTPLAN, PR01AN.PR01DESCORTA,' 'DESASOCIADA, PR04FECPLANIFIC FECHA,"
        sql = sql & " CI2200.CI21CODPERSONA, CI22NUMHISTORIA, CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
        sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
        sql = sql & " GCFN06(AD1500.AD15CODCAMA) AD15CODCAMA"
        sql = sql & " From"
        sql = sql & " PR0400 PR04AN, PR0100 PR01AN, PR0300,AD1500,CI2200"
        sql = sql & " Where"
        sql = sql & " PR04AN.PR37CODESTADO = 1"
        sql = sql & " AND PR04AN.AD02CODDPTO = " & constDPTO_ANESTESIA
        sql = sql & " AND PR04AN.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR03INDCITABLE = -1"
        sql = sql & " AND PR04AN.PR01CODACTUACION = PR01AN.PR01CODACTUACION"
        sql = sql & " AND PR04AN.CI21CODPERSONA = CI2200.CI21CODPERSONA"
        sql = sql & " AND PR04AN.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND PR04AN.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
        sql = sql & sqlOrder
        Set rs = objApp.rdoConnect.OpenResultset(sql)
    End If
    If optPruebas(1).Value = True Then
        sql = "SELECT"
        sql = sql & " PR04AN.PR04NUMACTPLAN, PR01AN.PR01DESCORTA,"
        sql = sql & " PR01AS.PR01DESCORTA DESASOCIADA, ' ' FECHA,"
        sql = sql & " CI2200.CI21CODPERSONA, CI22NUMHISTORIA, CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
        sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
        sql = sql & " GCFN06(AD1500.AD15CODCAMA) AD15CODCAMA"
        sql = sql & " From"
        sql = sql & " PR0400 PR04AN,PR0400 PR04AS,PR6100,PR0300, PR0100 PR01AS,"
        sql = sql & " PR0100 PR01AN, CI2200, AD1500"
        sql = sql & " Where"
        sql = sql & " PR04AN.PR37CODESTADO = 1"
        sql = sql & " AND PR04AN.AD02CODDPTO = 223"
        sql = sql & " AND PR04AN.PR04FECPLANIFIC IS NULL"
        sql = sql & " AND PR04AN.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR03INDCITABLE = 0"
        sql = sql & " AND PR04AN.PR01CODACTUACION= PR01AN.PR01CODACTUACION"
        sql = sql & " AND PR04AN.CI21CODPERSONA = CI2200.CI21CODPERSONA"
        sql = sql & " AND PR04AN.PR03NUMACTPEDI = PR6100.PR03NUMACTPEDI(+)"
        sql = sql & " AND PR6100.PR03NUMACTPEDI_ASO = PR04AS.PR03NUMACTPEDI(+)"
        sql = sql & " AND PR04AS.PR01CODACTUACION= PR01AS.PR01CODACTUACION(+)"
        sql = sql & " AND PR04AN.AD07CODPROCESO  = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND PR04AN.AD01CODASISTENCI  = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
        sql = sql & sqlOrder
        Set rs = objApp.rdoConnect.OpenResultset(sql)
    End If
    
    Do While Not rs.EOF
     
        If IsNull(rs!AD15CODCAMA) Then strCama = "" Else strCama = rs!AD15CODCAMA
        If IsNull(rs!DESASOCIADA) Then strActAsoc = "" Else strActAsoc = rs!DESASOCIADA

        
        grdActuaciones.AddItem rs!fecha & Chr$(9) _
                            & rs!PACIENTE & Chr$(9) _
                            & rs!PR01DESCORTA & Chr$(9) _
                            & strActAsoc & Chr$(9) _
                            & rs!CI22NUMHISTORIA & Chr$(9) _
                            & strCama & Chr$(9) _
                            & rs!PR04NUMACTPLAN & Chr$(9) _
                            & rs!CI21CODPERSONA & Chr$(9)
        If IsNull(rs!CI22PRIAPEL) Then strApel1 = "" Else strApel1 = rs!CI22PRIAPEL
        If IsNull(rs!CI22SEGAPEL) Then strApel2 = "" Else strApel2 = rs!CI22SEGAPEL
        cllBuscar.Add rs!CI22NUMHISTORIA & "|" & rs!CI22NOMBRE & "|" & strApel1 & "|" & strApel2 & "|" & rs!CI21CODPERSONA
        rs.MoveNext
    Loop
    rs.Close
'    qry.Close
    
    'se pone el scrollbar en su estado normal
    grdActuaciones.ScrollBars = ssScrollBarsAutomatic
    If optPruebas(2).Value = True Then cmdCitar.Enabled = True Else cmdCitar.Enabled = False
    Screen.MousePointer = vbDefault
'    End If
End Sub

Private Sub pFormatearGrids()
    With grdActuaciones
        .Columns(0).Caption = "Fecha"
        .Columns(0).Width = 1700
        .Columns(1).Caption = "Paciente"
        .Columns(1).Width = 3100
        .Columns(2).Caption = "Actuaci�n"
        .Columns(2).Width = 2600
        .Columns(3).Caption = "Actuaci�n asociada"
        .Columns(3).Width = 2000
        .Columns(4).Caption = "N� Hist."
        .Columns(4).Alignment = ssCaptionAlignmentRight
        .Columns(4).Width = 700
        .Columns(5).Caption = "Cama"
        .Columns(5).Width = 600
        .Columns(6).Caption = "Num. Act."
        .Columns(6).Visible = False
        .Columns(7).Caption = "Cod. Persona"
        .Columns(7).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Function fBlnBuscar() As Boolean
    Dim i%
    For i = 0 To txtBuscar.Count - 1
        If txtBuscar(i).Text <> "" Then fBlnBuscar = True: Exit Function
    Next i
End Function

Private Sub grdActuaciones_Click()
    If grdActuaciones.SelBookmarks.Count > 1 Then
        If grdActuaciones.Columns("N� Hist.").CellText(grdActuaciones.SelBookmarks(0)) <> grdActuaciones.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, Me.Caption
            grdActuaciones.SelBookmarks.Remove grdActuaciones.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub grdActuaciones_HeadClick(ByVal ColIndex As Integer)
    If grdActuaciones.Rows > 0 Then Call pActualizar(ColIndex)
End Sub

Private Sub txtBuscar_Change(Index As Integer)
    cmdB(0).Enabled = False: cmdB(1).Enabled = False
End Sub

Private Sub txtBuscar_GotFocus(Index As Integer)
    txtBuscar(Index).SelStart = 0: txtBuscar(Index).SelLength = Len(txtBuscar(Index).Text)
End Sub

Private Sub txtBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub


Private Sub pCitar()
    Dim strCodPersona$
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim i%, n%
    Dim nCitas%, blnYaCitada As Boolean
    Dim msg$, msg1$
    
    'Query para comprobar si la actuaci�n ha sido citada
    sql = "SELECT count(CI31NUMSOLICIT) FROM CI0100"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA = '" & constESTCITA_CITADA & "'"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    
    'C�digo de persona
    strCodPersona = grdActuaciones.Columns("Cod. Persona").CellText(grdActuaciones.SelBookmarks(0))
    
    'Se procesan todas las actuaciones del paciente que se encuentran en el grid
    n = cllBuscar.Count: i = 1
    Do While i <= n
        awk1 = cllBuscar(i)
        If awk1.F(5) = strCodPersona Then
            grdActuaciones.SelBookmarks.RemoveAll
            LockWindowUpdate Me.hWnd
            grdActuaciones.MoveFirst
            grdActuaciones.MoveRecords i - 1
            LockWindowUpdate 0&
            grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
'            If grdActuaciones.Columns("Cita").Text = "SI" Then
                'se comprueba que la actuaci�n no ha sido citada desde otro ordenador _
                por si todav�a no se ha refrescado la pantalla
                qry(0) = grdActuaciones.Columns("Num. Act.").Value
                Set rs = qry.OpenResultset()
                If rs(0) = 0 Then 'NO ha sido citada todav�a
                    Call objsecurity.LaunchProcess("CI1150", grdActuaciones.Columns("Num. Act.").Value)
                    qry(0) = grdActuaciones.Columns("Num. Act.").Value
                    Set rs = qry.OpenResultset()
                    If rs(0) > 0 Then 'Ha sido citada con �xito
                        nCitas = nCitas + 1
                        cllBuscar.Remove i
                        n = cllBuscar.Count
                        grdActuaciones.DeleteSelected
                        i = i - 1
                    End If
                    rs.Close
                Else
                    blnYaCitada = True
                End If
            End If
'        End If
        i = i + 1
    Loop
    qry.Close
    
    Select Case nCitas
    Case 0: msg = "No se ha citado ninguna actuaci�n."
'    Case 1: msg = "Se ha realizado la cita de 1 actuaci�n."
'    Case Else: msg = "Se ha realizado la cita de " & nCitas & " actuaciones."
    End Select
    If blnYaCitada Then
        msg1 = Chr$(13) & Chr$(13)
        msg1 = msg1 & "Alguna de las actuaciones del paciente ha sido citada previamente"
        msg1 = msg1 & " desde otro ordenador." & Chr$(13) & Chr$(13)
        msg1 = msg1 & "Se va a proceder a actualizar los datos de la pantalla."
    End If
    If msg & msg1 <> "" Then MsgBox msg & msg1, vbInformation, Me.Caption
    If msg1 <> "" Then
        Call pActualizar(-1)
    End If
End Sub
Private Sub grdActuaciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub
