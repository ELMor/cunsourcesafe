VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmCamasQuir 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Camas libres de quir�fano"
   ClientHeight    =   3825
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5160
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3825
   ScaleWidth      =   5160
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSinCama 
      Caption         =   "&Sin cama"
      Height          =   390
      Left            =   3825
      TabIndex        =   11
      Top             =   1950
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   390
      Left            =   3825
      TabIndex        =   10
      Top             =   3300
      Width           =   1140
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   3825
      TabIndex        =   9
      Top             =   1500
      Width           =   1140
   End
   Begin ComctlLib.ListView lvwPreanestesia 
      Height          =   2190
      Left            =   150
      TabIndex        =   7
      Top             =   1500
      Width           =   1740
      _ExtentX        =   3069
      _ExtentY        =   3863
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Preanestesia"
         Object.Width           =   1764
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Paciente"
      ForeColor       =   &H00800000&
      Height          =   1365
      Left            =   150
      TabIndex        =   0
      Top             =   75
      Width           =   4815
      Begin VB.TextBox txtPac 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   825
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   825
         Width           =   3840
      End
      Begin VB.TextBox txtNH 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   825
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   300
         Width           =   840
      End
      Begin VB.TextBox txtQuir 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   3150
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   300
         Width           =   1515
      End
      Begin VB.Label Label2 
         Caption         =   "Paciente:"
         Height          =   240
         Index           =   2
         Left            =   75
         TabIndex        =   6
         Top             =   900
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "N� Hist:"
         Height          =   240
         Index           =   1
         Left            =   150
         TabIndex        =   5
         Top             =   375
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Quir�fano citado:"
         Height          =   240
         Index           =   3
         Left            =   1800
         TabIndex        =   4
         Top             =   375
         Width           =   1290
      End
   End
   Begin ComctlLib.ListView lvwQuir 
      Height          =   2190
      Left            =   1950
      TabIndex        =   8
      Top             =   1500
      Width           =   1740
      _ExtentX        =   3069
      _ExtentY        =   3863
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Quir�fano"
         Object.Width           =   1764
      EndProperty
   End
End
Attribute VB_Name = "frmCamasQuir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public seguir As Integer '0: Error, 1: Va a preanestesia, 2: Va a quir�fano
Public camaIni As String, nProc As Long, nAsist As Long ' Datos iniciales del paciente
Public nPrs As String
Dim cCamaFin As String, cTipoCama As Integer

Private Sub cmdAceptar_Click()
      
  If cCamaFin = "" Then
    MsgBox "Debe seleccionar una cama a la que trasladar al paciente.", vbExclamation, Me.Caption
    Exit Sub
  End If
  If camaIni <> "" Then
    If CambioCamaPlantaQuir = False Then
      seguir = 0
      Exit Sub
    End If
  Else
    If IngresoQuir = False Then
      seguir = 0
      Exit Sub
    End If
  End If
  If cTipoCama = constCAMAPREANESTESIA Then
    seguir = 1
  ElseIf cTipoCama = constCAMAQUIROFANO Then
    seguir = 2
  End If
  Me.Visible = False
End Sub

Function CambioCamaPlantaQuir() As Integer
Dim sql As String, FechaHora As String
Dim cCamaIni As String, cControlIni As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
' Se traslada un paciente de planta a quir�fano
  Screen.MousePointer = 11
    
  sql = "SELECT GCFN07('" & camaIni & "'), AD02CodDpto FROM AD1500 WHERE AD15CodCama = GCFN07('" & camaIni & "')"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  cCamaIni = rdo(0)
  cControlIni = rdo(1)
  If cControlIni = constDPTO_QUIROFANO Then
    Screen.MousePointer = 0
    MsgBox "El paciente est� ingresado el la cama de quir�fano " & camaIni & ". Debe dar de alta al paciente antes de ingresarlo de nuevo en quir�fano.", vbExclamation, Me.Caption
    CambioCamaPlantaQuir = False
    Exit Function
  End If
  CambioCamaPlantaQuir = True
  FechaHora = FechaHoraActual

  objApp.BeginTrans

' ************ CAMA INICIAL (de la que viene el paciente) ******************
' Se deja la cama reservada para el paciente (paciente en quir�fano)
  sql = "UPDATE AD1500 SET AD14CODESTCAMA = ? WHERE AD15CODCAMA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = constCAMAPACQUIROFANO
  rdoQ(1) = cCamaIni
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then GoTo Cancelar
        
' Se indica que la cama deja de estar ocupada (hist�rico)
  sql = "UPDATE AD1600 SET AD16FECFIN = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') WHERE " _
      & "AD15CODCAMA = ? AND " _
      & "AD14CODESTCAMA = ? AND " _
      & "AD16FECFIN IS NULL"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = FechaHora
  rdoQ(1) = cCamaIni
  rdoQ(2) = constCAMAOCUPADA
  rdoQ.Execute
  If Err <> 0 Then GoTo Cancelar
        
' Se indica que el paciente se va a quir�fano
  sql = "INSERT INTO AD1600 (AD15CODCAMA, AD16FECCAMBIO, AD14CODESTCAMA, AD01CodAsistenci, " _
      & "AD07CodProceso) " _
      & "VALUES (?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cCamaIni
  rdoQ(1) = FechaHora
  rdoQ(2) = constCAMAPACQUIROFANO
  rdoQ(3) = nAsist
  rdoQ(4) = nProc
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then GoTo Cancelar
  Call IndicarCambios(cControlIni)
            
' ************ CAMA FINAL (a la que se traslada el paciente) *******************
' Se ocupa la cama por el paciente (CamaFin)
  sql = "UPDATE AD1500 SET AD14CODESTCAMA = ?, AD01CODASISTENCI = ?, AD07CODPROCESO = ? " _
      & "WHERE AD15CODCAMA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = constCAMAOCUPADA
  rdoQ(1) = nAsist
  rdoQ(2) = nProc
  rdoQ(3) = cCamaFin
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then GoTo Cancelar

' Se indica que la cama deja de estar libre
  sql = "UPDATE AD1600 SET AD16FECFIN = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') WHERE " _
      & "AD15CODCAMA = ? AND " _
      & "AD14CODESTCAMA = ? AND " _
      & "AD16FECFIN IS NULL"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = FechaHora
  rdoQ(1) = cCamaFin
  rdoQ(2) = constCAMALIBRE
  rdoQ.Execute
  If Err <> 0 Then GoTo Cancelar
        
' Se indica que la cama pasa a estar ocupada por el paciente
  sql = "INSERT INTO AD1600 (AD15CODCAMA, AD16FECCAMBIO, AD14CODESTCAMA, " _
      & "AD01CODASISTENCI, AD07CODPROCESO, AD16FECFIN) " _
      & "VALUES (?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, NULL)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cCamaFin
  rdoQ(1) = FechaHora
  rdoQ(2) = constCAMAOCUPADA
  rdoQ(3) = nAsist
  rdoQ(4) = nProc
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then GoTo Cancelar

' Se indica en dietas el cambio de cama
'  Call CambioDietas(camaIni, cCamaFin, nAsist)

  ' Si la cama es de quir�fano se inicia la prueba. Si es de preanestesia de da entrada en cola
  If cTipoCama = constCAMAQUIROFANO Then
    If Iniciar <> "" Then GoTo Cancelar
  ElseIf cTipoCama = constCAMAPREANESTESIA Then
    If Recibir <> "" Then GoTo Cancelar
  End If
  
  objApp.CommitTrans
  If cControlIni <> constDPTO_QUIROFANO Then Call IndicarCambios(constDPTO_QUIROFANO)
  Screen.MousePointer = 0
  Unload Me
  Exit Function
  
Cancelar:
  Screen.MousePointer = 0
  MsgBox "Error al iniciar la intervenci�n.", vbExclamation, Me.Caption
  objApp.RollbackTrans
  CambioCamaPlantaQuir = False

End Function


Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdSinCama_Click()
  seguir = 3
  Call Recibir
  Unload Me
'  Me.Visible = True
End Sub

Private Sub Form_Load()
  Call CargarCamas
End Sub

Sub CargarCamas()
Dim sql As String
Dim rdo As rdoResultset
Dim item As ListItem

  sql = "SELECT AD15CodCama, GCFN06(AD15CodCama), AD13CodTipoCama FROM AD1500 WHERE " _
      & "AD02CodDpto = " & constDPTO_QUIROFANO & " AND " _
      & "AD14CodEstCama = '" & constCAMALIBRE & "' AND " _
      & "AD13CodTipoCama IN ('" & constCAMAPREANESTESIA & "', '" & constCAMAQUIROFANO & "') " _
      & "ORDER BY AD13CodTipoCama, AD15CodCama"
      
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    If rdo(2) = constCAMAPREANESTESIA Then
      Set item = lvwPreanestesia.ListItems.Add(, , rdo(1))
      item.Tag = rdo(0)
    Else
      Set item = lvwQuir.ListItems.Add(, , rdo(1))
      item.Tag = rdo(0)
    End If
    rdo.MoveNext
  Wend
End Sub

Private Sub lvwPreanestesia_ItemClick(ByVal item As ComctlLib.ListItem)
Dim item1 As ListItem

  For Each item1 In lvwQuir.ListItems
    item1.Selected = False
  Next item1
  cCamaFin = item.Tag
  cTipoCama = constCAMAPREANESTESIA

End Sub

Private Sub lvwQuir_ItemClick(ByVal item As ComctlLib.ListItem)
Dim item1 As ListItem

  For Each item1 In lvwPreanestesia.ListItems
    item1.Selected = False
  Next item1
  cCamaFin = item.Tag
  cTipoCama = constCAMAQUIROFANO
End Sub

Private Function FechaHoraActual()
'Devuelve la fecha y hora actual obtenida del servidor
Dim sql As String, rdo As rdoResultset
    
    sql = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
    Set rdo = objApp.rdoConnect.OpenResultset(sql)
    FechaHoraActual = rdo(0)
End Function


Sub IndicarCambios(cControl As Integer)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AD0200 SET AD02IndActCamas = ? WHERE AD02CodDpto = ? AND AD02IndActCamas = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = True
  rdoQ(1) = cControl
  rdoQ(2) = True
  rdoQ.Execute
End Sub


Function IngresoQuir() As Integer
Dim sql As String, FechaHora As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
' Se traslada un paciente de planta a quir�fano
  Screen.MousePointer = 11
    
  IngresoQuir = True
  FechaHora = FechaHoraActual

  objApp.BeginTrans

' ************ CAMA FINAL (a la que se traslada el paciente) *******************
' Se ocupa la cama por el paciente (CamaFin)
  sql = "UPDATE AD1500 SET AD14CODESTCAMA = ?, AD01CODASISTENCI = ?, AD07CODPROCESO = ? " _
      & "WHERE AD15CODCAMA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = constCAMAOCUPADA
  rdoQ(1) = nAsist
  rdoQ(2) = nProc
  rdoQ(3) = cCamaFin
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then GoTo Cancelar

' Se indica que la cama deja de estar libre
  sql = "UPDATE AD1600 SET AD16FECFIN = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') WHERE " _
      & "AD15CODCAMA = ? AND " _
      & "AD14CODESTCAMA = ? AND " _
      & "AD16FECFIN IS NULL"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = FechaHora
  rdoQ(1) = cCamaFin
  rdoQ(2) = constCAMALIBRE
  rdoQ.Execute
  If Err <> 0 Then GoTo Cancelar
        
' Se indica que la cama pasa a estar ocupada por el paciente
  sql = "INSERT INTO AD1600 (AD15CODCAMA, AD16FECCAMBIO, AD14CODESTCAMA, " _
      & "AD01CODASISTENCI, AD07CODPROCESO, AD16FECFIN) " _
      & "VALUES (?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, NULL)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cCamaFin
  rdoQ(1) = FechaHora
  rdoQ(2) = constCAMAOCUPADA
  rdoQ(3) = nAsist
  rdoQ(4) = nProc
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then GoTo Cancelar

' Se indica en dietas el cambio de cama
'  Call CambioDietas(camaIni, cCamaFin, nAsist)
  
  ' Si la cama es de quir�fano se inicia la prueba. Si es de preanestesia de da entrada en cola
  If cTipoCama = constCAMAQUIROFANO Then
    If Iniciar <> "" Then GoTo Cancelar
  ElseIf cTipoCama = constCAMAPREANESTESIA Then
    If Recibir <> "" Then GoTo Cancelar
  End If
  

  objApp.CommitTrans
  Call IndicarCambios(constDPTO_QUIROFANO)
  Screen.MousePointer = 0
  Unload Me
  Exit Function
  
Cancelar:
  Screen.MousePointer = 0
  MsgBox "Error al realizar el cambio de cama.", vbExclamation, Me.Caption
  objApp.RollbackTrans
  IngresoQuir = False

End Function

Function Iniciar() As String
Dim sql As String
Dim rdoQ As rdoQuery
      
  Iniciar = ""
  On Error Resume Next
  sql = "UPDATE PR0400 SET PR04FecIniAct = SYSDATE, PR37CodEstado = ? " _
      & "WHERE PR04NumActPlan IN " & nPrs
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = constESTACT_REALIZANDOSE
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    Iniciar = "No se ha iniciado la intervenci�n. Error: " & Error
    Exit Function
  End If
  
  sql = "UPDATE PR0300 SET PR03INDCONSFIRM = -1 WHERE PR03NUMACTPEDI IN " _
    & " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN IN " & nPrs & ")"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ.Execute

End Function

Function Recibir() As String
Dim sql As String
Dim rdoQ As rdoQuery
      
  Recibir = ""
  On Error Resume Next
  sql = "UPDATE PR0400 SET PR04FecEntrCola = SYSDATE WHERE PR04NumActPlan IN " & nPrs
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    Recibir = "No se ha podido recibir al paciente. Error:" & Error
    Exit Function
  End If
  
  sql = "UPDATE PR0400 SET PR04FECENTRCOLA = SYSDATE WHERE PR03NUMACTPEDI IN (" _
      & " SELECT PR03NUMACTPEDI FROM PR6100 WHERE PR03NUMACTPEDI_ASO IN " _
      & "    (SELECT PR03NumActPedi FROM PR0400 WHERE PR04NumActPlan IN " & nPrs _
      & " )) AND " _
      & "PR04FECENTRCOLA IS NULL"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ.Execute
  
End Function



