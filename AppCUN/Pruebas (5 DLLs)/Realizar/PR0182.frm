VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmControlarPet 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Controlar Peticiones. Selecci�n Paciente"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   Icon            =   "PR0182.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdActuacionesPet 
      Caption         =   "Actuaciones Petici�n"
      Height          =   375
      Left            =   3600
      TabIndex        =   21
      Top             =   7440
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Index           =   1
      Left            =   480
      TabIndex        =   11
      Top             =   480
      Width           =   10305
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         Index           =   0
         Left            =   240
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   480
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   3201
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0182.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(18)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(17)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(16)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(15)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(14)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(28)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(17)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(15)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(14)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(13)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(25)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(16)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0182.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            Index           =   16
            Left            =   1920
            TabIndex        =   1
            Tag             =   "N�mero de Historia"
            Top             =   480
            Width           =   900
         End
         Begin VB.TextBox txtText1 
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   25
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo del Paciente"
            Top             =   480
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            Index           =   13
            Left            =   240
            TabIndex        =   3
            Tag             =   "Nombre del Paciente"
            Top             =   1200
            Width           =   2535
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            Index           =   14
            Left            =   3120
            TabIndex        =   4
            Tag             =   "Primer Apellido del Paciente"
            Top             =   1200
            Width           =   2535
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            Index           =   15
            Left            =   6000
            TabIndex        =   5
            Tag             =   "Segundo Apellido del Paciente"
            Top             =   1200
            Width           =   2535
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22DNI"
            Height          =   330
            Index           =   17
            Left            =   3120
            TabIndex        =   2
            Tag             =   "D.N.I  del Paciente"
            Top             =   480
            Width           =   1800
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1545
            Index           =   2
            Left            =   -74880
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   120
            Width           =   9255
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16325
            _ExtentY        =   2725
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   240
            TabIndex        =   20
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   1920
            TabIndex        =   19
            Top             =   240
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D.N.I"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   3120
            TabIndex        =   18
            Top             =   240
            Width           =   615
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   240
            TabIndex        =   17
            Top             =   960
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   3120
            TabIndex        =   16
            Top             =   960
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   6000
            TabIndex        =   15
            Top             =   960
            Width           =   1095
         End
      End
   End
   Begin VB.CommandButton cmdPonerPlan 
      Caption         =   "Poner para Plan"
      Height          =   375
      Left            =   6480
      TabIndex        =   10
      Top             =   7440
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Peticiones del Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4200
      Index           =   0
      Left            =   480
      TabIndex        =   7
      Top             =   3000
      Width           =   10335
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3705
         Index           =   0
         Left            =   135
         TabIndex        =   8
         Top             =   360
         Width           =   9960
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   17568
         _ExtentY        =   6535
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmControlarPet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1



Private Sub cmdActuacionesPet_Click()
   cmdActuacionesPet.Enabled = False
   If grdDBGrid1(0).Columns(3).Value <> "" Then
      frmEstadoPeticion.txtpetText1(0).Text = grdDBGrid1(0).Columns(3).Value
      frmEstadoPeticion.txtpetText1(1).Text = grdDBGrid1(0).Columns(4).Value
      frmEstadoPeticion.txtpetText1(2).Text = grdDBGrid1(0).Columns(5).Value
      'frmEstadoPeticion.dtcDateCombopet1(0).Text = grdDBgrid1(0).Columns(6).Value
      Call objsecurity.LaunchProcess("PR0183")
      'Load frmEstadoPeticion
      'frmEstadoPeticion.Show (vbModal)
      'Unload frmEstadoPeticion
      'Set frmEstadoPeticion = Nothing
    Else
      MsgBox " El paciente no tiene actuaciones                           "
    End If
      
   cmdActuacionesPet.Enabled = True
End Sub

Private Sub cmdPonerPlan_Click()
    Dim strupdate As String
    
    Dim strsql1 As String
    Dim rstA1 As rdoResultset
    Dim intResp As Integer
    Dim blnPonerPlan As Boolean
    
    If grdDBGrid1(0).Columns(3).Value <> "" Then
      strsql1 = "SELECT * FROM PR0400 WHERE pr03numactpedi in (select pr03numactpedi from pr0300 where pr09numpeticion=" & grdDBGrid1(0).Columns(3).Value & _
                ") and PR37CODESTADO NOT IN (5,6)"
      Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
      If rstA1.EOF = False Then
        intResp = MsgBox("Algun Actuaci�n de la Petici�n no est� todav�a firmada." & Chr(13) & _
                "�Desea Poner la Petici�n para Plan?", vbInformation + vbYesNo)
        If intResp = vbYes Then
          blnPonerPlan = True
        Else
          blnPonerPlan = False
        End If
      Else
        strsql1 = "SELECT * FROM PR0400 WHERE pr03numactpedi in (select pr03numactpedi from pr0300 where pr09numpeticion=" & grdDBGrid1(0).Columns(3).Value & _
                  ") and PR37CODESTADO<>6"
        Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
        If rstA1.EOF = True Then
          Call MsgBox("La Petici�n tiene canceladas todas sus Actuaciones", vbInformation)
          blnPonerPlan = False
        Else
          blnPonerPlan = True
        End If
      End If
      If blnPonerPlan = True Then
        'strupdate = "UPDATE PR0400 SET PR37CODESTADO=8" & _
        '   " WHERE PR03NUMACTPEDI IN (SELECT PR03NUMACTPEDI FROM PR0300 WHERE PR09NUMPETICION=" & grdDBGrid1(0).Columns(3).Value & ")" & _
        '   " AND PR37CODESTADO=7"
        'objApp.rdoConnect.Execute strupdate, 64
        strupdate = "UPDATE PR0900 SET PR09FECPUESPLAN=(SELECT SYSDATE FROM DUAL)" & _
           " WHERE PR09NUMPETICION=" & grdDBGrid1(0).Columns(3).Value
        objApp.rdoConnect.Execute strupdate, 64
        Call MsgBox("La Petici�n ha sido puesta para Plan")
        objWinInfo.DataRefresh
      End If
    End If
End Sub


Private Sub Form_Activate()
  'Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
      
    '.strDataBase = objEnv.GetValue("Main")
    
      
    .strTable = "CI2200"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
   
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0901J"
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    '.strWhere = "CI21CODPERSONA=" & IdPersona1.Text
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    
    Call .FormAddOrderField("PR09FECPETICION", cwDescending)
    Call .FormAddOrderField("PR09NUMPETICION", cwDescending)
    Call .FormAddRelation("CI21CODPERSONA", txtText1(25))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones Paciente")
    Call .FormAddFilterWhere(strKey, "PR09NUMPETICION", "N�mero Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR09NUMGRUPO", "N�mero Grupo", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "SG02NOM", "Nombre Solicitante", cwString)
    Call .FormAddFilterWhere(strKey, "SG02APE1", "Doctor", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02APE2", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "PR09FECPETICION", "Fecha Petici�n", cwDate)
    Call .FormAddFilterWhere(strKey, "PR09FECPUESPLAN", "Fecha Puesta Plan", cwDate)
    
    Call .FormAddFilterOrder(strKey, "PR09NUMPETICION", "N�mero Petici�n")
    Call .FormAddFilterOrder(strKey, "PR09NUMGRUPO", "N�mero Grupo")
    'Call .FormAddFilterOrder(strKey, "SG02NOM", "Nombre Solicitante")
    Call .FormAddFilterOrder(strKey, "SG02APE1", "Doctor")
    'Call .FormAddFilterOrder(strKey, "SG02APE2", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR09FECPETICION", "Fecha Petici�n")
    Call .FormAddFilterOrder(strKey, "PR09FECPUESPLAN", "Fecha Puesta Plan")
    
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "N�mero Petici�n", "PR09NUMPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "N�mero Grupo", "PR09NUMGRUPO", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "Nombre Solicitante", "SG02NOM", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Doctor", "SG02APE1", cwString, 25)
    'Call .GridAddColumn(objMultiInfo, "Segundo Apellido", "SG02APE2", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Fecha Petici�n", "PR09FECPETICION", cwDate, 8)
    Call .GridAddColumn(objMultiInfo, "Puesta Plan", "PR09FECPUESPLAN", cwDate, 8)
    
    Call .FormCreateInfo(objMasterInfo)
    'Call .FormCreateInfo(objMultiInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1


    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(17)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
  
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
    'Inicializamos el control Idpersona
    'Call objApp.AddCtrl(TypeName(IdPersona1))
    'Call IdPersona1.Init(objApp, objGen)
    'Call IdPersona1.BEGINCONTROL(objApp, objGen)
  grdDBGrid1(0).Columns(3).Width = 1400
  grdDBGrid1(0).Columns(4).Width = 1200
  'grdDBGrid1(0).Columns(5).Width = 1800
  grdDBGrid1(0).Columns(5).Width = 2800
  'grdDBGrid1(0).Columns(7).Width = 1800
  grdDBGrid1(0).Columns(6).Width = 1800
  grdDBGrid1(0).Columns(7).Width = 1800
  'Call objApp.SplashOff
'  objWinInfo.DataMoveFirst
  
  
  Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If btnButton.Index = 16 Then
        If txtText1(25).Text = "" Then
            grdDBGrid1(0).RemoveAll
        End If
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub
Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
      If intIndex = 10 Then
        If txtText1(25).Text = "" Then
            grdDBGrid1(0).RemoveAll
        End If
      End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    'Dim strsql1 As String
    'Dim rstA1 As rdoResultset
    
    'Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    'If grdDBGrid1(0).Columns(3).Value <> "" Then
    '  strsql1 = "SELECT * FROM PR0400 WHERE pr03numactpedi in (select pr03numactpedi from pr0300 where pr09numpeticion=" & grdDBGrid1(0).Columns(3).Value & _
    '            ") and PR37CODESTADO <> 7"
    '  Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
    '  If rstA1.EOF Then
    '    cmdPonerPlan.Enabled = True
    '  Else
    '    cmdPonerPlan.Enabled = False
    '  End If
    '  rstA1.Close
    '  Set rstA1 = Nothing
    'End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
'    Dim strsql1 As String
'    Dim rstA1 As rdoResultset
'
'    Call objWinInfo.CtrlDataChange
'    If grdDBGrid1(0).Columns(3).Value <> "" Then
'      strsql1 = "SELECT * FROM PR0400 WHERE pr04numactpedi in (select pr04numactpedi from pr0300 where pr09numpeticion=" & grdDBGrid1(0).Columns(3).Value & _
'                ") and PR37CODESTADO <> 7"
'      Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
'      If rstA1.EOF Then
'        cmdPonerPlan.Enabled = True
'      Else
'        cmdPonerPlan.Enabled = False
'      End If
'      rstA1.Close
'      Set rstA1 = Nothing
'    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


