VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmConsultaRecursos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recursos de la Actuaci�n"
   ClientHeight    =   4755
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7560
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4755
   ScaleWidth      =   7560
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtNH 
      Height          =   345
      Left            =   1140
      Locked          =   -1  'True
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox txtAct 
      Height          =   345
      Left            =   2100
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   540
      Width           =   4215
   End
   Begin VB.TextBox txtNumActPlan 
      Height          =   345
      Left            =   1140
      Locked          =   -1  'True
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   540
      Width           =   855
   End
   Begin VB.TextBox txtPac 
      Height          =   345
      Left            =   2100
      Locked          =   -1  'True
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   4215
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   435
      Left            =   6420
      TabIndex        =   0
      Top             =   420
      Width           =   1035
   End
   Begin SSDataWidgets_B.SSDBGrid ssgrdRecursos 
      Height          =   3690
      Left            =   120
      TabIndex        =   5
      Top             =   960
      Width           =   7305
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   -2147483633
      BackColorOdd    =   -2147483633
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   12885
      _ExtentY        =   6509
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Actuaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   180
      TabIndex        =   7
      Top             =   600
      Width           =   915
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   180
      Width           =   915
   End
End
Attribute VB_Name = "frmConsultaRecursos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngNumActPlan&

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    'se formatea el Grid
    Call pFormatearControles
    
    'se obtiene el n� de actuaci�n planificada
    If objPipe.PipeExist("PR_NActPlan") Then
        lngNumActPlan = objPipe.PipeGet("PR_NActPlan")
        Call objPipe.PipeRemove("PR_NActPlan")
    End If
    
    'se muestran los datos
    If lngNumActPlan > 0 Then
        Call pCargarCabecera
        Call pCargarGrid
    End If
End Sub

Private Sub pCargarGrid()
'*************************************************************************************
'*  Muestra los recursos asociados a la actuaci�n seleccionada
'*  Se muestra el Recurso si est� definido y el Tipo Recurso en caso de que el Recurso
'*  no se haya concretado todav�a
'*************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    ssgrdRecursos.RemoveAll
    
    'recursos planificados: recurso espec�fico
'    SQL = "SELECT AG11DESRECURSO"
'    SQL = SQL & " FROM AG1100, CI2700"
'    SQL = SQL & " WHERE (CI31NUMSOLICIT, CI01NUMCITA) ="
'    SQL = SQL & " (SELECT CI31NUMSOLICIT, CI01NUMCITA"
'    SQL = SQL & " FROM CI0100"
'    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
'    SQL = SQL & " AND '" & constESTCITA_CITADA & "'"
'    SQL = SQL & " AND ROWNUM = 1)" 'si por error est� citada varias veces, que s�lo coja una cita
'    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI2700.AG11CODRECURSO"
    SQL = "SELECT AG11DESRECURSO"
    SQL = SQL & " FROM AG1100, CI2700"
    'si por error la actuaci�n est� citada varias veces se busca el recurso en todas las citas
    'habr� que poner un = en lugar del IN cuando se haya solucionado el problema
    SQL = SQL & " WHERE (CI31NUMSOLICIT, CI01NUMCITA) IN"
    SQL = SQL & " (SELECT CI31NUMSOLICIT, CI01NUMCITA"
    SQL = SQL & " FROM CI0100"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI01SITCITA = '" & constESTCITA_CITADA & "')"
    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI2700.AG11CODRECURSO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        ssgrdRecursos.AddItem rs(0)
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'recursos no planificados: recurso espec�fico o tipo de recurso
    SQL = "SELECT NVL(AG11DESRECURSO, AG14DESTIPRECU)"
    SQL = SQL & " FROM AG1100, AG1400, PR1400"
    SQL = SQL & " WHERE PR1400.PR03NUMACTPEDI ="
    SQL = SQL & " (SELECT PR03NUMACTPEDI"
    SQL = SQL & " FROM PR0400"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?)"
    SQL = SQL & " AND AG1100.AG11CODRECURSO (+)= PR1400.AG11CODRECURSO"
    SQL = SQL & " AND AG1400.AG14CODTIPRECU = PR1400.AG14CODTIPRECU"
    SQL = SQL & " AND PR14INDPLANIF = 0"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        ssgrdRecursos.AddItem rs(0)
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pFormatearControles()
    txtNH.BackColor = objApp.objUserColor.lngReadOnly
    txtPac.BackColor = objApp.objUserColor.lngReadOnly
    txtNumActPlan.BackColor = objApp.objUserColor.lngReadOnly
    txtAct.BackColor = objApp.objUserColor.lngReadOnly
    
    ssgrdRecursos.BackColorEven = objApp.objUserColor.lngReadOnly
    ssgrdRecursos.BackColorOdd = objApp.objUserColor.lngReadOnly
    ssgrdRecursos.Columns(0).Caption = "Recursos"
    ssgrdRecursos.Columns(0).Width = 2500
End Sub

Private Sub pCargarCabecera()
'*************************************************************************************
'*  Muestra los datos del paciente al que se realiza la actuaci�n seleccionada
'*************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    SQL = SQL & " CI2200.CI22NUMHISTORIA,"
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE"
    SQL = SQL & " FROM CI2200, PR0100, PR0400"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    txtNH.Text = rs!CI22NUMHISTORIA
    txtPac.Text = rs!PACIENTE
    txtNumActPlan.Text = rs!PR04NUMACTPLAN
    txtAct.Text = rs!PR01DESCORTA
    rs.Close
    qry.Close
End Sub

