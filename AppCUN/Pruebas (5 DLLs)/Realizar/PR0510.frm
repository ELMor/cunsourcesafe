VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmInformacionAct 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informaci�n de la Actuaci�n"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9360
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   9360
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab tab 
      Height          =   7155
      Left            =   60
      TabIndex        =   11
      Top             =   1440
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   12621
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Datos"
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "ssgrdCuest"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1(3)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Seguimiento"
      TabPicture(1)   =   "PR0510.frx":0000
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdRecitadas"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "fraPeticion"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame3"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Frame4"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "fraCancel"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Frame5"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).ControlCount=   6
      Begin VB.Frame Frame5 
         Caption         =   "Pendiente de recitar"
         ForeColor       =   &H00800000&
         Height          =   1455
         Left            =   -74940
         TabIndex        =   63
         Top             =   4560
         Width           =   9135
         Begin VB.TextBox txtFecCitaPendiente 
            Height          =   315
            Left            =   1740
            TabIndex        =   72
            Top             =   240
            Width           =   1515
         End
         Begin VB.TextBox txtFecPendiente 
            Height          =   315
            Left            =   6300
            TabIndex        =   66
            Top             =   600
            Width           =   1515
         End
         Begin VB.TextBox txtPendiente 
            Height          =   315
            Left            =   1740
            TabIndex        =   65
            Top             =   600
            Width           =   3615
         End
         Begin VB.TextBox txtMotivoPendiente 
            Height          =   375
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   64
            Top             =   960
            Width           =   7275
         End
         Begin VB.Label Label12 
            Alignment       =   1  'Right Justify
            Caption         =   "Fecha anterior:"
            Height          =   195
            Left            =   120
            TabIndex        =   73
            Top             =   300
            Width           =   1575
         End
         Begin VB.Label Label7 
            Alignment       =   1  'Right Justify
            Caption         =   "el d�a:"
            Height          =   195
            Left            =   5700
            TabIndex        =   69
            Top             =   660
            Width           =   555
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            Caption         =   "Dejada pendiente por:"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   68
            Top             =   660
            Width           =   1575
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            Caption         =   "Motivo:"
            Height          =   195
            Index           =   2
            Left            =   660
            TabIndex        =   67
            Top             =   1020
            Width           =   1035
         End
      End
      Begin VB.Frame fraCancel 
         Caption         =   "Cancelaci�n"
         ForeColor       =   &H00800000&
         Height          =   1035
         Left            =   -74940
         TabIndex        =   51
         Top             =   6060
         Width           =   9135
         Begin VB.TextBox txtMotCancel 
            Height          =   375
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   57
            Top             =   540
            Width           =   7335
         End
         Begin VB.TextBox txtUsuCancel 
            Height          =   315
            Left            =   1740
            TabIndex        =   53
            Top             =   180
            Width           =   3615
         End
         Begin VB.TextBox txtFecCancel 
            Height          =   315
            Left            =   6300
            TabIndex        =   52
            Top             =   180
            Width           =   1515
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            Caption         =   "Motivo:"
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   56
            Top             =   600
            Width           =   1035
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            Caption         =   "Cancelada por:"
            Height          =   195
            Index           =   0
            Left            =   360
            TabIndex        =   55
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label14 
            Alignment       =   1  'Right Justify
            Caption         =   "el d�a:"
            Height          =   195
            Left            =   5700
            TabIndex        =   54
            Top             =   240
            Width           =   555
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Realizaci�n"
         ForeColor       =   &H00800000&
         Height          =   975
         Left            =   -74940
         TabIndex        =   36
         Top             =   2160
         Width           =   9135
         Begin VB.TextBox txtFecInfor 
            Height          =   315
            Left            =   6300
            TabIndex        =   48
            Top             =   540
            Width           =   1515
         End
         Begin VB.TextBox txtFecIni 
            Height          =   315
            Left            =   6300
            TabIndex        =   39
            Top             =   180
            Width           =   1515
         End
         Begin VB.TextBox txtFecFin 
            Height          =   315
            Left            =   1740
            TabIndex        =   38
            Top             =   540
            Width           =   1515
         End
         Begin VB.TextBox txtFecEntCola 
            Height          =   315
            Left            =   1740
            TabIndex        =   37
            Top             =   180
            Width           =   1515
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Informada el d�a:"
            Height          =   195
            Left            =   4920
            TabIndex        =   49
            Top             =   600
            Width           =   1335
         End
         Begin VB.Label Label18 
            Alignment       =   1  'Right Justify
            Caption         =   "Se inici� el d�a:"
            Height          =   195
            Left            =   5040
            TabIndex        =   42
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label17 
            Alignment       =   1  'Right Justify
            Caption         =   "Se termin� el d�a:"
            Height          =   255
            Left            =   420
            TabIndex        =   41
            Top             =   600
            Width           =   1275
         End
         Begin VB.Label Label15 
            Alignment       =   1  'Right Justify
            Caption         =   "Entr� en cola el d�a:"
            Height          =   195
            Left            =   120
            TabIndex        =   40
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Citaci�n"
         ForeColor       =   &H00800000&
         Height          =   1095
         Left            =   -74940
         TabIndex        =   29
         Top             =   1020
         Width           =   9135
         Begin VB.TextBox txtMotivoCita 
            Height          =   375
            Left            =   1080
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   74
            Top             =   600
            Width           =   7935
         End
         Begin VB.TextBox txtFecCita 
            Height          =   315
            Left            =   7500
            TabIndex        =   34
            Top             =   240
            Width           =   1515
         End
         Begin VB.TextBox txtFecCitacion 
            Height          =   315
            Left            =   4800
            TabIndex        =   32
            Top             =   240
            Width           =   1515
         End
         Begin VB.TextBox txtUsuCit 
            Height          =   315
            Left            =   1080
            TabIndex        =   31
            Top             =   240
            Width           =   3135
         End
         Begin VB.Label Label16 
            Alignment       =   1  'Right Justify
            Caption         =   "Motivo:"
            Height          =   195
            Index           =   4
            Left            =   420
            TabIndex        =   75
            Top             =   660
            Width           =   615
         End
         Begin VB.Label Label10 
            Alignment       =   1  'Right Justify
            Caption         =   "para el d�a:"
            Height          =   315
            Left            =   6540
            TabIndex        =   35
            Top             =   300
            Width           =   915
         End
         Begin VB.Label Label9 
            Alignment       =   1  'Right Justify
            Caption         =   "el d�a:"
            Height          =   195
            Left            =   4260
            TabIndex        =   33
            Top             =   300
            Width           =   495
         End
         Begin VB.Label Label8 
            Alignment       =   1  'Right Justify
            Caption         =   "Citada por:"
            Height          =   195
            Left            =   120
            TabIndex        =   30
            Top             =   300
            Width           =   915
         End
      End
      Begin VB.Frame fraPeticion 
         Caption         =   "Petici�n"
         ForeColor       =   &H00800000&
         Height          =   615
         Left            =   -74940
         TabIndex        =   28
         Top             =   360
         Width           =   9135
         Begin VB.TextBox txtFecPet 
            Height          =   315
            Left            =   4800
            TabIndex        =   58
            Top             =   180
            Width           =   1515
         End
         Begin VB.TextBox txtFecPlan 
            Height          =   315
            Left            =   7500
            TabIndex        =   46
            Top             =   180
            Width           =   1515
         End
         Begin VB.TextBox txtUsuPet 
            Height          =   315
            Left            =   1080
            TabIndex        =   44
            Top             =   180
            Width           =   3135
         End
         Begin VB.Label Label21 
            Alignment       =   1  'Right Justify
            Caption         =   "con fecha plan:"
            Height          =   255
            Left            =   6120
            TabIndex        =   47
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label20 
            Alignment       =   1  'Right Justify
            Caption         =   "el d�a:"
            Height          =   195
            Left            =   4260
            TabIndex        =   45
            Top             =   240
            Width           =   495
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Pedida por:"
            Height          =   195
            Left            =   60
            TabIndex        =   43
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Otros Datos"
         ForeColor       =   &H00800000&
         Height          =   1275
         Index           =   3
         Left            =   60
         TabIndex        =   19
         Top             =   2460
         Width           =   9075
         Begin VB.TextBox txtCantidad 
            Height          =   285
            Left            =   8340
            Locked          =   -1  'True
            TabIndex        =   25
            Top             =   900
            Width           =   615
         End
         Begin VB.TextBox txtASA 
            Height          =   285
            Left            =   4320
            Locked          =   -1  'True
            TabIndex        =   24
            Top             =   900
            Width           =   435
         End
         Begin VB.TextBox txtOtrosDatos 
            BackColor       =   &H00FFFFFF&
            Height          =   570
            HelpContextID   =   30104
            Left            =   1080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   240
            Width           =   7905
         End
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   375
            Left            =   60
            TabIndex        =   20
            Top             =   780
            Width           =   1575
            Begin VB.CheckBox chkCon 
               Alignment       =   1  'Right Justify
               Caption         =   "Consentimiento"
               Height          =   315
               Left            =   60
               TabIndex        =   21
               Top             =   120
               Width           =   1515
            End
         End
         Begin VB.Label Label11 
            Caption         =   "Ins. de Realizaci�n:"
            Height          =   675
            Left            =   180
            TabIndex        =   71
            Top             =   300
            Width           =   915
         End
         Begin VB.Label lblCantidad 
            Caption         =   "Cantidad:"
            Height          =   255
            Left            =   7560
            TabIndex        =   27
            Top             =   960
            Width           =   735
         End
         Begin VB.Label lblAsa 
            Caption         =   "ASA:"
            Height          =   255
            Left            =   3840
            TabIndex        =   26
            Top             =   960
            Width           =   495
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Observaciones"
         ForeColor       =   &H00800000&
         Height          =   1935
         Index           =   1
         Left            =   60
         TabIndex        =   12
         Top             =   540
         Width           =   9075
         Begin VB.TextBox txtObservPet 
            BackColor       =   &H00FFFFFF&
            Height          =   525
            HelpContextID   =   30104
            Left            =   1080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   240
            Width           =   7920
         End
         Begin VB.TextBox txtObservAct 
            BackColor       =   &H00FFFFFF&
            Height          =   570
            HelpContextID   =   30104
            Left            =   1080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   840
            Width           =   7920
         End
         Begin VB.TextBox txtRealizacion 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Left            =   1080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   1500
            Width           =   7920
         End
         Begin VB.Label Label1 
            Caption         =   "Petici�n:"
            Height          =   195
            Index           =   2
            Left            =   180
            TabIndex        =   18
            Top             =   240
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Actuaci�n:"
            Height          =   195
            Index           =   4
            Left            =   60
            TabIndex        =   17
            Top             =   780
            Width           =   795
         End
         Begin VB.Label Label1 
            Caption         =   "Realizaci�n:"
            Height          =   195
            Index           =   3
            Left            =   60
            TabIndex        =   16
            Top             =   1500
            Width           =   915
         End
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdCuest 
         Height          =   3255
         Left            =   60
         TabIndex        =   23
         Top             =   3780
         Width           =   9075
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   2
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         CaptionAlignment=   0
         Columns.Count   =   2
         Columns(0).Width=   7064
         Columns(0).Caption=   "Pregunta"
         Columns(0).Name =   "Pregunta"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   10583
         Columns(1).Caption=   "Respuesta"
         Columns(1).Name =   "Respuesta"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   16007
         _ExtentY        =   5741
         _StockProps     =   79
         Caption         =   "Cuestionario"
         ForeColor       =   -2147483635
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdRecitadas 
         Height          =   1275
         Left            =   -74940
         TabIndex        =   70
         Top             =   3240
         Width           =   9135
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   4
         AllowUpdate     =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         CaptionAlignment=   0
         Columns.Count   =   4
         Columns(0).Width=   4842
         Columns(0).Caption=   "Recitada por"
         Columns(0).Name =   "Recitada por"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Fecha anterior"
         Columns(1).Name =   "Fecha anterior"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3043
         Columns(2).Caption=   "Fecha recitaci�n"
         Columns(2).Name =   "Fecha recitaci�n"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   7858
         Columns(3).Caption=   "Motivo"
         Columns(3).Name =   "Motivo"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   16113
         _ExtentY        =   2249
         _StockProps     =   79
         Caption         =   "Recitaci�n"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Solicitante"
      ForeColor       =   &H00800000&
      Height          =   1395
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8415
      Begin VB.TextBox txtEstadoCita 
         Height          =   315
         Left            =   5340
         TabIndex        =   61
         Top             =   960
         Width           =   3015
      End
      Begin VB.TextBox txtEstadoActuacion 
         Height          =   315
         Left            =   1020
         TabIndex        =   59
         Top             =   960
         Width           =   3435
      End
      Begin VB.TextBox txtPaciente 
         Height          =   315
         Left            =   1740
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   240
         Width           =   3315
      End
      Begin VB.TextBox txtActuacion 
         Height          =   315
         Left            =   1020
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   600
         Width           =   3855
      End
      Begin VB.TextBox txtHistoria 
         Height          =   315
         Left            =   1020
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   240
         Width           =   675
      End
      Begin VB.TextBox txtDr 
         Height          =   315
         Left            =   5340
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   600
         Width           =   3015
      End
      Begin VB.TextBox txtDpto 
         Height          =   315
         Left            =   5580
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   240
         Width           =   2775
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Est. Cita:"
         Height          =   255
         Left            =   4620
         TabIndex        =   62
         Top             =   1020
         Width           =   675
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Est. Act:"
         Height          =   255
         Left            =   300
         TabIndex        =   60
         Top             =   1020
         Width           =   675
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Paciente:"
         Height          =   195
         Index           =   8
         Left            =   240
         TabIndex        =   10
         Top             =   300
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Actuaci�n:"
         Height          =   195
         Index           =   7
         Left            =   120
         TabIndex        =   9
         Top             =   660
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Dr.:"
         Height          =   195
         Index           =   1
         Left            =   4920
         TabIndex        =   5
         Top             =   660
         Width           =   375
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Dpto.:"
         Height          =   195
         Index           =   0
         Left            =   4920
         TabIndex        =   4
         Top             =   300
         Width           =   615
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   8460
      TabIndex        =   0
      Top             =   1020
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "Resultados disponibles el d�a:"
      Height          =   195
      Left            =   0
      TabIndex        =   50
      Top             =   0
      Width           =   2175
   End
End
Attribute VB_Name = "frmInformacionAct"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngActPlan&

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call pFormatearControles
    If objPipe.PipeExist("PR0510_PR04NUMACTPLAN") Then
        lngActPlan = objPipe.PipeGet("PR0510_PR04NUMACTPLAN")
        Call objPipe.PipeRemove("PR0510_PR04NUMACTPLAN")
    End If
    If lngActPlan > 0 Then
        Call pCargarCabecera
        Call pCargarDatos
        Call pCargarCuestionario
    End If
End Sub

Private Sub pFormatearControles()
    Dim con As Control
    For Each con In Me
        If TypeOf con Is TextBox Then
            con.BackColor = objApp.objUserColor.lngReadOnly
            con.Locked = True
        End If
    Next
    ssgrdCuest.BackColorEven = objApp.objUserColor.lngReadOnly
    ssgrdCuest.BackColorOdd = objApp.objUserColor.lngReadOnly
End Sub

Private Sub pCargarDatos()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, rs1 As rdoResultset
    Dim strObserPet$, strObserAct$, strOtrosDatos$, strObservRecita$
    
    'se traen los datos del solicitante, de la petici�n y de la actuaci�n
    SQL = " SELECT AD0200.AD02DESDPTO,"
    SQL = SQL & " NVL(SG02PR09DOC.SG02TXTFIRMA,SG02PR09DOC.SG02APE1||' '||SG02PR09DOC.SG02APE2||', '||SG02PR09DOC.SG02NOM) DOCTOR,"
    SQL = SQL & " PR09DESOBSERVAC, PR09DESINDICACIO, PR08DESOBSERV, PR08DESINDICAC, PR08DESMOTPET, PR01INDCONSFDO,"
    SQL = SQL & " PR04DESINSTREA,PR04ASA,PR04OBSERV,PR04CANTIDAD,"
    SQL = SQL & " DECODE(PR0900.SG02COD_ADD,'?????',PR0900.SG02COD_ADD,SG02PR09ADD.SG02APE1||' '||SG02PR09ADD.SG02APE2||', '||SG02PR09ADD.SG02NOM) USUPR09,"
    SQL = SQL & " DECODE(PR0400.SG02COD_ADD,'?????',PR0400.SG02COD_ADD,SG02PR04ADD.SG02APE1||' '||SG02PR04ADD.SG02APE2||', '||SG02PR04ADD.SG02NOM) USUPR04,"
    SQL = SQL & " DECODE(PR0900.SG02COD_UPD,'?????',PR0900.SG02COD_UPD,SG02PR09UPD.SG02APE1||' '||SG02PR09UPD.SG02APE2||', '||SG02PR09UPD.SG02NOM) USUACTPR09,"
    SQL = SQL & " DECODE(PR0400.SG02COD_UPD,'?????',PR0400.SG02COD_UPD,SG02PR04UPD.SG02APE1||' '||SG02PR04UPD.SG02APE2||', '||SG02PR04UPD.SG02NOM) USUACTPR04,"
    SQL = SQL & " PR04FECINIACT, PR04FECFINACT, PR04FECENTRCOLA, PR04FECCANCEL, PR04FECINFORM, PR04FECPLANIFIC,"
    SQL = SQL & " PR0400.PR04FECADD, PR0400.PR04FECUPD, PR09FECADD,PR04DESMOTCAN"
    SQL = SQL & " FROM SG0200 SG02PR09ADD, SG0200 SG02PR09UPD, SG0200 SG02PR04ADD, SG0200 SG02PR04UPD,"
    SQL = SQL & " SG0200 SG02PR09DOC, AD0200, PR0900,PR0100, PR0800, PR0300, PR0400"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0900.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    SQL = SQL & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    SQL = SQL & " AND AD0200.AD02CODDPTO = PR0900.AD02CODDPTO"
    SQL = SQL & " AND SG02PR09DOC.SG02COD(+) = PR0900.SG02COD"
    SQL = SQL & " AND SG02PR09ADD.SG02COD = PR0900.SG02COD_ADD"
    SQL = SQL & " AND SG02PR09UPD.SG02COD(+) = PR0900.SG02COD_UPD"
    SQL = SQL & " AND SG02PR04ADD.SG02COD = PR0400.SG02COD_ADD"
    SQL = SQL & " AND SG02PR04UPD.SG02COD(+) = PR0400.SG02COD_UPD"
    SQL = SQL & " AND PR0800.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    SQL = SQL & " AND PR0800.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        If Not IsNull(rs!AD02DESDPTO) Then txtDpto = rs!AD02DESDPTO
        If Not IsNull(rs!DOCTOR) Then txtDr = rs!DOCTOR
        If Not IsNull(rs!PR09DESOBSERVAC) Then
            strObserPet = rs!PR09DESOBSERVAC
            If Right(RTrim(strObserPet), 1) <> "." Then
                strObserPet = strObserPet & "."
            End If
            strObserPet = strObserPet & Chr(13)
        End If
        If Not IsNull(rs!PR09DESINDICACIO) Then
            strObserPet = strObserPet & rs!PR09DESINDICACIO
            If Right(RTrim(strObserPet), 1) <> "." Then
                strObserPet = strObserPet & "."
            End If
            strObserPet = strObserPet & Chr(13)
        End If
        If Not IsNull(rs!PR08DESOBSERV) Then
            strObserAct = rs!PR08DESOBSERV
            If Right(RTrim(strObserAct), 1) <> "." Then
                strObserAct = strObserAct & "."
            End If
            strObserAct = strObserAct & Chr(13)
        End If
        If Not IsNull(rs!PR08DESINDICAC) Then
            strObserAct = strObserAct & rs!PR08DESINDICAC
            If Right(RTrim(strObserAct), 1) <> "." Then
                strObserAct = strObserAct & "."
            End If
            strObserAct = strObserAct & Chr(13)
        End If
        If Not IsNull(rs!PR08DESMOTPET) Then
            strObserAct = strObserAct & rs!PR08DESMOTPET
            If Right(RTrim(strObserAct), 1) <> "." Then
                strObserPet = strObserAct & "."
            End If
            strObserAct = strObserAct & Chr(13)
        End If
        If Not IsNull(rs!PR04OBSERV) Then txtRealizacion.Text = rs!PR04OBSERV
        If Not IsNull(rs!PR04DESINSTREA) Then
            strOtrosDatos = rs!PR04DESINSTREA
            If Right(RTrim(strOtrosDatos), 1) <> "." Then
                strOtrosDatos = strOtrosDatos & "."
            End If
            strOtrosDatos = strOtrosDatos & Chr(13)
        End If
        If Not IsNull(rs!PR04ASA) Then
             txtASA.Text = rs!PR04ASA
        End If
        If Not IsNull(rs!PR04CANTIDAD) Then
            txtCantidad.Text = rs!PR04CANTIDAD
        End If
        If Not IsNull(rs!PR01INDCONSFDO) And rs!PR01INDCONSFDO = -1 Then chkCon.Value = 1
        If strObserPet <> "" Then txtObservPet.Text = Left(strObserPet, Len(strObserPet) - 1)
        If strObserAct <> "" Then txtObservAct.Text = Left(strObserAct, Len(strObserAct) - 1)
        If strOtrosDatos <> "" Then txtOtrosDatos.Text = Left(strOtrosDatos, Len(strOtrosDatos) - 1)
       
        'cargar los datos de seguimiento
        txtFecPet.Text = Format(rs!PR09FECADD, "dd/mm/yyyy hh:mm")
        txtUsuPet.Text = rs!USUPR09
        If Not IsNull(rs!PR04FECPLANIFIC) Then
            txtFecPlan.Text = Format(rs!PR04FECPLANIFIC, "dd/mm/yyyy hh:mm")
        End If
        If Not IsNull(rs!PR04FECENTRCOLA) Then
            txtFecEntCola.Text = Format(rs!PR04FECENTRCOLA, "dd/mm/yyyy hh:mm")
        End If
        If Not IsNull(rs!PR04FECINIACT) Then
            txtFecIni.Text = Format(rs!PR04FECINIACT, "dd/mm/yyyy hh:mm")
        End If
        If Not IsNull(rs!PR04FECFINACT) Then
            txtFecFin.Text = Format(rs!PR04FECFINACT, "dd/mm/yyyy hh:mm")
        End If
        If Not IsNull(rs!PR04DESMOTCAN) Then
            txtMotCancel.Text = rs!PR04DESMOTCAN
        End If
        If Not IsNull(rs!PR04FECCANCEL) Then
            txtFecCancel.Text = Format(rs!PR04FECCANCEL, "dd/mm/yyyy hh:mm")
            txtUsuCancel = rs!USUACTPR04
        End If
        If Not IsNull(rs!PR04FECINFORM) Then
            txtFecInfor.Text = Format(rs!PR04FECINFORM, "dd/mm/yyyy hh:mm")
        End If

        SQL = "SELECT CI01FECCONCERT, CI01SITCITA, CI01FECADD, CI01FECUPD, CI01OBSERV,"
        SQL = SQL & " DECODE(CI0100.SG02COD_ADD,'?????',CI0100.SG02COD_ADD,SG02ADD.SG02APE1||' '||SG02ADD.SG02APE2||', '||SG02ADD.SG02NOM) USUCI01,"
        SQL = SQL & " DECODE(CI0100.SG02COD_UPD,'?????',CI0100.SG02COD_UPD,SG02UPD.SG02APE1||' '||SG02UPD.SG02APE2||', '||SG02UPD.SG02NOM) USUUPDCI01"
        SQL = SQL & " FROM CI0100,SG0200 SG02ADD,SG0200 SG02UPD"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        SQL = SQL & " AND CI0100.SG02COD_ADD = SG02ADD.SG02COD"
        SQL = SQL & " AND CI0100.SG02COD_UPD = SG02UPD.SG02COD(+)"
        SQL = SQL & " ORDER BY CI01SITCITA, CI01FECADD"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngActPlan
        Set rs1 = qry.OpenResultset()
        Do While Not rs1.EOF
            If rs1!CI01SITCITA = constESTCITA_CITADA Then
                txtFecCita.Text = Format(rs1!CI01FECCONCERT, "dd/mm/yyyy hh:mm")
                txtFecCitacion.Text = Format(rs1!CI01FECADD, "dd/mm/yyyy hh:mm")
                txtUsuCit.Text = rs1!USUCI01
                txtEstadoCita.Text = "Citada"
                If Not IsNull(rs1!CI01OBSERV) Then txtMotivoCita.Text = rs1!CI01OBSERV
            End If
            If rs1!CI01SITCITA = constESTCITA_PENDRECITAR Then
                txtFecCitaPendiente.Text = Format(rs1!CI01FECCONCERT, "dd/mm/yyyy hh:mm")
                txtEstadoCita.Text = "Pendiente de recitar"
                txtPendiente.Text = rs1!USUUPDCI01
                txtFecPendiente.Text = Format(rs1!CI01FECUPD, "dd/mm/yyyy hh:mm")
                If Not IsNull(rs1!CI01OBSERV) Then txtMotivoPendiente.Text = rs1!CI01OBSERV
            End If
            If rs1!CI01SITCITA = constESTCITA_RECITADA Then
                If Not IsNull(rs1!CI01OBSERV) Then strObservRecita = rs1!CI01OBSERV Else strObservRecita = ""
                grdRecitadas.AddItem rs1!USUUPDCI01 & Chr(9) & _
                        Format(rs1!CI01FECCONCERT, "dd/mm/yyyy hh:mm") & Chr(9) & _
                        Format(rs1!CI01FECUPD, "dd/mm/yyyy hh:mm") & Chr(9) & _
                        strObservRecita
            End If
            rs1.MoveNext
        Loop
        rs1.Close
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pCargarCuestionario()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, qry1 As rdoQuery, rs1 As rdoResultset
    Dim strResp$, strPreg$
    Dim lngActPedi&
   
    'se busca el n� de actuaci�n pedida PR03NUMACTPEDI para que luego entre por �ndice _
    en las pr�ximas SELECT
    SQL = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    lngActPedi = rs!PR03NUMACTPEDI

    'preguntas y respuestas
    SQL = "SELECT PR40DESPREGUNTA, PR27CODTIPRESPU, NVL(PR41RESPUESTA,' ') PR41RESPUESTA"
    SQL = SQL & " FROM PR4000, PR4100"
    SQL = SQL & " WHERE PR03NUMACTPEDI =?"
    SQL = SQL & " AND PR4000.PR40CODPREGUNTA = PR4100.PR40CODPREGUNTA"
    SQL = SQL & " ORDER BY PR27CODTIPRESPU"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPedi
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    'If Not rs.EOF Then blnMostrarTab0 = True
    Do While Not rs.EOF
        If rs!PR27CODTIPRESPU = 3 Then
            If rs!PR41RESPUESTA = -1 Then strResp = "S�" Else strResp = "No"
        Else
            strResp = rs!PR41RESPUESTA
        End If
        ssgrdCuest.AddItem rs!PR40DESPREGUNTA & Chr$(9) & strResp
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
           
    'se cargan las restricciones del paciente (tipo movilidad, tipo asistencia...)
    SQL = "SELECT AG16DESTIPREST, AG18CODTIPDATO, NVL(AG16TABLAVALOR,' ') AG16TABLAVALOR,"
    SQL = SQL & " AG16COLCODVALO, AG16COLDESVALO, NVL(VALOR,' ') VALOR"
    SQL = SQL & " FROM PR0301J"
    SQL = SQL & " WHERE PR03NUMACTPEDI =?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPedi
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then ssgrdCuest.Visible = True
    Do While Not rs.EOF
        'pregunta
        strPreg = rs!AG16DESTIPREST
        'respuesta
        If Trim(rs!VALOR) = "" Then
            strResp = ""
        Else
            Select Case rs!AG18CODTIPDATO
            Case 1, 2 'respuesta num�rica o texto
                If Trim(rs!AG16TABLAVALOR) = "" Then
                    strResp = Trim(rs!VALOR)
                Else
                    SQL = "SELECT " & rs!AG16COLDESVALO
                    SQL = SQL & " FROM " & rs!AG16TABLAVALOR
                    SQL = SQL & " WHERE " & rs!AG16COLCODVALO & " = ?"
                    Set qry1 = objApp.rdoConnect.CreateQuery("", SQL)
                    qry1(0) = rs!VALOR
                    Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    strResp = rs1(0)
                    rs1.Close
                    qry1.Close
                End If
            Case 3 'respuesta tipo fecha
                strResp = Format(rs!VALOR, "dd/mm/yyyy")
            End Select
        End If
        
        ssgrdCuest.AddItem strPreg & Chr$(9) & strResp
        
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdCuest.ScrollBars = ssScrollBarsAutomatic
End Sub

Private Sub pCargarCabecera()
'*************************************************************************************
'*  Muestra los datos del paciente al que se realiza la actuaci�n seleccionada
'*************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    SQL = SQL & " CI2200.CI22NUMHISTORIA,PR3700.PR37DESESTADO,"
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE"
    SQL = SQL & " FROM CI2200, PR0100, PR0400,PR3700"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR0400.PR37CODESTADO = PR3700.PR37CODESTADO"

    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not IsNull(rs!CI22NUMHISTORIA) Then
        txtHistoria.Text = rs!CI22NUMHISTORIA
    Else
        txtHistoria.Text = ""
    End If
    txtPaciente.Text = rs!PACIENTE
    'txtNumActPlan.Text = rs!PR04NUMACTPLAN
    txtActuacion.Text = rs!PR01DESCORTA
    txtEstadoActuacion.Text = rs!PR37DESESTADO
    rs.Close
    qry.Close
End Sub



