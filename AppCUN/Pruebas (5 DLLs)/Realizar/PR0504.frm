VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmCitaControles 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Citaci�n por Controles"
   ClientHeight    =   8490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11415
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8490
   ScaleWidth      =   11415
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOverbooking 
      Caption         =   "Over&book."
      Height          =   375
      Left            =   8700
      TabIndex        =   37
      Top             =   8040
      Width           =   1215
   End
   Begin VB.CommandButton cmdAsociar 
      Caption         =   "Asoc. Anestesia"
      Height          =   375
      Left            =   6000
      TabIndex        =   36
      Top             =   8025
      Width           =   1425
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "A&nular"
      Height          =   375
      Left            =   1650
      TabIndex        =   35
      Top             =   8025
      Width           =   1290
   End
   Begin Crystal.CrystalReport crReport1 
      Left            =   10800
      Top             =   60
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   10320
      TabIndex        =   30
      Top             =   1680
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdPetPendientes 
      Caption         =   "&Pet. Pendientes"
      Height          =   375
      Left            =   4500
      TabIndex        =   29
      Top             =   8025
      Width           =   1440
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Ci&tar"
      Height          =   375
      Left            =   10020
      TabIndex        =   28
      Top             =   8040
      Width           =   1275
   End
   Begin VB.CommandButton cmdMantCitasPac 
      Caption         =   "&Citas del paciente"
      Height          =   375
      Left            =   3000
      TabIndex        =   27
      Top             =   8025
      Width           =   1440
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "&Datos Actuaci�n"
      Height          =   375
      Left            =   120
      TabIndex        =   26
      Top             =   8040
      Width           =   1440
   End
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   10320
      Top             =   60
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   "|"
   End
   Begin VB.Frame Frame5 
      Caption         =   "Consulta"
      ForeColor       =   &H00C00000&
      Height          =   2475
      Left            =   60
      TabIndex        =   16
      Top             =   60
      Width           =   5895
      Begin VB.CheckBox chkAnteriores 
         Caption         =   "Ver act. atrasadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   300
         TabIndex        =   32
         Top             =   2100
         Width           =   2355
      End
      Begin VB.Frame Frame3 
         Caption         =   "Tipo Asistencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   1695
         Left            =   2940
         TabIndex        =   22
         Top             =   660
         Width           =   1755
         Begin VB.OptionButton optTipoAsist 
            Caption         =   "Hospitalizados"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   25
            Top             =   780
            Width           =   1455
         End
         Begin VB.OptionButton optTipoAsist 
            Caption         =   "Todos"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   24
            Top             =   360
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.OptionButton optTipoAsist 
            Caption         =   "Ambulatorios"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   23
            Top             =   1200
            Width           =   1455
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Fechas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1395
         Left            =   120
         TabIndex        =   17
         Top             =   660
         Width           =   2715
         Begin VB.CheckBox chkFecha 
            Caption         =   "Incluir act. sin fecha planif."
            Height          =   255
            Left            =   180
            TabIndex        =   31
            Top             =   1080
            Value           =   1  'Checked
            Width           =   2295
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
            Height          =   315
            Left            =   900
            TabIndex        =   18
            Top             =   240
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2990
            _ExtentY        =   556
            _StockProps     =   93
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
            Height          =   315
            Left            =   900
            TabIndex        =   19
            Top             =   720
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2990
            _ExtentY        =   556
            _StockProps     =   93
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            StartofWeek     =   2
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Desde:"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   21
            Top             =   300
            Width           =   675
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Hasta:"
            Height          =   255
            Index           =   1
            Left            =   180
            TabIndex        =   20
            Top             =   780
            Width           =   615
         End
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   375
         Left            =   4800
         TabIndex        =   0
         Top             =   1980
         Width           =   975
      End
      Begin SSDataWidgets_B.SSDBCombo cboTipoRec 
         Height          =   315
         Left            =   1560
         TabIndex        =   33
         Top             =   240
         Width           =   3075
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4974
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5424
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo recurso:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   34
         Top             =   300
         Width           =   1335
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   2475
      Left            =   6060
      TabIndex        =   9
      Top             =   60
      Width           =   4155
      Begin VB.CommandButton cmdB 
         Caption         =   "&Anterior"
         Height          =   375
         Index           =   1
         Left            =   3060
         TabIndex        =   15
         Top             =   1740
         Width           =   975
      End
      Begin VB.CommandButton cmdB 
         Caption         =   "Si&guiente"
         Height          =   375
         Index           =   0
         Left            =   3060
         TabIndex        =   10
         Top             =   1260
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   3
         Left            =   900
         TabIndex        =   4
         Top             =   1860
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   2
         Left            =   900
         TabIndex        =   3
         Top             =   1380
         Width           =   1995
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   3060
         TabIndex        =   5
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   900
         TabIndex        =   2
         Top             =   900
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   0
         Left            =   900
         TabIndex        =   1
         Top             =   420
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Top             =   1920
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   1440
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   12
         Top             =   960
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   11
         Top             =   480
         Width           =   525
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10320
      TabIndex        =   8
      Top             =   2160
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Actuaciones planificadas"
      ForeColor       =   &H00C00000&
      Height          =   5355
      Left            =   60
      TabIndex        =   6
      Top             =   2580
      Width           =   11265
      Begin SSDataWidgets_B.SSDBGrid grdActuaciones 
         Height          =   5025
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   11025
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   19447
         _ExtentY        =   8864
         _StockProps     =   79
      End
   End
End
Attribute VB_Name = "frmCitaControles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strFecDesde$, strFecHasta$
Dim lngTipoAsist&
Dim lngDptoSel&, lngTipoRec&
'variable utilizadas para b�squedas
Dim intBuscar%, blnBuscarOtro As Boolean
Dim cllBuscar As New Collection 'historia|nombre|apel1|apel2|numActPlan|n� persona

Private Sub cboTipoRec_Click()
    lngTipoRec = cboTipoRec.Columns(0).Value
End Sub

Private Sub cmdanular_Click()
    Call pAnularAct
End Sub

Private Sub cmdasociar_Click()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim vntdata(1 To 4) As Variant
    Dim msg$

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ning�n Paciente."
    Case 1
        'Acceso para asociar anestesia
        SQL = "SELECT PR09NUMPETICION, PR0400.PR03NUMACTPEDI"
        SQL = SQL & " FROM PR0800, PR0400"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        SQL = SQL & " AND PR0800.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(0))
        Set rs = qry.OpenResultset()
        vntdata(1) = rs!PR09NUMPETICION
        vntdata(2) = grdActuaciones.Columns("Cod. Persona").CellText(grdActuaciones.SelBookmarks(0))
        vntdata(3) = rs!PR03NUMACTPEDI
        vntdata(4) = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(0))
        rs.Close
        Call objsecurity.LaunchProcess("pr0600", vntdata)
    Case Else
        msg = "Debe Ud. seleccionar una �nica Actuaci�n."
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdB_Click(Index As Integer)
    If Index = 0 Then intBuscar = intBuscar + 1 Else intBuscar = intBuscar - 1
    If intBuscar < 0 Then intBuscar = 0
    blnBuscarOtro = True
    cmdBuscar_Click
End Sub

Private Sub cmdBuscar_Click()
    Dim intN%, i%, blnE As Boolean
    Dim msg$, n%
    
    If fBlnBuscar Then
        cmdB(0).Enabled = True: cmdB(1).Enabled = True
        If blnBuscarOtro = True Then
            blnBuscarOtro = False
            If intBuscar = 0 Then intBuscar = 1: Exit Sub
        Else
            intBuscar = 1
        End If

label:
        For i = 1 To cllBuscar.Count
            blnE = False
            awk1 = cllBuscar(i)
            If UCase(Left$(awk1.F(2), Len(txtBuscar(1).Text))) = UCase(txtBuscar(1).Text) _
            And UCase(Left$(awk1.F(3), Len(txtBuscar(2).Text))) = UCase(txtBuscar(2).Text) _
            And UCase(Left$(awk1.F(4), Len(txtBuscar(3).Text))) = UCase(txtBuscar(3).Text) Then
                If txtBuscar(0).Text = "" Then
                    blnE = True
                Else
                    If awk1.F(1) = txtBuscar(0).Text Then blnE = True
                End If
            End If
            If blnE Then
                intN = intN + 1
                If intN = intBuscar Then
                    grdActuaciones.SelBookmarks.RemoveAll
                    LockWindowUpdate Me.hWnd
                    grdActuaciones.MoveFirst
                    grdActuaciones.MoveRecords i - 1
                    LockWindowUpdate 0&
                    grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
                    cmdB(0).SetFocus
                    Exit Sub
                End If
            End If
        Next i
        
        If intBuscar = 1 Then
            If n > 0 Then
                msg = "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda."
                MsgBox msg, vbInformation, Me.Caption
                Exit Sub
            Else
                msg = "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda."
                msg = msg & Chr$(13) & Chr$(13)
                msg = msg & "Se va a proceder a actualizar los datos de la pantalla y volver a intentar la b�squeda."
                If MsgBox(msg, vbInformation + vbYesNo, Me.Caption) = vbYes Then
                    n = 1
                    Call pActualizar(-1)
                    GoTo label
                End If
            End If
        Else
            intBuscar = intBuscar - 1
            Exit Sub
        End If
    Else
        blnBuscarOtro = False
    End If
End Sub

Private Sub cmdCitar_Click()
    Dim msg$

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0: msg = "No se ha seleccionado ninguna Actuaci�n."
    Case Else: Call pCitar
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdConsultar_Click()
    Call pActualizar(-1)
End Sub

Private Sub cmdDatosAct_Click()
    Dim msg$, lngNumActPlan&

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Actuaci�n."
    Case 1
        lngNumActPlan = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(0))
        'Acceso a los Datos de la Actuaci�n
        Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
        Call objsecurity.LaunchProcess("PR0502")
    Case Else
        msg = "Debe Ud. seleccionar una �nica Actuaci�n."
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdImprimir_Click()
''    Dim msg$, strW$, strFD$, strFH$
''
''    If grdActuaciones.Rows = 0 Then Exit Sub
''
''    crReport1.ReportFileName = objApp.strReportsPath & "PR0506.rpt"
''
''    strW = "{PR0462J.AD02CODDPTO} = " & constDPTO_ANESTESIA
''    strW = strW & " AND {PR0462J.PR37CODESTADO} = " & constESTACT_PLANIFICADA
''    If chkFecha.Value = 0 Then
''        strFD = strFecDesde
''        strFH = DateAdd("d", 1, strFecHasta)
''        strW = strW & " AND {PR0462J.PR04FECPLANIFIC} >= DATE(" & Year(strFD) & "," & Month(strFD) & "," & Day(strFD) & ")"
''        strW = strW & " AND {PR0462J.PR04FECPLANIFIC} < DATE(" & Year(strFH) & "," & Month(strFH) & "," & Day(strFH) & ")"
''    Else
''        strW = strW & " AND ISNULL({PR0462J.PR04FECPLANIFIC})"
''    End If
''    Select Case lngTipoAsist
''    Case constASIST_HOSP: strW = strW & " AND NOT ISNULL({PR0462J.AD15CODCAMA})"
''    Case constASIST_AMBUL: strW = strW & " AND ISNULL({PR0462J.AD15CODCAMA})"
''    End Select
''
''    With crReport1
''        .PrinterCopies = 1
''        .Destination = crptToPrinter
''        .SelectionFormula = "(" & strW & ")"
''        .Connect = objApp.rdoConnect.Connect
''        .DiscardSavedData = True
''        Screen.MousePointer = vbHourglass
''        .Action = 1
''        Screen.MousePointer = vbDefault
''    End With
End Sub

Private Sub cmdMantCitasPac_Click()
    Dim msg$, vntDatos As Variant

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ning�n Paciente."
    Case Else 'vale cualquier actuaci�n del paciente
        'Acceso al Mantenimiento de Citas del Paciente
        vntDatos = grdActuaciones.Columns("Cod. Persona").CellText(grdActuaciones.SelBookmarks(0))
        Call objsecurity.LaunchProcess("CI0202", vntDatos)
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdOverbooking_Click()
Dim strNAPlan$, i As Integer
    
  'se mira si hay alguna actuaci�n seleccionada
  If grdActuaciones.SelBookmarks.Count = 0 Then
    MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
    Exit Sub
  End If

    'se pasa a citar o recitar las actuaciones
  For i = 0 To grdActuaciones.SelBookmarks.Count - 1
    strNAPlan = strNAPlan & grdActuaciones.Columns("Num. Act.").CellValue(grdActuaciones.SelBookmarks(i)) & ","
  Next i
  Call pCitarOverbooking(strNAPlan)
  Call pActualizar(-1)

End Sub

Private Sub cmdPetPendientes_Click()
    Dim vntDatos(1 To 1) As Variant
    
    'Acceso a la Realizaci�n de Actuaciones
    vntDatos(1) = lngDptoSel
    Call objsecurity.LaunchProcess("PR1137", vntDatos)
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub dcboDesde_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboHasta.Date = dcboDesde.Date
    End If
End Sub

Private Sub dcboHasta_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboDesde.Date = dcboHasta.Date
    End If
End Sub

Private Sub Form_Load()
    Dim strHoy$
  
    Call Seleccionar_Dpto
    lngDptoSel = glngdptologin
    
    Call pFormatearGrids
    Call pCargarTiposRec
    
    strHoy = strFecha_Sistema
    dcboDesde.Date = strHoy
    'dcboHasta.Date = DateAdd("ww", 1, strHoy)
    dcboHasta.Date = dcboDesde.Date
    dcboDesde.BackColor = objApp.objUserColor.lngMandatory
    dcboHasta.BackColor = objApp.objUserColor.lngMandatory
    cboTipoRec.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub pActualizar(intCol%)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strFecPlan$, strHoraPlan$, strCama$
    Dim strApel1$, strApel2$
    Dim str1$, str2$, str3$
    Dim n%
    Static sqlOrder$
        
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PACIENTE, PR04FECPLANIFIC, PR01DESCORTA" 'Opci�n por defecto: Paciente
    Else
        Select Case UCase(grdActuaciones.Columns(intCol).Caption)
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, PR04FECPLANIFIC, PR01DESCORTA"
        Case UCase("Fecha"), UCase("Hora"): sqlOrder = " ORDER BY PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, PACIENTE, PR04FECPLANIFIC"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, PACIENTE, PR04FECPLANIFIC"
        Case UCase("Cama"): sqlOrder = " ORDER BY AD1500.AD15CODCAMA, PACIENTE, PR04FECPLANIFIC, PR01DESCORTA"
        Case UCase("Cita"): sqlOrder = " ORDER BY PR03INDCITABLE DESC, PACIENTE, PR04FECPLANIFIC, PR01DESCORTA"
        Case UCase("Urg."): sqlOrder = " ORDER BY PR48CODURGENCIA, PACIENTE, PR04FECPLANIFIC, PR01DESCORTA"
        Case Else: Exit Sub
        End Select
    End If
    
    'Fechas
    strFecDesde = dcboDesde.Date
    strFecHasta = dcboHasta.Date
    'Tipo Asistencia
    If optTipoAsist(1).Value = True Then 'Hospitalizados
        lngTipoAsist = constASIST_HOSP
    ElseIf optTipoAsist(2).Value = True Then 'Ambulatorios
        lngTipoAsist = constASIST_AMBUL
    Else 'Todos
        lngTipoAsist = 0
    End If

    'vaciado previo del grid y de la colecci�n de b�sqeda
    grdActuaciones.RemoveAll
    Do While cllBuscar.Count > 0: cllBuscar.Remove 1: Loop
    
    'se cambia el scrollbar para evitar el efecto de llenado del grid dejando la �ltima _
    fila vacia durante un cierto rato
    grdActuaciones.ScrollBars = ssScrollBarsNone

    SQL = "SELECT /*+ ORDERED INDEX (PR0400 PR0408) */" 'estado-dpto
    SQL = SQL & " PR0400.PR04NUMACTPLAN, PR04FECPLANIFIC,"
    SQL = SQL & " PR0400.PR01CODACTUACION, PR01DESCORTA,"
    SQL = SQL & " DECODE(PR0400.PR48CODURGENCIA,1,'SI') PR48CODURGENCIA,"
    SQL = SQL & " PR0400.CI21CODPERSONA, CI2200.CI22NUMHISTORIA,"
    SQL = SQL & " CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
    SQL = SQL & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
    SQL = SQL & " DECODE(PR03INDCITABLE,-1,'SI',0,'NO','NO') PR03INDCITABLE,"
    SQL = SQL & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    SQL = SQL & " PR59DESMOVPAC, PR60DESNTRAPAC, PR04DESINDSANIT, AG11DESRECURSO"
    SQL = SQL & " FROM PR0400, PR0100, CI2200, PR0300, PR1400, AG1100, AD1500, PR5900, PR6000"
    SQL = SQL & " WHERE PR0400.AD02CODDPTO = ?"
    SQL = SQL & " AND PR0400.PR37CODESTADO = " & constESTACT_PLANIFICADA
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_INTERVENCION
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND AD1500.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    SQL = SQL & " AND AD1500.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    SQL = SQL & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    SQL = SQL & " AND PR5900.PR59CODMOVPAC (+)= PR0400.PR59CODMOVPAC"
    SQL = SQL & " AND PR6000.PR60CODNTRAPAC (+)= PR0400.PR60CODNTRAPAC"
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR1400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR1400.PR14INDRECPREFE = -1"
    SQL = SQL & " AND PR1400.AG11CODRECURSO = AG1100.AG11CODRECURSO(+)"
    If chkAnteriores = 0 Then
        If chkFecha.Value = 0 Then 'NO incluir act. sin fecha de planificaci�n
            SQL = SQL & " AND PR04FECPLANIFIC >= TO_DATE(?,'DD/MM/YYYY')"
            SQL = SQL & " AND PR04FECPLANIFIC < TO_DATE(?,'DD/MM/YYYY')"
        Else 'Incluir act. sin fecha de planificaci�n
            SQL = SQL & " AND ((PR04FECPLANIFIC >= TO_DATE(?,'DD/MM/YYYY')"
            SQL = SQL & " AND PR04FECPLANIFIC < TO_DATE(?,'DD/MM/YYYY'))"
            SQL = SQL & " OR PR04FECPLANIFIC IS NULL)"
        End If
    Else 'Ver actuaciones atrasadas
        SQL = SQL & " AND PR04FECPLANIFIC < TRUNC(SYSDATE)"
    End If
    If lngTipoRec > 0 Then
        SQL = SQL & " AND PR0400.PR01CODACTUACION IN"
        SQL = SQL & " (SELECT PR01CODACTUACION"
        SQL = SQL & " FROM PR1300"
        SQL = SQL & " WHERE AG14CODTIPRECU = ?)"
    End If
    Select Case lngTipoAsist
    Case constASIST_HOSP: SQL = SQL & " AND AD1500.AD15CODCAMA IS NOT NULL"
    Case constASIST_AMBUL: SQL = SQL & " AND AD1500.AD15CODCAMA IS NULL"
    End Select
    SQL = SQL & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngDptoSel
    If chkAnteriores = 0 Then
        qry(1) = Format(strFecDesde, "dd/mm/yyyy")
        qry(2) = Format(DateAdd("d", 1, strFecHasta), "dd/mm/yyyy")
        n = 3
    Else
        n = 1
    End If
    If lngTipoRec > 0 Then qry(n) = lngTipoRec 'n: n� de par�metro del query
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        If IsNull(rs!PR04FECPLANIFIC) Then
            strFecPlan = ""
            strHoraPlan = ""
        Else
            strFecPlan = Format(rs!PR04FECPLANIFIC, "dd/mm/yyyy")
            strHoraPlan = Format(rs!PR04FECPLANIFIC, "hh:mm")
        End If
        If IsNull(rs!Cama) Then strCama = "" Else strCama = rs!Cama
        If Not IsNull(rs!PR59DESMOVPAC) Then str1 = rs!PR59DESMOVPAC & ". " Else str1 = ""
        If Not IsNull(rs!PR60DESNTRAPAC) Then str2 = rs!PR60DESNTRAPAC & ". " Else str2 = ""
        If Not IsNull(rs!PR04DESINDSANIT) Then str3 = rs!PR04DESINDSANIT & ". " Else str3 = ""
        grdActuaciones.AddItem rs!PR03INDCITABLE & Chr$(9) _
                            & rs!PR48CODURGENCIA & Chr$(9) _
                            & rs!PR01DESCORTA & Chr$(9) _
                            & rs!AG11DESRECURSO & Chr$(9) _
                            & strFecPlan & Chr$(9) _
                            & strHoraPlan & Chr$(9) _
                            & rs!PACIENTE & Chr$(9) _
                            & rs!CI22NUMHISTORIA & Chr$(9) _
                            & strCama & Chr$(9) _
                            & rs!PR04NUMACTPLAN & Chr$(9) _
                            & str1 & str2 & str3 & Chr$(9) _
                            & rs!CI21CODPERSONA & Chr$(9) _
                            & rs!PR01CODACTUACION

        If IsNull(rs!CI22PRIAPEL) Then strApel1 = "" Else strApel1 = rs!CI22PRIAPEL
        If IsNull(rs!CI22SEGAPEL) Then strApel2 = "" Else strApel2 = rs!CI22SEGAPEL
        cllBuscar.Add rs!CI22NUMHISTORIA & "|" & rs!CI22NOMBRE & "|" & strApel1 & "|" & strApel2 & "|" & rs!PR04NUMACTPLAN & "|" & rs!CI21CODPERSONA
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'se pone el scrollbar en su estado normal
    grdActuaciones.ScrollBars = ssScrollBarsAutomatic

    Screen.MousePointer = vbDefault
End Sub

Private Sub pFormatearGrids()
    Dim i%
    With grdActuaciones
        .Columns(0).Caption = "Cita"
        .Columns(0).Width = 405
        .Columns(1).Caption = "Urg."
        .Columns(1).Width = 500
        .Columns(2).Caption = "Actuaci�n"
        .Columns(2).Width = 2400
        .Columns(3).Caption = "Rec. Solicitado"
        .Columns(3).Width = 1455
        .Columns(4).Caption = "Fecha"
        .Columns(4).Width = 1000
        .Columns(5).Caption = "Hora"
        .Columns(5).Width = 600
        .Columns(6).Caption = "Paciente"
        .Columns(6).Width = 2790
        .Columns(7).Caption = "N� Hist."
        .Columns(7).Alignment = ssCaptionAlignmentRight
        .Columns(7).Width = 700
        .Columns(8).Caption = "Cama"
        .Columns(8).Width = 525
        .Columns(9).Caption = "Num. Act."
        .Columns(9).Width = 0
        .Columns(9).Visible = False
        .Columns(10).Caption = "Indicaciones"
        .Columns(10).Width = 6000
        .Columns(11).Caption = "Cod. Persona"
        .Columns(11).Visible = False
        .Columns(12).Caption = "Cod. Act."
        .Columns(12).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub grdActuaciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub grdActuaciones_Click()
    If grdActuaciones.SelBookmarks.Count > 1 Then
        If grdActuaciones.Columns("N� Hist.").CellText(grdActuaciones.SelBookmarks(0)) <> grdActuaciones.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, Me.Caption
            grdActuaciones.SelBookmarks.Remove grdActuaciones.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub grdActuaciones_HeadClick(ByVal ColIndex As Integer)
    If grdActuaciones.Rows > 0 Then Call pActualizar(ColIndex)
End Sub

Private Sub txtBuscar_Change(Index As Integer)
    cmdB(0).Enabled = False: cmdB(1).Enabled = False
End Sub

Private Sub txtBuscar_GotFocus(Index As Integer)
    txtBuscar(Index).SelStart = 0: txtBuscar(Index).SelLength = Len(txtBuscar(Index).Text)
End Sub

Private Sub txtBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Function fBlnBuscar() As Boolean
    Dim i%
    For i = 0 To txtBuscar.Count - 1
        If txtBuscar(i).Text <> "" Then fBlnBuscar = True: Exit Function
    Next i
End Function

Private Function fBlnCitaPorHuecos(strCodAct$) As Boolean
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT PR01CODACTUACION"
    SQL = SQL & " FROM PR6600"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    SQL = SQL & " AND AD02CODDPTO = ?"
    SQL = SQL & " AND (PR66INDCITAEXTERNA = -1 OR (PR66INDCITAEXTERNA = 0 AND AD02CODDPTO IN "
    SQL = SQL & " (SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD = ?)))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodAct
    qry(1) = lngDptoSel
    qry(2) = objsecurity.strUser
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then fBlnCitaPorHuecos = True
    rs.Close
    qry.Close
End Function

Private Sub pCargarTiposRec()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT AG14CODTIPRECU, AG14DESTIPRECU"
    SQL = SQL & " FROM AG1400"
    SQL = SQL & " WHERE AG14CODTIPRECU IN"
    SQL = SQL & " (SELECT AG14CODTIPRECU"
    SQL = SQL & " FROM AG1100"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND AG11FECINIVREC <= SYSDATE"
    SQL = SQL & " AND (AG11FECFINVREC IS NULL OR AG11FECFINVREC > SYSDATE)"
    SQL = SQL & " )"
    SQL = SQL & " AND AG14FECINVGTR <= SYSDATE"
    SQL = SQL & " AND (AG14FECFIVGTR IS NULL OR AG14FECFIVGTR > SYSDATE)"
    SQL = SQL & " ORDER BY AG14DESTIPRECU"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngDptoSel
    Set rs = qry.OpenResultset()
    cboTipoRec.AddItem "0" & Chr$(9) & "TODOS"
    cboTipoRec.Text = "TODOS"
    lngTipoRec = 0
    Do While Not rs.EOF
        cboTipoRec.AddItem rs(0) & Chr$(9) & rs(1)
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pCitar()
    Dim strCodPersona$
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim i%, n%
    Dim nCitas%, blnYaCitada As Boolean
    Dim msg$, msg1$
    
    'Query para comprobar si la actuaci�n ha sido citada
    SQL = "SELECT count(CI31NUMSOLICIT) FROM CI0100"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI01SITCITA = '" & constESTCITA_CITADA & "'"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    
    'C�digo de persona
    strCodPersona = grdActuaciones.Columns("Cod. Persona").CellText(grdActuaciones.SelBookmarks(0))
    
    'Se procesan todas las actuaciones del paciente que se encuentran en el grid
    n = cllBuscar.Count: i = 1
    Do While i <= n
        awk1 = cllBuscar(i)
        If awk1.F(6) = strCodPersona Then
            grdActuaciones.SelBookmarks.RemoveAll
            LockWindowUpdate Me.hWnd
            grdActuaciones.MoveFirst
            grdActuaciones.MoveRecords i - 1
            LockWindowUpdate 0&
            grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
            If grdActuaciones.Columns("Cita").Text = "SI" Then
                'se comprueba que la actuaci�n no ha sido citada desde otro ordenador _
                por si todav�a no se ha refrescado la pantalla
                qry(0) = grdActuaciones.Columns("Num. Act.").Value
                Set rs = qry.OpenResultset()
                If rs(0) = 0 Then 'NO ha sido citada todav�a
                    If fBlnCitaPorHuecos(grdActuaciones.Columns("Cod. Act.").Value) Then
                        Call objsecurity.LaunchProcess("AG0211", grdActuaciones.Columns("Num. Act.").Value)
                    Else
                        Call objsecurity.LaunchProcess("CI1150", grdActuaciones.Columns("Num. Act.").Value)
                    End If
                    qry(0) = grdActuaciones.Columns("Num. Act.").Value
                    Set rs = qry.OpenResultset()
                    If rs(0) > 0 Then 'Ha sido citada con �xito
                        nCitas = nCitas + 1
                        cllBuscar.Remove i
                        n = cllBuscar.Count
                        grdActuaciones.DeleteSelected
                        i = i - 1
                    End If
                    rs.Close
                Else
                    blnYaCitada = True
                End If
            End If
        End If
        i = i + 1
    Loop
    qry.Close
    
    Select Case nCitas
    Case 0: msg = "No se ha citado ninguna actuaci�n."
'    Case 1: msg = "Se ha realizado la cita de 1 actuaci�n."
'    Case Else: msg = "Se ha realizado la cita de " & nCitas & " actuaciones."
    End Select
    If blnYaCitada Then
        msg1 = Chr$(13) & Chr$(13)
        msg1 = msg1 & "Alguna de las actuaciones del paciente ha sido citada previamente"
        msg1 = msg1 & " desde otro ordenador." & Chr$(13) & Chr$(13)
        msg1 = msg1 & "Se va a proceder a actualizar los datos de la pantalla."
    End If
    If msg & msg1 <> "" Then MsgBox msg & msg1, vbInformation, Me.Caption
    If msg1 <> "" Then
        Call pActualizar(-1)
    End If
End Sub

Private Sub pAnularAct()
'**************************************************************************************
'*  Anula una actuaci�n planificada cambiando su estado a anulada
'**************************************************************************************
    Dim strCancel$, SQL$, qry As rdoQuery, i%, j%, lngNumActPlan&
    
    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        SQL = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox SQL, vbExclamation, Me.Caption
        Exit Sub
    Case 1
        SQL = "Si Ud. anula la actuaci�n ya no estar� disponible para ser realizada." & Chr$(13)
        SQL = SQL & "Indique el motivo por el que se va a anular la actuaci�n:"
    Case Is > 1
        SQL = "Si Ud. anula las actuaciones ya no estar�n disponibles para ser realizadas." & Chr$(13)
        SQL = SQL & "Indique el motivo por el que se van a anular las actuaciones:"
    End Select
    strCancel = Trim$(InputBox(SQL, "Anular Actuaci�n"))
    If strCancel = "" Then
        SQL = "La actuaci�n NO ha sido anulada: hace falta indicar un motivo."
        MsgBox SQL, vbExclamation, Me.Caption
        Exit Sub
    End If
    
    For i = 0 To grdActuaciones.SelBookmarks.Count - 1
        lngNumActPlan = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(i))
        
        SQL = "UPDATE PR0400 SET PR04DESMOTCAN = ?,"
        SQL = SQL & " PR04FECCANCEL = SYSDATE,"
        SQL = SQL & " PR37CODESTADO = " & constESTACT_CANCELADA
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        If Len(strCancel) > 50 Then qry(0) = Left$(strCancel, 50) Else qry(0) = strCancel
        qry(1) = lngNumActPlan
        qry.Execute
        qry.Close
        
        For j = 1 To cllBuscar.Count
            awk1 = cllBuscar(j)
            If awk1.F(5) = lngNumActPlan Then
                cllBuscar.Remove j
                Exit For
            End If
        Next j
        
        Call pAnularAsociadas(lngNumActPlan)
    Next i
    
    'se elimina la actuaci�n del Grid
    grdActuaciones.DeleteSelected
End Sub
