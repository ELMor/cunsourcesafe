VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmAbrirAsist 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Abrir asistencias"
   ClientHeight    =   6660
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9840
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6660
   ScaleWidth      =   9840
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraDatos 
      Caption         =   "Datos del responsable/asistencia"
      ForeColor       =   &H00800000&
      Height          =   4665
      Left            =   150
      TabIndex        =   11
      Top             =   1875
      Width           =   9540
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Enabled         =   0   'False
         Height          =   390
         Left            =   5850
         TabIndex        =   47
         Top             =   1500
         Width           =   2115
         Begin VB.CheckBox chkVolante 
            Alignment       =   1  'Right Justify
            Caption         =   "Pendiente de Volante"
            Enabled         =   0   'False
            Height          =   360
            Left            =   75
            TabIndex        =   48
            Tag             =   "Pendiente de Volante|Pendiente de Volante"
            Top             =   0
            Width           =   1905
         End
      End
      Begin SSDataWidgets_B.SSDBGrid grdDoc 
         Height          =   1440
         Left            =   225
         TabIndex        =   46
         Top             =   3150
         Width           =   9165
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   7
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         CaptionAlignment=   0
         Columns.Count   =   7
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "nDoc"
         Columns(0).Name =   "nDoc"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Tipo"
         Columns(1).Name =   "Tipo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3598
         Columns(2).Caption=   "Observ."
         Columns(2).Name =   "Observ."
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1217
         Columns(3).Caption=   "Volante"
         Columns(3).Name =   "Volante"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2090
         Columns(4).Caption=   "N� volante"
         Columns(4).Name =   "N� volante"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2090
         Columns(5).Caption=   "F. Inicio"
         Columns(5).Name =   "F. Inicio"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   2117
         Columns(6).Caption=   "F. Fin"
         Columns(6).Name =   "F. Fin"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         _ExtentX        =   16166
         _ExtentY        =   2540
         _StockProps     =   79
         Caption         =   "Documentaci�n"
         ForeColor       =   8388608
      End
      Begin VB.TextBox txtFIni 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   7725
         Locked          =   -1  'True
         TabIndex        =   40
         Text            =   "08/06/00 55:44"
         Top             =   375
         Width           =   1590
      End
      Begin VB.TextBox txtObserv 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   1425
         MaxLength       =   100
         TabIndex        =   22
         Tag             =   "Persona Env�o Informe|C�digo de Persona Env�o Informe"
         Top             =   2700
         Width           =   7935
      End
      Begin VB.CommandButton cmdEnvio 
         Caption         =   "..."
         Height          =   330
         Left            =   8970
         TabIndex        =   21
         Top             =   1950
         Width           =   345
      End
      Begin VB.CommandButton cmdMed 
         Caption         =   "..."
         Height          =   330
         Left            =   8970
         TabIndex        =   20
         Top             =   2325
         Width           =   345
      End
      Begin VB.CommandButton cmdResponsable 
         Caption         =   "..."
         Height          =   330
         Left            =   5400
         TabIndex        =   19
         Top             =   1575
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CommandButton cmdEntidad 
         Caption         =   "..."
         Height          =   330
         Left            =   3750
         TabIndex        =   18
         Top             =   750
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.TextBox txtNC 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   8700
         Locked          =   -1  'True
         MaxLength       =   4
         TabIndex        =   17
         Tag             =   "N�mero de Caso|N�mero de Caso en el Host"
         Top             =   1125
         Width           =   615
      End
      Begin VB.TextBox txtEnvio 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   2250
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Tag             =   "Nombre Persona Env�o Informe|Nombre de la Persona Env�o Informe"
         Top             =   1950
         Width           =   6690
      End
      Begin VB.TextBox txtMed 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   2250
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Tag             =   "Nombre M�dico Remitente|Nombre del M�dico Remitente"
         Top             =   2325
         Width           =   6690
      End
      Begin VB.CheckBox chkReqIM 
         Alignment       =   1  'Right Justify
         Caption         =   "Contestar en Informe M�dico"
         Height          =   360
         Left            =   5475
         TabIndex        =   14
         Tag             =   "Pendiente de Volante|Pendiente de Volante"
         Top             =   1125
         Width           =   2355
      End
      Begin VB.CommandButton cmdDocumentacion 
         Caption         =   "Documentaci�n"
         Height          =   390
         Left            =   8025
         TabIndex        =   13
         Top             =   1500
         Visible         =   0   'False
         Width           =   1290
      End
      Begin VB.CommandButton cmdHistorico 
         Caption         =   "&Hist�rico"
         Height          =   390
         Left            =   4200
         TabIndex        =   12
         Top             =   750
         Visible         =   0   'False
         Width           =   1140
      End
      Begin SSDataWidgets_B.SSDBCombo cboTiEco 
         Height          =   330
         Left            =   1350
         TabIndex        =   23
         Tag             =   "C�digo Tipo Econ�mico|C�digo Tipo Econ�mico"
         Top             =   750
         Width           =   2385
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5609
         Columns(1).Caption=   "Descripci�n "
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4207
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboEntidad 
         Height          =   330
         Left            =   1350
         TabIndex        =   24
         Tag             =   "C�digo Entidad|C�digo Entidad"
         Top             =   1125
         Width           =   3990
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1032
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4471
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7038
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboResp 
         Height          =   330
         Left            =   1350
         TabIndex        =   25
         Tag             =   "C�digo Entidad|C�digo Entidad"
         Top             =   1500
         Width           =   3990
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5318
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7038
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboTiPac 
         Height          =   330
         Left            =   6675
         TabIndex        =   26
         Tag             =   "Tipo de Paciente|Tipo de Paciente"
         Top             =   750
         Width           =   2625
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4180
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4630
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboDpt 
         Height          =   315
         Left            =   1350
         TabIndex        =   36
         Top             =   375
         Width           =   2340
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cDpt"
         Columns(0).Name =   "cDpt"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4154
         Columns(1).Caption=   "Dpt"
         Columns(1).Name =   "Dpt"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4128
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboDrResp 
         Height          =   315
         Left            =   4725
         TabIndex        =   38
         Top             =   375
         Width           =   2190
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cDr"
         Columns(0).Name =   "cDr"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4366
         Columns(1).Caption=   "Dr"
         Columns(1).Name =   "Dr"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3863
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label2 
         Caption         =   "F. Inicio:"
         Height          =   240
         Index           =   6
         Left            =   7050
         TabIndex        =   39
         Top             =   450
         Width           =   690
      End
      Begin VB.Label Label2 
         Caption         =   "Dr. resp:"
         Height          =   240
         Index           =   5
         Left            =   3900
         TabIndex        =   37
         Top             =   450
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Dpto. resp.:"
         Height          =   240
         Index           =   4
         Left            =   300
         TabIndex        =   35
         Top             =   450
         Width           =   915
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones:"
         Height          =   195
         Index           =   10
         Left            =   300
         TabIndex        =   34
         Top             =   2775
         Width           =   1110
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Responsable:"
         Height          =   195
         Index           =   3
         Left            =   300
         TabIndex        =   33
         Top             =   1575
         Width           =   975
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "T. Econ�mico:"
         Height          =   195
         Index           =   1
         Left            =   225
         TabIndex        =   32
         Top             =   825
         Width           =   1035
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Entidad:"
         Height          =   195
         Index           =   14
         Left            =   675
         TabIndex        =   31
         Top             =   1200
         Width           =   585
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N� Caso:"
         Height          =   195
         Index           =   12
         Left            =   7950
         TabIndex        =   30
         Top             =   1200
         Width           =   705
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Paciente:"
         Height          =   195
         Index           =   18
         Left            =   5550
         TabIndex        =   29
         Top             =   825
         Width           =   1035
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Persona env�o informe:"
         Height          =   195
         Index           =   19
         Left            =   450
         TabIndex        =   28
         Top             =   2025
         Width           =   1650
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "M�dico remitente:"
         Height          =   195
         Index           =   17
         Left            =   825
         TabIndex        =   27
         Top             =   2400
         Width           =   1260
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   390
      Left            =   8625
      TabIndex        =   45
      Top             =   1425
      Width           =   1065
   End
   Begin VB.CommandButton cmdAbrir 
      Caption         =   "&Abrir"
      Height          =   390
      Left            =   8625
      TabIndex        =   0
      Top             =   150
      Width           =   1065
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos de la asistencia anterior"
      ForeColor       =   &H00800000&
      Height          =   1740
      Left            =   150
      TabIndex        =   1
      Top             =   75
      Width           =   8265
      Begin VB.TextBox txtDrResp 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   5175
         Locked          =   -1  'True
         TabIndex        =   44
         Top             =   1275
         Width           =   2865
      End
      Begin VB.TextBox txtDptResp 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1125
         Locked          =   -1  'True
         TabIndex        =   43
         Top             =   1275
         Width           =   3090
      End
      Begin VB.TextBox txtTiAsist 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   5175
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   825
         Width           =   2865
      End
      Begin VB.TextBox txtFFinUltimaAsist 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   2850
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   825
         Width           =   1365
      End
      Begin VB.TextBox txtPac 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1725
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   300
         Width           =   6315
      End
      Begin VB.TextBox txtNH 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   825
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   300
         Width           =   840
      End
      Begin VB.TextBox txtFIniUltimaAsist 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   825
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   825
         Width           =   1365
      End
      Begin VB.Label Label2 
         Caption         =   "Dr. resp:"
         Height          =   240
         Index           =   8
         Left            =   4425
         TabIndex        =   42
         Top             =   1350
         Width           =   690
      End
      Begin VB.Label Label2 
         Caption         =   "Dpto. resp.:"
         Height          =   240
         Index           =   7
         Left            =   225
         TabIndex        =   41
         Top             =   1275
         Width           =   915
      End
      Begin VB.Label Label2 
         Caption         =   "Tipo asist.:"
         Height          =   240
         Index           =   1
         Left            =   4350
         TabIndex        =   10
         Top             =   900
         Width           =   840
      End
      Begin VB.Label Label2 
         Caption         =   "F. Fin:"
         Height          =   240
         Index           =   0
         Left            =   2325
         TabIndex        =   8
         Top             =   900
         Width           =   540
      End
      Begin VB.Label Label2 
         Caption         =   "Paciente:"
         Height          =   240
         Index           =   2
         Left            =   75
         TabIndex        =   6
         Top             =   375
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "F. Inicio:"
         Height          =   240
         Index           =   3
         Left            =   150
         TabIndex        =   5
         Top             =   900
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmAbrirAsist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public cDptResp As Integer, cDrResp As String, nPers As Long, cTiPac As Integer
Public nProc As Long, cPersEnvio As Long, cMed As Long, nAsist As Long, nAsistPadre As Long
Public seguir As Long ' False si se aprieta cancelar
Public cEntidad As String, cTiEco As String
Dim nPers_RespEco As Long

Private Sub cboDpt_Click()
  cDptResp = cboDpt.Columns(0).Text
  Call LlenarDrResp(cDptResp)
End Sub

Private Sub cboDrResp_Click()
  cDrResp = cboDrResp.Columns(0).Text
End Sub

Private Sub cboEntidad_Click()
  
  cEntidad = cboEntidad.Columns(0).Text
  
  Call BuscarResp(cEntidad)

End Sub

Private Sub cboResp_Click()
  nPers_RespEco = cboResp.Columns(0).Text

End Sub

Private Sub cboTiEco_Click()
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim sql As String

  cboEntidad.RemoveAll
  cboEntidad = ""
  cEntidad = ""
  
  cboResp.RemoveAll
  cboResp.Text = ""
  nPers_RespEco = 0
  
  cTiEco = cboTiEco.Columns(0).Text
  
  'Comprueba los tipos econ�micos
  Select Case cTiEco
    Case "J" ' Acunsa
      If EsAcunsa(Val(txtNH.Text)) = False Then
        MsgBox "Este paciente no pertence a Acunsa. Debe seleccionar otro tipo econ�mico", vbInformation, Me.Caption
        cboTiEco.Text = ""
        Exit Sub
      End If
    
    Case "I" ' Universidad
      If EsUniversidad(Val(txtNH.Text)) = False Then
        MsgBox "Este paciente no pertence a la Universidad. Debe seleccionar otro tipo econ�mico", vbInformation, Me.Caption
        cboTiEco.Text = ""
        Exit Sub
      End If
    
    Case "P" ' Privado. Se pone su responsable econ�mico
      cboResp.RemoveAll
      If nPers_RespEco = 0 Then
        cboResp.Text = txtPac.Text
        nPers_RespEco = nPers
      Else
        cboResp.Text = ResponsableEco(nPers_RespEco)
      End If
  
  End Select
    
  cboEntidad.RemoveAll
' Se buscan las entidades
  Call BuscarEntidad(cTiEco)

End Sub

Private Sub cboTiPac_Click()
  cTiPac = cboTiPac.Columns(0).Text

End Sub

Private Sub cmdAbrir_Click()
Dim texto As String
Dim res As Integer
  
  If chkVolante.Value = vbChecked Then
    res = MsgBox("El paciente est� pendiente de aportar un volante. �Desea continuar y abrir la asistencia?", vbInformation, Me.Caption)
    If res = vbNo Then Exit Sub
  End If
  seguir = True
  
  objApp.BeginTrans
  ' Asistencia
  If InsertAD01 = False Then
    texto = "Error al abrir la asistencia. Error: " & Error
    GoTo Cancelar
  End If
  
  ' Proceso-asistencia
  If InsertAD08 = False Then
    texto = "Error al abrir la asistencia. Error: " & Error
    GoTo Cancelar
  End If

  ' Responsable del proceso-asistencia
  If InsertAD05 = False Then
    texto = "Error al abrir la asistencia. Error: " & Error
    GoTo Cancelar
  End If

  ' Tipo econ�mico y entidad
  If InsertAD11 = False Then
    texto = "Error al abrir la asistencia. Error: " & Error
    GoTo Cancelar
  End If

  ' Tipo de asistencia
  If InsertAD25 = False Then
    texto = "Error al abrir la asistencia. Error: " & Error
    GoTo Cancelar
  End If

  ' Documentaci�n aportada
  If InsertAD43 = False Then
    texto = "Error al abrir la asistencia. Error: " & Error
    GoTo Cancelar
  End If

  objApp.CommitTrans
  Me.Visible = False
  Exit Sub

Cancelar:
  objApp.RollbackTrans
  MsgBox texto, vbExclamation, Me.Caption

End Sub

Private Sub cmdCancelar_Click()
  seguir = False
  Me.Visible = False
End Sub

Private Sub cmdEntidad_Click()
'  frmBuscarTipoEco.Show vbModal
'  With frmBuscarTipoEco
'    If .seguir = True Then
'      cTiEco = .grdRazonSocial.Columns("cTiEco").Text
'      cboTiEco.Text = cTiEco & " - " & .grdRazonSocial.Columns("TiEco").Text
'      Call BuscarEntidad(cTiEco)
'      cEntidad = .grdRazonSocial.Columns("cEntidad").Text
'      cboEntidad.Text = cEntidad & " - " & .grdRazonSocial.Columns("Entidad").Text
'      If cTiEco = "P" Then
'        cboResp.RemoveAll
'        If nPers_RespEco = 0 Then
'          cboResp.Text = txtPac.Text
'        Else
'          cboResp.Text = ResponsableEco(nPers_RespEco)
'        End If
'      Else
'        Call BuscarResp(cEntidad)
'      End If
'    End If
'  End With
'  Unload frmBuscarTipoEco

End Sub

Private Sub cmdEnvio_Click()
  
  Call objsecurity.LaunchProcess("AD2999")
  If objPipe.PipeExist("AD2999_CI21CODPERSONA") = True Then
    cPersEnvio = objPipe.PipeGet("AD2999_CI21CODPERSONA")
    txtEnvio.Text = SacarNombrePac(cPersEnvio)
  End If

End Sub

Private Sub cmdHistorico_Click()
'  If txtPac.Text = "" Then
'    MsgBox "Debe seleccionar alg�n paciente para consultar el hist�rico de tipos econ�micos.", vbInformation, Me.Caption
'    Exit Sub
'  End If
'
'  Load frmHistTiEco
'  frmHistTiEco.txtNH.Text = mskNH.Text
'  frmHistTiEco.txtPac = txtPac.Text
'  If frmHistTiEco.Consultar = True Then
'    frmHistTiEco.Show vbModal
'  End If

End Sub

Private Sub cmdMed_Click()
  Call objsecurity.LaunchProcess("AD2999")
  If objPipe.PipeExist("AD2999_CI21CODPERSONA") = True Then
    cMed = objPipe.PipeGet("AD2999_CI21CODPERSONA")
    txtMed.Text = SacarNombrePac(cMed)
  End If
End Sub

Private Sub cmdResponsable_Click()
Dim nPers As Long
  
  Call objsecurity.LaunchProcess("AD2999")
  If objPipe.PipeExist("AD2999_CI21CODPERSONA") = True Then
    nPers = objPipe.PipeGet("AD2999_CI21CODPERSONA")
    cboResp.Text = SacarNombrePac(nPers)
    cboResp.RemoveAll
  End If

End Sub

Private Sub Form_Load()
  Call CargarDpt
  Call CargarTiEco
  Call CargarTiPac
  txtFIni.Text = strFechaHora_Sistema
End Sub

Sub CargarDpt()
Dim sql As String
Dim rdo As rdoResultset

  sql = "SELECT AD02CodDpto, AD02DesDpto FROM AD0200 WHERE " _
      & "AD02FecFin IS NULL AND AD02IndResponProc = -1 ORDER BY AD02DesDpto"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    cboDpt.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend
End Sub

Sub CargarTiEco()
Dim sql As String
Dim rdo As rdoResultset

  sql = "SELECT CI32CodTipEcon, CI32DesTipEcon FROM CI3200 WHERE " _
      & "CI32FecFiVGTEC IS NULL ORDER BY CI32CodTipEcon"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    cboTiEco.AddItem rdo(0) & Chr$(9) & rdo(0) & " - " & rdo(1)
    rdo.MoveNext
  Wend

End Sub

Sub CargarTiPac()
Dim sql As String
Dim rdo As rdoResultset

  sql = "SELECT AD10CodTipPacien, AD10DesTipPacien FROM AD1000 WHERE " _
      & "AD10FecFin IS NULL ORDER BY AD10CodTipPacien"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    cboTiPac.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend

End Sub


Function EsAcunsa(NH As Long) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
 
' Comprueba si un paciente es de acunsa
 sql = "SELECT CI22NumHistoria FROM CI0300 WHERE " _
     & "CI22NumHistoria = ? AND " _
     & "(CI03FecFinPoli IS NULL OR CI03FecFinPoli > SYSDATE)"
       
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then EsAcunsa = False Else EsAcunsa = True

End Function

Function EsUniversidad(NH As Long) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
 
' Comprueba si un paciente es de Universidad
  sql = "SELECT CI22NumHistoria FROM CI2400 WHERE " _
      & "CI22NumHistoria =? AND " _
      & "(CI24FecFin IS NULL OR CI24FecFin > SYSDATE)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then EsUniversidad = False Else EsUniversidad = True

End Function


Function ResponsableEco(nPers As Long) As String
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT ADFN01(CI21CodPersona) FROM CI2100 WHERE CI21CodPersona = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPers
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    ResponsableEco = rdo(0)
  Else
    ResponsableEco = ""
  End If
End Function

Sub BuscarEntidad(cTiEco As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer
  
  cboEntidad.RemoveAll
  sql = "SELECT CI13CodEntidad, CI13DesEntidad FROM CI1300 WHERE " _
      & "CI32CodTipEcon = ? AND " _
      & "(CI13FecFiVGent IS NULL OR CI13FecFiVGent > SYSDATE) " _
      & "ORDER BY CI13CodEntidad"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cTiEco
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  i = 0
  While rdo.EOF = False
    i = i + 1
    cboEntidad.AddItem rdo(0) & Chr$(9) & rdo(0) & " - " & rdo(1)
    rdo.MoveNext
  Wend
  If i = 1 Then
    cEntidad = cboEntidad.Columns(0).Text
    cboEntidad.Text = cboEntidad.Columns(1).Text
    Call cboEntidad_Click
  End If
End Sub

Sub BuscarDoc(nProc As Long, nAsist As Long, cTiEco As String, cEntidad As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AD1900.AD19DesTipoDocum, AD1800.AD18DesDocumento, AD3500.AD35DesTipVol, " _
      & "AD1800.AD18NumVolante, AD1800.AD18FecVolante, AD1800.AD18FecFinVolante, " _
      & "AD1800.AD18CodDocumento " _
      & "FROM AD1800, AD1900, AD4300, AD3500 WHERE " _
      & "AD4300.CI21CodPersona = AD1800.CI21CodPersona AND " _
      & "AD4300.AD18CodDocumento = AD1800.AD18CodDocumento AND " _
      & "AD1800.AD35CodTipVol = AD3500.AD35CodTipVol (+) AND " _
      & "AD1800.AD19CodTipoDocum = AD1900.AD19CodTipoDocum AND " _
      & "(AD1800.AD18FecFinVolante IS NULL OR AD1800.AD18FecFinVolante <= SYSDATE) AND " _
      & "AD4300.AD01CodAsistenci = ? AND " _
      & "AD4300.AD07CodProceso = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nAsist
  rdoQ(1) = nProc
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    grdDoc.AddItem rdo(6) & Chr$(9) & rdo(0) & Chr$(9) & rdo(1) & Chr$(9) & rdo(2) & Chr$(9) & rdo(3) & Chr$(9) & rdo(4) & Chr$(9) & rdo(5)
    rdo.MoveNext
  Wend
  sql = "SELECT CI09DOCREQ FROM CI0900 WHERE CI32CodTipEcon = ? AND CI13CodEntidad = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cTiEco
  rdoQ(1) = cEntidad
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If IsNull(rdo(0)) Then
    chkVolante.Value = vbUnchecked
  Else
    If grdDoc.Rows > 0 Then chkVolante.Value = vbUnchecked Else chkVolante.Value = vbChecked
  End If

End Sub

Sub BuscarResp(cEntidad As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer
  
  cboResp.RemoveAll
  If cTiEco <> "" And cTiEco <> "P" Then ' Si el paciente no es privado
    nPers_RespEco = 0
    sql = "SELECT  CI21CodPersona, ADFN01(CI21CodPersona) FROM CI0900 WHERE " _
        & "CI32CodTipEcon = ? AND " _
        & "CI13CodEntidad = ? " _
        & "ORDER BY ADFN01(CI21CodPersona)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = cTiEco
    rdoQ(1) = cEntidad
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    i = 0
    While rdo.EOF = False
      i = i + 1
      cboResp.AddItem rdo(0) & Chr$(9) & rdo(1)
      rdo.MoveNext
    Wend
    cmdResponsable.Enabled = False
    If i = 1 Then
      cboResp.Text = cboResp.Columns(1).Text
      nPers_RespEco = cboResp.Columns(0).Text
    Else
      cboResp.Text = ""
    End If
  End If

End Sub

Function SacarNombrePac(nPers As Long) As String
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre " _
      & "FROM CI2200 WHERE CI21CodPersona = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPers
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    SacarNombrePac = rdo(0)
  End If
End Function


Sub LlenarDrResp(cDptResp As Integer)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT SG0200.SG02Cod, " _
      & "NVL(SG0200.SG02TxtFirma, 'Dr. '||SG0200.SG02Ape1||' '||SG0200.SG02Ape2) " _
      & "FROM AD0300, SG0200 WHERE " _
      & "AD0300.SG02Cod = SG0200.SG02Cod AND " _
      & "AD0300.AD03FecFin IS NULL AND " _
      & "SG0200.AD30CodCategoria = " & constCODDOCTOR & " AND " _
      & "AD0300.AD31CodPuesto IN " & constCATDOCTOR & " AND " _
      & "SG0200.SG02FECDES IS NULL AND " _
      & "AD0300.AD02CodDpto = ? " _
      & "ORDER BY 1"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cDptResp
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    cboDrResp.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend

End Sub

Function InsertAD01() As Integer
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "INSERT INTO AD0100 (CI21CodPersona, AD01CodAsistenci, AD01FecInicio, " _
      & "CI22NumHistoria, AD01CodAsistenci_Pad) VALUES " _
      & "(?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'), ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  nAsist = SigAsist()
  rdoQ(0) = nPers
  rdoQ(1) = nAsist
  rdoQ(2) = txtFIni.Text
  rdoQ(3) = Val(txtNH.Text)
  rdoQ(4) = nAsistPadre
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then InsertAD01 = True Else InsertAD01 = False

End Function
  
Function InsertAD25() As Integer
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "INSERT INTO AD2500 (AD01CodAsistenci, AD25FecInicio, AD12CodTipoAsist) VALUES " _
      & "(?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nAsist
  rdoQ(1) = txtFIni.Text
  rdoQ(2) = constTIASISTEXPLORACION
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then InsertAD25 = True Else InsertAD25 = False

End Function


Function InsertAD43() As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim i As Integer, nDoc As Long
  
  InsertAD43 = True
  If grdDoc.Rows = 0 Then Exit Function
  
  sql = "INSERT INTO AD4300 (CI21CodPersona, AD18CodDocumento, AD01CodAsistenci, " _
      & "AD07CodProceso) VALUES (?, ?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPers
  rdoQ(2) = nAsist
  rdoQ(3) = nProc
  grdDoc.MoveFirst
  For i = 0 To grdDoc.Rows - 1
    rdoQ(1) = grdDoc.Columns("nDoc").Value
    rdoQ.Execute
    If rdoQ.RowsAffected = 0 Then
      InsertAD43 = False
      Exit Function
    End If
    grdDoc.MoveNext
  Next i
End Function


Function InsertAD08() As Integer
Dim sql As String, NHC As String
Dim rdoQ As rdoQuery

  sql = "INSERT INTO AD0800 (AD07CodProceso, AD01CodAsistenci, AD10CodTipPacien, " _
      & "CI21CodPersona_Env, CI21CodPersona_Med, AD08NumCaso, AD08FecInicio, " _
      & "AD08IndReqIM, AD08Observac) VALUES " _
      & "(?, ?, ?, ?, ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'), ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nProc
  rdoQ(1) = nAsist
  rdoQ(2) = cTiPac
  rdoQ(3) = IIf(cPersEnvio = 0, Null, cPersEnvio)
  rdoQ(4) = IIf(cMed = 0, Null, cMed)
  NHC = SacarAD08NumCaso(Val(txtNH.Text), nAsist)
  rdoQ(5) = NHC
  rdoQ(6) = txtFIni.Text
  rdoQ(7) = IIf(chkReqIM.Value = vbChecked, -1, 0)
  rdoQ(8) = Trim$(txtObserv.Text)
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then
    InsertAD08 = True
    txtNC.Text = Val(Right$(NHC, 4))
  Else
    InsertAD08 = False
  End If
End Function

Function InsertAD05() As Integer
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "INSERT INTO AD0500 (AD07CodProceso, AD01CodAsistenci, AD05FecIniRespon, " _
      & "AD02CodDpto, SG02Cod) VALUES " _
      & "(?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'), ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nProc
  rdoQ(1) = nAsist
  rdoQ(2) = txtFIni.Text
  rdoQ(3) = cDptResp
  rdoQ(4) = cDrResp
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then InsertAD05 = True Else InsertAD05 = False

End Function
  
Function InsertAD11() As Integer
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "INSERT INTO AD1100 (AD01CodAsistenci, AD11FecInicio, CI32CodTipEcon, " _
      & "CI21CodPersona, CI13CodEntidad, AD11IndVolante, AD07CodProceso) VALUES " _
      & "(?, TO_DATE(?,'DD/MM/YYYY HH24:MI'), ?, ?, ?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nAsist
  rdoQ(1) = txtFIni.Text
  rdoQ(2) = cTiEco
  rdoQ(3) = IIf(nPers_RespEco = 0, Null, nPers_RespEco)
  rdoQ(4) = cEntidad
  rdoQ(5) = IIf(chkVolante.Value = vbChecked, -1, 0)
  rdoQ(6) = nProc
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then InsertAD11 = True Else InsertAD11 = False

End Function

Function SigAsist()
Dim sql As String
Dim rdo As rdoResultset

  sql = "SELECT AD01CODASISTENCI_S.NEXTVAL FROM DUAL"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  SigAsist = rdo(0)
End Function


Function SacarAD08NumCaso(NH As Long, nAsist As Long) As String
Dim sql As String, numCaso As String, NC As Long
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
        
  numCaso = Format$(NH, "000000") & Mid$(CStr(nAsist), 7, 4)
  NC = Val(Mid$(CStr(nAsist), 7, 4))
  sql = "SELECT AD08NumCaso FROM AD0800 WHERE AD08NumCaso = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = numCaso
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False  ' encontrado una historia/caso igual
    If NC = 9999 Then NC = 0
    NC = NC + 1
    numCaso = Format$(NH, "000000") & Format$(NC, "0000")
    rdoQ(0) = numCaso
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = numCaso
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  Wend
  SacarAD08NumCaso = numCaso

End Function


