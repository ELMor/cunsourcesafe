VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmPeticionesSanitarios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Realizacion de Pruebas. Peticiones a Sanitarios"
   ClientHeight    =   8340
   ClientLeft      =   -45
   ClientTop       =   525
   ClientWidth     =   11910
   HelpContextID   =   2
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin vsViewLib.vsPrinter vp 
      Height          =   375
      Left            =   5400
      TabIndex        =   14
      Top             =   600
      Visible         =   0   'False
      Width           =   255
      _Version        =   196608
      _ExtentX        =   450
      _ExtentY        =   661
      _StockProps     =   229
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
   Begin VB.Timer tmrRefrescar 
      Interval        =   60000
      Left            =   4680
      Top             =   840
   End
   Begin VB.CommandButton cmdEnviar 
      Caption         =   "Enviar Sanitario"
      Height          =   375
      Index           =   0
      Left            =   8880
      TabIndex        =   7
      Top             =   720
      Width           =   1755
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Peticiones a Sanitarios"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6795
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   11715
      Begin TabDlg.SSTab tabTab1 
         Height          =   6360
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   360
         Width           =   11505
         _ExtentX        =   20294
         _ExtentY        =   11218
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   -2147483638
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "&Pendientes"
         TabPicture(0)   =   "PR0275.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraframe1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Realiazadas"
         TabPicture(1)   =   "PR0275.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraframe1(2)"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraframe1 
            Height          =   5655
            Index           =   2
            Left            =   -74760
            TabIndex        =   12
            Top             =   480
            Width           =   10815
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5325
               HelpContextID   =   2
               Index           =   1
               Left            =   120
               TabIndex        =   13
               Top             =   240
               Width           =   10575
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               SelectTypeRow   =   3
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   -1  'True
               _ExtentX        =   18653
               _ExtentY        =   9393
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraframe1 
            Height          =   5775
            Index           =   1
            Left            =   120
            TabIndex        =   10
            Top             =   360
            Width           =   11175
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5445
               HelpContextID   =   2
               Index           =   0
               Left            =   120
               TabIndex        =   11
               Top             =   240
               Width           =   10935
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               stylesets.count =   1
               stylesets(0).Name=   "Urgente"
               stylesets(0).BackColor=   255
               stylesets(0).Picture=   "PR0275.frx":0038
               SelectTypeRow   =   3
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   19288
               _ExtentY        =   9604
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "Consultar"
      Height          =   375
      Index           =   1
      Left            =   5760
      TabIndex        =   1
      Top             =   720
      Width           =   1515
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   240
      TabIndex        =   4
      Tag             =   "Fecha Desde"
      ToolTipText     =   "Fecha Desde"
      Top             =   720
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   2
      Left            =   2520
      TabIndex        =   5
      Tag             =   "Fecha Hasta"
      ToolTipText     =   "Fecha Hasta"
      Top             =   720
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Height          =   480
      Left            =   0
      TabIndex        =   8
      Top             =   8040
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   847
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   1140
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   2520
      TabIndex        =   2
      Top             =   480
      Width           =   1095
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPeticionesSanitarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMultiInfo1 As New clsCWForm
Dim objMultiInfo2 As New clsCWForm
Const intSalto As Integer = 2100

Private Sub cmdConsultar_Click(Index As Integer)
Dim strfecha1 As String
Dim strfecha2 As String

strfecha1 = Right(dtcDateCombo1(1).Text, 4) & Mid(dtcDateCombo1(1).Text, 4, 2)
strfecha1 = strfecha1 & Left(dtcDateCombo1(1).Text, 2)
strfecha2 = Right(dtcDateCombo1(2).Text, 4) & Mid(dtcDateCombo1(2).Text, 4, 2)
strfecha2 = strfecha2 & Left(dtcDateCombo1(2).Text, 2)

objMultiInfo1.strWhere = "CI01FECCONCERT >=  TO_DATE('" & _
    strfecha1 & "000000','YYYYMMDDHH24MISS') " & _
    "AND CI01FECCONCERT <=  TO_DATE('" & _
    strfecha2 & "235959','YYYYMMDDHH24MISS') " & _
    "AND PR37CODESTADO = 2 AND PR04FECENVSANI IS NULL"
objWinInfo.DataRefresh

objMultiInfo2.strWhere = "CI01FECCONCERT >=  TO_DATE('" & _
    strfecha1 & "000000','YYYYMMDDHH24MISS') " & _
    "AND CI01FECCONCERT <=  TO_DATE('" & _
    strfecha2 & "235959','YYYYMMDDHH24MISS') " & _
    "AND PR37CODESTADO = 2 AND PR04FECENVSANI IS NOT NULL"

End Sub

Private Sub cmdEnviar_Click(Index As Integer)
Dim vntBookmark As Variant
Dim strsql As String
Dim qryUpdate As rdoQuery
Dim i As Integer
Dim intRes As Integer
Dim intGrid As Integer


On Error GoTo Canceltrans
If cmdEnviar(0).Caption = "Enviar Sanitario" Then
    intGrid = 0
    strsql = "UPDATE PR0400 SET PR04FECENVSANI = SYSDATE WHERE "
    strsql = strsql & "PR04NUMACTPLAN = ?"
    intRes = MsgBox("Desea Imprimir las peticiones?", vbYesNo)
    vp.StartDoc
    vp.CurrentY = 100
Else
    intRes = vbNo
    intGrid = 1
    strsql = "UPDATE PR0400 SET PR04FECENVSANI = NULL WHERE "
    strsql = strsql & "PR04NUMACTPLAN = ?"
End If
If grdDBGrid1(intGrid).SelBookmarks.Count = 0 Then
    MsgBox "Seleccione las peticiones", vbOKOnly
Else
    For i = 0 To grdDBGrid1(intGrid).SelBookmarks.Count - 1
        vntBookmark = grdDBGrid1(intGrid).SelBookmarks(i)
        Set qryUpdate = objApp.rdoConnect.CreateQuery("", strsql)
            qryUpdate(0) = grdDBGrid1(intGrid).Columns("ActPlan").CellValue(vntBookmark)
        qryUpdate.Execute
        If intRes = vbYes Then
            Call pCrear_Report(intGrid, vntBookmark, i)
        End If
    Next i
If intRes = vbYes Then
    vp.EndDoc
    vp.PrintDoc
End If
objWinInfo.DataRefresh
End If
Exit Sub
Canceltrans:
        MsgBox "Hay un problema al actualizar las fechas de envio de sanitario", vbCritical, "AVISO"
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  

  Dim strKey As String
  

  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  With objWinInfo.objDoc
    .cwPRJ = "REALIZACION DE PRUEBAS"
    .cwMOD = "M�dulo Maestro-detalle"
    .cwAUT = "EFS"
    .cwDAT = "10-09-1999"
    .cwDES = "Peticiones a Sanitarios"
    .cwEVT = "Descripci�n del evento xxxxx"
  End With
  
  dtcDateCombo1(1).Text = strFecha_Sistema()
  dtcDateCombo1(2).Text = dtcDateCombo1(1).Text
  
  With objMultiInfo1
    .strName = "Peticiones Pendientes"
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intAllowance = cwAllowReadOnly
    .strTable = "PR0455J"
    .strWhere = "CI01FECCONCERT >=  TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD')||'000000','YYYYMMDDHH24MISS') " & _
    "AND CI01FECCONCERT <=  TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD')||'235959','YYYYMMDDHH24MISS') " & _
    "AND PR37CODESTADO = 2 AND PR04FECENVSANI IS NULL"
    Call .FormAddOrderField("CI01FECCONCERT", cwAscending)       'nuevo
  End With
  With objMultiInfo2
    .strName = "Peticiones Realizadas"
    Set .objFormContainer = fraframe1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intAllowance = cwAllowReadOnly
    .strTable = "PR0455J"
    .strWhere = "CI01FECCONCERT >=  TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD')||'000000','YYYYMMDDHH24MISS') " & _
    "AND CI01FECCONCERT <=  TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD')||'235959','YYYYMMDDHH24MISS') " & _
    "AND PR37CODESTADO = 2 AND PR04FECENVSANI IS NOT NULL"
    Call .FormAddOrderField("CI01FECCONCERT", cwAscending)     'nuevo
  End With
  
  With objWinInfo
  Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
  Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo1, "Fecha/hora", "CI01FECCONCERT", cwDate)
    Call .GridAddColumn(objMultiInfo1, "Servicio", "DESDPTO2", cwString)
    Call .GridAddColumn(objMultiInfo1, "Cama", "CAMA", cwString)
    Call .GridAddColumn(objMultiInfo1, "Destino", "DESDPTO1", cwString)
    Call .GridAddColumn(objMultiInfo1, "Recurso", "RECURSO", cwString)
    Call .GridAddColumn(objMultiInfo1, "Transporte", "PR60DESNTRAPAC", cwString)
    Call .GridAddColumn(objMultiInfo1, "Movilidad", "PR59DESMOVPAC", cwString)
    Call .GridAddColumn(objMultiInfo1, "Indicaciones", "PR04DESINDSANIT", cwString)
    Call .GridAddColumn(objMultiInfo1, "Historia", "CI22NUMHISTORIA", cwNumeric)
    Call .GridAddColumn(objMultiInfo1, "Primer Apellido", "CI22PRIAPEL", cwString)
    Call .GridAddColumn(objMultiInfo1, "Segundo Apellido", "CI22SEGAPEL", cwString)
    Call .GridAddColumn(objMultiInfo1, "Nombre", "CI22NOMBRE", cwString)
    Call .GridAddColumn(objMultiInfo1, "Fecha Peticion", "PR03FECADD", cwDate)
    Call .GridAddColumn(objMultiInfo1, "ActPlan", "PR04NUMACTPLAN", cwNumeric)
Call .FormCreateInfo(objMultiInfo1)

    Call .GridAddColumn(objMultiInfo2, "Fecha/hora cita", "CI01FECCONCERT", cwDate)
    Call .GridAddColumn(objMultiInfo2, "Fecha/hora envio", "PR04FECENVSANI", cwDate)
    Call .GridAddColumn(objMultiInfo2, "Historia", "CI22NUMHISTORIA", cwNumeric)
    Call .GridAddColumn(objMultiInfo2, "Primer Apellido", "CI22PRIAPEL", cwString)
    Call .GridAddColumn(objMultiInfo2, "Segundo Apellido", "CI22SEGAPEL", cwString)
    Call .GridAddColumn(objMultiInfo2, "Nombre", "CI22NOMBRE", cwString)
    Call .GridAddColumn(objMultiInfo2, "Servicio", "DESDPTO2", cwString)
    Call .GridAddColumn(objMultiInfo2, "Cama", "CAMA", cwString)
    Call .GridAddColumn(objMultiInfo2, "Destino", "DESDPTO1", cwString)
    Call .GridAddColumn(objMultiInfo2, "Transporte", "PR60DESNTRAPAC", cwString)
    Call .GridAddColumn(objMultiInfo2, "Movilidad", "PR59DESMOVPAC", cwString)
    Call .GridAddColumn(objMultiInfo2, "Indicaciones", "PR04DESINDSANIT", cwString)
    Call .GridAddColumn(objMultiInfo2, "Fecha Peticion", "PR03FECADD", cwDate)
    Call .GridAddColumn(objMultiInfo2, "ActPlan", "PR04NUMACTPLAN", cwNumeric)
  Call .FormCreateInfo(objMultiInfo2)

    Call .WinRegister
    Call .WinStabilize
  End With
Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
 Call pOrganizarGrid

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
   

  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
  
End Sub


Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim intDifTemp As Integer
If Index = 0 Then
  intDifTemp = DateDiff("d", strFechaHora_Sistema, grdDBGrid1(0).Columns("Fecha/hora").Value)
  If intDifTemp = 0 Then
    intDifTemp = DateDiff("n", strFechaHora_Sistema, grdDBGrid1(0).Columns("Fecha/hora").Value)
    If intDifTemp < 15 Then
              Call grdDBGrid1(0).Columns(3).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(4).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(5).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(6).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(7).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(8).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(9).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(10).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(11).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(12).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(13).CellStyleSet("Urgente", grdDBGrid1(0).Row)
              Call grdDBGrid1(0).Columns(14).CellStyleSet("Urgente", grdDBGrid1(0).Row)
    End If
  End If
End If
If grdDBGrid1(0).Columns("Destino").Value = "Radiolog�a" Then
    If InStr(grdDBGrid1(0).Columns("Recurso").Value, "TAC") Or _
        InStr(grdDBGrid1(0).Columns("Recurso").Value, "ANGIO") Or _
        InStr(grdDBGrid1(0).Columns("Recurso").Value, "ECOGRAFO") Then
        grdDBGrid1(0).Columns("Fecha/hora").Value = Format(grdDBGrid1(0).Columns("Fecha/hora").Value, "dd/mm/yyyy")
    End If
End If
If grdDBGrid1(0).Columns("Destino").Value = "Endoscopias" Or _
    grdDBGrid1(0).Columns("Destino").Value = "Hemodinamica" Then
    grdDBGrid1(0).Columns("Fecha/hora").Value = Format(grdDBGrid1(0).Columns("Fecha/hora").Value, "dd/mm/yyyy")
End If
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
    objWinInfo.DataRefresh
If objWinInfo.objWinActiveForm.strName = "Peticiones Pendientes" Then
    cmdEnviar(0).Caption = "Enviar Sanitario"
Else
    cmdEnviar(0).Caption = "Poner en Pendiente"
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------






' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim blnPA As Boolean 'Indica si todos tienen proceso/asist asociados
Dim intI As Integer
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(fraframe1(tabTab1(0).Tab + 1), False, True)
    
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
If intIndex = 0 Then
  Call objWinInfo.CtrlGotFocus
End If
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command_Click
' -----------------------------------------------

Private Sub tmrRefrescar_Timer()
objWinInfo.DataRefresh

End Sub


Private Sub pOrganizarGrid()

    grdDBGrid1(0).Columns("Fecha/hora").Width = 1830
    grdDBGrid1(0).Columns("Servicio").Width = 1065
    grdDBGrid1(0).Columns("Cama").Width = 556
    grdDBGrid1(0).Columns("Destino").Width = 2355
    grdDBGrid1(0).Columns("Transporte").Width = 1905
    grdDBGrid1(0).Columns("Movilidad").Width = 2355
    grdDBGrid1(0).Columns("Indicaciones").Width = 1036
    grdDBGrid1(0).Columns("Historia").Width = 1800
    grdDBGrid1(0).Columns("Primer Apellido").Width = 1965
    grdDBGrid1(0).Columns("Segundo Apellido").Width = 1590
    grdDBGrid1(0).Columns("Nombre").Width = 2205
    grdDBGrid1(0).Columns("ActPlan").Visible = False
    grdDBGrid1(0).Columns("Recurso").Width = 1275
    
    grdDBGrid1(1).Columns("Fecha/hora Cita").Width = 1830
    grdDBGrid1(1).Columns("Fecha/hora Envio").Width = 1830
    grdDBGrid1(1).Columns("Servicio").Width = 1065
    grdDBGrid1(1).Columns("Cama").Width = 556
    grdDBGrid1(1).Columns("Destino").Width = 2355
    grdDBGrid1(1).Columns("Transporte").Width = 1905
    grdDBGrid1(1).Columns("Movilidad").Width = 2355
    grdDBGrid1(1).Columns("Indicaciones").Width = 1036
    grdDBGrid1(1).Columns("Historia").Width = 1800
    grdDBGrid1(1).Columns("Primer Apellido").Width = 1965
    grdDBGrid1(1).Columns("Segundo Apellido").Width = 1590
    grdDBGrid1(1).Columns("Nombre").Width = 2205
    grdDBGrid1(1).Columns("ActPlan").Visible = False

    
    
End Sub
Private Sub pCrear_Report(intG As Integer, vntB As Variant, k As Integer)
Dim strFec As String
    strFec = Format(grdDBGrid1(intG).Columns("Fecha Peticion").CellValue(vntB), "DD/MM/YYYY hh:mm")
    vp.Text = "Fecha Pet: " & strFec
    vp.Text = "  Fecha Imp: " & strFechaHora_Sistema
    vp.Text = Chr$(13)
    strFec = Format(grdDBGrid1(intG).Columns("Fecha/hora").CellValue(vntB), "DD/MM/YY hh:mm")
    vp.Text = "Fecha Cita: " & strFec
    vp.Text = Chr$(13)
    vp.Text = "Hist.: " & grdDBGrid1(intG).Columns("Historia").CellValue(vntB)
    vp.Text = "   Nombre: " & grdDBGrid1(intG).Columns("Primer Apellido").CellValue(vntB) & _
    " " & grdDBGrid1(intG).Columns("Segundo Apellido").CellValue(vntB) & _
    " " & grdDBGrid1(intG).Columns("Nombre").CellValue(vntB)
    vp.Text = Chr$(13)
    vp.Text = "Servicio " & grdDBGrid1(intG).Columns("Servicio").CellValue(vntB)
    vp.Text = " Cama " & grdDBGrid1(intG).Columns("Cama").CellValue(vntB)
    vp.Text = " Dest " & grdDBGrid1(intG).Columns("Destino").CellValue(vntB)
    vp.Text = " - " & grdDBGrid1(intG).Columns("Recurso").CellValue(vntB)
    vp.Text = Chr$(13)
    vp.Text = grdDBGrid1(intG).Columns("Transporte").CellValue(vntB) & _
    ". " & grdDBGrid1(intG).Columns("Movilidad").CellValue(vntB) & _
    ". " & grdDBGrid1(intG).Columns("Indicaciones").CellValue(vntB)
    vp.Text = Chr$(13)
    vp.CurrentY = (k + 1) * intSalto
End Sub
