VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmActuacionesRealizadas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Admitir Paciente"
   ClientHeight    =   8340
   ClientLeft      =   -45
   ClientTop       =   525
   ClientWidth     =   11910
   HelpContextID   =   2
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Recursos consumidos"
      Enabled         =   0   'False
      Height          =   375
      Index           =   3
      Left            =   2160
      TabIndex        =   22
      Top             =   7680
      Width           =   1755
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Observ./Cuestionario"
      Enabled         =   0   'False
      Height          =   375
      Index           =   2
      Left            =   240
      TabIndex        =   21
      Top             =   7680
      Width           =   1755
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2265
      Index           =   0
      Left            =   120
      TabIndex        =   13
      Top             =   480
      Width           =   11715
      Begin TabDlg.SSTab tabTab1 
         Height          =   1665
         Index           =   0
         Left            =   240
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   2937
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "&Detalle"
         TabPicture(0)   =   "PR0272.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(8)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "dtcDateCombo1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cboDBCombo1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "IdPersona1"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "PR0272.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   5
            Left            =   10410
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Sexo|Sexo de la Persona"
            Top             =   900
            Visible         =   0   'False
            Width           =   150
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1455
            Left            =   120
            TabIndex        =   15
            Top             =   120
            Width           =   10665
            _ExtentX        =   18812
            _ExtentY        =   2566
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1425
            Index           =   0
            Left            =   -74820
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   120
            Width           =   10965
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19341
            _ExtentY        =   2514
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "CI30CODSEXO"
            Height          =   330
            HelpContextID   =   30110
            Index           =   0
            Left            =   10590
            TabIndex        =   18
            Tag             =   "Sexo|Sexo"
            Top             =   900
            Visible         =   0   'False
            Width           =   495
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldDelimiter  =   """"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1296
            Columns(0).Caption=   "C�digo"
            Columns(0).Alignment=   1
            Columns(0).CaptionAlignment=   1
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Provincia"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   873
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI22FECNACIM"
            Height          =   330
            Index           =   0
            Left            =   10380
            TabIndex        =   19
            Tag             =   "Fecha Nacimiento|Fecha Nacimiento"
            Top             =   240
            Visible         =   0   'False
            Width           =   735
            _Version        =   65543
            _ExtentX        =   1296
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Sexo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   10410
            TabIndex        =   20
            Top             =   690
            Visible         =   0   'False
            Width           =   435
         End
      End
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   11400
      Top             =   7560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Imp. Petici�n"
      Enabled         =   0   'False
      Height          =   375
      Index           =   1
      Left            =   7920
      TabIndex        =   12
      Top             =   7680
      Width           =   1515
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "Consultar"
      Height          =   375
      Index           =   1
      Left            =   9120
      TabIndex        =   5
      Top             =   2955
      Width           =   1515
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Pedir Pruebas"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   9720
      TabIndex        =   4
      Top             =   7680
      Visible         =   0   'False
      Width           =   1515
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones Realizadas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4155
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   3360
      Width           =   11715
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3765
         HelpContextID   =   2
         Index           =   1
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   11415
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20135
         _ExtentY        =   6641
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   240
      TabIndex        =   8
      Tag             =   "Fecha Desde"
      ToolTipText     =   "Fecha Desde"
      Top             =   3000
      Width           =   1935
      _Version        =   65543
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   2
      Left            =   2280
      TabIndex        =   9
      Tag             =   "Fecha Hasta"
      ToolTipText     =   "Fecha Hasta"
      Top             =   3000
      Width           =   1935
      _Version        =   65543
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
      Height          =   330
      Index           =   1
      Left            =   5640
      TabIndex        =   10
      Top             =   3000
      Width           =   3135
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ";"
      DefColWidth     =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2540
      Columns(0).Caption=   "COD. DPTO"
      Columns(0).Name =   "COD. DPTO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5927
      Columns(1).Caption=   "DESCRIPCI�N"
      Columns(1).Name =   "DESCRIPCI�N"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Selecci�n del   Departamento:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   4
      Left            =   4320
      TabIndex        =   11
      Top             =   2835
      Width           =   1335
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   240
      TabIndex        =   7
      Top             =   2760
      Width           =   1140
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   2280
      TabIndex        =   6
      Top             =   2760
      Width           =   1095
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmActuacionesRealizadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMultiInfo As New clsCWForm


Private Sub cmdCommand1_Click(Index As Integer)
Dim vntdatos(8)
Dim vntA As Variant
Dim strsql1 As String
Dim strpeticion As String
Dim mensaje As String
Dim SQL As String
Dim qry As rdoQuery
Dim rst As rdoResultset
Dim lngProceso As Long
Dim lngAsistencia As Long
cmdCommand1(Index).Enabled = False
If grdDBGrid1(1).SelBookmarks.Count = 0 Then
    Call objError.SetError(cwCodeMsg, "Debe Seleccionar Alguna Actuaci�n Realizada", vntA)
    vntA = objError.Raise
ElseIf IdPersona1.Text <> "" Then
        Select Case Index
            Case 0
'''            'vntdata(1): Numero de grupo
'''                'vntdata(2): Numero de peticion
'''                'vntdata(3): Codigo de persona
'''                'vntdata(4): Aplicaci�n que hace la llamda (3 camas 4 recibir paciente)
'''                'vntdata(5): proceso
'''                'vntdata(6): Asistencia
'''                'vntdate(7): Actuacion plantificada que genera la peticion
'''                'vntdate(8): Departamento solicitante
'''                'EFS: miramos si tiene proceso asistencia y si esa asi se la asitencia
'''                'sigue abierta
'''                 If grdDBGrid1(1).Columns(9).Value <> "" And grdDBGrid1(1).Columns(10).Value <> "" Then
'''                    SQL = "SELECT AD01FECFIN FROM AD0100 WHERE AD01CODASISTENCI=?"
'''                    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'''                        qry(0) = grdDBGrid1(1).Columns(10).Value
'''                    Set rst = qry.OpenResultset()
'''                    If Not rst.EOF Then
'''                        If IsNull(rst!AD01FECFIN) Then
'''                            lngProceso = grdDBGrid1(1).Columns(9).Value
'''                            lngAsistencia = grdDBGrid1(1).Columns(10).Value
'''                        Else
'''                            MsgBox "La Asistencia de esta Actuacion tiene fecha fin." & _
'''                            Chr$(13) & "se asociara solo al proceso", vbOKOnly
'''                            lngProceso = grdDBGrid1(1).Columns(9).Value
'''                            lngAsistencia = 0
'''                        End If
'''                End If
'''                vntdatos(1) = 0
'''                vntdatos(2) = 0
'''                vntdatos(3) = IdPersona1.Text
'''                vntdatos(4) = 5
'''                vntdatos(5) = lngProceso
'''                vntdatos(6) = lngAsistencia
'''                vntdatos(7) = grdDBGrid1(1).Columns(8).Value 'NUM ACT. PLAN
'''                vntdatos(8) = grdDBGrid1(1).Columns(6).Value
'''                Call objsecurity.LaunchProcess("PR0152", vntdatos)
'''                Else
'''                    Call objError.SetError(cwCodeMsg, "Debe Seleccionar Alguna Actuaci�n Realizada", vntA)
'''                    vntA = objError.Raise
'''                End If
    Case 1
        crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
        With crtCrystalReport1
            .PrinterCopies = 1
            .Destination = crptToPrinter
            .SelectionFormula = "({PR0457J.PR04NUMACTPLAN}= " & grdDBGrid1(1).Columns(8).Value & ")"
            .Connect = objApp.rdoConnect.Connect
            .DiscardSavedData = True
            Screen.MousePointer = vbHourglass
            .Action = 1
            Screen.MousePointer = vbDefault
        End With
    Case 2
        'Acceso a los Datos de la Actuaci�n
        Call objPipe.PipeSet("PR_NActPlan", grdDBGrid1(1).Columns(8).Value)
        Call objsecurity.LaunchProcess("PR0502")
    Case 3
        Call objPipe.PipeSet("PR_NActPlan", grdDBGrid1(1).Columns(8).Value)
        Call objPipe.PipeSet("AD_CodDpto", grdDBGrid1(1).Columns("Cod.Dpto").Value)
        frmRecConsumidos.Show vbModal
        Set frmRecConsumidos = Nothing
    End Select
End If 'Hay actuacion y pacientes seleccionados
cmdCommand1(Index).Enabled = True
End Sub

Private Sub cmdConsultar_Click(Index As Integer)
    pRefrescar
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm

  Dim strKey As String
  
  Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  '/+++++++ LLENAMOS EL COMBO DE LOS DEPARTAMENTOS ++++++++++++/
    Dim sqlstr As String
    Dim rsta As rdoResultset
    Dim sqlstrdpto As String
    Dim rstadpto As rdoResultset
    sqlstr = "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM " & _
             "AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objsecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL ORDER BY AD0200.AD02DESDPTO"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    
    While rsta.EOF = False
        cboDBCombo1(1).AddItem rsta("AD02CODDPTO").Value & ";" & rsta("AD02DESDPTO").Value
        cboDBCombo1(1).Text = rsta("AD02DESDPTO").Value
        cboDBCombo1(1).MoveNext
        rsta.MoveNext
    Wend
    cboDBCombo1(1).AddItem "999" & ";" & "TODOS"
    rsta.Close
    Set rsta = Nothing
    cboDBCombo1(1).Enabled = True

' /+++++++++++++++++++++++++++++++++++++++++++++

  With objWinInfo.objDoc
    .cwPRJ = "REALIZACION DE PRUEBAS"
    .cwMOD = "M�dulo Maestro-detalle"
    .cwAUT = "EFS"
    .cwDAT = "10-09-1999"
    .cwDES = "Mantenimiento de pruebas realizadas"
    .cwEVT = "Descripci�n del evento xxxxx"
  End With
  
  dtcDateCombo1(0).Text = strFecha_Sistema()
  dtcDateCombo1(1).Text = strFecha_Sistema()
  
  With objMasterInfo
    .strName = "Pacientes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "CI2200"
    .strWhere = " CI2200.AD34CODESTADO = 1 "
    .intAllowance = cwAllowReadOnly
    .blnHasMaint = True
  End With
  
  With objMultiInfo
    .strName = "Actuaciones realizadas"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intAllowance = cwAllowReadOnly
    .strTable = "PR0453J"
'    .strWhere = "FECHAREALIZACION >= SYSDATE AND FECHAREALIZACION <= SYSDATE"
    .strWhere = "FECHAREALIZACION >=  TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD')||'000000','YYYYMMDDHH24MISS') AND FECHAREALIZACION <=  TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD')||'235959','YYYYMMDDHH24MISS') " & _
    "AND AD02CODDPTO = " & cboDBCombo1(1).Columns(0).Value
    Call .FormAddOrderField("FECHAREALIZACION", cwAscending)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)              'nuevo
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "C�digo Persona", "CI21CODPERSONA", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Fecha Realizacion", "FECHAREALIZACION", cwString)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n", "PR01DESCORTA", cwString)
    Call .GridAddColumn(objMultiInfo, "Cod.Dpto", "AD02CODDPTO", cwString)
    Call .GridAddColumn(objMultiInfo, "Departamento", "", cwString)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n Planificada", "PR04NUMACTPLAN", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "PR04CANTIDAD", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Proceso", "", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Asistencia", "", cwNumeric)
    
    Call .FormCreateInfo(objMasterInfo)
        
   ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
   ' .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
 
    grdDBGrid1(1).Columns(0).Visible = False
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(6).Visible = False
    grdDBGrid1(1).Columns(8).Visible = False
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(7), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("Actuaci�n Planificada")), "AD02CODDPTO", "SELECT AD07CODPROCESO,AD01CODASISTENCI FROM PR0400 WHERE PR04NUMACTPLAN = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("Actuaci�n Planificada")), grdDBGrid1(1).Columns("Proceso"), "AD07CODPROCESO")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("Actuaci�n Planificada")), grdDBGrid1(1).Columns("Asistencia"), "AD01CODASISTENCI")
    .CtrlGetInfo(txtText1(5)).blnInGrid = False
'    .CtrlGetInfo(cboDBCombo1(0)).blnInGrid = False
    .CtrlGetInfo(dtcDateCombo1(0)).blnInGrid = False

    .CtrlGetInfo(cboDBCombo1(0)).strsql = "SELECT CI30CODSEXO,CI30DESSEXO FROM CI3000 ORDER BY CI30CODSEXO"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(0)), "CI30CODSEXO", "SELECT CI30CODSEXO,CI30DESSEXO FROM CI3000 WHERE CI30CODSEXO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(0)), txtText1(5), "CI30DESSEXO")
      
    .CtrlGetInfo(IdPersona1).blnForeign = True 'nuevo
    .CtrlGetInfo(IdPersona1).blnInFind = True  'nuevo
    IdPersona1.ReadPersona                     'nuevo

    Call .WinRegister
    Call .WinStabilize
  End With
IdPersona1.blnSearchButton = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
   
  Call IdPersona1.EndControl        'nuevo
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
  If IdPersona1.Text = "" Then
    cmdCommand1(0).Enabled = False
    cmdCommand1(1).Enabled = False
    cmdCommand1(2).Enabled = False
    cmdCommand1(3).Enabled = False
    grdDBGrid1(1).Refresh
  Else
    cmdCommand1(0).Enabled = True
    cmdCommand1(1).Enabled = True
    cmdCommand1(2).Enabled = True
    cmdCommand1(3).Enabled = True
  End If
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
  If IdPersona1.Text <> "" And IdPersona1.Apellido1 <> "" Then
    cmdCommand1(0).Enabled = True
    cmdCommand1(1).Enabled = True
    cmdCommand1(2).Enabled = True
    cmdCommand1(3).Enabled = True
  Else
    cmdCommand1(0).Enabled = False
    cmdCommand1(1).Enabled = False
    cmdCommand1(2).Enabled = False
    cmdCommand1(3).Enabled = False
  End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Select Case strCtrl
    Case "IdPersona1"                  'nuevo
      IdPersona1.SearchPersona         'nuevo
  End Select

End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
Dim vntdatos() As Variant
  'Mantenimiento de personas
  If strFormName = "Pacientes" Then
    ReDim vntdatos(1)
    vntdatos(1) = IdPersona1.Text
    Call objsecurity.LaunchProcess("CI1017", vntdatos)
    objWinInfo.DataRefresh
  End If
End Sub


Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(3), IdPersona1.Text) 'nuevo
End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim blnPA As Boolean 'Indica si todos tienen proceso/asist asociados
Dim intI As Integer
'  objWinInfo.DataRefresh
  If btnButton.Index = 16 Then
    Call IdPersona1.Buscar
  Else
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
    
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
 
 

 

 


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
If intIndex <> 1 And intIndex <> 2 Then
  Call objWinInfo.CtrlGotFocus
End If
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

'Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
'End Sub
'
'Private Sub dtcDateCombo1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
'End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
If intIndex = 0 Then
  Call objWinInfo.CtrlGotFocus
End If
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
If intIndex = 1 Then
    pRefrescar
Else
  Call objWinInfo.CtrlDataChange
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
'Creamos un proceso asistencia con fecha de hoy y los mismos datos
'de la actuacion que estamos seleccionando.
Private Sub pCrearProAsis(lngProceso&, lngAsistencia&)
Dim SQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim qryI As rdoQuery
Dim lngNewProc As Long
Dim lngNewAsis As Long
On Error Resume Next
'Datos previos
lngNewProc = flngNextClaveSeq("AD07CODPROCESO")
lngNewAsis = flngNextClaveSeq("AD01CODASISTENCI")

objApp.BeginTrans
'CREAMOS EL PROCESO
SQL = "SELECT CI21CODPERSONA,AD07DESNOMBPROCE,AD07FECHORAINICI,CI22NUMHISTORIA,AD34CODESTADO "
SQL = SQL & "FROM AD0700 WHERE AD07CODPROCESO = ?"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngProceso
Set rs = qry.OpenResultset()
SQL = "INSERT INTO AD0700 (CI21CODPERSONA,AD07CODPROCESO,AD07DESNOMBPROCE,"
SQL = SQL & "AD07FECHORAINICI,CI22NUMHISTORIA,AD34CODESTADO)"
SQL = SQL & " VALUES(?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?)"
Set qryI = objApp.rdoConnect.CreateQuery("", SQL)
    qryI(0) = rs!CI21CODPERSONA
    qryI(1) = lngNewProc
    If Not IsNull(rs!AD07DESNOMBPROCE) Then
        qryI(2) = rs!AD07DESNOMBPROCE
    Else
        qryI(2) = Null
    End If
    qryI(3) = Format(rs!AD07FECHORAINICI, "dd/mm/yyyy hh:mm")
    qryI(4) = rs!CI22NUMHISTORIA
    qryI(5) = rs!AD34CODESTADO
qryI.Execute
rs.Close
qry.Close
qryI.Close
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede crear el proceso", vbCritical
    Exit Sub
End If
'CREAMOS EL DEPARTAMENTO RESPONSABLE DEL PROCESO
SQL = "SELECT AD04FECINIRESPON, AD02CODDPTO,SG02COD FROM AD0400 WHERE AD04FECFINRESPON IS NULL"
SQL = SQL & " AND AD07CODPROCESO = ?"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngProceso
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    SQL = "INSERT INTO AD0400 (AD07CODPROCESO,AD04FECINIRESPON,AD02CODDPTO,SG02COD)"
    SQL = SQL & " VALUES (?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?)"
    Set qryI = objApp.rdoConnect.CreateQuery("", SQL)
        qryI(0) = lngNewProc
        qryI(1) = Format(rs!AD04FECINIRESPON, "dd/mm/yyyy hh:mm")
        qryI(2) = rs!AD02CODDPTO
        qryI(3) = rs!SG02COD
    qryI.Execute
End If
rs.Close
qry.Close
qryI.Close
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede crear el Dpto Res. del proceso", vbCritical
    Exit Sub
End If

'CREAMOS LA ASISTENCIA
SQL = "SELECT CI21CODPERSONA, AD27CODALTAASIST,AD01INDURGENTE,AD01FECINICIO,"
SQL = SQL & " AD01DIAGNOSSALI,CI22NUMHISTORIA FROM AD0100 WHERE "
SQL = SQL & "AD01CODASISTENCI = ? "
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngAsistencia
Set rs = qry.OpenResultset()
SQL = "INSERT INTO AD0100 (CI21CODPERSONA, AD01CODASISTENCI,AD27CODALTAASIST,"
SQL = SQL & "AD01INDURGENTE,AD01FECINICIO,AD01DIAGNOSSALI,CI22NUMHISTORIA) VALUES"
SQL = SQL & "(?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?)"
Set qryI = objApp.rdoConnect.CreateQuery("", SQL)
    qryI(0) = rs!CI21CODPERSONA
    qryI(1) = lngNewAsis
    qryI(2) = rs!AD27CODALTAASIST
    If IsNull(rs!AD01INDURGENTE) Then
        qryI(3) = Null
    Else
        qryI(3) = rs!AD01INDURGENTE
    End If
    qryI(4) = Format(rs!AD01FECINICIO, "dd/mm/yyyy hh:mm")
    qryI(5) = rs!AD01DIAGNOSSALI
    qryI(6) = rs!CI22NUMHISTORIA
qryI.Execute
rs.Close
qry.Close
qryI.Close
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede crear la asistsencia", vbCritical
    Exit Sub
End If
'SITUACION ASISTENCIA
SQL = "SELECT AD25FECINICIO,AD12CODTIPOASIST,AD25FECPREVALTA FROM "
SQL = SQL & "AD2500 WHERE AD01CODASISTENCI = ? AND AD25FECFIN IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngAsistencia
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    SQL = "INSERT INTO AD2500 (AD01CODASISTENCI,AD25FECINICIO,AD12CODTIPOASIST)"
    SQL = SQL & " VALUES(?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?)"
    Set qryI = objApp.rdoConnect.CreateQuery("", SQL)
        qryI(0) = lngNewAsis
        qryI(1) = Format(rs!AD25FECINICIO, "dd/mm/yyyy hh:mm")
        qryI(2) = rs!AD12CODTIPOASIST
    qryI.Execute
End If
rs.Close
qry.Close
qryI.Close
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede crear la situacion de la Asistenci", vbCritical
    Exit Sub
End If
'CREAMOS EL PROCESO ASISTENCIA
SQL = "SELECT AD10CODTIPPACIEN,CI21CODPERSONA_ENV,CI21CODPERSONA_MED,"
SQL = SQL & "AD08INDREQIM,AD08INDPADRE,AD08FECINICIO,AD08OBSERVAC FROM AD0800 WHERE "
SQL = SQL & "AD07CODPROCESO = ? AND AD01CODASISTENCI = ?"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngProceso
    qry(1) = lngAsistencia
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    SQL = "INSERT INTO AD0800 (AD10CODTIPPACIEN,CI21CODPERSONA_ENV,CI21CODPERSONA_MED,"
    SQL = SQL & "AD08INDREQIM,AD08INDPADRE,AD08FECINICIO,AD08OBSERVAC,AD07CODPROCESO,AD01CODASISTENCI)"
    SQL = SQL & " VALUES(?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?,?)"
    Set qryI = objApp.rdoConnect.CreateQuery("", SQL)
        qryI(0) = rs!AD10CODTIPPACIEN
        If IsNull(rs!CI21CODPERSONA_ENV) Then
            qryI(1) = Null
        Else
            qryI(1) = rs!CI21CODPERSONA_ENV
        End If
        If IsNull(rs!CI21CODPERSONA_MED) Then
            qryI(2) = Null
        Else
            qryI(2) = rs!CI21CODPERSONA_MED
        End If
        qryI(3) = rs!AD08INDREQIM
        qryI(4) = rs!AD08INDPADRE
        qryI(5) = Format(rs!AD08FECINICIO, "dd/mm/yyyy hh:mm")
        If IsNull(rs!AD08OBSERVAC) Then
            qryI(6) = Null
        Else
            qryI(6) = rs!AD08OBSERVAC
        End If
        qryI(7) = lngNewProc
        qryI(8) = lngNewAsis
    qryI.Execute
End If
rs.Close
qry.Close
qryI.Close
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede crear el proceso/asistencia", vbCritical
    Exit Sub
End If
'RESPONSABLE ECONOMICO PROCESO/ASISTENCIA
SQL = "SELECT AD11FECINICIO,CI32CODTIPECON,CI21CODPERSONA,CI13CODENTIDAD,"
SQL = SQL & "AD11INDVOLANTE FROM AD1100 WHERE AD01CODASISTENCI = ? AND "
SQL = SQL & "AD07CODPROCESO = ? AND AD11FECFIN IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngAsistencia
    qry(1) = lngProceso
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    SQL = "INSERT INTO AD1100 (AD11FECINICIO,CI32CODTIPECON,CI21CODPERSONA,CI13CODENTIDAD,"
    SQL = SQL & "AD11INDVOLANTE,AD01CODASISTENCI,AD07CODPROCESO) VALUES(TO_DATE (?,'DD/MM/YYYY HH24:MI'),"
    SQL = SQL & "?,?,?,?,?,?)"
    Set qryI = objApp.rdoConnect.CreateQuery("", SQL)
        qryI(0) = Format(rs!AD11FECINICIO, "dd/mm/yyyy hh:mm")
        qryI(1) = rs!CI32CODTIPECON
        qryI(2) = rs!CI21CODPERSONA
        qryI(3) = rs!CI13CODENTIDAD
        qryI(4) = rs!AD11INDVOLANTE
        qryI(5) = lngNewAsis
        qryI(6) = lngNewProc
    qryI.Execute
End If
rs.Close
qry.Close
qryI.Close
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede crear el Resp. Economico proceso/asistencia", vbCritical
    Exit Sub
End If
'CREAMOS EL DEPARTAMENTO RESPONSABLE DEL PROCESO ASISTENCIA
SQL = "SELECT  AD05FECINIRESPON,AD02CODDPTO,SG02COD FROM AD0500"
SQL = SQL & " WHERE AD01CODASISTENCI = ? AND AD07CODPROCESO = ? AND  AD05FECFINRESPON IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngAsistencia
    qry(1) = lngProceso
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    SQL = "INSERT INTO AD0500 (AD05FECINIRESPON,AD02CODDPTO,SG02COD,"
    SQL = SQL & "AD01CODASISTENCI,AD07CODPROCESO) VALUES ("
    SQL = SQL & "TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?,?,?)"
    Set qryI = objApp.rdoConnect.CreateQuery("", SQL)
        qryI(0) = Format(rs!AD05FECINIRESPON, "dd/mm/yyyy hh:mm")
        qryI(1) = rs!AD02CODDPTO
        qryI(2) = rs!SG02COD
        qryI(3) = lngNewAsis
        qryI(4) = lngNewProc
    qryI.Execute
End If
rs.Close
qry.Close
qryI.Close
If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "No se puede crear el dpto resp. proceso/asistencia", vbCritical
    Exit Sub
End If
objApp.CommitTrans
lngProceso = lngNewProc
lngAsistencia = lngNewAsis

End Sub
Public Function flngNextClaveSeq(strCampo As String) As Long
    
Dim rstMaxClave As rdoResultset

On Error Resume Next
Err = 0
Set rstMaxClave = objApp.rdoConnect.OpenResultset("SELECT " & strCampo & "_SEQUENCE.NEXTVAL FROM DUAL")
flngNextClaveSeq = rstMaxClave(0)
rstMaxClave.Close
Set rstMaxClave = Nothing
If Err > 0 Then flngNextClaveSeq = -1
    
End Function
Public Sub pRefrescar()
cboDBCombo1(1).Text = cboDBCombo1(1).Columns(1).Value
    If IdPersona1.Text = "" Then
        MsgBox "Seleccione una paciente", vbCritical
        Exit Sub
    End If
    If IsDate(dtcDateCombo1(2).Date) And IsDate(dtcDateCombo1(1).Date) Then
            If DateDiff("d", dtcDateCombo1(2).Date, dtcDateCombo1(1).Date) > 0 Then
              Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
              Call objError.Raise
              Exit Sub
            End If
    End If
    If cboDBCombo1(1).Text = "" Then
            Call objError.SetError(cwCodeMsg, "Seleccione un departamento")
            Call objError.Raise
            Exit Sub
    End If
    If cboDBCombo1(1).Columns(0).Value = 999 Then
         objMultiInfo.strWhere = "FECHAREALIZACION >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND FECHAREALIZACION < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY')"
        objWinInfo.DataRefresh
    Else
        objMultiInfo.strWhere = "FECHAREALIZACION >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND FECHAREALIZACION < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY') AND " & _
        "AD02CODDPTO = " & cboDBCombo1(1).Columns(0).Value
        objWinInfo.DataRefresh
    End If
End Sub
