VERSION 5.00
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmImprimirEtiquetas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Imprimir Etiquetas"
   ClientHeight    =   1230
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5460
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1230
   ScaleWidth      =   5460
   StartUpPosition =   2  'CenterScreen
   Begin vsViewLib.vsPrinter vs 
      Height          =   255
      Left            =   360
      TabIndex        =   8
      Top             =   720
      Visible         =   0   'False
      Width           =   255
      _Version        =   196608
      _ExtentX        =   450
      _ExtentY        =   450
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   4320
      TabIndex        =   7
      Top             =   720
      Width           =   975
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir"
      Height          =   375
      Left            =   3120
      TabIndex        =   6
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtNetiq 
      BackColor       =   &H00FFFF00&
      Height          =   375
      Left            =   4920
      MaxLength       =   2
      TabIndex        =   5
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox txtColumna 
      BackColor       =   &H00FFFF00&
      Height          =   375
      Left            =   3072
      MaxLength       =   1
      TabIndex        =   3
      Top             =   120
      Width           =   375
   End
   Begin VB.TextBox txtFila 
      BackColor       =   &H00FFFF00&
      Height          =   375
      Left            =   1224
      MaxLength       =   2
      TabIndex        =   0
      Top             =   120
      Width           =   375
   End
   Begin VB.Label lblLabel1 
      Caption         =   "N� Etiquetas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   3576
      TabIndex        =   4
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Columna (1-3)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   1728
      TabIndex        =   2
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Fila (1-11)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmImprimirEtiquetas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strCodPersona As String

Dim intTipo As Integer
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim TG(9) As String
Dim BNS As Integer
Dim BBS As Integer
Dim BAL As Integer

Private Sub Form_Load()
intPosX = 75
intPosY = 335
intIncX = 3900
intIncY = 1433

TG(0) = "00110"
TG(1) = "10001"
TG(2) = "01001"
TG(3) = "11000"
TG(4) = "00101"
TG(5) = "10100"
TG(6) = "01100"
TG(7) = "00011"
TG(8) = "10010"
TG(9) = "01010"

BNS = 15
BBS = 45
BAL = 700

txtFila = "1"
txtColumna = "1"
txtNetiq = "1"
If objPipe.PipeExist("CI_CODPER") Then
    strCodPersona = objPipe.PipeGet("CI_CODPER")
    objPipe.PipeRemove ("CI_CODPER")
End If
End Sub
Private Sub cmdImprimir_Click()
Call pImprimirEtiquetas(strCodPersona)
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub txtColumna_KeyPress(KeyAscii As Integer)
If (KeyAscii < 48 Or KeyAscii > 51) And KeyAscii <> 8 Then
    KeyAscii = 13
End If
End Sub

Private Sub txtFila_Change()
If txtFila <> "" Then
    If Int(txtFila) < 1 Or Int(txtFila) > 11 Then
        MsgBox "Debe haber entre 1 y 11 filas"
        txtFila = "1"
    End If
End If
End Sub

Private Sub txtFila_KeyPress(KeyAscii As Integer)
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
        KeyAscii = 13
    End If
End Sub


Private Sub txtNetiq_KeyPress(KeyAscii As Integer)
If txtNetiq <> "1" And txtNetiq <> "2" And txtNetiq <> "3" Then
    txtNetiq = "1"
End If
End Sub
Private Sub pImprimirEtiquetas(strCodPersona As String)
Dim fila As Integer
Dim col As Integer
Dim intFilaIni As Integer
Dim intColIni  As Integer
Dim X As Integer
Dim strNombre As String
Dim strFecha As String
Dim strHistoria As String
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim intPX As Integer
Dim intPY As Integer
Dim intContEtiq As Integer
vs.PaperBin = 1

intPX = intPosX + (intIncX * (Int(txtColumna.Text) - 1))
intPY = intPosY + (intIncY * (Int(txtFila.Text) - 1))
vs.StartDoc
vs.CurrentX = intPX
vs.CurrentY = intPY
intFilaIni = Int(txtFila)
intColIni = Int(txtColumna)
'datos previos
sql = "SELECT CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI22FECNACIM, CI22NUMHISTORIA FROM CI2200 WHERE "
sql = sql & "CI21CODPERSONA = ? "
'
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodPersona
Set rs = qry.OpenResultset()
If rs.EOF Then
    MsgBox "Persona Fisica inexistente", vbOKOnly
    Exit Sub
End If
If Not IsNull(rs!CI22SEGAPEL) Then
    strNombre = rs!CI22PRIAPEL & " " & rs!CI22SEGAPEL & ", " & rs!CI22NOMBRE
Else
  strNombre = rs!CI22PRIAPEL & ", " & rs!CI22NOMBRE
End If
If Not IsNull(rs!CI22NUMHISTORIA) Then strHistoria = rs!CI22NUMHISTORIA Else strHistoria = ""

If Len(strNombre) > 40 Then _
strNombre = Right(strNombre, 40)

'strNombre = rs!CI22PRIAPEL & " " & rs!CI22SEGAPEL & Chr$(13) & rs!CI22NOMBRE
'fecha de nacimiento

If Not IsNull(rs!CI22FECNACIM) Then
    strFecha = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
Else
    strFecha = ""
End If
rs.Close
qry.Close
intContEtiq = 0
While intContEtiq < Int(txtNetiq)
    For fila = intFilaIni To 11
        For col = intColIni To 3
          X = vs.CurrentX
          vs.FontSize = 12
          vs.Text = "Hist: "
          vs.FontBold = True
          vs.Text = strHistoria
          vs.FontBold = False
          vs.Text = Chr$(13)
          vs.CurrentX = X
          vs.FontSize = 8
          vs.Text = strNombre
          vs.Text = Chr$(13)
          vs.CurrentX = X
          vs.Text = strFecha
          X = X + intIncX
          vs.CurrentX = X
          vs.CurrentY = intPY
          intContEtiq = intContEtiq + 1
          If intContEtiq >= Int(txtNetiq) Then Exit For
        Next col
        If intContEtiq >= Int(txtNetiq) Then Exit For
        intColIni = 1
        vs.CurrentX = intPosX
        intPY = intPY + intIncY
        vs.CurrentY = intPY
    Next fila
    intFilaIni = 1
    vs.EndDoc
    vs.PrintDoc
    intPX = intPosX
    intPY = intPosY
    vs.CurrentX = intPX
    vs.CurrentY = intPY
Wend
End Sub



