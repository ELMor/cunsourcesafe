VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmrellenarinstreal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Actuaciones que necesitan Instrucciones de Realizaci�n"
   ClientHeight    =   8250
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   Icon            =   "PR0195.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8250
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdconsultar 
      Caption         =   "Consultar"
      Height          =   615
      Left            =   10320
      TabIndex        =   9
      Top             =   480
      Width           =   1455
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "Citas-Agenda"
      Height          =   375
      Index           =   1
      Left            =   7320
      TabIndex        =   7
      Top             =   7560
      Width           =   1935
   End
   Begin VB.CommandButton cmdObservaciones 
      Caption         =   "Datos sobre la Act."
      Height          =   375
      Left            =   360
      TabIndex        =   6
      Top             =   7560
      Width           =   1935
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "Citas"
      Height          =   375
      Index           =   0
      Left            =   5040
      TabIndex        =   5
      Top             =   7560
      Width           =   1935
   End
   Begin VB.CommandButton cmdinstreal 
      Caption         =   "Introducir Instrucciones"
      Height          =   375
      Left            =   2640
      TabIndex        =   0
      Top             =   7560
      Width           =   2055
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones que necesitan Instrucciones de Realizaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6240
      Index           =   0
      Left            =   360
      TabIndex        =   2
      Top             =   1200
      Width           =   11295
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5745
         Index           =   0
         Left            =   240
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   10920
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   19262
         _ExtentY        =   10134
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   7965
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   360
      TabIndex        =   8
      Top             =   360
      Width           =   9855
      Begin VB.CheckBox chkanteriores 
         Caption         =   "Ver actuaciones anteriores"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   315
         Width           =   2655
      End
      Begin VB.CheckBox chksinfecha 
         Caption         =   "Actuaciones sin fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2920
         TabIndex        =   10
         Top             =   315
         Value           =   1  'Checked
         Width           =   2415
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   6240
         TabIndex        =   12
         Top             =   240
         Width           =   3135
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         DefColWidth     =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "COD. DPTO"
         Columns(0).Name =   "COD. DPTO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5927
         Columns(1).Caption=   "DESCRIPCI�N"
         Columns(1).Name =   "DESCRIPCI�N"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5530
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "GRUPO"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   5360
         TabIndex        =   11
         Top             =   315
         Width           =   855
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmrellenarinstreal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS  (REALIZACI�N DE ACTUACIONES)                      *
'* NOMBRE:                                                              *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 10 DE NOVIEMBRE DE 1997                                       *
'* DESCRIPCI�N: Consultar Estado de Actuaciones                         *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMultiInfo As New clsCWForm


Private Sub cmdCitar_Click(intIndex As Integer)
  Dim strsql As String
  Dim rsta As rdoResultset
  Dim vntA As Variant
  Dim vntDatos(1 To 3)
  
  If grdDBGrid1(0).SelBookmarks.Count = 0 Then
    Call objError.SetError(cwCodeMsg, "Debe Seleccionar Alguna Actuaci�n Planificada", vntA)
    vntA = objError.Raise
  Else
     Select Case intIndex
       Case 0
            Call objsecurity.LaunchProcess("CI1150", grdDBGrid1(0).Columns("C�DIGO").Value)
       Case 1
          vntDatos(1) = grdDBGrid1(0).Columns("Cod. Paciente").Value 'numero persona
          vntDatos(2) = grdDBGrid1(0).Columns(4).Value  'cod act plani.
          vntDatos(3) = grdDBGrid1(0).Columns(5).Value  'descrip. actuacion
          Call objsecurity.LaunchProcess("CI1048", vntDatos)
    End Select
    objWinInfo.DataRefresh
 End If
 
End Sub

Private Sub cmdConsultar_Click()
Dim sql As String
Dim qryC As rdoQuery
Dim rstC As rdoResultset
Dim strIn As String
Dim strin1 As String
Screen.MousePointer = vbHourglass
If chkanteriores.Value = 0 Then
        strin1 = " and  FECHA >= TRUNC(SYSDATE)"
        If chksinfecha.Value = 1 Then
            strin1 = "and  (FECHA >= TRUNC(SYSDATE) OR FECHA IS NULL)"
        End If
    Else
        strin1 = " AND FECHA < TRUNC(SYSDATE)"
        If chksinfecha.Value = 1 Then strin1 = " and  (FECHA < TRUNC(SYSDATE) OR FECHA IS NULL)"
       
End If
SSDBCombo1(0).Text = SSDBCombo1(0).Columns(1).Text
If SSDBCombo1(0).Text <> "TODOS" Then
    sql = "SELECT PR01CODACTUACION FROM PR1700 WHERE PR16CODGRUPO = ? "
    Set qryC = objApp.rdoConnect.CreateQuery("", sql)
        qryC(0) = SSDBCombo1(0).Columns(0).Value
    Set rstC = qryC.OpenResultset()
    If Not rstC.EOF Then
        strIn = rstC(0).Value
        rstC.MoveNext
        Do While Not rstC.EOF
            strIn = strIn & "," & rstC(0).Value
            rstC.MoveNext
        Loop
    End If
    rstC.Close
    qryC.Close
    
    'If chksinfecha.Value = 0 Then strin1 = strin1" and "
    
    objMultiInfo.strWhere = "AD02CODDPTO = " & glngdptologin & " AND PR01CODACTUACION IN (" & strIn & ")" & strin1
Else
    objMultiInfo.strWhere = "AD02CODDPTO = " & glngdptologin & strin1
End If
objWinInfo.DataRefresh
Screen.MousePointer = vbDefault

End Sub

Private Sub cmdinstreal_Click()
  Dim intResp As Integer
  Dim sql As String
  Dim qry As rdoQuery
  Dim rs As rdoResultset
  
  
  cmdinstreal.Enabled = False
  If grdDBGrid1(0).Rows > 0 Then
  Call objPipe.PipeSet("PR04NUMACTPLAN", grdDBGrid1(0).Columns(4).Value)
    Call objsecurity.LaunchProcess("PR0196")
  Call objPipe.PipeRemove("PR04NUMACTPLAN")
  sql = "SELECT PR04DESINSTREA FROM PR0400 WHERE PR04NUMACTPLAN = ?"
  Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = grdDBGrid1(0).Columns(4).Value
  Set rs = qry.OpenResultset()
  If IsNull(rs(0)) Then
        grdDBGrid1(0).Columns(3).Value = 0
  Else
        grdDBGrid1(0).Columns(3).Value = -1
  End If
  rs.Close
  qry.Close
'    If glnact = True Then
'                        grdDBGrid1(0).Columns(3).Value = -1
'
'                    Else: grdDBGrid1(0).Columns(3).Value = 0
'
'                    End If
  'call fComp_inst
  Else
    intResp = MsgBox("No hay actuaciones para rellenar sus instrucciones", vbInformation, "Aviso")
  End If
  cmdinstreal.Enabled = True
  Call pBotones
End Sub



Private Sub cmdObservaciones_Click()
If grdDBGrid1(0).Columns(4).Value <> "" Then
 Call objPipe.PipeSet("PR_NActPlan", grdDBGrid1(0).Columns(4).Value)
 Call objsecurity.LaunchProcess("PR0502")
End If
End Sub

Private Sub Form_Load()
  Dim strKey As String
  Dim sql As String
  Dim qry As rdoQuery
  Dim rs As rdoResultset
  Dim filas As Integer
    sql = "SELECT AD02CODDPTO FROM AD0300 WHERE   SG02COD=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objsecurity.strUser
    Set rs = qry.OpenResultset()
    Do While filas < 2 And rs.EOF = False
        glngdptologin = rs(0)
        filas = filas + 1
        rs.MoveNext
    Loop
    
    If filas >= 2 Then
        glngdptologin = ""
        Call objsecurity.LaunchProcess("PR0194")
    End If
    rs.Close
    qry.Close
    
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0423J"
    .intAllowance = cwAllowReadOnly
    
    '*************************************++
    '.strwhere FILTRAR� PARA UN DEPARTAMENTO concreto
    .strWhere = "AD02CODDPTO=" & glngdptologin & " and  (FECHA >= TRUNC(SYSDATE) OR FECHA IS NULL)"
               
    '*************************************+
    .intCursorSize = 0

    'Ordena por PRIMER APELLIDO
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Rellenar Instrucciones de Realizaci�n")
    Call .FormAddFilterWhere(strKey, "PR04NUMACTPLAN", "C�digo Actuaci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha de Citaci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "HORA", "Hora de Planificaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre Paciente", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", " Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    
    Call .FormAddFilterOrder(strKey, "PR04NUMACTPLAN", "C�digo Actuaci�n")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha de Citaci�n")
    Call .FormAddFilterOrder(strKey, "HORA", "Hora de Planificaci�n")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre Paciente")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", " Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
  End With
  
  With objWinInfo

    
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    '3/05/00
    
    Call .GridAddColumn(objMultiInfo, "Rellenadas", "PRINSREAL", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "C�DIGO", "PR04NUMACTPLAN", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "ACTUACI�N", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "FECHA PLAN.", "FECHA", cwDate, 10)
    Call .GridAddColumn(objMultiInfo, "HORA PLAN", "HORA", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "FECHA PREFE.", "FECHAPF", cwDate, 10)
    Call .GridAddColumn(objMultiInfo, "HORA PREFE.", "HORAPF", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "NOMBRE PACIENTE", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "APELLIDO 1�", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "APELLIDO 2�", "CI22SEGAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Cod. Paciente", "CI21CODPERSONA", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Cama", "CAMA", cwNumeric, 9)
    
    
    Call .FormCreateInfo(objMultiInfo)
    
    Call .FormChangeColor(objMultiInfo)
  
    '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True 'SE QUEDA COLGADO
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(13)).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  grdDBGrid1(0).Columns(3).Width = 885
  grdDBGrid1(0).Columns(5).Width = 2369
  grdDBGrid1(0).Columns(6).Width = 2205
  grdDBGrid1(0).Columns(7).Width = 8189
  grdDBGrid1(0).Columns(8).Width = 989
  grdDBGrid1(0).Columns(9).Width = 705
  grdDBGrid1(0).Columns(10).Width = 1544
  grdDBGrid1(0).Columns(11).Width = 1604
  grdDBGrid1(0).Columns(12).Width = 1544
  grdDBGrid1(0).Columns("FECHA PREFE.").Width = 985
  grdDBGrid1(0).Columns("HORA PREFE.").Width = 700
  grdDBGrid1(0).Columns(4).Visible = False
  grdDBGrid1(0).Columns("FECHA PLAN.").Visible = True
  grdDBGrid1(0).Columns("HORA PLAN").Visible = True
  grdDBGrid1(0).Columns("HORA PLAN").Width = 600
  grdDBGrid1(0).Columns("Cod. Paciente").Visible = False
  Call pCargar_Combo
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
 'Call fComp_inst
End Sub

'Public Sub fComp_inst()
'Dim strsql As String
' Dim qryInst As rdoQuery
' Dim rstInst As rdoResultset
'
' strsql = "SELECT PR04DESINSTREA FROM PR0400 WHERE PR04NUMACTPLAN = ?"
' Set qryInst = objApp.rdoConnect.CreateQuery("", strsql)
' qryInst(0) = grdDBGrid1(0).Columns(4).Value
' Set rstInst = qryInst.OpenResultset(strsql)
' If Not rstInst.EOF Then
'    If Not IsNull(rstInst(0).Value) Then
'        grdDBGrid1(0).Columns(3).Value = -1
'    Else
'        grdDBGrid1(0).Columns(3).Value = 0
'    End If
'End If
'
'rstInst.Close
'qryInst.Close
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)

End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)

End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  Call pBotones
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   Call pBotones
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
                                    
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  Call pBotones
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  Call pBotones
End Sub
Private Sub pBotones()
If grdDBGrid1(0).Rows > 0 Then
    If Val(grdDBGrid1(0).Columns(3).Value) <> 0 Then
        cmdCitar(0).Enabled = True
        cmdCitar(1).Enabled = True
    Else
        cmdCitar(0).Enabled = False
        cmdCitar(1).Enabled = False
    End If
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub pCargar_Combo()
Dim sql As String
Dim qryC As rdoQuery
Dim rstC As rdoResultset
Select Case glngdptologin
    Case 209   'RAYOS
        sql = "SELECT PR16CODGRUPO, PR16DESGRUPO FROM PR1600 WHERE "
        sql = sql & "PR15CODTIPGRUPO = ?"
        Set qryC = objApp.rdoConnect.CreateQuery("", sql)
            qryC(0) = "178"
        Set rstC = qryC.OpenResultset()
    Case Else
       SSDBCombo1(0).AddItem "" & ";" & "TODOS"
       SSDBCombo1(0).Text = "TODOS"
       SSDBCombo1(0).Enabled = False
       Exit Sub
End Select
    SSDBCombo1(0).Text = "TODOS"
    SSDBCombo1(0).AddItem "" & ";" & "TODOS"
    Do While Not rstC.EOF
       SSDBCombo1(0).AddItem rstC(0) & ";" & rstC(1)
       rstC.MoveNext
    Loop
End Sub
