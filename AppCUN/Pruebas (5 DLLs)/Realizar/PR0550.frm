VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "crystl32.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmIntervencionesPendientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recibir al paciente: Intervenciones pendientes"
   ClientHeight    =   8370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11805
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   11805
   StartUpPosition =   3  'Windows Default
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   11100
      Top             =   -120
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Frame fraFrame4 
      Caption         =   "Impresos"
      ForeColor       =   &H00800000&
      Height          =   1035
      Left            =   4260
      TabIndex        =   20
      Top             =   0
      Width           =   6135
      Begin VB.OptionButton optImprimir 
         Caption         =   "Listado Quir. 10,11"
         Height          =   195
         Index           =   4
         Left            =   180
         TabIndex        =   30
         Top             =   660
         Width           =   1695
      End
      Begin VB.OptionButton optImprimir 
         Caption         =   "Petici�n"
         Height          =   195
         Index           =   3
         Left            =   3780
         TabIndex        =   28
         Top             =   300
         Width           =   1095
      End
      Begin VB.OptionButton optImprimir 
         Caption         =   "Etiquetas"
         Height          =   195
         Index           =   2
         Left            =   2220
         TabIndex        =   27
         Top             =   300
         Width           =   1155
      End
      Begin VB.OptionButton optImprimir 
         Caption         =   "Programa Pac."
         Height          =   195
         Index           =   1
         Left            =   2220
         TabIndex        =   26
         Top             =   660
         Width           =   1515
      End
      Begin VB.OptionButton optImprimir 
         Caption         =   "Listado Quir. 1-8"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   25
         Top             =   300
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.CommandButton cmdComun 
         Caption         =   "&Imprimir"
         Height          =   375
         Index           =   4
         Left            =   4920
         TabIndex        =   21
         Top             =   420
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   3060
      TabIndex        =   19
      Top             =   660
      Width           =   1035
   End
   Begin Crystal.CrystalReport crtReport1 
      Left            =   10380
      Top             =   7920
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Rea&lizaci�n Interv."
      Height          =   375
      Index           =   3
      Left            =   4920
      TabIndex        =   13
      Top             =   7920
      Width           =   1695
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Vision &Global"
      Height          =   375
      Index           =   2
      Left            =   3480
      TabIndex        =   12
      Top             =   7920
      Width           =   1335
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "O&bserv. / Cuestionario"
      Height          =   375
      Index           =   1
      Left            =   1620
      TabIndex        =   11
      Top             =   7920
      Width           =   1815
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Ver Rec. &Previstos"
      Height          =   375
      Index           =   0
      Left            =   60
      TabIndex        =   10
      Top             =   7920
      Width           =   1515
   End
   Begin VB.Frame Frame3 
      Caption         =   "Intervenciones Previstas"
      ForeColor       =   &H00800000&
      Height          =   3810
      Left            =   60
      TabIndex        =   5
      Top             =   1080
      Width           =   11655
      Begin VB.CommandButton cmdRecitar 
         Caption         =   "Reci&tar"
         Height          =   375
         Left            =   10440
         TabIndex        =   29
         Top             =   1560
         Width           =   1095
      End
      Begin VB.CommandButton cmdPendienteCita 
         Caption         =   "Pen&d. Cita"
         Height          =   375
         Left            =   10440
         TabIndex        =   23
         Top             =   2040
         Width           =   1095
      End
      Begin VB.CommandButton cmdQuirOrd 
         Caption         =   "&Quir./Ord."
         Height          =   375
         Left            =   10440
         TabIndex        =   22
         Top             =   1080
         Width           =   1095
      End
      Begin VB.CommandButton cmdAnularPrevistas 
         Caption         =   "&Anular"
         Height          =   375
         Left            =   10440
         TabIndex        =   9
         Top             =   2520
         Width           =   1095
      End
      Begin VB.CommandButton cmdRecibir 
         Caption         =   "&Recibir"
         Height          =   375
         Left            =   10440
         TabIndex        =   7
         Top             =   600
         Width           =   1095
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdPrevistas 
         Height          =   3135
         Left            =   120
         TabIndex        =   6
         Top             =   600
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   5530
         _StockProps     =   79
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
         Height          =   285
         Left            =   3000
         TabIndex        =   15
         Tag             =   "Fecha Desde"
         ToolTipText     =   "Fecha Desde"
         Top             =   240
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
         Height          =   285
         Left            =   5340
         TabIndex        =   17
         Tag             =   "Fecha Hasta"
         ToolTipText     =   "Fecha Hasta"
         Top             =   240
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   1
         Left            =   4860
         TabIndex        =   18
         Top             =   300
         Width           =   465
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   7
         Left            =   2460
         TabIndex        =   16
         Top             =   300
         Width           =   510
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Intervenciones Pendientes"
      ForeColor       =   &H00800000&
      Height          =   2910
      Left            =   60
      TabIndex        =   3
      Top             =   4920
      Width           =   11655
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver Prev."
         Height          =   375
         Left            =   10440
         TabIndex        =   14
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdAnularPend 
         Caption         =   "A&nular"
         Height          =   375
         Left            =   10440
         TabIndex        =   8
         Top             =   720
         Width           =   1095
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdPendientes 
         Height          =   2595
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   4577
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10680
      TabIndex        =   2
      Top             =   660
      Width           =   1035
   End
   Begin VB.Frame Frame1 
      Caption         =   "Recursos"
      ForeColor       =   &H00800000&
      Height          =   1035
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   2895
      Begin ComctlLib.ListView lvwRec 
         Height          =   795
         Left            =   120
         TabIndex        =   1
         Top             =   180
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   1402
         View            =   3
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin vsViewLib.vsPrinter vp 
      Height          =   375
      Left            =   9840
      TabIndex        =   24
      Top             =   -60
      Visible         =   0   'False
      Width           =   1275
      _Version        =   196608
      _ExtentX        =   2249
      _ExtentY        =   661
      _StockProps     =   229
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1413783674
      PageBorder      =   0
      TableBorder     =   0
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
End
Attribute VB_Name = "frmIntervencionesPendientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intDptoSel%, strRecSel$
Dim strMeCaption$

Private Sub cmdAnularPrevistas_Click()
    Call pAnularAct(ssgrdPrevistas)
End Sub

Private Sub cmdAnularPend_Click()
    Call pAnularAct(ssgrdPendientes)
End Sub

Private Sub cmdPendienteCita_Click()
    Call pVolverACitas
End Sub

Private Sub cmdQuirOrd_Click()
    Dim i%, sql$, qry As rdoQuery, rs As rdoResultset
    Dim strQuirOrdIni$, strQuirOrdFin$, pos%
    
    'se comprueba que se ha seleccionado alguna actuaci�n
    If ssgrdPrevistas.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    Call pSeleccionarConjunto(ssgrdPrevistas)
        
        
    strQuirOrdIni = ssgrdPrevistas.Columns("Quir.").CellText(ssgrdPrevistas.SelBookmarks(0)) _
    & "|" & ssgrdPrevistas.Columns("Ord.").CellText(ssgrdPrevistas.SelBookmarks(0))
    Call objPipe.PipeSet("PR_QuirOrd", strQuirOrdIni)
    frmOrdenQuirofano.Show vbModal
    Set frmOrdenQuirofano = Nothing
    If objPipe.PipeExist("PR_QuirOrd") Then
        strQuirOrdFin = objPipe.PipeGet("PR_QuirOrd")
    End If
    If strQuirOrdFin <> strQuirOrdIni Then
        sql = "UPDATE PR4100"
        sql = sql & " SET PR41RESPUESTA = ?"
        sql = sql & " WHERE PR03NUMACTPEDI = "
        sql = sql & " (SELECT PR03NUMACTPEDI"
        sql = sql & " FROM PR0400"
        sql = sql & " WHERE PR04NUMACTPLAN = ?)"
        sql = sql & " AND PR40CODPREGUNTA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        
        LockWindowUpdate ssgrdPrevistas.hWnd
        awk1 = strQuirOrdFin
        awk1.FS = "|"
        For i = 0 To ssgrdPrevistas.SelBookmarks.Count - 1
            pos = ssgrdPrevistas.AddItemRowIndex(ssgrdPrevistas.SelBookmarks(i))
            ssgrdPrevistas.MoveFirst
            ssgrdPrevistas.MoveRecords ssgrdPrevistas.AddItemRowIndex(ssgrdPrevistas.SelBookmarks(i))
            ssgrdPrevistas.Columns("Quir.").Text = awk1.F(1)
            ssgrdPrevistas.Columns("Ord.").Text = awk1.F(2)
            
            qry(0) = awk1.F(1)
            qry(1) = ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i))
            qry(2) = constPREG_QUIROFANO
            qry.Execute
            qry(0) = awk1.F(2)
            qry(1) = ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i))
            qry(2) = constPREG_ORDEN
            qry.Execute
        Next i
        ssgrdPrevistas.Update
        ssgrdPrevistas.MoveFirst
        ssgrdPrevistas.MoveRecords pos - 1
        LockWindowUpdate 0&
        DoEvents
    End If
    ssgrdPrevistas.SetFocus
End Sub

Private Sub cmdRecitar_Click()
    Call pCitar
End Sub

Private Sub dcboDesde_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboHasta.Date = dcboDesde.Date
    End If
End Sub

Private Sub dcboHasta_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboDesde.Date = dcboHasta.Date
    End If
End Sub

Private Sub Form_Load()
    strMeCaption = "Recibir al paciente: Intervenciones Pendientes"
    
    intDptoSel = constDPTO_QUIROFANO
     
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ColumnHeaders.Add , , , 2000
    Call pCargarRecursos(intDptoSel)
    
    'se formatean los grids
    Call pFormatearGrids
End Sub

Private Sub pCargarRecursos(intDptoSel%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim item As ListItem
    
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ListItems.Clear
    Set item = lvwRec.ListItems.Add(, , "TODOS")
    item.Tag = 0
    item.Selected = True

    sql = "SELECT AG11CODRECURSO, AG11DESRECURSO"
    sql = sql & " FROM AG1100"
    sql = sql & " WHERE AD02CODDPTO = ?"
    sql = sql & " AND SYSDATE BETWEEN AG11FECINIVREC AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        Set item = lvwRec.ListItems.Add(, , rs!AG11DESRECURSO)
        item.Tag = rs!AG11CODRECURSO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub
Private Sub cmdConsultar_Click()
    Call pConsultar
End Sub

Private Sub cmdRecibir_Click()
  Call pRecibirCamasQuir
  
'  Call pRecibir
End Sub

Private Sub cmdVolver_Click()
    Call pVolverRecibir
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub pRecibir()
Dim sql$, qry As rdoQuery, i%
Dim strAhora$, lngNumActPedi&
Dim nPrs As String ' String con los n� de actuaci�n planificadas
Dim res As Integer '0: Error, 1: Va a preanestesia, 2: Va a quir�fano

    'se comprueba que se ha seleccionado alguna actuaci�n de las en espera
    If ssgrdPrevistas.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    Call pSeleccionarConjunto(ssgrdPrevistas)
    
    'se comprueba si tiene el proceso y la asistencia
    nPrs = "("
    For i = 0 To ssgrdPrevistas.SelBookmarks.Count - 1
        If ssgrdPrevistas.Columns("Proceso").CellText(ssgrdPrevistas.SelBookmarks(i)) = "" _
        Or ssgrdPrevistas.Columns("Asistencia").CellText(ssgrdPrevistas.SelBookmarks(i)) = "" Then
            sql = "Alguna de las intervenciones seleccionadas no tiene asociado el proceso/asistencia."
            MsgBox sql, vbExclamation, strMeCaption
            Exit Sub
        Else
          nPrs = nPrs & ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i)) & ", "
        End If
    Next i
    nPrs = Left$(nPrs, Len(nPrs) - 2) & ")"
    
    strAhora = strFechaHora_Sistema

    sql = "UPDATE PR0400 SET PR04FECENTRCOLA = TO_DATE(?,'DD/MM/YYYY HH24:MI') "
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(strAhora, "dd/mm/yyyy hh:mm")
    
        For i = 0 To ssgrdPrevistas.SelBookmarks.Count - 1
            'se anota la fecha-hora de entrada en cola en la base de datos
            qry(1) = ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i))
            qry.Execute

            'se anota la fecha-hora de entrada en cola de las actuaciones asociadas
            lngNumActPedi = ssgrdPrevistas.Columns("NAPedi").CellText(ssgrdPrevistas.SelBookmarks(i))
            Call pRecibirAsociadas(lngNumActPedi, strAhora)
            'se a�ade la actuaci�n al grid de actuaciones pendientes
            ssgrdPendientes.AddItem ssgrdPrevistas.Columns("Fecha Prog.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Quir.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Ord.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("T(min)").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Anest.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("N� Hist.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Paciente").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("T.E.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Edad").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Intervenci�n").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & Format(strAhora, "dd/mm/yy hh:mm") & Chr$(9) _
                    & ssgrdPrevistas.Columns("Dpto. Sol.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Dr. Sol.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Cama").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Cod. Persona").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Proceso").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("Asistencia").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPrevistas.Columns("NAPedi").CellText(ssgrdPrevistas.SelBookmarks(i)), 0
        Next i
        qry.Close
        'se quitan las actuaciones del grid de las actuaciones previstas
        ssgrdPrevistas.DeleteSelected
End Sub

Private Sub pRecibirCamasQuir()
Dim sql$, qry As rdoQuery, i%
Dim strAhora$, lngNumActPedi&
Dim nPrs As String ' String con los n� de actuaci�n planificadas
Dim res As Integer '0: Error, 1: Va a preanestesia, 2: Va a quir�fano

    'se comprueba que se ha seleccionado alguna actuaci�n de las en espera
    If ssgrdPrevistas.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    Call pSeleccionarConjunto(ssgrdPrevistas)
    
    'se comprueba si tiene el proceso y la asistencia
    nPrs = "("
    For i = 0 To ssgrdPrevistas.SelBookmarks.Count - 1
        If ssgrdPrevistas.Columns("Proceso").CellText(ssgrdPrevistas.SelBookmarks(i)) = "" _
        Or ssgrdPrevistas.Columns("Asistencia").CellText(ssgrdPrevistas.SelBookmarks(i)) = "" Then
            sql = "Alguna de las intervenciones seleccionadas no tiene asociado el proceso/asistencia."
            MsgBox sql, vbExclamation, strMeCaption
            Exit Sub
        Else
          nPrs = nPrs & ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i)) & ", "
        End If
    Next i
    nPrs = Left$(nPrs, Len(nPrs) - 2) & ")"
    
' Alberto: Camas de quir�fano
    res = IndicarCama(CLng(ssgrdPrevistas.Columns("Proceso").CellText(ssgrdPrevistas.SelBookmarks(0))), _
          CLng(ssgrdPrevistas.Columns("Asistencia").CellText(ssgrdPrevistas.SelBookmarks(0))), _
          CLng(ssgrdPrevistas.Columns("N� Hist.").CellText(ssgrdPrevistas.SelBookmarks(0))), _
          ssgrdPrevistas.Columns("Paciente").CellText(ssgrdPrevistas.SelBookmarks(i)), _
          ssgrdPrevistas.Columns("Quir.").CellText(ssgrdPrevistas.SelBookmarks(i)), _
          ssgrdPrevistas.Columns("Cama").CellText(ssgrdPrevistas.SelBookmarks(i)), nPrs)
    
    Select Case res
      Case 0 ' Error. No se recibe al paciente

      Case 1, 3 ' El paciente va a preanestesia. Se indica fecha de entrada en cola
        'se a�ade la actuaci�n al grid de actuaciones pendientes
        For i = 0 To ssgrdPrevistas.SelBookmarks.Count - 1
          ssgrdPendientes.AddItem ssgrdPrevistas.Columns("Fecha Prog.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Quir.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Ord.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("T(min)").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Anest.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("N� Hist.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Paciente").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("T.E.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Edad").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Intervenci�n").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & Format(strAhora, "dd/mm/yy hh:mm") & Chr$(9) _
                  & ssgrdPrevistas.Columns("Dpto. Sol.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Dr. Sol.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Cama").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Cod. Persona").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Proceso").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("Asistencia").CellText(ssgrdPrevistas.SelBookmarks(i)) & Chr$(9) _
                  & ssgrdPrevistas.Columns("NAPedi").CellText(ssgrdPrevistas.SelBookmarks(i)), 0
        Next i
        'se quitan las actuaciones del grid de las actuaciones previstas
        ssgrdPrevistas.DeleteSelected
      
      Case 2 ' El paciente va a quir�fano. Se inician las intervenciones
        'se quitan las actuaciones del grid de las actuaciones previstas
        ssgrdPrevistas.DeleteSelected
    End Select

End Sub



Function IndicarCama(nProc As Long, nAsist As Long, NH As Long, Pac As String, Quir As String, Cama As String, nPrs As String) As Integer

  Load frmCamasQuir
  With frmCamasQuir
    .txtNH.Text = NH
    .txtPac.Text = Pac
    .txtQuir.Text = Quir
    .seguir = False
    .nProc = nProc
    .nAsist = nAsist
    .camaIni = Cama
    .nPrs = nPrs
    .Show vbModal
    IndicarCama = .seguir
    Unload frmCamasQuir
  End With
End Function

Private Sub pRecibirAsociadas(lngNumActPedi&, strFecEntrCola$)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    sql = "UPDATE PR0400 SET PR04FECENTRCOLA = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " WHERE PR03NUMACTPEDI IN"
    sql = sql & " (SELECT PR03NUMACTPEDI"
    sql = sql & " FROM PR6100"
    sql = sql & " WHERE PR03NUMACTPEDI_ASO = ?)"
    sql = sql & " AND PR04FECENTRCOLA IS NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(strFecEntrCola, "dd/mm/yyyy hh:mm")
    qry(1) = lngNumActPedi
    qry.Execute
End Sub

Private Sub pVolverRecibirAsociadas(lngNumActPedi&)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    sql = "UPDATE PR0400 SET PR04FECENTRCOLA = NULL"
    sql = sql & " WHERE PR03NUMACTPEDI IN"
    sql = sql & " (SELECT PR03NUMACTPEDI"
    sql = sql & " FROM PR6100"
    sql = sql & " WHERE PR03NUMACTPEDI_ASO = ?)"
    sql = sql & " AND PR04FECENTRCOLA IS NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNumActPedi
    qry.Execute
End Sub

Private Sub pFormatearGrids()
    With ssgrdPrevistas
        .StyleSets.Add ("blocked")
        .StyleSets("blocked").BackColor = objApp.objUserColor.lngReadOnly
        
        .Columns(0).Caption = "Fecha Prog." 'Sin hora
        .Columns(0).Width = 1000
        .Columns(0).StyleSet = "blocked"
        .Columns(0).Locked = True
        .Columns(1).Caption = "Quir."
        .Columns(1).Width = 500
        .Columns(1).StyleSet = "blocked"
        .Columns(1).Locked = True
        .Columns(2).Caption = "Ord."
        .Columns(2).Width = 500
        .Columns(3).Caption = "T(min)"
        .Columns(3).Width = 600
        .Columns(3).StyleSet = "blocked"
        .Columns(3).Locked = True
        .Columns(4).Caption = "Anest."
        .Columns(4).Width = 600
        .Columns(4).StyleSet = "blocked"
        .Columns(4).Locked = True
        .Columns(5).Caption = "N� Hist."
        .Columns(5).Alignment = ssCaptionAlignmentRight
        .Columns(5).Width = 700
        .Columns(5).StyleSet = "blocked"
        .Columns(5).Locked = True
        .Columns(6).Caption = "Paciente"
        .Columns(6).Width = 3100
        .Columns(6).StyleSet = "blocked"
        .Columns(6).Locked = True
        .Columns(7).Caption = "T.E."
        .Columns(7).Width = 400
        .Columns(7).Alignment = ssCaptionAlignmentCenter
        .Columns(7).StyleSet = "blocked"
        .Columns(7).Locked = True
        .Columns(8).Caption = "Edad"
        .Columns(8).Width = 600
        .Columns(8).Alignment = ssCaptionAlignmentCenter
        .Columns(8).StyleSet = "blocked"
        .Columns(8).Locked = True
        .Columns(9).Caption = "Intervenci�n"
        .Columns(9).Width = 2400
        .Columns(9).StyleSet = "blocked"
        .Columns(9).Locked = True
        .Columns(10).Caption = "Dpto. Sol."
        .Columns(10).Width = 1500
        .Columns(10).StyleSet = "blocked"
        .Columns(10).Locked = True
        .Columns(11).Caption = "Dr. Sol."
        .Columns(11).Width = 1800
        .Columns(11).StyleSet = "blocked"
        .Columns(11).Locked = True
        .Columns(12).Caption = "Cama"
        .Columns(12).Width = 600
        .Columns(12).StyleSet = "blocked"
        .Columns(12).Locked = True
        .Columns(13).Caption = "Proceso"
        .Columns(13).Width = 1000
        .Columns(13).StyleSet = "blocked"
        .Columns(13).Locked = True
        .Columns(14).Caption = "Asistencia"
        .Columns(14).Width = 1000
        .Columns(14).StyleSet = "blocked"
        .Columns(14).Locked = True
        .Columns(15).Caption = "Num. Act."
        .Columns(15).Width = 0
        .Columns(15).Visible = False
        .Columns(16).Caption = "Cod. Persona"
        .Columns(16).Width = 0
        .Columns(16).Visible = False
        .Columns(17).Caption = "NAPedi"
        .Columns(17).Width = 0
        .Columns(17).Visible = False
'        .BackColorEven = objApp.objUserColor.lngReadOnly
'        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    With ssgrdPendientes
        .Columns(0).Caption = "Fecha Prog." 'Sin hora
        .Columns(0).Width = 1000
        .Columns(1).Caption = "Quir."
        .Columns(1).Width = 500
        .Columns(2).Caption = "Ord."
        .Columns(2).Width = 500
        .Columns(3).Caption = "T(min)"
        .Columns(3).Width = 600
        .Columns(4).Caption = "Anest."
        .Columns(4).Width = 600
        .Columns(5).Caption = "N� Hist."
        .Columns(5).Alignment = ssCaptionAlignmentRight
        .Columns(5).Width = 700
        .Columns(6).Caption = "Paciente"
        .Columns(6).Width = 3100
        .Columns(7).Caption = "T.E."
        .Columns(7).Width = 400
        .Columns(7).Alignment = ssCaptionAlignmentCenter
        .Columns(8).Caption = "Edad"
        .Columns(8).Width = 600
        .Columns(8).Alignment = ssCaptionAlignmentCenter
        .Columns(9).Caption = "Intervenci�n"
        .Columns(9).Width = 2400
        .Columns(10).Caption = "Entrada cola"
        .Columns(10).Width = 1300
        .Columns(11).Caption = "Dpto. Sol."
        .Columns(11).Width = 1500
        .Columns(12).Caption = "Dr. Sol."
        .Columns(12).Width = 1800
        .Columns(13).Caption = "Cama"
        .Columns(13).Width = 600
        .Columns(14).Caption = "Num. Act."
        .Columns(14).Width = 0
        .Columns(14).Visible = False
        .Columns(15).Caption = "Cod. Persona"
        .Columns(15).Width = 0
        .Columns(15).Visible = False
        .Columns(16).Caption = "Proceso"
        .Columns(16).Width = 0
        .Columns(16).Visible = False
        .Columns(17).Caption = "Asistencia"
        .Columns(17).Width = 0
        .Columns(17).Visible = False
        .Columns(18).Caption = "NAPedi"
        .Columns(18).Width = 0
        .Columns(18).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub lvwRec_Click()
    Dim i%, msg$
    
    ssgrdPrevistas.RemoveAll
    ssgrdPendientes.RemoveAll
 
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    If lvwRec.ListItems(1).Selected Then
        For i = 2 To lvwRec.ListItems.Count
            lvwRec.ListItems(i).Selected = False
        Next i
    End If
    'Se muestra en el Caption los recursos seleccionados
    Call pCaption
End Sub

Private Sub ssgrdPendientes_Click()
    ssgrdPrevistas.SelBookmarks.RemoveAll
    
    If ssgrdPendientes.SelBookmarks.Count > 1 Then
        If ssgrdPendientes.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(0)) <> ssgrdPendientes.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdPendientes.SelBookmarks.Remove ssgrdPendientes.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdPrevistas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdPrevistas_Click()
    ssgrdPendientes.SelBookmarks.RemoveAll
    
    If ssgrdPrevistas.SelBookmarks.Count > 1 Then
        If ssgrdPrevistas.Columns("N� Hist.").CellText(ssgrdPrevistas.SelBookmarks(0)) <> ssgrdPrevistas.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdPrevistas.SelBookmarks.Remove ssgrdPrevistas.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdPrevistas_HeadClick(ByVal ColIndex As Integer)
    If ssgrdPrevistas.Rows > 0 Then Call pCargarActPrevistas(ColIndex)
End Sub

Private Sub ssgrdPendientes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdPendientes_HeadClick(ByVal ColIndex As Integer)
    If ssgrdPendientes.Rows > 0 Then Call pCargarActPendientes(ColIndex)
End Sub

Private Sub pVolverRecibir()
    Dim sql$, qry As rdoQuery, i%
    Dim lngNumActPedi&
    
    'se comprueba que se ha seleccionado alguna actuaci�n
    If ssgrdPendientes.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    Call pSeleccionarConjunto(ssgrdPendientes)
    
    sql = "UPDATE PR0400 SET PR04FECENTRCOLA = NULL"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    objApp.BeginTrans
    For i = 0 To ssgrdPendientes.SelBookmarks.Count - 1
        'se pone a NULL la fecha-hora de entrada en cola
        qry(0) = ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i))
        qry.Execute
        
        'se pone a NULL la fecha-hora de entrada en cola de las actuaciones asociadas
        lngNumActPedi = ssgrdPendientes.Columns("NAPedi").CellText(ssgrdPendientes.SelBookmarks(i))
        Call pVolverRecibirAsociadas(lngNumActPedi)
        
        'se a�ade la actuaci�n al grid de actuaciones previstas
        ssgrdPrevistas.AddItem ssgrdPendientes.Columns("Fecha Prog.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Quir.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Ord.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("T(min)").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Anest.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Paciente").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("T.E.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Edad").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Intervenci�n").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Dpto. Sol.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Dr. Sol.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Cama").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Proceso").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Asistencia").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("Cod. Persona").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                & ssgrdPendientes.Columns("NAPedi").CellText(ssgrdPendientes.SelBookmarks(i)), 0
    Next i
    qry.Close
    
    objApp.CommitTrans
    
    'se quitan las actuaciones del grid de las actuaciones pendientes
    'Alberto: Sacar al paciente de quir�fano
    If SacarDeQuirofano(CLng(ssgrdPendientes.Columns("Asistencia").CellText(ssgrdPendientes.SelBookmarks(0)))) = True Then
      objApp.CommitTrans
    Else
      objApp.RollbackTrans
    End If
    ssgrdPendientes.DeleteSelected

End Sub

Function SacarDeQuirofano(nAsist As Long) As Integer
Dim sql As String, cCama As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  SacarDeQuirofano = True
  sql = "SELECT AD15CodCama, AD02CodDpto FROM AD1500 WHERE " _
      & "AD01CodAsistenci = ? AND " _
      & "AD02CodDpto = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nAsist
  rdoQ(1) = constDPTO_QUIROFANO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then ' Se libera la cama de quir�afano
    cCama = rdo(0)
      
    sql = "UPDATE AD1500 SET AD14CODESTCAMA = ?, AD01CODASISTENCI = NULL, " _
        & "AD07CODPROCESO = NULL " _
        & "WHERE AD15CODCAMA = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = constCAMALIBRE
    rdoQ(1) = cCama
    rdoQ.Execute
    If rdoQ.RowsAffected = 0 Then
      SacarDeQuirofano = False
      Exit Function
    End If
  ' Se indica que la cama deja de estar ocupada (hist�rico)
    sql = "UPDATE AD1600 SET AD16FECFIN = SYSDATE WHERE " _
        & "AD15CODCAMA = ? AND " _
        & "AD14CODESTCAMA = ? AND " _
        & "AD16FECFIN IS NULL"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = cCama
    rdoQ(1) = constCAMAOCUPADA
    rdoQ.Execute
    If Err <> 0 Then
      SacarDeQuirofano = False
      Exit Function
    End If
  
  ' Se indica que la cama pasa a estar libre (hist�rico)
    sql = "INSERT INTO AD1600 (AD15CODCAMA, AD16FECCAMBIO, AD14CODESTCAMA) " _
        & "VALUES (?, SYSDATE, ?)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = cCama
    rdoQ(1) = constCAMALIBRE
    rdoQ.Execute
    If rdoQ.RowsAffected = 0 Then
      SacarDeQuirofano = False
      Exit Function
    End If
  End If
End Function
Private Sub pAnularAct(ssgrd As SSDBGrid)
'**************************************************************************************
'*  Anula una actuaci�n planificada cambiando su estado a anulada
'**************************************************************************************
    Dim strCancel$, sql$, qry As rdoQuery, i%, lngNumActPlan&
    
    Select Case ssgrd.SelBookmarks.Count
    Case 0
        sql = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    Case 1
        sql = "Si Ud. anula la intervenci�n ya no estar� disponible para ser realizada." & Chr$(13)
        sql = sql & "Indique el motivo por el que se va a anular la intervenci�n:"
    Case Is > 1
        sql = "Si Ud. anula las intervenciones ya no estar�n disponibles para ser realizadas." & Chr$(13)
        sql = sql & "Indique el motivo por el que se van a anular las intervenciones:"
    End Select
    strCancel = Trim$(InputBox(sql, "Anular Intervenci�n"))
    If strCancel = "" Then
        sql = "La intervenci�n NO ha sido anulada: hace falta indicar un motivo."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    For i = 0 To ssgrd.SelBookmarks.Count - 1
        lngNumActPlan = ssgrd.Columns("Num. Act.").CellText(ssgrd.SelBookmarks(i))
        
        'se anula la actuaci�n
        sql = "UPDATE PR0400 SET PR04DESMOTCAN = ?,"
        sql = sql & " PR37CODESTADO = ?,"
        sql = sql & " PR04FECCANCEL = SYSDATE,"
        sql = sql & " PR04FECENTRCOLA = ?,"
        sql = sql & " PR04FECINIACT = ?,"
        'En principio siempre PR04FECFINACT = NULL ya que nunca estar� terminada, pero por si acaso...
        sql = sql & " PR04FECFINACT = ?"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        If Len(strCancel) > 50 Then qry(0) = Left$(strCancel, 50) Else qry(0) = strCancel
        qry(1) = constESTACT_CANCELADA
        qry(2) = Null
        qry(3) = Null
        qry(4) = Null
        qry(5) = lngNumActPlan
        qry.Execute
        qry.Close
        
        'se anula la cita
        sql = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_ANULADA & "'"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        sql = sql & " AND CI01SITCITA IN ('" & constESTCITA_CITADA & "','" & constESTCITA_PENDRECITAR & "')"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        qry.Execute
        qry.Close
        
        'Se anulan las actuaciones asociadas
        Call pAnularAsociadas(lngNumActPlan)
    Next i
    
    'se elimina la actuaci�n del Grid
    Select Case ssgrd.Name
    Case ssgrdPrevistas.Name: Call pCargarActPrevistas(-1)
    Case ssgrdPendientes.Name: Call pCargarActPendientes(-1)
    End Select
''    ssgrd.DeleteSelected
End Sub

Private Sub cmdComun_Click(Index As Integer)
    Dim ssgrid As SSDBGrid, lngNumActPlan&
    
    cmdComun(Index).Enabled = False
    If Index <> 3 Then
        If (Index = 4 And (optImprimir(0).Value = False And optImprimir(4).Value = False)) Or Index <> 4 Then
            If ssgrdPrevistas.SelBookmarks.Count > 0 Then
                Set ssgrid = ssgrdPrevistas
            ElseIf ssgrdPendientes.SelBookmarks.Count > 0 Then
                Set ssgrid = ssgrdPendientes
            Else
                MsgBox "No se ha seleccionado ninguna Intervenci�n.", vbExclamation, strMeCaption
                cmdComun(Index).Enabled = True
                Exit Sub
            End If
        
            If Index = 0 Or Index = 1 Then
                If ssgrid.SelBookmarks.Count = 1 Then
                    lngNumActPlan = ssgrid.Columns("Num. Act.").CellText(ssgrid.SelBookmarks(0))
                Else
                    MsgBox "Debe Ud. seleccionar una �nica Intervenci�n.", vbExclamation, strMeCaption
                    cmdComun(Index).Enabled = True
                    Exit Sub
                End If
            End If
        End If
    End If
    Select Case Index
    Case 0 'Ver recursos previstos
        Call pVerRecursos(lngNumActPlan)
    Case 1 'Ver datos / cuestionario
        Call pVerDatosActuacion(lngNumActPlan)
    Case 2 'Acceso a visi�n global
        Call pVisionGlobal(ssgrid.Columns("Cod. Persona").CellText(ssgrid.SelBookmarks(0)))
    Case 3 'Acceso a realizaci�n de intervenciones
        Call pRealizacionInterv
    Case 4 'Impresos
        Call pImprimir(ssgrid)
    End Select
    cmdComun(Index).Enabled = True
End Sub

Private Sub pVerRecursos(lngNumActPlan&)
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    frmConsultaRecursos.Show vbModal
    Set frmConsultaRecursos = Nothing
End Sub

Private Sub pVerDatosActuacion(lngNumActPlan&, Optional blnMostrarSoloSiHayDatos As Boolean)
'**************************************************************************************
'*  Llama a la pantalla que muestra los datos (observ., indic., intruccines...) de la
'*  actuaci�n y los cuestionarios (cuestionarios + restricciones)
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = True significa que s�lo hay que mostrar la
'*  pantalla en el caso de que exista alguna observ., indic. o pregunta en el cuestionario.
'*  En este caso se hace previamente el Load de la pantalla, la cual devolver� en un Pipe
'*  la informaci�n necesaria para saber si hay que hacer el Show o el Unload
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = False se muestra la pantalla directamente
'**************************************************************************************

    'se establece el Pipe con el n� de actuaci�n planificada
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    
    'se mira la opci�n seleccionada: cargar pantalla previamente o mostrar directamente
    If blnMostrarSoloSiHayDatos Then 'cargar pantalla previamente
        Call objPipe.PipeSet("PR_VerSoloSiHayDatos", True)
        'se elimina el posible Pipe devuelto por frmObservAct en operaciones previas
        If objPipe.PipeExist("PR_frmObservAct") Then Call objPipe.PipeRemove("PR_frmObservAct")
        'se carga en memoria la pantalla
        Load frmObservAct
        'se mira si hay que mostrarla o descargarla
        If objPipe.PipeExist("PR_frmObservAct") Then
            If objPipe.PipeGet("PR_frmObservAct") = True Then
                frmObservAct.Show vbModal
            Else
                Unload frmObservAct
            End If
            Call objPipe.PipeRemove("PR_frmObservAct")
        Else
            Unload frmObservAct
        End If
        Call objPipe.PipeRemove("PR_VerSoloSiHayDatos")
    Else 'mostrar pantalla directamente
        frmObservAct.Show vbModal
        Set frmObservAct = Nothing
    End If
End Sub

Private Sub pVisionGlobal(lngCodPersona As Long)
'    Dim vntdata(1) As Variant
'    vntdata(1) = lngCodPersona
'    Call objsecurity.LaunchProcess("AD1126", vntdata)

' Visi�n Global de Alberto
  ReDim arData(1 To 2) As Variant
  arData(1) = lngCodPersona
  arData(2) = 2
  Call objsecurity.LaunchProcess("HC02", arData())
  

End Sub

Private Sub pRealizacionInterv()
    Call objsecurity.LaunchProcess("PR0560")
    Call pCargarActPendientes(-1) 'no hace falta actualizar las Previstas
End Sub

Private Sub pImprimir(Grid1 As SSDBGrid)
    Dim i%, strWhere$
    
    If optImprimir(0).Value = True Then 'listado quir�fanos 1-8
        Screen.MousePointer = vbHourglass
        Call pImprimirHoja(0)
        Screen.MousePointer = vbDefault
'        frmHojaQuirofano.Show vbModal
'        Set frmHojaQuirofano = Nothing
    ElseIf optImprimir(4).Value = True Then 'listado quir�fanos 10 y 11
        Screen.MousePointer = vbHourglass
        Call pImprimirHoja(4)
        Screen.MousePointer = vbDefault
'        frmHojaQuirofano.Show vbModal
'        Set frmHojaQuirofano = Nothing
    ElseIf optImprimir(1).Value = True Then 'programa del paciente
        crtReport1.ReportFileName = objApp.strReportsPath & "propac1.rpt"
        strWhere = "{PR0460J.CI21CODPERSONA}= " & Grid1.Columns("Cod. Persona").CellText(Grid1.SelBookmarks(0))
        With crtReport1
            .PrinterCopies = 1
            .Destination = crptToPrinter
            .SelectionFormula = "(" & strWhere & ")"
            .Connect = objApp.rdoConnect.Connect
            .DiscardSavedData = True
            Screen.MousePointer = vbHourglass
            .Action = 1
            Screen.MousePointer = vbDefault
        End With
    ElseIf optImprimir(2).Value = True Then 'etiquetas
        Call pImprimirEtiquetas(Grid1.Columns("Cod. Persona").CellText(Grid1.SelBookmarks(0)), _
                                Grid1.Columns("Proceso").CellText(Grid1.SelBookmarks(0)), _
                                Grid1.Columns("Asistencia").CellText(Grid1.SelBookmarks(0)))
    ElseIf optImprimir(3).Value = True Then
      Dim vntdata(1) As Variant
      Dim sql As String
      For i = 0 To Grid1.SelBookmarks.Count - 1
        sql = sql & "PR0400.PR04NUMACTPLAN= " & Grid1.Columns("Num. Act.").CellText(Grid1.SelBookmarks(i)) & " OR "
      Next i
      sql = Left$(sql, Len(sql) - 4)
      vntdata(1) = sql
      Call objsecurity.LaunchProcess("PR2400", vntdata(1))

'        If Grid1.SelBookmarks.Count = 1 Then
'            crtReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
'        Else
'            crtReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
'        End If
'        strWhere = ""
'        For i = 0 To Grid1.SelBookmarks.Count - 1
'            strWhere = strWhere & "{PR0457J.PR04NUMACTPLAN}= " & Grid1.Columns("Num. Act.").CellText(Grid1.SelBookmarks(i)) & " OR "
'        Next i
'        strWhere = Left$(strWhere, Len(strWhere) - 4)
'        With crtReport1
'            .PrinterCopies = 1
'            .Destination = crptToWindow
'            .SelectionFormula = "(" & strWhere & ")"
'            .Connect = objApp.rdoConnect.Connect
'            .DiscardSavedData = True
'            Screen.MousePointer = vbHourglass
'            .Action = 1
'            Screen.MousePointer = vbDefault
'        End With
    End If
End Sub

Private Sub pConsultar()
    Dim i%
    strRecSel = ""
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected Then
            strRecSel = strRecSel & lvwRec.ListItems(i).Tag & ","
            If i = 1 Then Exit For 'TODOS
        End If
    Next i
    If strRecSel <> "" Then
        strRecSel = Left$(strRecSel, Len(strRecSel) - 1)
        Call pCargarActPrevistas(-1)
        Call pCargarActPendientes(-1)
    Else
        MsgBox "No se ha seleccionado ning�n recurso.", vbExclamation, strMeCaption
    End If
End Sub

Private Sub pCargarActPrevistas(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones en espera de ser recibido el paciente planificadas o citadas
'*  entre las fechas seleccionadas en funci�n del rescurso o recursos seleccionados
'*  ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Static sqlOrder$
    Dim strEdad$
    
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY DIA, PACIENTE, PR01DESCORTA" 'Opci�n por defecto: Fecha
    Else
        Select Case UCase(ssgrdPrevistas.Columns(intCol).Caption)
        Case UCase("Fecha Prog."): sqlOrder = " ORDER BY DIA, PACIENTE, PR01DESCORTA"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, DIA, PR01DESCORTA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, DIA"
        Case UCase("Intervenci�n"): sqlOrder = " ORDER BY PR01DESCORTA, DIA, PACIENTE"
        Case UCase("Dpto. Sol."): sqlOrder = " ORDER BY AD02DESDPTO, DIA, PACIENTE, PR01DESCORTA"
        Case UCase("Dr. Sol."): sqlOrder = " ORDER BY SG02TXTFIRMA, DIA, PACIENTE, PR01DESCORTA"
        Case UCase("Cama"): sqlOrder = " ORDER BY CAMA, DIA, PACIENTE, PR01DESCORTA"
        Case UCase("Ord."), UCase("Quir."): sqlOrder = " ORDER BY DIA, LPAD(QUIROFANO,2,'0'), LPAD(ORDEN,2,'0'), PACIENTE, PR01DESCORTA"
        Case UCase("T.E."): sqlOrder = " ORDER BY AD1100.CI32CODTIPECON, DIA, PACIENTE, PR01DESCORTA"
        Case Else: Exit Sub
        End Select
    End If
    
    ssgrdPrevistas.RemoveAll
    ssgrdPrevistas.ScrollBars = ssScrollBarsNone

'NOTA: TODAS LAS INTERVENCIONES SON CITABLES
    sql = "SELECT /*+ ORDERED INDEX (CI0100 CI0103) */"
    sql = sql & " CI01FECCONCERT FECHA, TRUNC(CI01FECCONCERT) DIA,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0400.PR03NUMACTPEDI, PR01DESCORTA,"
    sql = sql & " PR0400.CI21CODPERSONA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
    sql = sql & " CI2200.CI22NUMHISTORIA, (SYSDATE-NVL(CI22FECNACIM,SYSDATE))/365 EDAD,"
    sql = sql & " PR0400.AD02CODDPTO,"
    sql = sql & " PR0400.PR37CODESTADO,"
    sql = sql & " PR0400.AD07CODPROCESO, PR0400.AD01CODASISTENCI,"
    sql = sql & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & " AD02DESDPTO, SG02TXTFIRMA,"
    sql = sql & " AD1100.CI32CODTIPECON,"
    sql = sql & " CI0100.AG11CODRECURSO CODREC, LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') QUIROFANO,"
    sql = sql & " PR41DUR.PR41RESPUESTA DURACION, PR41ANE.PR41RESPUESTA ANESTESIA,"
    sql = sql & " PR41ORD.PR41RESPUESTA ORDEN"
    sql = sql & " FROM CI0100, PR0400, PR0100, CI2200, PR0300, PR0900, AD0200, SG0200,"
    sql = sql & " AD1500, AD1100, AG1100, PR4100 PR41DUR, PR4100 PR41ANE, PR4100 PR41ORD"
    sql = sql & " WHERE CI0100.CI01FECCONCERT >= TO_DATE(?,'DD/MM/YYYY')"
    sql = sql & " AND CI0100.CI01FECCONCERT < TO_DATE(?,'DD/MM/YYYY')"
    sql = sql & " AND CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
    If strRecSel = "0" Then
        sql = sql & " AND CI0100.AG11CODRECURSO IN ("
        sql = sql & " SELECT AG11CODRECURSO"
        sql = sql & " FROM AG1100"
        sql = sql & " WHERE AG14CODTIPRECU = " & constTIPORECQUIROFANO & ")"
    Else
        sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NULL"
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    sql = sql & " AND AD1500.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    sql = sql & " AND AD1500.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " AND AD1100.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    sql = sql & " AND AD1100.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    sql = sql & " AND AD1100.AD11FECFIN IS NULL"
    sql = sql & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    sql = sql & " AND PR0900.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    sql = sql & " AND AD0200.AD02CODDPTO = PR0900.AD02CODDPTO"
    sql = sql & " AND SG0200.SG02COD = PR0900.SG02COD"
    sql = sql & " AND PR41DUR.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41DUR.PR40CODPREGUNTA (+)= " & constPREG_DURACION
    sql = sql & " AND PR41ANE.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41ANE.PR40CODPREGUNTA (+)= " & constPREG_ANESTESIA
    sql = sql & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41ORD.PR40CODPREGUNTA (+)= " & constPREG_ORDEN
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.QueryTimeout = 0
    qry(0) = Format(dcboDesde.Text, "dd/mm/yyyy")
    qry(1) = Format(DateAdd("d", 1, dcboHasta.Text), "dd/mm/yyyy")
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        Select Case Val(rs!EDAD)
        Case 0: strEdad = "?"
        Case Is < 1: strEdad = "<1"
        Case Else: strEdad = Val(rs!EDAD)
        End Select
        ssgrdPrevistas.AddItem Format(rs!dia, "dd/mm/yy") & Chr$(9) _
                        & rs!QUIROFANO & Chr$(9) _
                        & rs!orden & Chr$(9) _
                        & rs!DURACION & Chr$(9) _
                        & rs!ANESTESIA & Chr$(9) _
                        & rs!CI22NUMHISTORIA & Chr$(9) _
                        & rs!paciente & Chr$(9) _
                        & rs!CI32CODTIPECON & Chr$(9) _
                        & strEdad & Chr$(9) _
                        & rs!PR01DESCORTA & Chr$(9) _
                        & rs!AD02DESDPTO & Chr$(9) _
                        & rs!SG02TXTFIRMA & Chr$(9) _
                        & rs!Cama & Chr$(9) _
                        & rs!AD07CODPROCESO & Chr$(9) _
                        & rs!AD01CODASISTENCI & Chr$(9) _
                        & rs!PR04NUMACTPLAN & Chr$(9) _
                        & rs!CI21CODPERSONA & Chr$(9) _
                        & rs!PR03NUMACTPEDI
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdPrevistas.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActPendientes(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones pendientes hasta hoy inclusive en funci�n del rescurso
'*  o recursos seleccionados ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Static sqlOrder$
    Dim strEdad$
    
    'para que no de error si se ha salido de la pantalla de Realizaci�n de Intervenciones
    'y no hab�a seleccionado ning�n recurso anteriormente
    If strRecSel = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY DIA, PACIENTE, PR01DESCORTA" 'Opci�n por defecto: Fecha
    Else
        Select Case UCase(ssgrdPendientes.Columns(intCol).Caption)
        Case UCase("Fecha Prog."): sqlOrder = " ORDER BY DIA, PACIENTE, PR01DESCORTA"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, DIA, PR01DESCORTA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, DIA"
        Case UCase("Intervenci�n"): sqlOrder = " ORDER BY PR01DESCORTA, DIA, PACIENTE"
        Case UCase("Dpto. Sol."): sqlOrder = " ORDER BY AD02DESDPTO, DIA, PACIENTE, PR01DESCORTA"
        Case UCase("Dr. Sol."): sqlOrder = " ORDER BY SG02TXTFIRMA, DIA, PACIENTE, PR01DESCORTA"
        Case UCase("Cama"): sqlOrder = " ORDER BY CAMA, DIA, PACIENTE, PR01DESCORTA"
        Case UCase("Ord."), UCase("Quir."): sqlOrder = " ORDER BY DIA, LPAD(QUIROFANO,2,'0'), LPAD(ORDEN,2,'0'), PACIENTE, PR01DESCORTA"
        Case UCase("Entrada cola"): sqlOrder = " ORDER BY PR04FECENTRCOLA, PACIENTE, PR01DESCORTA"
        Case UCase("T.E."): sqlOrder = " ORDER BY AD1100.CI32CODTIPECON, DIA, PACIENTE, PR01DESCORTA"
        Case Else: Exit Sub
        End Select
    End If
    
    ssgrdPendientes.RemoveAll
    ssgrdPendientes.ScrollBars = ssScrollBarsNone

'NOTA: TODAS LAS INTERVENCIONES SON CITABLES
    sql = "SELECT /*+ ORDERED INDEX(CI0100 CI0103) */"
    sql = sql & " CI01FECCONCERT FECHA, TRUNC(CI01FECCONCERT) DIA,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0400.PR03NUMACTPEDI, PR01DESCORTA,"
    sql = sql & " PR0400.PR04FECENTRCOLA,"
    sql = sql & " PR0400.CI21CODPERSONA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
    sql = sql & " CI2200.CI22NUMHISTORIA, (SYSDATE-NVL(CI22FECNACIM,SYSDATE))/365 EDAD,"
    sql = sql & " PR0400.AD02CODDPTO,"
    sql = sql & " PR0400.PR37CODESTADO,"
    sql = sql & " PR0400.AD07CODPROCESO, PR0400.AD01CODASISTENCI,"
    sql = sql & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & " AD02DESDPTO, SG02TXTFIRMA,"
    sql = sql & " AD1100.CI32CODTIPECON,"
    sql = sql & " CI0100.AG11CODRECURSO CODREC, LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') QUIROFANO,"
    sql = sql & " PR41DUR.PR41RESPUESTA DURACION, PR41ANE.PR41RESPUESTA ANESTESIA,"
    sql = sql & " PR41ORD.PR41RESPUESTA ORDEN"
    sql = sql & " FROM CI0100, PR0400, PR0100, CI2200, PR0300, PR0900, AD0200, SG0200,"
    sql = sql & " AD1500, AD1100, AG1100, PR4100 PR41DUR, PR4100 PR41ANE, PR4100 PR41ORD"
    sql = sql & " WHERE CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
    If strRecSel = "0" Then
        sql = sql & " AND CI0100.AG11CODRECURSO IN ("
        sql = sql & " SELECT AG11CODRECURSO"
        sql = sql & " FROM AG1100"
        sql = sql & " WHERE AG14CODTIPRECU = " & constTIPORECQUIROFANO & ")"
    Else
        sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
' Alberto: se buscan las intervenciones citadas entre el dia anterior y el mismo dia
    sql = sql & " AND CI0100.CI01FecConcert BETWEEN SYSDATE - 1 AND SYSDATE + 1"
' ***********************************************************************************
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NOT NULL"
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    sql = sql & " AND AD1500.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    sql = sql & " AND AD1500.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " AND AD1100.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    sql = sql & " AND AD1100.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    sql = sql & " AND AD1100.AD11FECFIN IS NULL"
    sql = sql & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    sql = sql & " AND PR0900.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    sql = sql & " AND AD0200.AD02CODDPTO = PR0900.AD02CODDPTO"
    sql = sql & " AND SG0200.SG02COD = PR0900.SG02COD"
    sql = sql & " AND PR41DUR.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41DUR.PR40CODPREGUNTA (+)= " & constPREG_DURACION
    sql = sql & " AND PR41ANE.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41ANE.PR40CODPREGUNTA (+)= " & constPREG_ANESTESIA
    sql = sql & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41ORD.PR40CODPREGUNTA (+)= " & constPREG_ORDEN
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.QueryTimeout = 0
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        Select Case Val(rs!EDAD)
        Case 0: strEdad = "?"
        Case Is < 1: strEdad = "<1"
        Case Else: strEdad = Val(rs!EDAD)
        End Select
        ssgrdPendientes.AddItem Format(rs!dia, "dd/mm/yy") & Chr$(9) _
                        & rs!QUIROFANO & Chr$(9) _
                        & rs!orden & Chr$(9) _
                        & rs!DURACION & Chr$(9) _
                        & rs!ANESTESIA & Chr$(9) _
                        & rs!CI22NUMHISTORIA & Chr$(9) _
                        & rs!paciente & Chr$(9) _
                        & rs!CI32CODTIPECON & Chr$(9) _
                        & strEdad & Chr$(9) _
                        & rs!PR01DESCORTA & Chr$(9) _
                        & Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
                        & rs!AD02DESDPTO & Chr$(9) _
                        & rs!SG02TXTFIRMA & Chr$(9) _
                        & rs!Cama & Chr$(9) _
                        & rs!PR04NUMACTPLAN & Chr$(9) _
                        & rs!CI21CODPERSONA & Chr$(9) _
                        & rs!AD07CODPROCESO & Chr$(9) _
                        & rs!AD01CODASISTENCI & Chr$(9) _
                        & rs!PR03NUMACTPEDI
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdPendientes.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCaption()
    Dim i%, msg$
    
    'Se muestra en el Caption del formulario los recursos seleccionados
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected = True Then
            msg = msg & lvwRec.ListItems(i).Text & ", "
            If i = 1 Then Exit For
        End If
    Next i
    If msg <> "" Then
        msg = Left$(msg, Len(msg) - 2)
        msg = strMeCaption & ". Recursos: " & msg
    Else
        msg = strMeCaption
    End If
    Me.Caption = msg
End Sub

Private Sub pVolverACitas()
'****************************************************************************************
'*  Se pone la actuaci�n en estado de Pendiente de Recitar para que sea recitada en otro
'*  momento
'****************************************************************************************
    Dim sql$, qryCI0100 As rdoQuery, qryPR0400 As rdoQuery, qryPR0300 As rdoQuery, i%
    Dim lngNumActPlan&
    Dim intResp%
    
    'se comprueba que se ha seleccionado alguna actuaci�n
    If ssgrdPrevistas.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    sql = "�Desea Ud. MANTENER la fecha de planificaci�n de las intervenciones"
    sql = sql & " seleccionadas al dejarlas como pendientes de recitar?"
    intResp = MsgBox(sql, vbQuestion + vbYesNoCancel + vbDefaultButton2, strMeCaption)
    If intResp = vbCancel Then Exit Sub
        
    'CI0100
    sql = "UPDATE CI0100 SET CI01SITCITA = ?"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA = ?"
    Set qryCI0100 = objApp.rdoConnect.CreateQuery("", sql)
    qryCI0100(0) = constESTCITA_PENDRECITAR
    qryCI0100(2) = constESTCITA_CITADA
    'PR0400
    sql = "UPDATE PR0400 SET PR37CODESTADO = ?"
    If intResp = vbNo Then sql = sql & ", PR04FECPLANIFIC = NULL"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR0400 = objApp.rdoConnect.CreateQuery("", sql)
    qryPR0400(0) = constESTACT_PLANIFICADA
    'PR0300
    If intResp = vbNo Then
        sql = "UPDATE PR0300 SET PR03FECPREFEREN = NULL"
        sql = sql & " WHERE PR03NUMACTPEDI = ("
        sql = sql & " SELECT PR03NUMACTPEDI"
        sql = sql & " FROM PR0400"
        sql = sql & " WHERE PR04NUMACTPLAN = ?)"
        Set qryPR0300 = objApp.rdoConnect.CreateQuery("", sql)
    End If
    
    sql = "Se ha producido un error al intentar dejar la intervenci�n como pendiente de recitar."
    
    objApp.BeginTrans
    On Error Resume Next
    For i = 0 To ssgrdPrevistas.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i))
    
        'CI0100: estado de la cita
        qryCI0100(1) = lngNumActPlan
        qryCI0100.Execute
        If Err > 0 Then
            objApp.RollbackTrans
            MsgBox sql & Chr$(13) & Chr$(13) & Error, vbExclamation, strMeCaption
            Exit Sub
        End If
    
        'PR0400: estado de la actuaci�n (y fecha de planificaci�n)
        qryPR0400(1) = lngNumActPlan
        qryPR0400.Execute
        If Err > 0 Then
            objApp.RollbackTrans
            MsgBox sql & Chr$(13) & Chr$(13) & Error, vbExclamation, strMeCaption
            Exit Sub
        End If
    
        If intResp = vbNo Then
            'PR0300: (fecha de preferencia)
            qryPR0300(0) = lngNumActPlan
            qryPR0300.Execute
            If Err > 0 Then
                objApp.RollbackTrans
                MsgBox sql & Chr$(13) & Chr$(13) & Error, vbExclamation, strMeCaption
                Exit Sub
            End If
        End If
    Next i
    qryCI0100.Close
    qryPR0400.Close
    If intResp = vbNo Then qryPR0300.Close
    On Error GoTo 0
    
    objApp.CommitTrans
    ssgrdPrevistas.DeleteSelected
End Sub


Private Sub pVolverACitas1()
'****************************************************************************************
'*  Se pone la actuaci�n en estado de Pendiente de Recitar para que sea recitada en otro
'*  momento
'****************************************************************************************
    Dim sql$, qryCI0100 As rdoQuery, qryPR0400 As rdoQuery, i%
    Dim lngNumActPlan&
    
    'se comprueba que se ha seleccionado alguna actuaci�n
    If ssgrdPrevistas.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    sql = "�Desea Ud. dejar las intervenciones seleccionadas como pendientes de recitar?"
    If MsgBox(sql, vbQuestion + vbYesNo, strMeCaption) = vbNo Then Exit Sub
    
    'CI0100
    sql = "UPDATE CI0100 SET CI01SITCITA = ?"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA = ?"
    Set qryCI0100 = objApp.rdoConnect.CreateQuery("", sql)
    qryCI0100(0) = constESTCITA_PENDRECITAR
    qryCI0100(2) = constESTCITA_CITADA
    'PR0400
    sql = "UPDATE PR0400 SET PR37CODESTADO = ?"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR0400 = objApp.rdoConnect.CreateQuery("", sql)
    qryPR0400(0) = constESTACT_PLANIFICADA
    
    sql = "Se ha producido un error al intentar dejar la intervenci�n como pendiente de recitar."
    
    objApp.BeginTrans
    On Error Resume Next
    For i = 0 To ssgrdPrevistas.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i))
    
        'CI0100: estado de la cita
        qryCI0100(1) = lngNumActPlan
        qryCI0100.Execute
        If Err > 0 Then
            objApp.RollbackTrans
            MsgBox sql & Chr$(13) & Chr$(13) & Error, vbExclamation, strMeCaption
            Exit Sub
        End If
    
        'PR0400: estado de la actuaci�n
        qryPR0400(1) = lngNumActPlan
        qryPR0400.Execute
        If Err > 0 Then
            objApp.RollbackTrans
            MsgBox sql & Chr$(13) & Chr$(13) & Error, vbExclamation, strMeCaption
            Exit Sub
        End If
    Next i
    qryCI0100.Close
    qryPR0400.Close
    On Error GoTo 0
    
    objApp.CommitTrans
    ssgrdPrevistas.DeleteSelected
End Sub

Private Sub pImprimirEtiquetas(strCodPersona$, strProc$, strAsist$)
    Dim fila%, col%
    Dim intPosX%, intPosY%, intIncX%, intIncY%
    Dim X%
    Dim strNH$, strFecha$, strPac$, strApellidos$, strNombre$, i%
    Dim strTipoEcon$, strEntidad$
    Dim sql$, rs As rdoResultset, qry As rdoQuery
    
    'caracter�sticas del documento
    vp.FontName = "Times New Roman"
    'vp.FontName = "MS Sans Serif"
    vp.FontSize = 8
    vp.Orientation = orPortrait
    vp.MarginLeft = 0: vp.MarginRight = 0: vp.MarginTop = 0: vp.MarginBottom = 0
    
    intPosX = 75: intPosY = 335: intIncX = 3900: intIncY = 1433
    
    'Datos del paciente
    sql = "SELECT CI22FECNACIM, CI22NOMBRE, CI22PRIAPEL||' '||CI22SEGAPEL APEL, CI22NUMHISTORIA"
    sql = sql & " FROM CI2200"
    sql = sql & " WHERE CI21CODPERSONA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodPersona
    Set rs = qry.OpenResultset()
    If Not IsNull(rs!CI22NUMHISTORIA) Then strNH = rs!CI22NUMHISTORIA Else strNH = "      "
    strNombre = rs!CI22NOMBRE
    strApellidos = rs!APEL
    If Not IsNull(rs!CI22FECNACIM) Then strFecha = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
    rs.Close
    qry.Close
    'nombre del paciente
    If Trim$(strApellidos) <> "" Then
        strPac = strApellidos & ", " & strNombre
    Else
        strPac = strNombre
    End If
    If Len(strPac) > 40 Then
        awk1 = strNombre
        awk1.FS = " "
        strNombre = ""
        For i = 1 To awk1.NF
            strNombre = strNombre & Left(awk1.F(i), 1) & "."
        Next i
        strPac = strApellidos & ", " & strNombre
    End If
    
    'Tipo econ�mico
    sql = "SELECT CI32CODTIPECON, CI13CODENTIDAD"
    sql = sql & " FROM AD1100"
    sql = sql & " WHERE AD07CODPROCESO = ?"
    sql = sql & " AND AD01CODASISTENCI = ?"
    sql = sql & " ORDER BY AD11FECINICIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strProc
    qry(1) = strAsist
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strTipoEcon = rs!CI32CODTIPECON
        strEntidad = rs!CI13CODENTIDAD
    End If
        
    vp.StartDoc
    vp.CurrentX = intPosX
    vp.CurrentY = intPosY
    For fila = 1 To 11
        For col = 1 To 3
            X = vp.CurrentX
            vp.FontSize = 12
            vp.Text = "Hist: "
            vp.FontBold = True
            vp.Text = strNH
            vp.FontBold = False
            vp.CurrentX = vp.CurrentX + 500
            vp.Text = strTipoEcon
            vp.CurrentX = vp.CurrentX + 500
            vp.Text = strEntidad
            vp.Text = Chr$(13)
            vp.CurrentX = X
            vp.FontSize = 8
            vp.Text = strPac
            vp.Text = Chr$(13)
            vp.CurrentX = X
            vp.Text = strFecha
            X = X + intIncX
            vp.CurrentX = X
            vp.CurrentY = intPosY
        Next col
        vp.CurrentX = intPosX
        intPosY = intPosY + intIncY
        vp.CurrentY = intPosY
    Next fila
    vp.EndDoc
    vp.PrintDoc
End Sub

Private Sub pImprimirHoja(intOpcion%)
'***************************************************************************************
'*  Imprime la Hoja de Quir�fano seg�n la opci�n seleccionada por intOpcion
'*  0 - Quir�fanos del 1 al 8
'*  4 - Quir�fanos 10 y 11
'***************************************************************************************
    Dim sql$, sqlOrder$, qry As rdoQuery, rs As rdoResultset
    Dim strAhora$
    Dim strDia$, strQuir$, strNH$, strCodPersona$, strEdad$, strCama$, strTipoEcon$
    Dim strC$, strH$, strRow$, lngAncho&
    Dim nPage%

    strAhora = strFechaHora_Sistema

    sql = "SELECT /*+ ORDERED INDEX (CI0100 CI0105) */"
    sql = sql & " CI01FECCONCERT, TRUNC(CI01FECCONCERT) DIA, PR01DESCORTA,"
    sql = sql & " CI2200.CI22NUMHISTORIA, CI2200.CI21CODPERSONA,"
    sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
    sql = sql & " (SYSDATE-NVL(CI22FECNACIM,SYSDATE))/365 EDAD, DECODE(CI30CODSEXO,1,'H',2,'M') SEXO,"
    sql = sql & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & " AD02DESDPTO, SG02TXTFIRMA,"
    sql = sql & " AD1100.CI32CODTIPECON,"
    sql = sql & " CI0100.AG11CODRECURSO, AG1100.AG11DESRECURSO,"
    sql = sql & " LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') QUIROFANO,"
    sql = sql & " NVL(PR41DUR.PR41RESPUESTA,' ') DURACION, NVL(PR41ANE.PR41RESPUESTA,' ') ANESTESIA,"
    sql = sql & " NVL(PR41ORD.PR41RESPUESTA,' ') ORDEN"
    sql = sql & " FROM CI0100, PR0400, CI2200, PR0100, PR0300, PR0900, AD0200, SG0200,"
    sql = sql & " AD1500, AD1100, AG1100, PR4100 PR41DUR, PR4100 PR41ANE, PR4100 PR41ORD"
    sql = sql & " WHERE CI0100.CI01FECCONCERT >= TO_DATE(?,'DD/MM/YYYY')"
    sql = sql & " AND CI0100.CI01FECCONCERT < TO_DATE(?,'DD/MM/YYYY')"
    sql = sql & " AND CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
    Select Case intOpcion
    Case 0
        sql = sql & " AND CI0100.AG11CODRECURSO BETWEEN " & constQUIROFANO_01 & " AND " & constQUIROFANO_08 'para poder entrar por �ndice
        sqlOrder = " ORDER BY DIA, LPAD(QUIROFANO,2,'0'), LPAD(ORDEN,2,'0'), PACIENTE, PR01DESCORTA"
    Case 4
        sql = sql & " AND CI0100.AG11CODRECURSO BETWEEN " & constQUIROFANO_10 & " AND " & constQUIROFANO_11 'para poder entrar por �ndice
        sqlOrder = " ORDER BY DIA, LPAD(QUIROFANO,2,'0'), CI01FECCONCERT, PACIENTE, PR01DESCORTA"
    End Select
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NULL"
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    sql = sql & " AND AD1500.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    sql = sql & " AND AD1500.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " AND AD1100.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    sql = sql & " AND AD1100.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    sql = sql & " AND AD1100.AD11FECFIN IS NULL"
    sql = sql & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    sql = sql & " AND PR0900.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    sql = sql & " AND AD0200.AD02CODDPTO = PR0900.AD02CODDPTO"
    sql = sql & " AND SG0200.SG02COD = PR0900.SG02COD"
    sql = sql & " AND PR41DUR.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41DUR.PR40CODPREGUNTA (+)= " & constPREG_DURACION
    sql = sql & " AND PR41ANE.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41ANE.PR40CODPREGUNTA (+)= " & constPREG_ANESTESIA
    sql = sql & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR41ORD.PR40CODPREGUNTA (+)= " & constPREG_ORDEN
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(dcboDesde.Text, "dd/mm/yyyy")
    qry(1) = Format(DateAdd("d", 1, dcboHasta.Text), "dd/mm/yyyy")
    Set rs = qry.OpenResultset()
    'caracter�sticas del documento
    vp.FontName = "Times New Roman"
    vp.FontSize = 8
    vp.Orientation = orLandscape
    vp.MarginLeft = 0: vp.MarginRight = 0: vp.MarginTop = 400: vp.MarginBottom = 400
    'comienzo de documento
    vp.StartDoc
    Do While Not rs.EOF
        'control de d�a
        If strDia <> rs!dia Then
            If strDia <> "" Then vp.NewPage
            strDia = rs!dia: strQuir = "": nPage = 1
            Call pIMprimirHojaCabecera(rs!dia, strAhora)
        End If
        'control de quir�fanos
        If strQuir <> rs!AG11CODRECURSO Then
            strQuir = rs!AG11CODRECURSO: strCodPersona = ""
            
            'control de saltos de p�gina
            Select Case rs!AG11CODRECURSO
            Case constQUIROFANO_04, constQUIROFANO_05, constQUIROFANO_06
                Do While nPage < 2
                    vp.NewPage
                    Call pIMprimirHojaCabecera(strDia, strAhora)
                    nPage = nPage + 1
                Loop
            Case constQUIROFANO_07, constQUIROFANO_08
                Do While nPage < 3
                    vp.NewPage
                    Call pIMprimirHojaCabecera(strDia, strAhora)
                    nPage = nPage + 1
                Loop
            End Select
            'posiciones de referencia de las tablas
            Select Case rs!AG11CODRECURSO
            Case constQUIROFANO_01, constQUIROFANO_04, constQUIROFANO_07, constQUIROFANO_10
                lngAncho = 350
            Case constQUIROFANO_02, constQUIROFANO_05, constQUIROFANO_08
                lngAncho = 5750
            Case constQUIROFANO_03, constQUIROFANO_06, constQUIROFANO_11
                lngAncho = 11150
            End Select
            't�tulo del Quir�fano
            vp.CurrentY = vp.MarginTop + 500
            vp.CurrentX = lngAncho - 350
            vp.FontBold = True
            vp.Text = UCase(rs!AG11DESRECURSO) & ":" & Space(5) & UCase(rs!AD02DESDPTO)
            vp.FontBold = False
            vp.Text = Chr$(13)
            'tama�o de los campos de las tablas
            strC = "+>" & lngAncho & "|+>700|+<2950|+^350|+^350|+<600"
            strH = "|||||"
        End If
        
        'paciente
        If strCodPersona <> rs!CI21CODPERSONA Then
            strCodPersona = rs!CI21CODPERSONA
            vp.AddTable strC, strH, "|||||", 0, 0, True
            If rs!EDAD = 0 Then
                strEdad = "?"
            ElseIf Val(rs!EDAD) < 1 Then
                strEdad = "<1"
            Else
                strEdad = Val(rs!EDAD)
            End If
            'el RTF hace cosas extra�as si los campos est�n vac�os
            If Not IsNull(rs!CI22NUMHISTORIA) Then strNH = "{\b\fs16 " & rs!CI22NUMHISTORIA & "}" Else strNH = ""
            If Not IsNull(rs!Cama) Then strCama = "{\b\fs16 " & rs!Cama & "}" Else strCama = ""
            strRow = rs!orden & "|" & strNH & "|{\b\fs16 " & rs!paciente & "}|" _
                    & strEdad & "|" & rs!SEXO & "|" & strCama
            vp.AddTable strC, strH, strRow, 0, 0, True
            'doctor
            Select Case intOpcion
            Case 0
                strRow = "||" & rs!SG02TXTFIRMA & "|||"
            Case 4
                strRow = "||" & rs!SG02TXTFIRMA & "|||" & Format(rs!CI01FECCONCERT, "hh:mm")
            End Select
            vp.AddTable strC, strH, strRow, 0, 0, True
        End If
        'actuaciones
        If Not IsNull(rs!CI32CODTIPECON) Then strTipoEcon = "{\b\fs16 " & rs!CI32CODTIPECON & "}" Else strTipoEcon = ""
        strRow = "|*|" & rs!PR01DESCORTA & "|" & strTipoEcon & "|" & Left$(rs!ANESTESIA, 2) & "|" & rs!DURACION
        vp.AddTable strC, strH, strRow, 0, 0, True
        
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    vp.EndDoc
    vp.PrintDoc
End Sub

Private Sub pIMprimirHojaCabecera(strDia$, strAhora$)
    vp.FontBold = True
    vp.TextAlign = taLeftTop: vp.Text = UCase(Format(strDia, "long date"))
    vp.FontBold = False
    vp.TextAlign = taRightTop: vp.Text = Format(strAhora, "dd/mm/yyyy hh:mm")
    vp.TextAlign = taLeftTop
    vp.Text = Chr$(13) & Chr$(13)
    vp.DrawLine 5300, 800, 5300, 11400
    vp.DrawLine 10700, 800, 10700, 11400
    vp.DrawLine 0, 1150, 5150, 1150
    vp.DrawLine 5400, 1150, 10550, 1150
    vp.DrawLine 10800, 1150, 15950, 1150
End Sub

Private Sub pCitar()
    Dim sql$, qry As rdoQuery, i%
            
    'se comprueba que se ha seleccionado alguna actuaci�n de las en espera
    If ssgrdPrevistas.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    For i = 0 To ssgrdPrevistas.SelBookmarks.Count - 1
        Call objsecurity.LaunchProcess("CI1150", ssgrdPrevistas.Columns("Num. Act.").CellText(ssgrdPrevistas.SelBookmarks(i)))
    Next i
    
    Call pCargarActPrevistas(-1)
End Sub

Private Sub pSeleccionarConjunto(grdGrid As SSDBGrid)
    Dim strCodPersona$, strFecIni$, strNAPlan$, i%, pos%
    
    strCodPersona = grdGrid.Columns("Cod. Persona").CellText(grdGrid.SelBookmarks(0))
    LockWindowUpdate Me.hWnd
    grdGrid.SelBookmarks.RemoveAll
    For i = 1 To grdGrid.Rows
        If i = 1 Then grdGrid.MoveFirst Else grdGrid.MoveNext
        If grdGrid.Columns("Cod. Persona").Text = strCodPersona Then
            If pos = 0 Then pos = i
            grdGrid.SelBookmarks.Add (grdGrid.RowBookmark(grdGrid.Row))
        End If
    Next i
    grdGrid.MoveFirst
    grdGrid.MoveRecords pos - 1 'reposicionameiento en el grid
    LockWindowUpdate 0&
    DoEvents
End Sub
