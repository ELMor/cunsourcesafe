VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmcuestrealizar 
   Caption         =   "GESTI�N DE ACTUACIONES. Cuestionario"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MousePointer    =   1  'Arrow
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdsalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   5280
      TabIndex        =   1
      Top             =   7800
      Width           =   1935
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   7065
      Left            =   240
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   480
      Width           =   10815
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   2
      BevelColorFrame =   0
      BevelColorHighlight=   16777215
      AllowAddNew     =   -1  'True
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      MaxSelectedRows =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   2
      Columns(0).Width=   9260
      Columns(0).Caption=   "PREGUNTA"
      Columns(0).Name =   "PREGUNTA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   8493
      Columns(1).Caption=   "RESPUESTA"
      Columns(1).Name =   "RESPUESTA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   19076
      _ExtentY        =   12462
      _StockProps     =   79
      Caption         =   "CUESTIONARIO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmcuestrealizar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00260.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: OCTUBRE 1998                                                  *
'* DESCRIPCION:  Cuestionario de la actuaci�n                           *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************








Private Sub cmdsalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim stra As String
Dim rsta As rdoResultset
Dim strpreg As String
Dim rstpreg As rdoResultset
Dim strrest As String
Dim rstrest As rdoResultset
Dim strcodigo As String
Dim rstcodigo As rdoResultset
Dim strvalor As String
Dim rstvalor As rdoResultset
Dim strrespuesta As String
Dim rstrespuesta As rdoResultset


stra = "SELECT * FROM PR4100 WHERE PR03NUMACTPEDI=" & glngactpedida
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
    grdDBGrid1.AddNew
    strpreg = "SELECT PR40DESPREGUNTA FROM PR4000 WHERE PR40CODPREGUNTA=" & _
              rsta.rdoColumns("PR40CODPREGUNTA").Value
    Set rstpreg = objApp.rdoConnect.OpenResultset(strpreg)
    grdDBGrid1.Columns(0).Value = rstpreg.rdoColumns("PR40DESPREGUNTA").Value
    rstpreg.Close
    Set rstpreg = Nothing
    If rsta.rdoColumns("PR27CODTIPRESPU").Value = 3 Then
        If rsta.rdoColumns("PR41RESPUESTA").Value = -1 Then
            grdDBGrid1.Columns(1).Value = "S�"
        Else
            grdDBGrid1.Columns(1).Value = "No"
        End If
    Else
        grdDBGrid1.Columns(1).Value = rsta.rdoColumns("PR41RESPUESTA").Value
    End If
    grdDBGrid1.Update
rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
'************************************************************************************
strrest = "select DISTINCT * from PR0301J where pr03numactpedi=" & glngactpedida
Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
While Not rstrest.EOF
    grdDBGrid1.AddNew
    grdDBGrid1.Columns(0).Value = rstrest.rdoColumns("AG16DESTIPREST").Value
    'se mira si el registro ya tiene una respuesta guardada
    If rstrest.rdoColumns("AG27CODAMBRESTR").Value = 1 Then
     strrespuesta = "select PR4700.pr47valor,AG1600.ag18codtipdato FROM PR4700,AG1600 " & _
                    "WHERE PR4700.pr09numpeticion=" & glngnumPeticion & _
                    " AND PR4700.ag16codtiprest=" & rstrest.rdoColumns("ag16codtiprest") & _
                    " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest"
    Else
     strrespuesta = "select PR5700.pr57valor,AG1600.ag18codtipdato FROM PR5700,AG1600 " & _
                    "WHERE PR5700.pr03NUMACTPEDI=" & glngactpedida & _
                    " AND PR5700.ag16codtiprest=" & rstrest.rdoColumns("ag16codtiprest") & _
                    " AND PR5700.ag16codtiprest=AG1600.ag16codtiprest"
    End If
     Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
   If Not rstrespuesta.EOF Then
     If Trim(rstrespuesta.rdoColumns(0).Value) = "" Then
        grdDBGrid1.Columns(1).Value = ""
      Else
      'si la respuesta es num�rica o tipo string
      If (rstrespuesta.rdoColumns(1).Value = 1) Or _
         (rstrespuesta.rdoColumns(1).Value = 2) Then
       'se mira si la respuesta es un c�digo o no
       If rstrest.rdoColumns("AG27CODAMBRESTR").Value = 1 Then
        strcodigo = "select PR4700.pr47valor,AG1600.ag16tablavalor," & _
                   "AG1600.ag16colcodvalo,AG1600.ag16coldesvalo " & _
                   "FROM AG1600,PR4700 WHERE " & _
                   "PR4700.pr09numpeticion=" & glngnumPeticion & _
                   " AND PR4700.ag16codtiprest=" & rstrest.rdoColumns("ag16codtiprest") & _
                   " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest"
       Else
        strcodigo = "select PR5700.pr57valor,AG1600.ag16tablavalor," & _
                   "AG1600.ag16colcodvalo,AG1600.ag16coldesvalo " & _
                   "FROM AG1600,PR5700 WHERE " & _
                   "PR5700.pr03NUMACTPEDI=" & glngactpedida & _
                   " AND PR5700.ag16codtiprest=" & rstrest.rdoColumns("ag16codtiprest") & _
                   " AND PR5700.ag16codtiprest=AG1600.ag16codtiprest"
       End If
       Set rstcodigo = objApp.rdoConnect.OpenResultset(strcodigo)
       If rstcodigo.rdoColumns(1).Value = "" Or IsNull(rstcodigo.rdoColumns(1).Value) Then
         grdDBGrid1.Columns(1).Value = rstrespuesta.rdoColumns(0).Value
       Else
         strvalor = "SELECT " & rstcodigo.rdoColumns(2).Value & _
                  "," & rstcodigo.rdoColumns(3).Value & _
                  " FROM " & _
                  rstcodigo.rdoColumns(1) & " WHERE " & _
                  rstcodigo.rdoColumns(2).Value & "='" & _
                  rstrespuesta.rdoColumns(0).Value & "'"
         Set rstvalor = objApp.rdoConnect.OpenResultset(strvalor)
         grdDBGrid1.Columns(1).Value = rstvalor.rdoColumns(1).Value
         rstvalor.Close
         Set rstvalor = Nothing
       End If
       rstcodigo.Close
       Set rstcodigo = Nothing
     End If
       If (rstrespuesta.rdoColumns(1).Value = 3) Then
        If rstrest.rdoColumns("AG27CODAMBRESTR").Value = 1 Then
          strrespuesta = "select to_char(to_date(PR4700.pr47valor),'DD/MM/YYYY') " & _
              "FROM PR4700,AG1600 " & _
              "WHERE PR4700.pr09numpeticion=" & glngnumPeticion & _
              " AND PR4700.ag16codtiprest=" & rstrest.rdoColumns("ag16codtiprest") & _
              " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest" & _
              " AND AG1600.ag18codtipdato=3"
        Else
         strrespuesta = "select to_char(to_date(PR5700.pr57valor),'DD/MM/YYYY') " & _
              "FROM PR5700,AG1600 " & _
              "WHERE PR5700.pr03NUMACTPEDI=" & glngactpedida & _
              " AND PR5700.ag16codtiprest=" & rstrest.rdoColumns("ag16codtiprest") & _
              " AND PR5700.ag16codtiprest=AG1600.ag16codtiprest" & _
              " AND AG1600.ag18codtipdato=3"
        End If
            Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
            grdDBGrid1.Columns(1).Value = rstrespuesta.rdoColumns(0).Value
       End If

       End If
    End If
       rstrespuesta.Close
       Set rstrespuesta = Nothing
grdDBGrid1.Update
rstrest.MoveNext
Wend
rstrest.Close
Set rstrest = Nothing

grdDBGrid1.MoveFirst
End Sub
