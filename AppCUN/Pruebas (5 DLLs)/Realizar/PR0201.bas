Attribute VB_Name = "modFunciones"

Option Explicit

Function AbrirAsistencia(nPers As Long, nProc As Long, nPrPlan As Long) As Long
Dim sql As String, Pac As String, fIni As String, fFin As String, TiPac As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim nAsist As Long, NH As Long, nAsistPadre As Long
  
' Se busca el proceso-asistencia del paciente.
  sql = "SELECT AD0100.AD01CodAsistenci, TO_CHAR(AD0100.AD01FecInicio, 'DD/MM/YY HH24:MI'), " _
      & "TO_CHAR(AD0100.AD01FecFin, 'DD/MM/YY HH24:MI'), " _
      & "CI2200.CI22NumHistoria, CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE, " _
      & "AD0100.AD01CodAsistenci_Pad " _
      & "FROM AD0100, CI2200, AD0800 WHERE " _
      & "CI2200.CI21CodPersona = AD0100.CI21CodPersona AND " _
      & "AD0100.AD01CodAsistenci = AD0800.AD01CodAsistenci AND " _
      & "CI2200.CI21CodPersona = ? AND " _
      & "AD0800.AD07CodProceso = ? " _
      & "ORDER BY AD0100.AD01FecInicio DESC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPers
  rdoQ(1) = nProc
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "El paciente no tiene ninguna asistencia abierta. Debe pasar por la unidad de " _
      & "coordinaci�n para abrirle hojas.", vbInformation, "Abrir asistencias"
    Exit Function
  End If
  nAsist = rdo(0)
  fIni = rdo(1)
  If IsNull(rdo(2)) Then
    sql = "UPDATE PR0400 SET AD01CodAsistenci = ? WHERE PR04NumActPlan = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = nAsist
    rdoQ(1) = nPrPlan
    rdoQ.Execute
    AbrirAsistencia = nAsist
    Exit Function
  End If
  fFin = rdo(2)
  NH = rdo(3)
  Pac = rdo(4)
  nAsistPadre = IIf(IsNull(rdo(5)), nAsist, rdo(5))
  
  sql = "SELECT AD0500.AD02CodDpto, AD0200.AD02DesDpto, AD0500.SG02Cod, " _
      & "NVL(SG0200.SG02TxtFirma, SG0200.SG02Ape1||' '||SG0200.SG02Ape2||', '||SG0200.SG02Nom), " _
      & "AD0800.CI21CodPersona_Env, CI22Env.CI22PRIAPEL||' '||CI22Env.CI22SEGAPEL||', '||CI22Env.CI22NOMBRE, " _
      & "AD0800.CI21CodPersona_Med, CI22Med.CI22PRIAPEL||' '||CI22Med.CI22SEGAPEL||', '||CI22Med.CI22NOMBRE, " _
      & "AD0800.AD08Observac, CI3200.CI32CodTipEcon, CI3200.CI32DesTipEcon, " _
      & "CI1300.CI13CodEntidad, CI1300.CI13DesEntidad, AD1200.AD12DesTipoAsist, " _
      & "AD1100.CI21CodPersona, ADFN01(AD1100.CI21CodPersona) " _
      & "FROM AD0800, AD0500, SG0200, AD0200, AD1100, CI3200, CI1300, CI2200 CI22Env, " _
      & "CI2200 CI22Med, AD2500, AD1200 WHERE "
  sql = sql & "AD0800.AD01CodAsistenci = AD0500.AD01CodAsistenci AND " _
      & "AD0800.AD07CodProceso = AD0500.AD07CodProceso AND " _
      & "AD0500.AD05FecFinRespon IS NULL AND " _
      & "AD0500.AD02CodDpto = AD0200.AD02CodDpto AND " _
      & "AD0500.SG02Cod = SG0200.SG02Cod AND " _
      & "AD0800.AD01CodAsistenci = AD2500.AD01CodAsistenci AND " _
      & "AD2500.AD25FecFin IS NULL AND " _
      & "AD2500.AD12CodTipoAsist = AD1200.AD12CodTipoAsist AND " _
      & "AD0800.AD01CodAsistenci = AD1100.AD01CodAsistenci AND " _
      & "AD0800.AD07CodProceso = AD1100.AD07CodProceso AND " _
      & "AD1100.AD11FecFin IS NULL AND " _
      & "AD1100.CI32CodTipEcon = CI3200.CI32CodTipEcon AND " _
      & "AD1100.CI32CodTipEcon = CI1300.CI32CodTipEcon AND " _
      & "AD1100.CI13CodEntidad = CI1300.CI13CodEntidad AND " _
      & "AD0800.CI21CodPersona_Env = CI22Env.CI21CodPersona (+) AND " _
      & "AD0800.CI21CodPersona_Med = CI22Med.CI21CodPersona (+) AND " _
      & "AD0800.AD01CodAsistenci = ? AND " _
      & "AD0800.AD07CodProceso = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nAsist
  rdoQ(1) = nProc
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "No se han encontrado datos de la asistencia.", vbExclamation, "Abrir asistencias"
    Exit Function
  End If
  Load frmAbrirAsist
  With frmAbrirAsist
    .nPers = nPers
    .nProc = nProc
    .nAsistPadre = nAsistPadre
    .txtNH.Text = NH
    .txtPac.Text = Pac
    .txtFIniUltimaAsist.Text = fIni
    .txtFFinUltimaAsist.Text = fFin
    .txtTiAsist.Text = rdo(13)
    .cDptResp = rdo(0)
    .txtDptResp.Text = rdo(1)
    .cboDpt.Text = rdo(1)
    Call .LlenarDrResp(.cDptResp)
    .cDrResp = rdo(2)
    .cboDrResp.Text = rdo(3)
    .txtDrResp.Text = rdo(3)
    .cboTiEco.Text = rdo(9) & " - " & rdo(10)
    .cTiEco = rdo(9)
    ' Se buscan las entidades
    Call .BuscarEntidad(rdo(9))
    .cboEntidad.Text = rdo(11) & " - " & rdo(12)
    .cEntidad = rdo(11)
    Call .BuscarResp(rdo(11))
    ' Buscar Documentaci�n
    Call .BuscarDoc(nProc, nAsist, .cTiEco, .cEntidad)
    .cboResp.Text = rdo(14) & " - " & rdo(15)
    .cTiPac = SacarTiPac(rdo(0), NH, TiPac)
    .cboTiPac.Text = TiPac
    .Show vbModal
    If .seguir = True Then
      sql = "UPDATE PR0400 SET AD01CodAsistenci = ? WHERE PR04NumActPlan = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = .nAsist
      rdoQ(1) = nPrPlan
      rdoQ.Execute
      
      sql = "UPDATE PR0800 SET AD01CodAsistenci = ? WHERE PR03NumActPedi IN ( " _
          & "SELECT PR03NumActPedi FROM PR0400 WHERE PR04NumActPlan = ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = .nAsist
      rdoQ(1) = nPrPlan
      rdoQ.Execute
      
      .seguir = .nAsist
    End If
    AbrirAsistencia = .seguir
    Unload frmAbrirAsist
  End With
End Function


Function SacarTiPac(cDpt As Integer, NH As Long, TiPac As String) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AD0500.AD02CodDpto " _
      & "FROM AD0500, AD0100 WHERE " _
      & "AD0100.AD01CodAsistenci = AD0500.AD01CodAsistenci AND " _
      & "AD0500.AD05FecFinRespon IS NULL AND " _
      & "AD0100.CI22NumHistoria = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    SacarTiPac = constTIPACNUEVO
    TiPac = "Nuevo"
    Exit Function
  End If
  SacarTiPac = constTIPACNUEVOREVISION
  TiPac = "Nuevo-Revisi�n"
  While rdo.EOF = False
    If rdo(0) = cDpt Then
      SacarTiPac = constTIPACREVISION
      TiPac = "Revisi�n"
      Exit Function
    End If
    rdo.MoveNext
  Wend
End Function


Function CerrarAsist(nPrPlan As Long, nAsist As Long, nProc As Long, fFin As String) As Integer
' Devuelve TRUE si no hay que cerrar la asistencia o proceso-asistencia o si se ha cerrado
' correctamente. En el caso de que haya dado error al cerrar, devuelve FALSE.
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim res As Integer
  
  sql = "SELECT PR0400.PR04NumActPlan, PR0400.AD07CodProceso, AD0100.AD01CodAsistenci_Pad " _
      & "FROM PR0400, AD0100 WHERE " _
      & "AD0100.AD01CodAsistenci = PR0400.AD01CodAsistenci AND " _
      & "AD0100.AD01CodAsistenci = ? AND " _
      & "AD0100.AD01CodAsistenci_Pad IS NOT NULL AND " _
      & "PR0400.PR04NumActPlan <> ? AND " _
      & "PR0400.PR37CodEstado < ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nAsist
  rdoQ(1) = nPrPlan
  rdoQ(2) = constESTACT_REALIZADA
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then ' No se han encontrado pruebas pendientes.
'    res = MsgBox("Esta es la �ltima prueba del paciente. �Desea cerrar la asistencia?", vbQuestion + vbYesNo, "Cerrar assistencias")
'    If res = vbNo Then
'      CerrarAsist = True
'      Exit Function
'    End If
    objApp.BeginTrans
    ' Se cierra la asistencia
    sql = "UPDATE AD0100 SET AD01FecFin = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') " _
        & "WHERE AD01CodAsistenci = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = fFin
    rdoQ(1) = nAsist
    rdoQ.Execute
    If rdoQ.RowsAffected = 0 Then
      objApp.RollbackTrans
      MsgBox "Error al cerrar la asistencia. Error: " & Error, vbExclamation
      CerrarAsist = False
      Exit Function
    End If
    
    ' Se cierran el procesos-asistencia de la prueba
    sql = "UPDATE AD0800 SET AD08FecFin = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') " _
        & "WHERE AD01CodAsistenci = ? AND AD07CodProceso = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = fFin
    rdoQ(1) = nAsist
    rdoQ(2) = nProc
    rdoQ.Execute
    If rdoQ.RowsAffected = 0 Then
      objApp.RollbackTrans
      MsgBox "Error al cerrar el proceso-asistencia. Error: " & Error, vbExclamation
      CerrarAsist = False
      Exit Function
    End If
    
    objApp.CommitTrans
    CerrarAsist = True
    Exit Function
  End If
  
  While rdo.EOF = False ' Si se ha encontrado alguna prueba, hay que ver de qu� proceso es
    If rdo(1) = nProc Then ' Hay una prueba sin terminar-> no se cierra nada
      CerrarAsist = True
      Exit Function
    End If
    rdo.MoveNext
  Wend

' Las pruebas activas no son del proceso -> Se cierra s�lo el proceso-asistencia
  sql = "UPDATE AD0800 SET AD08FecFin = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') " _
      & "WHERE AD01CodAsistenci = ? AND AD07CodProceso = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = fFin
  rdoQ(1) = nAsist
  rdoQ(2) = nProc
  rdoQ.Execute
  CerrarAsist = True
  If rdoQ.RowsAffected = 0 Then
    MsgBox "Error al cerrar el proceso-asistencia. Error: " & Error, vbExclamation
    CerrarAsist = False
    Exit Function
  End If
    
End Function
