VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "Crystl32.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmConsultaActReaAnestesia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de actuaciones realizadas de Anestesia"
   ClientHeight    =   7890
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11415
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7890
   ScaleWidth      =   11415
   StartUpPosition =   2  'CenterScreen
   Begin Crystal.CrystalReport crReport1 
      Left            =   11040
      Top             =   60
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imp. Listado"
      Height          =   375
      Left            =   6360
      TabIndex        =   20
      Top             =   7440
      Width           =   1455
   End
   Begin VB.CommandButton cmdImprimirPeticion 
      Caption         =   "Imp. &Petici�n"
      Height          =   375
      Left            =   4680
      TabIndex        =   19
      Top             =   7440
      Width           =   1455
   End
   Begin VB.CommandButton cmdMantCitasPac 
      Caption         =   "&Citas Paciente"
      Height          =   375
      Left            =   1800
      TabIndex        =   18
      Top             =   7440
      Width           =   1455
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "&Datos Actuaci�n"
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   7440
      Width           =   1455
   End
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   10920
      Top             =   600
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   "|"
   End
   Begin VB.Frame Frame5 
      Caption         =   "Consulta"
      ForeColor       =   &H00C00000&
      Height          =   1755
      Left            =   60
      TabIndex        =   16
      Top             =   60
      Width           =   3615
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   375
         Left            =   2520
         TabIndex        =   0
         Top             =   900
         Width           =   975
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
         Height          =   315
         Left            =   720
         TabIndex        =   21
         Top             =   360
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
         Height          =   315
         Left            =   720
         TabIndex        =   22
         Top             =   960
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   24
         Top             =   1020
         Width           =   465
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   0
         Left            =   165
         TabIndex        =   23
         Top             =   420
         Width           =   510
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1755
      Left            =   3780
      TabIndex        =   9
      Top             =   60
      Width           =   7515
      Begin VB.CommandButton cmdB 
         Caption         =   "&Anterior"
         Height          =   375
         Index           =   1
         Left            =   6060
         TabIndex        =   15
         Top             =   1320
         Width           =   975
      End
      Begin VB.CommandButton cmdB 
         Caption         =   "Si&guiente"
         Height          =   375
         Index           =   0
         Left            =   6060
         TabIndex        =   10
         Top             =   840
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   3
         Left            =   3780
         TabIndex        =   4
         Top             =   720
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   2
         Left            =   3780
         TabIndex        =   3
         Top             =   300
         Width           =   1995
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   6060
         TabIndex        =   5
         Top             =   300
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   900
         TabIndex        =   2
         Top             =   720
         Width           =   1875
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   0
         Left            =   900
         TabIndex        =   1
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido"
         Height          =   195
         Index           =   3
         Left            =   3000
         TabIndex        =   14
         Top             =   780
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido"
         Height          =   195
         Index           =   2
         Left            =   3000
         TabIndex        =   13
         Top             =   360
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   12
         Top             =   780
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   11
         Top             =   360
         Width           =   525
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   9840
      TabIndex        =   8
      Top             =   7440
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Caption         =   "Actuaciones realizadas"
      ForeColor       =   &H00C00000&
      Height          =   5475
      Left            =   60
      TabIndex        =   6
      Top             =   1860
      Width           =   11265
      Begin SSDataWidgets_B.SSDBGrid grdActuaciones 
         Height          =   5130
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   11025
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   19447
         _ExtentY        =   9049
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmConsultaActReaAnestesia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strFecDesde$, strFecHasta$
Dim lngTipoAsist&
'variable utilizadas para b�squedas
Dim intBuscar%, blnBuscarOtro As Boolean
Dim cllBuscar As New Collection 'historia|nombre|apel1|apel2
Dim sqlOrder$
Dim sqlOrderRpt As String




Private Sub cmdB_Click(Index As Integer)
    If Index = 0 Then intBuscar = intBuscar + 1 Else intBuscar = intBuscar - 1
    If intBuscar < 0 Then intBuscar = 0
    blnBuscarOtro = True
    cmdBuscar_Click
End Sub

Private Sub cmdBuscar_Click()
    Dim intN%, i%, blnE As Boolean
    
    If fBlnBuscar Then
        cmdB(0).Enabled = True: cmdB(1).Enabled = True
        If blnBuscarOtro = True Then
            blnBuscarOtro = False
            If intBuscar = 0 Then intBuscar = 1: Exit Sub
        Else
            intBuscar = 1
        End If
        
        For i = 1 To cllBuscar.Count
            blnE = False
            awk1 = cllBuscar(i)
            If UCase(Left$(awk1.F(2), Len(txtBuscar(1).Text))) = UCase(txtBuscar(1).Text) _
            And UCase(Left$(awk1.F(3), Len(txtBuscar(2).Text))) = UCase(txtBuscar(2).Text) _
            And UCase(Left$(awk1.F(4), Len(txtBuscar(3).Text))) = UCase(txtBuscar(3).Text) Then
                If txtBuscar(0).Text = "" Then
                    blnE = True
                Else
                    If awk1.F(1) = txtBuscar(0).Text Then blnE = True
                End If
            End If
            If blnE Then
                intN = intN + 1
                If intN = intBuscar Then
                    grdActuaciones.SelBookmarks.RemoveAll
                    LockWindowUpdate Me.hWnd
                    grdActuaciones.MoveFirst
                    grdActuaciones.MoveRecords i - 1
                    LockWindowUpdate 0&
                    grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
                    cmdB(0).SetFocus
                    Exit Sub
                End If
            End If
        Next i
        
        If intBuscar = 1 Then
            MsgBox "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda.", vbInformation, Me.Caption
            Exit Sub
        Else
            intBuscar = intBuscar - 1
            Exit Sub
        End If
    Else
        blnBuscarOtro = False
    End If
End Sub


Private Sub cmdConsultar_Click()
    Call pActualizar(-1)
End Sub

Private Sub cmdDatosAct_Click()
    Dim msg$, lngNumActPlan&

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Actuaci�n."
    Case 1
        lngNumActPlan = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(0))
        'Acceso a los Datos de la Actuaci�n
        Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
        Call objsecurity.LaunchProcess("PR0502")
    Case Else
        msg = "Debe Ud. seleccionar una �nica Actuaci�n."
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdImprimir_Click()
Dim strWhere As String
    strWhere = " PR0400AN.AD02CODDPTO = " & constDPTO_ANESTESIA
    strWhere = strWhere & " AND PR0400AN.PR37CODESTADO=4 "
    strWhere = strWhere & " AND PR0300.PR03INDCITABLE=0 "
    strWhere = strWhere & " AND PR0400AN.PR04FECFINACT >= TO_DATE('" & dcboDesde.Text & "','DD/MM/YYYY') "
    strWhere = strWhere & " AND PR0400AN.PR04FECFINACT < TO_DATE('" & CStr(DateAdd("d", 1, CDate(dcboHasta.Text))) & "','DD/MM/YYYY') "
    strWhere = strWhere & sqlOrderRpt
    Call Imprimir_API(strWhere, "PR05063.RPT")
End Sub



Private Sub cmdImprimirPeticion2_Click()
Dim vntdata(1) As Variant, i As Integer, sql As String
  
  For i = 0 To grdActuaciones.SelBookmarks.Count - 1
    sql = sql & "PR0400.PR04NUMACTPLAN= " & grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(i)) & " OR "
  Next i
  sql = Left$(sql, Len(sql) - 4)
  vntdata(1) = sql
  Call objsecurity.LaunchProcess("PR2400", vntdata(1))
End Sub
Private Sub cmdImprimirPeticion_Click()
    Dim msg$, strW$, i%
    
    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox msg, vbExclamation, Me.Caption: Exit Sub
    Case 1
        crReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
    Case Else
        crReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
    End Select
    For i = 0 To grdActuaciones.SelBookmarks.Count - 1
        strW = strW & "{PR0457J.PR04NUMACTPLAN}= " & _
        grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(i)) & " OR "
    Next i
    strW = Left$(strW, Len(strW) - 4)
    With crReport1
        .PrinterCopies = 1
        .Destination = crptToPrinter
        .SelectionFormula = "(" & strW & ")"
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Screen.MousePointer = vbHourglass
        .Action = 1
        Screen.MousePointer = vbDefault
    End With
End Sub

Private Sub cmdMantCitasPac_Click()
    Dim msg$, vntdatos(1 To 1) As Variant

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ning�n Paciente."
    Case Else 'vale cualquier actuaci�n del paciente
        'Acceso al Mantenimiento de Citas del Paciente
        vntdatos(1) = grdActuaciones.Columns("Cod. Persona").CellText(grdActuaciones.SelBookmarks(0))
        Call objsecurity.LaunchProcess("CI1024", vntdatos)
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub





Private Sub cmdSalir_Click()
    Unload Me
End Sub



Private Sub dcboDesde_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboHasta.Date = dcboDesde.Date
    End If
End Sub

Private Sub dcboHasta_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboHasta.Date = dcboDesde.Date
    End If
End Sub

Private Sub Form_Load()
    Dim strHoy$
    
    Call pFormatearGrids
    strHoy = strFecha_Sistema
    dcboDesde.Date = strHoy
    dcboHasta.Date = strHoy
    dcboDesde.BackColor = objApp.objUserColor.lngMandatory
    dcboHasta.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub pActualizar(intCol%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strFecPlan$, strHoraPlan$, strCama$, strActAsoc$
    Dim strApel1$, strApel2$
    Dim strCitable$
    Dim qry2 As rdoQuery
    Dim rs2 As rdoResultset
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then 'Opci�n por defecto: Fecha
        If sqlOrder = "" Then
            sqlOrder = " ORDER BY FECHA, PR01DESCORTA, PACIENTE"
            sqlOrderRpt = " ORDER BY PR0400AN.PR04FECFINACT, PR0100AN.PR01DESCORTA, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE "
        End If
        Else
            Select Case UCase(grdActuaciones.Columns(intCol).Caption)
                Case UCase("Fecha"), UCase("Hora"):
                    sqlOrder = " ORDER BY FECHA, PR01DESCORTA, PACIENTE"
                    sqlOrderRpt = " ORDER BY PR0400AN.PR04FECFINACT, PR0100AN.PR01DESCORTA, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE "
                Case UCase("Paciente"):
                    sqlOrder = " ORDER BY PACIENTE, FECHA, PR01DESCORTA"
                    sqlOrderRpt = " ORDER BY CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE, PR0400AN.PR04FECFINACT, PR0100AN.PR01DESCORTA "
                Case UCase("N� Hist."):
                    sqlOrder = " ORDER BY CI22NUMHISTORIA, FECHA, PR01DESCORTA"
                    sqlOrderRpt = " ORDER BY CI2200.CI22NUMHISTORIA, PR0400AN.PR04FECFINACT, PR0100AN.PR01DESCORTA, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE "
                Case UCase("Actuaci�n"):
                    sqlOrder = " ORDER BY PR01DESCORTA, FECHA, PACIENTE"
                    sqlOrderRpt = " ORDER BY PR0100AN.PR01DESCORTA, PR0400AN.PR04FECFINACT, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE "
                Case UCase("Actuaci�n asociada"):
                    sqlOrder = " ORDER BY DESASOCIADA, PR01DESCORTA, FECHA, PACIENTE"
                    sqlOrderRpt = " ORDER BY PR0100AS.PR01DESCORTA, PR0100AN.PR01DESCORTA, PR0400AN.PR04FECFINACT, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE "
                Case UCase("Cama"):
                    sqlOrder = " ORDER BY AD15CODCAMA, FECHA, PR01DESCORTA"
                    sqlOrderRpt = " ORDER BY AD1500.AD15CODCAMA, PR0400AN.PR04FECFINACT, PR0100AN.PR01DESCORTA, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE "
                Case Else: Exit Sub
            End Select
        End If
    
    
    'Fechas
    strFecDesde = dcboDesde.Date
    strFecHasta = dcboHasta.Date

    'vaciado previo del grid y de la colecci�n de b�sqeda
    grdActuaciones.RemoveAll
    Do While cllBuscar.Count > 0: cllBuscar.Remove 1: Loop
    
    'se cambia el scrollbar para evitar el efecto de llenado del grid dejando la �ltima _
    fila vacia durante un cierto rato
    grdActuaciones.ScrollBars = ssScrollBarsNone
        sql = "SELECT"
        sql = sql & " PR04AN.PR04NUMACTPLAN, PR01AN.PR01DESCORTA,"
        sql = sql & " PR01AS.PR01DESCORTA DESASOCIADA, PR04AN.PR04FECFINACT FECHA,"
        sql = sql & " CI2200.CI21CODPERSONA, CI22NUMHISTORIA, CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
        sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
        sql = sql & " GCFN06(AD1500.AD15CODCAMA) AD15CODCAMA,"
        sql = sql & " PR04AN.PR04ASA, PR04AN.PR04OBSERV, PR04AN.PR63CODTIPOANEST"
        sql = sql & " From"
        sql = sql & " PR0400 PR04AN,PR0400 PR04AS,PR6100,PR0300, PR0100 PR01AS,"
        sql = sql & " PR0100 PR01AN, CI2200, AD1500"
        sql = sql & " Where"
        sql = sql & " PR04AN.PR37CODESTADO = 4"
        sql = sql & " AND PR04AN.AD02CODDPTO = 223"
        sql = sql & " AND PR04AN.PR04FECFINACT > TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND PR04AN.PR04FECFINACT < TO_DATE(?,'DD/MM/YYYY HH24:MI')"
        sql = sql & " AND PR04AN.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR03INDCITABLE = 0"
        sql = sql & " AND PR04AN.PR01CODACTUACION= PR01AN.PR01CODACTUACION"
        sql = sql & " AND PR04AN.CI21CODPERSONA = CI2200.CI21CODPERSONA"
        sql = sql & " AND PR04AN.PR03NUMACTPEDI = PR6100.PR03NUMACTPEDI(+)"
        sql = sql & " AND PR6100.PR03NUMACTPEDI_ASO = PR04AS.PR03NUMACTPEDI(+)"
        sql = sql & " AND PR04AS.PR01CODACTUACION= PR01AS.PR01CODACTUACION(+)"
        sql = sql & " AND PR04AN.AD07CODPROCESO  = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND PR04AN.AD01CODASISTENCI  = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = Format(strFecDesde, "dd/mm/yyyy")
            qry(1) = Format(DateAdd("d", 1, strFecHasta), "dd/mm/yyyy")
        Set rs = qry.OpenResultset()
    
    Do While Not rs.EOF
     
        If IsNull(rs!AD15CODCAMA) Then strCama = "" Else strCama = rs!AD15CODCAMA
        If IsNull(rs!DESASOCIADA) Then strActAsoc = "" Else strActAsoc = rs!DESASOCIADA


        grdActuaciones.AddItem rs!fecha & Chr$(9) _
                            & rs!PACIENTE & Chr$(9) _
                            & rs!PR01DESCORTA & Chr$(9) _
                            & strActAsoc & Chr$(9) _
                            & rs!CI22NUMHISTORIA & Chr$(9) _
                            & strCama & Chr$(9) _
                            & rs!PR04ASA & Chr$(9) _
                            & rs!PR63CODTIPOANEST & Chr$(9) _
                            & rs!PR04OBSERV & Chr$(9) _
                            & rs!PR04NUMACTPLAN & Chr$(9) _
                            & rs!CI21CODPERSONA & Chr$(9)
        If IsNull(rs!CI22PRIAPEL) Then strApel1 = "" Else strApel1 = rs!CI22PRIAPEL
        If IsNull(rs!CI22SEGAPEL) Then strApel2 = "" Else strApel2 = rs!CI22SEGAPEL
        cllBuscar.Add rs!CI22NUMHISTORIA & "|" & rs!CI22NOMBRE & "|" & strApel1 & "|" & strApel2 & "|" & rs!CI21CODPERSONA
        rs.MoveNext
    Loop
    rs.Close
'    qry.Close
    
    'se pone el scrollbar en su estado normal
    grdActuaciones.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pFormatearGrids()
    With grdActuaciones
        .Columns(0).Caption = "Fecha"
        .Columns(0).Width = 1700
        .Columns(1).Caption = "Paciente"
        .Columns(1).Width = 3100
        .Columns(2).Caption = "Actuaci�n"
        .Columns(2).Width = 2600
        .Columns(3).Caption = "Actuaci�n asociada"
        .Columns(3).Width = 2000
        .Columns(4).Caption = "N� Hist."
        .Columns(4).Alignment = ssCaptionAlignmentRight
        .Columns(4).Width = 700
        .Columns(5).Caption = "Cama"
        .Columns(5).Width = 600
        .Columns(6).Caption = "Asa"
        .Columns(6).Width = 400
        .Columns(7).Caption = "Tipo"
        .Columns(7).Width = 400
        .Columns(8).Caption = "Incidencias"
        .Columns(8).Width = 3100
        .Columns(9).Caption = "Num. Act."
        .Columns(9).Visible = False
        .Columns(10).Caption = "Cod. Persona"
        .Columns(10).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Function fBlnBuscar() As Boolean
    Dim i%
    For i = 0 To txtBuscar.Count - 1
        If txtBuscar(i).Text <> "" Then fBlnBuscar = True: Exit Function
    Next i
End Function

Private Sub grdActuaciones_Click()
    If grdActuaciones.SelBookmarks.Count > 1 Then
        If grdActuaciones.Columns("N� Hist.").CellText(grdActuaciones.SelBookmarks(0)) <> grdActuaciones.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, Me.Caption
            grdActuaciones.SelBookmarks.Remove grdActuaciones.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub grdActuaciones_HeadClick(ByVal ColIndex As Integer)
    If grdActuaciones.Rows > 0 Then Call pActualizar(ColIndex)
End Sub

Private Sub txtBuscar_Change(Index As Integer)
    cmdB(0).Enabled = False: cmdB(1).Enabled = False
End Sub

Private Sub txtBuscar_GotFocus(Index As Integer)
    txtBuscar(Index).SelStart = 0: txtBuscar(Index).SelLength = Len(txtBuscar(Index).Text)
End Sub

Private Sub txtBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub grdActuaciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub
