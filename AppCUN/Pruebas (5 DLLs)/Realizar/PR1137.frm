VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmPeticionesPendientesNW 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recibir al paciente. Actuaciones pendientes"
   ClientHeight    =   8595
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11850
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   11850
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdIndSanit 
      Caption         =   "Ind &Sanitarios"
      Height          =   375
      Left            =   3840
      TabIndex        =   30
      Top             =   8040
      Width           =   1170
   End
   Begin Crystal.CrystalReport crtReport1 
      Left            =   60
      Top             =   6960
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame fraFrame4 
      Caption         =   "Impresos"
      ForeColor       =   &H00800000&
      Height          =   660
      Left            =   6300
      TabIndex        =   23
      Top             =   7860
      Width           =   5475
      Begin VB.CommandButton cmdEtiquetas 
         Caption         =   "&Etiquetas"
         Height          =   375
         Left            =   4440
         TabIndex        =   29
         Top             =   175
         Width           =   975
      End
      Begin VB.CommandButton cmdComun 
         Caption         =   "&Imprimir"
         Height          =   375
         Index           =   4
         Left            =   3360
         TabIndex        =   27
         Top             =   195
         Width           =   975
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Petici�n"
         Height          =   255
         Index           =   2
         Left            =   2280
         TabIndex        =   26
         Top             =   240
         Width           =   975
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Programa"
         Height          =   255
         Index           =   1
         Left            =   1200
         TabIndex        =   25
         Top             =   240
         Width           =   975
      End
      Begin VB.CheckBox Check1 
         Caption         =   "H.Filiaci�n"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Rea&lizaci�n"
      Height          =   375
      Index           =   3
      Left            =   5100
      TabIndex        =   22
      Top             =   8040
      Width           =   1170
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Vision &Global"
      Height          =   375
      Index           =   2
      Left            =   2580
      TabIndex        =   21
      Top             =   8040
      Width           =   1170
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "&Datos"
      Height          =   375
      Index           =   1
      Left            =   1320
      TabIndex        =   19
      Top             =   8040
      Width           =   1170
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Rec. &Previstos"
      Height          =   375
      Index           =   0
      Left            =   60
      TabIndex        =   18
      Top             =   8040
      Width           =   1170
   End
   Begin VB.Frame Frame3 
      Caption         =   "Actuaciones en Espera"
      ForeColor       =   &H00C00000&
      Height          =   3330
      Left            =   60
      TabIndex        =   13
      Top             =   1080
      Width           =   11745
      Begin VB.CheckBox chkInformes 
         Caption         =   "Informes Atrasados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   10440
         TabIndex        =   32
         Top             =   1680
         Width           =   1275
      End
      Begin VB.CheckBox chksinfecha 
         Caption         =   "Sin fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10440
         TabIndex        =   20
         Top             =   1200
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnularEnEspera 
         Caption         =   "&Anular"
         Height          =   375
         Left            =   10500
         TabIndex        =   17
         Top             =   720
         Width           =   1035
      End
      Begin VB.CommandButton cmdRecibir 
         Caption         =   "&Recibir"
         Height          =   375
         Left            =   10500
         TabIndex        =   15
         Top             =   225
         Width           =   1035
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdEnEspera 
         Height          =   3015
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowDelete     =   -1  'True
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   5318
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Actuaciones Pendientes"
      ForeColor       =   &H00C00000&
      Height          =   3330
      Left            =   60
      TabIndex        =   11
      Top             =   4440
      Width           =   11715
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver Rec."
         Height          =   375
         Left            =   10500
         TabIndex        =   28
         Top             =   240
         Width           =   1035
      End
      Begin VB.CommandButton cmdAnularPend 
         Caption         =   "A&nular"
         Height          =   375
         Left            =   10500
         TabIndex        =   16
         Top             =   720
         Width           =   1035
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdPendientes 
         Height          =   3015
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   10275
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18124
         _ExtentY        =   5318
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10560
      TabIndex        =   10
      Top             =   660
      Width           =   1155
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dpto. / Recurso / Fechas"
      ForeColor       =   &H00C00000&
      Height          =   1035
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   10395
      Begin VB.CheckBox chkSinAsist 
         Alignment       =   1  'Right Justify
         Caption         =   "Ver actuaciones sin asistencia"
         Height          =   240
         Left            =   525
         TabIndex        =   31
         Top             =   675
         Width           =   2490
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   375
         Left            =   9240
         TabIndex        =   2
         Top             =   480
         Width           =   1035
      End
      Begin ComctlLib.ListView lvwRec 
         Height          =   795
         Left            =   3975
         TabIndex        =   1
         Top             =   180
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   1402
         View            =   3
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin SSDataWidgets_B.SSDBCombo sscboDpto 
         Height          =   315
         Left            =   825
         TabIndex        =   3
         Top             =   270
         Width           =   2235
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4974
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3942
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDesde 
         Height          =   285
         Left            =   7440
         TabIndex        =   6
         Tag             =   "Fecha Desde"
         ToolTipText     =   "Fecha Desde"
         Top             =   180
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcHasta 
         Height          =   285
         Left            =   7440
         TabIndex        =   7
         Tag             =   "Fecha Hasta"
         ToolTipText     =   "Fecha Hasta"
         Top             =   600
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   1
         Left            =   6840
         TabIndex        =   9
         Top             =   660
         Width           =   465
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   7
         Left            =   6780
         TabIndex        =   8
         Top             =   240
         Width           =   510
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Dpto."
         Height          =   315
         Index           =   0
         Left            =   225
         TabIndex        =   5
         Top             =   270
         Width           =   555
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Recurso"
         Height          =   195
         Index           =   1
         Left            =   3150
         TabIndex        =   4
         Top             =   300
         Width           =   795
      End
   End
End
Attribute VB_Name = "frmPeticionesPendientesNW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intDptoSel As Integer, strRecSel$
Dim lngrec As Long
Dim strMeCaption As String

Private Sub chkInformes_Click()
    If chkInformes.Value = 0 Then
        
        Frame1.Enabled = True
        ssgrdPendientes.RemoveAll
        ssgrdEnEspera.RemoveAll
    Else
        chksinfecha.Value = 0
        Frame1.Enabled = False
        ssgrdEnEspera.RemoveAll
        Call pCargarInformesAtrasados(-1)
    End If
End Sub

Private Sub chksinfecha_Click()
    If chksinfecha.Value = 0 Then
        Frame1.Enabled = True
        ssgrdPendientes.RemoveAll
        ssgrdEnEspera.RemoveAll
    Else
        chkInformes.Value = 0
        Frame1.Enabled = False
        ssgrdEnEspera.RemoveAll
        pCargarActuacionesSinFecha
    End If
End Sub

Private Sub cmdAnularEnEspera_Click()
    Call pAnularAct(ssgrdEnEspera)
End Sub

Private Sub cmdAnularPend_Click()
 Call pAnularAct(ssgrdPendientes)
End Sub
Private Sub cmdIndSanit_Click()
Dim vntdata
Dim ssGrid As SSDBGrid
 If ssgrdPendientes.SelBookmarks.Count > 0 Then
    Set ssGrid = ssgrdPendientes
  ElseIf ssgrdEnEspera.SelBookmarks.Count > 0 Then
    Set ssGrid = ssgrdEnEspera
  Else
    MsgBox "Debe seleccionar alguna prueba.", vbExclamation, Me.Caption
    Exit Sub
  End If
    If ssGrid.SelBookmarks.Count > 1 Then
        MsgBox "Esta opci�n no est� disponible para una selecci�n m�ltiple", vbExclamation, Me.Caption
        Exit Sub
    End If
    vntdata = ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(0))
    Call objsecurity.LaunchProcess("CI2052", vntdata)
End Sub

Private Sub cmdEtiquetas_Click()
Dim ssGrid As SSDBGrid
        
  If ssgrdPendientes.SelBookmarks.Count > 0 Then
    Set ssGrid = ssgrdPendientes
  ElseIf ssgrdEnEspera.SelBookmarks.Count > 0 Then
    Set ssGrid = ssgrdEnEspera
  Else
    MsgBox "Debe seleccionar alguna prueba.", vbExclamation, Me.Caption
    Exit Sub
  End If
  
  If ssGrid.SelBookmarks.Count = 1 Then
    Dim vntdata(1)
    vntdata(1) = ssGrid.Columns("Persona").CellText(ssGrid.SelBookmarks(0))
    Call objsecurity.LaunchProcess("PR4000", vntdata)
  Else
    MsgBox "Debe seleccionar una �nica prueba.", vbExclamation, Me.Caption
  End If

End Sub

Private Sub dtcDesde_Change()
  On Error Resume Next
  If dtcDesde.Date <> "" Then
    If CDate(dtcHasta.Date) < CDate(dtcDesde.Date) Then
      dtcHasta.Date = dtcDesde.Date
      If Err > 0 Then dtcDesde.Date = dtcHasta.Date
    End If
  End If

End Sub

Private Sub dtcDesde_Click()
  On Error Resume Next
  If dtcDesde.Date <> "" Then
    If CDate(dtcHasta.Date) < CDate(dtcDesde.Date) Then
      dtcHasta.Date = dtcDesde.Date
      If Err > 0 Then dtcDesde.Date = dtcHasta.Date
    End If
  End If

End Sub

Private Sub Form_Load()
    Dim intDpto As Integer
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    strMeCaption = "Peticiones Pendientes"
    
    If objPipe.PipeExist("PR_dpto") Then
        intDpto = objPipe.PipeGet("PR_dpto")
        Call objPipe.PipeRemove("PR_dpto")
    End If

    'se cargan los departamentos realizadores a los que tiene acceso el usuario
    sql = "SELECT AD02CODDPTO, AD02DESDPTO"
    sql = sql & " FROM AD0200"
    sql = sql & " WHERE AD02CODDPTO IN ("
    sql = sql & " SELECT AD02CODDPTO FROM AD0300"
    sql = sql & " WHERE SG02COD = ?"
    sql = sql & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
    sql = sql & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " ORDER BY AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objsecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        intDptoSel = rs!AD02CODDPTO
        sscboDpto.Text = rs!AD02DESDPTO
        Do While Not rs.EOF
            If intDpto = rs!AD02CODDPTO Then
                intDptoSel = rs!AD02CODDPTO
                sscboDpto.Text = rs!AD02DESDPTO
            End If
            sscboDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
            rs.MoveNext
        Loop
    End If
     Me.Caption = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: TODOS"
    rs.Close
    qry.Close
    
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ColumnHeaders.Add , , , 2000
    Call pCargarRecursos(intDptoSel)
    
    'se formatean los grids
    Call pFormatearGrids
   
End Sub
Private Sub pCargarRecursos(intDptoSel%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim item As ListItem
    
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ListItems.Clear
    Set item = lvwRec.ListItems.Add(, , "TODOS")
    item.Tag = 0
    item.Selected = True

    sql = "SELECT AG11CODRECURSO, AG11DESRECURSO"
    sql = sql & " FROM AG1100"
    sql = sql & " WHERE AD02CODDPTO = ?"
    sql = sql & " AND SYSDATE BETWEEN AG11FECINIVREC AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        ''sscboRecurso.AddItem rs!AG11CODRECURSO & Chr$(9) & rs!AG11DESRECURSO
        Set item = lvwRec.ListItems.Add(, , rs!AG11DESRECURSO)
        item.Tag = rs!AG11CODRECURSO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub
Private Sub cmdConsultar_Click()
    Call pConsultar
End Sub

Private Sub cmdRecibir_Click()
    Call pRecibir
End Sub

Private Sub cmdVolver_Click()
 pVolverRecibir
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub pRecibir()
'Introduce la fecha de entrada en cola
' en los pacientes en espera y pasa la actuacin de un grid a otro

Dim sql$, qry As rdoQuery, strAhora$
    Dim lngNumActPlan&, i%, PrimeraVez As Integer, nAsist As Long
    Dim rs As rdoResultset
    Dim stractpedi As String
'    Dim strAhora$
    
    'se comprueba que se ha seleccionado alguna actuaci�n de las en espera
    If ssgrdEnEspera.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    strAhora = strFechaHora_Sistema
    PrimeraVez = True
    For i = 0 To ssgrdEnEspera.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdEnEspera.Columns("Num. Act.").CellText(ssgrdEnEspera.SelBookmarks(i))
            'COMPRUEBA SI TIENE PROCESO ASISTENCIA
        sql = "SELECT PR0400.AD07CODPROCESO, PR0400.AD01CODASISTENCI, PR0400.PR03NUMACTPEDI, " _
            & "PR0100.PR12CodActividad " _
            & "FROM PR0400, PR0100 WHERE " _
            & "PR0400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
            & "PR0400.PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
        Set rs = qry.OpenResultset(rdOpenForwardOnly)
        If IsNull(rs(0)) Then
            MsgBox "La actuaci�n no tiene responsable. Debe asociar el responsable desde el proceso de 'Admitir paciente'", vbExclamation, "Error"
            Exit Sub
        End If
       If IsNull(rs(1)) Then
          If rs(3) = constACTIV_TRATAMIENTO Or rs(3) = constACTIV_PRUEBA Then
            nAsist = AbrirAsistencia(CLng(ssgrdEnEspera.Columns("Persona").CellText(ssgrdEnEspera.SelBookmarks(i))), _
                 CLng(ssgrdEnEspera.Columns("Proceso").CellText(ssgrdEnEspera.SelBookmarks(i))), lngNumActPlan)
            If nAsist = 0 Then Exit Sub
            'ssgrdEnEspera.Columns("Asistencia").CellText(ssgrdEnEspera.SelBookmarks(i)) = nAsist
          Else
            MsgBox "La actuaci�n no tiene asociada ninguna asistencia. El paciente debe pasar por la Unidad de Coordinaci�n para abrir hojas.", vbInformation, Me.Caption
            Exit Sub
          End If
        End If
        If PrimeraVez = True Then
          If MsgBox("�Desea imprimir la hoja de filiaci�n?", vbYesNo + vbQuestion, "Imprimir") = vbYes Then Call pImprimir(ssgrdEnEspera, 1)
          PrimeraVez = False
        End If
        stractpedi = rs!PR03NUMACTPEDI
        If rs(3) = constACTIV_INFORME Then
            If ssgrdEnEspera.Columns("Cama").CellText(ssgrdEnEspera.SelBookmarks(i)) <> "" Then
                Call objPipe.PipeSet("PR03NUMACTPEDI", stractpedi)
                frmDoctorInforme.Show vbModal
            End If
        End If
        'se anota la fecha-hora de entrada en cola en la base de datos
        sql = "UPDATE PR0400 SET PR04FECENTRCOLA = TO_DATE(?,'DD/MM/YYYY HH24:MI') "
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strAhora
        qry(1) = lngNumActPlan
        qry.Execute
        pArrancarAsociadas (stractpedi)
        
        'se a�ade la actuaci�n al grid de actuaciones pendientes
        ssgrdPendientes.AddItem ssgrdEnEspera.Columns("Fecha Prog.").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdEnEspera.Columns("Actuaci�n").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdEnEspera.Columns("N� Hist.").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdEnEspera.Columns("Paciente").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & Format(strAhora, "dd/mm/yy hh:mm") & Chr$(9) _
                                & ssgrdEnEspera.Columns("Cons.").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdEnEspera.Columns("Num. Act.").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdEnEspera.Columns("Est. Muestra").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & " " & Chr$(9) & " " & Chr$(9) _
                                & ssgrdEnEspera.Columns("Persona").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdEnEspera.Columns("Proceso").CellText(ssgrdEnEspera.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdEnEspera.Columns("Asistencia").CellText(ssgrdEnEspera.SelBookmarks(i))
    
    Next i
    
    'se quitan las actuaciones del grid de las actuaciones pendientes
'    ssgrdEnEspera.AllowDelete
    ssgrdEnEspera.DeleteSelected
End Sub
Private Sub pArrancarAsociadas(strNumActPedi As String)
'Chequeo de las pruebas asociadas de anestesia para
'la prueba recibida
'La recibiremos automaticamente
    
  Dim sStmSql     As String
  Dim rstAsocia   As rdoResultset
  Dim sStmUpd     As String
      
    Screen.MousePointer = vbHourglass
    sStmSql = "SELECT PR03NUMACTPEDI FROM PR6100 "
    sStmSql = sStmSql & "WHERE PR03NUMACTPEDI_ASO = " & strNumActPedi
    Set rstAsocia = objApp.rdoConnect.OpenResultset(sStmSql)
    While Not rstAsocia.EOF
        sStmUpd = "UPDATE PR0400 SET PR04FECENTRCOLA=(SELECT SYSDATE FROM DUAL) "
        sStmUpd = sStmUpd & "WHERE PR03NUMACTPEDI = " & rstAsocia.rdoColumns(0).Value
        objApp.rdoConnect.Execute sStmUpd
        rstAsocia.MoveNext
    Wend
    Screen.MousePointer = vbDefault
End Sub

Private Sub pFormatearGrids()
    With ssgrdEnEspera
        .Columns(0).Caption = "Fecha Prog."
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Actuaci�n"
        .Columns(1).Width = 2400
        .Columns(2).Caption = "N� Hist."
        .Columns(2).Alignment = ssCaptionAlignmentRight
        .Columns(2).Width = 700
        .Columns(3).Caption = "Paciente"
        .Columns(3).Width = 3000
        .Columns(4).Caption = "Cama"
        .Columns(4).Width = 600
        .Columns(5).Caption = "Recurso"
        .Columns(5).Width = 1800
        .Columns(6).Caption = "Num. Act."
        .Columns(6).Width = 0
        .Columns(6).Visible = False
        .Columns(7).Caption = "Est. Muestra"
        .Columns(7).Width = 0
        .Columns(7).Visible = False
        .Columns(8).Caption = "Cons."
        .Columns(8).Width = 0
        .Columns(8).Visible = False
        .Columns(9).Caption = "Persona"
        .Columns(9).Width = 0
        .Columns(9).Visible = False
        .Columns(10).Caption = "Proceso"
        .Columns(10).Width = 0
        .Columns(10).Visible = False
        .Columns(11).Caption = "Asistencia"
        .Columns(11).Width = 0
        .Columns(11).Visible = False
        .Columns(12).Caption = "Indicaciones"
        .Columns(12).Width = 7000
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    With ssgrdPendientes
        .Columns(0).Caption = "Fecha Prog."
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Actuaci�n"
        .Columns(1).Width = 2400
        .Columns(2).Caption = "N� Hist."
        .Columns(2).Alignment = ssCaptionAlignmentRight
        .Columns(2).Width = 700
        .Columns(3).Caption = "Paciente"
        .Columns(3).Width = 3000
        .Columns(4).Caption = "Entrada cola"
        .Columns(4).Width = 1300
        .Columns(5).Caption = "Cons."
        .Columns(5).Width = 600
        .Columns(6).Caption = "Num. Act."
        .Columns(6).Alignment = ssCaptionAlignmentRight
        .Columns(6).Width = 0
        .Columns(6).Visible = False
        .Columns(7).Caption = "Est. Muestra"
        .Columns(7).Width = 0
        .Columns(7).Visible = False
        .Columns(8).Caption = "Cama"
        .Columns(8).Width = 0
        .Columns(8).Visible = False
        .Columns(9).Caption = "Recurso"
        .Columns(9).Width = 0
        .Columns(9).Visible = False
        .Columns(10).Caption = "Persona"
        .Columns(10).Width = 0
        .Columns(10).Visible = False
        .Columns(11).Caption = "Proceso"
        .Columns(11).Width = 0
        .Columns(11).Visible = False
        .Columns(12).Caption = "Asistencia"
        .Columns(12).Width = 0
        .Columns(12).Visible = False
        .Columns(13).Caption = "Indicaciones"
        .Columns(13).Width = 7000
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub pVaciarGrids()
    ssgrdPendientes.RemoveAll
    ssgrdEnEspera.RemoveAll
End Sub

Private Sub lvwRec_Click()
lngrec = lvwRec.SelectedItem.Index
End Sub

Private Sub sscboDpto_Click()
    If intDptoSel <> sscboDpto.Columns(0).Value Then Call pVaciarGrids
    intDptoSel = sscboDpto.Columns(0).Value
    Me.Caption = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: TODOS"
    Call pCargarRecursos(intDptoSel)
End Sub

Private Sub ssgrdEnEspera_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdEnEspera_Click()
    ssgrdPendientes.SelBookmarks.RemoveAll
    
    If ssgrdEnEspera.SelBookmarks.Count > 1 Then
        If ssgrdEnEspera.Columns("N� Hist.").CellText(ssgrdEnEspera.SelBookmarks(0)) <> ssgrdEnEspera.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdEnEspera.SelBookmarks.RemoveAll
            'ssgrdEnEspera.SelBookmarks.Remove ssgrdEnEspera.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdEnEspera_HeadClick(ByVal ColIndex As Integer)
    If ssgrdEnEspera.Rows > 0 Then
        If chkInformes.Value = 0 Then
            Call pCargarActEnEspera(ColIndex)
        Else
            Call pCargarInformesAtrasados(ColIndex)
        End If
    End If
End Sub


Private Sub ssgrdPendientes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub

Private Sub ssgrdPendientes_Click()
    ssgrdEnEspera.SelBookmarks.RemoveAll
    
    If ssgrdPendientes.SelBookmarks.Count > 1 Then
        If ssgrdPendientes.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(0)) <> ssgrdPendientes.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdPendientes.SelBookmarks.RemoveAll
            'ssgrdPendientes.SelBookmarks.Remove ssgrdPendientes.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub pVolverRecibir()
'quita la fecha de entrada en cola de las actuaciones seleccionadas
Dim sql$, qry As rdoQuery, strAhora$
    Dim lngNumActPlan&, i%
'    Dim strAhora$
    
    'se comprueba que se ha seleccionado alguna actuaci�n de las en espera
    If ssgrdPendientes.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    strAhora = strFechaHora_Sistema
    For i = 0 To ssgrdPendientes.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i))
        
        'se anota la fecha-hora de entrada en cola en la base de datos
        sql = "UPDATE PR0400 SET PR04FECENTRCOLA = NULL "
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
        qry.Execute
        
        'se a�ade la actuaci�n al grid de actuaciones pendientes
        ssgrdEnEspera.AddItem ssgrdPendientes.Columns("Fecha Prog.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Actuaci�n").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Paciente").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Cama").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & " " & Chr$(9) _
            & ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Est. Muestra").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Cons.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Persona").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Proceso").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Asistencia").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
            & ssgrdPendientes.Columns("Indicaciones").CellText(ssgrdPendientes.SelBookmarks(i))
    Next i
    
    'se quitan las actuaciones del grid de las actuaciones pendientes
    ssgrdPendientes.DeleteSelected
End Sub

Private Sub pAnularAct(ssgrd As SSDBGrid)
'**************************************************************************************
'*  Anula una actuaci�n planificada cambiando su estado a anulada
'**************************************************************************************
    Dim strCancel$, sql$, qry As rdoQuery, i%, lngNumActPlan&
    
    Select Case ssgrd.SelBookmarks.Count
    Case 0
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    Case 1
        sql = "Si Ud. anula la actuaci�n ya no estar� disponible para ser realizada." & Chr$(13)
        sql = sql & "Indique el motivo por el que se va a anular la actuaci�n:"
    Case Is > 1
        sql = "Si Ud. anula las actuaciones ya no estar�n disponibles para ser realizadas." & Chr$(13)
        sql = sql & "Indique el motivo por el que se van a anular las actuaciones:"
    End Select
    strCancel = Trim$(InputBox(sql, "Anular Actuaci�n"))
    If strCancel = "" Then
        sql = "La actuaci�n NO ha sido anulada: hace falta indicar un motivo."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    For i = 0 To ssgrd.SelBookmarks.Count - 1
        lngNumActPlan = ssgrd.Columns("Num. Act.").CellText(ssgrd.SelBookmarks(i))
        
        sql = "UPDATE PR0400 SET PR04DESMOTCAN = ?,"
        sql = sql & " PR37CODESTADO = ?,"
        sql = sql & " PR04FECCANCEL = SYSDATE,"
        sql = sql & " PR04FECENTRCOLA = ?,"
        sql = sql & " PR04FECINIACT = ?,"
        'En principio siempre PR04FECFINACT = NULL ya que nunca estar� terminada, pero por si acaso...
        sql = sql & " PR04FECFINACT = ?"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        If Len(strCancel) > 50 Then qry(0) = Left$(strCancel, 50) Else qry(0) = strCancel
            qry(1) = 6
            qry(2) = Null
            qry(3) = Null
            qry(4) = Null
            qry(5) = lngNumActPlan
        qry.Execute
        qry.Close
        sql = "UPDATE CI0100 SET CI01SITCITA='2' WHERE PR04NUMACTPLAN = ?"
        sql = sql & " AND CI01SITCITA ='1'"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
        qry.Execute
        qry.Close
        Call pAnularAsociadas(lngNumActPlan)
    Next i
    
    'se elimina la actuaci�n del Grid
    ssgrd.DeleteSelected
End Sub

Private Sub cmdComun_Click(Index As Integer)
    Dim ssGrid As SSDBGrid, lngNumActPlan&, strHistoria$
    cmdComun(Index).Enabled = False
    If Index <> 3 Then
        If ssgrdPendientes.SelBookmarks.Count > 0 Then
            Set ssGrid = ssgrdPendientes
        ElseIf ssgrdEnEspera.SelBookmarks.Count > 0 Then
            Set ssGrid = ssgrdEnEspera
        Else
            MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, strMeCaption
            cmdComun(Index).Enabled = True
            Exit Sub
        End If
    
        If ssGrid.SelBookmarks.Count = 1 Then
            lngNumActPlan = ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(0))
        Else
            MsgBox "Debe Ud. seleccionar una �nica Actuaci�n.", vbExclamation, strMeCaption
            cmdComun(Index).Enabled = True
            Exit Sub
        End If
    End If
    Select Case Index
    Case 0 'Ver recursos previstos
        Call pVerRecursos(lngNumActPlan)
    Case 1 'Ver datos / cuestionario
        Call pVerDatosActuacion(lngNumActPlan)
    Case 2
        Call pVisionGlobal(ssGrid.Columns("Persona").CellText(ssGrid.SelBookmarks(0)))
    Case 3
        pRealActuaciones
    Case 4
        Call pImprimir(ssGrid)
    End Select
    cmdComun(Index).Enabled = True
End Sub

Private Sub pVerRecursos(lngNumActPlan&)
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    frmConsultaRecursos.Show vbModal
    Set frmConsultaRecursos = Nothing
End Sub

Private Sub pVerDatosActuacion(lngNumActPlan&, Optional blnMostrarSoloSiHayDatos As Boolean)
'**************************************************************************************
'*  Llama a la pantalla que muestra los datos (observ., indic., intruccines...) de la
'*  actuaci�n y los cuestionarios (cuestionarios + restricciones)
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = True significa que s�lo hay que mostrar la
'*  pantalla en el caso de que exista alguna observ., indic. o pregunta en el cuestionario.
'*  En este caso se hace previamente el Load de la pantalla, la cual devolver� en un Pipe
'*  la informaci�n necesaria para saber si hay que hacer el Show o el Unload
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = False se muestra la pantalla directamente
'**************************************************************************************

    'se establece el Pipe con el n� de actuaci�n planificada
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    
    'se mira la opci�n seleccionada: cargar pantalla previamente o mostrar directamente
    If blnMostrarSoloSiHayDatos Then 'cargar pantalla previamente
        Call objPipe.PipeSet("PR_VerSoloSiHayDatos", True)
        'se elimina el posible Pipe devuelto por frmObservAct en operaciones previas
        If objPipe.PipeExist("PR_frmObservAct") Then Call objPipe.PipeRemove("PR_frmObservAct")
        'se carga en memoria la pantalla
        Load frmObservAct
        'se mira si hay que mostrarla o descargarla
        If objPipe.PipeExist("PR_frmObservAct") Then
            If objPipe.PipeGet("PR_frmObservAct") = True Then
                frmObservAct.Show vbModal
            Else
                Unload frmObservAct
            End If
            Call objPipe.PipeRemove("PR_frmObservAct")
        Else
            Unload frmObservAct
        End If
        Call objPipe.PipeRemove("PR_VerSoloSiHayDatos")
    Else 'mostrar pantalla directamente
        frmObservAct.Show vbModal
        Set frmObservAct = Nothing
    End If
End Sub

Private Sub pCargarActuacionesSinFecha()
    Dim sql As String
    Dim strConsent As String
    Dim rs As rdoResultset
    Dim qry As rdoQuery
    
    sql = "SELECT PR0100.PR01DESCORTA,"
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & " ' ' DESREC, PR0400.PR04NUMACTPLAN, PR0400.PR56CODESTMUES,"
    sql = sql & " PR0100.PR01INDCONSFDO,PR0300.PR03INDCONSFIRM, PR0400.CI21CODPERSONA"
    sql = sql & " FROM"
    sql = sql & " PR0400,PR0300, PR0100, CI2200, AD1500"
    sql = sql & " WHERE PR0400.PR04FECPLANIFIC IS NULL "
    sql = sql & " AND PR0400.AD01CODASISTENCI IS NOT NULL"
    sql = sql & " AND PR0400.AD07CODPROCESO IS NOT NULL"
    sql = sql & " AND PR0400.PR37CODESTADO = 1"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NULL"
    sql = sql & " AND PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR03INDCITABLE = 0"
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " ORDER BY PACIENTE"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset()
    
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        ssgrdEnEspera.AddItem " " & Chr$(9) _
                    & rs!PR01DESCORTA & Chr$(9) _
                    & rs!CI22NUMHISTORIA & Chr$(9) _
                    & rs!PACIENTE & Chr$(9) _
                    & rs!Cama & Chr$(9) _
                    & rs!DESREC & Chr$(9) _
                    & rs!PR04NUMACTPLAN & Chr$(9) _
                    & rs!PR56CODESTMUES & Chr$(9) _
                    & strConsent & Chr$(9) _
                    & rs!CI21CODPERSONA
        rs.MoveNext
    Loop
End Sub
Private Sub pVisionGlobal(lngCodPersona As Long)
'Dim vntData(1) As Variant
'    vntData(1) = lngCodPersona
'    Call objsecurity.LaunchProcess("AD1126", vntData)
  
  ReDim arData(1 To 2) As Variant
  arData(1) = lngCodPersona
  arData(2) = 2
  Call objsecurity.LaunchProcess("HC02", arData())

End Sub

Private Sub pRealActuaciones()
    Dim vntdata(1 To 2) As Variant
    vntdata(1) = intDptoSel
    vntdata(2) = lngrec
    glngcodpersona = 1
    Call objsecurity.LaunchProcess("PR0500", vntdata)
    Call pConsultar
    
End Sub


Private Sub pImprimir(ssgrid1 As SSDBGrid, Optional intF As Integer)

'Imprimimos los reports que esten seleccionados
Dim strReportName(3) As String
Dim intI As Integer
Dim strWhere$, sql As String
Dim i%


strReportName(0) = "filiacion2.rpt"
strReportName(1) = "propac1.rpt"
strReportName(2) = "Peticion.rpt"


For intI = 0 To 2
    If Check1(intI).Value = 1 Or (intI = 0 And intF = 1) Then
        crtReport1.ReportFileName = objApp.strReportsPath & strReportName(intI)
        strWhere = ""
        Select Case intI
            Case 0
                strWhere = "({Ad0823j.AD01CODASISTENCI}=" & ssgrid1.Columns("Asistencia").CellText(ssgrid1.SelBookmarks(0)) & _
                " AND {Ad0823j.AD07CODPROCESO}=" & ssgrid1.Columns("Proceso").CellText(ssgrid1.SelBookmarks(0)) & ")"
            Case 1
                strWhere = "{PR0460J.CI21CODPERSONA}= " & ssgrid1.Columns("Persona").CellText(ssgrid1.SelBookmarks(0))
            Case 2
                If ssgrid1.SelBookmarks.Count = 1 Then
                  crtReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
                Else
                  crtReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
                End If
                For i = 0 To ssgrid1.SelBookmarks.Count - 1
                    strWhere = strWhere & "{PR0457J.PR04NUMACTPLAN}= " & ssgrid1.Columns("Num. Act.").CellText(ssgrid1.SelBookmarks(i)) & " OR "
                Next i
                strWhere = Left$(strWhere, Len(strWhere) - 4)
'            Dim vntdata(1) As Variant
'            For i = 0 To ssgrid1.SelBookmarks.Count - 1
'              SQL = SQL & "PR0400.PR04NUMACTPLAN= " & ssgrid1.Columns("Num. Act.").CellText(ssgrid1.SelBookmarks(i)) & " OR "
'            Next i
'            SQL = Left$(SQL, Len(SQL) - 4)
'            vntdata(1) = SQL
'            Call objsecurity.LaunchProcess("PR2400", vntdata(1))
'            Exit Sub
                
        End Select
        With crtReport1
            .PrinterCopies = 1
            .Destination = crptToPrinter
            .SelectionFormula = "(" & strWhere & ")"
            .Connect = objApp.rdoConnect.Connect
            .DiscardSavedData = True
            Screen.MousePointer = vbHourglass
            .Action = 1
            Screen.MousePointer = vbDefault
       End With
    End If
Next intI
    
End Sub

Private Sub pConsultar()
    Dim i%
    strRecSel = ""
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected Then
            strRecSel = strRecSel & lvwRec.ListItems(i).Tag & ","
            If i = 1 Then Exit For 'TODOS
        End If
    Next i
    If strRecSel <> "" Then
        strRecSel = Left$(strRecSel, Len(strRecSel) - 1)
        Call pCargarActEnEspera(-1)
        Call pCargarActPendientes(-1)
    Else
        MsgBox "No se ha seleccionado ning�n recurso.", vbExclamation, strMeCaption
    End If
End Sub

Private Sub ssgrdPendientes_HeadClick(ByVal ColIndex As Integer)
    If ssgrdPendientes.Rows > 0 Then Call pCargarActPendientes(ColIndex)
End Sub

Private Sub pCargarActEnEspera(intCol%)
'**************************************************************************************
'* Muestra las actuaciones en Espera con proceso,asistencia
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strConsent$
    Static sqlOrder$
    Dim str1$, str2$, str3$
    
    Screen.MousePointer = vbHourglass
    ssgrdEnEspera.RemoveAll
    ssgrdEnEspera.ScrollBars = ssScrollBarsNone

    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PACIENTE, FECHA" 'Opci�n por defecto: Paciente
    Else
        Select Case UCase(ssgrdEnEspera.Columns(intCol).Caption)
        Case UCase("Fecha Prog."): sqlOrder = " ORDER BY FECHA, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, FECHA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, FECHA"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, FECHA"
        Case Else: Exit Sub
        End Select
    End If
        
    'ACTUACIONES EN ESPERA
    sql = "SELECT /*+ ORDERED */" 'PLANIFICADAS NO CITABLES
    sql = sql & " PR0400.PR04FECPLANIFIC FECHA,"
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " PR0100.PR01DESCORTA , "
    sql = sql & " PR0400.PR56CODESTMUES, PR0400.AD02CODDPTO, PR0400.PR37CODESTADO,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0400.CI21CODPERSONA, PR0400.AD07CODPROCESO,"
    sql = sql & " PR0400.AD01CODASISTENCI,  GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    sql = sql & " 0 CODREC,' ' DESREC,"
    sql = sql & " PR59DESMOVPAC, PR60DESNTRAPAC, PR04DESINDSANIT"
    If strRecSel <> "0" Then 'SI HAY RECURSO SELECCIONADO
        sql = sql & " FROM PR0400, PR0100, PR0300, PR1400, CI2200, AD1500, PR5900, PR6000"
    Else
        sql = sql & " FROM PR0400, PR0100, PR0300, CI2200, AD1500, PR5900, PR6000"
    End If
    sql = sql & " Where PR0400.PR04FECPLANIFIC >= TO_DATE(?,'DD/MM/YYYY')"
    sql = sql & " AND PR0400.PR04FECPLANIFIC < TO_DATE(?,'DD/MM/YYYY')"
    If strRecSel <> "0" Then
        sql = sql & " AND PR1400.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR1400.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sql = sql & " AND PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI"
    sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= '" & constCAMAOCUPADA & "'"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NULL"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_PLANIFICADA
    ' Alberto: Carga las actuaciones sin asistencia
    If chkSinAsist.Value = vbUnchecked Then ' Se muestran las que tienen asistencia
      sql = sql & " AND  PR0400.AD01CODASISTENCI IS NOT NULL"
    Else  ' Se muestran todas.
      sql = sql & " AND  (PR0400.AD01CODASISTENCI IS NOT NULL OR "
      sql = sql & "      (PR0400.AD01CODASISTENCI IS NULL AND "
      sql = sql & "      PR0100.PR12CodActividad IN (" & constACTIV_PRUEBA & ", " & constACTIV_TRATAMIENTO & ")))"
    End If
    ' **********************************************
    sql = sql & " AND PR0400.AD02CODDPTO = ? "
    sql = sql & " AND PR0300.PR03INDCITABLE = 0" 'NO CITABLE
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_HOSPITALIZACION
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_INTERVENCION
    sql = sql & " AND (PR01INDINSTRREA = 0 OR (PR01INDINSTRREA = -1 AND PR04DESINSTREA IS NOT NULL))"
    sql = sql & " AND PR5900.PR59CODMOVPAC (+)= PR0400.PR59CODMOVPAC"
    sql = sql & " AND PR6000.PR60CODNTRAPAC (+)= PR0400.PR60CODNTRAPAC"
    sql = sql & " UNION" 'CITADAS *************************************************
    sql = sql & " SELECT /*+ ORDERED INDEX (CI0100 CI0105) */"
    sql = sql & " CI0100.CI01FECCONCERT  FECHA, "
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " PR0100.PR01DESCORTA, "
    sql = sql & " PR0400.PR56CODESTMUES, PR0400.AD02CODDPTO, PR0400.PR37CODESTADO,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0400.CI21CODPERSONA, PR0400.AD07CODPROCESO,"
    sql = sql & " PR0400.AD01CODASISTENCI, GCFN06(AD1500.AD15CODCAMA) CAMA, "
    sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    sql = sql & " CI0100.AG11CODRECURSO CODREC,"
    sql = sql & " AG1100.AG11DESRECURSO DESREC,"
    sql = sql & " PR59DESMOVPAC, PR60DESNTRAPAC, PR04DESINDSANIT"
    sql = sql & " FROM  CI0100, PR0400,  PR0100, CI2200, PR0300, AG1100, AD1500, PR5900, PR6000"
    sql = sql & " Where CI0100.CI01FECCONCERT >= TO_DATE(?,'DD/MM/YYYY')"
    sql = sql & " AND CI0100.CI01FECCONCERT < TO_DATE(?,'DD/MM/YYYY')"
    sql = sql & " AND PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sql = sql & " AND  PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI"
    sql = sql & " AND  PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
    sql = sql & " AND  PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN"
    If chkSinAsist.Value = vbUnchecked Then
      sql = sql & " AND  PR0400.AD01CODASISTENCI IS NOT NULL"
    Else  ' Se muestran todas.
      sql = sql & " AND  (PR0400.AD01CODASISTENCI IS NOT NULL OR "
      sql = sql & "      (PR0400.AD01CODASISTENCI IS NULL AND "
      sql = sql & "      PR0100.PR12CodActividad IN (" & constACTIV_PRUEBA & ", " & constACTIV_TRATAMIENTO & ")))"
    End If
    sql = sql & " AND  PR0400.AD07CODPROCESO IS NOT NULL"
    If strRecSel <> "0" Then 'SI HAY RECURSOS SELECCIONADOS
        sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    sql = sql & " AND  CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO"
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= '" & constCAMAOCUPADA & "'"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NULL"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_HOSPITALIZACION
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_INTERVENCION
    sql = sql & " AND CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND PR5900.PR59CODMOVPAC (+)= PR0400.PR59CODMOVPAC"
    sql = sql & " AND PR6000.PR60CODNTRAPAC (+)= PR0400.PR60CODNTRAPAC"
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(dtcDesde.Text, "dd/mm/yyyy")
    qry(1) = Format(DateAdd("d", 1, dtcHasta.Text), "dd/mm/yyyy")
    qry(2) = intDptoSel
    qry(3) = qry(0)
    qry(4) = qry(1)
    qry(5) = qry(2)
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        If Not IsNull(rs!PR59DESMOVPAC) Then str1 = rs!PR59DESMOVPAC & ". " Else str1 = ""
        If Not IsNull(rs!PR60DESNTRAPAC) Then str2 = rs!PR60DESNTRAPAC & ". " Else str2 = ""
        If Not IsNull(rs!PR04DESINDSANIT) Then str3 = rs!PR04DESINDSANIT & ". " Else str3 = ""
        ssgrdEnEspera.AddItem Format(rs!fecha, "dd/mm/yy hh:mm") & Chr$(9) _
                    & rs!PR01DESCORTA & Chr$(9) _
                    & rs!CI22NUMHISTORIA & Chr$(9) _
                    & rs!PACIENTE & Chr$(9) _
                    & rs!Cama & Chr$(9) _
                    & rs!DESREC & Chr$(9) _
                    & rs!PR04NUMACTPLAN & Chr$(9) _
                    & rs!PR56CODESTMUES & Chr$(9) _
                    & strConsent & Chr$(9) _
                    & rs!CI21CODPERSONA & Chr$(9) _
                    & rs!AD07CODPROCESO & Chr$(9) _
                    & rs!AD01CODASISTENCI & Chr$(9) _
                    & str1 & str2 & str3
        rs.MoveNext
    Loop
    
    ssgrdEnEspera.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActPendientes(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones pendientes hasta hoy inclusive en funci�n del Dpto y rescurso
'*  o recursos seleccionados ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strConsent$
    Static sqlOrder$
    Dim str1$, str2$, str3$
        
    Screen.MousePointer = vbHourglass
    ssgrdPendientes.RemoveAll
    ssgrdPendientes.ScrollBars = ssScrollBarsNone
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY FECHA, PACIENTE" 'Opci�n por defecto: Fecha Prog.
    Else
        Select Case UCase(ssgrdPendientes.Columns(intCol).Caption)
        Case UCase("Fecha Prog."): sqlOrder = " ORDER BY FECHA, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, FECHA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, FECHA"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, FECHA"
        Case UCase("Entrada cola"): sqlOrder = " ORDER BY PR04FECENTRCOLA, PACIENTE"
        Case Else: Exit Sub
        End Select
    End If
        
    'ACTUACIONES PENDIENTES (citadas o planificadas con entrada en cola)
    sql = "SELECT CI0100.CI01FECCONCERT FECHA,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    'SQL = SQL & " PR0400.CI21CODPERSONA,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " PR0400.PR04FECENTRCOLA, "
    sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
    sql = sql & " AG11DESRECURSO RECURSO,"
    sql = sql & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & " PR0400.CI21CODPERSONA, PR0400.AD01CODASISTENCI, PR0400.AD07CODPROCESO,"
    sql = sql & " PR59DESMOVPAC, PR60DESNTRAPAC, PR04DESINDSANIT"
    sql = sql & " FROM PR0100, CI0100, PR0300, CI2200, PR0400, AG1100, AD1500, PR5900, PR6000"
    sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NOT NULL"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_CITADA 'citada --> pr04feciniact = null
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO"
    sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR03INDCITABLE = -1" 'citable
    sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    sql = sql & " AND CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_HOSPITALIZACION
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_INTERVENCION
    If strRecSel <> "0" Then
        sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    sql = sql & " AND PR5900.PR59CODMOVPAC (+)= PR0400.PR59CODMOVPAC"
    sql = sql & " AND PR6000.PR60CODNTRAPAC (+)= PR0400.PR60CODNTRAPAC"
    sql = sql & " UNION"
    sql = sql & " SELECT PR0400.PR04FECPLANIFIC FECHA,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " PR0400.PR04FECENTRCOLA,"
    sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES, "
    sql = sql & "  ' ' RECURSO,"
    sql = sql & "  GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & "  PR0400.CI21CODPERSONA, PR0400.AD01CODASISTENCI, PR0400.AD07CODPROCESO,"
    sql = sql & " PR59DESMOVPAC, PR60DESNTRAPAC, PR04DESINDSANIT"
    If strRecSel <> "0" Then
        sql = sql & " FROM PR0100, PR0300, CI2200, PR1400, PR0400, AD1500, PR5900, PR6000"
    Else
        sql = sql & " FROM PR0100, PR0300, CI2200, PR0400, AD1500, PR5900, PR6000"
    End If
    sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NOT NULL"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_PLANIFICADA 'planificada --> pr04feciniact = null
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR03INDCITABLE = 0" 'no citable
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_HOSPITALIZACION
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> " & constACTIV_INTERVENCION
    If strRecSel <> "0" Then
        sql = sql & " AND PR1400.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR1400.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    sql = sql & " AND PR5900.PR59CODMOVPAC (+)= PR0400.PR59CODMOVPAC"
    sql = sql & " AND PR6000.PR60CODNTRAPAC (+)= PR0400.PR60CODNTRAPAC"
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    qry(1) = qry(0)
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        If Not IsNull(rs!PR59DESMOVPAC) Then str1 = rs!PR59DESMOVPAC & ". " Else str1 = ""
        If Not IsNull(rs!PR60DESNTRAPAC) Then str2 = rs!PR60DESNTRAPAC & ". " Else str2 = ""
        If Not IsNull(rs!PR04DESINDSANIT) Then str3 = rs!PR04DESINDSANIT & ". " Else str3 = ""
        ssgrdPendientes.AddItem Format(rs!fecha, "dd/mm/yy hh:mm") & Chr$(9) _
            & rs!PR01DESCORTA & Chr$(9) _
            & rs!CI22NUMHISTORIA & Chr$(9) & rs!PACIENTE & Chr$(9) _
            & Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
            & strConsent & Chr$(9) & rs!PR04NUMACTPLAN & Chr$(9) _
            & rs!PR56CODESTMUES & Chr$(9) _
            & rs!Cama & Chr$(9) _
            & rs!recurso & Chr$(9) _
            & rs!CI21CODPERSONA & Chr$(9) _
            & rs!AD07CODPROCESO & Chr$(9) _
            & rs!AD01CODASISTENCI & Chr$(9) _
            & str1 & str2 & str3
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdPendientes.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarInformesAtrasados(intCol%)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strConsent As String
Dim sqlOrder As String
ssgrdEnEspera.RemoveAll
If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PACIENTE, FECHA" 'Opci�n por defecto: Paciente
    Else
        Select Case UCase(ssgrdEnEspera.Columns(intCol).Caption)
        Case UCase("Fecha Prog."): sqlOrder = " ORDER BY FECHA, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, FECHA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, FECHA"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, FECHA"
        Case Else: Exit Sub
        End Select
    End If
Screen.MousePointer = vbHourglass
    sql = "SELECT PR0100.PR01DESCORTA, PR0400.PR04FECPLANIFIC FECHA,"
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & " ' ' DESREC, PR0400.PR04NUMACTPLAN, PR0400.PR56CODESTMUES,"
    sql = sql & " PR0100.PR01INDCONSFDO,PR0300.PR03INDCONSFIRM, PR0400.CI21CODPERSONA,"
    sql = sql & " PR0400.AD01CODASISTENCI, PR0400.AD07CODPROCESO FROM"
    sql = sql & " PR0400,PR0300, PR0100, CI2200, AD1500"
    sql = sql & " WHERE PR0400.PR04FECPLANIFIC < SYSDATE "
    sql = sql & " AND PR0400.AD01CODASISTENCI IS NOT NULL"
    sql = sql & " AND PR0400.AD07CODPROCESO IS NOT NULL"
    sql = sql & " AND PR0400.PR37CODESTADO = 1"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NULL"
    sql = sql & " AND PR0400.PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR0100"
    sql = sql & " WHERE PR12CODACTIVIDAD = " & constACTIV_INFORME & ")"
    sql = sql & " AND PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR03INDCITABLE = 0"
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & " UNION "
    sql = sql & " SELECT PR0100.PR01DESCORTA, CI0100.CI01FECCONCERT FECHA ,"
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sql = sql & " ' ' DESREC, PR0400.PR04NUMACTPLAN, PR0400.PR56CODESTMUES,"
    sql = sql & " PR0100.PR01INDCONSFDO,PR0300.PR03INDCONSFIRM, PR0400.CI21CODPERSONA,"
    sql = sql & " PR0400.AD01CODASISTENCI, PR0400.AD07CODPROCESO FROM"
    sql = sql & " CI0100, PR0400,PR0300, PR0100, CI2200, AD1500"
    sql = sql & " WHERE CI0100.CI01FECCONCERT < SYSDATE "
    sql = sql & " AND CI0100.CI01SITCITA = '1'"
    sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    sql = sql & " AND PR0400.PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR0100"
    sql = sql & " WHERE PR12CODACTIVIDAD = " & constACTIV_INFORME & ")"
    sql = sql & " AND PR0400.AD01CODASISTENCI IS NOT NULL"
    sql = sql & " AND PR0400.AD07CODPROCESO IS NOT NULL"
    sql = sql & " AND PR0400.PR37CODESTADO = 2"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NULL"
    sql = sql & " AND PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR03INDCITABLE = -1"
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = intDptoSel
        qry(1) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        ssgrdEnEspera.AddItem Format(rs!fecha, "dd/mm/yy hh:mm") & Chr$(9) _
                    & rs!PR01DESCORTA & Chr$(9) _
                    & rs!CI22NUMHISTORIA & Chr$(9) _
                    & rs!PACIENTE & Chr$(9) _
                    & rs!Cama & Chr$(9) _
                    & rs!DESREC & Chr$(9) _
                    & rs!PR04NUMACTPLAN & Chr$(9) _
                    & rs!PR56CODESTMUES & Chr$(9) _
                    & strConsent & Chr$(9) _
                    & rs!CI21CODPERSONA & Chr$(9) _
                    & rs!AD07CODPROCESO & Chr$(9) _
                    & rs!AD01CODASISTENCI
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdPendientes.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault

End Sub
