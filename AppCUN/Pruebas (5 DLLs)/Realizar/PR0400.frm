VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmIngresoPend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Ingresos Pendientes"
   ClientHeight    =   8325
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11655
   ControlBox      =   0   'False
   Icon            =   "PR0400.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8325
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   5040
      Top             =   3840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Consultar"
      Height          =   375
      Left            =   4800
      TabIndex        =   35
      Top             =   690
      Width           =   1575
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   0
      Top             =   840
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   1080
      Width           =   11340
      Begin idperson.IdPersona IdPersona1 
         Height          =   1335
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   2355
         BackColor       =   12648384
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
         MaxLength       =   7
         blnAvisos       =   0   'False
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8040
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabTab1 
      Height          =   5175
      HelpContextID   =   90001
      Index           =   0
      Left            =   240
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2880
      Width           =   11340
      _ExtentX        =   20003
      _ExtentY        =   9128
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Citas Ingresos "
      TabPicture(0)   =   "PR0400.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdAnularReserva"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdReservarCama"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdAsignarCama"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmddesasignarCama"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "chksinfecha"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdActPlanif"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdcamasis"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      TabCaption(1)   =   "Camas"
      TabPicture(1)   =   "PR0400.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdDBGrid1(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.CommandButton cmdcamasis 
         Caption         =   "Cambios Asistencias"
         Height          =   375
         Left            =   9360
         TabIndex        =   36
         Top             =   4560
         Width           =   1815
      End
      Begin VB.CommandButton cmdActPlanif 
         Caption         =   "Actuaciones Planificadas"
         Height          =   375
         Left            =   7320
         TabIndex        =   13
         Top             =   4560
         Width           =   1935
      End
      Begin VB.CheckBox chksinfecha 
         Caption         =   "Ver actuaciones sin fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7680
         TabIndex        =   12
         Top             =   360
         Width           =   2775
      End
      Begin VB.CommandButton cmddesasignarCama 
         Caption         =   "Desasignar Cama"
         Height          =   375
         Left            =   5520
         TabIndex        =   11
         Top             =   4560
         Width           =   1695
      End
      Begin VB.CommandButton cmdAsignarCama 
         Caption         =   "Asignar Cama"
         Height          =   375
         Left            =   3720
         TabIndex        =   10
         Top             =   4560
         Width           =   1695
      End
      Begin VB.CommandButton cmdReservarCama 
         Caption         =   "Reservar Cama"
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   4560
         Width           =   1695
      End
      Begin VB.CommandButton cmdAnularReserva 
         Caption         =   "Anular Reserva Cama"
         Height          =   375
         Left            =   1920
         TabIndex        =   8
         Top             =   4560
         Width           =   1695
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Ingresos Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3960
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3345
            Index           =   0
            Left            =   120
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   360
            Width           =   10800
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19050
            _ExtentY        =   5900
            _StockProps     =   79
         End
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2130
         Index           =   2
         Left            =   -74880
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   90
         Width           =   10005
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   17648
         _ExtentY        =   3757
         _StockProps     =   79
         Caption         =   "PACIENTES"
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4305
         Index           =   1
         Left            =   -74760
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   600
         Width           =   10800
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   3
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   3
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cama"
         Columns(0).Name =   "cama"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   2
         Columns(0).FieldLen=   256
         Columns(1).Width=   3598
         Columns(1).Caption=   "Tipo Cama"
         Columns(1).Name =   "Tipo Cama"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Estado Cama"
         Columns(2).Name =   "Estado Cama"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   19050
         _ExtentY        =   7594
         _StockProps     =   79
      End
   End
   Begin TabDlg.SSTab tabTab1 
      Height          =   375
      HelpContextID   =   90001
      Index           =   1
      Left            =   0
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   480
      Visible         =   0   'False
      Width           =   1230
      _ExtentX        =   2170
      _ExtentY        =   661
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   2
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "PR0400.frx":0044
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(3)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Tab 1"
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdrecact(2)"
      Tab(1).Control(1)=   "Command1"
      Tab(1).Control(2)=   "grdDBGrid(2)"
      Tab(1).ControlCount=   3
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid 
         Height          =   1275
         Index           =   2
         Left            =   -74640
         TabIndex        =   28
         Top             =   360
         Visible         =   0   'False
         Width           =   3615
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns(0).Width=   3200
         _ExtentX        =   6376
         _ExtentY        =   2249
         _StockProps     =   79
         Caption         =   "SSDBGrid1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "&Departamentos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   0
         TabIndex        =   26
         Top             =   0
         Visible         =   0   'False
         Width           =   375
         Begin VB.TextBox txtText1 
            DataField       =   "AD02CODDPTO"
            Height          =   285
            Index           =   0
            Left            =   240
            TabIndex        =   27
            Top             =   240
            Visible         =   0   'False
            Width           =   1215
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Actuaciones Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3600
         Index           =   5
         Left            =   -74880
         TabIndex        =   23
         Top             =   480
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3225
            Index           =   4
            Left            =   120
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   360
            Width           =   10800
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19050
            _ExtentY        =   5689
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Estado Paciente"
         Height          =   375
         Left            =   -72720
         TabIndex        =   22
         Top             =   4320
         Width           =   1935
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Recibir Paciente"
         Height          =   375
         Left            =   -74880
         TabIndex        =   21
         Top             =   4320
         Width           =   1935
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Cancelar Prueba"
         Height          =   375
         Left            =   -70560
         TabIndex        =   20
         Top             =   4320
         Width           =   1935
      End
      Begin VB.CommandButton cmdrecact 
         Caption         =   "Recursos Actuaci�n"
         Height          =   375
         Index           =   3
         Left            =   -68400
         TabIndex        =   19
         Top             =   4320
         Width           =   1935
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Deshacer Entrada"
         Height          =   375
         Left            =   -72360
         TabIndex        =   18
         Top             =   4680
         Width           =   1935
      End
      Begin VB.CommandButton cmdrecact 
         Caption         =   "Recursos Actuaci�n"
         Height          =   375
         Index           =   2
         Left            =   -68760
         TabIndex        =   17
         Top             =   4680
         Width           =   1935
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Ver actuaciones sin fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -67320
         TabIndex        =   16
         Top             =   480
         Width           =   2775
      End
      Begin VB.CommandButton cmdinfadi 
         Caption         =   "Informaci�n Actuaci�n"
         Height          =   375
         Index           =   0
         Left            =   -66240
         TabIndex        =   15
         Top             =   4320
         Width           =   1935
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2130
         Index           =   5
         Left            =   -74880
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   90
         Width           =   10005
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   17648
         _ExtentY        =   3757
         _StockProps     =   79
         Caption         =   "PACIENTES"
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
      Height          =   330
      Index           =   0
      Left            =   8160
      TabIndex        =   29
      Top             =   690
      Width           =   3135
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ";"
      DefColWidth     =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2540
      Columns(0).Caption=   "COD. DPTO"
      Columns(0).Name =   "COD. DPTO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5927
      Columns(1).Caption=   "DESCRIPCI�N"
      Columns(1).Name =   "DESCRIPCI�N"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   0
      Left            =   720
      TabIndex        =   31
      Tag             =   "Fecha Desde"
      ToolTipText     =   "Fecha Desde"
      Top             =   690
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   2760
      TabIndex        =   32
      Tag             =   "Fecha Hasta"
      ToolTipText     =   "Fecha Hasta"
      Top             =   690
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   7
      Left            =   240
      TabIndex        =   34
      Top             =   480
      Width           =   1140
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   2685
      TabIndex        =   33
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Selecci�n del   Departamento:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   6720
      TabIndex        =   30
      Top             =   690
      Width           =   1335
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmIngresoPend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Constante para Botones 'Mariajo 14/6/99
Const iBUTLISTA = 0
'Constante para combos 'Mariajo 14/6/99
Const iCBODOCTOR = 0
'Constantes para listas
Const iLSTCitas = 0
Const iLSTCamas = 1

'Constantes de estados de camas
Const iEstado_Ocupado = 6
Const iEstado_Libre = 1

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMasterInfo   As New clsCWForm
Dim objMultiInfo1   As New clsCWForm
Dim objMultiInfo2   As New clsCWForm

Private Sub chksinfecha_Click()
If chksinfecha.Value = 0 Then
objMultiInfo1.strWhere = "PR0451J.AD07CODPROCESO IS NOT NULL " & _
                 " AND PR0451J.AD01CODASISTENCI IS NOT NULL" & _
                 " AND (FECHA < TO_DATE('" & _
                 dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI') OR FECHA IS NULL) " & _
                 "AND FECHA > =  TO_DATE('" & dtcDateCombo1(0).Text & _
                 "','DD/MM/YYYY') AND PR0451J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value

  objWinInfo.DataRefresh
End If
If chksinfecha.Value = 1 Then
  objMultiInfo1.strWhere = "PR0451J.AD07CODPROCESO IS NOT NULL " & _
                                         " AND PR0451J.AD01CODASISTENCI IS NOT NULL" & _
                                         " AND PR0451J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value & _
                                         " AND PR0451J.FECHA is null"
  objWinInfo.DataRefresh
End If
End Sub

Private Sub cmdActPlanif_Click()
    Dim lRow As Long
    lRow = grdDBGrid1(0).Row
    ReDim vntData(1) As Variant
    'Pasa el c�digo de persona para mostrarla despu�s
    vntData(1) = IdPersona1.Text
    Call objsecurity.LaunchProcess("AD0105", vntData)
    objWinInfo.DataRefresh
    grdDBGrid1(0).Row = lRow
    grdDBGrid1(0).SelBookmarks.Add (lRow)
End Sub
Private Sub cmdAnularReserva_Click()

Dim sStmSql As String
Dim lRow    As Long

    lRow = grdDBGrid1(0).Row
    sStmSql = "UPDATE CI0100 SET AD15CODCAMA = null "
    StmSql = sStmSql & " WHERE  PR04NUMACTPLAN =" & grdDBGrid1(1).Columns(9).Value
    objApp.rdoConnect.Execute strupdate, 64
    objWinInfo.DataRefresh
    grdDBGrid1(0).Row = lRow
    grdDBGrid1(0).SelBookmarks.Add (lRow)
    If grdDBGrid1(0).Rows = 0 Then
        cmdAsignarCama.Enabled = False
        cmdReservarCama.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis.Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdReservarCama.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdAsignarCama.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis.Enabled = False
        Else
            cmdReservarCama.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdAsignarCama.Enabled = True
            cmdrecact(0).Enabled = True
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
End Sub

Private Sub cmdcamasis_Click()
  
    Dim lRow As Long
    
    ReDim vntData(3) As Variant
    
    lRow = grdDBGrid1(0).Row
    'Pasa el c�digo de persona para mostrarla despu�s
    vntData(1) = IdPersona1.Text
    vntData(2) = grdDBGrid1(0).Columns(15).Value
    Call objsecurity.LaunchProcess("AD0115", vntData)
    objWinInfo.DataRefresh
    grdDBGrid1(0).Row = lRow
    grdDBGrid1(0).SelBookmarks.Add (lRow)
End Sub
Private Sub cmdAsignarCama_Click()
    
    Dim lRow    As Long
    Dim strsql  As String
    Dim rs      As rdoResultset
    Dim stra    As String
    Dim iResp   As Integer
    Dim vntA    As Variant
    Dim rsta    As rdoResultset
    Dim rstwhereTA  As rdoResultset
    
        
    'Llama a la pantalla de Asignar camas
    lRow = grdDBGrid1(0).Row
    
    ' Para asignar Cama, debe estar dada de alta la asistencia
    If grdDBGrid1(0).Columns(15).Text = 0 Or grdDBGrid1(0).Columns(14).Text = 0 Then
        Call MsgBox("Debe asociar antes el Proceso-Asistencia", vbInformation)
        Exit Sub
    End If
    strsql = "SELECT AD1500.AD01CODASISTENCI, AD15CODCAMA "
    strsql = strsql & "FROM AD1500,AD0100 "
    strsql = strsql & "WHERE AD1500.AD01CODASISTENCI=" & grdDBGrid1(0).Columns(15).Text & " "
    strsql = strsql & "AND AD1500.AD01CODASISTENCI=AD0100.AD01CODASISTENCI "
    strsql = strsql & "AND AD0100.CI21CODPERSONA=" & IdPersona1.Text
    strsql = strsql & "AND AD0100.AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 AND AD07CODPROCESO=" & grdDBGrid1(0).Columns(14).Text & " "
    Set rs = objApp.rdoConnect.OpenResultset(strsql, rdOpenDynamic)
    If Not rs.EOF Then
      Call objError.SetError(cwCodeMsg, "El paciente se encuentra en la cama:" & rs!AD15CODCAMA & " y tiene la asistencia:" & rs!AD01CODASISTENCI)
      vntA = objError.Raise
      Exit Sub
    End If
    rs.Close
    Set rs = Nothing
    iResp = MsgBox("�Se desea asignar la cama provisional como real?", vbYesNo)
    If iResp = 6 Then
        'Yes, pasamos la cama provisional ---> cama
        'Estado Real a Reservada
        'Estado2 a null
        Call bDBInsertarCamaReservada
    Else
        'Llamamos a Reservar camas,
        'solo estado real  libre, para que eliga una.
        '????? AJO
    End If
    If grdDBGrid(0).Columns(15) <> 0 Then
        '******************************************************************
        'IVM se pasa el proceso
        stra = "SELECT AD07CODPROCESO FROM PR0400 WHERE PR04NUMACTPLAN=" & _
            grdDBGrid1(0).Columns(8).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objPipe.PipeSet("Proceso", rsta.rdoColumns(0).Value)
        rsta.Close
        Set rsta = Nothing
        'Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        '*****************************************************************
        If rstwhereTA.EOF = False Then
            If IsNull(rstwhereTA("ad25fecinicio").Value) = False Then
                Call objPipe.PipeSet("FechaHoraInicioTipoAsistencia", rstwhereTA("ad25fecinicio").Value)
            End If
        End If
        Call objPipe.PipeSet("CodigoAsistencia", txtText1(9).Text)
        Call objPipe.PipeSet("Historia", IdPersona1.Historia)
        Call objPipe.PipeSet("Cama", txtText1(1).Text)
        Call objPipe.PipeSet("VieneCama", 1)
        Call objsecurity.LaunchProcess("AD0121")
        'Call objWinInfo.CtrlSet(txtText1(1), objPipe.PipeGet("Cama"))
        txtText1(1).Text = objPipe.PipeGet("Cama")
        Call objPipe.PipeRemove("FechaHoraInicioTipoAsistencia")
        Call objPipe.PipeRemove("CodigoAsistencia")
        Call objPipe.PipeRemove("Historia")
        Call objPipe.PipeRemove("Cama")
        Call objPipe.PipeRemove("VieneCama")
        Call objPipe.PipeRemove("Proceso")
    Else
        Call objError.SetError(cwCodeMsg, "Error, No hay ninguna Asistencia Abierta", vntA)
        vntA = objError.Raise
    End If
    grdDBGrid1(0).Row = lRow
    grdDBGrid1(0).SelBookmarks.Add (lRow)
End Sub
Private Sub bDBInsertarCamaReservada()
   
   Dim sStmSql      As String
   Dim qryInsert    As rdoQuery
   
    'Inserci�n del registro en la tabla AD1500 la cama reservada
    sStmSql = "INSERT INTO AD1500 "
    sStmSql = sStmSql & "(AD15CODCAMA, "
    sStmSql = sStmSql & "AD02CODDPTO, "
    sStmSql = sStmSql & "AD13CODTIPOCAMA, "
    sStmSql = sStmSql & "AD14CODESTCAMA, "
    sStmSql = sStmSql & "AD15FECINICIO, "
    sStmSql = sStmSql & "AD01CODASISTENCI, "
    sStmSql = sStmSql & "AD07CODPROCESO) "
 
    sStmSql = sStmSql & " ?, ?, ?, ?, ?, ?, ?)"
    Set qryInsert = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryInsert(1) = Trim(grdDBGrid1(0).Columns(19).Value)
    qryInsert(2) = Trim(SSDBCombo1(0).Columns(0).Value)
    qryInsert(3) = Trim(grdDBGrid1(0).Columns(18).Value)
    qryInsert(4) = 6
    qryInsert(5) = sysdate
    qryInsert(6) = Trim(grdDBGrid1(0).Columns(15).Value)
    qryInsert(7) = Trim(grdDBGrid1(0).Columns(14).Value)
    qryInsert.Execute
    qryInsert.Close
   
End Sub

Private Sub bDBBorrarCamaAsignada()


Dim sStmSql      As String
   Dim qryDelete    As rdoQuery
   
    'Inserci�n del registro en la tabla AD1500 la cama reservada
    sStmSql = "DELETE INTO AD1500 "
    sStmSql = sStmSql & "WHERE AD15CODCAMA = ? "
    sStmSql = sStmSql & "AND AD02CODDPTO = ? "
    sStmSql = sStmSql & "AND AD13CODTIPOCAMA = ? "
    sStmSql = sStmSql & "AND AD14CODESTCAMA = ? "
    sStmSql = sStmSql & "AND AD01CODASISTENCI = ? "
    sStmSql = sStmSql & "AND AD07CODPROCESO = ? "
 
    sStmSql = sStmSql & " ?, ?, ?, ?, ?, ?, ?)"
    Set qryDelete = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryDelete(1) = Trim(grdDBGrid1(0).Columns(19).Value)
    qryDelete(2) = Trim(SSDBCombo1(0).Columns(0).Value)
    qryDelete(3) = Trim(grdDBGrid1(0).Columns(18).Value)
    qryDelete(4) = iEstado_Reservada
    qryDelete(6) = Trim(grdDBGrid1(0).Columns(15).Value)
    qryDelete(7) = Trim(grdDBGrid1(0).Columns(14).Value)
    qryDelete.Execute
    qryDelete.Close

End Sub
Private Sub cmddesasignarCama_Click()
    If grdDBGrid1(0).Columns(19) <> "" Then
        Call bDBBorrarCamaAsignada
        Call
    Else
        MsgBox "Registro erroneo para dicho proceso", vbInformation
    End If
    objWinInfo.DataRefresh
End Sub

Private Sub cmdrecact_Click(Index As Integer)
   lRow = grdDBGrid1(0).Row
   gstrLlamadorSel = ""
   If Index = 0 Then
    gstrLlamadorSel = "Peticiones Pendientes 0"
    frmRecPacRecur.IdPersona1 = IdPersona1
    frmRecPacRecur.txtacttext1(0).Text = grdDBGrid1(0).Columns(9).Value
    frmRecPacRecur.txtacttext1(1).Text = grdDBGrid1(0).Columns(8).Value
   Else
    gstrLlamadorSel = "Peticiones Pendientes 1"
    frmRecPacRecur.IdPersona1 = IdPersona1
    frmRecPacRecur.txtacttext1(0).Text = grdDBGrid1(1).Columns(10).Value
    frmRecPacRecur.txtacttext1(1).Text = grdDBGrid1(1).Columns(8).Value
   End If
   Call objsecurity.LaunchProcess("PR0197")
   gstrLlamadorSel = ""
   grdDBGrid1(0).Row = lRow
End Sub

Private Sub Command5_Click()
If SSDBCombo1(0).Text = "" Then
    MsgBox "Por favor, seleccione un departamento", vbOKOnly
    Exit Sub
End If
 If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
        If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
          Call objError.Raise
          Exit Sub
        End If
End If
Me.MousePointer = vbHourglass
objMultiInfo1.strWhere = "PR0451J.AD07CODPROCESO IS NOT NULL " & _
                 " AND PR0451J.AD01CODASISTENCI IS NOT NULL" & _
                 " AND (FECHA < TO_DATE('" & _
                 dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI') OR FECHA IS NULL) " & _
                 "AND FECHA > = TO_DATE(' " & dtcDateCombo1(0).Text & _
                 "','DD/MM/YYYY') AND PR0451J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value
  objWinInfo.DataRefresh
Me.MousePointer = vbDefault
End Sub


Private Sub Form_Activate()
  objWinInfo.DataRefresh
    
    If grdDBGrid1(0).Rows = 0 Then
        cmdReservarCama.Enabled = False
        cmdAnularReserva.Enabled = False
        cmdAsignarCama.Enabled = False
        cmddesasignarCama.Enabled = False
        cmdActPlanif.Enabled = False
        cmdcamasis.Enabled = False
    Else
        If grdDBGrid1(0).Columns(19).Value = "" Then
            cmdReservarCama.Enabled = True
            cmdAnularReserva.Enabled = False
            cmdAsignarCama.Enabled = False
            cmddesasignarCama.Enabled = False
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        Else
            cmdReservarCama.Enabled = True
            cmdAnularReserva.Enabled = True
            cmdAsignarCama.Enabled = True
            cmddesasignarCama.Enabled = True
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
      
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  If primerallamada = True Then
    primerallamada = False
  End If
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
 
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
    
   With objMasterInfo
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithoutTab + cwWithoutKeys + cwWithGrid
    .strTable = "AD0200"
    .intAllowance = cwAllowModify
    .intCursorSize = 0
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
   End With
  With objMultiInfo1
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(3)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
      
    .strTable = "PR0451J"
    '''''''''''' ''''''''''''''''''''''''''''''''''''''''
    .strWhere = "PR0451J.AD07CODPROCESO IS NOT NULL " & _
                 " AND PR0451J.AD01CODASISTENCI IS NOT NULL" & _
                 " AND (FECHA < TO_DATE('" & _
                 strFecha_Sistema & " 23:59','DD/MM/YYYY HH24:MI') OR FECHA IS NULL) " & _
                 " AND FECHA > = (SELECT TO_date(SYSDATE) FROM DUAL)"
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call .FormAddRelation("AD02CODDPTO", txtText1(0))
    'Call .objPrinter.Add("PR0400", "Listado de Peticiones Pendientes")
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    'El grid estar� ordenado por la fecha de la consulta
    Call .FormAddOrderField("FECHA", cwAscending)
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    Call .FormAddOrderField("CI22SEGAPEL", cwAscending)
    Call .FormAddOrderField("CI22NOMBRE", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones Pendientes")
    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha", cwDate)
    Call .FormAddFilterWhere(strKey, "HORA", "Hora", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "PR37DESESTADO", "Estado", cwString)
    
    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha")
    Call .FormAddFilterOrder(strKey, "HORA", "Hora")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "PR37DESESTADO", "Estado")
    
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .GridAddColumn(objMasterInfo, "Codigo Dpto", "AD02CODDPTO", cwNumeric, 3)
    Call .FormCreateInfo(objMasterInfo)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo1, "Fecha", "FECHA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo1, "Hora", "HORA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo1, "Nombre", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Primer Apellido", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Segundo Apellido", "CI22SEGAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo1, "Cod act pedi", "PR04numactplan", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "Cod paciente", "CI21CODPERSONA", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "Estado", "PR37DESESTADO", cwString, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Estado", "PR37CODESTADO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Estado Muestra", "PR56CODESTMUES", cwNumeric, 10)
    'Proceso y asistencia
    Call .GridAddColumn(objMultiInfo1, "C�d.Proceso", "AD07CODPROCESO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
    'Tipo Economico, Entidad, Tipo cama y cama
    Call .GridAddColumn(objMultiInfo1, "Tip.Econ�mico", "CI32CODTIPECON", cwString, 1)
    Call .GridAddColumn(objMultiInfo1, "Entidad", "CI13CODENTIDAD", cwString, 2)
    Call .GridAddColumn(objMultiInfo1, "Tip.Cama", "AD13CODTIPOCAMA", cwString, 2)
    Call .GridAddColumn(objMultiInfo1, "Cama", "AD15CODCAMA", cwString, 7)
    'Edad, Sexo y Observaciones
    Call .GridAddColumn(objMultiInfo1, "Edad", "EDAD", cwString, 2)
    Call .GridAddColumn(objMultiInfo1, "Sexo", "SEXO", cwString, 8)
    Call .GridAddColumn(objMultiInfo1, "Observaciones", "PR08DESOBSERV", cwString, 50)
    Call .FormCreateInfo(objMultiInfo1)
    'La primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    Call .FormChangeColor(objMultiInfo1)
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
    'Los campos no estar�n visibles en el grid
    grdDBGrid1(0).Columns(9).Visible = False
    grdDBGrid1(0).Columns(10).Visible = False
    grdDBGrid1(0).Columns(11).Visible = False
    grdDBGrid1(0).Columns(12).Visible = False
    grdDBGrid1(0).Columns(13).Visible = False
    
    IdPersona1.blnSearchButton = False
    Dim sqlstr As String
    Dim rsta As rdoResultset
    Dim sqlstrdpto As String
    Dim rstadpto As rdoResultset

    sqlstr = "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objsecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL ORDER BY AD0200.AD02DESDPTO"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    While rsta.EOF = False
        SSDBCombo1(0).AddItem rsta("AD02CODDPTO").Value & ";" & rsta("AD02DESDPTO").Value
        SSDBCombo1(0).Text = rsta("AD02DESDPTO").Value
        SSDBCombo1(0).MoveNext
        rsta.MoveNext
    Wend

    gblnCancelar = False
    rsta.Close
    Set rsta = Nothing
    SSDBCombo1(0).Enabled = True
    SSDBCombo1(0).Text = ""
    Call .WinRegister
    Call .WinStabilize
  End With
  
    'Damos el ancho de las columnas del grid
    grdDBGrid1(0).Columns(3).Width = 1000
    grdDBGrid1(0).Columns(4).Width = 600
    grdDBGrid1(0).Columns(5).Width = 1400
    grdDBGrid1(0).Columns(6).Width = 1500
    grdDBGrid1(0).Columns(7).Width = 1500
    grdDBGrid1(0).Columns(8).Width = 2800
    grdDBGrid1(0).Columns(11).Width = 1400
  
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdReservarCama.Enabled = False
        cmdAnularReserva.Enabled = False
        cmdAsignarCama.Enabled = False
        cmddesasignarCama.Enabled = False
        cmdActPlanif.Enabled = False
        cmdcamasis.Enabled = False
    Else
        If grdDBGrid1(0).Columns(19).Value = "" Then
            cmdReservarCama.Enabled = True
            cmdAnularReserva.Enabled = False
            cmdAsignarCama.Enabled = False
            cmddesasignarCama.Enabled = False
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        Else
            cmdReservarCama.Enabled = True
            cmdAnularReserva.Enabled = True
            cmdAsignarCama.Enabled = True
            cmddesasignarCama.Enabled = True
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If

    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    IdPersona1.blnSearchButton = False
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
    
End Sub

Private Sub SSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 And KeyCode = 46 Then
    SSDBCombo1(intIndex).Text = ""
    objWinInfo.objWinActiveForm.strWhere = ""
    objWinInfo.DataRefresh
  End If

End Sub
Private Sub SSDBCombo1_CloseUp(Index As Integer)
    
    SSDBCombo1(0).Text = SSDBCombo1(0).Columns(1).Value
    
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    
    objMasterInfo.strInitialWhere = "AD0200.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value
    objMasterInfo.strWhere = "AD0200.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value
       
    'dtcDateCombo1(0).Text = strFecha_Sistema()
    'dtcDateCombo1(1).Text = strFecha_Sistema()
    Call chksinfecha_Click
    IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
    If tabTab1(0).Tab = 0 Then
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    End If
End Sub
Private Sub SSDBCombo1_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = 0
End Sub
Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub
Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub
Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
If Not gblnCancelar Then
  intCancel = objWinInfo.WinExit
End If
End Sub
Private Sub Form_Unload(intCancel As Integer)
If Not gblnCancelar Then
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End If
End Sub
Private Sub cmdReservarCama_Click()
    ReDim vntData(4) As Variant
       
    'Reservamos cama para las citas de ingresos
    Dim lRow As Long
    'Conservamos en el registro de la grid en la que estan
    lRow = grdDBGrid1(0).Row
    cmdReservarCama.Enabled = False
    If grdDBGrid1(0).Row <> 0 Then
        Call pCargarCamas
        tabTab1(0).Tab = 1
    Else
        MsgBox "Selecciona una cita para reservar la cama", vbInformation
    End If
        
    'Miramos si tiene asignada cama
    'Pasa el c�digo de persona para mostrarla despu�s
'    vntData(1) = IdPersona1.Text
'    vntData(2) = grdDBGrid1(0).Columns(18).Value    'Tipo Cama
'    vntData(3) = grdDBGrid1(0).Columns(14).Value    'Proceso
'    vntData(4) = grdDBGrid1(0).Columns(15).Value    'Asistencia
'    Call objsecurity.LaunchProcess("AD0120", vntData)
'    objWinInfo.DataRefresh
'
'    IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
'    IdPersona1.ReadPersona
    
    'Nos situamos de nuevo en
    'el registro que estabamos antes de reservar la cama
    grdDBGrid1(0).Row = lRow
    grdDBGrid1(0).SelBookmarks.Add (lRow)
    End Sub
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub
Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
  If tabTab1(0).Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  End If
  objWinInfo.DataRefresh

End Sub
Private Sub tabTab1_DblClick(Index As Integer)
  If tabTab1(0).Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  End If
    objWinInfo.DataRefresh
  
End Sub
Private Sub Timer1_Timer()
Dim cont As Integer

cont = cont + 1
If cont = 5 Then
    cont = 0
    objWinInfo.DataRefresh
    If grdDBGrid1(0).Rows = 0 Then
        cmdReservarCama.Enabled = False
        cmdAnularReserva.Enabled = False
        cmdAsignarCama.Enabled = False
        cmddesasignarCama.Enabled = False
        cmdActPlanif.Enabled = False
        cmdcamasis.Enabled = False
    Else
        If grdDBGrid1(0).Columns(19).Value = "" Then
            cmdReservarCama.Enabled = True
            cmdAnularReserva.Enabled = False
            cmdAsignarCama.Enabled = False
            cmddesasignarCama.Enabled = False
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        Else
            cmdReservarCama.Enabled = True
            cmdAnularReserva.Enabled = True
            cmdAsignarCama.Enabled = True
            cmddesasignarCama.Enabled = True
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
If btnButton.Index = 30 Then
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Exit Sub
End If
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdReservarCama.Enabled = False
        cmdAnularReserva.Enabled = False
        cmdAsignarCama.Enabled = False
        cmddesasignarCama.Enabled = False
        cmdActPlanif.Enabled = False
        cmdcamasis.Enabled = False
    Else
        If grdDBGrid1(0).Columns(19).Value = "" Then
            cmdReservarCama.Enabled = True
            cmdAnularReserva.Enabled = False
            cmdAsignarCama.Enabled = False
            cmddesasignarCama.Enabled = False
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        Else
            cmdReservarCama.Enabled = True
            cmdAnularReserva.Enabled = True
            cmdAsignarCama.Enabled = True
            cmddesasignarCama.Enabled = True
            cmdActPlanif.Enabled = True
            cmdcamasis.Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
End Sub
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub
Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub
Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub
Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub
Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub
Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Select Case intIndex
    Case iLSTCitas
        Call objWinInfo.CtrlGotFocus
          If grdDBGrid1(0).Rows = 0 Then
              cmdReservarCama.Enabled = False
              cmdAnularReserva.Enabled = False
              cmdAsignarCama.Enabled = False
              cmddesasignarCama.Enabled = False
              cmdActPlanif.Enabled = False
              cmdcamasis.Enabled = False
          Else
              If grdDBGrid1(0).Columns(19).Value = "" Then
                  cmdReservarCama.Enabled = True
                  cmdAnularReserva.Enabled = False
                  cmdAsignarCama.Enabled = False
                  cmddesasignarCama.Enabled = False
                  cmdActPlanif.Enabled = True
                  cmdcamasis.Enabled = True
              Else
                  cmdReservarCama.Enabled = True
                  cmdAnularReserva.Enabled = True
                  cmdAsignarCama.Enabled = True
                  cmddesasignarCama.Enabled = True
                  cmdActPlanif.Enabled = True
                  cmdcamasis.Enabled = True
              End If
              IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
              IdPersona1.ReadPersona
          End If
    End Select
End Sub
Private Sub grdDBGrid1_DblClick(intIndex As Integer)
            
    Dim sStmSql     As String
    Dim qryUpdate   As rdoQuery
    
    Select Case intIndex
        Case iLSTCitas
            Call objWinInfo.GridDblClick
        Case iLSTCamas
            'Han seleccionado alguna cama que insertamos
            'en el registro de citas. Modificando la cita  (CI0100)
            'para incluirle la cama como reservada provisional
            sStmSql = "UPDATE CI0100 SET AD15CODCAMA = ?,  "
            sStmSql = sStmSql & "AD13CODTIPOCAMA = ?, "
            sStmSql = sStmSql & "WHERE "
            sStmSql = sStmSql & "AD01CODASISTENCI = ? AND "
            sStmSql = sStmSql & "AD07CODPROCESO) ?"
            
            
            Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
            qryUpdate(1) = Trim(Left$(grdDBGrid1(1).Columns(1).Value, 2))
            qryUpdate(2) = Trim(grdDBGrid1(0).Columns(18).Value)
            qryUpdate(3) = Trim(grdDBGrid1(0).Columns(15).Value)
            qryUpdate(4) = Trim(grdDBGrid1(0).Columns(14).Value)
            qryUpdate.Execute
            qryUpdate.Close
    End Select
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Select Case intIndex
        Case iLSTCitas
            Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
                If grdDBGrid1(0).Rows = 0 Then
                cmdReservarCama.Enabled = False
                cmdAnularReserva.Enabled = False
                cmdAsignarCama.Enabled = False
                cmddesasignarCama.Enabled = False
                cmdActPlanif.Enabled = False
                cmdcamasis.Enabled = False
            Else
                If grdDBGrid1(0).Columns(19).Value = "" Then
                    cmdReservarCama.Enabled = True
                    cmdAnularReserva.Enabled = False
                    cmdAsignarCama.Enabled = False
                    cmddesasignarCama.Enabled = False
                    cmdActPlanif.Enabled = True
                    cmdcamasis.Enabled = True
                Else
                    cmdReservarCama.Enabled = True
                    cmdAnularReserva.Enabled = True
                    cmdAsignarCama.Enabled = True
                    cmddesasignarCama.Enabled = True
                    cmdActPlanif.Enabled = True
                    cmdcamasis.Enabled = True
                End If
                IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
                IdPersona1.ReadPersona
            End If
    End Select
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Select Case intIndex
        Case iLSTCitas
            Call objWinInfo.CtrlDataChange
    End Select
End Sub
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  If intIndex = 3 Then
    SSDBCombo1(0).Enabled = True
  End If
End Sub
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub
Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub
Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

End Sub

Private Sub pCargaCombo(Index As Integer)
   
    Dim SQL As String
    Dim rs  As rdoResultset
    Dim qry As rdoQuery
    
    Select Case Index
        Case iCBODOCTOR
            SQL = "SELECT AG11CODRECURSO, AG11DESRECURSO "
            SQL = SQL & "FROM AG1100 "
            SQL = SQL & "WHERE AG1100.AD02CODDPTO =  " & Trim(SSDBCombo1(0).Columns(0).Value)
            SQL = SQL & "AND (AG1100.AG11FECFINVREC > SYSDATE "
            SQL = SQL & "OR AG1100.AG11FECFINVREC IS NULL) "
            SSDBConsulta(iCBODOCTOR).RemoveAll
            Set rs = objApp.rdoConnect.OpenResultset(SQL)
            While rs.EOF = False
                SSDBConsulta(iCBODOCTOR).AddItem rs("AG11CODRECURSO").Value & ";" & rs("AG11DESRECURSO").Value
                SSDBConsulta(iCBODOCTOR).Text = rs("AG11DESRECURSO").Value
                SSDBConsulta(iCBODOCTOR).MoveNext
                rs.MoveNext
            Wend
            rs.Close
            Set rs = Nothing
    End Select
    SSDBConsulta(iCBODOCTOR).Text = ""
End Sub
Private Sub pCargarCamas()
            
    Dim sStmSql As String
    Dim sRow    As String
    Dim rs      As rdoResultset
    

    sStmSql = "SELECT AD1500.AD15CODCAMA, AD1500.AD13CODTIPOCAMA, AD1300.AD13DESTIPOCAMA, "
    sStmSql = sStmSql & "AD1500.AD14CODESTCAMA, AD1400.AD14DESESTCAMA "
    sStmSql = sStmSql & "FROM AD1500, AD1400, AD1300 "
    sStmSql = sStmSql & "WHERE AD1500.AD13CODTIPOCAMA = AD1300.AD13CODTIPOCAMA "
    sStmSql = sStmSql & "  AND AD1500.AD14CODESTCAMA = AD1400.AD14CODESTCAMA"
    sStmSql = sStmSql & "  AND AD1500.AD02CODDPTO = " & Trim(SSDBCombo1(0).Columns(0).Value)
    sStmSql = sStmSql & "  AND (AD1500.AD14CODESTCAMA = " & iEstado_Ocupado & " "
    sStmSql = sStmSql & "   OR AD1500.AD14CODESTCAMA = " & iEstado_Libre & ") "
    
    grdDBGrid1(iLSTCamas).RemoveAll
    Set rs = objApp.rdoConnect.OpenResultset(sStmSql)
    While rs.EOF = False
        sRow = rs("AD15CODCAMA") & Chr(9)
        sRow = sRow & rs("AD13CODTIPOCAMA") & "   " & rs("AD13DESTIPOCAMA") & Chr(9)
        sRow = sRow & rs("AD14CODESTCAMA") & "   " & rs("AD14DESESTCAMA") & Chr(9)
        grdDBGrid1(iLSTCamas).AddItem sRow
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
    
End Sub

