VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmRealizacionActuaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Realizaci�n de Actuaciones"
   ClientHeight    =   8580
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8580
   ScaleWidth      =   11910
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdComun 
      Caption         =   "Dr. Infor. Planta"
      Height          =   375
      Index           =   3
      Left            =   3120
      TabIndex        =   36
      Top             =   8100
      Width           =   1380
   End
   Begin VB.CommandButton cmdBiopsia 
      Caption         =   "&Anatom�a"
      Height          =   375
      Left            =   10575
      TabIndex        =   34
      Top             =   8100
      Visible         =   0   'False
      Width           =   1170
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir  ^"
      Height          =   375
      Left            =   6135
      TabIndex        =   33
      Top             =   8100
      Width           =   1365
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "&Datos Actuacion"
      Height          =   375
      Index           =   1
      Left            =   75
      TabIndex        =   31
      Top             =   8100
      Width           =   1380
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Rec. &Previstos"
      Height          =   375
      Index           =   0
      Left            =   1575
      TabIndex        =   30
      Top             =   8100
      Width           =   1380
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "&Farmacia"
      Height          =   375
      Index           =   6
      Left            =   9300
      TabIndex        =   28
      Top             =   8100
      Width           =   1140
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "&Visi�n global"
      Height          =   375
      Index           =   9
      Left            =   4635
      TabIndex        =   26
      Top             =   8100
      Width           =   1365
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Petici�&n"
      Height          =   375
      Index           =   10
      Left            =   8025
      TabIndex        =   25
      Top             =   8100
      Width           =   1170
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Mensa&jes"
      Height          =   375
      Index           =   8
      Left            =   10560
      TabIndex        =   23
      Top             =   600
      Width           =   1155
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   11580
      Top             =   5520
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10560
      TabIndex        =   11
      Top             =   120
      Width           =   1155
   End
   Begin VB.Frame Frame4 
      Caption         =   "Actuaciones Realizadas Hoy"
      ForeColor       =   &H00C00000&
      Height          =   2310
      Left            =   60
      TabIndex        =   3
      Top             =   5700
      Width           =   11715
      Begin VB.CommandButton cmdRecConsum 
         Caption         =   "&Rec. Consum."
         Height          =   375
         Left            =   10440
         TabIndex        =   21
         Top             =   1200
         Width           =   1155
      End
      Begin VB.CommandButton cmdVolverFinRea 
         Caption         =   "Volver Rea&liz."
         Height          =   375
         Left            =   10440
         TabIndex        =   20
         Top             =   720
         Width           =   1155
      End
      Begin VB.CommandButton cmdVolverFinPend 
         Caption         =   "Volver Pen&d."
         Height          =   375
         Left            =   10440
         TabIndex        =   19
         Top             =   240
         Width           =   1155
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdFinalizadas 
         Height          =   2010
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   3545
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Actuaciones Realiz�ndose"
      ForeColor       =   &H00C00000&
      Height          =   2310
      Left            =   60
      TabIndex        =   2
      Top             =   3360
      Width           =   11715
      Begin VB.CommandButton cmdAnularRea 
         Caption         =   "An&ular"
         Height          =   375
         Left            =   10440
         TabIndex        =   18
         Top             =   1200
         Width           =   1155
      End
      Begin VB.CommandButton cmdVolverReaPend 
         Caption         =   "&Volver Pend."
         Height          =   375
         Left            =   10440
         TabIndex        =   17
         Top             =   720
         Width           =   1155
      End
      Begin VB.CommandButton cmdTerminar 
         Caption         =   "&Terminar"
         Height          =   375
         Left            =   10440
         TabIndex        =   8
         Top             =   240
         Width           =   1155
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdRealizandose 
         Height          =   2010
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   3545
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Actuaciones Pendientes"
      ForeColor       =   &H00C00000&
      Height          =   2310
      Left            =   60
      TabIndex        =   1
      Top             =   1020
      Width           =   11715
      Begin VB.CommandButton cmdCambiar 
         Caption         =   "Cam&biar"
         Height          =   375
         Left            =   10440
         TabIndex        =   24
         Top             =   1440
         Width           =   1155
      End
      Begin VB.CommandButton cmdConsSI 
         Caption         =   "C&onsent.: SI"
         Height          =   375
         Left            =   10440
         TabIndex        =   16
         Top             =   600
         Width           =   1155
      End
      Begin VB.CommandButton cmdConsNO 
         Caption         =   "Co&nsent.: NO"
         Height          =   375
         Left            =   10440
         TabIndex        =   15
         Top             =   1020
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnularPend 
         Caption         =   "&Anular"
         Height          =   375
         Left            =   10425
         TabIndex        =   14
         Top             =   1875
         Width           =   1155
      End
      Begin VB.CommandButton cmdIniciar 
         Caption         =   "&Iniciar"
         Height          =   375
         Left            =   10440
         TabIndex        =   7
         Top             =   180
         Width           =   1155
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdPendientes 
         Height          =   2010
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   3545
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dpto. / Recurso"
      ForeColor       =   &H00C00000&
      Height          =   915
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   10335
      Begin ComctlLib.ListView lvwRec 
         Height          =   735
         Left            =   5460
         TabIndex        =   22
         Top             =   120
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   1296
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   375
         Left            =   8940
         TabIndex        =   13
         Top             =   300
         Width           =   1155
      End
      Begin SSDataWidgets_B.SSDBCombo sscboDpto 
         Height          =   315
         Left            =   1020
         TabIndex        =   12
         Top             =   360
         Width           =   3135
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4974
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5530
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Recurso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   4320
         TabIndex        =   10
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Dpto."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   180
         TabIndex        =   9
         Top             =   420
         Width           =   795
      End
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Imp. &Etiquetas"
      Height          =   375
      Index           =   2
      Left            =   9975
      TabIndex        =   32
      Top             =   7425
      Visible         =   0   'False
      Width           =   1260
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Imp. P&etici�n"
      Height          =   375
      Index           =   5
      Left            =   10050
      TabIndex        =   29
      Top             =   7350
      Visible         =   0   'False
      Width           =   1260
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Imp. P&rograma"
      Height          =   375
      Index           =   7
      Left            =   9975
      TabIndex        =   27
      Top             =   7425
      Visible         =   0   'False
      Width           =   1260
   End
   Begin vsViewLib.vsPrinter vsPet 
      Height          =   3315
      Left            =   1950
      TabIndex        =   35
      Top             =   1125
      Visible         =   0   'False
      Width           =   3765
      _Version        =   196608
      _ExtentX        =   6641
      _ExtentY        =   5847
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
   End
   Begin VB.Image imgEscudo 
      Height          =   1575
      Left            =   2625
      Picture         =   "PR0500.frx":0000
      Stretch         =   -1  'True
      Top             =   1275
      Width           =   1350
   End
   Begin VB.Menu mnuImprimir 
      Caption         =   "Imprimir"
      NegotiatePosition=   2  'Middle
      Visible         =   0   'False
      Begin VB.Menu mnuImprimirOpcion 
         Caption         =   "&Petici�n"
         Index           =   10
      End
      Begin VB.Menu mnuImprimirOpcion 
         Caption         =   "P&rograma"
         Index           =   20
      End
      Begin VB.Menu mnuImprimirOpcion 
         Caption         =   "&Etiquetas"
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRealizacionActuaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim intDptoSel%, strRecSel$
Dim strMeCaption$
Dim lngUrgencias&

Private Sub cmdAnularPend_Click()
    Call pAnularAct(ssgrdPendientes)
End Sub

Private Sub cmdAnularRea_Click()
    Call pAnularAct(ssgrdRealizandose)
End Sub

Private Sub cmdBiopsia_Click()
Dim grd As SSDBGrid
Dim cDptPeti As Integer, cDptCargo As Integer, PrRealizada As Integer
Dim nPrPlan As Long, nPet As Long, nProc As Long, nAsist As Long, nPrPed As Long
Dim nPers As Long, nPrPed_Endos As Long, cPrAP As Long
Dim sql As String, texto As String, Observ As String, Resumen As String, Muestras As String
Dim PrAP As String
Dim Diag As String
Dim cRecRea As Integer
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
    
  If ssgrdPendientes.SelBookmarks.Count > 0 Then
    MsgBox "Para pedir una biopsia es necesario que la prueba se haya empezado a realizar.", vbInformation, "Petici�n de biopsia asociada"
    Exit Sub
  ElseIf ssgrdRealizandose.SelBookmarks.Count > 0 Then
    Set grd = ssgrdRealizandose
    PrRealizada = False
  ElseIf ssgrdFinalizadas.SelBookmarks.Count > 0 Then
    PrRealizada = True
    Set grd = ssgrdFinalizadas
  Else
    MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, strMeCaption
    Exit Sub
  End If
  
  If grd.SelBookmarks.Count = 1 Then
    nPrPlan = CLng(grd.Columns("Num. Act.").CellText(grd.SelBookmarks(0)))
    nPers = CLng(grd.Columns("Persona").CellText(grd.SelBookmarks(0)))
  Else
    MsgBox "Debe seleccionar una �nica prueba.", vbExclamation, "Realizaci�n de actuaciones"
    Exit Sub
  End If
  
  sql = "SELECT PR0800.PR09NumPeticion, PR0400.AD01CodAsistenci, PR0400.AD07CodProceso, " _
      & "PR0800.PR08DesMotPet, PR0900.AD02CodDpto, PR0400.PR04IndIntCientif, " _
      & "PR0400.AD02CodDpto_Crg, PR0900.PR09DesObservac, PR4100.PR41Respuesta, " _
      & "PR0400.PR03NumActPedi " _
      & "FROM PR0400, PR0900, PR0800, PR4100 WHERE " _
      & "PR0400.PR03NumActPedi = PR0800.PR03NumActPedi AND " _
      & "PR0800.PR09NumPeticion = PR0900.PR09NumPeticion AND " _
      & "PR0400.PR03NumActPedi = PR4100.PR03NumActPedi (+) AND " _
      & "PR4100.PR40CodPregunta (+) = ? AND " _
      & "PR0400.PR04NumActPlan = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = constCPREGMOTIVO
  rdoQ(1) = nPrPlan
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "No se encuentra la petici�n.", vbExclamation, "Realizaci�n de actuaciones"
    Exit Sub
  End If
  
  nPet = rdo(0)
  nAsist = rdo(1)
  nProc = rdo(2)
  nPrPed_Endos = rdo(9)
  If IsNull(rdo(3)) Then
    If IsNull(rdo(8)) Then Resumen = "" Else Resumen = Trim$(rdo(8))
  Else
    Resumen = Trim$(rdo(3))
    If Len(Resumen) > 1 Then
      If Right$(Resumen, 1) = "." Then Resumen = Left$(Resumen, Len(Resumen) - 1)
    End If
    If Not IsNull(rdo(8)) Then Resumen = Resumen & ". " & rdo(8)
  End If
  
  If IsNull(rdo(7)) Then Observ = "" Else Observ = Trim$(rdo(7))
  cDptPeti = rdo(4)
  If IsNull(rdo(6)) Then cDptCargo = 0 Else cDptCargo = rdo(6)
  
  Load frmDatosBiopsia
  With frmDatosBiopsia
    .txtNH.Text = grd.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(0))
    .txtPac.Text = grd.Columns("Paciente").CellText(grd.SelBookmarks(0))
    .txtPrueba.Text = grd.Columns("Actuaci�n").CellText(ssgrdPendientes.SelBookmarks(0))
    .txtObserPet.Text = Observ
    .txtResumen.Text = Resumen
    .chkInvestigacion.Value = IIf(cDptCargo = 0, vbUnchecked, vbChecked)
    .Show vbModal
    If .seguir = False Then
      Unload frmDatosBiopsia
      Exit Sub
    Else
      Resumen = .txtResumen.Text
      Muestras = .txtMuestras
      cRecRea = .cRec
      Diag = .txtDiag.Text
      cPrAP = .cPrAP
      PrAP = .cboPrAP.Text
      If .chkInvestigacion.Value = vbChecked Then
        If cDptCargo = 0 Then cDptCargo = constDPTO_ENDOSCOPIAS
      Else
        cDptCargo = 0
      End If
      Unload frmDatosBiopsia
    End If
  End With
  
  objApp.BeginTrans
  nPrPed = InsertPR03(nPet, nPers, cDptCargo, cPrAP)
  If nPrPed = 0 Then ' Actuaci�n pedida
    texto = "Error al insertar la actuaci�n pedida. Error: " & Chr$(13) & Error
    GoTo Cancelar
  End If
  
  If InsertPR06(nPrPed, cPrAP) = False Then ' Fase pedida
    texto = "Error al insertar la fase pedida. Error: " & Chr$(13) & Error
    GoTo Cancelar
  End If
  
  If InsertPR14(nPrPed, cRecRea, nPrPed_Endos, PrRealizada, cPrAP) = False Then ' Tipo de recurso pedido
    texto = "Error al insertar el recurso que se ha pedido. Error: " & Chr$(13) & Error
    GoTo Cancelar
  End If
  
  If InsertPR08(nPet, nPrPed, nAsist, nProc, Resumen, Muestras, Diag) = False Then ' Observaciones prueba pedida - petici�n.
    texto = "Error al insertar la petici�n-actuaci�n pedida. Error: " & Chr$(13) & Error
    GoTo Cancelar
  End If

  If InsertPR04(nPrPed, nAsist, nProc, nPers, cDptCargo, cPrAP) = False Then ' Actuaci�n Planificada
    texto = "Error al insertar la prueba planificada. Error: " & Chr$(13) & Error
    GoTo Cancelar
  End If
  
  If InsertPR44(cDptPeti, cPrAP) = False Then ' Veces que un dpt pide una actuaci�n.
    texto = "Error al insertar las veces que el dpto ha pedido la prueba. Error: " & Chr$(13) & Error
    GoTo Cancelar
  End If
  
  Call ImprimirPetBiopsia(nPrPlan, cRecRea, Resumen, Muestras, Diag, PrAP)
  objApp.CommitTrans
  Exit Sub

Cancelar:
  objApp.RollbackTrans
  Screen.MousePointer = 0
  MsgBox texto, vbExclamation, Me.Caption

End Sub

Private Sub cmdCambiar_Click()
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim lngNumActPlan&, lngActiv&
    Dim strActSel$, i%
        
    Select Case ssgrdPendientes.SelBookmarks.Count
    Case 0
        sql = "No se ha seleccionado ninguna Actuaci�n."
    Case Else
        'se anota el N� Act. Plan. de una de las actuacciones seleccionadas para las _
        posteriores b�squedas de datos
        lngNumActPlan = ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(0))
        'se anotan todos los N� Act. Plan. seleccionados
        For i = 0 To ssgrdPendientes.SelBookmarks.Count - 1
            strActSel = strActSel & ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i)) & ","
        Next i
        'se comprueba que las actuaciones son del mismo tipo
        If Not fMismoTipoActividad(Left$(strActSel, Len(strActSel) - 1)) Then
            sql = "No se permite realizar el cambio conjunto de actuaciones de distinto tipo."
        End If
    End Select
    If sql <> "" Then MsgBox sql, vbExclamation, strMeCaption: Exit Sub

    'se busca el tipo de actividad
    sql = "SELECT PR0100.PR12CODACTIVIDAD"
    sql = sql & " FROM PR0400, PR0100"
    sql = sql & " WHERE PR0400.PR04NUMACTPLAN = ?"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset()
    lngActiv = rs(0)
    rs.Close
    qry.Close
    
    Call objPipe.PipeSet("PR_DPTO", intDptoSel)
    Call objPipe.PipeSet("PR_ACTIV", lngActiv)
    Call objPipe.PipeSet("PR_ACTPLAN", strActSel)
    frmCambioActuaciones.Show vbModal
    Set frmCambioActuaciones = Nothing
    DoEvents
    
    If objPipe.PipeExist("PR_Cambio") Then
        objPipe.PipeRemove ("PR_Cambio")
        Call pCargarActPendientes(-1)
    End If
End Sub

Private Sub cmdComun_Click(Index As Integer)
    Dim ssGrid As SSDBGrid, lngNumActPlan&, strHistoria$, strCodPersona$
    
    If ssgrdPendientes.SelBookmarks.Count > 0 Then
        Set ssGrid = ssgrdPendientes
    ElseIf ssgrdRealizandose.SelBookmarks.Count > 0 Then
        Set ssGrid = ssgrdRealizandose
    ElseIf ssgrdFinalizadas.SelBookmarks.Count > 0 Then
        Set ssGrid = ssgrdFinalizadas
    Else
        MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, strMeCaption
        Exit Sub
    End If
    
    Select Case Index
    Case 0, 1, 3, 10
      If ssGrid.SelBookmarks.Count = 1 Then
          lngNumActPlan = ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(0))
      Else
          MsgBox "Debe Ud. seleccionar una �nica Actuaci�n.", vbExclamation, strMeCaption
          Exit Sub
      End If
    Case 6
      'puede haber varias actuaciones seleccionadas del mismo paciente, pero lo que interesa _
      es el paciente, no la actuaci�n
      strHistoria = ssGrid.Columns("N� Hist.").CellText(ssGrid.SelBookmarks(0))
    Case 7, 8, 9, 2
      strCodPersona = ssGrid.Columns("Persona").CellText(ssGrid.SelBookmarks(0))
    End Select
    
    Select Case Index
    Case 0 'Ver recursos previstos
        Call pVerRecursos(lngNumActPlan)
    Case 1 'Ver datos / cuestionario
        Call pVerDatosActuacion(lngNumActPlan)
    Case 2 'imprimir etiquetas
       Call pImprimirEtiq(strCodPersona)
    Case 3
        Call pDrInforme(ssGrid)
    Case 5 'Imprimir petici�n
        Call pImprimirPeticion(ssGrid)
    Case 6 'Farmacia
        Call pFarmacia(strHistoria)
    Case 7 'imprimir programa del paciente
        Call pImprimirPrograma(strCodPersona)
    Case 8 'Dejar un aviso a una persona
        Call pDejarAviso(strCodPersona)
    Case 9
        Call pVisionGlobal(strCodPersona)
    Case 10 'Nueva petici�n
        Dim arData(1 To 1) As Variant
        If fAsociarAsist Then
            arData(1) = CVar(lngNumActPlan)
            Call objsecurity.LaunchProcess("PRA104", arData())
        Else
            Dim sql$, qry As rdoQuery, rs As rdoResultset
            Dim varCodProc As Variant
            sql = "SELECT AD07CODPROCESO"
            sql = sql & " FROM PR0400"
            sql = sql & " WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            Set rs = qry.OpenResultset()
            If Not IsNull(rs(0)) Then varCodProc = rs(0) Else varCodProc = 0
            rs.Close: qry.Close
            If varCodProc > 0 Then
                arData(1) = varCodProc
                Call objsecurity.LaunchProcess("PRA102", arData())
            Else
                MsgBox "No se ha encontrado el proceso.", vbExclamation, strMeCaption
                Exit Sub
            End If
        End If

    End Select
End Sub

Private Sub cmdConsNO_Click()
    Call pConsentimiento(False)
End Sub

Private Sub cmdConsSI_Click()
    Call pConsentimiento(True)
End Sub

Private Sub cmdConsultar_Click()
    Dim i%
    strRecSel = ""
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected Then
            strRecSel = strRecSel & lvwRec.ListItems(i).Tag & ","
            If i = 1 Then Exit For 'TODOS
        End If
    Next i
    If strRecSel <> "" Then
        strRecSel = Left$(strRecSel, Len(strRecSel) - 1)
        'Call pCargarActuaciones(strRecSel)
        Call pCargarActPendientes(-1)
        Call pCargarActRealizandose(-1)
        Call pCargarActFinalizadas(-1)
    Else
        MsgBox "No se ha seleccionado ning�n recurso.", vbExclamation, strMeCaption
    End If
End Sub

Private Sub cmdImprimir_Click()
  Me.PopupMenu mnuImprimir, vbPopupMenuLeftAlign, cmdImprimir.Left, cmdImprimir.Top + mnuImprimirOpcion.Count * mnuImprimirOpcion(10) * 280
End Sub

Private Sub cmdIniciar_Click()
    Call pIniciar
End Sub

Private Sub cmdRecConsum_Click()
    Dim msg$, sql$, qry As rdoQuery, rs As rdoResultset
    Dim lngNumActPlan&, strCodOldRec$, strCodNewRec$
    
    Select Case ssgrdFinalizadas.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Actuaci�n."
    Case 1
        lngNumActPlan = ssgrdFinalizadas.Columns("Num. Act.").CellText(ssgrdFinalizadas.SelBookmarks(0))
        
        'Acceso a Recursos Consumidos
        Call pVerRecursosConsumidos(lngNumActPlan)
    Case Else
        msg = "Debe Ud. seleccionar una �nica Actuaci�n."
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, strMeCaption
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdTerminar_Click()
    Call pTerminar
End Sub

Private Sub cmdVolverFinPend_Click()
    Call pVolverAPendientes(ssgrdFinalizadas)
End Sub

Private Sub cmdVolverFinRea_Click()
    Call pVolverARealizandose
End Sub

Private Sub cmdVolverReaPend_Click()
    Call pVolverAPendientes(ssgrdRealizandose)
End Sub

Private Sub Form_Load()
    Dim intDpto As Integer
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    strMeCaption = "Realizaci�n de Actuaciones"
    
    If objPipe.PipeExist("PR_dpto") Then
        intDpto = objPipe.PipeGet("PR_dpto")
        Call objPipe.PipeRemove("PR_dpto")
    End If

    'se cargan los departamentos realizadores a los que tiene acceso el usuario
    sql = "SELECT AD02CODDPTO, AD02DESDPTO"
    sql = sql & " FROM AD0200"
    sql = sql & " WHERE AD02CODDPTO IN ("
    sql = sql & " SELECT AD02CODDPTO FROM AD0300"
    sql = sql & " WHERE SG02COD = ?"
    sql = sql & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
    sql = sql & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " ORDER BY AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objsecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        intDptoSel = rs!AD02CODDPTO
        sscboDpto.Text = rs!AD02DESDPTO
        Me.Caption = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: TODOS"
        Do While Not rs.EOF
            If intDpto = rs!AD02CODDPTO Then
                intDptoSel = rs!AD02CODDPTO
                sscboDpto.Text = rs!AD02DESDPTO
            End If
            sscboDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
    
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ColumnHeaders.Add , , , 2000
    Call pCargarRecursos(intDptoSel)
    
    'se formatean los grids
    Call pFormatearGrids
    
    If intDptoSel = constDPTO_ENDOSCOPIAS Then
      cmdBiopsia.Visible = True
    Else
      cmdBiopsia.Visible = False
    End If
   
End Sub

Private Sub lvwRec_Click()
    Dim i%, msg$
    
    Call pVaciarGrids
 
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    If lvwRec.ListItems(1).Selected Then
        For i = 2 To lvwRec.ListItems.Count
            lvwRec.ListItems(i).Selected = False
        Next i
    End If
    'Se muestra en el Caption los recursos seleccionados
    Call pCaption
End Sub

Private Sub sscboDpto_Click()
    If intDptoSel <> sscboDpto.Columns(0).Value Then Call pVaciarGrids
    intDptoSel = sscboDpto.Columns(0).Value
    Me.Caption = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: TODOS"
    Call pCargarRecursos(intDptoSel)
    If intDptoSel = constDPTO_ENDOSCOPIAS Then
      cmdBiopsia.Visible = True
    Else
      cmdBiopsia.Visible = False
    End If
End Sub

Private Sub pCargarRecursos(intDptoSel%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim Item As ListItem
    
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ListItems.Clear
    Set Item = lvwRec.ListItems.Add(, , "TODOS")
    Item.Tag = 0
    Item.Selected = True

    sql = "SELECT AG11CODRECURSO, AG11DESRECURSO"
    sql = sql & " FROM AG1100"
    sql = sql & " WHERE AD02CODDPTO = ?"
    sql = sql & " AND SYSDATE BETWEEN AG11FECINIVREC "
    sql = sql & " AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        Set Item = lvwRec.ListItems.Add(, , rs!AG11DESRECURSO)
        Item.Tag = rs!AG11CODRECURSO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    
   If objPipe.PipeExist("PR_REC") Then
      If objPipe.PipeGet("PR_REC") <> 1 And objPipe.PipeGet("PR_REC") <> 0 Then
         lvwRec.SelectedItem.Selected = False
         lvwRec.DropHighlight = lvwRec.ListItems.Item(objPipe.PipeGet("PR_REC"))
         lvwRec.SelectedItem = lvwRec.FindItem(lvwRec.ListItems.Item(objPipe.PipeGet("PR_REC")), , objPipe.PipeGet("PR_REC"))
      End If
    End If
    
    
End Sub



Private Function fFechaActual()
'Devuelve la fecha y hora actual obtenida del servidor
    Dim sql$, rs As rdoResultset
    
    sql = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    fFechaActual = rs(0)
    rs.Close
End Function

Private Sub pFormatearGrids()
    'grid de actuaciones pendientes
    With ssgrdPendientes
        .Columns(0).Caption = "Fecha Prog."
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Actuaci�n"
        .Columns(1).Width = 2900
        .Columns(2).Caption = "N� Hist."
        .Columns(2).Alignment = ssCaptionAlignmentRight
        .Columns(2).Width = 700
        .Columns(3).Caption = "Paciente"
        .Columns(3).Width = 3100
        .Columns(4).Caption = "Entrada cola"
        .Columns(4).Width = 1300
        .Columns(5).Caption = "Cons."
        .Columns(5).Width = 600
        .Columns(6).Caption = "Num. Act."
        .Columns(6).Alignment = ssCaptionAlignmentRight
        .Columns(6).Width = 0
        .Columns(6).Visible = False
        .Columns(7).Caption = "Est. Muestra"
        .Columns(7).Width = 0
        .Columns(7).Visible = False
        .Columns(8).Caption = "Persona"
        .Columns(8).Width = 0
        .Columns(8).Visible = False
        .Columns(9).Caption = "Cama"
        .Columns(9).Width = 700
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
        
    'grid de actuaciones realiz�ndose
    With ssgrdRealizandose
        .Columns(0).Caption = "Fecha Inicio"
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Actuaci�n"
        .Columns(1).Width = 2900
        .Columns(2).Caption = "N� Hist."
        .Columns(2).Alignment = ssCaptionAlignmentRight
        .Columns(2).Width = 700
        .Columns(3).Caption = "Paciente"
        .Columns(3).Width = 3100
        .Columns(4).Caption = "Entrada cola"
        .Columns(4).Width = 1300
        .Columns(5).Caption = "Cons."
        .Columns(5).Width = 600
        .Columns(6).Caption = "Num. Act."
        .Columns(6).Width = 0
        .Columns(6).Visible = False
        .Columns(6).Alignment = ssCaptionAlignmentRight
        .Columns(7).Caption = "Est. Muestra"
        .Columns(7).Width = 0
        .Columns(7).Visible = False
        .Columns(8).Caption = "Persona"
        .Columns(8).Width = 0
        .Columns(8).Visible = False
        .Columns(9).Caption = "Cama"
        .Columns(9).Width = 700
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    
    'grid de actuaciones finalizadas
    With ssgrdFinalizadas
        .Columns(0).Caption = "Fecha Inicio"
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Fecha Fin"
        .Columns(1).Width = 1300
        .Columns(2).Caption = "Actuaci�n"
        .Columns(2).Width = 2900
        .Columns(3).Caption = "N� Hist."
        .Columns(3).Alignment = ssCaptionAlignmentRight
        .Columns(3).Width = 700
        .Columns(4).Caption = "Paciente"
        .Columns(4).Width = 3100
        .Columns(5).Caption = "Entrada cola"
        .Columns(5).Width = 0 '1300
        .Columns(5).Visible = False
        .Columns(6).Caption = "Cons."
        .Columns(6).Width = 600
        .Columns(7).Caption = "Num. Act."
        .Columns(7).Alignment = ssCaptionAlignmentRight
        .Columns(7).Width = 0
        .Columns(7).Visible = False
        .Columns(8).Caption = "Est. Muestra"
        .Columns(8).Width = 0
        .Columns(8).Visible = False
        .Columns(9).Caption = "Persona"
        .Columns(9).Width = 0
        .Columns(9).Visible = False
        .Columns(10).Caption = "Cama"
        .Columns(10).Width = 700
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub ssgrdFinalizadas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdFinalizadas_Click()
    ssgrdPendientes.SelBookmarks.RemoveAll
    ssgrdRealizandose.SelBookmarks.RemoveAll
    
    If ssgrdFinalizadas.SelBookmarks.Count > 1 Then
        If ssgrdFinalizadas.Columns("N� Hist.").CellText(ssgrdFinalizadas.SelBookmarks(0)) <> ssgrdFinalizadas.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdFinalizadas.SelBookmarks.Remove ssgrdFinalizadas.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdFinalizadas_HeadClick(ByVal ColIndex As Integer)
    If ssgrdFinalizadas.Rows > 0 Then Call pCargarActFinalizadas(ColIndex)
End Sub

Private Sub ssgrdPendientes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdPendientes_Click()
    ssgrdRealizandose.SelBookmarks.RemoveAll
    ssgrdFinalizadas.SelBookmarks.RemoveAll
    
    If ssgrdPendientes.SelBookmarks.Count > 1 Then
        If ssgrdPendientes.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(0)) <> ssgrdPendientes.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdPendientes.SelBookmarks.Remove ssgrdPendientes.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdPendientes_HeadClick(ByVal ColIndex As Integer)
    If ssgrdPendientes.Rows > 0 Then Call pCargarActPendientes(ColIndex)
End Sub

Private Sub ssgrdRealizandose_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdRealizandose_Click()
    ssgrdPendientes.SelBookmarks.RemoveAll
    ssgrdFinalizadas.SelBookmarks.RemoveAll
    
    If ssgrdRealizandose.SelBookmarks.Count > 1 Then
        If ssgrdRealizandose.Columns("N� Hist.").CellText(ssgrdRealizandose.SelBookmarks(0)) <> ssgrdRealizandose.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdRealizandose.SelBookmarks.Remove ssgrdRealizandose.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdRealizandose_HeadClick(ByVal ColIndex As Integer)
    If ssgrdRealizandose.Rows > 0 Then Call pCargarActRealizandose(ColIndex)
End Sub

Private Sub pVerRecursos(lngNumActPlan&)
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    frmConsultaRecursos.Show vbModal
    Set frmConsultaRecursos = Nothing
End Sub

Private Sub pConsentimiento(blnValor As Boolean)
    Dim lngNumActPlan&
    Dim sql$, qry As rdoQuery
    
    Select Case ssgrdPendientes.SelBookmarks.Count
    Case 0
        sql = "No se ha seleccionado ninguna Actuaci�n."
    Case 1
        Select Case ssgrdPendientes.Columns("Cons.").CellText(ssgrdPendientes.SelBookmarks(0))
        Case "SI"
            If blnValor = True Then Exit Sub
        Case "NO"
            If blnValor = False Then Exit Sub
        Case ""
            sql = "La Actuaci�n no necesita consentimiento del paciente."
        End Select
    Case Is > 1
        sql = "Debe Ud. seleccionar una �nica Actuaci�n."
    End Select
    If sql <> "" Then MsgBox sql, vbExclamation, strMeCaption: Exit Sub
    
    'esto s�lo es v�lido si s�lo hay un registro seleccionado
    lngNumActPlan = ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(0))
    
    sql = "UPDATE PR0300 SET PR03INDCONSFIRM = ?"
    sql = sql & " WHERE PR03NUMACTPEDI IN"
    sql = sql & " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    If blnValor = 0 Then qry(0) = 0 Else qry(0) = -1
    qry(1) = lngNumActPlan
    On Error Resume Next
    qry.Execute
    If Err = 0 Then
        'Esta asignaci�n s�lo es v�lido si el Click sobre el Grid supone la selcci�n del _
        registro
        If blnValor = 0 Then
            ssgrdPendientes.Columns("Cons.").Text = "NO"
        Else
            ssgrdPendientes.Columns("Cons.").Text = "SI"
        End If
        ssgrdPendientes.Update
    Else
        MsgBox Error
    End If
End Sub

Private Sub pIniciar()
'**************************************************************************************
'*  Inicia la realizaci�n de la actuaci�n seleccionada cambiando su estado a realiz�ndose
'*  Muestra la pantalla de observ., indic. y cuestionarios de la actuaci�n si las hubiera
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdo.rdoResultset, strAhora$
    Dim lngNumActPlan&, i%
    Dim blnBookmarks() As Boolean
    'se comprueba que se ha seleccionado alguna actuaci�n de las pendientes
    If ssgrdPendientes.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    'se comprueba si todas las actuaciones tienen el consentimiento firmado. Si alguna de _
    ellas no lo tiene no se puede seguir adelante
    For i = 0 To ssgrdPendientes.SelBookmarks.Count - 1
        If ssgrdPendientes.Columns("Cons.").CellText(ssgrdPendientes.SelBookmarks(i)) = "NO" Then
           sql = "El paciente no ha firmado el consentimiento necesario." & Chr$(13)
           MsgBox sql, vbInformation, strMeCaption
           Exit Sub
        End If
    Next i
    
    'se comprueba si est�n aportadas las muestras necesarias
    sql = ""
    For i = 0 To ssgrdPendientes.SelBookmarks.Count - 1
        If ssgrdPendientes.Columns("Est. Muestra").CellText(ssgrdPendientes.SelBookmarks(i)) < 3 Then
            sql = sql & ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i)) _
                    & Space(5) & ssgrdPendientes.Columns("Actuaci�n").CellText(ssgrdPendientes.SelBookmarks(i)) _
                    & Chr$(13)
        End If
    Next i
    If sql <> "" Then
        sql = "No se ha recepcionado la muestra o muestras de las siguientes actuaciones:" _
            & Chr$(13) & Chr$(13) & sql & Chr$(13) _
            & "�Desea Ud. iniciar las actuaciones?"
        If MsgBox(sql, vbQuestion + vbYesNo + vbDefaultButton2, strMeCaption) = vbNo Then Exit Sub
    End If
    
    'Inicia la actuaci�n
    ReDim blnBookmarks(ssgrdPendientes.SelBookmarks.Count)
    strAhora = fFechaActual
    
    For i = 0 To ssgrdPendientes.SelBookmarks.Count - 1
      lngNumActPlan = ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i))
  
      '---ANDREA, 21/03/2000
      'Old SQL statement
      'se anota la fecha-hora de inicio en la BD
      'SQL = "UPDATE PR0400 SET PR04FECINIACT = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),"
      'SQL = SQL & " PR37CODESTADO = ?"
      'SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
      
      'New one
      sql = "UPDATE PR0400 " + _
            "SET PR04FECINIACT = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " + _
            "    PR37CODESTADO = ? " + _
            "WHERE PR04NUMACTPLAN = ? " + _
            "      AND (AD01CODASISTENCI IS NOT NULL) " + _
            "      AND (AD07CODPROCESO IS NOT NULL)"
      
      Set qry = objApp.rdoConnect.CreateQuery("", sql)
      qry(0) = strAhora
      qry(1) = 3
      qry(2) = lngNumActPlan
      qry.Execute
      
      If (qry.RowsAffected > 0) Then
        'Update succeded --> Move row
        blnBookmarks(i) = True
        'se a�ade la actuaci�n al grid de actuaciones realiz�ndose
        ssgrdRealizandose.AddItem Format(strAhora, "dd/mm/yy hh:mm") & Chr$(9) _
                    & ssgrdPendientes.Columns("Actuaci�n").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Paciente").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Entrada cola").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Cons.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Est. Muestra").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Persona").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("CAMA").CellText(ssgrdPendientes.SelBookmarks(i)), 0
    
        'se muestran los datos de la actuaci�n y el cuestionario
        Call pVerDatosActuacion(lngNumActPlan, True)
      Else
        'Update failed (AD01CODASISTENCI or AD07CODPROCESO were null) --> Skip row
        blnBookmarks(i) = False
        'Display a message to the user with the rejected data
        With ssgrdPendientes
          MsgBox "No se puede iniciar la siguiente actuaci�n:" & vbCrLf & vbCrLf & _
                 "Actuaci�n: " & .Columns("Actuaci�n").CellText(.SelBookmarks(i)) & vbCrLf & _
                 "Paciente: " & .Columns("Paciente").CellText(.SelBookmarks(i)) & vbCrLf & _
                 "Entrada Cola: " & .Columns("Entrada cola").CellText(.SelBookmarks(i)) & vbCrLf & vbCrLf & _
                 "No est� asociado el Proceso/Asistencia.", _
                 vbExclamation, Me.Caption
          'The corresponding item is not moved to the other grid
        End With
      End If
    Next i
    
    '---ANDREA, 21/03/2000
    'Old code
    'se quitan las actuaciones del grid de las actuaciones pendientes
    'ssgrdPendientes.DeleteSelected
    
    'New code
    'se quitan solo las actuaciones iniciadas dejando selecionadas las que no se han podido
    'iniciar
    For i = ssgrdPendientes.SelBookmarks.Count - 1 To 0 Step -1
      If blnBookmarks(i) Then
        ssgrdPendientes.RemoveItem ssgrdPendientes.AddItemRowIndex(ssgrdPendientes.SelBookmarks(i))
      End If
    Next
End Sub

Private Sub pTerminar()

'**************************************************************************************
'*  Termina la realizaci�n de la actuaci�n seleccionada cambiando su estado a finalizada
'*  Anota las fases y los recursos (planificados y/o no planificables) consumidos
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset, strAhora$
    Dim lngNumActPlan&, i%
    Dim nProc As Long, nAsist As Long
    Dim qry2 As rdoQuery, rs2 As rdoResultset
    
    'se comprueba que se ha seleccionado alguna actuaci�n de las pendientes
    If ssgrdRealizandose.SelBookmarks.Count = 0 Then
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
        
    strAhora = fFechaActual
    
    On Error GoTo Canceltrans
    
    For i = 0 To ssgrdRealizandose.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdRealizandose.Columns("Num. Act.").CellText(ssgrdRealizandose.SelBookmarks(i))
    
        objApp.BeginTrans
        
        'se anota la fecha-hora de inicio en la BD
        sql = "UPDATE PR0400 SET PR04FECFINACT = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),"
        sql = sql & " PR37CODESTADO = ?"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strAhora
        qry(1) = constESTACT_REALIZADA
        qry(2) = lngNumActPlan
        qry.Execute
        
        'NOTA (22/08/2000): LAS SIGUIENTES L�NEAS DE C�DIGO SE HAN A�ADIDO PARA EVITAR EL
        'ERROR QUE SE PRODUCE DE CLAVE DUPLICADA YA QUE HAY VECES, NO SE SABE COMO, LOS
        'REGISTROS YA EST�N A�ADIDOS A LAS TABLAS PR0700 Y PR1000 PARA EL N� DE ACT. PLAN.
        'se eliminan las fases y los recursos
        sql = "DELETE FROM PR1000 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        qry.Execute
        sql = "DELETE FROM PR0700 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        qry.Execute
        If qry.RowsAffected > 0 Then
            'LOG para 'cazar' el error: quitar cuando se solucione
            sql = "UPDATE PR0400 SET PR04IndPadre = 3"
            sql = sql & " WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            qry.Execute
        End If
        'NOTA (22/08/2000)
        
        'Se anotan las fases
        sql = "INSERT INTO PR0700 (pr04numactplan, pr07numfase, pr07desfase,"
        sql = sql & " pr07numminocupac, pr07numfase_pre, pr07numminfpre, pr07nummaxfpre,"
        sql = sql & " pr07indhabnatu, pr07indinifin)"
        sql = sql & " SELECT ?, pr06numfase, pr06desfase, pr06numminocupac, pr06numfase_pre,"
        sql = sql & " pr06numminfpre, pr06nummaxfpre, pr06indhabnatu, pr06indinifin"
        sql = sql & " FROM PR0600"
        sql = sql & " WHERE pr03numactpedi ="
        sql = sql & " (SELECT pr03numactpedi from PR0400 WHERE pr04numactplan = ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        qry(1) = qry(0)
        qry.Execute
        qry.Close
        
        'para saber si es una actuaci�n de consulta de urgencias
        sql = "SELECT PR01CODACTUACION, AD01CodAsistenci, AD07CodProceso"
        sql = sql & " FROM PR0400"
        sql = sql & " WHERE PR0400.PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        Set rs = qry.OpenResultset()
        nAsist = rs(1)
        nProc = rs(2)
        lngUrgencias = rs("pr01codactuacion")
        Call objPipe.PipeSet("curgencias", lngUrgencias)
        
        'Se anotan los recursos consumidos (por defecto)
        'se mira si la actuaci�n est� citada o no
        sql = "SELECT ci31numsolicit, ci01numcita, ag11codrecurso"
        sql = sql & " FROM CI0100"
        sql = sql & " WHERE pr04numactplan = ?"
        sql = sql & " AND ci01sitcita = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        qry(1) = constESTCITA_CITADA
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If rs.EOF Then 'actuaci�n no citada: s�lo recursos no planificables
            'se cargan los recursos de la tabla PR1400 (recursos pedidos)
            sql = "INSERT INTO PR1000 (pr04numactplan, pr07numfase, pr10numnecesid,"
            sql = sql & " ag11codrecurso, pr10numunirec, pr10numminocurec, ag11codrecurso_pre)"
            sql = sql & " SELECT ?, pr06numfase, pr14numnecesid, ag11codrecurso,"
            sql = sql & " pr14numunirec, pr14numminocu, ag11codrecurso"
            sql = sql & " FROM PR1400"
            sql = sql & " WHERE pr03numactpedi ="
            sql = sql & " (SELECT pr03numactpedi from PR0400 WHERE pr04numactplan = ?)"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            qry(1) = qry(0)
            qry.Execute
        Else 'actuaci�n citada
            'hay que cargar los recursos planificados de la tabla CI2700 _
            y los no planificados de la tabla PR1400.
            
            'NOTA: '�COMO SE PUEDEN RELACIONAR LOS REGISTROS DE LA CI2700 Y DE LA PR1400?
            
            'recursos no planificados
            sql = "INSERT INTO PR1000 (pr04numactplan, pr07numfase, pr10numnecesid,"
            sql = sql & " ag11codrecurso,"
            sql = sql & " pr10numunirec, pr10numminocurec, ag11codrecurso_pre)"
            sql = sql & " SELECT ?, pr06numfase, pr14numnecesid, "
            sql = sql & " ag11codrecurso,"
            sql = sql & " pr14numunirec, pr14numminocu,"
            sql = sql & " ag11codrecurso"
            sql = sql & " FROM PR1400"
            sql = sql & " WHERE pr03numactpedi ="
            sql = sql & " (SELECT pr03numactpedi from PR0400 WHERE pr04numactplan = ?)"
            sql = sql & " AND pr14indplanif = 0"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            qry(1) = qry(0)
            qry.Execute
    
            'recursos planificados
            'NOTA: como pr10numnecesid se introduce el propio ag11codrecurso ya que es un _
            valor que s�lo se utiliza para completar la clave principal
            
            'NOTA: seg�n SATUR (V 24/09/99) el campo pr10numminocurec debe quedar en blanco _
            para que sea el usuario el que introduzca los datos. Con el campo pr10numunirec _
            todav�a no se sabe que hacer
            'EFS si el recurso citado es una persona arrastro su categoria
            sql = "SELECT AD31CODPUESTO FROM AD0300"
            sql = sql & " WHERE AD02CODDPTO = ?"
            sql = sql & " AND SG02COD = "
            sql = sql & " (SELECT NVL(SG02COD,'999')"
            sql = sql & " FROM AG1100"
            sql = sql & " WHERE AG11CODRECURSO = ?)"
            Set qry2 = objApp.rdoConnect.CreateQuery("", sql)
            qry2(0) = intDptoSel
            qry2(1) = rs(2)
            Set rs2 = qry2.OpenResultset()
            
            sql = "INSERT INTO PR1000 (pr04numactplan, AD31CODPUESTO, pr07numfase, pr10numnecesid,"
            sql = sql & " ag11codrecurso, pr10numunirec, pr10numminocurec, ag11codrecurso_pre)"
            sql = sql & " SELECT ?,?, ci15numfasecita, ag11codrecurso, "
            sql = sql & " ag11codrecurso, ?, ci27numdiasrec*24*60+ci27numhorarec*60+ci27numminurec,"
            sql = sql & " ag11codrecurso"
            sql = sql & " FROM CI2700"
            sql = sql & " WHERE ci31numsolicit = ?"
            sql = sql & " AND ci01numcita = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            If rs2.EOF Then qry(1) = Null Else qry(1) = rs2!AD31CODPUESTO 'EFS
            qry(2) = 1 'NOTA: hay que ver de donde se puede sacar este valor
            qry(3) = rs!CI31NUMSOLICIT
            qry(4) = rs!CI01NUMCITA
            qry.Execute
        End If
        rs.Close
        qry.Close
        
'        If CerrarAsist(lngNumActPlan, nAsist, nProc, strAhora) = True Then
          objApp.CommitTrans
'        Else
'          GoTo Canceltrans
'        End If
                
        Call pVerRecursosConsumidos(lngNumActPlan)
        'se a�ade la actuaci�n al grid de actuaciones finalizadas
        ssgrdFinalizadas.AddItem ssgrdRealizandose.Columns("Fecha Inicio").Text & Chr$(9) _
                & Format(strAhora, "dd/mm/yy hh:mm") & Chr$(9) _
                & ssgrdRealizandose.Columns("Actuaci�n").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                & ssgrdRealizandose.Columns("N� Hist.").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                & ssgrdRealizandose.Columns("Paciente").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                & ssgrdRealizandose.Columns("Entrada cola").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                & ssgrdRealizandose.Columns("Cons.").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                & ssgrdRealizandose.Columns("Num. Act.").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                & ssgrdRealizandose.Columns("Est. Muestra").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                & ssgrdRealizandose.Columns("Persona").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                & ssgrdRealizandose.Columns("CAMA").CellText(ssgrdRealizandose.SelBookmarks(i)), 0
    Next i
    
Canceltrans:
        If Err <> 0 Then
            objApp.RollbackTrans
            Call pCargarActRealizandose(-1)
            sql = "Se ha producido un error finalizando las actuaciones y alguna de ellas"
            sql = sql & " no han podido ser finalizada." & Chr$(13)
            sql = sql & "Avise al Servicio de Inform�tica."
            MsgBox sql, vbExclamation, strMeCaption
        Else
            ssgrdRealizandose.DeleteSelected
        End If
End Sub

Private Sub pVolverAPendientes(ssGrid As SSDBGrid)
'**************************************************************************************
'*  Se deja la actuaci�n seleccionada (realiz�ndose o finalizada) en estado pendiente
'*  Si la actuaci�n estaba terminada, se anulan los recursos y las fases asociados a la
'*  misma
'**************************************************************************************
    Dim lngNumActPlan&
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim intEstado%, strFechaProg$
    Dim i%
    Dim strCodAsist$, strCodProc$
    
    Select Case ssGrid.SelBookmarks.Count
    Case 0
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    Case 1
        sql = "�Desea Ud. volver a Pendiente la actuaci�n seleccionada?"
    Case Is > 1
        sql = "�Desea Ud. volver a Pendientes las " & ssGrid.SelBookmarks.Count & " actuaciones seleccionadas?"
    End Select
    If MsgBox(sql, vbQuestion + vbYesNo, strMeCaption) = vbNo Then Exit Sub
        
    'si alguna de las actuaciones est� terminada y facturada, no se deja volverla atr�s
    If ssGrid.Name = ssgrdFinalizadas.Name Then
        If fFacturada(ssgrdFinalizadas) Then Exit Sub
    End If
    
    objApp.BeginTrans
    On Error GoTo Canceltrans
    
    For i = 0 To ssGrid.SelBookmarks.Count - 1
        lngNumActPlan = ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(i))
        
        'se busca la fecha para la que se hab�a programado la actuaci�n dependiendo de si fue _
        citada o no
        sql = "SELECT CI01FECCONCERT FROM CI0100 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If Not rs.EOF Then
            intEstado = constESTACT_CITADA
            strFechaProg = rs!CI01FECCONCERT
        Else
            sql = "SELECT PR04FECPLANIFIC FROM PR0400 WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            intEstado = constESTACT_PLANIFICADA
            If Not IsNull(rs(0)) Then strFechaProg = rs(0) Else strFechaProg = ""
        End If
        rs.Close
        qry.Close
        
        'se elimina la fecha-hora de inicio y de fin de la actuaci�n y se deja en su _
        estado inicial (planificada o citada)
        sql = "UPDATE PR0400 SET PR04FECINIACT = ?, PR04FECFINACT = ?,"
        sql = sql & " PR37CODESTADO = ?"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Null
        qry(1) = Null
        qry(2) = intEstado
        qry(3) = lngNumActPlan
        qry.Execute
        
        'si la actuaci�n estaba finalizada, se eliminan las fases y los recursos
        If ssGrid.Name = ssgrdFinalizadas.Name Then
            'Se mira si es una Consulta de Urgencias...
            sql = "SELECT PR01CODACTUACION, AD01CODASISTENCI, AD07CODPROCESO"
            sql = sql & " FROM PR0400"
            sql = sql & " WHERE PR0400.PR04NUMACTPLAN = ?"
            sql = sql & " AND PR01CODACTUACION IN (" & constURGENCIAS_PR_CONSULTA & "," & constURGENCIAS_PR_VISITA & ")"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            Set rs = qry.OpenResultset()
            If Not rs.EOF Then '... es una Consulta de Urgencias
                strCodAsist = rs!AD01CODASISTENCI
                strCodProc = rs!AD07CODPROCESO
                
                'se cambia el responsable del Proceso/Asistencia
                sql = "DELETE FROM AD0500"
                sql = sql & " WHERE AD01CODASISTENCI = ?"
                sql = sql & " AND AD07CODPROCESO = ?"
                sql = sql & " AND AD05FECINIRESPON > "
                sql = sql & " (SELECT MIN(AD05FECINIRESPON)"
                sql = sql & " FROM AD0500"
                sql = sql & " WHERE AD01CODASISTENCI = ?"
                sql = sql & " AND AD07CODPROCESO = ?)"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = strCodAsist: qry(1) = strCodProc
                qry(2) = strCodAsist: qry(3) = strCodProc
                qry.Execute
                
                sql = "UPDATE AD0500 SET AD02CODDPTO = ?, SG02COD = ?, AD05FECFINRESPON = NULL"
                sql = sql & " WHERE AD01CODASISTENCI = ?"
                sql = sql & " AND AD07CODPROCESO = ?"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = constDPTO_URGENCIAS
                qry(1) = constURGENCIAS_DR_CONSCOLURG
                qry(2) = strCodAsist
                qry(3) = strCodProc
                qry.Execute
                qry.Close
                
                'se cambia el responsable del Proceso
                sql = "DELETE FROM AD0400"
                sql = sql & " WHERE AD07CODPROCESO = ?"
                sql = sql & " AND AD04FECINIRESPON > "
                sql = sql & " (SELECT MIN(AD04FECINIRESPON)"
                sql = sql & " FROM AD0400"
                sql = sql & " WHERE AD07CODPROCESO = ?)"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = strCodProc: qry(1) = strCodProc
                qry.Execute
                
                sql = "UPDATE AD0400 SET AD02CODDPTO = ?, SG02COD = ?, AD04FECFINRESPON = NULL"
                sql = sql & " WHERE AD07CODPROCESO = ?"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = constDPTO_URGENCIAS
                qry(1) = constURGENCIAS_DR_CONSCOLURG
                qry(2) = strCodProc
                qry.Execute
            End If
            rs.Close
            qry.Close
            
            sql = "DELETE FROM PR1000 WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            qry.Execute
            sql = "DELETE FROM PR0700 WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            qry.Execute
            If qry.RowsAffected = 0 Then
                'LOG para 'cazar' el error: quitar cuando se solucione
                sql = "UPDATE PR0400 SET PR04NUMMINDUR = 2"
                sql = sql & " WHERE PR04NUMACTPLAN = ?"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = lngNumActPlan
                qry.Execute
            End If
        End If
        
        'se a�ade la actuaci�n al grid de actuaciones pendientes
        ssgrdPendientes.AddItem Format(strFechaProg, "dd/mm/yy hh:mm") & Chr$(9) _
                                & ssGrid.Columns("Actuaci�n").CellText(ssGrid.SelBookmarks(i)) & Chr$(9) _
                                & ssGrid.Columns("N� Hist.").CellText(ssGrid.SelBookmarks(i)) & Chr$(9) _
                                & ssGrid.Columns("Paciente").CellText(ssGrid.SelBookmarks(i)) & Chr$(9) _
                                & ssGrid.Columns("Entrada Cola").CellText(ssGrid.SelBookmarks(i)) & Chr$(9) _
                                & ssGrid.Columns("Cons.").CellText(ssGrid.SelBookmarks(i)) & Chr$(9) _
                                & ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(i)) & Chr$(9) _
                                & ssGrid.Columns("Est. Muestra").CellText(ssGrid.SelBookmarks(i)) & Chr$(9) _
                                & ssGrid.Columns("Persona").CellText(ssGrid.SelBookmarks(i)) & Chr$(9) _
                                & ssGrid.Columns("CAMA").CellText(ssGrid.SelBookmarks(i)), 0
    Next i

    objApp.CommitTrans
    
    'se quitan las actuaciones del grid (realiz�ndose o pendientes)
    ssGrid.DeleteSelected
    Exit Sub
        
Canceltrans:
    objApp.RollbackTrans
    sql = "Se ha producido un error al Volver a Realiz�ndose." & Chr$(13)
    sql = sql & "Avise al Servicio de Inform�tica."
    MsgBox sql, vbExclamation, Me.Caption

End Sub

Private Sub pVolverARealizandose()
'**************************************************************************************
'*  Se deja la actuaci�n seleccionada (finalizada) en estado realiz�ndose
'*  Se anulan los recursos y las fases asociados a la misma
'**************************************************************************************
    Dim lngNumActPlan&
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim i%
    Dim strCodAsist$, strCodProc$
    
    Select Case ssgrdFinalizadas.SelBookmarks.Count
    Case 0
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    Case 1
        sql = "�Desea Ud. volver a Realiz�ndose la actuaci�n seleccionada?"
    Case Is > 1
        sql = "�Desea Ud. volver a Realiz�ndose las " & ssgrdFinalizadas.SelBookmarks.Count & " actuaciones seleccionadas?"
    End Select
    If MsgBox(sql, vbQuestion + vbYesNo, strMeCaption) = vbNo Then Exit Sub
    
    'si alguna de las actuaciones est� facturada, no se deja volverla atr�s
    If fFacturada(ssgrdFinalizadas) Then Exit Sub
    
    
    objApp.BeginTrans
    On Error GoTo Canceltrans
    
    For i = 0 To ssgrdFinalizadas.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdFinalizadas.Columns("Num. Act.").CellText(ssgrdFinalizadas.SelBookmarks(i))
        'se elimina la fecha-hora de inicio y de fin de la actuaci�n y se deja en su _
        estado inicial (planificada o citada)
        sql = "UPDATE PR0400 SET PR04FECFINACT = ?,"
        sql = sql & " PR37CODESTADO = ?"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Null
        qry(1) = constESTACT_REALIZANDOSE
        qry(2) = lngNumActPlan
        qry.Execute
        
        'Se mira si es una Consulta de Urgencias...
        sql = "SELECT PR01CODACTUACION, AD01CODASISTENCI, AD07CODPROCESO"
        sql = sql & " FROM PR0400"
        sql = sql & " WHERE PR0400.PR04NUMACTPLAN = ?"
        sql = sql & " AND PR01CODACTUACION IN (" & constURGENCIAS_PR_CONSULTA & "," & constURGENCIAS_PR_VISITA & ")"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        Set rs = qry.OpenResultset()
        If Not rs.EOF Then '... es una Consulta de Urgencias
            strCodAsist = rs!AD01CODASISTENCI
            strCodProc = rs!AD07CODPROCESO
            
            'se cambia el responsable del Proceso/Asistencia
            sql = "DELETE FROM AD0500"
            sql = sql & " WHERE AD01CODASISTENCI = ?"
            sql = sql & " AND AD07CODPROCESO = ?"
            sql = sql & " AND AD05FECINIRESPON > "
            sql = sql & " (SELECT MIN(AD05FECINIRESPON)"
            sql = sql & " FROM AD0500"
            sql = sql & " WHERE AD01CODASISTENCI = ?"
            sql = sql & " AND AD07CODPROCESO = ?)"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = strCodAsist: qry(1) = strCodProc
            qry(2) = strCodAsist: qry(3) = strCodProc
            qry.Execute
            
            sql = "UPDATE AD0500 SET AD02CODDPTO = ?, SG02COD = ?, AD05FECFINRESPON = NULL"
            sql = sql & " WHERE AD01CODASISTENCI = ?"
            sql = sql & " AND AD07CODPROCESO = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = constDPTO_URGENCIAS
            qry(1) = constURGENCIAS_DR_PASTRANA
            qry(2) = strCodAsist
            qry(3) = strCodProc
            qry.Execute
            qry.Close
            
            'se cambia el responsable del Proceso
            sql = "DELETE FROM AD0400"
            sql = sql & " WHERE AD07CODPROCESO = ?"
            sql = sql & " AND AD04FECINIRESPON > "
            sql = sql & " (SELECT MIN(AD04FECINIRESPON)"
            sql = sql & " FROM AD0400"
            sql = sql & " WHERE AD07CODPROCESO = ?)"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = strCodProc: qry(1) = strCodProc
            qry.Execute
            
            sql = "UPDATE AD0400 SET AD02CODDPTO = ?, SG02COD = ?, AD04FECFINRESPON = NULL"
            sql = sql & " WHERE AD07CODPROCESO = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = constDPTO_URGENCIAS
            qry(1) = constURGENCIAS_DR_PASTRANA
            qry(2) = strCodProc
            qry.Execute
        End If
        rs.Close
        qry.Close
        
        'se eliminan las fases y los recursos
        sql = "DELETE FROM PR1000 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        qry.Execute
        sql = "DELETE FROM PR0700 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNumActPlan
        qry.Execute
        If qry.RowsAffected = 0 Then
            'LOG para 'cazar' el error: quitar cuando se solucione
            sql = "UPDATE PR0400 SET PR04NUMMINDUR = 1"
            sql = sql & " WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngNumActPlan
            qry.Execute
        End If
        
        'se a�ade la actuaci�n al grid de actuaciones realiz�ndose
        ssgrdRealizandose.AddItem ssgrdFinalizadas.Columns("Fecha Inicio").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("Actuaci�n").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("N� Hist.").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("Paciente").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("Entrada Cola").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("Cons.").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("Num. Act.").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("Est. Muestra").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("Persona").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
                                & ssgrdFinalizadas.Columns("CAMA").CellText(ssgrdFinalizadas.SelBookmarks(i)), 0
    Next i

    objApp.CommitTrans
    
    'se quitan las actuaciones del grid de actuaciones finalizadas
    ssgrdFinalizadas.DeleteSelected
    Exit Sub
    
Canceltrans:
    objApp.RollbackTrans
    sql = "Se ha producido un error al Volver a Realiz�ndose." & Chr$(13)
    sql = sql & "Avise al Servicio de Inform�tica."
    MsgBox sql, vbExclamation, Me.Caption

End Sub

Private Sub pVerDatosActuacion(lngNumActPlan&, Optional blnMostrarSoloSiHayDatos As Boolean)
'**************************************************************************************
'*  Llama a la pantalla que muestra los datos (observ., indic., intruccines...) de la
'*  actuaci�n y los cuestionarios (cuestionarios + restricciones)
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = True significa que s�lo hay que mostrar la
'*  pantalla en el caso de que exista alguna observ., indic. o pregunta en el cuestionario.
'*  En este caso se hace previamente el Load de la pantalla, la cual devolver� en un Pipe
'*  la informaci�n necesaria para saber si hay que hacer el Show o el Unload
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = False se muestra la pantalla directamente
'**************************************************************************************

    'se establece el Pipe con el n� de actuaci�n planificada
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    
    'se mira la opci�n seleccionada: cargar pantalla previamente o mostrar directamente
    If blnMostrarSoloSiHayDatos Then 'cargar pantalla previamente
        Call objPipe.PipeSet("PR_VerSoloSiHayDatos", True)
        'se elimina el posible Pipe devuelto por frmObservAct en operaciones previas
        If objPipe.PipeExist("PR_frmObservAct") Then Call objPipe.PipeRemove("PR_frmObservAct")
        'se carga en memoria la pantalla
        Load frmObservAct
        'se mira si hay que mostrarla o descargarla
        If objPipe.PipeExist("PR_frmObservAct") Then
            If objPipe.PipeGet("PR_frmObservAct") = True Then
                frmObservAct.Show vbModal
            Else
                Unload frmObservAct
            End If
            Call objPipe.PipeRemove("PR_frmObservAct")
        Else
            Unload frmObservAct
        End If
        Call objPipe.PipeRemove("PR_VerSoloSiHayDatos")
    Else 'mostrar pantalla directamente
        frmObservAct.Show vbModal
        Set frmObservAct = Nothing
    End If
End Sub

Private Sub pAnularAct(ssgrd As SSDBGrid)
'**************************************************************************************
'*  Anula una actuaci�n planificada cambiando su estado a anulada
'**************************************************************************************
    Dim strCancel$, sql$, qryPR04 As rdoQuery, qryCI01 As rdoQuery, i%, lngNumActPlan&
    
    Select Case ssgrd.SelBookmarks.Count
    Case 0
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    Case 1
        sql = "Si Ud. anula la actuaci�n ya no estar� disponible para ser realizada." & Chr$(13)
        sql = sql & "Indique el motivo por el que se va a anular la actuaci�n:"
    Case Is > 1
        sql = "Si Ud. anula las actuaciones ya no estar�n disponibles para ser realizadas." & Chr$(13)
        sql = sql & "Indique el motivo por el que se van a anular las actuaciones:"
    End Select
    strCancel = Trim$(InputBox(sql, "Anular Actuaci�n"))
    If strCancel = "" Then
        sql = "La actuaci�n NO ha sido anulada: hace falta indicar un motivo."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    
        
    'Querys para la anulaci�n de la actuaci�n y la cita
    sql = "UPDATE PR0400 SET PR04DESMOTCAN = ?,"
    sql = sql & " PR37CODESTADO = " & constESTACT_CANCELADA & ","
    sql = sql & " PR04FECCANCEL = SYSDATE,"
    sql = sql & " PR04FECENTRCOLA = NULL,"
    sql = sql & " PR04FECINIACT = NULL,"
    sql = sql & " PR04FECFINACT = NULL"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR04 = objApp.rdoConnect.CreateQuery("", sql)
    
    sql = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_ANULADA & "'"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA IN ('" & constESTCITA_CITADA & "','" & constESTCITA_PENDRECITAR & "')"
    Set qryCI01 = objApp.rdoConnect.CreateQuery("", sql)

    For i = 0 To ssgrd.SelBookmarks.Count - 1
        lngNumActPlan = ssgrd.Columns("Num. Act.").CellText(ssgrd.SelBookmarks(i))
        
        'anular la actuaci�n
        If Len(strCancel) > 50 Then qryPR04(0) = Left$(strCancel, 50) Else qryPR04(0) = strCancel
        qryPR04(1) = lngNumActPlan
        qryPR04.Execute
        
        'anular la cita
        qryCI01(0) = lngNumActPlan
        qryCI01.Execute
        
        'anular las actuaciones y citas asociadas
        Call pAnularAsociadas(lngNumActPlan)
    Next i
    qryPR04.Close
    qryCI01.Close

    'se elimina la actuaci�n del Grid
    ssgrd.DeleteSelected
End Sub

Private Sub pVaciarGrids()
    ssgrdPendientes.RemoveAll
    ssgrdRealizandose.RemoveAll
    ssgrdFinalizadas.RemoveAll
End Sub

'''''Private Sub pPedirActuacion(lngNumActPlan&, intOpcion%)
''''''**************************************************************************************
''''''*  Llama a trav�s del Launcher al proceso de petici�n de actuaciones
''''''*  Hay que pasar una serie de par�metros a trav�s de un array tipo variant
''''''*
''''''*  El par�metro intOpcion indica el Tipo de Acceso:
''''''*    0 - Petici�n de Actuaciones en la Misma Petici�n
''''''*    1 - Petici�n de Actuaciones asociadas (actuaciones ya realizadas --> se da al paciente
''''''*          como recibido)
''''''*    2 - Petici�n de Actuaciones en una Nueva Petici�n
''''''**************************************************************************************
'''''  Dim vntdatos(8), SQL, qry As rdoQuery, rs As rdoResultset
'''''  Dim strMsg As String
'''''
'''''  Select Case intOpcion
'''''    Case 0, 1 '0 - Misma Petici�n; 1 - Asociadas
'''''      '-----------------------------------------------------------------------------------
'''''      'REGLAS :
'''''
'''''      '0 - Misma Peticion
'''''      'PROCESO IS NULL       --> PROCESO=0, ASISTENCIA=0 y pedir
'''''      'ASISTENCIA IS NULL    --> PROCESO=0, ASISTENCIA=0 y pedir
'''''      'ASISTENCIA CERRADA    --> no pedir
'''''      'ASISTENCIA NO CERRADA --> PROCESO=corriente, ASISTENCIA=0/corriente y pedir
'''''
'''''      '1 - Asociadas
'''''      'PROCESO IS NULL       --> no pedir
'''''      'ASISTENCIA IS NULL    --> no pedir
'''''      'ASISTENCIA CERRADA    --> no pedir
'''''      'ASISTENCIA NO CERRADA --> PROCESO=corriente, ASISTENCIA=corriente y pedir
'''''      '-----------------------------------------------------------------------------------
'''''
'''''      'Se obtiene el c�digo de persona, el proceso y la asistencia de la actuaci�n, el
'''''      'n�mero de petici�n y la fecha en que se ha cerrado la asistencia (si hay una)
'''''      SQL = "SELECT PR0400.CI21CODPERSONA, NVL(PR0400.AD01CODASISTENCI,0) AD01CODASISTENCI,"
'''''      SQL = SQL & " NVL(PR0400.AD07CODPROCESO,0) AD07CODPROCESO, PR0300.PR09NUMPETICION,"
'''''      SQL = SQL & " AD0100.AD01FECFIN"
'''''      SQL = SQL & " FROM PR0300, PR0400, AD0100"
'''''      SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
'''''      SQL = SQL & " AND (PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI)"
'''''      SQL = SQL & " AND (PR0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI(+) )"
'''''
'''''      Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'''''      qry(0) = lngNumActPlan
'''''      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'''''
'''''      'EFS: HACEMOS QUE LLAME DIRECTAMENTE A LA PANTALLA DE PEDIR PRUEBAS
'''''      If Not rs.EOF Then
'''''
'''''        vntdatos(4) = rs!AD07CODPROCESO  'Codigo proceso corriente
'''''
'''''        If intOpcion = 1 Then
'''''
'''''          If rs("AD07CODPROCESO") = 0 Or rs("AD01CODASISTENCI") = 0 Then
'''''            'No se puede pedir
'''''            'Mensaje para el usuario
'''''            strMsg = "No est� asociado el Proceso/Asistecia." + vbCrLf + _
'''''                     "No se pueden hacer peticiones asociadas."
'''''            MsgBox strMsg, vbCritical, Me.Caption
'''''            Exit Sub
'''''          End If
'''''
'''''          If Not IsNull(rs("AD01FECFIN")) Then
'''''            'Asistencia cerrada --> No se puede pedir
'''''            vntdatos(5) = 0
'''''            'Mensaje para el usuario
'''''            strMsg = "La asistencia est� cerrada." + vbCrLf + _
'''''                     "No se pueden hacer peticiones asociadas."
'''''            MsgBox strMsg, vbCritical, Me.Caption
'''''            Exit Sub
'''''          Else
'''''            'Asistencia abierta --> Asocia autom�ticamente la misma asistencia
'''''            vntdatos(5) = rs!AD01CODASISTENCI 'Codigo asistencia corriente
'''''          End If
'''''        Else
'''''
'''''          If rs("AD07CODPROCESO") = 0 Or rs("AD01CODASISTENCI") = 0 Then
'''''            'Pone proceso y asistencia a 0 y sigue
'''''            vntdatos(4) = 0
'''''            vntdatos(5) = 0
'''''            'Mensaje para el usuario
'''''            strMsg = "No est� asociado Proceso/Asistencia." + _
'''''                     "Las prubas que pida quedaran sin asociar." + vbCrLf + _
'''''                     "�Desea continuar?"
'''''            If MsgBox(strMsg, vbQuestion + vbYesNo, Me.Caption) = vbNo Then Exit Sub
'''''          Else
'''''            'Solo se puede asociar si la asistencia no est� cerrada (AD01FECFIN <> NULL)
'''''
'''''            If Not IsNull(rs("AD01FECFIN")) Then
'''''              'Asistencia cerrada --> no se puede pedir
'''''              'Mensaje para el usuario
'''''              strMsg = "La asistencia est� cerrada." + vbCrLf + _
'''''                       "No se puede pedir una actuaci�n."
'''''              MsgBox strMsg, vbCritical, Me.Caption
'''''              Exit Sub
'''''            End If
'''''
'''''            'Asistencia abierta --> se pide el tipo de asociaci�n
'''''            vntdatos(5) = 0
'''''            If fAsociarAsist Then
'''''              vntdatos(5) = rs!AD01CODASISTENCI 'Codigo asistencia corriente
'''''            End If
'''''          End If
'''''
'''''        End If
'''''
'''''        vntdatos(1) = rs!CI21CODPERSONA  'cod. persona
'''''        vntdatos(2) = rs!PR09NUMPETICION 'n� de petici�n
'''''      End If
'''''
'''''      rs.Close
'''''      qry.Close
'''''      vntdatos(3) = intDptoSel
'''''      vntdatos(6) = intOpcion '0 - Actuaciones Misma Pet; 1 - Actuaciones Asociadas
'''''
'''''      'Mensaje de comprobaci�n (luego quitar)
'''''      'If MsgBox("Proceso    = " & vntdatos(4) & vbCrLf & _
'''''      '          "Asistencia = " & vntdatos(5) & vbCrLf & _
'''''      '          "Seguir ?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
'''''
'''''      'Launch the process
'''''      Call objsecurity.LaunchProcess("PR0153", vntdatos)
'''''
'''''    Case 2 '2 - Nueva Petici�n
'''''      '-----------------------------------------------------------------------------------
'''''      'REGLAS :
'''''
'''''      'PROCESO IS NULL       --> PROCESO=0, ASISTENCIA=0 y pedir
'''''      'ASISTENCIA IS NULL    --> PROCESO=0, ASISTENCIA=0 y pedir
'''''
'''''      'ASISTENCIA CERRADA    --> PROCESO=corriente, ASISTENCIA=0 y pedir
'''''      'ASISTENCIA NO CERRADA --> PROCESO=corriente, ASISTENCIA=0/corriente y pedir
'''''      '-----------------------------------------------------------------------------------
'''''
'''''      'Se obtiene el c�digo de persona, el proceso y la asistencia de la actuaci�n, la
'''''      'fecha en que se ha cerrado la asistencia (si hay una).
'''''      'En este caso no se hace referencia a ninguna petici�n.
'''''      SQL = "SELECT PR0400.CI21CODPERSONA, NVL(PR0400.AD01CODASISTENCI,0) AD01CODASISTENCI,"
'''''      SQL = SQL & " NVL(PR0400.AD07CODPROCESO,0) AD07CODPROCESO, AD0100.AD01FECFIN"
'''''      SQL = SQL & " FROM PR0400, AD0100"
'''''      SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
'''''      SQL = SQL & " AND (PR0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI(+) )"
'''''
'''''      Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'''''      qry(0) = lngNumActPlan
'''''      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'''''      If Not rs.EOF Then
'''''
'''''        vntdatos(5) = rs!AD07CODPROCESO 'cod. proceso
'''''
'''''        If rs("AD07CODPROCESO") = 0 Or rs("AD01CODASISTENCI") = 0 Then
'''''          'Pone proceso y asistencia a 0 y sigue
'''''          vntdatos(5) = 0
'''''          vntdatos(6) = 0
'''''          'Mensaje para el usuario
'''''          strMsg = "No est� asociado Proceso/Asistencia." + _
'''''                   "Las prubas que pida quedaran sin asociar." + vbCrLf + _
'''''                   "�Desea continuar?"
'''''          If MsgBox(strMsg, vbQuestion + vbYesNo, Me.Caption) = vbNo Then Exit Sub
'''''        Else
'''''          'Solo se puede asociar si la asistencia no est� cerrada (AD01FECFIN <> NULL) y
'''''          'si el usuario lo pide explicitamente
'''''          If Not IsNull(rs("AD01FECFIN")) Then
'''''            'Pone asistecia a 0
'''''            vntdatos(6) = 0
'''''            'Mensaje para el usuario y continuar
'''''            strMsg = "La asistencia est� cerrada. Solo se puede asociar el proceso."
'''''            MsgBox strMsg, vbInformation, Me.Caption
'''''          Else
'''''            'Pedir tipo de asociaci�n y continuar
'''''            vntdatos(6) = 0
'''''            If fAsociarAsist Then
'''''              vntdatos(6) = rs!AD01CODASISTENCI 'Codigo asistencia corriente
'''''            End If
'''''          End If
'''''        End If
'''''
'''''        vntdatos(3) = rs!CI21CODPERSONA 'cod. persona
'''''      End If
'''''
'''''      rs.Close
'''''      qry.Close
'''''
'''''      vntdatos(1) = 0 'N� de grupo de petici�n: no hace falta
'''''      vntdatos(2) = 0 'N� de petici�n: no hace falta
'''''      vntdatos(4) = 4 'Aplicaci�n que hace la llamda (3 - camas; 4 - recibir paciente)
'''''      vntdatos(7) = lngNumActPlan 'n� act. planif.
'''''      vntdatos(8) = intDptoSel 'dpto. solicitante
'''''
'''''      'Mensaje de comprobaci�n (luego quitar)
'''''      'If MsgBox("Proceso    = " & vntdatos(5) & vbCrLf & _
'''''      '          "Asistencia = " & vntdatos(6) & vbCrLf & _
'''''      '          "Seguir ?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
'''''
'''''      'Launch the process
'''''      Call objsecurity.LaunchProcess("PR0152", vntdatos)
'''''  End Select
'''''End Sub

Private Sub pVerRecursosConsumidos(lngNumActPlan&)
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    Call objPipe.PipeSet("AD_CodDpto", sscboDpto.Columns(0).Value)
    frmRecConsumidos.Show vbModal
    Set frmRecConsumidos = Nothing
 End Sub

Private Sub pCaption()
Dim i%, msg$

'Se muestra en el Caption del formulario los recursos seleccionados
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected = True Then
            msg = msg & lvwRec.ListItems(i).Text & ", "
            If i = 1 Then Exit For
        End If
    Next i
    If msg <> "" Then
        msg = Left$(msg, Len(msg) - 2)
        msg = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: " & msg
    Else
        msg = strMeCaption & ". Dpto: " & sscboDpto.Text
    End If
    Me.Caption = msg
End Sub

Private Sub pImprimirPeticion(ssGrid As SSDBGrid)
    Dim i%, sql$
    
    If ssGrid.SelBookmarks.Count = 1 Then
      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
    Else
      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
    End If
    With crtCrystalReport1
        .PrinterCopies = 1
        .Destination = crptToPrinter
        For i = 0 To ssGrid.SelBookmarks.Count - 1
            sql = sql & "{PR0457J.PR04NUMACTPLAN}= " & ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(i)) & " OR "
        Next i
        sql = Left$(sql, Len(sql) - 4)
        .SelectionFormula = "(" & sql & ")"
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Screen.MousePointer = vbHourglass
        .Action = 1
        Screen.MousePointer = vbDefault
    End With

'Dim vntdata(1) As Variant
'
'  For i = 0 To ssgrid.SelBookmarks.Count - 1
'    SQL = SQL & "PR0400.PR04NUMACTPLAN= " & ssgrid.Columns("Num. Act.").CellText(ssgrid.SelBookmarks(i)) & " OR "
'  Next i
'  SQL = Left$(SQL, Len(SQL) - 4)
'  vntdata(1) = SQL
'  Call objsecurity.LaunchProcess("PR2400", vntdata(1))

End Sub

Private Sub pFarmacia(strHistoria$)
'****************************************************************************************
'*  Accede a farmacia
'****************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim vntdata(1 To 1)
    
    'se busca el c�digo de persona necesario para pasarlo como par�metro al Launcher
    sql = "SELECT CI21CODPERSONA" _
      & " FROM CI2200" _
      & " WHERE CI22NUMHISTORIA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strHistoria
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then vntdata(1) = rs(0)
    rs.Close
    qry.Close
    sql = "select AD30CODCATEGORIA FROM SG0200 WHERE SG02COD = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = objsecurity.strUser
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        Select Case rs!AD30CODCATEGORIA
            Case 1, 2, 3, 5, 7, 8, 18, 19, 21
              Call objsecurity.LaunchProcess("FR0115", vntdata)
            Case 6, 15
              Call objsecurity.LaunchProcess("FR0171", vntdata)
        End Select
    End If
   
End Sub
Private Sub pImprimirPrograma(strCodPersona)
Dim strWhere$
    strWhere = "{PR0460J.CI21CODPERSONA}= " & strCodPersona
    crtCrystalReport1.ReportFileName = objApp.strReportsPath & "PROPAC1.rpt"
        With crtCrystalReport1
        .PrinterCopies = 1
        .Destination = crptToWindow
        .SelectionFormula = "(" & strWhere & ")"
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Screen.MousePointer = vbHourglass
        .Action = 1
        Screen.MousePointer = vbDefault
    End With
End Sub

Private Sub pDejarAviso(strCodPersona)
    Dim vntdata(1)
    vntdata(1) = strCodPersona
    Call objsecurity.LaunchProcess("PR0580", vntdata)
End Sub

Private Sub pVisionGlobal(strCodPersona As String)
' Visi�n global de Alberto
  On Error Resume Next
  ReDim arData(1 To 2) As Variant
  arData(1) = strCodPersona
  arData(2) = constMOSTRARRESP
  
  Call objsecurity.LaunchProcess("HC02", arData())
    
'  If Err <> 0 Then
'    Dim vntdata(1)
'    vntdata(1) = strCodPersona
'    Call objsecurity.LaunchProcess("AD1126", vntdata)
'  End If
End Sub

Private Function fAsociarAsist() As Boolean
    frmPregunta.Show vbModal
    Set frmPregunta = Nothing
    fAsociarAsist = objPipe.PipeGet("PR_AsocAsist")
    objPipe.PipeRemove ("PR_AsocAsist")
End Function

Private Sub pCargarActPendientes(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones pendientes hasta hoy inclusive en funci�n del Dpto y rescurso
'*  o recursos seleccionados ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strConsent$, strFecha$
    Static sqlOrder$
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY FECHA, PACIENTE" 'Opci�n por defecto: Fecha Prog.
    Else
        Select Case UCase(ssgrdPendientes.Columns(intCol).Caption)
        Case UCase("Fecha Prog."): sqlOrder = " ORDER BY FECHA, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, FECHA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, FECHA"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, FECHA"
        Case UCase("Entrada cola"): sqlOrder = " ORDER BY PR04FECENTRCOLA, PACIENTE"
        Case Else: Exit Sub
        End Select
    End If
    
    ssgrdPendientes.RemoveAll
    ssgrdPendientes.ScrollBars = ssScrollBarsNone
    
    'ACTUACIONES PENDIENTES (citadas o planificadas con entrada en cola)
    sql = "SELECT /*+ ORDERED INDEX(PR0400 PR0403) INDEX(CI0100 CI0104) */"
    sql = sql & " CI0100.CI01FECCONCERT FECHA,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    'SQL = SQL & " PR0400.CI21CODPERSONA,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " PR0400.PR04FECENTRCOLA,"
    sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
    sql = sql & " PR0400.CI21CODPERSONA, GCFN06(AD1500.AD15CODCAMA) CAMA"
    sql = sql & " FROM PR0400, CI0100, PR0100, PR0300, CI2200, AD1500"
    sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NOT NULL"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND AD1500.AD02CODDPTO(+) <> " & constDPTO_QUIROFANO
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR03INDCITABLE = -1" 'citable
    sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    sql = sql & " AND CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_HOSPITALIZACION & "'"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_INTERVENCION & "'"
    If strRecSel <> "0" Then
        sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    sql = sql & " UNION"
    sql = sql & " SELECT /*+ ORDERED INDEX(PR0400 PR0403) */"
    sql = sql & " PR0400.PR04FECPLANIFIC FECHA,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    'SQL = SQL & " PR0400.CI21CODPERSONA,"
    sql = sql & " CI2200.CI22NUMHISTORIA,"
    sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    sql = sql & " PR0400.PR04FECENTRCOLA,"
    sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
    sql = sql & " PR0400.CI21CODPERSONA,GCFN06(AD1500.AD15CODCAMA)CAMA"
    If strRecSel <> "0" Then
        sql = sql & " FROM PR0400, PR0100, PR0300, CI2200, PR1400, AD1500"
    Else
        sql = sql & " FROM PR0400, PR0100, PR0300, CI2200, AD1500 "
    End If
    sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.PR04FECENTRCOLA IS NOT NULL"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_PLANIFICADA
    sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
    sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
    sql = sql & " AND AD1500.AD02CODDPTO(+) <> " & constDPTO_QUIROFANO
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR03INDCITABLE = 0" 'no citable
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_HOSPITALIZACION & "'"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_INTERVENCION & "'"
    If strRecSel <> "0" Then
        sql = sql & " AND PR1400.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR1400.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    qry(1) = qry(0)
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        ssgrdPendientes.AddItem Format(rs!fecha, "dd/mm/yy hh:mm") & Chr$(9) _
            & rs!PR01DESCORTA & Chr$(9) _
            & rs!CI22NUMHISTORIA & Chr$(9) & rs!PACIENTE & Chr$(9) _
            & Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
            & strConsent & Chr$(9) & rs!PR04NUMACTPLAN & Chr$(9) _
            & rs!PR56CODESTMUES & Chr$(9) _
            & rs!CI21CODPERSONA & Chr$(9) _
            & rs!CAMA
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdPendientes.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActRealizandose(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones que se est�n realizando hasta hoy inclusive en funci�n del
'*  Dpto y rescurso o recursos seleccionados ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strConsent$, strFecha$
    Static sqlOrder$
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PR04FECINIACT, PACIENTE" 'Opci�n por defecto: Fecha Inicio
    Else
        Select Case UCase(ssgrdRealizandose.Columns(intCol).Caption)
        Case UCase("Fecha Inicio"): sqlOrder = " ORDER BY PR04FECINIACT, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, PR04FECINIACT"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, PR04FECINIACT DESC"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, PR04FECINIACT"
        Case UCase("Entrada cola"): sqlOrder = " ORDER BY PR04FECENTRCOLA, PACIENTE"
        Case Else: Exit Sub
        End Select
    End If

    ssgrdRealizandose.RemoveAll
    ssgrdRealizandose.ScrollBars = ssScrollBarsNone

    'ACTUACIONES REALIZ�NDOSE
    If strRecSel = "0" Then 'TODOS LOS RECURSOS: no hace falta distinguir citables/no citables
        sql = "SELECT PR0400.PR04FECINIACT,"
        sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
        'SQL = SQL & " PR0400.CI21CODPERSONA,"
        sql = sql & " CI2200.CI22NUMHISTORIA,"
        sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
        sql = sql & " PR0400.PR04FECENTRCOLA,"
        sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
        sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
        sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
        sql = sql & " PR0400.CI21CODPERSONA,  GCFN06(AD1500.AD15CODCAMA) CAMA"
        sql = sql & " FROM PR0100, PR0300, CI2200, PR0400, AD1500"
        sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
        sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZANDOSE
        sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND AD1500.AD02CODDPTO(+) <> " & constDPTO_QUIROFANO
        sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
        sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_HOSPITALIZACION & "'"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_INTERVENCION & "'"
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = intDptoSel
    Else 'UN RECURSO CONCRETO: hay que distinguir citables/no citables para buscar recurso
        sql = "SELECT PR0400.PR04FECINIACT,"
        sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
        'SQL = SQL & " PR0400.CI21CODPERSONA,"
        sql = sql & " CI2200.CI22NUMHISTORIA,"
        sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
        sql = sql & " PR0400.PR04FECENTRCOLA,"
        sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
        sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
        sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
        sql = sql & " PR0400.CI21CODPERSONA,  GCFN06(AD1500.AD15CODCAMA) CAMA"
        sql = sql & " FROM PR0100, PR0300, CI2200, CI0100, PR0400, AD1500"
        sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
        sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZANDOSE
        sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND AD1500.AD02CODDPTO(+) <> " & constDPTO_QUIROFANO
        sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
        sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR03INDCITABLE = -1" 'citable
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_HOSPITALIZACION & "'"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_INTERVENCION & "'"
        sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
        sql = sql & " AND CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
        sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
        sql = sql & " UNION"
        sql = sql & " SELECT PR0400.PR04FECINIACT,"
        sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
        'SQL = SQL & " PR0400.CI21CODPERSONA,"
        sql = sql & " CI2200.CI22NUMHISTORIA,"
        sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
        sql = sql & " PR0400.PR04FECENTRCOLA,"
        sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
        sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
        sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
        sql = sql & " PR0400.CI21CODPERSONA,  GCFN06(AD1500.AD15CODCAMA) CAMA"
        sql = sql & " FROM PR0100, PR0300, CI2200, PR1400, PR0400, AD1500"
        sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
        sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZANDOSE
        sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
        sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR03INDCITABLE = 0" 'no citable
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_HOSPITALIZACION & "'"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_INTERVENCION & "'"
        sql = sql & " AND PR1400.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR1400.AG11CODRECURSO  IN (" & strRecSel & ")"
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = intDptoSel
        qry(1) = intDptoSel
    End If
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        ssgrdRealizandose.AddItem Format(rs!PR04FECINIACT, "dd/mm/yy hh:mm") & Chr$(9) _
            & rs!PR01DESCORTA & Chr$(9) _
            & rs!CI22NUMHISTORIA & Chr$(9) & rs!PACIENTE & Chr$(9) _
            & Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
            & strConsent & Chr$(9) & rs!PR04NUMACTPLAN & Chr$(9) _
            & rs!PR56CODESTMUES & Chr$(9) _
            & rs!CI21CODPERSONA & Chr$(9) _
            & rs!CAMA
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdRealizandose.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActFinalizadas(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones ya realizadas hoy en funci�n del Dpto y rescurso o recursos
'*  seleccionados ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strConsent$, strFecha$
    Static sqlOrder$
    'Dim fecha As Date
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PR04FECFINACT DESC, PACIENTE" 'Opci�n por defecto: Fecha Fin
    Else
        Select Case UCase(ssgrdFinalizadas.Columns(intCol).Caption)
        Case UCase("Fecha Fin"): sqlOrder = " ORDER BY PR04FECFINACT DESC, PACIENTE"
        Case UCase("Fecha Inicio"): sqlOrder = " ORDER BY PR04FECINIACT, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, PR04FECFINACT DESC"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, PR04FECFINACT DESC"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, PR04FECFINACT DESC"
        Case Else: Exit Sub
        End Select
    End If

    ssgrdFinalizadas.RemoveAll
    ssgrdFinalizadas.ScrollBars = ssScrollBarsNone

    If strRecSel = "0" Then
        sql = "SELECT PR0400.PR04FECINIACT, PR0400.PR04FECFINACT,"
        sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
        'SQL = SQL & " PR0400.CI21CODPERSONA,"
        sql = sql & " CI2200.CI22NUMHISTORIA,"
        sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
        sql = sql & " PR0400.PR04FECENTRCOLA,"
        sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
        sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
        sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
        sql = sql & " PR0400.CI21CODPERSONA, GCFN06(AD1500.AD15CODCAMA) CAMA"
        sql = sql & " FROM PR0100, PR0300, CI2200, PR0400, AD1500"
        sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
        sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZADA
        sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND AD1500.AD02CODDPTO(+) <> " & constDPTO_QUIROFANO
        sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
        sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_HOSPITALIZACION & "'"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_INTERVENCION & "'"
        sql = sql & " AND PR04FECFINACT BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE)+1"
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = intDptoSel
    Else
        sql = "SELECT PR0400.PR04FECINIACT, PR0400.PR04FECFINACT,"
        sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
        'SQL = SQL & " PR0400.CI21CODPERSONA,"
        sql = sql & " CI2200.CI22NUMHISTORIA,"
        sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
        sql = sql & " PR0400.PR04FECENTRCOLA,"
        sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
        sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
        sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
        sql = sql & " PR0400.CI21CODPERSONA, GCFN06(AD1500.AD15CODCAMA) CAMA"
        sql = sql & " FROM PR0100, PR0300, CI2200, CI0100, PR0400, AD1500"
        sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
        sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZADA
        sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND AD1500.AD02CODDPTO(+) <> " & constDPTO_QUIROFANO
        sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
        sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR03INDCITABLE = -1" 'citable
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_HOSPITALIZACION & "'"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_INTERVENCION & "'"
        sql = sql & " AND PR04FECFINACT BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE)+1"
        sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
        sql = sql & " AND CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
        sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
        sql = sql & " UNION"
        sql = sql & " SELECT PR0400.PR04FECINIACT, PR0400.PR04FECFINACT,"
        sql = sql & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
        'SQL = SQL & " PR0400.CI21CODPERSONA,"
        sql = sql & " CI2200.CI22NUMHISTORIA,"
        sql = sql & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
        sql = sql & " PR0400.PR04FECENTRCOLA,"
        sql = sql & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
        sql = sql & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
        sql = sql & " NVL(PR0400.PR56CODESTMUES,3) PR56CODESTMUES,"
        sql = sql & " PR0400.CI21CODPERSONA, GCFN06(AD1500.AD15CODCAMA) CAMA"
        sql = sql & " FROM PR0100, PR0300, CI2200, PR1400, PR0400, AD1500"
        sql = sql & " WHERE PR0400.AD02CODDPTO = ?"
        sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZADA
        sql = sql & " AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
        sql = sql & " AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO(+)"
        sql = sql & " AND AD1500.AD02CODDPTO(+) <> " & constDPTO_QUIROFANO
        sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
        sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR03INDCITABLE = 0" 'no citable
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_HOSPITALIZACION & "'"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD <> '" & constACTIV_INTERVENCION & "'"
        sql = sql & " AND PR04FECFINACT BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE)+1"
        sql = sql & " AND PR1400.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR1400.AG11CODRECURSO IN (" & strRecSel & ")"
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = intDptoSel
        qry(1) = intDptoSel
    End If
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        ssgrdFinalizadas.AddItem Format(rs!PR04FECINIACT, "dd/mm/yy hh:mm") & Chr$(9) _
            & Format(rs!PR04FECFINACT, "dd/mm/yy hh:mm") & Chr$(9) _
            & rs!PR01DESCORTA & Chr$(9) _
            & rs!CI22NUMHISTORIA & Chr$(9) & rs!PACIENTE & Chr$(9) _
            & Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
            & strConsent & Chr$(9) & rs!PR04NUMACTPLAN & Chr$(9) _
            & rs!PR56CODESTMUES & Chr$(9) _
            & rs!CI21CODPERSONA & Chr$(9) _
            & rs!CAMA
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdFinalizadas.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub



Private Function fMismoTipoActividad(strNAPlan$) As Boolean
    Dim sql$, rs As rdoResultset

    sql = "SELECT COUNT(DISTINCT PR12CODACTIVIDAD)"
    sql = sql & " FROM PR0100, PR0400"
    sql = sql & " WHERE PR04NUMACTPLAN IN (" & strNAPlan & ")"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    If rs(0) <= 1 Then fMismoTipoActividad = True
End Function

Private Sub pImprimirEtiq(strCodPersona As String)
Dim vntdata(1)
  
  vntdata(1) = strCodPersona
  Call objsecurity.LaunchProcess("PR4000", vntdata)
End Sub

Private Sub mnuImprimirOpcion_Click(Index As Integer)
Dim grd As SSDBGrid, strCodPersona As String
    
  If ssgrdPendientes.SelBookmarks.Count > 0 Then
    Set grd = ssgrdPendientes
  ElseIf ssgrdRealizandose.SelBookmarks.Count > 0 Then
    Set grd = ssgrdRealizandose
  ElseIf ssgrdFinalizadas.SelBookmarks.Count > 0 Then
    Set grd = ssgrdFinalizadas
  Else
    MsgBox "No se ha seleccionado ninguna actuaci�n.", vbExclamation, Me.Caption
    Exit Sub
  End If
  
  strCodPersona = grd.Columns("Persona").CellText(grd.SelBookmarks(0))
  
  Select Case Index
    Case 10 ' Petici�n
      Call pImprimirPeticion(grd)
    
    Case 20 ' Programa
      Call pImprimirPrograma(strCodPersona)
    
    Case 30 ' Etiquetas
       Call pImprimirEtiq(strCodPersona)
  
  End Select

End Sub
  
Function InsertPR03(nPet As Long, nPers As Long, cDptCargo As Integer, cPrAP As Long) As Long ' Actuaci�n pedida
Dim sql As String
Dim rdoQ As rdoQuery
Dim nPrPed As Long

' Se prepara el insert
  sql = "INSERT INTO PR0300 (PR09NumPeticion, CI21CodPersona, PR03NumActPedi, " _
      & "AD02CodDpto, PR01CodActuacion, PR03IndCitable, PR48CodUrgencia, " _
      & "PR03IndIntCientif, AD02CodDpto_Crg) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPet
  rdoQ(1) = nPers
  nPrPed = fNextClave("PR03NumActPedi")
  rdoQ(2) = nPrPed
  rdoQ(3) = constDPTO_ANATOMIA
  rdoQ(4) = cPrAP
  rdoQ(5) = False
  rdoQ(6) = Null
  If cDptCargo = 0 Then
    rdoQ(7) = False
    rdoQ(8) = False
  Else
    rdoQ(7) = True
    rdoQ(8) = cDptCargo
  End If
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    InsertPR03 = 0
  Else
    InsertPR03 = nPrPed
  End If

End Function

Function InsertPR06(nPrPed As Long, cPrAP As Long) As Integer   ' Fase pedida
Dim sql As String
Dim rdoQ As rdoQuery

  InsertPR06 = True
  sql = "INSERT INTO PR0600 (PR03NumActPedi, PR06NumFase, PR06DesFase, PR06NumMinOcuPac, " _
      & "PR06NumFase_Pre, PR06NumMinFPre, PR06NumMaxFPre, PR06IndHabNatu, PR06IndIniFin) " _
      & "SELECT ?, PR05NumFase, PR05DesFase, PR05NumOcuPaci, PR05NumFase_Pre, " _
      & "PR05NumTMinFPre, PR05NumTMaxFPre, PR05IndHabilNatu, PR05IndInicioFin " _
      & "FROM PR0500 WHERE PR01CodActuacion = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPrPed
  rdoQ(1) = cPrAP
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    InsertPR06 = False
    Exit Function
  End If

End Function

Function InsertPR14(nPrPed As Long, cRecRea As Integer, nPrPed_Endos As Long, PrRealizada As Integer, cPrAP As Long) As Integer ' Tipo de recurso pedido
Dim sql As String
Dim rdoQ As rdoQuery
Dim i As Integer

  InsertPR14 = True
' Se insertan los recursos de la actuaci�n
  sql = "INSERT INTO PR1400 " _
      & "(PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID, PR14NUMUNIREC, AD02CODDPTO, " _
      & "AG14CODTIPRECU, PR14INDRECPREFE, PR14NUMMINOCU, PR14NUMMINDESREC, PR14INDPLANIF) " _
      & "SELECT ?, PR05NUMFASE, PR13NUMNECESID, PR13NUMUNIREC, AD02CODDPTO, " _
      & "AG14CODTIPRECU, PR13INDPREFEREN, PR13NUMTIEMPREC, PR13NUMMINDESF, PR13INDPLANIF " _
      & "FROM PR1300 WHERE PR01CODACTUACION = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPrPed
  rdoQ(1) = cPrAP
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    InsertPR14 = False
    Exit Function
  End If

' Si se ha pedido alg�n recurso se modifica el registro correspondiente (de la endoscopia)
' De momento se modifica el recurso pedido, ya que el realizado es un follon.
  If cRecRea <> 0 Then
'    If PrRealizada = False Then ' Se inserta en el recurso pedido
      sql = "UPDATE PR1400 SET AG11CodRecurso = ? WHERE " _
          & "PR03NumActPedi = ? AND PR06NumFase = ? AND AG14CodTipRecu = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cRecRea
      rdoQ(1) = nPrPed_Endos
      rdoQ(2) = 1 ' De momento se mete en la fase 1
      rdoQ(3) = constTIRECDRENDOSCOPIAS
      rdoQ.Execute
      If rdoQ.RowsAffected = 0 Then
        InsertPR14 = False
        Exit Function
      End If
'    Else ' Se modifica el recurso consumido
'      sql = "UPDATE PR1400 SET AG11CodRecurso = ? WHERE " _
'          & "PR03NumActPedi = ? AND PR06NumFase = ? AND AG14CodTipRecu = ?"
'      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
'      rdoQ(0) = cRecRea
'      rdoQ(1) = nPrPed_Endos
'      rdoQ(2) = 1 ' De momento se mete en la fase 1
'      rdoQ(3) = constTIRECDRENDOSCOPIAS
'      rdoQ.Execute
'      If rdoQ.RowsAffected = 0 Then
'        InsertPR14 = False
'        Exit Function
'      End If
  End If
End Function

Function InsertPR08(nPet As Long, nPrPed As Long, nAsist As Long, nProc As Long, Motivo As String, Muestras As String, Diag As String) As Integer ' Observaciones prueba pedida - petici�n.
Dim sql As String, condicion As String
Dim rdoQ As rdoQuery
Dim i As Integer, nSec As Long

  InsertPR08 = True
  sql = "INSERT INTO PR0800 (PR09NumPeticion, PR03NumActPedi, PR08NumSecuencia, " _
      & "AD07CodProceso, AD01CodAsistenci, PR08DesIndicac, PR08DesObserv, PR08DesMotPet) " _
      & "VALUES (?, ?, ?, ?, ?, '" & Muestras & "','" & Diag & "', '" & Motivo & "')"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPet
  rdoQ(1) = nPrPed
  condicion = "PR09NumPeticion = " & nPet & " AND PR03NumActPedi = " & nPrPed
  nSec = fNextClaveMax_Condicion("PR08NumSecuencia", "PR0800", condicion)
  rdoQ(2) = nSec
  rdoQ(3) = nProc
  rdoQ(4) = nAsist
'  rdoQ(5) = Null ' Indicaciones: Aqu� voy a guardar las muestras
'  rdoQ(5) = Null ' Observaciones
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    InsertPR08 = False
    Exit Function
  End If

End Function

Function InsertPR04(nPrPed As Long, nAsist As Long, nProc As Long, nPers As Long, cDptCargo As Integer, cPrAP As Long) As Integer ' Actuaci�n Planificada
Dim sql As String
Dim rdoQ As rdoQuery
Dim i As Integer, nPrPlan As Long

  InsertPR04 = True
  sql = "INSERT INTO PR0400 (PR04NumActPlan, AD01CodAsistenci, PR01CodActuacion, " _
      & "AD02CodDpto, CI21CodPersona, PR37CodEstado, PR03NumActPedi, " _
      & "PR04IndIntCientif, AD02CodDpto_Crg, CI32CodTipEcon, CI13CodEntidad, " _
      & "AD07CodProceso, PR04IndReqDoc, " _
      & "PR56CodEstMues) " _
      & "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  nPrPlan = fNextClave("PR04NumActPlan")
  rdoQ(0) = nPrPlan
  rdoQ(1) = nAsist
  rdoQ(2) = cPrAP
  rdoQ(3) = constDPTO_ANATOMIA
  rdoQ(4) = nPers
  rdoQ(5) = constESTACT_PLANIFICADA
  rdoQ(6) = nPrPed
  If cDptCargo = 0 Then
    rdoQ(7) = Null
    rdoQ(8) = Null
    rdoQ(9) = Null
    rdoQ(10) = Null
  Else
    rdoQ(7) = True
    rdoQ(8) = cDptCargo
    rdoQ(9) = constTIECOINVESTIGACION ' El tipo econ�mico y entidad es el de Investigaci�n
    rdoQ(10) = constENTIDADINVESTIGACION
  End If
  rdoQ(11) = nProc
  rdoQ(12) = -1
  rdoQ(12) = 0
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    InsertPR04 = False
    Exit Function
  End If
End Function

Function InsertPR44(cDptPeti As Integer, cPrAP As Long) As Integer ' Veces que un dpt pide una actuaci�n.
Dim sql As String
Dim rdoQUpdate As rdoQuery, rdoQInsert As rdoQuery

  InsertPR44 = True
  On Error Resume Next
' Se prepara el query del update
  sql = "UPDATE PR4400 SET PR44NumCuenta = PR44NumCuenta + 1 WHERE " _
      & "AD02CodDpto = ? AND PR01CodActuacion = ? AND AD02CodDpto_Rea = ?"
  Set rdoQUpdate = objApp.rdoConnect.CreateQuery("", sql)
  rdoQUpdate(0) = cDptPeti
  
' Si es la primera vez que se pide la prueba se prepara el query del Insert
  sql = "INSERT INTO PR4400 (PR44NumCuenta, AD02CodDpto, PR01CodActuacion, AD02CodDpto_Rea) " _
      & "VALUES (?, ?, ?, ?)"
  Set rdoQInsert = objApp.rdoConnect.CreateQuery("", sql)
  rdoQInsert(0) = 1
  rdoQInsert(1) = cDptPeti
      
' Se intenta primero el UPDATE
  rdoQUpdate(1) = cPrAP
  rdoQUpdate(2) = constDPTO_ANATOMIA
  rdoQUpdate.Execute
  
  If rdoQUpdate.RowsAffected = 0 Then
    rdoQInsert(2) = cPrAP
    rdoQInsert(3) = constDPTO_ANATOMIA
    rdoQInsert.Execute
  End If

End Function



Public Function fNextClaveMax_Condicion(campo$, tabla$, strWhere$) As String
'Devuelve el siguiente valor de la clave principal seleccion�ndo el m�ximo del campo
    Dim sql$, rsMaxClave As rdoResultset
    
    sql = "SELECT MAX(" & campo & ") FROM " & tabla

    If strWhere <> "" Then
        sql = sql & " WHERE " & strWhere
    End If
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(sql)
    If Not IsNull(rsMaxClave(0)) Then
        fNextClaveMax_Condicion = rsMaxClave(0) + 1
    Else
        fNextClaveMax_Condicion = 1
    End If
    rsMaxClave.Close
    
End Function

Sub ImprimirPetBiopsia(nPrPlan As Long, cRec As Integer, Resumen As String, Muestras As String, Diag As String, Prueba As String)
Dim Mitad As Long, i As Integer
Dim sql As String, fmt As String, DrRea As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
  
  DrRea = ""
  If cRec <> 0 Then
    sql = "SELECT NVL(SG0200.SG02TxtFirma, SG0200.SG02Ape1||' '||SG0200.SG02Ape2||', '||SG0200.SG02Nom) " _
        & "FROM SG0200, AG1100 WHERE " _
        & "AG1100.SG02Cod = SG0200.SG02Cod AND " _
        & "AG1100.AG11CodRecurso = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = cRec
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo.EOF = False Then DrRea = rdo(0)
  End If
  
  sql = "SELECT AD0200.AD02DesDpto, NVL(SG0200.SG02TxtFirma, SG0200.SG02Ape1||' '||SG0200.SG02Ape2||', '||SG0200.SG02Nom), " _
      & "PR0100.PR01DesCorta, CI2200.CI22NumHistoria, " _
      & "CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE, " _
      & "PR0900.PR09DesObservac, CI2200.CI22FecNacim " _
      & "FROM PR0400, PR0800, PR0900, AD0200, SG0200, PR0100, CI2200 WHERE " _
      & "PR0400.PR03NumActPedi = PR0800.PR03NumActPedi AND " _
      & "PR0800.PR09NumPeticion = PR0900.PR09NumPeticion AND " _
      & "PR0900.AD02CodDpto = AD0200.AD02CodDpto AND " _
      & "PR0900.SG02Cod = SG0200.SG02Cod AND " _
      & "PR0400.CI21CodPersona = CI2200.CI21CodPersona AND " _
      & "PR0400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR0400.PR04NumActPlan = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPrPlan
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "No se encuentra la biopsia.", vbInformation, "Petici�n de biopsia"
    Exit Sub
  End If
  
  With vsPet
    Mitad = 5000
    .Action = 3 ' StartDoc
' Se pone la cabecera (escudo, CUN, fecha-hora)
    .X1 = 200
    .Y1 = 200
'    .X2 = 1000
    .X2 = 800
    .Y2 = 1000
    .Picture = imgEscudo.Picture
    
    .FontName = "Arial"
    .FontSize = 10
    .FontBold = False
    .CurrentX = 1200
    .CurrentY = 600
    .Text = "Cl�nica Universitaria de Navarra"
    
    .CurrentX = 8500
    .CurrentY = 400
    .Text = "Fecha: " & Format$(Now, "DD/MM/YYYY")
    .CurrentX = 8500
    .CurrentY = 700
    .Text = "Hora: " & Format$(Now, "HH:MM")
    
    .CurrentY = 1200
    .FontName = "Arial"
    .FontSize = 14
    .FontBold = True
    .FontUnderline = True
    .TableBorder = tbNone
    .TablePenTB = 20
    .TablePenLR = 20
    .Table = "^_10000;Petici�n de estudio anatomopatol�gico"
    
    .Paragraph = ""
    
    .FontBold = True
    .FontSize = 12
    .FontUnderline = True
    .Text = "Estudio solicitado: "
    .FontBold = False
    .FontUnderline = False
    .Paragraph = Prueba
    
    .Paragraph = ""
    
    .FontSize = 10
    .FontBold = True
    .FontUnderline = True
    .Text = "SOLICITANTE"
    .CurrentX = Mitad
    .Paragraph = "PACIENTE"
    
    .FontUnderline = False
    
    .FontBold = True
    .Text = "Dpto: "
    .FontBold = False
    .Text = rdo(0)
    
    .CurrentX = Mitad
    .FontBold = True
    .Text = "Historia: "
    .FontBold = False
    .Paragraph = CStr(rdo(3))
    
    .FontBold = True
    .Text = "Dr: "
    .FontBold = False
    .Text = rdo(1)
    
    .CurrentX = Mitad
    .FontBold = True
    .Text = "Paciente: "
    .FontBold = False
    .Paragraph = rdo(4)

    .CurrentX = Mitad
    .FontBold = True
    .Text = "F. Nacimiento: "
    .FontBold = False
    .Paragraph = IIf(IsNull(rdo(6)), "", rdo(6))

    .FontBold = True
    .FontUnderline = True
    .Paragraph = "EXTRACCI�N"
    .FontUnderline = False
    
    .FontBold = True
    .Text = "Dpto: "
    .FontBold = False
    .Paragraph = "Endoscopias"
    
    .FontBold = True
    .Text = "Prueba: "
    .FontBold = False
    .Paragraph = rdo(2)
    
    .FontBold = True
    .Text = "Dr: "
    .FontBold = False
    .Paragraph = DrRea
    
    .Paragraph = ""
    .Paragraph = ""
    
    .FontBold = True
    .Text = "Observaciones a la petici�n: "
    .FontBold = False
    .Paragraph = IIf(IsNull(rdo(5)), "", rdo(5))
    .Paragraph = ""
    
    .FontBold = True
    .Text = "Resumen: "
    .FontBold = False
    .Paragraph = Resumen
    .Paragraph = ""
    
    .FontBold = True
    .Text = "Diagn�stico endosc�pico: "
    .FontBold = False
    .Paragraph = Diag
    .Paragraph = ""
    
    .FontBold = True
    .Paragraph = "Muestras: "
    .FontBold = False
    .Paragraph = Muestras
          
' En el pie se pone espacio para la fecha y n�mero de entrada
    For i = 1 To 30
      .Paragraph = ""
    Next i
    .FontBold = True
    .FontSize = 12
    .CurrentX = 6000
    .Paragraph = "Fecha de Entrada: "
    .Paragraph = ""
    .CurrentX = 6000
    .Paragraph = "N� de Entrada: "
    
    .Action = 6 'End Document
    .PrintDoc  ' Se imprime
    .KillDoc
  
  End With
End Sub
Private Sub pDrInforme(ssGrid As SSDBGrid)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strAP As Long
sql = "SELECT PR03NUMACTPEDI, PR12CODACTIVIDAD FROM PR0400, PR0100 "
sql = sql & "WHERE PR04NUMACTPLAN = ? "
sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = ssGrid.Columns("Num. Act.").Value
Set rs = qry.OpenResultset()
    strAP = rs(0)


If rs(1) = constACTIV_INFORME Then
    If ssGrid.Columns("Cama").Text <> "" Then
        Call objPipe.PipeSet("PR03NUMACTPEDI", strAP)
        frmDoctorInforme.Show vbModal
    Else
        sql = "SELECT GCFN06(AD1600.AD15CODCAMA) FROM PR0400,AD1600 WHERE "
        sql = sql & " PR04NUMACTPLAN = ? "
        sql = sql & " AND PR0400.AD01CODASISTENCI = AD1600.AD01CODASISTENCI"
        sql = sql & " AND PR0400.AD07CODPROCESO = AD1600.AD07CODPROCESO"
        sql = sql & " AND AD1600.AD14CODESTCAMA = " & constCAMAOCUPADA
        sql = sql & " ORDER BY AD16FECCAMBIO DESC "
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = ssGrid.Columns("Num. Act.").Value
        Set rs = qry.OpenResultset()
        If Not rs.EOF Then
            MsgBox "Este paciente estuvo ingresado en la cama: " & rs(0), vbOKOnly, Me.Caption
            Call objPipe.PipeSet("PR03NUMACTPEDI", strAP)
            frmDoctorInforme.Show vbModal
        Else
            MsgBox "El informe no es de un paciente ingresado", vbExclamation, Me.Caption
        End If
    End If
Else
    MsgBox "Seleccione un Informe de Planta", vbExclamation, Me.Caption
End If
           
End Sub
