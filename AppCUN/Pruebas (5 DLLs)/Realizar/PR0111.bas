Attribute VB_Name = "PED002"

Public primerallamada As Boolean

Public Sub Seleccionar_Dpto()
    Dim sqlstr As String
    Dim rsta As rdoResultset
    
    sqlstr = "SELECT * FROM AD0300 WHERE SG02COD ='" & objsecurity.strUser & "'"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta.EOF = False Then
        glngdptologin = rsta("AD02CODDPTO").Value
        rsta.MoveNext
        If rsta.EOF = False Then
            'Load frmEscogerDpto
            'frmEscogerDpto.Show (vbModal)
            'Unload frmEscogerDpto
            'Set frmEscogerDpto = Nothing
            Call objsecurity.LaunchProcess("PR0194")
        End If
    End If
    rsta.Close
    Set rsta = Nothing
End Sub

Public Sub pAnularAsociadas(lngActPlan As Long)
Dim SQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim lngActPedi As Long

SQL = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ? "
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPlan
Set rs = qry.OpenResultset()
lngActPedi = rs(0)
rs.Close
qry.Close

SQL = "UPDATE PR0400 SET PR37CODESTADO = 6 WHERE PR04NUMACTPLAN IN ("
SQL = SQL & "SELECT PR04NUMACTPLAN FROM PR6100, PR0400"
SQL = SQL & " WHERE PR6100.PR03NUMACTPEDI_ASO = ? "
SQL = SQL & " AND PR6100.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
SQL = SQL & " AND PR0400.PR37CODESTADO IN (1,2) )"

Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'    QRY(0) = Format(fecPlan, "DD/MM/YYYY HH:MM:SS")
    qry(0) = lngActPedi
qry.Execute
qry.Close

SQL = "UPDATE CI0100 SET CI01SITCITA = '2' WHERE CI01SITCITA = '1'"
SQL = SQL & "AND PR04NUMACTPLAN IN ("
SQL = SQL & "SELECT PR04NUMACTPLAN FROM PR6100, PR0400"
SQL = SQL & " WHERE PR6100.PR03NUMACTPEDI_ASO = ? "
SQL = SQL & " AND PR6100.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
SQL = SQL & " AND PR0400.PR37CODESTADO IN (1,2) )"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
'    QRY(0) = Format(fecPlan, "DD/MM/YYYY HH:MM:SS")
    qry(0) = lngActPedi
qry.Execute
qry.Close
End Sub

Public Function fNextClave(strCampo$) As String
    Dim rs As rdoResultset, SQL$
    SQL = "SELECT " & strCampo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    fNextClave = rs(0)
    rs.Close
End Function

Public Function fActFacturada(strNumActPlan$) As Boolean
'****************************************************************************************
'*  Comprueba si una actuaci�n est� ya facturada
'*  El par�metro strNumActPlan puede corresponder a una �nica actuaci�n o a varias (separadas
'*  por comas) por si se quiere comprobar si alguna de las actuaciones est� facturada
'****************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim blnVarias As Boolean
    
    If InStr(strNumActPlan, ",") > 0 Then blnVarias = True
    
    SQL = "SELECT PR04NUMACTPLAN"
    SQL = SQL & " FROM FA0302J"
    If blnVarias Then
        SQL = SQL & " WHERE PR04NUMACTPLAN IN (" & strNumActPlan & ")"
    Else
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    End If
    SQL = SQL & " AND FA04NUMFACT IS NOT NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    If Not blnVarias Then qry(0) = strNumActPlan
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then fActFacturada = True
    rs.Close
    qry.Close
End Function

Public Function fAsociarAsist() As Boolean
    frmPregunta.Show vbModal
    Set frmPregunta = Nothing
    DoEvents
    fAsociarAsist = objPipe.PipeGet("PR_AsocAsist")
    objPipe.PipeRemove ("PR_AsocAsist")
End Function

Public Sub pInsertarActAsocidas(lngNAPlan&)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, qryD As rdoQuery, rsD As rdoResultset
    Dim qryPR03 As rdoQuery, qryPR04 As rdoQuery, qryPR06 As rdoQuery
    Dim qryPR08 As rdoQuery, qryPR14 As rdoQuery, qryPR61 As rdoQuery
    Dim lngNAPedi&, lngCodAct&, intReqDoc%, intConsFdo%
    Dim lngNAPlanNew&, lngNAPediNew&
    
    'se buscan las actuaciones asociadas a la actuaci�n pedida
    SQL = "SELECT PR01CODACTUACION_ASO, AD02CODDPTO"
    SQL = SQL & " FROM PR3100"
    SQL = SQL & " WHERE PR01CODACTUACION = "
    SQL = SQL & " (SELECT PR01CODACTUACION"
    SQL = SQL & " FROM PR0400"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNAPlan
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        SQL = "INSERT INTO PR0300 (PR09NumPeticion, CI21CodPersona, CI32CodTipEcon,"
        SQL = SQL & " CI13CodEntidad, PR03NumActPedi, AD02CodDpto, PR01CodActuacion,"
        SQL = SQL & " PR03IndConsFirm, PR03NumAcPedi_Pri, PR03IndCitable, PR48CodUrgencia,"
        SQL = SQL & " PR03IndIntCientif, AD02CodDpto_Crg)"
        SQL = SQL & " SELECT PR09NumPeticion, CI21CodPersona, CI32CodTipEcon,"
        SQL = SQL & " CI13CodEntidad, ?, ?, ?,"
        SQL = SQL & " ?, ?, 0, PR48CodUrgencia,"
        SQL = SQL & " PR03IndIntCientif, AD02CodDpto_Crg"
        SQL = SQL & " FROM PR0300"
        SQL = SQL & " WHERE PR03NUMACTPEDI = ?"
        Set qryPR03 = objApp.rdoConnect.CreateQuery("", SQL)
  
        SQL = "INSERT INTO PR0800 (PR09NumPeticion, PR03NumActPedi, PR08NumSecuencia,"
        SQL = SQL & " AD07CodProceso, AD01CodAsistenci,"
        SQL = SQL & " PR08DesIndicac, PR08DesObserv, PR08DesMotPet)"
        SQL = SQL & " SELECT PR09NumPeticion, ?, PR08NumSecuencia,"
        SQL = SQL & " AD07CodProceso, AD01CodAsistenci,"
        SQL = SQL & " PR08DesIndicac, PR08DesObserv, PR08DesMotPet"
        SQL = SQL & " FROM PR0800"
        SQL = SQL & " WHERE PR03NUMACTPEDI = ?"
        Set qryPR08 = objApp.rdoConnect.CreateQuery("", SQL)

        SQL = "INSERT INTO PR0400 (PR04NumActPlan, AD01CodAsistenci, PR01CodActuacion,"
        SQL = SQL & " AD02CodDpto, CI21CodPersona, CI32CodTipEcon, CI13CodEntidad, PR37CodEstado,"
        SQL = SQL & " PR03NumActPedi, PR04FecPlanific, PR04FecEntrCola, PR48CodUrgencia,"
        SQL = SQL & " PR04IndIntCientif, AD02CodDpto_Crg, AD07CodProceso, PR04IndReqDoc)"
        SQL = SQL & " SELECT ?, AD01CodAsistenci, ?,"
        SQL = SQL & " ?, CI21CodPersona, CI32CodTipEcon, CI13CodEntidad, " & constESTACT_PLANIFICADA & ","
        SQL = SQL & " ?, PR04FecPlanific, PR04FecEntrCola, PR48CodUrgencia,"
        SQL = SQL & " PR04IndIntCientif, AD02CodDpto_Crg, AD07CodProceso, ?"
        SQL = SQL & " FROM PR0400"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qryPR04 = objApp.rdoConnect.CreateQuery("", SQL)
    
        SQL = "INSERT INTO PR0600 (PR03NumActPedi, PR06NumFase, PR06DesFase, PR06NumMinOcuPac,"
        SQL = SQL & " PR06NumFase_Pre, PR06NumMinFPre, PR06NumMaxFPre, PR06IndHabNatu, PR06IndIniFin)"
        SQL = SQL & " SELECT ?, PR05NumFase, PR05DesFase, PR05NumOcuPaci, PR05NumFase_Pre,"
        SQL = SQL & " PR05NumTMinFPre, PR05NumTMaxFPre, PR05IndHabilNatu, PR05IndInicioFin"
        SQL = SQL & " FROM PR0500"
        SQL = SQL & " WHERE PR01CodActuacion = ?"
        Set qryPR06 = objApp.rdoConnect.CreateQuery("", SQL)

        SQL = "INSERT INTO PR1400 (PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID,"
        SQL = SQL & " PR14NUMUNIREC, AD02CODDPTO, AG14CODTIPRECU, PR14INDRECPREFE,"
        SQL = SQL & " PR14NUMMINOCU, PR14NUMMINDESREC, PR14INDPLANIF)"
        SQL = SQL & " SELECT ?, PR05NUMFASE, PR13NUMNECESID,"
        SQL = SQL & " PR13NUMUNIREC, AD02CODDPTO, AG14CODTIPRECU, PR13INDPREFEREN,"
        SQL = SQL & " PR13NUMTIEMPREC, PR13NUMMINDESF, PR13INDPLANIF"
        SQL = SQL & " FROM PR1300"
        SQL = SQL & " WHERE PR01CODACTUACION = ?"
        Set qryPR14 = objApp.rdoConnect.CreateQuery("", SQL)
        
        SQL = "INSERT INTO PR6100 (PR03NUMACTPEDI_ASO, PR03NUMACTPEDI) VALUES (?,?)"
        Set qryPR61 = objApp.rdoConnect.CreateQuery("", SQL)
    
        'se buscan los datos necesarios
        SQL = "SELECT PR03NUMACTPEDI, PR0400.PR01CODACTUACION,"
        SQL = SQL & " nvl(PR01INDREQDOC,0) PR01INDREQDOC, nvl(PR01INDCONSFDO,0) PR01INDCONSFDO"
        SQL = SQL & " FROM PR0400, PR0100"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        Set qryD = objApp.rdoConnect.CreateQuery("", SQL)
        qryD(0) = lngNAPlan
        Set rsD = qryD.OpenResultset()
        lngNAPedi = rsD!PR03NUMACTPEDI
        lngCodAct = rsD!PR01CODACTUACION
        intReqDoc = rsD!PR01INDREQDOC
        intConsFdo = rsD!PR01INDCONSFDO
        rsD.Close
        qryD.Close
        Do While Not rs.EOF
            lngNAPediNew = fNextClave("PR03NUMACTPEDI")
            lngNAPlanNew = fNextClave("PR04NUMACTPLAN")

            'pr0300
            qryPR03(0) = lngNAPediNew
            qryPR03(1) = rs!AD02CODDPTO
            qryPR03(2) = rs!PR01CODACTUACION_ASO
            qryPR03(3) = intConsFdo
            qryPR03(4) = lngCodAct
            qryPR03(5) = lngNAPedi
            qryPR03.Execute

            'pr0800
            qryPR08(0) = lngNAPediNew
            qryPR08(1) = lngNAPedi
            qryPR08.Execute

            'pr0400
            qryPR04(0) = lngNAPlanNew
            qryPR04(1) = rs!PR01CODACTUACION_ASO
            qryPR04(2) = rs!AD02CODDPTO
            qryPR04(3) = lngNAPediNew
            qryPR04(4) = intReqDoc
            qryPR04(5) = lngNAPlan
            qryPR04.Execute

            'pr0600
            qryPR06(0) = lngNAPediNew
            qryPR06(1) = rs!PR01CODACTUACION_ASO
            qryPR06.Execute

            'pr1400
            qryPR14(0) = lngNAPediNew
            qryPR14(1) = rs!PR01CODACTUACION_ASO
            qryPR14.Execute

            'pr6100
            qryPR61(0) = lngNAPedi
            qryPR61(1) = lngNAPediNew
            qryPR61.Execute
        
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
End Sub
Public Function fFacturada(grdGrid As SSDBGrid) As Boolean
    Dim strNAPlan$, i%, msg$
    
    For i = 0 To grdGrid.SelBookmarks.Count - 1
        strNAPlan = strNAPlan & "," & grdGrid.Columns("Num. Act.").CellText(grdGrid.SelBookmarks(i))
    Next i
    strNAPlan = Mid$(strNAPlan, 2)
    
    'se comprueba si alguna de las actuaciones est� ya facturada
    If fActFacturada(strNAPlan) Then
        msg = "Alguna de las actuaciones seleccionadas est� facturada."
        msg = msg & " No se puede realizar la operaci�n."
        msg = msg & Chr$(13) & Chr$(13)
        msg = msg & "Consulte con Administraci�n."
        MsgBox msg, vbExclamation, strMeCaption
        fFacturada = True
    End If
End Function

Public Sub pResponsableUrg(lngNumActPlan As Long)
    Dim SQL$, qry1 As rdoQuery, rs1 As rdoResultset
    Dim qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT AD0500.SG02COD DR_ACTUAL,"
    SQL = SQL & " AG1100.SG02COD DR_NUEVO, AG1100.AD02CODDPTO,"
    SQL = SQL & " NVL(SG02TXTFIRMA,SG02APE1||' '||SG02APE2||', '||SG02NOM) DESDR_NUEVO,"
    SQL = SQL & " PR0400.AD01CODASISTENCI, PR0400.AD07CODPROCESO"
    SQL = SQL & " FROM SG0200, AD0500, PR0400, AG1100, PR1000"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND AD0500.AD01CODASISTENCI = PR0400.AD01CODASISTENCI"
    SQL = SQL & " AND AD0500.AD07CODPROCESO = PR0400.AD07CODPROCESO"
    SQL = SQL & " AND AD0500.AD05FECFINRESPON IS NULL"
    SQL = SQL & " AND PR1000.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    SQL = SQL & " AND AG1100.AG11CODRECURSO = PR1000.AG11CODRECURSO"
    SQL = SQL & " AND SG0200.SG02COD = AG1100.SG02COD"
    Set qry1 = objApp.rdoConnect.CreateQuery("", SQL)
    qry1(0) = lngNumActPlan
    Set rs1 = qry1.OpenResultset()
    On Error GoTo Canceltrans
    objApp.BeginTrans
    If rs1!DR_ACTUAL <> rs1!DR_NUEVO Then
        If rs1!DR_ACTUAL <> constURGENCIAS_DR_PASTRANA Then ' Responsable distinto que Pastrana
            SQL = "UPDATE AD0500 SET SG02COD = ?, AD02CODDPTO = ?"
            SQL = SQL & " WHERE AD01CODASISTENCI = ?"
            SQL = SQL & " AND AD07CODPROCESO = ?"
            SQL = SQL & " AND AD05FECFINRESPON IS NULL"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = rs1!DR_NUEVO
            qry(1) = constDPTO_URGENCIAS
            'qry(1) = rs1!AD02CODDPTO
            qry(2) = rs1!AD01CODASISTENCI
            qry(3) = rs1!AD07CODPROCESO
            qry.Execute
            qry.Close
            
            SQL = "SELECT COUNT(*) FROM AD0400 WHERE AD07CODPROCESO = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = rs1!AD07CODPROCESO
            Set rs = qry.OpenResultset()
            If rs(0) = 1 Then 'rs(0) Existe uno
                SQL = "UPDATE AD0400 SET SG02COD = ?, AD02CODDPTO = ?"
                SQL = SQL & " WHERE AD07CODPROCESO = ?"
                SQL = SQL & " AND AD04FECFINRESPON IS NULL"
                Set qry = objApp.rdoConnect.CreateQuery("", SQL)
                qry(0) = rs1!DR_NUEVO
                qry(1) = constDPTO_URGENCIAS
                'qry(1) = rs1!AD02CODDPTO
                qry(2) = rs1!AD07CODPROCESO
                qry.Execute
            End If
            rs.Close
            qry.Close
        Else 'Responsable es Pastrana
            SQL = "UPDATE AD0500 SET AD05FECFINRESPON = SYSDATE"
            SQL = SQL & " WHERE AD01CODASISTENCI = ?"
            SQL = SQL & " AND AD07CODPROCESO = ?"
            SQL = SQL & " AND AD05FECFINRESPON IS NULL"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = rs1!AD01CODASISTENCI
            qry(1) = rs1!AD07CODPROCESO
            qry.Execute
            qry.Close
            
            SQL = "INSERT INTO AD0500 (AD07CODPROCESO, AD01CODASISTENCI,AD05FECINIRESPON,"
            SQL = SQL & " AD02CODDPTO,SG02COD) VALUES (?, ?, SYSDATE, ?, ?)"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = rs1!AD07CODPROCESO
            qry(1) = rs1!AD01CODASISTENCI
            qry(2) = constDPTO_URGENCIAS
            'qry(2) = rs1!AD02CODDPTO
            qry(3) = rs1!DR_NUEVO
            qry.Execute
            qry.Close
            
            SQL = "SELECT COUNT(*) FROM AD0400"
            SQL = SQL & " WHERE AD07CODPROCESO = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = rs1!AD07CODPROCESO
            Set rs = qry.OpenResultset()
            If rs(0) = 1 Then ' existe uno
                SQL = "UPDATE AD0400 SET AD04FECFINRESPON = SYSDATE"
                SQL = SQL & " WHERE AD07CODPROCESO = ?"
                SQL = SQL & " AND AD04FECFINRESPON IS NULL"
                Set qry = objApp.rdoConnect.CreateQuery("", SQL)
                qry(0) = rs1!AD07CODPROCESO
                qry.Execute
                
                SQL = "INSERT INTO AD0400 (AD07CODPROCESO, AD04FECINIRESPON, AD02CODDPTO, SG02COD) "
                SQL = SQL & " VALUES (?, SYSDATE, ?, ?)"
                Set qry = objApp.rdoConnect.CreateQuery("", SQL)
                qry(0) = rs1!AD07CODPROCESO
                qry(1) = constDPTO_URGENCIAS
                'qry(1) = rs1!AD02CODDPTO
                qry(2) = rs1!DR_NUEVO
                qry.Execute
            End If
            rs.Close
            qry.Close
        End If
        objApp.CommitTrans
        SQL = "Se ha realizado un cambio de responsabilidad del proceso." & Chr$(13)
        SQL = SQL & "El nuevo responsable es: " & rs1!DESDR_NUEVO
        MsgBox SQL, vbInformation, "Cambio de responsabilidad"
    End If
    rs1.Close
    qry1.Close
    
    Exit Sub
    
Canceltrans:
    objApp.RollbackTrans
    SQL = "Se ha producido un error al realizar el cambio de responsablidad."
    MsgBox SQL, vbExclamation, "Cambio de responsabilidad"
End Sub

Public Sub pResponsableOnco(lngNumActPlan As Long)
Dim SQL As String
Dim rs1 As rdoResultset
Dim rs2 As rdoResultset
Dim qry1 As rdoQuery
Dim qry2 As rdoQuery
Dim strProceso$
Dim strAsistencia$
Dim intNumSolicitud%
Dim intNumcita%
Dim lngNumSolicit As Long

'EFS: 12/06/2000 Cambiamos la cita y el responsable del proceso asistencia en funcion de
'el recurso consumido para las revisiones y los nuevos de oncologia.
SQL = "SELECT AD01CODASISTENCI, AD07CODPROCESO FROM PR0400 WHERE PR04NUMACTPLAN = ?"
Set qry1 = objApp.rdoConnect.CreateQuery("", SQL)
    qry1(0) = lngNumActPlan
Set rs1 = qry1.OpenResultset()
strAsistencia = rs1!AD01CODASISTENCI
strProceso = rs1!AD07CODPROCESO
rs1.Close
qry1.Close


SQL = "SELECT SG02COD, PR1000.AG11CODRECURSO FROM PR1000,AG1100 WHERE PR04NUMACTPLAN = ? "
SQL = SQL & " AND PR1000.AG11CODRECURSO = AG1100.AG11CODRECURSO"
SQL = SQL & " AND PR1000.AD31CODPUESTO IN (" & constPUESTO_CONSULTOR & "," & constPUESTO_COLABORADOR & ")"
SQL = SQL & " ORDER BY PR10FECADD"

Set qry1 = objApp.rdoConnect.CreateQuery("", SQL)
    qry1(0) = lngNumActPlan
Set rs1 = qry1.OpenResultset()
If Not rs1.EOF Then
    objApp.BeginTrans
    'CAMBIAMOS LA CITA
        SQL = "SELECT  CI31NUMSOLICIT, CI01NUMCITA FROM CI0100 WHERE PR04NUMACTPLAN = ? "
        SQL = SQL & " AND CI01SITCITA = '1'"
        Set qry2 = objApp.rdoConnect.CreateQuery("", SQL)
            qry2(0) = lngNumActPlan
        Set rs2 = qry2.OpenResultset()
            intNumcita = rs2!CI01NUMCITA
            lngNumSolicit = rs2!CI31NUMSOLICIT
        rs2.Close
        qry2.Close
        SQL = "UPDATE CI0100 SET AG11CODRECURSO = ? WHERE PR04NUMACTPLAN = ? "
        SQL = SQL & " AND CI01SITCITA = '1'"
        Set qry2 = objApp.rdoConnect.CreateQuery("", SQL)
            qry2(0) = rs1!AG11CODRECURSO
            qry2(1) = lngNumActPlan
        qry2.Execute
        qry2.Close
        SQL = "UPDATE CI2700 SET AG11CODRECURSO = ? WHERE CI31NUMSOLICIT = ? "
        SQL = SQL & " AND CI01NUMCITA = ? "
        Set qry2 = objApp.rdoConnect.CreateQuery("", SQL)
            qry2(0) = rs1!AG11CODRECURSO
            qry2(1) = lngNumSolicit
            qry2(2) = intNumcita
        qry2.Execute
        qry2.Close
     'Cambiamos el responsable del proceso/asistencia
        SQL = "UPDATE AD0500 SET SG02COD = ? WHERE AD01CODASISTENCI = ? AND "
        SQL = SQL & " AD07CODPROCESO = ? "
        Set qry2 = objApp.rdoConnect.CreateQuery("", SQL)
            qry2(0) = rs1!SG02COD
            qry2(1) = strAsistencia
            qry2(2) = strProceso
        qry2.Execute
        qry2.Close
    'Si es el unico proceso actualizamos el responsable
        SQL = "SELECT COUNT(*) FROM AD0800 WHERE AD07CODPROCESO = ? "
        Set qry2 = objApp.rdoConnect.CreateQuery("", SQL)
            qry2(0) = strProceso
        Set rs2 = qry2.OpenResultset()
        If rs2(0) = 1 Then
            rs2.Close
            qry2.Close
            SQL = "UPDATE AD0400 SET SG02COD = ? WHERE AD07CODPROCESO = ? "
            Set qry2 = objApp.rdoConnect.CreateQuery("", SQL)
                qry2(0) = rs1!SG02COD
                qry2(1) = strProceso
            qry2.Execute
            qry2.Close
        Else
            rs2.Close
            qry2.Close
        End If
    'Cambiamos el solicitante
        SQL = "UPDATE PR0900 SET SG02COD = ? WHERE PR09NUMPETICION = (SELECT PR09NUMPETICION"
        SQL = SQL & " FROM PR0300 WHERE PR03NUMACTPEDI = ( SELECT PR03NUMACTPEDI FROM PR0400"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?))"
        Set qry2 = objApp.rdoConnect.CreateQuery("", SQL)
            qry2(0) = rs1!SG02COD
            qry2(1) = lngNumActPlan
        qry2.Execute
        qry2.Close
    objApp.CommitTrans
End If

End Sub

Public Sub pCitarOverbooking(strNAPlan$)
    Dim SQL As String, rs As rdoResultset
    Dim cllNAP As New Collection
    Dim msgDptoRea$, msgNoCitables$
    
    If Right$(strNAPlan, 1) = "," Then strNAPlan = Left$(strNAPlan, Len(strNAPlan) - 1)
    
    'Sacamos los datos necesarios para ver si las actuacione son citables por ese usuario
    SQL = "SELECT PR0400.PR04NUMACTPLAN, PR0100.PR12CODACTIVIDAD,"
    SQL = SQL & " PR0400.AD02CODDPTO, PR0400.PR01CODACTUACION, PR0100.PR01DESCORTA"
    SQL = SQL & " FROM PR0400, PR0100 WHERE"
    SQL = SQL & " PR04NUMACTPLAN IN (" & strNAPlan & ")"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " ORDER BY PR0100.PR01DESCORTA"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    While Not rs.EOF
        Select Case rs!PR12CODACTIVIDAD
        Case constACTIV_PRUEBA, constACTIV_CONSULTA, constACTIV_INFORME, constACTIV_CONTCONSULTA
            If fblnCitarModiAnular(rs!AD02CODDPTO, rs!PR12CODACTIVIDAD, rs!PR01CODACTUACION) Then
                cllNAP.Add CStr(rs!PR04NUMACTPLAN)
            Else 'citables s�lo desde el Dpto realizador
                msgDptoRea = msgDptoRea & "--> " & rs!PR01DESCORTA & Chr$(13)
            End If
        Case constACTIV_HOSPITALIZACION
                If fblnCitarModiAnular(rs!AD02CODDPTO, rs!PR12CODACTIVIDAD, rs!PR01CODACTUACION) Then
                    cllNAP.Add CStr(rs!PR04NUMACTPLAN)
                Else 'citables s�lo desde el Dpto realizador
                    msgNoCitables = msgNoCitables & "--> " & rs!PR01DESCORTA & Chr$(13)
                End If
        Case Else
            msgNoCitables = msgNoCitables & "--> " & rs!PR01DESCORTA & Chr$(13)
        End Select
        rs.MoveNext
    Wend

    If msgDptoRea <> "" Then
        msgDptoRea = "Las siguientes actuaciones son s�lo citables por el Dpto. realizador:" & Chr$(13) & msgDptoRea & Chr$(13)
    End If
    If msgNoCitables <> "" Then
        msgNoCitables = "Las siguientes actuaciones no pueden ser citadas:" & Chr$(13) & msgNoCitables
    End If
    If msgDptoRea <> "" Or msgNoCitables <> "" Then
        MsgBox msgDptoRea & msgNoCitables, vbInformation, "Citaci�n"
    End If
    
    If cllNAP.Count > 0 Then
        Dim Item
        For Each Item In cllNAP
            Call objsecurity.LaunchProcess("CI1150", Item)
        Next
    End If
End Sub

Public Function fblnCitarModiAnular(strDptoPrueba As String, strTipoPrueba As String, strCodAct As String) As Boolean
Dim SQL As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim vntDeptNoCitables
Dim vntPruebasCitables
Dim i As Integer
Dim K As Integer

  vntDeptNoCitables = Array("lun", "209", "170", "208", "250", "210", "11", "121", "206", "104", "106", "207", "120", "123", "113", "155", "172")
  vntPruebasCitables = Array("2248", "2473", "2474", "2466", "2477", "3196", "2788", "2793", "2802", "2803", _
    "2804", "3198", "2795", "2796", "2806", "3479")

  fblnCitarModiAnular = True
  
  If strTipoPrueba = constACTIV_PRUEBA Then  'en caso de ser una prueba
    For i = 1 To 16
      'miramos si pertenece a alguno de los dptos que se citan elllos mismos
      If strDptoPrueba = vntDeptNoCitables(i) Then
      'si es asi miramos si el usuario pertenece o no a ese departamento
        SQL = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? AND AD03FECFIN IS NULL"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = vntDeptNoCitables(i)
        qry(1) = objsecurity.strUser
        Set rs = qry.OpenResultset()
        If rs(0) = 0 Then fblnCitarModiAnular = False
        rs.Close
        qry.Close
        If strDptoPrueba = constDPTO_RAYOS Then
        For K = 0 To 15
          If strCodAct = vntPruebasCitables(K) Then fblnCitarModiAnular = True
        Next K
        End If
      End If
    Next i
  End If
  
  If strTipoPrueba = constACTIV_INTERVENCION Then
    'SI ES UNA INTERVENCION COMPROBAMOS QUE EL USUARIO PERTENECE A QUIROFANO
    SQL = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = constDPTO_QUIROFANO
    qry(1) = objsecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly)
    If rs(0) = 0 Then fblnCitarModiAnular = False
    rs.Close
    qry.Close
  End If
  If strTipoPrueba = constACTIV_HOSPITALIZACION Then
    'SI ES UNA HOSPITALLIZACION MIRAMOS QUE EL USUARIO SE A DE ADMISION
      SQL = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? "
      Set qry = objApp.rdoConnect.CreateQuery("", SQL)
      qry(0) = constDPTO_ADMISION
      qry(1) = objsecurity.strUser
      Set rs = qry.OpenResultset(rdOpenForwardOnly)
      If rs(0) = 0 Then fblnCitarModiAnular = False
      rs.Close
      qry.Close
   End If
End Function

Public Function strFecha_Sistema() As String
  Dim rs As rdoResultset
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY') from dual")
  strFecha_Sistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
End Function
Public Function strFechaHora_Sistema() As String
  Dim rs As rdoResultset
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY HH24:MI') from dual")
  strFechaHora_Sistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
End Function

