VERSION 5.00
Begin VB.Form frmPregunta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Petici�n de Actuaciones"
   ClientHeight    =   1965
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3795
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1965
   ScaleWidth      =   3795
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   495
      Left            =   1080
      TabIndex        =   0
      Top             =   1320
      Width           =   1455
   End
   Begin VB.OptionButton optOption1 
      Caption         =   "Asociar Proceso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   180
      TabIndex        =   2
      Top             =   660
      Width           =   3375
   End
   Begin VB.OptionButton optOption1 
      Caption         =   "Asociar Proceso y Asistencia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   180
      TabIndex        =   1
      Top             =   120
      Value           =   -1  'True
      Width           =   3375
   End
End
Attribute VB_Name = "frmPregunta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAceptar_Click()
    Dim blnAsociarAsist As Boolean
    If optOption1(0).Value = True Then
        blnAsociarAsist = True
    Else
        blnAsociarAsist = False
    End If
    Call objPipe.PipeSet("PR_AsocAsist", blnAsociarAsist)
    Unload Me
End Sub
