VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher DLL Modulo de Extracciones de Muestras
' Coded by SIC Donosti
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
Const PRRepMuesPacPen              As String = "PR2261" 'Aportaciones pendientes
Const PRRepMuesLabPen              As String = "PR2271" 'Extracciones pendientes

Const PRWinRealizTipoMuestra       As String = "PR0216"
Const PRWinRealizMuestra           As String = "PR0217"
Const PRWinMuestrasPendientes      As String = "PR0221"
Const PRWinMuestrasPendientes2      As String = "PR0222"
Const PRWinAsignarMuestra          As String = "PR0224"
Const PRWinConsAporPend            As String = "PR0226"
Const PRWinConsExtrPend            As String = "PR0227"
Const PRWinCuestMuestra1           As String = "PR0245"
Const PRWinCuestMuestra2           As String = "PR0246"
Const PRWINEscogerDptoM            As String = "PR0247"
Const PRWINCuestionarioMuestra     As String = "PR0303"
Const PRWINCuestionarioTipoMuestra     As String = "PR0304"
Const PRWINCuestionarioMuestraTotal    As String = "PR0305"

' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    'Extracciones/Aportaciones de Muestras Pedientes

    'Case PRWinRealizTipoMuestra
    '  Load frmcuestPTipoMuestra
    '  Call frmcuestPTipoMuestra.Show(vbModal)
    '  Unload frmcuestPTipoMuestra
    '  Set frmcuestPTipoMuestra = Nothing
    
    'Case PRWinRealizMuestra
    '  Load frmcuestPMuestra
    '  Call frmcuestPMuestra.Show(vbModal)
    '  Unload frmcuestPMuestra
    '  Set frmcuestPMuestra = Nothing

    Case PRWinMuestrasPendientes
      Load frmMuestrasPendientes
      Call frmMuestrasPendientes.Show(vbModal)
      Unload frmMuestrasPendientes
      Set frmMuestrasPendientes = Nothing
    Case PRWinMuestrasPendientes2
      Load frmMuestrasPendientes2
      Call frmMuestrasPendientes2.Show(vbModal)
      Unload frmMuestrasPendientes2
      Set frmMuestrasPendientes2 = Nothing
    
     Case PRWinAsignarMuestra
      Load frmAsigMuestras
      Call frmAsigMuestras.Show(vbModal)
      Unload frmAsigMuestras
      Set frmAsigMuestras = Nothing
    
    Case PRWinConsAporPend
      Load frmMuesPacPend
      Call frmMuesPacPend.Show(vbModal)
      Unload frmMuesPacPend
      Set frmMuesPacPend = Nothing
    
    Case PRWinConsExtrPend
      Load frmMuesLabPend
      Call frmMuesLabPend.Show(vbModal)
      Unload frmMuesLabPend
      Set frmMuesLabPend = Nothing
  
    'Case PRWinCuestMuestra1
    '  Load frmcuestMuestra1
    '  Call frmcuestMuestra1.Show(vbModal)
    '  Unload frmcuestMuestra1
    '  Set frmcuestMuestra1 = Nothing
    
    'Case PRWinCuestMuestra2
    '  Load frmcuestMuestra2
    '  Call frmcuestMuestra2.Show(vbModal)
    '  Unload frmcuestMuestra2
    '  Set frmcuestMuestra2 = Nothing
    Case PRWINEscogerDptoM
      Load frmEscogerDptoM
      Call objsecurity.AddHelpContext(538)
      Call frmEscogerDptoM.Show(vbModal)
      Unload frmEscogerDptoM
      Call objsecurity.RemoveHelpContext
      Set frmEscogerDptoM = Nothing
    Case PRWINCuestionarioMuestra
      Load frmcuestionarioMuestra
      Call objsecurity.AddHelpContext(538)
      Call frmcuestionarioMuestra.Show(vbModal)
      Unload frmcuestionarioMuestra
      Call objsecurity.RemoveHelpContext
      Set frmcuestionarioMuestra = Nothing
   Case PRWINCuestionarioTipoMuestra
      Load frmcuestionarioTipoMuestra
      Call objsecurity.AddHelpContext(538)
      Call frmcuestionarioTipoMuestra.Show(vbModal)
      Unload frmcuestionarioTipoMuestra
      Call objsecurity.RemoveHelpContext
      Set frmcuestionarioTipoMuestra = Nothing
   Case PRWINCuestionarioMuestraTotal
      Load frmcuestionarioMuestraTotal
      Call objsecurity.AddHelpContext(538)
      Call frmcuestionarioMuestraTotal.Show(vbModal)
      Unload frmcuestionarioMuestraTotal
      Call objsecurity.RemoveHelpContext
      Set frmcuestionarioMuestraTotal = Nothing
  End Select
  Call Err.Clear

End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 9 procesos
  ReDim aProcess(1 To 14, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = PRWinRealizTipoMuestra
  aProcess(1, 2) = "Cuestionario Tipo Muestra en la Realizaci�n"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinRealizMuestra
  aProcess(2, 2) = "Cuestionario Muestra en la Realizaci�n"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow

  aProcess(3, 1) = PRWinMuestrasPendientes
  aProcess(3, 2) = "Asignar Muestras Extra�das"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow

  aProcess(4, 1) = PRWinAsignarMuestra
  aProcess(4, 2) = "Asignar Muestra-Actuaci�n"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow

  aProcess(5, 1) = PRWinConsAporPend
  aProcess(5, 2) = "Consultar Aportaci�n Pendiente"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
 
  aProcess(6, 1) = PRWinConsExtrPend
  aProcess(6, 2) = "Consultar Extracci�n Pendiente"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinCuestMuestra1
  aProcess(7, 2) = "Cuestionario Muestra Primera Parte"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = PRWinCuestMuestra2
  aProcess(8, 2) = "Cuestionario Muestra Segunda Parte"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRRepMuesPacPen
  aProcess(9, 2) = "Aportaciones pendientes"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeReport
  
  aProcess(10, 1) = PRRepMuesLabPen
  aProcess(10, 2) = "Extracciones pendientes"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeReport
  
  aProcess(11, 1) = PRWINEscogerDptoM
  aProcess(11, 2) = "Selecci�n de Dptos"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow

  aProcess(12, 1) = PRWINCuestionarioMuestra
  aProcess(12, 2) = "Cuestionario de la Muestra"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWINCuestionarioTipoMuestra
  aProcess(13, 2) = "Cuestionario del Tipo de la Muestra"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = PRWINCuestionarioMuestraTotal
  aProcess(14, 2) = "Cuestionario Total de la Muestra"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeWindow
End Sub
