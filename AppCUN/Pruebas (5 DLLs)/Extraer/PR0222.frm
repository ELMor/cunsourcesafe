VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmMuestrasPendientes2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Extracciones. Asignaci�n de Muestras."
   ClientHeight    =   8025
   ClientLeft      =   315
   ClientTop       =   1005
   ClientWidth     =   11610
   ControlBox      =   0   'False
   Icon            =   "PR0222.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8025
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "Departamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   240
      TabIndex        =   9
      Top             =   480
      Width           =   7215
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Consultar"
         Height          =   375
         Left            =   5880
         TabIndex        =   14
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Planta"
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   2
         Left            =   4920
         TabIndex        =   13
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Realizador"
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   1
         Left            =   3600
         TabIndex        =   12
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Solicitante"
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   0
         Left            =   2400
         TabIndex        =   11
         Top             =   240
         Width           =   1215
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   2175
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         DefColWidth     =   2
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "COD. DPTO"
         Columns(0).Name =   "COD. DPTO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5927
         Columns(1).Caption=   "DESCRIPCI�N"
         Columns(1).Name =   "DESCRIPCI�N"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3836
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
   End
   Begin VB.CommandButton cmdAsignar 
      Caption         =   "Asignar"
      Height          =   375
      Left            =   5040
      TabIndex        =   7
      Top             =   7560
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Caption         =   " Pacientes  "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   7680
      TabIndex        =   3
      Top             =   480
      Width           =   3855
      Begin VB.OptionButton optpaciente 
         Caption         =   "Hospitalizados"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1455
      End
      Begin VB.OptionButton optpaciente 
         Caption         =   "Ambulatorios"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   1560
         TabIndex        =   5
         Top             =   360
         Width           =   1335
      End
      Begin VB.OptionButton optpaciente 
         Caption         =   "Todos"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   2880
         TabIndex        =   4
         Top             =   360
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6135
      Index           =   0
      Left            =   240
      TabIndex        =   2
      Top             =   1320
      Width           =   11535
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5640
         Index           =   0
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   360
         Width           =   11250
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   3
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   19844
         _ExtentY        =   9948
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   7740
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMuestrasPendientes2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
Dim refrescar As Boolean





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    
    Dim strKey As String
    
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
    With objMasterInfo
        .strName = "Actuaciones"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "PR0458J"
        strKey = .strDataBase & .strTable
        .intAllowance = cwAllowReadOnly
        .intCursorSize = 0
        .strWhere = "CODDPTOSOLICIT = '999'"
        Call .FormAddOrderField("CI22NUMHISTORIA", cwAscending)
        '.blnHasMaint = True
        Call .FormCreateFilterWhere(strKey, "Actuaci�n")
        Call .FormAddFilterWhere(strKey, "PR04NUMACTPLAN", "N�m Act Planific", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR03NUMACTPEDI", "N�m Act Pedi", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�d Actuaci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Desc Actuaci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR04FECPLANIFIC", "Fecha Planificaci�n", cwString)
        Call .FormAddFilterWhere(strKey, "HORA", "Hora", cwDate)
        Call .FormAddFilterWhere(strKey, "PR37DESESTADO", "Estado", cwString)
        Call .FormAddFilterWhere(strKey, "PR56DESESTMUES", "Estado Muestra", cwString)
        Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "Cod Persona", cwNumeric)
        Call .FormAddFilterWhere(strKey, "CI22NUMHISTORIA", "N�m. Historia", cwNumeric)
''        Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
''        Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
''        Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)

        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR04NUMACTPLAN", "N�m Act Planific")
        Call .FormAddFilterOrder(strKey, "PR03NUMACTPEDI", "N�m Act Pedi")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�d Actuaci�n")
        Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Desc Actuaci�n")
        Call .FormAddFilterOrder(strKey, "PR04FECPLANIFIC", "Fecha Planificaci�n")
        Call .FormAddFilterOrder(strKey, "HORA", "Hora")
        Call .FormAddFilterOrder(strKey, "PR37DESESTADO", "Estado")
        Call .FormAddFilterOrder(strKey, "PR56DESESTMUES", "Estado Muestra")
        Call .FormAddFilterOrder(strKey, "CI21CODPERSONA", "Cod Persona")
        Call .FormAddFilterOrder(strKey, "CI22NUMHISTORIA", "N�m. Historia")
        Call .FormAddFilterOrder(strKey, "NOMBRE", "Nombre")
    End With
  
  
    

    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormMultiLine)
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del grupo

        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del grupo
        Call .GridAddColumn(objMasterInfo, "Actuaci�n", "PR01DESCORTA", cwString, 30)
        Call .GridAddColumn(objMasterInfo, "Fecha", "PR04FECPLANIFIC", cwDate, 16)
        Call .GridAddColumn(objMasterInfo, "Hora", "HORA", cwString, 5)
        Call .GridAddColumn(objMasterInfo, "CAMA", "CAMA", cwString, 5)
        Call .GridAddColumn(objMasterInfo, "Historia", "CI22NUMHISTORIA", cwNumeric, 9)
        Call .GridAddColumn(objMasterInfo, "PACIENTE", "NOMBRE", cwString, 30)
        Call .GridAddColumn(objMasterInfo, "Dpto. Solicitante", "DESDPTOSOLICIT", cwString, 30)
        Call .GridAddColumn(objMasterInfo, "Dpto. Realizador", "DESDPTOPRUEBA", cwString, 9)
        Call .GridAddColumn(objMasterInfo, "NUMACTPLAN", "PR04NUMACTPLAN", cwString, 9)
        Call .FormCreateInfo(objMasterInfo)
        Call .WinRegister
        Call .WinStabilize
    End With
grdDBGrid1(0).Columns("NUMACTPLAN").Visible = False
grdDBGrid1(0).Columns("Actuaci�n").Width = 2205
grdDBGrid1(0).Columns("Fecha").Width = 1515
grdDBGrid1(0).Columns("Hora").Width = 960
grdDBGrid1(0).Columns("CAMA").Width = 645
grdDBGrid1(0).Columns("Historia").Width = 1035
grdDBGrid1(0).Columns("Dpto. Solicitante").Width = 2625
grdDBGrid1(0).Columns("Dpto. Realizador").Width = 1575
grdDBGrid1(0).Columns("PACIENTE").Width = 4335

pCargarGrid
End Sub
Private Sub cmdCommand1_Click()
    pRecargarGrid
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub









Private Sub Option1_Click(Index As Integer)
  grdDBGrid1(0).RemoveAll
End Sub

Private Sub SSDBCombo1_CloseUp(Index As Integer)
    pRecargarGrid
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    If intIndex = 20 Then
      Call MsgBox("Imposible abrir registros", vbExclamation)
    Else
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub





' -----------------------------------------------




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub



Private Sub optpaciente_Click(Index As Integer)
pRecargarGrid
End Sub




Private Sub cmdAsignar_Click()
    Dim i As Integer
    Dim vntRowBookmark As Variant
    Dim lngActPlan As Long
    Dim sql As String
    Dim qry As rdoQuery
    Dim rs As rdoResultset
    
    If grdDBGrid1(0).SelBookmarks.Count = 0 Then
        MsgBox "Debe elegir una prueba", vbCritical, "Aviso"
        Exit Sub
    End If
    cmdAsignar.Enabled = False
    For i = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
       vntRowBookmark = grdDBGrid1(0).SelBookmarks(i)
       lngActPlan = grdDBGrid1(0).Columns("NUMACTPLAN").CellValue(vntRowBookmark)
       sql = "UPDATE PR0400 SET PR56CODESTMUES = 2,PR04FECPLANIFIC=SYSDATE WHERE PR04NUMACTPLAN = ? "
       sql = sql & " AND PR56CODESTMUES = 1"
       Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngActPlan
       qry.Execute
    Next i
    objWinInfo.DataRefresh
    cmdAsignar.Enabled = True
End Sub
Private Sub pCargarGrid()
Dim sql As String
Dim rs  As rdoResultset
Dim qry As rdoQuery
    'Departamento solicitante
    sql = "SELECT AD02CODDPTO, AD02DESDPTO"
    sql = sql & " FROM AD0200"
    sql = sql & " WHERE AD02CODDPTO IN ("
    sql = sql & " SELECT AD02CODDPTO FROM AD0300"
    sql = sql & " WHERE SG02COD = ?"
    sql = sql & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
    sql = sql & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " ORDER BY AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objsecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        Do While Not rs.EOF
            SSDBCombo1(0).AddItem rs!AD02CODDPTO & ";" & rs!AD02DESDPTO
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pRecargarGrid()
If Option1(0).Value = True Then
    objMasterInfo.strWhere = "CODDPTOSOLICIT = " & SSDBCombo1(0).Columns(0).Value
ElseIf Option1(1).Value = True Then
     objMasterInfo.strWhere = "CODDPTOPRUEBA = " & SSDBCombo1(0).Columns(0).Value
Else
     objMasterInfo.strWhere = "CODDPTOCAMA = " & SSDBCombo1(0).Columns(0).Value
End If
If optpaciente(0).Value = True Then
    objMasterInfo.strWhere = objMasterInfo.strWhere & " AND CAMA IS NOT NULL"
ElseIf optpaciente(1).Value = True Then
    objMasterInfo.strWhere = objMasterInfo.strWhere & " AND CAMA IS NULL"
End If
objWinInfo.DataRefresh

End Sub
