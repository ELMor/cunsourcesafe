Attribute VB_Name = "PED003"
'************************************************************************
'* PROYECTO: PRUEBAS  (PETICI�N DE ACTUACIONES)                         *
'* NOMBRE:                                                              *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 20 DE OCTUBRE DE 1997                                         *
'* DESCRIPCI�N: Funciones y Procedimientos para obtener las fechas      *
'*               de planificaci�n                                       *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

'Public Sub insertar(planificada As Date)
'Dim strsql As String
'Dim rstA As rdoResultset
'Dim strsql1 As String
'Dim rstA1 As rdoResultset
'Dim strsql2 As String
'Dim rstA2 As rdoResultset
'Dim strsql3 As String
'Dim rstA3 As rdoResultset
'Dim actplanif As Long 'c�digo de actuaci�n planificada
  'para insertar en PR0400
'Dim strinsert As String
'Dim dpto As Integer
'Dim act As Integer
'Dim persona As Integer
'Dim entidad As Integer
'Dim tipo As Integer

'Dim quedia As Integer
'Dim quemes As Integer
'Dim queano As Integer
'Dim texto As String

'quedia = Day(planificada)
'quemes = Month(planificada)
'queano = Year(planificada)
'texto = quedia & "/" & quemes & "/" & queano



  ' generaci�n autom�tica del c�digo
'strsql = "SELECT PR04NUMACTPLAN_SEQUENCE.nextval FROM dual"
'Set rstA = objApp.rdoConnect.OpenResultset(strsql)
'actplanif = rstA.rdoColumns(0).Value
   'se busca en PR0300 (para la actuaci�n pedida):departamento,persona,actuaci�n,
   'tipo econ�mico,entidad
'strsql1 = "SELECT ad02coddpto,pr01codactuacion,ci21codpersona,ci32codtipecon," & _
         "ci13codentidad FROM PR0300 WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0)
'Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)


    'INSERTAR EN PR0400 ACTUACION_PLANIFICADA
'strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,ad01codasistenci,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme)" & _
            " VALUES (" & actplanif & ",null," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "to_date('" & texto & "','DD/MM/YYYY')" & "," & _
            "null,null,null,null,null,null,null,null,null,null)"
          
       '"to_date(" & "'" & planificada & "'" & " , " & "'" & "DD/MM/YYYY" & "'" & ")" & "," & _





'On Error GoTo Err_Ejecutar
'objApp.rdoConnect.Execute strinsert, 64

'rstA.Close
'Set rstA = Nothing
'rstA1.Close
'Set rstA1 = Nothing

'Exit Sub
'Err_Ejecutar:
'  MsgBox "Error: " & Err.Number & " " & Err.Description
'  Exit Sub
'End Sub

'****************************************************************************************

Public Sub borrar_NO_SE_USA(actpedida As Long)
'Dim strsql1 As String
'Dim rstA1 As rdoResultset
'Dim strupdate As String'

'On Error GoTo Err_Ejecutar

'se mira si la actuacion pedida ya habia generado actuaciones planificadas
'strsql1 = "select count(*) from PR0400 where pr03numactpedi=" & actpedida & _
          " and pr37codestado=1"
'Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
'If (rstA1.rdoColumns(0).Value <> 0) Then
'   'se modifica PR0400 poniendo el estado=cancelada
'   strupdate = "UPDATE PR0400 " & _
'           "SET pr37codestado=6" & _
'           " where pr03numactpedi=" & actpedida
'   objApp.rdoConnect.Execute strupdate, 64
'   objApp.rdoConnect.Execute "Commit", 64
'End If
'rstA1.close
'Set rstA1 = Nothing
'Exit Sub
'Err_Ejecutar:
'  MsgBox "Error: " & Err.Number & " " & Err.Description
'  Exit Sub
End Sub

Public Sub borrar_del_todo_NO_SE_USA(actpedida As Long)
Dim strsql1 As String
Dim rstA1 As rdoResultset
Dim strupdate As String

On Error GoTo Err_Ejecutar

'se mira si la actuacion pedida ya habia generado actuaciones planificadas
strsql1 = "select count(*) from PR0400 where pr03numactpedi=" & actpedida & _
          " and pr37codestado=1"
Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
If (rstA1.rdoColumns(0).Value <> 0) Then
   'se modifica PR0400 poniendo el estado=cancelada
   strupdate = "delete PR0400 " & _
           "where pr03numactpedi=" & actpedida & " and pr37codestado=1"
   objApp.rdoConnect.Execute strupdate, 64
   objApp.rdoConnect.Execute "Commit", 64
End If
rstA1.Close
Set rstA1 = Nothing
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


Public Sub insertarpreferencia_NO_SE_USA(planificada As Date)
Dim strsql As String
Dim rsta As rdoResultset
Dim strsql1 As String
Dim rstA1 As rdoResultset
Dim strsql2 As String
Dim rstA2 As rdoResultset
Dim strsql3 As String
Dim rstA3 As rdoResultset

Dim actplanif As Long 'c�digo de actuaci�n planificada
'para insertar en PR0400
Dim strinsert As String
Dim dpto As Integer
Dim act As Integer
Dim persona As Integer
Dim entidad As Integer
Dim tipo As Integer

Dim quedia As Integer
Dim quemes As Integer
Dim queano As Integer
Dim texto As String

Dim rstasis As rdoResultset
Dim strasis As String
Dim rstdoc As rdoResultset
Dim strdoc As String

  quedia = Day(planificada)
  quemes = Month(planificada)
  queano = Year(planificada)
  texto = quedia & "/" & quemes & "/" & queano


' generaci�n autom�tica del c�digo
strsql = "SELECT PR04NUMACTPLAN_SEQUENCE.nextval FROM dual"
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
actplanif = rsta.rdoColumns(0).Value
'se busca en PR0300 (para la actuaci�n pedida):departamento,persona,actuaci�n,
'tipo econ�mico,entidad
strsql1 = "SELECT ad02coddpto,pr01codactuacion,ci21codpersona,ci32codtipecon," & _
         "ci13codentidad FROM PR0300 WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0)
Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)

strdoc = "SELECT pr01indreqdoc FROM PR0100 WHERE pr01codactuacion=" & _
          rstA1.rdoColumns("pr01codactuacion").Value
Set rstdoc = objApp.rdoConnect.OpenResultset(strdoc)

strasis = "SELECT AD07CODPROCESO,AD01CODASISTENCI FROM PR0800 WHERE " & _
          "PR03NUMACTPEDI=" & frmdatosactpedida.txtText1(0) & " AND " & _
          "PR08NUMSECUENCIA=1"
  Set rstasis = objApp.rdoConnect.OpenResultset(strasis)

'INSERTAR EN PR0400 ACTUACION_PLANIFICADA
If frmdatosactpedida.dtcfecha = "" Then
  strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre,ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null,null,"
    If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
    End If
    If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
    Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
    End If
    
Else
  If (frmdatosactpedida.txthora.Text <> "" And frmdatosactpedida.txtminuto.Text <> "") Then
   texto = texto & " " & frmdatosactpedida.txthora.Text & ":" & frmdatosactpedida.txtminuto.Text
   strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre,ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1," & _
            frmdatosactpedida.txtText1(0) & "," & _
             "to_date('" & texto & "','DD/MM/YYYY hh24:mi')" & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null,"
            
   If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
    End If
    If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
    Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
    End If
            
'"to_date(" & "'" & planificada & "'" & " , " & "'" & "DD/MM/YYYY hh24:mi:ss" & "'" & ")" & "," & _

  
  Else
    strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre,ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & actplanif & "," & rstA1.rdoColumns(1).Value & "," & _
            rstA1.rdoColumns(0).Value & "," & rstA1.rdoColumns(2).Value & "," & "'" & rstA1.rdoColumns(3).Value & "'" & "," & "'" & rstA1.rdoColumns(4).Value & "'" & ",1," & _
            frmdatosactpedida.txtText1(0) & "," & _
            "to_date('" & texto & "','DD/MM/YYYY')" & "," & _
            "null,null,null,null,null,null,null,null,null,null,null,null,"
    If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
    End If
    If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
    Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
    End If
            
    '"to_date(" & "'" & planificada & "'" & " , " & "'" & "DD/MM/YYYY" & "'" & ")" & "," & _

  End If
End If
On Error GoTo Err_Ejecutar
objApp.rdoConnect.Execute strinsert, 64


rsta.Close
Set rsta = Nothing
rstA1.Close
Set rstA1 = Nothing
rstasis.Close
Set rstasis = Nothing
rstdoc.Close
Set rstdoc = Nothing

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub



Public Sub diariamente(inicio As Date, fin As Date, dias As Integer)
'gplan guarda la fecha planificada
Dim gfechaplan As Date
Call insertar(inicio)
gfechaplan = inicio
gfechaplan = DateAdd("d", dias, gfechaplan)
While (gfechaplan <= fin)
 Call insertar(gfechaplan)
 gfechaplan = DateAdd("d", dias, gfechaplan)
Wend
End Sub


Public Sub semanalmente(inicio As Date, fin As Date, semanas As Integer, lunes As Integer, _
                         martes As Integer, miercoles As Integer, jueves As Integer, _
                         viernes As Integer, sabado As Integer, domingo As Integer)
                         
Dim gfechaplan As Date
Dim cuantosdias As Integer

cuantosdias = (7 * semanas)

gfechaplan = inicio
If (lunes = 0 And martes = 0 And miercoles = 0 And jueves = 0 And _
    viernes = 0 And sabado = 0 And domingo = 0) Then
    Call insertar(inicio)
    gfechaplan = DateAdd("ww", semanas, gfechaplan)
    While (gfechaplan <= fin)
     Call insertar(gfechaplan)
     gfechaplan = DateAdd("ww", semanas, gfechaplan)
    Wend
End If
gfechaplan = inicio
If (lunes = 1) Then
 'se busca el primer lunes a partir de la fecha de inicio
 Do While (gfechaplan <= fin)
  If WeekDay(gfechaplan) <> 2 Then
     gfechaplan = DateAdd("d", 1, gfechaplan)
   Else 'ha encontrado el primer lunes
   Call insertar(gfechaplan)
     Exit Do
   End If
 Loop
 'se planifican los pr�ximos lunes
 gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Do While (gfechaplan <= fin)
  Call insertar(gfechaplan)
  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Loop
End If
gfechaplan = inicio
If (martes = 1) Then
 'se busca el primer martes a partir de la fecha de inicio
 Do While (gfechaplan <= fin)
  If WeekDay(gfechaplan) <> 3 Then
     gfechaplan = DateAdd("d", 1, gfechaplan)
   Else 'ha encontrado el primer martes
   Call insertar(gfechaplan)
     Exit Do
   End If
 Loop
 'se planifican los pr�ximos martes
 gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Do While (gfechaplan <= fin)
  Call insertar(gfechaplan)
  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Loop
End If
gfechaplan = inicio
If (miercoles = 1) Then
 'se busca el primer miercoles a partir de la fecha de inicio
 Do While (gfechaplan <= fin)
  If WeekDay(gfechaplan) <> 4 Then
     gfechaplan = DateAdd("d", 1, gfechaplan)
   Else 'ha encontrado el primer miercoles
   Call insertar(gfechaplan)
     Exit Do
   End If
 Loop
 'se planifican los pr�ximos miercoles
 gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Do While (gfechaplan <= fin)
  Call insertar(gfechaplan)
  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Loop
End If
gfechaplan = inicio
If (jueves = 1) Then
 'se busca el primer jueves a partir de la fecha de inicio
 Do While (gfechaplan <= fin)
  If WeekDay(gfechaplan) <> 5 Then
     gfechaplan = DateAdd("d", 1, gfechaplan)
   Else 'ha encontrado el primer jueves
   Call insertar(gfechaplan)
     Exit Do
   End If
 Loop
 'se planifican los pr�ximos jueves
 gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Do While (gfechaplan <= fin)
  Call insertar(gfechaplan)
  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Loop
End If
gfechaplan = inicio
If (viernes = 1) Then
 'se busca el primer viernes a partir de la fecha de inicio
 Do While (gfechaplan <= fin)
  If WeekDay(gfechaplan) <> 6 Then
     gfechaplan = DateAdd("d", 1, gfechaplan)
   Else 'ha encontrado el primer viernes
   Call insertar(gfechaplan)
     Exit Do
   End If
 Loop
 'se planifican los pr�ximos viernes
 gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Do While (gfechaplan <= fin)
  Call insertar(gfechaplan)
  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Loop
End If
gfechaplan = inicio
If (sabado = 1) Then
 'se busca el primer sabado a partir de la fecha de inicio
 Do While (gfechaplan <= fin)
  If WeekDay(gfechaplan) <> 7 Then
     gfechaplan = DateAdd("d", 1, gfechaplan)
   Else 'ha encontrado el primer sabado
   Call insertar(gfechaplan)
     Exit Do
   End If
 Loop
 'se planifican los pr�ximos sabado
 gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Do While (gfechaplan <= fin)
  Call insertar(gfechaplan)
  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Loop
End If
gfechaplan = inicio
If (domingo = 1) Then
 'se busca el primer domingo a partir de la fecha de inicio
 Do While (gfechaplan <= fin)
  If WeekDay(gfechaplan) <> 1 Then
     gfechaplan = DateAdd("d", 1, gfechaplan)
   Else 'ha encontrado el primer domingo
   Call insertar(gfechaplan)
     Exit Do
   End If
 Loop
 'se planifican los pr�ximos domingos
 gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Do While (gfechaplan <= fin)
  Call insertar(gfechaplan)
  gfechaplan = DateAdd("d", cuantosdias, gfechaplan)
 Loop
End If

End Sub



Public Sub mensualmente_1(inicio As Date, fin As Date, dia As Integer, mes As Integer)
Dim gfechaplan As Date
'dia contiene el d�a del mes de la actuacion
'mes contiene cada cu�ntos meses se hace la actuaci�n
gfechaplan = inicio
'si el d�a de inicio es igual al d�a en que debe hacerse la actuaci�n
Do While (gfechaplan <= fin)
 If Day(gfechaplan) = dia Then
    insertar (gfechaplan)
    Exit Do
 Else
    gfechaplan = DateAdd("d", 1, gfechaplan)
 End If
 Loop
 'ya se ha encontrado la primera fecha planificada, se suman los meses
 gfechaplan = DateAdd("m", mes, gfechaplan)
 Do While (gfechaplan <= fin)
  insertar (gfechaplan)
  gfechaplan = DateAdd("m", mes, gfechaplan)
 Loop
End Sub


Public Sub anualmente_1(inicio As Date, fin As Date, dia As Integer, mes As Integer, _
                        ano As Integer)
'dia contiene el n�mero de d�a del mes
'mes contiene el n�mero de mes
'ano contiene cada cu�ntos a�os
Dim gfechaplan As Date

gfechaplan = inicio
'se mira si la fecha de inicio coincide con la pedida
If (Day(gfechaplan) = dia And Month(gfechaplan) = mes) Then
   insertar (gfechaplan)
Else
 'la fecha no coincide ,hay que buscar la primera fecha que la cumpla
   Do While (gfechaplan <= fin)
     gfechaplan = DateAdd("d", 1, gfechaplan)
     If (Day(gfechaplan) = dia And Month(gfechaplan) = mes) Then
       insertar (gfechaplan)
       Exit Do
     End If
   Loop
End If

'se crean todas las fechas planificadas,sumando el intervalo en a�os
gfechaplan = DateAdd("yyyy", ano, gfechaplan)
Do While (gfechaplan <= fin)
   insertar (gfechaplan)
   gfechaplan = DateAdd("yyyy", ano, gfechaplan)
Loop
End Sub

Public Function dia1atras(fecha As Date) As Date
While Day(fecha) <> 1
    fecha = DateAdd("d", -1, fecha)
Wend
dia1atras = fecha
End Function
Public Function dia1adelante(diabuscado As Integer, fecha As Date) As Date
While Day(fecha) <> diabuscado
    fecha = DateAdd("d", 1, fecha)
Wend
dia1adelante = fecha
End Function
Public Function buscardia(fecha As Date, diasem As Integer) As Date
'funci�n que coge el d�a 1 del mes y busca el primer diasem
'diasem=lunes,...,domingo
If diasem < 7 Then
  While WeekDay(fecha) <> (diasem + 1)
    fecha = DateAdd("d", 1, fecha)
  Wend
End If
If diasem = 7 Then
  While WeekDay(fecha) <> 1
    fecha = DateAdd("d", 1, fecha)
  Wend
End If
buscardia = fecha
End Function
Public Function ultimodiadelmes(mes As Integer, ano As Integer) As Integer
'dado un mes y un a�o devuelve el n� de d�as del mes
Select Case mes
     Case 1, 3, 5, 7, 8, 10, 12
          ultimodiadelmes = 31
     Case 4, 6, 9, 11
          ultimodiadelmes = 30
     Case 2
          If (ano Mod 4) = 0 Then
            ultimodiadelmes = 29
          Else
            ultimodiadelmes = 28
          End If
End Select
End Function


Public Sub mensualmente_2(inicio As Date, fin As Date, dia As Integer, _
                          semana As Integer, mes As Integer)
Dim gfechaplan As Date
Dim ultimo As Integer
Dim fechaaux As Date
Dim proxfecha As Date
Dim guardarinicio As Date
Dim guardarfechaaux As Date
'dia indica el lugar que ocupa el d�a(primero,segundo,...,�ltimo)
'semana indica el d�a de la semana
'mes indica el intervalo de meses
guardarinicio = inicio
If dia <> 9 Then  'dia<>"�ltimo"
If semana <> 0 Then  'semana<>"D�a",es decir, semana=lunes,....,domingo
   gfechaplan = dia1atras(guardarinicio)
   gfechaplan = buscardia(gfechaplan, semana)
   gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
   If (gfechaplan < inicio) Then
     guardarinicio = inicio
     gfechaplan = dia1adelante(1, guardarinicio)
     gfechaplan = buscardia(gfechaplan, semana)
     'se suma dia-1,es decir,si dia=primero=1 no suma ninguna semana
     'pero si es dia=segundo=2 suma 1 semana
     gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
   End If
   If (gfechaplan <= fin) Then
     'inserto si la fecha obtenida no supera a la de fin
     insertar (gfechaplan)
   End If
   
   'calculo la fecha dentro de X meses pero puede que no coincida el d�a
   gfechaplan = DateAdd("m", mes, gfechaplan)
   proxfecha = gfechaplan
   While (gfechaplan <= fin)
    gfechaplan = dia1atras(proxfecha)
    gfechaplan = buscardia(gfechaplan, semana)
    gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
    If (gfechaplan < proxfecha) Then
     gfechaplan = dia1adelante(1, proxfecha)
     gfechaplan = buscardia(gfechaplan, semana)
     gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
     End If
    insertar (gfechaplan)
    gfechaplan = DateAdd("m", mes, gfechaplan)
    proxfecha = gfechaplan
   Wend
End If
If semana = 0 Then
   guardarinicio = inicio
   gfechaplan = dia1atras(guardarinicio)
   gfechaplan = DateAdd("d", (dia - 1), gfechaplan)
   If (gfechaplan < inicio) Then
     guardarinicio = inicio
     gfechaplan = dia1adelante(1, guardarinicio)
     gfechaplan = DateAdd("d", (dia - 1), gfechaplan)
   End If
   insertar (gfechaplan)
   gfechaplan = DateAdd("m", mes, gfechaplan)
   While (gfechaplan <= fin)
    insertar (gfechaplan)
    gfechaplan = DateAdd("m", mes, gfechaplan)
   Wend
 End If
End If

If dia = 9 Then '�ltimo
If semana = 0 Then '�ltimo d�a del mes
  guardarinicio = inicio
  ultimo = ultimodiadelmes(Month(guardarinicio), Year(guardarinicio))
  guardarinicio = inicio
  gfechaplan = dia1adelante(ultimo, guardarinicio)
  insertar (gfechaplan)
  'ya tenemos el �ltimo d�a del mes
  gfechaplan = DateAdd("m", mes, gfechaplan)
  Do While (gfechaplan <= fin)
    guardarinicio = gfechaplan
    ultimo = ultimodiadelmes(Month(guardarinicio), Year(guardarinicio))
    guardarinicio = gfechaplan
    gfechaplan = dia1adelante(ultimo, guardarinicio)
    insertar (gfechaplan)
    gfechaplan = DateAdd("m", mes, gfechaplan)
  Loop
 End If
 
 If semana <> 0 Then '�ltimo lunes,�ltimo martes,...,�ltimo domingo
   'primero calculo el cuarto lunes,...,domingo
   guardarinicio = inicio
   gfechaplan = dia1atras(guardarinicio)
   guardarinicio = gfechaplan
   gfechaplan = buscardia(guardarinicio, semana)
   gfechaplan = DateAdd("ww", 3, gfechaplan) 'sumo 3 semanas
   If (gfechaplan < inicio) Then
     gfechaplan = dia1adelante(1, inicio)
     gfechaplan = buscardia(gfechaplan, semana)
     gfechaplan = DateAdd("ww", 3, gfechaplan)  'se suma 3 semanas
   End If
   'ya tengo el cuarto (lunes por ejemplo),ahora miro si hay un quinto
   Do While gfechaplan <= fin
      fechaaux = gfechaplan
      Do While (fechaaux <= fin) And (Month(fechaaux) = Month(gfechaplan))
          If (semana < 7) Then
            If (WeekDay(fechaaux) = (semana + 1)) And (fechaaux <> gfechaplan) Then
             gfechaplan = fechaaux
             Exit Do
            End If
          End If
          If (semana = 7) Then
            If (WeekDay(fechaaux) = 1) And (fechaaux <> gfechaplan) Then
             gfechaplan = fechaaux
             Exit Do
            End If
          End If
          fechaaux = DateAdd("d", 1, fechaaux)
      Loop
      insertar (gfechaplan)
 
      gfechaplan = DateAdd("m", mes, gfechaplan)
      guardarinicio = gfechaplan
      gfechaplan = dia1atras(guardarinicio)
      guardarinicio = gfechaplan
      gfechaplan = buscardia(gfechaplan, semana)
      gfechaplan = DateAdd("ww", 3, gfechaplan) 'sumo 3 semanas
      If (gfechaplan < inicio) Then
       gfechaplan = dia1adelante(1, inicio)
       gfechaplan = buscardia(gfechaplan, semana)
       gfechaplan = DateAdd("ww", 3, gfechaplan)  'se suma 3 semanas
      End If
    Loop
  End If
End If
End Sub

'++++++++++++++++++++++++++++++++++++++++++++++++

Public Sub anualmente_2(inicio As Date, fin As Date, dia As Integer, _
                          semana As Integer, mes As Integer, ano As Integer)
Dim gfechaplan As Date
Dim ultimo As Integer
Dim fechaaux As Date
Dim proxfecha As Date
Dim guardarinicio As Date
Dim guardarfechaaux As Date
Dim guardargfechaplan As Date
'dia indica el lugar que ocupa el d�a(primero,segundo,...,�ltimo)
'semana indica el d�a de la semana
'mes indica el mes
'ano indica el intervalo en a�os

gfechaplan = inicio
'primero se busca la fecha de inicio del mes indicado
Do While (gfechaplan <= fin)
   If (Month(gfechaplan) = mes) Then
     Exit Do
   Else
     gfechaplan = DateAdd("m", 1, gfechaplan)
   End If
Loop
'ya tenemos una fecha del mes indicado
     
guardargfechaplan = gfechaplan
guardarinicio = gfechaplan
If dia <> 9 Then  'dia<>"�ltimo"
If semana <> 0 Then  'semana<>"D�a",es decir, semana=lunes,....,domingo
   gfechaplan = dia1atras(guardarinicio)
   gfechaplan = buscardia(gfechaplan, semana)
   gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
   If (gfechaplan < inicio) Then
     guardarinicio = guardargfechaplan
     gfechaplan = dia1adelante(1, guardarinicio)
     gfechaplan = buscardia(gfechaplan, semana)
     'se suma dia-1,es decir,si dia=primero=1 no suma ninguna semana
     'pero si es dia=segundo=2 suma 1 semana
     gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
   End If
   If (gfechaplan <= fin) Then
     'inserto si la fecha obtenida no supera a la de fin
     insertar (gfechaplan)
   End If
   
   'calculo la fecha dentro de X meses pero puede que no coincida el d�a
   gfechaplan = DateAdd("yyyy", ano, gfechaplan)
   proxfecha = gfechaplan
   While (gfechaplan <= fin)
    gfechaplan = dia1atras(proxfecha)
    gfechaplan = buscardia(gfechaplan, semana)
    gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
    If (gfechaplan < proxfecha) Then
     gfechaplan = dia1adelante(1, proxfecha)
     gfechaplan = buscardia(gfechaplan, semana)
     gfechaplan = DateAdd("ww", (dia - 1), gfechaplan)
     End If
    insertar (gfechaplan)
    gfechaplan = DateAdd("yyyy", ano, gfechaplan)
    proxfecha = gfechaplan
   Wend
End If
If semana = 0 Then
   guardargfechaplan = gfechaplan
   guardarinicio = gfechaplan
   gfechaplan = dia1atras(guardarinicio)
   gfechaplan = DateAdd("d", (dia - 1), gfechaplan)
   If (gfechaplan < inicio) Then
     guardargfechaplan = gfechaplan
     guardarinicio = gfechaplan
     gfechaplan = dia1adelante(1, guardarinicio)
     gfechaplan = DateAdd("d", (dia - 1), gfechaplan)
   End If
   insertar (gfechaplan)
   gfechaplan = DateAdd("yyyy", ano, gfechaplan)
   While (gfechaplan <= fin)
    insertar (gfechaplan)
    gfechaplan = DateAdd("yyyy", ano, gfechaplan)
   Wend
 End If
End If

If dia = 9 Then '�ltimo
If semana = 0 Then '�ltimo d�a del mes
  guardargfechaplan = gfechaplan
  guardarinicio = gfechaplan
  ultimo = ultimodiadelmes(Month(guardarinicio), Year(guardarinicio))
  guardargfechaplan = gfechaplan
  guardarinicio = gfechaplan
  gfechaplan = dia1adelante(ultimo, guardarinicio)
  insertar (gfechaplan)
  'ya tenemos el �ltimo d�a del mes
  gfechaplan = DateAdd("yyyy", ano, gfechaplan)
  Do While (gfechaplan <= fin)
    guardarinicio = gfechaplan
    ultimo = ultimodiadelmes(Month(guardarinicio), Year(guardarinicio))
    guardarinicio = gfechaplan
    gfechaplan = dia1adelante(ultimo, guardarinicio)
    insertar (gfechaplan)
    gfechaplan = DateAdd("yyyy", ano, gfechaplan)
  Loop
 End If
 
 If semana <> 0 Then '�ltimo lunes,�ltimo martes,...,�ltimo domingo
   'primero calculo el cuarto lunes,...,domingo
   guardargfechaplan = gfechaplan
   guardarinicio = gfechaplan
   gfechaplan = dia1atras(guardarinicio)
   guardarinicio = gfechaplan
   gfechaplan = buscardia(guardarinicio, semana)
   gfechaplan = DateAdd("ww", 3, gfechaplan) 'sumo 3 semanas
   If (gfechaplan < inicio) Then
     gfechaplan = dia1adelante(1, inicio)
     gfechaplan = buscardia(gfechaplan, semana)
     gfechaplan = DateAdd("ww", 3, gfechaplan)  'se suma 3 semanas
   End If
   'ya tengo el cuarto (lunes por ejemplo),ahora miro si hay un quinto
   Do While gfechaplan <= fin
      fechaaux = gfechaplan
      Do While (fechaaux <= fin) And (Month(fechaaux) = Month(gfechaplan))
          If (semana < 7) Then
            If (WeekDay(fechaaux) = (semana + 1)) And (fechaaux <> gfechaplan) Then
             gfechaplan = fechaaux
             Exit Do
            End If
          End If
          If (semana = 7) Then
            If (WeekDay(fechaaux) = 1) And (fechaaux <> gfechaplan) Then
             gfechaplan = fechaaux
             Exit Do
            End If
          End If
          fechaaux = DateAdd("d", 1, fechaaux)
      Loop
      insertar (gfechaplan)
 
      gfechaplan = DateAdd("yyyy", ano, gfechaplan)
      guardarinicio = gfechaplan
      gfechaplan = dia1atras(guardarinicio)
      guardarinicio = gfechaplan
      gfechaplan = buscardia(gfechaplan, semana)
      gfechaplan = DateAdd("ww", 3, gfechaplan) 'sumo 3 semanas
      If (gfechaplan < inicio) Then
       gfechaplan = dia1adelante(1, inicio)
       gfechaplan = buscardia(gfechaplan, semana)
       gfechaplan = DateAdd("ww", 3, gfechaplan)  'se suma 3 semanas
      End If
    Loop
  End If
End If
End Sub




