VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmSelDptos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Selecci�n de Actuaciones. Departamentos"
   ClientHeight    =   8220
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0165.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8220
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdA�adirSel 
      Caption         =   "A�adir Seleccionadas"
      Height          =   375
      Left            =   1800
      TabIndex        =   12
      Top             =   7560
      Width           =   2055
   End
   Begin VB.CommandButton cmdAceptardpto 
      Caption         =   "Aceptar"
      Height          =   375
      Index           =   0
      Left            =   6480
      TabIndex        =   4
      Top             =   7560
      Width           =   2055
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones del Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6855
      Index           =   2
      Left            =   5280
      TabIndex        =   3
      Top             =   600
      Width           =   6615
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6375
         Index           =   1
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   360
         Width           =   6330
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   11165
         _ExtentY        =   11245
         _StockProps     =   79
         Caption         =   "ACTUACIONES DEL DEPARTAMENTO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6855
      HelpContextID   =   2
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   5055
      Begin TabDlg.SSTab tabTab1 
         Height          =   6255
         HelpContextID   =   90001
         Index           =   1
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   4770
         _ExtentX        =   8414
         _ExtentY        =   11033
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0165.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(0)"
         Tab(0).Control(1)=   "txtText1(1)"
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0165.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   -74760
            TabIndex        =   1
            Tag             =   "C�digo"
            Top             =   960
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   1050
            Index           =   1
            Left            =   -74760
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   2
            Tag             =   "Descripci�n"
            Top             =   1800
            Width           =   3840
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5970
            Index           =   2
            Left            =   120
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   120
            Width           =   4095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   7223
            _ExtentY        =   10530
            _StockProps     =   79
            Caption         =   "DEPARTAMENTOS"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   -74760
            TabIndex        =   11
            Top             =   1560
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   -74760
            TabIndex        =   10
            Top             =   720
            Width           =   600
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   7935
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSelDptos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00165.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 30 DE OCTUBRE DE 1997                                         *
'* DESCRIPCION: PERMITE SELECCIONAR DEPARTAMENTOS                       *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gblnselec As Boolean

Private Sub cmdAceptardpto_Click(Index As Integer)
    Dim mlonNTotal As Long
    Dim mintNTotalSelRows As Integer
    Dim mintI As Integer
    Dim mvarBkmrk As Variant
    Dim mintCodDepa As Long
    Dim mintCodAct As Long
    Dim mintCodProt As Long
    Dim mlngcoddpto As Long
    Dim mlngCodPaq As Long
    Dim strSelect As String
    Dim rsta As rdoResultset
    Dim intCont As Integer
    Dim mblna�adido As Boolean
    Dim mlngActuacion As Long
    Dim mlngGrupo As Long
    Dim blnExisteBucle As Boolean
    Dim intResp As Integer
    Dim strmensaje As String
 
    intCont = 0
    blnExisteBucle = False
    
    'Dejamos que se pueda a�adir en la tabla de actuaciones del protocolo
    'objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
    
    'A�adimos a las actuciones del protocolo las actuaciones que se han
    'seleccionado previamente en la tabla de actuaciones
    
    'Guardamos el n�mero de filas seleccionadas
    mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
    gintIndice = 0
    ReDim gtblactsel(3, 1)

    
    For mintI = 0 To mintNTotalSelRows - 1
        'Guardamos el n�mero de fila que est� seleccionada
        mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintI)
        'Guardamos los valores de esa fila en variables
        mintCodAct = grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk)
        mintCodDepa = grdDBGrid1(1).Columns(5).CellValue(mvarBkmrk)
        If gstrLlamadorSel = "Protocolos" Then
            mintCodProt = frmDefProtocolos.txtText1(0).Text
            'Comprobamos que la actuaci�n no pertenezca ya al protocolo
            strSelect = "Select count(*) from PR3600 " _
                & "where PR3600.PR01CODACTUACION = " & mintCodAct _
                & " and PR3600.PR35CODPROTOCOLO = " & mintCodProt _
                & " and PR3600.AD02CODDPTO=" & mintCodDepa
            Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
        Else
            If gstrLlamadorSel = "Paquetes" Then
                mlngcoddpto = frmpaquetes.txtText1(2).Text
                mlngCodPaq = frmpaquetes.txtText1(0).Text
                'Comprobamos que la actuaci�n no pertenezca ya al dpto
                strSelect = "Select count(*) from PR3400 " _
                    & "where PR3400.PR01CODACTUACION = " & mintCodAct _
                    & " and PR3400.PR33CODESTANDARD = " & mlngCodPaq _
                    & " and PR3400.AD02CODDPTO =" & mlngcoddpto _
                    & " and pr3400.AD02CODDPTO_REA =" & mintCodDepa
                Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
            Else
                If gstrLlamadorSel = "Asociadas" Then
                    mlngActuacion = frmActAsociadas.txtactText1(0).Text
                    strSelect = "Select count(*) from PR3100 " _
                        & "where PR3100.PR01CODACTUACION = " & mlngActuacion _
                        & " and PR3100.PR01CODACTUACION_ASO = " & mintCodAct
                        '& " and PR3100.AD02CODDPTO=" & mintCodDepa
                    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
                    'Para que en las asocias no aparezcan las previsibles
                    If rsta.rdoColumns(0).Value = 0 Then
                      mlngActuacion = frmActAsociadas.txtactText1(0).Text
                      strSelect = "Select count(*) from PR3200 " _
                                & "where PR3200.PR01CODACTUACION = " & mlngActuacion _
                                & " and PR3200.PR32CODACTUACION_PRE = " & mintCodAct
                                '& " and PR3200.AD02CODDPTO=" & mintCodDepa
                      Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
                    End If
                     'Para evitar que una actuaci�n A tenga a otra actuaci�n B como asociada
                    'Cuan ya en B a est� como asociada
                    blnExisteBucle = Hay_Bucle(mlngActuacion, mintCodAct)
                    If blnExisteBucle = True Then
                      strSelect = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION = " & mintCodAct
                      Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
                      strmensaje = "La actuaci�n " & rsta.rdoColumns(0).Value & " no se puede asociar porque se crear�a un ciclo"
                      intResp = MsgBox(strmensaje, vbInformation, "Importante")
                      'rsta.Close
                      'Set rsta = Nothing
                    End If
                Else
                    If gstrLlamadorSel = "Previsibles" Then
                        mlngActuacion = frmActPrevisibles.txtactText1(0).Text
                        strSelect = "Select count(*) from PR3200 " _
                            & "where PR3200.PR01CODACTUACION = " & mlngActuacion _
                            & " and PR3200.PR32CODACTUACION_PRE = " & mintCodAct
                            '& " and PR3200.AD02CODDPTO=" & mintCodDepa
                        Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
                        'Para que en las previsibles no aparezcan las asociadas
                        If rsta.rdoColumns(0).Value = 0 Then
                          mlngActuacion = frmActPrevisibles.txtactText1(0).Text
                          strSelect = "Select count(*) from PR3100 " _
                                    & "where PR3100.PR01CODACTUACION = " & mlngActuacion _
                                    & " and PR3100.PR01CODACTUACION_ASO = " & mintCodAct
                                    '& " and PR3100.AD02CODDPTO=" & mintCodDepa
                          Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
                        End If
                    Else
                        mlngGrupo = frmDefGrupos.txtText1(0).Text
                        strSelect = "Select count(*) from PR1700 " _
                            & "where PR1700.PR16CODGRUPO = " & mlngGrupo _
                            & " and PR1700.PR01CODACTUACION=" & mintCodAct
                        Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
                    End If
                End If
            End If
        End If
        If blnExisteBucle = False Then
          intCont = rsta.rdoColumns(0).Value
        End If
        If gstrCodAct <> "" Then
          'venimos de actuaciones asociadas o previsibles
          If gstrCodAct = mintCodAct Then
            intCont = 1
          End If
        End If
        If (intCont = 0) And (blnExisteBucle = False) Then
            'A�adimos una nueva fila a la tabla de actuaciones del protocolo
            'Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
            'Call objWinInfo.DataNewmintCodAct
            'Pasamos los valores a la tabla de actuaciones del protocolo
            gtblactsel(0, gintIndice) = mintCodAct 'C�digo Departamento
            gtblactsel(1, gintIndice) = mintCodDepa  'C�digo Actuaci�n a incluir en el protocolo
            If gstrLlamadorSel = "Protocolos" Then
                gtblactsel(2, gintIndice) = mintCodProt 'C�digo Protocolo
            Else
                If gstrLlamadorSel = "Paquetes" Then
                    gtblactsel(2, gintIndice) = mlngCodPaq 'C�digo Protocolo
                Else
                  If gstrLlamadorSel = "Asociadas" Then
                    gtblactsel(2, gintIndice) = mlngActuacion
                    Else
                        If gstrLlamadorSel = "Previsibles" Then
                            gtblactsel(2, gintIndice) = mlngActuacion
                        Else
                            gtblactsel(2, gintIndice) = mlngGrupo
                        End If
                    End If
                End If
            End If
            gintIndice = gintIndice + 1
            ReDim Preserve gtblactsel(3, gintIndice)
        Else
            intCont = 0
        End If
        rsta.Close
        Set rsta = Nothing
    Next mintI
    Unload Me
    'Pasamos el foco al frame de actuaciones
    'Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
   
   'Solo dejamos borrar en la tabla de actuaciones del protocolo
   'objWinInfo.objWinActiveForm.intAllowance = cwAllowDelete
  'Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
End Sub

'JRC 1-2-1999
Private Sub cmdA�adirSel_Click()
Dim mintNTotalSelRows As Integer
Dim mintI As Integer
Dim mvarBkmrk As Variant
Dim mintCodDepa As Long
Dim mintCodAct As Long
Dim mintCodProt As Long
Dim mlngcoddpto As Long
Dim mlngCodPaq As Long
Dim strSelect As String
Dim rsta As rdoResultset
Dim intCont As Integer
Dim mlngActuacion As Long
Dim mlngGrupo As Long
Dim blnExisteBucle As Boolean
Dim intResp As Integer
Dim strmensaje As String
Dim strinsert As String

  cmdA�adirSel.Enabled = False

  intCont = 0
  blnExisteBucle = False
  
  'Guardamos el n�mero de filas seleccionadas
  mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  'gintIndice = 0
  'ReDim gtblactsel(3, 1)
  
  For mintI = 0 To mintNTotalSelRows - 1
    'Guardamos el n�mero de fila que est� seleccionada
    mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintI)
    'Guardamos los valores de esa fila en variables
    mintCodAct = grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk)
    mintCodDepa = grdDBGrid1(1).Columns(5).CellValue(mvarBkmrk)
    If gstrLlamadorSel = "Protocolos" Then
      mintCodProt = frmDefProtocolos.txtText1(0).Text
      'Comprobamos que la actuaci�n no pertenezca ya al protocolo
      strSelect = "Select count(*) from PR3600 " _
                  & "where PR3600.PR01CODACTUACION = " & mintCodAct _
                  & " and PR3600.PR35CODPROTOCOLO = " & mintCodProt _
                  & " and PR3600.AD02CODDPTO=" & mintCodDepa
      Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    Else
      If gstrLlamadorSel = "Paquetes" Then
        mlngcoddpto = frmpaquetes.txtText1(2).Text
        mlngCodPaq = frmpaquetes.txtText1(0).Text
        'Comprobamos que la actuaci�n no pertenezca ya al dpto
        strSelect = "Select count(*) from PR3400 " _
                    & "where PR3400.PR01CODACTUACION = " & mintCodAct _
                    & " and PR3400.PR33CODESTANDARD = " & mlngCodPaq _
                    & " and PR3400.AD02CODDPTO =" & mlngcoddpto _
                    & " and pr3400.AD02CODDPTO_REA =" & mintCodDepa
        Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
      Else
        If gstrLlamadorSel = "Asociadas" Then
          mlngActuacion = frmActAsociadas.txtactText1(0).Text
          strSelect = "Select count(*) from PR3100 " _
                      & "where PR3100.PR01CODACTUACION = " & mlngActuacion _
                      & " and PR3100.PR01CODACTUACION_ASO = " & mintCodAct
                      '& " and PR3100.AD02CODDPTO=" & mintCodDepa
          Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
          'Para que en las asocias no aparezcan las previsibles
          If rsta.rdoColumns(0).Value = 0 Then
            mlngActuacion = frmActAsociadas.txtactText1(0).Text
            strSelect = "Select count(*) from PR3200 " _
                        & "where PR3200.PR01CODACTUACION = " & mlngActuacion _
                        & " and PR3200.PR32CODACTUACION_PRE = " & mintCodAct
                        '& " and PR3200.AD02CODDPTO=" & mintCodDepa
            Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
          End If
          'Para evitar que una actuaci�n A tenga a otra actuaci�n B como asociada
          'Cuan ya en B a est� como asociada
          blnExisteBucle = Hay_Bucle(mlngActuacion, mintCodAct)
          If blnExisteBucle = True Then
            strSelect = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION = " & mintCodAct
            Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
            strmensaje = "La actuaci�n " & rsta.rdoColumns(0).Value & " no se puede asociar porque se crear�a un ciclo"
            intResp = MsgBox(strmensaje, vbInformation, "Importante")
            'rsta.Close
            'Set rsta = Nothing
          End If
        Else
          If gstrLlamadorSel = "Previsibles" Then
            mlngActuacion = frmActPrevisibles.txtactText1(0).Text
            strSelect = "Select count(*) from PR3200 " _
                        & "where PR3200.PR01CODACTUACION = " & mlngActuacion _
                        & " and PR3200.PR32CODACTUACION_PRE = " & mintCodAct
                        '& " and PR3200.AD02CODDPTO=" & mintCodDepa
            Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
            'Para que en las previsibles no aparezcan las asociadas
            If rsta.rdoColumns(0).Value = 0 Then
              mlngActuacion = frmActPrevisibles.txtactText1(0).Text
              strSelect = "Select count(*) from PR3100 " _
                          & "where PR3100.PR01CODACTUACION = " & mlngActuacion _
                          & " and PR3100.PR01CODACTUACION_ASO = " & mintCodAct
                          '& " and PR3100.AD02CODDPTO=" & mintCodDepa
              Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
            End If
          Else
            mlngGrupo = frmDefGrupos.txtText1(0).Text
            strSelect = "Select count(*) from PR1700 " _
                        & "where PR1700.PR16CODGRUPO = " & mlngGrupo _
                        & " and PR1700.PR01CODACTUACION=" & mintCodAct
            Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
          End If
        End If
      End If
    End If
    If blnExisteBucle = False Then
      intCont = rsta.rdoColumns(0).Value
    End If
    If gstrCodAct <> "" Then
      'venimos de actuaciones asociadas o previsibles
      If gstrCodAct = mintCodAct Then
        intCont = 1
      End If
    End If
    If (intCont = 0) And (blnExisteBucle = False) Then
      'A�adimos una nueva fila a la tabla de actuaciones del protocolo
      'Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
      'Call objWinInfo.DataNewmintCodAct
      'Pasamos los valores a la tabla de actuaciones del protocolo
      'gtblactsel(0, gintIndice) = mintCodAct 'C�digo Departamento
      'gtblactsel(1, gintIndice) = mintCodDepa  'C�digo Actuaci�n a incluir en el protocolo
      If gstrLlamadorSel = "Protocolos" Then
        'gtblactsel(2, gintIndice) = mintCodProt 'C�digo Protocolo
        'Protocolos
        strinsert = "INSERT INTO PR3600 (PR35CODPROTOCOLO,PR01CODACTUACION,AD02CODDPTO) " & _
                    "VALUES (" & mintCodProt & "," & mintCodAct & "," & mintCodDepa & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
      Else
        If gstrLlamadorSel = "Paquetes" Then
          'gtblactsel(2, gintIndice) = mlngCodPaq 'Codigo Paquete
          'Paquetes
          strinsert = "INSERT INTO PR3400 " & _
                      "(AD02CODDPTO,PR33CODESTANDARD,PR01CODACTUACION,AD02CODDPTO_REA) " & _
                      "VALUES (" & mlngcoddpto & "," _
                      & mlngCodPaq & "," _
                      & mintCodAct & "," _
                      & mintCodDepa & ")"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
        Else
          If gstrLlamadorSel = "Asociadas" Then
            'gtblactsel(2, gintIndice) = mlngActuacion
            'Asociadas
            strinsert = "INSERT INTO PR3100 (PR01CODACTUACION,PR01CODACTUACION_ASO,AD02CODDPTO) " & _
                        "VALUES (" & mlngActuacion & "," & mintCodAct & "," & mintCodDepa & ")"
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
          Else
            If gstrLlamadorSel = "Previsibles" Then
              'gtblactsel(2, gintIndice) = mlngActuacion
              'Previsibles
              strinsert = "INSERT INTO PR3200 (PR01CODACTUACION," & _
                          "PR32CODACTUACION_PRE,PR32NUMPORCPREV," & _
                          "AD02CODDPTO) " & _
                          "VALUES (" & mlngActuacion & _
                          "," & mintCodAct & _
                          mintCodDepa & ")"
              objApp.rdoConnect.Execute strinsert, 64
              objApp.rdoConnect.Execute "Commit", 64
            Else
              'gtblactsel(2, gintIndice) = mlngGrupo
              'Grupos
              strinsert = "INSERT INTO PR1700 (PR16CODGRUPO," & _
                          "PR01CODACTUACION) " & _
                          "VALUES (" & mlngGrupo & "," & mintCodAct & ")"
              objApp.rdoConnect.Execute strinsert, 64
              objApp.rdoConnect.Execute "Commit", 64
            End If
          End If
        End If
      End If
      'gintIndice = gintIndice + 1
      'ReDim Preserve gtblactsel(3, gintIndice)
    Else
      intCont = 0
    End If
    rsta.Close
    Set rsta = Nothing
  Next mintI

  grdDBGrid1(1).SelBookmarks.RemoveAll

  cmdA�adirSel.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    
    Dim strKey As String
    
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
  
    With objMasterInfo
        .strName = "Departamentos"
        Set .objFormContainer = fraFrame1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(1)
        Set .grdGrid = grdDBGrid1(2)
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "AD0200"
        .intAllowance = cwAllowReadOnly
        .strWhere = " AD02FECFIN IS NULL OR AD02FECFIN>=SYSDATE "
        
        .intCursorSize = 0
        
        strKey = .strDataBase & .strTable
                           
        Call .FormAddOrderField("AD02CODDPTO", cwAscending)
        
        'Call .objPrinter.Add("PR001051", "Listado de protocolos")
        'Call .objPrinter.Add("PR001052", "Listado de actuaciones de un protocolo")
        
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Departamentos")
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n", cwString)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo")
        Call .FormAddFilterOrder(strKey, "AD02DESDPTO", "Descripci�n")
  
    End With
  
  
    With objMultiInfo
        .strName = "Actuaciones del departamento"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = fraFrame1(0)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .intAllowance = cwAllowReadOnly
        
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR0201J"
        'If gstrCodAct <> "" Then
        '  .strWhere "PR01CODACTUACION <> " & gstrCodAct
        'End If
        ' s�lo las actuaciones con alguna fase (y la fase con al menos un tipo de recurso)
        .strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion))) AND " _
            & "pr01codactuacion not in (select pr01codactuacion from PR0100 " _
            & " where pr01fecfin < (select sysdate from dual))"
        
        .intCursorSize = 0
        Call .FormAddOrderField("PR01CODACTUACION", cwAscending)
        
        Call .FormAddRelation("AD02CODDPTO", txtText1(0))
 
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Actuaciones del Departamento")
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Departamento", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Departamento")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
    End With

    With objWinInfo

        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del protocolo
        Call .GridAddColumn(objMultiInfo, "C�d. Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Actuaci�n", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Dpto.", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Departamento", "", cwString, 30)
    
    
        Call .FormCreateInfo(objMasterInfo)
    
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnInFind = True

        'Se indica que campos son obligatorios y cuales son clave primaria
        'en el grid que contiene las actuaciones del protocolo
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
 
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True

        'A�adinos las columnas que tienen las descripciones de los c�digos en el grid que
        'contiene las actuaciones del protocolo
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "AD02DESDPTO")

        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), grdDBGrid1(1).Columns(4), "PR01DESCORTA")
    

        Call .WinRegister
        Call .WinStabilize
    End With

    'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Function Hay_Bucle(lngPadre As Long, lngHijo As Long) As Boolean
'JMRL 4/12/97
'Hay_Bucle de vuelve true si al pedir una actuaci�n y �sta tener asociadas, podemos
'llegar al caso de que no pida la misma actuaci�n, y asi indefinidamente

  Dim strSelect As String
  Dim rstBucle As rdoResultset
  Dim blnBucle_Encontrado As Boolean
  Dim lngSobrino As Long
  
  blnBucle_Encontrado = False
  
  strSelect = "SELECT count(*) FROM PR3100 " _
            & " WHERE PR3100.PR01CODACTUACION = " & lngHijo _
            & "   AND PR3100.PR01CODACTUACION_ASO = " & lngPadre
  Set rstBucle = objApp.rdoConnect.OpenResultset(strSelect)
  If rstBucle.rdoColumns(0).Value > 0 Then
    rstBucle.Close
    Set rstBucle = Nothing
    blnBucle_Encontrado = True
  Else
    rstBucle.Close
    Set rstBucle = Nothing
    strSelect = "SELECT PR01CODACTUACION_ASO FROM PR3100 " _
              & " WHERE PR3100.PR01CODACTUACION = " & lngHijo
    Set rstBucle = objApp.rdoConnect.OpenResultset(strSelect)
    Do Until (rstBucle.EOF) Or (blnBucle_Encontrado = True)
       lngSobrino = rstBucle.rdoColumns(0).Value
       blnBucle_Encontrado = Hay_Bucle(lngPadre, lngSobrino)
       rstBucle.MoveNext
    Loop
    rstBucle.Close
    Set rstBucle = Nothing
  End If
  
  If blnBucle_Encontrado = True Then
    Hay_Bucle = True
  Else
    Hay_Bucle = False
  End If
  
End Function



