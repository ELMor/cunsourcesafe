VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmDefProtocolos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Protocolos"
   ClientHeight    =   7905
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11610
   HelpContextID   =   1
   Icon            =   "PR0105.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7905
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "Actuaciones"
      ForeColor       =   &H00800000&
      Height          =   4665
      Left            =   6075
      TabIndex        =   13
      Top             =   2850
      Width           =   5490
      Begin ComctlLib.TreeView tvwPr 
         Height          =   4290
         Left            =   150
         TabIndex        =   14
         Top             =   225
         Width           =   5190
         _ExtentX        =   9155
         _ExtentY        =   7567
         _Version        =   327682
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "imgIcos"
         Appearance      =   1
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Departamentos"
      ForeColor       =   &H00800000&
      Height          =   2340
      Left            =   8475
      TabIndex        =   11
      Top             =   450
      Width           =   3090
      Begin ComctlLib.ListView lvwDpt 
         Height          =   2040
         Left            =   75
         TabIndex        =   12
         Top             =   225
         Width           =   2940
         _ExtentX        =   5186
         _ExtentY        =   3598
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         SmallIcons      =   "imgIcos"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Departamentos"
            Object.Width           =   4233
         EndProperty
      End
   End
   Begin VB.Frame fraAct 
      Caption         =   "Actuaciones del Protocolo"
      ForeColor       =   &H00800000&
      Height          =   4650
      Left            =   75
      TabIndex        =   3
      Top             =   2850
      Width           =   5910
      Begin ComctlLib.ListView lvwPrProt 
         Height          =   4290
         Left            =   150
         TabIndex        =   10
         Top             =   225
         Width           =   5640
         _ExtentX        =   9948
         _ExtentY        =   7567
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327682
         SmallIcons      =   "imgIcos"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Actuaci�n"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   1
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Departamento"
            Object.Width           =   2646
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Protocolo"
      Height          =   2325
      HelpContextID   =   2
      Index           =   0
      Left            =   75
      TabIndex        =   0
      Top             =   450
      Width           =   8385
      Begin ComctlLib.ListView lvwDptoProt 
         Height          =   1965
         Left            =   5325
         TabIndex        =   9
         Top             =   225
         Width           =   2940
         _ExtentX        =   5186
         _ExtentY        =   3466
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   327682
         SmallIcons      =   "imgIcos"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Departamento propietario"
            Object.Width           =   4233
         EndProperty
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   1890
         HelpContextID   =   90001
         Index           =   1
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   285
         Width           =   5100
         _ExtentX        =   8996
         _ExtentY        =   3334
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0105.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cboProt(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0105.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR35DESPROTOCOLO"
            Height          =   330
            Index           =   1
            Left            =   750
            MultiLine       =   -1  'True
            TabIndex        =   2
            Tag             =   "Descripci�n Protocolo|Descripci�n del Protocolo"
            Top             =   600
            Width           =   3900
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR35CODPROTOCOLO"
            Height          =   330
            HelpContextID   =   1
            Index           =   0
            Left            =   225
            TabIndex        =   1
            Tag             =   "C�digo Protocolo"
            Top             =   600
            Width           =   480
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1605
            Index           =   2
            Left            =   -74925
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   75
            Width           =   4650
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   8202
            _ExtentY        =   2831
            _StockProps     =   79
            Caption         =   "PROTOCOLOS"
            ForeColor       =   0
         End
         Begin SSDataWidgets_B.SSDBCombo cboProt 
            DataField       =   "UI01CodUnidad"
            Height          =   315
            Index           =   0
            Left            =   225
            TabIndex        =   16
            Tag             =   "Unidad interdepartamental"
            Top             =   1275
            Width           =   3315
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5715
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5847
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Unidad Interdepartamental:"
            Height          =   255
            Index           =   0
            Left            =   225
            TabIndex        =   15
            Top             =   1050
            Width           =   2445
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Protocolo:"
            Height          =   255
            Index           =   1
            Left            =   225
            TabIndex        =   7
            Top             =   375
            Width           =   1320
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   7620
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.ImageList imgIcos 
      Left            =   0
      Top             =   2475
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   24
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":0044
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":021E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":0538
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":0852
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":0B6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":0E86
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":11A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":14BA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":17D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":1AEE
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":1E08
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":2122
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":243C
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":2756
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":2A70
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":2D8A
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":2F64
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":313E
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":3318
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":34F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":380C
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":3B26
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":3E40
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0105.frx":415A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefProtocolos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRuEBAS                                                    *
'* NOMBRE: PR00105.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 14 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: permite crear nuevos protocolos de actuaciones. Tambi�n *
'*              se puede modificar un protocolo                         *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
Dim nOrden As Integer ' Orden de las pruebas en el protocolo
Dim Dpts() As typeDpt

Private Sub cmdSeleccAct_Click()
  Dim vResp As Variant
  
  If txtText1(0).Text <> "" Then
   objWinInfo.DataSave
   'Load frmSeleccionarActProt
   'frmSeleccionarActProt.Show (vbModal)
   'Unload frmSeleccionarActProt
   'Set frmSeleccionarActProt = Nothing
   Call objsecurity.LaunchProcess("PR0168")
   objWinInfo.DataRefresh
  Else
    vResp = MsgBox("Primero debe seleccionar un protocolo", vbInformation)
  End If
    
End Sub

Private Sub cboProt_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
    Call cboProt_LostFocus(intIndex)

End Sub

Private Sub cboProt_Change(Index As Integer)
      Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboProt_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub

Private Sub cboProt_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim strProtocolo As String
Dim strRestoClausula As String
Dim strKey As String
Dim sql As String
    
    'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  
  With objMasterInfo
    .strName = "Protocolos"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(2)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR3500"
    strKey = .strDataBase & .strTable
    .blnAskPrimary = False
                       
    Call .FormAddOrderField("PR35CODPROTOCOLO", cwAscending)
    
    Call .objPrinter.Add("PR1051", "Listado de protocolos")
    
    'Se establecen los campos por los que se puede filtrar
    Call .FormCreateFilterWhere(strKey, "Protocolos")
    Call .FormAddFilterWhere(strKey, "PR35CODPROTOCOLO", "C�digo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR35DESPROTOCOLO", "Descripci�n", cwString)
  
    'Se establecen los campos por los que se puede ordenar con el filtro
    Call .FormAddFilterOrder(strKey, "PR35CODPROTOCOLO", "C�digo")
    Call .FormAddFilterOrder(strKey, "PR35DESPROTOCOLO", "Descripci�n")
  
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
  
    Call .FormCreateInfo(objMasterInfo)
  
    'Se indican los campos por los que se desea buscar
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnValidate = False
    
    sql = "SELECT UI01CodUnidad, UI01Desig FROM UI0100 WHERE UI01IndActiva = -1 ORDER BY UI01Desig"
    .CtrlGetInfo(cboProt(0)).strsql = sql
  
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Call LlenarPr(tvwPr)
  Call LlenarDpt
End Sub

Sub LlenarDpt()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer

  sql = "SELECT AD02CodDpto, AD02DesDpto FROM AD0200 WHERE " _
      & "AD02CodDpto IN (SELECT AD02CodDpto FROM PR0200) ORDER BY AD02CodDpto"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  i = 0
  While rdo.EOF = False
    i = i + 1
    ReDim Preserve Dpts(1 To i)
    Dpts(i).cDpt = rdo(0)
    Dpts(i).Dpt = rdo(1)
    rdo.MoveNext
  Wend

End Sub

Sub LlenarDptoProt(cProt As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  lvwDptoProt.ListItems.Clear
  sql = "SELECT AD02CodDpto, AD02DesDpto FROM AD0200 WHERE " _
      & "AD02CodDpto IN (SELECT AD02CodDpto FROM PR5800 WHERE PR35CodProtocolo = ?) " _
      & "ORDER BY AD02CodDpto"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cProt
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    lvwDptoProt.ListItems.Add , "D" & rdo(0), rdo(0) & " - " & rdo(1), , hcICONDPT
    rdo.MoveNext
  Wend
End Sub

Sub LlenarPrProt(cProt As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem

  lvwPrProt.ListItems.Clear
  sql = "SELECT PR0100.PR01CodActuacion, PR0100.PR01DesCorta, AD0200.AD02CodDpto, " _
      & "AD0200.AD02DesDpto, PR3600.PR36Orden " _
      & "FROM PR0100, PR3600, AD0200 WHERE " _
      & "PR3600.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR3600.AD02CodDpto = AD0200.AD02CodDpto AND " _
      & "PR3600.PR35CodProtocolo = ? " _
      & "ORDER BY PR3600.PR36Orden"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cProt
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  nOrden = 0
  While rdo.EOF = False
    Set item = lvwPrProt.ListItems.Add(, "P" & rdo(0), rdo(4) & ".- " & rdo(1), , hcICONPR)
    item.SubItems(1) = rdo(3)
    item.Tag = rdo(2) ' cod. del departamento realizador de la actuacion
    nOrden = rdo(4)
    rdo.MoveNext
  Wend
End Sub


Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub lvwDpt_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
'se coloca el DragIcon correcto
  If Source.Name = lvwDpt.Name Then
    If State = 0 Then
      Source.DragIcon = imgIcos.ListImages(Source.SelectedItem.SmallIcon).Picture
    ElseIf State = 1 Then
      Source.DragIcon = imgIcos.ListImages(hcICONPROHIBIDO).Picture
    End If
  End If
End Sub

Private Sub lvwDpt_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim item As ListItem, comp As String
    
  Set lvwDpt.DropHighlight = Nothing
  Set item = lvwDpt.HitTest(X, Y)
    
  On Error Resume Next
  comp = item.Key         'Para comprobar que se haya seleccionado un nodo
  If Err = 0 Then
    If objMasterInfo.rdoCursor.EOF = False And objMasterInfo.rdoCursor.BOF = False Then
      Set lvwDpt.SelectedItem = item
      lvwDpt.Drag
      lvwDpt.DragIcon = imgIcos.ListImages(item.SmallIcon).Picture
    End If
  End If
End Sub

Private Sub lvwDptoProt_DragDrop(Source As Control, X As Single, Y As Single)
Dim cDpt As Long
Dim sql As String
Dim rdoQ As rdoQuery

  cDpt = Mid(Source.SelectedItem.Key, 2)
  sql = "INSERT INTO PR5800 (AD02CodDpto, PR35CodProtocolo) VALUES (?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cDpt
  rdoQ(1) = objMasterInfo.rdoCursor("PR35CodProtocolo")
  On Error Resume Next
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then
    lvwDptoProt.ListItems.Add , Source.SelectedItem.Key, Source.SelectedItem.Text, , hcICONDPT
  Else
    MsgBox "Error al a�adir el departamento al protocolo." & Error, vbExclamation, Me.Caption
  End If
End Sub

Private Sub lvwDptoProt_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
    
  Set lvwDptoProt.DropHighlight = lvwDptoProt.HitTest(X, Y)
    
  If State = 1 Then
    Set lvwDptoProt.DropHighlight = lvwDptoProt.SelectedItem
  End If
  
  Source.DragIcon = imgIcos.ListImages(Source.SelectedItem.SmallIcon).Picture
End Sub

Private Sub lvwPrProt_DblClick()
  Call LlamarCuestionario
End Sub

Private Sub lvwPrProt_DragDrop(Source As Control, X As Single, Y As Single)
Dim cDpt As Long, cPr As Long, cPaq As Long, cProtCopiar  As Long, i As Integer
Dim sql As String
Dim rdoQ As rdoQuery, rdoQOrden As rdoQuery
Dim nodo As Node, nodoPr As Node, item As ListItem, itemPr As ListItem
Dim OrdenPr As Long, cProt As Long, cDptPaq As Long

  Screen.MousePointer = 11
  Set nodo = Source.SelectedItem
  cProt = objMasterInfo.rdoCursor("PR35CodProtocolo")
  Select Case nodo.Image
    Case hcICONPR  ' Se ha arrastrado una prueba
      cDpt = Val(Mid(nodo.Tag, InStr(nodo.Tag, "-") + 1))
      cPr = Val(nodo.Tag)
      sql = "INSERT INTO PR3600 (AD02CodDpto, PR35CodProtocolo, PR01CodActuacion, PR36Orden) " _
          & "VALUES (?, ?, ?, ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cDpt
      rdoQ(1) = cProt
      rdoQ(2) = cPr
      nOrden = nOrden + 1 ' Una nueva prueba
      On Error Resume Next
      Set itemPr = lvwPrProt.DropHighlight
      OrdenPr = Val(itemPr.Text)
      If OrdenPr <> 0 Then ' Si se ha arrastrado sobre una prueba, se modica el orden
        ' Se actualiza el orden del resto de las pruebas
        sql = "UPDATE PR3600 SET PR36Orden = PR36Orden + 1 WHERE " _
            & "PR35CodProtocolo = ? AND " _
            & "PR36Orden >= ?"
        Set rdoQOrden = objApp.rdoConnect.CreateQuery("", sql)
        rdoQOrden(0) = cProt
        rdoQOrden(1) = OrdenPr
        rdoQOrden.Execute
        
        rdoQ(3) = OrdenPr
        rdoQ.Execute
        Call LlenarPrProt(cProt)
              
      Else
        rdoQ(3) = nOrden
        On Error Resume Next
        rdoQ.Execute
        If rdoQ.RowsAffected = 1 Then
          Call LlenarPrProt(cProt)
'          Set item = lvwPrProt.ListItems.Add(, "P" & nodo.Tag, nOrden & ".- " & nodo.Text, , hcICONPR)
'          item.SubItems(1) = Dpts("D" & cDpt)
'          item.Tag = cDpt
        Else
          MsgBox "Error al a�adir la actuaci�n al protocolo." & Error, vbExclamation, Me.Caption
          nOrden = nOrden - 1
        End If
      End If
      
    Case hcICONPAQUETE ' Se ha arrastrado un paquete
      cPaq = Val(nodo.Tag)
      cDptPaq = Val(Mid(nodo.Tag, InStr(nodo.Tag, "-") + 1))
      Set itemPr = lvwPrProt.DropHighlight
      On Error Resume Next
      OrdenPr = Val(itemPr.Text)
      If OrdenPr <> 0 Then ' Si se ha arrastrado sobre una prueba, se modica el orden
        ' Se actualiza el orden del resto de las pruebas
        sql = "UPDATE PR3600 SET PR36Orden = PR36Orden + " & nodo.Children & " WHERE " _
            & "PR35CodProtocolo = ? AND " _
            & "PR36Orden >= ?"
        Set rdoQOrden = objApp.rdoConnect.CreateQuery("", sql)
        rdoQOrden(0) = cProt
        rdoQOrden(1) = OrdenPr
        rdoQOrden.Execute
        nOrden = OrdenPr - 1
      End If
      
      sql = "INSERT INTO PR3600 (AD02CodDpto, PR35CodProtocolo, PR01CodActuacion) " _
          & "(SELECT AD02CodDpto_Rea, " & cProt & ", " _
          & "PR01CodActuacion FROM PR3400 WHERE PR33CodEstandard = ? AND AD02CodDpto = ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cPaq
      rdoQ(1) = cDptPaq
      On Error Resume Next
      rdoQ.Execute
      If rdoQ.RowsAffected > 0 Then
        Set nodoPr = nodo.Child
        For i = 1 To nodo.Children
          cPr = Val(nodoPr.Tag)
          sql = "UPDATE PR3600 SET PR36Orden = ? WHERE " _
              & "PR35CodProtocolo = ? AND " _
              & "PR01CodActuacion = ?"
          Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
          nOrden = nOrden + 1
          rdoQ(0) = nOrden
          rdoQ(1) = cProt
          rdoQ(2) = cPr
          On Error Resume Next
          rdoQ.Execute
          Set nodoPr = nodoPr.Next
        Next i
        Call LlenarPrProt(cProt)
      Else
        MsgBox "Error al a�adir el paquete al protocolo." & Error, vbExclamation, Me.Caption
      End If
    
    Case hcICONPROTOCOLO ' Se ha arrastrado un protocolo
      cProtCopiar = Val(Mid(nodo.Key, 2))
      Set itemPr = lvwPrProt.DropHighlight
      On Error Resume Next
      OrdenPr = Val(itemPr.Text)
      If OrdenPr <> 0 Then ' Si se ha arrastrado sobre una prueba, se modica el orden
        ' Se actualiza el orden del resto de las pruebas
        sql = "UPDATE PR3600 SET PR36Orden = PR36Orden + " & nodo.Children & " WHERE " _
            & "PR35CodProtocolo = ? AND " _
            & "PR36Orden >= ?"
        Set rdoQOrden = objApp.rdoConnect.CreateQuery("", sql)
        rdoQOrden(0) = cProt
        rdoQOrden(1) = OrdenPr
        rdoQOrden.Execute
        nOrden = OrdenPr - 1
      End If
      
      sql = "INSERT INTO PR3600 (AD02CodDpto, PR35CodProtocolo, PR01CodActuacion, PR36Orden) " _
          & "SELECT AD02CodDpto, " & cProt & ", " _
          & "PR01CodActuacion, PR36Orden + " & nOrden & " FROM PR3600 WHERE PR35CodProtocolo = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cProtCopiar
      On Error Resume Next
      rdoQ.Execute
      If rdoQ.RowsAffected > 0 Then
        Call LlenarPrProt(cProt)
      Else
        MsgBox "Error al a�adir el protocolo." & Error, vbExclamation, Me.Caption
      End If

  End Select
  Screen.MousePointer = 0

End Sub

Private Sub lvwPrProt_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
    
  Set lvwPrProt.DropHighlight = lvwPrProt.HitTest(X, Y)
    
  If State = 1 Then
    Set lvwPrProt.DropHighlight = lvwPrProt.SelectedItem
  End If
  
  Source.DragIcon = imgIcos.ListImages(Source.SelectedItem.Image).Picture
End Sub

Private Sub lvwDptoProt_KeyDown(KeyCode As Integer, Shift As Integer)
Dim cDpt As Long, res As Integer
Dim sql As String
Dim rdoQ As rdoQuery

  If KeyCode = 46 Then ' Suprimir
    On Error Resume Next
    cDpt = Mid(lvwDptoProt.SelectedItem.Key, 2)
    If cDpt <> 0 Then
      res = MsgBox("�Desea borrar el departamento '" & lvwDptoProt.SelectedItem.Text & "' del protocolo?", vbQuestion + vbYesNo, Me.Caption)
      If res = vbNo Then Exit Sub
      sql = "DELETE FROM PR5800 WHERE AD02CodDpto = ? AND PR35CodProtocolo = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cDpt
      rdoQ(1) = objMasterInfo.rdoCursor("PR35CodProtocolo")
      rdoQ.Execute
      If rdoQ.RowsAffected = 1 Then
        lvwDptoProt.ListItems.Remove lvwDptoProt.SelectedItem.Index
      Else
        MsgBox "Error al quitar el departamento al protocolo.", vbExclamation, Me.Caption
      End If
    End If
  End If
End Sub

Private Sub lvwPrProt_ItemClick(ByVal item As ComctlLib.ListItem)
  If Not IsNull(objMasterInfo.rdoCursor("UI01CodUnidad")) Then
    tlbToolbar1.Buttons(28).Enabled = True
  End If
End Sub

Private Sub lvwPrProt_KeyDown(KeyCode As Integer, Shift As Integer)
Dim cPr As Long, res As Integer, OrdenPr As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim item As ListItem, itemActualizar As ListItem

  If KeyCode = 46 Then ' Suprimir
    On Error Resume Next
    cPr = Mid(lvwPrProt.SelectedItem.Key, 2)
    If cPr <> 0 Then
      Set item = lvwPrProt.SelectedItem
      res = MsgBox("�Desea borrar la actuaci�n '" & item.Text & "' del protocolo?", vbQuestion + vbYesNo, Me.Caption)
      If res = vbNo Then Exit Sub
      OrdenPr = Val(item.Text)

      objApp.BeginTrans
      On Error Resume Next
' Se borran las preguntas de la prueba
      sql = "DELETE FROM UI0400 WHERE UI03CodGrupo IN (" _
          & "SELECT UI06CodGrupo FROM UI0600 WHERE " _
          & "PR35CodProtocolo = ? AND " _
          & "UI06Cod = ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = objMasterInfo.rdoCursor("PR35CodProtocolo")
      rdoQ(1) = cPr
      rdoQ.Execute
' Se borran los grupos de preguntas de la prueba
      sql = "DELETE FROM UI0600 WHERE PR35CodProtocolo = ? AND UI06Cod = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = objMasterInfo.rdoCursor("PR35CodProtocolo")
      rdoQ(1) = cPr
      rdoQ.Execute
' Se borra la actuaci�n del protocolo
      sql = "DELETE FROM PR3600 WHERE PR01CodActuacion = ? AND PR35CodProtocolo = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cPr
      rdoQ(1) = objMasterInfo.rdoCursor("PR35CodProtocolo")
      rdoQ.Execute
      If rdoQ.RowsAffected > 0 Then
        objApp.CommitTrans
        sql = "UPDATE PR3600 SET PR36Orden = PR36Orden -1 WHERE " _
            & "PR35CodProtocolo = ? AND " _
            & "PR36Orden > ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = objMasterInfo.rdoCursor("PR35CodProtocolo")
        rdoQ(1) = OrdenPr
        rdoQ.Execute
' Si se han actualizado filas, hay que actualizar el orden de las pruebas en pantalla
        If rdoQ.RowsAffected > 0 Then
          Call LlenarPrProt(objMasterInfo.rdoCursor("PR35CodProtocolo"))
        Else
          lvwPrProt.ListItems.Remove item.Index
          nOrden = nOrden - 1
        End If
      Else
        objApp.RollbackTrans
        MsgBox "Error al borrar la prueba del protocolo. " & Error, vbExclamation, Me.Caption
      End If
    End If
  End If

End Sub


Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  If intNewStatus = cwModeSingleAddRest And intOldStatus = cwModeSingleEdit Then
    lvwPrProt.ListItems.Clear
    lvwDptoProt.ListItems.Clear
    fraAct.Enabled = False
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If objMasterInfo.rdoCursor.EOF = False And txtText1(0).Text <> "" Then
    Call LlenarDptoProt(objMasterInfo.rdoCursor("PR35CodProtocolo"))
    Call LlenarPrProt(objMasterInfo.rdoCursor("PR35CodProtocolo"))
  End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  fraAct.Enabled = True
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)

  If strFormName = objMasterInfo.strName Then
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      txtText1(0).Text = fNextClave("PR35CodProtocolo", "PR3500")
      objMasterInfo.rdoCursor("PR35CodProtocolo") = txtText1(0).Text
    End If
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Protocolos" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim item As ListItem
  
  If btnButton.Index <> 28 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Else
    Call LlamarCuestionario
  End If
End Sub
Sub LlamarCuestionario()
Dim item As ListItem
  
  If lvwPrProt.ListItems(lvwPrProt.SelectedItem.Index).Selected = True _
  And Not IsNull(objMasterInfo.rdoCursor("UI01CodUnidad")) Then
    Load frmPregPR
    With frmPregPR
      Set item = lvwPrProt.SelectedItem
      .cProt = objMasterInfo.rdoCursor("PR35CodProtocolo") 'C�d. del protocolo
      .cPr = Mid(item.Key, 2) 'C�d. de la actuaci�n
      .txtPaquete.Text = objMasterInfo.rdoCursor("PR35DesProtocolo") 'Descripci�n del protocolo
      .cDptReali = item.Tag 'Cod. del dpt realizador
      .txtDpt.Text = item.SubItems(1) 'Descripci�n del departamento realizador
      .txtPr.Text = Mid(item.Text, InStr(item.Text, ".-") + 2) 'Descripci�n de la actuaci�n
      .Orden = Val(item.Text) 'Orden de la Act. en el Paquete
      .Ventana = "Protocolo"
      .lblConcepto.Caption = "Protocolo:"
    End With
    frmPregPR.Show vbModal
    Set frmPregPR = Nothing
  End If

End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange

End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(lbllabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



Private Sub tvwPr_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
'se coloca el DragIcon correcto
  If Source.Name = tvwPr.Name Then
    If State = 0 Then
      Source.DragIcon = imgIcos.ListImages(Source.SelectedItem.Image).Picture
    ElseIf State = 1 Then
      Source.DragIcon = imgIcos.ListImages(hcICONPROHIBIDO).Picture
    End If
  End If
End Sub

Private Sub tvwPr_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nodo As Node, comp As String
    
  Set tvwPr.DropHighlight = Nothing
  Set nodo = tvwPr.HitTest(X, Y)
    
  On Error Resume Next
  comp = nodo.Key         'Para comprobar que se haya seleccionado un nodo
  If Err = 0 Then
    If objMasterInfo.rdoCursor.EOF = False And objMasterInfo.rdoCursor.BOF = False Then
      Select Case nodo.Image
        Case hcICONPR  ' Se ha seleccionado una prueba
          Set tvwPr.SelectedItem = nodo
          tvwPr.Drag
          tvwPr.DragIcon = imgIcos.ListImages(nodo.Image).Picture
        Case hcICONPAQUETE ' Se ha seleccionado un paquete
          Set tvwPr.SelectedItem = nodo
          tvwPr.Drag
          tvwPr.DragIcon = imgIcos.ListImages(nodo.Image).Picture
        Case hcICONPROTOCOLO ' Se ha seleccionado un protocolo
          Set tvwPr.SelectedItem = nodo
          tvwPr.Drag
          tvwPr.DragIcon = imgIcos.ListImages(nodo.Image).Picture
      End Select
    End If
  End If
End Sub

Private Sub tvwPr_NodeClick(ByVal Node As ComctlLib.Node)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer, cAct As Integer
Dim nodoDpt As Node, nodoPr As Node
Dim item As Variant
  
  Screen.MousePointer = 11
  Select Case Node.Image
    Case hcICONACTIVIDAD
      If Node.Children = 0 Then
        For i = 1 To UBound(Dpts)
          Set nodoDpt = tvwPr.Nodes.Add(Node.Index, tvwChild, , Dpts(i).cDpt & ".- " & Dpts(i).Dpt, hcICONDPT)
          nodoDpt.Tag = Dpts(i).cDpt
        Next i
      End If
    Case hcICONDPT
      If Node.Children = 0 Then
        cAct = Node.Parent.Tag
        sql = "SELECT PR0100.PR01CodActuacion, PR0100.PR01DesCorta, PR0100.PR01DesCompleta " _
            & "FROM PR0100, PR0200 WHERE " _
            & "PR0100.PR01CodActuacion = PR0200.PR01CodActuacion AND " _
            & "PR0100.PR01FecFin IS NULL AND " _
            & "PR0200.AD02CodDpto = ? AND " _
            & "PR0100.PR12CodActividad = ? " _
            & "ORDER BY PR0100.PR01DesCorta"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = Val(Node.Text)
        rdoQ(1) = cAct
        Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
        While rdo.EOF = False
          Set nodoPr = tvwPr.Nodes.Add(Node.Index, tvwChild, , rdo(1) & IIf(IsNull(rdo(2)), "", " (" & rdo(2) & ")"), hcICONPR)
          nodoPr.Tag = rdo(0) & "-" & Val(Node.Text)
          rdo.MoveNext
        Wend
      End If
  End Select
  Screen.MousePointer = 0


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
    'Se vac�a el grid en el caso de que se pulse abrir
    If intIndex = 0 And objWinInfo.intWinStatus = cwModeSingleOpen Then
        grdDBGrid1(1).RemoveAll
    End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    objWinInfo.CtrlDataChange
End Sub

Public Function Tiene_Actuaciones() As Boolean
'JMRL 4/12/97
'Tiene_Actuaciones es una funci�n que devuelve TRUE si hay ning�n problema a la hora de
'borrar un protocolo

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  
  strmensaje = "El protocolo NO puede se borrado por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
    
      
  'Se comprueba que el paquete no tiene ACTUACIONES
  strSelect = "SELECT COUNT(*) FROM PR3600" & _
              " WHERE PR35CODPROTOCOLO=" & txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� El protocolo TIENE ACTUACIONES." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
  
  If blnSacarMensaje = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Tiene_Actuaciones = True
  Else
    Tiene_Actuaciones = False
  End If
  

End Function




