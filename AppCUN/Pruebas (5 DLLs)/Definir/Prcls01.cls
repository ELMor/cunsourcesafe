VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher DLL Pruebas Definir
' Coded by SIC Donosti
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
'DEFINIR
Const PRRepPaquetes      As String = "PR1011" 'Paquete y sus actuaciones (FrmPaquetes)
Const PRRepActGrupo      As String = "PR1061" 'Actuaciones del grupo (FrmDefGrupos)
Const PRRepCuestGrupo    As String = "PR1062" 'Cuestionario del grupo (FrmDefGrupos)
Const PRRepActDpto       As String = "PR1281" 'Actuaciones de un departamento (FrmDefDptos)
Const PRRepCondAct       As String = "PR1041" 'Condiciones de la actuaci�n (FrmCondiciones)
Const PRRepProtocolos    As String = "PR1051" 'Listado de protocolos (FrmDefProtocolos)
Const PRRepCuestAct      As String = "PR1091" 'Cuestionario de la actuaci�n (FrmCuestActuacion)
Const PRRepIntConAct     As String = "PR1121" 'Interacciones con otras actuaciones (FrmInterConAct)
Const PRRepIntConGrp     As String = "PR1131" 'Interacciones con otros grupos (FrmInterConGrp)
Const PRRepIntActAct     As String = "PR1151" 'Interacciones entre actuaciones (FrmInterActAct)
Const PRRepIntActGrp     As String = "PR1161" 'Interacciones actuaciones-grupo (FrmInterActGrp)
Const PRRepIntGrpAct     As String = "PR1171" 'Interacciones grupo-actuaci�n (FrmInterGrpAct)
Const PRRepIntGrpGrp     As String = "PR1181" 'Interacciones grupo-grupo (FrmInterGrpGrp)
Const PRRepDefFases      As String = "PR1231" 'Fases de una actuaci�n (FrmDefFases)
Const PRRepDefTipRec     As String = "PR1241" 'Tipos de recurso de una actuaci�n (FrmDefTiposRecurso)
Const PRRepMuestra       As String = "PR1021" 'Muestras de la actuaci�n (FrmMuestra)
Const PRRepFaseSimple    As String = "PR1361" 'Tipos de recurso (FrmFaseSimple)
Const PRRepDefActCUN     As String = "PR1221" 'Actuaciones de la CUN (FrmDefActuacionesCUN)
Const PRRepDptoAct       As String = "PR1222" 'Actuaciones de la CUN, por dpto (FrmDefActuacionesCUN)
Const PRRepTipoMuest     As String = "PR2301" 'Tipos de muestra (FrmTiposDeMuestra)

'Definir actuaciones y agrupaciones de actuaciones
Const PRWinPaquetes                As String = "PR0101"
Const PRWinMuestra                 As String = "PR0102"
Const PRWinCuestionarioMuestra     As String = "PR0103"
Const PRWinCondiciones             As String = "PR0104"
Const PRWinDefProtocolos           As String = "PR0105"
Const PRWinDefGrupos               As String = "PR0106"
Const PRWinActAsociadas            As String = "PR0107"
Const PRWinActPrevisibles          As String = "PR0108"
Const PRWinCuestActuacion          As String = "PR0109"
Const PRWinCuestGrupo              As String = "PR0110"
Const PRWinMenuInterac2            As String = "PR0111"
Const PRWinInterconAct             As String = "PR0112"
Const PRWinInterconGrp             As String = "PR0113"
Const PRWinMenuInterac             As String = "PR0114"
Const PRWinInterActAct             As String = "PR0115"
Const PRWinInterActGrp             As String = "PR0116"
Const PRWinInterGrpAct             As String = "PR0117"
Const PRWinInterGrpGrp             As String = "PR0118"
Const PRWinDefActuacionesCun       As String = "PR0122"
Const PRWinDefFases                As String = "PR0123"
Const PRWinDefTiposRecurso         As String = "PR0124"
Const PRWinDefRecursoValido        As String = "PR0126"
Const PRWinDefDptos                As String = "PR0128"
Const PRWinInternoGen              As String = "PR0130"
Const PRWinFaseSimple              As String = "PR0136"
Const PRWinSelGrupos               As String = "PR0160"
Const PRWinSeleccionarActPaq       As String = "PR0162"
Const PRWinSelPaquetes             As String = "PR0163"
Const PRWinSelProtocolos           As String = "PR0164"
Const PRWinSelDptos                As String = "PR0165"
Const PRWinSelDptosGrupo           As String = "PR0166"
Const PRWinSeleccionarActPrev      As String = "PR0167"
Const PRWinSeleccionarActProt      As String = "PR0168"
Const PRWinSeleccionarActGrupo     As String = "PR0169"
Const PRWinSeleccionarActAsoc      As String = "PR0170"
Const PRWincuestpropiomuestra      As String = "PR0225"
Const PRWinTiposdeMuestra          As String = "PR0230"
Const PRWincuestionariopaquete     As String = "PR0401"


' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
'DEFINIR
  Select Case strProcess
    
    Case PRWinPaquetes
      Load frmpaquetes
      Call objsecurity.AddHelpContext(528)
      Call frmpaquetes.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmpaquetes
      Set frmpaquetes = Nothing
    Case PRWinMuestra
      Load frmmuestra
      Call objsecurity.AddHelpContext(520)
      Call frmmuestra.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmmuestra
      Set frmmuestra = Nothing
    Case PRWinCuestionarioMuestra
      Load frmcuestionariomuestra
      Call objsecurity.AddHelpContext(521)
      Call frmcuestionariomuestra.Show(vbModal)
      Unload frmcuestionariomuestra
      Call objsecurity.RemoveHelpContext
      Set frmcuestionariomuestra = Nothing
    Case PRWinCondiciones
      Load frmCondiciones
      Call objsecurity.AddHelpContext(516)
      Call frmCondiciones.Show(vbModal)
      Unload frmCondiciones
      Call objsecurity.RemoveHelpContext
      Set frmCondiciones = Nothing
    Case PRWinDefProtocolos
      Load frmDefProtocolos
      Call objsecurity.AddHelpContext(529)
      Call frmDefProtocolos.Show(vbModal)
      Unload frmDefProtocolos
      Call objsecurity.RemoveHelpContext
      Set frmDefProtocolos = Nothing
    Case PRWinDefGrupos
      Load frmDefGrupos
      Call objsecurity.AddHelpContext(526)
      Call frmDefGrupos.Show(vbModal)
      Unload frmDefGrupos
      Call objsecurity.RemoveHelpContext
      Set frmDefGrupos = Nothing
    Case PRWinActAsociadas
      Load frmActAsociadas
      Call objsecurity.AddHelpContext(524)
      Call frmActAsociadas.Show(vbModal)
      Unload frmActAsociadas
      Call objsecurity.RemoveHelpContext
      Set frmActAsociadas = Nothing
    Case PRWinActPrevisibles
      Load frmActPrevisibles
      Call objsecurity.AddHelpContext(523)
      Call frmActPrevisibles.Show(vbModal)
      Unload frmActPrevisibles
      Call objsecurity.RemoveHelpContext
      Set frmActPrevisibles = Nothing
    Case PRWinCuestActuacion
      Load frmCuestActuacion
      Call objsecurity.AddHelpContext(522)
      Call frmCuestActuacion.Show(vbModal)
      Unload frmCuestActuacion
      Call objsecurity.RemoveHelpContext
      Set frmCuestActuacion = Nothing
    Case PRWinCuestGrupo
      Load frmCuestGrupo
      Call objsecurity.AddHelpContext(527)
      Call frmCuestGrupo.Show(vbModal)
      Unload frmCuestGrupo
      Call objsecurity.RemoveHelpContext
      Set frmCuestGrupo = Nothing
    Case PRWinMenuInterac2
      Load frmmenuinterac2
      Call objsecurity.AddHelpContext(517)
      Call frmmenuinterac2.Show(vbModal)
      Unload frmmenuinterac2
      Call objsecurity.RemoveHelpContext
      Set frmmenuinterac2 = Nothing
    Case PRWinInterconAct
      Load frminterconact
      Call objsecurity.AddHelpContext(518)
      Call frminterconact.Show(vbModal)
      Unload frminterconact
      Call objsecurity.RemoveHelpContext
      Set frminterconact = Nothing
    Case PRWinInterconGrp
      Load frmintercongrp
      Call objsecurity.AddHelpContext(519)
      Call frmintercongrp.Show(vbModal)
      Unload frmintercongrp
      Call objsecurity.RemoveHelpContext
      Set frmintercongrp = Nothing
    Case PRWinMenuInterac
      Load frmmenuinterac
      Call objsecurity.AddHelpContext(517)
      Call frmmenuinterac.Show(vbModal)
      Unload frmmenuinterac
      Call objsecurity.RemoveHelpContext
      Set frmmenuinterac = Nothing
    Case PRWinInterActAct
      Load frminteractact
      Call objsecurity.AddHelpContext(517)
      Call frminteractact.Show(vbModal)
      Unload frminteractact
      Call objsecurity.RemoveHelpContext
      Set frminteractact = Nothing
    Case PRWinInterActGrp
      Load frminteractgrp
      Call objsecurity.AddHelpContext(517)
      Call frminteractgrp.Show(vbModal)
      Unload frminteractgrp
      Call objsecurity.RemoveHelpContext
      Set frminteractgrp = Nothing
    Case PRWinInterGrpAct
      Load frmintergrpact
      Call objsecurity.AddHelpContext(517)
      Call frmintergrpact.Show(vbModal)
      Unload frmintergrpact
      Call objsecurity.RemoveHelpContext
      Set frmintergrpact = Nothing
    Case PRWinInterGrpGrp
      Load frmintergrpgrp
      Call objsecurity.AddHelpContext(517)
      Call frmintergrpgrp.Show(vbModal)
      Unload frmintergrpgrp
      Call objsecurity.RemoveHelpContext
      Set frmintergrpgrp = Nothing
    Case PRWinDefActuacionesCun
      Load frmdefactuacionesCUN
      Call objsecurity.AddHelpContext(511)
      Call frmdefactuacionesCUN.Show(vbModal)
      Unload frmdefactuacionesCUN
      Call objsecurity.RemoveHelpContext
      Set frmdefactuacionesCUN = Nothing
    Case PRWinDefFases
      Load frmdeffases
      Call objsecurity.AddHelpContext(512)
      Call frmdeffases.Show(vbModal)
      Unload frmdeffases
      Call objsecurity.RemoveHelpContext
      Set frmdeffases = Nothing
    Case PRWinDefTiposRecurso
      Load frmdeftiposrecurso
      Call objsecurity.AddHelpContext(513)
      Call frmdeftiposrecurso.Show(vbModal)
      Unload frmdeftiposrecurso
      Set frmdeftiposrecurso = Nothing
    Case PRWinDefRecursoValido
      Load frmdefrecursovalido
      Call objsecurity.AddHelpContext(514)
      Call frmdefrecursovalido.Show(vbModal)
      Unload frmdefrecursovalido
      Call objsecurity.RemoveHelpContext
      Set frmdefrecursovalido = Nothing
    Case PRWinDefDptos
      Load frmdefdptos
      Call objsecurity.AddHelpContext(515)
      Call frmdefdptos.Show(vbModal)
      Unload frmdefdptos
      Call objsecurity.RemoveHelpContext
      Set frmdefdptos = Nothing
    Case PRWinInternoGen
      Load frminternogen
      Call objsecurity.AddHelpContext(517)
      Call frminternogen.Show(vbModal)
      Unload frminternogen
      Call objsecurity.RemoveHelpContext
      Set frminternogen = Nothing
    Case PRWinFaseSimple
      Load frmfasesimple
      Call objsecurity.AddHelpContext(512)
      Call frmfasesimple.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmfasesimple
      Set frmfasesimple = Nothing
    Case PRWinSelGrupos
      Load frmSelGrupos
      Call objsecurity.AddHelpContext(538)
      Call frmSelGrupos.Show(vbModal)
      Unload frmSelGrupos
      Call objsecurity.RemoveHelpContext
      Set frmSelGrupos = Nothing
    Case PRWinSeleccionarActPaq
      Load frmSeleccionarActPaq
      Call objsecurity.AddHelpContext(532)
      Call frmSeleccionarActPaq.Show(vbModal)
      Unload frmSeleccionarActPaq
      Call objsecurity.RemoveHelpContext
      Set frmSeleccionarActPaq = Nothing
    Case PRWinSelPaquetes
      Load frmSelPaquetes
      Call objsecurity.AddHelpContext(535)
      Call frmSelPaquetes.Show(vbModal)
      Unload frmSelPaquetes
      Call objsecurity.RemoveHelpContext
      Set frmSelPaquetes = Nothing
    Case PRWinSelProtocolos
      Load frmSelProtocolos
      Call objsecurity.AddHelpContext(536)
      Call frmSelProtocolos.Show(vbModal)
      Unload frmSelProtocolos
      Call objsecurity.RemoveHelpContext
      Set frmSelProtocolos = Nothing
    Case PRWinSelDptos
      Load frmSelDptos
      Call objsecurity.AddHelpContext(533)
      Call frmSelDptos.Show(vbModal)
      Unload frmSelDptos
      Call objsecurity.RemoveHelpContext
      Set frmSelDptos = Nothing
    Case PRWinSelDptosGrupo
      Load frmseldptosgrupo
      Call objsecurity.AddHelpContext(538)
      Call frmseldptosgrupo.Show(vbModal)
      Unload frmseldptosgrupo
      Call objsecurity.RemoveHelpContext
      Set frmseldptosgrupo = Nothing
    Case PRWinSeleccionarActPrev
      Load frmSeleccionarActPrev
      Call objsecurity.AddHelpContext(532)
      Call frmSeleccionarActPrev.Show(vbModal)
      Unload frmSeleccionarActPrev
      Call objsecurity.RemoveHelpContext
      Set frmSeleccionarActPrev = Nothing
    Case PRWinSeleccionarActProt
      Load frmSeleccionarActProt
      Call objsecurity.AddHelpContext(532)
      Call frmSeleccionarActProt.Show(vbModal)
      Unload frmSeleccionarActProt
      Call objsecurity.RemoveHelpContext
      Set frmSeleccionarActProt = Nothing
    Case PRWinSeleccionarActGrupo
      Load frmSeleccionarActGrupo
      Call objsecurity.AddHelpContext(532)
      Call frmSeleccionarActGrupo.Show(vbModal)
      Unload frmSeleccionarActGrupo
      Call objsecurity.RemoveHelpContext
      Set frmSeleccionarActGrupo = Nothing
    Case PRWincuestpropiomuestra
      Load frmcuestpropiomuestra
      'Call objsecurity.AddHelpContext(538)
      Call frmcuestpropiomuestra.Show(vbModal)
      Unload frmcuestpropiomuestra
      'Call objsecurity.RemoveHelpContext
      Set frmcuestpropiomuestra = Nothing
    Case PRWinSeleccionarActAsoc
      Load frmSeleccionarActAsoc
      Call objsecurity.AddHelpContext(532)
      Call frmSeleccionarActAsoc.Show(vbModal)
      Unload frmSeleccionarActAsoc
      Call objsecurity.RemoveHelpContext
      Set frmSeleccionarActAsoc = Nothing
    Case PRWinTiposdeMuestra
      Load frmtiposdemuestra
      Call objsecurity.AddHelpContext(503)
      Call frmtiposdemuestra.Show(vbModal)
      Unload frmtiposdemuestra
      Call objsecurity.RemoveHelpContext
      Set frmtiposdemuestra = Nothing
      
    Case PRWincuestionariopaquete
      frmPregPR.Show vbModal
'      Load frmcuestionariopaquete
'      'Call objsecurity.AddHelpContext(521)
'      Call frmcuestionariopaquete.Show(vbModal)
'      Unload frmcuestionariopaquete
'      'Call objsecurity.RemoveHelpContext
'      Set frmcuestionariopaquete = Nothing
      
  End Select
  Call Err.Clear
End Function

Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 58 procesos
  ReDim aProcess(1 To 62, 1 To 4) As Variant
      
  ' VENTANAS
  'DEFINIR
  
  aProcess(1, 1) = PRWinPaquetes
  aProcess(1, 2) = "Definici�n de Paquetes"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinMuestra
  aProcess(2, 2) = "Definici�n de Muestras"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
      
  aProcess(3, 1) = PRWinCuestionarioMuestra
  aProcess(3, 2) = "Cuestionario de la Muestra"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinCondiciones
  aProcess(4, 2) = "Condiciones"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
    
  aProcess(5, 1) = PRWinDefProtocolos
  aProcess(5, 2) = "Definici�n de Protocolos"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = PRWinDefGrupos
  aProcess(6, 2) = "Definici�n de Grupos"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinActAsociadas
  aProcess(7, 2) = "Actuaciones Asociadas"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = PRWinActPrevisibles
  aProcess(8, 2) = "Actuaciones Previsibles"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinCuestActuacion
  aProcess(9, 2) = "Cuestionario de la Actuaci�n"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinCuestGrupo
  aProcess(10, 2) = "Cuestionario del Grupo"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = PRWinMenuInterac2
  aProcess(11, 2) = "Men� de Interacciones 2"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = PRWinInterconAct
  aProcess(12, 2) = "Interacci�n de Actuaciones"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWinInterconGrp
  aProcess(13, 2) = "Interacci�n de Grupos"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow
      
  aProcess(14, 1) = PRWinMenuInterac
  aProcess(14, 2) = "Definici�n de Interacciones"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
  
  aProcess(15, 1) = PRWinInterActAct
  aProcess(15, 2) = "Interacci�n Act-Act"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = PRWinInterActGrp
  aProcess(16, 2) = "Interacci�n Act-Grupo"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeWindow
  
  aProcess(17, 1) = PRWinInterGrpAct
  aProcess(17, 2) = "Interacci�n Grupo-Act"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeWindow
  
  aProcess(18, 1) = PRWinInterGrpGrp
  aProcess(18, 2) = "Interacci�n Grupo-Grupo"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = PRWinDefActuacionesCun
  aProcess(19, 2) = "Definici�n de Actuaciones"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = PRWinDefFases
  aProcess(20, 2) = "Definici�n de Fases"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = PRWinDefTiposRecurso
  aProcess(21, 2) = "Def de Tipos de Recurso"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow
  
  aProcess(22, 1) = PRWinDefRecursoValido
  aProcess(22, 2) = "Def de tipos de Recurso V�lido"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeWindow
  
  aProcess(23, 1) = PRWinDefDptos
  aProcess(23, 2) = "Definici�n de Departamentos"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeWindow
      
  aProcess(24, 1) = PRWinInternoGen
  aProcess(24, 2) = "Generador Interno"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeWindow
  
  aProcess(25, 1) = PRWinFaseSimple
  aProcess(25, 2) = "Definir Fase Simple"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeWindow
      
  aProcess(26, 1) = PRWinSelGrupos
  aProcess(26, 2) = "Selecci�n de Grupos"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeWindow
      
  aProcess(27, 1) = PRWinSeleccionarActPaq
  aProcess(27, 2) = "Selecci�n de Actuaciones Paq"
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeWindow
      
  aProcess(28, 1) = PRWinSelPaquetes
  aProcess(28, 2) = "Selecci�n de Paquetes"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeWindow
      
  aProcess(29, 1) = PRWinSelProtocolos
  aProcess(29, 2) = "Selecci�n de Protocolos"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeWindow
      
  aProcess(30, 1) = PRWinSelDptos
  aProcess(30, 2) = "Selecci�n de Departamentos"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeWindow
      
  aProcess(31, 1) = PRWinSelDptosGrupo
  aProcess(31, 2) = "Selecci�n de Dptos. Grupo"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeWindow
      
  aProcess(32, 1) = PRWinSeleccionarActPrev
  aProcess(32, 2) = "Selecci�n de Actuaciones Prev"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeWindow
      
  aProcess(33, 1) = PRWinSeleccionarActProt
  aProcess(33, 2) = "Selecci�n de Actuaciones Prot"
  aProcess(33, 3) = False
  aProcess(33, 4) = cwTypeWindow
      
  aProcess(34, 1) = PRWinSeleccionarActGrupo
  aProcess(34, 2) = "Selecci�n de Actuaciones Grupo"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeWindow
      
  aProcess(35, 1) = PRWinSeleccionarActAsoc
  aProcess(35, 2) = "Selecci�n de Actuaciones Asoc"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeWindow

  aProcess(36, 1) = PRWincuestpropiomuestra
  aProcess(36, 2) = "Cuestionario propio muestra"
  aProcess(36, 3) = False
  aProcess(36, 4) = cwTypeWindow
  
  aProcess(37, 1) = PRWinTiposdeMuestra
  aProcess(37, 2) = "Tipos de Muestra"
  aProcess(37, 3) = False
  aProcess(37, 4) = cwTypeWindow
  
  aProcess(62, 1) = PRWincuestionariopaquete
  aProcess(62, 2) = "Cuestionario de la Actuaci�n-Paquete"
  aProcess(62, 3) = False
  aProcess(62, 4) = cwTypeWindow
  
'LISTADOS

  aProcess(41, 1) = PRRepActDpto
  aProcess(41, 2) = "Actuaciones de un departamento"
  aProcess(41, 3) = False
  aProcess(41, 4) = cwTypeReport
  
  aProcess(42, 1) = PRRepCondAct
  aProcess(42, 2) = "Condiciones de la actuaci�n"
  aProcess(42, 3) = False
  aProcess(42, 4) = cwTypeReport
  
  aProcess(43, 1) = PRRepProtocolos
  aProcess(43, 2) = "Actuaciones del protocolo"
  aProcess(43, 3) = False
  aProcess(43, 4) = cwTypeReport
  
  aProcess(44, 1) = PRRepCuestAct
  aProcess(44, 2) = "Cuestionario de la actuaci�n"
  aProcess(44, 3) = False
  aProcess(44, 4) = cwTypeReport
  
  aProcess(45, 1) = PRRepIntConAct
  aProcess(45, 2) = "Interacciones con otras actuaciones"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeReport
  
  aProcess(46, 1) = PRRepIntConGrp
  aProcess(46, 2) = "interacciones con grupos"
  aProcess(46, 3) = False
  aProcess(46, 4) = cwTypeReport
  
  aProcess(47, 1) = PRRepIntActAct
  aProcess(47, 2) = "Interacciones entre actuaciones"
  aProcess(47, 3) = False
  aProcess(47, 4) = cwTypeReport
  
  aProcess(48, 1) = PRRepIntActGrp
  aProcess(48, 2) = "Interacciones actuaci�n-grupo"
  aProcess(48, 3) = False
  aProcess(48, 4) = cwTypeReport
  
  aProcess(49, 1) = PRRepIntGrpAct
  aProcess(49, 2) = "Interaciones grupo-actuaci�n"
  aProcess(49, 3) = False
  aProcess(49, 4) = cwTypeReport
  
  aProcess(50, 1) = PRRepIntGrpGrp
  aProcess(50, 2) = "Interaciones grupo-grupo"
  aProcess(50, 3) = False
  aProcess(50, 4) = cwTypeReport
  
  aProcess(51, 1) = PRRepDefFases
  aProcess(51, 2) = "Fases de una actuaci�n"
  aProcess(51, 3) = False
  aProcess(51, 4) = cwTypeReport

  aProcess(52, 1) = PRRepFaseSimple
  aProcess(52, 2) = "Tipos de recurso de la actuaci�n"
  aProcess(52, 3) = False
  aProcess(52, 4) = cwTypeReport
  
  aProcess(53, 1) = PRRepDefActCUN
  aProcess(53, 2) = "Actuaciones de C.U.N."
  aProcess(53, 3) = False
  aProcess(53, 4) = cwTypeReport
  
  aProcess(54, 1) = PRRepDptoAct
  aProcess(54, 2) = "Actuaciones de la C.U.N. (Por departamento)"
  aProcess(54, 3) = False
  aProcess(54, 4) = cwTypeReport
  
  aProcess(56, 1) = PRRepPaquetes
  aProcess(56, 2) = "Actuaciones del paquete"
  aProcess(56, 3) = False
  aProcess(56, 4) = cwTypeReport
  
  aProcess(57, 1) = PRRepActGrupo
  aProcess(57, 2) = "Actuaciones del grupo"
  aProcess(57, 3) = False
  aProcess(57, 4) = cwTypeReport
  
  aProcess(58, 1) = PRRepCuestGrupo
  aProcess(58, 2) = "Cuestionario del grupo"
  aProcess(58, 3) = False
  aProcess(58, 4) = cwTypeReport
  
  aProcess(59, 1) = PRRepDefTipRec
  aProcess(59, 2) = "Tipos de recurso de una actuaci�n"
  aProcess(59, 3) = False
  aProcess(59, 4) = cwTypeReport

  aProcess(60, 1) = PRRepMuestra
  aProcess(60, 2) = "Muetras de una actuaci�n"
  aProcess(60, 3) = False
  aProcess(60, 4) = cwTypeReport

  aProcess(61, 1) = PRRepTipoMuest
  aProcess(61, 2) = "Tipos de muestra"
  aProcess(61, 3) = False
  aProcess(61, 4) = cwTypeReport

  
End Sub
