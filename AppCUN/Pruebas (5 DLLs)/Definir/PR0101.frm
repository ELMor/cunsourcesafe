VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmPaquetes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Paquetes"
   ClientHeight    =   8220
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11400
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8220
   ScaleWidth      =   11400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraAct 
      Caption         =   "Actuaciones del Paquete"
      ForeColor       =   &H00800000&
      Height          =   5100
      Left            =   75
      TabIndex        =   12
      Top             =   2700
      Width           =   5910
      Begin ComctlLib.ListView lvwPrPaq 
         Height          =   4815
         Left            =   150
         TabIndex        =   13
         Top             =   225
         Width           =   5640
         _ExtentX        =   9948
         _ExtentY        =   8493
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327682
         SmallIcons      =   "imgIcos"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Actuaci�n"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   1
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Departamento"
            Object.Width           =   2646
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Actuaciones"
      ForeColor       =   &H00800000&
      Height          =   5115
      Left            =   6075
      TabIndex        =   10
      Top             =   2700
      Width           =   5190
      Begin ComctlLib.TreeView tvwPr 
         Height          =   4815
         Left            =   150
         TabIndex        =   11
         Top             =   225
         Width           =   4890
         _ExtentX        =   8625
         _ExtentY        =   8493
         _Version        =   327682
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "imgIcos"
         Appearance      =   1
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Paquete"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      Index           =   0
      Left            =   75
      TabIndex        =   0
      Top             =   480
      Width           =   11220
      Begin TabDlg.SSTab tabTab1 
         Height          =   1650
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   10785
         _ExtentX        =   19024
         _ExtentY        =   2910
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0101.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblPaquete(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblPaquete(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chkCheck1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(3)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0101.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            Height          =   330
            HelpContextID   =   40101
            Index           =   3
            Left            =   975
            TabIndex        =   15
            Tag             =   "Departamento|Departamento propietario del paquete"
            Top             =   375
            Width           =   3690
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD02CodDpto"
            Height          =   330
            HelpContextID   =   40101
            Index           =   2
            Left            =   300
            TabIndex        =   14
            Tag             =   "C�d. Departamento|C�digo del departamento"
            Top             =   375
            Width           =   612
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Selecci�n Autom�tica"
            DataField       =   "PR33INDSELAUTOMA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   6525
            TabIndex        =   3
            Tag             =   "Selecci�n Autom�tica"
            Top             =   450
            Width           =   2265
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR33DESESTANDARD"
            Height          =   330
            Index           =   1
            Left            =   300
            TabIndex        =   2
            Tag             =   "Descripci�n Paquete|Descripci�n del Paquete"
            Top             =   1110
            Width           =   8475
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR33CODESTANDARD"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   1425
            TabIndex        =   1
            Tag             =   "C�d. Paquete|C�digo Paquete"
            Top             =   825
            Visible         =   0   'False
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1440
            Index           =   0
            Left            =   -74880
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   75
            Width           =   10335
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18230
            _ExtentY        =   2540
            _StockProps     =   79
            Caption         =   "PAQUETES"
            ForeColor       =   0
         End
         Begin VB.Label lblPaquete 
            Caption         =   "Departamento:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   300
            TabIndex        =   8
            Top             =   90
            Width           =   1215
         End
         Begin VB.Label lblPaquete 
            Caption         =   "Paquete:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   300
            TabIndex        =   7
            Top             =   825
            Width           =   2055
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   7935
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.ImageList imgIcos 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   24
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":0038
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":0212
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":052C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":0846
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":0B60
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":0E7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":1194
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":14AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":17C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":1AE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":1DFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":2116
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":2430
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":274A
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":2A64
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":2D7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":2F58
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":3132
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":330C
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":34E6
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":3800
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":3B1A
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":3E34
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0101.frx":414E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPaquetes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00101.FRM                                                  *
'* AUTOR: JESUS MARIA RODILLA LARA                                      *
'* FECHA: 4 DE AGOSTO DE 1997                                           *
'* DESCRIPCION: permite crear nuevos paquetes de actuaciones. Tambi�n se*
'*              puede modificar un paquete                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'*  JUAN RODRIGUEZ 28-12-1998 A�ADIR UNIDADES INTERDEPARTAMENTALES      *
'************************************************************************

Option Explicit
'Dim mblnInicio As Integer
'Dim mblnesta_lleno As Integer
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
Dim Dpts() As typeDpt
Dim nOrden As Integer

Private Sub LlamarCuestionario()
Dim item As ListItem
  
  If lvwPrPaq.ListItems(lvwPrPaq.SelectedItem.Index).Selected = True _
  And Not IsNull(objMasterInfo.rdoCursor("UI01CodUnidad")) Then
    Load frmPregPR
    With frmPregPR
      .cPaq = txtText1(0).Text 'C�d. del paquete
      .cPr = grdDBGrid1(2).Columns(3).Value 'C�d. de la actuaci�n
      .txtPaquete.Text = txtText1(1).Text 'Descripci�n del paquete
      .cDptPaq = txtText1(2).Text 'Cod. del dpt del paquete
      .cDptReali = grdDBGrid1(2).Columns(5).Value 'Cod. del dpt realizador
      .txtDpt.Text = grdDBGrid1(2).Columns(6).Text 'Descripci�n del departamento realizador
      .txtPr.Text = grdDBGrid1(2).Columns(4).Value 'Descripci�n de la actuaci�n
      .Orden = grdDBGrid1(2).Columns(11).Value 'Orden de la Act. en el Paquete
      .Ventana = "Paquete"
      .lblConcepto.Caption = "Paquete:"
    End With
    frmPregPR.Show vbModal
  End If
End Sub

Private Sub cboPaq_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
    Call cboPaq_LostFocus(intIndex)

End Sub

Private Sub cboPaq_Change(Index As Integer)
      Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboPaq_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub

Private Sub cboPaq_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim strKey As String, sql As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, Me, tlbToolbar1, stbStatusBar1, cwWithAll)

  With objMasterInfo
    .strName = "Paquetes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR3300"
    strKey = .strTable
    Call .FormAddOrderField("PR33CODESTANDARD", cwAscending)
    
    Call .objPrinter.Add("PR1011", "Listado de paquetes")
    
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    
    'Se establecen los campos por los que se puede filtrar
    Call .FormCreateFilterWhere(strKey, "Paquetes")
    Call .FormAddFilterWhere(strKey, "PR33CODESTANDARD", "C�digo Paquete", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR33DESESTANDARD", "Descripci�n Paquete", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Departamento", cwNumeric)
    
    'Se establecen los campos por los que se puede ordenar con el filtro
    Call .FormAddFilterOrder(strKey, "PR33CODESTANDARD", "C�digo Paquete")
    Call .FormAddFilterOrder(strKey, "PR33DESESTANDARD", "Descripci�n Paquete")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Departamento")
    
  End With
  

  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    'Se establecen los campos por los que se puede hacer una busqueda
    .CtrlGetInfo(txtText1(0)).blnInFind = True 'Codigo paquete
    .CtrlGetInfo(txtText1(1)).blnInFind = True 'Descipci�n del paquete
    .CtrlGetInfo(txtText1(2)).blnInFind = True 'Departamento
'    .CtrlGetInfo(txtText1(0)).blnValidate = False
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CodDpto", "SELECT AD02DesDpto FROM AD0200 WHERE AD02CodDpto=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "AD02DesDpto")
    
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Call LlenarPr(tvwPr)
  Call LlenarDpt
End Sub

Sub LlenarDpt()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer

  sql = "SELECT AD02CodDpto, AD02DesDpto FROM AD0200 WHERE " _
      & "AD02CodDpto IN (SELECT AD02CodDpto FROM PR0200) ORDER BY AD02CodDpto"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  i = 0
  While rdo.EOF = False
    i = i + 1
    ReDim Preserve Dpts(1 To i)
    Dpts(i).cDpt = rdo(0)
    Dpts(i).Dpt = rdo(1)
    rdo.MoveNext
  Wend

End Sub
Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChanged(ByVal strFormName As String)
  MsgBox "objWinInfo_cwPostChanged:  " & strFormName
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  MsgBox "objWinInfo_cwPostChangeForm: " & strFormName
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  If intNewStatus = cwModeSingleAddKey And intOldStatus = cwModeSingleEdit Then
    lvwPrPaq.ListItems.Clear
    fraAct.Enabled = False
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If objMasterInfo.rdoCursor.EOF = False And txtText1(0).Text <> "" Then
    Call LlenarPrPaq(objMasterInfo.rdoCursor("PR33CodEstandard"), objMasterInfo.rdoCursor("AD02CodDpto"))
  End If
End Sub

Private Sub objWinInfo_cwPostReadLine(ByVal strFormName As String)
  If objMasterInfo.rdoCursor.EOF = False And txtText1(0).Text <> "" Then
    Call LlenarPrPaq(objMasterInfo.rdoCursor("PR33CodEstandard"), objMasterInfo.rdoCursor("AD02CodDpto"))
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  fraAct.Enabled = True
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Paquetes" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
      .strOrder = "ORDER BY AD02CODDPTO ASC"
      
      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�d. Dpto."
      
      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Desc. Dpto."
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
        'Call objWinInfo.CtrlSet(txtDesDpto, .cllValues("AD02DESDPTO"))
      End If
      
    End With
    Set objSearch = Nothing
  End If

  If strFormName = "Paquetes" And strCtrl = "txtText1(3)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "UI0100"
      .strOrder = "ORDER BY UI01CODUNIDAD ASC"
      
      Set objField = .AddField("UI01CODUNIDAD")
      objField.strSmallDesc = "Unidad Interdepartamental"
      
      Set objField = .AddField("UI01DESIG")
      objField.strSmallDesc = "Desc. Uni. Interdep."
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(3), .cllValues("UI01CODUNIDAD"))
      End If
      
    End With
    Set objSearch = Nothing
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
 
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblPquete_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblPaquete(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub tvwPr_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
'se coloca el DragIcon correcto
  If Source.Name = tvwPr.Name Then
    If State = 0 Then
      Source.DragIcon = imgIcos.ListImages(Source.SelectedItem.Image).Picture
    ElseIf State = 1 Then
      Source.DragIcon = imgIcos.ListImages(hcICONPROHIBIDO).Picture
    End If
  End If
End Sub

Private Sub tvwPr_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nodo As Node, comp As String
    
  Set tvwPr.DropHighlight = Nothing
  Set nodo = tvwPr.HitTest(X, Y)
    
  On Error Resume Next
  comp = nodo.Key         'Para comprobar que se haya seleccionado un nodo
  If Err = 0 Then
    If objMasterInfo.rdoCursor.EOF = False And objMasterInfo.rdoCursor.BOF = False Then
      Select Case nodo.Image
        Case hcICONPR  ' Se ha seleccionado una prueba
          Set tvwPr.SelectedItem = nodo
          tvwPr.Drag
          tvwPr.DragIcon = imgIcos.ListImages(nodo.Image).Picture
        Case hcICONPAQUETE ' Se ha seleccionado un paquete
          Set tvwPr.SelectedItem = nodo
          tvwPr.Drag
          tvwPr.DragIcon = imgIcos.ListImages(nodo.Image).Picture
        Case hcICONPROTOCOLO ' Se ha seleccionado un protocolo
          Set tvwPr.SelectedItem = nodo
          tvwPr.Drag
          tvwPr.DragIcon = imgIcos.ListImages(nodo.Image).Picture
      End Select
    End If
  End If
End Sub

Private Sub lvwPrPaq_DblClick()
'  Call LlamarCuestionario
End Sub

Private Sub lvwPrPaq_DragDrop(Source As Control, X As Single, Y As Single)
Dim cDptRea As Long, cPr As Long, cPaq As Long, cProt As Long, i As Integer
Dim sql As String, texto As String
Dim rdoQ As rdoQuery, rdoQOrden As rdoQuery
Dim nodo As Node, nodoPr As Node, item As ListItem, itemPr As ListItem
Dim OrdenPr As Long, cDptPaq As Long, cPaqCopiar As Long, cDptPaqCopiar As Long

  Screen.MousePointer = 11
  Set nodo = Source.SelectedItem
  cPaq = objMasterInfo.rdoCursor("PR33CodEstandard")
  cDptPaq = objMasterInfo.rdoCursor("AD02CodDpto")
  Select Case nodo.Image
    Case hcICONPR  ' Se ha arrastrado una prueba
      cDptRea = Val(Mid(nodo.Tag, InStr(nodo.Tag, "-") + 1))
      cPr = Val(nodo.Tag)
      sql = "INSERT INTO PR3400 (AD02CodDpto, PR33CodEstandard, PR01CodActuacion, " _
          & "AD02CodDpto_Rea, PR34Orden) " _
          & "VALUES (?, ?, ?, ?, ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cDptPaq
      rdoQ(1) = cPaq
      rdoQ(2) = cPr
      rdoQ(3) = cDptRea
      nOrden = nOrden + 1 ' Una nueva prueba
      On Error Resume Next
      Set itemPr = lvwPrPaq.DropHighlight
      OrdenPr = Val(itemPr.Text)
      If OrdenPr <> 0 Then ' Si se ha arrastrado sobre una prueba, se modica el orden
        ' Se actualiza el orden del resto de las pruebas
        sql = "UPDATE PR3400 SET PR34Orden = PR34Orden + 1 WHERE " _
            & "PR33CodEstandard = ? AND " _
            & "AD02CodDpto = ? AND " _
            & "PR34Orden >= ?"
        Set rdoQOrden = objApp.rdoConnect.CreateQuery("", sql)
        rdoQOrden(0) = cPaq
        rdoQOrden(1) = cDptPaq
        rdoQOrden(2) = OrdenPr
        rdoQOrden.Execute
        
        rdoQ(4) = OrdenPr
        rdoQ.Execute
        Call LlenarPrPaq(cPaq, cDptPaq)
              
      Else
        rdoQ(4) = nOrden
        On Error Resume Next
        rdoQ.Execute
        If rdoQ.RowsAffected = 1 Then
          Call LlenarPrPaq(cPaq, cDptPaq)
'          Set item = lvwPrPaq.ListItems.Add(, "P" & nodo.Tag, nOrden & ".- " & nodo.Text, , hcICONPR)
'          item.SubItems(1) = Dpts("D" & cDptRea)
'          item.Tag = cDptRea
        Else
          MsgBox "Error al a�adir la actuaci�n al paquete." & Error, vbExclamation, Me.Caption
          nOrden = nOrden - 1
        End If
      End If
      
    Case hcICONPAQUETE ' Se ha arrastrado un paquete
      cPaqCopiar = Val(nodo.Tag)
      cDptPaqCopiar = Val(Mid(nodo.Tag, InStr(nodo.Tag, "-") + 1))
      Set itemPr = lvwPrPaq.DropHighlight
      On Error Resume Next
      OrdenPr = Val(itemPr.Text)
      If OrdenPr <> 0 Then ' Si se ha arrastrado sobre una prueba, se modica el orden
        ' Se actualiza el orden del resto de las pruebas
        sql = "UPDATE PR3400 SET PR34Orden = PR34Orden + " & nodo.Children & " WHERE " _
            & "PR33CodEstandard = ? AND " _
            & "AD02CodDpto = ? AND " _
            & "PR34Orden >= ?"
        Set rdoQOrden = objApp.rdoConnect.CreateQuery("", sql)
        rdoQOrden(0) = cPaq
        rdoQOrden(1) = cDptPaq
        rdoQOrden(2) = OrdenPr
        rdoQOrden.Execute
        nOrden = OrdenPr - 1
      End If
      
      sql = "INSERT INTO PR3400 " _
          & "(AD02CodDpto_Rea, PR33CodEstandard, PR01CodActuacion, AD02CodDpto) " _
          & "(SELECT AD02CodDpto_Rea, " & cPaq & ", PR01CodActuacion, " & cDptPaq & " " _
          & "FROM PR3400 WHERE PR33CodEstandard = ? AND AD02CodDpto = ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cPaqCopiar
      rdoQ(1) = cDptPaqCopiar
      On Error Resume Next
      rdoQ.Execute
      If rdoQ.RowsAffected > 0 Then
        Set nodoPr = nodo.Child
        For i = 1 To nodo.Children
          cPr = Val(nodoPr.Tag)
          sql = "UPDATE PR3400 SET PR34Orden = ? WHERE " _
              & "PR33CodEstandard = ? AND " _
              & "AD02CodDpto = ? AND " _
              & "PR01CodActuacion = ?"
          Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
          nOrden = nOrden + 1
          rdoQ(0) = nOrden
          rdoQ(1) = cPaq
          rdoQ(2) = cDptPaq
          rdoQ(3) = cPr
          On Error Resume Next
          rdoQ.Execute
          Set nodoPr = nodoPr.Next
        Next i
        Call LlenarPrPaq(cPaq, cDptPaq)
      Else
        MsgBox "Error al a�adir el paquete." & Error, vbExclamation, Me.Caption
      End If
    
    Case hcICONPROTOCOLO ' Se ha arrastrado un protocolo
      cProt = Val(Mid(nodo.Key, 2))
      On Error Resume Next
      Set itemPr = lvwPrPaq.DropHighlight
      OrdenPr = Val(itemPr.Text)
      If OrdenPr <> 0 Then ' Si se ha arrastrado sobre una prueba, se modica el orden
        ' Se actualiza el orden del resto de las pruebas
        sql = "UPDATE PR3400 SET PR34Orden = PR34Orden + " & nodo.Children & " WHERE " _
            & "PR33CodEstandard = ? AND " _
            & "AD02CodDpto = ? AND " _
            & "PR36Orden >= ?"
        Set rdoQOrden = objApp.rdoConnect.CreateQuery("", sql)
        rdoQOrden(0) = cPaq
        rdoQOrden(1) = cDptPaq
        rdoQOrden(2) = OrdenPr
        rdoQOrden.Execute
        nOrden = OrdenPr - 1
      End If
      
      sql = "INSERT INTO PR3400 (AD02CodDpto_Rea, PR33CodEstandard, PR01CodActuacion, " _
          & "AD02CodDpto, PR34Orden) " _
          & "SELECT AD02CodDpto, " & cPaq & ", " _
          & "PR01CodActuacion, " & cDptPaq & ", PR36Orden + " & nOrden & " FROM PR3600 WHERE PR35CodProtocolo = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cProt
      On Error Resume Next
      rdoQ.Execute
      If rdoQ.RowsAffected > 0 Then
        Call LlenarPrPaq(cPaq, cDptPaq)
      Else
        MsgBox "Error al a�adir el protocolo." & Error, vbExclamation, Me.Caption
      End If

  End Select
  Screen.MousePointer = 0

End Sub

Private Sub lvwPrPaq_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
    
  Set lvwPrPaq.DropHighlight = lvwPrPaq.HitTest(X, Y)
    
  If State = 1 Then
    Set lvwPrPaq.DropHighlight = lvwPrPaq.SelectedItem
  End If
  
  Source.DragIcon = imgIcos.ListImages(Source.SelectedItem.Image).Picture
End Sub
Private Sub lvwPrPaq_ItemClick(ByVal item As ComctlLib.ListItem)
  tlbToolbar1.Buttons(28).Enabled = True
End Sub

Private Sub lvwPrPaq_KeyDown(KeyCode As Integer, Shift As Integer)
Dim cPr As Long, res As Integer, OrdenPr As Integer, cPaq As Long, cDpt As Long
Dim sql As String
Dim rdoQ As rdoQuery
Dim item As ListItem, itemActualizar As ListItem

  If KeyCode = 46 Then ' Suprimir
    On Error Resume Next
    cPr = Mid(lvwPrPaq.SelectedItem.Key, 2)
    If cPr <> 0 Then
      cPaq = objMasterInfo.rdoCursor("PR33CodEstandard")
      cDpt = objMasterInfo.rdoCursor("AD02CodDpto")
      Set item = lvwPrPaq.SelectedItem
      res = MsgBox("�Desea borrar la actuaci�n '" & item.Text & "' del protocolo?", vbQuestion + vbYesNo, Me.Caption)
      If res = vbNo Then Exit Sub
      OrdenPr = Val(item.Text)
      sql = "DELETE FROM PR3400 WHERE " _
          & "PR01CodActuacion = ? AND " _
          & "PR33CodEstandard = ? AND " _
          & "AD02CodDpto = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = cPr
      rdoQ(1) = cPaq
      rdoQ(2) = cDpt
      rdoQ.Execute
      If rdoQ.RowsAffected > 0 Then
        sql = "UPDATE PR3400 SET PR34Orden = PR34Orden -1 WHERE " _
          & "PR33CodEstandard = ? AND " _
          & "AD02CodDpto = ? AND " _
          & "PR34Orden > ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = cPaq
        rdoQ(1) = cDpt
        rdoQ(2) = OrdenPr
        rdoQ.Execute
' Si se han actualizado filas, hay que actualizar el orden de las pruebas en pantalla
        If rdoQ.RowsAffected > 0 Then
          Call LlenarPrPaq(cPaq, cDpt)
        Else
          lvwPrPaq.ListItems.Remove item.Index
          nOrden = nOrden - 1
        End If
      Else
        MsgBox "Error al borrar la prueba del paquete. " & Error, vbExclamation, Me.Caption
      End If
    End If
  End If

End Sub

Private Sub tvwPr_NodeClick(ByVal Node As ComctlLib.Node)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer, cAct As Integer
Dim nodoDpt As Node, nodoPr As Node
Dim item As Variant
  
  Screen.MousePointer = 11
  Select Case Node.Image
    Case hcICONACTIVIDAD
      If Node.Children = 0 Then
        For i = 1 To UBound(Dpts)
          Set nodoDpt = tvwPr.Nodes.Add(Node.Index, tvwChild, , Dpts(i).cDpt & ".- " & Dpts(i).Dpt, hcICONDPT)
          nodoDpt.Tag = Dpts(i).cDpt
        Next i
      End If
    Case hcICONDPT
      If Node.Children = 0 Then
        cAct = Node.Parent.Tag
        sql = "SELECT PR0100.PR01CodActuacion, PR0100.PR01DesCorta, PR0100.PR01DesCompleta " _
            & "FROM PR0100, PR0200 WHERE " _
            & "PR0100.PR01CodActuacion = PR0200.PR01CodActuacion AND " _
            & "PR0100.PR01FecFin IS NULL AND " _
            & "PR0200.AD02CodDpto = ? AND " _
            & "PR0100.PR12CodActividad = ? " _
            & "ORDER BY PR0100.PR01DesCorta"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = Val(Node.Text)
        rdoQ(1) = cAct
        Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
        While rdo.EOF = False
          Set nodoPr = tvwPr.Nodes.Add(Node.Index, tvwChild, , rdo(1) & IIf(IsNull(rdo(2)), "", " (" & rdo(2) & ")"), hcICONPR)
          nodoPr.Tag = rdo(0) & "-" & Val(Node.Text)
          rdo.MoveNext
        Wend
      End If
  End Select
  Screen.MousePointer = 0

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  Call objWinInfo.CtrlDataChange
  If intIndex = 3 And objWinInfo.intWinStatus = cwModeSingleAddKey And txtText1(intIndex).Text <> "" Then
    sql = "SELECT MAX(PR33CodEstandard) FROM PR3300 WHERE AD02CodDpto = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = txtText1(2).Text
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If IsNull(rdo(0)) Then txtText1(0).Text = 1 Else txtText1(0).Text = rdo(0) + 1
  End If
End Sub

Sub LlenarPrPaq(cPaq As Long, cDpt As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem

  lvwPrPaq.ListItems.Clear
  sql = "SELECT PR0100.PR01CodActuacion, PR0100.PR01DesCorta, " _
      & "AD0200.AD02CodDpto, AD0200.AD02DesDpto, PR3400.PR34Orden " _
      & "FROM PR0100, PR3400, AD0200 WHERE " _
      & "PR3400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR3400.AD02CodDpto_Rea = AD0200.AD02CodDpto AND " _
      & "PR3400.PR33CodEstandard = ? AND " _
      & "PR3400.AD02CodDpto = ? " _
      & "ORDER BY PR3400.PR34Orden"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cPaq
  rdoQ(1) = cDpt
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  nOrden = 0
  While rdo.EOF = False
    Set item = lvwPrPaq.ListItems.Add(, "P" & rdo(0), rdo(4) & ".- " & rdo(1), , hcICONPR)
    item.SubItems(1) = rdo(3)
    item.Tag = rdo(2) ' cod. del departamento realizador de la actuacion
    nOrden = rdo(4)
    rdo.MoveNext
  Wend
End Sub



