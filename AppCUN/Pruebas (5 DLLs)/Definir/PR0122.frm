VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmdefactuacionesCUN 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones C.U.N"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   10245
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0122.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   10245
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   10245
      _ExtentX        =   18071
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Height          =   1575
      Index           =   1
      Left            =   9240
      TabIndex        =   42
      Top             =   5880
      Width           =   2535
      Begin VB.CommandButton cmddptos 
         Caption         =   "Dptos. Realizadores"
         Height          =   375
         Left            =   480
         TabIndex        =   24
         Top             =   840
         Width           =   1935
      End
      Begin VB.CommandButton cmdfases 
         Caption         =   "Fases"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   23
         Top             =   360
         Width           =   1935
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   7
         Left            =   0
         Picture         =   "PR0122.frx":000C
         Top             =   720
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   6
         Left            =   0
         Picture         =   "PR0122.frx":044E
         Top             =   240
         Width           =   480
      End
   End
   Begin VB.Frame fraFrame1 
      Height          =   4575
      Index           =   2
      Left            =   9240
      TabIndex        =   38
      Top             =   1080
      Width           =   2535
      Begin VB.CommandButton cmdinteracciones 
         Caption         =   "Interacciones"
         Height          =   375
         Left            =   480
         TabIndex        =   18
         Top             =   1200
         Width           =   1935
      End
      Begin VB.CommandButton cmdmuestras 
         Caption         =   "Muestras"
         Height          =   375
         Left            =   480
         TabIndex        =   19
         Top             =   1800
         Width           =   1935
      End
      Begin VB.CommandButton cmdcuestionario 
         Caption         =   "Cuestionario"
         Height          =   375
         Left            =   480
         TabIndex        =   20
         Top             =   2400
         Width           =   1935
      End
      Begin VB.CommandButton cmdactprev 
         Caption         =   "Actuaciones Previsibles"
         Height          =   375
         Left            =   480
         TabIndex        =   21
         Top             =   3000
         Width           =   1935
      End
      Begin VB.CommandButton cmdactasoc 
         Caption         =   "Actuaciones Asociadas"
         Height          =   375
         Left            =   480
         TabIndex        =   22
         Top             =   3600
         Width           =   1935
      End
      Begin VB.CommandButton cmdcondiciones 
         Caption         =   "Condiciones"
         Height          =   375
         Left            =   480
         TabIndex        =   17
         Top             =   600
         Width           =   1935
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   5
         Left            =   0
         Picture         =   "PR0122.frx":0890
         Top             =   3480
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   4
         Left            =   0
         Picture         =   "PR0122.frx":0CD2
         Top             =   2880
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   3
         Left            =   0
         Picture         =   "PR0122.frx":1114
         Top             =   2280
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   2
         Left            =   0
         Picture         =   "PR0122.frx":1556
         Top             =   1680
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   1
         Left            =   0
         Picture         =   "PR0122.frx":1998
         Top             =   1080
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   0
         Left            =   0
         Picture         =   "PR0122.frx":1DDA
         Top             =   480
         Width           =   480
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7455
      Index           =   0
      Left            =   0
      TabIndex        =   31
      Top             =   480
      Width           =   9135
      Begin TabDlg.SSTab tabTab1 
         Height          =   6975
         Index           =   0
         Left            =   120
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   360
         Width           =   8895
         _ExtentX        =   15690
         _ExtentY        =   12303
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0122.frx":221C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(13)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(9)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(4)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(8)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(10)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(11)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(12)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(16)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(3)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(15)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(14)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(17)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(18)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(20)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(21)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(22)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "cboSSDBCombo1(0)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "dtcDateCombo1(1)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "dtcDateCombo1(0)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "chkCheck1(1)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "chkCheck1(0)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(2)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(6)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(7)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(8)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "tabTab1(1)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "Text1(0)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "cmdfasesimple"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtdesact"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtdia"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txthora"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtminuto"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "chkCheck1(2)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(1)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(9)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(10)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(11)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).ControlCount=   41
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0122.frx":2238
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBgrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   11
            Left            =   1320
            ScrollBars      =   1  'Horizontal
            TabIndex        =   62
            Tag             =   "Descripci�n Corta"
            Top             =   6480
            Width           =   6960
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   10
            Left            =   1320
            ScrollBars      =   1  'Horizontal
            TabIndex        =   61
            Tag             =   "Descripci�n Corta"
            Top             =   5880
            Width           =   4000
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "IM06CODLOCALIIM"
            Height          =   330
            Index           =   9
            Left            =   240
            TabIndex        =   13
            Tag             =   "Locallizaci�n del documento|Localizaci�n Doc."
            Top             =   6480
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "IM05CODPLANTILL"
            Height          =   330
            Index           =   1
            Left            =   240
            TabIndex        =   12
            Tag             =   "Plantilla"
            Top             =   5880
            Width           =   852
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "PR01INDREQDOC"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   3720
            TabIndex        =   16
            Tag             =   "�Se contesta mediante un documento?"
            Top             =   5280
            Width           =   255
         End
         Begin VB.TextBox txtminuto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   5160
            MaxLength       =   2
            TabIndex        =   8
            ToolTipText     =   "Duraci�n media en minutos"
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txthora 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   3840
            MaxLength       =   2
            TabIndex        =   7
            ToolTipText     =   "Duraci�n media en horas"
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txtdia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   2640
            MaxLength       =   4
            TabIndex        =   6
            ToolTipText     =   "Duraci�n media en d�as"
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txtdesact 
            Height          =   330
            Left            =   1800
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Tag             =   "Descripci�n de la Actividad"
            Top             =   1800
            Width           =   5805
         End
         Begin VB.CommandButton cmdfasesimple 
            BackColor       =   &H00000000&
            Caption         =   "Fase �nica"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2280
            TabIndex        =   25
            Top             =   360
            Width           =   1935
         End
         Begin VB.TextBox Text1 
            Height          =   375
            Index           =   0
            Left            =   4560
            TabIndex        =   50
            Top             =   360
            Visible         =   0   'False
            Width           =   1095
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   1695
            Index           =   1
            Left            =   240
            TabIndex        =   46
            Top             =   2280
            Width           =   7575
            _ExtentX        =   13361
            _ExtentY        =   2990
            _Version        =   327681
            Style           =   1
            TabHeight       =   520
            TabCaption(0)   =   "Descripci�n completa"
            TabPicture(0)   =   "PR0122.frx":2254
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "txtText1(3)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).ControlCount=   2
            TabCaption(1)   =   "Procedimiento Paciente"
            TabPicture(1)   =   "PR0122.frx":2270
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(4)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblLabel1(5)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).ControlCount=   2
            TabCaption(2)   =   "Instrucciones Paciente"
            TabPicture(2)   =   "PR0122.frx":228C
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(5)"
            Tab(2).Control(0).Enabled=   0   'False
            Tab(2).Control(1)=   "lblLabel1(6)"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).ControlCount=   2
            Begin VB.TextBox txtText1 
               DataField       =   "PR01DESINSTRPACI"
               Height          =   975
               Index           =   5
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   27
               Tag             =   "Instrucciones|Instrucciones para el paciente"
               Top             =   600
               Width           =   6780
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR01DESPROCPACI"
               Height          =   975
               Index           =   4
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   26
               Tag             =   "Procedimiento|Procedimiento que debe seguir el paciente"
               Top             =   600
               Width           =   6780
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR01DESCOMPLETA"
               Height          =   975
               Index           =   3
               Left            =   240
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   4
               Tag             =   "Descripci�n Completa"
               Top             =   600
               Width           =   6780
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Completa"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   240
               TabIndex        =   49
               Top             =   360
               Width           =   2535
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Instrucciones Paciente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -74760
               TabIndex        =   48
               Top             =   360
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Procedimiento Paciente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Index           =   5
               Left            =   -74760
               TabIndex        =   47
               Top             =   360
               Width           =   2535
            End
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR01NUMVALRESU"
            Height          =   330
            Index           =   8
            Left            =   240
            TabIndex        =   5
            Tag             =   "Validez de Resultados"
            Top             =   4320
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR01NUMDESVTIPICA"
            Height          =   330
            Index           =   7
            Left            =   6840
            TabIndex        =   11
            Tag             =   "Desviaci�n T�pica|Desviaci�n T�pica. Formato de entrada : 9999999,9999 "
            Top             =   4320
            Width           =   1452
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "C�digo Actuaci�n|C�digo de la Actuaci�n"
            Top             =   360
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR01NUMDURACMED"
            Height          =   330
            Index           =   6
            Left            =   7680
            TabIndex        =   56
            Tag             =   "Duraci�n Media"
            Top             =   1560
            Visible         =   0   'False
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR01DESCORTA"
            Height          =   330
            Index           =   2
            Left            =   240
            ScrollBars      =   1  'Horizontal
            TabIndex        =   1
            Tag             =   "Descripci�n Corta"
            Top             =   1080
            Width           =   5400
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "PR01INDCONSFDO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   2640
            TabIndex        =   14
            Tag             =   "Consentimiento Firmado|�Se necesita consentimiento firmado?"
            Top             =   4920
            Width           =   255
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "PR01INDINSTRREA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   7440
            TabIndex        =   15
            Tag             =   "�Instrucciones de  Realizaci�n?|�Necesita Instrucciones de Realizaci�n?"
            Top             =   4920
            Width           =   255
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBgrid1 
            Height          =   6615
            Index           =   0
            Left            =   -74880
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   120
            Width           =   8175
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            RowHeight       =   423
            Columns(0).Width=   3200
            _ExtentX        =   14420
            _ExtentY        =   11668
            _StockProps     =   79
            Caption         =   "ACTUACIONES"
            ForeColor       =   0
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "PR01FECINICO"
            Height          =   330
            Index           =   0
            Left            =   5880
            TabIndex        =   9
            Tag             =   "Inicio Vigencia|Fecha de inicio de vigencia de la actuaci�n"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "PR01FECFIN"
            Height          =   330
            Index           =   1
            Left            =   5880
            TabIndex        =   10
            Tag             =   "Fin Vigencia|Fecha de fin de vigencia de la actuaci�n"
            Top             =   1080
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR12CODACTIVIDAD"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   2
            Tag             =   "C�digo Actividad"
            Top             =   1800
            Width           =   660
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   820
            Columns(0).Caption=   "C�D"
            Columns(0).Name =   "C�DIGO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   9525
            Columns(1).Caption=   "DESCRIPCI�N"
            Columns(1).Name =   "DESCRIPCI�N"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1164
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Plantilla"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   240
            TabIndex        =   60
            Top             =   5640
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Localizaci�n del documento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   21
            Left            =   240
            TabIndex        =   59
            Top             =   6240
            Width           =   2415
         End
         Begin VB.Label lblLabel1 
            Caption         =   "�Se contesta mediante un documento?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   240
            TabIndex        =   58
            Tag             =   "Instrucciones de  Realizaci�n"
            Top             =   5280
            Width           =   3375
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   5880
            TabIndex        =   55
            Top             =   4440
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   4560
            TabIndex        =   54
            Top             =   4440
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   3360
            TabIndex        =   53
            Top             =   4440
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Actividad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   1800
            TabIndex        =   52
            Top             =   1560
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d Actividad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   240
            TabIndex        =   51
            Top             =   1560
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   7800
            TabIndex        =   45
            Top             =   1920
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "�Consentimiento Firmado?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   240
            TabIndex        =   44
            Tag             =   "�Se necesita consentimiento firmado?"
            Top             =   4920
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            Caption         =   "�Necesita Instrucciones de Realizaci�n?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   3840
            TabIndex        =   43
            Tag             =   "Instrucciones de  Realizaci�n"
            Top             =   4920
            Width           =   3495
         End
         Begin VB.Label lblLabel1 
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   1200
            TabIndex        =   41
            Top             =   4440
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   5880
            TabIndex        =   40
            Top             =   840
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   4
            Left            =   5880
            TabIndex        =   39
            Top             =   120
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   37
            Tag             =   "C�digo de la Actuaci�n"
            Top             =   120
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Duraci�n Media"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   2640
            TabIndex        =   36
            Top             =   4110
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Desviaci�n T�pica"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   6840
            TabIndex        =   35
            Top             =   4080
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Corta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   34
            Top             =   840
            Width           =   2895
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Validez de  Resultados"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   240
            TabIndex        =   33
            Tag             =   "Fecha tope de validez de los resultados de la actuaci�n"
            Top             =   4080
            Width           =   2295
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   29
      Top             =   7935
      Width           =   10245
      _ExtentX        =   18071
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "�Necesita Instrucciones de Realizaci�n?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   19
      Left            =   0
      TabIndex        =   57
      Tag             =   "Instrucciones de  Realizaci�n"
      Top             =   0
      Width           =   3495
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmdefactuacionesCUN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00122.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: Definici�n de Actuaciones                               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
' mnbotonnuevo indica si se ha pulsado el bot�n Nuevo
Dim mnbotonnuevo As Integer
'mnmenunuevo indica si se ha pulsado la opci�n de men� Nuevo
Dim mnmenunuevo As Integer
'mnabrir indica si se ha pulsado la opci�n Abrir
Dim mnabrir As Integer
'mncancelar controla los mensajes en la funci�n Borrar_Actuaciones
Dim mncancelar As Integer
'intborrar indica recoge el resultado de la funci�n Borrar_Actuaciones
'si se quiere borrar una actuaci�n sin fases
Dim intborrar As Integer
Dim ncontador As Integer
'intactcurso recoge la actuaci�n en curso
Dim intactcurso As Variant
' intdelete controla si se ha borrado una actuaci�n sin fases
Dim intdelete As Integer
' intposicion controla que al pulsar Nuevo el cursor se posicione en Desc.Corta
Dim intposicion As Integer
' TiempoTotal calcula la duraci�n media en minutos
Dim TiempoTotal As Variant
'entrar sirve para refrescar la pantalla en el Activate pero no la primera
'vez que se hace el Activate sini s�lo al volver de la pantalla de Fases
Dim entrar As Integer
'intmoverprimeravez para controlar que la primera vez que se pulsa PrimerRegistro,
'SiguienteRegistro,UltimoRegistro,RegistroAnterior y no hay cambios que no haga
'la pregunta de si desea salvar los cambios
Dim intmoverprimeravez As Integer
'localizarymoverse controla si se pulsa Localizar y seguido nos desplazamos
'por los registros,esto es para que no pregunte si se desean guardar los cambios
Dim localizarymoverse As Integer
'intcambioalgo controla si se cambia algo en la pantalla:si se pulsa una tecla en una
'caja de texto,se hace click en la check o se cierra la Combo o la Datecombo
Dim intcambioalgo As Integer
'intregcambiado detecta en PreRead de Objwininfo que se cambia de registro,esto se
'hace porque si no, al entrar hace Click de la Check y detectar�a cambios que en
'realidad no hay.
Dim intregcambiado As Integer





Private Sub cmdfasesimple_Click()
  Dim sqlstr As String
  Dim strsql As String
  Dim rsta As rdoResultset
  Dim sqlstr1 As String
  Dim rstA1 As rdoResultset
  Dim sqlstr2 As String
  Dim rstA2 As rdoResultset
  Dim strdep As String
  Dim rstdep As rdoResultset
  Dim strmensaje As String

gstrWhereSinFases = objWinInfo.objWinActiveForm.strWhere

strdep = "select count(*) from pr0200 where pr01codactuacion=" & txtText1(0)
Set rstdep = objApp.rdoConnect.OpenResultset(strdep)

If rstdep.rdoColumns(0).Value = 0 Then
        strmensaje = MsgBox("El departamento realizador es obligatorio", vbCritical, _
                            "Departamento Realizador Obligatorio")
Else

'se guarda la fase 1 si �sta no est� guardada
sqlstr2 = "select count(*) from PR0500 where pr01codactuacion=" & txtText1(0) _
        & " and pr05numfase=1"
On Error GoTo Err_Ejecutar
Set rstA2 = objApp.rdoConnect.OpenResultset(sqlstr2)
If (rstA2.rdoColumns(0).Value = 0) Then
  sqlstr = "INSERT INTO PR0500 " & _
           "(pr01codactuacion,pr05numfase,pr05desfase,pr05numocupaci," & _
           "pr05numfase_pre,pr05numtminfpre,pr05numtmaxfpre,pr05indhabilnatu,pr05indiniciofin) " & _
           "VALUES (" _
        & txtText1(0).Text & "," _
        & 1 & "," _
        & "'" & txtText1(2) & "'" & ",0,null,0,0,0,0)"
  On Error GoTo Err_Ejecutar
  objApp.rdoConnect.Execute sqlstr, 64
End If
rstA2.Close
Set rstA2 = Nothing
 
strsql = "SELECT pr05numocupaci " _
        & "FROM PR0500 " _
        & "WHERE pr01codactuacion = " & txtText1(0) & _
          " AND pr05numfase=1"
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
Text1(0) = rsta.rdoColumns(0).Value


'Load frmfasesimple
'frmfasesimple!tabTab1(1).Tab = 0 'para mostrar el detalle del Tab
'frmfasesimple.Show (vbModal)
'Unload frmfasesimple
'Set frmfasesimple = Nothing
Call objsecurity.LaunchProcess("PR0136")
rsta.Close
Set rsta = Nothing
End If

rstdep.Close
Set rstdep = Nothing
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
objWinInfo.objWinActiveForm.strWhere = gstrWhereSinFases
Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub
Private Sub Indicadores()
  Dim rsta As rdoResultset
  Dim strsql As String
  
  If txtText1(0).Text = "" Then
    Image1(0).Visible = False
    Image1(1).Visible = False
    Image1(2).Visible = False
    Image1(3).Visible = False
    Image1(4).Visible = False
    Image1(5).Visible = False
    Image1(6).Visible = False
    Image1(7).Visible = False
  Else
    strsql = "select * from PR2300 where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.EOF Then
      Image1(0).Visible = False
    Else
      Image1(0).Visible = True
    End If
    rsta.Close
    Set rsta = Nothing
    
    strsql = "select * from PR2000 where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.EOF Then
      Image1(1).Visible = False
    Else
      Image1(1).Visible = True
    End If
    rsta.Close
    Set rsta = Nothing
    
    strsql = "select * from PR2500 where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.EOF Then
      Image1(2).Visible = False
    Else
      Image1(2).Visible = True
    End If
    rsta.Close
    Set rsta = Nothing
    
    strsql = "select * from PR2900 where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.EOF Then
      Image1(3).Visible = False
    Else
      Image1(3).Visible = True
    End If
    rsta.Close
    Set rsta = Nothing
    
    strsql = "select * from PR3200 where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.EOF Then
      Image1(4).Visible = False
    Else
      Image1(4).Visible = True
    End If
    rsta.Close
    Set rsta = Nothing
    
    strsql = "select * from PR3100 where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.EOF Then
      Image1(5).Visible = False
    Else
      Image1(5).Visible = True
    End If
    rsta.Close
    Set rsta = Nothing
    
    strsql = "select * from PR0500 where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.EOF Then
      Image1(6).Visible = False
    Else
      Image1(6).Visible = True
      cmdfases.Enabled = True
    End If
    rsta.Close
    Set rsta = Nothing
    
    strsql = "select * from PR0200 where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    If rsta.EOF Then
      Image1(7).Visible = False
    Else
      Image1(7).Visible = True
    End If
    rsta.Close
    Set rsta = Nothing
    
  End If

End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Activate()
Dim rsta As rdoResultset
Dim strsql As String
Dim rstA1 As rdoResultset
Dim strsql1 As String
Dim rstA2 As rdoResultset
Dim strsql2 As String
Dim entroenelif As Integer
Dim cont As Integer

    intposicion = 1
    
    Call Indicadores
    'para que al entrar no detecte los cambios de los Change de Dias,Horas,Minutos
    objWinInfo.objWinActiveForm.blnChanged = False
    
    'se recoge el c�digo de la actuaci�n que est� en pantalla
    intactcurso = txtText1(0).Text
    'para que se active la pesta�a de descripci�n completa
    tabTab1(1).Tab = 0
    'S�lo se refresca cuando se vuelve de la pantalla Fases
    If entrar = 1 Then
       entrar = 0
    Else
      'gintactsinfases controla que al volver de FaseUnica no se desean salvar los
      'cambios sin embargo debe mostrar la actuaci�n que se est� definiendo y no
      'refrescar
      If gintactsinfases = 0 Then
        Call objWinInfo.WinProcess(6, 26, 0)
      Else
        gintactsinfases = 0
      End If
    End If
    cmdfasesimple.Enabled = False
    cmddptos.Enabled = False
    
    'habilitar el resto de botones
    If (txtText1(0) <> "") Then
        entroenelif = 1
        strsql = "select count(*) from PR0100 " & _
             "where PR01CODACTUACION=" & txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(strsql)
        
        strsql2 = "select count(*) from PR0500 " & _
             "where PR01CODACTUACION=" & txtText1(0).Text
        Set rstA2 = objApp.rdoConnect.OpenResultset(strsql2)
        
        If (rsta.rdoColumns(0).Value > 0) Then
         If (rstA2.rdoColumns(0).Value > 0) Then
            cmdcondiciones.Enabled = True
            cmdinteracciones.Enabled = True
            cmdmuestras.Enabled = True
            cmdcuestionario.Enabled = True
            cmdactprev.Enabled = True
            cmdactasoc.Enabled = True
            cmdfases.Enabled = True
            cmddptos.Enabled = True
            ' mira si la actuaci�n tiene alg�n departamento realizador en la
            ' tabla de Departamentos Realizadores
            strsql1 = "SELECT COUNT(*) " _
                & "FROM PR0200 " _
                & "WHERE pr01codactuacion = " & txtText1(0)
            Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
            cont = rstA1.rdoColumns(0).Value
            If (rstA1.rdoColumns(0).Value = 0) Then
                    cmdcondiciones.Enabled = False
                    cmdinteracciones.Enabled = False
                    cmdmuestras.Enabled = False
                    cmdcuestionario.Enabled = False
                    cmdactprev.Enabled = False
                    cmdactasoc.Enabled = False
                    cmdfases.Enabled = False
                    cmdfasesimple.Enabled = False
            Else
                    cmdinteracciones.Enabled = True
            End If
            rstA1.Close
            Set rstA1 = Nothing
            Else
              strsql1 = "SELECT COUNT(*) " _
                & "FROM PR0200 " _
                & "WHERE pr01codactuacion = " & txtText1(0)
              Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
              cont = rstA1.rdoColumns(0).Value
              If (rstA1.rdoColumns(0).Value > 0) Then
               cmdfases.Enabled = True
               cmdfasesimple.Enabled = True
               cmddptos.Enabled = True
              Else
               cmdfases.Enabled = False
               cmdfasesimple.Enabled = False
              End If
              rstA1.Close
              Set rstA1 = Nothing
        End If
        End If
    End If
    
If (txtText1(0).Text <> "") Then
    If (rsta.rdoColumns(0).Value > 0) Then
      'si no hay fases
      If rstA2.rdoColumns(0).Value = 0 Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        cmdfases.Enabled = True
        cmdfasesimple.Enabled = True
      End If
    'no hay fases,no hay dpto.realizador
    If (rstA2.rdoColumns(0).Value = 0 And cont = 0) Then
      cmdfases.Enabled = False
      cmdfasesimple.Enabled = False
    End If
  End If
End If

If entroenelif = 1 Then
  entroenelif = 0
  rsta.Close
  Set rsta = Nothing
  rstA2.Close
  Set rstA2 = Nothing
End If

End Sub

Private Sub Form_Load()
 
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  'inicializaci�n de variables
  mnbotonnuevo = 0
  mnmenunuevo = 0
  intmoverprimeravez = 0

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  'se actualiza esta variable para que en el Activate se refresque la pantalla o no
  entrar = 1
   
  With objDetailInfo
    .strName = "Actuaciones"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0100"
    .blnMasive = False
    ' s�lo las actuaciones con alguna fase (y la fase con al menos un tipo de recurso)
''    .strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
''
    
    Call .FormAddOrderField("PR01CODACTUACION", cwAscending)
    
    Call .objPrinter.Add("PR1221", "Listado de Actuaciones de la C.U.N")
    Call .objPrinter.Add("PR1222", "Listado  Actuaciones-Departamentos Realizadores")
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Actuaciones")
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Descripci�n Corta", cwString)
    Call .FormAddFilterWhere(strKey, "PR01NUMDURACMED", "Duraci�n Media", cwString)
    Call .FormAddFilterWhere(strKey, "PR01NUMDESVTIPICA", "Desviaci�n T�pica", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01NUMVALRESU", "Fecha Tope de Validez de los resultados", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01FECINICO", "Fecha Inicio de Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "PR01FECFIN", "Fecha Fin de Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "PR01INDITERATIVA", "� Es Iterativa ?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR01INDCONSFDO", "� Necesita Consentimiento Firmado ?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR01INDINSTRREA", "� Necesita Instrucciones de Realizaci�n ?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR01INDREQDOC", "� Se contesta mediante un documento ?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Descripci�n Corta")
    Call .FormAddFilterOrder(strKey, "PR01NUMDURACMED", "Duraci�n Media")
    Call .FormAddFilterOrder(strKey, "PR01NUMDESVTIPICA", "Desviaci�n T�pica")
    Call .FormAddFilterOrder(strKey, "PR01NUMVALRESU", "Fecha Tope de Validez de los Resultados")
    Call .FormAddFilterOrder(strKey, "PR01FECINICO", "Fecha Inicio de Vigencia")
    Call .FormAddFilterOrder(strKey, "PR01FECFIN", "Fecha Fin de Vigencia")
    Call .FormAddFilterOrder(strKey, "PR01INDREQDOC", "� Se contesta mediante un documento ?")
End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo)
   
    .CtrlGetInfo(txtText1(3)).blnInGrid = False
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
    .CtrlGetInfo(txtText1(5)).blnInGrid = False
   
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(2)).blnInFind = True
'*    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
'*    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True

    .CtrlGetInfo(txtdia).blnNegotiated = False
    .CtrlGetInfo(txthora).blnNegotiated = False
    .CtrlGetInfo(txtminuto).blnNegotiated = False
    
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    .CtrlGetInfo(txtText1(9)).blnForeign = True
  
  
    'JCR 6/3/98
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR12CODACTIVIDAD", "SELECT * FROM PR1200 WHERE PR12CODACTIVIDAD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtdesact, "PR12DESACTIVIDAD")
  
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "IM05CODPLANTILL", "SELECT * FROM IM0500 WHERE IM05CODPLANTILL= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(10), "IM05NOMDOC")
  
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(9)), "IM06CODLOCALIIM", "SELECT * FROM IM0600 WHERE IM06CODLOCALIIM= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(9)), txtText1(11), "IM06NOMSECCION")
    
    .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR12CODACTIVIDAD,PR12DESACTIVIDAD" & _
    " FROM PR1200 order by PR12CODACTIVIDAD asc"
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
' si al cargar la pantalla no hay ninguna actuaci�n todos los botones est�n desactivados
    If txtText1(0).Text = "" Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        cmddptos.Enabled = False
        cmdfases.Enabled = False
    End If
   intactcurso = txtText1(0).Text
   cmdfasesimple.Enabled = False
   cmddptos.Enabled = False
  
   'para que al entrar no detecte cambios por el evento Change de
   'txtdia,txthora,txtminuto
   objWinInfo.objWinActiveForm.blnChanged = False
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub



Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  If txtText1(0).Text = "" Then
    intCancel = objWinInfo.WinExit
  Else
  'intborrar=1 dice que se quiere borrar la actuaci�n sin fases
  intborrar = Borrar_Actuaciones
  If intborrar = 1 Then
    ' volver a la pantalla del men� principal
    intCancel = objWinInfo.WinExit
    Else
    ' se mantiene en la misma pantalla de Actuaciones
    intCancel = 1
  End If
  End If
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
     'deshabilitar el bot�n Fases si se pulsa Abrir
    Dim sqlstring As String
    Dim strsql As String
    Dim rsta As rdoResultset
    Dim sqlstr1 As String
    Dim rstA1 As rdoResultset
    Dim sqlstr2 As String
    Dim rstA2 As rdoResultset
    
    
    
    
    If (intactcurso <> "") And (txtText1(0).Text <> "") Then
        'se busca en la tabla Fases
        sqlstring = "select count(*) from PR0500 " & _
             "where PR01CODACTUACION=" & txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
        If (rsta.rdoColumns(0).Value <> 0) Then
            cmdfases.Enabled = True
            cmddptos.Enabled = True
        End If
        rsta.Close
        Set rsta = Nothing
    End If

    If intdelete = 1 Then
        If (txtText1(0) <> "") Then
            sqlstring = "select count(*) from PR0100 " & _
                         "where PR01CODACTUACION=" & txtText1(0)
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
            If (rsta.rdoColumns(0).Value = 0) Then
                cmdfases.Enabled = False
                cmddptos.Enabled = False
                intdelete = 0
            End If
            rsta.Close
            Set rsta = Nothing
        End If
    End If
  
    If (ncontador = 0) Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        '*cmddptos.Enabled = False
    Else
        cmdfases.Enabled = True
        cmdcondiciones.Enabled = True
        cmdinteracciones.Enabled = True
        cmdmuestras.Enabled = True
        cmdcuestionario.Enabled = True
        cmdactprev.Enabled = True
        cmdactasoc.Enabled = True
        cmddptos.Enabled = True
        ' mira si la actuaci�n tiene alg�n departamento realizador en la
        ' tabla de Departamentos Realizadores
        sqlstr1 = "SELECT COUNT(*) " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(0)
       Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
       If (rstA1.rdoColumns(0).Value = 0) Then
        cmdinteracciones.Enabled = False
       Else
        cmdinteracciones.Enabled = True
       End If
       rstA1.Close
       Set rstA1 = Nothing
   End If
   'si la actuaci�n no est� guardada
   If (txtText1(0) <> "") Then
     sqlstr2 = "select count(*) from PR0100 " & _
     "where PR01CODACTUACION=" & txtText1(0)
     Set rstA2 = objApp.rdoConnect.OpenResultset(sqlstr2)
     If (rstA2.rdoColumns(0).Value = 0) Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        'cmddptos.Enabled = False
     End If
     rstA2.Close
     Set rstA2 = Nothing
    End If
   
  If (strFormName = "Actuaciones") Then
    If (cboSSDBCombo1(0) <> "") And (Not IsNull(cboSSDBCombo1(0))) Then
      strsql = "Select pr12desactividad from PR1200 where pr12codactividad=" & cboSSDBCombo1(0).Value
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      txtdesact.Text = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
    End If
  End If
  txtdesact.Locked = True

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
      
 Dim sqlstr5 As String
 Dim rstA5 As rdoResultset
 
 intcambioalgo = 0
 
 '********************************************* IRENE 12-3-98
    cmddptos.Enabled = True
    'se mira si la actuaci�n tiene dpto. asociado
    sqlstr5 = "select count(*) from pr0200 where pr01codactuacion=" & txtText1(0)
    Set rstA5 = objApp.rdoConnect.OpenResultset(sqlstr5)
     'no tiene dpto
     If (rstA5.rdoColumns(0).Value = 0) Then
        cmdfases.Enabled = False
        cmdfasesimple.Enabled = False
        rstA5.Close
        Set rstA5 = Nothing
     Else
       rstA5.Close
       Set rstA5 = Nothing
       'tiene alg�n dpto. asociado
       sqlstr5 = "select count(*) from PR0500 where pr01codactuacion=" & txtText1(0)
       Set rstA5 = objApp.rdoConnect.OpenResultset(sqlstr5)
       'si no tiene ninguna fase
       If (rstA5.rdoColumns(0).Value = 0) Then
        cmdfasesimple.Enabled = True
        cmdfases.Enabled = True
       Else
        'tiene ya fases
        cmdfases.Enabled = True
        cmdfasesimple.Enabled = False
       End If
       rstA5.Close
       Set rstA5 = Nothing
    End If
End Sub



Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
  intregcambiado = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Actuaciones" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
   Call objsecurity.LaunchProcess("PR0208")

  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "IM0500"
     .strOrder = "ORDER BY IM05CODPLANTILL ASC"
     
     Set objField = .AddField("IM05CODPLANTILL")
     objField.strSmallDesc = "C�digo de la plantilla"
          
     Set objField = .AddField("IM05NOMDOC")
     objField.strSmallDesc = "Descripci�n"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("IM05CODPLANTILL"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
   If strCtrl = "txtText1(9)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "IM0600"
     .strOrder = "ORDER BY IM06CODLOCALIIM ASC"
     
     Set objField = .AddField("IM06CODLOCALIIM")
     objField.strSmallDesc = "Localizaci�n del documento"
         
     Set objField = .AddField("IM06NOMSECCION")
     objField.strSmallDesc = "Secci�n"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(9), .cllValues("IM06CODLOCALIIM"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub





Private Sub Text1_GotFocus(Index As Integer)
'para que se posicione en descripci�n corta y nunca en el c�digo
If Index = 0 Then
    txtText1(2).SetFocus
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim sqlstr1 As String
Dim rstA1 As rdoResultset
Dim ncontador As Integer
Dim intactu As Integer
Dim intCont As Integer
Dim sqlstr11 As String
Dim rstA11 As rdoResultset
Dim intcont11 As Integer
Dim sqlstr5 As String
Dim rstA5 As rdoResultset
Dim strWarning As String
Dim intResp As Integer

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text


'si se pulsa localizar intposicion=0 porque Localizar no activa el Change
'de txtText1 y entonces llega con valor 1
If btnButton.Index = 16 Then
  intposicion = 0
End If
'*****************************************************************************
'cuando se entra en la pantalla y tras refrescarla, al movernos por los registros
'pregunta si se desean guardar los cambios,por esto se pone este c�digo para
'que la primera vez no lo pregunte
If txtText1(0).Text <> "" And (btnButton.Index = 21 Or btnButton.Index = 22 Or _
                               btnButton.Index = 23 Or btnButton.Index = 24) _
                          And (intmoverprimeravez = 0) Then
  intmoverprimeravez = 1
  objWinInfo.objWinActiveForm.blnChanged = False
End If

'*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
 'If txtText1(0).Text <> "" And btnButton.Index = 4 Then
 If txtText1(0).Text <> "" Then
  'se mira si la actuaci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
  strTiempo = "SELECT count(*) FROM PR0100 where" & _
         " pr01codactuacion=" & txtText1(0).Text & _
         " AND pr01numduracmed=" & TiempoTotal

  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(6), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'*********************************************************************
If txtText1(0).Text <> "" And (btnButton.Index = 21 Or btnButton.Index = 22 Or btnButton.Index = 23 Or btnButton.Index = 24) Then
strTiempo = "SELECT count(*) FROM PR0100 where" & _
         " pr01codactuacion=" & txtText1(0).Text & _
         " AND pr01numduracmed=" & TiempoTotal

  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(6), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

'********************************************************************


'para que s�lo se vean las actuaciones con fases
'EFS: LO QUITO. QUE SE VEAN TODAS. Sino se quedan
'perdidas.
''''''If (txtText1(0).Text <> "") Then
''''''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
''''''            & "pr01codactuacion=" & txtText1(0).Text & ")"
''''''Else
''''''  If (txtText1(0).Text = "") Then
''''''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
''''''End If
''''''End If

'Salir
If btnButton.Index = 30 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
End If

'4:guardar, 6:imprimir, 8:borrar, 10:cortar, 11:copiar
'12:pegar, 14:deshacer, 26:refrescar, 28:mantenimiento
If (btnButton.Index <> 4 And btnButton.Index <> 6 And _
       btnButton.Index <> 8 And btnButton.Index <> 10 And _
       btnButton.Index <> 11 And btnButton.Index <> 12 And _
       btnButton.Index <> 14 And btnButton.Index <> 26 And _
       btnButton.Index <> 28) Then
    'si se pulsa Abrir Registro que no saque el mensaje de Guardar cambios
  If btnButton.Index = 3 Then
    If intcambioalgo = 0 Then
      objWinInfo.objWinActiveForm.blnChanged = False
      objWinInfo.DataRefresh
      objWinInfo.objWinActiveForm.blnChanged = False
    Else
      objWinInfo.objWinActiveForm.blnChanged = True
    End If
  End If
  
  If btnButton.Index = 18 Then
    objWinInfo.DataRefresh
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  
  
  If btnButton.Index = 16 Then
    localizarymoverse = 1
  End If
  
 If btnButton.Index = 2 Or btnButton.Index = 3 Or btnButton.Index = 30 Then
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
 End If

  intborrar = Borrar_Actuaciones
 ' se quiere borrar la actuaci�n sin fases
 If (intborrar = 1) Then
    'si se pulsa Localizar y seguido nos desplazamos por los registros que no detecte
    'los cambios.
      If localizarymoverse = 1 And (btnButton.Index = 21 Or btnButton.Index = 22 Or _
                                   btnButton.Index = 23 Or btnButton.Index = 24) Then
          localizarymoverse = 0
          If intcambioalgo = 0 Then
            objWinInfo.objWinActiveForm.blnChanged = False
          Else
            objWinInfo.objWinActiveForm.blnChanged = True
          End If
      End If
    
  
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
   If txtText1(0).Text = "" Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        cmddptos.Enabled = False
        cmdfases.Enabled = False
   End If

  ' si se pulsa Nuevo
   If (btnButton.Index = 2) Then
   intposicion = 1
    On Error GoTo Err_Ejecutar
    ' generaci�n autom�tica del c�digo
    sqlstr = "SELECT PR01CODACTUACION_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0) = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    txtText1(0).Locked = True
    txtText1(0).SetFocus
    txtText1(2).SetFocus
    txtdia.Text = 0
    txthora.Text = 0
    txtminuto.Text = 0
    
    cboSSDBCombo1(0).Value = ""

    mnbotonnuevo = 1 'se ha pulsado Nuevo
    cmdcondiciones.Enabled = False
    cmdinteracciones.Enabled = False
    cmdmuestras.Enabled = False
    cmdcuestionario.Enabled = False
    cmdactprev.Enabled = False
    cmdactasoc.Enabled = False
    cmddptos.Enabled = False
    ' se desactiva el bot�n Fases si la actuaci�n no est� guardada en Actuaciones
    sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
             " where PR01CODACTUACION=" & txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
    ncontador = rstA1.rdoColumns(0).Value
    rstA1.Close
    Set rstA1 = Nothing
    If ncontador = 0 Then 'la actuaci�n no est� guardada
      cmdfases.Enabled = False
      cmddptos.Enabled = False
    Else
     cmdfases.Enabled = True
     cmddptos.Enabled = True
    End If
  Else ' no se pulsa Nuevo
     mnbotonnuevo = 0
  End If
  End If
   
  Else
     If (btnButton.Index <> 8) Then  'distinto de Borrar
     Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
     End If
  End If

  'Borrar
  If (btnButton.Index = 8 And txtText1(0).Text <> "") Then
     sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
            " where PR01CODACTUACION=" & txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
    intactu = rstA1.rdoColumns(0).Value
    rstA1.Close
    Set rstA1 = Nothing
    'If (intactu = 1 And intactcurso <> "") Then
    If (intactu = 1) Then
      strWarning = Obtener_Hijos()
      If Len(strWarning) = 0 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      Else
        intResp = MsgBox(strWarning, vbInformation, "Importante")
      End If
    End If
  End If

'no hay actuaci�n en detalle
If intactcurso = "" Then
 'si se pulsa Borrar ya no tiene que hacer el Call pues ya lo ha hecho
 If btnButton.Index <> 8 Then
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
 End If
 ' si se pulsa Nuevo
   If (btnButton.Index = 2) Then
   'se refresca la pantalla para que deje meter el resto de campos
   Call objWinInfo.DataRefresh
    intposicion = 1
    On Error GoTo Err_Ejecutar
    ' generaci�n autom�tica del c�digo
    sqlstr = "SELECT PR01CODACTUACION_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0) = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    txtText1(0).Locked = True
    txtText1(2).SetFocus
    txtText1(0).SetFocus
    txtText1(2).SetFocus
    txtdia.Text = 0
    txthora.Text = 0
    txtminuto.Text = 0
    mnbotonnuevo = 1 'se ha pulsado Nuevo
    cmdcondiciones.Enabled = False
    cmdinteracciones.Enabled = False
    cmdmuestras.Enabled = False
    cmdcuestionario.Enabled = False
    cmdactprev.Enabled = False
    cmdactasoc.Enabled = False
    cmddptos.Enabled = False
    ' se desactiva el bot�n Fases si la actuaci�n no est� guardada
    sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
            " where PR01CODACTUACION=" & txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
    ncontador = rstA1.rdoColumns(0).Value
    rstA1.Close
    Set rstA1 = Nothing
    If ncontador = 0 Then 'la actuaci�n no est� guardada
        cmdfases.Enabled = False
        cmddptos.Enabled = False
    Else
        cmdfases.Enabled = True
        cmddptos.Enabled = True
    End If
  Else ' no se pulsa Nuevo
     mnbotonnuevo = 0
   End If
End If

 ' si se pulsa Guardar se habilita el bot�n Fases
 If (btnButton.Index = 4) Then
    cmdfases.Enabled = True
    '********************************************* IRENE 12-3-98
    sqlstr5 = "select count(*) from PR0500 where pr01codactuacion=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rstA5 = objApp.rdoConnect.OpenResultset(sqlstr5)
    If (rstA5.rdoColumns(0).Value = 0) Then
            cmdfasesimple.Enabled = True
             sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
                       " where PR01CODACTUACION=" & txtText1(0).Text
             Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
             ncontador = rstA1.rdoColumns(0).Value
             If ncontador = 0 Then 'la actuaci�n no est� guardada
                    cmddptos.Enabled = False
             Else
                    cmddptos.Enabled = True
             End If
             '*****************************************************
    End If
    rstA5.Close
    Set rstA5 = Nothing
    sqlstr5 = "select count(*) from pr0200 where pr01codactuacion=" & txtText1(0)
    Set rstA5 = objApp.rdoConnect.OpenResultset(sqlstr5)
     If (rstA5.rdoColumns(0).Value = 0) Then
        cmdfases.Enabled = False
        cmdfasesimple.Enabled = False
     End If
     rstA5.Close
    Set rstA5 = Nothing
 End If
 
If (btnButton.Index <> 3) Then
    'no se ha pulsado Abrir Registro
    mnabrir = 0
Else
    cmdfases.Enabled = False
    cmddptos.Enabled = False
    If mncancelar = 0 Then
       mnabrir = 1
    Else
       mnabrir = 0
    End If
End If

  'Localizar o refrescar
  If (((btnButton.Index = 16) Or (btnButton.Index = 26)) And txtText1(0).Text <> "") Then
        sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
                  " where PR01CODACTUACION=" & txtText1(0).Text
        Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
        intCont = rstA1.rdoColumns(0).Value
        rstA1.Close
        Set rstA1 = Nothing
        
        sqlstr11 = "select count(PR05NUMFASE)  from PR0500" & _
                 " where PR01CODACTUACION=" & txtText1(0).Text
        Set rstA11 = objApp.rdoConnect.OpenResultset(sqlstr11)
        intcont11 = rstA11.rdoColumns(0).Value
        rstA11.Close
        Set rstA11 = Nothing
        
        If intCont = 1 Then
            '**cmdfases.Enabled = True
            cmdcondiciones.Enabled = True
            cmdmuestras.Enabled = True
            cmdcuestionario.Enabled = True
            cmdactprev.Enabled = True
            cmdactasoc.Enabled = True
            cmddptos.Enabled = True
            ' mira si la actuaci�n tiene alg�n departamento realizador
            ' en la tabla Departamentos Realizadores
            sqlstr1 = "SELECT COUNT(*) " _
                    & "FROM PR0200 " _
                    & "WHERE pr01codactuacion = " & txtText1(0)
            Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
            If (rstA1.rdoColumns(0).Value = 0) Then
                cmdinteracciones.Enabled = False
            Else
                cmdinteracciones.Enabled = True
            End If
            rstA1.Close
            Set rstA1 = Nothing
            ' la actuaci�n no tiene fases
            If intcont11 = 0 Then
                cmdcondiciones.Enabled = False
                cmdmuestras.Enabled = False
                cmdcuestionario.Enabled = False
                cmdactprev.Enabled = False
                cmdactasoc.Enabled = False
                '*cmddptos.Enabled = False
                cmdinteracciones.Enabled = False
            End If
            If (intCont = 0 And intcont11 = 0) Then
                cmdfases.Enabled = False
                cmddptos.Enabled = False
            End If
        End If
    End If
    
    
'si se pulsa PrimerRegistro,SiguienteRegistro,RegistroAnterior,UltimoRegistro se
'refresca la pantalla
If btnButton.Index = 21 Or btnButton.Index = 22 Or btnButton.Index = 23 Or btnButton.Index = 24 Then
  If intcambioalgo = 0 Then
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  txtText1(3).SetFocus
  If tabTab1(0).Tab = 0 Then
    If intcambioalgo = 0 Then
      Call objWinInfo.DataRefresh
    End If
  End If
End If

'si Localizar o Filtro para que active los p�jaros
If (btnButton.Index = 16) Or (btnButton.Index = 18) Then
  Call Indicadores
End If

'cuando se pulsa Localizar no se hacen los Change de las cajas de texto as� que
'los campos d�as-horas-minutos no se actualizan bien.Hay que refrescar,para eso se
'hace este c�digo que es como si se llamara a la Toolbar
If btnButton.Index = 16 Then  ' IF LOCALIZAR
'************************************
Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  'If txtText1(0).Text <> "" And btnButton.Index = 4 Then
  If txtText1(0).Text <> "" Then
  'se mira si la actuaci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
 strTiempo = "SELECT count(*) FROM PR0100 where" & _
         " pr01codactuacion=" & txtText1(0).Text & _
         " AND pr01numduracmed=" & TiempoTotal

  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(6), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

'''''para que s�lo se vean las actuaciones con fases
''''If (txtText1(0).Text <> "") Then
''''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
''''            & "pr01codactuacion=" & txtText1(0).Text & ")"
''''Else
''''  If (txtText1(0).Text = "") Then
''''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
''''End If
''''End If
objWinInfo.objWinActiveForm.blnChanged = False
Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)
'****************************************
End If 'END IF LOCALIZAR

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text


 Exit Sub
 
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim sqlstr1 As String
Dim rstA1 As rdoResultset
Dim ncontador As Integer
Dim intactu As Integer
Dim rstA5 As rdoResultset
Dim sqlstr5 As String
Dim strWarning As String
Dim intResp As Integer


'*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  If txtText1(0).Text <> "" And intIndex = 40 Then
  'se mira si la actuaci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
 strTiempo = "SELECT count(*) FROM PR0100 where" & _
         " pr01codactuacion=" & txtText1(0).Text & _
         " AND pr01numduracmed=" & TiempoTotal

  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(6), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    'detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'*********************************************************************


'para que s�lo se vean las actuaciones con fases
'''''If (txtText1(0).Text <> "") Then
'''''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
'''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
'''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
'''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
'''''            & "pr01codactuacion=" & txtText1(0).Text & ")"
'''''Else
'''''  If (txtText1(0).Text = "") Then
'''''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
'''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
'''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
'''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
'''''End If
''End If

'Salir
If (intIndex = 100) Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    Exit Sub
End If

'Guardar
If (intIndex = 40) Then
    
    '********************************************* IRENE 12-3-98
    'cmdfases.Enabled = True
    'cmddptos.Enabled = True
    sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
                       " where PR01CODACTUACION=" & txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
    ncontador = rstA1.rdoColumns(0).Value
    rstA1.Close
    Set rstA1 = Nothing
    If ncontador = 0 Then 'la actuaci�n no est� guardada
        cmdfases.Enabled = False
    Else
        cmdfases.Enabled = True
    End If
    '*****************************************************
    sqlstr5 = "select count(*) from PR0500 where pr01codactuacion=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rstA5 = objApp.rdoConnect.OpenResultset(sqlstr5)
    If (rstA5.rdoColumns(0).Value = 0) Then
            
             '********************************************* IRENE 12-3-98
             'cmddptos.Enabled = True
             'cmdfasesimple.Enabled = True
             sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
                       " where PR01CODACTUACION=" & txtText1(0).Text
             Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
             ncontador = rstA1.rdoColumns(0).Value
             If ncontador = 0 Then 'la actuaci�n no est� guardada
                    cmddptos.Enabled = False
                    cmdfasesimple.Enabled = False
             Else
                    cmddptos.Enabled = True
                    cmdfasesimple.Enabled = True
             End If
             '*****************************************************
    End If
    rstA5.Close
    Set rstA5 = Nothing
End If

'40:guardar, 60:eliminar, 80:imprimir
  If (intIndex <> 40 And intIndex <> 60 And intIndex <> 80) Then
    'si se pulsa Abrir Registro que no saque el mensaje de Guardar cambios
    If intIndex = 20 Then
    If intcambioalgo = 0 Then
      objWinInfo.DataRefresh
      objWinInfo.objWinActiveForm.blnChanged = False
    Else
      objWinInfo.objWinActiveForm.blnChanged = True
    End If
    End If
    
  If intIndex = 10 Or intIndex = 20 Then
    If intcambioalgo = 1 Then
      objWinInfo.objWinActiveForm.blnChanged = True
    End If
  End If
    
    intborrar = Borrar_Actuaciones
    ' se quiere borrar la actuaci�n sin fases
    If intborrar = 1 Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        If txtText1(0).Text = "" Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        cmddptos.Enabled = False
        cmdfases.Enabled = False
        End If
        If intIndex = 16 Then
             intactcurso = ""
        End If
        ' si se pulsa Nuevo
        ' generaci�n autom�tica del c�digo
        If (intIndex = 10) Then
            intposicion = 1
            On Error GoTo Err_Ejecutar
            sqlstr = "SELECT PR01CODACTUACION_SEQUENCE.nextval FROM dual"
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            txtText1(0) = rsta.rdoColumns(0).Value
            rsta.Close
            Set rsta = Nothing
            txtText1(0).Locked = True
            txtText1(0).SetFocus
            txtText1(2).SetFocus
            txtdia.Text = 0
            txthora.Text = 0
            txtminuto.Text = 0
            cboSSDBCombo1(0).Value = ""
    
            mnmenunuevo = 1
            cmdcondiciones.Enabled = False
            cmdinteracciones.Enabled = False
            cmdmuestras.Enabled = False
            cmdcuestionario.Enabled = False
            cmdactprev.Enabled = False
            cmdactasoc.Enabled = False
            cmddptos.Enabled = False
            ' se desactiva el bot�n Fases si la actuaci�n no est� guardada
            sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
                     " where PR01CODACTUACION=" & txtText1(0).Text
            Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
            ncontador = rstA1.rdoColumns(0).Value
            rstA1.Close
            Set rstA1 = Nothing
            If ncontador = 0 Then
                cmdfases.Enabled = False
                cmddptos.Enabled = False
            Else
                cmdfases.Enabled = True
                cmddptos.Enabled = True
            End If
        Else
            mnmenunuevo = 0
    End If
 End If
 Else
    If (intIndex <> 60) Then 'distinto de Eliminar
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
 End If
 
  'Borrar
  If (intIndex = 60) Then
     sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
            " where PR01CODACTUACION=" & txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
    intactu = rstA1.rdoColumns(0).Value
    rstA1.Close
    Set rstA1 = Nothing
    'If (intactu = 1 And intactcurso <> "") Then
    If (intactu = 1) Then
      strWarning = Obtener_Hijos()
      If Len(strWarning) = 0 Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      Else
        intResp = MsgBox(strWarning, vbInformation, "Importante")
      End If
    End If
  End If
 
'no hay actuaci�n en detalle
If intactcurso = "" Then
 'solo si distinto de borrar
 If intIndex <> 60 Then
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
 End If
 ' si se pulsa Nuevo
   If (intIndex = 10) Then
    intposicion = 1
    On Error GoTo Err_Ejecutar
    ' generaci�n autom�tica del c�digo
    sqlstr = "SELECT PR01CODACTUACION_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0) = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    txtText1(0).Locked = True
    txtText1(2).SetFocus
    txtText1(0).SetFocus
    txtText1(2).SetFocus
    txtdia.Text = 0
    txthora.Text = 0
    txtminuto.Text = 0
    mnbotonnuevo = 1 'se ha pulsado Nuevo
    cmdcondiciones.Enabled = False
    cmdinteracciones.Enabled = False
    cmdmuestras.Enabled = False
    cmdcuestionario.Enabled = False
    cmdactprev.Enabled = False
    cmdactasoc.Enabled = False
    cmddptos.Enabled = False
    ' se desactiva el bot�n Fases si la actuaci�n no est� guardada
    sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
            " where PR01CODACTUACION=" & txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
    ncontador = rstA1.rdoColumns(0).Value
    rstA1.Close
    Set rstA1 = Nothing
    If ncontador = 0 Then 'la actuaci�n no est� guardada
        cmdfases.Enabled = False
        cmddptos.Enabled = False
    Else
        cmdfases.Enabled = True
        cmddptos.Enabled = True
    End If
  Else ' no se pulsa Nuevo
     mnbotonnuevo = 0
   End If
End If
 
 ' si se pulsa Guardar se habilita el bot�n Fases
 If (intIndex = 40) Then
   ' cmdfases.Enabled = True
   ' cmddptos.Enabled = True
 End If

 If (intIndex <> 20) Then
    'no se ha pulsado Abrir Registro
    mnabrir = 0
Else
    cmdfases.Enabled = False
    cmddptos.Enabled = False
    If mncancelar = 0 Then
       mnabrir = 1
    Else
       mnabrir = 0
    End If
End If

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

 Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

'''''para que s�lo se vean las actuaciones con fases
''''If (txtText1(0).Text <> "") Then
''''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
''''            & "pr01codactuacion=" & txtText1(0).Text & ")"
''''Else
''''  If (txtText1(0).Text = "") Then
''''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
''''End If
''''End If

Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

'''''para que s�lo se vean las actuaciones con fases
''''If (txtText1(0).Text <> "") Then
''''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
''''            & "pr01codactuacion=" & txtText1(0).Text & ")"
''''Else
''''  If (txtText1(0).Text = "") Then
''''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
''''End If
''''End If

    intborrar = Borrar_Actuaciones
    ' se quiere borrar la actuaci�n sin fases
    If intborrar = 1 Then
        Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
        If txtText1(0).Text = "" Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        cmddptos.Enabled = False
        cmdfases.Enabled = False
        End If
    End If
     
'no hay actuaci�n en detalle
If intactcurso = "" Then
 Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End If

If intIndex = 10 Then
  Call Indicadores
End If

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
Dim sqlstr1 As String
Dim rstA1 As rdoResultset
Dim intCont As Integer
Dim sqlstr11 As String
Dim rstA11 As rdoResultset
Dim intcont11 As Integer


  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

'si se pulsa localizar intposicion=0 porque Localizar no activa el Change
'de txtText1 y entonces llega con valor 1
If intIndex = 10 Then
  intposicion = 0
End If

'*****************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

'cuando se entra en la pantalla y tras refrescarla, al movernos por los registros
'pregunta si se desean guardar los cambios,por esto se pone este c�digo para
'que la primera vez no lo pregunte
If txtText1(0).Text <> "" And (intIndex = 40 Or intIndex = 50 Or _
                               intIndex = 60 Or intIndex = 70) _
                          And (intmoverprimeravez = 0) Then
  intmoverprimeravez = 1
  objWinInfo.objWinActiveForm.blnChanged = False
End If

'*************************************************************************
Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And (intIndex = 40 Or intIndex = 50 Or _
                               intIndex = 60 Or intIndex = 70) Then
strTiempo = "SELECT count(*) FROM PR0100 where" & _
         " pr01codactuacion=" & txtText1(0).Text & _
         " AND pr01numduracmed=" & TiempoTotal

  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(6), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    'detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

'********************************************************************


''''''para que s�lo se vean las actuaciones con fases
'''''If (txtText1(0).Text <> "") Then
'''''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
'''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
'''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
'''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
'''''            & "pr01codactuacion=" & txtText1(0).Text & ")"
'''''Else
'''''  If (txtText1(0).Text = "") Then
'''''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
'''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
'''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
'''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
'''''End If
'''''End If

If intIndex = 10 Then
  localizarymoverse = 1
End If

intborrar = Borrar_Actuaciones
' se quiere borrar la actuaci�n sin fases
If intborrar = 1 Then
      'si se pulsa Localizar y seguido nos desplazamos por los registros que no detecte
      'los cambios.
      If localizarymoverse = 1 And (intIndex = 40 Or intIndex = 50 Or _
                                   intIndex = 60 Or intIndex = 70) Then
          localizarymoverse = 0
          If intcambioalgo = 0 Then
            objWinInfo.objWinActiveForm.blnChanged = False
          Else
            objWinInfo.objWinActiveForm.blnChanged = True
          End If
      End If
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
    If txtText1(0).Text = "" Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        cmddptos.Enabled = False
        cmdfases.Enabled = False
    End If
End If

'no hay actuaci�n en detalle
If intactcurso = "" Then
 Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End If

  'Localizar o restaurar
  If (((intIndex = 10) Or (intIndex = 20)) And txtText1(0).Text <> "") Then
        sqlstr1 = "select count(PR01CODACTUACION) from PR0100" & _
                  " where PR01CODACTUACION=" & txtText1(0).Text
        Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
        intCont = rstA1.rdoColumns(0).Value
        rstA1.Close
        Set rstA1 = Nothing
        
        sqlstr11 = "select count(PR05NUMFASE)  from PR0500" & _
                 " where PR01CODACTUACION=" & txtText1(0).Text
        Set rstA11 = objApp.rdoConnect.OpenResultset(sqlstr11)
        intcont11 = rstA11.rdoColumns(0).Value
        rstA11.Close
        Set rstA11 = Nothing
        
        If intCont = 1 Then
            cmdfases.Enabled = True
            cmdcondiciones.Enabled = True
            cmdmuestras.Enabled = True
            cmdcuestionario.Enabled = True
            cmdactprev.Enabled = True
            cmdactasoc.Enabled = True
            cmddptos.Enabled = True
            ' mira si la actuaci�n tiene alg�n departamento realizador
            sqlstr1 = "SELECT COUNT(*) " _
                    & "FROM PR0200 " _
                    & "WHERE pr01codactuacion = " & txtText1(0)
            Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
            If (rstA1.rdoColumns(0).Value = 0) Then
                cmdinteracciones.Enabled = False
            Else
                cmdinteracciones.Enabled = True
            End If
            rstA1.Close
            Set rstA1 = Nothing
             ' la actuaci�n no tiene fases
            If intcont11 = 0 Then
                cmdcondiciones.Enabled = False
                cmdmuestras.Enabled = False
                cmdcuestionario.Enabled = False
                cmdactprev.Enabled = False
                cmdactasoc.Enabled = False
                cmddptos.Enabled = False
                cmdinteracciones.Enabled = False
            End If
            If (intCont = 0 And intcont11 = 0) Then
                cmdfases.Enabled = False
                cmddptos.Enabled = False
            End If
        End If
    End If

'si se pulsa PrimerRegistro,SiguienteRegistro,RegistroAnterior,UltimoRegistro se
'refresca la pantalla
If intIndex = 40 Or intIndex = 50 Or intIndex = 60 Or intIndex = 70 Then
  If intcambioalgo = 0 Then
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  txtText1(3).SetFocus
  If tabTab1(0).Tab = 0 Then
    If intcambioalgo = 0 Then
      Call objWinInfo.DataRefresh
    End If
  End If
End If

If intIndex = 10 Then
  Call Indicadores
End If


'cuando se pulsa Localizar no se hacen los Change de las cajas de texto as� que
'los campos d�as-horas-minutos no se actualizan bien.Hay que refrescar,para eso se
'hace este c�digo que es como si se llamara a la Toolbar
If intIndex = 10 Then  ' IF LOCALIZAR
'************************************
Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  'If txtText1(0).Text <> "" And btnButton.Index = 4 Then
  If txtText1(0).Text <> "" Then
  'se mira si la actuaci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
 strTiempo = "SELECT count(*) FROM PR0100 where" & _
         " pr01codactuacion=" & txtText1(0).Text & _
         " AND pr01numduracmed=" & TiempoTotal

  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(6), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

''''''para que s�lo se vean las actuaciones con fases
'''''If (txtText1(0).Text <> "") Then
'''''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
'''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
'''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
'''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
'''''            & "pr01codactuacion=" & txtText1(0).Text & ")"
'''''Else
'''''  If (txtText1(0).Text = "") Then
'''''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
'''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
'''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
'''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
'''''End If
'''''End If
objWinInfo.objWinActiveForm.blnChanged = False
Call objWinInfo.WinProcess(cwProcessOptions, 10, 0)
'****************************************
End If 'END IF LOCALIZAR

  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  
  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text
  
'''''  'para que s�lo se vean las actuaciones con fases
'''''If (txtText1(0).Text <> "") Then
'''''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
'''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
'''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
'''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
'''''            & "pr01codactuacion=" & txtText1(0).Text & ")"
'''''Else
'''''  If (txtText1(0).Text = "") Then
'''''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
'''''            & "where pr05numfase in (select pr05numfase from PR1300 " _
'''''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
'''''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
'''''End If
'''''End If

  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  
  'se recoge el c�digo de la actuaci�n que est� en pantalla
  intactcurso = txtText1(0).Text

End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
''  'para que s�lo se vean las actuaciones con fases
''If (txtText1(0).Text <> "") Then
''  objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
''            & "pr01codactuacion=" & txtText1(0).Text & ")"
''Else
''  If (txtText1(0).Text = "") Then
''    objWinInfo.objWinActiveForm.strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
''            & "where pr05numfase in (select pr05numfase from PR1300 " _
''            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
''            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
''End If
''End If

  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)

Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)

End Sub

Private Sub tabTab1_GotFocus(Index As Integer)
Dim strsql As String
Dim rsta As rdoResultset
If Index = 0 Then
' se rellena la descripci�n de la actuaci�n
If (cboSSDBCombo1(0).Text <> "") And (Index = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) Then
strsql = "Select pr12desactividad from PR1200 where pr12codactividad=" & cboSSDBCombo1(0).Value
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
txtdesact.Text = rsta.rdoColumns("pr12desactividad").Value
rsta.Close
Set rsta = Nothing
End If
txtdesact.Locked = True
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Dim intResp As Integer
  
  If intIndex = 1 Then
    If (dtcDateCombo1(0).Text = "") Then
      'A la Fecha Inicio Vigencia se le asigna la fecha del sistema
      dtcDateCombo1(0).Text = Obtener_Fecha_Sistema()
    End If
    If (Not (IsNull(dtcDateCombo1(1).Text))) And (dtcDateCombo1(1).Text <> "") Then
      If DateValue(dtcDateCombo1(1).Text) < DateValue(dtcDateCombo1(0).Text) Then
        intResp = MsgBox("La Fecha Fin Vigencia debe ser mayor que la Fecha Inicio Vigencia", vbExclamation, "Importante")
        dtcDateCombo1(1).Text = ""
        dtcDateCombo1(1).SetFocus
      Else
        Call objWinInfo.CtrlLostFocus
      End If
    End If
  Else
    Call objWinInfo.CtrlLostFocus
  End If
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Dim intResp As Integer
  
  If intIndex = 1 Then
    If (dtcDateCombo1(0).Text = "") Then
      'A la Fecha Inicio Vigencia se le asigna la fecha del sistema
      dtcDateCombo1(0).Text = Obtener_Fecha_Sistema()
    End If
    If (Not (IsNull(dtcDateCombo1(1).Text))) And (dtcDateCombo1(1).Text <> "") Then
      If DateValue(dtcDateCombo1(1).Text) < DateValue(dtcDateCombo1(0).Text) Then
        intResp = MsgBox("La Fecha Fin Vigencia debe ser mayor que la Fecha Inicio Vigencia", vbExclamation, "Importante")
        dtcDateCombo1(1).Text = ""
        dtcDateCombo1(1).SetFocus
      Else
        Call objWinInfo.CtrlDataChange
        intcambioalgo = 1
      End If
    End If
  Else
    Call objWinInfo.CtrlDataChange
    intcambioalgo = 1
  End If
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intregcambiado = 1 Then
    intregcambiado = 0
  Else
  intcambioalgo = 1
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)

Dim strsql As String
Dim rsta As rdoResultset
Call objWinInfo.CtrlGotFocus
' se rellena la descripci�n de la actuaci�n
'If (intIndex = 0) Then
'If (cboSSDBCombo1(0) <> "") And (Not IsNull(cboSSDBCombo1(0))) Then
'strsql = "Select pr12desactividad from PR1200 where pr12codactividad=" & cboSSDBCombo1(0).Value
'Set rstA = objApp.rdoConnect.OpenResultset(strsql)
'txtdesact.Text = rstA.rdoColumns("pr12desactividad").Value
'rstA.Close
'Set rstA = Nothing
'End If
'End If
'txtdesact.Locked = True
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
Dim strsql As String
Dim rsta As rdoResultset

Call objWinInfo.CtrlDataChange
intcambioalgo = 1

' se rellena la descripci�n de la actuaci�n
If (intIndex = 0) Then
  If (cboSSDBCombo1(0).Text <> "") And (Not IsNull(cboSSDBCombo1(0).Text)) Then
    strsql = "Select pr12desactividad from PR1200 where pr12codactividad=" & cboSSDBCombo1(0).Value
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    txtdesact.Text = rsta.rdoColumns("pr12desactividad").Value
    rsta.Close
    Set rsta = Nothing
  End If
End If
txtdesact.Locked = True
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
Dim strsql As String
Dim rsta As rdoResultset
Call objWinInfo.CtrlDataChange
' se rellena la descripci�n de la actuaci�n
If (Index = 0) Then
  If (cboSSDBCombo1(0) <> "") And (Not IsNull(cboSSDBCombo1(0))) Then
    strsql = "Select pr12desactividad from PR1200 where pr12codactividad=" & cboSSDBCombo1(0).Value
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    txtdesact.Text = rsta.rdoColumns("pr12desactividad").Value
    rsta.Close
    Set rsta = Nothing
    End If
End If
txtdesact.Locked = True
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
Dim strsql As String
Dim rsta As rdoResultset
Call objWinInfo.CtrlDataChange
' se rellena la descripci�n de la actuaci�n
If (Index = 0) Then
  If (cboSSDBCombo1(0).Text <> "") And (Not IsNull(cboSSDBCombo1(0).Text)) Then
    strsql = "Select pr12desactividad from PR1200 where pr12codactividad=" & cboSSDBCombo1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    txtdesact.Text = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
  End If
End If
txtdesact.Locked = True
End Sub


Private Sub txtdia_Change()
'  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia.Text) = False Then
    Beep
    txtdia.Text = ""
    txtdia.SetFocus
  End If
  If txtdia.Text <> "" Then
     If txtdia.Text > 6500 Then
      txtdia.Text = 6500
     End If
  End If
End Sub

Private Sub txtdia_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora_Change()
 ' objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora.Text) = False Then
    Beep
    txthora.Text = ""
    txthora.SetFocus
  End If
  If txthora.Text <> "" Then
     If txthora.Text > 23 Then
      txthora.Text = 23
     End If
  End If
End Sub

Private Sub txthora_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto_Change()
'  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto.Text) = False Then
    Beep
    txtminuto.Text = ""
    txtminuto.SetFocus
  End If
  If txtminuto.Text <> "" Then
     If txtminuto.Text > 59 Then
      txtminuto.Text = 59
     End If
  End If
End Sub

Private Sub txtminuto_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Dim strsql As String
  Dim rsta As rdoResultset
  
  Call objWinInfo.CtrlGotFocus
  If (intIndex = 0) Then
    If (cboSSDBCombo1(0).Text <> "") And (Not IsNull(cboSSDBCombo1(0).Text)) Then
      strsql = "Select pr12desactividad from PR1200 where pr12codactividad=" & cboSSDBCombo1(0).Value
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      txtdesact.Text = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
    End If
  End If
  txtdesact.Locked = True
  'para que el cursor se posicione en descripci�n corta
  'If intIndex = 0 Then
  '  txtText1(2).SetFocus
  'End If
  
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
 ' se controla en este evento la activaci�n de los botones
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim rstA1 As rdoResultset
  Dim sqlstr1 As String
  
  If intIndex = 0 Then
    intcambioalgo = 0
  End If

  If intIndex = 0 Then
    Call Indicadores
  End If
  'si el c�digo de la actuaci�n es vac�o se deshabilitan los botones
  If (intIndex = 0) Then
     If (txtText1(0).Text = "") Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        cmddptos.Enabled = False
        cmdfases.Enabled = False
        cmdfasesimple.Enabled = False
     End If
   End If
  
  'intborrar recoge el resultado de la funci�n
  Dim intborrar As Integer
    If intborrar = 1 Then
        Call objWinInfo.CtrlDataChange
        ' si cambia el c�digo de la actuaci�n
        If (intIndex = 0) Then
            ' si no se ha pulsado el bot�n Nuevo
            If (mnbotonnuevo = 0) Then
                ' si no se ha pulsado la opci�n de men� Nuevo
                If (mnmenunuevo = 0) Then
                    If txtText1(0).Text <> "" Then
                        ' se mira el n� de fases
                        sqlstr = "select count(*) from PR0500" & _
                        " where PR01CODACTUACION=" & txtText1(0).Text
                        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                        ' ncontador contiene el n�mero de fases de la actuaci�n
                        ncontador = rsta.rdoColumns(0).Value
                        rsta.Close
                        Set rsta = Nothing
                    Else
                        ncontador = 0
                    End If
                End If
            End If
      End If
End If

If txtText1(0).Text <> "" Then
    ' cu�ntas fases tiene la actuaci�n
    sqlstr = "select count(*) from PR0500" & _
    " where PR01CODACTUACION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' ncontador contiene el n�mero de fases de la actuaci�n
    ncontador = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
Else
    ncontador = 0
End If


If (ncontador = 0) Then
        cmdcondiciones.Enabled = False
        cmdinteracciones.Enabled = False
        cmdmuestras.Enabled = False
        cmdcuestionario.Enabled = False
        cmdactprev.Enabled = False
        cmdactasoc.Enabled = False
        cmddptos.Enabled = False
Else
        cmdfases.Enabled = True
        cmdcondiciones.Enabled = True
        cmdmuestras.Enabled = True
        cmdcuestionario.Enabled = True
        cmdactprev.Enabled = True
        cmdactasoc.Enabled = True
        cmddptos.Enabled = True
        ' mira si la actuaci�n tiene alg�n departamento realizador
        sqlstr1 = "SELECT COUNT(*) " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(0)
       Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
       If (rstA1.rdoColumns(0).Value = 0) Then
        cmdinteracciones.Enabled = False
       Else
        cmdinteracciones.Enabled = True
       End If
       rstA1.Close
       Set rstA1 = Nothing
End If

 'si cambia otra caja de texto que no es la del c�digo
If (intIndex <> 0) Then
    Call objWinInfo.CtrlDataChange
End If

'****************************************************************************
  'cuando cambie la columna invisible Tiempo que se actualice dias,horas,minutos
  If intIndex = 6 Then
    If txtText1(6).Text = "" Then
      txtdia.Text = 0
      txthora.Text = 0
      txtminuto.Text = 0
    Else
      txtdia.Text = txtText1(6).Text \ 1440                 'd�as
      txthora.Text = (txtText1(6).Text Mod 1440) \ 60       'horas
      txtminuto.Text = (txtText1(6).Text Mod 1440) Mod 60   'minutos
    End If
  End If
  '****************************************************************************

' para controlar que al pulsar nuevo el cursor se quede en Desc.Corta
If intposicion = 1 Then
    intposicion = 0
    Call objWinInfo.FormChangeStatus(cwModeSingleAddRest)
    objWinInfo.objWinActiveForm.blnChanged = False
End If

End Sub


'---------------------------------------------------------
'
' FUNCION Borrar_Actuaciones
' borra las actuaciones que no tienen ninguna fase
'
'---------------------------------------------------------

Private Function Borrar_Actuaciones() As Integer
  Dim sqlstring1 As String
  Dim rstA1 As rdoResultset
  Dim sqlstring2 As String
  Dim rstA2 As rdoResultset
  Dim sqlstring3 As String
  Dim rstA3 As rdoResultset
  Dim strrespuesta As String
  Dim strrespuesta1 As String
  Dim blnrespuesta As Integer
  Dim sqlstr4 As String
  Dim rstdep As rdoResultset
  Dim strdep As String
  
  On Error GoTo Err_Ejecutar
  Borrar_Actuaciones = 0
  intactcurso = txtText1(0).Text
  If intactcurso <> "" Then
    sqlstring3 = "select count(*) from PR0100 " & _
             "where PR01CODACTUACION=" & intactcurso
    Set rstA3 = objApp.rdoConnect.OpenResultset(sqlstring3)
    'la actuaci�n est� guardada, se mira en la tabla Fases
    sqlstring1 = "select count(*) from PR0500 " & _
             "where PR01CODACTUACION=" & intactcurso
    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstring1)
    ' se mira el n� de Fases
    If (rstA1.rdoColumns(0).Value = 0 And mnabrir = 0) Then
      If Comprobar_Relaciones() Then
             strrespuesta = MsgBox("La Actuaci�n con C�digo = " & intactcurso & _
             " no tiene definida ninguna Fase." & Chr(13) & _
             "� Desea borrar dicha actuaci�n ?" & Chr(13) & _
             "Si pulsa SI la eliminar�, si pulsa NO deber� " & _
             "asociarle al menos una Fase. ", 36, "Actuaciones")
             If strrespuesta = vbYes Then
                 'eliminar la actuaci�n si est� guardada
                 If rstA3.rdoColumns(0).Value = 1 Then
                    ' se hace DELETE para que no saque el mensaje de
                    ' confirmaci�n de borrado
                    
                    strdep = "select * from pr0200 where pr01codactuacion=" & txtText1(0)
                    Set rstdep = objApp.rdoConnect.OpenResultset(strdep)
                    While Not rstdep.EOF
                     strdep = "delete from pr0200 where pr01codactuacion=" & txtText1(0)
                     objApp.rdoConnect.Execute strdep, 64
                     rstdep.MoveNext
                    Wend
                    rstdep.Close
                    Set rstdep = Nothing
                    
                    sqlstr4 = "DELETE FROM PR0100 " _
                             & "WHERE (pr01codactuacion = " & txtText1(0) & ")"
                             
                    On Error GoTo Err_Ejecutar
                    objApp.rdoConnect.Execute sqlstr4, 64
                    objWinInfo.objWinActiveForm.blnChanged = False
                    intactcurso = txtText1(0).Text
                End If
                intdelete = 1
                mncancelar = 0
                objWinInfo.objWinActiveForm.blnChanged = False
            Else
                blnrespuesta = 1
                mncancelar = 1
            End If
      End If
    Else
        mncancelar = 0
    End If
    If (blnrespuesta = 0) Then
        ' he borrado la actuacion
        Borrar_Actuaciones = 1
    End If
    intactcurso = txtText1(0).Text

    intactcurso = txtText1(0).Text
    rstA1.Close
    Set rstA1 = Nothing
    rstA3.Close
    Set rstA3 = Nothing
End If

Exit Function
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Function
End Function


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command Button
' -----------------------------------------------

Private Sub cmddptos_Click()
   cmddptos.Enabled = False
  ' Se activa el bot�n Guardar
   Call objWinInfo.WinProcess(6, 4, 0)
   frmdefdptos.txtactText1(0).Text = txtText1(0).Text
   frmdefdptos.txtactText1(1).Text = txtText1(2).Text
   Call objsecurity.LaunchProcess("PR0128")
   'Load frmdefdptos
   'frmdefdptos.Show (vbModal)
   'Unload frmdefdptos
   'Set frmdefdptos = Nothing
   intposicion = 0
   cmddptos.Enabled = True
End Sub

Private Sub cmdactasoc_Click()
   cmdactasoc.Enabled = False
   ' Se activa el bot�n Guardar
   Call objWinInfo.WinProcess(6, 4, 0)
   frmActAsociadas.txtactText1(0).Text = txtText1(0).Text
   frmActAsociadas.txtactText1(1).Text = txtText1(2).Text
   'Load frmActAsociadas
   'frmActAsociadas.Show (vbModal)
   'Unload frmActAsociadas
   'Set frmActAsociadas = Nothing
   Call objsecurity.LaunchProcess("PR0107")
   intposicion = 0
   cmdactasoc.Enabled = True
End Sub

Private Sub cmdactprev_Click()
   cmdactprev.Enabled = False
   ' Se activa el bot�n Guardar
   Call objWinInfo.WinProcess(6, 4, 0)
   frmActPrevisibles.txtactText1(0).Text = txtText1(0).Text
   frmActPrevisibles.txtactText1(1).Text = txtText1(2).Text
   'Load frmActPrevisibles
   'frmActPrevisibles.Show (vbModal)
   'Unload frmActPrevisibles
   'Set frmActPrevisibles = Nothing
   Call objsecurity.LaunchProcess("PR0108")
   intposicion = 0
   cmdactprev.Enabled = True
End Sub

Private Sub cmdcondiciones_Click()
   cmdcondiciones.Enabled = False
   ' Se activa el bot�n Guardar
   Call objWinInfo.WinProcess(6, 4, 0)
   'Load frmCondiciones
   'frmCondiciones.Show (vbModal)
   'Unload frmCondiciones
   'Set frmCondiciones = Nothing
   Call objsecurity.LaunchProcess("PR0104")
   intposicion = 0
   cmdcondiciones.Enabled = True
End Sub

Private Sub cmdcuestionario_Click()
   cmdcuestionario.Enabled = False
   ' Se activa el bot�n Guardar
   Call objWinInfo.WinProcess(6, 4, 0)
   frmCuestActuacion.txtCuestext1(0).Text = txtText1(0).Text
   frmCuestActuacion.txtCuestext1(1).Text = txtText1(2).Text
   'Load frmCuestActuacion
   'frmCuestActuacion.Show (vbModal)
   'Unload frmCuestActuacion
   'Set frmCuestActuacion = Nothing
   Call objsecurity.LaunchProcess("PR0109")
   intposicion = 0
   cmdcuestionario.Enabled = True
End Sub

Private Sub cmdinteracciones_Click()
      cmdinteracciones.Enabled = False
      ' Se activa el bot�n Guardar
      Call objWinInfo.WinProcess(6, 4, 0)
      'Load frmmenuinterac2
      'Call frmmenuinterac2.Show(vbModal)
      Call objsecurity.LaunchProcess("PR0111")
      'Unload frmmenuinterac2
      'Set frmmenuinterac2 = Nothing
      intposicion = 0
      cmdinteracciones.Enabled = True
End Sub

Private Sub cmdmuestras_Click()
   cmdmuestras.Enabled = False
   ' Se activa el bot�n Guardar
   Call objWinInfo.WinProcess(6, 4, 0)
   'Load frmmuestra
   'frmmuestra.Show (vbModal)
   'Unload frmmuestra
   'Set frmmuestra = Nothing
   Call objsecurity.LaunchProcess("PR0102")
   intposicion = 0
   cmdmuestras.Enabled = True
End Sub
Private Sub cmdfases_Click()
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim intCont As Integer
  Dim strmensaje As String
  Dim rstdep As rdoResultset
  Dim strdep As String

  gstrWhereSinFases = objWinInfo.objWinActiveForm.strWhere
 
   cmdfases.Enabled = False
   cmdfasesimple.Enabled = False
   
    ' se mira que la actuaci�n est� guardada antes de ir a sus fases. Puede no estarlo
    ' si se est� en una actuaci�n guardada y a�n sin fases y se pulsa Localizar,
    ' respondemos que queremos borrar la actuaci�n y en la ventana de Localizar
    ' inmediatamente se da a Cerrar y la actuaci�n sin fases sigue en pantalla pero
    ' ya no est� guardada y el bot�n Fases sigue activo
    If (txtText1(0).Text <> "") Then
        sqlstr = "select count(PR01CODACTUACION) from PR0100" & _
                  " where PR01CODACTUACION=" & txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        intCont = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
        
        If intCont = 1 Then
        
     
        strdep = "select count(*) from pr0200 where pr01codactuacion=" & txtText1(0)
        Set rstdep = objApp.rdoConnect.OpenResultset(strdep)
        If rstdep.rdoColumns(0).Value = 0 Then
          strmensaje = MsgBox("El departamento realizador es obligatorio", vbCritical, _
                            "Departamento Realizador Obligatorio")
        Else
              
            Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
            frmdeffases.txtfaseText1(0).Text = txtText1(0).Text
            frmdeffases.txtfaseText1(2).Text = txtText1(2).Text
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            Call objsecurity.LaunchProcess("PR0123")
            'Load frmdeffases
            'frmdeffases.Show (vbModal)
            'Unload frmdeffases
            'Set frmdeffases = Nothing
            cmdfases.Enabled = True
            cmddptos.Enabled = True
         End If
        rstdep.Close
        Set rstdep = Nothing
        Else
            strmensaje = MsgBox("No puede definir ninguna Fase para la Actuaci�n : " _
                          & txtText1(0).Text & " puesto que no est� guardada.", _
                          vbCritical, "Definici�n de Actuaciones")
             Call objWinInfo.WinProcess(6, 26, 0)
        End If
   End If
   
  intposicion = 0
  
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.objWinActiveForm.strWhere = gstrWhereSinFases
  
  Call Indicadores
  strdep = "select count(*) from pr0500 where pr01codactuacion=" & txtText1(0)
  Set rstdep = objApp.rdoConnect.OpenResultset(strdep)
  If rstdep.rdoColumns(0).Value = 0 Then
     cmdfasesimple.Enabled = True
  Else
     cmdfasesimple.Enabled = False
  End If
  rstdep.Close
  Set rstdep = Nothing
  End Sub

Private Function Obtener_Hijos() As String
'JMRL 2/12/97
'Esta funci�n se crea para evitar el error 4002 Imposible borrar registros que da la
'CodeWizard cuado se intenta borrar un registro referenciado en otras tablas.
'
'Obtener_Hijos devuelve la lista de tablas en las que se hace referencia al registro
'que queremos borrar. Si NO se hace referencia en ninguna tabla al registro que se quiere
'borrar, Obtener_Hijos devuelve vacio ""
  
  Dim rstH As rdoResultset
  Dim strSelect As String
  Dim strmensaje As String
  Dim blnConprobado1 As Boolean
  Dim blnConprobado2 As Boolean
  Dim blnTieneHijos As Boolean
  
  strmensaje = "La actuaci�n NO puede ser borrada por: " & Chr(13) & Chr(13)
  blnConprobado1 = False
  blnConprobado2 = False
  blnTieneHijos = False
  
  'Se comprueba si se hace referencia en la tabla PR0200 DEPARTAMENTO REALIZADOR
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR0200 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La actuaci�n TIENE DEPARTAMENTO REALIZADOR." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR0300 ACTUACIONES PEDIDAS
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR0300 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La actuaci�n HA SIDO PEDIDA." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR0400 ACTUACIONES PLANIFICADAS
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR0400 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strMensaje = strMensaje & "� La actuaci�n HA SIDO PEDIDA." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.Close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR0500 FASES DE LA ACTUACION
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR0500 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La actuaci�n TIENE FASES." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR1100 RECURSO VALIDOS
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR1100 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strMensaje = strMensaje & "� La actuaci�n TIENE RECURSOS." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.Close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR1300 TIPOS DE RECURSOS DE LA FASE
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR1300 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strMensaje = strMensaje & "� La actuaci�n TIENE TIPOS DE RECURSOS." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR1700 ACTUACIONES DE UN GRUPO
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR1700 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La actuaci�n ESTA EN ALGUN GRUPO." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR1800 INTERACCIONES
  'PR01CODACTUACION y PR01CODACTUACION_DES
  'strSelect = "SELECT COUNT(*) FROM PR1800 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strmensaje = strmensaje & "� La actuaci�n INTERACCIONA con otras actuaciones." & Chr(13)
  '  blnConprobado1 = True
  '  blnTieneHijos = True
  'End If
  'rstH.Close
  'Set rstH = Nothing
  
  'strSelect = "SELECT COUNT(*) FROM PR1800 WHERE PR01CODACTUACION_DES = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strmensaje = strmensaje & "� Otras actuaciones INTERACCIONAN con ella." & Chr(13)
  '  blnConprobado2 = True
  '  blnTieneHijos = True
  'End If
  'rstH.Close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR2000 INTERACCIONES
  'PR01CODACTUACION y PR01CODACTUACION_DES
  strSelect = "SELECT COUNT(*) FROM PR2000 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) And (blnConprobado1 = False) Then
    strmensaje = strmensaje & "� La actuaci�n INTERACCIONA con otras actuaciones." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  strSelect = "SELECT COUNT(*) FROM PR2000 WHERE PR01CODACTUACION_DES = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) And (blnConprobado1 = False) Then
    strmensaje = strmensaje & "� Otras actuaciones INTERACCIONAN con ella." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR2300 CONDICIONES DE LA ACTUACION
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR2300 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE CONDICIONES." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR2500 MUESTRAS
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR2500 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE MUESTRAS." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si e hace referencia en la tabla PR2600 CUESTIONARIO MUESTRA
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR2600 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If (rstH.rdoColumns(0).Value > 0) Then
  '  strMensaje = strMensaje & "� La actuaci�n TIENE MUESTRAS CON CUESTIONARIO." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR2900 CUESTIONARIO ACTUACION
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR2900 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE CUESTIONARIO." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3100 ACTUACIONES ASOCIADAS
  'PR01CODACTUACION y PR01CODACTUACION_ASO
  strSelect = "SELECT COUNT(*) FROM PR3100 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE ACTUACIONES ASOCIADAS." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  strSelect = "SELECT COUNT(*) FROM PR3100 WHERE PR01CODACTUACION_ASO = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� Otras actuaciones INCLUYEN ESTA ACTUACION COMO ASOCIADA." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3200 ACTUACIONES PREVISIBLES
  'PR01CODACTUACION y PR01CODACTUACION_PRE
  strSelect = "SELECT COUNT(*) FROM PR3200 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE ACTUACIONES PREVISIBLES." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  strSelect = "SELECT COUNT(*) FROM PR3200 WHERE PR32CODACTUACION_PRE = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� Otras actuaciones INCLUYEN ESTA ACTUACION COMO PREVISIBLE." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3400 ACTUACIONES DE UN PAQUETE
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR3400 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n ESTA EN ALGUN PAQUETE." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3600 ACTUACIONES DE UN PROTOCOLO
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR3600 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n ESTA EN ALGUN PROTOCOLO." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'los siguientes casos no se validan por que la actuaci�n ya ha sido pedida
  'Se comprueba si se hace referencia en la tabla PR3800 CONDICIONES DE LA PETICION
  'PR01CODACTUACION
  'Se comprueba si se hace referencia en la tabla PR4400 PRINCIPALES
  'PR01CODACTUACION
  'Se comprueba si se hace referencia en la tabla AG0100 ACTUACION POR FRANJA
  'PR01CODACTUACION
  'Se comprueba si se hace referencia en la tabla CI0200 ACTUACION PATOLOGIA
  'PR01CODACTUACION
  
  If blnTieneHijos = True Then
    Obtener_Hijos = strmensaje
  Else
     Obtener_Hijos = ""
  End If
  
End Function

Private Function Usada_Por() As String
'JMRL 3/12/97
'Esta funci�n se crea para evitar el error 4002 Imposible borrar registros que da la
'CodeWizard cuado se intenta borrar un registro referenciado en otras tablas.
'
'Usada_Por devuelve la lista de tablas en las que se hace referencia al registro
'que queremos borrar. Si NO se hace referencia en ninguna tabla al registro que se quiere
'borrar, Usada_Por devuelve vacio ""
'Usada_Por se usa en la funci�n Comprobar_Relaciones
  
  Dim rstH As rdoResultset
  Dim strSelect As String
  Dim strmensaje As String
  Dim blnConprobado1 As Boolean
  Dim blnConprobado2 As Boolean
  Dim blnTieneHijos As Boolean
  
  strmensaje = "La actuaci�n DEBE TENER alguna FASE porque: " & Chr(13) & Chr(13)
  blnConprobado1 = False
  blnConprobado2 = False
  blnTieneHijos = False
  
  'Se comprueba si se hace referencia en la tabla PR0200 DEPARTAMENTO REALIZADOR
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR0200 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strMensaje = strMensaje & "� La actuaci�n TIENE DEPARTAMENTO REALIZADOR." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.Close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR0300 ACTUACIONES PEDIDAS
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR0300 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La actuaci�n ESTA PEDIDA." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR0400 ACTUACIONES PLANIFICADAS
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR0400 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strMensaje = strMensaje & "� La actuaci�n HA SIDO PEDIDA." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.Close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR0500 FASES DE LA ACTUACION
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR0500 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strMensaje = strMensaje & "� La actuaci�n TIENE FASES." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR1100 RECURSO VALIDOS
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR1100 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strMensaje = strMensaje & "� La actuaci�n TIENE RECURSOS." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR1300 TIPOS DE RECURSOS DE LA FASE
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR1300 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If rstH.rdoColumns(0).Value > 0 Then
  '  strMensaje = strMensaje & "� La actuaci�n TIENE TIPOS DE RECURSOS." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR1700 ACTUACIONES DE UN GRUPO
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR1700 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La actuaci�n ESTA EN ALGUN GRUPO." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR1800 INTERACCIONES
  'PR01CODACTUACION y PR01CODACTUACION_DES
  strSelect = "SELECT COUNT(*) FROM PR1800 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La actuaci�n INTERACCIONA con otras actuaciones." & Chr(13)
    blnConprobado1 = True
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  strSelect = "SELECT COUNT(*) FROM PR1800 WHERE PR01CODACTUACION_DES = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� Otras actuaciones INTERACCIONAN con ella." & Chr(13)
    blnConprobado2 = True
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR2000 INTERACCIONES
  'PR01CODACTUACION y PR01CODACTUACION_DES
  strSelect = "SELECT COUNT(*) FROM PR2000 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) And (blnConprobado1 = False) Then
    strmensaje = strmensaje & "� La actuaci�n INTERACCIONA con otras actuaciones." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  strSelect = "SELECT COUNT(*) FROM PR2000 WHERE PR01CODACTUACION_DES = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) And (blnConprobado1 = False) Then
    strmensaje = strmensaje & "� Otras actuaciones INTERACCIONAN con ella." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR2300 CONDICIONES DE LA ACTUACION
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR2300 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE CONDICIONES." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR2500 MUESTRAS
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR2500 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE MUESTRAS." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si e hace referencia en la tabla PR2600 CUESTIONARIO MUESTRA
  'PR01CODACTUACION
  'strSelect = "SELECT COUNT(*) FROM PR2600 WHERE PR01CODACTUACION = " & txtText1(0).Text
  'Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  'If (rstH.rdoColumns(0).Value > 0) Then
  '  strMensaje = strMensaje & "� La actuaci�n TIENE MUESTRAS CON CUESTIONARIO." & Chr(13)
  '  blnTieneHijos = True
  'End If
  'rstH.close
  'Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR2900 CUESTIONARIO ACTUACION
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR2900 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE CUESTIONARIO." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3100 ACTUACIONES ASOCIADAS
  'PR01CODACTUACION y PR01CODACTUACION_ASO
  strSelect = "SELECT COUNT(*) FROM PR3100 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE ACTUACIONES ASOCIADAS." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  strSelect = "SELECT COUNT(*) FROM PR3100 WHERE PR01CODACTUACION_ASO = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� Otras actuaciones INCLUYEN ESTA ACTUACION COMO ASOCIADA." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3200 ACTUACIONES PREVISIBLES
  'PR01CODACTUACION y PR01CODACTUACION_PRE
  strSelect = "SELECT COUNT(*) FROM PR3200 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n TIENE ACTUACIONES PREVISIBLES." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  strSelect = "SELECT COUNT(*) FROM PR3200 WHERE PR32CODACTUACION_PRE = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� Otras actuaciones INCLUYEN ESTA ACTUACION COMO PREVISIBLE." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3400 ACTUACIONES DE UN PAQUETE
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR3400 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n ESTA EN ALGUN PAQUETE." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3600 ACTUACIONES DE UN PROTOCOLO
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR3600 WHERE PR01CODACTUACION = " & txtText1(0).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n ESTA EN ALGUN PROTOCOLO." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'los siguientes casos no se validan por que la actuaci�n ya ha sido pedida
  'Se comprueba si se hace referencia en la tabla PR3800 CONDICIONES DE LA PETICION
  'PR01CODACTUACION
  'Se comprueba si se hace referencia en la tabla PR4400 PRINCIPALES
  'PR01CODACTUACION
  'Se comprueba si se hace referencia en la tabla AG0100 ACTUACION POR FRANJA
  'PR01CODACTUACION
  'Se comprueba si se hace referencia en la tabla CI0200 ACTUACION PATOLOGIA
  'PR01CODACTUACION
  
  If blnTieneHijos = True Then
    Usada_Por = strmensaje
  Else
    Usada_Por = ""
  End If
  
End Function


Private Function Comprobar_Relaciones() As Boolean
'JMRL 3/12/97
'Comprobar_Relaciones es una funci�n que devuelve true se la actuaci�n que se quiere borrar
'solo est� relacionada conla tabla PR0200; tanbi�n devuelve true si la actuaci�n solo aparece
'en la tabla PR0100.
'Comprobar_Relaciones se usa en el procedimiento Borrar_Actuaciones
  Dim strWarning As String
  Dim intResp As Integer
  
  strWarning = Usada_Por()
  If Len(strWarning) = 0 Then
    Comprobar_Relaciones = True
  Else
    intResp = MsgBox(strWarning, vbInformation, "Importante")
    Comprobar_Relaciones = False
  End If
End Function

Private Function Obtener_Fecha_Sistema() As String
'JMRL 3/12/97
'Obtener_Fecha_Sistema obtiene la fecha del sistema
  Dim rstfecha As rdoResultset
  
  'se coge la fecha del sistema
  Set rstfecha = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY')from dual")
  Obtener_Fecha_Sistema = rstfecha.rdoColumns(0).Value
rstfecha.Close
Set rstfecha = Nothing
End Function
Private Sub Calcular_Tiempo(d, h, m)
'procedimiento que transforma los d�as,horas y minutos a Minutos

If d = "" Or IsNumeric(d) = False Then
  d = 0
Else
  If d > 6500 Then
    d = 6500
  End If
End If
If h = "" Or IsNumeric(h) = False Then
  h = 0
Else
  If h > 23 Then
    h = 23
  End If
End If
If m = "" Or IsNumeric(m) = False Then
  m = 0
Else
  If m > 59 Then
    m = 59
  End If
End If

TiempoTotal = (d * 1440) + (h * 60) + m
  
End Sub

