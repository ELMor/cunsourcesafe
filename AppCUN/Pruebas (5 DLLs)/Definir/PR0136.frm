VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "cOMCTL32.OCX"
Begin VB.Form frmfasesimple 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones. Tipos de Recurso "
   ClientHeight    =   4485
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0136.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdrecursos 
      Caption         =   "Recursos"
      Height          =   375
      Left            =   10200
      TabIndex        =   15
      Top             =   5160
      Width           =   1575
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipos de Recurso Seleccionados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Index           =   1
      Left            =   240
      TabIndex        =   0
      Top             =   2280
      Width           =   9855
      Begin TabDlg.SSTab tabTab1 
         Height          =   5100
         Index           =   1
         Left            =   240
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   480
         Width           =   9495
         _ExtentX        =   16748
         _ExtentY        =   8996
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0136.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(10)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(11)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(12)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(13)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(14)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(16)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(15)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(17)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(18)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(5)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(7)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(8)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(2)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(9)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "chkCheck1(1)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(4)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(6)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(8)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "chkCheck1(2)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(7)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(9)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(2)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(5)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(10)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(0)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "Frame1"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtdia"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txthora"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtminuto"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(1)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).ControlCount=   33
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0136.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   1
            Left            =   240
            TabIndex        =   56
            Tag             =   "N� unidades del tipo de recurso"
            Top             =   2880
            Width           =   420
         End
         Begin VB.TextBox txtminuto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   5520
            MaxLength       =   2
            TabIndex        =   7
            Top             =   1560
            Width           =   615
         End
         Begin VB.TextBox txthora 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   4200
            MaxLength       =   2
            TabIndex        =   6
            Top             =   1560
            Width           =   615
         End
         Begin VB.TextBox txtdia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   333
            Left            =   3000
            MaxLength       =   2
            TabIndex        =   5
            Top             =   1560
            Width           =   615
         End
         Begin VB.Frame Frame1 
            Caption         =   "Tiempo de Ocupaci�n del Paciente durante la Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1335
            Left            =   240
            TabIndex        =   48
            Top             =   3480
            Width           =   5775
            Begin VB.TextBox txtdia1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               Height          =   333
               Left            =   240
               MaxLength       =   2
               TabIndex        =   12
               Top             =   600
               Width           =   615
            End
            Begin VB.TextBox txthora1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               Height          =   330
               Left            =   1440
               MaxLength       =   2
               TabIndex        =   13
               Top             =   600
               Width           =   615
            End
            Begin VB.TextBox txtminuto1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               Height          =   330
               Left            =   2760
               MaxLength       =   2
               TabIndex        =   14
               Top             =   600
               Width           =   615
            End
            Begin VB.TextBox txtactText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               Height          =   330
               Index           =   4
               Left            =   4320
               MaxLength       =   5
               TabIndex        =   17
               Tag             =   "Tiempo de Ocupaci�n del Paciente"
               Top             =   240
               Visible         =   0   'False
               Width           =   650
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "d�as"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   960
               TabIndex        =   55
               Top             =   720
               Width           =   390
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "horas"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   2160
               TabIndex        =   54
               Top             =   720
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "minutos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   3480
               TabIndex        =   53
               Top             =   720
               Width           =   660
            End
            Begin VB.Label lblactLabel1 
               Caption         =   "Minutos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   4920
               TabIndex        =   49
               Top             =   240
               Visible         =   0   'False
               Width           =   735
            End
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   0
            Left            =   3000
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Descripci�n Departamento"
            Top             =   2880
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   10
            Left            =   3480
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Descripci�n Tipo Recurso"
            Top             =   720
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR13NUMTIEMPREC"
            Height          =   330
            Index           =   5
            Left            =   6720
            TabIndex        =   16
            Tag             =   "Tiempo de Ocupaci�n"
            Top             =   3480
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR13NUMUNIREC"
            Height          =   330
            Index           =   2
            Left            =   240
            TabIndex        =   4
            Tag             =   "N� unidades del tipo de recurso"
            Top             =   1560
            Width           =   420
         End
         Begin VB.TextBox txtText1 
            DataField       =   "PR05NUMFASE"
            Height          =   375
            Index           =   9
            Left            =   6720
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "N�mero de Fase"
            Top             =   4080
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            DataField       =   "PR01CODACTUACION"
            Height          =   375
            Index           =   7
            Left            =   6720
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "C�digo de Actuaci�n"
            Top             =   4560
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "PR13INDPREFEREN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   2
            Left            =   240
            TabIndex        =   9
            Tag             =   "�Preferente?"
            Top             =   2160
            Width           =   225
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR13NUMNECESID"
            Height          =   330
            HelpContextID   =   30101
            Index           =   8
            Left            =   240
            TabIndex        =   1
            Tag             =   "N� de Necesidad"
            Top             =   720
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG14CODTIPRECU"
            Height          =   330
            HelpContextID   =   30101
            Index           =   6
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   2
            Tag             =   "C�digo Tipo Recurso"
            Top             =   720
            Width           =   420
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR13NUMMINDESF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   7440
            TabIndex        =   8
            Tag             =   "M�nimo Desfase"
            Top             =   1560
            Width           =   612
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "PR13INDPLANIF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   3000
            TabIndex        =   10
            Tag             =   "�Planificable?"
            Top             =   2160
            Width           =   225
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   1
            Left            =   -74910
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   5054
            _StockProps     =   79
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4785
            Index           =   2
            Left            =   -74880
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   120
            Width           =   8895
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15690
            _ExtentY        =   8440
            _StockProps     =   79
            Caption         =   "TIPOS DE RECURSO SELECCIONADOS"
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   6240
            TabIndex        =   52
            Top             =   1680
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   4920
            TabIndex        =   51
            Top             =   1680
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   3720
            TabIndex        =   50
            Top             =   1680
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�mero Fase"
            Height          =   375
            Index           =   8
            Left            =   7680
            TabIndex        =   47
            Top             =   4200
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Actuaci�n"
            Height          =   255
            Index           =   7
            Left            =   7680
            TabIndex        =   46
            Top             =   4680
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   8160
            TabIndex        =   45
            Top             =   1680
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   7440
            TabIndex        =   44
            Top             =   3600
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   3000
            TabIndex        =   43
            Top             =   2640
            Width           =   2895
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Tipo de Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   3480
            TabIndex        =   42
            Top             =   480
            Width           =   2775
         End
         Begin VB.Label lblLabel1 
            Caption         =   "�Preferente?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   480
            TabIndex        =   41
            Top             =   2160
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            Caption         =   "�Planificable?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   3240
            TabIndex        =   40
            Top             =   2160
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   240
            TabIndex        =   38
            Top             =   480
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Tipo Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   1680
            TabIndex        =   37
            Top             =   480
            Width           =   1545
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tiempo Ocupaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   3000
            TabIndex        =   36
            Top             =   1320
            Width           =   1605
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "M�nimo Desfase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   7440
            TabIndex        =   35
            Top             =   1320
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   240
            TabIndex        =   34
            Top             =   2685
            Width           =   1845
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Unidades Tipo Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   240
            TabIndex        =   33
            Top             =   1320
            Width           =   2010
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaci�n - Fase "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   2
      Left            =   240
      TabIndex        =   18
      Top             =   480
      Width           =   9855
      Begin VB.TextBox txtactText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR01CODACTUACION"
         Height          =   330
         Index           =   0
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Tag             =   "C�digo de la Actuaci�n"
         Top             =   600
         Width           =   1092
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR01DESCORTA"
         Height          =   330
         Index           =   1
         Left            =   2640
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   600
         Width           =   5400
      End
      Begin VB.TextBox txtactText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR05NUMFASE"
         Height          =   330
         Index           =   2
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Tag             =   "C�digo de la Fase"
         Top             =   1200
         Width           =   612
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR05DESFASE"
         Height          =   330
         Index           =   3
         Left            =   2640
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   22
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Fase"
         Top             =   1200
         Width           =   5400
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "C�digo Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   4
         Left            =   240
         TabIndex        =   30
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "Descripci�n Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   2640
         TabIndex        =   29
         Top             =   360
         Width           =   2535
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "C�digo Fase"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   28
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "Descripci�n Fase"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   2640
         TabIndex        =   27
         Top             =   960
         Width           =   1815
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   25
      Top             =   4200
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmfasesimple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00124.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCI�N: Descripci�n de los tipos de recurso de cada fase        *
'* ARGUMENTOS:  PR01CODACTUACION, PR01DESCORTA, PR05NUMFASE,            *
'*              PR05DESFASE (por valor)                                 *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

' intabrir indica si se ha pulsado la opci�n Abrir Registro
Dim intabrir As Integer
'strmensaje guarda el string de los mensajes por pantalla
Dim strmensaje As String
' gintunload para que Query_Unload no se haga dos veces
Dim gintunload As Integer
'variable que controla que el campo de ocupacion del paciente sea obligatorio
Dim gblnborrar As Boolean
Dim TiempoTotal As Variant
Dim intcambioalgo As Integer


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------


Private Sub Form_Load()
  Dim objDetailInfo1 As New clsCWForm
  Dim strKey As String
  Dim objDetailInfo2 As New clsCWForm
  Dim strKey2 As String
  Dim strsql As String
  Dim rsta As rdoResultset
  
  'Call objApp.SplashOn
  
txtactText1(0) = frmdefactuacionesCUN.txtText1(0)
txtactText1(1) = frmdefactuacionesCUN.txtText1(2)
txtactText1(2) = "1"
txtactText1(3) = frmdefactuacionesCUN.txtText1(2)
'jcr 16/3/98 txtactText1(4) = frmdefactuacionesCUN.Text1(0)

 gblnborrar = False
Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
 
   With objDetailInfo2
    .strName = "Tipos de Recurso Seleccionados"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(2)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR1300"
    .strWhere = "pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & _
    " and pr05numfase=1"
    .intAllowance = cwAllowAdd + cwAllowModify
    Call .FormAddOrderField("AG14CODTIPRECU", cwAscending)
    Call .FormAddOrderField("PR13NUMNECESID", cwAscending)
    
    Call .objPrinter.Add("PR1361", "Listado de Tipos de Recurso y Recurso para cada Fase")
    
    .blnHasMaint = True
  
    strKey2 = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey2, "Tipos de Recurso Seleccionados")
    Call .FormAddFilterWhere(strKey2, "PR13NUMNECESID", "Necesidad", cwNumeric)
    Call .FormAddFilterWhere(strKey2, "AG14CODTIPRECU", "Tipo de Recurso", cwNumeric)
    Call .FormAddFilterWhere(strKey2, "PR13NUMUNIREC", "Unidades", cwNumeric)
    Call .FormAddFilterWhere(strKey2, "PR13NUMTIEMPREC", "Tiempo", cwNumeric)
    Call .FormAddFilterWhere(strKey2, "PR13NUMMINDESF", "M�nimo Desfase", cwNumeric)
    Call .FormAddFilterWhere(strKey2, "PR13INDPREFEREN", "�Preferente?", cwBoolean)
    Call .FormAddFilterWhere(strKey2, "PR13INDPLANIF", "�Planificable?", cwBoolean)
    
    
    Call .FormAddFilterOrder(strKey2, "PR13NUMNECESID", "Necesidad")
    Call .FormAddFilterOrder(strKey2, "AG14CODTIPRECU", "Tipo de Recurso")
    Call .FormAddFilterOrder(strKey2, "PR13NUMUNIREC", "Unidades")
    Call .FormAddFilterOrder(strKey2, "PR13NUMTIEMPREC", "Tiempo")
    Call .FormAddFilterOrder(strKey2, "PR13NUMMINDESF", "M�nimo Desfase")
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo2)
    
    .CtrlGetInfo(txtText1(7)).blnInGrid = False
    .CtrlGetInfo(txtText1(9)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    
    .CtrlGetInfo(txtText1(6)).blnMandatory = True
    
    .CtrlGetInfo(txtdia).blnNegotiated = False
    .CtrlGetInfo(txthora).blnNegotiated = False
    .CtrlGetInfo(txtminuto).blnNegotiated = False
    .CtrlGetInfo(txtdia1).blnNegotiated = False
    .CtrlGetInfo(txthora1).blnNegotiated = False
    .CtrlGetInfo(txtminuto1).blnNegotiated = False
   
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    .CtrlGetInfo(txtText1(4)).blnForeign = True
    .CtrlGetInfo(txtText1(1)).blnForeign = True
     
    '.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT AD02CODDPTO,AD02DESDPTO" & _
    '" FROM AD0200 order by AD02CODDPTO asc"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "AG14CODTIPRECU", "SELECT * FROM AG1400 WHERE AG14CODTIPRECU=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(10), "AG14DESTIPRECU")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtText1(0), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(0), "AD02DESDPTO")
    
   .CtrlGetInfo(txtactText1(4)).blnNegotiated = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  'si al cargarse la pantalla no hay ning�n tipo de recurso el bot�n Recursos se deshabilita
  If txtText1(8).Text = "" Then
    cmdrecursos.Enabled = False
  End If

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    Dim sqlstr1 As String
    Dim rstA1 As rdoResultset
    Dim sqlstr As String
    Dim sqlstr2 As String
    Dim rstA2 As rdoResultset
    Dim strmen As String
    Dim sqlstr3 As String
    Dim strSelect As String
    Dim strBorrado As String
    Dim rstD As rdoResultset
    Dim rstF As rdoResultset
    
    If (gintunload = 0) Then
    
   ' mira si la actuaci�n tiene alg�n departamento realizador
        sqlstr1 = "SELECT count(*) " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & frmdefactuacionesCUN.txtText1(0)
       Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
       If (rstA1.rdoColumns(0).Value = 0) Then
            frmdefactuacionesCUN.cmdinteracciones.Enabled = False
       Else
            frmdefactuacionesCUN.cmdinteracciones.Enabled = True
       End If
       rstA1.Close
       Set rstA1 = Nothing
       
       
    ' se modifica la fase con el tiempo de ocupaci�n del paciente
      If txtactText1(4).Text <> "" Then
        sqlstr = "UPDATE PR0500 " _
             & "SET pr05numocupaci = " & txtactText1(4).Text _
             & "WHERE (pr01codactuacion = " & txtactText1(0).Text _
             & " and pr05numfase=1) "
        objApp.rdoConnect.Execute sqlstr, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      
      'mira si se ha metido alg�n tipo de recurso para la fase 1
    sqlstr2 = "select count(*) from PR1300 " _
           & "where pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0) _
           & " and pr05numfase=1"
    Set rstA2 = objApp.rdoConnect.OpenResultset(sqlstr2)
    If (rstA2.rdoColumns(0).Value = 0) Then
        'se mira si el n� de necesidad es vac�o o no
        If txtText1(8).Text <> "" Then
         strmen = MsgBox("�Desea  salvar  los  cambios  realizados?", vbYesNo, "Pregunta")
         If (strmen = vbYes) Then
            objWinInfo.DataSave
            cmdrecursos.Enabled = False
            'si alg�n campo obligatorio est� sin rellenar para que no vuelva a la
            'pantalla anterior
            If txtText1(6).Text = "" Or txtText1(2).Text = "" Or txtText1(4) = "" Then
                 intCancel = 1
            End If
         Else  'se contesta NO a salvar los cambios
          objWinInfo.objWinActiveForm.blnChanged = False
          'strmen = MsgBox("La actuaci�n se borrar� pues no puede existir sin fases", vbInformation, "Aviso")
          'si se borra la fase �nica la actuaci�n se borra,poniendo la variable global
          'gintactsinfases a 1 al volver a la pantalla de actuaciones ya no muestra
          'esta actuaci�n que no tiene ninguna fase.
          gintactsinfases = 1
          'strSelect = "SELECT * FROM PR0200 WHERE PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0)
          'Set rstD = objApp.rdoConnect.OpenResultset(strSelect)
          'While Not rstD.EOF
          '  strBorrado = "DELETE FROM PR0200 WHERE PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0)
          '  objApp.rdoConnect.Execute strBorrado, 64
          '  rstD.MoveNext
          'Wend
          'objApp.rdoConnect.Execute "Commit", 64
          'rstD.Close
          'Set rstD = Nothing
         
          If txtText1(8).Text <> "" Then
              sqlstr3 = "DELETE FROM PR1300 " _
                  & "WHERE (pr01codactuacion = " & frmdefactuacionesCUN.txtText1(0) & _
                  " AND pr05numfase=1)" & _
                  " AND pr13numnecesid=" & txtText1(8).Text
              On Error GoTo Err_Ejecutar
              objApp.rdoConnect.Execute sqlstr3, 64
              objApp.rdoConnect.Execute "Commit", 64
          End If
         
         
           sqlstr3 = "DELETE FROM PR0500 " _
               & "WHERE (pr01codactuacion = " & frmdefactuacionesCUN.txtText1(0) & _
               " AND pr05numfase=1)"
           On Error GoTo Err_Ejecutar
           objApp.rdoConnect.Execute sqlstr3, 64
           objApp.rdoConnect.Execute "Commit", 64
         
          'sqlstr3 = "DELETE FROM PR0100 " _
          '    & "WHERE pr01codactuacion = " & frmdefactuacionesCUN.txtText1(0)
          'On Error GoTo Err_Ejecutar
          'objApp.rdoConnect.Execute sqlstr3, 64
          'objApp.rdoConnect.Execute "Commit", 64
          
          objWinInfo.objWinActiveForm.blnChanged = False
          intCancel = objWinInfo.WinExit
        End If
      Else 'el c�digo de n� de necesidad es vac�o,se sale directamente sin guardar nada
      
        objWinInfo.objWinActiveForm.blnChanged = False
          'si se borra la fase �nica la actuaci�n se borra,poniendo la variable global
          'gintactsinfases a 1 al volver a la pantalla de actuaciones ya no muestra
          'esta actuaci�n que no tiene ninguna fase.
          gintactsinfases = 1
          'strSelect = "SELECT * FROM PR0200 WHERE PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0)
          'Set rstD = objApp.rdoConnect.OpenResultset(strSelect)
          'While Not rstD.EOF
          '  strBorrado = "DELETE FROM PR0200 WHERE PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0)
          '  objApp.rdoConnect.Execute strBorrado, 64
          '  rstD.MoveNext
          'Wend
          'objApp.rdoConnect.Execute "Commit", 64
          'rstD.Close
          'Set rstD = Nothing
            sqlstr3 = "DELETE FROM PR0500 " _
               & "WHERE (pr01codactuacion = " & frmdefactuacionesCUN.txtText1(0) & _
               " AND pr05numfase=1)"
           On Error GoTo Err_Ejecutar
           objApp.rdoConnect.Execute sqlstr3, 64
           objApp.rdoConnect.Execute "Commit", 64
           intCancel = objWinInfo.WinExit
      End If
    Else
      If (intcambioalgo = 0) Then
        objWinInfo.objWinActiveForm.blnChanged = False
        intCancel = objWinInfo.WinExit
      End If
      If (intcambioalgo = 1) Then
      strmen = MsgBox("�Desea  salvar  los  cambios  realizados?", vbYesNo, "Pregunta")
      If (strmen = vbYes) Then
        objWinInfo.DataSave
        cmdrecursos.Enabled = False
        'si alg�n campo obligatorio est� sin rellenar para que no vuelva a la
        'pantalla anterior
        If txtText1(6).Text = "" Or txtText1(2).Text = "" Or txtText1(4) = "" Then
            intCancel = 1
        End If
      Else  'se contesta NO a salvar los cambios
          objWinInfo.objWinActiveForm.blnChanged = False
      End If
      End If
     
    End If
    gintunload = 1
    
    
    rstA2.Close
    Set rstA2 = Nothing
  End If
  
  cmdrecursos.Enabled = False
  
  
  
  'Si la fase borrada era la �ltima fase, hay que cambiar la clausula strWhere del frmdefactuacionesCUN
  
  strSelect = "SELECT COUNT(*) " & _
              "  FROM PR0500 " & _
              " WHERE PR01CODACTUACION = " & frmdefactuacionesCUN.txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value = 0 Then
    gstrWhereSinFases = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)) or " _
            & "pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & ")"
  Else
    gstrWhereSinFases = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion)))"
  End If
  rstF.Close
  Set rstF = Nothing
  Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub lblactLabel1_Click(Index As Integer)
txtText1(8).SetFocus
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
'If intNewStatus = cwModeSingleAddKey Then
  ' se deshabilita el bot�n <Recursos>
    'cmdrecursos.Enabled = False
'End If

  If txtText1(6).Text = "" Or txtText1(2).Text = "" Or txtText1(4).Text = "" Then
      cmdrecursos.Enabled = False
  End If
  If txtText1(6).Text <> "" And txtText1(2).Text <> "" And txtText1(4).Text <> "" Then
      cmdrecursos.Enabled = True
  End If
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  txtText1(7).Text = txtactText1(0).Text
  txtText1(9).Text = txtactText1(2).Text
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
   'deshabilitar el bot�n Recursos si se pulsa Abrir
    Dim sqlstring As String
    Dim rsta As rdoResultset
    
    If (txtText1(8).Text <> "") And (intabrir = 1) Then
    intabrir = 0
        ' cu�ntos tipos de recurso
        sqlstring = "select count(*) from PR1300 " & _
             "where PR05NUMFASE=1" & _
             " and PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
             " and PR13NUMNECESID=" & txtText1(8).Text
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstring)
        If rsta.rdoColumns(0).Value = 1 Then
            cmdrecursos.Enabled = True
        End If
        rsta.Close
        Set rsta = Nothing
    End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Dim strRec As String
    
    intcambioalgo = 0
    cmdrecursos.Enabled = True
    
    If gblnborrar = True Then
      Call MsgBox("El Campo Tiempo de Ocupaci�n del Paciente es obligatorio", vbInformation, "Aviso")
      strRec = "delete from pr1300 where pr01codactuacion=" & txtText1(7).Text & _
                " and pr05numfase=" & txtText1(9).Text & " and pr13numnecesid=" & txtText1(8).Text
      objApp.rdoConnect.Execute strRec, 64
      gblnborrar = False
    End If
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  If txtactText1(4).Text = "" Then
    gblnborrar = True
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Tipos de Recurso Seleccionados" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  If strFormName = "Tipos de Recurso Seleccionados" And strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AG1400"
     'no se puede seleccionar un tipo de recurso cuya fecha de fin
     'de vigencia es anterior a la fecha actual
     .strWhere = " where ag14codtiprecu not in (select ag14codtiprecu from AG1400 " _
                                    & "where ag14fecfivgtr < (select sysdate from dual))"
     .strOrder = "ORDER BY ag14codtiprecu ASC"
         
     Set objField = .AddField("ag14codtiprecu")
     objField.strSmallDesc = "C�digo Tipo de Recurso"
         
     Set objField = .AddField("ag14destiprecu")
     objField.strSmallDesc = "Descripci�n Tipo de Recurso"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(6)), .cllValues("ag14codtiprecu"))
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("ag14codtiprecu"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Tipos de Recurso Seleccionados" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strOrder = "ORDER BY ad02coddpto ASC"
         
     Set objField = .AddField("ad02coddpto")
     objField.strSmallDesc = "C�digo Departamento"
         
     Set objField = .AddField("ad02desdpto")
     objField.strSmallDesc = "Descripci�n Departamento"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("ad02coddpto"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim contador As Integer
'*************************************************************************
Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtactText1(0).Text <> "" And txtactText1(2).Text <> "" And _
    txtText1(8).Text <> "" And (btnButton.Index = 4 Or btnButton.Index = 30) Then
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
End If

Call Calcular_Tiempo(txtdia1.Text, txthora1.Text, txtminuto1.Text)
If txtactText1(0).Text <> "" And txtactText1(2).Text <> "" And _
    txtText1(8).Text <> "" And (btnButton.Index = 4 Or btnButton.Index = 30) Then
       Call objWinInfo.CtrlSet(txtactText1(4), TiempoTotal)
       objWinInfo.objWinActiveForm.blnChanged = True
End If
'*********************************************************************
  
    
  'Salir
  If (btnButton.Index = 30) Then
    gintunload = 0
  End If
  
  ' controla que el Tipo de Recurso nuevo existe antes de hacer Guardar
   If (btnButton.Index = 4) Then
      If (txtText1(6).Text <> "") Then
        sqlstr = "select count(AG14CODTIPRECU) from AG1400" & _
             " where AG14CODTIPRECU=" & txtText1(6).Text
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        contador = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
        If (contador = 0) Then
          strmensaje = MsgBox("El Tipo de Recurso " & txtText1(6) & " no existe. " _
           & "Elija otro, por favor.", vbCritical, "Tipos de Recurso")
          txtText1(6).SetFocus
          objWinInfo.objWinActiveForm.blnChanged = False
          Exit Sub
        Else
        cmdrecursos.Enabled = True
       End If
      End If
  End If
   ' controla que el nuevo tipo de recurso existe antes de pulsar un bot�n
  ' distinto de imprimir,borrar,anterior,siguiente,guardar.
  'If (btnButton.Index <> 6 And btnButton.Index <> 8 And btnButton.Index <> 4) Then
  '     If (txtText1(6).Text <> "") Then
  '     sqlstr = "select count(AG14CODTIPRECU) from AG1400" & _
  '           " where AG14CODTIPRECU=" & txtText1(6).Text
  '     Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  '     If (rsta.rdoColumns(0).Value = 0) Then
  '     'objWinInfo.objWinActiveForm.blnChanged = False
  '     End If
  '     End If
  'End If

  If btnButton.Index = 4 Then
    If txtactText1(4).Text = "" Then
      Call MsgBox("El Campo Tiempo de Ocupaci�n del Paciente es obligatorio", vbInformation, "Aviso")
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  
  
   ' si se pulsa Nuevo
   If (btnButton.Index = 2) Then
        ' generaci�n autom�tica del c�digo
        sqlstr = "SELECT PR13NUMNECESID_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(8) = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
        txtText1(8).Locked = True
        txtText1(8).SetFocus
        txtText1(6).SetFocus
        txtdia.Text = 0
        txthora.Text = 0
        txtminuto.Text = 0
        txtdia1.Text = 0
        txtminuto1.Text = 0
        txthora1.Text = 0
        txtText1(8).SetFocus
  End If
  If btnButton.Index <> 30 Then
    If (txtText1(8).Text = "" Or txtText1(6).Text = "") Then
      cmdrecursos = False
    Else
      'cmdrecursos.Enabled = True
    End If
  End If
  ' Nuevo
  If btnButton.Index = 2 Then
     ' se deshabilita el bot�n <Recursos>
      cmdrecursos.Enabled = False
  End If
  'Abrir Registro
  If btnButton.Index = 3 Then
     ' se deshabilita el bot�n <Recursos>
      cmdrecursos.Enabled = False
      intabrir = 1
  End If
 
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim sqlstr5 As String
  Dim rstA5 As rdoResultset
  Dim contador As Integer
  
'*************************************************************************
Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtactText1(0).Text <> "" And txtactText1(2).Text <> "" And _
    txtText1(8).Text <> "" And (intIndex = 40 Or intIndex = 100) Then
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
End If

Call Calcular_Tiempo(txtdia1.Text, txthora1.Text, txtminuto1.Text)
If txtactText1(0).Text <> "" And txtactText1(2).Text <> "" And _
    txtText1(8).Text <> "" And (intIndex = 40) Then
       Call objWinInfo.CtrlSet(txtactText1(4), TiempoTotal)
       objWinInfo.objWinActiveForm.blnChanged = True
End If
'*********************************************************************
  
  'Salir
  If (intIndex = 100) Then
    gintunload = 0
  End If
  
  ' controla que el nuevo tipo de recurso existe antes de hacer Guardar
  If (intIndex = 40) Then
      If (txtText1(6).Text <> "") Then
      sqlstr = "select count(AG14CODTIPRECU) from AG1400" & _
             " where AG14CODTIPRECU=" & txtText1(6).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      contador = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
      If (contador = 0) Then
        strmensaje = MsgBox("El Tipo de Recurso " & txtText1(6) & " no existe. " _
           & "Elija otro, por favor.", vbCritical, "Tipos de Recurso")
       txtText1(6).SetFocus
       objWinInfo.objWinActiveForm.blnChanged = False
       Exit Sub
      Else
        cmdrecursos.Enabled = True
      End If
      End If
  End If
  
  ' controla que el nuevo tipo de recurso existe antes de hacer la opci�n de men�.
  ' control si la opci�n es distinta de Eliminar y De Imprimir
  'If (intIndex <> 60 And intIndex <> 80) Then
  '    If (txtText1(6).Text <> "") Then
  '    sqlstr = "select count(AG14CODTIPRECU) from AG1400" & _
  '           " where AG14CODTIPRECU=" & txtText1(6).Text
  '    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  '    If (rsta.rdoColumns(0).Value = 0) Then
  '    'objWinInfo.objWinActiveForm.blnChanged = False
  '    End If
  '    End If
  'End If
  
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  'objWinInfo.objWinActiveForm.blnChanged = False
  If (txtText1(8).Text = "" Or txtText1(6).Text = "") Then
    cmdrecursos = False
  Else
    'cmdrecursos.Enabled = True
  End If
  ' Nuevo
  If (intIndex = 10) Then
     ' se deshabilita el bot�n <Recursos>
      cmdrecursos.Enabled = True
      ' generaci�n autom�tica del c�digo
        sqlstr5 = "SELECT PR13NUMNECESID_SEQUENCE.nextval FROM dual"
        Set rstA5 = objApp.rdoConnect.OpenResultset(sqlstr5)
        txtText1(8) = rstA5.rdoColumns(0).Value
        rstA5.Close
        Set rstA5 = Nothing
        txtText1(8).Locked = True
        txtText1(8).SetFocus
        txtText1(6).SetFocus
        txtdia.Text = 0
        txthora.Text = 0
        txtminuto.Text = 0
  End If
  'Abrir Registro
  If (intIndex = 20) Then
     ' se deshabilita el bot�n <Recursos>
      cmdrecursos.Enabled = False
      intabrir = 1
  End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
 Dim sqlstr As String
 Dim rsta As rdoResultset
 
   ' controla que el nuevo tipo de recurso existe antes de hacer la opci�n de men�.
   ' control al pulsar Filtro
 '  If (intIndex = 10) Then
 '     If (txtText1(6).Text <> "") Then
 '     sqlstr = "select count(AG14CODTIPRECU) from AG1400" & _
 '            " where AG14CODTIPRECU=" & txtText1(6).Text
 '     Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
 '     If (rsta.rdoColumns(0).Value = 0) Then
 '     'objWinInfo.objWinActiveForm.blnChanged = False
 '     End If
 '     End If
 ' End If

  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  'objWinInfo.objWinActiveForm.blnChanged = False
  If (txtText1(8).Text = "" Or txtText1(6).Text = "") Then
    cmdrecursos = False
  Else
    cmdrecursos.Enabled = True
  End If

End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   
   ' controla que el nuevo tipo de recurso existe antes de hacer la opci�n de men�.
   ' control si es distinto de Anterior y de Siguiente
   'If (intIndex <> 50 And intIndex <> 60) Then
   '   If (txtText1(6).Text <> "") Then
   '   sqlstr = "select count(AG14CODTIPRECU) from AG1400" & _
   '          " where AG14CODTIPRECU=" & txtText1(6).Text
   '   Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
   '   If (rsta.rdoColumns(0).Value = 0) Then
   '   'objWinInfo.objWinActiveForm.blnChanged = False
   '   End If
   '   End If
   'End If

  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  'objWinInfo.objWinActiveForm.blnChanged = False
  If (txtText1(8).Text = "" Or txtText1(6).Text = "") Then
    cmdrecursos = False
  Else
    cmdrecursos.Enabled = True
  End If

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  intcambioalgo = 1
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
   'Dim strsql As String
   'Dim rsta As rdoResultset
   Call objWinInfo.CtrlDataChange
  ' strsql = "select AD02DESDPTO from AD0200 " & _
  '        "where AD02CODDPTO=" & cboSSDBCombo1(0).Text
  ' Set rsta = objApp.rdoConnect.OpenResultset(strsql)
  ' txtText1(0).Text = rsta.rdoColumns(0).Value
  ' rsta.close
  ' Set rsta = Nothing
End Sub




Private Sub txtactText1_Change(Index As Integer)

'****************************************************************************
  'cuando cambie la columna invisible Tiempo que se actualice dias,horas,minutos
  If Index = 4 Then
    If txtactText1(4).Text = "" Then
      txtdia1.Text = 0
      txthora1.Text = 0
      txtminuto1.Text = 0
    Else
      txtdia1.Text = txtactText1(4).Text \ 1440                 'd�as
      txthora1.Text = (txtactText1(4).Text Mod 1440) \ 60       'horas
      txtminuto1.Text = (txtactText1(4).Text Mod 1440) Mod 60   'minutos
    End If
  End If
  '****************************************************************************
End Sub

Private Sub txtactText1_GotFocus(Index As Integer)
If (Index <> 4) Then
   txtText1(8).SetFocus
End If
End Sub



Private Sub txtdia_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtdia1_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia1.Text) = False Then
    Beep
    txtdia1.Text = ""
    txtdia1.SetFocus
  End If
  If txtdia.Text <> "" Then
     If txtdia.Text > 65 Then
      txtdia.Text = 65
     End If
  End If
End Sub

Private Sub txtdia1_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora1_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora1.Text) = False Then
    Beep
    txthora1.Text = ""
    txthora1.SetFocus
  End If
  If txthora.Text <> "" Then
     If txthora.Text > 23 Then
      txthora.Text = 23
     End If
  End If
End Sub

Private Sub txthora1_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto1_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto1.Text) = False Then
    Beep
    txtminuto1.Text = ""
    txtminuto1.SetFocus
  End If
  If txtminuto.Text <> "" Then
     If txtminuto.Text > 59 Then
      txtminuto.Text = 59
     End If
  End If
End Sub

Private Sub txtdia_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia.Text) = False Then
    Beep
    txtdia.Text = ""
    txtdia.SetFocus
  End If
  If txtdia.Text <> "" Then
     If txtdia.Text > 65 Then
      txtdia.Text = 65
     End If
  End If
End Sub

Private Sub txthora_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora.Text) = False Then
    Beep
    txthora.Text = ""
    txthora.SetFocus
  End If
  If txthora.Text <> "" Then
     If txthora.Text > 23 Then
      txthora.Text = 23
     End If
  End If
End Sub

Private Sub txtminuto_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto.Text) = False Then
    Beep
    txtminuto.Text = ""
    txtminuto.SetFocus
  End If
  If txtminuto.Text <> "" Then
     If txtminuto.Text > 59 Then
      txtminuto.Text = 59
     End If
  End If
End Sub

Private Sub txtminuto1_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim strtipo As String
Dim rsttipo As rdoResultset

Dim strFecha As String
Dim rstfecha As rdoResultset

Dim strlanzarmensaje As String

  Call objWinInfo.CtrlLostFocus
  'no se puede seleccionar un tipo de recurso cuya fecha de fin
  'de vigencia es anterior a la fecha actual
'  If intIndex = 6 Then
'    If txtText1(6).Text <> "" Then
'      strtipo = "SELECT ag14fecfivgtr FROM AG1400 WHERE ag14codtiprecu=" & txtText1(6).Text
'      Set rsttipo = objApp.rdoConnect.OpenResultset(strtipo)
'      strFecha = "SELECT sysdate FROM dual"
'      Set rstfecha = objApp.rdoConnect.OpenResultset(strFecha)
'      If rsttipo.rdoColumns(0).Value < rstfecha.rdoColumns(0).Value Then
'         strlanzarmensaje = MsgBox("El Tipo de Recurso est� ya dado de baja", vbInformation, "Importante")
'         Call objWinInfo.CtrlSet(txtText1(6), "")
'         txtText1(6).SetFocus
'      End If
'      rsttipo.Close
'      Set rsttipo = Nothing
'      rstfecha.Close
'      Set rstfecha = Nothing
'    End If
'  End If
' LAS 7.4.1999
    If txtText1(6).Text <> "" Then
      strtipo = "SELECT count(*) FROM AG1400 WHERE ag14codtiprecu=" & txtText1(6).Text
      Set rsttipo = objApp.rdoConnect.OpenResultset(strtipo)
      If rsttipo.rdoColumns(0).Value > 0 Then
        strtipo = "SELECT ag14fecfivgtr FROM AG1400 WHERE ag14codtiprecu=" & txtText1(6).Text
        Set rsttipo = objApp.rdoConnect.OpenResultset(strtipo)
        strFecha = "SELECT sysdate FROM dual"
        Set rstfecha = objApp.rdoConnect.OpenResultset(strFecha)
        If rsttipo.rdoColumns(0).Value < rstfecha.rdoColumns(0).Value Then
           strlanzarmensaje = MsgBox("El Tipo de Recurso est� ya dado de baja", vbInformation, "Importante")
          Call objWinInfo.CtrlSet(txtText1(6), "")
          txtText1(6).SetFocus
        End If
        rstfecha.Close
        Set rstfecha = Nothing
      Else
         strlanzarmensaje = MsgBox("El Tipo de Recurso no es v�lido", vbInformation, "Importante")
         Call objWinInfo.CtrlSet(txtText1(6), "")
         txtText1(6).SetFocus
      End If
    rsttipo.Close
    Set rsttipo = Nothing
    End If
    
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String

If intIndex = 8 Then
  intcambioalgo = 0
End If

If (intIndex = 8 And intabrir = 1) Then
   txtText1(7).Text = txtactText1(0).Text
   txtText1(9).Text = txtactText1(2).Text
End If

If (intIndex = 6) Then
  If (txtText1(6).Text <> "" And txtText1(8).Text <> "") Then
      ' se cuentan los tipos de recurso
      sqlstr = "select count(*) from PR1300" & _
             " where PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text & _
             " and PR05NUMFASE=1" & _
             " and PR13NUMNECESID=" & txtText1(8) & _
             " and AG14CODTIPRECU=" & txtText1(6)
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If (rsta.rdoColumns(0).Value) = 0 Then
          cmdrecursos.Enabled = False
      End If
      rsta.Close
      Set rsta = Nothing
 End If
 If txtText1(6).Text = "" Then
        cmdrecursos.Enabled = False
 End If
End If
 If intIndex = 6 Then
  If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      If txtText1(6).Text <> "" Then
        sqlstr = "select count(*) from AG1400 where AG14CODTIPRECU=" & txtText1(6).Text
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        If rsta.rdoColumns(0).Value > 0 Then
          sqlstr = "select * from AG1400 where AG14CODTIPRECU=" & txtText1(6).Text
          Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
          If (rsta("ag14indplanifi").Value = 0) Or (rsta("ag14indplanifi").Value = -1) Then
            Call objWinInfo.CtrlSet(chkCheck1(1), rsta("ag14indplanifi").Value)
          End If
        End If
        rsta.Close
        Set rsta = Nothing
      End If
  End If
 End If
 
Call objWinInfo.CtrlDataChange

'****************************************************************************
  'cuando cambie la columna invisible Tiempo que se actualice dias,horas,minutos
  If intIndex = 5 Then
    If txtText1(5).Text = "" Then
      txtdia.Text = 0
      txthora.Text = 0
      txtminuto.Text = 0
    Else
      txtdia.Text = txtText1(5).Text \ 1440                 'd�as
      txthora.Text = (txtText1(5).Text Mod 1440) \ 60       'horas
      txtminuto.Text = (txtText1(5).Text Mod 1440) Mod 60   'minutos
    End If
  End If
  '****************************************************************************

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command Button
' -----------------------------------------------


Private Sub cmdrecursos_Click()
       cmdrecursos.Enabled = False
       objWinInfo.objWinActiveForm.blnChanged = False
       Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        frmdefrecursovalido.txtactText1(0).Text = txtactText1(0).Text
        frmdefrecursovalido.txtactText1(1).Text = txtactText1(1).Text
        frmdefrecursovalido.txtactText1(2).Text = txtactText1(2).Text
        frmdefrecursovalido.txtactText1(3).Text = txtactText1(3).Text
        frmdefrecursovalido.txtactText1(4).Text = txtText1(6)
        frmdefrecursovalido.txtactText1(5).Text = txtText1(10)
        frmdefrecursovalido.txtactText1(6).Text = txtText1(8)
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)

        'Load frmdefrecursovalido
        'frmdefrecursovalido!tabTab1(1).Tab = 0 'para mostrar el detalle del Tab
        'frmdefrecursovalido.Show (vbModal)
        'Unload frmdefrecursovalido
        'Set frmdefrecursovalido = Nothing
        Call objsecurity.LaunchProcess("PR0126")
        cmdrecursos.Enabled = True
End Sub


Private Sub Calcular_Tiempo(d, h, m)
'procedimiento que transforma los d�as,horas y minutos a Minutos

If d = "" Or IsNumeric(d) = False Then
  d = 0
Else
  If d > 65 Then
    d = 65
  End If
End If
If h = "" Or IsNumeric(h) = False Then
  h = 0
Else
  If h > 23 Then
    h = 23
  End If
End If
If m = "" Or IsNumeric(m) = False Then
  m = 0
Else
  If m > 59 Then
    m = 59
  End If
End If
TiempoTotal = (d * 1440) + (h * 60) + m
  
End Sub

