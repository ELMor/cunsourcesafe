Attribute VB_Name = "modFunciones"
Option Explicit
Type typeDpt
  cDpt As Integer
  Dpt As String
End Type

Public Function fNextClave(campo$, tabla$) As String
'Devuelve el siguiente valor de la clave principal obtenido del sequence
    Dim sql$, rsMaxClave As rdoResultset
    
    sql = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(sql)
    fNextClave = rsMaxClave(0)
    rsMaxClave.Close
    
End Function


Sub LlenarDpts(lvwDpt As ListView)
Dim sql As String
Dim rdo As rdoResultset

  sql = "SELECT AD02CodDpto, AD02DesDpto FROM AD0200 WHERE " _
      & "AD32CodTipoDpto IN (1, 2) " _
      & "ORDER BY AD02CodDpto"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    lvwDpt.ListItems.Add , "D" & rdo(0), rdo(0) & " - " & rdo(1), , hcICONDPT
    rdo.MoveNext
  Wend
End Sub

Sub LlenarPr(tvwPr As TreeView)
Dim sql As String
Dim rdo As rdoResultset
Dim cAct As Integer, cDpt As Integer, cGrupo As Long, cPaq As Long, cProt As Long
Dim cTiGrupo As Long, i As Long
Dim nodoAct As Node, nodoDpt As Node, nodoPr As Node, nodoPadre As Node
Dim nodoPaq As Node, nodoGrupo As Node, nodoTiGrupo As Node, nodoProt As Node

  Screen.MousePointer = 11
' Primero se clasifican las pruebas en funci�n del tipo de prueba y departamento
  Set nodoPadre = tvwPr.Nodes.Add(, , , "Pruebas por departamentos", hcICONPADRE)
  sql = "SELECT PR12CodActividad, PR12DesActividad FROM PR1200 ORDER BY PR12CodActividad"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set nodoAct = tvwPr.Nodes.Add(nodoPadre.Index, tvwChild, , rdo(1), hcICONACTIVIDAD)
    nodoAct.Tag = rdo(0)
    rdo.MoveNext
  Wend
  
' Se clasifican las pruebas en protocolos
  Set nodoPadre = tvwPr.Nodes.Add(, , , "Protocolos", hcICONPADRE)
  sql = "SELECT PR3500.PR35CodProtocolo, PR3500.PR35DesProtocolo, PR3600.AD02CodDpto, " _
      & "PR0100.PR01CodActuacion, NVL(PR0100.PR01DesCompleta, PR0100.PR01DesCorta), " _
      & "PR3600.PR36Orden " _
      & "FROM PR0100, PR3500, PR3600 WHERE " _
      & "PR3500.PR35CodProtocolo = PR3600.PR35CodProtocolo AND " _
      & "PR3600.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR0100.PR01FecFin IS NULL " _
      & "ORDER BY PR3500.PR35CodProtocolo, PR3600.PR36Orden"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  cProt = 0
  While rdo.EOF = False
    If cProt <> rdo(0) Then
      Set nodoProt = tvwPr.Nodes.Add(nodoPadre.Index, tvwChild, "P" & rdo(0), rdo(1), hcICONPROTOCOLO)
      nodoProt.Tag = rdo(2)
    End If
    Set nodoPr = tvwPr.Nodes.Add(nodoProt.Index, tvwChild, , rdo(5) & ".- " & rdo(4), hcICONPR)
    nodoPr.Tag = rdo(3) & "-" & rdo(2)
    cProt = rdo(0)
    rdo.MoveNext
  Wend

' Se clasifican las pruebas en grupos
'  Set nodoPadre = tvwPr.Nodes.Add(, , , "Grupos", hcICONPADRE)
'  sql = "SELECT PR1500.PR15CodTipGrupo, PR1500.PR15DesTipGrupo, PR1600.PR16CodGrupo, " _
'      & "PR1600.PR16DesGrupo, PR0100.PR01CodActuacion, PR0100.PR01DesCorta " _
'      & "FROM PR0100, PR1500, PR1600, PR1700 WHERE " _
'      & "PR1500.PR15CodTipGrupo = PR1600.PR15CodTipGrupo AND " _
'      & "PR1600.PR16CodGrupo = PR1700.PR16CodGrupo AND " _
'      & "PR1700.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
'      & "PR0100.PR01FecFin IS NULL " _
'      & "ORDER BY PR1500.PR15CodTipGrupo, PR1600.PR16CodGrupo, PR0100.PR01DesCorta"
'  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
'  cTiGrupo = 0
'  cGrupo = 0
'  While rdo.EOF = False
'    If cTiGrupo <> rdo(0) Then Set nodoTiGrupo = tvwPr.Nodes.Add(nodoPadre.Index, tvwChild, , rdo(1), hcICONACTIVIDAD)
'    If cGrupo <> rdo(2) Then
'      Set nodoGrupo = tvwPr.Nodes.Add(nodoTiGrupo.Index, tvwChild, , rdo(2) & " - " & rdo(3), hcICONGRUPO)
'      nodoGrupo.Tag = rdo(2)
'    End If
'    Set nodoPr = tvwPr.Nodes.Add(nodoGrupo.Index, tvwChild, , rdo(5), hcICONPR)
'    nodoPr.Tag = rdo(4)
'    cTiGrupo = rdo(0)
'    cGrupo = rdo(2)
'    rdo.MoveNext
'  Wend

' Se clasifican las pruebas en paquetes
  Set nodoPadre = tvwPr.Nodes.Add(, , , "Paquetes", hcICONPADRE)
  sql = "SELECT PR3300.PR33CodEstandard, PR3300.PR33DesEstandard, PR3400.AD02CodDpto, " _
      & "PR0100.PR01CodActuacion, NVL(PR0100.PR01DesCompleta, PR0100.PR01DesCorta), " _
      & "PR3400.AD02CodDpto_Rea, PR3400.PR34Orden " _
      & "FROM PR0100, PR3300, PR3400 WHERE " _
      & "PR3300.PR33CodEstandard = PR3400.PR33CodEstandard AND " _
      & "PR3300.AD02CodDpto = PR3400.AD02CodDpto AND " _
      & "PR3400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR0100.PR01FecFin IS NULL " _
      & "ORDER BY PR3300.PR33CodEstandard, PR3400.AD02CodDpto, PR3400.PR34Orden"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  cPaq = 0
  cDpt = 0
  While rdo.EOF = False
    If cPaq <> rdo(0) Or cDpt <> rdo(2) Then
      Set nodoPaq = tvwPr.Nodes.Add(nodoPadre.Index, tvwChild, , rdo(1), hcICONPAQUETE)
      nodoPaq.Tag = rdo(0) & "-" & rdo(2)
    End If
    Set nodoPr = tvwPr.Nodes.Add(nodoPaq.Index, tvwChild, , rdo(6) & ".- " & rdo(4), hcICONPR)
    nodoPr.Tag = rdo(3) & "-" & rdo(5)
    cPaq = rdo(0)
    cDpt = rdo(2)
    rdo.MoveNext
  Wend

  Screen.MousePointer = 0

End Sub

