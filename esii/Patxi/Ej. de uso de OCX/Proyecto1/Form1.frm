VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{32E7ABB5-68D2-11D2-94DF-00C04F7D9FC8}#1.0#0"; "ac00100.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   8235
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11355
   LinkTopic       =   "Form1"
   ScaleHeight     =   8235
   ScaleWidth      =   11355
   StartUpPosition =   3  'Windows Default
   Begin ac00100.Actuaciones Actuaciones1 
      Height          =   6435
      Left            =   5040
      TabIndex        =   4
      Top             =   1680
      Width           =   6435
      _ExtentX        =   11351
      _ExtentY        =   11351
   End
   Begin ac00100.Procesos Procesos1 
      Height          =   6435
      Left            =   0
      TabIndex        =   3
      Top             =   1680
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   11351
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command1"
      Height          =   495
      Left            =   7920
      TabIndex        =   2
      Top             =   900
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   7920
      TabIndex        =   1
      Top             =   180
      Width           =   1215
   End
   Begin ac00100.Paciente Paciente1 
      Height          =   1635
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7575
      _ExtentX        =   13361
      _ExtentY        =   2884
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   5880
      Top             =   5460
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    Dim str As String
    Paciente1.blnMostrar_Asistencia = False
    Paciente1.lngCodPersona = 11

    'Procesos1.blnVerOpciones = False
    Procesos1.lngCodPersona = 11
    Procesos1.blnOpcionMostrarFechaRealizacion = True
    Procesos1.Refresh
End Sub

Private Sub Command2_Click()
'   Paciente1.blnMostrar_FechaNacimiento = False
'Actuaciones1.cllSelectedItems
    Dim objSelect As Object
    Dim strTexto As String
    
    For Each objSelect In Actuaciones1.cllSelectedItems
        strTexto = strTexto & objSelect.lngnumactuacion & " " & objSelect.strNombreActuacion & Chr$(13) & Chr$(10)
    Next
    MsgBox "Selecionados: " & Chr$(13) & Chr$(10) & strTexto
End Sub


Private Sub Form_Load()
  Dim strEntorno1 As String
  Dim strEntorno2 As String
  Dim res%
  Dim strDir As String
  On Error Resume Next
  
  Call InitApp
 
  With objApp
  
    Call .Register(App, Screen)
    Set .imlImageList = imlImageList1
    Set .frmFormMain = Me
    .strUserName = "CUN"
    .strPassword = "TUY"
    .strDataSource = "Oracle7_Micro"
    .intUserEnv = 1
    .blnAskPassword = False
    .objUserColor.lngReadOnly = &HC0C0C0
    .objUserColor.lngMandatory = &HFFFF00
    .blnUseRegistry = True
  End With
  
  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotación"
  
  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "Main", "CUNLAB.")
  
    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "Main", "")
  
  End With
    
  If Not objApp.CreateInfo Then
    Call ExitApp
  End If


  Set Paciente1.Connect = objApp.rdoConnect
  Set Procesos1.Connect = objApp.rdoConnect
  Set Actuaciones1.Connect = objApp.rdoConnect
End Sub


Private Sub Procesos1_Selected(strProceso As String, strAsistencia As String, strFecha As String, lngDpto As Long, lngActuacion As Long, lngGrActuacion As Long)
    Actuaciones1.lngCodPersona = 11
    Actuaciones1.strProceso = strProceso
    Actuaciones1.strAsistencia = strAsistencia
    Actuaciones1.strFechaReali = strFecha
    Actuaciones1.lngDpto = lngDpto
    Actuaciones1.lngCodActuacion = lngActuacion
    Actuaciones1.lngCodGrActuacion = lngGrActuacion
    
    Actuaciones1.Refresh

End Sub

Private Sub Procesos1_SelectedItem(strProceso As String, strAsistencia As String, strFecha As String, lngDpto As Long, lngActuacion As Long, lngGrActuacion As Long)
    Actuaciones1.lngCodPersona = 11
    Actuaciones1.strProceso = strProceso
    Actuaciones1.strAsistencia = strAsistencia
    Actuaciones1.strFechaReali = strFecha
    Actuaciones1.lngDpto = lngDpto
    Actuaciones1.lngCodActuacion = lngActuacion
    Actuaciones1.lngCodGrActuacion = lngGrActuacion
    
    Actuaciones1.Refresh


End Sub


