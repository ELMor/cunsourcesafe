VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   6720
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10695
   LinkTopic       =   "Form1"
   ScaleHeight     =   6720
   ScaleWidth      =   10695
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Dim strEntorno1 As String
  Dim strEntorno2 As String
  Dim res%
  Dim strDir As String
  On Error Resume Next
  
  Call InitApp
 
  With objApp
  
    Call .Register(App, Screen)
    Set .imlImageList = imlImageList1
    Set .frmFormMain = Me
    .strUserName = "CUN"
    .strPassword = "TUY"
    .strDataSource = "Oracle7_Micro"
    .intUserEnv = 1
    .blnAskPassword = False
    .objUserColor.lngReadOnly = &HC0C0C0
    .objUserColor.lngMandatory = &HFFFF00
    .blnUseRegistry = True
  End With
  
  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotación"
  
  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "Main", "CUNLAB.")
  
    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "Main", "")
  
  End With
    
  If Not objApp.CreateInfo Then
    Call ExitApp
  End If

Set Procesos1.Connect = objApp.rdoConnect
Set Actuaciones1.Connect = objApp.rdoConnect
Set persona.Connect = objApp.rdoConnect

End Sub


