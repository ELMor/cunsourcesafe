VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmP_Peticion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gesti�n de Pruebas"
   ClientHeight    =   8415
   ClientLeft      =   1485
   ClientTop       =   1185
   ClientWidth     =   11685
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8415
   ScaleWidth      =   11685
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtObserv 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   0  'None
      Height          =   240
      Left            =   2775
      TabIndex        =   24
      Top             =   2775
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Frame Frame1 
      ForeColor       =   &H00800000&
      Height          =   765
      Left            =   225
      TabIndex        =   15
      Top             =   7575
      Width           =   7065
      Begin VB.TextBox txtFec 
         Height          =   285
         Index           =   3
         Left            =   5250
         TabIndex        =   22
         Text            =   "Text1"
         Top             =   375
         Width           =   1700
      End
      Begin VB.TextBox txtFec 
         Height          =   285
         Index           =   2
         Left            =   3525
         TabIndex        =   21
         Text            =   "Text1"
         Top             =   375
         Width           =   1700
      End
      Begin VB.TextBox txtFec 
         Height          =   285
         Index           =   1
         Left            =   1800
         TabIndex        =   19
         Text            =   "Text1"
         Top             =   375
         Width           =   1700
      End
      Begin VB.TextBox txtFec 
         Height          =   285
         Index           =   0
         Left            =   75
         TabIndex        =   17
         Text            =   "Text1"
         Top             =   375
         Width           =   1700
      End
      Begin VB.Label lblFec 
         Caption         =   "Label1"
         Height          =   240
         Index           =   3
         Left            =   5250
         TabIndex        =   23
         Top             =   150
         Width           =   1140
      End
      Begin VB.Label lblFec 
         Caption         =   "Label1"
         Height          =   240
         Index           =   2
         Left            =   3525
         TabIndex        =   20
         Top             =   150
         Width           =   1215
      End
      Begin VB.Label lblFec 
         Caption         =   "Label1"
         Height          =   240
         Index           =   1
         Left            =   1800
         TabIndex        =   18
         Top             =   150
         Width           =   1065
      End
      Begin VB.Label lblFec 
         Caption         =   "Label1"
         Height          =   240
         Index           =   0
         Left            =   75
         TabIndex        =   16
         Top             =   150
         Width           =   1365
      End
   End
   Begin VB.PictureBox picSep3 
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   4725
      MousePointer    =   9  'Size W E
      ScaleHeight     =   465
      ScaleWidth      =   120
      TabIndex        =   14
      Top             =   0
      Width           =   120
   End
   Begin VB.PictureBox picSep2 
      BorderStyle     =   0  'None
      Height          =   20
      Left            =   0
      MousePointer    =   7  'Size N S
      ScaleHeight     =   15
      ScaleWidth      =   3870
      TabIndex        =   13
      Top             =   0
      Width           =   3870
   End
   Begin VB.Frame fraPr 
      Caption         =   "Prueba"
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   240
      TabIndex        =   0
      Top             =   75
      Width           =   6945
      Begin VB.TextBox txtPr 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Index           =   2
         Left            =   4425
         TabIndex        =   12
         Tag             =   "Nombre|Nombre del paciente"
         Top             =   750
         Width           =   2265
      End
      Begin VB.TextBox txtPr 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Index           =   0
         Left            =   1050
         TabIndex        =   5
         Tag             =   "Historia|N� de historia del paciente"
         Top             =   750
         Width           =   1050
      End
      Begin VB.TextBox txtPr 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Index           =   1
         Left            =   2175
         TabIndex        =   4
         Tag             =   "Nombre|Nombre del paciente"
         Top             =   750
         Width           =   2190
      End
      Begin VB.TextBox txtPaciente 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Index           =   0
         Left            =   1050
         MaxLength       =   12
         TabIndex        =   2
         Tag             =   "Historia|N� de historia del paciente"
         Top             =   375
         Width           =   1200
      End
      Begin VB.TextBox txtPaciente 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Index           =   1
         Left            =   2325
         TabIndex        =   1
         Tag             =   "Nombre|Nombre del paciente"
         Top             =   375
         Width           =   4365
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Prueba:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   225
         TabIndex        =   6
         Top             =   750
         Width           =   765
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Paciente:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   150
         TabIndex        =   3
         Top             =   375
         Width           =   840
      End
   End
   Begin Threed.SSCommand cmdVer 
      Height          =   375
      Left            =   8850
      TabIndex        =   8
      Top             =   7575
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "&Ver ^"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSCommand cmdSalir 
      Height          =   375
      Left            =   10200
      TabIndex        =   9
      Top             =   8025
      Width           =   1200
      _Version        =   65536
      _ExtentX        =   2117
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "&Salir"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSCommand cmdSeleccionar 
      Height          =   375
      Left            =   10200
      TabIndex        =   10
      Top             =   7575
      Width           =   1185
      _Version        =   65536
      _ExtentX        =   2090
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "A&brir"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSCommand cmdIntroducir 
      Height          =   375
      Left            =   8850
      TabIndex        =   11
      Top             =   8025
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "&Introducir ^"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.TreeView tvwMuestra 
      Height          =   6165
      Left            =   225
      TabIndex        =   7
      Top             =   1350
      Width           =   6990
      _ExtentX        =   12330
      _ExtentY        =   10874
      _Version        =   327682
      LabelEdit       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin ComctlLib.TreeView tvwVarios 
      Height          =   7320
      Left            =   7275
      TabIndex        =   26
      Top             =   150
      Width           =   4140
      _ExtentX        =   7303
      _ExtentY        =   12912
      _Version        =   327682
      LabelEdit       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Label lblObserv 
      AutoSize        =   -1  'True
      Caption         =   "Label1"
      Height          =   195
      Left            =   3450
      TabIndex        =   25
      Top             =   3375
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Menu mnuVer 
      Caption         =   "Ver"
      NegotiatePosition=   2  'Middle
      Visible         =   0   'False
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "&Historial"
         Index           =   10
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "&Informe"
         Index           =   20
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "&Diagn�sticos ..."
         Index           =   30
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "D&atos"
         Index           =   40
      End
      Begin VB.Menu mnuVerOpcion 
         Caption         =   "&Fotos"
         Index           =   50
      End
   End
   Begin VB.Menu mnuIntroducir 
      Caption         =   "Acciones"
      Visible         =   0   'False
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "&Observaciones ..."
         Index           =   10
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "&Problemas ..."
         Index           =   20
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "&Inter�s ..."
         Index           =   30
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "&Enviar ..."
         Index           =   40
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "&Almacenar ..."
         Index           =   50
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "&Foto ..."
         Index           =   60
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "&Diagn�stico ..."
         Index           =   70
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "-"
         Index           =   72
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "Di&sponible"
         Index           =   75
      End
      Begin VB.Menu mnuIntroducirOpcion 
         Caption         =   "A&gotado"
         Index           =   80
      End
   End
End
Attribute VB_Name = "frmP_Peticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim NBloq As String, NumBloques As Integer, GenBloqAuto As Integer
Dim nRef As String
Dim nTec As String, TiTec As Integer
Dim cFija As Integer, cDecal As Integer, Pedir As Integer
Dim Carp() As Integer
Dim Tec() As String
Dim Org() As String ' Matriz para guardar las muestras distintas
Dim Diag() As String
Dim DatosMues() As Mues
Dim FecPr(1 To 3) As String
Dim Bloq() As Bloque
Dim cPat As String, FSotud As String
Public NH As Long, NAct As Long
Dim TiPr As Integer, num As Integer, numportas As Integer

Dim Mover1 As Integer, Mover2 As Integer, Mover3 As Integer
Dim AltoInitvwBloque As Long, AltoInitvwTecnica As Long, AltoInitvwPerfil As Long
Dim AnchoIni As Long, TopInitvwvarios As Long, TopInitvwTecnica As Long, TopInitvwPerfil As Long
Dim LeftIni As Long
Dim valido As Boolean

Dim origen As Integer
' Constantes para saber qu� se ha arrastrado
Const apORIGENBLOQUE = 1
Const apORIGENFIJADOR = 2
Const apORIGENDECAL = 3
Const apORIGENTECNICA = 4
Const apORIGENPERFIL = 5

Option Explicit

Sub CambiarFijadorBloque(clave As String)
Dim sql As String
Dim rdoQ As rdoQuery
Dim num As Integer
Dim numMues As Integer
Dim texto As String, nuevo_texto As String

  num = Right(clave, Len(clave) - 1)
  sql = "UPDATE AP3400 SET AP19_CodFija = ? WHERE AP21_CodRef = ? AND AP34_CodBloq = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeVARCHAR
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(0) = cFija
  rdoQ(1) = nRef
  rdoQ(2) = num
  rdoQ.Execute
  numMues = Right(tvwMuestra.Nodes(clave).Parent.Key, Len(tvwMuestra.Nodes(clave).Parent.Key) - 1)
'  If DatosMues(numMues).cFija <> cFija Then
    texto = tvwMuestra.Nodes.item(clave).Text
    If InStr(texto, "      Fijador") <> 0 Then
      nuevo_texto = Left(texto, InStr(texto, "      Fijador") - 1) & "      Fijador: " & tvwVarios.SelectedItem.Text
      If InStr(texto, "      Decal") <> 0 Then
        nuevo_texto = nuevo_texto & Right(texto, Len(texto) - InStr(texto, "      Decal") + 1)
      End If
    ElseIf InStr(texto, "      Decal") <> 0 Then
      nuevo_texto = Left(texto, InStr(texto, "      Decal") - 1) & "      Fijador: " & tvwVarios.SelectedItem.Text
      nuevo_texto = nuevo_texto & Right(texto, Len(texto) - InStr(texto, "      Decal") + 1)
    Else
      nuevo_texto = texto & "      Fijador: " & tvwVarios.SelectedItem.Text
    End If
    tvwMuestra.Nodes.item(clave).Text = nuevo_texto
'  End If
End Sub

Sub CambiarFijadorMuestra(clave As String)
Dim sql As String
Dim rdoQ As rdoQuery
Dim num As Integer
Dim numMues As Integer
Dim texto As String, nuevo_texto As String

  num = Right(clave, Len(clave) - 1)
  sql = "UPDATE AP2500 SET AP19_CodFija = ? WHERE AP21_CodRef = ? AND PR52NumIDMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cFija
  rdoQ(1) = nRef
  rdoQ(2) = num
  rdoQ.Execute
'  If DatosMues(numMues).cFija <> cFija Then
    texto = tvwMuestra.Nodes.item(clave).Text
    If InStr(texto, "      Fijador") <> 0 Then
      nuevo_texto = Left(texto, InStr(texto, "      Fijador") - 1) & "      Fijador: " & tvwVarios.SelectedItem.Text
      If InStr(texto, "      Decal") <> 0 Then
        nuevo_texto = nuevo_texto & Right(texto, Len(texto) - InStr(texto, "      Decal") + 1)
      End If
    Else
      nuevo_texto = texto & "      Fijador: " & tvwVarios.SelectedItem.Text
    End If
    tvwMuestra.Nodes.item(clave).Text = nuevo_texto
'  End If
End Sub


Sub CambiarDecalBloque(clave As String)
Dim sql As String, texto As String
Dim rdoQ As rdoQuery
Dim num As Integer
Dim numMues As Integer

  num = Right(clave, Len(clave) - 1)
  sql = "UPDATE AP3400 SET AP22_Coddecal = ? WHERE AP21_CodRef = ? AND AP34_CodBloq = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeVARCHAR
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(0) = cDecal
  rdoQ(1) = nRef
  rdoQ(2) = num
  rdoQ.Execute
  numMues = Right(tvwMuestra.Nodes(clave).Parent.Key, Len(tvwMuestra.Nodes(clave).Parent.Key) - 1)
'  If Val(DatosMues(numMues).cDecal) <> cDecal Then
    texto = tvwMuestra.Nodes.item(clave).Text
    If InStr(texto, "      Decal.") <> 0 Then
      texto = Left(texto, InStr(texto, "      Decal.") - 1) & "      Decal.: " & tvwVarios.SelectedItem.Text
    Else
      texto = texto & "      Decal.: " & tvwVarios.SelectedItem.Text
    End If
    tvwMuestra.Nodes.item(clave).Text = texto
'  End If
End Sub


Sub CambiarDecalMuestra(clave As String)
Dim sql As String, texto As String
Dim rdoQ As rdoQuery
Dim num As Integer

  num = Right(clave, Len(clave) - 1)
  sql = "UPDATE AP2500 SET AP22_Coddecal = ? WHERE AP21_CodRef = ? AND PR52NumIDMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeVARCHAR
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(0) = cDecal
  rdoQ(1) = nRef
  rdoQ(2) = num
  rdoQ.Execute
'  If Val(DatosMues(numMues).cDecal) <> cDecal Then
    texto = tvwMuestra.Nodes.item(clave).Text
    If InStr(texto, "      Decal.") <> 0 Then
      texto = Left(texto, InStr(texto, "      Decal.") - 1) & "      Decal.: " & tvwVarios.SelectedItem.Text
    Else
      texto = texto & "      Decal.: " & tvwVarios.SelectedItem.Text
    End If
    tvwMuestra.Nodes.item(clave).Text = texto
'  End If
End Sub

Sub LlenarMuestras()
Dim sql As String, sqlBloque As String, sqlTec As String
Dim rdo As rdoResultset, rdoTec As rdoResultset, rdoBloq As rdoResultset
Dim rdoQ As rdoQuery, rdoQTec As rdoQuery, rdoQBloq As rdoQuery
Dim nodo As Node, nodo1 As Node
Dim i As Integer, ctrl As Integer, j As Integer, k As Integer
Dim Raiz As String, texto As String, nMues As Integer, NBloq As Integer, nTec As Integer

'*************************************************************************************
'Identificaci�n de las claves en el TreeView de Muestras
'   Para la muestra la clave es:
'       M & Identificaci�n de la muestra (correlativo dentro de la prueba)
'   Para los bloques asociados a cada muestra la clave es:
'       B & Identificaci�n del bloque (correlativo dentro de la prueba)
'   Para las t�cnicas asociadas a cada bloque la clave es:
'       T & Identificaci�n de la t�cnica (correlativo dentro del bloque) _
'     & B & Identificaci�n del bloque (correlativo dentro de la prueba)
'*************************************************************************************
  
 ' Set tvwTecnica.ImageList = frmPrincipal.imgIcos
  Set tvwMuestra.ImageList = frmPrincipal.imgIcos
  If tvwMuestra.Nodes.Count > 0 Then tvwMuestra.Nodes.Clear
  NumBloques = 0
  i = 0
  k = 0
  Set nodo = tvwMuestra.Nodes.Add(, , "Raiz", "Muestras", 1)
  sql = "SELECT AP2500.PR52NumIDMuestra, PR5200.PR24CodMuestra, " _
      & "PR2400.PR24DesCompleta||'. '||PR5200.PR52DesMuestra," _
      & "AP2500.AP46_CodEstMues, AP4600.AP46_Desig, PR2400.PR24DesCompleta, " _
      & "PR5200.PR52IndIntra, AP2500.AP22_CodDecal, AP2200.AP22_Desig, " _
      & "AP2500.AP19_CodFija, AP1900.AP19_Desig, PR5200.PR52FecExtrac, " _
      & "AP2500.AP25_FecRecep, AP2500.AP25_FecAlm, AP2500.AP25_FecRetir " _
      & "FROM AP2500, PR5200, PR2400, AP4600, AP2200, AP1900 WHERE " _
      & "AP2500.PR52NumMuestra = PR5200.PR52NumMuestra AND " _
      & "PR5200.PR24CodMuestra = PR2400.PR24CodMuestra AND " _
      & "AP2500.AP46_CodEstMues = AP4600.AP46_CodEstMues AND " _
      & "AP2500.AP19_CodFija = AP1900.AP19_CodFija (+) AND " _
      & "AP2500.AP22_CodDecal = AP2200.AP22_CodDecal (+) AND " _
      & "AP2500.AP21_CodRef = ? ORDER BY AP2500.PR52NumIDMuestra"
  Set rdoQ = objApp.rdoConnect.CreateQuery("Pac", sql)
  rdoQ.sql = sql
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  ReDim Org(1 To 2, 1 To 1)
  ReDim Diag(1 To 2, 1 To 1)
  
  sqlBloque = "SELECT AP3400.AP34_CodBloq, AP0500.AP05_Desig, AP3400.AP47_CodEstBloq, " _
      & "AP4700.AP47_Desig, AP3400.AP05_CodTiBloq, AP3400.AP19_CodFija, AP3400.AP22_CodDecal, " _
      & "AP1900.AP19_Desig, AP2200.AP22_Desig, AP3400.AP34_NumBloq, AP3400.AP34_FecSotud, " _
      & "AP3400.AP34_FecTalla, AP3400.AP34_FecReali, AP3400.AP34_FecCorte " _
      & "FROM AP4700,AP3400,AP0500, AP1900, AP2200 WHERE " _
      & "AP3400.AP05_CodTiBloq = AP0500.AP05_CodTiBloq AND " _
      & "AP3400.AP47_CodEstBloq = AP4700.AP47_CodEstBloq AND " _
      & "AP3400.AP19_CodFija = AP1900.AP19_CodFija (+) AND " _
      & "AP3400.AP22_CodDecal = AP2200.AP22_CodDecal (+) AND " _
      & "AP3400.AP21_CodRef = ? AND " _
      & "AP3400.PR52NumIDMuestra = ? ORDER BY AP34_CodBloq"
  
  Set rdoQBloq = objApp.rdoConnect.CreateQuery("Bloq", sqlBloque)
  
  sqlTec = "SELECT AP3600.AP36_CodPorta, AP1200.AP12_Desig, AP3600.AP48_CodEstPorta, " _
      & "AP4800.AP48_Desig, AP3600.AP12_CodTec, AP3600.AP36_FecSotud, " _
      & "AP3600.AP36_FecCorte, AP3600.AP36_FecReali " _
      & "FROM AP3600, AP1200, AP4800 WHERE " _
      & "AP3600.AP12_CodTec = AP1200.AP12_CodTec AND " _
      & "AP3600.AP48_CodEstPorta = AP4800.AP48_CodEstPorta AND " _
      & "AP3600.AP21_CodRef = ? AND " _
      & "AP3600.AP34_CodBloq = ? ORDER BY AP3600.AP36_CodPorta"
  Set rdoQTec = objApp.rdoConnect.CreateQuery("Tec", sqlTec)
  
  While rdo.EOF = False
    nMues = rdo(0)
    ReDim Preserve DatosMues(1 To nMues)
    If IsNull(rdo(7)) Then
      DatosMues(nMues).cDecal = ""
      DatosMues(nMues).Decal = ""
    Else
      DatosMues(nMues).cDecal = rdo(7)
      DatosMues(nMues).Decal = rdo(8)
    End If
    If IsNull(rdo(9)) Then
      DatosMues(nMues).cFija = ""
      DatosMues(nMues).Fija = ""
    Else
      DatosMues(nMues).cFija = rdo(9)
      DatosMues(nMues).Fija = rdo(10)
    End If
    DatosMues(nMues).FExtrac = rdo(11)
    DatosMues(nMues).fRecep = rdo(12)
    If Not IsNull(rdo(13)) Then DatosMues(nMues).FAlm = rdo(13) Else DatosMues(nMues).FAlm = ""
    If Not IsNull(rdo(14)) Then DatosMues(nMues).FRet = rdo(14) Else DatosMues(nMues).FRet = ""
    
' Llenar la matriz con la muestras distintas
    ctrl = False
    For j = 1 To UBound(Org, 2)
      If Org(1, j) = rdo(1) Then
        ctrl = True
        Exit For
      End If
    Next j
    If ctrl = False Then
      i = i + 1
      ReDim Preserve Org(1 To 2, 1 To i)
      Org(1, i) = rdo(1)
      Org(2, i) = rdo(5)
    End If
    
    texto = rdo(0) & ".- " & rdo(2) & " (" & rdo(4) & ")"
    If Not IsNull(rdo(7)) Then texto = texto & "      Decal.: " & rdo(8)
    If Not IsNull(rdo(9)) Then texto = texto & "      Fijador: " & rdo(10)
    If rdo(6) = 0 Then
      If rdo(3) = apMUESTRANODISPONIBLE Then
        Set nodo1 = tvwMuestra.Nodes.Add("Raiz", tvwChild, "M" & nMues, texto, apICONMUESTRAAGOTADA, apICONMUESTRAAGOTADA)
      Else
        Set nodo1 = tvwMuestra.Nodes.Add("Raiz", tvwChild, "M" & nMues, texto, apICONMUESTRA, apICONMUESTRA)
      End If
    Else
      If rdo(3) = apMUESTRARECEPCIONADA Then
        Set nodo1 = tvwMuestra.Nodes.Add("Raiz", tvwChild, "M" & nMues, texto, apICONINTRA, apICONINTRA)
      ElseIf rdo(3) = apMUESTRANODISPONIBLE Then
        Set nodo1 = tvwMuestra.Nodes.Add("Raiz", tvwChild, "M" & nMues, texto, apICONINTRAAGOTADA, apICONINTRAAGOTADA)
      End If
    End If
    nodo1.Tag = rdo(6)
    rdoQBloq(0) = nRef
    rdoQBloq(1) = rdo(0)
    Set rdoBloq = rdoQBloq.OpenResultset(rdOpenForwardOnly)
    While rdoBloq.EOF = False
      NBloq = rdoBloq(0)
      ReDim Preserve Bloq(1 To NBloq)
      Bloq(NBloq).FSotud = rdoBloq(10)
      If IsNull(rdoBloq(11)) Then Bloq(NBloq).FTalla = "" Else Bloq(NBloq).FTalla = rdoBloq(11)
      If IsNull(rdoBloq(12)) Then Bloq(NBloq).FReali = "" Else Bloq(NBloq).FReali = rdoBloq(12)
      If IsNull(rdoBloq(13)) Then Bloq(NBloq).FCorte = "" Else Bloq(NBloq).FCorte = rdoBloq(13)
      If rdoBloq(4) <> 1 Then
        If rdoBloq(2) <> apBLOQUEBORRADO Then
          If IsNull(rdoBloq(9)) Then
            texto = rdoBloq(1)
          Else
            texto = rdoBloq(9) & ".- " & rdoBloq(1)
          End If
          texto = texto & " (" & rdoBloq(3) & ")"
          If rdoBloq(5) <> DatosMues(rdo(0)).cFija Then texto = texto & "      Fijador: " & rdoBloq(7)
          If rdoBloq(6) <> DatosMues(rdo(0)).cDecal Then texto = texto & "      Decal.: " & rdoBloq(8)
          If rdoBloq(2) = apBLOQUENODISPONIBLE Then
            Set nodo = tvwMuestra.Nodes.Add("M" & rdo(0), tvwChild, "B" & rdoBloq(0), texto, apICONBLOQUEAGOTADO, apICONBLOQUEAGOTADO)
          Else
            Set nodo = tvwMuestra.Nodes.Add("M" & rdo(0), tvwChild, "B" & rdoBloq(0), texto, apICONBLOQUE, apICONBLOQUE)
          End If
        End If
        nodo.Tag = rdoBloq(4)
      End If
      NumBloques = NumBloques + 1
      rdoQTec(0) = nRef
      rdoQTec(1) = rdoBloq(0)
      Set rdoTec = rdoQTec.OpenResultset(rdOpenForwardOnly)
      If rdoBloq(4) = 1 Then Raiz = "M" & rdo(0) Else Raiz = "B" & rdoBloq(0)
      While rdoTec.EOF = False
        nTec = rdoTec(0)
        ReDim Preserve Bloq(NBloq).port(1 To nTec)
        Bloq(NBloq).port(nTec).FSotud = rdoTec(5)
        If IsNull(rdoTec(6)) Then Bloq(NBloq).port(nTec).FReali = "" Else Bloq(NBloq).port(nTec).FReali = rdoTec(6)
        If IsNull(rdoTec(7)) Then Bloq(NBloq).port(nTec).FTincion = "" Else Bloq(NBloq).port(nTec).FTincion = rdoTec(7)
        If rdoTec(2) <> apPORTABORRADO Then
          Set nodo = tvwMuestra.Nodes.Add(Raiz, tvwChild, "T" & rdoTec(0) & "B" & rdoBloq(0), rdoTec(0) & ".- " & rdoTec(1) & " (" & rdoTec(3) & ")", apICONTECNICA, apICONTECNICA)
          nodo.Tag = rdoTec(4)
        End If
        rdoTec.MoveNext
      Wend
      rdoBloq.MoveNext
      rdoTec.Close
    Wend
    rdoBloq.Close
    rdo.MoveNext
  Wend
  rdo.Close
  If tvwMuestra.Nodes("Raiz").Children > 0 Then
    tvwMuestra.Nodes(nodo1.Index).EnsureVisible
  End If
  Call CerrarrdoQueries

End Sub
Sub LlenarMuestras1()
Dim sql As String, sqlBloque As String, sqlTec As String
Dim rdo As rdoResultset, rdoTec As rdoResultset, rdoBloq As rdoResultset
Dim rdoQ As rdoQuery, rdoQTec As rdoQuery, rdoQBloq As rdoQuery
Dim nodo As Node, nodo1 As Node
Dim i As Integer, ctrl As Integer, j As Integer, k As Integer
Dim Raiz As String, texto As String
Dim cMues As Integer, cBloq As Integer

'*************************************************************************************
'Identificaci�n de las claves en el TreeView de Muestras
'   Para la muestra la clave es:
'       M & Identificaci�n de la muestra (correlativo dentro de la prueba)
'   Para los bloques asociados a cada muestra la clave es:
'       B & Identificaci�n del bloque (correlativo dentro de la prueba)
'   Para las t�cnicas asociadas a cada bloque la clave es:
'       T & Identificaci�n de la t�cnica (correlativo dentro del bloque) _
'     & B & Identificaci�n del bloque (correlativo dentro de la prueba)
'*************************************************************************************
  
  If tvwMuestra.Nodes.Count > 0 Then tvwMuestra.Nodes.Clear
  NumBloques = 0
  i = 0
  k = 0
  Set nodo = tvwMuestra.Nodes.Add(, , "Raiz", "Muestras", 1)
  sql = "SELECT AP2500.PR52NumIDMuestra, " _
      & "AP3400.AP34_CodBloq " _
      & " FROM " _
      & "AP2500, AP3400 " _
      & " WHERE " _
      & "AP2500.AP21_CodRef = AP3400.AP21_CodRef (+) AND " _
      & "AP2500.PR52NumIDMuestra = AP3400.PR52NumIDMuestra (+) AND "
  sql = sql & "AP2500.AP21_CodRef = ?"
  
'  SQL = "SELECT AP2500.PR52NumIDMuestra, AP2300.AP23_Desig||'. '||AP2500.AP25_TiMues, " _
      & "AP2500.AP46_CodEstMues, AP4600.AP46_Desig, AP2500.AP23_CodOrg, " _
      & "AP2500.AP16_CodDiag, AP1600.AP16_Desig, AP2500.AP25_IndIntra, M19.AP19_Desig, " _
      & "M22.AP22_Desig, AP2500.AP19_CodFija, AP2500.AP22_CodDecal, AP3400.AP34_CodBloq, " _
      & "AP0500.AP05_Desig, AP3400.AP47_CodEstBloq, AP4700.AP47_Desig, AP3400.AP05_CodTiBloq, " _
      & "AP3400.AP19_CodFija, AP3400.AP22_CodDecal, B19.AP19_Desig, B22.AP22_Desig, " _
      & "AP3600.AP36_CodPorta, AP1200.AP12_Desig, AP3600.AP48_CodEstPorta, " _
      & "AP4800.AP48_Desig, AP3600.AP36_FecSotud FROM " _
      & "AP2500, AP2300, AP4600, AP1600, AP1900 M19, AP2200 M22, AP3400, AP0500, AP4700, " _
      & "AP1900 B19, AP2200 B22, AP3600, AP1200, AP4800 WHERE " _
      & "AP2500.AP23_CodOrg = AP2300.AP23_CodOrg AND " _
      & "AP2500.AP46_CodEstMues = AP4600.AP46_CodEstMues AND " _
      & "AP2500.AP16_CodDiag = AP1600.AP16_CodDiag AND " _
      & "AP2500.AP19_CodFija = M19.AP19_CodFija (+) AND " _
      & "AP2500.AP22_CodDecal = M22.AP22_CodDecal (+) AND " _
      & "AP2500.AP21_CodRef = AP3400.AP21_CodRef (+) AND " _
      & "AP2500.PR52NumIDMuestra = AP3400.PR52NumIDMuestra (+) AND " _
      & "AP3400.AP05_CodTiBloq = AP0500.AP05_CodTiBloq AND " _
      & "AP3400.AP47_CodEstBloq = AP4700.AP47_CodEstBloq AND " _
      & "AP3400.AP19_CodFija = B19.AP19_CodFija (+) AND " _
      & "AP3400.AP22_CodDecal = B22.AP22_CodDecal (+) AND " _
      & "AP3400.AP21_CodRef = AP3600.AP21_CodRef (+) AND " _
      & "AP3400.AP34_CodBloq = AP3600.AP34_CodBloq (+) AND " _
      & "AP3600.AP12_CodTec = AP1200.AP12_CodTec AND " _
      & "AP3600.AP48_CodEstPorta = AP4800.AP48_CodEstPorta AND "
'  SQL = SQL & "AP2500.AP21_CodRef = ?  AND " _
      & "AP3400.AP47_CodEstBloq <> ? AND " _
      & "AP3600.AP48_CodEstPorta <> ? " _
      & "ORDER BY AP2500.PR52NumIDMuestra ASC, AP3400.AP34_CodBloq ASC, AP3600.AP36_CodPorta ASC"
  
  
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ.sql = sql
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = apBLOQUEBORRADO
  rdoQ(2) = apPORTABORRADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  ReDim Org(1 To 2, 1 To 1)
  ReDim Diag(1 To 2, 1 To 1)
  
  cMues = -1
  cBloq = -1
  While rdo.EOF = False
    ReDim Preserve DatosMues(1 To rdo(0))
    If cMues <> rdo(0) Then
      If IsNull(rdo(10)) Then
        DatosMues(rdo(0)).cFija = ""
        DatosMues(rdo(0)).Fija = ""
      Else
        DatosMues(rdo(0)).cFija = rdo(10)
        DatosMues(rdo(0)).Fija = rdo(8)
      End If
      If IsNull(rdo(11)) Then
        DatosMues(rdo(0)).cDecal = ""
        DatosMues(rdo(0)).Decal = ""
      Else
        DatosMues(rdo(0)).cDecal = rdo(11)
        DatosMues(rdo(0)).Decal = rdo(9)
      End If
      ' Llenar la matriz con la muestras distintas
      ctrl = False
      For j = 1 To UBound(Org, 2)
        If Org(1, j) = rdo(4) Then
          ctrl = True
          Exit For
        End If
      Next j
      If ctrl = False Then
        i = i + 1
        ReDim Preserve Org(1 To 2, 1 To i)
        Org(1, i) = rdo(4)
        Org(2, i) = rdo(5)
      End If
    ' Llenar la matriz con los diagn�sticos de presunci�n distintos
      ctrl = False
      For j = 1 To UBound(Diag, 2)
        If Diag(1, j) = rdo(6) Then
          ctrl = True
          Exit For
        End If
      Next j
      If ctrl = False Then
        k = k + 1
        ReDim Preserve Diag(1 To 2, 1 To k)
        Diag(1, k) = rdo(6)
        Diag(2, k) = rdo(7)
      End If
      texto = rdo(1) & " (" & rdo(3) & ")"
      If Not IsNull(rdo(8)) Then texto = texto & "      Fijador: " & rdo(8)
      If Not IsNull(rdo(9)) Then texto = texto & "      Decal.: " & rdo(9)
      Set nodo1 = tvwMuestra.Nodes.Add("Raiz", tvwChild, "M" & rdo(0), texto, 2, 2)
      nodo1.Tag = rdo(7)  ' Si la muestra es intra
    End If
    
    
    
'      rdoQBloq(0).Type = rdTypeVARCHAR
'      rdoQBloq(0) = NRef
'      rdoQBloq(1).Type = rdTypeINTEGER
'      rdoQBloq(1) = rdo(0)
'      Set rdoBloq = rdoQBloq.OpenResultset(rdOpenForwardOnly)
'      While rdoBloq.EOF = False
'        If rdoBloq(4) <> 1 Then
'          NumBloques = NumBloques + 1
'          texto = rdoBloq(0) & ".- " & rdoBloq(1) & " (" & rdoBloq(3) & ")"
'          If rdoBloq(5) <> DatosMues(rdo(0)).cFija Then texto = texto & "      Fijador: " & rdoBloq(7)
'          If rdoBloq(6) <> DatosMues(rdo(0)).cDecal Then texto = texto & "      Decal.: " & rdoBloq(8)
'          Set nodo = tvwMuestra.Nodes.Add("M" & rdo(0), tvwChild, "B" & rdoBloq(0), texto, 3, 3)
'          nodo.Tag = rdoBloq(4)
'        End If
'        rdoQTec(0).Type = rdTypeVARCHAR
'        rdoQTec(0) = NRef
'        rdoQTec(1).Type = rdTypeINTEGER
'        rdoQTec(1) = rdoBloq(0)
'        Set rdoTec = rdoQTec.OpenResultset(rdOpenForwardOnly)
'        If rdoBloq(4) = 1 Then Raiz = "M" & rdo(0) Else Raiz = "B" & rdoBloq(0)
'        While rdoTec.EOF = False
'          Set nodo = tvwMuestra.Nodes.Add(Raiz, tvwChild, "T" & rdoTec(0) & "B" & rdoBloq(0), rdoTec(0) & ".- " & rdoTec(1) & ". " & rdoTec(4) & " (" & rdoTec(3) & ")", 4, 4)
'          rdoTec.MoveNext
'        Wend
'        rdoBloq.MoveNext
'        rdoTec.Close
'      Wend
'      rdoBloq.Close
      cMues = rdo(0)
      rdo.MoveNext
    Wend
    rdo.Close
  
    
  If tvwMuestra.Nodes("Raiz").Children > 0 Then
    tvwMuestra.Nodes(nodo1.Index).EnsureVisible
  End If
  Call CerrarrdoQueries

End Sub

Private Sub cmdIntroducir_Click()
  Me.PopupMenu mnuIntroducir, vbPopupMenuLeftAlign, cmdIntroducir.Left, 5520
End Sub


Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub cmdSeleccionar_Click()
  frmBuscPr.Show vbModal
End Sub

Private Sub cmdVer_Click()
  Me.PopupMenu mnuVer, vbPopupMenuLeftAlign, cmdVer.Left, 6230
End Sub

Private Sub Form_Activate()
  If nRef <> objPipe.PipeGet("NumRef") Then Call LlenarDatosPr
End Sub

Private Sub Form_Load()
  Call LlenarDatosComunes
  Call LlenarDatosPr
  Call Dibujar
End Sub
Sub Dibujar()

  AnchoIni = 4000
  LeftIni = 7350
  TopInitvwvarios = 150
  tvwVarios.Width = AnchoIni
  tvwVarios.Left = LeftIni
  picSep3.Width = 25
  picSep3.Left = LeftIni - picSep3.Width
  picSep3.Top = tvwVarios.Top
  picSep3.Height = tvwVarios.Height
  
  tvwMuestra.Width = picSep3.Left - tvwMuestra.Left
  fraPr.Width = picSep3.Left - fraPr.Left
End Sub
Sub LlenarDatosPr()

  nRef = objPipe.PipeGet("NumRef")
  cmdVer.Tag = frmBuscPr.rdo(3)
  TiPr = frmBuscPr.rdo(7)
  Call LlenarTexto(frmBuscPr.rdo)
  Call LlenarMuestras
  Call LlenarPerfiles
  Unload frmBuscPr

End Sub
Sub LlenarDatosComunes()

  Set tvwVarios.ImageList = frmPrincipal.imgIcos
  tvwVarios.Nodes.Add , , "Raiz", "Varios", apICONRAIZ, apICONRAIZ
  Call LlenarBloques
  Call LlenarTecnicas
  Call LlenarFijador
  Call LlenarDecal
  tvwVarios.Nodes("Raiz").Child.EnsureVisible
End Sub
Sub LlenarPerfilOrgano()
Dim rdo As rdoResultset
Dim sql As String, strOrg As String
Dim nodo As Node
Dim i As Integer, j As Integer, cPerf As Long
  
  strOrg = "("
  For i = 1 To UBound(Org, 2)
    strOrg = strOrg & Org(1, i) & ", "
  Next i
  strOrg = Left(strOrg, Len(strOrg) - 2) & ")"
  sql = "SELECT AP0100.AP01_CodPerf, AP0100.AP01_Desig, AP1200.AP12_CodTec, " _
      & "AP1200.AP12_Desig, AP2700.AP27_NumTec, AP1200.AP04_CodTiTec FROM " _
      & "AP0100, AP1200, AP2700 WHERE " _
      & "AP0100.AP01_CodPerf = AP2700.AP01_CodPerf AND " _
      & "AP2700.AP12_CodTec = AP1200.AP12_CodTec AND " _
      & "AP0100.AP01_CodPerf IN (" _
      & "SELECT DISTINCT AP01_CodPerf FROM AP5800 WHERE PR24CodMuestra IN " & strOrg & ") AND " _
      & "AP0100.AP01_CodPerf NOT IN (" _
      & "SELECT AP01_CodPerf FROM AP5800)"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  cPerf = 0
  While rdo.EOF = False
    If cPerf <> rdo(0) Then
      cPerf = rdo(0)
      Set nodo = tvwVarios.Nodes.Add("Raiz", tvwChild, "P" & cPerf, rdo(1), apICONVARIOSTECNICA, apICONVARIOSTECNICA)
    End If
    For j = 1 To rdo(4)
      Set nodo = tvwVarios.Nodes.Add("P" & cPerf, tvwChild, "T" & rdo(2) & "P" & cPerf & "N" & j, rdo(3), apICONTECNICA, apICONTECNICA)
      nodo.Tag = rdo(5)
    Next j
    rdo.MoveNext
  Wend
  rdo.Close
End Sub
Sub LlenarBloques()
Dim rdo As rdoResultset
Dim sql As String
  
'*********************************************************************
'Identificaci�n de las claves en el TreeView de Bloques:
'   B & C�digo del tipo de bloque & A & Genere bloque autom�tico
'*********************************************************************
  tvwVarios.Nodes.Add "Raiz", tvwChild, "B", "Bloques", apICONBLOQUES
  sql = "SELECT AP05_CodTiBloq, AP05_Desig, AP05_IndAuto FROM AP0500 WHERE " _
      & "AP05_IndActiva = -1 AND AP05_CodTiBloq > 1"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    tvwVarios.Nodes.Add "B", tvwChild, "B" & rdo(0) & "A" & rdo(2), rdo(1), apICONBLOQUE, apICONBLOQUE
    rdo.MoveNext
  Wend
  rdo.Close

End Sub
Sub LlenarTecnicas()
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim sql As String
Dim nodo As Node
Dim NCarp As Integer, cTiTec As Integer
  
'*********************************************************************
'Identificaci�n de las claves en el TreeView de T�cnicas
'   Para el tipo de t�cnica la clave es:
'       I & C�digo del tipo de t�cnica
'   Para la t�cnica la clave es:
'       T & C�digo de la t�cnica
'*********************************************************************
  tvwVarios.Nodes.Add "Raiz", tvwChild, "T", "T�cnicas", apICONVARIOSTECNICA
    
  sql = "SELECT AP0400.AP04_CodTiTec, AP0400.AP04_Desig, AP0400.AP06_CodCarp, " _
      & "AP1200.AP12_CodTec, AP1200.AP12_Desig " _
      & "FROM AP0400, AP1200 WHERE " _
      & "AP0400.AP04_CodTiTec = AP1200.AP04_CodTiTec AND " _
      & "AP0400.AP04_IndActiva = -1 AND " _
      & "AP1200.AP12_IndActiva = -1 " _
      & "ORDER BY AP0400.AP04_CodTiTec, AP1200.AP12_CodTec"
  
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  NCarp = 0
  cTiTec = 0
  While rdo.EOF = False
    If cTiTec <> rdo(0) Then
      NCarp = NCarp + 1
      ReDim Preserve Carp(1 To 2, 1 To NCarp)
      Carp(1, NCarp) = rdo(0)
      Carp(2, NCarp) = rdo(2)
      tvwVarios.Nodes.Add "T", tvwChild, "I" & rdo(0), rdo(1), apICONVARIOSGRUPOTECNICA, apICONVARIOSGRUPOTECNICA
      cTiTec = rdo(0)
    End If
    If Not IsNull(rdo(3)) Then
      Set nodo = tvwVarios.Nodes.Add("I" & rdo(0), tvwChild, "T" & rdo(3), rdo(4), apICONTECNICA, apICONTECNICA)
      nodo.Tag = rdo(0)
    End If
    rdo.MoveNext
  Wend
  rdo.Close
End Sub
Sub LlenarFijador()
Dim rdo As rdoResultset
Dim sql As String
  
'*********************************************************************
'Identificaci�n de las claves en el TreeView de Fijador
'   Para el fijador la clave es:
'       F & C�digo del Fijador
'*********************************************************************
  tvwVarios.Nodes.Add "Raiz", tvwChild, "F", "Fijador", apICONFIJADOR
  
  sql = "SELECT AP19_CodFija, AP19_Desig FROM AP1900 ORDER BY AP19_Desig"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    tvwVarios.Nodes.Add "F", tvwChild, "F" & rdo(0), rdo(1), apICONFIJADORES, apICONFIJADORES
    rdo.MoveNext
  Wend
  
  rdo.Close
End Sub

Sub LlenarDecal()
Dim rdo As rdoResultset
Dim sql As String
  
'*********************************************************************
'Identificaci�n de las claves en el TreeView de Decalcificador
'   Para el Decalcificador la clave es:
'       A & C�digo del Decalcificador
'*********************************************************************
  tvwVarios.Nodes.Add "Raiz", tvwChild, "D", "Decalcificador", apICONDECALCIFICADOR
  
  sql = "SELECT AP22_CodDecal, AP22_Desig FROM AP2200 ORDER BY AP22_Desig"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    tvwVarios.Nodes.Add "D", tvwChild, "D" & rdo(0), rdo(1), apICONDECALCIFICADORES, apICONDECALCIFICADORES
    rdo.MoveNext
  Wend
  
  rdo.Close
End Sub

Sub LlenarPerfiles()
Dim rdo As rdoResultset, rdoTec As rdoResultset
Dim rdoQ As rdoQuery
Dim sql As String, sqlTec As String
Dim nodo As Node, nodo1 As Node
Dim i As Integer
  
'*************************************************************************************
'Identificaci�n de las claves en el TreeView de Perfiles
'   Para el perfil la clave es:
'       P & C�digo del Perfil
'   Para la t�cnica asociada al perfil la clave es:
'       T & C�digo de la t�cnica & P & C�digo del perfil & N & N�mero de t�cnica
'   N: Indica el n�mero de t�cnicas de una clase que hay que realizar
'NOTA: Las claves no se pueden duplicar, por lo que se ha construido de esa forma
'      la clave para la t�cnica
'**************************************************************************************
  
  On Error Resume Next
  tvwVarios.Nodes.Remove "P"
  On Error GoTo 0
  tvwVarios.Nodes.Add "Raiz", tvwChild, "P", "Perfiles", apICONRAIZ
  
  sql = "SELECT AP01_CodPerf, AP01_Desig FROM AP0100 ORDER BY AP01_Desig"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  
  sqlTec = "SELECT AP1200.AP12_CodTec, AP1200.AP12_Desig, AP2700.AP27_NumTec, " _
      & "AP1200.AP04_CodTiTec  FROM " _
      & "AP1200, AP2700 WHERE " _
      & "AP2700.AP12_CodTec = AP1200.Ap12_CodTec AND AP2700.AP01_CodPerf = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("Tec", sqlTec)
  
  While rdo.EOF = False
    tvwVarios.Nodes.Add "P", tvwChild, "P" & rdo(0), rdo(1), apICONPERFIL, apICONPERFIL
    rdoQ(0) = rdo(0)
    Set rdoTec = rdoQ.OpenResultset(rdOpenForwardOnly)
    While rdoTec.EOF = False
      For i = 1 To rdoTec(2)
        Set nodo1 = tvwVarios.Nodes.Add("P" & rdo(0), tvwChild, "T" & rdoTec(0) & "P" & rdo(0) & "N" & i, rdoTec(1), apICONTECNICA, apICONTECNICA)
        nodo1.Tag = rdoTec(3)
      Next i
      rdoTec.MoveNext
    Wend
    rdoTec.Close
    rdo.MoveNext
  Wend
  
  rdo.Close
End Sub
Sub LlenarTexto(rdoPac As rdoResultset)
  NH = rdoPac(0)
  NAct = rdoPac(5)
  FSotud = rdoPac(6)
  cPat = rdoPac(3)
  txtPaciente(0).Text = NH & "/" & NAct
  txtPaciente(1).Text = rdoPac(1)
  txtPr(0).Text = Right(nRef, 8)
  txtPr(1).Text = rdoPac(9) & " - " & rdoPac(2)
  txtPr(2).Text = rdoPac(8)
  
  FecPr(1) = rdoPac(10)
  FecPr(2) = rdoPac(11)
  If Not IsNull(rdoPac(12)) Then FecPr(3) = rdoPac(12) Else FecPr(3) = ""
  Call LlenarFechasPr
  rdoPac.Close
End Sub
Sub LlenarFechasPr()
  lblFec(0).Caption = "F. Sotud."
  txtFec(0).Text = FecPr(1)
  lblFec(1).Caption = "F. Recep."
  txtFec(1).Text = FecPr(2)
  lblFec(2).Caption = "F. Firma"
  txtFec(2).Text = FecPr(3)
  lblFec(3).Caption = ""
  txtFec(3).Text = ""
End Sub
Sub LlenarMuestrasBiop()
Dim sql As String, sqlBloque As String, sqlTec As String
Dim rdo As rdoResultset, rdoTec As rdoResultset, rdoBloq As rdoResultset
Dim rdoQ As rdoQuery, rdoQTec As rdoQuery, rdoQBloq As rdoQuery
Dim nodo As Node, nodo1 As Node
Dim i As Integer, ctrl As Integer, j As Integer, k As Integer

'*************************************************************************************
'Identificaci�n de las claves en el TreeView de Muestras
'   Para la muestra la clave es:
'       M & Identificaci�n de la muestra (correlativo dentro de la prueba)
'   Para los bloques asociados a cada muestra la clave es:
'       B & Identificaci�n del bloque (correlativo dentro de la prueba)
'   Para las t�cnicas asociadas a cada bloque la clave es:
'       T & Identificaci�n de la t�cnica (correlativo dentro del bloque) _
'     & B & Identificaci�n del bloque (correlativo dentro de la prueba)
'*************************************************************************************
  
  If tvwMuestra.Nodes.Count > 0 Then tvwMuestra.Nodes.Clear
  NumBloques = 0
  i = 0
  k = 0
  Set nodo = tvwMuestra.Nodes.Add(, , "Raiz", "Muestras", apICONRAIZ)
  sql = "SELECT AP2500.PR52NumIDMuestra, AP2300.AP23_Desig||'. '||AP2500.AP25_TiMues," _
      & "AP2500.AP46_CodEstMues, AP4600.AP46_Desig, AP2500.AP23_CodOrg, AP2300.AP23_Desig, " _
      & "AP2500.AP16_CodDiag, AP1600.AP16_Desig, AP2500.AP25_IndIntra " _
      & "FROM AP2500,AP2300,AP4600,AP1600 WHERE " _
      & "AP2500.AP23_CodOrg = AP2300.AP23_CodOrg AND " _
      & "AP2500.AP46_CodEstMues = AP4600.AP46_CodEstMues AND " _
      & "AP2500.AP16_CodDiag = AP1600.AP16_CodDiag AND " _
      & "AP2500.AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("Pac", sql)
  rdoQ.sql = sql
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  ReDim Org(1 To 2, 1 To 1)
  ReDim Diag(1 To 2, 1 To 1)
  
  sqlBloque = "SELECT AP34_CodBloq, AP05_Desig, AP3400.AP47_CodEstBloq, AP4700.AP47_Desig " _
      & "FROM AP4700,AP3400,AP0500 WHERE " _
      & "AP3400.AP05_CodTiBloq = AP0500.AP05_CodTiBloq AND " _
      & "AP3400.AP47_CodEstBloq = AP4700.AP47_CodEstBloq AND " _
      & "AP3400.AP21_CodRef = ? AND " _
      & "AP3400.PR52NumIDMuestra = ?"
  Set rdoQBloq = objApp.rdoConnect.CreateQuery("Bloq", sqlBloque)
  
  sqlTec = "SELECT AP3600.AP36_CodPorta, AP12_Desig, AP3600.AP48_CodEstPorta, " _
      & "AP4800.AP48_Desig, AP3600.AP36_FecSotud " _
      & "FROM AP3600,AP1200,AP4800 WHERE " _
      & "AP3600.AP12_CodTec = AP1200.AP12_CodTec AND " _
      & "AP3600.AP48_CodEstPorta = AP4800.AP48_CodEstPorta AND " _
      & "AP3600.AP21_CodRef = ? AND " _
      & "AP3600.AP34_CodBloq = ?"
  Set rdoQTec = objApp.rdoConnect.CreateQuery("Tec", sqlTec)
  
  While rdo.EOF = False
' Llenar la matriz con la muestras distintas
    ctrl = False
    For j = 1 To UBound(Org, 2)
      If Org(1, j) = rdo(4) Then
        ctrl = True
        Exit For
      End If
    Next j
    If ctrl = False Then
      i = i + 1
      ReDim Preserve Org(1 To 2, 1 To i)
      Org(1, i) = rdo(4)
      Org(2, i) = rdo(5)
    End If
    
' Llenar la matriz con los diagn�sticos de presunci�n distintos
    ctrl = False
    For j = 1 To UBound(Diag, 2)
      If Diag(1, j) = rdo(6) Then
        ctrl = True
        Exit For
      End If
    Next j
    If ctrl = False Then
      k = k + 1
      ReDim Preserve Diag(1 To 2, 1 To k)
      Diag(1, k) = rdo(6)
      Diag(2, k) = rdo(7)
    End If
    Set nodo1 = tvwMuestra.Nodes.Add("Raiz", tvwChild, "M" & rdo(0), rdo(1) & " (" & rdo(3) & ")", apICONMUESTRA, apICONMUESTRA)
    nodo1.Tag = rdo(8)
    rdoQBloq(0).Type = rdTypeVARCHAR
    rdoQBloq(0) = nRef
    rdoQBloq(1).Type = rdTypeINTEGER
    rdoQBloq(1) = rdo(0)
    Set rdoBloq = rdoQBloq.OpenResultset(rdOpenForwardOnly)
    While rdoBloq.EOF = False
      NumBloques = NumBloques + 1
      Set nodo = tvwMuestra.Nodes.Add("M" & rdo(0), tvwChild, "B" & rdoBloq(0), rdoBloq(0) & ".- " & rdoBloq(1) & " (" & rdoBloq(3) & ")", apICONBLOQUE, apICONBLOQUE)
      rdoQTec(0).Type = rdTypeVARCHAR
      rdoQTec(0) = nRef
      rdoQTec(1).Type = rdTypeINTEGER
      rdoQTec(1) = rdoBloq(0)
      Set rdoTec = rdoQTec.OpenResultset(rdOpenForwardOnly)
      While rdoTec.EOF = False
        Set nodo = tvwMuestra.Nodes.Add("B" & rdoBloq(0), tvwChild, "T" & rdoTec(0) & "B" & rdoBloq(0), rdoTec(0) & ".- " & rdoTec(1) & ". " & rdoTec(4) & " (" & rdoTec(3) & ")", apICONTECNICA, apICONTECNICA)
        rdoTec.MoveNext
      Wend
      rdoBloq.MoveNext
      rdoTec.Close
    Wend
    rdoBloq.Close
    rdo.MoveNext
  Wend
  rdo.Close
  If tvwMuestra.Nodes("Raiz").Children > 0 Then
    tvwMuestra.Nodes(nodo1.Index).EnsureVisible
  End If
  Call CerrarrdoQueries

End Sub
Private Sub Form_Unload(Cancel As Integer)
Dim frm As Form
  
  Erase Carp
  Erase Tec
  Erase Org
  Erase Diag
  Erase DatosMues
  For Each frm In Forms
    If frm.Name = "frmDiag" Then
'      Unload frmDiag
'      Exit For
    End If
  Next frm
  Call CerrarrdoQueries
End Sub


Private Sub mnuIntroducirOpcion_Click(Index As Integer)
Dim cMuestra As Integer, cBloque As Integer
  
  Select Case Index
    Case 10 'Observaciones
      On Error GoTo Observaciones
'      If tvwMuestra.SelectedItem.key <> "Raiz" Then
        Call objPipe.PipeSet("Boton", "Observaciones")
        frmDatos.Show vbModal
        Exit Sub
'      End If
    Case 20 'Problemas
      On Error GoTo Problemas
      If tvwMuestra.SelectedItem.Key <> "Raiz" Then
        Call objPipe.PipeSet("Boton", "Problemas")
        frmDatos.Show vbModal
        Exit Sub
      End If
      Exit Sub
    Case 30 'Inter�s
      On Error GoTo Interes
      If Left(tvwMuestra.SelectedItem.Key, 1) = "T" Or Left(tvwMuestra.SelectedItem.Key, 1) = "B" Then
        Call objPipe.PipeSet("Boton", "Interes")
        frmDatos.Show vbModal
        Exit Sub
      Else
        GoTo Interes
      End If
    Case 40 'Enviar
      On Error GoTo Enviar
      If Left(tvwMuestra.SelectedItem.Key, 1) = "T" Or Left(tvwMuestra.SelectedItem.Key, 1) = "B" Then
        Call objPipe.PipeSet("Boton", "Envio")
        frmDatos.Show vbModal
        Exit Sub
      Else
        GoTo Enviar
      End If
    Case 50 'Almacenar
      On Error GoTo Almacenar
      If Left(tvwMuestra.SelectedItem.Key, 1) = "M" Then
        Call objPipe.PipeSet("Boton", "Almacen_Muestra")
        frmDatos.Show vbModal
      ElseIf Left(tvwMuestra.SelectedItem.Key, 1) = "B" Then
        Call objPipe.PipeSet("Boton", "Almacen_Bloque")
        frmDatos.Show vbModal
      Else
        GoTo Almacenar
      End If
      Exit Sub
    Case 60 'Fotos
      On Error GoTo Fotos
      Call objPipe.PipeSet("Boton", "Fotos")
      If Left(tvwMuestra.SelectedItem.Key, 1) = "M" Or Left(tvwMuestra.SelectedItem.Key, 1) = "T" Then
        frmDatos.Show vbModal
      Else
        GoTo Fotos
      End If
      Exit Sub
    Case 70 'Diagn�sticos
      On Error GoTo Diag
      If tvwMuestra.SelectedItem.Key <> "Raiz" Then
        frmDiag.Show vbModal
        Exit Sub
      Else
        GoTo Diag
      End If
      Exit Sub
    Case 75 'Bloque o muestra disponible
      On Error GoTo Disponible
      If Left(tvwMuestra.SelectedItem.Key, 1) = "M" Then
        cMuestra = Right(tvwMuestra.SelectedItem.Key, Len(tvwMuestra.SelectedItem.Key) - 1)
        Call Disponible(nRef, cMuestra, -1)
      ElseIf Left(tvwMuestra.SelectedItem.Key, 1) = "B" Then
        cBloque = Right(tvwMuestra.SelectedItem.Key, Len(tvwMuestra.SelectedItem.Key) - 1)
        cMuestra = Right(tvwMuestra.SelectedItem.Parent.Key, Len(tvwMuestra.SelectedItem.Parent.Key) - 1)
        Call Disponible(nRef, cMuestra, cBloque)
      Else
        GoTo Disponible
      End If
      Exit Sub
    Case 80 'Bloque o muestra agotados
      On Error GoTo Agotado
      If Left(tvwMuestra.SelectedItem.Key, 1) = "M" Then
        cMuestra = Right(tvwMuestra.SelectedItem.Key, Len(tvwMuestra.SelectedItem.Key) - 1)
        Call Agotar(nRef, cMuestra, -1)
      ElseIf Left(tvwMuestra.SelectedItem.Key, 1) = "B" Then
        cBloque = Right(tvwMuestra.SelectedItem.Key, Len(tvwMuestra.SelectedItem.Key) - 1)
        cMuestra = Right(tvwMuestra.SelectedItem.Parent.Key, Len(tvwMuestra.SelectedItem.Parent.Key) - 1)
        Call Agotar(nRef, cMuestra, cBloque)
      Else
        GoTo Agotado
      End If
      Exit Sub
  End Select
  
Observaciones:
      MsgBox "Debe seleccionar alg�n elemento para introducir observaciones.", vbInformation, "Introducci�n de observaciones"
      Exit Sub
Diag:
      MsgBox "Los diagn�sticos deben estar asociados a las muestras, bloques o portas.", vbInformation, "Introducci�n de observaciones"
      Exit Sub
Problemas:
      MsgBox "Debe seleccionar alg�n elemento para introducir problemas.", vbInformation, "Introducci�n de observaciones"
      Exit Sub
Interes:
      MsgBox "Debe seleccionar alg�n bloque o porta.", vbInformation, "Introducci�n de motivos de inter�s"
      Exit Sub
Enviar:
      MsgBox "Debe seleccionar alg�n bloque o porta.", vbInformation, "Introducci�n de env�os"
      Exit Sub
Almacenar:
      MsgBox "Debe seleccionar alguna muestra o bloque para almacenar.", vbInformation, "Almacenamiento"
      Exit Sub
Fotos:
      MsgBox "Debe seleccionar una muestra o un porta", vbInformation, "Selecci�n incorrecta"
      Exit Sub
Disponible:
      MsgBox "Debe seleccionar alguna muestra o bloque.", vbInformation, "Elementos agotados"
      Exit Sub
Agotado:
      MsgBox "Debe seleccionar alguna muestra o bloque.", vbInformation, "Elementos agotados"
      Exit Sub
End Sub
Sub Agotar(nRef As String, cMuestra As Integer, cBloque As Integer)
Dim sql As String
Dim rdoQ As rdoQuery
Dim texto As String

  If cBloque = -1 Then
    sql = "UPDATE AP2500 SET AP46_CodEstMues = ? WHERE " _
        & "AP21_CodRef = ? AND " _
        & "PR52NumIDMuestra = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeVARCHAR
    rdoQ(2).Type = rdTypeINTEGER
    rdoQ(0) = apMUESTRANODISPONIBLE
    rdoQ(1) = nRef
    rdoQ(2) = cMuestra
    tvwMuestra.SelectedItem.Image = apICONMUESTRAAGOTADA
    tvwMuestra.SelectedItem.SelectedImage = apICONMUESTRAAGOTADA
  Else
    sql = "UPDATE AP3400 SET AP47_CodEstBloq = ? WHERE " _
        & "AP21_CodRef = ? AND " _
        & "AP34_CodBloq = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeVARCHAR
    rdoQ(2).Type = rdTypeINTEGER
    rdoQ(0) = apBLOQUENODISPONIBLE
    rdoQ(1) = nRef
    rdoQ(2) = cBloque
    tvwMuestra.SelectedItem.Image = apICONBLOQUEAGOTADO
    tvwMuestra.SelectedItem.SelectedImage = apICONBLOQUEAGOTADO
  End If
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then
    texto = tvwMuestra.SelectedItem.Text
    texto = Left(texto, InStr(texto, " (")) & "(No disponible)" & Right(texto, Len(texto) - InStr(texto, ")"))
    tvwMuestra.SelectedItem.Text = texto
  End If
End Sub
Sub Disponible(nRef As String, cMuestra As Integer, cBloque As Integer)
Dim sql As String
Dim rdoQ As rdoQuery
Dim texto As String, Estado As String

  If cBloque = -1 Then
    sql = "UPDATE AP2500 SET AP46_CodEstMues = ? WHERE " _
        & "AP21_CodRef = ? AND " _
        & "PR52NumIDMuestra = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = apMUESTRARECEPCIONADA
    rdoQ(1) = nRef
    rdoQ(2) = cMuestra
    Estado = "(Recepcionada)"
    tvwMuestra.SelectedItem.Image = apICONMUESTRA
    tvwMuestra.SelectedItem.SelectedImage = apICONMUESTRA
  Else
    sql = "UPDATE AP3400 SET AP47_CodEstBloq = ? WHERE " _
        & "AP21_CodRef = ? AND " _
        & "AP34_CodBloq = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeVARCHAR
    rdoQ(2).Type = rdTypeINTEGER
    rdoQ(0) = apBLOQUECORTADO
    rdoQ(1) = nRef
    rdoQ(2) = cBloque
    Estado = "(Cortado)"
    tvwMuestra.SelectedItem.Image = apICONBLOQUE
    tvwMuestra.SelectedItem.SelectedImage = apICONBLOQUE
  End If
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then
    texto = tvwMuestra.SelectedItem.Text
    texto = Left(texto, InStr(texto, " (")) & Estado & Right(texto, Len(texto) - InStr(texto, ")"))
    tvwMuestra.SelectedItem.Text = texto
  End If
End Sub

Private Sub mnuVerOpcion_Click(Index As Integer)
  Select Case Index
    Case 10 'Paciente
'      Call objPipe.PipeSet("NH", Left(txtPaciente(0).Text, InStr(txtPaciente(0).Text, "/") - 1))
'      frmA_Paciente.Show vbModal
    Case 20 'Informe
      If VerInforme(nRef) = 1 Then Call cmdSeleccionar_Click
    Case 30 'Diagn�sticos
      If ComprobarDatos() = True Then
        Call objPipe.PipeSet("Boton", "Pr")
        Call frmPrAnteriores.Show(vbModal)
      End If
    Case 40 'Datos
'      Call objPipe.PipeSet("NH", NH)
'      Call objPipe.PipeSet("NC", NC)
      frmDatosPr.Show vbModal
      Set frmDatosPr = Nothing
    Case 50 'Fotos
      On Error GoTo seguir
      Call objPipe.PipeSet("Boton", "Fotos")
      frmPrAnteriores.Show vbModal
      Exit Sub
seguir:
      MsgBox "Debe seleccionar una muestra o un porta", vbInformation, "Selecci�n incorrecta"
  End Select
End Sub
Function TieneBloquesInclusion(nMues As Integer) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset

  sql = "SELECT AP34_CodBloq FROM AP3400 WHERE " _
      & "AP21_CodRef = ? AND " _
      & "PR52NumIDMuestra = ? AND " _
      & "AP05_CodTiBloq = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = nMues
  rdoQ(2) = apBLOQUEINCLUSION
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    TieneBloquesInclusion = False
  Else
    TieneBloquesInclusion = True
  End If
  rdo.Close
End Function

Private Sub picSep3_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Mover3 = True
End Sub

Private Sub picSep3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Mover3 = True Then
    If picSep3.Left - tvwMuestra.Left + X > 100 And tvwVarios.Width - X > 100 Then 'And 2265 + fraPr.Width - 6945 > 100 Then
      picSep3.Left = picSep3.Left + X
      tvwMuestra.Width = picSep3.Left - tvwMuestra.Left
      fraPr.Width = picSep3.Left - fraPr.Left
      tvwVarios.Left = picSep3.Left + picSep3.Width
      tvwVarios.Width = tvwVarios.Width - X
    End If
  End If
End Sub

Private Sub picSep3_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Mover3 = False
End Sub

Private Sub tvwMuestra_Click()
  tvwMuestra.DropHighlight = Nothing
End Sub
Sub PedirBloques(clave As String)
Dim texto As String, sql As String, texto1 As String
Dim insertar As Integer, Disponible As Integer, HT As Integer, NumBloqInclusion As Integer
Dim i As Integer, nodo As Node
Dim rdo As rdoResultset, rdoQ As rdoQuery
  
  If InStr(tvwMuestra.Nodes(clave).Text, apMUESTRANODISPONIBLE_TXT) > 0 Then
    texto = tvwMuestra.Nodes(clave).Text
    MsgBox "La muestra '" & Left(texto, InStr(texto, "(" & apMUESTRANODISPONIBLE_TXT & ")") - 2) & "' est� agotada.", vbInformation, "Petici�n de t�cnicas"
  Else
    If Left$(clave, 1) = "M" Then
      If tvwMuestra.DropHighlight.Tag = -1 Then
        If NBloq = apBLOQUEINTRA Then ' si se ha pedido un bloque de intra
          If TieneBloquesInclusion(Right(clave, Len(clave) - 1)) = True Then
            MsgBox "Ya se han pedido bloques de inclusi�n para esta muestra, por lo que no se pueden pedir bloques de intra.", vbInformation, "Petici�n de t�cnicas"
            Exit Sub
          End If
        End If
      Else
        If NBloq = apBLOQUEINTRA Then
          MsgBox "No se puede pedir un bloque de intra para una muestra que no es intra", vbInformation, "Petici�n de t�cnicas"
          Exit Sub
        End If
      End If
      Load frmBloques
      With frmBloques
        .txtBloque.Text = tvwVarios.SelectedItem.Text
        .txtNumBloques.Text = 1
        .chkIncluido.Value = Unchecked
        .chkHT.Value = Unchecked
        .Show vbModal
        insertar = 0
        Disponible = False
        If .seguir = True Then
          insertar = .txtNumBloques.Text
          Disponible = .chkIncluido.Value
          HT = .chkHT.Value
        End If
      End With
      Unload frmBloques
      num = Right(clave, Len(clave) - 1)
      If insertar > 0 And Disponible = 1 Then
        sql = "UPDATE AP2500 SET AP46_CodEstMues = " & apMUESTRANODISPONIBLE & " WHERE " _
            & "AP21_CodRef = ? AND PR52NumIDMuestra = ? "
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = nRef
        rdoQ(1) = num
        rdoQ.Execute sql
        
        texto = tvwMuestra.DropHighlight.Text
        texto = Left(texto, InStr(texto, " (")) & " (" & apMUESTRANODISPONIBLE_TXT & ")"
        tvwMuestra.DropHighlight.Text = texto
      End If
      If NBloq = apBLOQUEINCLUSION Then
        NumBloqInclusion = SiguienteBloqueInclusion(nRef)
        If HT = 1 Then
          sql = "INSERT INTO AP3400 (AP21_CodRef, PR52NumIDMuestra, AP34_CodBloq, " _
              & "AP34_FecSotud, AP05_CodTiBloq, AP47_CodEstBloq, AP19_CodFija, " _
              & "AP22_CodDecal, AP34_NumBloq) " _
              & "VALUES (?, ?, ?, SYSDATE, ?, ?, ?, ?, ?)"
        Else
          sql = "INSERT INTO AP3400 (AP21_CodRef, PR52NumIDMuestra, AP34_CodBloq, " _
              & "AP34_FecSotud, AP34_FecTalla, AP05_CodTiBloq, AP47_CodEstBloq, AP19_CodFija, " _
              & "AP22_CodDecal, AP34_NumBloq) " _
              & "VALUES (?, ?, ?, SYSDATE, SYSDATE, ?, ?, ?, ?, ?)"
        End If
      Else
        If HT = 1 Then
          sql = "INSERT INTO AP3400 (AP21_CodRef, PR52NumIDMuestra, AP34_CodBloq, " _
              & "AP34_FecSotud, AP05_CodTiBloq, AP47_CodEstBloq, AP19_CodFija, " _
              & "AP22_CodDecal) " _
              & "VALUES (?, ?, ?, SYSDATE, ?, ?, ?, ?)"
        Else
          sql = "INSERT INTO AP3400 (AP21_CodRef, PR52NumIDMuestra, AP34_CodBloq, " _
              & "AP34_FecSotud, AP34_FecTalla, AP05_CodTiBloq, AP47_CodEstBloq, AP19_CodFija, " _
              & "AP22_CodDecal) " _
              & "VALUES (?, ?, ?, SYSDATE, SYSDATE, ?, ?, ?, ?)"
        End If
      End If
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nRef
      rdoQ(1) = num
      rdoQ(3) = NBloq
      rdoQ(5) = DatosMues(num).cFija
      rdoQ(6) = DatosMues(num).cDecal
      If HT = 1 Then
        rdoQ(4) = apBLOQUESOLICITADO
        texto = apBLOQUESOLICITADO_TXT
      Else
        If NBloq = apBLOQUEINTRA Then
          rdoQ(4) = apBLOQUEREALIZADO
          texto = apBLOQUEREALIZADO_TXT
        Else
          rdoQ(4) = apBLOQUETALLADO
          texto = apBLOQUETALLADO_TXT
        End If
      End If
      For i = 1 To insertar ' Se insertan los bloques en la muestra
        NumBloques = NumBloques + 1
        rdoQ(2) = NumBloques
        If NBloq = apBLOQUEINCLUSION Then
          rdoQ(7) = NumBloqInclusion
          texto1 = NumBloqInclusion & ".- " & tvwVarios.SelectedItem.Text & " (" & texto & ")"
          NumBloqInclusion = NumBloqInclusion + 1
        Else
          texto1 = tvwVarios.SelectedItem.Text & " (" & texto & ")"
        End If
        rdoQ.Execute sql
        
        ReDim Preserve Bloq(1 To NumBloques)
        Bloq(NumBloques).FSotud = Hoy()
        If HT = 1 Then Bloq(NumBloques).FTalla = "" Else Bloq(NumBloques).FTalla = Hoy()
        
        Set nodo = tvwMuestra.Nodes.Add(clave, tvwChild, "B" & NumBloques, texto1, apICONBLOQUE, apICONBLOQUE)
        nodo.Tag = NBloq
        tvwMuestra.Nodes(nodo.Key).EnsureVisible
        If NBloq = apBLOQUEINCLUSION Then  ' C�digo del bloque de inclusi�n
          Call InsertarHematoxilina(nRef, NumBloques, nodo)
        End If
      Next i
      If HT = 0 And NBloq <> apBLOQUEINTRA Then
        If ActualizarEstPr_Macro(nRef) = 1 Then txtPr(2).Text = "Disponible Macro"
      End If
    End If
  End If

End Sub
Private Sub tvwMuestra_DragDrop(Source As Control, X As Single, Y As Single)
Dim clave As String, num As Integer, sql As String, texto As String, texto1 As String
Dim nodo As Node, numportas As Integer, Bloque As Integer, NumBloqInclusion As Integer
Dim i As Integer
Dim rdoQ As rdoQuery
Dim res As Integer, TiBloq As Integer
  
'  On Error Resume Next
  clave = tvwMuestra.DropHighlight.Key
  Select Case origen
    Case apORIGENBLOQUE  'Se insertan bloques en las muestras
      Call PedirBloques(clave)
    Case apORIGENFIJADOR ' Se insertan los fijadores
      If Left$(clave, 1) = "B" Then
        Call CambiarFijadorBloque(clave)
      ElseIf Left$(clave, 1) = "M" Then
        Call CambiarFijadorMuestra(clave)
      End If
    Case apORIGENDECAL ' Se insertan los decalcificadores
      If Left$(clave, 1) = "B" Then
        Call CambiarDecalBloque(clave)
      ElseIf Left$(clave, 1) = "M" Then
        Call CambiarDecalMuestra(clave)
      End If
    Case apORIGENTECNICA ' Se insertan las t�cnicas
      Select Case Left$(clave, 1)
        Case "B"  ' Se ha arrastrado sobre un bloque
          Call NuevaTecnica(clave)
        Case "M"  ' Se ha arrastrado sobre la muestra
          If TiPr = 1 Then 'Biopsias
            If TiTec = apTECCITOLOGIAS Then 'Tipo de t�cnica de citolog�as (impronta)
              Call PedirImpronta(nRef, Right(clave, Len(clave) - 1), clave)
            Else
              For i = 1 To tvwMuestra.Nodes(clave).Children
                If i = 1 Then Set nodo = tvwMuestra.Nodes(clave).Child Else Set nodo = nodo.Next
                If Left(nodo.Key, 1) = "B" Then Call NuevaTecnica(nodo.Key)
              Next i
            End If
          ElseIf TiPr = 2 Then
            If TecBloque(TiTec, 1) = False Then
              MsgBox "La t�cnica '" & tvwVarios.SelectedItem.Text & "' no se puede pedir a esta muestra.", vbExclamation, "Introducci�n incorrecta"
            Else
              Call TecnicaMuestra(clave)
            End If
          End If
        Case "T"
          If valido = True Then
            If TiPr = 1 Then
              TiBloq = tvwMuestra.Nodes(clave).Parent.Tag
            Else
              TiBloq = 1
            End If
            If TecBloque(TiTec, TiBloq) = False Then
              MsgBox "La t�cnica '" & tvwVarios.SelectedItem.Text & "' no se puede pedir al tipo de bloque " & tvwMuestra.Nodes(clave).Parent.Text & ".", vbExclamation, "Introducci�n incorrecta"
            Else
              Call ModificarTecnica(clave)
            End If
          End If
      End Select
    Case apORIGENPERFIL ' Se insertan los perfiles
      If Left$(clave, 1) = "B" Then
        Call NuevoPerfil(clave)
      ElseIf Left$(clave, 1) = "M" Then
        If TiPr = 1 Then
          For i = 1 To tvwMuestra.Nodes(clave).Children
            If i = 1 Then
              Set nodo = tvwMuestra.Nodes(clave).Child
            Else
              Set nodo = nodo.Next
            End If
            Call NuevoPerfil(nodo.Key)
          Next i
        ElseIf TiPr = 2 Then
          Call NuevoPerfilCito(clave)
        End If
      End If
      Erase Tec
  End Select
  nTec = 0
  Set tvwMuestra.DropHighlight = Nothing
  Call CerrarrdoQueries
End Sub
Sub PedirImpronta(nRef As String, nMues As Integer, clave As String)
Dim NBloq As Integer, nRefCito As String
Dim cPat As String, cRes As String


  ' Se inserta la impronta como prueba
  nRefCito = ExisteImpronta(nRef)
  If nRefCito = "" Then
    frmImpronta.Show vbModal
    If frmImpronta.seguir = True Then
      cPat = frmImpronta.cPat
      cRes = frmImpronta.cRes
      Unload frmImpronta
      nRefCito = InsertarPrCito(nRef, cPat, cRes)
      If nRefCito = "" Then GoTo Error
      If InsertarMuestraCito(nRef, nRefCito, nMues) = 0 Then GoTo Error
      Call ImprimirHojaRecepcion(nRefCito, 1)
    Else
      Unload frmImpronta
      Exit Sub
    End If
  Else
    If ExisteMuestraImpronta(nRefCito, nMues) = False Then
      If InsertarMuestraCito(nRef, nRefCito, nMues) = 0 Then GoTo Error
    End If
  End If
  NBloq = CrearBloqueCito(nRefCito, nMues, True)
  If NBloq = -1 Then GoTo Error
  If InsertarImpronta(nRefCito, NBloq, False) = False Then GoTo Error
    
  ' Se inserta la impronta en biopsias
  NBloq = CrearBloqueCito(nRef, nMues, False)
  If NBloq = -1 Then GoTo Error
  If InsertarImpronta(nRef, NBloq, True, clave) = False Then GoTo Error
  objApp.rdoConnect.CommitTrans
  Load frmNumRef
  frmNumRef.lblTit.Caption = "Se ha efectuado la petici�n de una impronta con el n�mero: "
  frmNumRef.lblnRef.Caption = Val(Right(nRefCito, 5))
  frmNumRef.Show vbModal
  
  Exit Sub

Error:
  objApp.rdoConnect.RollbackTrans
  MsgBox "Error al insertar la impronta", vbError, "Impronta"
  Exit Sub
End Sub
Function InsertarMuestraCito(nRef As String, nRefCito As String, nMues As Integer) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
  
  sql = "INSERT INTO AP2500 (AP21_CodRef, PR52NumIDMuestra, " _
      & "AP25_FECRECEP, SG02Cod, AP18_CODMOTIVO, " _
      & "AP19_CODFIJA, AP22_CODDECAL, PR52NumMuestra, AP25_FECALM, " _
      & "AP25_FECRETIR, AP25_OBSERV, AP46_CODESTMUES) " _
      & "(SELECT '" & nRefCito & "', '" & nMues & "', " _
      & "AP25_FECRECEP, SG02Cod, AP18_CODMOTIVO, " _
      & "AP19_CODFIJA, AP22_CODDECAL, PR52NumMuestra, AP25_FECALM, " _
      & "AP25_FECRETIR, AP25_OBSERV, AP46_CODESTMUES FROM AP2500 WHERE " _
      & "AP21_CodRef = ? AND PR52NumIDMuestra = ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = nMues
  rdoQ.Execute sql
  InsertarMuestraCito = rdoQ.RowsAffected

End Function
Function InsertarPrCito(nRefBio As String, cPat As String, cRes As String) As String
Dim sql As String, nRef As String, texto As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim NBloq As Integer
  
  nRef = Year(Now) & SiguienteNum(apCODCITO)
  texto = "Impronta proviniente de la biopsia " & nRefBio
  sql = "INSERT INTO AP2100 (AP21_CodRef, PR01CodActuacion, AP21_TIPAC, " _
        & "PR04NumActPlan, AP11_CODCTRO, AD02CodDpto, SG02Cod_Sote, " _
        & "AP21_FECSOTUD, AP21_FecGen, SG02Cod_PAT, SG02Cod_RES, SG02Cod_CITO, " _
        & "AP21_Observ, AP45_CODESTPR, CI22NumHistoria) (" _
        & "SELECT '" & nRef & "', " & apCODPRIMPRONTA & ", AP21_TIPAC, " _
        & "PR04NumActPlan, AP11_CODCTRO, AD02CodDpto, SG02Cod_Sote, " _
        & "AP21_FECSOTUD, SYSDATE, '" & cPat & "', '" & cRes & "', SG02Cod_CITO, '" _
        & texto & "', " & apPRUEBAMACRO & ", CI22NumHistoria FROM AP2100 WHERE " _
        & "AP21_CodRef = ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRefBio
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then InsertarPrCito = "" Else InsertarPrCito = nRef
  rdoQ.Close
End Function
Function ExisteImpronta(nRefBio As String) As String
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
  
  sql = "SELECT AP21_CodRef FROM AP2100 WHERE PR04NumActPlan IN " _
      & "(SELECT PR04NumActPlan FROM AP2100 WHERE AP21_CodRef = ?) " _
      & " AND PR01CodActuacion = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRefBio
  rdoQ(1) = apCODPRIMPRONTA
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then ExisteImpronta = rdo(0) Else ExisteImpronta = ""

End Function
Function ExisteMuestraImpronta(nRefCito As String, nMues As Integer) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
  
  sql = "SELECT AP21_CodRef, PR52NumIDMuestra FROM AP2500 WHERE " _
      & "AP21_CodRef = ? AND " _
      & "PR52NumIDMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRefCito
  rdoQ(1) = nMues
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then ExisteMuestraImpronta = False Else ExisteMuestraImpronta = True

End Function
Function SiguienteBloqueInclusion(nRef As String)
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset

  sql = "SELECT MAX(AP34_NumBloq) FROM AP3400 WHERE AP21_CODREF = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If IsNull(rdo(0)) Then
    SiguienteBloqueInclusion = 1
  Else
    SiguienteBloqueInclusion = rdo(0) + 1
  End If
  rdo.Close
End Function
Function InsertarImpronta(nRef As String, NBloq As Integer, Insert As Integer, Optional clave As String) As Integer
Dim sql As String, texto As String
Dim rdoQ As rdoQuery
Dim nodo As Node

  numportas = SigPorta(nRef, NBloq)
  sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
      & "AP36_FecSotud, AP48_CodEstPorta) VALUES (?, ?, ?, ?, SYSDATE, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = NBloq
  rdoQ(2) = numportas
  rdoQ(3) = nTec
  If Insert = True Then
    rdoQ(4) = apPORTAREALIZADO
    texto = apPORTAREALIZADO_TXT
  Else
    rdoQ(4) = apPORTACORTADO
    texto = apPORTACORTADO_TXT
  End If
  rdoQ.Execute sql
  If rdoQ.RowsAffected = 0 Then
    InsertarImpronta = False
  Else
    InsertarImpronta = True
    If Insert = True Then
      Set nodo = tvwMuestra.Nodes.Add(clave, tvwChild, "T" & numportas & "B" & NBloq, numportas & ".- " & tvwVarios.SelectedItem.Text & " " & rdoQ(4) & " (" & texto & ")", apICONTECNICA, apICONTECNICA)
      nodo.Tag = nTec
      tvwMuestra.Nodes(nodo.Key).EnsureVisible
    End If
  End If
  ReDim Preserve Bloq(NBloq).port(1 To numportas)
  Bloq(NBloq).port(numportas).FSotud = rdoQ(4)

End Function
Function cBloqueCito(nRef As String, nMues As Integer, cBloq As Integer, impronta As Integer) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset

  sql = "SELECT AP34_CodBloq FROM AP3400 WHERE " _
      & "AP21_CodRef = ? AND PR52NumIDMuestra = ? AND AP05_CODTIBLOQ = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = nMues
  rdoQ(2) = apBLOQUECITOLOGIAS
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    If impronta = True Then
      sql = "SELECT MAX(AP34_CodBloq) FROM AP3400 WHERE " _
          & "AP21_CodRef = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(0) = nRef
      Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
      If IsNull(rdo(0)) Then cBloq = 1 Else cBloq = rdo(0) + 1
    Else
      NumBloques = NumBloques + 1
      cBloq = NumBloques
      cBloqueCito = False
    End If
  Else
    cBloq = rdo(0)
    cBloqueCito = True
  End If
  rdo.Close
End Function
Function CrearBloqueCito(nRef As String, nMues As Integer, impronta As Integer) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim cBloq As Integer

  If cBloqueCito(nRef, nMues, cBloq, impronta) = False Then
    sql = "INSERT INTO AP3400 (AP21_CodRef, PR52NumIDMuestra, AP34_CodBloq, " _
        & "AP34_FecSotud, AP34_FecTalla, AP05_CodTiBloq, AP47_CodEstBloq, AP19_CodFija, " _
        & "AP22_CodDecal) " _
        & "VALUES (?, ?, ?, SYSDATE, SYSDATE, ?, ?, ?, ?)" '  & apSOLICITADO & ")"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = nRef
    rdoQ(1) = nMues
    rdoQ(2) = cBloq
    rdoQ(3) = apBLOQUECITOLOGIAS
    rdoQ(4) = apBLOQUECORTADO
    rdoQ(5) = DatosMues(nMues).cFija
    rdoQ(6) = DatosMues(nMues).cDecal
    rdoQ.Execute sql
    If rdoQ.RowsAffected = 0 Then
      CrearBloqueCito = -1
      Exit Function
    End If
  End If
  CrearBloqueCito = cBloq
  ReDim Preserve Bloq(1 To cBloq)
  Bloq(cBloq).FSotud = Hoy()
  Bloq(cBloq).FTalla = Hoy()

End Function

Sub ModificarTecnica(clave As String)
Dim sql As String, desig As String
Dim rdoQ As rdoQuery
Dim num As Integer, numporta As Integer
    
  num = Val(Right(clave, Len(clave) - InStr(clave, "B")))
  sql = "UPDATE AP3600 SET " _
      & "AP48_CodEstPorta = ?, " _
      & "AP36_Observ = AP36_Observ||' '||? WHERE " _
      & "AP21_CodRef = ? AND AP34_CodBloq = ? AND AP36_CodPorta = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = apPORTAANULADO
  rdoQ(1) = "Porta anulado debido a que se ha hecho encima una t�cnica de '" & tvwVarios.SelectedItem.Text & "'"
  rdoQ(2) = nRef
  rdoQ(3) = num
  rdoQ(4) = Val(tvwMuestra.Nodes(clave).Text)
  rdoQ.Execute sql
  
  desig = DesigTec(CInt(nTec))
  sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
      & "AP36_FecSotud, AP36_FecCorte, AP36_Observ, AP48_CodEstPorta) VALUES " _
      & "(?, ?, ?, ?, SYSDATE, SYSDATE, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = num
  rdoQ(2) = SigPorta(nRef, num)
  rdoQ(3) = nTec
  rdoQ(4) = "Porta realizado sobre una t�cnica de '" & desig & "'"
  rdoQ(5) = apPORTACORTADO
  rdoQ.Execute sql
  tvwMuestra.Nodes(clave).Text = rdoQ(2) & ".- " & desig & " (" & apPORTACORTADO_TXT & ")"
  tvwMuestra.Nodes(clave).Tag = nTec
End Sub
Private Function DesigTec(cTec As Integer) As String
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset

  sql = "SELECT AP12_Desig FROM AP1200 WHERE AP12_CodTec = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cTec
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  DesigTec = rdo(0)
End Function
Sub InsertarHematoxilina(nRef As String, NBloq As Integer, nodo As Node)
Dim TiBloq As Integer, res As Integer, texto As String
Dim i As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim nodo1 As Node
  
  numportas = SigPorta(nRef, NBloq)
  sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
      & "AP36_FecSotud, AP48_CodEstPorta) VALUES " _
      & "(?, ?, ?, ?, SYSDATE, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = NBloq
  rdoQ(2) = numportas
  rdoQ(3) = apHEMATOXILINA
  
  rdoQ(4) = CodEstado(apBLOQUEINCLUSION, texto, nRef)
  
  rdoQ.Execute sql
  Set nodo1 = tvwMuestra.Nodes.Add(nodo.Key, tvwChild, "T" & numportas & "B" & NBloq, numportas & ".- Hematoxilina-Floxina " & " (" & texto & ")", apICONTECNICA, apICONTECNICA)
  tvwMuestra.Nodes(nodo.Key).EnsureVisible
  ReDim Preserve Bloq(NBloq).port(1 To numportas)
  Bloq(NBloq).port(numportas).FSotud = rdoQ(4)
End Sub
Function CodEstado(cBloq As Integer, texto As String, nRef As String) As Integer
Dim res As Integer, Estado As Integer
  
  If cBloq = apBLOQUEINTRA Then
    Estado = apPORTAREALIZADO
    texto = apPORTAREALIZADO_TXT
  Else
    Estado = apPORTASOLICITADO
    texto = apPORTASOLICITADO_TXT
    res = ActualizarEstPr_Cont(nRef)
    If res = 1 Then txtPr(2).Text = "Continuada"
  End If
  CodEstado = Estado
End Function
Sub ActualizarEstadoPruebaPte(nRef As String)
Dim sql As String, Estado As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP48_CodEstPorta FROM AP3600 WHERE AP21_CodRef = ? AND AP48_CodEstPorta = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(0) = apPORTAREALIZADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeVARCHAR
    rdoQ(0) = apPRUEBACONTINUADA
    rdoQ(1) = nRef
    rdoQ.Execute
  End If
  rdo.Close
  
End Sub
Sub ActualizarEstadoPruebaReali(nRef As String)
Dim sql As String, Estado As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP48_CodEstPorta FROM AP3600 WHERE AP21_CodRef = ? AND AP48_CodEstPorta < ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(0) = apPORTAREALIZADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  
  sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeVARCHAR
  rdoQ(1) = nRef
  
  If rdo.EOF = True Then
    rdoQ(0) = apPRUEBAFINALIZADA
  Else
    rdoQ(0) = apPRUEBACONTINUADA
  End If
  
  rdoQ.Execute
  rdo.Close
  
End Sub
Function BloqueCito(nRef As String, nMues As Integer) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  sql = "SELECT AP34_CodBloq FROM AP3400 WHERE Ap21_CodRef = ? AND PR52NumIDMuestra = ? AND AP05_CodTiBloq = 1"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = nMues
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  BloqueCito = rdo(0)

End Function
Sub TecnicaMuestra(clave As String)
Dim num As Integer
Dim numportas As Integer
Dim sql As String, texto As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim res As Integer, i As Integer, cBloq As Integer, nMues As Integer
Dim nodo As Node

  nMues = Val(Right(clave, Len(clave) - 1))
  cBloq = BloqueCito(nRef, nMues)
  
  num = 0
  numportas = 1
  For i = 1 To tvwMuestra.Nodes(clave).Children
    If i = 1 Then Set nodo = tvwMuestra.Nodes(clave).Child
'    If nodo.Tag = "" Then numportas = numportas + 1
    numportas = numportas + 1
    Set nodo = nodo.Next
  Next i
  sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
      & "AP36_FecSotud, AP48_CodEstPorta) VALUES " _
      & "(?, ?, ?, ?, SYSDATE, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = cBloq
  rdoQ(2) = numportas
  rdoQ(3) = nTec
  rdoQ(4) = CodEstado(tvwMuestra.DropHighlight.Tag, texto, nRef)
  
  rdoQ.Execute sql
  Set nodo = tvwMuestra.Nodes.Add(clave, tvwChild, "T" & numportas & "B" & cBloq, numportas & ".- " & tvwVarios.SelectedItem.Text & " " & rdoQ(4) & " (" & texto & ")", apICONTECNICA, apICONTECNICA)
  nodo.Tag = nTec
  nodo.EnsureVisible
  ReDim Preserve Bloq(NBloq).port(1 To numportas)
  Bloq(NBloq).port(numportas).FSotud = Hoy()
End Sub
Sub NuevoPerfil(clave As String)
Dim texto As String, sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim res As Integer, i As Integer, TiBloq As Integer
Dim nodo As Node
  
  If InStr(tvwMuestra.Nodes(clave).Text, "Anulado") > 0 Then
    texto = tvwMuestra.Nodes(clave).Text
    MsgBox "El bloque '" & Left(texto, InStr(texto, "(Anulado)") - 2) & "' ha sido anulado.", vbInformation, "Petici�n de t�cnicas"
  Else
    num = Right(clave, Len(clave) - 1)
    numportas = SigPorta(nRef, num)
    TiBloq = tvwMuestra.Nodes(clave).Tag
    If nTec > 0 Then
      res = TecBloque(TiTec, TiBloq)
      If res = True Then
        sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
            & "AP36_FecSotud, AP48_CodEstPorta) VALUES " _
            & "(?, ?, ?, ?, SYSDATE, ?)"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = nRef
        rdoQ(1) = num
        rdoQ(2) = numportas
        rdoQ(3) = nTec
        rdoQ(4) = CodEstado(TiBloq, texto, nRef)
        
        rdoQ.Execute sql
        Set nodo = tvwMuestra.Nodes.Add(clave, tvwChild, "T" & numportas & "B" & num, numportas & ".- " & tvwVarios.SelectedItem.Text & " (" & texto & ")", apICONTECNICA, apICONTECNICA)
        nodo.Tag = nTec
        ReDim Preserve Bloq(num).port(1 To numportas)
        Bloq(num).port(numportas).FSotud = rdoQ(4)
        tvwMuestra.Nodes(nodo.Key).EnsureVisible
      Else
        MsgBox "La t�cnica " & tvwVarios.SelectedItem.Text & " no se puede pedir en este tipo de bloque.", vbExclamation, "Introducci�n incorrecta"
      End If
    ElseIf nTec = -1 Then
      sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
          & "AP36_FecSotud, AP48_CodEstPorta) VALUES (?, ?, ?, ?, SYSDATE, ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nRef
      rdoQ(1) = num

      For i = 1 To UBound(Tec, 2)
        If TecBloque(Val(Tec(3, i)), TiBloq) = True Then
          rdoQ(2) = numportas
          rdoQ(3) = Tec(1, i)
          rdoQ(4) = CodEstado(TiBloq, texto, nRef)
          rdoQ.Execute sql
          Set nodo = tvwMuestra.Nodes.Add(clave, tvwChild, "T" & numportas & "B" & num, numportas & ".- " & Tec(2, i) & " (" & texto & ")", apICONTECNICA, apICONTECNICA)
          nodo.Tag = Tec(1, i)
          
          ReDim Preserve Bloq(num).port(1 To numportas)
          Bloq(num).port(numportas).FSotud = Hoy()
          
          tvwMuestra.Nodes(nodo.Key).EnsureVisible
          numportas = numportas + 1
        Else
          MsgBox "La t�cnica " & Tec(2, i) & " no se puede pedir en este tipo de bloque.", vbExclamation, "Introducci�n incorrecta"
        End If
      Next i
    End If
  End If
End Sub

Sub NuevoPerfilCito(clave As String)
Dim texto As String, sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim res As Integer, i As Integer, TiBloq As Integer
Dim nodo As Node
  
  If InStr(tvwMuestra.Nodes(clave).Text, "Anulado") > 0 Then
    texto = tvwMuestra.Nodes(clave).Text
    MsgBox "La muestra '" & Left(texto, InStr(texto, "(Anulado)") - 2) & "' ha sido anulada.", vbInformation, "Petici�n de t�cnicas"
  Else
    num = BloqueCito(nRef, Right(clave, Len(clave) - 1))
    numportas = SigPorta(nRef, num)
    TiBloq = 1  ' Bloque virtual de citolog�as
    If nTec > 0 Then
      res = TecBloque(TiTec, TiBloq)
      If res = True Then
        sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
            & "AP36_FecSotud, AP48_CodEstPorta) VALUES (?, ?, ?, ?, SYSDATE, ?)"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = nRef
        rdoQ(1) = num
        rdoQ(2) = numportas
        rdoQ(3) = nTec
        rdoQ(4) = CodEstado(apBLOQUECITOLOGIAS, texto, nRef)
        
        rdoQ.Execute sql
        Set nodo = tvwMuestra.Nodes.Add(clave, tvwChild, "T" & numportas & "B" & num, numportas & ".- " & tvwVarios.SelectedItem.Text & " (" & texto & ")", apICONTECNICA, apICONTECNICA)
        ReDim Preserve Bloq(num).port(1 To numportas)
        Bloq(num).port(numportas).FSotud = Hoy()
        
        nodo.Tag = nTec
        tvwMuestra.Nodes(nodo.Key).EnsureVisible
      Else
        MsgBox "La t�cnica " & tvwVarios.SelectedItem.Text & " no se puede pedir en este tipo de bloque.", vbExclamation, "Introducci�n incorrecta"
      End If
    ElseIf nTec = -1 Then
      sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
          & "AP36_FecSotud, AP48_CodEstPorta) VALUES " _
          & "(?,?,?,?,SYSDATE, ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nRef
      rdoQ(1) = num

      For i = 1 To UBound(Tec, 2)
        If TecBloque(Val(Tec(3, i)), TiBloq) = True Then
          rdoQ(2) = numportas
          rdoQ(3) = Tec(1, i)
          rdoQ(4) = CodEstado(apBLOQUECITOLOGIAS, texto, nRef)
          rdoQ.Execute sql
          Set nodo = tvwMuestra.Nodes.Add(clave, tvwChild, "T" & numportas & "B" & num, numportas & ".- " & Tec(2, i) & " (" & texto & ")", apICONTECNICA, apICONTECNICA)
          nodo.Tag = Tec(1, i)
          tvwMuestra.Nodes(nodo.Key).EnsureVisible
          ReDim Preserve Bloq(num).port(1 To numportas)
          Bloq(num).port(numportas).FSotud = Hoy()
          
          numportas = numportas + 1
        Else
          MsgBox "La t�cnica '" & Tec(2, i) & "' no se puede pedir en este tipo de bloque.", vbExclamation, "Introducci�n incorrecta"
        End If
      Next i
    End If
  End If
End Sub

Sub NuevaTecnica(clave As String)
Dim TiBloq As Integer, res As Integer, texto As String
Dim i As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim nodo As Node
  
  If InStr(tvwMuestra.Nodes(clave).Text, "Anulado") > 0 Then
    texto = tvwMuestra.Nodes(clave).Text
    MsgBox "El bloque '" & Left(texto, InStr(texto, "(Anulado)") - 2) & "' ha sido anulado.", vbInformation, "Petici�n de t�cnicas"
  Else
    TiBloq = tvwMuestra.Nodes(clave).Tag
    res = TecBloque(TiTec, TiBloq)
    If res = True Then
      num = Right(clave, Len(clave) - 1)
      numportas = SigPorta(nRef, num)
      i = 1
      While Carp(1, i) <> TiTec
        i = i + 1
      Wend
      sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
          & "AP36_FecSotud, AP48_CodEstPorta, AP36_IndIntra) VALUES " _
          & "(?, ?, ?, ?, SYSDATE, ?, ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nRef
      rdoQ(1) = num
      rdoQ(2) = numportas
      rdoQ(3) = nTec
      rdoQ(4) = CodEstado(TiBloq, texto, nRef)
      If TiBloq = apBLOQUEINTRA Then rdoQ(5) = -1 Else rdoQ(5) = 0
      rdoQ.Execute sql
      Set nodo = tvwMuestra.Nodes.Add(clave, tvwChild, "T" & numportas & "B" & num, numportas & ".- " & tvwVarios.SelectedItem.Text & " (" & texto & ")", apICONTECNICA, apICONTECNICA)
      nodo.Tag = nTec
      tvwMuestra.Nodes(nodo.Key).EnsureVisible
    
      ReDim Preserve Bloq(num).port(1 To numportas)
      Bloq(num).port(numportas).FSotud = Hoy()
    
    Else
      MsgBox "La t�cnica '" & tvwVarios.SelectedItem.Text & "' no se puede pedir en este tipo de bloque.", vbExclamation, "Introducci�n incorrecta"
    End If
  End If
End Sub
Function TecBloque(TiTec As Integer, TiBloq As Integer) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  TecBloque = False
  sql = "SELECT AP04_CodTiTec FROM AP5300 WHERE AP05_CodTiBloq = ? AND AP04_CodTiTec = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = TiBloq
  rdoQ(1) = TiTec
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then TecBloque = False Else TecBloque = True
  rdo.Close
End Function
Function RenumerarMuestra(nRef As String, nMuestra As Integer) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim numMues As Integer, res As Integer

  numMues = nMuestra
  res = True
  objApp.rdoConnect.BeginTrans
' Se borra la muestra
  sql = "DELETE FROM AP2500 WHERE AP21_CodRef = ? AND PR52NumIDMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = numMues
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then res = False
' se seleccionan todas las muestras de c�digos posteriores
  sql = "SELECT PR52NumIDMuestra, AP25_FECEXTRA, AP25_INDINTRA, AP25_FECRECEP, AP08_CODPERS, " _
      & "AP16_CODDIAG, AP18_CODMOTIVO, AP19_CODFIJA, AP22_CODDECAL, AP23_CODORG, " _
      & "AP25_TIMUES, AP25_FECALM, AP25_FECRETIR, AP25_OBSERV, AP46_CODESTMUES " _
      & "FROM AP2500 WHERE " _
      & "AP21_CodRef = ? AND " _
      & "PR52NumIDMuestra > ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = nMuestra
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    numMues = rdo(0)

' Se inserta una copia de la muestra, pero con el c�digo anterior
    sql = "INSERT INTO AP2500 (AP21_CodRef, PR52NumIDMuestra, AP25_FECEXTRA, " _
      & "AP25_INDINTRA, AP25_FECRECEP, AP08_CODPERS, AP16_CODDIAG, AP18_CODMOTIVO, " _
      & "AP19_CODFIJA, AP22_CODDECAL, AP23_CODORG, AP25_TIMUES, AP25_FECALM, " _
      & "AP25_FECRETIR, AP25_OBSERV, AP46_CODESTMUES) " _
      & "(SELECT '" & nRef & "', " & numMues - 1 & ", AP25_FECEXTRA, " _
      & "AP25_INDINTRA, AP25_FECRECEP, AP08_CODPERS, AP16_CODDIAG, AP18_CODMOTIVO, " _
      & "AP19_CODFIJA, AP22_CODDECAL, AP23_CODORG, AP25_TIMUES, AP25_FECALM, " _
      & "AP25_FECRETIR, AP25_OBSERV, AP46_CODESTMUES FROM AP2500 WHERE " _
      & "AP21_CodRef = ? AND PR52NumIDMuestra = ?)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(1).Type = rdTypeINTEGER
    rdoQ(0) = nRef
    rdoQ(1) = numMues
    rdoQ.Execute
    If rdoQ.RowsAffected = 0 Then res = False
    
' Se copian los bloques a la muestra anterior
    sql = "UPDATE AP3400 SET PR52NumIDMuestra = ? WHERE " _
        & "AP21_CodRef = ? AND PR52NumIDMuestra = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeVARCHAR
    rdoQ(2).Type = rdTypeINTEGER
    rdoQ(0) = numMues - 1
    rdoQ(1) = nRef
    rdoQ(2) = numMues
    rdoQ.Execute

' se borra la muestra
    sql = "DELETE FROM AP2500 WHERE AP21_CodRef = ? AND PR52NumIDMuestra = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(1).Type = rdTypeINTEGER
    rdoQ(0) = nRef
    rdoQ(1) = numMues
    rdoQ.Execute
    If rdoQ.RowsAffected = 0 Then res = False
    rdo.MoveNext
  Wend
  
  rdo.Close
  If res = False Then
    objApp.rdoConnect.RollbackTrans
    MsgBox "Error al renumerar las muestras", vbError + vbOKOnly, "Borrado de muestras"
    RenumerarMuestra = False
  Else
    objApp.rdoConnect.CommitTrans
    RenumerarMuestra = True
  End If
End Function
Function PuedeBorrarMuestra(nRef As String, nMuestra As Integer) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  sql = "SELECT AP34_CodBloq FROM AP3400 WHERE " _
      & "AP3400.AP21_CodRef = ? AND " _
      & "AP3400.PR52NumIDMuestra = ?"
  
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = nMuestra
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then PuedeBorrarMuestra = True Else PuedeBorrarMuestra = False
  rdo.Close

End Function
Function PuedeBorrarBloque(nRef As String, nMuestra As Integer, nBloque As Integer) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  sql = "SELECT AP3400.AP47_CodEstBloq FROM AP3400 WHERE " _
      & "AP3400.AP21_CodRef = ? AND " _
      & "AP3400.PR52NumIDMuestra = ? AND " _
      & "AP3400.AP34_CodBloq = ? AND " _
      & "AP3400.AP47_CodEstBloq > ?"
  
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = nMuestra
  rdoQ(2) = nBloque
  rdoQ(3) = apBLOQUETALLADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then PuedeBorrarBloque = True Else PuedeBorrarBloque = False
  rdo.Close

End Function
Function PuedeBorrarPorta(nRef As String, nBloque As Integer, nPorta As Integer) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  sql = "SELECT /*+ RULE */ AP3600.AP48_CodEstPorta FROM AP3600 WHERE " _
      & "AP3600.AP21_CodRef = ? AND " _
      & "AP3600.AP34_CodBloq = ? AND " _
      & "AP3600.AP36_CodPorta= ? AND " _
      & "AP3600.AP48_CodEstPorta > ?"
  
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = nBloque
  rdoQ(2) = nPorta
  rdoQ(3) = apPORTACORTADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then PuedeBorrarPorta = True Else PuedeBorrarPorta = False
  rdo.Close

End Function

Private Sub tvwMuestra_KeyDown(KeyCode As Integer, Shift As Integer)
Dim texto As String, sql As String, puede As Integer
Dim nodo As Node, padre As Node
Dim nMuestra As Integer, nBloque As Integer, nPorta As Integer, res As Integer
Dim rdoQ As rdoQuery
  
  If KeyCode = 46 Then    ' Tecla Suprimir
    Set nodo = tvwMuestra.SelectedItem
    Select Case Left(nodo.Key, 1)
      Case "M"
        res = MsgBox("�Est� seguro de que desea borrar la muestra '" & nodo.Text & "'?. Se renumerar�n las muestras posteriores, por lo que deber� sacar nuevas etiquetas y corregir las hojas de trabajo si las ha sacado.", vbQuestion + vbYesNo, "Gesti�n de pruebas")
        If res = vbYes Then
          Set padre = nodo.Parent
          If padre.Children = 1 Then
            Screen.MousePointer = 0
            MsgBox "No puede borrar la muestra, ya que es la �nica pedida para esta prueba. Si quiere modificarla entre en la pantalla Ver/Datos.", vbExclamation, "Borrado de bloques"
          Else
            nMuestra = Right(nodo.Key, Len(nodo.Key) - 1)
            If PuedeBorrarMuestra(nRef, nMuestra) = True Then
              res = RenumerarMuestra(nRef, nMuestra)
              If res = True Then
                tvwMuestra.Nodes.Clear
                Call LlenarMuestras
              End If
            Else
              Screen.MousePointer = 0
              MsgBox "No puede borrar la muestra, ya que ya se han pedido bloques.", vbExclamation, "Borrado de bloques"
            End If
          End If
        End If
      Case "B"
        res = MsgBox("�Desea borrar el bloque '" & nodo.Text & "'?", vbQuestion + vbYesNo, "Gesti�n de pruebas")
        If res = vbYes Then
          Set padre = nodo.Parent
          nMuestra = Right(padre.Key, Len(padre.Key) - 1)
          nBloque = Right(nodo.Key, Len(nodo.Key) - 1)
          If PuedeBorrarBloque(nRef, nMuestra, nBloque) = True And nBloque = NumBloques Then
            sql = "DELETE FROM AP3600 WHERE " _
                & "AP21_CodRef = ? AND AP34_CodBloq = ?"
            Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
            rdoQ(0) = nRef
            rdoQ(1) = nBloque
            rdoQ.Execute sql
            
            sql = "DELETE FROM AP3400 WHERE " _
                & "AP21_CodRef = ? AND PR52NumIDMuestra = ? AND AP34_CodBloq = ?"
            Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
            rdoQ(0) = nRef
            rdoQ(1) = nMuestra
            rdoQ(2) = nBloque
            rdoQ.Execute sql
            
            NumBloques = NumBloques - 1
            tvwMuestra.Nodes.Remove (nodo.Key)
          Else
            Screen.MousePointer = 0
            MsgBox "No puede borrar el bloque, pues est� realizado o no es el �ltimo introducido.", vbExclamation, "Borrado de bloques"
          End If
        End If
      Case "T"
        res = MsgBox("�Desea borrar el porta '" & nodo.Text & "'?", vbQuestion + vbYesNo, "Gesti�n de pruebas")
        If res = vbYes Then
          Set padre = nodo.Parent
          If TiPr = 2 Then
            nBloque = BloqueCito(nRef, Right(padre.Key, Len(padre.Key) - 1))
          Else
            nBloque = Right(padre.Key, Len(padre.Key) - 1)
          End If
          nPorta = Val(Right(nodo.Key, Len(nodo.Key) - 1))
                      
          puede = False
          If nodo.Key = nodo.LastSibling.Key Then
            If SuperUser() = False Then
              puede = PuedeBorrarPorta(nRef, nBloque, nPorta)
            Else
              puede = True
            End If
          End If
          If puede = False Then
            Screen.MousePointer = 0
            MsgBox "No puede borrar el porta, pues ya se ha cortado o el porta no es el �ltimo de ese bloque.", vbExclamation, "Borrado de portas"
          Else
            sql = "DELETE FROM AP3600 WHERE " _
                & "AP21_CodRef = ? AND AP34_CodBloq = ? AND AP36_CodPorta = ?"
            Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
            rdoQ(0) = nRef
            rdoQ(1) = nBloque
            rdoQ(2) = nPorta
            rdoQ.Execute sql
            tvwMuestra.Nodes.Remove (nodo.Key)
          End If
        End If
    End Select
  End If
End Sub

Private Sub tvwMuestra_LostFocus()
  Set tvwMuestra.DropHighlight = tvwMuestra.SelectedItem
End Sub
Private Sub tvwMuestra_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Tipo As String, cMues As Long, cBloq As Long, cTec As Long, clave As String
  
  If Button = vbRightButton Then
    clave = tvwMuestra.SelectedItem.Key
    Tipo = Left(clave, 1)
    Select Case Tipo
      Case "R"
        lblObserv.Caption = Observaciones(Tipo, nRef)
        If lblObserv.Caption <> "" Then
          txtObserv.Text = lblObserv.Caption
          txtObserv.Width = lblObserv.Width
          txtObserv.Left = X + tvwMuestra.Left
          txtObserv.Top = Y + tvwMuestra.Top + 400
          txtObserv.Visible = True
        End If
      Case "M"
        cMues = Val(Right(clave, Len(clave) - 1))
        lblObserv.Caption = Observaciones(Tipo, nRef, cMues)
        If lblObserv.Caption <> "" Then
          txtObserv.Text = lblObserv.Caption
          txtObserv.Width = lblObserv.Width
          txtObserv.Left = X + tvwMuestra.Left
          txtObserv.Top = Y + tvwMuestra.Top + 400
          txtObserv.Visible = True
        End If
      Case "B"
        cBloq = Val(Right(clave, Len(clave) - 1))
        lblObserv.Caption = Observaciones(Tipo, nRef, , cBloq)
        If lblObserv.Caption <> "" Then
          txtObserv.Text = lblObserv.Caption
          txtObserv.Width = lblObserv.Width
          txtObserv.Left = X + tvwMuestra.Left
          txtObserv.Top = Y + tvwMuestra.Top + 400
          txtObserv.Visible = True
        End If
      Case "T"
        cBloq = Val(Mid(clave, InStr(clave, "B") + 1))
        cTec = Val(Right(clave, Len(clave) - 1))
        lblObserv.Caption = Observaciones(Tipo, nRef, , cBloq, cTec)
        If lblObserv.Caption <> "" Then
          txtObserv.Text = lblObserv.Caption
          txtObserv.Width = lblObserv.Width
          txtObserv.Left = X + tvwMuestra.Left
          txtObserv.Top = Y + tvwMuestra.Top + 400
          txtObserv.Visible = True
        End If
    End Select
  End If

End Sub

Private Sub tvwMuestra_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If txtObserv.Visible = True Then txtObserv.Visible = False
End Sub

Private Sub tvwMuestra_NodeClick(ByVal Node As ComctlLib.Node)
  Select Case Left(Node.Key, 1)
    Case "R" ' nodo ra�z
      Call LlenarFechasPr
    Case "M" ' Muestras
      Call LlenarFechasMuestra(Right(Node.Key, Len(Node.Key) - 1))
    Case "B" ' Bloques
      Call LlenarFechasBloque(Right(Node.Key, Len(Node.Key) - 1))
    Case "T" ' T�cnicas
      Call LlenarFechasPorta(Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "B")), Mid(Node.Key, 2, InStr(Node.Key, "B") - InStr(Node.Key, "T") - 1))
  End Select
End Sub

Sub LlenarFechasMuestra(nMues As Integer)
  lblFec(0).Caption = "F. Extrac."
  txtFec(0).Text = DatosMues(nMues).FExtrac
  lblFec(1).Caption = "F. Recep."
  txtFec(1).Text = DatosMues(nMues).fRecep
  lblFec(2).Caption = "F. Almacen"
  txtFec(2).Text = DatosMues(nMues).FAlm
  lblFec(3).Caption = "F. Ret. alm."
  txtFec(3).Text = DatosMues(nMues).FRet
End Sub

Sub LlenarFechasBloque(NBloq As Integer)
  lblFec(0).Caption = "F. Sotud."
  txtFec(0).Text = Bloq(NBloq).FSotud
  lblFec(1).Caption = "F. Tallado"
  txtFec(1).Text = Bloq(NBloq).FTalla
  lblFec(2).Caption = "F. Reali."
  txtFec(2).Text = Bloq(NBloq).FReali
  lblFec(3).Caption = "F. Corte"
  txtFec(3).Text = Bloq(NBloq).FCorte
End Sub

Sub LlenarFechasPorta(NBloq As Integer, nTec As Integer)
  lblFec(0).Caption = "F. Sotud."
  txtFec(0).Text = Bloq(NBloq).port(nTec).FSotud
  lblFec(1).Caption = "F. Corte"
  txtFec(1).Text = Bloq(NBloq).port(nTec).FReali
  lblFec(2).Caption = "F. Reali"
  txtFec(2).Text = Bloq(NBloq).port(nTec).FTincion
  lblFec(3).Caption = ""
  txtFec(3).Text = ""
End Sub

Private Sub tvwMuestra_DragOver(Source As Control, X As Single, Y As Single, State As Integer)

  Set tvwMuestra.DropHighlight = tvwMuestra.HitTest(X, Y)
  With frmPrincipal.imgIcos
    Select Case State
      Case 1
        tvwVarios.DragIcon = .ListImages(apICONPROHIBIDO).Picture
      Case 0
        tvwVarios.DragIcon = .ListImages(Source.SelectedItem.Image).Picture
    End Select
  End With
    
On Error Resume Next

  If Err = 0 Then
    valido = fArrastreValido()
    If valido = True Then
      Source.DragIcon = frmPrincipal.imgIcos.ListImages(Source.SelectedItem.Image).Picture
    Else
      Source.DragIcon = frmPrincipal.imgIcos.ListImages(apICONPROHIBIDO).Picture
    End If
  Else
    valido = False
    Source.DragIcon = frmPrincipal.imgIcos.ListImages(apICONPROHIBIDO).Picture
  End If
End Sub
Private Sub tvwVarios_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
  Select Case State
    Case 1
      tvwVarios.DragIcon = frmPrincipal.imgIcos.ListImages(apICONPROHIBIDO).Picture
    Case 0
      tvwVarios.DragIcon = frmPrincipal.imgIcos.ListImages(Source.SelectedItem.Image).Picture
  End Select
End Sub

Private Function fArrastreValido() As Boolean
Dim nodo As Node

  Set nodo = tvwMuestra.DropHighlight
  
  Select Case origen
    Case apORIGENBLOQUE
      If Left$(nodo.Key, 1) = "M" Then fArrastreValido = True Else fArrastreValido = False
    Case apORIGENTECNICA
      If Left$(nodo.Key, 1) = "M" Or Left$(nodo.Key, 1) = "B" Then
        fArrastreValido = True
      ElseIf PuedeCambiar(Right(tvwVarios.SelectedItem.Key, Len(tvwVarios.SelectedItem.Key) - 1), nodo.Tag) = True Then
        fArrastreValido = True
      Else
        fArrastreValido = False
      End If
    Case apORIGENFIJADOR, apORIGENDECAL, apORIGENPERFIL
      If Left$(nodo.Key, 1) = "M" Or Left$(nodo.Key, 1) = "B" Then
        fArrastreValido = True
      Else
        fArrastreValido = False
      End If
  End Select
End Function

Public Function PuedeCambiar(cTecIni As Long, cTecFin As Long) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP12_CodTec_Ini FROM AP6100 WHERE " _
      & "AP12_CodTec_Ini = ? AND " _
      & "AP12_CodTec_Fin = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cTecIni
  rdoQ(1) = cTecFin
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then PuedeCambiar = True Else PuedeCambiar = False
  
End Function

Private Sub tvwVarios_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim clave As String, nodo As Node, i As Integer
  
  On Error Resume Next
  Set tvwVarios.DropHighlight = Nothing
  Set nodo = tvwVarios.HitTest(X, Y)
  tvwVarios.SelectedItem = nodo
  clave = nodo.Key
  nTec = 0
  
  origen = 0
  Select Case Left$(clave, 1)
    Case "B"
      If Len(clave) > 1 Then
        origen = apORIGENBLOQUE
        NBloq = Val(Right(clave, Len(clave) - 1))
        GenBloqAuto = Right(clave, 1)
      End If
    Case "T"
      If Len(clave) > 1 Then
        origen = apORIGENTECNICA
        nTec = Val(Right(clave, Len(clave) - 1))
        TiTec = nodo.Tag
      End If
    Case "F"
      If Len(clave) > 1 Then
        origen = apORIGENFIJADOR
        cFija = Right(clave, Len(clave) - 1)
      End If
    Case "D"
      If Len(clave) > 1 Then
        origen = apORIGENDECAL
        cDecal = Right(clave, Len(clave) - 1)
      End If
    Case "P"
      If Len(clave) > 1 Then
        origen = apORIGENPERFIL
        For i = 1 To nodo.Children
          nTec = -1
          If i = 1 Then
            Set nodo = nodo.Child
          Else
            Set nodo = nodo.Next
          End If
          clave = nodo.Key
          ReDim Preserve Tec(1 To 3, 1 To i)
          Tec(1, i) = Val(Right(clave, Len(clave) - 1))
          Tec(2, i) = nodo.Text
          Tec(3, i) = nodo.Tag
        Next i
      End If
  End Select
  
  If origen <> 0 Then
    tvwVarios.Drag 1 ' Se inicia el arrastre
    Set tvwVarios.DragIcon = frmPrincipal.imgIcos.ListImages(tvwVarios.SelectedItem.Image).Picture
  End If
End Sub
