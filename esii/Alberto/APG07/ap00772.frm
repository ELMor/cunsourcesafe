VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmNecropsia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Introducción de necropsias"
   ClientHeight    =   4140
   ClientLeft      =   3720
   ClientTop       =   4005
   ClientWidth     =   6765
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   6765
   Begin VB.Frame Frame1 
      Caption         =   "Buscar Casos"
      ForeColor       =   &H00800000&
      Height          =   915
      Left            =   225
      TabIndex        =   5
      Top             =   75
      Width           =   3765
      Begin VB.TextBox txtNH 
         Height          =   315
         Left            =   1050
         TabIndex        =   0
         Top             =   300
         Width           =   840
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   390
         Left            =   2250
         TabIndex        =   1
         Top             =   300
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Historia:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   6
         Top             =   375
         Width           =   765
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   390
      Left            =   5325
      TabIndex        =   4
      Top             =   675
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   5325
      TabIndex        =   3
      Top             =   150
      Width           =   1215
   End
   Begin ComctlLib.ListView lvwPr 
      Height          =   2640
      Left            =   225
      TabIndex        =   2
      Top             =   1200
      Width           =   6315
      _ExtentX        =   11139
      _ExtentY        =   4657
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "frmNecropsia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim NC As Long, NH As Long
Sub Buscar()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim NH As Long

  lvwPr.ListItems.Clear
  NC = 0
  NH = Val(txtNH.Text)
  If NH = 0 Then
    MsgBox "Debe introducir un número de historia válido", vbOKOnly + vbInformation, "Introducción incorrecta"
  Else
    Screen.MousePointer = 11
    sql = "SELECT Asist.NC, Asist.cDptResp, Dpt.Desig, Asist.cDrResp, " _
        & "NVL(DR.txtFirma,DR.AP1||' '||DR.AP2||', '||DR.NOM), Asist.FIngreso, Asist.FAlta " _
        & "FROM Asist, Dpt, Dr WHERE " _
        & "Asist.cDptResp = Dpt.cDpt AND " _
        & "Asist.cDrResp = Dr.cDr AND " _
        & "Asist.NH = ? " _
        & "ORDER BY FIngreso DESC"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(0) = NH
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    While rdo.EOF = False
      Set item = lvwPr.ListItems.Add(, "C" & rdo(0), rdo(0))
      item.SubItems(1) = rdo(2)
      item.SubItems(2) = rdo(4)
      If Not IsNull(rdo(5)) Then item.SubItems(3) = rdo(5)
      If Not IsNull(rdo(6)) Then item.SubItems(4) = rdo(6)
      item.Tag = "H" & NH & "D" & rdo(1) & "P" & rdo(3)
      rdo.MoveNext
    Wend
    Screen.MousePointer = 0
  End If
End Sub
Private Sub cmdAceptar_Click()
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim sql As String, texto As String
Dim NS As Long, cDpt As Integer, cDr As String, NH As Long

  If NC = 0 Then
    MsgBox "Debe seleccionar una Historia/Caso.", vbOKOnly + vbInformation, "Introducción incorrecta"
  Else
    texto = lvwPr.ListItems("C" & NC).Tag
    NH = Mid(texto, 2, InStr(texto, "D") - 2)
    cDpt = Mid(texto, InStr(texto, "D") + 1, 3)
    cDr = Mid(texto, InStr(texto, "P") + 1, 4)
    sql = "SELECT MAX(NS) FROM PrAsist WHERE NH = ? AND NC = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = NH
    rdoQ(1) = NC
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If IsNull(rdo(0)) Then NS = 1 Else NS = rdo(0) + 1
    sql = "INSERT INTO PrAsist (NH, NC, NS, PERTII, CDPTPR, cPr, CDPTSOTE, CDRSOTE, REQCONT, " _
        & "REALIURG, CONTURG, ESTPR, ESTII, FSotud, HSOTUD) VALUES " _
        & "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE, SYSDATE)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = NH
    rdoQ(1) = NC
    rdoQ(2) = NS
    rdoQ(3) = 0
    rdoQ(4) = cDptAP
    rdoQ(5) = apCODPRNECROPSIA
    rdoQ(6) = cDpt
    rdoQ(7) = cDr
    rdoQ(8) = 1
    rdoQ(9) = 0
    rdoQ(10) = 0
    rdoQ(11) = 0
    rdoQ(12) = 0
    rdoQ.Execute
    If rdoQ.RowsAffected = 1 Then
      frmP_Recepcion.objPr.strInitialWhere = "NH = " & NH & " AND NC = " & NC & " AND NS = " & NS
      frmP_Recepcion.objWinInfo.DataRefresh
    End If
    Call InsertarHToPrasist(NH, NC, NS, cDpt, cDr)
    Unload Me
  End If
End Sub
Sub InsertarHToPrasist(NH As Long, NC As Long, NS As Long, cDpt As Integer, cDr As String)
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim sql As String
  
  sql = "INSERT INTO HToPrAsist (FTrans, HTrans, NH, NC, NS, Accion, CDPTPR, cPr, CDPTSOTE, " _
      & "CDRSOTE, REQCONT, REALIURG, CONTURG, FSotud, HSOTUD, Estado, EstadoDoc) VALUES " _
      & "(SYSDATE, SYSDATE, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE, SYSDATE, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  rdoQ(1) = NC
  rdoQ(2) = NS
  rdoQ(3) = "A"
  rdoQ(4) = cDptAP
  rdoQ(5) = apCODPRNECROPSIA
  rdoQ(6) = cDpt
  rdoQ(7) = cDr
  rdoQ(8) = 1
  rdoQ(9) = 0
  rdoQ(10) = 0
  rdoQ(11) = -1
  rdoQ(12) = 1
  rdoQ.Execute

End Sub

Private Sub cmdBuscar_Click()
  Call Buscar
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  With lvwPr
    .ColumnHeaders.Add , , "Caso", 300
    .ColumnHeaders.Add , , "Dpt.", 1600
    .ColumnHeaders.Add , , "Doctor", 1400
    .ColumnHeaders.Add , , "F. Ingreso", 600
    .ColumnHeaders.Add , , "F. Alta", 600
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
End Sub

Private Sub lvwPr_ItemClick(ByVal item As ComctlLib.ListItem)
Dim texto As String
  
  texto = item.Tag
  NC = item.Text
  NH = Mid(texto, 2, InStr(texto, "D"))
  
End Sub

Private Sub txtNH_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then Call Buscar
End Sub
