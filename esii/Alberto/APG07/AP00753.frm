VERSION 5.00
Begin VB.Form frmBloques 
   Caption         =   "Generaci�n de bloques"
   ClientHeight    =   2040
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5355
   LinkTopic       =   "Form1"
   ScaleHeight     =   2040
   ScaleWidth      =   5355
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkHT 
      Alignment       =   1  'Right Justify
      Caption         =   "Incluir en hoja de trabajo de tallado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   7
      Top             =   1575
      Width           =   3390
   End
   Begin VB.CheckBox chkIncluido 
      Alignment       =   1  'Right Justify
      Caption         =   "Muestra incluida en su totalidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   450
      TabIndex        =   3
      Top             =   1200
      Width           =   3090
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   390
      Left            =   3900
      TabIndex        =   2
      Top             =   1050
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   3900
      TabIndex        =   1
      Top             =   225
      Width           =   1215
   End
   Begin VB.TextBox txtNumBloques 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   3150
      TabIndex        =   0
      Top             =   750
      Width           =   375
   End
   Begin VB.TextBox txtBloque 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   1200
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   300
      Width           =   2325
   End
   Begin VB.Label lblLabel1 
      Caption         =   "N� de bloques a generar:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   825
      TabIndex        =   6
      Top             =   750
      Width           =   2190
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Bloque:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   20
      Left            =   375
      TabIndex        =   5
      Top             =   300
      Width           =   690
   End
End
Attribute VB_Name = "frmBloques"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public seguir As Integer

Private Sub cmdAceptar_Click()
  seguir = True
  Me.Hide
End Sub

Private Sub cmdCancelar_Click()
  seguir = False
  Me.Hide
End Sub

Private Sub Form_Activate()
  txtNumBloques.SelStart = 0
  txtNumBloques.SelLength = Len(txtNumBloques.Text)
  
End Sub

Private Sub txtNumBloques_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdAceptar_Click
    Case 27
      cmdCancelar_Click
  End Select
End Sub
