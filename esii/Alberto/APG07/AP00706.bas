Attribute VB_Name = "modMecano"
Option Explicit

Function AbreDocumento(NH As Long, NC As Long, NS As Long, documento As String, fecha As String) As Integer
Dim res As Integer
Dim directorio As String, nombredoc As String
Dim msg As String
    
  ' Con Historia, Caso, Secuencia se crea el nombre del fichero, y con la FSolicitud y
  ' Departamento el path.
  On Error Resume Next
  ' Primero se comprueba que el word está abierto, y si no se abre.
  pArrancarWord
  Err = 0
  word.System.Cursor = 3
  word.Options.ReplaceSelection = True
  Err = 0
  If word.Windows.Count > 0 Then
    word.Documents.Close SaveChanges:=0
  End If
  ' Se genera el path del fichero.
  directorio = DocsPath & "\" & cDptAP & "\pruebas\"
  directorio = directorio & Year(fecha) & "\" & Month(fecha) & "\"
  nombredoc = NombreFichpruebas(CStr(NH), CStr(NC), CStr(NS))
  documento = directorio & nombredoc

  Err = 0
  ' Se comprueba si existe el documento
  res = GetAttr(documento)
  AbreDocumento = 1
  If Err <> 0 Then
    Err = 0
    msg = "El documento de la prueba no existe. "
    res = MsgBox(msg, vbExclamation + vbOKOnly, "Error Apertura Documento")
'    If res = vbYes Then
'      res = CrearPrueba(NH, NC, NS, documento)
'      Err = 0
'      FileCopy "g:\sihc\data\docs\" & cDptAP & "\pruebas\" & Year(fecha) & "\" & Month(fecha) & "\" & NombreFichpruebas(CStr(NH), CStr(NC), CStr(NS)), documento
'      If Err <> 0 Then
'        MkDir DocsPath & "\" & cDptAP & "\pruebas\" & Year(fecha)
'        MkDir DocsPath & "\" & cDptAP & "\pruebas\" & Year(fecha) & "\" & Month(fecha)
'        FileCopy "g:\sihc\data\docs\" & cDptAP & "\pruebas\" & Year(fecha) & "\" & Month(fecha) & "\" & NombreFichpruebas(CStr(NH), CStr(NC), CStr(NS)), documento
'      End If
'      AbreDocumento = 1
'    Else
      AbreDocumento = 0
'    End If
  End If
  If AbreDocumento = 1 Then
    word.System.Cursor = 3
    word.Application.ScreenUpdating = True
    Err = 0
    word.Documents.Open documento, 0, 0, 0                  ' Word.ArchivoAbrir
    ' Se comprueba si está en uso
    If Err = 440 Then
      msg = "El fichero está en uso. Espere hasta que se cierre en el otro sistema."
      res = MsgBox(msg, vbExclamation, "Error Apertura Documento")
      word.ArchivoCerrartodo 2
      AbreDocumento = 0
    ElseIf Err <> 0 Then
      msg = "No se puede abrir el fichero. Inténtelo de nuevo o consulte con el supervisor del sistema."
      res = MsgBox(msg, vbExclamation, "Error Apertura Documento")
      AbreDocumento = 0
    Else
      With word
        .activewindow.View.Type = 3
        .ActiveDocument.Unprotect Password:="sihc"
        .ActiveDocument.ShowRevisions = False
        .ActiveDocument.PrintRevisions = False
        .ActiveDocument.Protect Password:="sihc", NoReset:=False, Type:=0
        .activewindow.ActivePane.View.Zoom.Percentage = 120
        .activewindow.View.ShowAll = False
        .Selection.HomeKey Unit:=6
        .Selection.MoveDown Unit:=5, Count:=9
        .Application.ScreenUpdating = True
      End With
      AbreDocumento = 1
    End If
  End If
  word.System.Cursor = 0
End Function
Sub ActualizarDatos(nRef As String)
Dim DrContesta As Integer, DrReferencia As Integer, i As Integer, Planta As Integer
Dim InsertarFecha As String
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim cama As String
Dim firma As String, ref As String
Dim firmaAyu As String, refAyu As String

  On Error Resume Next
    ' Introduce los datos en la cabecera de los documentos de pruebas.
  With word
    .Application.ScreenUpdating = False
    .System.Cursor = 3
    .Options.ReplaceSelection = True
    With .ActiveDocument
      .Unprotect Password:="sihc"
      .TrackRevisions = False
      .PrintRevisions = False
      .ShowRevisions = False
    End With

    ' Se obtienen los datos del personal
    sql = "SELECT Pat.AP08_Firma, Pat.AP08_ref, Res.AP08_Firma, Res.AP08_ref, Camas.Cama " _
        & "FROM AP0800 Pat, AP0800 Res, AP2100, Camas WHERE " _
        & "AP2100.AP08_CodPers_Pat = Pat.AP08_CodPers AND " _
        & "AP2100.AP08_CodPers_Res = Res.AP08_CodPers (+) AND " _
        & "AP2100.AP21_CodHist = Camas.NH (+) AND " _
        & "AP2100.AP21_CodCaso = Camas.NC (+) AND " _
        & "AP2100.AP21_CodRef = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(0) = nRef
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    firma = rdo(0)
    ref = rdo(1)
    If IsNull(rdo(2)) Then firmaAyu = "" Else firmaAyu = rdo(2)
    If IsNull(rdo(3)) Then refAyu = "" Else refAyu = rdo(3)
    If IsNull(rdo(4)) Then cama = "" Else cama = rdo(4)
    firmaAyu = "" ' Esto se ha puesto porque no se quiere quye aparezcan los res. en el inf.

' Se señala para cada tipo de documento las celdas que ha de avanzar para posicionarse
' en los lugares donde se renuevan los datos.
      
    Planta = 6
    DrContesta = 4
    DrReferencia = 3
    word.Options.ReplaceSelection = True
    With .Selection
      .HomeKey Unit:=6
      ' Nos posicionamos en la primera celda de la tabla de datos de la prueba.
      .GoTo What:=2, Which:=2
      If .Information(12) = True Then ' Si se ha posicionado en la tabla
          .MoveRight Unit:=12, Count:=Planta
      End If
  
      If Right$(cama, 1) = "0" Then
        .TypeText Text:=Left$(cama, 3)
      Else
        .TypeText Text:=Left$(cama, 3) & "-" & Right$(cama, 1)
      End If
      .MoveRight Unit:=12, Count:=DrContesta
      .TypeText Text:=firma
      .MoveRight Unit:=12, Count:=DrReferencia
      ' Si se ha introducido Dr. residente -> se introduce la referencia de él, no del responsable.
      If Trim$(refAyu) <> "" Then
        .TypeText Text:=Trim$(refAyu)
      Else
        .TypeText Text:=ref
      End If
      .MoveRight Unit:=12, Count:=1
      .TypeText Text:=Format$(Now, "dd/mm/yy")
      .MoveRight Unit:=1, Count:=3
      .MoveRight Unit:=2, Count:=1, Extend:=1
      .TypeText Text:=nRef
      .EndKey Unit:=6
      ' Se introduce la firma del Dr responsable de la prueba, y del residente, si existe.
      .HomeKey Unit:=5  ' Principio de línea
      .MoveLeft Unit:=1, Count:=2
      .Move Unit:=12, Count:=-3
      .Delete
      .MoveRight Unit:=12, Count:=1
      .TypeText Text:=firma
      .HomeKey Unit:=6
      .MoveDown Unit:=5, Count:=9
    End With
    Call IntroducirDiag(nRef)
    .Selection.HomeKey Unit:=6
    .System.Cursor = 0
    .Application.ScreenUpdating = True
    .ActiveDocument.Protect Password:="sihc", NoReset:=False, Type:=0
  End With
End Sub
Sub IntroducirDiag(nRef As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim res As Integer, Diag As Long
Dim ctrl As Integer

  ctrl = 0 ' Variable que controla si hay diagnósticos confirmados
  sql = "SELECT DISTINCT AP3700.AP50_CodTiSno||'-'||AP3700.AP37_NumSno, AP3700.AP37_CodICD, " _
      & "AP3700.AP37_Descrip, AP3900.AP39_Descrip, AP3700.AP37_CodSno " _
      & "FROM AP3700, AP3900 WHERE " _
      & "AP3900.AP37_CodSno = AP3700.AP37_CodSno AND " _
      & "AP3900.AP21_CodRef = ? AND " _
      & "AP3900.AP49_CodEstDiag = ? AND " _
      & "AP3900.AP39_IndInfor = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = apDIAGDEFINITIVO
  rdoQ(2) = -1
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    ctrl = ctrl + 1
    If word.ActiveDocument.Bookmarks.Exists("D" & rdo(4)) = False Then
      If ctrl = 1 Then
        Call SituarseEnDiag
      Else
        word.ActiveDocument.Bookmarks("D" & Diag).Select
        word.Selection.EndKey Unit:=5
        word.Selection.TypeText ", "
        word.Selection.typeparagraph
      End If
      If IsNull(rdo(1)) Then
'        If IsNull(rdo(3)) Then
          word.Selection.TypeText CStr(rdo(0))
'        Else
'          word.Selection.TypeText rdo(0) & " " & Trim(rdo(3)) & " "
'        End If
      Else
'        If IsNull(rdo(3)) Then
          word.Selection.TypeText rdo(0) & " " & "(" & Trim(rdo(1)) & ")"
'        Else
'          word.Selection.TypeText rdo(0) & " (" & Trim(rdo(1)) & ") " & Trim(rdo(3)) & " "
'        End If
      End If
      word.Selection.HomeKey Unit:=5, Extend:=1
      With word.ActiveDocument.Bookmarks
        .Add Range:=word.Selection.Range, Name:="D" & rdo(4)
        .ShowHidden = False
      End With
      word.Selection.HomeKey Unit:=5
      word.Selection.TypeBackspace
      word.Selection.EndKey Unit:=5
'      word.Selection.TypeText Text:=" "

    End If
    Diag = rdo(4)
    rdo.MoveNext
  Wend
End Sub

Sub SituarseEnDiag()
Dim res As Integer

  With word
    .Selection.Find.ClearFormatting
    .Selection.Find.Font.Underline = 1
    With .Selection.Find
      .Text = "Diagnóstico citológico"
      .Replacement.Text = ""
      .Forward = True
      .Wrap = 1
      .Format = False
      .MatchCase = False
      .MatchWholeWord = False
      .MatchWildcards = False
      .MatchSoundsLike = False
      .MatchAllWordForms = False
    End With
    res = .Selection.Find.Execute
    If res = False Then
      .Selection.Find.ClearFormatting
      .Selection.Find.Font.Underline = 1
      With .Selection.Find
        .Text = "Diagnóstico anatomopatológico"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = 1
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
      End With
      res = .Selection.Find.Execute
      If res = False Then
        .Selection.EndKey Unit:=6
        .Selection.MoveUp Unit:=5, Count:=3
      End If
    End If
    .Selection.EndKey Unit:=5
    .Selection.typeparagraph
    .Selection.Font.Underline = 0
    .Selection.typeparagraph
    .Selection.style = .ActiveDocument.Styles("Normal")
  End With

End Sub
Function CrearPrueba(NH As Long, NC As Long, NS As Long, documento As String) As Integer
Dim res As Integer, i As Integer
Dim paciente As String, Condicion As String, MedicoResp As String, DptoResp As String
Dim directorio As String, nombredoc As String, Plantilla As String
Dim NombreBook As String, DocError As String, texto As String
Dim NHBarras As String, NCBarras As String, NSBarras As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim pr As String
Dim sql As String
Dim nRef As String
Dim DptoPr As String
Dim FSotud As String
    
    ' Si el documento de una prueba no existe, esta función da la oportunidad de
    ' crearlo.
    On Error Resume Next
    word.ActualizarPantalla 0
    word.EsperaCursor 1
    DocError = "NO"
    Err = 0
    If NH <> 0 And NC <> 0 And NS <> 0 Then
      sql = "SELECT PAC.nom||' '||PAC.ap1||' '||PAC.ap2 nombre, DR.txtFirma, DPTSOTUD.Desig, " _
            & "PR.cEstruct, PR.Descrip, AP2100.AP21_FecSotud, AP2100.AP21_FecSotud, " _
            & "AP2100.AP21_CodRef " _
            & "FROM AP2100, Pac, Dr, Dpt AS DPTSOTUD, cDptAP AS cDptAP, PR " _
            & "WHERE AP2100.AP21_CodHist = Pac.NH AND" _
            & "AP2100.AP21_CodDr = Dr.CDr AND " _
            & "AP2100.cDpt = DPTSOTUD.cDpt AND " _
            & "AP2100.cPr = Pr.cPr AND " _
            & "AP2100.AP21_CodHist = ? AND AP2100.AP21_CodCaso = ? AND AP2100.AP21_CodSec = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery(sql)
        rdoQ(0).Type = rdTypeINTEGER
        rdoQ(1).Type = rdTypeINTEGER
        rdoQ(2).Type = rdTypeINTEGER
        rdoQ(0) = NH
        rdoQ(0) = NC
        rdoQ(0) = NS
        Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
        paciente = rdo(0)
        MedicoResp = rdo(1)
        DptoResp = rdo(2)
        DptoPr = DptAP
        Plantilla = "\estruct\Plant" & rdo(3) & ".dot"
        pr = rdo(4)
        FSotud = rdo(5)
        
        ' Se comprueba si existe la plantilla.
        res = GetAttr(DotsPath & Plantilla)
        If Err <> 0 Then
            Err = 0
            Plantilla = "\estruct\txtlibre.dot"
            word.ArchivoNuevo DotsPath & Plantilla                   ' Word.ArchivoNuevo
            If Err <> 0 Then
                DocError = "SI"
                texto = "No se puede generar un documento nuevo con la plantilla de texto libre."
                GoTo ErrorPrueba:
            End If
            word.HerramDesprotegerDocumento "sihc"                  ' Word.HerramDesprotegerDocumento
        Else
            word.ArchivoNuevo DotsPath & Plantilla                   ' Word.ArchivoNuevo
            If Err <> 0 Then
                DocError = "SI"
                texto = "No se puede generar un documento nuevo con la plantilla de la prueba."
                GoTo ErrorPrueba:
            End If
            word.HerramDesprotegerDocumento "sihc"                  ' Word.HerramDesprotegerDocumento
        End If
                                
        Err = 0
        word.PrincipioDeDocumento                                    ' Word.PrincipioDeDocumento
        ' Se introduce el nombre de la prueba como título.
        word.insertar pr
        
        ' Se completan los datos de las cabeceras de las pruebas.
        word.EdiciónIrA "t"
        word.insertar paciente
        word.celdasiguiente
        word.insertar NH & "/" & NC
        word.celdasiguiente
        word.insertar pr
        word.celdasiguiente
        word.celdasiguiente
        word.insertar DptoResp
        word.celdasiguiente
        word.insertar MedicoResp
        word.celdasiguiente
        word.celdasiguiente
        word.insertar Format$(FSotud, "dd/mm/yy") ' Fecha de solicitud
        word.celdasiguiente
        word.celdasiguiente
        word.insertar DptoPr
        word.celdasiguiente
        word.celdasiguiente
        word.insertar Format$(rdo(6), "dd/mm/yy") ' Fecha de realización
        word.IrSecciónSiguiente

        'DoEvents
        
        ' En el encabezado se introducen NH, NC y NS en un formato
        ' que pueda ser interpretado por un lector de código de barras.
        word.VerEncabezado                                         ' Word.VerEncabezado
        word.Subrayar 0                                        ' Word.Subrayar
        word.Negrita 0                                             ' Word.Negrita
        word.Fuente "C39HrP24DhTt", 36                            ' Word.Fuente
        
        NHBarras = Trim$(NH)
        NCBarras = Trim$(NC)
        NSBarras = Trim$(NS)

        
        For i = 1 To 7 - Len(NHBarras)
            NHBarras = "0" & NHBarras
        Next i

        For i = 1 To 4 - Len(NCBarras)
            NCBarras = "0" & NCBarras
        Next i

        For i = 1 To 3 - Len(NSBarras)
            NSBarras = "0" & NSBarras
        Next i
                
        word.insertar NHBarras & NCBarras & NSBarras                       ' Word.Insertar
        
        word.PárrafoDerecha                                          ' Word.PárrafoDerecha
        word.VerEncabezado                                         ' Word.VerEncabezado
        
        Err = 0
        
        ' En el NC de que no sea un documento de Anamnesis, se renombran los bookmarks
        ' existentes en la plantilla, para que si se introduce este y otros documentos en
        ' algún informe, no coincidan los nombres de sus bookmark.
        word.EdiciónMarcador "Titulo", 0, 0, 0, 1                  ' Word.EdiciónMarcador
        NombreBook = "Titulo" & NS
        word.EdiciónMarcador NombreBook, 0, 1, 0, 0                ' Word.EdiciónMarcador
        word.EdiciónMarcador "Titulo", 0, 0, 1, 0                  ' Word.EdiciónMarcador
        If Err <> 0 Then
            DocError = "SI"
            texto = "No se encuentra el bookmark Titulo."
            GoTo ErrorPrueba:
        End If
        
        word.EdiciónMarcador "Cabecera", 0, 0, 0, 1                ' Word.EdiciónMarcador
        NombreBook = "Cabecera" & NS
        word.EdiciónMarcador NombreBook, 0, 1, 0, 0                ' Word.EdiciónMarcador
        word.EdiciónMarcador "Cabecera", 0, 0, 1, 0                ' Word.EdiciónMarcador
        If Err <> 0 Then
            DocError = "SI"
            texto = "No se encuentra el bookmark Cabecera."
            GoTo ErrorPrueba:
        End If
        
        word.EdiciónMarcador "Contenido", 0, 0, 0, 1               ' Word.EdiciónMarcador
        NombreBook = "Contenido" & NS
        word.EdiciónMarcador NombreBook, 0, 1, 0, 0                ' Word.EdiciónMarcador
        word.EdiciónMarcador "Contenido", 0, 0, 1, 0               ' Word.EdiciónMarcador
        If Err <> 0 Then
            DocError = "SI"
            texto = "No se encuentra el bookmark Contenido."
            GoTo ErrorPrueba:
        End If

        word.HerramRevisiones 0, 0, 0, 0, 0                       ' Word.HerramRevisiones
        word.HerramProtegerDocumento "sihc", 1, 2                  ' Word.HerramProtegerDocumento
        word.VerPágina
        word.PrincipioDeDocumento
        word.insertar rdo(7)  ' Número de referencia

        Err = 0
        ' Se obtiene el fichero del documento de la prueba
        directorio = DocsPath & "\" & cDptAP & "\pruebas\" & Year(FSotud) & "\" & Month(FSotud)
        ' Se comprueba si existe el directorio, y si no, se crea.
        res = GetAttr(directorio)
        If Err <> 0 Then
            res = CreacionDirectorio(directorio)
        End If
        Err = 0
        'DoEvents
        word.ArchivoGuardarComo documento                               ' Word.ArchivoGuardarComo
        If Err <> 0 Then
            DocError = "SI"
            texto = "No se puede salvar el fichero del documento."
            GoTo ErrorPrueba:
        End If
        
        
        word.PrincipioDeDocumento
        word.HerramRevisiones 1, 0, 0, 0, 0                       ' Word.HerramRevisiones
        word.HerramDesprotegerDocumento "sihc"
        word.HerramProtegerDocumento "sihc", 0, 0                ' Word.HerramProtegerDocumento
        word.LíneaAbajo 2
        CrearPrueba = 1
    End If
    
ErrorPrueba:
    If DocError = "SI" Then
        MsgBox texto, 16, "Error en la generación de documento"
        CrearPrueba = 0
        word.ArchivoCerrartodo 2
    Else
        CrearPrueba = 1
    End If
    
    word.ActualizarPantalla 1                                           ' Word.ActualizarPantalla
    word.EsperaCursor 0
    Exit Function
End Function

Function CreacionDirectorio(directorio As String)
    ' Si no existe un directorio lo crea
    
    Dim Control As Integer, Posicion1 As Integer, Posicion2 As Integer
    Dim ResCode As Integer
    Dim ParteDirectorio As String
    
    Posicion2 = 0
    Do While Control = 0
        Posicion1 = Posicion2
        Posicion2 = InStr(Posicion1 + 1, directorio, "\")
        If Posicion2 = 0 Then
            Posicion2 = Len(directorio) + 1
            Control = 1
        End If

        ParteDirectorio = Left(directorio, Posicion2 - 1)
        On Error Resume Next
        ResCode = GetAttr(ParteDirectorio)
        If Err <> 0 Then
            MkDir (ParteDirectorio)
        End If
        On Error GoTo 0
    Loop

End Function

Function GuardaCambios(NH As Long, NC As Long, NS As Long, Estado As Integer, cPat As String, cRes As Integer, documento As String) As Integer
    ' Se utiliza para guardar los documentos (y formatearlos si es necesario. Por ej:
    ' introducir la fecha de mecanografiado o Dr que contesta).
    
'Dim ResCode As Integer, res As Integer, Res1 As Integer, i As Integer
'Dim contador As Integer, Control As Integer
'Dim msg As String, sql As String
'Dim Condicion As String, Winword As String
'Dim firmaDrAyu As String
'Dim firma As String
'Dim snp As Snapshot
'Dim ref As String
'
'    Err = 0
'    Screen.MousePointer = 11
''    On Error Resume Next
'    ' Primero se comprueba que el word está abierto, y si no se abre.
'    Winword = word.Version                                   ' Word.ApInfo(2)
'    If Err = 0 Then
'      With word
'        ' Si el médico que contesta ha sido modificado, guardamos el dato en
'        ' la tabla e introducimos el dato en el documento.
'        word.System.Cursor = 3
'        ' Se salva el documento
'        Control = 0
'        contador = 0
'        word.Application.ScreenUpdating = False
'        'DoEvents
'        ' Se insertan los datos en la cabecera de los documentos (salvo en IM).
'        .ActiveDocument.Unprotect Password:="sihc"
'        'DoEvents
''        If Estado = 1 Then ' Recepcionada pero no mecanografiada
''          res = InsertarDatosCabecera(NH, NC, NS, cPat, cRes)
'          Estado = ActualizarPr(cPat)
''        End If
'        .ActiveDocument.Protect Password:="sihc", NoReset:=False, Type:=0
'
'        Err = 0
'        res = GetAttr(documento)
'        If Err <> 0 Then
'            Err = 0
'            .ActiveDocument.SaveAs filename:=documento                   ' Word.ArchivoGuardarComo
'        Else
'            Err = 0
'            .ActiveDocument.Save                               ' Word.ArchivoGuardar
'        End If
'        .Application.ScreenUpdating = False
'        GuardaCambios = 1
'        .System.Cursor = 0
''        word.PrincipioDeDocumento
'      End With
'    Else
'      Err = 0
'      ' Si el Word está cerrado, se cierra el form
'      msg = "No existe comunicación con MS WinWord. No se guardarán las últimas modificaciones."
'      MsgBox msg, vbExclamation, "Error de Archivo"
'      Screen.MousePointer = 0
'      Call Descarga
'    End If
'    Screen.MousePointer = 0
End Function
Function ActualizarPr(cPat As String) As Integer
Dim nRef As String
Dim sql As String
Dim rdoQ As rdoQuery

  ' Actualización de AP2100
'  ActualizarPr = 1
  nRef = objPipe.PipeGet("NumRef")
'  sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ?"
'  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
'  rdoQ(0).Type = rdTypeINTEGER
'  rdoQ(1).Type = rdTypeVARCHAR
'  rdoQ(0) = apMECANOGRAFIADA
'  rdoQ(1) = nref
'  rdoQ.Execute
'  If rdoQ.RowsAffected = 1 Then
'    ActualizarPr = 2
'  Else
'    MsgBox "Error en la actualización de la base de datos", vbExclamation, "Error"
'  End If


  ' Actualización de PRASIST
  sql = "UPDATE PrAsist SET EstPr = ?, FMeca = SYSDATE, " _
      & "HMeca =  SYSDATE, cDrCont = ? WHERE nRayos = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = 3
  rdoQ(1) = cPat
  rdoQ(2) = nRef
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then
    ActualizarPr = 2
  Else
    MsgBox "Error en la actualización de la base de datos", vbExclamation, "Error"
  End If



End Function

Function InsertarDatosCabecera(NH As Long, NC As Long, NS As Long, cPat As Integer, cRes As Integer) As Integer
Dim DrContesta As Integer, DrReferencia As Integer, i As Integer, Planta As Integer, CabeceraExterna As Integer
Dim InsertarFecha As String
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim cama As String
Dim firma As String, ref As String
Dim firmaAyu As String, refAyu As String
Dim nRef As String

    ' Introduce los datos en la cabecera de los documentos de pruebas.
'  On Error Resume Next
      
  nRef = objPipe.PipeGet("NumRef")
  word.EsperaCursor 1
  word.HerramOpcionesEdición 1

  ' Se obtienen los datos del patólogo
  sql = "SELECT AP08_Firma, AP08_ref FROM AP0800 WHERE AP08_CodPers = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = cPat
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  firma = rdo(0)
  ref = rdo(1)
  rdo.Close
  
  ' Se obtienen los datos del residente
  rdoQ(0) = cRes
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  firmaAyu = rdo(0)
  refAyu = rdo(1)
  rdo.Close
    
    ' Se señala para cada tipo de documento las celdas que ha de avanzar para posicionarse
    ' en los lugares donde se renuevan los datos.
    
  Planta = 6
  DrContesta = 4
  DrReferencia = 3
    
  word.PrincipioDeDocumento                                        ' Word.PrincipioDeDocumento
  ' Nos posicionamos en la primera celda de la tabla de datos de la prueba.
  word.EdiciónIrA "t"                                           ' Word.EdiciónIrA
  For i = 1 To Planta
      word.celdasiguiente                                           ' Word.CeldaSiguiente
  Next i
      'DoEvents
  sql = "SELECT cama FROM asist WHERE nh = ? AND nc = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = NH
  rdoQ(1) = NC
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If IsNull(rdo(0)) Then cama = "" Else cama = rdo(0)
  rdo.Close
  
  If Right$(cama, 1) = "0" Then
      word.insertar Left$(cama, 3)
  Else
      word.insertar Left$(cama, 3) & "-" & Right$(cama, 1)
  End If
  'DoEvents
            
    For i = 1 To DrContesta
        word.celdasiguiente                                           ' Word.CeldaSiguiente
    Next i
    'DoEvents
    word.insertar firma
    For i = 1 To DrReferencia
        word.celdasiguiente                                           ' Word.CeldaSiguiente
    Next i
    DoEvents
    ' Si se ha introducido Dr. residente -> se introduce la referencia de él, no del responsable.
    If Trim$(refAyu) <> "" Then
        word.insertar Trim$(refAyu)
    Else
        word.insertar ref
    End If
    word.celdasiguiente                                               ' Word.CeldaSiguiente
    word.insertar Format$(Now, "dd/mm/yy")                    ' Word.Insertar
    word.CarácterDerecha 3
    word.insertar nRef
    
    ' Se introduce la firma del Dr responsable de la prueba, y del residente, si existe.
    word.FinDeDocumento
    'DoEvents
    word.PrincipioDeLínea
    'DoEvents
    word.CarácterIzquierda 2
    'DoEvents
    word.CeldaAnterior
    'DoEvents
    word.celdasiguiente
    'DoEvents
    word.CeldaAnterior
    'DoEvents
    word.EdiciónDespejar
    'DoEvents
    word.celdasiguiente
    'DoEvents
    word.insertar firma
    'DoEvents
    word.EsperaCursor 0
End Function


Public Function FormatoVentana(vertical As Long, titulo As String, pantalla As Form) As Integer
    
    With frmPrincipal
      .WindowState = 0
      .Top = 0
      .Left = 0
      .Height = 100
      .Width = 12000
    End With
    
    With pantalla
      .WindowState = 0
      .Caption = titulo
      .Top = 0
      .Left = 0
      .Height = vertical
      .Width = 12000
      .Visible = True
    End With
    
    With word
      .Application.WindowState = 0
      .Application.Move Left:=0, Top:=60
      .Application.Resize Width:=600, Height:=390
    End With
End Function

Public Sub pActivarWord(documento As String)
  On Error Resume Next
  AppActivate "Microsoft Word - " '& Right(documento, 12)
End Sub


Sub pArrancarWord()
Dim Winword As String
  
  On Error Resume Next
  Err = 0
  Winword = word.Version
  If Winword = "" Then
    Err = 0
    Set word = GetObject(, "word.Application")
    If Err <> 0 Then
      Set word = CreateObject("word.application")
    Else
      word.Documents.Close SaveChanges:=0
    End If
    Err = 0
    With word
      .Visible = True
      .addins.Unload True
      .Application.DefaultSaveFormat = "MSWord6Exp"
      .Application.DisplayRecentFiles = False
      .activewindow.View.FieldShading = 0
      .Options.UpdateLinksAtOpen = False
    End With
  End If

End Sub
Sub CerrarWord()
  On Error Resume Next
  Set word = Nothing
  'word.Application.quit
End Sub
Sub Descarga()
    Dim res As Integer
    On Error Resume Next

    ' Se le llama desde el botón salir. Cierra todos los querys utilizados
    ' en la pantalla de mecanografiado y devuelve el form principal a su estado original.

    With frmPrincipal
      .WindowState = 0
      .Top = 0
      .Left = 0
      .Height = 9000
      .Width = 12000
      .WindowState = 2
    End With
'    Unload frmMecaDocumento
    Unload frmBuscPr
End Sub

Function VerInforme(nRef As String) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim NH As Long, NC As Long, NS As Long
Dim res As Integer, ano As Integer, mes As Integer
Dim documento As String

  sql = "SELECT PR04NumActPlan FROM AP2100 WHERE AP2100.AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)

  With vs
    Call .AddPr(vs.vsTIPODOCPRUEBA, CLng(rdo(0)))
    VerInforme = .VerInforme
    Call .BorrarDocs
  End With
  

End Function
Sub MecaInforme(nRef As String)
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset

'  Call objPipe.PipeSet("NumRef", nRef)
  sql = "SELECT AP21_CodHist, AP21_CodCaso, AP21_CodSec, AP21_FecSotud FROM AP2100 WHERE " _
      & "AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)

  With meca
    Call .AddPr(meca.vsTIPODOCPRUEBA, rdo(0), rdo(1), rdo(2))
    Call .MecaInforme
    Call .BorrarDocs
  End With
End Sub
