VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmPrAnteriores 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pruebas anteriores"
   ClientHeight    =   7680
   ClientLeft      =   150
   ClientTop       =   1125
   ClientWidth     =   11670
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7680
   ScaleWidth      =   11670
   Begin VB.CommandButton cmdAbrir 
      Appearance      =   0  'Flat
      Caption         =   "&Abrir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7425
      TabIndex        =   24
      Top             =   225
      Width           =   1275
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Confirmar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10275
      TabIndex        =   17
      Top             =   4275
      Width           =   1290
   End
   Begin VB.Frame fraDiag 
      Caption         =   "Acciones sobre los diag."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1440
      Index           =   1
      Left            =   9450
      TabIndex        =   14
      Top             =   300
      Width           =   2040
      Begin VB.CommandButton cmdInfor 
         Caption         =   "&Informar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   450
         TabIndex        =   16
         Top             =   375
         Width           =   1215
      End
      Begin VB.CommandButton cmdAnular 
         Caption         =   "A&nular"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   450
         TabIndex        =   15
         Top             =   900
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdInforme 
      Appearance      =   0  'Flat
      Caption         =   "Ver &Informe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10200
      TabIndex        =   12
      Top             =   7050
      Width           =   1275
   End
   Begin VB.Frame fraDiag 
      Caption         =   "Situación"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1890
      Index           =   0
      Left            =   8850
      TabIndex        =   11
      Top             =   4875
      Width           =   2715
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Enabled         =   0   'False
         Height          =   1515
         Left            =   75
         TabIndex        =   18
         Top             =   300
         Width           =   2565
         Begin VB.TextBox txtEstado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "GC02_CODIGO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   22
            Tag             =   "Fase|Código de la Fase"
            Top             =   225
            Width           =   2355
         End
         Begin VB.TextBox txtFInfor 
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1125
            Locked          =   -1  'True
            TabIndex        =   21
            Top             =   1050
            Width           =   1365
         End
         Begin VB.CheckBox chkInfor 
            Caption         =   "Informado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   0
            TabIndex        =   20
            Top             =   1050
            Width           =   1065
         End
         Begin VB.CheckBox chkIntra 
            Caption         =   "Intraoperatoria"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   0
            TabIndex        =   19
            Top             =   600
            Width           =   1515
         End
         Begin VB.Label Label2 
            Caption         =   "Estado:"
            Height          =   165
            Left            =   0
            TabIndex        =   23
            Top             =   0
            Width           =   1815
         End
      End
   End
   Begin VB.Frame fraFoto 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   615
      Left            =   150
      TabIndex        =   6
      Top             =   6825
      Visible         =   0   'False
      Width           =   8715
      Begin VB.TextBox txtPos 
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5625
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   27
         Top             =   225
         Width           =   1365
      End
      Begin VB.TextBox txtCarrete 
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3975
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   25
         Top             =   225
         Width           =   1590
      End
      Begin VB.TextBox txtFecha 
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7050
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   10
         Top             =   225
         Width           =   1590
      End
      Begin VB.TextBox txtLugar 
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   0
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   225
         Width           =   3915
      End
      Begin VB.Label Label4 
         Caption         =   "Posición:"
         Height          =   165
         Left            =   5625
         TabIndex        =   28
         Top             =   0
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Carrete:"
         Height          =   165
         Left            =   3975
         TabIndex        =   26
         Top             =   0
         Width           =   1515
      End
      Begin VB.Label Label1 
         Caption         =   "Fecha creación:"
         Height          =   165
         Left            =   7050
         TabIndex        =   9
         Top             =   0
         Width           =   1515
      End
      Begin VB.Label lblTit 
         Caption         =   "Lugar de la fotografía:"
         Height          =   240
         Left            =   0
         TabIndex        =   8
         Top             =   0
         Width           =   1965
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Identificación"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   765
      Index           =   0
      Left            =   150
      TabIndex        =   2
      Top             =   225
      Width           =   6990
      Begin VB.TextBox txtPaciente 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         DataField       =   "GC02_DESCRIP"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1950
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   4
         Tag             =   "Fase|Descripción de la Fase"
         Top             =   375
         Width           =   4905
      End
      Begin VB.TextBox txtPaciente 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         DataField       =   "GC02_CODIGO"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1050
         Locked          =   -1  'True
         MaxLength       =   7
         TabIndex        =   3
         Tag             =   "Fase|Código de la Fase"
         Top             =   375
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Paciente:"
         Height          =   255
         Index           =   3
         Left            =   150
         TabIndex        =   5
         Top             =   375
         Width           =   885
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7425
      TabIndex        =   0
      Top             =   750
      Width           =   1275
   End
   Begin ComctlLib.TreeView tvwDatos 
      Height          =   5490
      Left            =   150
      TabIndex        =   1
      Top             =   1275
      Width           =   8565
      _ExtentX        =   15108
      _ExtentY        =   9684
      _Version        =   327682
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "imgIcos"
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.ListView lvwPat 
      Height          =   2340
      Left            =   8850
      TabIndex        =   13
      Top             =   1875
      Width           =   2715
      _ExtentX        =   4789
      _ExtentY        =   4128
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin ComctlLib.ImageList imgIcos 
      Left            =   8850
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   3
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00739.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00739.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00739.frx":0634
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPrAnteriores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public PwdCorrect As Integer
Dim NH As Long, nRef As String
Dim lugar() As String
Dim Diag() As DatosDiag
Dim pr() As DatosPr
Dim pantalla As String
Dim Pat() As String, PatSelec() As String
Dim TiDiag(0 To 3) As Integer
Dim posicion As Integer
Dim cDiag As Long
Dim i As Integer
Option Explicit

Sub LlenarDatosFotos()
Dim nodo As Node
    
  tvwDatos.Nodes.Clear
  Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Fotografías", 1, 1)
  With frmBuscPr
    txtPaciente(0).Text = .rdo(0) ' Número de historia
    txtPaciente(1).Text = .rdo(1) ' Paciente
    While .rdo.EOF = False
      nRef = .rdo(2)
      Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "P" & nRef, Right(nRef, 8) & " - " & .rdo(3), 3, 3)
      Call LlenarFotos("P" & nRef)
      .rdo.MoveNext
    Wend
  End With
  Unload frmBuscPr
  nodo.EnsureVisible
End Sub

Sub LlenarDatosPrAnteriores()
Dim nodo As Node
  
  With frmBuscPr
    NH = .rdo(0)
    txtPaciente(1).Text = .rdo(1) ' Paciente
  End With
  txtPaciente(0).Text = NH ' Número de historia
  tvwDatos.Nodes.Clear
  Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Pruebas", 1, 1)
  Call LlenarPrAnteriores
  nodo.Child.EnsureVisible
  Unload frmBuscPr

End Sub

Private Sub cmdAbrir_Click()
  frmBuscPr.Show vbModal
End Sub

Private Sub cmdAnular_Click()
Dim sql As String
Dim rdoQ As rdoQuery
Dim cDiags As String
      
  On Error GoTo seguir
  posicion = 1
  While cDiag <> Diag(posicion).Numero
    posicion = posicion + 1
  Wend
  
  If cDiag > 0 And Diag(posicion).Estado <> apDIAGANULADO Then
    sql = "UPDATE AP3900 SET AP49_CODESTDIAG = ? " _
        & "WHERE AP39_CodDiag = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeINTEGER
    rdoQ(0) = apDIAGANULADO
    rdoQ(1) = cDiag
    rdoQ.Execute
    txtEstado.Text = "Anulado"
    Diag(posicion).Estado = apDIAGANULADO
  End If
  Exit Sub
seguir:
  Exit Sub
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub


Private Sub cmdGuardar_Click()
Dim cDiag As Long
Dim nodo As Node
Dim num As Long, i As Integer, res As Integer
Dim Diag As String, texto As String
  
'  On Error Resume Next
  Err = 0
  Set nodo = tvwDatos.SelectedItem
  cDiag = Val(Right(nodo.key, Len(nodo.key) - 1))
  Diag = nodo.Text
  If Err = 91 Then 'Si no se ha seleccionado ningún diagnóstico
    MsgBox "Debe seleccionar un diagnóstico.", vbInformation, "Selección incorrecta"
  Else
    Select Case Cambios()
      Case 0
        Exit Sub
      Case 1
        Call modDiag(cDiag)
      Case 2
        Load frmIDDr
        Call LlenarPatNuevos(Pat())
        If Val(Pat(1, 1)) <> -1 Then
          texto = "Desea que el diagnóstico '" & Diag & "' sea confirmado por los patólogos:" & Chr$(13) & Chr$(13)
          For i = 1 To UBound(Pat, 2)
            texto = texto & Chr$(9) & "- " & Pat(2, i) & Chr$(13)
          Next i
          res = MsgBox(texto, vbQuestion + vbYesNo, "Introducción de diagnósticos")
          If res = vbYes Then
            Call InsertDiagDr(cDiag, 8)
          End If
        End If
        Call CerrarrdoQueries
      
      Case 3
        Load frmIDDr
        Call LlenarPatNuevos(Pat())
        If Pat(1, 1) <> -1 Then
          texto = "Desea que el diagnóstico '" & Diag & "' sea confirmado por los patólogos:" & Chr$(13) & Chr$(13)
          For i = 1 To UBound(Pat, 2)
            texto = texto & Chr$(9) & "- " & Pat(2, i) & Chr$(13)
          Next i
          res = MsgBox(texto, vbQuestion + vbYesNo, "Introducción de diagnósticos")
          If res = vbYes Then
            Call modDiag(cDiag)
            Call InsertDiagDr(cDiag, 8)
          End If
        End If
        Call CerrarrdoQueries
      
      Case 10 To 19  ' Definitivo. Proviene del estado Sin Confirmar
        Load frmIDDr
        Call LlenarIDDr(Pat(), Me)
        If Pat(1, 1) <> -1 Then
          texto = "Desea que el diagnóstico '" & Diag & "' sea confirmado por los patólogos:" & Chr$(13) & Chr$(13)
          For i = 1 To UBound(Pat, 2)
            texto = texto & Chr$(9) & "- " & Pat(2, i) & Chr$(13)
          Next i
          res = MsgBox(texto, vbQuestion + vbYesNo, "Introducción de diagnósticos")
          If res = vbYes Then
            Call modDiag(cDiag)
            Call InsertDiagDr(cDiag, 0)
          End If
        End If
      
      Case 20 To 29  ' Definitivo. Proviene del estado Provisional
        Load frmIDDr
        Call LlenarIDDr(Pat(), Me)
        If Pat(1, 1) <> -1 Then
          texto = "Desea que el diagnóstico '" & Diag & "' sea confirmado por los patólogos:" & Chr$(13) & Chr$(13)
          For i = 1 To UBound(Pat, 2)
            texto = texto & Chr$(9) & "- " & Pat(2, i) & Chr$(13)
          Next i
          res = MsgBox(texto, vbQuestion + vbYesNo, "Introducción de diagnósticos")
          If res = vbYes Then
            num = fNextClave("AP39_CodDiag")
            'num = NumDiag()
            Call InsertDiagCopia(cDiag, num)
            Call InsertDiagDr(num, 0)
'            Call AnularDiag(cDiag, apPROVISIONALANULADA)
          End If
        End If
      
      Case 30 To 39 ' Provisional
        Load frmIDDr
        Call LlenarIDDr(Pat(), Me)
        If Pat(1, 1) <> -1 Then
          texto = "Desea que el diagnóstico '" & Diag & "' sea confirmado por los patólogos:" & Chr$(13) & Chr$(13)
          For i = 1 To UBound(Pat, 2)
            texto = texto & Chr$(9) & "- " & Pat(2, i) & Chr$(13)
          Next i
          res = MsgBox(texto, vbQuestion + vbYesNo, "Introducción de diagnósticos")
          If res = vbYes Then
            Call modDiag(cDiag)
            Call InsertDiagDr(cDiag, 0)
          End If
        End If
      
      Case Is >= 40
        Call AnularDiag(cDiag, apDIAGANULADO)
      
    End Select
    Call CerrarrdoQueries
  End If
End Sub
Function Cambios() As Integer
Dim item As ListItem
Dim numItems As Integer, dimension As Integer

  On Error Resume Next
  Cambios = 0
  With Diag(posicion)
'    If optDiag(.Estado - 1) = False Then ' Si se ha cambiado el estado
'      If optDiag(3).Value = True Then  ' Anulado
'        Cambios = 40
'      ElseIf optDiag(2).Value = True Then ' Definitivo
'        If .Estado = 1 Then  ' Proviene del estado Sin Confirmar
'          Cambios = 10
'        ElseIf .Estado = 2 Then ' Proviene del estado Provisional
'          Cambios = 20
'        End If
'      ElseIf optDiag(1).Value = True Then  ' Povisional
'        Cambios = 30
'      End If
    If .Estado = 2 Or .Estado = 3 Then  ' Si el Diag es definitivo hay que ver si otros Dr lo han confirmado
      numItems = 0
      For Each item In lvwPat.ListItems
        If item.Selected = True Then numItems = numItems + 1
      Next item
      dimension = UBound(PatSelec)
      If dimension = 0 Then
        Cambios = 0
      ElseIf numItems <> dimension Then
        Cambios = 2
      End If
    End If
    
    ' Comprobacion de cambios en los CHECK
    If chkIntra.Value <> Abs(.intra) Then Cambios = Cambios + 1
    If chkInfor.Value <> Abs(.Infor) Then Cambios = Cambios + 1
  End With
  
End Function
Sub LlenarPatNuevos(Pat() As String)
Dim ctrl As Integer, i As Integer, nuevo As Integer
Dim item As ListItem
Dim CodPat As String, NumPat As Integer

  ReDim Pat(1 To 2, 1 To 1)
  Pat(1, 1) = -1
  ctrl = 0
  For Each item In lvwPat.ListItems
    If item.Selected = True Then
      nuevo = True
      For i = 1 To UBound(PatSelec)
        If Right(item.key, Len(item.key) - 1) = PatSelec(i) Then
          nuevo = False
          Exit For
        End If
      Next i
      If nuevo = True Then
        ctrl = 1
        With frmIDDr
          CodPat = Right(item.key, Len(item.key) - 1)
          .txtPwd.Tag = CodPat
          .txtPat.Text = item.Text
          .txtPwd.Text = ""
          .Show vbModal
          If .PwdCorrect = True Then
            If UBound(Pat, 2) = 1 And Pat(1, 1) = -1 Then
              Pat(1, 1) = CodPat
              Pat(2, 1) = item.Text
            Else
              NumPat = UBound(Pat, 2) + 1
              ReDim Preserve Pat(1 To 2, 1 To NumPat)
              Pat(1, NumPat) = CodPat
              Pat(2, NumPat) = item.Text
            End If
          End If
        End With
      End If
    End If
  Next item
  If ctrl = 0 Then
    MsgBox "Debe seleccionar un patólogo.", vbInformation, "Introducción incorrecta"
  End If
End Sub
Sub modDiag(cDiag As Long)
Dim sql As String, hora As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP3900 SET AP39_IndIntra = ?, AP49_CodEstDiag = ? "
  If chkInfor.Value = 1 Then sql = sql & ", AP39_IndInfor = ?, AP39_FecInfor = SYSDATE "
  sql = sql & "WHERE AP39_CodDiag = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = -chkIntra.Value
'  rdoQ(1) = EstadoDiag()
  If chkInfor.Value = 1 Then
    rdoQ(2) = -chkInfor.Value
    rdoQ(3) = cDiag
  Else
    rdoQ(2) = cDiag
  End If
  rdoQ.Execute sql

  With Diag(posicion)
    .intra = -chkIntra.Value
    .Infor = -chkInfor.Value
'    .Estado = EstadoDiag()
    If chkInfor.Value = 1 Then
      hora = Format$(Hoy(), "DD/MM/YY HH:MM")
      txtFInfor.Text = hora
      .FInfor = hora
    End If
    If .Estado = 3 Then
'      optDiag(0).Enabled = False
'      optDiag(1).Enabled = False
'      optDiag(3).Enabled = False
'    ElseIf .Estado = 2 Then
'      optDiag(0).Enabled = False
    End If
  End With
End Sub
Sub InsertDiagDr(num As Long, Cambio As Integer)
Dim sql As String, fecha As String
Dim rdoQ As rdoQuery
Dim i As Integer
  
  For i = 1 To UBound(Pat, 2)
    sql = "INSERT INTO AP4000 (AP39_CodDiag, AP40_TiDr, AP40_CodDr, AP40_FecConf) VALUES " _
        & "(?, ?, ?, SYSDATE)"  ' TiDr=1 Es doctor de CUN  TiDr=2 Es doctor externo
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = num
    rdoQ(1) = 1
    rdoQ(2) = Pat(1, i)
    rdoQ.Execute sql
    lvwPat.ListItems("P" & Pat(1, i)).SubItems(1) = Format$(Hoy(), "DD/MM/YY HH:MM")
    If Cambio = 8 Then
      ReDim Preserve PatSelec(1 To UBound(PatSelec) + 1)
      PatSelec(UBound(PatSelec)) = Pat(1, i)
    End If
  Next i
End Sub

Sub AnularDiag(cDiag As Long, Estado As Integer)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP3900 SET AP49_CodEstDiag = ? WHERE AP39_CodDiag = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeVARCHAR
  
  rdoQ(0) = Estado
  rdoQ(1) = cDiag
  rdoQ.Execute sql
  
  If Estado = apDIAGANULADO Then
    tvwDatos.Nodes.Remove tvwDatos.SelectedItem.key
    Call tvwDatos_NodeClick(tvwDatos.SelectedItem)
  Else
    Diag(posicion).Estado = 3
'    optDiag(1).Enabled = False
'    optDiag(3).Enabled = False
  End If
End Sub

Sub InsertDiagCopia(cDiag As Long, num As Long)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "INSERT INTO AP3900 (AP39_CodDiag, AP21_CodRef, AP25_CodMues, AP34_CodBloq, " _
      & "AP36_CodPorta, AP37_CodSno, AP39_Descrip, Ap39_FecDiag, " _
      & "AP39_Observ, AP49_CodEstDiag, AP39_INDINTRA, AP39_INDInfor"
  If chkInfor.Value = 1 Then
    sql = sql & ", AP39_FecInfor) "
  Else
    sql = sql & ") "
  End If
  sql = sql & "SELECT ?, AP21_CodRef, AP25_CodMues, AP34_CodBloq, AP36_CodPorta, " _
      & "AP37_CodSno, AP39_Descrip, Ap39_FecDiag, AP39_Observ, ?, ?, ? "
  If chkInfor.Value = 1 Then sql = sql & ", SYSDATE "
  sql = sql & "FROM  AP3900 WHERE AP39_CodDiag = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = num
'  rdoQ(1) = EstadoDiag()
  rdoQ(2) = -chkIntra.Value
  rdoQ(3) = -chkInfor.Value
  rdoQ(4) = cDiag
  rdoQ.Execute sql
  If chkInfor.Value = 1 Then
    txtFInfor.Text = Format$(Hoy(), "DD/MM/YY HH:MM")
  End If
End Sub

Private Sub cmdInfor_Click()
Dim sql As String
Dim rdoQ As rdoQuery
Dim posicion As Integer

  If cDiag > 0 And chkInfor.Value = vbUnchecked Then
    sql = "UPDATE AP3900 SET AP39_INDINFOR = -1, " _
        & "AP39_FecInfor = SYSDATE " _
        & "WHERE AP39_CodDiag = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = cDiag
    rdoQ.Execute
    
    posicion = 1
    While cDiag <> Diag(posicion).Numero
      posicion = posicion + 1
    Wend
    chkInfor.Value = vbChecked
    txtFInfor.Text = Format$(Hoy(), "DD/MM/YY HH:MM")
    With Diag(posicion)
      .Infor = 1
      .FInfor = txtFInfor.Text
    End With
  End If
End Sub

Private Sub cmdInforme_Click()
Dim posicion As Integer, clave As String
Dim ref As String, Anyo As String, mes As String
Dim nombredoc As String, res As Integer
Dim msg As String
  
  On Error Resume Next
  clave = tvwDatos.SelectedItem.key
  If clave = "Raiz" Then
    MsgBox "Debe seleccionar alguna prueba o diagnóstico", vbInformation, "Ver informe"
    Exit Sub
  End If
  If Left(clave, 1) = "D" Then
    clave = tvwDatos.SelectedItem.Parent.key
  End If
  ref = Right(clave, Len(clave) - 1)
  If ref <> "" Then
    posicion = 1
    While ref <> pr(posicion).nRef
      posicion = posicion + 1
    Wend
    Call VerInforme(ref)
  Else
    MsgBox "Debe seleccionar alguna prueba o diagnóstico", vbInformation, "Ver informe"
  End If
  Screen.MousePointer = Default


End Sub

Private Sub Form_Activate()
  
  i = 0
  Select Case objPipe.PipeGet("menu")
    Case "Fotos"
      If NH <> objPipe.PipeGet("NH") Then Call LlenarDatosFotos
    Case "Diag"
      Call VaciarDatos
      If NH <> objPipe.PipeGet("NH") Then Call LlenarDatosPrAnteriores
  End Select
End Sub

Private Sub Form_Load()
Dim boton As String
Dim nodo As Node, i As Integer
  
  Set tvwDatos.ImageList = frmPrincipal.imgIcos
  Select Case objPipe.PipeGet("menu")
    Case "Petición"
      nRef = objPipe.PipeGet("NumRef")
      With frmP_Peticion
        NH = Left(.txtPaciente(0).Text, InStr(.txtPaciente(0).Text, "/") - 1)
        txtPaciente(1).Text = .txtPaciente(1).Text
      End With
      txtPaciente(0).Text = NH
      boton = objPipe.PipeGet("Boton")
      cmdAbrir.Enabled = False
      Select Case boton
        Case "Fotos"
          pantalla = "Fotos"
          Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Fotografías", apICONRAIZ, apICONRAIZ)
          Call FormatearFotos
          Call LlenarFotos("Raiz")
        Case "Pr"
          pantalla = "Diag"
          Call FormatearDiag
          Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Pruebas", apICONRAIZ, apICONRAIZ)
          Call LlenarPrAnteriores
          Call LlenarPat
      End Select
    
    Case "Archivo"
'      With frmP_Recepcion
'        NH = .txtPaciente(0).Text
'        txtPaciente(1).Text = .txtPaciente(1).Text
'      End With
'      txtPaciente(0).Text = NH
'      boton = objPipe.PipeGet("Boton")
'      Select Case boton
'        Case "Fotos"
'          nRef = objPipe.PipeGet("NumRef")
'          pantalla = "Fotos"
'          Call FormatearFotos
'          Call LlenarFotos("Raiz")
'        Case "Pr"
'          pantalla = "Diag"
'          Call FormatearDiag
''          Call LlenarOptDiag
'          Call LlenarPrAnteriores
'          Call LlenarPat
'      End Select
'
    Case "Fotos"
      pantalla = "Fotos"
      NH = objPipe.PipeGet("NH")
      Call FormatearFotos
      Call LlenarDatosFotos
  
    Case "Diag"
      pantalla = "Diag"
      NH = objPipe.PipeGet("NH")
      Call FormatearDiag
      Call LlenarDatosPrAnteriores
      Call LlenarPat
  End Select
  If tvwDatos.Nodes("Raiz").Children > 0 Then
    tvwDatos.Nodes("Raiz").Child.EnsureVisible
  End If
End Sub
Sub FormatearFotos()
Dim nodo As Node
  
  With Me
    .Caption = "Fotografías"
    .Width = 9180
    .Left = 1440
  End With
  fraFoto.Visible = True
  fraDiag(0).Visible = False
  lvwPat.Visible = False
End Sub
Sub FormatearDiag()
Dim nodo As Node
  
  With Me
    .Caption = "Pruebas anteriores"
    .Width = 11730
  End With
  fraFoto.Visible = False
  fraDiag(0).Visible = True
  lvwPat.Visible = True
  lblTit.Caption = "Observaciones:"
End Sub

Sub LlenarPrAnteriores()
Dim sql As String, texto As String
Dim nodo As Node
Dim rdoPr As rdoResultset, rdoDiag As rdoResultset
Dim rdoQPr As rdoQuery, rdoQDiag As rdoQuery
Dim i As Integer, j As Integer, Estado As Integer, nPr As String
  
  Estado = 0
  j = 0
  i = 0
  nPr = ""
  sql = "SELECT AP2100.AP21_CodRef, AP2100.PR01CodActuacion, PR0100.PR01DesCompleta, " _
      & "AP2100.AP45_CODESTPR, AP2100.PR04NumActPlan, AP2100.AP21_FecSotud, " _
      & "AP2100.AP21_FecGen, PR5200.PR52NumMuestra, " _
      & "PR5200.PR52NumIDMuestra, PR2400.PR24DesCompleta||'. '||PR5200.PR52DesMuestra " _
      & "FROM AP2100, PR0100, AP2500, PR2400, PR5200 WHERE " _
      & "AP2100.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "AP2100.AP21_CodRef = AP2500.AP21_CodRef AND " _
      & "AP2500.PR52NumMuestra = PR5200.PR52NumMuestra AND " _
      & "PR5200.PR24CodMuestra = PR2400.PR24CodMuestra AND " _
      & "AP2100.CI22NumHistoria = ? " _
      & "ORDER BY AP2100.AP21_CodRef DESC, AP2500.PR52NumIDMuestra ASC"
  Set rdoQPr = objApp.rdoConnect.CreateQuery("", sql)
  rdoQPr(0) = NH
  Set rdoPr = rdoQPr.OpenResultset(rdOpenForwardOnly)
  While rdoPr.EOF = False
    If nPr <> rdoPr(0) Then
      If rdoPr(3) < apPRUEBACERRADA Then Estado = 0 Else Estado = 10
      j = j + 1
      ReDim Preserve pr(1 To j)
      With pr(j)
        .nRef = rdoPr(0)
        .nPr = rdoPr(4)
        .FSotud = rdoPr(5)
        .fRecep = rdoPr(6)
      End With
      Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "P" & rdoPr(0), Right(rdoPr(0), 8) & ". Prueba: " & rdoPr(1) & " (" & rdoPr(2) & ") Solicitud: " & rdoPr(6), apICONPRUEBA, apICONPRUEBA)
      nPr = rdoPr(0)
    End If
    Set nodo = tvwDatos.Nodes.Add("P" & nPr, tvwChild, "M" & rdoPr(7) & "P" & nPr, rdoPr(8) & ". " & rdoPr(9), apICONMUESTRA, apICONMUESTRA)
    sql = "SELECT AP3900.AP39_CodDiag, AP3700.AP37_NumSno, AP3900.AP39_Descrip, " _
        & "AP3700.AP37_Descrip, AP3900.AP39_Observ, AP3700.AP50_CODTISNO, " _
        & "TO_CHAR(AP3900.AP39_FECDIAG,'DD/MM/YY HH24:MI'), AP3900.AP49_CODESTDIAG, " _
        & "AP3900.AP39_INDINTRA, AP3900.AP39_INDInfor, " _
        & "TO_CHAR(AP3900.AP39_FECInfor,'DD/MM/YY HH24:MI') " _
        & "FROM AP3900, AP3700 WHERE " _
        & "AP3900.AP37_CodSno = AP3700.AP37_CodSno AND " _
        & "AP3900.AP49_CodEstDiag < 4 AND " _
        & "AP3900.AP21_CodRef = ? AND " _
        & "AP3900.PR52NumIDMuestra = ? " _
        & "ORDER BY AP3900.AP39_FECDIAG"
    Set rdoQDiag = objApp.rdoConnect.CreateQuery("", sql)
    rdoQDiag(0) = rdoPr(0)
    rdoQDiag(1) = rdoPr(8)
    Set rdoDiag = rdoQDiag.OpenResultset(rdOpenForwardOnly)
    While rdoDiag.EOF = False
      i = i + 1
      ReDim Preserve Diag(1 To i)
      With Diag(i)
        If IsNull(rdoDiag(4)) Then .Observ = "" Else .Observ = rdoDiag(4)
        .Numero = rdoDiag(0)
        .Tipo = rdoDiag(5)
        .fecha = rdoDiag(6)
        .Estado = rdoDiag(7)
        .intra = rdoDiag(8)
        .Infor = rdoDiag(9)
        If IsNull(rdoDiag(10)) Then .FInfor = "" Else .FInfor = rdoDiag(10)
      End With
      texto = rdoDiag(5) & "-" & rdoDiag(1) & ". " & rdoDiag(3)
      If Not IsNull(rdoDiag(2)) Then texto = texto & " (" & rdoDiag(2) & ")"
      Set nodo = tvwDatos.Nodes.Add("M" & rdoPr(7) & "P" & nPr, tvwChild, "D" & rdoDiag(0) & "T" & Estado + rdoDiag(7), texto, apICONDIAG, apICONDIAG)
      rdoDiag.MoveNext
    Wend
    rdoPr.MoveNext
    rdoDiag.Close
  Wend
'  If tvwDatos.Nodes("Raiz").Children > 0 Then
'    tvwDatos.Nodes(nodo.index).EnsureVisible
'  End If

End Sub

Sub LlenarFotos(padre As String)
Dim sql As String
Dim nodo As Node
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim donde As String, texto As String
  
  sql = "SELECT AP3800.AP38_CodFoto, AP3800.AP38_Observ, " _
      & "AP3800.AP38_Lugar, PR2400.PR24DesCorta||'. '||PR5200.PR52DesMuestra, " _
      & "AP3800.AP38_FecReali, AP3800.AP38_Carrete, AP3800.AP38_Pos " _
      & "FROM AP3800, AP2500, PR5200, PR2400 WHERE " _
      & "AP2500.AP21_CodRef = AP3800.AP21_CodRef AND " _
      & "AP2500.PR52NumIDMuestra = AP3800.PR52NumIDMuestra AND " _
      & "AP2500.PR52NumMuestra = PR5200.PR52NumMuestra AND " _
      & "PR5200.PR24CodMuestra = PR2400.PR24CodMuestra AND " _
      & "AP3800.AP21_CodRef = ? AND AP3800.AP38_TiFoto = ? ORDER BY  AP3800.AP38_CodFoto"
  
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = apFOTO_MACRO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    Set nodo = tvwDatos.Nodes.Add(padre, tvwChild, "Macro" & nRef, "Macroscópicas", apICONFOTOSMACRO, apICONFOTOSMACRO)
    While rdo.EOF = False
      i = i + 1
      ReDim Preserve lugar(1 To 4, 1 To i)
      If IsNull(rdo(2)) Then lugar(1, i) = "" Else lugar(1, i) = rdo(2)
      lugar(2, i) = rdo(4)
      If IsNull(rdo(5)) Then lugar(3, i) = "" Else lugar(3, i) = rdo(5)
      If IsNull(rdo(6)) Then lugar(4, i) = "" Else lugar(4, i) = rdo(6)
      texto = rdo(0) & ".- " & rdo(3) & ". " & rdo(1)
      Set nodo = tvwDatos.Nodes.Add("Macro" & nRef, tvwChild, "F" & i, texto, apICONFOTOMACRO, apICONFOTOMACRO)
      rdo.MoveNext
    Wend
  End If
  sql = "SELECT AP3800.AP38_CodFoto, AP3800.AP38_Observ, " _
      & "AP3800.AP38_Lugar, Ap1200.AP12_Desig, AP3800.AP38_FecReali, AP3800.AP38_Carrete, " _
      & "AP3800.AP38_Pos " _
      & "FROM AP3800, AP1200, AP3600 WHERE " _
      & "AP3600.AP21_CodRef = AP3800.AP21_CodRef AND " _
      & "AP3600.AP34_CodBloq = AP3800.AP34_CodBloq AND " _
      & "AP3600.AP36_CodPorta = AP3800.AP36_CodPorta AND " _
      & "AP3600.AP12_CodTec = AP1200.AP12_CodTec AND " _
      & "AP3800.AP21_CodRef = ? AND AP3800.AP38_TiFoto = ? ORDER BY AP3800.AP38_CodFoto"
  
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = apFOTO_MICRO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    Set nodo = tvwDatos.Nodes.Add(padre, tvwChild, "Micro" & nRef, "Microscópicas", apICONFOTOMICRO, apICONFOTOMICRO)
    While rdo.EOF = False
      i = i + 1
      ReDim Preserve lugar(1 To 4, 1 To i)
      If IsNull(rdo(2)) Then lugar(1, i) = "" Else lugar(1, i) = rdo(2)
      lugar(2, i) = rdo(4)
      If IsNull(rdo(5)) Then lugar(3, i) = "" Else lugar(3, i) = rdo(5)
      If IsNull(rdo(6)) Then lugar(4, i) = "" Else lugar(4, i) = rdo(6)
      texto = rdo(0) & ".- " & rdo(3) & ". " & rdo(1)
      Set nodo = tvwDatos.Nodes.Add("Micro" & nRef, tvwChild, "F" & i, texto, apICONFOTOMICRO, apICONFOTOMICRO)
      rdo.MoveNext
    Wend
  End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
  Erase lugar
  Erase Diag
End Sub
Sub LlenarPat()
Dim sql As String
Dim rdo As rdoResultset
Dim item As ListItem

  With lvwPat
    .ColumnHeaders.Add , , "Patólogo", 900
    .ColumnHeaders.Add , , "Confirmado", 1100
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
  sql = "SELECT AP0800.SG02Cod, SG0200.SG02TxtFirma FROM AP0800, SG0200 WHERE " _
      & "AP0800.SG02Cod = SG0200.SG02Cod AND " _
      & "AP0800.AP07_CodTiPers = " & apCODPATOLOGO _
      & "ORDER BY SG0200.SG02TxtFirma"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwPat.ListItems.Add(, "P" & rdo(0), rdo(1))
    rdo.MoveNext
  Wend
End Sub


Sub VaciarDatos()
Dim i As Integer
  
  chkIntra.Value = 0
  
'  For i = 0 To 3
'    optDiag(i).Value = False
'  Next i
  txtEstado.Text = ""
  txtLugar.Text = ""
  txtFecha.Text = ""
  chkInfor.Value = vbUnchecked
  txtFInfor.Text = ""
  Call DeseleccionarPat
End Sub
Sub SeleccionarPat(num As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer

  On Error Resume Next
  Erase PatSelec
  sql = "SELECT AP40_CodDr, TO_CHAR(AP40_FecConf,'DD/MM/YY HH24:MI') FROM AP4000 WHERE AP39_CodDiag = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = num
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  i = 0
  While rdo.EOF = False
    i = i + 1
    ReDim Preserve PatSelec(1 To i)
    PatSelec(i) = rdo(0)
    Set lvwPat.SelectedItem = lvwPat.ListItems("P" & rdo(0))
    lvwPat.ListItems("P" & rdo(0)).SubItems(1) = rdo(1)
    rdo.MoveNext
  Wend
End Sub
Sub DeseleccionarPat()
Dim item As ListItem

  For Each item In lvwPat.ListItems
    item.Selected = False
    item.SubItems(1) = ""
  Next item
End Sub


Private Sub lvwPat_Click()
  Call MantenerPat
End Sub

Private Sub lvwPat_ItemClick(ByVal item As ComctlLib.ListItem)
  Call MantenerPat
End Sub
Sub MantenerPat()
Dim i As Integer, dimension As Integer
  
  On Error Resume Next
  dimension = UBound(PatSelec)
  For i = 1 To dimension
    lvwPat.ListItems("P" & PatSelec(i)).Selected = True
  Next i
End Sub

Private Sub tvwDatos_NodeClick(ByVal Node As ComctlLib.Node)
Dim clave As String
Dim num As Long, Estado As Integer
Dim i As Integer

  clave = Node.key
'  clave = tvwDatos.SelectedItem.key
  
  cDiag = 0
  Select Case pantalla
    Case "Diag"
      If Left(clave, 1) = "D" Then
        num = Val(Right(clave, Len(clave) - 1))
        cDiag = num
        Estado = Right(clave, Len(clave) - InStr(clave, "T"))
        posicion = 1
        While num <> Diag(posicion).Numero
          posicion = posicion + 1
        Wend
        With Diag(posicion)
          txtLugar.Text = .Observ
          txtFecha.Text = .fecha
          txtFInfor.Text = .FInfor
          chkIntra.Value = Abs(.intra)
          chkInfor.Value = Abs(.Infor)
          txtEstado.Text = DescripEstadoDiag(.Estado)
          
          If Estado > 10 Then
            cmdInfor.Enabled = False
            If SuperUser() = False Then cmdAnular.Enabled = False Else cmdAnular.Enabled = True
            cmdGuardar.Enabled = False
          Else
            Select Case .Estado
              Case apDIAGNOCONFIRMADO
                lvwPat.Enabled = False
                cmdInfor.Enabled = False
                cmdAnular.Enabled = True
              Case apDIAGPROVISIONAL
                lvwPat.Enabled = True
                cmdInfor.Enabled = True
                cmdAnular.Enabled = True
              Case apDIAGDEFINITIVO
                lvwPat.Enabled = True
                cmdInfor.Enabled = True
                If SuperUser() = False Then cmdAnular.Enabled = False Else cmdAnular.Enabled = True
            End Select
          End If
          Call DeseleccionarPat
          Call SeleccionarPat(.Numero)
        End With
      Else
        Call VaciarDatos
      End If
    Case "Fotos"
      If Left(clave, 1) = "F" Then
        posicion = Val(Right(clave, Len(clave) - 1))
        txtLugar.Text = lugar(1, posicion)
        txtFecha.Text = lugar(2, posicion)
        txtCarrete.Text = lugar(3, posicion)
        txtPos.Text = lugar(4, posicion)
      Else
        txtLugar.Text = ""
        txtFecha.Text = ""
        txtCarrete.Text = ""
        txtPos.Text = ""
      End If
  End Select
End Sub

Function DescripEstadoDiag(Estado As Integer) As String
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP49_Desig FROM AP4900 WHERE AP49_CodEstDiag = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = Estado
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  DescripEstadoDiag = rdo(0)
End Function
