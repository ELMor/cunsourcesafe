VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form frmI_C_Diagnosticos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Correlaci�n entre Diagn�sticos"
   ClientHeight    =   8055
   ClientLeft      =   1125
   ClientTop       =   1575
   ClientWidth     =   11415
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8055
   ScaleWidth      =   11415
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   405
      Left            =   10050
      TabIndex        =   19
      Top             =   750
      Width           =   1215
   End
   Begin Threed.SSFrame SSFrame2 
      Height          =   2115
      Left            =   150
      TabIndex        =   4
      Top             =   0
      Width           =   7515
      _Version        =   65536
      _ExtentX        =   13256
      _ExtentY        =   3731
      _StockProps     =   14
      Caption         =   "Opciones"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.Frame Frame2 
         Caption         =   "Comparar con las pruebas anteriores"
         ForeColor       =   &H00800000&
         Height          =   1590
         Left            =   4200
         TabIndex        =   13
         Top             =   300
         Width           =   3015
         Begin VB.CheckBox chkTiPr2 
            Caption         =   "Improntas"
            Height          =   240
            Index           =   2
            Left            =   225
            TabIndex        =   18
            Top             =   900
            Width           =   2265
         End
         Begin VB.TextBox txtDias 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "GC02_CODIGO"
            Height          =   285
            Left            =   1800
            TabIndex        =   16
            Text            =   "30"
            Top             =   1200
            Width           =   495
         End
         Begin VB.CheckBox chkTiPr2 
            Caption         =   "Biopsias"
            Height          =   240
            Index           =   0
            Left            =   225
            TabIndex        =   15
            Top             =   300
            Width           =   1890
         End
         Begin VB.CheckBox chkTiPr2 
            Caption         =   "Citolog�as (no improntas)"
            Height          =   240
            Index           =   1
            Left            =   225
            TabIndex        =   14
            Top             =   600
            Width           =   2265
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D�as entre pruebas:"
            Height          =   255
            Index           =   7
            Left            =   225
            TabIndex        =   17
            Top             =   1200
            Width           =   1605
         End
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   1590
         Left            =   225
         TabIndex        =   5
         Top             =   300
         Width           =   3765
         _Version        =   65536
         _ExtentX        =   6641
         _ExtentY        =   2805
         _StockProps     =   14
         Caption         =   "Buscar"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.CheckBox chkTiPr1 
            Caption         =   "Biopsias"
            Height          =   240
            Index           =   0
            Left            =   225
            TabIndex        =   8
            Top             =   300
            Width           =   1365
         End
         Begin VB.CheckBox chkTiPr1 
            Caption         =   "Citolog�as"
            Height          =   240
            Index           =   1
            Left            =   225
            TabIndex        =   7
            Top             =   750
            Width           =   1290
         End
         Begin VB.CheckBox chkTiPr1 
            Caption         =   "Necropsias"
            Height          =   240
            Index           =   2
            Left            =   225
            TabIndex        =   6
            Top             =   1200
            Width           =   1215
         End
         Begin SSCalendarWidgets_A.SSDateCombo cbossFecha 
            Height          =   330
            Index           =   0
            Left            =   1875
            TabIndex        =   9
            Top             =   525
            Width           =   1485
            _Version        =   65537
            _ExtentX        =   2619
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YY"
            Mask            =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo cbossFecha 
            Height          =   330
            Index           =   1
            Left            =   1875
            TabIndex        =   10
            Top             =   1125
            Width           =   1485
            _Version        =   65537
            _ExtentX        =   2619
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Desde:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   1950
            TabIndex        =   12
            Top             =   225
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hasta:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   1950
            TabIndex        =   11
            Top             =   900
            Width           =   780
         End
      End
   End
   Begin VB.CheckBox chkMuestras 
      Caption         =   "Incluir las muestras"
      Height          =   315
      Left            =   7800
      TabIndex        =   3
      Top             =   225
      Width           =   2190
   End
   Begin ComctlLib.TreeView tvwDatos 
      Height          =   5715
      Left            =   150
      TabIndex        =   2
      Top             =   2250
      Width           =   11190
      _ExtentX        =   19738
      _ExtentY        =   10081
      _Version        =   327682
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   405
      Left            =   10050
      TabIndex        =   1
      Top             =   225
      Width           =   1215
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   405
      Left            =   10050
      TabIndex        =   0
      Top             =   1725
      Width           =   1215
   End
   Begin VB.Menu mnuDiscrep 
      Caption         =   "Discrep"
      Visible         =   0   'False
      Begin VB.Menu mnuDiscrepOpcion 
         Caption         =   "No hay discrepancia"
         Index           =   0
      End
      Begin VB.Menu mnuDiscrepOpcion 
         Caption         =   "Discrepancia menor"
         Index           =   10
      End
      Begin VB.Menu mnuDiscrepOpcion 
         Caption         =   "Discrepancia mayor"
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmI_C_Diagnosticos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdBuscar_Click()
  
  If (chkTiPr1(0).Value = vbUnchecked And chkTiPr1(1).Value = vbUnchecked And chkTiPr1(2).Value = vbUnchecked) Or _
      (chkTiPr2(0).Value = vbUnchecked And chkTiPr2(1).Value = vbUnchecked And chkTiPr2(2).Value = vbUnchecked) Then
    MsgBox "Debe seleccionar alg�n tipo de prueba.", vbOKOnly + vbInformation, "Selecci�n de pruebas"
  Else
    Screen.MousePointer = 11
    tvwDatos.Nodes.Clear
    tvwDatos.Nodes.Add , , "Raiz", "Pruebas", apICONRAIZ, apICONRAIZ
    If chkMuestras.Value = vbChecked Then Call BuscarMues Else Call Buscar
    Screen.MousePointer = 0
  End If
End Sub
Sub Buscar()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItems
Dim ref As String, texto As String
Dim nodo As Node

  sql = "SELECT Pr1.AP21_CodRef, Pr1.cPr, Pr1.AP21_FecGen, Pr2.AP21_CodRef, Pr2.cPr, " _
      & "Pr2.AP21_FecGen, Pac.AP1||' '||Pac.AP2||', '||Pac.Nom, Pr1.AP21_CodHist, " _
      & "AP6200.AP21_CodRef_Padre, AP6200.AP63_CodDiscrep " _
      & "FROM AP2100 Pr1, AP2100 Pr2, Pac, AP6200 WHERE " _
      & "Pr1.AP21_CodHist = Pac.NH AND " _
      & "Pr1.AP21_CodHist = Pr2.AP21_CodHist AND " _
      & "Pr1.AP21_CodRef <> Pr2.AP21_CodRef AND " _
      & "Pr2.AP21_CodRef = AP6200.AP21_CodRef_Discrep (+) AND " _
      & "Pr1.AP21_TiPr IN " & TiPr1 & " AND " _
      & "Pr1.AP21_FecGen BETWEEN TO_DATE(?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY') AND "
  If chkTiPr2(1).Value = vbChecked Then
    sql = sql & "Pr2.AP21_TiPr IN " & TiPr2 & " AND "
    If chkTiPr2(2).Value = vbUnchecked Then
      sql = sql & "Pr2.AP09_CodTiPr <> " & apCODTIPRIMPRONTA & " AND "
    End If
  Else
    If chkTiPr2(2).Value = vbChecked Then
      sql = sql & "Pr2.AP09_CodTiPr = " & apCODTIPRIMPRONTA & " AND "
    Else
      sql = sql & "Pr2.AP21_TiPr IN " & TiPr2 & " AND "
    End If
  End If
  sql = sql & "Pr2.AP21_FecGen BETWEEN Pr1.AP21_FecGen - ? AND Pr1.AP21_FecGen " _
      & "ORDER BY Pr1.AP21_FecGen ASC, Pr2.AP21_FecGen DESC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = Format$(cbossFecha(0).Date, "DD/MM/YYYY")
  rdoQ(1) = Format$(DateAdd("d", 1, cbossFecha(1).Date), "DD/MM/YYYY")
  rdoQ(2) = txtDias.Text
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  ref = ""
  While rdo.EOF = False
    If ref <> rdo(0) Then
      texto = Right(rdo(0), 8) & " (" & rdo(2) & "). " & rdo(6)
      Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "P" & rdo(0), texto, apICONPRUEBA, apICONPRUEBA)
      nodo.Tag = rdo(7)
      ref = rdo(0)
      Set nodo = tvwDatos.Nodes.Add("P" & ref, tvwChild, "D" & rdo(0), "Diagn�sticos", apICONDIAGS, apICONDIAGS)
      Call LlenarDiag(nodo, ref)
    End If
    texto = Right(rdo(3), 8) & " (" & rdo(5) & ")"
    Set nodo = tvwDatos.Nodes.Add("P" & ref, tvwChild, "S" & rdo(3) & ref, texto, apICONPRUEBA, apICONPRUEBA)
    If Not IsNull(rdo(8)) Then
      nodo.Tag = rdo(8)
      Call CambiarIcons(rdo(0), rdo(8), rdo(9), nodo)
    End If
    Call LlenarDiag(nodo, rdo(3))
    rdo.MoveNext
  Wend
  If tvwDatos.Nodes.Count > 1 Then tvwDatos.Nodes(tvwDatos.Nodes(1).Child.Key).EnsureVisible
End Sub

Sub CambiarIcons(nRefPadre As String, nRef As String, cDiscrep As Integer, nodo As Node)
      
  Select Case cDiscrep
    Case apNODISCREP
      nodo.Image = apICONNODISCREP
      nodo.SelectedImage = apICONNODISCREP
' Se compara el nodo padre con el que ha originado la discrepancia
      If nRefPadre = nRef And nodo.Parent.Image <> apICONDISCREPMAYOR And nodo.Parent.Image <> apICONDISCREPMENOR Then
        nodo.Parent.Image = apICONNODISCREP
        nodo.Parent.SelectedImage = apICONNODISCREP
      End If
    Case apDISCREPMENOR
      nodo.Image = apICONDISCREPMENOR
      nodo.SelectedImage = apICONDISCREPMENOR
' Se compara el nodo padre con el que ha originado la discrepancia
      If nRefPadre = nRef And nodo.Parent.Image <> apICONDISCREPMAYOR Then
        nodo.Parent.Image = apICONDISCREPMENOR
        nodo.Parent.SelectedImage = apICONDISCREPMENOR
      End If
    Case apDISCREPMAYOR
      nodo.Image = apICONDISCREPMAYOR
      nodo.SelectedImage = apICONDISCREPMAYOR
      nodo.Parent.Image = apICONDISCREPMAYOR
      nodo.Parent.SelectedImage = apICONDISCREPMAYOR
  End Select

End Sub
Sub BuscarMues()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItems
Dim ref As String, texto As String
Dim nodo As Node

  sql = "SELECT Pr1.AP21_CodRef, Pr1.cPr, Pr1.AP21_FecGen, Pr2.AP21_CodRef, Pr2.cPr, " _
      & "Pr2.AP21_FecGen, Pac.AP1||' '||Pac.AP2||', '||Pac.Nom, Pr1.ap21_CodHist, " _
      & "AP6200.AP21_CodRef_Padre, AP6200.AP63_CodDiscrep " _
      & "FROM AP2100 Pr1, AP2100 Pr2, Pac, AP6200 WHERE " _
      & "Pr1.AP21_CodHist = Pac.NH AND " _
      & "Pr1.AP21_CodHist = Pr2.AP21_CodHist AND " _
      & "Pr1.AP21_CodRef <> Pr2.AP21_CodRef AND " _
      & "Pr2.AP21_CodRef = AP6200.AP21_CodRef_Discrep (+) AND " _
      & "Pr1.AP21_TiPr IN " & TiPr1 & " AND " _
      & "Pr1.AP21_FecGen BETWEEN TO_DATE(?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY') AND "
  If chkTiPr2(1).Value = vbChecked Then
    sql = sql & "Pr2.AP21_TiPr IN " & TiPr2 & " AND "
    If chkTiPr2(2).Value = vbUnchecked Then
      sql = sql & "Pr2.AP09_CodTiPr <> " & apCODTIPRIMPRONTA & " AND "
    End If
  Else
    If chkTiPr2(2).Value = vbChecked Then
      sql = sql & "Pr2.AP09_CodTiPr = " & apCODTIPRIMPRONTA & " AND "
    Else
      sql = sql & "Pr2.AP21_TiPr IN " & TiPr2 & " AND "
    End If
  End If
  sql = sql & "Pr2.AP21_FecGen BETWEEN Pr1.AP21_FecGen - ? AND Pr1.AP21_FecGen " _
      & "ORDER BY Pr1.AP21_FecGen ASC, Pr2.AP21_FecGen DESC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = Format$(cbossFecha(0).Date, "DD/MM/YYYY")
  rdoQ(1) = Format$(DateAdd("d", 1, cbossFecha(1).Date), "DD/MM/YYYY")
  rdoQ(2) = txtDias.Text
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  ref = ""
  While rdo.EOF = False
    If ref <> rdo(0) Then
      texto = Right(rdo(0), 8) & " (" & rdo(2) & "). " & rdo(6)
      Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "P" & rdo(0), texto, apICONPRUEBA, apICONPRUEBA)
      nodo.Tag = rdo(7)
      ref = rdo(0)
      Call LlenarMues(nodo, ref)
    End If
    texto = Right(rdo(3), 8) & " (" & rdo(5) & ")"
    Set nodo = tvwDatos.Nodes.Add("P" & ref, tvwChild, "S" & rdo(3) & ref, texto, apICONPRUEBA, apICONPRUEBA)
    Call LlenarMues(nodo, rdo(3))
    If Not IsNull(rdo(8)) Then
      nodo.Tag = rdo(8)
      Call CambiarIcons(rdo(0), rdo(8), rdo(9), nodo)
    End If
    rdo.MoveNext
  Wend
  If tvwDatos.Nodes.Count > 1 Then tvwDatos.Nodes(tvwDatos.Nodes(1).Child.Key).EnsureVisible
End Sub

Function TiPr1() As String
Dim texto As String
  
  texto = "("
  If chkTiPr1(0).Value = vbChecked Then texto = texto & "1, "
  If chkTiPr1(1).Value = vbChecked Then texto = texto & "2, "
  If chkTiPr1(2).Value = vbChecked Then texto = texto & "3, "
  TiPr1 = Left(texto, Len(texto) - 2) & ")"
End Function

Function TiPr2() As String
Dim texto As String
  
  texto = "("
  If chkTiPr2(0).Value = vbChecked Then texto = texto & "1, "
  If chkTiPr2(1).Value = vbChecked Then texto = texto & "2, "
  If chkTiPr2(2).Value = vbChecked Then texto = texto & "2, "
  TiPr2 = Left(texto, Len(texto) - 2) & ")"
End Function

Function TiPr1_text() As String
Dim texto As String
  
  If chkTiPr1(0).Value = vbChecked Then texto = "Biosias, "
  If chkTiPr1(1).Value = vbChecked Then texto = texto & "Citolog�as, "
  If chkTiPr1(2).Value = vbChecked Then texto = texto & "Necropsias, "
  TiPr1_text = Left(texto, Len(texto) - 2)
End Function

Function TiPr2_text() As String
Dim texto As String
  
  If chkTiPr2(0).Value = vbChecked Then texto = texto & "Biopsias, "
  If chkTiPr2(1).Value = vbChecked Then texto = texto & "Citolog�as (no improntas), "
  If chkTiPr2(2).Value = vbChecked Then texto = texto & "Improntas, "
  TiPr2_text = Left(texto, Len(texto) - 2)
End Function

Private Sub cmdImprimir_Click()
Dim sql As String
Dim texto As String
  
  If (chkTiPr1(0).Value = vbUnchecked And chkTiPr1(1).Value = vbUnchecked And chkTiPr1(2).Value = vbUnchecked) Or _
      (chkTiPr2(0).Value = vbUnchecked And chkTiPr2(1).Value = vbUnchecked And chkTiPr2(2).Value = vbUnchecked) Then
    MsgBox "Debe seleccionar alg�n tipo de prueba.", vbOKOnly + vbInformation, "Selecci�n de pruebas"
  Else
    Screen.MousePointer = 11
    sql = Chr$(13) & Chr$(9) & "AP2100.AP21_FecGen BETWEEN " _
        & "TO_DATE('" & Format(cbossFecha(0).Text, "DD/MM/YYYY") & "', 'DD/MM/YYYY') AND " _
        & "TO_DATE('" & Format(DateAdd("d", 1, cbossFecha(1).Text), "DD/MM/YYYY") & "', 'DD/MM/YYYY') AND " _
        & Chr$(13) & Chr$(9) & "AP2100.AP21_TiPr IN " & TiPr1 & " AND " & Chr$(13) & Chr$(9)
    If chkTiPr2(1).Value = vbChecked Then
      sql = sql & "Pr2.AP21_TiPr IN " & TiPr2 & " AND "
      If chkTiPr2(2).Value = vbUnchecked Then
        sql = sql & "Pr2.AP09_CodTiPr <> " & apCODTIPRIMPRONTA & " AND "
      End If
    Else
      If chkTiPr2(2).Value = vbChecked Then
        sql = sql & "Pr2.AP09_CodTiPr = " & apCODTIPRIMPRONTA & " AND "
      Else
        sql = sql & "Pr2.AP21_TiPr IN " & TiPr2 & " AND "
      End If
    End If
    sql = sql & Chr$(13) & Chr$(9) & "Pr2.AP21_FecGen BETWEEN AP2100.AP21_FecGen - " & txtDias.Text & " AND AP2100.AP21_FecGen   "
    
    texto = "'B�squeda: " & TiPr1_text & " entre " & cbossFecha(0).Text & " y " & cbossFecha(1).Text & "' + Chr(13) + " _
          & "'Comparar con : " & TiPr2_text & " con " & txtDias.Text & " d�as de diferencia'"
    If chkMuestras.Value = 1 Then
      Call Imprimir_API(sql, "AP3102.rpt", "FechasVB", texto, Me.Caption)
    Else
      Call Imprimir_API(sql, "AP3101.rpt", "FechasVB", texto, Me.Caption)
    End If
    Screen.MousePointer = 0
  End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
  Set tvwDatos.ImageList = frmPrincipal.imgIcos
  tvwDatos.Nodes.Add , , "Raiz", "Pruebas", apICONRAIZ
'  Call LlenarTiPr
End Sub
Sub LlenarTiPr()
Dim sql As String
Dim rdo As rdoResultset
Dim TiPr As Integer, i As Integer

'  Set lvwTiPr(0).SmallIcons = frmPrincipal.imgIcos
'  Set lvwTiPr(1).SmallIcons = frmPrincipal.imgIcos
'
'  lvwTiPr(0).ListItems.Add , "T1", "Biopsia", , apICONPRUEBA
'  lvwTiPr(1).ListItems.Add , "T1", "Biopsia", , apICONPRUEBA
'
'  lvwTiPr(0).ListItems.Add , "T2", "Citolog�a", , apICONPRUEBA
'  lvwTiPr(1).ListItems.Add , "T2", "Citolog�a", , apICONPRUEBA
'
'  lvwTiPr(0).ListItems.Add , "T3", "Necropsia", , apICONPRUEBA
'

'  sql = "SELECT AP09_TiPr, PR_Descrip, AP09_CodTiPr, AP09_Descrip FROM AP0900 ORDER BY AP09_TiPr"
'  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
'  TiPr = 0
'  While rdo.EOF = False
'    If TiPr <> rdo(0) Then
'      tvwTiPr(0).Nodes.Add "Raiz", tvwChild, "T" & rdo(0), rdo(1), apICONPRUEBA, apICONPRUEBA
'      tvwTiPr(1).Nodes.Add "Raiz", tvwChild, "T" & rdo(0), rdo(1), apICONPRUEBA, apICONPRUEBA
'      TiPr = rdo(0)
'    End If
'    tvwTiPr(0).Nodes.Add "T" & rdo(0), tvwChild, "P" & rdo(2), rdo(3), apICONPRUEBA, apICONPRUEBA
'    tvwTiPr(1).Nodes.Add "T" & rdo(0), tvwChild, "P" & rdo(2), rdo(3), apICONPRUEBA, apICONPRUEBA
'    rdo.MoveNext
'  Wend

End Sub

Private Sub mnuDiscrepOpcion_Click(Index As Integer)
  
  If mnuDiscrepOpcion(Index).Checked = True Then
    mnuDiscrepOpcion(Index).Checked = False
  Else
    mnuDiscrepOpcion(0).Checked = False
    mnuDiscrepOpcion(10).Checked = False
    mnuDiscrepOpcion(20).Checked = False
    mnuDiscrepOpcion(Index).Checked = True
  End If
  
  Call BorrarDiscrep(tvwDatos.SelectedItem)
  
  If mnuDiscrepOpcion(Index).Checked = True Then
    Call LlenarDiscrep(Index, tvwDatos.SelectedItem)
  Else
    tvwDatos.SelectedItem.Image = apICONPRUEBA
    tvwDatos.SelectedItem.SelectedImage = apICONPRUEBA
  End If
End Sub

Sub BorrarDiscrep(nodo As Node)
Dim sql As String
Dim rdoQ As rdoQuery
Dim nRef As String

  nRef = Mid(nodo.Key, 2, 10)
  sql = "DELETE FROM AP6200 WHERE AP21_CodRef_Discrep = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ.Execute
End Sub

Sub LlenarDiscrep(Tipo As Integer, nodo As Node)
Dim sql As String
Dim rdoQ As rdoQuery
Dim nRefPadre As String, nRef As String
Dim i As Integer
Dim nodo1 As Node

  nRef = Mid(nodo.Key, 2, 10)
  nRefPadre = Mid(nodo.Parent.Key, 2, 10)

  sql = "INSERT INTO AP6200 (AP21_CodRef_Discrep, AP21_CodRef_Padre, AP63_CodDiscrep) " _
      & "VALUES (?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = nRefPadre
  Select Case Tipo
    Case 0  ' No hay discrepancias
      rdoQ(2) = apNODISCREP
    Case 10 ' Discrepancia menor
      rdoQ(2) = apDISCREPMENOR
    Case 20 ' Discrepancia mayor
      rdoQ(2) = apDISCREPMAYOR
  End Select
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then
    nodo.Tag = nRefPadre
    Select Case Tipo
      Case 0  ' No hay discrepancias
        nodo.Image = apICONNODISCREP
        nodo.SelectedImage = apICONNODISCREP
        nodo.Parent.Image = apICONNODISCREP
        nodo.Parent.SelectedImage = apICONNODISCREP
      Case 10 ' Discrepancia menor
        nodo.Image = apICONDISCREPMENOR
        nodo.SelectedImage = apICONDISCREPMENOR
        nodo.Parent.Image = apICONDISCREPMENOR
        nodo.Parent.SelectedImage = apICONDISCREPMENOR
      Case 20 ' Discrepancia mayor
        nodo.Image = apICONDISCREPMAYOR
        nodo.SelectedImage = apICONDISCREPMAYOR
        nodo.Parent.Image = apICONDISCREPMAYOR
        nodo.Parent.SelectedImage = apICONDISCREPMAYOR
    End Select
  End If
End Sub

Private Sub tvwDatos_DblClick()
Dim nodo As Node
  
  Set nodo = tvwDatos.SelectedItem
  If Left(nodo.Key, 1) = "P" Or Left(nodo.Key, 1) = "S" Then
    Call objPipe.PipeSet("Boton", "Pr")
    frmPrAnteriores.Show vbModal
  End If
End Sub

Private Sub tvwDatos_KeyDown(KeyCode As Integer, Shift As Integer)
Dim nodo As Node
  
  If KeyCode = 46 Then    ' Tecla Suprimir
    Set nodo = tvwDatos.SelectedItem
    If Left(nodo.Key, 1) = "P" Or Left(nodo.Key, 1) = "S" Then
      tvwDatos.Nodes.Remove nodo.Key
    End If
  End If
End Sub

Sub LlenarDiag(nodo As Node, nRef As String, Optional nMues As Variant)
Dim sql As String, texto As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
      
  sql = "SELECT AP3900.AP39_CodDiag, AP3700.AP50_CodTiSno||'-'||AP3700.AP37_NumSno, " _
      & "AP3700.AP37_Descrip, AP3900.AP39_Descrip, AP3900.AP39_Observ FROM " _
      & "AP3700, AP3900 WHERE " _
      & "AP3700.AP37_CodSno = AP3900.AP37_CodSno AND " _
      & "AP3900.AP21_CodRef = ?"
  If Not IsMissing(nMues) Then sql = sql & " AND AP3900.AP25_CodMues = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(0) = nRef
  If Not IsMissing(nMues) Then rdoQ(1) = nMues
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    texto = rdo(1) & ". " & rdo(2) & ". " & rdo(3)
    If Not IsNull(rdo(4)) Then texto = texto & "(" & rdo(4) & ")"
    tvwDatos.Nodes.Add nodo.Key, tvwChild, "T" & rdo(0) & nodo.Key, texto, apICONDIAG, apICONDIAG
    rdo.MoveNext
  Wend
  
End Sub

Sub LlenarMues(nodo As Node, nRef As String)
Dim sql As String, texto As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo1 As Node
      
  sql = "SELECT AP2500.AP25_CodMues, AP2300.AP23_Desig||'. '||AP25_TiMues " _
      & "FROM AP2500, AP2300 WHERE " _
      & "AP2500.AP23_CodOrg = AP2300.AP23_CodOrg AND " _
      & "AP2500.AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    texto = rdo(0) & ".- " & rdo(1)
    Set nodo1 = tvwDatos.Nodes.Add(nodo.Key, tvwChild, "M" & rdo(0) & nodo.Key, texto, apICONMUESTRA, apICONMUESTRA)
    Call LlenarDiag(nodo1, nRef, rdo(0))
    rdo.MoveNext
  Wend
  
End Sub

Private Sub tvwDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nRef As String
      
  If Button = vbRightButton Then
    If Left(tvwDatos.SelectedItem.Key, 1) = "S" Then
      nRef = tvwDatos.SelectedItem.Tag
      Select Case tvwDatos.SelectedItem.Image
        Case apICONPRUEBA
          mnuDiscrepOpcion(0).Checked = False
          mnuDiscrepOpcion(0).Caption = "No hay discrepancia"
          mnuDiscrepOpcion(10).Checked = False
          mnuDiscrepOpcion(10).Caption = "Discrepancia menor"
          mnuDiscrepOpcion(20).Checked = False
          mnuDiscrepOpcion(20).Caption = "Discrepancia mayor"
        Case apICONNODISCREP
          mnuDiscrepOpcion(0).Checked = True
          mnuDiscrepOpcion(0).Caption = "No hay discrepancia (" & nRef & ")"
          mnuDiscrepOpcion(10).Checked = False
          mnuDiscrepOpcion(10).Caption = "Discrepancia menor"
          mnuDiscrepOpcion(20).Checked = False
          mnuDiscrepOpcion(20).Caption = "Discrepancia mayor"
        Case apICONDISCREPMENOR
          mnuDiscrepOpcion(0).Checked = False
          mnuDiscrepOpcion(0).Caption = "No hay discrepancia"
          mnuDiscrepOpcion(10).Checked = True
          mnuDiscrepOpcion(10).Caption = "Discrepancia menor (" & nRef & ")"
          mnuDiscrepOpcion(20).Checked = False
          mnuDiscrepOpcion(20).Caption = "Discrepancia mayor"
        Case apICONDISCREPMAYOR
          mnuDiscrepOpcion(0).Checked = False
          mnuDiscrepOpcion(0).Caption = "No hay discrepancia"
          mnuDiscrepOpcion(10).Checked = False
          mnuDiscrepOpcion(10).Caption = "Discrepancia menor"
          mnuDiscrepOpcion(20).Checked = True
          mnuDiscrepOpcion(20).Caption = "Discrepancia mayor (" & nRef & ")"
      End Select
      Me.PopupMenu mnuDiscrep, vbPopupMenuLeftAlign, tvwDatos.Left + X, tvwDatos.Top + Y
    End If
  End If
End Sub
