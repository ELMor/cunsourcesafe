VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDiag 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Introducci�n de diagn�sticos"
   ClientHeight    =   8250
   ClientLeft      =   15
   ClientTop       =   300
   ClientWidth     =   11400
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8250
   ScaleWidth      =   11400
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAnt 
      Caption         =   "Muestra &Anterior"
      Height          =   390
      Left            =   9225
      TabIndex        =   37
      Top             =   5625
      Width           =   1590
   End
   Begin VB.CommandButton cmdSig 
      Caption         =   "Muestra &Siguiente"
      Height          =   390
      Left            =   9225
      TabIndex        =   36
      Top             =   5175
      Width           =   1590
   End
   Begin VB.CheckBox chkInfor 
      Caption         =   "Informar"
      Height          =   315
      Left            =   9225
      TabIndex        =   19
      Top             =   4500
      Width           =   1065
   End
   Begin VB.Frame fraOpciones 
      Caption         =   "Opciones"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   9150
      TabIndex        =   16
      Top             =   3150
      Width           =   2115
      Begin VB.CheckBox chkIntra 
         Caption         =   "Intraoperatoria"
         Height          =   315
         Left            =   150
         TabIndex        =   18
         Top             =   300
         Width           =   1365
      End
      Begin VB.CheckBox chkDiscrepancia 
         Caption         =   "Discrepancia"
         Height          =   315
         Left            =   150
         TabIndex        =   17
         Top             =   675
         Width           =   1290
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Estado de diagn�stico"
      ForeColor       =   &H00800000&
      Height          =   1365
      Left            =   6225
      TabIndex        =   12
      Top             =   150
      Width           =   2715
      Begin VB.OptionButton optDiag 
         Caption         =   "Option1"
         Height          =   240
         Index           =   0
         Left            =   150
         TabIndex        =   15
         Top             =   375
         Value           =   -1  'True
         Width           =   2340
      End
      Begin VB.OptionButton optDiag 
         Caption         =   "Option1"
         Height          =   240
         Index           =   2
         Left            =   150
         TabIndex        =   14
         Top             =   975
         Width           =   2415
      End
      Begin VB.OptionButton optDiag 
         Caption         =   "Option1"
         Height          =   240
         Index           =   1
         Left            =   150
         TabIndex        =   13
         Top             =   675
         Width           =   2415
      End
   End
   Begin VB.Frame fraBuscar 
      Caption         =   "Diagn�sticos SNOMED"
      ForeColor       =   &H00800000&
      Height          =   6540
      Index           =   1
      Left            =   300
      TabIndex        =   9
      Top             =   1650
      Width           =   8715
      Begin TabDlg.SSTab tabDiag 
         Height          =   4800
         Left            =   225
         TabIndex        =   32
         Top             =   1575
         Width           =   8295
         _ExtentX        =   14631
         _ExtentY        =   8467
         _Version        =   327681
         Style           =   1
         TabHeight       =   520
         TabCaption(0)   =   "Resultados de b�squeda"
         TabPicture(0)   =   "AP00751.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "tvwDiag"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Perfiles"
         TabPicture(1)   =   "AP00751.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "tvwPerfil"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "SNOMED Seleccionados"
         TabPicture(2)   =   "AP00751.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "grdssDiag"
         Tab(2).ControlCount=   1
         Begin ComctlLib.TreeView tvwDiag 
            Height          =   4215
            Left            =   150
            TabIndex        =   33
            Top             =   450
            Width           =   7965
            _ExtentX        =   14049
            _ExtentY        =   7435
            _Version        =   327682
            LabelEdit       =   1
            Style           =   7
            BorderStyle     =   1
            Appearance      =   1
         End
         Begin ComctlLib.TreeView tvwPerfil 
            Height          =   4140
            Left            =   -74850
            TabIndex        =   34
            Top             =   450
            Width           =   7965
            _ExtentX        =   14049
            _ExtentY        =   7303
            _Version        =   327682
            LabelEdit       =   1
            Style           =   7
            ImageList       =   "imgIcos"
            BorderStyle     =   1
            Appearance      =   1
         End
         Begin SSDataWidgets_B.SSDBGrid grdssDiag 
            Height          =   4140
            Left            =   -74775
            TabIndex        =   35
            Top             =   450
            Width           =   7935
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   4
            AllowDelete     =   -1  'True
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            SelectByCell    =   -1  'True
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   4
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cDiag"
            Columns(0).Name =   "cDiag"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6324
            Columns(1).Caption=   "Diagn�stico"
            Columns(1).Name =   "Diag"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   4577
            Columns(2).Caption=   "Descripci�n"
            Columns(2).Name =   "Descrip"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   4128
            Columns(3).Caption=   "Observaciones"
            Columns(3).Name =   "Observ"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            _ExtentX        =   13996
            _ExtentY        =   7302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Buscar"
         ForeColor       =   &H00800000&
         Height          =   1215
         Left            =   4200
         TabIndex        =   20
         Top             =   225
         Width           =   4365
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "&Buscar"
            Height          =   390
            Left            =   2775
            TabIndex        =   31
            Top             =   225
            Width           =   1365
         End
         Begin VB.TextBox txtDescrip 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1350
            TabIndex        =   22
            Top             =   675
            Width           =   2790
         End
         Begin VB.TextBox txtCodigo 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1350
            TabIndex        =   21
            Top             =   225
            Width           =   690
         End
         Begin VB.Label Label2 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   4
            Left            =   150
            TabIndex        =   24
            Top             =   750
            Width           =   1065
         End
         Begin VB.Label Label2 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   5
            Left            =   525
            TabIndex        =   23
            Top             =   225
            Width           =   690
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Tipo de diagn�stico"
         ForeColor       =   &H00800000&
         Height          =   1215
         Left            =   225
         TabIndex        =   10
         Top             =   225
         Width           =   3840
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   5
            Left            =   2025
            TabIndex        =   30
            Top             =   825
            Value           =   1  'Checked
            Width           =   1665
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   4
            Left            =   2025
            TabIndex        =   29
            Top             =   525
            Value           =   1  'Checked
            Width           =   1665
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   3
            Left            =   2025
            TabIndex        =   28
            Top             =   225
            Value           =   1  'Checked
            Width           =   1590
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   2
            Left            =   150
            TabIndex        =   27
            Top             =   825
            Value           =   1  'Checked
            Width           =   1815
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   1
            Left            =   150
            TabIndex        =   26
            Top             =   525
            Value           =   1  'Checked
            Width           =   1815
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   0
            Left            =   150
            TabIndex        =   25
            Top             =   225
            Value           =   1  'Checked
            Width           =   1815
         End
      End
   End
   Begin VB.Frame fraPr 
      Caption         =   "Diagn�stico asociado a:"
      ForeColor       =   &H00800000&
      Height          =   1440
      Left            =   300
      TabIndex        =   2
      Top             =   75
      Width           =   5790
      Begin VB.TextBox txtTecnica 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   975
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   975
         Width           =   4665
      End
      Begin VB.TextBox txtBloque 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   975
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   600
         Width           =   4665
      End
      Begin VB.TextBox txtMuestra 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   975
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   225
         Width           =   4665
      End
      Begin VB.Label Label2 
         Caption         =   "T�cnica:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   3
         Left            =   150
         TabIndex        =   8
         Top             =   1050
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Bloque:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   225
         TabIndex        =   7
         Top             =   675
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Muestra:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   150
         TabIndex        =   6
         Top             =   300
         Width           =   765
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   9225
      TabIndex        =   0
      Top             =   7275
      Width           =   1590
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   9225
      TabIndex        =   1
      Top             =   7800
      Width           =   1590
   End
   Begin ComctlLib.ListView lvwPat 
      Height          =   2715
      Left            =   9075
      TabIndex        =   11
      Top             =   225
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   4789
      View            =   2
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin ComctlLib.ImageList imgIcos 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   6
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00751.frx":0054
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00751.frx":036E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00751.frx":0688
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00751.frx":09A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00751.frx":0CBC
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AP00751.frx":0FD6
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmDiag"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public PwdCorrect As Integer
Option Explicit
Dim Tipo As String, cMuestra As Integer, cBloque As Integer, cTecnica As Integer
Dim donde As Integer, intra As Integer, seguir As Integer
Dim TiSno() As String, TiDiag(0 To 2) As Integer
Dim Pat() As String, nRef As String
Dim Mues() As DatosMues
Sub LlenarPerfil()
Dim rdo As rdoResultset
Dim sql As String
Dim rdoQ As rdoQuery
Dim perfil As Integer
Dim nodo As Node

  Set nodo = tvwPerfil.Nodes.Add(, , "Raiz", "Perfiles de diagn�sticos", apICONRAIZ, apICONRAIZ)
  sql = "SELECT AP5500.AP55_CodPerf, AP5500.AP55_Descrip, AP3700.AP37_CodSno, " _
      & "AP3700.AP50_CodTiSno, AP3700.AP37_NumSno, AP3700.AP37_Descrip " _
      & "FROM AP5500, AP5600, AP3700 WHERE " _
      & "AP5500.AP55_CodPerf = AP5600.AP55_CodPerf AND " _
      & "AP5600.AP37_CodSno = AP3700.AP37_CodSno " _
      & "ORDER BY AP55_Descrip"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
    
  perfil = 0
  While rdo.EOF = False
    If perfil <> rdo(0) Then
      Set nodo = tvwPerfil.Nodes.Add("Raiz", tvwChild, "P" & rdo(0), rdo(1), apICONPERFIL, apICONPERFIL)
    End If
    Set nodo = tvwPerfil.Nodes.Add("P" & rdo(0), tvwChild, "P" & rdo(0) & "D" & rdo(2), rdo(3) & "-" & rdo(4) & ". " & rdo(5), 4, 4)
    If rdo.EOF = False Then perfil = rdo(0)
    rdo.MoveNext
  Wend
  rdo.Close
  tvwPerfil.Nodes("Raiz").Child.EnsureVisible
End Sub

Function FaltanDiag(cDiag, nRef, fila As Integer) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim j As Integer

  sql = "SELECT AP5200.AP50_CodTiSno, AP5200.AP37_NumSno, AP3700.AP37_Descrip " _
      & "FROM AP5200, AP3700 WHERE " _
      & "AP5200.AP50_CodTiSno = AP3700.AP50_CodTiSno (+) AND " _
      & "AP5200.AP37_NumSno = AP3700.AP37_NumSno (+) AND " _
      & "AP3700.AP51_CodClaseSno = 1 AND " _
      & "AP5200.AP37_CodSno = ? AND AP5200.AP50_CodTiSno||AP5200.AP37_NumSno NOT IN " _
      & "(SELECT SUBSTR(AP3700.AP50_CodTiSno||AP3700.AP37_NumSno,1,LENGTH(AP5200.AP50_CodTiSno||AP5200.AP37_NumSno)) " _
      & "FROM AP3900, AP3700 WHERE " _
      & "AP3900.AP37_CodSno = AP3700.AP37_CodSno AND " _
      & "AP3900.AP21_CodRef = ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cDiag
  rdoQ(1) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    FaltanDiag = False
  Else
    While rdo.EOF = False
      FaltanDiag = True
      grdssDiag.MoveFirst
      grdssDiag.MoveRecords (fila + 1)
      For j = fila + 1 To grdssDiag.Rows - 1
        If InStr(grdssDiag.Columns(1).Text, rdo(0) & "-" & rdo(1)) > 0 Then
          FaltanDiag = False
          Exit For
        End If
        grdssDiag.MoveNext
      Next j
      If FaltanDiag = True Then
        Screen.MousePointer = 0
        MsgBox "Antes de introducir el diagn�stico " & Chr$(13) & Chr$(13) & "  '" _
               & grdssDiag.Columns("Diag").Text & "'" & Chr$(13) & Chr$(13) & _
               " es necesario introducir los diagn�sticos " & Chr$(13) & Chr$(13) & "   '" & rdo(0) & _
               "-" & rdo(1) & ". " & rdo(2) & "'", vbExclamation, "Introducci�n incorrecta"
      End If
      rdo.MoveNext
    Wend
  End If
  rdo.Close
End Function

Sub LlenarPat()
Dim sql As String
Dim rdo As rdoResultset
Dim item As ListItem
  
  sql = "SELECT AP0800.SG02Cod, SG0200.SG02TxtFirma FROM AP0800, SG0200 WHERE " _
      & "AP0800.SG02Cod = SG0200.SG02Cod AND " _
      & "AP0800.AP07_CodTiPers = " & apCODPATOLOGO _
      & "ORDER BY SG0200.SG02TxtFirma"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwPat.ListItems.Add(, "P" & rdo(0), rdo(1))
    rdo.MoveNext
  Wend
  rdo.Close

End Sub

Private Sub chkDiscrepancia_Click()
  If chkDiscrepancia.Value = 1 And chkIntra.Value = 1 Then
    MsgBox "No se puede introducir discrepancias en el diagn�stico de la intra, " _
    & "sino sobre el diagn�stico definitivo.", vbExclamation, "Introducci�n incorrecta"
    chkDiscrepancia.Value = 0
  End If
End Sub

Private Sub chkIntra_Click()
  If chkIntra.Value = 1 Then
    If chkDiscrepancia.Value = 1 Then
      MsgBox "No se puede introducir discrepancias en el diagn�stico de la intra, " _
      & "sino sobre el diagn�stico definitivo.", vbExclamation, "Introducci�n incorrecta"
      chkIntra.Value = 0
      Exit Sub
    End If
    If optDiag(2).Value = True Then optDiag(1).Value = 1
    optDiag(2).Enabled = False
  Else
    optDiag(2).Enabled = True
  End If
End Sub

Private Sub cmdAceptar_Click()
Dim texto As String, Diag As String, cDiag As Integer
Dim i As Integer, res As Integer, num As Long
  
'  On Error Resume Next
  Screen.MousePointer = 11
  seguir = False
  Err = 0
  If grdssDiag.Rows = 0 Then 'Si no se ha seleccionado ning�n diagn�stico
    Screen.MousePointer = 0
    MsgBox "Debe seleccionar alg�n diagn�stico.", vbInformation, "Selecci�n incorrecta"
    Exit Sub
  Else
    ' En el caso de intras, el diagn�stico se debe asociar a la muestra
    If chkIntra.Value = 1 And txtMuestra.Text = "" Then
      Screen.MousePointer = 0
      MsgBox "En el caso de pruebas intraoperatorias, el diagn�stico debe ser asociado a la muestra.", vbInformation, "Selecci�n incorrecta"
      Exit Sub
    End If
    If optDiag(0).Value = False Then ' Si van a confirmar un diagn�stico
      If optDiag(2).Value = True Then  ' Van a poner un diagn�stico definitivo
        If FaltanTecnicas(nRef) = True Then
          Screen.MousePointer = 0
          MsgBox "Faltan t�cnicas por realizar, por lo que los diagn�sticos no pueden ser definitivos.", vbExclamation, "Introducci�n de diagn�sticos"
          Exit Sub
        End If
        If chkIntra.Value = 1 Then
          Screen.MousePointer = 0
          MsgBox "No se puede introducir un diagn�stico definitivo en una muestra intraoperatoria.", vbExclamation, "Introducci�n de diagn�sticos"
          Exit Sub
        End If
        If HayDiagProvisionales(nRef) = True Then
          Screen.MousePointer = 0
          MsgBox "Esta prueba tiene diagn�sticos provisionales, por lo que no se pueden introducir diagn�sticos definitivos hasta confirmar los provisionales.", vbExclamation, "Introducci�n de diagn�sticos"
          Exit Sub
        End If
      Else  ' Van a poner un diagn�stico provisional
        If HayTecnicas(nRef) = False Then
          Screen.MousePointer = 0
          MsgBox "Es necesario que alguna t�cnica est� realizada para introducir diagn�sticos provisionales.", vbExclamation, "Introducci�n de diagn�sticos"
          Exit Sub
        End If
      End If
      Screen.MousePointer = 0
      seguir = False
'      Load frmIDDr
'      Call LlenarIDDr(Pat(), Me)
'      Screen.MousePointer = 11
'      Unload frmIDDr
      If Pat(1, 1) <> -1 Then
        If InsertDiag(nRef) = True Then
          If chkDiscrepancia.Value = 1 Then
            Screen.MousePointer = 0
            seguir = False
'            frmDiscrep.Show vbModal
          End If
          
          If chkIntra.Value = 1 Then
            Screen.MousePointer = 0
            res = MsgBox("�Desea imprimir el informe de biopsia intraoperatoria?", vbYesNo + vbQuestion, "Emisi�n de informe")
            If res = vbYes Then
              Call ImprimirInforme(nRef)
              Call ImprimirInforme(nRef)
            End If
          End If
        End If
      End If
    Else
      res = InsertDiag(nRef)
    End If
    Call CerrarrdoQueries
  End If
  Screen.MousePointer = 0
'  Unload Me

End Sub


Sub ImprimirInforme(nRef As String)

Dim sql As String
Dim rdoQ As rdoQuery
Dim rdoQ1 As rdoQuery
Dim cama As String
Dim doctor As String
Dim depart As String
Dim rdo1 As rdoResultset
Dim rdo As rdoResultset
Dim i As Integer, j As Integer
Dim posi As Long
Dim cMues As Integer, cDiag As Long, cDiags As String

Const interlinea1 = 500
Const interlinea2 = 300
Const margensuperior = 1300
Const margenizquierdo = 1000
  
  With Printer
    .PaperSize = vbPRPSA4
    .DrawWidth = 5
    
    Printer.Line (margenizquierdo - 100, margensuperior + interlinea1 - 130)-(11000, margensuperior + interlinea1 + interlinea2 * 3 + 250), , B
    Printer.Line (margenizquierdo - 100, margensuperior + interlinea2 * 3 + interlinea1 * 2 - 150)-(11000, margensuperior + interlinea2 * 6 + interlinea1 * 2 + 250), , B
    Printer.Line (margenizquierdo + 4950, margensuperior + interlinea2 * 3 + interlinea1 * 2 - 150)-(margenizquierdo + 4950, margensuperior + interlinea2 * 6 + interlinea1 * 2 + 250)
    .DrawWidth = 5
    .FontName = 1
    .FontSize = 15
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = margensuperior
    Printer.Print "Informe de biopsia intraoperatoria"
    
    .FontSize = 12
    posi = margensuperior + interlinea1
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "N� Ref: " & Val(Right(nRef, 5))
    .FontSize = 9
'    Call ImprimirNegrita(margenizquierdo + 3300, posi, "Historia/Caso: ", frmP_Peticion.NH & "/" & frmP_Peticion.NC)
    
    sql = "SELECT AD1500.AD15CodCama FROM " _
        & "AD1500, PR0400 WHERE " _
        & "PR0400.AD01CodAsistenci = AD0100.AD01CodAsistenci AND " _
        & "PR0400.PR04NumActPlan = ?"
    Set rdoQ1 = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ1(0) = frmP_Peticion.NAct
    Set rdo1 = rdoQ1.OpenResultset(rdOpenForwardOnly)
    If rdo1.EOF = False Then cama = rdo1(0) Else cama = ""
        
    Call ImprimirNegrita(margenizquierdo + 7200, posi, "Cama: ", cama)
    
    posi = posicion(interlinea2, posi)
    Call ImprimirNegrita(margenizquierdo, posi, "Paciente: ", frmP_Peticion.txtPaciente(1).Text)
    
     sql = "SELECT  DR.txtFirma,DPT.DESIG FROM DR,DPT,ASIST " _
        & "WHERE ASIST.CDRRESP=DR.CDR AND ASIST.CDPTRESP=DPT.CDPT " _
        & "AND NH = ? AND NC = ? "
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeCHAR
    rdoQ(1).Type = rdTypeINTEGER
    rdoQ(0) = frmP_Peticion.NH
'    rdoQ(1) = frmP_Peticion.NC
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo.EOF = False Then
        doctor = rdo(0): depart = rdo(1)
    Else
        doctor = "": depart = ""
    End If
    posi = posicion(interlinea2, posi)
    Call ImprimirNegrita(margenizquierdo, posi, "Dpto. Responsable: ", depart)
    Call ImprimirNegrita(margenizquierdo + 5400, posi, "Dr. Responsable: ", doctor)
    posi = posicion(posi, interlinea2)
    Call ImprimirNegrita(margenizquierdo, posi, "Prueba Solicitada: ", frmP_Peticion.txtPr(1).Text)
     
    posi = posicion(posi, interlinea1)
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "SOLICITUD"
    
    .CurrentX = margenizquierdo + 5000
    .CurrentY = posi
    Printer.Print "REALIZACION"
    
    sql = "SELECT NVL(Dr.txtFirma, Dr.Ap1), Dpt.Desig, Pat.AP08_Firma, NVL(Res.AP08_Firma, ' '), AP2100.AP21_FecSotud, AP2100.cPr " _
        & "FROM Dr, Dpt, AP2100, AP0800 Pat, AP0800 Res WHERE " _
        & "AP2100.cDpt = Dpt.cDpt AND " _
        & "AP2100.AP21_CodDr = Dr.cDr AND " _
        & "AP2100.AP08_CodPers_Pat = Pat.AP08_CodPers AND " _
        & "AP2100.AP08_CodPers_Res = Res.AP08_CodPers (+) AND " _
        & "AP2100.AP21_CodRef = ?"

    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(0) = nRef
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    
    .FontSize = 9
    posi = posicion(posi, interlinea2)
    Call ImprimirNegrita(margenizquierdo, posi, "Doctor Solicitante: ", rdo(0))
    Call ImprimirNegrita(margenizquierdo + 5000, posi, "Micro: ", rdo(2))
    posi = posicion(posi, interlinea2)
    Call ImprimirNegrita(margenizquierdo, posi, "Departamento Solicitante: ", rdo(1))
    Call ImprimirNegrita(margenizquierdo + 5000, margensuperior + interlinea2 * 5 + interlinea1 * 2, "Macro: ", rdo(3))
    posi = posicion(posi, interlinea2)
    Call ImprimirNegrita(margenizquierdo, margensuperior + interlinea2 * 6 + interlinea1 * 2, "Fecha/Hora Solicitud: ", rdo(4))
    
    posi = posicion(posi, interlinea1)
    .CurrentX = margenizquierdo + 300
    .CurrentY = posi
    .FontBold = True
    .FontUnderline = True
    Printer.Print "MUESTRA"
    
    .CurrentX = margenizquierdo + 6000 + 300
    .CurrentY = posi
    .FontBold = True
    Printer.Print "EXTRACCI�N"
    .FontBold = False
    
    .CurrentX = margenizquierdo + 8000 + 300
    .CurrentY = posi
    .FontBold = True
    Printer.Print "RECEPCI�N"
    .FontBold = False
    
    .FontUnderline = False
    
    Printer.FontUnderline = False
    
    sql = "SELECT AP2500.PR52NumIDMuestra, AP2300.AP23_Desig||'. '||AP2500.AP25_TiMues , " _
        & "TO_CHAR(AP2500.AP25_FecExtra,'DD/MM/YY HH24:MI'), " _
        & "TO_CHAR(AP2500.AP25_FecRecep,'DD/MM/YY HH24:MI'), " _
        & "AP3900.AP39_CodDiag, AP3700.AP50_CodTiSNO||'-'||AP3700.AP37_NumSNO, " _
        & "AP3700.AP37_Descrip||'. '||AP3900.AP39_Descrip, " _
        & "TO_CHAR(AP3900.AP39_FecDiag,'HH24:MI') FROM " _
        & "AP2500, AP2300, AP3700, AP3900 WHERE " _
        & "AP2500.AP23_CodOrg = AP2300.AP23_CodOrg AND " _
        & "AP2500.AP21_CodRef = AP3900.AP21_CodRef AND " _
        & "AP2500.PR52NumIDMuestra = AP3900.PR52NumIDMuestra AND " _
        & "AP3900.AP37_CodSNO = AP3700.AP37_CodSNO AND " _
        & "AP3900.AP49_CodEstDiag <> ? AND " _
        & "AP2500.AP21_CodRef = ? " _
        & "ORDER BY AP2500.PR52NumIDMuestra"
    
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeVARCHAR
    rdoQ(0) = apDIAGANULADO
    rdoQ(1) = nRef
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    cMues = 0
    cDiag = 0
    i = 0
    cDiags = "("
    While rdo.EOF = False
      cDiags = cDiags & rdo(4) & ","
      If cMues <> rdo(0) Then
        If cMues <> 0 Then
          cDiags = Left(cDiags, Len(cDiags) - 1) & ")"
          sql = "SELECT DISTINCT AP0800.AP08_Firma " _
              & "FROM AP0800, AP4000 WHERE " _
              & "AP4000.AP40_CodDr = AP0800.AP08_CodPers AND " _
              & "AP4000.AP39_CodDiag IN " & cDiags
          Set rdo1 = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
          i = 0
          If rdo1.EOF = False Then posi = posicion(.CurrentY, 1000)
          While rdo1.EOF = False
            .CurrentY = posi
            .CurrentX = margenizquierdo + 1500 * i + 900
            Printer.Print rdo1(0)
            i = i + 1
            rdo1.MoveNext
          Wend
          cDiags = "("
        End If
        posi = posicion(posi, interlinea2)
        .CurrentX = margenizquierdo + 300
        .CurrentY = posi
        .FontBold = True
        Printer.Print rdo(0) & ". " & rdo(1)  ' Muestra
      End If
      .CurrentX = margenizquierdo + 6000 + 300
      .CurrentY = posi
      If cMues <> rdo(0) Then
        Printer.Print rdo(2) ' Extracci�n
        .CurrentX = margenizquierdo + 8000 + 300
        .CurrentY = posi
        Printer.Print rdo(3) ' Recepcion
      End If
      .FontBold = False
      .CurrentX = margenizquierdo + 300 + 300
      posi = posicion(posi, interlinea2)
      .CurrentY = posi
      Call ImprimirConSaltos(rdo(7) & " " & rdo(5) & ". " & TipoFrase(rdo(6)), margenizquierdo + 300 + 300, posi)
      
      cMues = rdo(0) 'Se guarda el c�digo de muestra para no mostrarlo dos veces
      rdo.MoveNext
    Wend
    cDiags = Left(cDiags, Len(cDiags) - 1) & ")"
    sql = "SELECT DISTINCT AP0800.AP08_Firma " _
        & "FROM AP0800, AP4000 WHERE " _
        & "AP4000.AP40_CodDr = AP0800.AP08_CodPers AND " _
        & "AP4000.AP39_CodDiag IN " & cDiags
    Set rdo1 = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
    i = 0
    If rdo1.EOF = False Then posi = posicion(.CurrentY, 1000)
    While rdo1.EOF = False
      .CurrentY = posi
      .CurrentX = margenizquierdo + 1500 * i + 900
      Printer.Print rdo1(0)
      i = i + 1
      rdo1.MoveNext
    Wend
    
    Printer.EndDoc
  End With
  Call CerrarrdoQueries
End Sub

Sub InsertDiagDr(num As Long)
Dim sql As String
Dim rdoQ As rdoQuery
Dim i As Integer
  
  sql = "INSERT INTO AP4000 (AP39_CodDiag, AP40_TiDr, AP40_CodDr, AP40_FecConf) VALUES " _
      & "(?, ?, ?, SYSDATE)"  ' TiDr=1 Es doctor de CUN  TiDr=2 Es doctor externo
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(3).Type = rdTypeVARCHAR
  rdoQ(0) = num
  rdoQ(1) = 1
  For i = 1 To UBound(Pat, 2)
    rdoQ(2) = Pat(1, i)
    rdoQ.Execute sql
  Next i

End Sub
Function InsertDiag(nRef As String) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim i As Integer, res As Integer, num As Long

  On Error Resume Next
  objApp.rdoConnect.BeginTrans
  sql = "INSERT INTO AP3900 (AP39_CodDiag, AP21_CodRef, PR52NumIDMuestra, AP34_CodBloq, " _
      & "AP36_CodPorta, AP37_CodSno, AP39_Descrip, Ap39_FecDiag, " _
      & "AP39_Observ, AP49_CodEstDiag, AP39_INDDISCREP, AP39_INDINTRA, AP39_INDInfor"
  If chkInfor.Value = 1 Then
    sql = sql & ", AP39_FecInfor) VALUES (?, ?, ?, ?, ?, ?, ?, " _
              & "SYSDATE, ?, ?, ?, ?, ?, " _
              & "SYSDATE)"
  Else
    sql = sql & ") VALUES (?, ?, ?, ?, ?, ?, ?, SYSDATE, ?, ?, ?, ?, ?)"
  End If
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(1) = nRef
  rdoQ(2) = Dato(cMuestra)
  rdoQ(3) = Dato(cBloque)
  rdoQ(4) = Dato(cTecnica)
  rdoQ(8) = EstadoDiag()
  If chkDiscrepancia.Value = 1 Then rdoQ(9) = -1 Else rdoQ(9) = 0
  If chkIntra.Value = 1 Then rdoQ(10) = -1 Else rdoQ(10) = 0
  If chkInfor.Value = 1 Then
    rdoQ(11) = -1
  Else
    rdoQ(11) = 0
  End If
  grdssDiag.MoveFirst
  For i = 0 To grdssDiag.Rows - 1
    If ExisteDiag(nRef, Dato(cMuestra), grdssDiag.Columns("cDiag").Text) = True Then ' clave duplicada
      Screen.MousePointer = 0
      MsgBox "Ya ha introducido el diagn�stico: " & Chr$(13) & Chr$(13) & " - " & grdssDiag.Columns("Diag").Text & Chr$(13) & Chr$(13) & " para esta muestra.", vbExclamation, "Introducci�n incorrecta"
      Err = 0
    Else
      num = fNextClave("AP39_CodDiag")
      rdoQ(0) = num
      rdoQ(5) = grdssDiag.Columns("cDiag").Text
      If FaltanDiag(Val(grdssDiag.Columns("cDiag").Text), nRef, i) = True Then
        objApp.rdoConnect.RollbackTrans
        Exit Function
      End If
      rdoQ(6) = grdssDiag.Columns("Descrip").Text
      rdoQ(7) = grdssDiag.Columns("Observ").Text
      rdoQ.Execute sql
      If Err = 40002 Then ' clave duplicada
        Screen.MousePointer = 0
        MsgBox "Ya ha introducido el diagn�stico: " & Chr$(13) & Chr$(13) & " - " & grdssDiag.Columns("Diag").Text & Chr$(13) & Chr$(13) & " para esta muestra.", vbExclamation, "Introducci�n incorrecta"
        Err = 0
      Else
        If optDiag(0).Value = False Then Call InsertDiagDr(num)
      End If
    End If
    grdssDiag.MoveNext
  Next i
  If rdoQ.RowsAffected > 0 Then
    Call ActualizarEstPr_Diag(nRef, rdoQ(8))
    If objPipe.PipeExist("Donde") = False Then
      If rdoQ(8) = apDIAGPROVISIONAL Then
        frmP_Peticion.txtPr(2).Text = "Provisional"
      ElseIf rdoQ(8) = apDIAGDEFINITIVO Then
        frmP_Peticion.txtPr(2).Text = "Finalizada"
      End If
    End If
    objApp.rdoConnect.CommitTrans
    InsertDiag = True
  Else
    objApp.rdoConnect.RollbackTrans
    InsertDiag = False
  End If
  grdssDiag.RemoveAll
End Function
Sub LimpiarPantalla()
  txtCodigo.Text = ""
  txtDescrip.Text = ""
  grdssDiag.RemoveAll
  Call Formateartvw
End Sub
Function ExisteDiag(nRef As String, cMues As Integer, cDiag As Long) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP21_CodRef FROM AP3900 WHERE " _
      & "AP21_CodRef = ? AND " _
      & "PR52NumIDMuestra = ? AND " _
      & "AP37_CodSno = ? AND " _
      & "AP49_CodEstDiag <> ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(3).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = cMues
  rdoQ(2) = cDiag
  rdoQ(3) = apDIAGANULADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then ExisteDiag = False Else ExisteDiag = True
  rdo.Close
End Function

Function Dato(num As Integer) As String
  If num > 0 Then
    Dato = num
  Else
    Dato = ""
  End If
End Function

Function TipoSnomed() As String
Dim i As Integer
  For i = 0 To 3
    If chkSno(i).Value = True Then
      TipoSnomed = TiSno(i)
      Exit Function
    End If
  Next i
End Function
Function EstadoDiag() As Integer
Dim i As Integer
  For i = 0 To 2
    If optDiag(i).Value = True Then
      EstadoDiag = TiDiag(i)
      Exit Function
    End If
  Next i
End Function

Private Sub cmdAnt_Click()
Dim res As Integer
  If grdssDiag.Rows > 0 Then 'Si no se ha seleccionado ning�n diagn�stico
    Screen.MousePointer = 0
    res = MsgBox("Ha seleccionado diagn�sticos pero no los ha introducido. �Desea cambiar de muestra?", vbQuestion + vbYesNo, "Selecci�n de diagnosticos")
    If res = vbNo Then Exit Sub
  End If
  donde = donde - 1
  Call IdentificarMuestra(donde)
  If donde = 1 Then cmdAnt.Enabled = False
  cmdSig.Enabled = True
  Call LimpiarPantalla
End Sub

Private Sub cmdBuscar_Click()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim nodo As Node
Dim cSno As String

  If ComprobarSel(Me, TiSno()) = False Then
    MsgBox "Debe seleccionar alg�n tipo de diagn�stico.", vbInformation, "B�squeda de diagn�sticos"
    Call Formateartvw
  Else
    Screen.MousePointer = 11
'    lvwDiag.ListItems.Clear
    Call Formateartvw
    cSno = chkSelec(Me, TiSno())
    sql = "SELECT AP37_CodSno, AP50_CodTiSno, AP37_NumSno, AP51_CodClaseSno, AP37_Descrip, " _
        & "AP37_CodICD FROM AP3700 WHERE AP51_CodClaseSno = 1 AND AP50_CodTiSno IN " & cSno
    If txtCodigo.Text <> "" Then sql = sql & " AND UPPER(AP37_NumSno) Like ?"
    If txtDescrip.Text <> "" Then sql = sql & " AND UPPER(AP37_Descrip) Like ?"
    sql = sql & " UNION " _
        & "SELECT AP37_CodSno, AP50_CodTiSno, AP37_NumSno, AP51_CodClaseSno, AP37_Descrip, " _
        & "AP37_CodICD FROM AP3700 WHERE " _
        & "AP51_CodClaseSno = 1 AND (AP50_CodTiSno, AP37_NumSno) IN (" _
        & "SELECT AP50_CodTiSno, AP37_NumSno FROM AP3700 WHERE " _
        & "AP51_CodClaseSno > 1 AND AP50_CodTiSno IN " & cSno
    If txtCodigo.Text <> "" Then sql = sql & " AND UPPER(AP37_NumSno) Like ?"
    If txtDescrip.Text <> "" Then sql = sql & " AND UPPER(AP37_Descrip) Like ?"
    sql = sql & " )"
    sql = sql & " ORDER BY AP50_CodTiSno, AP37_NumSno"

    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    If txtCodigo.Text <> "" Then
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(0) = UCase(txtCodigo.Text) & "%"
      If txtDescrip.Text <> "" Then
        rdoQ(1).Type = rdTypeVARCHAR
        rdoQ(1) = "%" & UCase(txtDescrip.Text) & "%"
        rdoQ(2).Type = rdTypeVARCHAR
        rdoQ(2) = UCase(txtCodigo.Text) & "%"
        rdoQ(3).Type = rdTypeVARCHAR
        rdoQ(3) = "%" & UCase(txtDescrip.Text) & "%"
      Else
        rdoQ(1).Type = rdTypeVARCHAR
        rdoQ(1) = UCase(txtCodigo.Text) & "%"
      End If
    ElseIf txtDescrip.Text <> "" Then
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(0) = "%" & UCase(txtDescrip.Text) & "%"
      rdoQ(1).Type = rdTypeVARCHAR
      rdoQ(1) = "%" & UCase(txtDescrip.Text) & "%"
    End If

    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    While rdo.EOF = False
      If rdo(3) = 1 Then
        Set nodo = tvwDiag.Nodes.Add("Raiz", tvwChild, "D" & rdo(0) & "P", rdo(1) & "-" & rdo(2) & ". " & rdo(4), apICONDIAG, apICONDIAG)
        nodo.Tag = rdo(2)
      End If
'      Set item = lvwDiag.ListItems.Add(, , rdo(1) & "-" & rdo(2))
'      item.Tag = rdo(0)
'      item.SubItems(1) = CStr(rdo(4))
'      'If rdo(3) = 1 Then item.SubItems(2) = "Primario" Else item.SubItems(2) = "Sin�nimo"
'       If Not IsNull(rdo(5)) Then item.SubItems(2) = rdo(5)
      rdo.MoveNext
    Wend
    Screen.MousePointer = 0
'    If lvwDiag.ListItems.Count = 0 Then
    If tvwDiag.Nodes("Raiz").Children = 0 Then
      MsgBox "No se ha encontrado ning�n diagn�stico.", vbInformation, "B�squeda de diagn�sticos"
    Else
      tvwDiag.Nodes("Raiz").Child.EnsureVisible
    End If
  End If
End Sub
Private Sub cmdSalir_Click()
  Call LimpiarPantalla
  Me.Visible = False
End Sub

Private Sub cmdSig_Click()
Dim res As Integer
  
  If grdssDiag.Rows > 0 Then 'Si no se ha seleccionado ning�n diagn�stico
    Screen.MousePointer = 0
    res = MsgBox("Ha seleccionado diagn�sticos pero no los ha introducido. �Desea cambiar de muestra?", vbQuestion + vbYesNo, "Selecci�n de diagnosticos")
    If res = vbNo Then Exit Sub
  End If
  donde = donde + 1
  Call IdentificarMuestra(donde)
  If UBound(Mues) = donde Then cmdSig.Enabled = False
  cmdAnt.Enabled = True
  Call LimpiarPantalla
End Sub

Private Sub Form_Activate()

  On Error Resume Next
'  If objPipe.PipeGet("NumRef") <> nRef Then
  If seguir = True Then
    nRef = objPipe.PipeGet("NumRef")
    If objPipe.PipeExist("Donde") = True Then
      Call LlenarDatosMuestrasDiag
      donde = 1
    Else
      Call LlenarDatosMuestrasPeti
      donde = cMuestra
    End If
    Call IdentificarPat
    Call IdentificarMuestra(donde)
    If UBound(Mues) = cMuestra Then cmdSig.Enabled = False Else cmdSig.Enabled = True
    If donde = 1 Then cmdAnt.Enabled = False
  Else
    seguir = True
  End If
End Sub
Sub IdentificarPat()
Dim item As ListItem
Dim cPat As Integer

  If objPipe.PipeExist("Donde") = False Then
    cPat = frmP_Peticion.cmdVer.Tag
  Else
    cPat = objPipe.PipeGet("Pat")
  End If
  For Each item In lvwPat.ListItems
    If "P" & cPat = item.key Then
      item.Selected = True
    Else
      item.Selected = False
    End If
  Next item
End Sub

Sub LlenarDatosMuestrasPeti()
Dim padre As Node, abuelo As Node, NodoPeticion As Node
Dim nodoMuestra As Node
  
  On Error Resume Next
  Set NodoPeticion = frmP_Peticion.tvwMuestra.SelectedItem
  Tipo = Left(NodoPeticion.key, 1)
  Select Case Tipo
    Case "M"
      Set nodoMuestra = NodoPeticion
      cMuestra = Right(nodoMuestra.key, Len(nodoMuestra.key) - 1)
      If nodoMuestra.Tag = 0 Then intra = False Else intra = True
    Case "B"
      Set nodoMuestra = NodoPeticion.Parent
      txtBloque.Text = NodoPeticion.Text
      cBloque = Right(NodoPeticion.key, Len(NodoPeticion.key) - 1)
      cMuestra = Right(nodoMuestra.key, Len(nodoMuestra.key) - 1)
      If padre.Tag = 0 Then intra = False Else intra = True
    Case "T"
      Set padre = NodoPeticion.Parent
      Set nodoMuestra = padre.Parent
      txtBloque.Text = padre.Text
      txtTecnica.Text = NodoPeticion.Text
      cTecnica = Val(Right(NodoPeticion.key, Len(NodoPeticion.key) - 1))
      cBloque = Right(padre.key, Len(padre.key) - 1)
      cMuestra = Right(nodoMuestra.key, Len(nodoMuestra.key) - 1)
      If abuelo.Tag = 0 Then intra = False Else intra = True
  End Select
  Call LlenarEstructMuestra(nodoMuestra)
End Sub

Sub LlenarEstructMuestra(nodoMues As Node)
Dim nodo As Node
Dim i As Integer
  
  Set nodo = nodoMues.FirstSibling
  i = 1
  While nodo <> nodoMues.LastSibling
    ReDim Preserve Mues(1 To i)
    With Mues(i)
      .cMues = Right(nodo.key, Len(nodo.key) - 1)
      .Mues = Right(nodo.Text, Len(nodo.Text) - InStr(nodo.Text, ".- ") - 2)
      .intra = nodo.Tag
    End With
    Set nodo = nodo.Next
    i = i + 1
  Wend
  ReDim Preserve Mues(1 To i)
  With Mues(i)
    .cMues = Right(nodo.key, Len(nodo.key) - 1)
    .Mues = Right(nodo.Text, Len(nodo.Text) - InStr(nodo.Text, ".- ") - 2)
    .intra = nodo.Tag
  End With
End Sub

Sub LlenarDatosMuestrasDiag()
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim i As Integer
  
  sql = "SELECT AP2500.PR52NumIDMuestra, PR2400.PR24DesCorta||'. '||PR5200.PR52DesMuestra, " _
      & "PR5200.PR52IndIntra " _
      & "FROM AP2500, PR2400, PR5200 WHERE " _
      & "AP2500.PR52NumMuestra = PR5200.PR52NumMuestra AND " _
      & "PR5200.PR24CodMuestra = PR2400.PR24CodMuestra AND " _
      & "AP2500.AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  i = 1
  While rdo.EOF = False
    ReDim Preserve Mues(1 To i)
    With Mues(i)
      .cMues = rdo(0)
      .Mues = rdo(1)
      .intra = rdo(2)
    End With
    rdo.MoveNext
    i = i + 1
  Wend
End Sub
Sub IdentificarMuestra(lugar As Integer)
  cMuestra = Mues(lugar).cMues
  txtMuestra.Text = cMuestra & ".- " & Mues(lugar).Mues
  If Mues(lugar).intra = 0 Then intra = False Else intra = True
  fraPr.Caption = "Diagn�sticos asociados a la prueba " & Right(nRef, 8)
  Call LlenarIntra(Mues(lugar).intra)
End Sub

Private Sub Form_Deactivate()
  seguir = True
End Sub

Private Sub Form_Load()
  
'  On Error Resume Next
  Set tvwDiag.ImageList = frmPrincipal.imgIcos
  Set tvwPerfil.ImageList = frmPrincipal.imgIcos
  Call LlenarOptSno(Me, TiSno())
  Call LlenarOptDiag
  Call LlenarPat
  Call LlenarPerfil
  Call Formatearlvw
  Call Formateartvw
  seguir = True
End Sub
Sub LlenarIntra(intra As Integer)
  
  If intra = False Then ' La muestra no es intra
    chkDiscrepancia.Enabled = False
    chkIntra.Enabled = False
    optDiag(2).Enabled = True
    chkIntra.Value = 0
  Else
    chkDiscrepancia.Enabled = True
    chkIntra.Enabled = True
    optDiag(2).Enabled = False
    chkIntra.Value = 1
  End If
End Sub
Sub Formateartvw()
Dim nodo As Node
  
  tvwDiag.Nodes.Clear
  Set nodo = tvwDiag.Nodes.Add(, , "Raiz", "Diagn�sticos SNOMED", apICONDIAGS, apICONDIAGS)

End Sub
Sub Formatearlvw()
  
  With lvwPat
    .ColumnHeaders.Add , , "Confirma el diagn�stico", 1800
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With

End Sub

'
Sub LlenarOptDiag()
Dim sql As String
Dim rdo As rdoResultset
Dim i As Integer
  
  sql = "SELECT AP49_CodEstDiag, AP49_Desig FROM AP4900 WHERE AP49_CodEstDiag < 4 " _
      & "ORDER BY AP49_CodEstDiag"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  For i = 0 To 2
    TiDiag(i) = rdo(0)
    optDiag(i).Caption = rdo(1)
    rdo.MoveNext
  Next i
  rdo.Close
  chkInfor.Enabled = False
  lvwPat.Enabled = False
End Sub




Private Sub grdssDiag_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
  DispPromptMsg = False
End Sub

Private Sub grdssDiag_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = 46 Then    ' Tecla Suprimir
    grdssDiag.DeleteSelected
  End If

End Sub


Private Sub optDiag_Click(Index As Integer)
  If Index = 0 Then
    chkInfor.Enabled = False
    lvwPat.Enabled = False
    optDiag(2).Enabled = True
  Else
    chkInfor.Enabled = True
    lvwPat.Enabled = True
    If Index = 1 Then
      chkInfor.Value = 0
    Else
      chkInfor.Value = 1
    End If
  End If
  If chkIntra.Value = 1 Then
    optDiag(2).Enabled = False
  Else
    optDiag(2).Enabled = True
  End If
End Sub

Private Sub tvwDiag_DblClick()
Dim nodo As Node
Dim cDiag As Long, i As Integer
  
  'On Error Resume Next
  Set nodo = tvwDiag.SelectedItem
  If Left(nodo.key, 1) = "D" Then
    nodo.Expanded = False ' Se evita la expansi�n.
    cDiag = Val(Right(nodo.key, Len(nodo.key) - 1))
    grdssDiag.MoveFirst
    For i = 0 To grdssDiag.Rows - 1
      If Val(grdssDiag.Columns("cDiag").Text) = cDiag Then
        MsgBox "Ya ha seleccionado este diagn�stico", vbExclamation, "Introducci�n incorrecta"
        Exit Sub
      End If
      grdssDiag.MoveNext
    Next i
    grdssDiag.AddItem cDiag & Chr$(9) & nodo.Text & Chr$(9) & Chr$(9)
  End If
  
End Sub

Private Sub tvwDiag_NodeClick(ByVal Node As ComctlLib.Node)
Dim cDiag As Integer

  If Node.key <> "Raiz" Then
    If Node.Parent.key = "Raiz" And Node.Children = 0 Then
      cDiag = Val(Right(Node.key, Len(Node.key) - 1))
      Call LlenarSinonimos(Node, cDiag) ' Llena los sin�nimos y la forma adjetival
      Call LlenarRelacionados(Node, cDiag)
    End If
  End If
End Sub
Sub LlenarRelacionados(nodo As Node, cDiag As Integer)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo1 As Node
Dim clave As String, texto As String
Dim i As Integer, ctrl As Integer, codIcon As Integer

  ctrl = False
  sql = "SELECT AP3700.AP37_CodSno, AP5200.AP50_CodTiSno, AP5200.AP37_NumSno, " _
      & "AP3700.AP37_Descrip, AP3700.AP51_CodClaseSno FROM " _
      & "AP5200, AP3700 WHERE " _
      & "AP5200.AP50_CodTiSno = AP3700.AP50_CodTiSno (+) AND " _
      & "AP5200.AP37_NumSno = AP3700.AP37_NumSno (+) AND " _
      & "AP5200.AP37_CodSno = ? AND " _
      & "AP3700.AP51_CodClaseSno = 1"
      
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = cDiag
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  i = 0
  While rdo.EOF = False
    If ctrl = False Then
      Set nodo1 = tvwDiag.Nodes.Add(nodo.key, tvwChild, "R" & cDiag & "Relacionado", "Diagn�sticos relacionados", apICONDIAGS, apICONDIAGS)
      ctrl = True
    End If
    If IsNull(rdo(0)) Then
      i = i + 1
      clave = "X" & cDiag & rdo(1) & rdo(2) & i  ' La clave indicar� que no se puede asociar a un diagn�stico
      texto = rdo(1) & "-" & rdo(2)
      codIcon = apICONDIAGREL
    Else
      clave = "D" & rdo(0) & "P" & cDiag ' La clave recoger� el c�digo de diagn�stico
      texto = rdo(1) & "-" & rdo(2) & ". " & rdo(3)
      codIcon = apICONDIAG
    End If
    If rdo(4) = 1 Then
      Set nodo1 = tvwDiag.Nodes.Add("R" & cDiag & "Relacionado", tvwChild, clave, texto, codIcon, codIcon)
    Else
      Set nodo1 = tvwDiag.Nodes.Add("R" & cDiag & "Relacionado", tvwChild, clave, texto, codIcon, codIcon)
    End If
    rdo.MoveNext
  Wend
End Sub
Sub LlenarSinonimos(nodo As Node, cDiag As Integer)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo1 As Node
Dim clave As String, texto As String, padre As String
Dim ctrlSin As Integer, ctrlAdj As Integer

  sql = "SELECT AP37_CodSno, AP50_CodTiSno, AP37_NumSno, AP37_Descrip, AP51_CodClaseSno FROM " _
      & "AP3700 WHERE " _
      & "AP51_CodClaseSno > 1 AND " _
      & "AP37_NumSno = ? AND " _
      & "AP50_CodTiSno = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nodo.Tag
  rdoQ(1) = Left(nodo.Text, 1)
  ctrlAdj = False
  ctrlSin = False
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    While rdo.EOF = False
      clave = "D" & rdo(0) & "P"  ' La clave recoger� el c�digo de diagn�stico
      texto = rdo(1) & "-" & rdo(2) & ". " & rdo(3)
      If rdo(4) = 5 Then
        If ctrlAdj = False Then
          Set nodo1 = tvwDiag.Nodes.Add(nodo.key, tvwChild, "A" & cDiag & "Adjetivo", "Forma adjetival", apICONDIAGS, apICONDIAGS)
          ctrlSin = True
        End If
        padre = "A" & cDiag & "Adjetivo"
      Else
        If ctrlSin = False Then
          Set nodo1 = tvwDiag.Nodes.Add(nodo.key, tvwChild, "S" & cDiag & "Sinonimo", "Sin�nimos", apICONDIAGS, apICONDIAGS)
          ctrlSin = True
        End If
        padre = "S" & cDiag & "Sinonimo"
      End If
      Set nodo1 = tvwDiag.Nodes.Add(padre, tvwChild, clave, texto, apICONDIAG, apICONDIAG)
      rdo.MoveNext
    Wend
  End If
End Sub


Private Sub tvwPerfil_DblClick()
Dim nodo As Node, nodo1 As Node
Dim cDiag As Long, i As Integer, j As Integer
  
  On Error Resume Next
  Set nodo = tvwPerfil.SelectedItem
  If Left(nodo.key, 1) = "P" Then
    nodo.Expanded = False ' Se evita la expansi�n.
    Set nodo1 = nodo.Child
    For j = 1 To nodo.Children
      cDiag = Val(Right(nodo1.key, Len(nodo1.key) - InStr(nodo1.key, "D")))
      grdssDiag.MoveFirst
      For i = 0 To grdssDiag.Rows - 1
        If Val(grdssDiag.Columns("cDiag").Text) = cDiag Then
          MsgBox "Ya ha seleccionado este diagn�stico", vbExclamation, "Introducci�n incorrecta"
          Exit Sub
        End If
        grdssDiag.MoveNext
      Next i
      grdssDiag.AddItem cDiag & Chr$(9) & nodo1.Text & Chr$(9) & Chr$(9)
      Set nodo1 = nodo1.Next
    Next j
  End If
End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then cmdBuscar_Click
End Sub

Private Sub txtDescrip_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then cmdBuscar_Click
End Sub

