VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmI_Pruebas 
   Caption         =   "Form1"
   ClientHeight    =   7290
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10785
   LinkTopic       =   "Form1"
   ScaleHeight     =   7290
   ScaleWidth      =   10785
   StartUpPosition =   3  'Windows Default
   Begin SSCalendarWidgets_A.SSDateCombo dtcRecep 
      Height          =   315
      Left            =   7275
      TabIndex        =   21
      Top             =   525
      Width           =   1590
      _Version        =   65537
      _ExtentX        =   2805
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular"
      Height          =   360
      Left            =   9150
      TabIndex        =   20
      Top             =   1125
      Width           =   1260
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   360
      Left            =   9150
      TabIndex        =   19
      Top             =   675
      Width           =   1260
   End
   Begin ComctlLib.ListView lvwPr 
      Height          =   4680
      Left            =   360
      TabIndex        =   0
      Top             =   2295
      Width           =   10035
      _ExtentX        =   17701
      _ExtentY        =   8255
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin VB.Frame Frame2 
      Caption         =   "Pruebas"
      ForeColor       =   &H00800000&
      Height          =   1200
      Left            =   7200
      TabIndex        =   10
      Top             =   900
      Width           =   1635
      Begin VB.CheckBox chkTiPr 
         Caption         =   "Auptopsia"
         Height          =   315
         Index           =   2
         Left            =   180
         TabIndex        =   13
         Top             =   825
         Width           =   1215
      End
      Begin VB.CheckBox chkTiPr 
         Caption         =   "Citologia"
         Height          =   315
         Index           =   1
         Left            =   180
         TabIndex        =   12
         Top             =   525
         Width           =   1095
      End
      Begin VB.CheckBox chkTiPr 
         Caption         =   "Biopsia"
         Height          =   315
         Index           =   0
         Left            =   180
         TabIndex        =   11
         Top             =   225
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdCommand2 
      Caption         =   "&Salir"
      Height          =   360
      Left            =   9150
      TabIndex        =   9
      Top             =   1725
      Width           =   1260
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   360
      Left            =   9150
      TabIndex        =   8
      Top             =   225
      Width           =   1260
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cerrada"
      ForeColor       =   &H00800000&
      Height          =   1950
      Left            =   3075
      TabIndex        =   1
      Top             =   150
      Width           =   4035
      Begin VB.OptionButton optEstPr 
         Caption         =   "Para Recepcionar"
         Height          =   315
         Index           =   9
         Left            =   75
         TabIndex        =   18
         Top             =   225
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Anulada"
         Height          =   315
         Index           =   8
         Left            =   2025
         TabIndex        =   17
         Top             =   1425
         Width           =   1650
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Cerrada"
         Height          =   315
         Index           =   7
         Left            =   2025
         TabIndex        =   16
         Top             =   1125
         Width           =   1650
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Diag. definitivos"
         Height          =   315
         Index           =   6
         Left            =   2025
         TabIndex        =   15
         Top             =   825
         Width           =   1650
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Diag. provisionales"
         Height          =   315
         Index           =   5
         Left            =   2025
         TabIndex        =   7
         Top             =   525
         Width           =   1650
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Continuada"
         Height          =   315
         Index           =   4
         Left            =   2025
         TabIndex        =   6
         Top             =   225
         Width           =   1635
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Disponible micro"
         Height          =   315
         Index           =   3
         Left            =   75
         TabIndex        =   5
         Top             =   1425
         Width           =   1560
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Disponible macro"
         Height          =   315
         Index           =   2
         Left            =   75
         TabIndex        =   4
         Top             =   1125
         Width           =   1680
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Recepcionada"
         Height          =   315
         Index           =   1
         Left            =   75
         TabIndex        =   3
         Top             =   825
         Width           =   1395
      End
      Begin VB.OptionButton optEstPr 
         Caption         =   "Recepcionando"
         Height          =   315
         Index           =   0
         Left            =   75
         TabIndex        =   2
         Top             =   525
         Width           =   2115
      End
   End
   Begin ComctlLib.ListView lvwPat 
      Height          =   1890
      Left            =   375
      TabIndex        =   14
      Top             =   225
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   3334
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Label lblRecep 
      Caption         =   "Anteriores a:"
      Height          =   240
      Left            =   7275
      TabIndex        =   22
      Top             =   300
      Width           =   1590
   End
End
Attribute VB_Name = "frmI_Pruebas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim h As String
Dim cep As String
Dim c As String
Dim ITMX As ListItem
Dim rpt As CrystalReport
Dim NH As Long, NC As Long, NS As Integer, cod As Integer
Dim nRef As String

Private Sub cmdAnular_Click()
Dim texto As String
Dim res As Integer

  If optEstPr(9) = False Then
    texto = "�Est� seguro de que desea anular la prueba de n�mero de referencia " & nRef & "?"
    res = MsgBox(texto, vbYesNo + vbQuestion, "Anulaci�n de pruebas")
    If res = vbYes Then
      Call AnularAP21(nRef)
      Call Buscar
    End If
  Else
    texto = "�Est� seguro de que desea anular la prueba:" & Chr$(13) & Chr$(13) _
          & Chr$(9) & "Historia: " & NH & Chr$(13) _
          & Chr$(9) & "Caso: " & NC & Chr$(13) _
          & Chr$(9) & "C�digo: " & cod & "?"
    res = MsgBox(texto, vbYesNo + vbQuestion, "Anulaci�n de pruebas")
    If res = vbYes Then
      Call AnularPrAsist(NH, NC, NS)
      Call Buscar
    End If
  End If
End Sub
Sub AnularAP21(nRef As String)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeVARCHAR
  rdoQ(0) = apPRUEBAANULADA
  rdoQ(1) = nRef
  rdoQ.Execute
  
End Sub
Sub AnularPrAsist(NH As Long, NC As Long, NS As Integer)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE PrAsist SET EstPr = ? WHERE NH = ? AND NC = ? AND NS = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(2).Type = rdTypeINTEGER
  rdoQ(3).Type = rdTypeINTEGER
  rdoQ(0) = 6
  rdoQ(1) = NH
  rdoQ(2) = NC
  rdoQ(3) = NS
  rdoQ.Execute
  
End Sub
Private Sub cmdBuscar_Click()
  Call Buscar
End Sub

Private Sub cmdCommand2_Click()
    Unload Me
End Sub
Sub Buscar()
Dim TiPr As String

  'On Error Resume Next
  Screen.MousePointer = 11
  lvwPr.ListItems.Clear
  TiPr = "("
  If chkTiPr(0).Value = 1 Then TiPr = TiPr & apCODBIOPSIA & ","
  If chkTiPr(1).Value = 1 Then TiPr = TiPr & apCODCITO & ","
  If chkTiPr(2).Value = 1 Then TiPr = TiPr & apCODAUTOPSIA & ","
  If TiPr = "(" Then
    Screen.MousePointer = 0
    MsgBox "Debe seleccionar un tipo de prueba", vbOKOnly + vbInformation, "Estados de las pruebas"
    Exit Sub
  End If
  TiPr = Left(TiPr, Len(TiPr) - 1) & ")"
  If optEstPr(9) = False Then
    Call ConfigurarPr
    Call llenarPrAnat(TiPr)
  Else
    Call ConfigurarPrRecep
    Call llenarPrRecep(TiPr, Format(dtcRecep.Date, "DD/MM/YYYY"))
  End If
  NH = 0
  NC = 0
  NS = 0
  cod = 0
  nRef = ""
  cmdAnular.Enabled = False
  Screen.MousePointer = 0
End Sub
Sub llenarPrAnat(TiPr As String)
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim sql As String
Dim cDr As Integer
Dim c As String
Dim item As ListItem
  
  cDr = Val(lvwPat.SelectedItem.Tag)
  If cDr = 0 Then
    MsgBox "Debe seleccionar un pat�logo.", vbOKOnly + vbInformation, "Estados de las pruebas"
    Exit Sub
  End If
  If optEstPr(0) = True Then cep = apPRUEBARECEPCIONANDO
  If optEstPr(1) = True Then cep = apPRUEBARECEPCIONADA
  If optEstPr(2) = True Then cep = apPRUEBAMACRO
  If optEstPr(3) = True Then cep = apPRUEBAMICRO
  If optEstPr(4) = True Then cep = apPRUEBACONTINUADA
  If optEstPr(5) = True Then cep = apPRUEBAPROVISIONAL
  If optEstPr(6) = True Then cep = apPRUEBAFINALIZADA
  If optEstPr(7) = True Then cep = apPRUEBACERRADA
  If optEstPr(8) = True Then cep = apPRUEBAANULADA

  sql = "SELECT /*+ RULE */ AP21_CodRef, AP21_CODHIST, TO_CHAR(AP21_FecSotud,'DD/MM/YYYY'), " _
      & "PAC.AP1||' '||PAC.AP2||', '||PAC.NOM, AP21_TiPr " _
      & "FROM AP2100, PAC WHERE " _
      & "AP2100.AP21_CODHIST = PAC.NH AND " _
      & "AP2100.AP08_CodPers_Pat = ? AND " _
      & "AP2100.AP45_CodEstPr = ? AND " _
      & "AP2100.AP21_TIPR IN " & TiPr _
      & " ORDER BY AP2100.AP21_CodRef"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = cDr
  rdoQ(1) = cep
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "No se ha encontrado ninguna prueba que cumpla esas condiciones", vbOKOnly + vbInformation, "Listado de pruebas"
    Exit Sub
  End If
  While rdo.EOF = False
    Select Case rdo(4)
      Case 1
        c = "Biopsia"
      Case 2
        c = "Citologia"
      Case 3
        c = "Auptopsia"
    End Select
    Set item = lvwPr.ListItems.Add(, , Right(rdo(0), 8))
    item.Tag = rdo(0)
    item.SubItems(4) = c

    If Not IsNull(rdo(1)) Then item.SubItems(1) = CStr(rdo(1))
    If Not IsNull(rdo(2)) Then item.SubItems(3) = CStr(rdo(2))
    If Not IsNull(rdo(3)) Then item.SubItems(2) = CStr(rdo(3))
    rdo.MoveNext
  Wend

End Sub
Sub llenarPrRecep(TiPr As String, fecha As String)
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim sql As String
Dim cDr As Integer
Dim c As String
Dim item As ListItem
  
  sql = "SELECT PrAsist_AP.NH, PrAsist_AP.NC, Pac.AP1||' '||Pac.AP2||', '||Pac.NOM, " _
      & "PrAsist_AP.fSotud, PrAsist_AP.hSotud, PrAsist_AP.cPr, Pr.Desig, Dpt.Desig, " _
      & "NVL(DR.txtFirma,DR.AP1||' '||DR.AP2||', '||DR.NOM), PrAsist_AP.NS " _
      & "FROM PrAsist_AP, Pac, Dpt, Dr, Pr, AP0900 WHERE " _
      & "PrAsist_AP.NH = Pac.NH AND " _
      & "PrAsist_AP.cDptSote = Dpt.cDpt AND " _
      & "PrAsist_AP.cDrSote = Dr.cDr AND " _
      & "PrAsist_AP.cPr = Pr.cPr AND " _
      & "PrAsist_AP.cDptPr = Pr.cDpt AND " _
      & "Pr.cGrpStat = AP0900.AP09_CodTiPr AND " _
      & "AP0900.AP09_TiPr IN " & TiPr & " AND " _
      & "PrAsist_AP.fSotud < TO_DATE('" & fecha & "', 'DD/MM/YYYY') " _
      & "ORDER BY PrAsist_AP.fSotud, PrAsist_AP.hsotud ASC"
  objApp.rdoConnect.Execute "ALTER SESSION SET OPTIMIZER_GOAL = RULE"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  objApp.rdoConnect.Execute "ALTER SESSION SET OPTIMIZER_GOAL = FIRST_ROWS"
  If rdo.EOF = True Then
    MsgBox "No se ha encontrado ninguna prueba que cumpla esas condiciones", vbOKOnly + vbInformation, "Listado de pruebas"
    Exit Sub
  End If
  While rdo.EOF = False
    Set item = lvwPr.ListItems.Add(, , rdo(0))
    item.SubItems(1) = CStr(rdo(1))
    item.SubItems(2) = CStr(rdo(2))
    item.SubItems(3) = Format$(rdo(3), "DD/MM/YY") & " " & Format$(rdo(4), "HH:MM")
    item.SubItems(4) = CStr(rdo(5))
    item.SubItems(5) = CStr(rdo(6))
    item.SubItems(6) = CStr(rdo(7))
    item.SubItems(7) = CStr(rdo(8))
    item.Tag = rdo(9)
    rdo.MoveNext
  Wend

End Sub

Private Sub cmdImprimir_Click()
Dim sql As String, TiPr As String, titulo As String
Dim cep As Integer, cDr As Integer, fich As String
  
  Screen.MousePointer = 11
  TiPr = "["
  If chkTiPr(0).Value = 1 Then TiPr = TiPr & apCODBIOPSIA & ","
  If chkTiPr(1).Value = 1 Then TiPr = TiPr & apCODCITO & ","
  If chkTiPr(2).Value = 1 Then TiPr = TiPr & apCODAUTOPSIA & ","
  If TiPr = "[" Then
    MsgBox "Debe seleccionar un tipo de prueba", vbOKOnly + vbInformation, "Estados de las pruebas"
    Exit Sub
  End If
  TiPr = Left(TiPr, Len(TiPr) - 1) & "]"
  
  If optEstPr(9) = False Then
    cDr = Val(lvwPat.SelectedItem.Tag)
    If cDr = 0 Then
      MsgBox "Debe seleccionar un pat�logo.", vbOKOnly + vbInformation, "Estados de las pruebas"
      Exit Sub
    End If
    If optEstPr(0) = True Then cep = apPRUEBARECEPCIONANDO: titulo = "recepcionando"
    If optEstPr(1) = True Then cep = apPRUEBARECEPCIONADA: titulo = "recepcionadas"
    If optEstPr(2) = True Then cep = apPRUEBAMACRO: titulo = "disponible macro"
    If optEstPr(3) = True Then cep = apPRUEBAMICRO: titulo = "dicponible micro"
    If optEstPr(4) = True Then cep = apPRUEBACONTINUADA: titulo = "continuadas"
    If optEstPr(5) = True Then cep = apPRUEBAPROVISIONAL: titulo = "con diagn�sticos provisionales"
    If optEstPr(6) = True Then cep = apPRUEBAFINALIZADA: titulo = "finalizadas"
    If optEstPr(7) = True Then cep = apPRUEBACERRADA: titulo = "cerradas"
    If optEstPr(8) = True Then cep = apPRUEBAANULADA: titulo = "anuladas"
    fich = "AP002602.rpt"
    sql = "{AP2100.AP08_CodPers_Pat} = " & cDr & " AND " _
        & "{AP2100.AP45_CodEstPr} = " & cep & " AND " _
        & "{AP2100.AP21_TIPR} IN " & TiPr
    rpt.Formulas(0) = "Titulo= 'Listado de pruebas " & titulo & "'"
  Else
    sql = "{PrAsist_AP.FSotud} < " & fFechaCrystal1(dtcRecep.Date) & " AND " _
        & "{AP0900.AP09_TiPr} IN " & TiPr
    fich = "AP002601.rpt"
  End If
  With rpt
    .ReportFileName = App.Path & "\rpt\" & fich
    If .Connect = "" Then
        .Connect = objApp.rdoConnect.Connect
    End If
    .WindowControls = True
    .WindowControlBox = True
    .WindowTitle = Me.Caption
    .WindowState = crptMaximized
    .Destination = crptToWindow
    .SelectionFormula = sql
    res = .PrintReport
    .SelectionFormula = ""
    .WindowTitle = ""
    .ReportFileName = ""
    .Formulas(0) = ""
  End With
  Screen.MousePointer = 0

End Sub

Private Sub Form_Load()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem

  With lvwPat
    .ColumnHeaders.Add
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
  
  sql = "SELECT AP08_CodPers, AP08_FIRMA FROM AP0800 WHERE AP07_CodTiPers = ? AND AP08_IndActiva = -1"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = apCODPATOLOGO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwPat.ListItems.Add(, , rdo(1))
    item.Tag = rdo(0)
    rdo.MoveNext
  Wend
  Me.Caption = "Pruebas"
  Me.Left = (Screen.Width - Me.Width) / 2
  Me.Top = (Screen.Height - Me.Height) / 2
  Set rpt = frmPrincipal.crCrystalReport1
  If SuperUser() = False Then
    cmdAnular.Visible = False
  Else
    cmdAnular.Visible = True
    cmdAnular.Enabled = False
  End If
End Sub
Sub ConfigurarPr()
  With lvwPr
    .Enabled = True
    With .ColumnHeaders
      .Clear
      .Add , , "Prueba", lvwPr.Width / 15
      .Add , , "Historia", lvwPr.Width / 12
      .Add , , "Nombre del Paciente", 2 * (lvwPr.Width / 5)
      .Add , , "F.Solicitud", lvwPr.Width / 12
      .Add , , "Prueba", lvwPr.Width / 6
    End With
    .View = lvwReport
  End With

End Sub
Sub ConfigurarPrRecep()
  With lvwPr
    .Enabled = True
    .ColumnHeaders.Clear
    With .ColumnHeaders
      .Clear
      .Add , , "Historia", lvwPr.Width / 17
      .Add , , "Caso", lvwPr.Width / 23
      .Add , , "Paciente", 2 * (lvwPr.Width / 7)
      .Add , , "F.Solicitud", lvwPr.Width / 8
      .Add , , "C�digo", lvwPr.Width / 25
      .Add , , "Prueba", lvwPr.Width / 10
      .Add , , "Dpto.", lvwPr.Width / 6
      .Add , , "Doctor", lvwPr.Width / 6
    End With
    .View = lvwReport
  End With

End Sub
Private Sub lvwPr_ColumnClick(ByVal ColumnHeader As ComctlLib.ColumnHeader)
  
'  With lvwPr
'    .SortKey = ColumnHeader.Index - 1
'    .Sorted = True
'    .SortOrder = lvwDescending
'  End With
End Sub

Private Sub lvwPr_ItemClick(ByVal item As ComctlLib.ListItem)
  cmdAnular.Enabled = True
  If optEstPr(9) = False Then
    nRef = item.Tag
  Else
    NH = item.Text
    NC = item.SubItems(1)
    NS = item.Tag
    cod = item.SubItems(4)
  End If
    
End Sub

Private Sub optEstPr_Click(Index As Integer)
    
  lvwPr.ListItems.Clear
  If Index = 9 Then
    lblRecep.Visible = True
    dtcRecep.Visible = True
  Else
    lblRecep.Visible = False
    dtcRecep.Visible = False
  End If
End Sub
