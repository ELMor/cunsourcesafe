VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmP_Recepcion 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recepci�n de pruebas"
   ClientHeight    =   7995
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11670
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7995
   ScaleWidth      =   11670
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   11670
      _ExtentX        =   20585
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraMuestra 
      Caption         =   "Muestras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2970
      Left            =   75
      TabIndex        =   17
      Top             =   4050
      Width           =   11565
      Begin SSDataWidgets_B.SSDBDropDown ddssDecal 
         Height          =   765
         Left            =   6600
         TabIndex        =   21
         Top             =   750
         Width           =   2640
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cFijador"
         Columns(0).Name =   "cFijador"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Fijador"
         Columns(1).Name =   "Fijador"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4657
         _ExtentY        =   1349
         _StockProps     =   77
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBDropDown ddssFijador 
         Height          =   765
         Left            =   3000
         TabIndex        =   20
         Top             =   1350
         Width           =   2640
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cFijador"
         Columns(0).Name =   "cFijador"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Fijador"
         Columns(1).Name =   "Fijador"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4657
         _ExtentY        =   1349
         _StockProps     =   77
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBGrid grdssMuestra 
         Height          =   2505
         Index           =   0
         Left            =   150
         TabIndex        =   18
         Top             =   300
         Width           =   11220
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   12
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   12
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "nMues"
         Columns(0).Name =   "nMues"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Recep."
         Columns(1).Name =   "Recep."
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   11
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).Style=   2
         Columns(2).Width=   953
         Columns(2).Caption=   "N�"
         Columns(2).Name =   "N�"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   2
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "Organo"
         Columns(3).Name =   "Organo"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Caption=   "Tipo"
         Columns(4).Name =   "Tipo"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   979
         Columns(5).Caption=   "Intra"
         Columns(5).Name =   "Intra"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   11
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   12632256
         Columns(6).Width=   3200
         Columns(6).Caption=   "F. Extracci�n"
         Columns(6).Name =   "F. Extracci�n"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   12632256
         Columns(7).Width=   3200
         Columns(7).Caption=   "Fijador"
         Columns(7).Name =   "Fijador"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   16777215
         Columns(8).Width=   3200
         Columns(8).Caption=   "Decalcificador"
         Columns(8).Name =   "Decalcificador"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   16777215
         Columns(9).Width=   3200
         Columns(9).Caption=   "Observaciones"
         Columns(9).Name =   "Observaciones"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   11
         Columns(9).FieldLen=   256
         Columns(9).HasBackColor=   -1  'True
         Columns(9).BackColor=   16777215
         Columns(10).Width=   3200
         Columns(10).Caption=   "Extracci�n"
         Columns(10).Name=   "Extracci�n"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "cMues"
         Columns(11).Name=   "cMues"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         UseDefaults     =   0   'False
         _ExtentX        =   19791
         _ExtentY        =   4419
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdRecepcion 
      Caption         =   "&Recepci�n"
      Height          =   390
      Left            =   10050
      TabIndex        =   24
      Top             =   7125
      Width           =   1440
   End
   Begin VB.Frame fraPr 
      Caption         =   "Pacientes/Pruebas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3540
      Index           =   0
      Left            =   75
      TabIndex        =   8
      Top             =   450
      Width           =   11595
      Begin TabDlg.SSTab tabPr 
         Height          =   3150
         HelpContextID   =   90001
         Index           =   0
         Left            =   150
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   300
         Width           =   11310
         _ExtentX        =   19950
         _ExtentY        =   5556
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AP00725.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(14)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(15)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(3)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "tabDr"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtPr(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtPr(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtPr(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtPr(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtPr(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtPr(5)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtPr(6)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtPr(11)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtPr(12)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtPr(9)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).ControlCount=   16
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AP00725.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssPr"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "SUBSTR(PR04NUMIDACTDEPR,3)"
            Height          =   285
            Index           =   9
            Left            =   900
            TabIndex        =   2
            Tag             =   "N�mero de la prueba"
            Top             =   900
            Width           =   975
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR03MOTPETCION"
            Height          =   285
            Index           =   12
            Left            =   1650
            MaxLength       =   50
            TabIndex        =   22
            Tag             =   "Motivo de petici�n"
            Top             =   1500
            Width           =   5715
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR04NUMACTPLAN"
            Height          =   285
            Index           =   11
            Left            =   225
            TabIndex        =   19
            Tag             =   "N� Actuaci�n planificada"
            Top             =   1200
            Visible         =   0   'False
            Width           =   675
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR03DESINDICAC"
            Height          =   285
            Index           =   6
            Left            =   1635
            MaxLength       =   50
            TabIndex        =   7
            Tag             =   "Indicaciones a la prueba"
            Top             =   2100
            Width           =   5760
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR03DESOBSERVAC"
            Height          =   285
            Index           =   5
            Left            =   1635
            MaxLength       =   50
            TabIndex        =   6
            Tag             =   "Observaciones a la prueba"
            Top             =   2640
            Width           =   9210
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "AP60_DESCRIP"
            Height          =   285
            Index           =   3
            Left            =   2400
            MaxLength       =   40
            TabIndex        =   4
            Tag             =   "Tipo de prueba"
            Top             =   900
            Width           =   1185
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR01DESCOMPLETA"
            Height          =   285
            Index           =   4
            Left            =   3600
            MaxLength       =   50
            TabIndex        =   5
            Tag             =   "Prueba|Nombre de la prueba"
            Top             =   900
            Width           =   3810
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR01CODACTUACION"
            Height          =   285
            Index           =   2
            Left            =   1875
            TabIndex        =   3
            Tag             =   "N�mero de la prueba"
            Top             =   900
            Width           =   525
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   285
            Index           =   0
            Left            =   885
            MaxLength       =   7
            TabIndex        =   0
            Tag             =   "Historia|N� de historia del paciente"
            Top             =   390
            Width           =   1020
         End
         Begin VB.TextBox txtPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "Nombre"
            Height          =   285
            Index           =   1
            Left            =   1905
            MaxLength       =   50
            TabIndex        =   1
            Tag             =   "Nombre|Nombre del paciente"
            Top             =   390
            Width           =   5490
         End
         Begin SSDataWidgets_B.SSDBGrid grdssPr 
            Height          =   2970
            Left            =   -74850
            TabIndex        =   12
            Top             =   75
            Width           =   10725
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18918
            _ExtentY        =   5239
            _StockProps     =   79
            ForeColor       =   0
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tabDr 
            Height          =   2040
            Left            =   7575
            TabIndex        =   25
            Top             =   375
            Width           =   3315
            _ExtentX        =   5847
            _ExtentY        =   3598
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Anatom�a"
            TabPicture(0)   =   "AP00725.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(1)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(11)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(12)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(16)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "cbossRealiza(3)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "cbossRealiza(2)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "cbossRealiza(1)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "cbossRealiza(0)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).ControlCount=   8
            TabCaption(1)   =   "Peticionario"
            TabPicture(1)   =   "AP00725.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtPr(7)"
            Tab(1).Control(1)=   "txtPr(8)"
            Tab(1).Control(2)=   "txtPr(10)"
            Tab(1).Control(3)=   "cbossPeti(0)"
            Tab(1).Control(4)=   "cbossPeti(1)"
            Tab(1).Control(5)=   "lblLabel1(13)"
            Tab(1).Control(6)=   "lblLabel1(7)"
            Tab(1).Control(7)=   "lblLabel1(6)"
            Tab(1).Control(8)=   "lblLabel1(0)"
            Tab(1).ControlCount=   9
            Begin VB.TextBox txtPr 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "DrPeti"
               Height          =   285
               Index           =   7
               Left            =   -74250
               TabIndex        =   42
               Tag             =   "Fecha de Solicitud"
               Top             =   1200
               Width           =   2205
            End
            Begin VB.TextBox txtPr 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "AD02DESDPTO"
               Height          =   285
               Index           =   8
               Left            =   -74250
               MaxLength       =   20
               TabIndex        =   27
               Tag             =   "Departamento peticionario"
               Top             =   825
               Width           =   2190
            End
            Begin VB.TextBox txtPr 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               DataField       =   "PR09FECPETICION"
               Height          =   285
               Index           =   10
               Left            =   -74250
               MaxLength       =   20
               TabIndex        =   26
               Tag             =   "Fecha de Solicitud"
               Top             =   1575
               Width           =   2205
            End
            Begin SSDataWidgets_B.SSDBCombo cbossRealiza 
               Height          =   285
               Index           =   0
               Left            =   825
               TabIndex        =   28
               Tag             =   "Doctor que realiza la Descripci�n Microsc�pica"
               Top             =   375
               Width           =   2280
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AllowNull       =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "cDr"
               Columns(0).Name =   "cDr"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Dr"
               Columns(1).Name =   "Dr"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4022
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cbossRealiza 
               Height          =   285
               Index           =   1
               Left            =   825
               TabIndex        =   29
               Tag             =   "Doctor que realiza la Descripci�n Macrosc�pica"
               Top             =   795
               Width           =   2280
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "cRes"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Nombre"
               Columns(1).Name =   "Res"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Ref"
               Columns(2).Name =   "RefRes"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   4022
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cbossRealiza 
               Height          =   285
               Index           =   2
               Left            =   825
               TabIndex        =   30
               Tag             =   "T�cnico de citolog�as"
               Top             =   1200
               Width           =   2280
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Nombre"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Ref"
               Columns(2).Name =   "Ref"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   4022
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cbossRealiza 
               Height          =   285
               Index           =   3
               Left            =   825
               TabIndex        =   31
               Tag             =   "Persona que efect�a la recepci�n"
               Top             =   1620
               Width           =   2280
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Nombre"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "Ref"
               Columns(2).Name =   "Ref"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   4022
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cbossPeti 
               Height          =   285
               Index           =   0
               Left            =   -74250
               TabIndex        =   32
               Tag             =   "Doctor que realiza la Descripci�n Microsc�pica"
               Top             =   450
               Width           =   2205
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Nombre"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "cDr"
               Columns(2).Name =   "cDr"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   3889
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cbossPeti 
               Height          =   285
               Index           =   1
               Left            =   -74250
               TabIndex        =   33
               Tag             =   "Doctor que realiza la Descripci�n Microsc�pica"
               Top             =   1200
               Visible         =   0   'False
               Width           =   2205
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Nombre"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "cDr"
               Columns(2).Name =   "cDr"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   3889
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Centro:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   -74925
               TabIndex        =   41
               Top             =   450
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Doctor:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   -74925
               TabIndex        =   40
               Top             =   1215
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dpto:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -74775
               TabIndex        =   39
               Top             =   840
               Width           =   540
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74850
               TabIndex        =   38
               Top             =   1590
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Recep:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   150
               TabIndex        =   37
               Top             =   1650
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Micro:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   150
               TabIndex        =   36
               Top             =   450
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Macro:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   150
               TabIndex        =   35
               Top             =   870
               Width           =   585
            End
            Begin VB.Label lblLabel1 
               Caption         =   "T�cnico:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   75
               TabIndex        =   34
               Top             =   1245
               Width           =   735
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Motivo petici�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   150
            TabIndex        =   23
            Top             =   1515
            Width           =   1440
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicaciones:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   180
            TabIndex        =   16
            Top             =   2115
            Width           =   1440
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Observaciones:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   180
            TabIndex        =   15
            Top             =   2640
            Width           =   1440
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Prueba:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   75
            TabIndex        =   14
            Top             =   915
            Width           =   675
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Paciente:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   75
            TabIndex        =   13
            Top             =   375
            Width           =   840
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   9
      Top             =   7710
      Width           =   11670
      _ExtentX        =   20585
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmP_Recepcion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public objPr As New clsCWForm
Dim TiPrAnterior As Integer, TiPr As Integer, TiPac As Integer
Dim cPat As String, cFija As Long, cDecal As Long, cRes As String, cCito As String
Dim cRecep As String, nRef As String, cCtro As Long, cPeti As String
Dim ExistePr As Integer, MuesRecep As String

Private Sub cbossPeti_CloseUp(Index As Integer)
Dim sql As String

  Select Case Index
    Case 0
      cCtro = cbossPeti(0).Columns(0).Text
      If cbossPeti(0).Columns(0).Text = apCODCUN Then
        cbossPeti(1).Visible = False
        txtPr(7).Visible = True
        TiPac = 1
      Else
        cbossPeti(1).Visible = True
        txtPr(7).Visible = False
        sql = "SELECT AP15_CodPat, AP15_Firma FROM AP1500 WHERE AP11_CodCtro = " & cCtro
        Call LlenarCombo(cbossPeti(1), sql)
        TiPac = 2
      End If
    Case 1
      cPeti = cbossPeti(1).Columns(0).Text
  End Select
End Sub

Private Sub cmdRecepcion_Click()
Dim res As Integer
Dim sql As String

  If ComprobacionDatos() = True Then
    TiPr = objPr.rdoCursor("AP60_TiPr")
    objApp.rdoConnect.BeginTrans
    If ExistePr = False Then
      If InsertPr() <> 1 Then
        objApp.rdoConnect.RollbackTrans
        MsgBox "Error en la recepci�n de la prueba.", vbOKOnly, "Recepci�n de pruebas"
        Exit Sub
      End If
    Else
      nRef = objPr.rdoCursor("PR04NUMIDACTDEPR")
    End If
    If InsertMuestra() = 0 Then
      objApp.rdoConnect.RollbackTrans
      MsgBox "Error en la recepci�n de las muestras.", vbOKOnly, "Recepci�n de pruebas"
      Exit Sub
    End If
    objApp.rdoConnect.CommitTrans
    Load frmNumRef
    frmNumRef.lblTit.Caption = "Se ha efectuado la recepci�n de la prueba de n�mero:"
    frmNumRef.lblnRef.Caption = Val(Right(nRef, 5))
    frmNumRef.Show vbModal

    If TiPac = 1 Then  ' Pacientes de CUN
      If TiPr = apCODCITO Then
        res = MsgBox("�Desea imprimir la etiqueta para la muestra?", vbQuestion + vbYesNo, "Recepci�n de muestras")
        If res = vbYes Then Call ImprimirEtiqueta(nRef, MuesRecep)
      ElseIf TiPr = apCODBIOPSIA Then
        Call ImprimirEtiqueta(nRef, MuesRecep)
      End If
    End If
'    Call ImprimirHojaRecepcion(nRef, TiPac)
    Call ImprimirHoja(nRef)
    Call ActualizarEstadoPrueba(nRef)
    Call objWinInfo.DataRefresh
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim sql As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objPr
    .strName = "Pr"
    Set .objFormContainer = fraPr(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPr(0)
    Set .grdGrid = grdssPr
    .strTable = "PR0441J"

    .intAllowance = cwAllowReadOnly
    Call .FormCreateFilterWhere(.strTable, "Pruebas")
    Call .FormAddFilterWhere(.strTable, "CI21NumHistoria", "N� Historia", cwNumeric)
    Call .FormAddFilterWhere(.strTable, "CI21CODPERSONA", "C. Persona", cwNumeric)
    Call .FormAddFilterWhere(.strTable, "PR01CODACTUACION", "C�digo de la prueba", cwNumeric)
    Call .FormAddFilterWhere(.strTable, "PR09FECPETICION", "F. Solicitud", cwDate)
    Call .FormAddFilterWhere(.strTable, "AD02DESDPTO", "Dpt. peticionario", cwString)
    Call .FormAddFilterOrder(.strTable, "CI21CODPERSONA", "Historia")
    Call .FormAddFilterOrder(.strTable, "PR09FECPETICION", "F. Solicitud")
    .strColumns = "AP60_TiPr, SG02Cod, PR04NUMACTPLAN, AD02CodDpto"
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
    .intCursorSize = 100
    .blnHasMaint = False
  End With
   
  With objWinInfo
    Call .FormAddInfo(objPr, cwFormDetail)
    
    Call .FormCreateInfo(objPr)
    .CtrlGetInfo(txtPr(0)).blnInFind = True
    .CtrlGetInfo(txtPr(1)).blnInFind = True
    .CtrlGetInfo(txtPr(2)).blnInFind = True
    .CtrlGetInfo(txtPr(3)).blnInFind = True
    .CtrlGetInfo(txtPr(4)).blnInFind = True
    .CtrlGetInfo(txtPr(8)).blnInFind = True
'    .CtrlGetInfo(txtPr(9)).blnInFind = True
    .CtrlGetInfo(txtPr(10)).blnInFind = True
    .CtrlGetInfo(cbossRealiza(0)).blnNegotiated = False
    .CtrlGetInfo(cbossRealiza(1)).blnNegotiated = False
    .CtrlGetInfo(cbossRealiza(2)).blnNegotiated = False
    .CtrlGetInfo(cbossRealiza(3)).blnNegotiated = False
    .CtrlGetInfo(cbossPeti(0)).blnNegotiated = False
    .CtrlGetInfo(cbossPeti(1)).blnNegotiated = False
  
    sql = "SELECT AP19_CodFija, AP19_Desig FROM AP1900 ORDER BY AP19_Desig"
    Call LlenarCombo(ddssFijador, sql)
    sql = "SELECT AP22_CODDecal, AP22_Desig FROM AP2200 ORDER BY AP22_Desig"
    Call LlenarCombo(ddssDecal, sql)
    sql = "SELECT AP11_CodCtro, AP11_Desig FROM AP1100 ORDER BY AP11_CodCtro"
    Call LlenarCombo(cbossPeti(0), sql)
    cbossPeti(0).Text = cbossPeti(0).Columns(1).Text
    TiPac = 1
    grdssMuestra(0).Columns("Fijador").DropDownHwnd = ddssFijador.hwnd
    grdssMuestra(0).Columns("Decalcificador").DropDownHwnd = ddssDecal.hwnd
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdssMuestra(0).SelectTypeRow = ssSelectionTypeMultiSelect
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdssMuestra_SelChange(Index As Integer, ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
  If Val(grdssMuestra(0).Columns("Recep.").Text) = -1 Then Cancel = True
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  
  If objPr.rdoCursor.EOF = False Then
    TiPr = objPr.rdoCursor("AP60_TiPr")
    Call LlenarMuestras(txtPr(11).Text)
    If TiPrAnterior <> TiPr Then
      Select Case TiPr
        Case 1  ' Biopsias
          Call ConfigurarBiopsias
        Case 2  ' Citolog�as
          Call ConfigurarCitologias
        Case 3  ' Autopsias
          Call ConfigurarNecropsias
      End Select
      TiPrAnterior = objPr.rdoCursor("AP60_TiPr")
    End If
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssPr_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssPr_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPr_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssPr_Change()
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabpr_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabPr(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraPr_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraPr(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtPr_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtPr_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtPr_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Sub ConfigurarGridMuestras()
  With grdssMuestra(0)
    .Columns.RemoveAll
    .Columns.Add (0)
    .Columns(0).Caption = "N�"
    .Columns.Add (1)
    .Columns(1).Caption = "Organo"
    .Columns.Add (2)
    .Columns(2).Caption = "Tipo"
    .Columns.Add (3)
    .Columns(3).Caption = "F. Extracc."
    .Columns.Add (4)
    .Columns(4).Caption = "cFija"
    .Columns(4).Visible = False
    .Columns.Add (5)
    .Columns(5).Caption = "Fijador"
    .Columns.Add (6)
    .Columns(6).Caption = "cDecal"
    .Columns(6).Visible = False
    .Columns.Add (7)
    .Columns(7).Caption = "Decalcificador"
    .Columns.Add (8)
    .Columns(8).Caption = "Observaciones"
    .Columns.Add (9)
    .Columns(9).Caption = "Extracci�n"
    .Columns.Add (10)
    .Columns(10).Caption = "Intra"
    .Columns(10).DataType = 11
  End With
End Sub

Sub LlenarMuestras(cPr As Long)
Dim sql As String, Recep As Integer, fij As String, i As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  grdssMuestra(0).RemoveAll
  sql = "SELECT PR52NUMMUESTRA, PR52NUMIDMUESTRA, PR24DESCORTA, PR52DESMUESTRA, " _
      & "PR52IndIntra, PR52FECEXTRAC, DREXTRAC, PR24CodMuestra, PR55CODESTADO " _
      & "FROM PR5241J WHERE " _
      & "PR04NUMACTPLAN = ? ORDER BY PR52NUMIDMUESTRA"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cPr
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    Call LlenarPatOrg(rdo(7))
    While rdo.EOF = False
      If rdo(8) = 1 Then Recep = False Else Recep = True
      grdssMuestra(0).AddItem rdo(0) & Chr$(9) & Recep & Chr$(9) & rdo(1) & Chr$(9) & rdo(2) _
                            & Chr$(9) & rdo(3) & Chr$(9) & rdo(4) & Chr$(9) & rdo(5) & Chr$(9) _
                            & Chr$(9) & Chr$(9) & Chr$(9) & rdo(6) & Chr$(9) & rdo(7)
      rdo.MoveNext
    Wend
    grdssMuestra(0).MoveFirst
    ExistePr = False
    For i = 1 To grdssMuestra(0).Rows
      If grdssMuestra(0).Columns("Recep.").Text = 0 Then
        grdssMuestra(0).Columns("Fijador").Text = "Formol"
      Else
        ExistePr = True
        Call LlenarFijaDecal(txtPr(9).Text, Val(grdssMuestra(0).Columns("N�").Text))
      End If
      grdssMuestra(0).MoveNext
    Next i
    grdssMuestra(0).MoveFirst
  End If
End Sub

Sub LlenarFijaDecal(nRef As String, nMues As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP1900.AP19_Descrip, AP2200.AP22_Descrip FROM AP1900, AP2200, AP2500 WHERE " _
      & "AP2500.AP19_CodFija = AP1900.AP19_CodFija AND " _
      & "AP2500.AP22_CodDecal = AP2200.AP22_CodDecal (+) AND " _
      & "AP2500.AP21_CodRef = ? AND " _
      & "AP2500.PR52NumIDMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = nMues
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    If Not IsNull(rdo(0)) Then grdssMuestra(0).Columns("Fijador").Text = rdo(0)
    If Not IsNull(rdo(1)) Then grdssMuestra(0).Columns("Decalcificador").Text = rdo(1)
  End If
End Sub

Sub LlenarPatOrg(cMues As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT SG0200.SG02Cod, SG0200.SG02TxtFirma FROM SG0200, AP2300 WHERE " _
      & "SG0200.SG02Cod = AP2300.SG02Cod AND " _
      & "AP2300.PR24CodMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cMues
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    cPat = rdo(0)
    cbossRealiza(0).Text = rdo(1)
    If cbossRealiza(1).Text = "" Then cRes = ResAsoc(cPat)
  End If
End Sub
Sub ConfigurarBiopsias()
Dim sql As String
  
  cbossRealiza(2).Enabled = False
  grdssMuestra(0).Columns("Fijador").BackColor = cwGRISMEDIO
  grdssMuestra(0).Columns("Decalcificador").Visible = True
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND " _
      & "AD31CodPuesto = " & apCODPATOLOGO & " AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(0), sql)
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND " _
      & "AD31CodPuesto IN (" & apCODPATOLOGO & "," & apCODRES & ") AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(1), sql)
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND " _
      & "AD31CodPuesto = " & apCODPERSCITO & " AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(3), sql)

End Sub
Sub ConfigurarCitologias()
Dim sql As String
  
  cbossRealiza(2).Enabled = True
  grdssMuestra(0).Columns("Fijador").BackColor = cwBLANCO
  grdssMuestra(0).Columns("Decalcificador").Visible = False
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND " _
      & "AD31CodPuesto = " & apCODPATOLOGO & " AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(0), sql)
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND " _
      & "AD31CodPuesto IN (" & apCODPERSCITO & "," & apCODRES & ") AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(1), sql)
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND " _
      & "AD31CodPuesto IN (" & apCODPERSCITO & "," & apCODRES & ") AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(2), sql)
  Call LlenarCombo(cbossRealiza(3), sql)
  
End Sub
Sub ConfigurarNecropsias()
Dim sql As String
  
  cbossRealiza(2).Enabled = False
  grdssMuestra(0).Columns("Fijador").BackColor = cwGRIS
  grdssMuestra(0).Columns("Decalcificador").Visible = True
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND " _
      & "AD31CodPuesto = " & apCODPATOLOGO & " AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(0), sql)
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND " _
      & "AD31CodPuesto IN (" & apCODPATOLOGO & "," & apCODRES & ") AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(1), sql)
  sql = "SELECT SG02Cod, SG02TxtFirma FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & " AND AD03FecFin IS NULL) " _
      & "ORDER BY SG02TxtFirma"
  Call LlenarCombo(cbossRealiza(3), sql)

End Sub


Sub LlenarCombo(cbo As Control, sql As String)
Dim rdo As rdoResultset
    
  cbo.RemoveAll
  Set rdo = objApp.rdoConnect.OpenResultset(sql)
  While rdo.EOF = False
    cbo.AddItem rdo(0) & Chr(9) & rdo(1)
    rdo.MoveNext
  Wend
  rdo.Close
End Sub

Private Function ComprobacionDatos() As Integer
Dim msg As String, ctrl As Integer, i As Integer, bk As Variant

  ComprobacionDatos = True
  If grdssMuestra(0).SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar las muestras que desee recepcionar", vbInformation + vbOKOnly, "Recepci�n de pruebas"
    ComprobacionDatos = False
  Else
    msg = "Debe especificar los siguientes datos:" & Chr$(13) & Chr$(13)
    ctrl = True
  
    If cbossRealiza(0).Text = "" Then
      msg = msg & Chr$(9) & "- Pat�logo responsable de la prueba." & Chr$(13)
      ctrl = False
    End If
    If cbossRealiza(3).Text = "" Then
      msg = msg & Chr$(9) & "- Persona que efectua la recepci�n." & Chr$(13)
      ctrl = False
    End If
    If objPr.rdoCursor("AP60_TiPr") = apCODBIOPSIA Then
      For i = 0 To grdssMuestra(0).SelBookmarks.Count - 1
        bk = grdssMuestra(0).SelBookmarks(i)
        If grdssMuestra(0).Columns("Fijador").CellText(bk) = "" Then
          msg = msg & Chr$(9) & "- Fijador." & Chr$(13)
          ctrl = False
          Exit For
        End If
      Next i
    End If
  '  If chkPaciente.Value = True Then
  '    If cbossPeti(0).Text = "" Then
  '      msg = msg & Chr$(9) & "- Centro externo que remite la prueba." & Chr$(13)
  '      ctrl = False
  '    End If
  '    If cbossPeti(1).Text = "" Then
  '      msg = msg & Chr$(9) & "- Pat�logo externo que remite la prueba." & Chr$(13)
  '      ctrl = False
  '    End If
    'End If
    If ctrl = False Then
      MsgBox msg, vbInformation + vbOKOnly, "Recepci�n de pruebas"
      ComprobacionDatos = False
    End If
  End If
End Function

Private Sub cbossRealiza_Click(Index As Integer)
'  Call Cod(Index)
End Sub

Private Sub cbossRealiza_CloseUp(Index As Integer)
  Call cod(Index)
End Sub

Private Sub cbossRealiza_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'  Call Cod(Index)
End Sub

Sub cod(Index As Integer)
  On Error Resume Next
  Select Case Index
    Case 0
      cPat = cbossRealiza(Index).Columns(0).Text
      If cbossRealiza(1).Text = "" Then cRes = ResAsoc(cPat)
    Case 1
      cRes = cbossRealiza(Index).Columns(0).Text
    Case 2
      cCito = cbossRealiza(Index).Columns(0).Text
    Case 3
      cRecep = cbossRealiza(Index).Columns(0).Text
  End Select

End Sub
Function ResAsoc(cPat As String) As String
Dim rdoQ As rdoQuery, sql As String, rdo As rdoResultset
    
  sql = "SELECT AP0800.SG02Cod_Aso, SG0200.SG02TxtFirma FROM SG0200, AP0800 WHERE " _
      & "AP0800.SG02Cod_Aso = SG0200.SG02Cod AND AP0800.SG02Cod = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cPat
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    ResAsoc = rdo(0)
    cbossRealiza(1).Text = rdo(1)
  Else
    ResAsoc = ""
  End If

End Function
Function InsertPr() As Integer
Dim sql As String
Dim rdoQ As rdoQuery
      
  If cbossRealiza(1).Text = "" Then cRes = ""
  If cbossRealiza(2).Text = "" Then cCito = ""
  nRef = Format$(Hoy(), "YYYY") & SiguienteNum(TiPr)
  Call objPipe.PipeSet("NumRef", nRef)

  sql = "INSERT INTO AP2100 (AP21_CodRef, PR04NUMACTPLAN, PR01CODACTUACION , AP21_TIPAC, " _
        & "CI22NUMHISTORIA, AP11_CODCTRO, AP15_CodPat, AD02CODDPTO, SG02COD_SOTE, " _
        & "AP21_FECSOTUD, AP21_FECGen, SG02Cod_PAT, SG02Cod_RES, SG02Cod_CITO, " _
        & "AP45_CODESTPR)" _
        & " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " _
        & "SYSDATE, ? , ?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = objPr.rdoCursor("PR04NUMACTPLAN")
  rdoQ(2) = objPr.rdoCursor("PR01CodActuacion")
  rdoQ(3) = TiPac
  rdoQ(4) = objPr.rdoCursor("CI22NUMHISTORIA")
  If TiPac = 2 Then
    rdoQ(5) = cCtro
    rdoQ(6) = cPeti
  Else
    rdoQ(5) = 1
    rdoQ(6) = ""
  End If
  rdoQ(7) = objPr.rdoCursor("AD02CodDpto")
  rdoQ(8) = objPr.rdoCursor("SG02Cod")
  rdoQ(9) = Format$(objPr.rdoCursor("PR09FecPeticion"), "DD/MM/YYYY HH:MM:SS")
  rdoQ(10) = cPat
  rdoQ(11) = cRes
  rdoQ(12) = cCito
  rdoQ(13) = apPRUEBARECEPCIONANDO
  rdoQ.Execute
  InsertPr = rdoQ.RowsAffected
End Function

Function ActualizarPR04() As Integer
Dim sql As String
Dim rdoQ As rdoQuery
      
  
  If grdssMuestra(0).SelBookmarks.Count <> grdssMuestra(0).Rows Then
    sql = "UPDATE PR0400 SET PR37CodEstado = ?, " _
        & "PR04FecIniAct = SYSDATE, " _
        & "PR04NumIDActDePr = ? " _
        & "WHERE PR04NumActPlan = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = apPRUEBARECEPCIONADA
    rdoQ(1) = nRef
    rdoQ(2) = objPr.rdoCursor("PR04NumActPlan")
    rdoQ.Execute sql
    ActualizarPR04 = rdoQ.RowsAffected
  Else
    ActualizarPR04 = 1
  End If
End Function

Function InsertMuestra() As Integer
Dim i As Integer, res As Integer, sql As String, bk As Variant, NumBloq As Integer
Dim rdoQ As rdoQuery

  res = 0
  sql = "INSERT INTO AP2500 (AP21_CODREF, PR52NumIDMuestra, PR52NumMuestra, AP25_FECRECEP," _
        & "SG02Cod, AP19_CODFIJA, AP22_CODDECAL, AP25_OBSERV, AP46_CODESTMUES) VALUES " _
        & "(?, ?, ?, SYSDATE, ?, ?, ?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(3) = cRecep
  rdoQ(7) = apMUESTRARECEPCIONADA
  MuesRecep = "("
  For i = 0 To grdssMuestra(0).SelBookmarks.Count - 1
    bk = grdssMuestra(0).SelBookmarks(i)
    rdoQ(1) = grdssMuestra(0).Columns("N�").CellValue(bk)
    rdoQ(2) = grdssMuestra(0).Columns("nMues").CellValue(bk)
    rdoQ(4) = grdssMuestra(0).Columns("Fijador").CellValue(bk)
    rdoQ(5) = grdssMuestra(0).Columns("Decalcificador").CellValue(bk)
    rdoQ(6) = grdssMuestra(0).Columns("Observaciones").CellText(bk)
    rdoQ.Execute
    Call ActualizarPR5200(rdoQ(2))
    MuesRecep = MuesRecep & rdoQ(1) & ", "
    If TiPr = apCODCITO Then
      NumBloq = InsertBloqueCito(rdoQ(1))
      Call InsertarPerfil(NumBloq, grdssMuestra(0).Columns("cMues").CellValue(bk))
    End If
  Next i
  MuesRecep = Left(MuesRecep, Len(MuesRecep) - 2) & ")"
  InsertMuestra = rdoQ.RowsAffected
End Function

Sub ActualizarPR5200(nMues As Long)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE PR5200 SET PR55CodEstado = ? WHERE PR52NumMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = 2
  rdoQ(1) = nMues
  rdoQ.Execute
End Sub
Function InsertBloqueCito(nMues As Integer) As Integer
Dim NumBloq As Integer, sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT MAX(AP34_CodBloq) FROM AP3400 WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If IsNull(rdo(0)) Then NumBloq = 1 Else NumBloq = rdo(0) + 1
  rdoQ.Close
  
  sql = "INSERT INTO AP3400 (AP21_CodRef, PR52NumIDMuestra, AP34_CodBloq, " _
      & "AP34_FecSotud, AP34_FecTalla, AP05_CodTiBloq, AP47_CodEstBloq) " _
      & "VALUES (?, ?, ?, SYSDATE, SYSDATE, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = nMues
  rdoQ(2) = NumBloq
  rdoQ(3) = apBLOQUECITOLOGIAS  ' Bloque virtual de las citolog�as
  rdoQ(4) = apBLOQUECORTADO
  rdoQ.Execute sql
  InsertBloqueCito = NumBloq
  
End Function

Sub InsertarPerfil(cBloq As Integer, cOrg As Integer)
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim sql As String
Dim i As Integer

  sql = "SELECT AP12_CodTec, AP27_NumTec FROM AP2700 WHERE AP01_CodPerf IN (" _
      & "SELECT AP01_CodPerf FROM AP5800 WHERE PR24CodMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cOrg
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  
  sql = "INSERT INTO AP3600 (AP21_CodRef, AP34_CodBloq, AP36_CodPorta, AP12_CodTec, " _
      & "AP36_FecSotud, AP48_CodEstPorta) VALUES " _
      & "(?, ?, ?, ?, SYSDATE, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = cBloq
  rdoQ(4) = apPORTASOLICITADO
  While rdo.EOF = False
    For i = 1 To rdo(1)
      rdoQ(2) = SigPorta(nRef, cBloq)
      rdoQ(3) = rdo(0)
      rdoQ.Execute
    Next i
    rdo.MoveNext
  Wend
  Call ActualizarEstPr_Cont(nRef)

End Sub

