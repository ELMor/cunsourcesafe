VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmI_HT 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2430
   ClientLeft      =   1125
   ClientTop       =   1575
   ClientWidth     =   8235
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2430
   ScaleWidth      =   8235
   Begin VB.Frame Frame1 
      Caption         =   "Tipos de pruebas"
      ForeColor       =   &H00800000&
      Height          =   1965
      Left            =   4800
      TabIndex        =   4
      Top             =   150
      Width           =   1590
      Begin VB.OptionButton optTiPr 
         Caption         =   "Biopsias"
         Height          =   315
         Index           =   0
         Left            =   150
         TabIndex        =   8
         Top             =   300
         Width           =   1290
      End
      Begin VB.OptionButton optTiPr 
         Caption         =   "Citolog�as"
         Height          =   315
         Index           =   1
         Left            =   150
         TabIndex        =   7
         Top             =   675
         Width           =   1290
      End
      Begin VB.OptionButton optTiPr 
         Caption         =   "Necropsias"
         Height          =   315
         Index           =   2
         Left            =   150
         TabIndex        =   6
         Top             =   1050
         Width           =   1290
      End
      Begin VB.OptionButton optTiPr 
         Caption         =   "Todas"
         Height          =   315
         Index           =   3
         Left            =   150
         TabIndex        =   5
         Top             =   1425
         Value           =   -1  'True
         Width           =   1290
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   6750
      TabIndex        =   2
      Top             =   1650
      Width           =   1215
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   6750
      TabIndex        =   1
      Top             =   300
      Width           =   1215
   End
   Begin VB.CommandButton cmdImprimirTodo 
      Caption         =   "&Imprimir Todo"
      Height          =   375
      Left            =   6750
      TabIndex        =   0
      Top             =   750
      Width           =   1215
   End
   Begin ComctlLib.ListView lvwDatos 
      Height          =   1890
      Left            =   225
      TabIndex        =   3
      Top             =   225
      Width           =   4365
      _ExtentX        =   7699
      _ExtentY        =   3334
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "frmI_HT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rptCrystal As CrystalReport
Dim Estado As Integer

Private Sub cmdImprimir_Click()
Dim sql As String
Dim cBloque As Integer, cCarpeta As Integer, i As Integer
Dim item As ListItem
  
  cBloque = 0
  If lvwDatos.ListItems.Count > 0 Then
    Select Case objPipe.PipeGet("menu")
      Case "Tallado"
        sql = "{AP3400.AP47_CODESTBLOQ} = 1 AND ("
        For Each item In lvwDatos.ListItems
          If item.Selected = True Then
            cBloque = Right(item.key, Len(item.key) - 1)
            sql = sql & "{AP3400.AP05_CODTIBLOQ} = " & cBloque & " OR "
          End If
        Next item
        If cBloque = 0 Then
          MsgBox "Debe seleccionar alg�n tipo de bloque.", vbInformation, "Selecci�n incorrecta"
        Else
          sql = Left(sql, Len(sql) - 4) & ")"
          If optTiPr(0).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 1"
          ElseIf optTiPr(1).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 2"
          ElseIf optTiPr(2).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 3"
          End If
          Call ImprimirHT(sql, "ap2301.rpt")
        End If
      
      Case "Realizacion"
        sql = "{AP3400.AP47_CODESTBLOQ} = 2 AND ("
        For Each item In lvwDatos.ListItems
          If item.Selected = True Then
            cBloque = Right(item.key, Len(item.key) - 1)
            sql = sql & "{AP3400.AP05_CODTIBLOQ} = " & cBloque & " OR "
          End If
        Next item
        If cBloque = 0 Then
          MsgBox "Debe seleccionar alg�n tipo de bloque.", vbInformation, "Selecci�n incorrecta"
        Else
          sql = Left(sql, Len(sql) - 4) & ")"
          If optTiPr(0).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 1"
          ElseIf optTiPr(1).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 2"
          ElseIf optTiPr(2).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 3"
          End If
          Call ImprimirHT(sql, "ap2302.rpt")
        End If
      
      Case "Corte"
        sql = "("
        For Each item In lvwDatos.ListItems
          If item.Selected = True Then
            cBloque = Right(item.key, Len(item.key) - 1)
            sql = sql & "{AP3400.AP05_CODTIBLOQ} = " & cBloque & " OR "
'            sql = "{AP3400.AP05_CODTIBLOQ} = " & cBloque
'            Call Imprimir(sql, "ap002303.rpt")
          End If
        Next item
        If cBloque = 0 Then
          MsgBox "Debe seleccionar alg�n tipo de bloque.", vbInformation, "Selecci�n incorrecta"
        Else
          sql = " AND " & Left(sql, Len(sql) - 4) & ")"
          If optTiPr(0).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 1"
          ElseIf optTiPr(1).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 2"
          ElseIf optTiPr(2).Value = True Then
            sql = sql & " AND {AP0901J.AP60_TiPr} = 3"
          End If
          rptCrystal.Formulas(0) = "Titulo= 'Pruebas Continuadas'"
          Call ImprimirHT("{AP2100.AP45_CODESTPR} >= " & apPRUEBACONTINUADA & sql, "ap2303.rpt")
          rptCrystal.Formulas(0) = "Titulo= 'Pruebas Nuevas'"
          Call ImprimirHT("{AP2100.AP45_CODESTPR} < " & apPRUEBACONTINUADA & sql, "ap2303.rpt")
        End If
      
      Case "Tecnicas"
        sql = "{AP3601J.AP48_CODESTPORTA} >= " & apPORTASOLICITADO & " AND " & _
              "{AP3601J.AP48_CODESTPORTA} <= " & apPORTACORTADO & " AND ("
        For Each item In lvwDatos.ListItems
          If item.Selected = True Then
            cCarpeta = Right(item.key, Len(item.key) - 1)
            sql = sql & "{AP3601J.AP06_CodCarp} = " & cCarpeta & " OR "
          End If
        Next item
        sql = Left(sql, Len(sql) - 4) & ")"
        If optTiPr(0).Value = True Then
          sql = sql & " AND {AP0901J.AP60_TiPr} = 1"
        ElseIf optTiPr(1).Value = True Then
          sql = sql & " AND {AP0901J.AP60_TiPr} = 2"
        ElseIf optTiPr(2).Value = True Then
          sql = sql & " AND {AP0901J.AP60_TiPr} = 3"
        End If
        If cCarpeta = 0 Then
          MsgBox "Debe seleccionar alguna carpeta.", vbInformation, "Selecci�n incorrecta"
        Else
          Call ImprimirHT(sql, "ap2304_1.rpt")
        End If
    
    End Select
  End If
End Sub
Sub Imprimir(sql As String, report As String)
Dim hwndPreviewWindow As Long
Dim res As Integer
    
    With rptCrystal
      .ReportFileName = App.Path & "\rpt\" & report
      If .Connect = "" Then
          .Connect = objApp.rdoConnect.Connect
      End If
      .WindowControls = True
      .WindowControlBox = True
      .WindowTitle = Me.Caption
      .WindowState = crptMaximized
'      .Destination = crptToPrinter
      .Destination = crptToWindow
      .SelectionFormula = sql
      res = .PrintReport
      .SelectionFormula = ""
      .WindowTitle = ""
      .ReportFileName = ""
      .Formulas(0) = ""
    End With
End Sub
Function NombreFichero() As String
Dim ruta As String
  
  On Error Resume Next
'  ruta = APP.Path & "\hojas\"
  Select Case objPipe.PipeGet("menu")
    Case "Tallado"
      ruta = ruta & "extracc\"
    Case "Corte"
      ruta = ruta & "corte\"
    Case "Tecnicas"
      ruta = ruta & "tecnica\"
  End Select
  NombreFichero = FicheroMasAntiguo(ruta)
  Exit Function

End Function
Function FicheroMasAntiguo(ruta As String) As String
Dim i As Integer, pos As Integer, Nombre As String, fecha As Date
Dim fich() As Att
      
  On Error Resume Next
  ReDim fich(1 To NumHojas)
  Err = 0
  GetAttr (ruta)
  If Err <> 0 Then Call ConstruirArbol
  Err = 0
  Nombre = Dir(ruta)
  i = 0
  While Nombre <> ""
    i = i + 1
    fich(i).Nombre = Nombre
    fich(i).fecha = FileDateTime(ruta & Nombre)
    Nombre = Dir
  Wend
  If i < NumHojas Then
    FicheroMasAntiguo = ruta & "Hoja" & i + 1 & ".rtf"
  Else
    pos = 1
    For i = 1 To NumHojas - 1
      If DateDiff("s", fich(i).fecha, fich(pos).fecha) > 0 Then
        pos = i
      End If
    Next i
    FicheroMasAntiguo = ruta & fich(pos).Nombre
  End If

End Function
Sub ConstruirArbol()
  
  On Error Resume Next
  Err = 0
'  GetAttr APP.Path & "\hojas\"
'  If Err <> 0 Then MkDir APP.Path & "\hojas\"
  Err = 0
'  GetAttr APP.Path & "\hojas\extracc\"
'  If Err <> 0 Then MkDir APP.Path & "\hojas\extracc\"
  Err = 0
'  GetAttr APP.Path & "\hojas\corte\"
'  If Err <> 0 Then MkDir APP.Path & "\hojas\corte\"
  Err = 0
'  GetAttr APP.Path & "\hojas\tecnica\"
'  If Err <> 0 Then MkDir APP.Path & "\hojas\tecnica\"
  
End Sub
Sub ActualizarEstadoBloques(EstIni As Integer, EstFin As Integer)
Dim sql As String, TiBloque As String, cBloque As Integer
Dim rdoQ As rdoQuery
Dim i As Integer

  For i = 1 To lvwDatos.ListItems.Count
    If lvwDatos.ListItems(i).Selected = True Then
      cBloque = Right(lvwDatos.ListItems(i).key, Len(lvwDatos.ListItems(i).key) - 1)
      TiBloque = TiBloque & cBloque & ","
    End If
  Next i
  TiBloque = Left(TiBloque, Len(TiBloque) - 1)
  
' No le paso por par�metros el contenido del IN porque no funciona si tiene m�s de un valor
  sql = "UPDATE AP3400 SET AP47_CodEstBloq = ? WHERE " _
      & "AP47_CodEstBloq = ? AND AP05_CodTiBloq IN (" & TiBloque & ")"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = EstFin
  rdoQ(1) = EstIni
  rdoQ.Execute sql

End Sub
Sub ActualizarEstadoPortasCorte()
Dim sql As String, TiBloque As String, cBloque As Integer
Dim rdoQ As rdoQuery
Dim i As Integer

  For i = 1 To lvwDatos.ListItems.Count
    If lvwDatos.ListItems(i).Selected = True Then
      cBloque = Right(lvwDatos.ListItems(i).key, Len(lvwDatos.ListItems(i).key) - 1)
      TiBloque = TiBloque & cBloque & ","
    End If
  Next i
  TiBloque = Left(TiBloque, Len(TiBloque) - 1)
  
' No le paso por par�metros el contenido del IN porque no funciona si tiene m�s de un valor
  sql = "UPDATE AP3600 SET AP48_CodEstPorta = ? WHERE " _
      & "AP48_CodEstPorta = ? AND (AP21_CodRef, AP34_CodBloq) IN (" _
      & "SELECT AP21_CodRef, Ap34_CodBloq FROM AP3400 WHERE AP05_CodTiBloq IN " _
      & "(" & TiBloque & "))"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = apPORTACORTADO
  rdoQ(1) = apPORTASOLICITADO
  rdoQ.Execute sql

End Sub
Sub ActualizarEstadoPortasReali()
Dim sql As String, TiCarpeta As String, cCarpeta As Integer
Dim rdoQ As rdoQuery
Dim i As Integer

  For i = 1 To lvwDatos.ListItems.Count
    If lvwDatos.ListItems(i).Selected = True Then
      cCarpeta = Right(lvwDatos.ListItems(i).key, Len(lvwDatos.ListItems(i).key) - 1)
      TiCarpeta = TiCarpeta & cCarpeta & ","
    End If
  Next i
  TiCarpeta = Left(TiCarpeta, Len(TiCarpeta) - 1)
  
' No le paso por par�metros el contenido del IN porque no funciona si tiene m�s de un valor
  sql = "UPDATE AP3600 SET AP48_CodEstPorta = ? WHERE " _
      & "AP48_CodEstPorta = ? AND AP12_CodTec IN (" _
      & "SELECT DISTINCT AP1200.AP12_CodTec FROM AP1200, AP0400 WHERE " _
      & "AP1200.AP04_CodTiTec = AP0400.AP04_CodTiTec AND " _
      & "AP0400.AP06_CodCarp IN " _
      & "(" & TiCarpeta & "))"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = apPORTAREALIZADO
  rdoQ(1) = apPORTACORTADO
  rdoQ.Execute sql

End Sub


Private Sub cmdImprimirTodo_Click()
  
  If lvwDatos.ListItems.Count > 0 Then
    Select Case objPipe.PipeGet("menu")
      Case "Tallado"
        Call ImprimirHT("", "ap2301.rpt")
      Case "Realizacion"
        Call ImprimirHT("", "ap2302.rpt")
      Case "Corte"
        Call ImprimirHT("", "ap2303.rpt")
      Case "Tecnicas"
        Call ImprimirHT("", "ap2304.rpt")
    End Select
  End If
End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  
  Select Case objPipe.PipeGet("menu")
    Case "Tallado"
      Me.Caption = "Hoja de trabajo de tallado de bloques"
      Estado = apBLOQUESOLICITADO
      Call FormatearBloquesTallar
      Call LlenarlvwBloquesSolicitados
    Case "Realizacion"
      Me.Caption = "Hoja de trabajo de realizaci�n de bloques"
      Estado = apBLOQUETALLADO
      Call FormatearBloquesRealizar
      Call LlenarlvwBloquesTallados
    Case "Corte"
      Me.Caption = "Hoja de trabajo de corte"
      Estado = apBLOQUEREALIZADO
      Call FormatearBloquesCorte
      Call LlenarlvwBloquesRealizados
    Case "Tecnicas"
      Me.Caption = "Hoja de trabajo de realizaci�n de t�cnicas"
      Call FormatearCarpetas
      Call LlenarlvwCarpetas
  End Select
  Set rptCrystal = frmPrincipal.crCrystalReport1

End Sub
Sub FormatearBloquesCorte()
  With lvwDatos
    .ColumnHeaders.Add , , "Tipo de bloque", 1600
    .ColumnHeaders.Add , , "N� de bloques", 1000
    .ColumnHeaders.Add , , "N� de portas", 800
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
End Sub
Sub FormatearBloquesTallar()
  With lvwDatos
    .ColumnHeaders.Add , , "Tipo de bloque", 1800
    .ColumnHeaders.Add , , "N� de bloques", 1000
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
End Sub
Sub FormatearBloquesRealizar()
  With lvwDatos
    .ColumnHeaders.Add , , "Tipo de bloque", 1800
    .ColumnHeaders.Add , , "N� de bloques", 1000
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
End Sub
Sub FormatearCarpetas()
  With lvwDatos
    .ColumnHeaders.Add , , "Carpeta", 1800
    .ColumnHeaders.Add , , "N� T�cnicas", 1000
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
End Sub
Sub LlenarlvwBloquesSolicitados()
Dim sql As String
Dim rdo As rdoResultset, rdo1 As rdoResultset
Dim rdoQ As rdoQuery, rdoQ1 As rdoQuery
Dim item As ListItem

  sql = "SELECT AP05_CodTiBloq, Count(AP21_CodRef) FROM AP3400 WHERE " _
      & "AP3400.AP47_CodEstBloq = ? GROUP BY AP05_CodTiBloq"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = Estado
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    sql = "SELECT AP05_Desig FROM AP0500 WHERE AP05_CodTiBloq = ?"
    Set rdoQ1 = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ1(0).Type = rdTypeINTEGER
    rdoQ1(0) = rdo(0)
    Set rdo1 = rdoQ1.OpenResultset(rdOpenForwardOnly)
    Set item = lvwDatos.ListItems.Add(, "B" & rdo(0), rdo1(0))
    item.SubItems(1) = CStr(rdo(1))
    rdo1.Close
    rdo.MoveNext
  Wend
  rdo.Close
'  Call CerrarrdoQueries

End Sub
Sub LlenarlvwBloquesTallados()
Dim sql As String
Dim rdo As rdoResultset, rdo1 As rdoResultset
Dim rdoQ As rdoQuery, rdoQ1 As rdoQuery
Dim item As ListItem

  sql = "SELECT AP0500.AP05_Desig, AP3400.AP05_CodTiBloq, COUNT(*) " _
      & "FROM AP0500, AP3400 WHERE " _
      & "AP0500.AP05_CodTiBloq = AP3400.AP05_CodTiBloq AND " _
      & "AP3400.AP47_CodEstBloq = ? " _
      & "GROUP BY AP0500.AP05_Desig, AP3400.AP05_CodTiBloq"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = Estado
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwDatos.ListItems.Add(, "B" & rdo(1), rdo(0))
    item.SubItems(1) = CStr(rdo(2))
    rdo.MoveNext
  Wend
  rdo.Close
'  Call CerrarrdoQueries

End Sub
Sub LlenarlvwBloquesRealizados()
Dim sql As String
Dim rdo As rdoResultset, rdo1 As rdoResultset
Dim rdoQ As rdoQuery, rdoQ1 As rdoQuery
Dim item As ListItem

  sql = "SELECT AP0500.AP05_Desig, AP3400.AP05_CodTiBloq, " _
      & "COUNT(DISTINCT(AP3400.AP21_CodRef || AP3400.AP34_CodBloq)), COUNT(*) " _
      & "FROM AP0500, AP3400, AP3600 WHERE " _
      & "AP0500.AP05_CodTiBloq = AP3400.AP05_CodTiBloq AND " _
      & "AP3400.AP21_CodRef = AP3600.AP21_CodRef AND " _
      & "AP3400.AP34_CodBloq = AP3600.AP34_CodBloq AND " _
      & "AP48_CodEstPorta = ? " _
      & "GROUP BY AP0500.AP05_Desig, AP3400.AP05_CodTiBloq"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = apPORTASOLICITADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwDatos.ListItems.Add(, "B" & rdo(1), rdo(0))
    item.SubItems(1) = CStr(rdo(2))
    item.SubItems(2) = CStr(rdo(3))
    rdo.MoveNext
  Wend
  rdo.Close
'  Call CerrarrdoQueries

End Sub
Sub LlenarlvwCarpetas()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem

  sql = "SELECT AP0600.AP06_CodCarp, AP0600.AP06_Desig, Count(AP3600.AP21_CodRef) FROM " _
      & "AP0600, AP0400, AP1200, AP3600 WHERE " _
      & "AP0600.AP06_CodCarp = AP0400.AP06_CodCarp AND " _
      & "AP0400.AP04_CodTiTec = AP1200.AP04_CodTiTec AND " _
      & "AP1200.AP12_CodTec = AP3600.AP12_CodTec AND " _
      & "AP3600.AP48_CodEstPorta >= ? AND " _
      & "AP3600.AP48_CodEstPorta <= ? GROUP BY " _
      & "AP0600.AP06_CodCarp, AP0600.AP06_Desig"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = apPORTASOLICITADO
  rdoQ(1) = apPORTACORTADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwDatos.ListItems.Add(, "C" & rdo(0), rdo(1))
    item.SubItems(1) = CStr(rdo(2))
    rdo.MoveNext
  Wend
  rdo.Close
'  Call CerrarrdoQueries

End Sub


