VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
'Const APRepProblemas            As String = "AP0001"
'Const APRepMicroMuestra         As String = "AP0002"
'Const APRepAlmacenColonias      As String = "AP0003"
'Const APRepAislamientos         As String = "AP0004"
'Const APRepMicroAntibiograma    As String = "AP0005"
'Const APRepMicroTecnicas        As String = "AP0006"
'Const APRepBionumeros           As String = "AP0007"
'Const APRepDesglosePeticiones   As String = "AP0008"
'Const APRepMicroEdades          As String = "AP0009"

' Ahora las ventanas comunes a m�s de un proceso
Const APWinI_HT_Tallado             As String = "AP3001"
Const APWinI_HT_Bloques             As String = "AP3002"
Const APWinI_HT_Corte               As String = "AP3003"
Const APWinI_HT_Tincion             As String = "AP3004"
Const APWinL_HT_Tallado             As String = "AP3005"
Const APWinL_HT_Bloques             As String = "AP3006"
Const APWinL_HT_Corte               As String = "AP3007"
Const APWinL_HT_Tincion             As String = "AP3008"

Const APWinI_Reali_Tallado          As String = "AP6101"
Const APWinI_Reali_Bloques          As String = "AP6102"
Const APWinI_Reali_Corte            As String = "AP6103"
Const APWinI_Reali_Tincion          As String = "AP6104"

' Ahora las ventanas. Continuan la numeraci�n a partir del AP1000
Const APWinG_Bloques                As String = "AP53"
Const APWinG_BuscPr                 As String = "AP49"
Const APWinG_Datos                  As String = "AP50"
Const APWinG_DatosPr                As String = "AP62"
Const APWinG_Diag                   As String = "AP51"
Const APWinG_Discrep                As String = "AP63"
Const APWinG_IDDR                   As String = "AP16"
Const APWinG_Impronta               As String = "AP71"
Const APWinG_Interes                As String = "AP42"
Const APWinG_ModMuestras            As String = "AP69"
Const APWinG_Necropsia              As String = "AP72"
Const APWinG_NumRef                 As String = "AP70"
Const APWinG_PrAnteriores           As String = "AP39"
Const APWinG_PrExterna              As String = "AP65"
Const APWinI_HT                     As String = "AP30"
Const APWinI_Reali                  As String = "AP61"
Const APWinI_Recepcion              As String = "AP60"
Const APWinP_Descripcion            As String = "AP46"
Const APWinP_Fotos                  As String = "AP22"
Const APWinP_Peticion               As String = "AP23"
Const APWinP_Recepcion              As String = "AP25"

'Constantes pata los HelpContextID de las ventanas
'Const AGHelpIDTipoRest             As Integer = 26
'Const AGHelpIDTipoRecursos         As Integer = 1
'Const AGHelpIDCalendarios          As Integer = 3
'Const AGHelpIDRecursos             As Integer = 6
'Const AGHelpIDRestriccionesR       As Integer = 15
'Const AGHelpIDRestriccionesF       As Integer = 20
'Const AGHelpIDPerfiles             As Integer = 9
'Const AGHelpIDFranjas              As Integer = 18
'Const AGHelpIDIncidencias          As Integer = 0
'Const AGHelpIDAgendaRecurso        As Integer = 24
'Const AGHelpIDDietarioRecurso      As Integer = 25
'Const AGHelpIDMotivosIncidencia    As Integer = 29


' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

'  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case APWinG_Bloques  ' No se acceden a trav�s del Launcher
    Case APWinG_BuscPr
    Case APWinG_Datos
    Case APWinG_DatosPr
    Case APWinG_Diag
    Case APWinG_Discrep
    Case APWinG_IDDR
    Case APWinG_Impronta
    Case APWinG_Interes
    Case APWinG_ModMuestras
    Case APWinG_Necropsia
    Case APWinG_NumRef
    Case APWinG_PrAnteriores
    Case APWinG_PrExterna
    
    Case APWinI_HT ' Se desglosa en otros procesos:
    Case APWinI_HT_Tallado
      PEOpenEngine
      Call objPipe.PipeSet("Menu", "Tallado")
      Call frmI_HT.Show(vbModal)
      Set frmI_HT = Nothing
    Case APWinI_HT_Bloques
      PEOpenEngine
      Call objPipe.PipeSet("Menu", "Realizacion")
      Call frmI_HT.Show(vbModal)
      Set frmI_HT = Nothing
    Case APWinI_HT_Corte
      PEOpenEngine
      Call objPipe.PipeSet("Menu", "Corte")
      Call frmI_HT.Show(vbModal)
      Set frmI_HT = Nothing
    Case APWinI_HT_Tincion
      PEOpenEngine
      Call objPipe.PipeSet("Menu", "Tecnicas")
      Call frmI_HT.Show(vbModal)
      Set frmI_HT = Nothing

'    Case APWinI_Reali ' Se desglosa en otros procesos:
    Case APWinI_Reali_Tallado
      Call objPipe.PipeSet("Menu", "Tallado")
      Call frmI_Reali.Show(vbModal)
      Set frmI_Reali = Nothing
    Case APWinI_Reali_Bloques
      Call objPipe.PipeSet("Menu", "Realizacion")
      Call frmI_Reali.Show(vbModal)
      Set frmI_Reali = Nothing
    Case APWinI_Reali_Corte
      Call objPipe.PipeSet("Menu", "Corte")
      Call frmI_Reali.Show(vbModal)
      Set frmI_Reali = Nothing
    Case APWinI_Reali_Tincion
      Call objPipe.PipeSet("Menu", "Tecnicas")
      Call frmI_Reali.Show(vbModal)
      Set frmI_Reali = Nothing

'    Case APWinP_Descripcion
'      Call objPipe.PipeSet("Menu", "Descripci�n")
'      Call frmBuscPr.Show(vbModal)
'      Set frmBuscPr = Nothing
    Case APWinP_Fotos
      Call objPipe.PipeSet("Menu", "Fotos")
      Call frmBuscPr.Show(vbModal)
      Set frmBuscPr = Nothing
    Case APWinP_Peticion
      Call objPipe.PipeSet("Menu", "Petici�n")
      Call frmBuscPr.Show(vbModal)
      Set frmBuscPr = Nothing
    Case APWinP_Recepcion
'      Call objPipe.PipeSet("Menu", "Recepcion")
      Call frmP_Recepcion.Show(vbModal)
      Set frmP_Recepcion = Nothing
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  
'  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n� de procesos que se definan
ReDim aProcess(1 To 16, 1 To 4) As Variant
  
  aProcess(1, 1) = APWinP_Recepcion
  aProcess(1, 2) = "Recepci�n de muestras"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = APWinP_Peticion
  aProcess(2, 2) = "Gesti�n de pruebas"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = APWinP_Fotos
  aProcess(3, 2) = "Fotograf�as"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = APWinP_Descripcion
  aProcess(4, 2) = "Mecanografiado de documentos"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = APWinI_HT_Tallado
  aProcess(5, 2) = "Hoja de trabajo de tallado de bloques"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeReport
      
  aProcess(6, 1) = APWinI_HT_Bloques
  aProcess(6, 2) = "Hoja de trabajo de bloques"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeReport
      
  aProcess(7, 1) = APWinI_HT_Corte
  aProcess(7, 2) = "Hoja de trabajo de portas"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeReport
      
  aProcess(8, 1) = APWinI_HT_Tincion
  aProcess(8, 2) = "Hoja de trabajo de t�cnicas"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeReport
      
  aProcess(9, 1) = APWinI_Reali_Tallado
  aProcess(9, 2) = "Tallado de bloques"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = APWinI_Reali_Bloques
  aProcess(10, 2) = "Realizaci�n de bloques"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = APWinI_Reali_Corte
  aProcess(11, 2) = "Realizaci�n de portas"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = APWinI_Reali_Tincion
  aProcess(12, 2) = "Realizaci�n de t�cnicas"
  aProcess(12, 3) = True
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = APWinL_HT_Tallado
  aProcess(13, 2) = "HT de tallado de bloques"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeReport
      
  aProcess(14, 1) = APWinL_HT_Bloques
  aProcess(14, 2) = "HT de realizaci�n de bloques"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeReport
      
  aProcess(15, 1) = APWinL_HT_Corte
  aProcess(15, 2) = "HT de realizaci�n de portas"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeReport
      
  aProcess(16, 1) = APWinL_HT_Tincion
  aProcess(16, 2) = "HT de realizaci�n de t�cnicas"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeReport
      

End Sub

