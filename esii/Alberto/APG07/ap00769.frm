VERSION 5.00
Begin VB.Form frmModMuestras 
   Caption         =   "Modificaci�n de muestras"
   ClientHeight    =   1635
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6435
   LinkTopic       =   "Form1"
   ScaleHeight     =   1635
   ScaleWidth      =   6435
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtTiMues 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1200
      MaxLength       =   50
      TabIndex        =   0
      Tag             =   "Tipo de �rgano"
      ToolTipText     =   "Tipo de �rgano"
      Top             =   975
      Width           =   3690
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   390
      Left            =   5100
      TabIndex        =   2
      Top             =   900
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   5100
      TabIndex        =   1
      Top             =   225
      Width           =   1215
   End
   Begin VB.TextBox txtMuestra 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   1200
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   300
      Width           =   3675
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Descripci�n:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   75
      TabIndex        =   5
      Top             =   975
      Width           =   1140
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Muestra:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   20
      Left            =   300
      TabIndex        =   4
      Top             =   300
      Width           =   765
   End
End
Attribute VB_Name = "frmModMuestras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public seguir As Integer

Private Sub cmdAceptar_Click()
  seguir = True
  Me.Hide
End Sub

Private Sub cmdCancelar_Click()
  seguir = False
  Me.Hide
End Sub
Private Sub txtMuestra_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdAceptar_Click
    Case 27
      cmdCancelar_Click
  End Select
End Sub
