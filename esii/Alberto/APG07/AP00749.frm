VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmBuscPr 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Buscar pruebas"
   ClientHeight    =   6180
   ClientLeft      =   1950
   ClientTop       =   1830
   ClientWidth     =   8220
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6180
   ScaleWidth      =   8220
   Begin Threed.SSCommand cmdMenos 
      Height          =   390
      Left            =   6750
      TabIndex        =   28
      Top             =   1575
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   688
      _StockProps     =   78
      Caption         =   "&M�s"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "AP00749.frx":0000
   End
   Begin VB.Frame Frame1 
      Caption         =   "B�squeda de pruebas"
      ForeColor       =   &H00800000&
      Height          =   3840
      Left            =   225
      TabIndex        =   11
      Top             =   2175
      Width           =   7890
      Begin SSCalendarWidgets_A.SSDateCombo ssdcDesde 
         Height          =   315
         Left            =   6075
         TabIndex        =   24
         Top             =   2325
         Width           =   1665
         _Version        =   65537
         _ExtentX        =   2937
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
         ClipMode        =   1
      End
      Begin VB.CommandButton cmdPr 
         Caption         =   "&Buscar pruebas"
         Height          =   390
         Left            =   6225
         TabIndex        =   23
         Top             =   3225
         Width           =   1365
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   1740
         Left            =   225
         TabIndex        =   14
         Top             =   1950
         Width           =   5145
         _ExtentX        =   9075
         _ExtentY        =   3069
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Paciente"
         TabPicture(0)   =   "AP00749.frx":001C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtNom"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtAp2"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtAp1"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtNH"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Doctor"
         TabPicture(1)   =   "AP00749.frx":0038
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lvwDr"
         Tab(1).Control(1)=   "lvwDpt"
         Tab(1).ControlCount=   2
         Begin VB.TextBox txtNH 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1425
            TabIndex        =   18
            Top             =   300
            Width           =   750
         End
         Begin VB.TextBox txtAp1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1425
            TabIndex        =   17
            Top             =   600
            Width           =   2700
         End
         Begin VB.TextBox txtAp2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1425
            TabIndex        =   16
            Top             =   900
            Width           =   2700
         End
         Begin VB.TextBox txtNom 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1425
            TabIndex        =   15
            Top             =   1200
            Width           =   2700
         End
         Begin ComctlLib.ListView lvwDpt 
            Height          =   1590
            Left            =   -74850
            TabIndex        =   29
            Top             =   75
            Width           =   2265
            _ExtentX        =   3995
            _ExtentY        =   2805
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            _Version        =   327682
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "N� Ref."
               Object.Width           =   1941
            EndProperty
         End
         Begin ComctlLib.ListView lvwDr 
            Height          =   1590
            Left            =   -72525
            TabIndex        =   30
            Top             =   75
            Width           =   2265
            _ExtentX        =   3995
            _ExtentY        =   2805
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            _Version        =   327682
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Text            =   "N� Ref."
               Object.Width           =   3175
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Historia:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   225
            TabIndex        =   22
            Top             =   300
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 1:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   300
            TabIndex        =   21
            Top             =   600
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 2:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   300
            TabIndex        =   20
            Top             =   900
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   525
            TabIndex        =   19
            Top             =   1200
            Width           =   765
         End
      End
      Begin ComctlLib.ListView lvwPr 
         Height          =   1665
         Left            =   6225
         TabIndex        =   12
         Top             =   225
         Width           =   1590
         _ExtentX        =   2805
         _ExtentY        =   2937
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "N� Ref."
            Object.Width           =   1941
         EndProperty
      End
      Begin ComctlLib.ListView lvwPac 
         Height          =   1665
         Left            =   225
         TabIndex        =   13
         Top             =   225
         Width           =   5940
         _ExtentX        =   10478
         _ExtentY        =   2937
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Pacientes"
            Object.Width           =   9701
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo ssdcHasta 
         Height          =   315
         Left            =   6075
         TabIndex        =   25
         Top             =   2775
         Width           =   1665
         _Version        =   65537
         _ExtentX        =   2937
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   5475
         TabIndex        =   27
         Top             =   2850
         Width           =   690
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   5475
         TabIndex        =   26
         Top             =   2325
         Width           =   615
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Prueba"
      ForeColor       =   &H00800000&
      Height          =   1740
      Left            =   675
      TabIndex        =   7
      Top             =   225
      Width           =   4365
      Begin VB.TextBox txtNRef 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1125
         MaxLength       =   5
         TabIndex        =   0
         Top             =   525
         Width           =   750
      End
      Begin VB.TextBox txtAno 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1125
         MaxLength       =   4
         TabIndex        =   1
         Top             =   1050
         Width           =   750
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   1290
         Left            =   2475
         TabIndex        =   8
         Top             =   300
         Width           =   1590
         _Version        =   65536
         _ExtentX        =   2805
         _ExtentY        =   2275
         _StockProps     =   14
         Caption         =   "Tipo de prueba"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.OptionButton optTiPr 
            Caption         =   "Biopsia"
            Height          =   240
            Index           =   0
            Left            =   225
            TabIndex        =   2
            Top             =   300
            Width           =   1290
         End
         Begin VB.OptionButton optTiPr 
            Caption         =   "Citolog�a"
            Height          =   240
            Index           =   1
            Left            =   225
            TabIndex        =   3
            Top             =   600
            Width           =   1290
         End
         Begin VB.OptionButton optTiPr 
            Caption         =   "Autopsia"
            Height          =   240
            Index           =   2
            Left            =   225
            TabIndex        =   4
            Top             =   900
            Width           =   1290
         End
      End
      Begin VB.Label lblLabel1 
         Caption         =   "N� Ref:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   20
         Left            =   375
         TabIndex        =   10
         Top             =   525
         Width           =   690
      End
      Begin VB.Label lblLabel1 
         Caption         =   "A�o:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   600
         TabIndex        =   9
         Top             =   1050
         Width           =   465
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   6750
      TabIndex        =   6
      Top             =   825
      Width           =   1215
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   6750
      TabIndex        =   5
      Top             =   300
      Width           =   1215
   End
End
Attribute VB_Name = "frmBuscPr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public rdo As rdoResultset
'Public rdoPac As rdoResultset
Dim cDr As String
Option Explicit
Sub LlenarGridMuestras(nRef As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nMues As Integer

  Call ConfigurarGridMuestras
  sql = "SELECT AP2500.PR52NumIDMuestra, AP2300.AP23_Descrip, AP2500.AP25_TiMues, " _
    & "AP2500.AP25_FecExtra, AP2500.AP25_FecRecep, AP0800.AP08_Firma, AP1600.AP16_Desig, " _
    & "AP1900.AP19_CodFija, AP1900.AP19_Desig, AP2200.AP22_CodDecal, AP2200.AP22_Desig, " _
    & "AP2500.AP25_Observ, AP2500.AP25_IndIntra " _
    & "FROM AP2500, AP2300, AP0800, AP1600, AP1900, AP2200 WHERE " _
    & "AP2500.AP23_CodOrg = AP2300.AP23_CodOrg AND " _
    & "AP2500.AP08_CodPers = AP0800.AP08_CodPers AND " _
    & "AP2500.AP16_CodDiag = AP1600.AP16_CodDiag AND " _
    & "AP2500.AP19_CodFija = AP1900.AP19_CodFija (+) AND " _
    & "AP2500.AP22_CodDecal = AP2200.AP22_CodDecal (+) AND " _
    & "AP2500.AP21_CodRef = ? ORDER BY AP2500.PR52NumIDMuestra"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    nMues = rdo(0)
'    frmP_Recepcion.grdssMuestra.AddItem rdo(0) & Chr$(9) & rdo(1) & Chr$(9) & rdo(2) & _
    Chr$(9) & rdo(3) & Chr$(9) & rdo(4) & Chr$(9) & rdo(5) & Chr$(9) & rdo(6) & Chr$(9) & rdo(7) & _
    Chr$(9) & rdo(8) & Chr$(9) & rdo(9) & Chr$(9) & rdo(10) & Chr$(9) & rdo(11) & Chr$(9) & rdo(12)
    rdo.MoveNext
  Wend
'  frmP_Recepcion.cMuestraActiva = nMues
'  frmP_Recepcion.dimMuestra = 0
'  Call llenarlblMuestra(frmP_Recepcion.lblnMues, frmP_Recepcion.cMuestraActiva)
  rdo.Close
'  frmP_Recepcion.grdssMuestra.Columns(8).DropDownHwnd = frmP_Recepcion.ddnssFijador.hwnd
'  frmP_Recepcion.grdssMuestra.Columns(10).DropDownHwnd = frmP_Recepcion.ddnssDecal.hwnd

End Sub
Sub ConfigurarGridMuestras()
'  With frmP_Recepcion.grdssMuestra
'    .Columns.RemoveAll
'    .Columns.Add (0)
'    .Columns(0).Caption = "cMues"
'    .Columns(0).Visible = False
'    .Columns.Add (1)
'    .Columns(1).Caption = "Organo"
'    .Columns.Add (2)
'    .Columns(2).Caption = "Tipo"
'    .Columns.Add (3)
'    .Columns(3).Caption = "F. Extracc."
'    .Columns.Add (4)
'    .Columns(4).Caption = "F. Recep."
'    .Columns.Add (5)
'    .Columns(5).Caption = "Recepciona"
'    .Columns.Add (6)
'    .Columns(6).Caption = "Diag. Presunci�n"
'    .Columns.Add (7)
'    .Columns(7).Caption = "cFija"
'    .Columns(7).Visible = False
'    .Columns.Add (8)
'    .Columns(8).Caption = "Fijador"
'    .Columns.Add (9)
'    .Columns(9).Caption = "cDecal"
'    .Columns(9).Visible = False
'    .Columns.Add (10)
'    .Columns(10).Caption = "Decalcificador"
'    .Columns.Add (11)
'    .Columns(11).Caption = "Observaciones"
'    .Columns.Add (12)
'    .Columns(12).Caption = "Intra"
'  End With
End Sub
Function SacarNH(nRef As String) As Long
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT CI22NUMHISTORIA FROM AP2100 WHERE AP21_CodRef = ?"
  If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
    sql = sql & " AND AP21_FecGen BETWEEN ? AND ?"
  End If
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
    rdoQ(1) = ssdcDesde.Date
    rdoQ(2) = ssdcHasta.Date
  End If
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "No se ha encontrado ninguna prueba.", vbInformation + vbOKOnly, "B�squeda de pruebas"
    SacarNH = 0
    txtNRef.SetFocus
    txtNRef.SelStart = 0
    txtNRef.SelLength = Len(txtNRef.Text)
  Else
    SacarNH = rdo(0)
  End If
  rdo.Close
End Function
Function SacarNRef(NH As Long) As String
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem

  lvwPr.ListItems.Clear
  sql = "SELECT AP21_CodRef FROM AP2100 WHERE CI22NumHistoria = ? ORDER BY AP21_FecGen DESC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "No se ha encontrado ninguna prueba.", vbInformation + vbOKOnly, "B�squeda de pruebas"
    SacarNRef = ""
  Else
    While rdo.EOF = False
      Set item = lvwPr.ListItems.Add(, rdo(0), Right(rdo(0), 8))
      rdo.MoveNext
    Wend
  End If
  rdo.Close
End Function
Function SacarNHAp(Ap1 As String, Ap2 As String, Nom As String) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdoPac As rdoResultset

  sql = "SELECT DISTINCT CI22NumHistoria, CI22PriApel, CI22SegApel, CI22Nombre FROM CI2200 WHERE "
  If Ap1 <> "" Then sql = sql & "UPPER(CI22PriApel) LIKE ? AND "
  If Ap2 <> "" Then sql = sql & "UPPER(CI22SegApel) LIKE ? AND "
  If Nom <> "" Then sql = sql & "UPPER(CI22Nombre) LIKE ? AND "
  sql = sql & "EXISTS (" _
            & "SELECT CI22NumHistoria FROM AP2100 WHERE " _
            & "CI2200.CI22NumHistoria = AP2100.CI22NumHistoria"
  If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
    sql = sql & " AND AP21_FecGen BETWEEN ? AND ?"
  End If
  sql = sql & ")"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  If Ap1 <> "" Then
    rdoQ(0) = "%" & UCase(Ap1) & "%"
    If Ap2 <> "" Then
      rdoQ(1) = "%" & UCase(Ap2) & "%"
      If Nom <> "" Then
        rdoQ(2) = "%" & UCase(Nom) & "%"
        If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
          rdoQ(3) = ssdcDesde.Date
          rdoQ(4) = ssdcHasta.Date
        End If
      Else
        If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
          rdoQ(2) = ssdcDesde.Date
          rdoQ(3) = ssdcHasta.Date
        End If
      End If
    Else
      If Nom <> "" Then
        rdoQ(1) = "%" & UCase(Nom) & "%"
        If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
          rdoQ(2) = ssdcDesde.Date
          rdoQ(3) = ssdcHasta.Date
        End If
      Else
        If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
          rdoQ(1) = ssdcDesde.Date
          rdoQ(2) = ssdcHasta.Date
        End If
      End If
    End If
  Else
    If Ap2 <> "" Then
      rdoQ(0) = "%" & UCase(Ap2) & "%"
      If Nom <> "" Then
        rdoQ(1) = "%" & UCase(Nom) & "%"
        If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
          rdoQ(2) = ssdcDesde.Date
          rdoQ(3) = ssdcHasta.Date
        End If
      Else
        If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
          rdoQ(1) = ssdcDesde.Date
          rdoQ(2) = ssdcHasta.Date
        End If
      End If
    Else
      If Nom <> "" Then
        rdoQ(0) = "%" & UCase(Nom) & "%"
        If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
          rdoQ(1) = ssdcDesde.Date
          rdoQ(2) = ssdcHasta.Date
        End If
      Else
        If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
          rdoQ(0) = ssdcDesde.Date
          rdoQ(1) = ssdcHasta.Date
        End If
      End If
    End If
  End If
  lvwPr.ListItems.Clear
  lvwPac.ListItems.Clear
  Set rdoPac = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdoPac.EOF = True Then
    txtNH.Text = ""
    MsgBox "No se ha encontrado ning�n paciente.", vbInformation + vbOKOnly, "B�squeda de pruebas"
  Else
    Call AsignarPac(rdoPac)
  End If

End Function
Sub AsignarPac(rdo As rdoResultset)
Dim item As ListItem
  
  While rdo.EOF = False
    Set item = lvwPac.ListItems.Add(, "P" & rdo(0), rdo(0) & " - " & rdo(3) & " " & rdo(1) & " " & rdo(2))
    rdo.MoveNext
  Wend
  lvwPac.DropHighlight = lvwPac.ListItems(1)
  Call SacarNRef(Right(lvwPac.ListItems(1).key, Len(lvwPac.ListItems(1).key) - 1))
End Sub

Function SacarNombre(NH As Long) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT DISTINCT AP2100.CI22NumHistoria, CI2200.CI22PriApel, CI2200.CI22SegApel, CI2200.CI22Nombre " _
      & "FROM CI2200, AP2100 WHERE " _
      & "CI2200.CI22NumHistoria = AP2100.CI22NumHistoria AND CI2200.CI22NumHistoria = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = NH
  lvwPr.ListItems.Clear
  lvwPac.ListItems.Clear
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "No existe el n�mero de historia o no tiene ninguna prueba de Anatom�a.", vbInformation + vbOKOnly, "B�squeda de pruebas"
    SacarNombre = False
  Else
    Call AsignarPac(rdo)
    SacarNombre = True
  End If
  rdo.Close

End Function
Function SacarDatos(NH As Long, nRef As String, TiPr As Integer) As Integer
  If Val(txtNRef.Text) <> 0 Then
    If optTiPr(0) = True Then
      TiPr = 1
      nRef = "B"
    ElseIf optTiPr(1) = True Then
      TiPr = 2
      nRef = "C"
    ElseIf optTiPr(2) = True Then
      TiPr = 3
      nRef = "A"
    End If
    If Len(Trim$(txtNRef.Text)) > 5 Then
      nRef = txtAno.Text & nRef & Trim$(txtNRef.Text)
    Else
      nRef = txtAno.Text & nRef & String(5 - Len(Trim$(txtNRef.Text)), "0") & Trim$(txtNRef.Text)
    End If
    NH = SacarNH(nRef)
  ElseIf Val(txtNH.Text) <> 0 Then
    NH = Val(txtNH.Text)
    nRef = SacarNRef(NH)
  ElseIf Trim(txtAp1.Text) <> "" Then
    NH = SacarNHAp(Trim(txtAp1.Text), Trim(txtAp2.Text), Trim(txtNom.Text))
    nRef = SacarNRef(NH)
  Else
  End If
End Function
Private Sub cmdBuscar_Click()
Dim sql As String, TiPr As Integer, nRef As String, NH As Long
Dim rdoRef As rdoResultset
Dim rdoQ As rdoQuery
Dim res As Integer
Dim frm As Form
Dim ctrl As Integer
  
  res = SacarDatos(NH, nRef, TiPr)
  
  If NH <> 0 And nRef <> "" Then
    Select Case objPipe.PipeGet("Menu")
      Case "Petici�n"
        sql = "SELECT CI2200.CI22NumHistoria, " _
            & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
            & "PR0100.PR01DesCorta, AP2100.SG02Cod_PAT, AP2100.AP45_CodEstPr, " _
            & "AP2100.PR04NumActPlan, AP2100.AP21_FecSotud, AP0901J.AP60_TiPr, " _
            & "AP4500.AP45_Desig, AP2100.PR01CODACTUACION, AP2100.AP21_FecSotud, " _
            & "AP2100.AP21_FecGen, AP2100.AP21_FecFirma " _
            & "FROM AP2100, CI2200, PR0100, AP4500, AP0901J WHERE " _
            & "CI2200.CI22NumHistoria = AP2100.CI22NumHistoria AND " _
            & "PR0100.PR01CODACTUACION = AP2100.PR01CODACTUACION AND " _
            & "AP2100.AP45_CodEstPr = AP4500.AP45_CodEstPr AND " _
            & "AP0901J.PR01CodActuacion = AP2100.PR01CodActuacion AND " _
            & "AP2100.AP21_CodRef = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = nRef
        Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
        If rdo.EOF = True Then
          MsgBox "No se ha encontrado ninguna prueba.", vbInformation + vbOKOnly, "B�squeda de pruebas"
          Call CerrarrdoQueries
        Else
'          If rdo(4) = apPRUEBACERRADA Then
'            MsgBox "La prueba est� cerrada.", vbInformation + vbOKOnly, "B�squeda de pruebas"
'            Call CerrarrdoQueries
'          Else
            Call objPipe.PipeSet("NumRef", nRef)
'            Call objPipe.PipeSet("NH", rdo(0))
            ctrl = 0
            For Each frm In Forms
              If frm.Name = "frmP_Peticion" Then
                Me.Visible = False
                ctrl = 1
                Exit For
              End If
            Next frm
            If ctrl = 0 Then
              frmP_Peticion.Show vbModal
              Set frmP_Peticion = Nothing
            End If
'          End If
        End If
      Case "Recepcion"
        sql = "SELECT AP2100.AP21_CodHist, AP2100.AP21_CodCaso, AP2100.AP21_CodSec," _
              & "AP2100.AP21_FecSotud, AP2100.AP21_Anam, AP1100.AP11_Desig," _
              & "Pac.Ap1||' '||Pac.Ap2||', '||Pac.Nom as Nombre, Dpt.Desig, Dr.txtFirma," _
              & "AP2100.CPr, AP2100.AP21_TiPr, AP0900.AP09_Descrip, AP2100.AP21_Preg, " _
              & "AP2100.AP21_Observ, Pat.AP08_Firma, Res.AP08_Firma, Cito.AP08_Firma, " _
              & "AP2100.AP21_CodRef, AP2100.AP45_CodEstPr " _
              & "FROM AP2100, Pac, Dpt, Dr, AP0900, AP0800 Pat, AP0800 Res, AP0800 Cito, AP1100 WHERE " _
              & "AP2100.AP11_CodCtro = AP1100.AP11_CodCtro AND " _
              & "AP2100.AP21_CodHist = Pac.NH AND " _
              & "AP2100.AP08_CodPers_Pat = Pat.AP08_CodPers AND " _
              & "Res.AP08_CodPers (+) = AP2100.AP08_CodPers_Res AND " _
              & "Cito.AP08_CodPers (+) = AP2100.AP08_CodPers_Cito AND " _
              & "AP2100.CDpt = Dpt.CDpt AND " _
              & "AP2100.AP21_CodDr = Dr.cDr AND " _
              & "AP2100.AP09_CodTiPr = AP0900.AP09_CodTiPr AND " _
              & "AP2100.AP21_CodRef = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("Pruebas", sql)
        rdoQ(0).Type = rdTypeCHAR
        rdoQ(0) = nRef
        Set rdoRef = rdoQ.OpenResultset(rdOpenForwardOnly)
        If rdoRef.EOF = True Then
          MsgBox "No se ha encontrado ninguna prueba", vbInformation + vbOKOnly, "B�squeda de pruebas"
        Else
          If rdoRef(18) = apPRUEBACERRADA Then
            MsgBox "La prueba est� cerrada.", vbInformation + vbOKOnly, "B�squeda de pruebas"
'            Call CerrarrdoQueries
          Else
            Call LlenarGridMuestras(nRef)
            Call AsignarValores(rdoRef)
            Call LlenarDiagPres(nRef)
            Call objPipe.PipeSet("Recepcion", "Muestra")
            Call objPipe.PipeSet("NumRef", nRef)
            rdoRef.Close
            Unload Me
          End If
        End If
      Case "Descripci�n"
        sql = "SELECT PrAsist.CPr, Pr.Desig, Pac.Ap1||' '||Pac.Ap2||', '||Pac.Nom as Nombre, " _
        & "PrAsist.NH, PrAsist.NC, PrAsist.NS, " _
        & "PrAsist.FSotud, PrAsist.EstPr, PrAsist.cDrCont, " _
        & "PrAsist.cDrAyu, AP0800.AP08_Firma " _
        & "FROM Pac, Pr, AP0800, PrAsist WHERE " _
        & "PrAsist.NH = Pac.NH AND " _
        & "PrAsist.cDrCont = AP0800.cDr AND " _
        & "Pr.Cdpt = " & cDptAP & " AND " _
        & "Pr.CPr = PrAsist.CPr AND " _
        & "PrAsist.nRayos = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0).Type = rdTypeVARCHAR
        rdoQ(0) = nRef
        Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
        If rdo.EOF = True Then
          MsgBox "No se ha encontrado ninguna prueba.", vbInformation + vbOKOnly, "B�squeda de pruebas"
        Else
          If rdo(7) = 5 Then
            MsgBox "La prueba est� cerrada.", vbCritical + vbOKOnly, "B�squeda de pruebas"
          Else
            Call objPipe.PipeSet("NumRef", nRef)
            ctrl = 0
            For Each frm In Forms
              If frm.Name = "frmMecaDocumento" Then
                Me.Visible = False
                ctrl = 1
                Set frm.rdo = rdo
                Exit For
              End If
            Next frm
            If ctrl = 0 Then
'              frmMecaDocumento.Show vbModal
'              Set frmMecaDocumento = Nothing
            End If
          End If
        End If
        
      Case "Fotos", "Diag"
        sql = "SELECT CI2200.CI22NumHistoria, " _
              & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre as Nombre, " _
              & "AP2100.AP21_CodRef, PR0100.PR01DesCorta " _
              & "FROM AP2100, CI2200, PR0100 WHERE " _
              & "AP2100.CI22NumHistoria = CI2200.CI22NumHistoria AND " _
              & "AP2100.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
              & "AP2100.CI22NumHistoria = ? " _
              & "ORDER BY AP2100.AP21_CodRef ASC"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = NH
        Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
        If rdo.EOF = True Then
          MsgBox "No se ha encontrado el paciente.", vbInformation + vbOKOnly, "B�squeda de pruebas"
        Else
          Call objPipe.PipeSet("NH", NH)
          ctrl = 0
          For Each frm In Forms
            If frm.Name = "frmPrAnteriores" Then
              Me.Visible = False
              ctrl = 1
              Exit For
            End If
          Next frm
          If ctrl = 0 Then
            Load frmPrAnteriores
            frmPrAnteriores.Show vbModal
            Set frmPrAnteriores = Nothing
          End If
        End If
        Call CerrarrdoQueries
    End Select
  End If
End Sub

Sub LlenarDiagPres(nRef As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP1600.AP16_CodDiag, AP16_Desig, AP2300.AP23_CodOrg, AP23_Descrip, AP2500.AP08_CodPers, AP08_Firma " _
      & "FROM AP1600, AP2300, AP0800, AP2500 WHERE " _
      & "AP2500.AP16_CodDiag = AP1600.AP16_CodDiag AND " _
      & "AP2500.AP23_CodOrg = AP2300.AP23_CodOrg AND " _
      & "AP2500.AP08_CodPers = AP0800.AP08_CodPers AND " _
      & "AP2500.AP21_CodRef = ? " _
      & "ORDER BY AP2500.PR52NumIDMuestra DESC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    With frmP_Recepcion
'      .cDiag = rdo(0)
'      .cbossMuestra(2).Text = rdo(1)
'
'      .cOrgano = rdo(2)
'      .cbossMuestra(0).Text = rdo(3)
'
'      .cRecep = rdo(4)
'      .cbossMuestra(1).Text = rdo(5)
    End With
  End If
  rdo.Close
End Sub
Sub AsignarValores(rdo As rdoResultset)
Dim sql As String

  On Error Resume Next
'  With frmP_Recepcion
'    .txtPaciente(0).Text = rdo(0) ' N�mero de historia
'    .NH = rdo(0)                  ' N�mero de historia
'    .NC = rdo(1)                  ' N�mero de caso
'    .txtFSotud.Text = rdo(3)      ' Fecha de solicitud
'    .txtPaciente(2).Text = rdo(4) ' Anamnesis
'    .txtPeticion(0).Text = rdo(5) ' Centro peticionario
'    .txtPaciente(1).Text = rdo(6) ' Nombre del paciente
'    .txtPeticion(1).Text = rdo(7) ' Departamento peticionario
'    .txtPeticion(2).Text = rdo(8) ' Doctor peticionario
'    .txtPrueba(14).Text = rdo(9)   ' C�digo de la prueba
'
'    Select Case rdo(10)
'      Case 1
'        .txtPrueba(2).Text = "Biopsia"
'        .cbossRealiza(2).Enabled = False
'        .lblLabel1(1).Enabled = False
'        .lblMuestra(23).Enabled = True
'        .cbossMuestra(3).Enabled = True
'        .cbossMuestra(3).Text = "Formol"
'        .lblMuestra(24).Enabled = True
'        .cbossMuestra(4).Enabled = True
'        sql = "SELECT AP08_CODPers, AP08_Firma FROM AP0800 WHERE AP07_CODTiPers > 2  AND AP08_IndActiva = -1 ORDER BY AP07_CODTiPers, AP08_CODPers"
'        Call .LlenarCombo(.cbossMuestra(1), sql)
'
'      Case 2
'        .txtPrueba(2).Text = "Citolog�a"
'        .cbossRealiza(2).Enabled = True
'        .lblLabel1(1).Enabled = True
'        .lblMuestra(23).Enabled = False
'        .cbossMuestra(3).Enabled = False
'        .cbossMuestra(3).Text = ""
'        .lblMuestra(24).Enabled = False
'        .cbossMuestra(4).Enabled = False
'        sql = "SELECT AP08_CODPers, AP08_Firma FROM AP0800 WHERE AP08_IndActiva = -1 AND AP07_CODTiPers = " & apCODCITOTECNICO _
'            & " OR AP07_CODTiPers = " & apCODTECCITOLOGIAS & " ORDER BY AP08_CODPers"
'        Call .LlenarCombo(.cbossMuestra(1), sql)
'      Case 3
'        .txtPrueba(2).Text = "Autopsia"
'    End Select
'
'    .txtPrueba(1).Text = txtNRef
'    .txtPrueba(3).Text = rdo(11)  ' Descripci�n de la prueba
'    .txtPrueba(4).Text = rdo(12)  ' Pregunta clave
'    .txtPrueba(5).Text = rdo(13)  ' Observaciones
'    .cbossRealiza(0).Text = rdo(14) ' Pat�logo
'    .cbossRealiza(1).Text = rdo(15) ' Residente
'    .cbossRealiza(2).Text = rdo(16) ' Citot�cnico
'    If rdo(17) = 1 Then .chkPaciente = False Else .chkPaciente = True
'    .fraPr.Enabled = False
'  End With
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdMenos_Click()
  If cmdMenos.Caption = "&M�s >>" Then
    Me.Height = 6585
    cmdMenos.Caption = "&Menos <<"
    If lvwDpt.ListItems.Count = 0 Then Call LlenarDpt
  Else
    Me.Height = 2500
    cmdMenos.Caption = "&M�s >>"
  End If
End Sub
Sub LlenarDpt()
Dim sql As String
Dim rdo As rdoResultset
Dim item As ListItem

  sql = "SELECT AD02CodDpto, AD02DesDpto FROM AD0200 WHERE " _
      & "AD32CodTipoDpto IN (" & apDPTOCLINICO & ", " & apDPTOBASICO & ")"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwDpt.ListItems.Add(, "D" & rdo(0), rdo(1))
    rdo.MoveNext
  Wend
  
End Sub
Private Sub cmdPr_Click()
Dim NH As Long
  
  NH = Val(txtNH.Text)
  If NH <> 0 Or Trim$(txtAp1.Text) <> "" Or Trim$(txtAp2.Text) <> "" Or Trim$(txtNom.Text) <> "" Then
    If NH = 0 Then
      Call SacarNHAp(Trim(txtAp1.Text), Trim(txtAp2.Text), Trim(txtNom.Text))
    Else
      Call SacarNombre(NH)
    End If
  ElseIf cDr <> "" Then
    Call SacarPac(cDr)
  End If
End Sub
Sub SacarPac(cDr As String)
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset

  sql = "SELECT DISTINCT AP2100.CI22NumHistoria, CI2200.CI22PriApel, CI2200.CI22SegApel, CI2200.CI22Nombre " _
      & "FROM CI2200, AP2100 WHERE " _
      & "CI2200.CI22NumHistoria = AP2100.CI22NumHistoria AND " _
      & "AP2100.SG02Cod_Sote = ?"
  If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
    sql = sql & " AND AP21_FecGen BETWEEN ? AND ?"
  End If
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cDr
  If ssdcDesde.Text <> "" And ssdcHasta.Text <> "" Then
    rdoQ(1) = ssdcDesde.Date
    rdoQ(2) = ssdcHasta.Date
  End If
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  lvwPr.ListItems.Clear
  lvwPac.ListItems.Clear
  If rdo.EOF = False Then
    Call AsignarPac(rdo)
  End If
End Sub
Private Sub Form_Activate()
  txtNRef.SelStart = 0
  txtNRef.SelLength = Len(txtNRef.Text)
End Sub

Private Sub Form_Load()
  optTiPr(0) = True
  txtAno.Text = Format$(Now, "YYYY")
  Me.Height = 2500
  cmdMenos.Caption = "&M�s >>"
  Call IniVisor
End Sub
Private Sub Form_Unload(Cancel As Integer)
'  If objPipe.PipeGet("Menu") <> "Recepcion" Then Call CerrarrdoQueries
End Sub

Private Sub lvwDpt_ItemClick(ByVal item As ComctlLib.ListItem)
  lvwDr.ListItems.Clear
  cDr = ""
  txtNH.Text = ""
  txtAp1.Text = ""
  txtAp2.Text = ""
  txtNom.Text = ""
  Call LlenarDr(Right(item.key, Len(item.key) - 1))
End Sub
Sub LlenarDr(cDpt As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem

  sql = "SELECT SG02Cod, NVL(SG02TxtFirma,SG02Ape1||' '||SG02Ape2||', '||SG02Nom) " _
      & "FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cDpt
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwDr.ListItems.Add(, "D" & rdo(0), rdo(1))
    rdo.MoveNext
  Wend

End Sub

Private Sub lvwDr_ItemClick(ByVal item As ComctlLib.ListItem)
  cDr = Right(item.key, Len(item.key) - 1)
  Call SacarPac(cDr)
End Sub

Private Sub lvwPac_ItemClick(ByVal item As ComctlLib.ListItem)
  cDr = ""
  Call SacarNRef(Right(item.key, Len(item.key) - 1))
End Sub

Private Sub lvwPr_DblClick()
Dim item As ListItem
Dim txt As String

  If lvwPr.ListItems.Count > 0 Then
    Set item = lvwPr.SelectedItem
    txt = item.key
    txtAno.Text = Left(txt, 4)
    Select Case Mid(txt, 5, 1)
      Case "B"
        optTiPr(0).Value = True
      Case "C"
        optTiPr(0).Value = True
      Case "A"
        optTiPr(0).Value = True
    End Select
    txtNRef.Text = Val(Right(txt, 5))
    cmdBuscar_Click
  End If
End Sub

Private Sub lvwPr_ItemClick(ByVal item As ComctlLib.ListItem)
  txtAno.Text = Left(item.key, 4)
  Select Case Mid(item.key, 5, 1)
    Case "B"
      optTiPr(0).Value = True
    Case "C"
      optTiPr(1).Value = True
    Case "A"
      optTiPr(2).Value = True
  End Select
  txtNRef.Text = Val(Right(item.key, 5))

End Sub

Private Sub tabBuscar_DblClick()

End Sub


Private Sub ssdcDesde_CloseUp()
  If ssdcDesde.Text <> "" Then ssdcHasta.MinDate = ssdcDesde.Date
End Sub

Private Sub ssdcHasta_CloseUp()
  If ssdcHasta.Text <> "" Then ssdcDesde.MaxDate = ssdcHasta.Date
End Sub

Private Sub txtAp1_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdPr_Click
    Case 27
      cmdCancelar_Click
  End Select
End Sub

Private Sub txtAp2_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdPr_Click
    Case 27
      cmdCancelar_Click
  End Select

End Sub

Private Sub txtNH_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdPr_Click
    Case 27
      cmdCancelar_Click
    Case 48 To 57 ' Los n�meros
    Case Else
      KeyAscii = 0
  End Select
End Sub

Private Sub txtNom_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdPr_Click
    Case 27
      cmdCancelar_Click
  End Select

End Sub

Private Sub txtNRef_KeyPress(KeyAscii As Integer)
  Select Case KeyAscii
    Case 13
      cmdBuscar_Click
    Case 27
      cmdCancelar_Click
    Case 66, 98 ' se ha presionado la 'b' o 'B'
      optTiPr(0).Value = True
      KeyAscii = 0
    Case 67, 99 ' se ha presionado la 'c' o 'C'
      optTiPr(1).Value = True
      KeyAscii = 0
    Case 65, 97 ' se ha presionado la 'a' o 'A'
      optTiPr(2).Value = True
      KeyAscii = 0
    Case 48 To 57
    Case Else
      KeyAscii = 0
  End Select
End Sub

