VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmConfirmarDiag 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Confirmaci�n de Diagn�sticos"
   ClientHeight    =   7245
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11595
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7245
   ScaleWidth      =   11595
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picSep 
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   0
      MousePointer    =   9  'Size W E
      ScaleHeight     =   465
      ScaleWidth      =   120
      TabIndex        =   19
      Top             =   0
      Width           =   120
   End
   Begin VB.CheckBox chkInformar 
      Caption         =   "Informar los diagn�sticos definitivos"
      Height          =   315
      Left            =   6975
      TabIndex        =   16
      Top             =   1200
      Value           =   1  'Checked
      Width           =   2940
   End
   Begin VB.Frame Frame3 
      Caption         =   "Acciones sobre los diag."
      ForeColor       =   &H00800000&
      Height          =   1440
      Index           =   1
      Left            =   4725
      TabIndex        =   13
      Top             =   150
      Width           =   1965
      Begin VB.CommandButton cmdAnular 
         Caption         =   "A&nular"
         Height          =   390
         Left            =   375
         TabIndex        =   15
         Top             =   825
         Width           =   1215
      End
      Begin VB.CommandButton cmdInfor 
         Caption         =   "&Informar"
         Height          =   390
         Left            =   375
         TabIndex        =   14
         Top             =   300
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdInforme 
      Caption         =   "&Ver Informe"
      Height          =   390
      Left            =   10125
      TabIndex        =   7
      Top             =   675
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selecci�n"
      ForeColor       =   &H00800000&
      Height          =   1440
      Left            =   225
      TabIndex        =   5
      Top             =   150
      Width           =   4215
      Begin VB.OptionButton optDiagIni 
         Caption         =   "Continuada"
         Height          =   240
         Index           =   3
         Left            =   225
         TabIndex        =   18
         Top             =   1050
         Width           =   1740
      End
      Begin VB.OptionButton optDiagIni 
         Caption         =   "Disponible Micro"
         Height          =   240
         Index           =   2
         Left            =   225
         TabIndex        =   17
         Top             =   750
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   390
         Left            =   2850
         TabIndex        =   10
         Top             =   300
         Width           =   1215
      End
      Begin VB.OptionButton optDiagIni 
         Caption         =   "Option1"
         Height          =   240
         Index           =   0
         Left            =   2325
         TabIndex        =   9
         Top             =   750
         Width           =   1590
      End
      Begin VB.OptionButton optDiagIni 
         Caption         =   "Option1"
         Height          =   240
         Index           =   1
         Left            =   2325
         TabIndex        =   8
         Top             =   1050
         Width           =   1515
      End
      Begin SSDataWidgets_B.SSDBCombo cbossPat 
         Height          =   315
         Left            =   225
         TabIndex        =   6
         Top             =   300
         Width           =   2490
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         BevelColorFace  =   12632256
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4948
         Columns(1).Caption=   "Pat�logo"
         Columns(1).Name =   "Pat�logo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4392
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   10125
      TabIndex        =   4
      Top             =   1125
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   10125
      TabIndex        =   3
      Top             =   225
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      Caption         =   "Estado final de diagn�stico"
      ForeColor       =   &H00800000&
      Height          =   915
      Index           =   0
      Left            =   6900
      TabIndex        =   0
      Top             =   150
      Width           =   2265
      Begin VB.OptionButton optDiag 
         Caption         =   "Option1"
         Height          =   240
         Index           =   2
         Left            =   150
         TabIndex        =   2
         Top             =   525
         Width           =   1665
      End
      Begin VB.OptionButton optDiag 
         Caption         =   "Option1"
         Height          =   240
         Index           =   1
         Left            =   150
         TabIndex        =   1
         Top             =   225
         Value           =   -1  'True
         Width           =   1740
      End
   End
   Begin ComctlLib.ListView lvwPr 
      Height          =   5340
      Left            =   225
      TabIndex        =   11
      Top             =   1800
      Width           =   5340
      _ExtentX        =   9419
      _ExtentY        =   9419
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin ComctlLib.ListView lvwDiag 
      Height          =   5340
      Left            =   5625
      TabIndex        =   12
      Top             =   1800
      Width           =   5790
      _ExtentX        =   10213
      _ExtentY        =   9419
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "frmConfirmarDiag"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TiDiag(0 To 1) As Integer, TiDiagIni(0 To 1) As Integer
Dim Pat() As String
Dim cPat As String
Dim CodRefs() As String
Dim nRef As String
Dim AnchoIni As Long, LeftIni As Long, mover As Integer

Private Sub cbossPat_CloseUp()
  Call Vaciarlvw
End Sub

Private Sub cbossPat_InitColumnProps()
Dim i As Integer

  For i = 1 To UBound(Pat, 2)
    cbossPat.AddItem Pat(1, i) & Chr$(9) & Pat(2, i)
  Next i
End Sub

Private Sub cmdAceptar_Click()
Dim cPr As String
Dim sql As String, sqlInsert As String
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim i As Integer, numPrs As Integer, res As Integer, nRef As String
Dim ctrl As Integer, texto As String, est As Integer
  
  cPr = "('"
  numPrs = 0
  For Each item In lvwPr.ListItems
    If item.Selected = True Then
      ctrl = True
      nRef = Right(item.key, Len(item.key) - 1)
      If optDiag(2).Value = True Then  ' Van a poner un diagn�stico definitivo
        If FaltanTecnicas(nRef) = True Then
          Screen.MousePointer = 0
          texto = "Faltan t�cnicas por realizar de la " & item.SubItems(1) & " n�mero " _
                & item.Text & ", por lo que los diagn�sticos no pueden ser definitivos."
          MsgBox texto, vbExclamation, "Introducci�n de diagn�sticos"
          ctrl = False
        End If
      Else  ' Van a poner un diagn�stico provisional
        If HayTecnicas(nRef) = False Then
          Screen.MousePointer = 0
          texto = "Es necesario que alguna t�cnica de la " & item.SubItems(1) & " n�mero " _
              & item.Text & " est� realizada para introducir diagn�sticos provisionales."
          MsgBox texto, vbExclamation, "Introducci�n de diagn�sticos"
          ctrl = False
        End If
      End If
      If ctrl = True Then
        numPrs = numPrs + 1
        ReDim Preserve CodRefs(1 To numPrs)
        CodRefs(numPrs) = nRef
        cPr = cPr & nRef & "', '"
      End If
    End If
  Next item
      
  If numPrs > 0 Then
    Load frmIDDr
    With frmIDDr
      .txtPat.Text = cbossPat.Text
      .txtPwd.Tag = cPat
      .Show vbModal
    End With
    If frmIDDr.PwdCorrect = True Then
      cPr = Left(cPr, Len(cPr) - 3) & ")"
      If optDiagIni(0).Value = True Then  ' Se muestran los diag sin confirmar
        sqlInsert = "INSERT INTO AP4000 (AP39_CodDiag, AP40_TiDr, AP40_CodDr, AP40_FecConf) " _
        & "SELECT AP39_CodDiag, ?, ?, SYSDATE) " _
        & "FROM AP3900 WHERE " _
        & "AP21_CodRef IN " & cPr & " AND AP49_CodEstDiag = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sqlInsert)
        rdoQ(0) = 1
        rdoQ(1) = cPat
        rdoQ(2) = EstadoDiagIni
        rdoQ.Execute
      End If
      est = EstadoDiag()
      If est = apDIAGDEFINITIVO And chkInformar.Value = vbChecked Then
        sql = "UPDATE AP3900 SET AP49_CODESTDIAG = ?, AP39_IndInfor = ?, " _
            & "AP39_FecInfor = SYSDATE " _
            & "WHERE AP21_CodRef IN " & cPr _
            & " AND AP49_CodEstDiag = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = est
        rdoQ(1) = -1
        rdoQ(2) = EstadoDiagIni
      Else
        sql = "UPDATE AP3900 SET AP49_CODESTDIAG = ?" _
            & " WHERE AP21_CodRef IN " & cPr _
            & " AND AP49_CodEstDiag = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0) = est
        rdoQ(1) = EstadoDiagIni
      End If
      rdoQ.Execute sql
      Call ActualizarEstPr_ConfirmDiag(cPr, rdoQ(0))
      Call CerrarrdoQueries
      res = MostrarDiag()
    End If
    Unload frmIDDr
  ElseIf ctrl = True Then
    MsgBox "Debe seleccionar alg�n diagn�stico.", vbInformation, "Confirmaci�n de diagn�sticos"
  End If
End Sub
Function EstadoDiag() As Integer
Dim i As Integer
  For i = 1 To 3
    If optDiag(i).Value = True Then
      EstadoDiag = TiDiag(i - 1)
      Exit Function
    End If
  Next i
End Function

Function EstadoDiagIni() As Integer
Dim i As Integer
  For i = 0 To 1
    If optDiagIni(i).Value = True Then
      EstadoDiagIni = TiDiagIni(i)
      Exit Function
    End If
  Next i
End Function

Private Sub cmdAnular_Click()
Dim sql As String
Dim rdoQ As rdoQuery
Dim cDiags As String
      
  cDiags = DiagSelec()
  If cDiags = "(" Then
    MsgBox "Debe seleccionar alg�n diagn�stico.", vbInformation, "Anular diagn�sticos"
  Else
    cDiags = Left(cDiags, Len(cDiags) - 2) & ")"
    sql = "UPDATE AP3900 SET AP49_CODESTDIAG = ? " _
        & "WHERE AP39_CodDiag IN " & cDiags
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(0) = apDIAGANULADO
    rdoQ.Execute
    lvwDiag.ListItems.Clear
    Call LlenarDiag(nRef)
  End If

End Sub

Private Sub cmdBuscar_Click()
Dim res As Integer

  If cbossPat.Text <> "" Then
    If optDiagIni(2).Value = False And optDiagIni(3).Value = False Then
      res = MostrarDiag()
      If res = False Then
        MsgBox "No hay ning�n diagn�stico disponible para confirmar", vbInformation, "Confirmaci�n de diagn�sticos"
      End If
    Else
      res = MostrarPr()
      If res = False Then
        MsgBox "No hay ninguna prueba con diagn�sticos pendientes de introducir.", vbInformation, "Confirmaci�n de diagn�sticos"
      End If
    End If
  End If
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Function MostrarDiag() As Integer
Dim i As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem

  Call Vaciarlvw
  cPat = Patologo()
  
  sql = "SELECT /*+ RULE */ AP2100.AP21_CodRef, AP0901J.AP60_TiPr, AP2100.CI22NumHistoria, " _
      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre as Nombre, " _
      & "AP2100.PR04NumActPlan, AP2100.AP21_FecGen, AP2100.AP21_FecSotud FROM " _
      & "AP2100, CI2200, AP0901J WHERE " _
      & "AP2100.CI22NumHistoria = CI2200.CI22NumHistoria AND " _
      & "AP2100.PR01CodActuacion = AP0901J.PR01CodActuacion AND " _
      & "AP2100.SG02Cod_Pat = ? AND " _
      & "AP2100.AP21_CodRef IN (" _
      & "SELECT DISTINCT AP21_CodRef FROM AP3900 WHERE AP3900.AP49_CodEstDiag = ?) " _
      & "ORDER BY AP0901J.AP60_TiPr, AP2100.AP21_CodRef"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cPat
  rdoQ(1) = EstadoDiagIni
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwPr.ListItems.Add(, "P" & rdo(0), Right(rdo(0), 8))
    Select Case rdo(1)
      Case 1
        item.SubItems(1) = "Biopsia"
      Case 2
        item.SubItems(1) = "Citolog�a"
      Case 3
        item.SubItems(1) = "Necropsia"
    End Select
    item.SubItems(2) = CStr(rdo(2))
    item.SubItems(3) = CStr(rdo(3))
    item.Tag = rdo(4) & "F" & rdo(6)
    rdo.MoveNext
  Wend
  If lvwPr.ListItems.Count = 0 Then MostrarDiag = False Else MostrarDiag = True
End Function
Function Patologo() As String
Dim i As Integer

  For i = 1 To UBound(Pat, 2)
    If cbossPat.Text = Pat(2, i) Then
      Patologo = Pat(1, i)
      Exit Function
    End If
  Next i
End Function
Function MostrarPr()
Dim i As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem

  Call Vaciarlvw
  cPat = Patologo()
  sql = "SELECT /*+ RULE */ AP2100.AP21_CodRef, AP0901J.AP60_TiPr, AP2100.CI22NumHistoria, " _
      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre as Nombre, " _
      & "AP2100.PR04NumActPlan, AP2100.AP21_FecGen, AP2100.AP21_FecSotud FROM " _
      & "AP2100, CI2200, AP0901J WHERE " _
      & "AP2100.CI22NumHistoria = CI2200.CI22NumHistoria AND " _
      & "AP2100.PR01CodActuacion = AP0901J.PR01CodActuacion AND " _
      & "AP2100.SG02Cod_Pat = ? AND " _
      & "AP2100.AP45_CodEstPr = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cPat
  If optDiagIni(2).Value = True Then rdoQ(1) = apPRUEBAMICRO Else rdoQ(1) = apPRUEBACONTINUADA
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvwPr.ListItems.Add(, "P" & rdo(0), Right(rdo(0), 8))
    Select Case rdo(1)
      Case 1
        item.SubItems(1) = "Biopsia"
      Case 2
        item.SubItems(1) = "Citolog�a"
      Case 3
        item.SubItems(1) = "Necropsia"
    End Select
    item.SubItems(2) = CStr(rdo(2))
    item.SubItems(3) = CStr(rdo(3))
    item.Tag = rdo(4) & "F" & rdo(6)
    rdo.MoveNext
  Wend
  If lvwPr.ListItems.Count = 0 Then MostrarPr = False Else MostrarPr = True

End Function


Private Sub cmdInfor_Click()
Dim sql As String
Dim cDiags As String
      
  cDiags = DiagSelec()
  If cDiags = "(" Then
    MsgBox "Debe seleccionar alg�n diagn�stico.", vbInformation, "Informar diagn�sticos"
  Else
    cDiags = Left(cDiags, Len(cDiags) - 2) & ")"
    sql = "UPDATE AP3900 SET AP39_INDINFOR = -1, " _
        & "AP39_FecInfor = SYSDATE " _
        & "WHERE AP39_CodDiag IN " & cDiags
    objApp.rdoConnect.Execute sql
    lvwDiag.ListItems.Clear
    Call LlenarDiag(nRef)
  End If
End Sub
Function DiagSelec() As String
Dim cDiags As String
Dim item As ListItem
  
  cDiags = "("
  For Each item In lvwDiag.ListItems
    If item.Selected = True Then
      cDiags = cDiags & Right(item.key, Len(item.key) - 1) & ", "
    End If
  Next item
  DiagSelec = cDiags
End Function
Private Sub cmdInforme_Click()
Dim Historia As String, Caso As String, Secuencia As String
Dim Anyo As String, mes As String, msg  As String, nombredoc As String
Dim res As Integer, seguir As Integer
Dim item As ListItem

  archDocumento = ""
  seguir = False
  For Each item In lvwPr.ListItems
    If item.Selected = True Then
      seguir = True
      Exit For
    End If
  Next item
  If seguir = False Then
    MsgBox "Debe seleccionar alguna prueba", vbInformation + vbOKOnly, "Visualizaci�n de informes"
  Else
    Screen.MousePointer = 11
    Historia = item.SubItems(2)
    Caso = Mid(item.Tag, 2, InStr(item.Tag, "S") - 2)
    Secuencia = Mid(item.Tag, InStr(item.Tag, "S") + 1, InStr(item.Tag, "F") - InStr(item.Tag, "S") - 1)
    Anyo = Year(Right(item.Tag, Len(item.Tag) - InStr(item.Tag, "F")))
    mes = Month(Right(item.Tag, Len(item.Tag) - InStr(item.Tag, "F")))
    Call objPipe.PipeSet("NumRef", Right(item.key, Len(item.key) - 1))
    nombredoc = NombreFichpruebas(Historia, Caso, Secuencia)
    
    archDocumento = DocsPath & "\" & cDptAP & "\pruebas\"
    archDocumento = archDocumento & Anyo & "\" & mes & "\" & nombredoc
  
  ' Comprobamos la existencia del fichero
    On Error Resume Next
    res = GetAttr(archDocumento)
    If Err <> 0 Then
      Err = 0
      msg = "El fichero no existe. �Desea crearlo ahora?"
      res = MsgBox(msg, vbExclamation + vbYesNo, "Error Apertura Documento")
      If res = vbYes Then
  '      res = CrearPrueba(NH, NC, NS, documento)
        Err = 0
        FileCopy "g:\sihc\data\docs\" & cDptAP & "\pruebas\" & Anyo & "\" & mes & "\" & NombreFichpruebas(CStr(Historia), CStr(Caso), CStr(Secuencia)), archDocumento
        If Err <> 0 Then
          MkDir DocsPath & "\" & cDptAP & "\pruebas\" & Anyo
          MkDir DocsPath & "\" & cDptAP & "\pruebas\" & Anyo & "\" & mes
          FileCopy "g:\sihc\data\docs\" & cDptAP & "\pruebas\" & Anyo & "\" & mes & "\" & NombreFichpruebas(CStr(Historia), CStr(Caso), CStr(Secuencia)), archDocumento
        End If
      End If
    Else
  '    If lblPrAsist_FFirma.Caption = "" And lblPrAsist_NS.Caption <> "0" Then
  '      MsgBox "La prueba todav�a no ha sido firmada. Los datos que aparecen en su documento no son definitivos.", MB_ICONINFORMATION, "Aviso"
  '    Else
  '      If dataPrAsist.Recordset("EstPr") = 4 Then
  '        If dataPrAsist.Recordset("CDptPr") = DPTANATPAT And InStr(CBiopsias, CStr(dataPrAsist.Recordset("CPr"))) <> 0 Then
  '          MsgBox "La prueba est� firmada, pero no insertada definitivamente en el Informe M�dico.", MB_ICONINFORMATION, "Aviso"
  '        Else
  '          MsgBox "La prueba est� firmada, pero no insertada en el Informe M�dico.", MB_ICONINFORMATION, "Aviso"
  '        End If
  '      End If
  '    End If
  '    frmVerDoc.Show vbModal
  '    frmArchDocumento.Show vbModal
    End If
  
    Screen.MousePointer = Default
  End If

End Sub


Private Sub Command1_Click()

End Sub

Private Sub Form_Load()
  Call LlenarOptDiag
  Call LlenarPat
  Call Formatearlvw
  cmdInfor.Enabled = False
  Call objPipe.PipeSet("Donde", "Confirmar")
  Call Dibujar
End Sub
Sub Formatearlvw()
  With lvwPr
    .ColumnHeaders.Add , , "N� Ref", 600
    .ColumnHeaders.Add , , "Prueba", 600
    .ColumnHeaders.Add , , "Historia", 600
    .ColumnHeaders.Add , , "Paciente", 4000
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
  With lvwDiag
    .ColumnHeaders.Add , , "Cod.", 600
    .ColumnHeaders.Add , , "SNOMED", 3000
    .ColumnHeaders.Add , , "Infor.", 400
    .ColumnHeaders.Add , , "Descripci�n", 4000
    .ColumnHeaders.Add , , "Observaciones", 4000
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With

End Sub
Sub LlenarOptDiag()
Dim sql As String
Dim rdo As rdoResultset
Dim i As Integer
  
  sql = "SELECT AP49_CodEstDiag, AP49_Desig FROM AP4900 ORDER BY AP49_CodEstDiag"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  For i = 0 To 2
    If i <> 0 Then
      TiDiag(i - 1) = rdo(0)
      optDiag(i).Caption = rdo(1)
    End If
    If i < 2 Then
      TiDiagIni(i) = rdo(0)
      optDiagIni(i).Caption = rdo(1)
    End If
    rdo.MoveNext
  Next i
  rdo.Close
End Sub

Sub LlenarPat()
Dim sql As String
Dim rdo As rdoResultset
Dim i As Integer

  sql = "SELECT AP0800.SG02Cod, " _
      & "NVL(SG0200.SG02txtFirma,SG0200.SG02Ape1||' '||SG0200.SG02Ape2||', '||SG0200.SG02Nom) " _
      & "FROM AP0800, SG0200 WHERE " _
      & "AP0800.SG02Cod = SG0200.SG02Cod AND " _
      & "AP0800.AP07_CodTiPers = " & apCODPATOLOGO & " AND " _
      & "SG0200.SG02FecDes Is NULL " _
      & "ORDER BY AP0800.SG02Cod"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  i = 0
  While rdo.EOF = False
    i = i + 1
    ReDim Preserve Pat(1 To 2, 1 To i)
    Pat(1, i) = rdo(0)
    Pat(2, i) = rdo(1)
    rdo.MoveNext
  Wend
  rdo.Close
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim frm As Form
  Erase Pat
  Erase TiDiag
  Erase TiDiagIni
  Call objPipe.PipeRemove("Donde")
  For Each frm In Forms
    If frm.Name = "frmDiag" Then
      Unload frmDiag
      Exit For
    End If
  Next frm

End Sub

Private Sub grdssDiag_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
  DispPromptMsg = False
End Sub

Private Sub lvwPr_DblClick()
Dim item As ListItem, frm As Form
Dim sql As String

  Set item = lvwPr.SelectedItem
  If optDiagIni(0) = True Or optDiagIni(1) = True Then
  Else
    nRef = Right(item.key, Len(item.key) - 1)
    Call objPipe.PipeSet("NumRef", nRef)
    Call objPipe.PipeSet("Pat", Patologo())
    frmDiag.Show vbModal
    cmdBuscar_Click
  End If
End Sub

Private Sub lvwPr_ItemClick(ByVal item As ComctlLib.ListItem)
Dim ItemPr As ListItem
Dim ndiags As Integer, ctrl As Integer

  If optDiagIni(0) = True Or optDiagIni(1) = True Then
    ctrl = True
    For Each ItemPr In lvwPr.ListItems
      If ItemPr.Selected = True Then
        If ndiags = 1 Then
          ctrl = False
          Exit For
        Else
          ndiags = ndiags + 1
        End If
      End If
    Next ItemPr
    
    lvwDiag.ListItems.Clear
    If ctrl = True Then
      nRef = Right(item.key, Len(item.key) - 1)
      Call LlenarDiag(nRef)
    Else
      nRef = ""
    End If
  End If
End Sub
Sub LlenarDiag(nRef As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim itemDiag As ListItem
    
  sql = "SELECT AP3900.AP39_CodDiag, AP3700.AP50_CodTiSno||'-'||AP3700.AP37_NumSno, " _
      & "AP3700.AP37_Descrip, Ap3900.AP39_FecDiag, AP3900.AP39_Descrip, AP3900.AP39_Observ, " _
      & "AP3900.AP39_IndInfor " _
      & "FROM AP3700, AP3900 WHERE " _
      & "AP3900.AP37_CodSno = AP3700.AP37_CodSno AND " _
      & "AP3900.AP21_CodRef = ? AND " _
      & "AP3900.AP49_CodEstDiag = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = EstadoDiagIni
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set itemDiag = lvwDiag.ListItems.Add(, "D" & rdo(0), rdo(1))
    itemDiag.SubItems(1) = rdo(2)
    If rdo(6) = -1 Then itemDiag.SubItems(2) = "Si" Else itemDiag.SubItems(2) = "No"
    If Not IsNull(rdo(4)) Then itemDiag.SubItems(3) = CStr(rdo(4))
    If Not IsNull(rdo(5)) Then itemDiag.SubItems(4) = CStr(rdo(5))
    rdo.MoveNext
  Wend
  rdo.Close

End Sub
Private Sub optDiagIni_Click(Index As Integer)
  
  Call Vaciarlvw
  If Index = 1 Then
    If optDiag(1).Value = True Then optDiag(2).Value = True
    optDiag(1).Enabled = False
    cmdInfor.Enabled = True
    optDiag(2).Enabled = True
  ElseIf Index = 0 Then
    cmdInfor.Enabled = False
    optDiag(1).Enabled = True
    optDiag(2).Enabled = True
  ElseIf Index = 2 Then
    cmdInfor.Enabled = False
    optDiag(1).Enabled = False
    optDiag(2).Enabled = False
  End If
End Sub
Sub Vaciarlvw()
  lvwPr.ListItems.Clear
  lvwDiag.ListItems.Clear
  nRef = ""
End Sub

Sub Dibujar()
  AnchoIni = 5790
  LeftIni = 5625
  lvwPr.Height = lvwDiag.Height
  lvwDiag.Width = AnchoIni
  lvwDiag.Left = LeftIni
  picSep.Width = 25
  picSep.Left = LeftIni - picSep.Width
  picSep.Top = lvwDiag.Top
  picSep.Height = lvwDiag.Height
  lvwPr.Width = picSep.Left - lvwPr.Left
End Sub

Private Sub picSep_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  mover = True
End Sub

Private Sub picSep_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim izda As Long, ancho As Long
  If mover = True Then
    If picSep.Left - lvwPr.Left + X > 100 And lvwDiag.Width - X > 100 Then
      picSep.Left = picSep.Left + X
      lvwPr.Width = picSep.Left - lvwPr.Left
      izda = picSep.Left + picSep.Width
      lvwDiag.Left = izda
      ancho = lvwDiag.Width - X
      lvwDiag.Width = ancho
    End If
  End If
End Sub

Private Sub picSep_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  mover = False
End Sub

