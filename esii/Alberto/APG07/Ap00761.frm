VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmI_Reali 
   Caption         =   "Realizaci�n de "
   ClientHeight    =   8205
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11595
   LinkTopic       =   "Form1"
   ScaleHeight     =   8205
   ScaleWidth      =   11595
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraEleccion 
      Caption         =   "Pat�logo"
      ForeColor       =   &H00800000&
      Height          =   690
      Left            =   300
      TabIndex        =   3
      Top             =   0
      Width           =   3165
      Begin SSDataWidgets_B.SSDBCombo cbossPat 
         Height          =   315
         Left            =   225
         TabIndex        =   4
         Top             =   225
         Width           =   2715
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         BevelColorFace  =   12632256
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4948
         Columns(1).Caption=   "Pat�logo"
         Columns(1).Name =   "Pat�logo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4789
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
   End
   Begin ComctlLib.ListView lvwDatos 
      Height          =   7290
      Left            =   300
      TabIndex        =   2
      Top             =   825
      Width           =   11115
      _ExtentX        =   19606
      _ExtentY        =   12859
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   10125
      TabIndex        =   1
      Top             =   150
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   8775
      TabIndex        =   0
      Top             =   150
      Width           =   1215
   End
End
Attribute VB_Name = "frmI_Reali"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Estado As Integer
Dim arr() As String

Private Sub cbossPat_CloseUp()
Dim res As Integer
  
  If cbossPat.Text <> "" Then
    Screen.MousePointer = 11
    Select Case objPipe.PipeGet("menu")
      Case "Tallado"
        res = LlenarlvwBloquesSoli
      Case "Realizacion"
        res = LlenarlvwBloquesTallados
      Case "Corte"
        res = LlenarlvwTecCorte
      Case "Tecnicas"
        res = LlenarlvwTecReali
    End Select
    Screen.MousePointer = 0
    If res = False Then
      MsgBox "No se han encontrado datos que cumplan esas condiciones", vbInformation, "B�squeda"
    End If
  End If
End Sub

Private Sub cbossPat_InitColumnProps()
Dim i As Integer
  
  For i = 1 To UBound(arr, 2)
    cbossPat.AddItem arr(1, i) & Chr$(9) & arr(2, i)
  Next i

End Sub

Private Sub cmdAceptar_Click()
Dim sql As String
Dim cBloque As Integer, carreta As Integer, i As Integer
Dim item As ListItem
  
  Select Case objPipe.PipeGet("menu")
    Case "Tallado"
      Call ActualizarEstadoBloques(apBLOQUETALLADO)
      lvwDatos.ListItems.Clear
      Call LlenarlvwBloquesSoli
    
    Case "Realizacion"
      Call ActualizarEstadoBloques(apBLOQUEREALIZADO)
      lvwDatos.ListItems.Clear
      Call LlenarlvwBloquesTallados
    
    Case "Corte"
      Call ActualizarEstadoPortas(apPORTACORTADO)
      lvwDatos.ListItems.Clear
      Call LlenarlvwTecCorte
    
    Case "Tecnicas"
      Call ActualizarEstadoPortas(apPORTAREALIZADO)
      lvwDatos.ListItems.Clear
      Call LlenarlvwTecReali
  
  End Select

End Sub

Sub ActualizarEstadoPortas(Estado As Integer)
Dim sql As String, ctrl As Integer, numItems As Integer, res As Integer
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim nRef As String, nref1 As String, cTec As Integer

  sql = "UPDATE AP3600 SET AP48_CodEstPorta = ?, "
  Select Case Estado
    Case apPORTACORTADO
      sql = sql & "AP36_FecCorte = SYSDATE "
      If cTec = apPORTASINTE�IRCONVENCIONAL Or cTec = apPORTASINTE�IRIHQ Or cTec = apPORTASINTE�IRCITO Then
        sql = sql & ", AP36_FecReali = SYSDATE "
      End If
    Case apPORTAREALIZADO
      sql = sql & "AP36_FecReali = SYSDATE "
  End Select
  sql = sql & "WHERE AP21_CodRef = ? AND AP34_CodBloq = ? AND AP36_CodPorta = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = Estado
  nRef = ""
  ctrl = -1
  For Each item In lvwDatos.ListItems
    If item.Selected = True Then
      If nRef <> Left(item.Tag, 10) And nRef <> "" Then ctrl = 1 Else ctrl = 0
      nRef = Left(item.Tag, 10) ' N�mero de referencia
      rdoQ(1) = nRef
      rdoQ(2) = Val(Mid(item.Tag, InStr(item.Tag, "I") + 1, InStr(item.Tag, "T")))
      rdoQ(3) = Val(Mid(item.Tag, InStr(item.Tag, "T") + 1, InStr(item.Tag, "P")))
      cTec = Val(Right(item.Tag, Len(item.Tag) - InStr(item.Tag, "P")))
      If Estado = apPORTACORTADO Then
        If cTec = apPORTASINTE�IRCONVENCIONAL Or cTec = apPORTASINTE�IRIHQ Or cTec = apPORTASINTE�IRCITO Then
          rdoQ(0) = apPORTAREALIZADO
        Else
          rdoQ(0) = Estado
        End If
      End If
      objApp.rdoConnect.BeginTrans
      rdoQ.Execute sql
      If rdoQ.RowsAffected = 0 Then
        objApp.rdoConnect.RollbackTrans
        MsgBox "Error en la actualizaci�n de los estados de los portas: " & nRef & " / " & rdoQ(2) & " / " & rdoQ(3), vbError, "Actualizaci�n de estados"
      Else
        res = 1
        If Estado = apPORTACORTADO Then
          res = ActualizarBloqueCortado(nRef, rdoQ(2))
'        ElseIf ctrl = 1 Then
        Else
          res = ActualizarEstPr_Micro(nRef)
        End If
        If res = 1 Then
          objApp.rdoConnect.CommitTrans
        Else
          objApp.rdoConnect.RollbackTrans
          MsgBox "Error en la actualizaci�n de los estados de los portas: " & nRef & " / " & rdoQ(2) & " / " & rdoQ(3), vbError, "Actualizaci�n de estados"
        End If
      End If
    End If
  Next item
  If ctrl = 0 And Estado = apPORTAREALIZADO Then res = ActualizarEstPr_Micro(nRef)

End Sub

Function ActualizarBloqueCortado(nRef As String, NBloq As Integer) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim item As ListItem

  sql = "UPDATE AP3400 SET AP47_CodEstBloq = ?, AP34_FecCorte = SYSDATE WHERE " _
      & "AP21_CodRef = ? AND AP34_CodBloq =?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = apBLOQUECORTADO
  rdoQ(1) = nRef
  rdoQ(2) = NBloq
  rdoQ.Execute sql
  ActualizarBloqueCortado = rdoQ.RowsAffected
End Function

Sub ActualizarEstadoBloques(Estado As Integer)
Dim sql As String, nRef As String, texto As String
Dim rdoQ As rdoQuery
Dim item As ListItem

  sql = "UPDATE AP3400 SET AP47_CodEstBloq = ?, "
  Select Case Estado
    Case apBLOQUETALLADO
      sql = sql & "AP34_FecTalla = SYSDATE "
    Case apBLOQUEREALIZADO
      sql = sql & "AP34_FecReali = SYSDATE "
    Case apBLOQUECORTADO
      sql = sql & "AP34_FecCorte = SYSDATE "
  End Select
  sql = sql & " WHERE AP21_CodRef = ? AND AP34_CodBloq =?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = Estado
  For Each item In lvwDatos.ListItems
    If item.Selected = True Then
      texto = item.Tag
      nRef = Left(texto, 10)
      rdoQ(1) = nRef
      rdoQ(2) = Right(texto, Len(texto) - 11)
      rdoQ.Execute sql
      If Estado >= apBLOQUETALLADO Then Call ActualizarEstPr_Macro(nRef)
    End If
  Next item

End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  
  Select Case objPipe.PipeGet("menu")
    Case "Tallado"
      Me.Caption = "Tallado de bloques"
      fraEleccion.Caption = "Tipos de bloques"
      Estado = apBLOQUESOLICITADO
      Call FormatearBloquesTallar
      Call LlenarTiposBloque
    Case "Realizacion"
      Me.Caption = "Realizaci�n de bloques"
      Estado = apBLOQUETALLADO
      fraEleccion.Caption = "Tipos de bloques"
      Call FormatearBloquesTallar
      Call LlenarTiposBloque
    Case "Corte"
      Me.Caption = "Realizaci�n de portas"
      Estado = apBLOQUEREALIZADO
      fraEleccion.Caption = "Tipos de bloques"
      Call LlenarTiposBloque
      Call FormatearTecCorte
    Case "Tecnicas"
      Me.Caption = "Realizaci�n de t�cnicas"
      Estado = apPORTACORTADO
      fraEleccion.Caption = "Carpetas"
      Call LlenarCarpetas
      Call FormatearTecReali
  End Select

End Sub
Sub LlenarTiposBloque()
Dim sql As String
Dim rdo As rdoResultset
Dim i As Integer

  sql = "SELECT AP05_CodTiBloq, AP05_Desig FROM AP0500 ORDER BY AP05_CodTiBloq ASC"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  ReDim Preserve arr(1 To 2, 1 To 1)
  arr(1, 1) = ""
  arr(2, 1) = "Todos"
  i = 1
  While rdo.EOF = False
    i = i + 1
    ReDim Preserve arr(1 To 2, 1 To i)
    arr(1, i) = rdo(0)
    arr(2, i) = rdo(1)
    rdo.MoveNext
  Wend
  rdo.Close
End Sub
Sub LlenarCarpetas()
Dim sql As String
Dim rdo As rdoResultset
Dim i As Integer

  sql = "SELECT AP06_Codcarp, AP06_Desig FROM AP0600 ORDER BY AP06_CodCarp ASC"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  ReDim Preserve arr(1 To 2, 1 To 1)
  arr(1, 1) = ""
  arr(2, 1) = "Todas"
  i = 1
  While rdo.EOF = False
    i = i + 1
    ReDim Preserve arr(1 To 2, 1 To i)
    arr(1, i) = rdo(0)
    arr(2, i) = rdo(1)
    rdo.MoveNext
  Wend
  rdo.Close
End Sub

Sub FormatearBloquesTallar()
  With lvwDatos
    .ColumnHeaders.Add , , "Bloque", 1500
    .ColumnHeaders.Add , , "Tipo de bloque", 1800
    .ColumnHeaders.Add , , "Muestra", 4000
    .ColumnHeaders.Add , , "Observaciones", 6000
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
End Sub
Sub FormatearTecCorte()
  With lvwDatos
    .ColumnHeaders.Add , , "T�cnica", 1500
    .ColumnHeaders.Add , , "Tipo de t�cnica", 1800
    .ColumnHeaders.Add , , "Bloque", 1800
    .ColumnHeaders.Add , , "Observaciones", 6000
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
End Sub
Sub FormatearTecReali()
  With lvwDatos
    .ColumnHeaders.Add , , "T�cnica", 1500
    .ColumnHeaders.Add , , "Tipo de t�cnica", 1800
    .ColumnHeaders.Add , , "Observaciones", 6000
    .View = lvwReport
    .MultiSelect = True
    .LabelEdit = lvwManual
  End With
End Sub
Function LlenarlvwBloquesSoli() As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim i As Integer, carr As String, texto As String

  LlenarlvwBloquesSoli = False
  lvwDatos.ListItems.Clear
  For i = 1 To UBound(arr, 2)
    If cbossPat.Text = arr(2, i) Then
      carr = arr(1, i)
      Exit For
    End If
  Next i
  
  sql = "SELECT AP3400.AP21_CodRef, AP0500.AP05_Desig, AP3400.AP34_CodBloq, " _
      & "PR2400.PR24DesCorta||'. '||PR5200.PR52DesMuestra, AP3400.AP34_Observ, " _
      & "AP3400.AP34_NumBloq FROM " _
      & "AP0500, AP3400, AP2500, PR5200, PR2400 WHERE " _
      & "AP3400.AP05_CodTiBloq = AP0500.AP05_CodTiBloq AND " _
      & "AP3400.AP21_CodRef = AP2500.AP21_CodRef AND " _
      & "AP3400.PR52NumIDMuestra = AP2500.PR52NumIDMuestra AND " _
      & "AP2500.PR52NumMuestra = PR5200.PR52NumMuestra AND " _
      & "PR5200.PR24CodMuestra = PR2400.PR24CodMuestra AND " _
      & "AP3400.AP47_CodEstBloq = ?"
  If carr <> "" Then sql = sql & " AND AP3400.AP05_CodTiBloq = ?"
  sql = sql & " ORDER BY AP0500.AP05_CodTiBloq ASC, AP3400.AP21_CodRef ASC, AP3400.AP34_CodBloq ASC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = Estado
  If carr <> "" Then
    rdoQ(1) = carr
  End If
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    LlenarlvwBloquesSoli = True
    If IsNull(rdo(5)) Then
      texto = Right(rdo(0), 8)
    Else
      texto = Right(rdo(0), 8) & " - " & rdo(5)
    End If
    Set item = lvwDatos.ListItems.Add(, , texto)
    item.Tag = rdo(0) & "R" & rdo(2)
    item.SubItems(1) = CStr(rdo(1))
    item.SubItems(2) = CStr(rdo(3))
    If IsNull(rdo(4)) Then item.SubItems(3) = "" Else item.SubItems(3) = CStr(rdo(4))
    rdo.MoveNext
  Wend
  rdo.Close
  Call CerrarrdoQueries

End Function
Function LlenarlvwBloquesTallados() As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim i As Integer, carr As String, texto As String

  LlenarlvwBloquesTallados = False
  lvwDatos.ListItems.Clear
  For i = 1 To UBound(arr, 2)
    If cbossPat.Text = arr(2, i) Then
      carr = arr(1, i)
      Exit For
    End If
  Next i
  
  sql = "SELECT AP3400.AP21_CodRef, AP0500.AP05_Desig, AP3400.AP34_CodBloq, " _
      & "PR2400.PR24DesCorta||'. '||PR5200.PR52DesMuestra, AP3400.AP34_Observ, " _
      & "AP3400.AP34_NumBloq FROM " _
      & "AP0500, AP3400, AP2500, PR5200, PR2400 WHERE " _
      & "AP3400.AP05_CodTiBloq = AP0500.AP05_CodTiBloq AND " _
      & "AP3400.AP21_CodRef = AP2500.AP21_CodRef AND " _
      & "AP3400.PR52NumIDMuestra = AP2500.PR52NumIDMuestra AND " _
      & "AP2500.PR52NumMuestra = PR5200.PR52NumMuestra AND " _
      & "PR5200.PR24CodMuestra = PR2400.PR24CodMuestra AND " _
      & "AP3400.AP47_CodEstBloq = ?"
  If carr <> "" Then sql = sql & " AND AP3400.AP05_CodTiBloq = ?"
  sql = sql & " ORDER BY AP0500.AP05_CodTiBloq ASC, AP3400.AP21_CodRef ASC, AP3400.AP34_CodBloq ASC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = Estado
  If carr <> "" Then
    rdoQ(1).Type = rdTypeINTEGER
    rdoQ(1) = carr
  End If
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    LlenarlvwBloquesTallados = True
    If IsNull(rdo(5)) Then
      texto = Right(rdo(0), 8)
    Else
      texto = Right(rdo(0), 8) & " - " & rdo(5)
    End If
    Set item = lvwDatos.ListItems.Add(, , texto)
    item.Tag = rdo(0) & "R" & rdo(2)
    item.SubItems(1) = CStr(rdo(1))
    item.SubItems(2) = CStr(rdo(3))
    If IsNull(rdo(4)) Then item.SubItems(3) = "" Else item.SubItems(3) = CStr(rdo(4))
    rdo.MoveNext
  Wend
  rdo.Close
  Call CerrarrdoQueries

End Function


Function LlenarlvwTecCorte() As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim i As Integer, TiBloq As String, texto As String

  LlenarlvwTecCorte = False
  lvwDatos.ListItems.Clear
  For i = 1 To UBound(arr, 2)
    If cbossPat.Text = arr(2, i) Then
      TiBloq = arr(1, i)
      Exit For
    End If
  Next i
  
  sql = "SELECT /*+ RULE */ AP3600.AP21_CodRef, AP3400.AP34_CodBloq, AP3600.AP36_CodPorta, " _
      & "AP1200.AP12_Desig, AP0500.AP05_Desig, AP3600.AP36_Observ, AP3600.AP12_CodTec,AP3400.AP34_NumBloq FROM " _
      & "AP0500, AP1200, AP3400, AP3600 WHERE " _
      & "AP3600.AP12_CodTec = AP1200.AP12_CodTec AND " _
      & "AP3600.AP21_CodRef = AP3400.AP21_CodRef AND " _
      & "AP3600.AP34_CodBloq = AP3400.AP34_CodBloq AND " _
      & "AP3400.AP05_CodTiBloq = AP0500.AP05_CodTiBloq AND " _
      & "AP3400.AP47_CodEstBloq >= ? AND " _
      & "AP3600.AP48_CodEstPorta = ?"
  If TiBloq <> "" Then sql = sql & " AND AP3400.AP05_CodTiBloq = ?"
  sql = sql & " ORDER BY AP0500.AP05_CodTiBloq ASC, AP3400.AP21_CodRef ASC, " _
      & "AP3400.AP34_CodBloq ASC, AP3600.AP36_CodPorta ASC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = Estado
  rdoQ(1) = apPORTASOLICITADO
  If TiBloq <> "" Then
    rdoQ(2).Type = rdTypeINTEGER
    rdoQ(2) = TiBloq
  End If
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    LlenarlvwTecCorte = True
    If IsNull(rdo(7)) Then
      texto = Right(rdo(0), 8) & " -  - " & rdo(2)
    Else
      texto = Right(rdo(0), 8) & " - " & rdo(7) & " - " & rdo(2)
    End If
    Set item = lvwDatos.ListItems.Add(, , texto)
    item.Tag = rdo(0) & "I" & rdo(1) & "T" & rdo(2) & "P" & rdo(6)
    item.SubItems(1) = CStr(rdo(3))
    item.SubItems(2) = CStr(rdo(4))
    If IsNull(rdo(5)) Then item.SubItems(3) = "" Else item.SubItems(3) = CStr(rdo(5))
    rdo.MoveNext
  Wend
  rdo.Close
  Call CerrarrdoQueries

End Function
Function LlenarlvwTecReali() As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim i As Integer, carr As String, texto As String

  LlenarlvwTecReali = False
  lvwDatos.ListItems.Clear
  For i = 1 To UBound(arr, 2)
    If cbossPat.Text = arr(2, i) Then
      carr = arr(1, i)
      Exit For
    End If
  Next i
  
  sql = "SELECT /*+ RULE */ AP3600.AP21_CodRef, AP3400.AP34_CodBloq, AP3600.AP36_CodPorta, " _
      & "AP1200.AP12_Desig, AP0500.AP05_Desig, AP3600.AP36_Observ, AP3600.AP12_CodTec, " _
      & "AP3400.AP34_NumBloq FROM " _
      & "AP0500, AP1200, AP3400, AP3600, AP0400 WHERE " _
      & "AP3600.AP12_CodTec = AP1200.AP12_CodTec AND " _
      & "AP1200.AP04_CodTiTec = AP0400.AP04_CodTiTec AND " _
      & "AP3600.AP21_CodRef = AP3400.AP21_CodRef AND " _
      & "AP3600.AP34_CodBloq = AP3400.AP34_CodBloq AND " _
      & "AP3400.AP05_CodTiBloq = AP0500.AP05_CodTiBloq AND " _
      & "AP3400.AP47_CodEstBloq >= ? AND " _
      & "AP3600.AP48_CodEstPorta = ?"
  If carr <> "" Then sql = sql & " AND AP0400.AP06_CodCarp = ?"
  sql = sql & " ORDER BY AP0500.AP05_CodTiBloq ASC, AP3400.AP21_CodRef ASC, " _
            & "AP3400.AP34_CodBloq ASC, AP3600.AP36_CodPorta"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = Estado
  rdoQ(1) = apPORTACORTADO
  If carr <> "" Then
    rdoQ(2).Type = rdTypeINTEGER
    rdoQ(2) = carr
  End If
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    LlenarlvwTecReali = True
    If IsNull(rdo(7)) Then
      texto = Right(rdo(0), 8) & " -  - " & rdo(2)
    Else
      texto = Right(rdo(0), 8) & " - " & rdo(7) & " - " & rdo(2)
    End If
    Set item = lvwDatos.ListItems.Add(, , texto)
    item.Tag = rdo(0) & "I" & rdo(1) & "T" & rdo(2) & "P" & rdo(6)
    item.SubItems(1) = CStr(rdo(3))
    If IsNull(rdo(5)) Then item.SubItems(2) = "" Else item.SubItems(2) = CStr(rdo(5))
    rdo.MoveNext
  Wend
  rdo.Close
  Call CerrarrdoQueries

End Function


Private Sub Form_Unload(Cancel As Integer)
  Erase arr
End Sub
