VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmImpronta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Petici�n de impronta"
   ClientHeight    =   4485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6915
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   6915
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   225
      Picture         =   "ap00771.frx":0000
      ScaleHeight     =   615
      ScaleWidth      =   540
      TabIndex        =   6
      Top             =   3150
      Width           =   540
   End
   Begin VB.CommandButton cmdNo 
      Caption         =   "&No"
      Height          =   465
      Left            =   3900
      TabIndex        =   5
      Top             =   3900
      Width           =   1215
   End
   Begin VB.CommandButton cmdSi 
      Caption         =   "&S�"
      Height          =   465
      Left            =   1800
      TabIndex        =   4
      Top             =   3900
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Doctores asociados a la impronta"
      ForeColor       =   &H00800000&
      Height          =   2865
      Left            =   225
      TabIndex        =   0
      Top             =   150
      Width           =   6240
      Begin ComctlLib.ListView lvwRes 
         Height          =   2340
         Left            =   3225
         TabIndex        =   2
         Top             =   375
         Width           =   2715
         _ExtentX        =   4789
         _ExtentY        =   4128
         View            =   2
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   16777215
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Residente"
            Object.Width           =   2540
         EndProperty
      End
      Begin ComctlLib.ListView lvwPat 
         Height          =   2340
         Left            =   300
         TabIndex        =   1
         Top             =   375
         Width           =   2715
         _ExtentX        =   4789
         _ExtentY        =   4128
         View            =   2
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   16777215
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Pat�logo"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Label lblImpronta 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   1125
      TabIndex        =   3
      Top             =   3150
      Width           =   5340
   End
End
Attribute VB_Name = "frmImpronta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public seguir As Integer, cPat As String, cRes As String
Option Explicit

Private Sub cmdNo_Click()
  seguir = False
  Me.Hide
End Sub

Private Sub cmdSi_Click()
  If cPat = "" Then
    MsgBox "Debe seleccionar un pat�logo responsable de la impronta", vbInformation, "Impronta"
  Else
    seguir = True
    Me.Hide
  End If
End Sub

Private Sub Form_Load()
  Call LlenarDr(apCODPATOLOGO, lvwPat)
  Call LlenarDr(apCODRES, lvwRes)
  lblImpronta.Caption = "�Desea realizar una impronta sobre la muestra de la biopsia seleccionada?"
End Sub
Sub LlenarDr(cod As Integer, lvw As ListView)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem
  
  sql = "SELECT SG02Cod, NVL(SG02TxtFirma, SG02Nom||' '||SG02Ape1||' '||SG02Ape2) " _
      & "FROM SG0200 WHERE SG02Cod IN (" _
      & "SELECT AD0300.SG02Cod FROM AD0300, AD3300 WHERE " _
      & "AD0300.AD31CodPuesto = AD3300.AD31CodPuesto AND " _
      & "AD0300.AD02CodDpto = AD3300.AD02CodDpto AND " _
      & "AD0300.AD02CodDpto = " & cDptAP & " AND " _
      & "AD3300.AD33IndFirma = -1) AND " _
      & "SG02FECDES IS NULL"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set item = lvw.ListItems.Add(, , rdo(1))
    item.Tag = rdo(0)
    rdo.MoveNext
  Wend
  rdo.Close

End Sub
Private Sub lvwPat_ItemClick(ByVal item As ComctlLib.ListItem)
  cPat = item.Tag
End Sub
Private Sub lvwRes_ItemClick(ByVal item As ComctlLib.ListItem)
  cRes = item.Tag
End Sub
