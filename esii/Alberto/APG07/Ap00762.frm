VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmDatosPr 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Datos adicionales a la prueba"
   ClientHeight    =   6870
   ClientLeft      =   240
   ClientTop       =   1560
   ClientWidth     =   11325
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6870
   ScaleWidth      =   11325
   Begin VB.PictureBox picSep 
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   0
      MousePointer    =   9  'Size W E
      ScaleHeight     =   465
      ScaleWidth      =   120
      TabIndex        =   13
      Top             =   0
      Width           =   120
   End
   Begin VB.CommandButton cmdIntra 
      Caption         =   "&Marcar como Intra"
      Enabled         =   0   'False
      Height          =   390
      Left            =   9375
      TabIndex        =   12
      Top             =   750
      Width           =   1515
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   9375
      TabIndex        =   1
      Top             =   225
      Width           =   1515
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir datos"
      Height          =   390
      Left            =   7725
      TabIndex        =   11
      Top             =   225
      Width           =   1515
   End
   Begin VB.CommandButton cmdEtiquetas 
      Caption         =   "Imprimir &etiquetas"
      Enabled         =   0   'False
      Height          =   390
      Left            =   7725
      TabIndex        =   10
      Top             =   750
      Width           =   1515
   End
   Begin VB.Frame Frame1 
      Caption         =   "Identificaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1365
      Index           =   0
      Left            =   300
      TabIndex        =   2
      Top             =   150
      Width           =   6315
      Begin VB.TextBox txtMuestra 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   2
         Left            =   1275
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   825
         Width           =   1065
      End
      Begin VB.TextBox txtMuestra 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   3
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   825
         Width           =   3690
      End
      Begin VB.TextBox txtMuestra 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   0
         Left            =   1275
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   375
         Width           =   690
      End
      Begin VB.TextBox txtMuestra 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   1
         Left            =   2025
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   375
         Width           =   4065
      End
      Begin VB.Label Label2 
         Caption         =   "Prueba:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   450
         TabIndex        =   7
         Top             =   900
         Width           =   690
      End
      Begin VB.Label Label2 
         Caption         =   "Paciente:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   375
         TabIndex        =   4
         Top             =   450
         Width           =   840
      End
   End
   Begin ComctlLib.TreeView tvwPr 
      Height          =   5040
      Left            =   300
      TabIndex        =   0
      Top             =   1650
      Width           =   5940
      _ExtentX        =   10478
      _ExtentY        =   8890
      _Version        =   327682
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin ComctlLib.TreeView tvwMaestro 
      Height          =   5040
      Left            =   6300
      TabIndex        =   9
      Top             =   1650
      Width           =   4590
      _ExtentX        =   8096
      _ExtentY        =   8890
      _Version        =   327682
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin ComctlLib.ImageList imgIcos1 
      Left            =   6975
      Top             =   225
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   6
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Ap00762.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Ap00762.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Ap00762.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Ap00762.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Ap00762.frx":0C68
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Ap00762.frx":0F82
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmDatosPr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Org As String
Dim Pedir As Integer
Dim cod As String
Dim nRef As String
Dim cDr As String
Dim mover As Integer

Private Sub cmdImprimir_Click()
Dim sql As String, TiPac As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  Call ImprimirHoja(nRef)
'  sql = "SELECT AP21_TiPac FROM AP2100 WHERE AP21_CodRef = ?"
'  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
'  rdoQ(0).Type = rdTypeVARCHAR
'  rdoQ(0) = nRef
'  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
'  TiPac = rdo(0)
'  Call ImprimirHojaRecepcion1(nRef, TiPac)
End Sub

Private Sub cmdEtiquetas_Click()
Dim clave As String

'  On Error Resume Next
  clave = tvwPr.SelectedItem.key
  If Left(clave, 1) = "M" Then
    Call ImprimirEtiqueta(nRef, "(" & Right(clave, Len(clave) - 1) & ")")
  Else
    MsgBox "Debe seleccionar una muestra para imprimir su etiqueta.", vbInformation + vbOKOnly, "Impresi�n de etiquetas"
  End If
  Set tvwPr.DropHighlight = Nothing
End Sub

Private Sub cmdIntra_Click()
Dim sql As String
Dim rdoQ As rdoQuery
Dim clave As String, res As Integer, intra As Integer

  On Error Resume Next
  clave = tvwPr.SelectedItem.key
  If Left(clave, 1) = "M" Then
    intra = tvwPr.SelectedItem.Tag
    If intra = -1 Then
      res = MsgBox("�Est� seguro que desea que la muestra deje de ser intraoperatoria?", vbQuestion + vbYesNo, "Muestras intraoperatorias")
    Else
      res = MsgBox("�Est� seguro que desea marcar muestra como intraoperatoria?", vbQuestion + vbYesNo, "Muestras intraoperatorias")
    End If
    If res = vbYes Then
      sql = "UPDATE PR5200 SET PR52IndIntra = ? WHERE PR52NumMuestra = (" _
          & "SELECT PR52NumMuestra FROM AP2500 WHERE AP21_CodRef = ? AND PR52NumIDMuestra = ?)"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      If intra = -1 Then rdoQ(0) = 0 Else rdoQ(0) = -1
      rdoQ(1) = nRef
      rdoQ(2) = Val(Right(clave, Len(clave) - 1))
      rdoQ.Execute
      Call CambiarTvw(tvwPr.Nodes(clave), intra)
      Call CambiarTvw(frmP_Peticion.tvwMuestra.Nodes(clave), intra)
      If intra = -1 Then cmdIntra.Caption = "Marcar como Intra" Else cmdIntra.Caption = "Quitar como Intra"
    End If
  Else
    MsgBox "Debe seleccionar una muestra.", vbInformation + vbOKOnly, "Muestras intraoperatorias"
  End If
  Set tvwPr.DropHighlight = Nothing
End Sub
Sub CambiarTvw(nodo As Node, intra As Integer)
  With nodo
    If intra = -1 Then
      .Image = apICONMUESTRA
      .SelectedImage = apICONMUESTRA
      .Tag = 0
    Else
      .Image = apICONINTRA
      .SelectedImage = apICONINTRA
      .Tag = -1
    End If
  End With
End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  nRef = objPipe.PipeGet("NumRef")
  Call objPipe.PipeSet("Cambios", False)
  Set tvwPr.ImageList = frmPrincipal.imgIcos
  Set tvwMaestro.ImageList = frmPrincipal.imgIcos
  Call ConfigurarTvw
  Call LlenarDatosPr
  Call LlenarMaestro
  Call Dibujar
End Sub
Sub Dibujar()
  picSep.Width = 25
  picSep.Left = tvwPr.Left + tvwPr.Width
  picSep.Top = tvwPr.Top
  picSep.Height = tvwPr.Height
End Sub
Private Sub picSep_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  mover = True
End Sub

Private Sub picSep_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim izda As Long, ancho As Long
  If mover = True Then
    If picSep.Left - tvwPr.Left + X > 100 And tvwMaestro.Width - X > 100 Then 'And 2265 + fraPr.Width - 6945 > 100 Then
      picSep.Left = picSep.Left + X
      tvwPr.Width = picSep.Left - tvwPr.Left
      tvwMaestro.Left = picSep.Left + picSep.Width
      ancho = tvwPr.Width - X
      tvwMaestro.Width = tvwMaestro.Width - X
    End If
  End If
End Sub

Private Sub picSep_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  mover = False
End Sub

Sub ConfigurarTvw()
Dim nodo As Node

  Set nodo = tvwPr.Nodes.Add(, , "Raiz", "Prueba", 1, 1)
  Set nodo = tvwPr.Nodes.Add("Raiz", tvwChild, "Organos", "Muestras", apICONMUESTRAS, apICONMUESTRAS)
  Set nodo = tvwPr.Nodes.Add("Raiz", tvwChild, "Gente", "Personal", apICONPERSONAL, apICONPERSONAL)
  Set nodo = tvwPr.Nodes.Add("Raiz", tvwChild, "Analitica", "Anal�tica", apICONANALITICA, apICONANALITICA)
  Set nodo = tvwPr.Nodes.Add("Raiz", tvwChild, "Datos", "Datos complementarios", apICONDATO, apICONDATO)
  nodo.EnsureVisible
  Set nodo = tvwMaestro.Nodes.Add(, , "Inicio", "Datos generales", 1, 1)
  Set nodo = tvwMaestro.Nodes.Add("Inicio", tvwChild, "Muestras", "Organos", apICONMUESTRAS, apICONMUESTRAS)
  Set nodo = tvwMaestro.Nodes.Add("Inicio", tvwChild, "Gente", "Personal", apICONPERSONAL, apICONPERSONAL)
  nodo.EnsureVisible
  Set nodo = tvwMaestro.Nodes.Add("Gente", tvwChild, "Jefe", "Pat�logos", apICONPAT, apICONPAT)
  Set nodo = tvwMaestro.Nodes.Add("Gente", tvwChild, "Dr", "Residentes", apICONRES, apICONRES)
  Set nodo = tvwMaestro.Nodes.Add("Gente", tvwChild, "TecCito", "Personal de citolog�as", apICONCITO, apICONCITO)
  Set nodo = tvwMaestro.Nodes.Add("Gente", tvwChild, "TecBio", "Personal de biopsias", apICONPERSBIO, apICONPERSBIO)
End Sub
Sub LlenarDatosPr()
  txtMuestra(0).Text = Val(frmP_Peticion.txtPaciente(0))
  txtMuestra(1).Text = frmP_Peticion.txtPaciente(1)
  txtMuestra(2).Text = frmP_Peticion.txtPr(0)
  txtMuestra(3).Text = frmP_Peticion.txtPr(1)
  
  Call LlenarMuestras(nRef)
  Call LlenarPersonal(nRef)
  Call LlenarAnalitica(Val(frmP_Peticion.txtPaciente(0)))
  Call LlenarDatos(nRef)
  
End Sub
Sub LlenarMaestro()
  Call LlenarOrgano
  Call LlenarPersonalMaestra
End Sub
Sub LlenarPersonalMaestra()
Dim sql As String
Dim rdo As rdoResultset
Dim nodo As Node
  
  sql = "SELECT AP0800.SG02Cod, SG0200.SG02TxtFirma, AP0800.AP07_CodTiPers, AP0700.AP07_Desig " _
      & "FROM AP0800, SG0200, AP0700 WHERE " _
      & "AP0800.SG02Cod = SG0200.SG02Cod AND " _
      & "AP0800.AP07_CodTiPers = AP0700.AP07_CodTiPers " _
      & "ORDER BY AP0800.AP07_CodTiPers"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Select Case rdo(2)
      Case apCODPATOLOGO
        Set nodo = tvwMaestro.Nodes.Add("Jefe", tvwChild, "P" & rdo(0), rdo(1), apICONPAT, apICONPAT)
        nodo.Tag = rdo(3)
      Case apCODRES
        Set nodo = tvwMaestro.Nodes.Add("Dr", tvwChild, "R" & rdo(0), rdo(1), apICONRES, apICONRES)
      Case apCODPERSCITO
        Set nodo = tvwMaestro.Nodes.Add("TecCito", tvwChild, "C" & rdo(0), rdo(1), apICONCITO, apICONCITO)
      Case apCODPERSBIO
        Set nodo = tvwMaestro.Nodes.Add("TecBio", tvwChild, "B" & rdo(0), rdo(1), apICONPERBIO, apICONPERBIO)
    End Select
    rdo.MoveNext
  Wend
End Sub
Sub LlenarOrgano()
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim nodo As Node
  
  sql = "SELECT PR24CodMuestra, PR24DesCorta FROM PR2400 " _
        & "WHERE PR24CodMuestra IN (" _
        & "SELECT PR24CodMuestra FROM PR2500 WHERE " _
        & "PR01CodActuacion = ?)" _
        & "ORDER BY PR24DesCorta "
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = Val(Left(txtMuestra(3).Text, InStr(txtMuestra(3).Text, "-")))
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set nodo = tvwMaestro.Nodes.Add("Muestras", tvwChild, "O" & rdo(0), rdo(1), apICONMUESTRA, apICONMUESTRA)
    rdo.MoveNext
  Wend
End Sub
Sub LlenarMuestras(nRef As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo As Node

  Org = "("
  sql = "SELECT PR5200.PR24CodMuestra, PR2400.PR24DesCompleta||'. '||PR5200.PR52DesMuestra, " _
      & "PR52IndIntra, NVL(SG0200.SG02TxtFirma,SG0200.SG02Ape1||' '||SG0200.SG02Ape2||', '||SG0200.SG02Nom), " _
      & "AP2500.PR52NumMuestra, PR5200.PR52NumIDMuestra " _
      & "FROM AP2500, PR5200, PR2400, SG0200 WHERE " _
      & "AP2500.PR52NumMuestra = PR5200.PR52NumMuestra AND " _
      & "PR5200.PR24CodMuestra = PR2400.PR24CodMuestra AND " _
      & "AP2500.SG02Cod = SG0200.SG02Cod AND " _
      & "AP2500.AP21_CodRef = ? ORDER BY PR5200.PR52NumIDMuestra"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Org = Org & rdo(0) & ","
    If rdo(2) = 0 Then
      Set nodo = tvwPr.Nodes.Add("Organos", tvwChild, "M" & rdo(5), rdo(5) & ". " & rdo(1), apICONMUESTRA, apICONMUESTRA)
    Else
      Set nodo = tvwPr.Nodes.Add("Organos", tvwChild, "M" & rdo(5), rdo(5) & ". " & rdo(1), apICONINTRA, apICONINTRA)
    End If
    nodo.Tag = rdo(2)
    
    Set nodo = tvwPr.Nodes.Add("M" & rdo(5), tvwChild, "Z" & rdo(0), "Recepciona: " & rdo(3), apICONPERBIO, apICONPERBIO)
    Call LlenarCuestionarioMuestra(rdo(4), nRef)
    rdo.MoveNext
  Wend
  Org = Left(Org, Len(Org) - 1) & ")"
  rdo.Close
End Sub
Sub LlenarCuestionarioMuestra(nMues As Long, nRef As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo As Node

  sql = "SELECT PR4000.PR40DesPregunta, PR5400.PR54Respuesta " _
      & "FROM PR5400, PR4000 WHERE " _
      & "PR5400.PR40CodPregunta = PR4000.PR40CodPregunta AND " _
      & "PR5400.PR52NumMuestra = ? " _
      & "UNION " _
      & "SELECT PR4000.PR40DesPregunta, PR4300.PR43Respuesta " _
      & "FROM PR4300, PR4000, PR0400 WHERE " _
      & "PR4300.PR40CodPregunta = PR4000.PR40CodPregunta AND " _
      & "PR4300.PR04NumActPlan = PR0400.PR04NumActPlan AND " _
      & "PR0400.PR04NumIDActDePr = ? AND " _
      & "PR4300.PR52NumMuestra = ?"

  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nMues
  rdoQ(1) = nRef
  rdoQ(2) = nMues
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set nodo = tvwPr.Nodes.Add("M" & rdo(0), tvwChild, , rdo(0) & ": " & rdo(1), apICONDATO, apICONDATO)
    rdo.MoveNext
  Wend
 
End Sub
Sub LlenarPersonal(nRef As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo As Node

  sql = "SELECT Pat.SG02Cod, Pat.SG02TxtFirma, Res.SG02Cod, Res.SG02TxtFirma, " _
      & "Cito.SG02Cod, Cito.SG02TxtFirma " _
      & "FROM AP2100, SG0200 Pat, SG0200 Res, SG0200 Cito WHERE " _
      & "AP2100.SG02Cod_Pat = Pat.SG02Cod AND " _
      & "AP2100.SG02Cod_Res = Res.SG02Cod (+) AND " _
      & "AP2100.SG02Cod_Cito = Cito.SG02Cod (+) AND " _
      & "AP2100.AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  Set nodo = tvwPr.Nodes.Add("Gente", tvwChild, "P" & rdo(0), "Pat�logo responsable: " & rdo(1), apICONPAT, apICONPAT)
  Set nodo = tvwPr.Nodes.Add("Gente", tvwChild, "R" & rdo(2), "Descripci�n macrosc�pica: " & rdo(3), apICONRES, apICONRES)
  If InStr(nRef, "C") > 0 Then
    Set nodo = tvwPr.Nodes.Add("Gente", tvwChild, "C" & rdo(4), "Citot�cnico: " & rdo(5), apICONCITO, apICONCITO)
  End If
  rdo.Close
End Sub

Sub LlenarAnalitica(NH As Long)
Dim texto As String
Dim nodo As Node
Dim Ana() As ResAnalitica
Dim i As Integer
Dim j As Integer
  
  If BuscarAnalitica(NH, Org, Ana()) = True Then
    For i = 1 To UBound(Ana)
      Set nodo = tvwPr.Nodes.Add("Analitica", tvwChild, "A" & Ana(i).cPr, Ana(i).pr & "  " & Ana(i).fecha, apICONPRUEBAANALITICA, apICONPRUEBAANALITICA)
      With Ana(i)
        For j = 1 To UBound(.ResPr)
          texto = .ResPr(j).res & ": " & .ResPr(j).Resultado & " " & .ResPr(j).Unidades
          Set nodo = tvwPr.Nodes.Add("A" & .cPr, tvwChild, "A" & .cPr & "R" & .ResPr(j).cRes, texto, apICONRESULTADOANALITICA, apICONRESULTADOANALITICA)
        Next j
      End With
    Next i
  End If

End Sub

Sub LlenarDatos(nRef As String)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo As Node

  sql = "SELECT PR4000.PR40DesPregunta, PR4100.PR41Respuesta " _
      & "FROM PR4000, PR4100, PR0300, PR0400 WHERE " _
      & "PR0400.PR03NumActPedi = PR0300.PR03NumActPedi AND " _
      & "PR0300.PR03NumActPedi = PR4100.PR03NumActPedi AND " _
      & "PR4100.PR40CodPregunta = PR4000.PR40CodPregunta AND " _
      & "PR0400.PR04NumIDActDepr = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    Set nodo = tvwPr.Nodes.Add("Datos", tvwChild, , rdo(0) & ": " & rdo(1), apICONRESDATO, apICONRESDATO)
    rdo.MoveNext
  Wend
End Sub



Private Sub tvwMaestro_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
  Select Case State
    Case 1
      tvwMaestro.DragIcon = frmPrincipal.imgIcos.ListImages(apICONPROHIBIDO).Picture
    Case 0
      tvwMaestro.DragIcon = frmPrincipal.imgIcos.ListImages(Source.SelectedItem.Image).Picture
  End Select
End Sub

Private Sub tvwMaestro_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim KeyTecnica As String, nodo As Node
  
  On Error Resume Next
  Set tvwMaestro.DropHighlight = Nothing
  Set nodo = tvwMaestro.HitTest(X, Y)
  tvwMaestro.SelectedItem = nodo
  KeyTecnica = nodo.key
  Select Case Left(KeyTecnica, 1)
    Case "P"
      Pedir = 1
      tvwMaestro.Drag 1
      cod = Right$(KeyTecnica, Len(KeyTecnica) - 1)
      cDr = nodo.Tag
    Case "R"
      Pedir = 2
      tvwMaestro.Drag 1
      cod = Right(KeyTecnica, Len(KeyTecnica) - 1)
    Case "C"
      Pedir = 3
      tvwMaestro.Drag 1
      cod = Right(KeyTecnica, Len(KeyTecnica) - 1)
    Case "O"
      Pedir = 4
      tvwMaestro.Drag 1
      cod = Right(KeyTecnica, Len(KeyTecnica) - 1)
    Case "B"
      Pedir = 5
      tvwMaestro.Drag 1
      cod = Right(KeyTecnica, Len(KeyTecnica) - 1)
  End Select
End Sub

Private Sub tvwPr_DragDrop(Source As Control, X As Single, Y As Single)
Dim clave As String, TiMues As String
  
  On Error Resume Next
  clave = tvwPr.DropHighlight.key
  If Source.Name = "tvwMaestro" Then 'Se permiten cambiar datos
    Select Case Pedir
      Case 1  ' Se ha seleccionado un pat�logo
        If Left$(clave, 1) = "P" Then
          Call ActualizarPatologo
          tvwPr.Nodes.item(clave).Text = "Pat�logo responsable: " & tvwMaestro.SelectedItem.Text
        ElseIf Left$(clave, 1) = "R" Then
          Call ActualizarRes
          tvwPr.Nodes.item(clave).Text = "Descripci�n macrosc�pica: " & tvwMaestro.SelectedItem.Text
        ElseIf Left$(clave, 1) = "Z" Then
          Call ActualizarRecep
          tvwPr.Nodes.item(clave).Text = "Recepciona: " & tvwMaestro.SelectedItem.Text
        End If
      Case 2  ' Se ha seleccionado un residente
        If Left$(clave, 1) = "R" Then
          Call ActualizarRes
          tvwPr.Nodes.item(clave).Text = "Descripci�n macrosc�pica: " & tvwMaestro.SelectedItem.Text
        ElseIf Left$(clave, 1) = "Z" Then
          Call ActualizarRecep
          tvwPr.Nodes.item(clave).Text = "Recepciona: " & tvwMaestro.SelectedItem.Text
        End If
      Case 3  ' Se ha seleccionado un citot�cnico
        If Left$(clave, 1) = "C" Then
          Call ActualizarCito
          tvwPr.Nodes.item(clave).Text = "Citot�cnico: " & tvwMaestro.SelectedItem.Text
        ElseIf Left$(clave, 1) = "Z" Then
          Call ActualizarRecep
          tvwPr.Nodes.item(clave).Text = "Recepciona: " & tvwMaestro.SelectedItem.Text
        End If
      Case 4  ' Se ha seleccionado un organo
        If Left$(clave, 1) = "M" Then
          Load frmModMuestras
          With frmModMuestras
            .txtMuestra.Text = tvwMaestro.SelectedItem.Text
            .Show vbModal
            TiMues = ""
            If .seguir = True Then
              TiMues = .txtTiMues.Text
              Call ActualizarOrgano(Right(clave, Len(clave) - 1), TiMues)
              tvwPr.Nodes.item(clave).Text = tvwMaestro.SelectedItem.Text & " " & TiMues
              Call objPipe.PipeSet("Cambios", True)
              Call ActualizartvwMuestras(nRef, Right(clave, Len(clave) - 1))
            End If
          End With
          Unload frmModMuestras
        End If
      Case 5  ' Se ha seleccionado un t�cnico de biopsias
        If Left$(clave, 1) = "Z" Then
          Call ActualizarRecep
          tvwPr.Nodes.item(clave).Text = "Recepciona: " & tvwMaestro.SelectedItem.Text
        End If
    End Select
  End If
  Set tvwPr.DropHighlight = Nothing
End Sub
Sub ActualizartvwMuestras(nRef As String, cMues As Integer)
Dim texto As String

  With frmP_Peticion
    texto = .tvwMuestra.Nodes.item("M" & cMues).Text
    texto = tvwPr.DropHighlight.Text & ". " & Right(texto, Len(texto) - InStr(texto, "(") + 1)
    .tvwMuestra.Nodes.item("M" & cMues).Text = texto
  End With
End Sub

Sub ActualizarPatologo()
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset

  ' Actualizar AP2100
  sql = "UPDATE AP2100 SET SG02Cod_Pat = ? WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cod
  rdoQ(1) = nRef
  rdoQ.Execute
  
  ' Actualizar el pat�logo en cuanto a recurso consumido
End Sub

Sub ActualizarRes()
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP2100 SET SG02Cod_Res = ? WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cod
  rdoQ(1) = nRef
  rdoQ.Execute
End Sub

Sub ActualizarCito()
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP2100 SET SG02Cod_Cito = ? WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cod
  rdoQ(1) = nRef
  rdoQ.Execute
End Sub

Sub ActualizarRecep()
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP2500 SET SG02Cod = ? WHERE AP21_CodRef = ? AND PR52NumIDMuestra = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cod
  rdoQ(1) = nRef
  rdoQ(2) = Right(tvwPr.DropHighlight.Parent.key, Len(tvwPr.DropHighlight.Parent.key) - 1)
  rdoQ.Execute
End Sub

Sub ActualizarOrgano(cMues As Integer, TiMues As String)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE PR5200 SET PR24CodMuestra = ?, PR52DesMuestra = ? WHERE PR52NumMuestra = (" _
      & "SELECT PR52NumMuestra FROM AP2500 WHERE AP21_CodRef = ? AND PR52NumIDMuestra = ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cod
  rdoQ(1) = TiMues
  rdoQ(2) = nRef
  rdoQ(3) = cMues
  rdoQ.Execute
End Sub

Private Sub tvwPr_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
Dim valido As Boolean
  
  Set tvwPr.DropHighlight = tvwPr.HitTest(X, Y)
  Select Case State
    Case 1
      tvwMaestro.DragIcon = frmPrincipal.imgIcos.ListImages(apICONPROHIBIDO).Picture
    Case 0
      tvwMaestro.DragIcon = frmPrincipal.imgIcos.ListImages(Source.SelectedItem.Image).Picture
  End Select
    
On Error Resume Next

  If Err = 0 Then
    valido = fArrastreValido(Source)
    If valido = True Then
      Source.DragIcon = frmPrincipal.imgIcos.ListImages(Source.SelectedItem.Image).Picture
    Else
      Source.DragIcon = frmPrincipal.imgIcos.ListImages(apICONPROHIBIDO).Picture
    End If
  Else
    valido = False
    Source.DragIcon = frmPrincipal.imgIcos.ListImages(apICONPROHIBIDO).Picture
  End If
End Sub

Private Function fArrastreValido(Source As Control) As Boolean
Dim nodo As Node
Dim origen As String

  Set nodo = tvwPr.DropHighlight
  If Source.Name = "tvwMaestro" Then
    Select Case Left$(nodo.key, 1)
      Case "P"
        If Left$(tvwMaestro.SelectedItem.key, 1) = "P" Then fArrastreValido = True Else fArrastreValido = False
      Case "R"
        If Left$(tvwMaestro.SelectedItem.key, 1) = "P" Or Left$(tvwMaestro.SelectedItem.key, 1) = "R" Then fArrastreValido = True Else fArrastreValido = False
      Case "C"
        If Left$(tvwMaestro.SelectedItem.key, 1) = "C" Then fArrastreValido = True Else fArrastreValido = False
      Case "M"
        If Left$(tvwMaestro.SelectedItem.key, 1) = "O" Then fArrastreValido = True Else fArrastreValido = False
      Case "Z"
        If Left$(tvwMaestro.SelectedItem.key, 1) = "P" Or _
        Left$(tvwMaestro.SelectedItem.key, 1) = "R" Or _
        Left$(tvwMaestro.SelectedItem.key, 1) = "C" Or _
        Left$(tvwMaestro.SelectedItem.key, 1) = "B" Then fArrastreValido = True Else fArrastreValido = False
    End Select
  End If
End Function

Private Sub tvwPr_KeyDown(KeyCode As Integer, Shift As Integer)
Dim texto As String, sql As String
Dim nodo As Node
Dim cDr As Integer, res As Integer
Dim rdoQ As rdoQuery
  
  If KeyCode = 46 Then    ' Tecla Suprimir
    Set nodo = tvwPr.SelectedItem
    If nodo.Parent.key = "Gente" Then
      Select Case Left(nodo.key, 1)
        Case "P"
          res = MsgBox("No se puede borrar el pat�logo asignado a la prueba.", vbOKOnly, "Gesti�n de pruebas")
        Case "R"
          res = MsgBox("�Desea borrar responsable de la descripci�n macrosc�pica?", vbQuestion + vbYesNo, "Gesti�n de pruebas")
          If res = vbYes Then
            cDr = Val(Right(nodo.key, Len(nodo.key) - 1))
            sql = "UPDATE AP2100 SET AP08_CodPers_Res = NULL WHERE AP21_CodRef = ?"
            Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
            rdoQ(0).Type = rdTypeVARCHAR
            rdoQ(0) = nRef
            rdoQ.Execute sql
            If rdoQ.RowsAffected = 1 Then
              nodo.Text = "Descripci�n macrosc�pica: "
            End If
          End If
        Case "C"
          res = MsgBox("�Desea borrar al citot�cnico asociado a esta prueba?", vbQuestion + vbYesNo, "Gesti�n de pruebas")
          If res = vbYes Then
            sql = "UPDATE AP2100 SET AP08_CodPers_Cito = NULL WHERE AP21_CodRef = ?"
            Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
            rdoQ(0).Type = rdTypeVARCHAR
            rdoQ(0) = nRef
            rdoQ.Execute sql
            If rdoQ.RowsAffected = 1 Then
              nodo.Text = "Citot�cnico: "
            End If
          End If
      End Select
    End If
  End If

End Sub

Private Sub tvwPr_LostFocus()
  Set tvwPr.DropHighlight = tvwPr.SelectedItem
End Sub

Private Sub tvwPr_NodeClick(ByVal Node As ComctlLib.Node)
Dim nodo As Node
    
  Set nodo = tvwPr.SelectedItem
  Set tvwPr.DropHighlight = tvwPr.SelectedItem
  If Left(nodo.key, 1) = "M" Then ' Ha seleccionado una muestra
    cmdIntra.Enabled = True
    cmdEtiquetas.Enabled = True
    If nodo.Tag = -1 Then
      cmdIntra.Caption = "Quitar como Intra"
    Else
      cmdIntra.Caption = "Marcar como Intra"
    End If
  Else
    cmdIntra.Enabled = False
    cmdEtiquetas.Enabled = False
  End If
End Sub
