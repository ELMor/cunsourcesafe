VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmI_Diagnostico 
   Caption         =   "Diagn�sticos"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11820
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   11820
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.ListView lvwPr 
      Height          =   4305
      Left            =   105
      TabIndex        =   7
      Top             =   3930
      Width           =   11730
      _ExtentX        =   20690
      _ExtentY        =   7594
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Frame Frame1 
      Height          =   3495
      Left            =   75
      TabIndex        =   1
      Top             =   150
      Width           =   11655
      Begin TabDlg.SSTab SSTab1 
         Height          =   1350
         Left            =   150
         TabIndex        =   19
         Top             =   225
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   2381
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Diagn�sticos"
         TabPicture(0)   =   "ap00767.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "chkSno(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "chkSno(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "chkSno(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "chkSno(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chkSno(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkSno(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Pruebas"
         TabPicture(1)   =   "ap00767.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "optTiPr(3)"
         Tab(1).Control(1)=   "optTiPr(2)"
         Tab(1).Control(2)=   "optTiPr(1)"
         Tab(1).Control(3)=   "optTiPr(0)"
         Tab(1).ControlCount=   4
         Begin VB.OptionButton optTiPr 
            Caption         =   "Biopsias"
            Height          =   315
            Index           =   0
            Left            =   -74775
            TabIndex        =   29
            Top             =   450
            Width           =   1290
         End
         Begin VB.OptionButton optTiPr 
            Caption         =   "Citolog�as"
            Height          =   315
            Index           =   1
            Left            =   -74775
            TabIndex        =   28
            Top             =   825
            Width           =   1290
         End
         Begin VB.OptionButton optTiPr 
            Caption         =   "Necropsias"
            Height          =   315
            Index           =   2
            Left            =   -73050
            TabIndex        =   27
            Top             =   450
            Width           =   1290
         End
         Begin VB.OptionButton optTiPr 
            Caption         =   "Todas"
            Height          =   315
            Index           =   3
            Left            =   -73050
            TabIndex        =   26
            Top             =   825
            Value           =   -1  'True
            Width           =   1290
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   5
            Left            =   1800
            TabIndex        =   25
            Top             =   975
            Value           =   1  'Checked
            Width           =   1515
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   4
            Left            =   1800
            TabIndex        =   24
            Top             =   675
            Value           =   1  'Checked
            Width           =   1515
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   3
            Left            =   1800
            TabIndex        =   23
            Top             =   375
            Value           =   1  'Checked
            Width           =   1515
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   2
            Left            =   150
            TabIndex        =   22
            Top             =   975
            Value           =   1  'Checked
            Width           =   1590
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   1
            Left            =   150
            TabIndex        =   21
            Top             =   675
            Value           =   1  'Checked
            Width           =   1590
         End
         Begin VB.CheckBox chkSno 
            Caption         =   "Check"
            Height          =   315
            Index           =   0
            Left            =   150
            TabIndex        =   20
            Top             =   375
            Value           =   1  'Checked
            Width           =   1590
         End
      End
      Begin VB.CommandButton cmdParent1 
         Caption         =   "&("
         Height          =   390
         Left            =   7500
         TabIndex        =   17
         Top             =   2925
         Width           =   690
      End
      Begin VB.CommandButton cmdParent2 
         Caption         =   "&)"
         Height          =   390
         Left            =   10800
         TabIndex        =   16
         Top             =   2925
         Width           =   690
      End
      Begin VB.CommandButton cmdAnd 
         Caption         =   "&Y"
         Height          =   390
         Left            =   8625
         TabIndex        =   15
         Top             =   2925
         Width           =   690
      End
      Begin VB.CommandButton cmdOr 
         Caption         =   "&O"
         Height          =   390
         Left            =   9675
         TabIndex        =   14
         Top             =   2925
         Width           =   690
      End
      Begin VB.PictureBox picSep 
         BorderStyle     =   0  'None
         Height          =   465
         Left            =   7425
         MousePointer    =   9  'Size W E
         ScaleHeight     =   465
         ScaleWidth      =   120
         TabIndex        =   13
         Top             =   1275
         Width           =   120
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "&Imprimir"
         Height          =   360
         Left            =   1275
         TabIndex        =   12
         Top             =   2925
         Width           =   1050
      End
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Salir"
         Height          =   360
         Left            =   2400
         TabIndex        =   10
         Top             =   2925
         Width           =   1050
      End
      Begin VB.CommandButton cmdVer 
         Caption         =   "&Ver"
         Height          =   360
         Left            =   150
         TabIndex        =   8
         Top             =   2925
         Width           =   1050
      End
      Begin ComctlLib.ListView lvwDiag 
         Height          =   3090
         Left            =   3600
         TabIndex        =   6
         Top             =   225
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   5450
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Frame Frame3 
         Caption         =   "Busqueda"
         ForeColor       =   &H00800000&
         Height          =   1245
         Left            =   150
         TabIndex        =   2
         Top             =   1650
         Width           =   3405
         Begin VB.CommandButton cmdLimpiar 
            Caption         =   "&Limpiar"
            Height          =   360
            Left            =   2400
            TabIndex        =   11
            Top             =   300
            Width           =   900
         End
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "&Buscar"
            Height          =   360
            Left            =   1425
            TabIndex        =   9
            Top             =   300
            Width           =   915
         End
         Begin VB.TextBox txtDescrip 
            Height          =   315
            Left            =   675
            TabIndex        =   0
            Top             =   750
            Width           =   2655
         End
         Begin VB.TextBox txtCodigo 
            Height          =   315
            Left            =   675
            TabIndex        =   3
            Top             =   300
            Width           =   675
         End
         Begin VB.Label Label2 
            Caption         =   "Descrip:"
            Height          =   255
            Left            =   60
            TabIndex        =   5
            Top             =   810
            Width           =   690
         End
         Begin VB.Label Label1 
            Caption         =   "Codigo:"
            Height          =   255
            Left            =   120
            TabIndex        =   4
            Top             =   360
            Width           =   555
         End
      End
      Begin ComctlLib.TreeView tvwDiagSelec 
         Height          =   2640
         Left            =   7500
         TabIndex        =   18
         Top             =   225
         Width           =   3990
         _ExtentX        =   7038
         _ExtentY        =   4657
         _Version        =   327682
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         Appearance      =   1
      End
   End
   Begin VB.Label lblnPr 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   150
      TabIndex        =   30
      Top             =   3675
      Width           =   75
   End
   Begin VB.Menu mnuDiag 
      Caption         =   "Diag"
      Visible         =   0   'False
      Begin VB.Menu mnuDiagOpciones 
         Caption         =   "Buscar cifras significativas"
         Index           =   10
      End
   End
End
Attribute VB_Name = "frmI_Diagnostico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TiSno() As String
Dim sqlDiag As String, sqlCR As String
Dim nodoUlt As NodosDiag, nodoSelec As Node
Dim nParent1 As Integer, nParent2 As Integer, nop1 As Integer, nop2 As Integer, ndiags As Integer
Dim anchoIni As Long, LeftIni As Long, mover As Integer
Dim cSno() As String
Dim Tipo As Integer, opAnt As Integer, cParent As String
Dim titulo As String

Private Sub cmdBuscar_Click()

  Call Buscar
End Sub
Sub Buscar()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim item As ListItem
Dim nodo As Node
Dim cSno As String
  
  If ComprobarSel(Me, TiSno()) = False Then
    MsgBox "Debe seleccionar alg�n tipo de diagn�stico.", vbInformation, "B�squeda de diagn�sticos"
  Else
    Screen.MousePointer = 11
    lvwDiag.ListItems.Clear
    cSno = chkSelec(Me, TiSno())
    sql = "SELECT AP37_CodSno, AP50_CodTiSno, AP37_NumSno, AP51_CodClaseSno, AP37_Descrip, " _
        & "AP37_CodICD FROM AP3700 WHERE AP50_CodTiSno IN " & cSno
    If txtCodigo.Text <> "" Then sql = sql & " AND UPPER(AP37_NumSno) Like ?"
    If txtDescrip.Text <> "" Then sql = sql & " AND UPPER(AP37_Descrip) Like ?"
    sql = sql & " ORDER BY AP50_CodTiSno, AP37_NumSno"
        
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    If txtCodigo.Text <> "" Then
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(0) = UCase(txtCodigo.Text) & "%"
      If txtDescrip.Text <> "" Then
        rdoQ(1).Type = rdTypeVARCHAR
        rdoQ(1) = "%" & UCase(txtDescrip.Text) & "%"
      End If
    ElseIf txtDescrip.Text <> "" Then
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(0) = "%" & UCase(txtDescrip.Text) & "%"
    End If
    
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    While rdo.EOF = False
      Set item = lvwDiag.ListItems.Add(, "D" & rdo(0), rdo(1) & "-" & rdo(2) & ": " & rdo(4))
      item.Tag = rdo(0)
      item.SmallIcon = apICONDIAG
      rdo.MoveNext
    Wend
    Screen.MousePointer = 0
    If lvwDiag.ListItems.Count = 0 Then
      MsgBox "No se ha encontrado ning�n diagn�stico.", vbInformation, "B�squeda de diagn�sticos"
    End If
  End If

End Sub
Private Sub cmdLimpiar_Click()
Dim item As ListItem
Dim nodo As Node
   
  tvwDiagSelec.Nodes.Clear
  Set nodo = tvwDiagSelec.Nodes.Add(, , "Raiz", "Pruebas que contengan ...", apICONRAIZ)
  nParent1 = 1
  nParent2 = 1
  nop1 = 1
  nop2 = 1
  ndiags = 1
  titulo = ""
  sqlDiag = "SELECT AP2100.AP21_CodRef, AP2100.AP21_CODHIST, " _
      & "to_char(AP2100.AP21_FecSotud,'DD/MM/YYYY'), " _
      & "PAC.AP1||' '||PAC.AP2||', '||PAC.NOM, AP2100.AP21_TiPr, AP2100.AP21_FecGen " _
      & "FROM AP2100, PAC WHERE " _
      & "AP21_CODHIST = PAC.NH AND AP2100.AP21_CodRef IN ("
  
  sqlCR = "AP21_CodRef IN ("
  For Each item In lvwDiag.ListItems
    item.Ghosted = False
  Next item
End Sub

Private Sub cmdSalir_Click()
  Unload Me

End Sub
Private Sub cmdVer_Click()
Dim sql As String, TiPr As String, sql1 As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim i As Integer
Dim item As ListItem
Dim nodo As Node
    
  If ndiags = 1 Then
    MsgBox "Debe seleccionar alg�n diagn�stico.", vbInformation + vbOKOnly, "Selecci�n de diagn�sticos"
    Exit Sub
  End If
  Screen.MousePointer = 11
  lvwPr.ListItems.Clear
  
  Set nodo = tvwDiagSelec.Nodes("Raiz")
'  If nodo.Children > 0 Then sql1 = calcHijo(nodo.Child)
  
  If optTiPr(0).Value = True Then
    sql1 = sqlDiag & ") AND AP2100.AP21_TiPr = 1 ORDER BY AP2100.AP21_CodRef"
  ElseIf optTiPr(1).Value = True Then
    sql1 = sqlDiag & ") AND AP2100.AP21_TiPr = 2 ORDER BY AP2100.AP21_CodRef"
  ElseIf optTiPr(2).Value = True Then
    sql1 = sqlDiag & ") AND AP2100.AP21_TiPr = 3 ORDER BY AP2100.AP21_CodRef"
  ElseIf optTiPr(3).Value = True Then
    sql1 = sqlDiag & ") ORDER BY AP2100.AP21_CodRef"
  End If
  
  On Error GoTo sql_error
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql1)
  For i = 0 To ndiags - 2
    rdoQ(i) = cSno(i + 1)
  Next i
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  i = 0
  If rdo.EOF = True Then MsgBox "No se ha encontrado ninguna prueba con esos diagn�sticos", vbInformation + vbOKOnly, "B�squeda de pruebas"
  While Not rdo.EOF
    i = i + 1
    Set item = lvwPr.ListItems.Add(, , Right(rdo(0), 8))
    item.SubItems(1) = CStr(rdo(1))
    item.SubItems(2) = CStr(rdo(3))
    item.SubItems(3) = CStr(rdo(2))
    item.SubItems(4) = CStr(rdo(5))
    Select Case rdo(4)
      Case 1
        TiPr = "Biopsia"
      Case 2
        TiPr = "Citologia"
      Case 3
        TiPr = "Auptopsia"
    End Select
    item.SubItems(5) = TiPr
    item.Tag = rdo(0)
    rdo.MoveNext
  Wend
  lblnPr.Caption = "N� de pruebas encontradas: " & i
  Screen.MousePointer = 0
  Exit Sub
  
sql_error:
  MsgBox "No se han introducido correctamente los diagn�sticos a visualizar", vbInformation + vbOKOnly, "B�squeda de pruebas"
  Screen.MousePointer = 0
  Exit Sub
End Sub

Private Sub cmdVer1_Click()
'Dim sql As String, TiPr As String
'Dim rdo As rdoResultset
'Dim rdoQ As rdoQuery
'Dim i As Integer, NumDiags As Integer
'Dim item As ListItem
'
'  NumDiags = lvwDiagSelec.ListItems.Count
'  If NumDiags = 0 Then
'    MsgBox "Debe seleccionar alg�n diagn�stico.", vbInformation + vbOKOnly, "Selecci�n de diagn�sticos"
'    Exit Sub
'  End If
'  Screen.MousePointer = 11
'  lvwPr.ListItems.Clear
'
'  sql = "SELECT AP2100.AP21_CodRef, AP2100.AP21_CODHIST, " _
'      & "to_char(AP2100.AP21_FecSotud,'DD/MM/YYYY'), " _
'      & "PAC.AP1||' '||PAC.AP2||','||PAC.NOM, AP2100.AP21_TiPr " _
'      & "FROM AP2100, PAC WHERE " _
'      & "AP21_CODHIST = PAC.NH AND AP2100.AP21_CodRef IN ("
'  For i = 1 To NumDiags - 1
'    sql = sql & "SELECT /*+ RULE */ AP3900.AP21_CodRef FROM AP3900 WHERE " _
'        & "AP3900.AP37_CodSno = ? INTERSECT "
'  Next i
'  sql = sql & "SELECT /*+ RULE */ AP3900.AP21_CodRef FROM AP3900 WHERE " _
'      & "AP3900.AP37_CodSno = ?) ORDER BY AP2100.AP21_CodRef ASC"
'
'  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
'  i = 0
'  For Each item In lvwDiagSelec.ListItems
'    rdoQ(i) = item.Tag
'    i = i + 1
'  Next item
'  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
'
'  If rdo.EOF = True Then MsgBox "No se ha encontrado ninguna prueba con esos diagn�sticos", vbInformation + vbOKOnly, "B�squeda de pruebas"
'  While Not rdo.EOF
'    Set item = lvwPr.ListItems.Add(, , Right(rdo(0), 8))
'    item.SubItems(1) = CStr(rdo(1))
'    item.SubItems(2) = CStr(rdo(3))
'    item.SubItems(3) = CStr(rdo(2))
'    Select Case rdo(4)
'      Case 1
'        TiPr = "Biopsia"
'      Case 2
'        TiPr = "Citologia"
'      Case 3
'        TiPr = "Auptopsia"
'    End Select
'    item.SubItems(4) = TiPr
'    rdo.MoveNext
'  Wend
'  Screen.MousePointer = 0
End Sub




Private Sub Form_Load()
Dim sql As String
Dim rdo As rdoResultset
Dim i As Integer
Dim nodo As Node
    
  Call LlenarOptSno(Me, TiSno())
  lvwDiag.ColumnHeaders.Add , , "Diagn�sticos encontrados", 3000
  
  lvwPr.ColumnHeaders.Add , , "n� Ref", lvwPr.Width / (1.13 * 13)
  lvwPr.ColumnHeaders.Add , , "Historia", lvwPr.Width / 15
  lvwPr.ColumnHeaders.Add , , "Nombre del Paciente", 1.15 * (lvwPr.Width / 3)
  lvwPr.ColumnHeaders.Add , , "F.Solicitud", lvwPr.Width / 10
  lvwPr.ColumnHeaders.Add , , "F.Recepci�n", lvwPr.Width / 10
  lvwPr.ColumnHeaders.Add , , "Prueba", lvwPr.Width / 15
  
'  lvwDiag.View = lvwReport
  lvwDiag.SmallIcons = frmPrincipal.imgIcos
  lvwPr.View = lvwReport
  
  Call Dibujar

  tvwDiagSelec.ImageList = frmPrincipal.imgIcos
  Set nodo = tvwDiagSelec.Nodes.Add(, , "Raiz", "Pruebas que contengan: ", apICONRAIZ)

  nParent1 = 1
  nParent2 = 1
  nop1 = 1
  nop2 = 1
  ndiags = 1
  
  sqlDiag = "SELECT /*+ RULE */ AP2100.AP21_CodRef, AP2100.AP21_CODHIST, " _
      & "TO_CHAR(AP2100.AP21_FecSotud,'DD/MM/YYYY'), " _
      & "PAC.AP1||' '||PAC.AP2||', '||PAC.NOM, AP2100.AP21_TiPr, AP2100.AP21_FecGen " _
      & "FROM AP2100, PAC WHERE " _
      & "AP21_CODHIST = PAC.NH AND AP2100.AP21_CodRef IN ("
  opAnt = 0

  sqlCR = "AP21_CodRef IN ("
End Sub

Private Sub lvwDiag_DblClick()
Dim item As ListItem, nodo As Node, nodo1 As Node
  
'  On Error Resume Next
  If lvwDiag.SelectedItem Is Nothing Then Exit Sub
  Set item = lvwDiag.SelectedItem
  If tvwDiagSelec.Nodes.Count = 1 Then
    titulo = "'"
    Set nodo = tvwDiagSelec.Nodes.Add("Raiz", tvwChild, "D" & item.Tag, item.Text, item.SmallIcon, item.SmallIcon)
  Else
    Select Case nodoUlt.Tipo
      Case apTIPODIAG
        MsgBox "Debe introducir un operador.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
        Exit Sub
      Case apTIPOOPAND, apTIPOOPOR
        On Error GoTo ClaveDuplicada
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Key, tvwNext, "D" & item.Tag, item.Text, item.SmallIcon, item.SmallIcon)
      Case apTIPOPARENT1
        On Error GoTo ClaveDuplicada
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Key, tvwChild, "D" & item.Tag, item.Text, item.SmallIcon, item.SmallIcon)
        titulo = titulo & " "
      Case apTIPOPARENT2
        MsgBox "Debe introducir un operador.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
        Exit Sub
    End Select
  End If
  Set nodoUlt.nodo = nodo
  nodoUlt.Tipo = apTIPODIAG
  nodo.EnsureVisible
  ReDim Preserve cSno(1 To ndiags)
  item.Ghosted = True
  If item.SmallIcon = apICONDIAG Then
    cSno(ndiags) = item.Tag
    sqlDiag = sqlDiag & "SELECT /*+ RULE */ AP21_CodRef FROM AP3900 WHERE AP37_CodSno = ? "
    sqlCR = sqlCR & "SELECT /*+ RULE */ AP21_CodRef FROM AP3900 WHERE AP37_CodSno = " & item.Tag & " "
    titulo = titulo & item.Text
  Else
    cSno(ndiags) = Left(item.Text, 1)
    sqlDiag = sqlDiag & "SELECT /*+ RULE */ AP21_CodRef FROM AP3900 WHERE AP37_CodSno IN (" _
      & "SELECT AP37_CodSno FROM AP3700 WHERE AP50_CodTiSno = ? AND " _
      & "AP37_NumSno LIKE " & CifrasSignificativas(item.Text) & ") "
    sqlCR = sqlCR & "SELECT /*+ RULE */ AP21_CodRef FROM AP3900 WHERE AP37_CodSno IN (" _
      & "SELECT AP37_CodSno FROM AP3700 WHERE AP50_CodTiSno = '" & Left(item.Text, 1) & "' AND " _
      & "AP37_NumSno LIKE " & CifrasSignificativas(item.Text) & ") "
    titulo = titulo & item.Text & " (cifras significativas)"
  End If
  ndiags = ndiags + 1
  Exit Sub

ClaveDuplicada:
  MsgBox "Ya ha introducido ese diagn�stico.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
End Sub

Private Sub lvwDiag_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim texto As String

  If Button = vbRightButton Then
    On Error Resume Next
    texto = ""
    texto = lvwDiag.SelectedItem.Text
    If texto <> "" Then
      If lvwDiag.SelectedItem.SmallIcon = apICONDIAG Then
        mnuDiagOpciones(10).Checked = False
      Else
        mnuDiagOpciones(10).Checked = True
      End If
      Me.PopupMenu mnuDiag, vbPopupMenuLeftAlign, lvwDiag.Left + X, lvwDiag.Top + Y
    End If
  End If
End Sub

Private Sub lvwPr_DblClick()
Dim nRef As String
Dim sql As String
Dim rdoQ As rdoQuery

  Call objPipe.PipeSet("Boton", "Pr")
  frmPrAnteriores.Show vbModal
' Para entrar a la ventana de petici�n
'  nRef = lvwPr.SelectedItem.Tag
'  Call objPipe.PipeSet("NumRef", nRef)
'  sql = "SELECT NH, Ap1||' '||Ap2||', '||Nom as Nombre, Pr.Desig, " _
'      & "AP2100.AP08_CODPERS_PAT, AP2100.AP45_CodEstPr, AP2100.AP21_CodCaso, " _
'      & "AP2100.AP21_CodSec, AP2100.AP21_FecSotud, AP2100.AP21_TiPr, " _
'      & "AP4500.AP45_Desig, AP2100.cPr " _
'      & "FROM AP2100, Pac, Pr, AP4500 WHERE " _
'      & "Pac.nh = AP2100.AP21_CodHist AND " _
'      & "Pr.Cdpt = " & cDptAP & " AND " _
'      & "Pr.CPr = AP2100.CPr AND " _
'      & "AP2100.AP45_CodEstPr = AP4500.AP45_CodEstPr AND " _
'      & "AP2100.AP21_CodRef = ?"
'  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
'  rdoQ(0) = nRef
'  Set rdoPet = rdoQ.OpenResultset(rdOpenForwardOnly)
'  frmP_Peticion.Show vbModal
End Sub

Private Sub mnuDiagOpciones_Click(Index As Integer)
  If Index = 10 Then
    If mnuDiagOpciones(10).Checked = False Then
      lvwDiag.SelectedItem.SmallIcon = apICONDIAGS
    Else
      lvwDiag.SelectedItem.SmallIcon = apICONDIAG
    End If
  End If
End Sub

Private Sub tvwDiagSelec_KeyDown(KeyCode As Integer, Shift As Integer)
  
'  If KeyCode = 46 Then ' Tecla suprimir
'    If nodoSelec.key = nodoUlt.nodo.key Then
'      Set nodoUlt.nodo = nodoSelec.Previous
'      Select Case nodoUlt.nodo.Text
'        Case "Y"
'          nodoUlt.Tipo = apTIPOOPAND
'        Case "O"
'          nodoUlt.Tipo = apTIPOOPOR
'        Case "("
'          nodoUlt.Tipo = apTIPOPARENT1
'        Case ")"
'          nodoUlt.Tipo = apTIPOPARENT2
'        Case Else
'          nodoUlt.Tipo = apTIPODIAG
'      End Select
'      Select Case nodoSelec.Text
'        Case "Y"
'          titulo = Left(titulo, Len(titulo) - 17)
'          sqlDiag = Left(sqlDiag, Len(sqlDiag) - 10)
'          sqlCR = Left(sqlCR, Len(sqlCR) - 10)
'        Case "O"
'          titulo = Left(titulo, Len(titulo) - 17)
'          sqlDiag = Left(sqlDiag, Len(sqlDiag) - 6)
'          sqlCR = Left(sqlCR, Len(sqlCR) - 6)
'        Case "("
'          titulo = Left(titulo, Len(titulo) - 2)
'          sqlDiag = Left(sqlDiag, Len(sqlDiag) - 2)
'          sqlCR = Left(sqlCR, Len(sqlCR) - 2)
'        Case ")"
'          titulo = Left(titulo, Len(titulo) - 2)
'          sqlDiag = Left(sqlDiag, Len(sqlDiag) - 2)
'          sqlCR = Left(sqlCR, Len(sqlCR) - 2)
'        Case Else
'          titulo = Left(titulo, InStr(titulo, nodoUlt.nodo.Text) - 16)
'          sqlDiag = Left(sqlDiag, Len(sqlDiag) - 65)
'          ndiags = ndiags - 1
'          ReDim Preserve cSno(1 To ndiags - 1)
'      End Select
'      tvwDiagSelec.Nodes.Remove nodoSelec.key
'    Else
'      MsgBox "S�lo se puede borrar el �ltimo elemento", vbInformation, "Selecci�n de diagn�sticos"
'    End If
'  End If

End Sub

Private Sub tvwDiagSelec_NodeClick(ByVal Node As ComctlLib.Node)
  Set nodoSelec = Node
End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then cmdBuscar_Click
End Sub

Private Sub txtDescrip_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then cmdBuscar_Click
End Sub
Private Sub cmdImprimir_Click()
Dim sql1 As String

  If ndiags = 1 Then
    MsgBox "Debe seleccionar alg�n diagn�stico.", vbInformation + vbOKOnly, "Selecci�n de diagn�sticos"
    Exit Sub
  End If
  Screen.MousePointer = 11
  
  sql1 = sqlCR & ")"
  If optTiPr(0).Value = True Then
    sql1 = sql1 & " AND AP21_TiPr = 1"
  ElseIf optTiPr(1).Value = True Then
    sql1 = sql1 & " AND AP21_TiPr = 2"
  ElseIf optTiPr(2).Value = True Then
    sql1 = sql1 & " AND AP21_TiPr = 3"
  End If

  Call Imprimir_API(sql1, "ap006701.rpt", , titulo & "'", Me.Caption)
  Screen.MousePointer = 0
End Sub

Sub Dibujar()
  
  anchoIni = 4000
  LeftIni = 7350
  tvwDiagSelec.Width = anchoIni
  tvwDiagSelec.Left = LeftIni
  picSep.Width = 25
  picSep.Left = LeftIni - picSep.Width
  picSep.Top = tvwDiagSelec.Top
  picSep.Height = tvwDiagSelec.Height
  lvwDiag.Width = picSep.Left - lvwDiag.Left

  Call DibujarBotones

End Sub
Sub DibujarBotones()
Dim ancho As Integer

' Para los botones
  ancho = tvwDiagSelec.Width / 11
  cmdParent1.Left = tvwDiagSelec.Left
  cmdParent1.Width = ancho * 2
  cmdAnd.Left = tvwDiagSelec.Left + ancho * 3
  cmdAnd.Width = ancho * 2
  cmdOr.Left = tvwDiagSelec.Left + ancho * 6
  cmdOr.Width = ancho * 2
  cmdParent2.Left = tvwDiagSelec.Left + ancho * 9
  cmdParent2.Width = ancho * 2

End Sub
Function calcHijo(nodo As Node) As String
Dim sql As String
Dim i As Integer
  
  If nodo.Children > 0 Then calcHijo = "(" & calcHijo(nodo.Child)
  Tipo = 1
  For i = 1 To nodo.Parent.Children - 1 Step 2
    If Left(nodo.Key, 1) = "D" Then
      If nodo.Next.Text = "AND" Then
        sql = sql & " = ? INTERSECT SELECT /*+ RULE */ AP3900.AP21_CodRef FROM AP3900 WHERE AP3900.AP37_CodSno "
        Tipo = 1
      ElseIf nodo.Next.Text = "OR" Then
        sql = sql & " IN (?, "
        Tipo = 2
      End If
      Set nodo = nodo.Next
      Set nodo = nodo.Next
      If nodo.Children > 0 Then
        sql = sql & calcHijo(nodo.Child)
        sql = Left(sql, Len(sql) - 3)
      End If
    ElseIf Left(nodo.Key, 1) = "I" Then
      sql = sql & "("
    End If
  Next i
  If Tipo = 1 Then sql = sql & "= ?" Else sql = sql & "? )"
  calcHijo = sql
End Function

Function calcHijo_impr(nodo As Node) As String
Dim sql As String
Dim i As Integer
  
  If nodo.Children > 0 Then calcHijo_impr = "(" & calcHijo(nodo.Child)
  Tipo = 1
  For i = 1 To nodo.Parent.Children - 1 Step 2
    If Left(nodo.Key, 1) = "D" Then
      If nodo.Next.Text = "AND" Then
        sql = sql & " = " & Right(nodo.Key, Len(nodo.Key) - 1) & " INTERSECT SELECT /*+ RULE */ AP3900.AP21_CodRef FROM AP3900 WHERE AP3900.AP37_CodSno "
        Tipo = 1
      ElseIf nodo.Next.Text = "OR" Then
        sql = sql & " IN (" & Right(nodo.Key, Len(nodo.Key) - 1) & ", "
        Tipo = 2
      End If
      Set nodo = nodo.Next
      Set nodo = nodo.Next
      If nodo.Children > 0 Then
        sql = sql & calcHijo(nodo.Child)
        sql = Left(sql, Len(sql) - 3)
      End If
    ElseIf Left(nodo.Key, 1) = "I" Then
      sql = sql & "("
    End If
  Next i
  If Tipo = 1 Then sql = sql & "= " & Right(nodo.Key, Len(nodo.Key) - 1) Else sql = sql & Right(nodo.Key, Len(nodo.Key) - 1) & ")"
  calcHijo_impr = sql
End Function


Private Sub picSep_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  mover = True
End Sub

Private Sub picSep_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim izda As Long, ancho As Long
  If mover = True Then
    If picSep.Left - lvwDiag.Left + X > 100 And tvwDiagSelec.Width - X > 100 Then
      picSep.Left = picSep.Left + X
      lvwDiag.Width = picSep.Left - lvwDiag.Left
      izda = picSep.Left + picSep.Width
      tvwDiagSelec.Left = izda
      ancho = tvwDiagSelec.Width - X
      tvwDiagSelec.Width = ancho
      Call DibujarBotones
    End If
  End If
End Sub

Private Sub picSep_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  mover = False
End Sub

Private Sub tvwDiagSelec_Collapse(ByVal Node As ComctlLib.Node)
Dim nodo As Node, i As Integer

  If Node.Key = "Raiz" Then Node.Text = ""
  Set nodo = Node.Child
  For i = 1 To Node.Children
    Node.Text = Node.Text & " " & nodo.Text
    Set nodo = nodo.Next
  Next i

  If Node.Key <> "Raiz" Then
    Node.Text = Node.Text & ")"
    cParent = Node.Next.Key
    tvwDiagSelec.Nodes.Remove (Node.Next.Key)
  End If
End Sub

Private Sub tvwDiagSelec_Expand(ByVal Node As ComctlLib.Node)

  If Left(Node.Key, 1) = "L" And cParent <> "" Then
    Node.Text = "("
    tvwDiagSelec.Nodes.Add Node.Key, tvwNext, cParent, ")"
    cParent = ""
  End If
  If Node.Key = "Raiz" Then Node.Text = "Pruebas que contengan: "
  
End Sub

Private Sub cmdAnd_Click()
Dim nodo As Node
  
  If tvwDiagSelec.Nodes.Count = 1 Then
    MsgBox "Debe introducir un diagn�stico antes de los operadores.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
  Else
    Select Case nodoUlt.Tipo
      Case apTIPODIAG
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Key, tvwNext, "A" & nop1, "Y")
        Set nodoUlt.nodo = nodo
        nodoUlt.Tipo = apTIPOOPAND
        nodo.EnsureVisible
        nop1 = nop1 + 1
        sqlDiag = sqlDiag & "INTERSECT "
        sqlCR = sqlCR & "INTERSECT "
        titulo = titulo & " Y' + Chr(13) + '"
        opAnt = 1
      Case apTIPOOPAND, apTIPOOPOR
        MsgBox "No se pueden introducir dos operadores seguidos.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
      Case apTIPOPARENT1
        MsgBox "Debe introducir un diagn�stico", vbOKOnly + vbInformation, "Introducci�n incorrecta"
      Case apTIPOPARENT2
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Key, tvwNext, "A" & nop1, "Y")
        Set nodoUlt.nodo = nodo
        nodoUlt.Tipo = apTIPOOPAND
        nodo.EnsureVisible
        nop1 = nop1 + 1
        sqlDiag = sqlDiag & "INTERSECT "
        sqlCR = sqlCR & "INTERSECT "
        titulo = titulo & " Y' + Chr(13) + '"
        opAnt = 1
    End Select
  End If

End Sub
Private Sub cmdOr_Click()
Dim nodo As Node
  
  If tvwDiagSelec.Nodes.Count = 1 Then
    MsgBox "Debe introducir un diagn�stico antes de los operadores.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
  Else
    Select Case nodoUlt.Tipo
      Case apTIPODIAG
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Key, tvwNext, "O" & nop2, "O")
        Set nodoUlt.nodo = nodo
        nodoUlt.Tipo = apTIPOOPOR
        nodo.EnsureVisible
        nop2 = nop2 + 1
        sqlDiag = sqlDiag & "UNION "
        sqlCR = sqlCR & "UNION "
        opAnt = 2
        titulo = titulo & " O' + Chr(13) + '"
      Case apTIPOOPAND, apTIPOOPOR
        MsgBox "No se pueden introducir dos operadores seguidos.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
      Case apTIPOPARENT1
        MsgBox "Debe introducir un diagn�stico", vbOKOnly + vbInformation, "Introducci�n incorrecta"
      Case apTIPOPARENT2
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Key, tvwNext, "O" & nop2, "O")
        Set nodoUlt.nodo = nodo
        nodoUlt.Tipo = apTIPOOPOR
        nodo.EnsureVisible
        nop2 = nop2 + 1
        sqlDiag = sqlDiag & "UNION "
        sqlCR = sqlCR & "UNION "
        titulo = titulo & " O' + Chr(13) + '"
        opAnt = 2
    End Select
  End If

End Sub

Private Sub cmdParent1_Click()
Dim nodo As Node

  If tvwDiagSelec.Nodes.Count = 1 Then
    Set nodo = tvwDiagSelec.Nodes.Add("Raiz", tvwChild, "L" & nParent1, "(")
    Set nodoUlt.nodo = nodo
    nodo.EnsureVisible
    nodoUlt.Tipo = apTIPOPARENT1
    nParent1 = nParent1 + 1
    sqlDiag = sqlDiag & "("
    sqlCR = sqlCR & "("
    titulo = "'" & " ("
  Else
    Select Case nodoUlt.Tipo
      Case apTIPODIAG
        MsgBox "Debe introducir un operador", vbOKOnly + vbInformation, "Introducci�n incorrecta"
      Case apTIPOOPAND, apTIPOOPOR
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Key, tvwNext, "L" & nParent1, "(")
        Set nodoUlt.nodo = nodo
        sqlDiag = sqlDiag & "("
        nodoUlt.Tipo = apTIPOPARENT1
        nParent1 = nParent1 + 1
        titulo = titulo & " ("
        sqlCR = sqlCR & "("
      Case apTIPOPARENT1
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Key, tvwChild, "L" & nParent1, "(")
        Set nodoUlt.nodo = nodo
        nodoUlt.Tipo = apTIPOPARENT1
        nParent1 = nParent1 + 1
        sqlDiag = sqlDiag & "("
        titulo = titulo & " ("
        sqlCR = sqlCR & "("
      Case apTIPOPARENT2
        MsgBox "Debe introducir un operador.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
      Case Else
        MsgBox "Debe introducir un diagn�stico.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
    End Select
  End If
End Sub

Private Sub cmdParent2_Click()
Dim nodo As Node

  If tvwDiagSelec.Nodes.Count = 1 Then
    MsgBox "Debe introducir un diagn�stico.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
  Else
    Select Case nodoUlt.Tipo
      Case apTIPODIAG
        Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Parent.Key, tvwNext, "R" & nParent2, ")")
        Set nodoUlt.nodo = nodo
        nodoUlt.Tipo = apTIPOPARENT2
        nParent2 = nParent2 + 1
        sqlDiag = sqlDiag & ")"
        sqlCR = sqlCR & ")"
        titulo = titulo & " )"
      Case apTIPOOPAND, apTIPOOPOR, apTIPOPARENT1
        MsgBox "Debe introducir un diagn�stico.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
      Case apTIPOPARENT2
        If nParent1 = nParent2 Then
          MsgBox "No puede cerrar el par�ntesis.", vbOKOnly + vbInformation, "Introducci�n incorrecta"
        Else
          Set nodo = tvwDiagSelec.Nodes.Add(nodoUlt.nodo.Parent.Key, tvwNext, "R" & nParent2, ")")
          Set nodoUlt.nodo = nodo
          nodoUlt.Tipo = apTIPOPARENT2
          nParent2 = nParent2 + 1
          sqlDiag = sqlDiag & ")"
          sqlCR = sqlCR & ")"
          titulo = titulo & " )"
        End If
    End Select
  End If
End Sub

Function CifrasSignificativas(cod As String) As String
Dim texto As String

  texto = Mid(cod, 3, InStr(cod, ":") - 3)
  While Right(texto, 1) = "0"
    texto = Left(texto, Len(texto) - 1)
  Wend
  texto = "'" & texto & "%'"
  CifrasSignificativas = texto
End Function
