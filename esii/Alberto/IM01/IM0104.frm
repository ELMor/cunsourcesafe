VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmImprimir 
   Caption         =   "Cola de impresi�n"
   ClientHeight    =   7680
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11370
   LinkTopic       =   "Form1"
   ScaleHeight     =   7680
   ScaleWidth      =   11370
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdVer 
      Caption         =   "&Ver Docs"
      Height          =   390
      Left            =   8325
      TabIndex        =   12
      Top             =   225
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipos de documentos"
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   3300
      TabIndex        =   4
      Top             =   150
      Width           =   3015
      Begin VB.CheckBox chkTiDoc 
         Caption         =   "Otros documentos de la historia"
         Height          =   240
         Index           =   2
         Left            =   150
         TabIndex        =   8
         Top             =   900
         Value           =   1  'Checked
         Width           =   2790
      End
      Begin VB.CheckBox chkTiDoc 
         Caption         =   "Informes M�dicos"
         Height          =   240
         Index           =   1
         Left            =   150
         TabIndex        =   7
         Top             =   600
         Value           =   1  'Checked
         Width           =   1590
      End
      Begin VB.CheckBox chkTiDoc 
         Caption         =   "Pruebas"
         Height          =   240
         Index           =   0
         Left            =   150
         TabIndex        =   6
         Top             =   300
         Value           =   1  'Checked
         Width           =   1440
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   9825
      TabIndex        =   3
      Top             =   825
      Width           =   1215
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   390
      Left            =   9825
      TabIndex        =   2
      Top             =   225
      Width           =   1215
   End
   Begin Threed.SSFrame fraDr 
      Height          =   1215
      Left            =   150
      TabIndex        =   0
      Top             =   150
      Width           =   3090
      _Version        =   65536
      _ExtentX        =   5450
      _ExtentY        =   2143
      _StockProps     =   14
      Caption         =   "Usuario"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin SSDataWidgets_B.SSDBCombo cboUsers 
         Height          =   330
         Left            =   225
         TabIndex        =   5
         Tag             =   "T�cnico de citolog�as"
         Top             =   375
         Width           =   2655
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1402
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4419
         Columns(1).Caption=   "Usuario"
         Columns(1).Name =   "Usuario"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4683
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
   End
   Begin ComctlLib.ListView lvwDocs 
      Height          =   5640
      Left            =   150
      TabIndex        =   1
      Top             =   1500
      Width           =   11040
      _ExtentX        =   19473
      _ExtentY        =   9948
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   10
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Tipo"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   1
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Proceso"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   2
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Asist."
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(4) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   3
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "N� prueba"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(5) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   4
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Prueba/Proceso"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(6) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   5
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "N� pers."
         Object.Width           =   706
      EndProperty
      BeginProperty ColumnHeader(7) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   6
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Paciente"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(8) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   7
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "F. impresi�n"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(9) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   8
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "F. Proceso"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(10) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   9
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Copias"
         Object.Width           =   353
      EndProperty
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   1215
      Left            =   6375
      TabIndex        =   9
      Top             =   150
      Width           =   1815
      _Version        =   65536
      _ExtentX        =   3201
      _ExtentY        =   2143
      _StockProps     =   14
      Caption         =   "Procesamiento"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.OptionButton optAccion 
         Caption         =   "Procesados"
         Height          =   240
         Index           =   1
         Left            =   225
         TabIndex        =   11
         Top             =   300
         Value           =   -1  'True
         Width           =   1290
      End
      Begin VB.OptionButton optAccion 
         Caption         =   "Sin procesar"
         Height          =   240
         Index           =   0
         Left            =   225
         TabIndex        =   10
         Top             =   750
         Width           =   1290
      End
   End
End
Attribute VB_Name = "frmImprimir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub cmdImprimir_Click()
Dim sql As String
Dim rdoQ As rdoQuery
Dim Item As ListItem
Dim prSel As String

  Screen.MousePointer = 11
  prSel = "("
  If wword.Documents.Count = 0 Then wword.Documents.Add
  For Each Item In lvwDocs.ListItems
    If Item.Selected = True Then
      prSel = prSel & "(" & Mid(Item.Key, 2, InStr(Item.Key, "O") - 2) & ", " & Right(Item.Key, Len(Item.Key) - InStr(Item.Key, "O")) & "), "
      wword.Application.PrintOut filename:=Item.Tag, Range:=wdPrintAllDocument, Item:= _
      wdPrintDocumentContent, Copies:=Item.SubItems(9), Pages:="", PageType:=wdPrintAllPages, _
      Collate:=True, Background:=True, PrintToFile:=False
    End If
  Next Item
  wword.Documents.Close
  If prSel = "(" Then
    MsgBox "Debe seleccionar alguna prueba para introducir la fecha de dictado", vbInformation, "Introducci�n de la fecha de dictado"
  Else
    prSel = Left(prSel, Len(prSel) - 2) & ")"
    sql = "DELETE FROM IM1300 WHERE (IM01NumDoc, IM13NumOrden) IN " & prSel
    objApp.rdoConnect.Execute sql, rdOpenForwardOnly
    Call LlenarDocs
  End If
  Screen.MousePointer = 0

End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub


Private Sub cmdVer_Click()

  Call LlenarDocs

End Sub

Private Sub Form_Load()
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset

  sql = "SELECT SG0200.SG02Cod, " _
      & "NVL(SG0200.SG02TxtFirma, SG0200.SG02Nom||' '||SG0200.SG02Ape1||' '||SG0200.SG02Ape2) " _
      & "FROM SG0200, AD0300 WHERE " _
      & "AD0300.SG02Cod = SG0200.SG02Cod AND " _
      & "AD0300.AD02CodDpto = ? " _
      & "ORDER BY SG0200.SG02Cod"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cDpt
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    cboUsers.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend
End Sub
Sub LlenarDocs()
Dim sql As String
Dim Item As ListItem
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  If chkTiDoc(0).Value = 0 And chkTiDoc(1).Value = 0 And chkTiDoc(2).Value = 0 Then
    MsgBox "Debe seleccionar un tipo de documento.", vbInformation, "Cola de impresi�n"
    Exit Sub
  End If
  lvwDocs.ListItems.Clear
  
  If chkTiDoc(0).Value = 1 Then
    sql = "SELECT IM1300.IM13NomDoc, PR0400.AD07CodProceso, PR0400.AD01CodAsistenci, " _
        & "PR0400.PR04NumActPlan, PR0100.PR01Descorta, PR0400.CI21CodPersona, " _
        & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
        & "IM1300.IM13FecTrans, IM13FecImpr, IM1300.IM13NumCopias, IM1300.IM01NumDoc, " _
        & "IM1300.IM13NumOrden " _
        & "FROM IM1300, IM0100, PR0400, PR0100, CI2200 WHERE " _
        & "IM1300.IM01NumDoc = PR0400.IM01NumDoc AND " _
        & "IM1300.IM01NumDoc = IM0100.IM01NumDoc AND " _
        & "PR0400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
        & "PR0400.CI21CodPersona = CI2200.CI21CodPersona AND " _
        & "IM1300.SG02Cod = ? AND " _
        & "IM0100.IM10CodTiDoc = ? AND " _
        & "IM1300.IM14CodEstImpr = ? AND " _
        & "PR0400.PR04IndPadre = -1"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = cboUsers.Columns(0).Text
    rdoQ(1) = imTIPODOCPR
    If optAccion(0).Value = True Then rdoQ(2) = 1 Else rdoQ(2) = 2
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    While rdo.EOF = False
      Set Item = lvwDocs.ListItems.Add(, "D" & rdo(10) & "O" & rdo(11), "Pruebas")
      If Not IsNull(rdo(1)) Then Item.SubItems(1) = rdo(1)
      If Not IsNull(rdo(2)) Then Item.SubItems(2) = rdo(2)
      Item.SubItems(3) = rdo(3)
      Item.SubItems(4) = rdo(4)
      Item.SubItems(5) = rdo(5)
      Item.SubItems(6) = rdo(6)
      Item.SubItems(7) = rdo(7)
      If Not IsNull(rdo(8)) Then Item.SubItems(8) = rdo(8)
      Item.SubItems(9) = rdo(9)
      Item.Tag = rdo(0)
      rdo.MoveNext
    Wend
  
  End If
  If chkTiDoc(1).Value = 1 Then
    sql = "SELECT IM1300.IM13NomDoc, AD0800.AD07CodProceso, AD0800.AD01CodAsistenci, " _
        & "AD0700.AD07DesNombProce, CI2200.CI21CodPersona, " _
        & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
        & "IM1300.IM13FecTrans, IM13FecImpr, IM1300.IM13NumCopias, IM1300.IM01NumDoc, " _
        & "IM1300.IM13NumCopias " _
        & "FROM IM1300, IM0100, AD0800, AD0700, CI2200 WHERE " _
        & "IM1300.IM01NumDoc = AD0800.IM01NumDoc AND " _
        & "IM1300.IM01NumDoc = IM0100.IM01NumDoc AND " _
        & "AD0800.AD07CodProceso = AD0700.AD07CodProceso AND " _
        & "AD0700.CI21CodPersona = CI2200.CI21CodPersona AND " _
        & "IM1300.SG02Cod = ? AND " _
        & "IM0100.IM10CodTiDoc = ? AND " _
        & "IM1300.IM14CodEstImpr = ? AND " _
        & "AD0800.AD08IndPadre = -1"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = cboUsers.Columns(0).Text
    rdoQ(1) = imTIPODOCIM
    If optAccion(0).Value = True Then rdoQ(2) = 2 Else rdoQ(2) = 1
    While rdo.EOF = False
      Set Item = lvwDocs.ListItems.Add(, "D" & rdo(9) & "O" & rdo(10), "Informes M�dicos")
      Item.SubItems(1) = rdo(1)
      Item.SubItems(2) = rdo(2)
      Item.SubItems(3) = ""
      Item.SubItems(4) = rdo(3)
      Item.SubItems(5) = rdo(4)
      Item.SubItems(6) = rdo(5)
      Item.SubItems(7) = rdo(6)
      Item.SubItems(8) = rdo(7)
      Item.SubItems(9) = rdo(8)
      Item.Tag = rdo(0)
      rdo.MoveNext
    Wend
  End If
  
  If optAccion(0).Value = True Then rdoQ(2) = 2 Else rdoQ(2) = 1
End Sub

Private Sub lvwDocs_ColumnClick(ByVal ColumnHeader As ComctlLib.ColumnHeader)
  lvwDocs.SortKey = ColumnHeader.Index - 1
  lvwDocs.Sorted = True
End Sub

