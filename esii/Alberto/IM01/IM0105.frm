VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmP_Firma 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Firma de pruebas"
   ClientHeight    =   7395
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11535
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7395
   ScaleWidth      =   11535
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdVer 
      Caption         =   "&Ver pruebas"
      Height          =   390
      Left            =   1950
      TabIndex        =   17
      Top             =   900
      Width           =   1215
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular selec."
      Height          =   390
      Left            =   8775
      TabIndex        =   14
      Top             =   900
      Width           =   1215
   End
   Begin VB.CommandButton cmdSel 
      Caption         =   "Selec. &todo"
      Height          =   390
      Left            =   8775
      TabIndex        =   13
      Top             =   450
      Width           =   1215
   End
   Begin VB.CheckBox chkImprimir 
      Caption         =   "Mandar a la cola de impresi�n"
      Height          =   240
      Left            =   8775
      TabIndex        =   12
      Top             =   150
      Width           =   2415
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   10125
      TabIndex        =   11
      Top             =   900
      Width           =   1215
   End
   Begin VB.CommandButton cmdFirmar 
      Caption         =   "&Firmar"
      Height          =   390
      Left            =   10125
      TabIndex        =   10
      Top             =   450
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ordenar por:"
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   6675
      TabIndex        =   6
      Top             =   75
      Width           =   1965
      Begin VB.OptionButton optOrden 
         Caption         =   "N� Referencia"
         Height          =   315
         Index           =   2
         Left            =   150
         TabIndex        =   9
         Top             =   825
         Width           =   1740
      End
      Begin VB.OptionButton optOrden 
         Caption         =   "F. Mecanografiado"
         Height          =   315
         Index           =   1
         Left            =   150
         TabIndex        =   8
         Top             =   525
         Width           =   1740
      End
      Begin VB.OptionButton optOrden 
         Caption         =   "N� Historia"
         Height          =   315
         Index           =   0
         Left            =   150
         TabIndex        =   7
         Top             =   225
         Value           =   -1  'True
         Width           =   1740
      End
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   1215
      Left            =   3300
      TabIndex        =   0
      Top             =   75
      Width           =   3315
      _Version        =   65536
      _ExtentX        =   5847
      _ExtentY        =   2143
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin SSCalendarWidgets_A.SSDateCombo dtcFirma 
         Height          =   315
         Left            =   1200
         TabIndex        =   4
         Top             =   750
         Width           =   1365
         _Version        =   65537
         _ExtentX        =   2408
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo cboDr 
         Height          =   330
         Left            =   900
         TabIndex        =   1
         Tag             =   "T�cnico de citolog�as"
         Top             =   225
         Width           =   2280
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1402
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4419
         Columns(1).Caption=   "Usuario"
         Columns(1).Name =   "Usuario"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4022
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin MSMask.MaskEdBox mskHora 
         Height          =   315
         Left            =   2625
         TabIndex        =   5
         Top             =   750
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   556
         _Version        =   327681
         ClipMode        =   1
         MaxLength       =   5
         Format          =   "hh:mm"
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
      Begin VB.Label Label1 
         Caption         =   "F. Firma:"
         Height          =   240
         Index           =   1
         Left            =   225
         TabIndex        =   3
         Top             =   750
         Width           =   840
      End
      Begin VB.Label Label1 
         Caption         =   "Doctor:"
         Height          =   240
         Index           =   0
         Left            =   150
         TabIndex        =   2
         Top             =   300
         Width           =   690
      End
   End
   Begin ComctlLib.ListView lvwPr 
      Height          =   5940
      Left            =   150
      TabIndex        =   15
      Top             =   1350
      Width           =   11190
      _ExtentX        =   19738
      _ExtentY        =   10478
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   6
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "N� Ref"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   1
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Prueba"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   2
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "N� persona"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   3
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Paciente"
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(5) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   4
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Dr. Responsable"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   5
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Dr. Ayudante"
         Object.Width           =   2540
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cboUsers 
      Height          =   330
      Left            =   900
      TabIndex        =   16
      Tag             =   "T�cnico de citolog�as"
      Top             =   300
      Width           =   2280
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1402
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4419
      Columns(1).Caption=   "Usuario"
      Columns(1).Name =   "Usuario"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4022
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label Label1 
      Caption         =   "Usuario:"
      Height          =   240
      Index           =   2
      Left            =   225
      TabIndex        =   18
      Top             =   375
      Width           =   690
   End
End
Attribute VB_Name = "frmP_Firma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub cmdVer_Click()
Dim cuser As String, cdr As String
  
  Call LlenarPr

End Sub
Sub LlenarPr()
Dim sql As String
Dim Item As ListItem
Dim rdo As rdoResultset

  lvwPr.ListItems.Clear
  sql = "SELECT PR0400.PR04NumActPlan, PR0100.PR01DesCorta, PR0400.CI21CodPersona, " _
      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
      & "PR0400.IM01NumDoc " _
      & "FROM IM0100, PR0400, PR0100, CI2200 WHERE " _
      & "IM0100.IM01NumDoc = PR0400.IM01NumDoc AND " _
      & "PR0400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR0400.CI21CodPersona = CI2200.CI21CodPersona AND " _
      & "IM0100.IM10CodTiDoc = " & imTIPODOCPR & " AND " _
      & "IM0100.IM08CodEstDoc = " & imDOCPENDIENTE & " AND " _
      & "PR0400.AD02CodDpto = " & cDpt & " AND " _
      & "PR0400.IM01NumDoc Is Not NULL " _
      & " ORDER BY PR0400.PR04NumActPlan"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set Item = lvwPr.ListItems.Add(, "I" & rdo(4), rdo(0))
    Item.SubItems(1) = rdo(1)
    Item.SubItems(2) = rdo(2)
    Item.SubItems(3) = rdo(3)
    rdo.MoveNext
  Wend

End Sub
Private Sub Form_Load()
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim fecha As String

  sql = "SELECT SG0200.SG02Cod, " _
      & "NVL(SG0200.SG02TxtFirma, SG0200.SG02Nom||' '||SG0200.SG02Ape1||' '||SG0200.SG02Ape2), " _
      & "AD3300.AD33IndFirma " _
      & "FROM SG0200, AD0300, AD3300 WHERE " _
      & "AD0300.SG02Cod = SG0200.SG02Cod AND " _
      & "AD0300.AD02CodDpto = AD3300.AD02CodDpto AND " _
      & "AD0300.AD31CodPuesto = AD3300.AD31CodPuesto AND " _
      & "AD0300.AD02CodDpto = ? " _
      & "ORDER BY SG0200.SG02Cod"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cDpt
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    cboUsers.AddItem rdo(0) & Chr$(9) & rdo(1)
    If rdo(2) = -1 Then cboDr.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend
  cboUsers.AddItem "" & Chr$(9) & "Todos", 0
  cboDr.AddItem "" & Chr$(9) & "Todos", 0
  cboDr.AddItem "" & Chr$(9) & "Por cinta", 1
  fecha = Hoy()
  dtcFirma.MaxDate = Format(fecha, "DD/MM/YY")
  dtcFirma.Date = Format(fecha, "DD/MM/YY")
  mskHora.Text = Format(fecha, "HH:MM")
End Sub
