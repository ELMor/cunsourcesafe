VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' **********************************************************************************


' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
'Const APRepProblemas            As String = "AP0001"
'Const APRepMicroMuestra         As String = "AP0002"
'Const APRepAlmacenColonias      As String = "AP0003"
'Const APRepAislamientos         As String = "AP0004"
'Const APRepMicroAntibiograma    As String = "AP0005"
'Const APRepMicroTecnicas        As String = "AP0006"
'Const APRepBionumeros           As String = "AP0007"
'Const APRepDesglosePeticiones   As String = "AP0008"
'Const APRepMicroEdades          As String = "AP0009"

' Las 4 primeras letras identifican al fichero
' Ahora las ventanas. Continuan la numeraci�n a partir del AP1000
Const APWinI_Estadisticas           As String = "AP47"

' Ahora las ventanas comunes a m�s de un proceso (informes)

'Constantes pata los HelpContextID de las ventanas
'Const AGHelpIDTipoRest             As Integer = 26
'Const AGHelpIDTipoRecursos         As Integer = 1
'Const AGHelpIDCalendarios          As Integer = 3
'Const AGHelpIDRecursos             As Integer = 6
'Const AGHelpIDRestriccionesR       As Integer = 15
'Const AGHelpIDRestriccionesF       As Integer = 20
'Const AGHelpIDPerfiles             As Integer = 9
'Const AGHelpIDFranjas              As Integer = 18
'Const AGHelpIDIncidencias          As Integer = 0
'Const AGHelpIDAgendaRecurso        As Integer = 24
'Const AGHelpIDDietarioRecurso      As Integer = 25
'Const AGHelpIDMotivosIncidencia    As Integer = 29


' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

'  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case APWinInformes
      frmInformes.Show (vbModal)
      Set frmInformes = Nothing
    Case APWinI_Discrepancias
      Call objPipe.PipeSet("informe", constINFDiscrep)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Problemas
      Call objPipe.PipeSet("informe", constINFProblemas)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Portas_Interes
      Call objPipe.PipeSet("INFORME", constINFInteresantes)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Bloques_Interes
      Call objPipe.PipeSet("INFORME", constINFInteresantesbloque)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Fotografias
      Call objPipe.PipeSet("INFORME", constINFFotografias)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Almacen
      Call objPipe.PipeSet("INFORME", constINFMuestrasalmacenadas)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Locali_Bloques
      Call objPipe.PipeSet("INFORME", constINFLocalizacion)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Mues_Extradep
      Call objPipe.PipeSet("INFORME", constINFExtradepartamentales)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Inf_Pendientes
      Call objPipe.PipeSet("INFORME", constINFInformespendientes)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Tiempo_Pr
      Call objPipe.PipeSet("INFORME", constINFTiemposContestacion)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Tiempo_Intra
      Call objPipe.PipeSet("INFORME", constINFIntra)
      frmI_Informes.Show (vbModal)
      Set frmI_Informes = Nothing
    Case APWinI_Estadisticas
      Call frmI_Estadisticas.Show(vbModal)
      Set frmI_Estadisticas = Nothing
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  
'  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n� de procesos que se definan
ReDim aProcess(1 To 13, 1 To 4) As Variant
  
  aProcess(1, 1) = APWinInformes
  aProcess(1, 2) = "Informes"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeReport
  
  aProcess(2, 1) = APWinI_Estadisticas
  aProcess(2, 2) = "Estad�sticas"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = APWinI_Discrepancias
  aProcess(3, 2) = "Discrepancias entre diagn�sticos"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeReport
  
  aProcess(4, 1) = APWinI_Problemas
  aProcess(4, 2) = "Problemas detectados"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeReport
  
  aProcess(5, 1) = APWinI_Portas_Interes
  aProcess(5, 2) = "Portas interesantes"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeReport
  
  aProcess(6, 1) = APWinI_Tiempo_Pr
  aProcess(6, 2) = "Tiempos de contestaci�n de pruebas"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeReport
  
  aProcess(7, 1) = APWinI_Tiempo_Intra
  aProcess(7, 2) = "Tiempos de contestaci�n de intras"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeReport
  
  aProcess(8, 1) = APWinI_Inf_Pendientes
  aProcess(8, 2) = "Informes pendientes"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeReport
  
  aProcess(9, 1) = APWinI_Mues_Extradep
  aProcess(9, 2) = "Muestras mandadas a otros centros"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeReport
  
  aProcess(10, 1) = APWinI_Bloques_Interes
  aProcess(10, 2) = "Bloques interesantes"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeReport
  
  aProcess(11, 1) = APWinI_Fotografias
  aProcess(11, 2) = "Fotograf�as"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeReport
  
  aProcess(12, 1) = APWinI_Almacen
  aProcess(12, 2) = "Muestras almacenadas"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeReport
  
  aProcess(13, 1) = APWinI_Locali_Bloques
  aProcess(13, 2) = "Bloques almacenados"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeReport
  
End Sub
