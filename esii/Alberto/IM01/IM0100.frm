VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmPrincipal 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SIHC"
   ClientHeight    =   5955
   ClientLeft      =   285
   ClientTop       =   1785
   ClientWidth     =   8430
   ClipControls    =   0   'False
   HelpContextID   =   101
   Icon            =   "IM0100.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5955
   ScaleWidth      =   8430
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command5 
      Caption         =   "Firma de pruebas"
      Height          =   540
      Left            =   2475
      TabIndex        =   6
      Top             =   4275
      Width           =   2115
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Cola de impresi�n"
      Height          =   540
      Left            =   2475
      TabIndex        =   5
      Top             =   3600
      Width           =   2115
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Mecanografiado de pruebas"
      Height          =   540
      Left            =   2475
      TabIndex        =   4
      Top             =   2925
      Width           =   2115
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Left            =   5850
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   3000
      Width           =   840
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Dictado de pruebas"
      Height          =   540
      Left            =   2475
      TabIndex        =   1
      Top             =   2175
      Width           =   2115
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Transmisi�n"
      Height          =   540
      Left            =   2475
      TabIndex        =   0
      Top             =   1350
      Width           =   2115
   End
   Begin Crystal.CrystalReport crCrystalReport1 
      Left            =   4650
      Top             =   1575
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      WindowState     =   2
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog1 
      Left            =   6255
      Top             =   270
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
      HelpFile        =   "csc.hlp"
   End
   Begin VB.Label Label1 
      Caption         =   "c. Dpto."
      Height          =   315
      Left            =   5100
      TabIndex        =   3
      Top             =   3000
      Width           =   690
   End
   Begin ComctlLib.ImageList imgIcos 
      Left            =   6075
      Top             =   3600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   28
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":030A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":0624
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":093E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":0C58
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":0F72
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":128C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":15A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":18C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":1BDA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":1EF4
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":220E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":2528
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":2842
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":2B5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":2E76
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":3190
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":34AA
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":37C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":3ADE
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":3DF8
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":4112
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":442C
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":4746
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":4A60
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":4D7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":5094
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":53AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "IM0100.frx":56C8
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   6240
      Top             =   1530
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mnuArchivo 
      Caption         =   "&Archivo"
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Paciente"
         Index           =   10
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "Prue&bas"
         Index           =   20
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "P&ersonal"
         Index           =   40
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "P&at�logo externo"
         Index           =   50
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "-"
         Index           =   60
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Decalcificador"
         Index           =   70
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Fijador"
         Index           =   80
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Organo/L�quido"
         Index           =   90
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "A&lmac�n"
         Index           =   100
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "-"
         Index           =   110
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Motivo de inter�s"
         Index           =   120
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "Moti&vo de almacenamiento"
         Index           =   130
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "P&roblemas"
         Index           =   140
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Diagn�stico de presunci�n"
         Index           =   150
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&SNOMED"
         Index           =   160
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "-"
         Index           =   170
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&T�cnica"
         Index           =   180
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Carpeta"
         Index           =   190
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "Per&files de T�cnicas"
         Index           =   200
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "Perfiles de Dia&gn�sticos"
         Index           =   205
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "Tip&os de Porta"
         Index           =   210
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "Tipos de &Bloque"
         Index           =   220
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "Tipo de Pr&ueba"
         Index           =   230
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "Tipo T�cni&ca"
         Index           =   240
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "-"
         Index           =   250
      End
      Begin VB.Menu mnuArchivoOpcion 
         Caption         =   "&Salir"
         Index           =   260
         Shortcut        =   ^S
      End
   End
   Begin VB.Menu mnuProceso 
      Caption         =   "&Proceso"
      Begin VB.Menu mnuProcesoOpcion 
         Caption         =   "&Recepci�n"
         Index           =   10
      End
      Begin VB.Menu mnuProcesoOpcion 
         Caption         =   "&Gesti�n de pruebas"
         Index           =   20
      End
      Begin VB.Menu mnuProcesoOpcion 
         Caption         =   "&Fotos"
         Index           =   30
      End
      Begin VB.Menu mnuProcesoOpcion 
         Caption         =   "Descri&pci�n"
         Index           =   40
      End
      Begin VB.Menu mnuProcesoOpcion 
         Caption         =   "&Diagn�sticos"
         Index           =   50
         Begin VB.Menu mnuDiagOpcion 
            Caption         =   "&Ver Diagn�sticos"
            Index           =   10
         End
         Begin VB.Menu mnuDiagOpcion 
            Caption         =   "&Confirmar Diagn�sticos"
            Index           =   20
         End
      End
   End
   Begin VB.Menu mnuTrabajo 
      Caption         =   "&Trabajo"
      Begin VB.Menu mnuTrabajoOpcion 
         Caption         =   "Hojas de &Recepci�n"
         Index           =   5
      End
      Begin VB.Menu mnuTrabajoOpcion 
         Caption         =   "&Hojas de Trabajo"
         Index           =   10
         Begin VB.Menu mnuHojaOpcion 
            Caption         =   "&Tallado de bloques"
            Index           =   15
         End
         Begin VB.Menu mnuHojaOpcion 
            Caption         =   "&Realizaci�n de bloques"
            Index           =   20
         End
         Begin VB.Menu mnuHojaOpcion 
            Caption         =   "Realizaci�n de &portas"
            Index           =   25
         End
         Begin VB.Menu mnuHojaOpcion 
            Caption         =   "T&�cnicas"
            Index           =   30
         End
      End
      Begin VB.Menu mnuTrabajoOpcion 
         Caption         =   "Reali&zaci�n"
         Index           =   40
         Begin VB.Menu mnuRealiOpcion 
            Caption         =   "&Tallado de bloques"
            Index           =   15
         End
         Begin VB.Menu mnuRealiOpcion 
            Caption         =   "&Realizaci�n de bloques"
            Index           =   20
         End
         Begin VB.Menu mnuRealiOpcion 
            Caption         =   "Realizaci�n de &portas"
            Index           =   25
         End
         Begin VB.Menu mnuRealiOpcion 
            Caption         =   "T&�cnicas"
            Index           =   30
         End
      End
   End
   Begin VB.Menu mnuInformes 
      Caption         =   "&Informes"
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "&Correlaciones"
         Index           =   20
         Begin VB.Menu mnuCorrelacionOpcion 
            Caption         =   "&Diagn�sticos"
            Index           =   10
         End
         Begin VB.Menu mnuCorrelacionOpcion 
            Caption         =   "&Palabras"
            Index           =   20
         End
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "Pro&blemas"
         Index           =   40
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "Muestras interesantes"
         Index           =   50
         Begin VB.Menu mnumuestrasinteresantesopcion 
            Caption         =   "&Portas"
            Index           =   10
         End
         Begin VB.Menu mnumuestrasinteresantesopcion 
            Caption         =   "&Bloques"
            Index           =   20
         End
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "&Fotograf�as"
         Index           =   60
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "Muestras en a&lmac�n"
         Index           =   70
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "Localizaci�n de bloques"
         Index           =   75
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "Muestras &extradepartamentales"
         Index           =   80
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "&Informes pendientes"
         Index           =   90
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "&Tiempos de contestaci�n"
         Index           =   100
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "Esta&d�sticas"
         Index           =   110
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "&Pruebas"
         Index           =   120
      End
      Begin VB.Menu mnuInformesOpcion 
         Caption         =   "Dia&gnostico"
         Index           =   130
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim blnShowFrmMain As Boolean


Private Sub Command1_Click()
  frmTrans.Show
End Sub

Private Sub Command2_Click()
  frmP_Dictado.Show
End Sub

Private Sub Command3_Click()
  frmMeca.Show
End Sub

Private Sub Command4_Click()
  frmImprimir.Show vbModal
End Sub

Private Sub Command5_Click()
  frmP_Firma.Show vbModal
End Sub

Private Sub Form_Load()
  Dim strEntorno1 As String
  Dim strEntorno2 As String
  
  Call InitApp
 
  With objApp
  
    Call .Register(App, Screen)
    
    Set .frmFormMain = Me
    Set .imlImageList = imlImageList1
    Set .crCrystal = crCrystalReport1
    Set .dlgCommon = dlgCommonDialog1
    .objUserColor.lngReadOnly = &HC0C0C0
    .objUserColor.lngMandatory = &HFFFF00
  End With
  
  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotaci�n"
  
  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "Main", "")
  
    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "Main", "")
  
  End With
  
  With objApp
    .blnUseRegistry = True
    .strUserName = "CUN"
    .strPassword = "TUY"
    .strDataSource = "Oracle73_DESA"
'    .strDataSource = "Oracle73_Prep"
'    Me.Caption = Me.Caption & " BASE DE DATOS DE PRUEBAS " & .strDataSource
    .blnAskPassword = False
  End With
    
  If Not objApp.CreateInfo Then
    Call ExitApp
  End If
  cDpt = 204
  Call ArrancarWord
End Sub
Sub OcultarMenu()
Dim i As Integer
  
  For i = 40 To 250 Step 10
    mnuArchivoOpcion(i).Visible = False
  Next i
  mnuArchivoOpcion(205).Visible = False
End Sub

Private Sub Image1_DblClick(intIndex As Integer)
  Call Icon_Execute(intIndex)
End Sub



Private Sub mnuArchivoOpcion_Click(Index As Integer)
'  Select Case Index
'    Case 10
'        Call frmA_Paciente.Show(vbModal)
'        Set frmA_Paciente = Nothing
'    Case 20
'        Call objPipe.PipeSet("Menu", "Archivo")
'        Call frmP_Recepcion.Show(vbModal)
'        Set frmP_Recepcion = Nothing
'    Case 30
''      Separaci�n
'    Case 40
'        Call frmA_Personal.Show(vbModal)
'        Set frmA_Personal = Nothing
'    Case 50
'        Call frmA_CentroPatologo.Show(vbModal)
'        Set frmA_CentroPatologo = Nothing
'    Case 60
''      Separaci�n
'    Case 70
'        Call frmA_Decalcificador.Show(vbModal)
'        Set frmA_Decalcificador = Nothing
'    Case 80
'        Call frmA_Fijador.Show(vbModal)
'        Set frmA_Fijador = Nothing
'    Case 90
'        Call frmA_Organo.Show(vbModal)
'        Set frmA_Organo = Nothing
'    Case 100
'        Call frmA_Almacen.Show(vbModal)
'        Set frmA_Almacen = Nothing
'    Case 110
''      Separaci�n
'    Case 120
'        Call frmA_Interes.Show(vbModal)
'        Set frmA_Interes = Nothing
'    Case 130
'        Call frmA_Almacenamiento.Show(vbModal)
'        Set frmA_Almacenamiento = Nothing
'    Case 140
'        Call frmA_Problema.Show(vbModal)
'        Set frmA_Problema = Nothing
'    Case 150
'        Call frmA_DiagPresuncion.Show(vbModal)
'        Set frmA_DiagPresuncion = Nothing
'    Case 160
'        Call frmA_Snomed.Show(vbModal)
'        Set frmA_Snomed = Nothing
'    Case 170
''      Separacion
'    Case 180
'        Call frmA_Tecnica.Show(vbModal)
'        Set frmA_Tecnica = Nothing
'    Case 190
'        Call frmA_Carpeta.Show(vbModal)
'        Set frmA_Carpeta = Nothing
'    Case 200
'        Call frmA_PerfilTecnica.Show(vbModal)
'        Set frmA_PerfilTecnica = Nothing
'    Case 205
'        Call frmA_PerfilDiag.Show(vbModal)
'        Set frmA_PerfilDiag = Nothing
'    Case 210
'        Call frmA_TipoPorta.Show(vbModal)
'        Set frmA_TipoPorta = Nothing
'    Case 220
'        Call frmA_TipoBloque.Show(vbModal)
'        Set frmA_TipoBloque = Nothing
'    Case 230
'        Call frmA_TipoPrueba.Show(vbModal)
'        Set frmA_TipoPrueba = Nothing
'    Case 240
'        Call frmA_TipoTecnica.Show(vbModal)
'        Set frmA_TipoTecnica = Nothing
'    Case 250
''        Separaci�n
'    Case 260
'        Call CerrarWord
'        End
'  End Select
'
End Sub


Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      'Call objCW.stdAppHelpContext
    Case 20
      'Call objCW.stdAppAbout
  End Select
End Sub

Private Sub Icon_Execute(intIndex As Integer)
'Select Case intIndex
'   Case 0
'      Call objPipe.PipeSet("Menu", "Recepcion")
'      Call frmP_Recepcion.Show(vbModal)
'      Set frmP_Recepcion = Nothing
'   Case 1
'      Call objPipe.PipeSet("Menu", "Petici�n")
'      Call frmBuscPr.Show(vbModal)
'      Set frmBuscPr = Nothing
'   Case 2
'      Call objPipe.PipeSet("Menu", "Fotos")
'      Call frmBuscPr.Show(vbModal)
'      Set frmBuscPr = Nothing
'   Case 3
'      Call objPipe.PipeSet("Menu", "Diag")
'      Call frmBuscPr.Show(vbModal)
'      Set frmBuscPr = Nothing
'   Case 4
'      Call objPipe.PipeSet("Menu", "Descripci�n")
'      Call frmBuscPr.Show(vbModal)
'      Set frmBuscPr = Nothing
'   Case 5
'      'Call objCW.stdAppHelpContext
'End Select
End Sub


Private Sub mnuCorrelacionOpcion_Click(Index As Integer)
'  Select Case Index
'    Case 10
'      Call frmI_C_Diagnosticos.Show(vbModal)
'      Set frmI_C_Diagnosticos = Nothing
'    Case 20
'      Call frmI_C_Palabras.Show(vbModal)
'      Set frmI_C_Palabras = Nothing
'  End Select

End Sub

Private Sub mnuDiagOpcion_Click(Index As Integer)
'  Select Case Index
'    Case 10
'      If ComprobarDatos() = True Then
'        Call objPipe.PipeSet("Menu", "Diag")
'        Call frmBuscPr.Show(vbModal)
'        Set frmBuscPr = Nothing
'      End If
'    Case 20
'      Call frmConfirmarDiag.Show(vbModal)
'      Set frmConfirmarDiag = Nothing
'  End Select
End Sub

'Private Sub mnuEstadisticasOpcion_Click(Index As Integer)
  'Select Case Index
  'Case 10
    'Call frmI_E_Pruebas.Show(vbModal)
    'Set frmI_E_Pruebas = Nothing
  'Case 20
    'Call frmI_E_Tecnicas.Show(vbModal)
    'Set frmI_E_Tecnicas = Nothing
  'End Select
'End Sub

Private Sub mnuHojaOpcion_Click(Index As Integer)
'  Select Case Index
'    Case 15
'      Call objPipe.PipeSet("Menu", "Tallado")
'    Case 20
'      Call objPipe.PipeSet("Menu", "Realizacion")
'    Case 25
'      Call objPipe.PipeSet("Menu", "Corte")
'    Case 30
'      Call objPipe.PipeSet("Menu", "Tecnicas")
'  End Select
'  Call frmI_HT.Show(vbModal)
'  Set frmI_HT = Nothing

End Sub

Private Sub mnuInformesOpcion_Click(Index As Integer)
'    Select Case Index
'        Case 10
''           Hojas de trabajo
'        Case 20
''           Correlaciones
'        Case 30
''           Separaci�n
'        Case 40
'            Call objPipe.PipeSet("informe", constINFProblemas)
'            frmI_Informes.Show (vbModal)
'            'Call frmI_Problemas.Show(vbModal)
'            'Set frmI_Problemas = Nothing
'        'Case 50
'            'Call frmI_Interes.Show(vbModal)
'            'Set frmI_Interes = Nothing
'        Case 60
'            Call objPipe.PipeSet("INFORME", constINFFotografias)
'            frmI_Informes.Show (vbModal)
'            'Call frmI_Fotos.Show(vbModal)
'            'Set frmI_Fotos = Nothing
'        Case 70
'            Call objPipe.PipeSet("INFORME", constINFMuestrasalmacenadas)
'            frmI_Informes.Show (vbModal)
'            'Call frmI_Almacen.Show(vbModal)
'            'Set frmI_Almacen = Nothing
'        Case 75
'            Call objPipe.PipeSet("INFORME", constINFLocalizacion)
'            frmI_Informes.Show (vbModal)
'        Case 80
'            Call objPipe.PipeSet("INFORME", constINFExtradepartamentales)
'            frmI_Informes.Show (vbModal)
'            'Call frmI_Externas.Show(vbModal)
'            'Set frmI_Externas = Nothing
'        Case 90
'            Call objPipe.PipeSet("INFORME", constINFInformespendientes)
'            frmI_Informes.Show (vbModal)
'            'Call frmI_Pendientes.Show(vbModal)
'            'Set frmI_Pendientes = Nothing
'        Case 100
'            Call objPipe.PipeSet("INFORME", constINFTiemposContestacion)
'            frmI_Informes.Show (vbModal)
'            'Call frmI_Tiempos.Show(vbModal)
'            'Set frmI_Tiempos = Nothing
'        Case 110
'            frmI_Estadisticas.Show (vbModal)
'        Case 120
'            frmI_Pruebas.Show (vbModal)
'        Case 130
'            frmI_Diagnostico.Show (vbModal)
'    End Select

End Sub
Private Sub mnumuestrasinteresantesopcion_click(Index As Integer)
'   Select Case Index
'    Case 10
'            Call objPipe.PipeSet("INFORME", constINFInteresantes)
'            frmI_Informes.Show (vbModal)
'    Case 20
'            Call objPipe.PipeSet("INFORME", constINFInteresantesbloque)
'            frmI_Informes.Show (vbModal)
'    End Select
End Sub


Private Sub mnuProcesoOpcion_Click(Index As Integer)
'  Select Case Index
'    Case 10
'      Call objPipe.PipeSet("Menu", "Recepcion")
'      Call frmP_Recepcion.Show(vbModal)
'      Set frmP_Recepcion = Nothing
'  Case 20
'      Call objPipe.PipeSet("Menu", "Petici�n")
'      Call frmBuscPr.Show(vbModal)
'      Set frmBuscPr = Nothing
'    Case 30
'      Call objPipe.PipeSet("Menu", "Fotos")
'      Call frmBuscPr.Show(vbModal)
'      Set frmBuscPr = Nothing
'    Case 40
'      Call objPipe.PipeSet("Menu", "Descripci�n")
'      Call frmBuscPr.Show(vbModal)
'      Set frmBuscPr = Nothing
'  End Select
End Sub

Private Sub mnuRealiOpcion_Click(Index As Integer)
'  Select Case Index
'    Case 15
'      Call objPipe.PipeSet("Menu", "Tallado")
'    Case 20
'      Call objPipe.PipeSet("Menu", "Realizacion")
'    Case 25
'      Call objPipe.PipeSet("Menu", "Corte")
'    Case 30
'      Call objPipe.PipeSet("Menu", "Tecnicas")
'  End Select
'  Call frmI_Reali.Show(vbModal)
'  Set frmI_Reali = Nothing
End Sub

Private Sub mnuTrabajoOpcion_Click(Index As Integer)
'  Select Case Index
'    Case 5
'      Call frmI_Recepcion.Show(vbModal)
'      Set frmI_Recepcion = Nothing
'  End Select
End Sub


Private Sub Text1_Change()
  cDpt = Text1.Text
End Sub
