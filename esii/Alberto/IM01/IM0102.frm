VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmTrans 
   Caption         =   "Creaci�n de documentos"
   ClientHeight    =   7590
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11610
   LinkTopic       =   "Form1"
   ScaleHeight     =   7590
   ScaleWidth      =   11610
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   9600
      TabIndex        =   14
      Top             =   750
      Width           =   1515
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Enabled         =   0   'False
      Height          =   390
      Left            =   7800
      TabIndex        =   13
      Top             =   750
      Width           =   1515
   End
   Begin VB.CommandButton cmdAtras 
      Caption         =   "Volver a &procesar"
      Enabled         =   0   'False
      Height          =   390
      Left            =   7800
      TabIndex        =   12
      Top             =   150
      Width           =   1515
   End
   Begin VB.CommandButton cmdVer 
      Caption         =   "&Ver datos"
      Height          =   390
      Left            =   9600
      TabIndex        =   11
      Top             =   150
      Width           =   1515
   End
   Begin Threed.SSFrame SSFrame3 
      Height          =   1140
      Left            =   6075
      TabIndex        =   7
      Top             =   75
      Width           =   1440
      _Version        =   65536
      _ExtentX        =   2540
      _ExtentY        =   2011
      _StockProps     =   14
      Caption         =   "Acci�n"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CheckBox chkAccion 
         Caption         =   "Borrar"
         Height          =   195
         Index           =   2
         Left            =   150
         TabIndex        =   10
         Top             =   900
         Value           =   1  'Checked
         Width           =   1065
      End
      Begin VB.CheckBox chkAccion 
         Caption         =   "Modificar"
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   9
         Top             =   600
         Value           =   1  'Checked
         Width           =   1065
      End
      Begin VB.CheckBox chkAccion 
         Caption         =   "A�adir"
         Height          =   195
         Index           =   0
         Left            =   150
         TabIndex        =   8
         Top             =   300
         Value           =   1  'Checked
         Width           =   1065
      End
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   1140
      Left            =   225
      TabIndex        =   1
      Top             =   75
      Width           =   2490
      _Version        =   65536
      _ExtentX        =   4392
      _ExtentY        =   2011
      _StockProps     =   14
      Caption         =   "Tipos de documentos"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.OptionButton optTiDoc 
         Caption         =   "Informe M�dicos"
         Height          =   240
         Index           =   1
         Left            =   225
         TabIndex        =   3
         Top             =   675
         Width           =   1815
      End
      Begin VB.OptionButton optTiDoc 
         Caption         =   "Pruebas"
         Height          =   240
         Index           =   0
         Left            =   225
         TabIndex        =   2
         Top             =   300
         Value           =   -1  'True
         Width           =   1365
      End
   End
   Begin ComctlLib.ListView lvwDoc 
      Height          =   6165
      Left            =   225
      TabIndex        =   0
      Top             =   1350
      Width           =   11115
      _ExtentX        =   19606
      _ExtentY        =   10874
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin Threed.SSFrame SSFrame2 
      Height          =   1140
      Left            =   3150
      TabIndex        =   4
      Top             =   75
      Width           =   2640
      _Version        =   65536
      _ExtentX        =   4657
      _ExtentY        =   2011
      _StockProps     =   14
      Caption         =   "Estado"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.OptionButton optEstado 
         Caption         =   "Error en la generaci�n"
         Height          =   240
         Index           =   1
         Left            =   225
         TabIndex        =   6
         Top             =   675
         Width           =   2115
      End
      Begin VB.OptionButton optEstado 
         Caption         =   "Pendientes de procesar"
         Height          =   240
         Index           =   0
         Left            =   225
         TabIndex        =   5
         Top             =   300
         Value           =   -1  'True
         Width           =   2190
      End
   End
End
Attribute VB_Name = "frmTrans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim nProc As Long, nAsist As Long, fTrans As String, nPr As Long

Private Sub cmdAtras_Click()
Dim sql As String
Dim rdoQ As rdoQuery

  If optTiDoc(0).Value = True Then ' Pruebas
    sql = "UPDATE IM0200 SET IM02CodEstado = 1 WHERE " _
        & "PR04NumActPlan = ? AND " _
        & "IM02FecTrans = TO_DATE(?, 'DD/MM/YY HH24:MI:SS')"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = nPr
    rdoQ(1) = fTrans
    rdoQ.Execute
    Call LlenarPr
  Else ' Informes M�dicos
    sql = "UPDATE IM0300 SET IM03CodEstado = 1 WHERE " _
        & "AD01CodAsistenci = ? AND " _
        & "AD07CodProceso = ? AND " _
        & "IM03FecTrans = TO_DATE(?, 'DD/MM/YY HH24:MI:SS')"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = nAsist
    rdoQ(1) = nProc
    rdoQ(2) = fTrans
    rdoQ.Execute
    Call LlenarIM
  End If
End Sub

Private Sub cmdBorrar_Click()
Dim sql As String
Dim rdoQ As rdoQuery
Dim res As Integer
    
  res = MsgBox("�Desea borrar el registro de transmisi�n?", vbYesNo + vbQuestion, "Transmisi�n de pruebas")
  If res = vbYes Then
    If optTiDoc(0).Value = True Then ' Pruebas
      sql = "DELETE IM0200 WHERE " _
          & "PR04NumActPlan = ? AND " _
          & "IM02FecTrans = TO_DATE(?, 'DD/MM/YY HH24:MI:SS')"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nPr
      rdoQ(1) = fTrans
      rdoQ.Execute
      Call LlenarPr
    Else ' Informes M�dicos
      sql = "DELETE IM0300 WHERE " _
          & "AD01CodAsistenci = ? AND " _
          & "AD07CodProceso = ? AND " _
          & "IM03FecTrans = TO_DATE(?, 'DD/MM/YY HH24:MI:SS')"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nAsist
      rdoQ(1) = nProc
      rdoQ(2) = fTrans
      rdoQ.Execute
      Call LlenarIM
    End If
  End If
End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub cmdVer_Click()
  If chkAccion(0).Value = Unchecked And chkAccion(1).Value = Unchecked And chkAccion(2).Value = Unchecked Then
    MsgBox "Debe seleccionar una acci�n a visualizar", vbInformation, "Tablas de transmisi�n"
  Else
    If optTiDoc(0).Value = True Then Call LlenarPr Else Call LlenarIM
  End If
End Sub

Private Sub Form_Load()
  Call LlenarPr
End Sub

Sub LlenarPr()
Dim sql As String
Dim rdo As rdoResultset
Dim Item As ListItem
Dim estado As Integer
Dim i As Integer

  lvwDoc.ListItems.Clear
  If optEstado(0).Value = True Then estado = 1 Else estado = 0
  With lvwDoc.ColumnHeaders
    .Clear
    .Add , , "N� Pr", 500
    .Add , , "Prueba", 3000
    .Add , , "N� persona", 500
    .Add , , "Paciente", 4000
    .Add , , "F. Transmisi�n", 1400
    If estado = 0 Then
      .Add , , "Error", 2000
      .Add , , "F. Procesado", 1400
    End If
  End With
  
  sql = "SELECT IM0200.PR04NumActPlan, PR0100.PR01DesCorta, PR0400.CI21CodPersona, " _
      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
      & "IM0200.IM02FecTrans, IM0200.IM02Observ, IM0200.IM02FecRecop " _
      & "FROM IM0200, PR0100, PR0400, CI2200 WHERE " _
      & "IM0200.PR04NumActPlan = PR0400.PR04NumActPlan AND " _
      & "PR0400.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "PR0400.CI21CodPersona = CI2200.CI21CodPersona AND " _
      & "IM0200.IM02CodEstado = " & estado
  If chkAccion(0).Value = Checked Then
    sql = sql & " AND IM0200.IM02Accion = 'A'"
  ElseIf chkAccion(1).Value = Checked Then
    sql = sql & " AND IM0200.IM02Accion = 'M'"
  ElseIf chkAccion(1).Value = Checked Then
    sql = sql & " AND IM0200.IM02Accion = 'D'"
  End If
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    i = i + 1
    Set Item = lvwDoc.ListItems.Add(, , rdo(0))
    Item.SubItems(1) = rdo(1)
    Item.SubItems(2) = rdo(2)
    Item.SubItems(3) = rdo(3)
    Item.SubItems(4) = rdo(4)
    If estado = 0 Then
      Item.SubItems(5) = rdo(5)
      Item.SubItems(6) = rdo(6)
    End If
    rdo.MoveNext
  Wend
  cmdBorrar.Enabled = False
  Me.Caption = "Creaci�n de documentos: " & i & " pendientes de procesar."
End Sub

Sub LlenarIM()
Dim sql As String
Dim rdo As rdoResultset
Dim Item As ListItem
Dim estado As Integer
Dim i As Integer
  
  lvwDoc.ListItems.Clear
  i = 0
  If optEstado(0).Value = True Then estado = 1 Else estado = 0
  With lvwDoc.ColumnHeaders
    .Clear
    .Add , , "N� Proc.", 500
    .Add , , "N� Asist", 500
    .Add , , "Proceso", 2000
    .Add , , "N� persona", 500
    .Add , , "Paciente", 2000
    .Add , , "F. Transmisi�n", 1400
    If estado = 0 Then
      .Add , , "Error", 2000
      .Add , , "F. Procesado", 1400
    End If
  End With
  sql = "SELECT IM0300.AD07CodProceso, IM0300.AD01CodAsistenci, AD0700.AD07DesNombProce, " _
      & "AD0700.CI21CodPersona, " _
      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre, " _
      & "IM0300.IM03FecTrans, IM0300.IM03Observ, IM0300.IM03FecRecop " _
      & "FROM IM0300, AD0700, CI2200 WHERE " _
      & "IM0300.AD07CodProceso = AD0700.AD07CodProceso AND " _
      & "AD0700.CI21CodPersona = CI2200.CI21CodPersona AND " _
      & "IM0300.IM03CodEstado = " & estado
  If chkAccion(0).Value = Checked Then
    sql = sql & " AND IM0300.IM03Accion = 'A'"
  ElseIf chkAccion(1).Value = Checked Then
    sql = sql & " AND IM0300.IM03Accion = 'M'"
  ElseIf chkAccion(1).Value = Checked Then
    sql = sql & " AND IM0300.IM03Accion = 'D'"
  End If
  
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    i = i + 1
    Set Item = lvwDoc.ListItems.Add(, , rdo(0))
    Item.SubItems(1) = rdo(1)
    If IsNull(rdo(2)) Then Item.SubItems(2) = "" Else Item.SubItems(2) = rdo(2)
    Item.SubItems(3) = rdo(3)
    Item.SubItems(4) = rdo(4)
    Item.SubItems(5) = rdo(5)
    If estado = 0 Then
      Item.SubItems(6) = rdo(6)
      Item.SubItems(7) = rdo(7)
    End If
    rdo.MoveNext
  Wend
  cmdBorrar.Enabled = False
  Me.Caption = "Creaci�n de documentos: " & i & " pendientes de procesar."
End Sub

Private Sub lvwDoc_Click()
  Set lvwDoc.DropHighlight = lvwDoc.SelectedItem
End Sub

Private Sub lvwDoc_ColumnClick(ByVal ColumnHeader As ComctlLib.ColumnHeader)
  lvwDoc.SortKey = ColumnHeader.Index - 1
  lvwDoc.Sorted = True

End Sub

Private Sub lvwDoc_ItemClick(ByVal Item As ComctlLib.ListItem)
  
  cmdBorrar.Enabled = True
  If optEstado(1).Value = True Then cmdAtras.Enabled = True
  If optTiDoc(1).Value = True Then ' Pruebas
    nProc = Item.Text
    nAsist = Item.SubItems(1)
    fTrans = Item.SubItems(5)
  Else
    nPr = Item.Text
    fTrans = Item.SubItems(4)
  End If
End Sub

Private Sub optEstado_Click(Index As Integer)
  lvwDoc.ListItems.Clear
  cmdAtras.Enabled = False
  cmdBorrar.Enabled = False
End Sub

Private Sub optTiDoc_Click(Index As Integer)
  lvwDoc.ListItems.Clear
End Sub
