Attribute VB_Name = "modComun"
Option Explicit

Sub DptUsuario()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AD02CodDpto FROM AD0300 WHERE SG02Cod = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = objSecurity.strUser
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "El usuario no tiene definido ning�n departamento.", vbCritical, "Usuario err�neo"
  Else
    cDpt = rdo(0)
  End If
End Sub
Public Sub ArrancarWord()
Dim Winword As String
  
  On Error Resume Next
  Err = 0
  Winword = wword.Application.Version
  If Winword = "" Then
    Err = 0
    Set wword = Word.Application
    If Err <> 0 Then
      Err = 0
      Set wword = New Word.Application
      If Err <> 0 Then
        MsgBox "No se ha conseguido establecer comunicaci�n con el WWord: " & Error, vbCritical + vbOKOnly, "Visualizaci�n de documentos"
        End
      End If
    Else
      wword.Documents.Close SaveChanges:=0
    End If
    wword.Application.Visible = True
    wword.Application.WindowState = wdWindowStateMinimize
    wword.CommandBars.LargeButtons = False
    wword.CommandBars.DisplayTooltips = True
    wword.ActiveWindow.ActivePane.View.ShowAll = False
    Err = 0
  End If
End Sub

Function Hoy() As String
Dim sql As String
Dim rdo As rdoResultset
  
  sql = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  Hoy = rdo(0)
  rdo.Close
End Function

