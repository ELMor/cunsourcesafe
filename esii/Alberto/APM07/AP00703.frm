VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_TipoBloque 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de los Tipos de Bloques"
   ClientHeight    =   6795
   ClientLeft      =   2550
   ClientTop       =   3090
   ClientWidth     =   9765
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6795
   ScaleWidth      =   9765
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraTecnica 
      Caption         =   "Tipos de T�cnicas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3075
      Left            =   90
      TabIndex        =   6
      Top             =   3240
      Width           =   9465
      Begin SSDataWidgets_B.SSDBGrid grdssTecnica 
         Height          =   2580
         Index           =   0
         Left            =   135
         TabIndex        =   7
         Top             =   360
         Width           =   9195
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   16219
         _ExtentY        =   4551
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraTipo 
      Caption         =   "Tipos de Bloques"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2670
      Left            =   90
      TabIndex        =   5
      Top             =   450
      Width           =   9465
      Begin TabDlg.SSTab tabTipo 
         Height          =   2130
         HelpContextID   =   90001
         Left            =   150
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   375
         Width           =   9000
         _ExtentX        =   15875
         _ExtentY        =   3757
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtTipo(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtTipo(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtTipo(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkTipo(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTipo"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkTipo 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP05_INDActiva"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   7350
            TabIndex        =   3
            Tag             =   "Activo|Registro activo"
            Top             =   600
            Width           =   990
         End
         Begin VB.TextBox txtTipo 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP05_Desig"
            Height          =   285
            Index           =   1
            Left            =   1635
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Nombre abreviado del Tipo de Bloque"
            Top             =   555
            Width           =   3750
         End
         Begin VB.TextBox txtTipo 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP05_CODTiBloq"
            Height          =   285
            Index           =   0
            Left            =   1650
            TabIndex        =   0
            Tag             =   "C�digo|C�digo del Tipo de Bloque"
            Top             =   30
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtTipo 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP05_Descrip"
            Height          =   285
            Index           =   2
            Left            =   1620
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del Tipo de Bloque"
            Top             =   1200
            Width           =   6705
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTipo 
            Height          =   1920
            Left            =   -74925
            TabIndex        =   10
            Top             =   75
            Width           =   8460
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   14922
            _ExtentY        =   3387
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   360
            TabIndex        =   12
            Top             =   555
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   495
            TabIndex        =   11
            Top             =   1200
            Width           =   1080
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   6510
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_TipoBloque"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objTipo As New clsCWForm
Dim objTecnica As New clsCWForm


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  
  Dim strKey As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objTipo
    .strName = "Tipo"
    Set .objFormContainer = fraTipo
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTipo
    Set .grdGrid = grdssTipo
    .strTable = "AP0500"
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("AP05_Desig", cwAscending)
    Call .objPrinter.Add("AP0301.rpt", "Listado de Tipos de bloques")
    Call .objPrinter.Add("AP0302.rpt", "Listado de Tipos de bloques/t�cnicas")
    
    Call .FormCreateFilterWhere(.strTable, "Tipos de bloques")
    Call .FormAddFilterWhere(.strTable, "AP05_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(.strTable, "AP05_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(.strTable, "AP05_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(.strTable, "AP05_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(.strTable, "AP05_DESCRIP", "Descripci�n")
    .blnAskPrimary = False

  End With
  
  With objTecnica
    .strName = "Tecnica"
    Set .objFormContainer = fraTecnica
    Set .objFatherContainer = fraTipo
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssTecnica(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP5300"

    Call .FormAddOrderField("AP04_CodTiTec", cwAscending)
    Call .FormAddRelation("AP05_CodTiBloq", txtTipo(0))
  
  End With
  
  With objWinInfo
    Call .FormAddInfo(objTipo, cwFormDetail)
    
    Call .FormAddInfo(objTecnica, cwFormMultiLine)
    Call .GridAddColumn(objTecnica, "Tipo Bloque", "AP05_CodTiBloq", cwNumeric, 2)
    Call .GridAddColumn(objTecnica, "Tipo t�cnica", "AP04_CodTiTec", cwNumeric, 2)
        
    Call .FormCreateInfo(objTipo)
    .CtrlGetInfo(txtTipo(0)).blnValidate = False
    .CtrlGetInfo(txtTipo(1)).blnInFind = True
    .CtrlGetInfo(txtTipo(2)).blnInFind = True
    .CtrlGetInfo(chkTipo(0)).blnInFind = True

    .CtrlGetInfo(grdssTecnica(0).Columns(4)).blnForeign = True
    
    Call .FormChangeColor(objTecnica)
    
    .CtrlGetInfo(grdssTecnica(0).Columns(4)).strSQL = "SELECT AP04_CodTiTec, AP04_Desig FROM AP0400 ORDER BY AP04_CodTiTec"
    grdssTecnica(0).Columns(4).Width = 4000
    
    Call .FormChangeColor(objTecnica)
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdssTecnica(0).Columns(3).Visible = False
  grdssTipo.Columns(1).Visible = False
  
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim sql As String
  Select Case strFormName
    Case objTecnica.strName
      frmA_TipoTecnica.Show vbModal
      Set frmA_TipoTecnica = Nothing
  End Select
  Call objWinInfo.DataRefresh

End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Select Case strFormName
    Case objTipo.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtTipo(0).Text = fNextClave("AP05_CODTiBloq")
        objTipo.rdoCursor("AP05_CODTiBloq") = txtTipo(0).Text
      End If
  End Select
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssTipo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTipo_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTipo_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTipo_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssTecnica_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTecnica_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTecnica_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTecnica_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTipo_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTipo, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraTipo_Click()
  Call objWinInfo.FormChangeActive(fraTipo, False, True)
End Sub

'==========================================================================
Private Sub fraTecnica_Click()
  Call objWinInfo.FormChangeActive(fraTecnica, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkTipo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkTipo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkTipo_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtTipo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtTipo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTipo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


