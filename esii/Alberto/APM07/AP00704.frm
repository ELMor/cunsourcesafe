VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_Tecnica 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de T�cnicas"
   ClientHeight    =   7170
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9855
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7170
   ScaleWidth      =   9855
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3300
      Left            =   150
      TabIndex        =   15
      Top             =   3450
      Width           =   9465
      Begin TabDlg.SSTab tabDatos 
         Height          =   2910
         HelpContextID   =   90001
         Left            =   225
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   300
         Width           =   8730
         _ExtentX        =   15399
         _ExtentY        =   5133
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   -2147483644
         TabCaption(0)   =   "Perfiles"
         TabPicture(0)   =   "AP00704.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "fraPerfil"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Puede hacerse sobre"
         TabPicture(1)   =   "AP00704.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "fraSust"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.Frame fraPerfil 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2415
            Left            =   -74775
            TabIndex        =   19
            Top             =   375
            Width           =   8160
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2250
               Index           =   0
               Left            =   75
               TabIndex        =   20
               Top             =   75
               Width           =   7875
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   13891
               _ExtentY        =   3969
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraSust 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2415
            Left            =   150
            TabIndex        =   17
            Top             =   375
            Width           =   8505
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2250
               Index           =   1
               Left            =   150
               TabIndex        =   18
               Top             =   75
               Width           =   7875
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   13891
               _ExtentY        =   3969
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   500
   End
   Begin VB.Frame fraTecnica 
      Caption         =   "T�cnicas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   9525
      Begin TabDlg.SSTab tabTecnica 
         Height          =   2445
         HelpContextID   =   90001
         Left            =   240
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "C�digo|C�digo de la T�cnica"
         Top             =   360
         Width           =   9000
         _ExtentX        =   15875
         _ExtentY        =   4313
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AP00704.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cbossTecnica"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtTecnica(3)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkTecnica"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtTecnica(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtTecnica(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtTecnica(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTecnica"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtTecnica 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP12_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1335
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n de la T�cnica"
            Top             =   660
            Width           =   6870
         End
         Begin VB.TextBox txtTecnica 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP12_CODTEC"
            Height          =   285
            Index           =   0
            Left            =   6300
            TabIndex        =   0
            Tag             =   "C�digo|C�digo de la T�cnica"
            Top             =   225
            Visible         =   0   'False
            Width           =   570
         End
         Begin VB.TextBox txtTecnica 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP12_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1335
            MaxLength       =   30
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n de la T�cnica"
            Top             =   225
            Width           =   4800
         End
         Begin VB.CheckBox chkTecnica 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP12_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7200
            TabIndex        =   4
            Tag             =   "Activa|Activa"
            Top             =   225
            Width           =   990
         End
         Begin VB.TextBox txtTecnica 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP12_OBSERV"
            Height          =   750
            Index           =   3
            Left            =   150
            MaxLength       =   2000
            TabIndex        =   5
            Tag             =   "Observaciones|Observaciones sobre la t�cnica"
            Top             =   1500
            Width           =   8040
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTecnica 
            DataField       =   "AP04_CODTITEC"
            Height          =   315
            Left            =   3570
            TabIndex        =   3
            Tag             =   "Tipo T�cnica|Tipo de T�cnica"
            Top             =   1080
            Width           =   2865
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1693
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3810
            Columns(1).Caption=   "Designaci�n"
            Columns(1).Name =   "Designaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5054
            _ExtentY        =   556
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTecnica 
            Height          =   2070
            Left            =   -74820
            TabIndex        =   14
            Top             =   180
            Width           =   8280
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   14605
            _ExtentY        =   3651
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   135
            TabIndex        =   13
            Top             =   705
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   12
            Top             =   240
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Observaciones:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   150
            TabIndex        =   11
            Top             =   1200
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo T�cnica:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   2295
            TabIndex        =   10
            Top             =   1125
            Width           =   1230
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   6885
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Tecnica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objTecnica As New clsCWForm
Dim objPerfil As New clsCWForm
Dim objSust As New clsCWForm
Dim desigTiTec As String
Private Sub cbossTecnica_Click()
    Call objWinInfo.CtrlDataChange
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossTecnica_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossTecnica_GotFocus()
  Call objWinInfo.CtrlGotFocus
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub cbossTecnica_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkTecnica_Click()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkTecnica_GotFocus()
  Call objWinInfo.CtrlGotFocus
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub chkTecnica_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

' -----------------------------------------------
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim strKey As String, sql As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, Me, tlbToolbar1, stbStatusBar1, cwWithAll)
  
  With objTecnica
    Set .objFormContainer = fraTecnica
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTecnica
    Set .grdGrid = grdssTecnica
    .strTable = "AP1200"
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("AP12_CODTEC", cwAscending)
    Call .objPrinter.Add("AP0401.rpt", "Listado de t�cnicas")
    Call .objPrinter.Add("AP0402.rpt", "Listado de tipos de t�cnicas/t�cnicas")
    Call .objPrinter.Add("AP0403.rpt", "Listado de t�cnicas/perfiles")
    .blnAskPrimary = False
    strKey = .strTable
    Call .FormCreateFilterWhere(strKey, "Tipos de t�cnicas")
    Call .FormAddFilterWhere(strKey, "AP12_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AP12_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AP04_CodTiTec", "C�digo tipo de t�cnica", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AP12_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "AP04_CodTiTec", "C�digo tipo de t�cnica")
    Call .FormAddFilterOrder(strKey, "AP12_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "AP12_DESCRIP", "Descripci�n")
  
  End With
  
  With objPerfil
    Set .objFormContainer = fraPerfil
    Set .objFatherContainer = fraTecnica
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP2700"
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("AP01_CODPERF", cwAscending)
    Call .FormAddRelation("AP12_CODTEC", txtTecnica(0))
  End With
  
  With objSust
    Set .objFormContainer = fraSust
    Set .objFatherContainer = fraTecnica
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP6100"
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("AP12_CODTec_Fin", cwAscending)
    Call .FormAddRelation("AP12_CODTEC_Ini", txtTecnica(0))
  End With
  
  With objWinInfo
    Call .FormAddInfo(objTecnica, cwFormDetail)
    
    Call .FormAddInfo(objPerfil, cwFormMultiLine)
    Call .GridAddColumn(objPerfil, "T�cnica", "AP12_CODTEC", cwNumeric, 3)
    Call .GridAddColumn(objPerfil, "Perfil", "AP01_CODPERF", cwNumeric, 3)
    Call .GridAddColumn(objPerfil, "N� de T�cnicas", "AP27_NUMTEC", cwNumeric, 2)
    
    Call .FormAddInfo(objSust, cwFormMultiLine)
    Call .GridAddColumn(objSust, "T�cnica", "AP12_CODTEC_INI", cwNumeric, 3)
    Call .GridAddColumn(objSust, "Tipo de t�cnica", "", cwNumeric, 3)
    Call .GridAddColumn(objSust, "T�cnica a sustituir", "AP12_CODTEC_Fin", cwNumeric, 2)
    
    Call .FormCreateInfo(objTecnica)
    .CtrlGetInfo(cbossTecnica).strSQL = "SELECT AP04_CodTiTec, AP04_Desig FROM AP0400 ORDER BY AP04_Desig"
    .CtrlGetInfo(txtTecnica(0)).blnValidate = False
    
    .CtrlGetInfo(grdssDatos(0).Columns(4)).strSQL = "SELECT AP01_CodPerf, AP01_Desig FROM AP0100 ORDER BY AP01_Desig"
    Call .FormChangeColor(objPerfil)
   
    .CtrlGetInfo(grdssDatos(1).Columns(4)).strSQL = "SELECT AP04_CodTiTec, AP04_Desig FROM AP0400 ORDER BY AP04_Desig"
    .CtrlGetInfo(grdssDatos(1).Columns(5)).strSQL = "SELECT AP12_CodTec, AP12_Desig FROM AP1200 ORDER BY AP12_Desig"
    
    .CtrlGetInfo(cbossTecnica).blnForeign = True
    .CtrlGetInfo(grdssDatos(0).Columns(4)).blnForeign = True
    
    .CtrlGetInfo(txtTecnica(1)).blnInFind = True
    .CtrlGetInfo(txtTecnica(2)).blnInFind = True
    .CtrlGetInfo(cbossTecnica).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdssTecnica.Columns(1).Visible = False
  grdssDatos(0).Columns(3).Visible = False
  grdssDatos(1).Columns(3).Visible = False
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraTecnica_Click()
    Call objWinInfo.FormChangeActive(fraTecnica, False, True)
    'fraTecnica.ForeColor = cwAZUL
    'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub fraDatos_Click()
  Call objWinInfo.FormChangeActive(fraPerfil, True, True)
End Sub

Private Sub fraPerfil_Click()
    Call objWinInfo.FormChangeActive(fraPerfil, False, True)
    'fraTecnica.ForeColor = cwGRIS
    'fraDatos.ForeColor = cwAZUL
End Sub


Private Sub grdssDatos_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssDatos_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssDatos_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraTecnica.ForeColor = cwGRIS
  'fraDatos.ForeColor = cwAZUL
End Sub
Private Sub grdssDatos_RowColChange(Index As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  If grdssDatos(1).Col = 5 Then
    If grdssDatos(1).Columns(1).Text = 6 Then
      desigTiTec = grdssDatos(1).Columns(4).Text
      Call pCargarDropDownTec
    End If
  End If

End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim sql As String
  Select Case strFormName
    Case objTecnica.strName
      Select Case strCtrl
        Case "cbossTecnica"
          frmA_TipoTecnica.Show vbModal
          Set frmA_TipoTecnica = Nothing
          sql = "SELECT AP04_CodTiTec, AP04_Desig FROM AP0400 ORDER BY AP04_Desig"
          objWinInfo.CtrlGetInfo(cbossTecnica).strSQL = sql
          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossTecnica))
      End Select
    Case objPerfil.strName
      frmA_PerfilTecnica.Show vbModal
      Set frmA_PerfilTecnica = Nothing
      sql = "SELECT AP01_CodPerf, AP01_Desig FROM AP0100 ORDER BY AP01_Desig"
      objWinInfo.CtrlGetInfo(grdssDatos(0).Columns(4)).strSQL = sql
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(0).Columns(4)))
  End Select

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Select Case strFormName
    Case objSust.strName
      If desigTiTec <> "" Then
        desigTiTec = ""
        Call pCargarDropDownTec
      End If
  End Select
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Select Case strFormName
    Case objTecnica.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtTecnica(0).Text = fNextClave("AP12_CODTEC")
        objTecnica.rdoCursor("AP12_CODTEC") = txtTecnica(0).Text
      End If
   End Select
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing

End Sub
Sub pCargarDropDownTec()
Dim sql As String
    
  sql = "SELECT AP12_CodTec, AP12_Desig FROM AP1200"
  If desigTiTec <> "" Then
      sql = sql & " WHERE AP04_CodTiTec IN "
      sql = sql & "    (SELECT AP04_CodTiTec FROM AP0400"
      sql = sql & "     WHERE AP04_Desig = '" & desigTiTec & "')"
  End If
  sql = sql & " ORDER BY AP12_Desig"
  objWinInfo.CtrlGetInfo(grdssDatos(1).Columns(5)).strSQL = sql
  Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(1).Columns(5)))
  objWinInfo.WinPrepareScr

End Sub

' -----------------------------------------------
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabTecnica_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(tabTecnica, False, True)
    'fraTecnica.ForeColor = cwAZUL
    'fraDatos.ForeColor = cwGRIS
End Sub


' -----------------------------------------------
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Select Case btnButton.Index
    Case 2
        If Me.ActiveControl.Name = "grdssDatos(0)" Then
            grdssDatos(0).Columns(3).Text = txtTecnica(0).Text
        End If
  End Select
End Sub


' -----------------------------------------------
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' eventos del Grid
' -----------------------------------------------
Private Sub grdssTecnica_GotFocus()
  Call objWinInfo.CtrlGotFocus
  'fraTecnica.ForeColor = cwAZUL
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub grdssTecnica_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTecnica_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTecnica_Change()
    Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtTecnica_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub txtTecnica_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTecnica_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

