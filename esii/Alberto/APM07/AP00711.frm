VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_CentroPatologo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento del Personal"
   ClientHeight    =   7770
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11430
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7770
   ScaleWidth      =   11430
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   11430
      _ExtentX        =   20161
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraPatologo 
      Caption         =   "Pat�logos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3795
      Left            =   75
      TabIndex        =   29
      Top             =   3525
      Width           =   11265
      Begin TabDlg.SSTab tabPatologo 
         Height          =   3255
         Left            =   150
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   375
         Width           =   11025
         _ExtentX        =   19447
         _ExtentY        =   5741
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(26)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(25)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "tabDatos(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "chkPatologo"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtPatologo(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtPatologo(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtPatologo(11)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssPatologo"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP11_CodCtro"
            Height          =   285
            Index           =   11
            Left            =   1440
            TabIndex        =   56
            Tag             =   "Centro|C�digo del centro"
            Top             =   -120
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP15_CodCOL"
            Height          =   285
            Index           =   1
            Left            =   1500
            MaxLength       =   50
            TabIndex        =   13
            Tag             =   "N�mero de colegiado"
            Top             =   450
            Width           =   1350
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP15_CodPat"
            Height          =   285
            Index           =   0
            Left            =   2775
            TabIndex        =   12
            Tag             =   "Pat�logo|C�digo del pat�logo"
            Top             =   -30
            Visible         =   0   'False
            Width           =   570
         End
         Begin VB.CheckBox chkPatologo 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP15_IndActiva"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   3525
            TabIndex        =   14
            Tag             =   "Activo|Registro activo"
            Top             =   450
            Width           =   990
         End
         Begin SSDataWidgets_B.SSDBGrid grdssPatologo 
            Height          =   2790
            Left            =   -74850
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   225
            Width           =   10305
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18177
            _ExtentY        =   4921
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tabDatos 
            Height          =   2160
            HelpContextID   =   90001
            Index           =   1
            Left            =   120
            TabIndex        =   44
            TabStop         =   0   'False
            Top             =   900
            Width           =   10335
            _ExtentX        =   18230
            _ExtentY        =   3810
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BackColor       =   12632256
            TabCaption(0)   =   "Datos Personales"
            TabPicture(0)   =   "AP00711.frx":0000
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(22)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(21)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(20)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(19)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(18)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtPatologo(2)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtPatologo(3)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtPatologo(4)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtPatologo(5)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtPatologo(6)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).ControlCount=   10
            TabCaption(1)   =   "Contacto"
            TabPicture(1)   =   "AP00711.frx":001C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtPatologo(9)"
            Tab(1).Control(1)=   "txtPatologo(8)"
            Tab(1).Control(2)=   "txtPatologo(7)"
            Tab(1).Control(3)=   "txtPatologo(10)"
            Tab(1).Control(4)=   "lblLabel1(27)"
            Tab(1).Control(5)=   "lblLabel1(28)"
            Tab(1).Control(6)=   "lblLabel1(29)"
            Tab(1).Control(7)=   "lblLabel1(30)"
            Tab(1).ControlCount=   8
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_Firma"
               Height          =   285
               Index           =   6
               Left            =   7350
               MaxLength       =   50
               TabIndex        =   19
               Tag             =   "Firma|Texto de firma"
               Top             =   1050
               Width           =   2355
            End
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_Ref"
               Height          =   285
               Index           =   5
               Left            =   7350
               MaxLength       =   50
               TabIndex        =   18
               Tag             =   "Referencia|Referencia del pat�logo"
               Top             =   600
               Width           =   1005
            End
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_Ap2"
               Height          =   285
               Index           =   4
               Left            =   1350
               MaxLength       =   50
               TabIndex        =   17
               Tag             =   "Apellido 2"
               Top             =   1500
               Width           =   4230
            End
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_Ap1"
               Height          =   285
               Index           =   3
               Left            =   1350
               MaxLength       =   50
               TabIndex        =   16
               Tag             =   "Apellido 1"
               Top             =   1050
               Width           =   4230
            End
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_Nom"
               Height          =   285
               Index           =   2
               Left            =   1350
               MaxLength       =   50
               TabIndex        =   15
               Tag             =   "Nombre|Nombre del pat�logo"
               Top             =   600
               Width           =   4230
            End
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_EMail"
               Height          =   285
               Index           =   9
               Left            =   -73875
               MaxLength       =   50
               TabIndex        =   22
               Tag             =   "E-Mail"
               Top             =   1425
               Width           =   1530
            End
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_Fax"
               Height          =   285
               Index           =   8
               Left            =   -73875
               MaxLength       =   50
               TabIndex        =   21
               Tag             =   "Fax"
               Top             =   975
               Width           =   1530
            End
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_Tlfno"
               Height          =   285
               Index           =   7
               Left            =   -73875
               MaxLength       =   50
               TabIndex        =   20
               Tag             =   "Tfno|Tel�fono de contacto"
               Top             =   525
               Width           =   1530
            End
            Begin VB.TextBox txtPatologo 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP15_Observ"
               Height          =   705
               Index           =   10
               Left            =   -71700
               MaxLength       =   50
               TabIndex        =   23
               Tag             =   "Observaciones|Observaciones al pat�logo"
               Top             =   975
               Width           =   6780
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Texto de firma:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   5850
               TabIndex        =   53
               Top             =   1050
               Width           =   1380
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Referencia:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   6075
               TabIndex        =   52
               Top             =   600
               Width           =   1230
            End
            Begin VB.Label lblLabel1 
               Caption         =   "2� Apellido:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   150
               TabIndex        =   51
               Top             =   1500
               Width           =   1080
            End
            Begin VB.Label lblLabel1 
               Caption         =   "1� Apellido:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   21
               Left            =   150
               TabIndex        =   50
               Top             =   1050
               Width           =   1005
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   450
               TabIndex        =   49
               Top             =   600
               Width           =   780
            End
            Begin VB.Label lblLabel1 
               Caption         =   "e-mail:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   -74700
               TabIndex        =   48
               Top             =   1425
               Width           =   630
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fax:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   -74475
               TabIndex        =   47
               Top             =   975
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tfno:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   -74550
               TabIndex        =   46
               Top             =   525
               Width           =   555
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   -71700
               TabIndex        =   45
               Top             =   675
               Width           =   1455
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Colegiado:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   25
            Left            =   150
            TabIndex        =   55
            Top             =   450
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   26
            Left            =   2025
            TabIndex        =   54
            Top             =   -30
            Visible         =   0   'False
            Width           =   705
         End
      End
   End
   Begin VB.Frame fraCentro 
      Caption         =   "Centro"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   90
      TabIndex        =   26
      Top             =   450
      Width           =   11265
      Begin TabDlg.SSTab tabCentro 
         Height          =   2400
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   300
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   4233
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(16)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtCentro(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtCentro(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtCentro(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "tabDatos(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssCentro"
         Tab(1).ControlCount=   1
         Begin TabDlg.SSTab tabDatos 
            Height          =   1485
            HelpContextID   =   90001
            Index           =   0
            Left            =   120
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   750
            Width           =   10410
            _ExtentX        =   18362
            _ExtentY        =   2619
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BackColor       =   12632256
            TabCaption(0)   =   "Direcci�n"
            TabPicture(0)   =   "AP00711.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(11)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(10)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(9)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(8)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(7)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "cbossCentro(0)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtCentro(6)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtCentro(5)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtCentro(4)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtCentro(3)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).ControlCount=   10
            TabCaption(1)   =   "Contacto"
            TabPicture(1)   =   "AP00711.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtCentro(8)"
            Tab(1).Control(1)=   "txtCentro(9)"
            Tab(1).Control(2)=   "txtCentro(10)"
            Tab(1).Control(3)=   "txtCentro(11)"
            Tab(1).Control(4)=   "lblLabel1(12)"
            Tab(1).Control(5)=   "lblLabel1(13)"
            Tab(1).Control(6)=   "lblLabel1(14)"
            Tab(1).Control(7)=   "lblLabel1(15)"
            Tab(1).ControlCount=   8
            Begin VB.TextBox txtCentro 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP11_Calle"
               Height          =   285
               Index           =   3
               Left            =   1050
               MaxLength       =   50
               TabIndex        =   3
               Tag             =   "Calle|Calle del centro externo"
               Top             =   525
               Width           =   7455
            End
            Begin VB.TextBox txtCentro 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP11_Pobl"
               Height          =   285
               Index           =   4
               Left            =   1050
               MaxLength       =   50
               TabIndex        =   5
               Tag             =   "Poblaci�n"
               Top             =   975
               Width           =   3555
            End
            Begin VB.TextBox txtCentro 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP11_Prov"
               Height          =   285
               Index           =   5
               Left            =   5625
               MaxLength       =   50
               TabIndex        =   6
               Tag             =   "Provincia"
               Top             =   975
               Width           =   1830
            End
            Begin VB.TextBox txtCentro 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP11_CP"
               Height          =   285
               Index           =   6
               Left            =   9150
               MaxLength       =   50
               TabIndex        =   4
               Tag             =   "C.P.|C�digo postal"
               Top             =   525
               Width           =   1005
            End
            Begin VB.TextBox txtCentro 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP11_Tlfno"
               Height          =   285
               Index           =   8
               Left            =   -73875
               MaxLength       =   50
               TabIndex        =   8
               Tag             =   "Tel�fono|Tel�fono de contacto"
               Top             =   525
               Width           =   1530
            End
            Begin VB.TextBox txtCentro 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP11_Contacto"
               Height          =   285
               Index           =   9
               Left            =   -73875
               MaxLength       =   50
               TabIndex        =   11
               Tag             =   "Persona de contacto"
               Top             =   975
               Width           =   8055
            End
            Begin VB.TextBox txtCentro 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP11_Fax"
               Height          =   285
               Index           =   10
               Left            =   -71025
               MaxLength       =   50
               TabIndex        =   9
               Tag             =   "Fax|Fax del centro"
               Top             =   525
               Width           =   1830
            End
            Begin VB.TextBox txtCentro 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AP11_EMail"
               Height          =   285
               Index           =   11
               Left            =   -67650
               MaxLength       =   50
               TabIndex        =   10
               Tag             =   "e-mail|Correo electr�nico del centro"
               Top             =   525
               Width           =   1830
            End
            Begin SSDataWidgets_B.SSDBCombo cbossCentro 
               DataField       =   "CI19CodPais"
               Height          =   315
               Index           =   0
               Left            =   8100
               TabIndex        =   7
               Tag             =   "Pais"
               Top             =   975
               Width           =   2115
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3731
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Calle:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   450
               TabIndex        =   41
               Top             =   525
               Width           =   555
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Poblaci�n:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   75
               TabIndex        =   40
               Top             =   975
               Width           =   930
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Provincia:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   4725
               TabIndex        =   39
               Top             =   975
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "CP:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   8700
               TabIndex        =   38
               Top             =   525
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Pais:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   7575
               TabIndex        =   37
               Top             =   975
               Width           =   555
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tfno:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -74475
               TabIndex        =   36
               Top             =   525
               Width           =   555
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Contacto:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   -74850
               TabIndex        =   35
               Top             =   975
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fax:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   -71625
               TabIndex        =   34
               Top             =   525
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               Caption         =   "e-mail:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   -68325
               TabIndex        =   33
               Top             =   525
               Width           =   630
            End
         End
         Begin VB.TextBox txtCentro 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP11_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   5865
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del centro"
            Top             =   225
            Width           =   4590
         End
         Begin VB.TextBox txtCentro 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP11_CodCtro"
            Height          =   285
            Index           =   0
            Left            =   3510
            TabIndex        =   0
            Tag             =   "Centro|C�digo del centro"
            Top             =   600
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtCentro 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP11_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1350
            MaxLength       =   50
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n del centro"
            Top             =   225
            Width           =   2925
         End
         Begin SSDataWidgets_B.SSDBGrid grdssCentro 
            Height          =   2070
            Left            =   -74850
            TabIndex        =   25
            Top             =   150
            Width           =   10350
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18256
            _ExtentY        =   3651
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   4695
            TabIndex        =   43
            Top             =   240
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   150
            TabIndex        =   42
            Top             =   240
            Width           =   1110
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   30
      Top             =   7485
      Width           =   11430
      _ExtentX        =   20161
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_CentroPatologo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objCentro As New clsCWForm
Dim objPatologo As New clsCWForm


Private Sub cbossCentro_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
    Call objWinInfo.CtrlLostFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objCentro
    .strName = "Centro"
    Set .objFormContainer = fraCentro
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabCentro
    Set .grdGrid = grdssCentro
    .strTable = "AP1100"
    .intAllowance = cwAllowAll
    .blnAskPrimary = False
    
    Call .FormAddOrderField("AP11_Desig", cwAscending)
    Call .objPrinter.Add("AP1101.rpt", "Listado de centros/pat�logos externos")
    
    Call .FormCreateFilterWhere(.strTable, "Centros externos")
    Call .FormAddFilterWhere(.strTable, "AP11_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(.strTable, "AP11_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(.strTable, "AP11_POBL", "Poblaci�n", cwString)
    Call .FormAddFilterWhere(.strTable, "AP11_PROV", "Provincia", cwString)
    Call .FormAddFilterWhere(.strTable, "AP11_Tfno", "Tfno.", cwString)
    Call .FormAddFilterWhere(.strTable, "AP11_Fax", "Fax", cwString)
    Call .FormAddFilterWhere(.strTable, "AP11_EMail", "e-Mail", cwString)
    Call .FormAddFilterWhere(.strTable, "AP11_Contacto", "Contacto", cwString)

    Call .FormAddFilterOrder(.strTable, "AP33_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(.strTable, "AP33_DESCRIP", "Descripci�n")
  
  End With
  
  With objPatologo
    .strName = "Patologo"
    Set .objFormContainer = fraPatologo
    Set .objFatherContainer = fraCentro
    Set .tabMainTab = tabPatologo
    Set .grdGrid = grdssPatologo
    .strTable = "AP1500"
    .blnAskPrimary = False
    .intAllowance = cwAllowAll

    Call .FormAddOrderField("AP15_CodCol", cwAscending)
    Call .FormAddRelation("AP11_CODCtro", txtCentro(0))
    
    Call .FormCreateFilterWhere(.strTable, "Pat�logos externos")
    Call .FormAddFilterWhere(.strTable, "AP15_CodCol", "N� Colegiado", cwNumeric)
    Call .FormAddFilterWhere(.strTable, "AP15_Nom", "Nombre", cwString)
    Call .FormAddFilterWhere(.strTable, "AP15_Ap1", "Apellido 1", cwString)
    Call .FormAddFilterWhere(.strTable, "AP15_Ap2", "Apellido 2", cwString)
    Call .FormAddFilterWhere(.strTable, "AP15_Ref", "Referencia", cwString)
    Call .FormAddFilterWhere(.strTable, "AP15_Firma", "Texto de firma", cwString)
    Call .FormAddFilterWhere(.strTable, "AP15_Tfno", "Tfno.", cwString)
    Call .FormAddFilterWhere(.strTable, "AP15_Fax", "Fax", cwString)

    Call .FormAddFilterOrder(.strTable, "AP15_CodCol", "N� Colegiado")
    Call .FormAddFilterOrder(.strTable, "AP15_Nom", "Nombre")
    Call .FormAddFilterOrder(.strTable, "AP15_AP1", "Apellido 1")
    Call .FormAddFilterOrder(.strTable, "AP15_AP2", "Apellido 2")
  
  End With
   
  With objWinInfo
    Call .FormAddInfo(objCentro, cwFormDetail)
    Call .FormAddInfo(objPatologo, cwFormDetail)
   
    Call .FormCreateInfo(objCentro)
    .CtrlGetInfo(cbossCentro(0)).strSQL = "SELECT CI19CodPais, CI19DesPais FROM CI1900 ORDER BY CI19DesPais"
    
    .CtrlGetInfo(txtCentro(1)).blnInFind = True
    .CtrlGetInfo(txtCentro(2)).blnInFind = True
    .CtrlGetInfo(txtCentro(3)).blnInFind = True
    .CtrlGetInfo(txtCentro(4)).blnInFind = True
    .CtrlGetInfo(txtCentro(5)).blnInFind = True
    .CtrlGetInfo(txtCentro(6)).blnInFind = True
    .CtrlGetInfo(txtCentro(8)).blnInFind = True
    .CtrlGetInfo(txtCentro(9)).blnInFind = True
    .CtrlGetInfo(txtCentro(10)).blnInFind = True
    .CtrlGetInfo(txtCentro(11)).blnInFind = True
    .CtrlGetInfo(cbossCentro(0)).blnInFind = True

    .CtrlGetInfo(txtPatologo(1)).blnInFind = True
    .CtrlGetInfo(txtPatologo(2)).blnInFind = True
    .CtrlGetInfo(txtPatologo(3)).blnInFind = True
    .CtrlGetInfo(txtPatologo(4)).blnInFind = True
    .CtrlGetInfo(txtPatologo(5)).blnInFind = True
    .CtrlGetInfo(txtPatologo(6)).blnInFind = True
    .CtrlGetInfo(txtPatologo(8)).blnInFind = True
    .CtrlGetInfo(txtPatologo(9)).blnInFind = True
    .CtrlGetInfo(txtPatologo(10)).blnInFind = True
    .CtrlGetInfo(txtPatologo(11)).blnInFind = True
    
    .CtrlGetInfo(txtCentro(0)).blnValidate = False
    .CtrlGetInfo(txtPatologo(0)).blnValidate = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdssCentro.Columns(1).Visible = False
  grdssPatologo.Columns(1).Visible = False
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Select Case strFormName
    Case objCentro.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtCentro(0).Text = fNextClave("AP11_CodCtro")
        objCentro.rdoCursor("AP11_CodCtro") = txtCentro(0).Text
      End If
    Case objPatologo.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtPatologo(0).Text = fNextClave("AP15_CodPat")
        objPatologo.rdoCursor("AP15_CodPat") = txtPatologo(0).Text
      End If
  End Select

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabTab1_DblClick(Index As Integer)

End Sub

Private Sub tabDatos_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  
  If Index = 0 Then
    Call objWinInfo.FormChangeActive(tabCentro, False, True)
  Else
    Call objWinInfo.FormChangeActive(tabPatologo, False, True)
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index = 2 Then txtPatologo(11) = txtCentro(0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssCentro_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssCentro_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssCentro_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssCentro_Change()
  Call objWinInfo.CtrlDataChange
End Sub

'=======================================================
Private Sub grdssPatologo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssPatologo_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPatologo_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssPatologo_Change()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabCentro_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabCentro, False, True)
End Sub

'================================================================
Private Sub tabPatologo_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabPatologo, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraCentro_Click()
  Call objWinInfo.FormChangeActive(fraCentro, False, True)
End Sub

'==================================================================
Private Sub fraPatologo_Click()
  Call objWinInfo.FormChangeActive(fraPatologo, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
 Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkPatologo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkPatologo_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkPatologo_Click()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------

Private Sub cbossCentro_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub cbossCentro_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossCentro_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossCentro_CloseUp(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtCentro_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtCentro_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtCentro_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

'==================================================================
Private Sub txtPatologo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtPatologo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtPatologo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

