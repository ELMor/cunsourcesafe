VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_Decalcificador 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Decalcificadores"
   ClientHeight    =   3795
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9795
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3795
   ScaleWidth      =   9795
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9795
      _ExtentX        =   17277
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraDecal 
      Caption         =   "Decalcificadores"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2820
      Left            =   90
      TabIndex        =   6
      Top             =   525
      Width           =   9465
      Begin TabDlg.SSTab tabDecal 
         Height          =   2250
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   3969
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "chkDecal"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtDecal(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtDecal(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtDecal(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssDecal"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtDecal 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP22_CODDECAL"
            Height          =   285
            Index           =   0
            Left            =   5850
            TabIndex        =   0
            Tag             =   "C�digo|C�digo del Decalcificador"
            Top             =   375
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtDecal 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP22_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1875
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Nombre abreviado del Decalcificador"
            Top             =   450
            Width           =   3375
         End
         Begin VB.TextBox txtDecal 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP22_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1875
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del Decalcificador"
            Top             =   1050
            Width           =   5835
         End
         Begin VB.CheckBox chkDecal 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP22_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   615
            TabIndex        =   3
            Tag             =   "Activa|Activa"
            Top             =   1560
            Width           =   1485
         End
         Begin SSDataWidgets_B.SSDBGrid grdssDecal 
            Height          =   1995
            Left            =   -74910
            TabIndex        =   5
            Top             =   90
            Width           =   8700
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15346
            _ExtentY        =   3519
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   615
            TabIndex        =   10
            Top             =   450
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   525
            TabIndex        =   9
            Top             =   1050
            Width           =   1200
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   3510
      Width           =   9795
      _ExtentX        =   17277
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Decalcificador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm


' -----------------------------------------------
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    Set .objFormContainer = fraDecal
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabDecal
    Set .grdGrid = grdssDecal
    .strTable = "AP2200"
    .blnAskPrimary = False
    Call .FormAddOrderField("AP22_Desig", cwAscending)
    Call .objPrinter.Add("AP1001.rpt", "Listado de decalcificadores")
  
    Call .FormCreateFilterWhere(.strTable, "Decacificadores")
    Call .FormAddFilterWhere(.strTable, "AP22_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(.strTable, "AP22_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(.strTable, "AP22_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(.strTable, "AP22_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(.strTable, "AP22_DESCRIP", "Descripci�n")
  
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)

    Call .FormCreateInfo(objMasterInfo)
    .CtrlGetInfo(txtDecal(1)).blnInFind = True
    .CtrlGetInfo(txtDecal(2)).blnInFind = True
    .CtrlGetInfo(chkDecal).blnInFind = True
    
    .CtrlGetInfo(txtDecal(0)).blnValidate = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdssDecal.Columns(1).Visible = False
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Select Case strFormName
    Case objMasterInfo.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtDecal(0).Text = fNextClave("AP22_CODDECAL")
        objMasterInfo.rdoCursor("AP22_CODDECAL") = txtDecal(0).Text
      End If
  End Select

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing


End Sub

' -----------------------------------------------
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' eventos del Grid
' -----------------------------------------------
Private Sub grdssDecal_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssDecal_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssDecal_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssDecal_Change()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssOrganos_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssOrganos_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssOrganos_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssOrganos_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' eventos del tab
' -----------------------------------------------
Private Sub tabDecal_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabDecal, False, True)
End Sub



' -----------------------------------------------
' eventos del frame
' -----------------------------------------------
Private Sub fraDecal_Click()
  Call objWinInfo.FormChangeActive(fraDecal, False, True)
End Sub

' -----------------------------------------------
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

' -----------------------------------------------
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkDecal_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkDecal_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkDecal_Click()
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtDecal_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtDecal_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtDecal_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

