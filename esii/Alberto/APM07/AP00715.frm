VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_Personal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento del Personal"
   ClientHeight    =   8055
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11175
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8055
   ScaleWidth      =   11175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraCat 
      Caption         =   "Categor�a"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1770
      Left            =   75
      TabIndex        =   2
      Top             =   525
      Width           =   10890
      Begin TabDlg.SSTab tabCat 
         Height          =   1380
         Left            =   150
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   300
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   2434
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AP00715.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtCat(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtCat(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AP00715.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssCat(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtCat 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP07_CodTiPers"
            Height          =   285
            Index           =   0
            Left            =   7050
            MaxLength       =   50
            TabIndex        =   1
            Tag             =   "C�digo|C�digo del puesto"
            Top             =   525
            Visible         =   0   'False
            Width           =   480
         End
         Begin VB.TextBox txtCat 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP07_Desig"
            Height          =   285
            Index           =   1
            Left            =   1650
            MaxLength       =   50
            TabIndex        =   0
            Tag             =   "Puesto|Descripci�n del puesto"
            Top             =   300
            Width           =   3180
         End
         Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
            Height          =   1965
            Left            =   -74850
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   75
            Width           =   10005
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17648
            _ExtentY        =   3466
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdssCat 
            Height          =   1215
            Index           =   0
            Left            =   -74925
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   75
            Width           =   10005
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17648
            _ExtentY        =   2143
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Categor�a:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   525
            TabIndex        =   9
            Top             =   300
            Width           =   1155
         End
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2790
      Left            =   75
      TabIndex        =   6
      Top             =   4875
      Width           =   10890
      Begin TabDlg.SSTab tabDatos 
         Height          =   2340
         Left            =   150
         TabIndex        =   12
         Top             =   300
         Width           =   10065
         _ExtentX        =   17754
         _ExtentY        =   4128
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "�rganos asociados"
         TabPicture(0)   =   "AP00715.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraOrg"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Pat�logo asociado"
         TabPicture(1)   =   "AP00715.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).ControlCount=   0
         Begin VB.Frame fraOrg 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   1890
            Left            =   225
            TabIndex        =   13
            Top             =   375
            Width           =   9690
            Begin SSDataWidgets_B.SSDBGrid grdssOrg 
               Height          =   1815
               Index           =   0
               Left            =   75
               TabIndex        =   14
               Top             =   75
               Width           =   9615
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GroupHeaders    =   0   'False
               GroupHeadLines  =   0
               Col.Count       =   0
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   16960
               _ExtentY        =   3201
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraPersonal 
      Caption         =   "Personal"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2520
      Left            =   75
      TabIndex        =   3
      Top             =   2325
      Width           =   10890
      Begin SSDataWidgets_B.SSDBGrid grdssPersonal 
         Height          =   2055
         Index           =   0
         Left            =   225
         TabIndex        =   11
         Top             =   300
         Width           =   10545
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18600
         _ExtentY        =   3625
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   7770
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Personal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objOrg As New clsCWForm
Dim objPatAsoc As New clsCWForm
Dim objPersonal As New clsCWForm
Dim objCat As New clsCWForm
Dim DesigTiOrg As String

Sub HabilitarCampos()
'  Select Case txtTipo(0)
'  Case 1
'    txtPatologo(1).Enabled = True
'    cbossPersonal.Enabled = False
'  Case 2
'    txtPatologo(1).Enabled = False
'    cbossPersonal.Enabled = True
'  Case Else
'    cbossPersonal.Enabled = False
'    txtPatologo(1).Enabled = False
'  End Select

End Sub
Private Sub cbossTipoTecnica_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossTipoTecnica_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossTipoTecnica_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDatos.ForeColor = cwGRIS
End Sub


Private Sub cbossPersonal_Click()
'  Call objWinInfo.CtrlDataChange
  Call objWinInfo.CtrlLostFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim strKey As String
Dim sql As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objCat
    .strName = "Cat"
    Set .objFormContainer = fraCat
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabCat
    Set .grdGrid = grdssCat(0)
    .intAllowance = cwAllowReadOnly
    Call .objPrinter.Add("AP1501.rpt", "Listado de categor�as/personal")
    Call .objPrinter.Add("AP1502.rpt", "Listado de personal/organos")
    .strTable = "AP0700"
    Call .FormAddOrderField("AP07_Desig", cwAscending)
    
    strKey = .strTable
    Call .FormCreateFilterWhere(strKey, "Categor�as")
    Call .FormAddFilterWhere(strKey, "AP07_Desig", "Categor�a", cwString)
    Call .FormAddFilterOrder(strKey, "AP07_Desig", "Categor�a")
  End With
   
  With objPersonal
    .strName = "Personal"
    Set .objFormContainer = fraPersonal
    Set .objFatherContainer = fraCat
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssPersonal(0)
    .intAllowance = cwAllowAll
    .strTable = "AP0800"
    strKey = .strTable
    Call .FormAddRelation("AP07_CodTiPers", txtCat(0))
  End With

'  With objOrg
'    .strName = "Organo"
'    Set .objFormContainer = fraOrg
'    Set .objFatherContainer = fraPersonal
'    Set .tabMainTab = Nothing
'    Set .grdGrid = grdssOrg(0)
'    .intAllowance = cwAllowAll
'    .strTable = "AP2300"
'    strKey = .strTable
'    Call .FormAddRelation("SG02Cod", grdssPersonal(0).Columns("Personal"))
'  End With
'
  With objWinInfo
    Call .FormAddInfo(objCat, cwFormDetail)
    Call .FormAddInfo(objPersonal, cwFormMultiLine)
    Call .GridAddColumn(objPersonal, "Tipo", "AP07_CodTiPers", cwNumeric, 4)
    Call .GridAddColumn(objPersonal, "Personal", "SG02Cod", cwNumeric, 4)
    Call .GridAddColumn(objPersonal, "Nombre", "", cwString, 15)
    Call .GridAddColumn(objPersonal, "Apellido 1", "", cwString, 15)
    Call .GridAddColumn(objPersonal, "Apellido 2", "", cwString, 15)
    
'    Call .FormAddInfo(objOrg, cwFormMultiLine)
'    Call .GridAddColumn(objOrg, "Personal", "SG02Cod", cwNumeric, 4)
'    Call .GridAddColumn(objOrg, "Muestra", "PR04CodMuestra", cwNumeric, 4)
    
    Call .FormCreateInfo(objCat)
    Call .FormChangeColor(objCat)
    .CtrlGetInfo(txtCat(0)).blnInGrid = False
    .CtrlGetInfo(grdssPersonal(0).Columns("Personal")).strSQL = "SELECT SG02Cod, " _
        & "NVL(SG02txtFirma, SG02APE1||' '||SG02APE2||', '||SG02NOM) FROM SG0200 " _
        & "WHERE SG02Cod IN (" _
        & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & ") ORDER BY SG02txtFirma"
    
    sql = "SELECT SG02Nom, SG02Ape1, SG02Ape2 FROM SG0200 WHERE SG02Cod = ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdssPersonal(0).Columns("Personal")), "SG02Cod", sql)
    Call .CtrlAddLinked(.CtrlGetInfo(grdssPersonal(0).Columns("Personal")), grdssPersonal(0).Columns("Nombre"), "SG02Nom")
    Call .CtrlAddLinked(.CtrlGetInfo(grdssPersonal(0).Columns("Personal")), grdssPersonal(0).Columns("Apellido 1"), "SG02Ape1")
    Call .CtrlAddLinked(.CtrlGetInfo(grdssPersonal(0).Columns("Personal")), grdssPersonal(0).Columns("Apellido 2"), "SG02Ape2")

'    .CtrlGetInfo(cbossPersonal).strSQL = "SELECT SG02Cod, NVL(SG02txtFirma, SG02APE1||' '||SG02APE2||', '||SG02NOM) FROM " _
'        & "SG0200 WHERE AD30CodCategoria = " & cPat & " AND SG02Cod IN (" _
'        & "SELECT SG02Cod FROM AD0300 WHERE AD02CodDpto = " & cDptAP & ") ORDER BY SG02txtFirma"
'    .CtrlGetInfo(grdssOrgano(0).Columns(4)).strSQL = "SELECT PR49CodTipMuestr, PR49DesTipMuestr FROM PR4900 ORDER BY PR49DesTipMuestr"
'    .CtrlGetInfo(grdssOrgano(0).Columns(5)).strSQL = "SELECT PR24CodMuestra, PR24DesCorta FROM PR2400 ORDER BY PR24DesCorta"

    Call .WinRegister
    Call .WinStabilize
  End With
'  grdssOrgano(0).Columns(3).Visible = False
  grdssPersonal(0).Columns("Tipo").Visible = False
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraCat_Click()
  Call objWinInfo.FormChangeActive(fraCat, False, True)
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  If strFormName = objCat.strName Then
'    Select Case txtCat(0).Text
'      Case cPat
'        fraOrg.Enabled = True
'        fraPat.Enabled = False
'      Case cRes
'        fraOrg.Enabled = False
'        fraPat.Enabled = True
'      Case Else
'        fraOrg.Enabled = False
'        fraPat.Enabled = False
'    End Select
'  End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
'  Select Case strFormName
'    Case objOrganos.strName
'      If DesigTiOrg <> "" Then
'        DesigTiOrg = ""
'        Call pCargarDropDownOrg
'      End If
'  End Select

End Sub
Sub pCargarDropDownOrg()
'    Dim sql As String
'
'    sql = "SELECT PR24CodMuestra, PR24DesCorta FROM PR2400"
'    If DesigTiOrg <> "" Then
'        sql = sql & " WHERE PR49CodTipMuestr IN "
'        sql = sql & "    (SELECT PR49CodTipMuestr FROM PR4900"
'        sql = sql & "     WHERE PR49DesTipMuestr = '" & DesigTiOrg & "')"
'    End If
'    sql = sql & " ORDER BY PR24DesCorta"
'    objWinInfo.CtrlGetInfo(grdssOrgano(0).Columns(5)).strSQL = sql
'    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssOrgano(0).Columns(5)))
'    objWinInfo.WinPrepareScr

End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'    Select Case strFormName
'        Case objTipo.strName
'           If objWinInfo.intWinStatus = cwModeSingleAddRest Then
'                txtTipo(0).Text = fNextClave("AP07_CODTiPers")
'                objTipo.rdoCursor("AP07_CODTiPers") = txtTipo(0).Text
'            End If
'        Case objPersonal.strName
'           If objWinInfo.intWinStatus = cwModeSingleAddRest Then
'                txtPatologo(0).Text = fNextClave("AP08_CODPers")
'                objPersonal.rdoCursor("AP08_CODPers") = txtPatologo(0).Text
'            End If
'            If cbossPersonal.Text = "" Then
'
'            End If
'    End Select
'
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabTab1_DblClick(Index As Integer)

End Sub

Private Sub tabCat_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(tabCat, False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssTipo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTipo_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTipo_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTipo_Change()
  Call objWinInfo.CtrlDataChange
End Sub

'=======================================================
Private Sub grdssCat_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  Call HabilitarCampos
End Sub

Private Sub grdssCat_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssCat_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssCat_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssPersonal_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  Call HabilitarCampos
End Sub

Private Sub grdssPersonal_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPersonal_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssPersonal_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssOrgano_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssOrgano_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssOrgano_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
'    If grdssOrgano(0).Col = 5 Then
'        If grdssOrgano(0).Columns(1).Text = 6 Then
'            DesigTiOrg = grdssOrgano(0).Columns(4).Text
'            Call pCargarDropDownOrg
'        End If
'    End If

End Sub

Private Sub grdssOrgano_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabDatos_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
'  Call objWinInfo.FormChangeActive(tabDatos, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraOrg_Click()
'  Call objWinInfo.FormChangeActive(fraOrg, False, True)
End Sub
Private Sub fraPat_Click()
'  Call objWinInfo.FormChangeActive(fraPat, False, True)
End Sub

'==================================================================
Private Sub fraPersonal_Click()
  Call objWinInfo.FormChangeActive(fraPersonal, False, True)
  'Call HabilitarCampos
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkTipo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkTipo_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkTipo_Click()
  Call objWinInfo.CtrlDataChange
End Sub

'=================================================================
Private Sub chkPatologo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkPatologo_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkPatologo_Click()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cbossPersonal_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossPersonal_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossPersonal_CloseUp()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtTipo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtTipo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTipo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

'==================================================================
Private Sub txtPatologo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtPatologo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtPatologo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtCat_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtCat_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtCat_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

