VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' **********************************************************************************


' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el noAPre del archivo en disco
'Const APRepProblemas            As String = "AP0001"
'Const APRepMicroMuestra         As String = "AP0002"
'Const APRepAlmacenColonias      As String = "AP0003"
'Const APRepAislamientos         As String = "AP0004"
'Const APRepMicroAntibiograma    As String = "AP0005"
'Const APRepMicroTecnicas        As String = "AP0006"
'Const APRepBionumeros           As String = "AP0007"
'Const APRepDesglosePeticiones   As String = "AP0008"
'Const APRepMicroEdades          As String = "AP0009"


'Constantes pata los HelpContextID de las ventanas
'Const AGHelpIDTipoRest             As Integer = 26
'Const AGHelpIDTipoRecursos         As Integer = 1
'Const AGHelpIDCalendarios          As Integer = 3
'Const AGHelpIDRecursos             As Integer = 6
'Const AGHelpIDRestriccionesR       As Integer = 15
'Const AGHelpIDRestriccionesF       As Integer = 20
'Const AGHelpIDPerfiles             As Integer = 9
'Const AGHelpIDFranjas              As Integer = 18
'Const AGHelpIDIncidencias          As Integer = 0
'Const AGHelpIDAgendaRecurso        As Integer = 24
'Const AGHelpIDDietarioRecurso      As Integer = 25
'Const AGHelpIDMotivosIncidencia    As Integer = 29


' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

'  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case APWinMantenimiento
      Call frmMantenimiento.Show(vbModal)
      Set frmMantenimiento = Nothing
    Case APWinA_Almacen
      Call frmA_Almacen.Show(vbModal)
      Set frmA_Almacen = Nothing
    Case APWinA_Almacenamiento
      Call frmA_Almacenamiento.Show(vbModal)
      Set frmA_Almacenamiento = Nothing
    Case APWinA_Carpeta
      Call frmA_Carpeta.Show(vbModal)
      Set frmA_Carpeta = Nothing
    Case APWinA_CentroPatologo
      Call frmA_CentroPatologo.Show(vbModal)
      Set frmA_CentroPatologo = Nothing
    Case APWinA_Decalcificador
      Call frmA_Decalcificador.Show(vbModal)
      Set frmA_Decalcificador = Nothing
'    Case APWinA_DiagPresuncion
'      Call frmA_DiagPresuncion.Show(vbModal)
'      Set frmA_DiagPresuncion = Nothing
    Case APWinA_Fijador
      Call frmA_Fijador.Show(vbModal)
      Set frmA_Fijador = Nothing
    Case APWinA_Interes
      Call frmA_Interes.Show(vbModal)
      Set frmA_Interes = Nothing
    Case APWinA_Organo
      Call frmA_Organo.Show(vbModal)
      Set frmA_Organo = Nothing
    Case APWinA_PerfilDiag
      Call frmA_PerfilDiag.Show(vbModal)
      Set frmA_PerfilDiag = Nothing
    Case APWinA_PerfilTecnica
      Call frmA_PerfilTecnica.Show(vbModal)
      Set frmA_PerfilTecnica = Nothing
    Case APWinA_Personal
      Call frmA_Personal.Show(vbModal)
      Set frmA_Personal = Nothing
    Case APWinA_Problema
      Call frmA_Problema.Show(vbModal)
      Set frmA_Problema = Nothing
    Case APWinA_Snomed
      Call frmA_Snomed.Show(vbModal)
      Set frmA_Snomed = Nothing
    Case APWinA_Tecnica
      Call frmA_Tecnica.Show(vbModal)
      Set frmA_Tecnica = Nothing
    Case APWinA_TipoBloque
      Call frmA_TipoBloque.Show(vbModal)
      Set frmA_TipoBloque = Nothing
    Case APWinA_TipoPorta
      Call frmA_TipoPorta.Show(vbModal)
      Set frmA_TipoPorta = Nothing
    Case APWinA_TipoPrueba
      Call frmA_TipoPrueba.Show(vbModal)
      Set frmA_TipoPrueba = Nothing
    Case APWinA_TipoSnomed
      Call frmA_TipoSnomed.Show(vbModal)
      Set frmA_TipoSnomed = Nothing
    Case APWinA_TipoTecnica
      Call frmA_TipoTecnica.Show(vbModal)
      Set frmA_TipoTecnica = Nothing
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  
'  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n� de procesos que se definan
ReDim aProcess(1 To 56, 1 To 4) As Variant
  
  aProcess(1, 1) = APWinA_Almacen
  aProcess(1, 2) = "Mto. de Almacenes"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = APWinA_Almacenamiento
  aProcess(2, 2) = "Mto. Motivos Almacenamiento Muestras"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = APWinA_Carpeta
  aProcess(3, 2) = "Mto. de Carpetas"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = APWinA_CentroPatologo
  aProcess(4, 2) = "Mto. de Pat�logos Externos"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = APWinA_Decalcificador
  aProcess(5, 2) = "Mto. de Decalcificadores"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = APWinA_Fijador
  aProcess(6, 2) = "Mto. de Fijadores"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = APWinA_Interes
  aProcess(7, 2) = "Mto. de Motivos de Inter�s"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = APWinA_Organo
  aProcess(8, 2) = "Mto. de Muestras"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
      
  aProcess(9, 1) = APWinA_PerfilDiag
  aProcess(9, 2) = "Mto. de Perfiles de Diagn�sticos"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = APWinA_PerfilTecnica
  aProcess(10, 2) = "Mto. de Perfiles de T�cnicas"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow

  aProcess(11, 1) = APWinA_Personal
  aProcess(11, 2) = "Mto. de Personal"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = APWinL_Personal1
  aProcess(12, 2) = "Listado de categor�as/personal"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeReport
  
  aProcess(13, 1) = APWinL_Personal2
  aProcess(13, 2) = "Listado de personal/�rgano"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeReport
  
  aProcess(14, 1) = APWinA_Problema
  aProcess(14, 2) = "Mto. de Problemas Detectados"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeWindow
  
  aProcess(15, 1) = APWinA_Snomed
  aProcess(15, 2) = "Mto. de Diagn�sticos SNOMED"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = APWinA_Tecnica
  aProcess(16, 2) = "Mto. de T�cnicas"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeWindow
  
  aProcess(17, 1) = APWinA_TipoBloque
  aProcess(17, 2) = "Mto. de Tipos de Bloques"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeWindow
  
  aProcess(18, 1) = APWinA_TipoPrueba
  aProcess(18, 2) = "Mto. de Tipos de Pruebas"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = APWinA_TipoPorta
  aProcess(19, 2) = "Mto. de Tipos de Portas"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = APWinA_TipoTecnica
  aProcess(20, 2) = "Mto. de Tipos de T�cnicas"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = APWinA_TipoSnomed
  aProcess(21, 2) = "Mto. de Tipos de SNOMED"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow
  
' Listados
  aProcess(22, 1) = APWinL_Almacen
  aProcess(22, 2) = "Listado de Almacenes"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeReport
  
  aProcess(23, 1) = APWinL_Almacenamiento
  aProcess(23, 2) = "Listado de Motivos de almacenamiento"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeReport
  
  aProcess(24, 1) = APWinL_Carpeta
  aProcess(24, 2) = "Listado de Carpetas"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeReport
  
  aProcess(25, 1) = APWinL_CentroPatologo
  aProcess(25, 2) = "Listado de Centro/pat�logo externo"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeReport
  
  aProcess(26, 1) = APWinL_Decalcificador
  aProcess(26, 2) = "Listado de decalcificadores"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeReport
  
  aProcess(27, 1) = APWinL_Fijador
  aProcess(27, 2) = "Listado de fijadores"
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeReport
  
  aProcess(28, 1) = APWinL_Interes
  aProcess(28, 2) = "Listado de motivos de inter�s"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeReport
  
  aProcess(29, 1) = APWinL_Organo1
  aProcess(29, 2) = "Listado de tipos de muestras/muestras"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeReport
      
  aProcess(30, 1) = APWinL_Organo2
  aProcess(30, 2) = "Listado de muestras/perfiles de t�cnicas"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeReport
      
  aProcess(31, 1) = APWinL_Organo3
  aProcess(31, 2) = "Listado de muestras/anal�tica"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeReport
      
  aProcess(32, 1) = APWinL_Organo4
  aProcess(32, 2) = "Listado de muestras/perfiles de diagn�sticos"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeReport
      
  aProcess(33, 1) = APWinL_PerfilDiag1
  aProcess(33, 2) = "Listado de perfiles de diagn�sticos"
  aProcess(33, 3) = False
  aProcess(33, 4) = cwTypeReport
  
  aProcess(34, 1) = APWinL_PerfilDiag2
  aProcess(34, 2) = "Listado de perfiles/diagn�sticos"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeReport
  
  aProcess(35, 1) = APWinL_PerfilDiag3
  aProcess(35, 2) = "Listado de perfiles/�rganos"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeReport
  
  aProcess(36, 1) = APWinL_PerfilTecnica1
  aProcess(36, 2) = "Listado de perfiles de t�cnicas"
  aProcess(36, 3) = False
  aProcess(36, 4) = cwTypeReport
  
  aProcess(37, 1) = APWinL_PerfilTecnica2
  aProcess(37, 2) = "Listado de perfiles/t�cnicas"
  aProcess(37, 3) = False
  aProcess(37, 4) = cwTypeReport
  
  aProcess(38, 1) = APWinL_PerfilTecnica3
  aProcess(38, 2) = "Listado de perfiles/�rganos"
  aProcess(38, 3) = False
  aProcess(38, 4) = cwTypeReport
  
  aProcess(39, 1) = APWinL_Problema
  aProcess(39, 2) = "Listado de tipos de problemas"
  aProcess(39, 3) = False
  aProcess(39, 4) = cwTypeReport
  
  aProcess(40, 1) = APWinL_Snomed
  aProcess(40, 2) = "Listado de diagn�sticos SNOMED"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeReport
  
  aProcess(41, 1) = APWinL_Tecnica1
  aProcess(41, 2) = "Listado de t�cnicas"
  aProcess(41, 3) = False
  aProcess(41, 4) = cwTypeReport
  
  aProcess(42, 1) = APWinL_Tecnica2
  aProcess(42, 2) = "Listado de tipos de t�cnicas/t�cnicas"
  aProcess(42, 3) = False
  aProcess(42, 4) = cwTypeReport
  
  aProcess(43, 1) = APWinL_Tecnica3
  aProcess(43, 2) = "Listado de perfiles/t�cnicas"
  aProcess(43, 3) = False
  aProcess(43, 4) = cwTypeReport
  
  aProcess(44, 1) = APWinL_TipoBloque1
  aProcess(44, 2) = "Listado de tipos de bloques"
  aProcess(44, 3) = False
  aProcess(44, 4) = cwTypeReport
  
  aProcess(45, 1) = APWinL_TipoBloque2
  aProcess(45, 2) = "Listado de tipos de bloques/t�cnicas"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeReport
  
  aProcess(46, 1) = APWinL_TipoPorta1
  aProcess(46, 2) = "Listado de tipos de porta"
  aProcess(46, 3) = False
  aProcess(46, 4) = cwTypeReport
  
  aProcess(47, 1) = APWinL_TipoPorta2
  aProcess(47, 2) = "Listado de tipos de porta/t�cnicas"
  aProcess(47, 3) = False
  aProcess(47, 4) = cwTypeReport
  
  aProcess(48, 1) = APWinL_TipoPrueba1
  aProcess(48, 2) = "Listado de tipos de prueba/pruebas"
  aProcess(48, 3) = False
  aProcess(48, 4) = cwTypeReport
  
  aProcess(49, 1) = APWinL_TipoPrueba2
  aProcess(49, 2) = "Listado de tipos de prueba/resultados"
  aProcess(49, 3) = False
  aProcess(49, 4) = cwTypeReport
  
  aProcess(50, 1) = APWinL_TipoPrueba3
  aProcess(50, 2) = "Listado de prueba no dadas de alta em AP"
  aProcess(50, 3) = False
  aProcess(50, 4) = cwTypeReport
  
  aProcess(51, 1) = APWinL_TipoSnomed
  aProcess(51, 2) = "Listado de tipos SNOMED"
  aProcess(51, 3) = False
  aProcess(51, 4) = cwTypeReport
  
  aProcess(52, 1) = APWinL_TipoTecnica1
  aProcess(52, 2) = "Listado de tipos de t�cnicas"
  aProcess(52, 3) = False
  aProcess(52, 4) = cwTypeReport
  
  aProcess(53, 1) = APWinL_TipoTecnica2
  aProcess(53, 2) = "Listado de tipos de t�cnicas/t�cnicas"
  aProcess(53, 3) = False
  aProcess(53, 4) = cwTypeReport
  
  aProcess(54, 1) = APWinL_TipoTecnica3
  aProcess(54, 2) = "Listado de tipos de t�cnicas/pruebas"
  aProcess(54, 3) = False
  aProcess(54, 4) = cwTypeReport
  
  aProcess(55, 1) = APWinL_TipoTecnica4
  aProcess(55, 2) = "Listado de tipos de t�cnicas/bloques"
  aProcess(55, 3) = False
  aProcess(55, 4) = cwTypeReport
  
  aProcess(56, 1) = APWinMantenimiento
  aProcess(56, 2) = "Mantenimiento de tablas"
  aProcess(56, 3) = True
  aProcess(56, 4) = cwTypeWindow
  
End Sub
