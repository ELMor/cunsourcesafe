VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_TipoTecnica 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Tipos de T�cnicas"
   ClientHeight    =   7155
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7155
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   0
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3750
      Left            =   180
      TabIndex        =   6
      Top             =   3000
      Width           =   9165
      Begin TabDlg.SSTab tabDatos 
         Height          =   3285
         HelpContextID   =   90001
         Left            =   225
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   375
         Width           =   8730
         _ExtentX        =   15399
         _ExtentY        =   5794
         _Version        =   327681
         Style           =   1
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   -2147483644
         TabCaption(0)   =   "T�cnicas"
         TabPicture(0)   =   "AP00705.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraTecnicas"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Pruebas"
         TabPicture(1)   =   "AP00705.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraPruebas"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Tipos de bloque"
         TabPicture(2)   =   "AP00705.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "fraTiBloq"
         Tab(2).ControlCount=   1
         Begin VB.Frame fraTiBloq 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2865
            Left            =   -74850
            TabIndex        =   21
            Top             =   375
            Width           =   8505
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2745
               Index           =   2
               Left            =   0
               TabIndex        =   22
               Top             =   75
               Width           =   8400
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   14817
               _ExtentY        =   4842
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraPruebas 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2865
            Left            =   -74850
            TabIndex        =   11
            Top             =   375
            Width           =   8505
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2700
               Index           =   1
               Left            =   0
               TabIndex        =   13
               Top             =   75
               Width           =   8400
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   14817
               _ExtentY        =   4762
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraTecnicas 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2865
            Left            =   150
            TabIndex        =   10
            Top             =   375
            Width           =   8535
            Begin SSDataWidgets_B.SSDBGrid grdssDatos 
               Height          =   2730
               Index           =   0
               Left            =   0
               TabIndex        =   12
               Top             =   75
               Width           =   8400
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   14817
               _ExtentY        =   4815
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraTiTec 
      Caption         =   "Tipos de T�cnicas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2340
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   9225
      Begin TabDlg.SSTab tabTitec 
         Height          =   1860
         HelpContextID   =   90001
         Left            =   300
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   345
         Width           =   8595
         _ExtentX        =   15161
         _ExtentY        =   3281
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cbossTipoTecnica(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cbossTipoTecnica(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkTipoTecnica"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtTipoTecnica(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtTipoTecnica(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtTipoTecnica(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTiTec"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtTipoTecnica 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP04_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1350
            MaxLength       =   200
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del Tipo de T�cnica"
            Top             =   750
            Width           =   6555
         End
         Begin VB.TextBox txtTipoTecnica 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AP04_CODTITEC"
            Height          =   285
            Index           =   0
            Left            =   6450
            TabIndex        =   0
            Tag             =   "Tipo T�cnica|C�digo del Tipo de T�cnica"
            Top             =   75
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtTipoTecnica 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP04_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1350
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n del Tipo de T�cnica"
            Top             =   270
            Width           =   5250
         End
         Begin VB.CheckBox chkTipoTecnica 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP04_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6900
            TabIndex        =   4
            Tag             =   "Activa|Activa"
            Top             =   225
            Width           =   990
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTipoTecnica 
            DataField       =   "AP02_CODTIPOR"
            Height          =   315
            Index           =   1
            Left            =   5175
            TabIndex        =   3
            Tag             =   "Tipo Porta|Tipo de Porta"
            Top             =   1200
            Width           =   2730
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   4210752
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1614
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Designaci�n"
            Columns(1).Name =   "Designaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4815
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTiTec 
            Height          =   1710
            Left            =   -74850
            TabIndex        =   18
            Top             =   75
            Width           =   7920
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   13970
            _ExtentY        =   3016
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cbossTipoTecnica 
            DataField       =   "AP06_CODCARP"
            Height          =   315
            Index           =   2
            Left            =   1320
            TabIndex        =   19
            Tag             =   "Carpeta|Carpeta asociada al Tipo de T�cnica"
            Top             =   1200
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1614
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Designaci�n"
            Columns(1).Name =   "Designaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carpeta:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   450
            TabIndex        =   20
            Top             =   1200
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   17
            Top             =   720
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   150
            TabIndex        =   16
            Top             =   270
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo Porta:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   4050
            TabIndex        =   15
            Top             =   1200
            Width           =   1080
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   6870
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_TipoTecnica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim objTiTec As New clsCWForm
Dim objTecnicas As New clsCWForm
Dim objPruebas As New clsCWForm
Dim objTiBloq As New clsCWForm

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cbossTipoTecnica_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossTipoTecnica_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cbossTipoTecnica_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub cbossTipoTecnica_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim strKey As String, sql As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objTiTec
    .strName = "TipoTecnica"
    Set .objFormContainer = fraTiTec
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTitec
    Set .grdGrid = grdssTiTec
    .strTable = "AP0400"
    .intAllowance = cwAllowAll
    .blnAskPrimary = False
    Call .FormAddOrderField("AP04_Desig", cwAscending)
    strKey = .strTable
    Call .objPrinter.Add("AP0501.rpt", "Listado de tipos de t�cnicas")
    Call .objPrinter.Add("AP0402.rpt", "Listado de tipos de t�cnicas/t�cnicas")
    Call .objPrinter.Add("AP0502.rpt", "Listado de tipos de t�cnicas/pruebas")
    Call .objPrinter.Add("AP0503.rpt", "Listado de tipos de t�cnicas/bloques")
    Call .FormCreateFilterWhere(strKey, "Tipos de t�cnicas")
    Call .FormAddFilterWhere(strKey, "AP04_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AP04_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AP04_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "AP04_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "AP04_DESCRIP", "Descripci�n")
  End With
  
  With objTecnicas
    .strName = "Tecnica"
    Set .objFormContainer = fraTecnicas
    Set .objFatherContainer = fraTiTec
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP1200"
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("AP12_Desig", cwAscending)
    Call .FormAddRelation("AP04_CodTiTec", txtTipoTecnica(0))
  End With
  
  With objPruebas
    .strName = "Pr"
    Set .objFormContainer = fraPruebas
    Set .objFatherContainer = fraTiTec
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP1700"
    .intAllowance = cwAllowAll
    Call .FormAddRelation("AP04_CodTiTec", txtTipoTecnica(0))
  End With
 
  With objTiBloq
    .strName = "TipoBloque"
    Set .objFormContainer = fraTiBloq
    Set .objFatherContainer = fraTiTec
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDatos(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP5300"
    .intAllowance = cwAllowAll
    Call .FormAddRelation("AP04_CodTiTec", txtTipoTecnica(0))
  End With
 
  With objWinInfo
    Call .FormAddInfo(objTiTec, cwFormDetail)
    
    Call .FormAddInfo(objTecnicas, cwFormMultiLine)
    Call .GridAddColumn(objTecnicas, "Tipo T�cnica", "AP04_CodTiTec", cwNumeric, 2)
    Call .GridAddColumn(objTecnicas, "T�cnica", "AP12_Desig", cwString, 20)
    
    Call .FormAddInfo(objPruebas, cwFormMultiLine)
    Call .GridAddColumn(objPruebas, "Tipo T�cnica", "AP04_CodTiTec", cwNumeric, 2)
    Call .GridAddColumn(objPruebas, "Prueba", "PR01CodActuacion", cwNumeric, 2)
    
    Call .FormAddInfo(objTiBloq, cwFormMultiLine)
    Call .GridAddColumn(objTiBloq, "Tipo T�cnica", "AP04_CodTiTec", cwNumeric, 2)
    Call .GridAddColumn(objTiBloq, "Tipo de bloque", "AP05_CodTiBloq", cwNumeric, 2)
    
    Call .FormCreateInfo(objTiTec)
    
    .CtrlGetInfo(txtTipoTecnica(0)).blnValidate = False
    .CtrlGetInfo(cbossTipoTecnica(1)).strSQL = "SELECT AP02_CodTiPor, AP02_Desig FROM AP0200 ORDER BY AP02_Desig"
    .CtrlGetInfo(cbossTipoTecnica(2)).strSQL = "SELECT AP06_CodCarp, AP06_Desig FROM AP0600 ORDER BY AP06_Desig"
    
    Call .FormChangeColor(objTecnicas)

    grdssDatos(0).Columns(3).Visible = False
    grdssDatos(0).Columns(4).Width = 10000
    Call .FormChangeColor(objTecnicas)
    
    grdssDatos(1).Columns(3).Visible = False
    .CtrlGetInfo(grdssDatos(1).Columns(4)).strSQL = "SELECT PR01CodActuacion, PR01DesCorta " _
        & "FROM PR0100 WHERE PR01CodActuacion IN (" _
        & "SELECT PR01CodActuacion FROM PR0200 WHERE AD02CodDpto = " & cDptAP _
        & ") ORDER BY PR01CodActuacion"
    grdssDatos(1).Columns(4).Width = 10000
    Call .FormChangeColor(objPruebas)

    grdssDatos(2).Columns(3).Visible = False
    .CtrlGetInfo(grdssDatos(2).Columns(4)).strSQL = "SELECT AP05_CodTiBloq, AP05_Desig FROM AP0500 ORDER BY AP05_CodTiBloq"
    grdssDatos(2).Columns(4).Width = 10000
    Call .FormChangeColor(objTiBloq)

    .CtrlGetInfo(cbossTipoTecnica(1)).blnForeign = True
    .CtrlGetInfo(cbossTipoTecnica(2)).blnForeign = True
    .CtrlGetInfo(grdssDatos(0).Columns(4)).blnForeign = True
    .CtrlGetInfo(grdssDatos(2).Columns(4)).blnForeign = True
        
    .CtrlGetInfo(txtTipoTecnica(1)).blnInFind = True
    .CtrlGetInfo(txtTipoTecnica(2)).blnInFind = True
    .CtrlGetInfo(cbossTipoTecnica(1)).blnInFind = True
    .CtrlGetInfo(cbossTipoTecnica(2)).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraDatos_Click()
    Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraTecnicas, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraPruebas, True, True)
    End Select
    'fraTiTec.ForeColor = cwGRIS
    'fraDatos.ForeColor = cwAZUL
End Sub


Private Sub grdssDatos_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssDatos_DblClick(Index As Integer)
  Call objWinInfo.GridDblClick
End Sub

Private Sub grdssDatos_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraTiTec.ForeColor = cwGRIS
  'fraDatos.ForeColor = cwAZUL
End Sub


Private Sub grdssDatos_RowColChange(Index As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim sql As String
  Select Case strFormName
    Case objTiTec.strName
      Select Case strCtrl
        Case "cbossTipoTecnica(1)"
          frmA_TipoPorta.Show vbModal
          Set frmA_TipoPorta = Nothing
          sql = "SELECT AP02_CodTiPor, AP02_Desig FROM AP0200 ORDER BY AP02_Desig"
          objWinInfo.CtrlGetInfo(cbossTipoTecnica(1)).strSQL = sql
          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossTipoTecnica(1)))
        Case "cbossTipoTecnica(2)"
          frmA_Carpeta.Show vbModal
          Set frmA_Carpeta = Nothing
          sql = "SELECT AP06_CodCarp, AP06_Desig FROM AP0600 ORDER BY AP06_Desig"
          objWinInfo.CtrlGetInfo(cbossTipoTecnica(2)).strSQL = sql
          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossTipoTecnica(2)))
      End Select
    Case objTecnicas.strName
      frmA_Tecnica.Show vbModal
      Set frmA_Tecnica = Nothing
      Call objWinInfo.DataRefresh
    Case objTiBloq.strName
      frmA_TipoBloque.Show vbModal
      Set frmA_TipoBloque = Nothing
      sql = "SELECT AP05_CodTiBloq, AP05_Desig FROM AP0500 ORDER BY AP05_CodTiBloq"
      objWinInfo.CtrlGetInfo(grdssDatos(2).Columns(4)).strSQL = sql
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDatos(2).Columns(4)))
  End Select

End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Select Case strFormName
    Case objTiTec.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtTipoTecnica(0).Text = fNextClave("AP04_CODTiTec")
        objTiTec.rdoCursor("AP04_CODTiTec") = txtTipoTecnica(0).Text
      End If
  End Select

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case tabDatos.Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraTecnicas, True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraPruebas, True, True)
    Case 2
        Call objWinInfo.FormChangeActive(fraTiBloq, True, True)
    End Select
    'fraTiTec.ForeColor = cwGRIS
    'fraDatos.ForeColor = cwAZUL
End Sub

Private Sub Timer1_Timer()
    Select Case tabDatos.Tab
    Case 0
        fraDatos.ForeColor = fraTecnicas.ForeColor
    Case 1
        fraDatos.ForeColor = fraPruebas.ForeColor
    Case 2
        fraDatos.ForeColor = fraTiBloq.ForeColor
    End Select
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index = 2 Then
    If Me.ActiveControl.Name = "grdssDatos" Then
      grdssDatos(tabDatos.Tab).Columns(3).Text = txtTipoTecnica(0)
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssTiTec_GotFocus()
  Call objWinInfo.CtrlGotFocus
  'fraTiTec.ForeColor = cwAZUL
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub grdssTiTec_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTiTec_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTiTec_Change()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabtiTec_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
 Call objWinInfo.FormChangeActive(tabTitec, False, True)
  'fraTiTec.ForeColor = cwAZUL
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub fraTiTec_Click()
 Call objWinInfo.FormChangeActive(fraTiTec, False, True)
  'fraDatos.ForeColor = cwGRIS
  'fraTiTec.ForeColor = cwAZUL
End Sub
Private Sub fraPruebas_Click()
  Call objWinInfo.FormChangeActive(fraPruebas, False, True)
  'fraTiTec.ForeColor = cwGRIS
  'fraDatos.ForeColor = cwAZUL
End Sub
Private Sub fraTecnicas_Click()
  Call objWinInfo.FormChangeActive(fraTecnicas, False, True)
  'fraTiTec.ForeColor = cwGRIS
  'fraDatos.ForeColor = cwAZUL
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
  'fraDatos.ForeColor = cwAZUL
End Sub

Private Sub chktipoTecnica_GotFocus()
  Call objWinInfo.CtrlGotFocus
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub chktipoTecnica_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chktipoTecnica_Click()
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtTipoTecnica_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDatos.ForeColor = cwGRIS
End Sub

Private Sub txtTipoTecnica_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTipoTecnica_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

