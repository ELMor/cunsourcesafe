VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_PerfilDiag 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Perfiles de Diagnósticos"
   ClientHeight    =   7065
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9840
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7065
   ScaleWidth      =   9840
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9840
      _ExtentX        =   17357
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   0
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3525
      Left            =   75
      TabIndex        =   11
      Top             =   3075
      Width           =   9540
      Begin TabDlg.SSTab tabDatos 
         Height          =   3060
         HelpContextID   =   90001
         Left            =   150
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   300
         Width           =   9180
         _ExtentX        =   16193
         _ExtentY        =   5398
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   -2147483644
         TabCaption(0)   =   "Diagnósticos"
         TabPicture(0)   =   "Ap00764.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraDiag"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Organos"
         TabPicture(1)   =   "Ap00764.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraOrg"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraOrg 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   2640
            Left            =   -74925
            TabIndex        =   15
            Top             =   375
            Width           =   8955
            Begin SSDataWidgets_B.SSDBGrid grdssOrg 
               Height          =   2475
               Index           =   0
               Left            =   75
               TabIndex        =   16
               Top             =   75
               Width           =   8775
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15478
               _ExtentY        =   4366
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraDiag 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2595
            Left            =   75
            TabIndex        =   13
            Top             =   375
            Width           =   9060
            Begin SSDataWidgets_B.SSDBGrid grdssDiag 
               Height          =   2490
               Index           =   0
               Left            =   75
               TabIndex        =   14
               Top             =   75
               Width           =   8820
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15557
               _ExtentY        =   4392
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraPerfil 
      Caption         =   "Perfiles"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2445
      Left            =   90
      TabIndex        =   6
      Top             =   450
      Width           =   9585
      Begin TabDlg.SSTab tabPerfil 
         Height          =   1950
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   9270
         _ExtentX        =   16351
         _ExtentY        =   3440
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "chkPerfil"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtPerfil(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtPerfil(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtPerfil(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssPerfil"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtPerfil 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP55_CODPERF"
            Height          =   285
            Index           =   0
            Left            =   1860
            TabIndex        =   0
            Tag             =   "Código|Código del Perfil"
            Top             =   0
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtPerfil 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP55_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1620
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designación|Nombre abreviado del Perfil"
            Top             =   420
            Width           =   4875
         End
         Begin VB.TextBox txtPerfil 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP55_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1620
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripción|Descripción del Perfil"
            Top             =   900
            Width           =   7035
         End
         Begin VB.CheckBox chkPerfil 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP55_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   900
            TabIndex        =   3
            Tag             =   "Activa|Activa"
            Top             =   1320
            Width           =   930
         End
         Begin SSDataWidgets_B.SSDBGrid grdssPerfil 
            Height          =   1695
            Left            =   -74880
            TabIndex        =   5
            Top             =   120
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   2990
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designación:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   375
            TabIndex        =   10
            Top             =   480
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripción:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   450
            TabIndex        =   9
            Top             =   900
            Width           =   1140
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   6780
      Width           =   9840
      _ExtentX        =   17357
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edición"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar último valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_PerfilDiag"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objPerfil As New clsCWForm
Dim objDiag As New clsCWForm
Dim objOrg As New clsCWForm

Dim desigTiDiag As String, DesigTiOrg As String

' -----------------------------------------------
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objPerfil
    Set .objFormContainer = fraPerfil
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPerfil
    Set .grdGrid = grdssPerfil
    .strTable = "AP5500"
    Call .FormAddOrderField("AP55_Desig", cwAscending)
    
    Call .objPrinter.Add("AP6401.rpt", "Listado de perfiles de diagnósticos")
    Call .objPrinter.Add("AP6402.rpt", "Listado de perfiles/diagnósticos")
    Call .objPrinter.Add("AP6403.rpt", "Listado de perfiles/órganos")
    strKey = .strTable
    Call .FormCreateFilterWhere(strKey, "Perfiles diagnósticos")
    Call .FormAddFilterWhere(strKey, "AP55_DESIG", "Designación", cwString)
    Call .FormAddFilterWhere(strKey, "AP55_DESCRIP", "Descripción", cwString)
    Call .FormAddFilterWhere(strKey, "AP55_INDACTIVA", "¿Activa?", cwBoolean)

    Call .FormAddFilterOrder(strKey, "AP55_DESIG", "Designación")
    Call .FormAddFilterOrder(strKey, "AP55_DESCRIP", "Descripción")
    .blnAskPrimary = False
    .blnHasMaint = False
  End With
  
  With objDiag
    Set .objFormContainer = fraDiag
    Set .objFatherContainer = fraPerfil
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssDiag(0)
    .intAllowance = cwAllowAdd + cwAllowDelete
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP5600"

    Call .FormAddOrderField("AP37_CODSno", cwAscending)
    Call .FormAddRelation("AP55_CODPERF", txtPerfil(0))
  End With

  With objOrg
    Set .objFormContainer = fraOrg
    Set .objFatherContainer = fraPerfil
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssOrg(0)
    .intAllowance = cwAllowAdd + cwAllowDelete
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP5700"
    Call .FormAddRelation("AP55_CODPERF", txtPerfil(0))
  End With

  With objWinInfo
    Call .FormAddInfo(objPerfil, cwFormDetail)
    
    Call .FormAddInfo(objDiag, cwFormMultiLine)
    Call .GridAddColumn(objDiag, "Perfil", "AP55_CODPERF", cwNumeric, 3)
    Call .GridAddColumn(objDiag, "Tipo Diagnóstico", "", cwNumeric, 3)
    Call .GridAddColumn(objDiag, "Diagnóstico", "AP37_CODSNO", cwNumeric, 3)

    Call .FormAddInfo(objOrg, cwFormMultiLine)
    Call .GridAddColumn(objOrg, "Perfil", "AP55_CODPERF", cwNumeric, 3)
    Call .GridAddColumn(objOrg, "Tipo Organo", "", cwNumeric, 3)
    Call .GridAddColumn(objOrg, "Organo", "PR24CodMuestra", cwNumeric, 3)

    Call .FormCreateInfo(objPerfil)
    .CtrlGetInfo(txtPerfil(0)).blnValidate = False
    .CtrlGetInfo(chkPerfil).blnInFind = True
    .CtrlGetInfo(txtPerfil(1)).blnInFind = True
    .CtrlGetInfo(txtPerfil(2)).blnInFind = True
    .CtrlGetInfo(grdssDiag(0).Columns(5)).blnForeign = True
    
    Call .FormChangeColor(objDiag)
    .CtrlGetInfo(grdssDiag(0).Columns(4)).strSQL = "SELECT AP50_CODTISno, AP50_DESIG FROM AP5000 ORDER BY AP50_DESIG"
    .CtrlGetInfo(grdssDiag(0).Columns(5)).strSQL = "SELECT AP37_CODSno, AP37_DEScrip FROM AP3700 ORDER BY AP37_DESCRIP"

    .CtrlGetInfo(grdssOrg(0).Columns(4)).strSQL = "SELECT PR49CodTipMuestr, PR49DesTipMuestr FROM PR4900 ORDER BY PR49DesTipMuestr"
    .CtrlGetInfo(grdssOrg(0).Columns(5)).strSQL = "SELECT PR24CodMuestra, PR24DesCorta FROM PR2400 ORDER BY PR24DesCorta"
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdssPerfil.Columns(1).Visible = False
  grdssDiag(0).Columns(3).Visible = False
  grdssOrg(0).Columns(3).Visible = False
  
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraOrg_Click()
  Call objWinInfo.FormChangeActive(fraOrg, False, True)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim sql As String
  Select Case strFormName
    Case objDiag.strName
      frmA_Snomed.Show vbModal
      Set frmA_Snomed = Nothing
      sql = "SELECT AP37_CODSno, AP37_DEScrip FROM AP3700 ORDER BY AP37_DESCRIP"
      objWinInfo.CtrlGetInfo(grdssDiag(0).Columns(5)).strSQL = sql
  End Select
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
    
  Select Case strFormName
    Case objDiag.strName
      If desigTiDiag <> "" Then
        desigTiDiag = ""
        Call pCargarDropDownDiag
      End If
      If DesigTiOrg <> "" Then
        DesigTiOrg = ""
        Call pCargarDropDownOrg
      End If
  End Select
    
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)

  Select Case strFormName
    Case objPerfil.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtPerfil(0).Text = fNextClave("AP55_codPerf")
        objPerfil.rdoCursor("AP55_codPerf") = txtPerfil(0).Text
      End If
  End Select
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing

End Sub

' -----------------------------------------------
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Select Case tabDatos.Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraDiag, True, True)
    Case 1
      Call objWinInfo.FormChangeActive(fraOrg, True, True)
  End Select

End Sub

Private Sub Timer1_Timer()
  Select Case tabDatos.Tab
    Case 0
      fraDatos.ForeColor = fraDiag.ForeColor
    Case 1
      fraDatos.ForeColor = fraOrg.ForeColor
  End Select
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub
Sub pCargarDropDownDiag()
    Dim sql As String
    
    sql = "SELECT AP37_codSno, AP37_descrip FROM AP3700"
    If desigTiDiag <> "" Then
        sql = sql & " WHERE AP50_codTiSno IN "
        sql = sql & "    (SELECT AP50_codTiSno FROM AP5000"
        sql = sql & "     WHERE AP50_desig = '" & desigTiDiag & "')"
    End If
    sql = sql & " ORDER BY AP37_descrip"
    objWinInfo.CtrlGetInfo(grdssDiag(0).Columns(5)).strSQL = sql
    'grdssDiag(0).Columns(5).RemoveAll
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssDiag(0).Columns(5)))
    objWinInfo.WinPrepareScr

End Sub


' -----------------------------------------------
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' eventos del Grid
' -----------------------------------------------
Private Sub grdssPerfil_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssPerfil_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPerfil_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssPerfil_Change()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssDiag_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssDiag_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssDiag_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
  If grdssDiag(0).Col = 5 Then
    If grdssDiag(0).Columns(1).Text = 6 Then
      desigTiDiag = grdssDiag(0).Columns(4).Text
      Call pCargarDropDownDiag
    End If
  End If
    
End Sub

Private Sub grdssOrg_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdssOrg_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssOrg_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssOrg_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If grdssOrg(0).Col = 5 Then
        If grdssOrg(0).Columns(1).Text = 6 Then
            DesigTiOrg = grdssOrg(0).Columns(4).Text
            Call pCargarDropDownOrg
        End If
    End If
    
End Sub
Sub pCargarDropDownOrg()
    Dim sql As String
    
    sql = "SELECT PR24CodMuestra, PR24DesCorta FROM PR2400"
    If DesigTiOrg <> "" Then
        sql = sql & " WHERE PR49CodTipMuestr IN "
        sql = sql & "    (SELECT PR49CodTipMuestr FROM PR4900"
        sql = sql & "     WHERE PR49DesTipMuestr = '" & DesigTiOrg & "')"
    End If
    sql = sql & " ORDER BY PR24DesCorta"
    objWinInfo.CtrlGetInfo(grdssOrg(0).Columns(5)).strSQL = sql
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdssOrg(0).Columns(5)))
    objWinInfo.WinPrepareScr

End Sub


' -----------------------------------------------
' eventos del tab
' -----------------------------------------------
Private Sub tabPerfil_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabPerfil, False, True)
End Sub
' -----------------------------------------------
' eventos del frame
' -----------------------------------------------
Private Sub fraPerfil_Click()
  Call objWinInfo.FormChangeActive(fraPerfil, False, True)
End Sub
Private Sub fraDiag_Click()
  Call objWinInfo.FormChangeActive(fraDiag, False, True)
End Sub


' -----------------------------------------------
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

' -----------------------------------------------
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkPerfil_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkPerfil_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkPerfil_Click()
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtPerfil_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtPerfil_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtPerfil_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
