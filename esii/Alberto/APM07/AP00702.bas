Attribute VB_Name = "modSequence"
Option Explicit

Public Function fNextClave(campo$) As String
    Dim sql$, rsMaxClave As rdoResultset
    
    sql = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(sql)
    fNextClave = rsMaxClave(0)
    rsMaxClave.Close
    
End Function
Public Function fMaxCodDetalle(tablaDetalle$, codigoDetalle$, _
  refMaestro$, valorMaestro$) As String
    
 Dim sql$, rsMaxClave As rdoResultset
    
    sql = "SELECT (MAX(" & codigoDetalle & ")+1) FROM " & tablaDetalle
    sql = sql + " WHERE " & refMaestro & " = " & valorMaestro
 
 Set rsMaxClave = objApp.rdoConnect.OpenResultset(sql)
  If IsNull(rsMaxClave(0)) Then
      fMaxCodDetalle = 1
  Else
      fMaxCodDetalle = rsMaxClave(0)
  End If

    rsMaxClave.Close
    
End Function

