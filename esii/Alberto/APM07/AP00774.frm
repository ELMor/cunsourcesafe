VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form frmMantenimiento 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de tablas"
   ClientHeight    =   4875
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9540
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4875
   ScaleWidth      =   9540
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   465
      Left            =   8025
      TabIndex        =   21
      Top             =   3975
      Width           =   1290
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   465
      Left            =   8025
      TabIndex        =   20
      Top             =   300
      Width           =   1290
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   4215
      Left            =   300
      TabIndex        =   0
      Top             =   225
      Width           =   7440
      _Version        =   65536
      _ExtentX        =   13123
      _ExtentY        =   7435
      _StockProps     =   14
      Caption         =   "Tipos de tablas"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.OptionButton optMto 
         Caption         =   "Tipos de t�cnicas"
         Height          =   315
         Index           =   18
         Left            =   4125
         TabIndex        =   19
         Top             =   3375
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Tipos de SNOMED"
         Height          =   315
         Index           =   17
         Left            =   4125
         TabIndex        =   18
         Top             =   3000
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Tipos de pruebas"
         Height          =   315
         Index           =   16
         Left            =   4125
         TabIndex        =   17
         Top             =   2625
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Tipos de portas"
         Height          =   315
         Index           =   15
         Left            =   4125
         TabIndex        =   16
         Top             =   2250
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Tipos de bloques"
         Height          =   315
         Index           =   14
         Left            =   4125
         TabIndex        =   15
         Top             =   1875
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "T�cnicas"
         Height          =   315
         Index           =   13
         Left            =   4125
         TabIndex        =   14
         Top             =   1500
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "SNOMED"
         Height          =   315
         Index           =   12
         Left            =   4125
         TabIndex        =   13
         Top             =   1125
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Tipos de problemas"
         Height          =   315
         Index           =   11
         Left            =   4125
         TabIndex        =   12
         Top             =   750
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Datos asociados al personal"
         Height          =   315
         Index           =   10
         Left            =   4125
         TabIndex        =   11
         Top             =   375
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Perfiles de t�cnicas"
         Height          =   315
         Index           =   9
         Left            =   225
         TabIndex        =   10
         Top             =   3750
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Perfiles de diagn�sticos"
         Height          =   315
         Index           =   8
         Left            =   225
         TabIndex        =   9
         Top             =   3375
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Datos asociados a �rganos"
         Height          =   315
         Index           =   7
         Left            =   225
         TabIndex        =   8
         Top             =   3000
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Motivos de inter�s"
         Height          =   315
         Index           =   6
         Left            =   225
         TabIndex        =   7
         Top             =   2625
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Fijadores"
         Height          =   315
         Index           =   5
         Left            =   225
         TabIndex        =   6
         Top             =   2250
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Decalcificadores"
         Height          =   315
         Index           =   4
         Left            =   225
         TabIndex        =   5
         Top             =   1875
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Centros externos"
         Height          =   315
         Index           =   3
         Left            =   225
         TabIndex        =   4
         Top             =   1500
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Carpetas"
         Height          =   315
         Index           =   2
         Left            =   225
         TabIndex        =   3
         Top             =   1125
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Motivos de almacenamiento"
         Height          =   315
         Index           =   1
         Left            =   225
         TabIndex        =   2
         Top             =   750
         Width           =   3000
      End
      Begin VB.OptionButton optMto 
         Caption         =   "Almacenes"
         Height          =   315
         Index           =   0
         Left            =   225
         TabIndex        =   1
         Top             =   375
         Value           =   -1  'True
         Width           =   3000
      End
   End
End
Attribute VB_Name = "frmMantenimiento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
Dim res As Integer
Dim launc As clsCWLauncher

  If optMto(0).Value = True Then res = objSecurity.LaunchProcess(APWinA_Almacen)
  If optMto(1).Value = True Then res = objSecurity.LaunchProcess(APWinA_Almacenamiento)
  If optMto(2).Value = True Then res = objSecurity.LaunchProcess(APWinA_Carpeta)
  If optMto(3).Value = True Then res = objSecurity.LaunchProcess(APWinA_CentroPatologo)
  If optMto(4).Value = True Then res = objSecurity.LaunchProcess(APWinA_Decalcificador)
  If optMto(5).Value = True Then res = objSecurity.LaunchProcess(APWinA_Fijador)
  If optMto(6).Value = True Then res = objSecurity.LaunchProcess(APWinA_Interes)
  If optMto(7).Value = True Then res = objSecurity.LaunchProcess(APWinA_Organo)
  If optMto(8).Value = True Then res = objSecurity.LaunchProcess(APWinA_PerfilDiag)
  If optMto(9).Value = True Then res = objSecurity.LaunchProcess(APWinA_PerfilTecnica)
  If optMto(10).Value = True Then res = objSecurity.LaunchProcess(APWinA_Personal)
  If optMto(11).Value = True Then res = objSecurity.LaunchProcess(APWinA_Problema)
  If optMto(12).Value = True Then res = objSecurity.LaunchProcess(APWinA_Snomed)
  If optMto(13).Value = True Then res = objSecurity.LaunchProcess(APWinA_Tecnica)
  If optMto(14).Value = True Then res = objSecurity.LaunchProcess(APWinA_TipoBloque)
  If optMto(15).Value = True Then res = objSecurity.LaunchProcess(APWinA_TipoPorta)
  If optMto(16).Value = True Then res = objSecurity.LaunchProcess(APWinA_TipoSnomed)
  If optMto(17).Value = True Then res = objSecurity.LaunchProcess(APWinA_TipoPrueba)
  If optMto(18).Value = True Then res = objSecurity.LaunchProcess(APWinA_TipoTecnica)
End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub
