VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_Problema 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento del Problemas"
   ClientHeight    =   6510
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9780
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6510
   ScaleWidth      =   9780
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   9780
      _ExtentX        =   17251
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraProblema 
      Caption         =   "Problemas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2955
      Left            =   75
      TabIndex        =   12
      Top             =   3180
      Width           =   9525
      Begin TabDlg.SSTab tabProblema 
         Height          =   2295
         Left            =   150
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   375
         Width           =   9165
         _ExtentX        =   16166
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(4)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtProblema(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chkProblema"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtProblema(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtProblema(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtProblema(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssProblema"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtProblema 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP35_CODTIPROBL"
            Height          =   285
            HelpContextID   =   40101
            Index           =   3
            Left            =   2940
            Locked          =   -1  'True
            TabIndex        =   22
            Tag             =   "C�digo Tipo Problema|C�digo del Tipo de Problema"
            Top             =   240
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.TextBox txtProblema 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP41_CODPROBL"
            Height          =   285
            HelpContextID   =   40101
            Index           =   0
            Left            =   1740
            Locked          =   -1  'True
            TabIndex        =   4
            Tag             =   "C�digo|C�digo del Problema"
            Top             =   240
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.TextBox txtProblema 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP41_DESIG"
            Height          =   285
            HelpContextID   =   40101
            Index           =   1
            Left            =   1755
            Locked          =   -1  'True
            MaxLength       =   20
            TabIndex        =   5
            Tag             =   "Designaci�n|Nombre abreviado del Problema"
            Top             =   600
            Width           =   3240
         End
         Begin VB.CheckBox chkProblema 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP41_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   960
            TabIndex        =   18
            Tag             =   "Activa|Activa"
            Top             =   1500
            Width           =   990
         End
         Begin VB.TextBox txtProblema 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP41_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1740
            MaxLength       =   50
            TabIndex        =   6
            Tag             =   "Descripci�n|Descripci�n del Problema"
            Top             =   1080
            Width           =   6630
         End
         Begin SSDataWidgets_B.SSDBGrid grdssProblema 
            Height          =   1890
            Left            =   -74850
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   225
            Width           =   8445
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   14896
            _ExtentY        =   3334
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   420
            TabIndex        =   21
            Top             =   600
            Width           =   1125
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   900
            TabIndex        =   20
            Top             =   240
            Visible         =   0   'False
            Width           =   795
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   540
            TabIndex        =   19
            Top             =   1080
            Width           =   1080
         End
      End
   End
   Begin VB.Frame fraTiProbl 
      Caption         =   "Tipo de Problemas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Left            =   90
      TabIndex        =   9
      Top             =   450
      Width           =   9465
      Begin TabDlg.SSTab tabTiProbl 
         Height          =   2100
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   9090
         _ExtentX        =   16034
         _ExtentY        =   3704
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "chkTiProbl"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtTiProbl(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtTiProbl(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtTiProbl(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTiProbl"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtTiProbl 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP35_DESCRIP"
            Height          =   285
            Index           =   2
            Left            =   1800
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del Tipo de Problema"
            Top             =   960
            Width           =   6630
         End
         Begin VB.TextBox txtTiProbl 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP35_CODTIPROBL"
            Height          =   285
            Index           =   0
            Left            =   1800
            TabIndex        =   0
            Tag             =   "C�digo|C�digo del Tipo de Problema"
            Top             =   0
            Visible         =   0   'False
            Width           =   570
         End
         Begin VB.TextBox txtTiProbl 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP35_DESIG"
            Height          =   285
            Index           =   1
            Left            =   1815
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Nombre abreviado del Tipo de Problema"
            Top             =   480
            Width           =   3375
         End
         Begin VB.CheckBox chkTiProbl 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP35_INDACTIVA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1020
            TabIndex        =   3
            Tag             =   "Activa|Activa"
            Top             =   1380
            Width           =   990
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTiProbl 
            Height          =   1770
            Left            =   -74850
            TabIndex        =   8
            Top             =   150
            Width           =   8370
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   14764
            _ExtentY        =   3122
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   600
            TabIndex        =   17
            Top             =   960
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   960
            TabIndex        =   16
            Top             =   0
            Visible         =   0   'False
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   540
            TabIndex        =   15
            Top             =   480
            Width           =   1200
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   13
      Top             =   6225
      Width           =   9780
      _ExtentX        =   17251
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Problema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
  Dim objTiProbl As New clsCWForm
  Dim objProblema As New clsCWForm

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objTiProbl
    .strName = "TipoProblema"
    Set .objFormContainer = fraTiProbl
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTiProbl
    Set .grdGrid = grdssTiProbl
    .strTable = "AP3500"
    .intAllowance = cwAllowAll
    .blnAskPrimary = False
    Call .FormAddOrderField("AP35_Desig", cwAscending)
    
    Call .objPrinter.Add("AP1801.rpt", "Listado de problemas")
    strKey = .strTable
    Call .FormCreateFilterWhere(strKey, "Tipos de problemas")
    Call .FormAddFilterWhere(strKey, "AP35_DESIG", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AP35_DESCRIP", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AP35_INDACTIVA", "�Activa?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "AP35_DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "AP35_DESCRIP", "Descripci�n")
  End With
  
  With objProblema
    .strName = "Problema"
    Set .objFormContainer = fraProblema
    Set .objFatherContainer = fraTiProbl
    Set .tabMainTab = tabProblema
    Set .grdGrid = grdssProblema
    .strTable = "AP4100"
    .blnAskPrimary = False
    Call .FormAddOrderField("AP41_CodProbl", cwAscending)
    Call .FormAddRelation("AP35_CodTiProbl", txtTiProbl(0))
  End With
   
  With objWinInfo
    Call .FormAddInfo(objTiProbl, cwFormDetail)
    Call .FormAddInfo(objProblema, cwFormDetail)
   
    Call .FormCreateInfo(objTiProbl)
    .CtrlGetInfo(txtProblema(3)).blnInGrid = False
    .CtrlGetInfo(txtTiProbl(0)).blnValidate = False
    .CtrlGetInfo(txtProblema(0)).blnValidate = False
    
    .CtrlGetInfo(txtTiProbl(1)).blnInFind = True
    .CtrlGetInfo(txtTiProbl(2)).blnInFind = True
    .CtrlGetInfo(txtProblema(1)).blnInFind = True
    .CtrlGetInfo(txtProblema(2)).blnInFind = True
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Select Case strFormName
    Case objTiProbl.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtTiProbl(0).Text = fNextClave("AP35_CODTIPROBL")
        objTiProbl.rdoCursor("AP35_CODTIPROBL") = txtTiProbl(0).Text
      End If
    Case objProblema.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtProblema(0).Text = fNextClave("AP41_CODPROBL")
        objProblema.rdoCursor("AP41_CODPROBL") = txtProblema(0).Text
      End If
  End Select

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index = 2 Then txtProblema(3) = txtTiProbl(0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssTiProbl_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTiProbl_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTiProbl_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTiProbl_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssProblema_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssProblema_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssProblema_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssProblema_Change()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTiProbl_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTiProbl, False, True)
End Sub

Private Sub tabProblema_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabProblema, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraTiProbl_Click()
  Call objWinInfo.FormChangeActive(fraTiProbl, False, True)
End Sub

Private Sub fraProblema_Click()
  Call objWinInfo.FormChangeActive(fraProblema, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
 Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkProblema_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkProblema_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkProblema_Click()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub chkTiProbl_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkTiProbl_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkTiProbl_Click()
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtTiProbl_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtTiProbl_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTiProbl_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtProblema_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtProblema_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtProblema_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

