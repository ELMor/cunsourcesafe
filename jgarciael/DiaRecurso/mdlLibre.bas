Attribute VB_Name = "mdlLibre"
Option Explicit

Public Sub pDiasLibres(datDia As Date, calendario As SSMonth, lngCodRecurso As Long)

Dim bytMes As Byte 'N�mero del mes (1 a 12)
Dim datPrimerDia As Date 'Primer d�a del mes
Dim strPDMesSig As String 'Primer d�a del mes siguiente
'Bucle For...Next para los d�as del mes
Dim bytFor As Byte
Dim bytMax As Byte

'Busca en el mes que comprende al d�a datDia...
bytMes = Month(datDia)
datPrimerDia = Format("01/" & bytMes & "/" & Year(datDia), "DD/MM/YYYY", vbMonday, _
                                                                                vbFirstJan1)
If bytMes = 12 Then
    bytMax = 30
Else
    strPDMesSig = "01/" & (bytMes + 1) & "/" & Year(datDia)
    bytMax = DateDiff("d", datPrimerDia, CDate(strPDMesSig)) - 1
End If

For bytFor = 0 To bytMax 'Todos los d�as del mes
    If fCitasLibres(datPrimerDia + bytFor, calendario, lngCodRecurso) Then _
                                calendario.Day(datPrimerDia + bytFor).StyleSet = "Libre"
Next bytFor

End Sub

Private Function fCitasLibres(datDia As Date, calendario As SSMonth, lngCodRecurso As Long) _
                                                                                As Boolean
'Consultas SQL...
Dim SQL As String
Dim qryFranjasRec As rdoQuery
Dim rstFranjasRec As rdoResultset

'Inicializaci�n de variables
fCitasLibres = False 'No hay citas libres

'Si no es d�a festivo semanal...
If calendario.DayofWeek(DatePart("w", (datDia), vbMonday, vbFirstJan1)).StyleSet <> "Ausente" Then
  '..y no es festivo especial...
  If calendario.Day(datDia).StyleSet <> "Ausente" Then
    calendario.Day(datDia).StyleSet = "Normal"

   'Se obtienen los datos de las franjas para ese recurso y...
   'SELECT
   SQL = "SELECT AG04CODFRANJA, AG04HORINFRJHH, AG04HORINFRJMM, "
   SQL = SQL & "AG04HORFIFRJHH, AG04HORFIFRJMM, PR1200.PR12DESACTIVIDAD, "
   SQL = SQL & "AG04MODASIGCITA, AG04INTERVCITA, AG04NUMCITADMI, PR1200.PR12INDPLANIFIC "
   'FROM
   SQL = SQL & "FROM AG0400, PR1200 "
   'WHERE
   SQL = SQL & "WHERE AG0400.PR12CODACTIVIDAD = PR1200.PR12CODACTIVIDAD "
   SQL = SQL & "AND AG11CODRECURSO = ? AND AG07CODPERFIL =" 'qryFranjasRec(0) = codRecurso
      '...para el perfil cuyo periodo de vigencia contenga a la fecha y...
      'SELECT
      SQL = SQL & "(SELECT AG0700.AG07CODPERFIL "
      'FROM
      SQL = SQL & "FROM AG0700, AG0900 "
      'WHERE
      SQL = SQL & "WHERE AG0900.AG11CODRECURSO = ? " 'qryFranjasRec(1) = codRecurso
      SQL = SQL & "AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO "
      SQL = SQL & "AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL "
      Select Case fstrBuscarDiaSemana(datDia)
        Case "Lunes"
            SQL = SQL & "AND AG04INDLUNFRJA = -1 "
        Case "Martes"
            SQL = SQL & "AND AG04INDMARFRJA = -1 "
        Case "Mi�rcoles"
            SQL = SQL & "AND AG04INDMIEFRJA = -1 "
        Case "Jueves"
            SQL = SQL & "AND AG04INDJUEFRJA = -1 "
        Case "Viernes"
            SQL = SQL & "AND AG04INDVIEFRJA = -1 "
        Case "S�bado"
            SQL = SQL & "AND AG04INDSABFRJA = -1 "
        Case "Domingo"
            SQL = SQL & "AND AG04INDDOMFRJA = -1 "
      End Select
      SQL = SQL & "AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG09FECINVIPER AND AG09FECFIVIPER "
    '  SQL = SQL & "AND ? BETWEEN TO_CHAR(AG09FECINVIPER, 'DD/MM/YYYY') AND "
    '  SQL = SQL & "TO_CHAR(AG09FECFIVIPER, 'DD/MM/YYYY') "
      SQL = SQL & "AND AG07INDPERGENE ="
         '...si s�lo est� el gen�rico (-1) ser� ese, a no ser que haya uno m�s (0)...
         'SELECT
         SQL = SQL & "(SELECT MAX(AG07INDPERGENE) "
         'FROM
         SQL = SQL & "FROM AG0900, AG0700 " ', AG0400 "
         'WHERE
         SQL = SQL & "WHERE AG0900.AG11CODRECURSO = ? " 'qryFranjasRec(3) = codRecurso
         SQL = SQL & "AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO "
         SQL = SQL & "AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL "
'         SQL = SQL & "AND AG0400.AG11CODRECURSO = AG0700.AG11CODRECURSO "
'         SQL = SQL & "AND AG0400.AG07CODPERFIL = AG0700.AG07CODPERFIL "
         '...y cuya franja sea la del d�a seleccionado
         Select Case fstrBuscarDiaSemana(datDia)
            Case "Lunes"
               SQL = SQL & "AND AG04INDLUNFRJA = -1 "
            Case "Martes"
               SQL = SQL & "AND AG04INDMARFRJA = -1 "
            Case "Mi�rcoles"
               SQL = SQL & "AND AG04INDMIEFRJA = -1 "
            Case "Jueves"
               SQL = SQL & "AND AG04INDJUEFRJA = -1 "
            Case "Viernes"
               SQL = SQL & "AND AG04INDVIEFRJA = -1 "
            Case "S�bado"
               SQL = SQL & "AND AG04INDSABFRJA = -1 "
            Case "Domingo"
               SQL = SQL & "AND AG04INDDOMFRJA = -1 "
         End Select
         SQL = SQL & "AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG09FECINVIPER AND AG09FECFIVIPER "
         SQL = SQL & "AND (AG07FECBAJA >= TO_DATE(?, 'DD/MM/YYYY') OR AG07FECBAJA IS NULL)" 'qryFranjasRec(5) = SSMonth1.Date
         SQL = SQL & ")"
      SQL = SQL & ") "
   'ORDER BY
   SQL = SQL & "ORDER BY AG04HORINFRJHH, AG04HORINFRJMM" 'Se ordena cronol�gicamente
      
   Set qryFranjasRec = Conexion.CreateQuery("", SQL)
      qryFranjasRec(0) = lngCodRecurso
      qryFranjasRec(1) = lngCodRecurso
      qryFranjasRec(2) = Format(datDia, "DD/MM/YYYY")
      qryFranjasRec(3) = lngCodRecurso
      qryFranjasRec(4) = qryFranjasRec(2)
      qryFranjasRec(5) = qryFranjasRec(2)
   Set rstFranjasRec = qryFranjasRec.OpenResultset()
   
    If rstFranjasRec.EOF = False Then 'Si hay franjas...
        'Si hay citas libres...
        If fblnCitas(datDia, lngCodRecurso, rstFranjasRec) Then fCitasLibres = True
    Else
        calendario.Day(datDia).StyleSet = "NoTareas"
    End If
       
    'Se cierra la consulta
    qryFranjasRec.Close
    If Not rstFranjasRec.ActiveConnection Is Nothing Then rstFranjasRec.Close
  End If
End If

End Function

Private Function fblnCitas(datDia As Date, lngCodRecurso As Long, _
                                            rstFranjasRecurso As rdoResultset) As Boolean

Dim bytNumCitasAdm As Byte 'Citas admitidas en una franja (supongo que no pasar�n de 255)
Dim lngCodFranja As Long 'C�digo de la franja actual
Dim bytNumCitas As Byte 'N�mero de citas existentes
Dim datHoraInicio As Date 'Hora de inicio de la fanja
Dim datHoraFin As Date 'Hora de fin de la fanja
'Consultas SQL...
Dim SQL As String 'Consulta SQL
Dim qryActCitadas As rdoQuery 'Objeto de la consulta
Dim rstActCitadas As rdoResultset 'Resultado de la consulta

fblnCitas = False

'Se obtienen los datos de las citas para ese recurso y ese d�a
'SELECT
SQL = "SELECT CI2700.CI31NUMSOLICIT, CI2700.CI01NUMCITA, CI2700.CI15NUMFASECITA, "
SQL = SQL & "TO_CHAR(CI2700.CI27FECOCUPREC,'DD/MM/YYYY HH24:MI:SS'), "
SQL = SQL & "TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YYYY HH24:MI:SS'), "
'Minutos de ocupaci�n del recurso
SQL = SQL & "1440*CI27NUMDIASREC+60*CI27NUMHORAREC+CI27NUMMINUREC, "
'Datos del paciente
SQL = SQL & "CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, "
SQL = SQL & "PR0100.PR01DESCORTA " 'Datos de la actuaci�n
'FROM
SQL = SQL & "FROM CI2700, CI0100, CI2200, PR0400, PR0300, PR0100 "
'WHERE
SQL = SQL & "WHERE CI2700.AG11CODRECURSO = ? " 'qryActCitadas(0) = codRecurso
SQL = SQL & "AND CI0100.CI31NUMSOLICIT = CI2700.CI31NUMSOLICIT "
SQL = SQL & "AND CI0100.CI01NUMCITA = CI2700.CI01NUMCITA "
SQL = SQL & "AND TO_CHAR(CI2700.CI27FECOCUPREC, 'DD/MM/YYYY') = ? " 'qryActCitadas(1) = SSMonth1
SQL = SQL & "AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
SQL = SQL & "AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA "
SQL = SQL & "AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI "
SQL = SQL & "AND PR0300.PR01CODACTUACION = PR0100.PR01CODACTUACION "
SQL = SQL & "AND CI01SITCITA <> 2 " 'Anulada
'ORDER BY
SQL = SQL & "ORDER BY "
SQL = SQL & "CI2700.CI27FECOCUPREC, CI2700.CI31NUMSOLICIT, CI2700.CI15NUMFASECITA"
    
Set qryActCitadas = Conexion.CreateQuery("ActCitadas", SQL)
    qryActCitadas(0) = lngCodRecurso
    qryActCitadas(1) = Format(datDia, "DD/MM/YYYY")
Set rstActCitadas = qryActCitadas.OpenResultset()

Do While Not rstFranjasRecurso.EOF 'Mientras haya franjas...
  'Si la franja tiene actividades planificables, se buscar�n las citas
  If rstFranjasRecurso(9) = -1 Then 'PR1200.PR12INDPLANIFIC

    Select Case rstFranjasRecurso(6) 'AG04MODASIGCITA
    
      Case 1 'Por cantidad
        If lngCodFranja <> rstFranjasRecurso(0) Then 'AG04CODFRANJA
            lngCodFranja = rstFranjasRecurso(0)
            bytNumCitasAdm = rstFranjasRecurso(8) 'AG04NUMCITADMI
            bytNumCitas = 0
        End If
        
        datHoraFin = rstFranjasRecurso(3) & ":" & rstFranjasRecurso(4)
        datHoraFin = Format(datHoraFin, "HH:MM:SS")
        If Not rstActCitadas.EOF Then
            Do While Not rstActCitadas.EOF 'Mientras haya citas...
                If Format(rstActCitadas(3), "HH:MM:SS") < Format(datHoraFin, "HH:MM:SS") Then
                    If rstActCitadas(5) > 0 Then bytNumCitas = bytNumCitas + 1
                Else
                    If bytNumCitas < bytNumCitasAdm Then fblnCitas = True
                    Exit Do
                End If
                
                rstActCitadas.MoveNext 'Siguiente cita
            Loop
            If fblnCitas = False And bytNumCitas < bytNumCitasAdm Then fblnCitas = True
        Else
            fblnCitas = True
        End If 'Not rstActCitadas.EOF
        If fblnCitas = True Then Exit Do
        
      Case 2, 4 'Secuencial o intervalos oscilantes
        
        Dim lngDurFranja As Long 'Duraci�n de la franja en minutos
        
        lngDurFranja = rstFranjasRecurso(3) * 60 + rstFranjasRecurso(4) _
                                        - rstFranjasRecurso(1) * 60 - rstFranjasRecurso(2)
    
        datHoraFin = rstFranjasRecurso(3) & ":" & rstFranjasRecurso(4)
        datHoraFin = Format(datHoraFin, "HH:MM:SS")
        
        If Not rstActCitadas.EOF Then
            Do While Not rstActCitadas.EOF 'Mientras haya citas...
            
                If Format(rstActCitadas(3), "HH:MM:SS") < Format(datHoraFin, "HH:MM:SS") Then
                    lngDurFranja = lngDurFranja - rstActCitadas(5)
                Else
                    If lngDurFranja > 5 Then fblnCitas = True
                    Exit Do
                End If
                
                rstActCitadas.MoveNext 'Siguiente cita
            Loop
        
        Else
            fblnCitas = True
        End If 'Not rstActCitadas.EOF
        
        If fblnCitas = True Then Exit Do
      Case 3 'Intervalos Predeterminados
        
        Dim bytLongNumCitas As Byte 'N�mero de Citas que ocupa esa Actuaci�n
        Dim intDurCita As Integer 'Duraci�n de las Citas (recurso) en minutos
        
        If lngCodFranja <> rstFranjasRecurso(0) Then 'AG04CODFRANJA
            lngCodFranja = rstFranjasRecurso(0)
            intDurCita = rstFranjasRecurso(7) 'AG04INTERVCITA
            bytNumCitasAdm = rstFranjasRecurso(8) 'AG04NUMCITADMI
            bytNumCitas = 0
        End If
        datHoraInicio = Format(rstFranjasRecurso(1) & ":" & rstFranjasRecurso(2), "HH:MM:SS")
        datHoraFin = Format(rstFranjasRecurso(3) & ":" & rstFranjasRecurso(4), "HH:MM:SS")
        
        If Not rstActCitadas.EOF Then
            Do While Not rstActCitadas.EOF 'Mientras haya citas...
               If Format(rstActCitadas(3), "HH:MM:SS") >= Format(datHoraInicio, "HH:MM:SS") Then
                If Format(rstActCitadas(3), "HH:MM:SS") < Format(datHoraFin, "HH:MM:SS") Then
                    'rstActCitadas(5) = 1440*CI27NUMDIASREC+60*CI27NUMHORAREC+CI27NUMMINUREC
                    bytLongNumCitas = rstActCitadas(5) \ intDurCita
                    'si rstActCitadas(5)=0, al menos hay esa cita ocupada
                    If rstActCitadas(5) Mod intDurCita > 0 Then _
                                                    bytLongNumCitas = bytLongNumCitas + 1
''                    If bytLongNumCitas = 0 Then
''                     bytLongNumCitas = 1
''                    End If
                    bytNumCitas = bytNumCitas + bytLongNumCitas
                Else
                    If bytNumCitas < bytNumCitasAdm Then fblnCitas = True
                    Exit Do
                End If
               End If
                rstActCitadas.MoveNext 'Siguiente cita
            Loop
            If fblnCitas = False And bytNumCitas < bytNumCitasAdm Then fblnCitas = True
        Else
            fblnCitas = True
        End If 'Not rstActCitadas.EOF
        If fblnCitas = True Then Exit Do
        
    End Select
    
  End If 'rstFranjasRecurso(9) = -1
  rstFranjasRecurso.MoveNext 'Siguiente franja
        
Loop

'Para el caso de que se acaban las citas en la �ltima franja
If fblnCitas = False And lngDurFranja > 5 Then fblnCitas = True
'Se cierra la consulta
qryActCitadas.Close
If Not rstActCitadas.ActiveConnection Is Nothing Then rstActCitadas.Close

End Function
