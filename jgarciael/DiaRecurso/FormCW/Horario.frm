VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "comdlg32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{35ED254B-6E76-11D2-A484-00C04F7D9E25}#1.0#0"; "horario.ocx"
Begin VB.Form PruebaHorario 
   Caption         =   "Form1"
   ClientHeight    =   8160
   ClientLeft      =   -45
   ClientTop       =   135
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8160
   ScaleWidth      =   11880
   WindowState     =   2  'Maximized
   Begin Control_Agenda.DiaRecurso DiaRecurso1 
      Height          =   7095
      Left            =   120
      TabIndex        =   5
      Top             =   960
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   12515
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      BackColor       =   &H00FF0000&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   735
      Left            =   4320
      TabIndex        =   0
      Top             =   60
      Width           =   1035
   End
   Begin VB.CommandButton cmdCodigo 
      Height          =   375
      Index           =   1
      Left            =   5340
      Picture         =   "Horario.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   420
      Width           =   375
   End
   Begin VB.CommandButton cmdCodigo 
      Height          =   375
      Index           =   0
      Left            =   5340
      Picture         =   "Horario.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   60
      Width           =   375
   End
   Begin ComctlLib.TabStrip TabStrip1 
      Height          =   7515
      Left            =   60
      TabIndex        =   1
      Top             =   600
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   13256
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   1
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   ""
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog DLGCommonDialog1 
      Left            =   7200
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6360
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin Crystal.CrystalReport CRCrystalReport1 
      Left            =   8040
      Top             =   840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo del recurso:"
      BeginProperty Font 
         Name            =   "Bookman Old Style"
         Size            =   18
         Charset         =   0
         Weight          =   300
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   900
      TabIndex        =   2
      Top             =   180
      Width           =   3435
   End
   Begin ComctlLib.ImageList IMLImageList1 
      Left            =   7440
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
End
Attribute VB_Name = "PruebaHorario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmdCodigo_Click(Index As Integer)
   Text1.SetFocus
   If Index = 0 Then
      If cmdCodigo(1).Enabled = False Then cmdCodigo(1).Enabled = True
      Text1.Text = Val(Text1.Text) + 1
   Else
      If (Val(Text1.Text) - 1) >= 1 Then
         Text1.Text = Val(Text1.Text) - 1
      Else
         Text1.Text = 1
         cmdCodigo(1).Enabled = False
      End If
   End If
End Sub

Private Sub Form_Load()
  
  Call InitApp
 
  With objApp
'    .blnReg = False
    Call .Register(App, Screen)
    Set .frmFormMain = Me
    Set .imlImageList = IMLImageList1
    Set .crCrystal = CRCrystalReport1
    Set .dlgCommon = DLGCommonDialog1
    .objUserColor.lngReadOnly = &HC0C0C0
    .objUserColor.lngMandatory = &HFFFF00
  End With
  
  With objApp
    .blnUseRegistry = True
    .strUserName = "cun"
    .strPassword = "jkeplr"
    .strDataSource = "Oracle73"
    .blnAskPassword = False
  End With
    
  If Not objApp.CreateInfo Then
    MsgBox ("Int�ntalo de nuevo")
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub Text1_Change()
   If Val(Text1.Text) <> 0 Then DiaRecurso1.pRepresentar Text1.Text, objApp.rdoConnect
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)
   
   If KeyCode = 38 Then cmdCodigo_Click 0 'Se ha pulsado la flecha abajo
   If KeyCode = 40 Then cmdCodigo_Click 1 'Se ha pulsado la flecha arriba
   
End Sub
