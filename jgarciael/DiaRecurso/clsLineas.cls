VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsLineas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private intLeft As Integer, lngTop As Long, blnPunteado As Boolean

Public Property Let Left(intLeftNuevo As Integer)
   intLeft = intLeftNuevo
End Property

Public Property Let Top(lngTopNuevo As Long)
   lngTop = lngTopNuevo
End Property

Public Property Let Punteado(blnPunteadoNuevo As Boolean)
   blnPunteado = blnPunteadoNuevo
End Property

Public Property Get Top() As Long
   Top = lngTop
End Property

Public Property Get Left() As Integer
   Left = intLeft
End Property

Public Property Get Punteado() As Boolean
   Punteado = blnPunteado
End Property

