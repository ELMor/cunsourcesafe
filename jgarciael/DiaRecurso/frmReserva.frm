VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmReserva 
   Caption         =   "RESERVA DE CITAS"
   ClientHeight    =   6255
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   6255
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAnular 
      Caption         =   "Anular reserva"
      Height          =   495
      Left            =   2400
      TabIndex        =   9
      Top             =   4920
      Width           =   1935
   End
   Begin VB.CommandButton cmdReReserva 
      Caption         =   "Realizar la reserva"
      Height          =   495
      Left            =   0
      TabIndex        =   3
      Top             =   4920
      Width           =   1935
   End
   Begin VB.TextBox txtActual 
      Appearance      =   0  'Flat
      BackColor       =   &H80000000&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   5760
      Width           =   4575
   End
   Begin ComctlLib.ListView lstCitasLibres 
      Height          =   1935
      Left            =   120
      TabIndex        =   1
      Top             =   1080
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   3413
      View            =   2
      Arrange         =   1
      Sorted          =   -1  'True
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Frame fraReserva 
      Caption         =   "Reservas"
      Height          =   4695
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4575
      Begin VB.TextBox txtText1 
         Height          =   1095
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   3480
         Width           =   4335
      End
      Begin ComctlLib.ListView lstReservas 
         Height          =   1935
         Left            =   2520
         TabIndex        =   8
         Top             =   1080
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   3413
         View            =   2
         Arrange         =   1
         Sorted          =   -1  'True
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbPrueba 
         Height          =   255
         Left            =   2400
         TabIndex        =   10
         Top             =   480
         Width           =   1815
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         BackColorEven   =   16776960
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3201
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbActividad 
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   480
         Width           =   1815
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         BackColorEven   =   16776960
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3201
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin VB.Label Label2 
         Caption         =   "Actividad"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Actuaci�n"
         Height          =   255
         Left            =   2400
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Reservas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   2520
         TabIndex        =   7
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Horas libres"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   3120
         Width           =   1695
      End
   End
End
Attribute VB_Name = "frmReserva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Huecos
'N�mero de D�as encontrados con Huecos disponibles (contador), para dejar de buscar al
'llegar al valor de la constante conDias
Dim bytDias As Byte
'Si hay hueco (TRUE), no sigue buscando cuando est� cargando los d�as con huecos libres...
Dim blnHueco As Boolean
Dim nodNodo As Node 'Nodo del TreeView(tvwcitlibres)
Dim lstItem As ListItem 'item de la lista
'Longitudes de los strings
Dim L1 As Integer
Dim L2 As Integer
Dim L3 As Integer
'EFS: PAra recorrer las lista
Dim strtexto As String
Dim strkey As String
'EFS: Variable para ver las actuaciones por franja
Dim strActuaciones As String
    
Private Sub pConvertMinutos(lngMin As Long, intDias As Integer, bytHoras As Byte, _
                                 bytMinutos As Byte)
Dim lngMinutosRestSinHoras As Long

intDias = Int(lngMin / 60 / 24)
lngMinutosRestSinHoras = lngMin - intDias * 24
bytHoras = Int(lngMinutosRestSinHoras / 60)
bytMinutos = lngMinutosRestSinHoras - bytHoras * 60

End Sub

Public Function flngNextClave(strCampo As String, strTabla As String) As Long

Dim rstClave As rdoResultset

On Error Resume Next
Err = 0
Set rstClave = Conexion.OpenResultset("SELECT MAX(" & strCampo & ") + 1 FROM " & strTabla)
If IsNull(rstClave(0)) Then flngNextClave = 1 Else flngNextClave = rstClave(0)
rstClave.Close
Set rstClave = Nothing
If Err > 0 Then flngNextClave = -1

End Function
Private Function fblnTipRec(intTipRec As Integer) As Boolean
Dim sql As String
Dim qryInsert As rdoQuery
Dim rstRespuesta As rdoResultset

'Se obtiene el Tipo de Recurso al que pertenece al Doctor
sql = "SELECT AG14CODTIPRECU FROM AG1100 WHERE AG11CODRECURSO = ?"
Set qryInsert = Conexion.CreateQuery("", sql)
   qryInsert(0) = strRecurso
Set rstRespuesta = qryInsert.OpenResultset()
If rstRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   intTipRec = rstRespuesta(0)
   rstRespuesta.MoveNext
   If Not rstRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
rstRespuesta.Close
qryInsert.Close
Set rstRespuesta = Nothing
Set qryInsert = Nothing
fblnTipRec = True

End Function
Private Function fstrFranjas() As String
                                                             
Dim sql As String

'SELECT
sql = "SELECT AG0400.AG04CODFRANJA, AG04HORINFRJHH, AG04HORINFRJMM, AG04HORFIFRJHH, "
sql = sql & "AG04HORFIFRJMM, PR1200.PR12DESACTIVIDAD, AG04MODASIGCITA, AG04INTERVCITA, "
sql = sql & "AG04NUMCITADMI, PR1200.PR12INDPLANIFIC, AG0400.AG07CODPERFIL,AG0100.AG01NUMASIGADM, "
'If SSDBCmbTipEc.Text <> "" Then SQL = SQL & "AG12INDINCEXCL, "
sql = sql & "SYSDATE "
'FROM
sql = sql & "FROM AG0400, PR1200, AG1200, AG0100 "
'If SSDBCmbTipEc.Text <> "" Then SQL = SQL & ", AG1200 "
'WHERE
sql = sql & "WHERE (AG04FECBAJA >= SYSDATE OR AG04FECBAJA IS NULL) AND " 'Franja activa
sql = sql & "(AG0400.PR12CODACTIVIDAD = ? OR AG0400.PR12CODACTIVIDAD IS NULL) AND " 'Consultas o lo que sea
sql = sql & "AG0400.PR12CODACTIVIDAD = PR1200.PR12CODACTIVIDAD(+) AND "
sql = sql & "AG0400.AG11CODRECURSO = ? AND AG0400.AG07CODPERFIL = " 'qryFranjasRec(0) = codRecurso
   '...para el perfil cuyo periodo de vigencia contenga a la fecha y...
   'SELECT
   sql = sql & "(SELECT AG0700.AG07CODPERFIL "
   'FROM
   sql = sql & "FROM AG0700, AG0900 "
   'WHERE
   sql = sql & "WHERE AG0900.AG11CODRECURSO = ? " 'qryFranjasRec(1) = codRecurso
   sql = sql & "AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO "
   sql = sql & "AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL "
   Select Case fstrBuscarDiaSemana(FechaActual)
       Case "Lunes"
           sql = sql & "AND AG04INDLUNFRJA = -1 "
       Case "Martes"
           sql = sql & "AND AG04INDMARFRJA = -1 "
       Case "Mi�rcoles"
           sql = sql & "AND AG04INDMIEFRJA = -1 "
       Case "Jueves"
           sql = sql & "AND AG04INDJUEFRJA = -1 "
       Case "Viernes"
           sql = sql & "AND AG04INDVIEFRJA = -1 "
       Case "S�bado"
           sql = sql & "AND AG04INDSABFRJA = -1 "
       Case "Domingo"
           sql = sql & "AND AG04INDDOMFRJA = -1 "
   End Select
   sql = sql & "AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG09FECINVIPER AND AG09FECFIVIPER "
   sql = sql & "AND AG07INDPERGENE ="
       '...si s�lo est� el gen�rico (-1) ser� ese, a no ser que haya uno m�s (0)...
       'SELECT
       sql = sql & "(SELECT MAX(AG07INDPERGENE) "
       'FROM
       sql = sql & "FROM AG0900, AG0700 " ', AG0400 "
       'WHERE
       sql = sql & "WHERE AG0900.AG11CODRECURSO = ? " 'qryFranjasRec(3) = codRecurso
       sql = sql & "AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO "
       sql = sql & "AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL "
'          SQL = SQL & "AND AG0400.AG11CODRECURSO = AG0700.AG11CODRECURSO "
'          SQL = SQL & "AND AG0400.AG07CODPERFIL = AG0700.AG07CODPERFIL "
       '...y cuya franja sea la del d�a seleccionado
       Select Case fstrBuscarDiaSemana(FechaActual)
           Case "Lunes"
              sql = sql & "AND AG04INDLUNFRJA = -1 "
           Case "Martes"
              sql = sql & "AND AG04INDMARFRJA = -1 "
           Case "Mi�rcoles"
              sql = sql & "AND AG04INDMIEFRJA = -1 "
           Case "Jueves"
              sql = sql & "AND AG04INDJUEFRJA = -1 "
           Case "Viernes"
              sql = sql & "AND AG04INDVIEFRJA = -1 "
           Case "S�bado"
              sql = sql & "AND AG04INDSABFRJA = -1 "
           Case "Domingo"
              sql = sql & "AND AG04INDDOMFRJA = -1 "
       End Select
       sql = sql & "AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG09FECINVIPER AND AG09FECFIVIPER "
       sql = sql & "AND (AG07FECBAJA >= TO_DATE(?, 'DD/MM/YYYY') OR AG07FECBAJA IS NULL)" 'qryFranjasRec(5) = SSMonth1.Date
       sql = sql & ")"
   sql = sql & ") "
'Filtro por actuaci�n
sql = sql & "AND AG0100.AG11CODRECURSO (+) = AG0400.AG11CODRECURSO AND "
sql = sql & "AG0100.AG07CODPERFIL (+) = AG0400.AG07CODPERFIL AND "
sql = sql & "AG0100.AG04CODFRANJA (+) = AG0400.AG04CODFRANJA AND "
sql = sql & "AG0100.PR01CODACTUACION = ? AND PR12INDPLANIFIC = -1 "
'Filtro por Tipo Econ�mico
'If SSDBCmbTipEc.Text <> "" Then
'   'Tipo de restricci�n por franja: Tipo Econ�mico
'   SQL = SQL & "AND AG0400.AG11CODRECURSO = AG1200.AG11CODRECURSO (+) AND "
'   SQL = SQL & "AG0400.AG07CODPERFIL = AG1200.AG07CODPERFIL (+) AND "
'   SQL = SQL & "AG0400.AG04CODFRANJA = AG1200.AG04CODFRANJA (+) "
'   SQL = SQL & "AND AG1200.AG16CODTIPREST (+) = 3 " 'Restric. Tipo Econ�mico
'   SQL = SQL & "AND AG12VALDESDERES (+) = ? AND AG12NIVELSUPER (+) = 0 "
'   SQL = SQL & "AND AG12INDINCEXCL (+) = 0" 'Inclusi�n
'Else
   sql = sql & "AND AG0400.AG11CODRECURSO = AG1200.AG11CODRECURSO (+) AND "
   sql = sql & "AG0400.AG07CODPERFIL = AG1200.AG07CODPERFIL (+) AND "
   sql = sql & "AG0400.AG04CODFRANJA = AG1200.AG04CODFRANJA (+) "
   sql = sql & "AND ((AG1200.AG16CODTIPREST  = '16' " 'Restric. Usuario
   sql = sql & "AND AG12VALDESDERES  = ? AND AG12INDINCEXCL=0)"
   sql = sql & "OR AG1200.AG16CODTIPREST IS NULL OR AG1200.AG16CODTIPREST <> '16') " 'Inclusi�n
'End If 'SSDBCmbTipEc.Text <> ""

fstrFranjas = sql

End Function
Private Sub pBuscar_HuecosDia()

'Se buscar�n citas entre las siguientes fechas de...
   '...inicio (formato)
   Dim HoraInicio As String 'Hora de inicio de la franja (00)
   Dim MinInicio As String 'Minuto de inicio de la franja (00)
   Dim FechaInicio As String 'Fecha total de Inicio (DD/MM/YYYY 00:00)
   '...fin (formato)
   Dim HoraFin As String 'Hora de fin de la franja (00)
   Dim MinFin As String 'Minuto de fin de la franja (00)
   Dim FechaFin As String 'Fecha total de Fin (DD/MM/YYYY 00:00)
'Cuando algunos campos son NULL...
Dim intNumCitasAdmi As Integer 'N�mero de citas admitidas
Dim bytModoAsig As Byte 'Modo de asignaci�n del recurso en esa franja
Dim intIntervCitas As Integer
'Consulta SQL
Dim sql As String
Dim qryResultado As rdoQuery
Dim rstResultado As rdoResultset


sql = fstrFranjas & " ORDER BY AG04HORINFRJHH, AG04HORINFRJMM" 'Se ordena cronol�gicamente
Set qryResultado = Conexion.CreateQuery("", sql)
   qryResultado(0) = SSDBCmbActividad.Columns(0).Value
   qryResultado(1) = strRecurso
   qryResultado(2) = qryResultado(1)
   qryResultado(3) = Format(FechaActual, "DD/MM/YYYY")
   qryResultado(4) = qryResultado(1)
   qryResultado(5) = qryResultado(3)
   qryResultado(6) = qryResultado(3)
   qryResultado(7) = SSDBCmbPrueba.Columns(0).Text
'        If SSDBCmbTipEc.Text <> "" Then
'        qryResultado(7) = SSDBCmbTipEc.Columns(0).Text
'     Else
        qryResultado(8) = strCodUsuario
'     End If
Set rstResultado = qryResultado.OpenResultset()
 
 If Not rstResultado.EOF Then 'Si hay franjas...
     Do While Not rstResultado.EOF 'Mientras halla franjas...
         If IsNull(rstResultado(8)) Then 'Si no hay citas admitidas...
             intNumCitasAdmi = 0 '...se establece como cero
         Else
             intNumCitasAdmi = rstResultado(8) 'AG04NUMCITADMI
         End If
         If IsNull(rstResultado(7)) Then
             intIntervCitas = 0
         Else
             intIntervCitas = rstResultado(7) 'AG04INTERVCITA
         End If
         If IsNull(rstResultado(6)) Then
             bytModoAsig = 0
         Else
             bytModoAsig = rstResultado(6) 'AG04MODASIGCITA
         End If
         'Para la franja, se mira las citas que tiene para obtener los huecos
         HoraInicio = rstResultado(1)
         If Len(HoraInicio) < 2 Then HoraInicio = "0" & HoraInicio
         MinInicio = rstResultado(2)
         If Len(MinInicio) < 2 Then MinInicio = "0" & MinInicio
         HoraFin = rstResultado(3)
         If Len(HoraFin) < 2 Then HoraFin = "0" & HoraFin
         MinFin = rstResultado(4)
         If Len(MinFin) < 2 Then MinFin = "0" & MinFin
        
         FechaInicio = Format(FechaActual & " " & HoraInicio & ":" & _
                                          MinInicio, "DD/MM/YYYY HH:MM:SS")
         FechaFin = Format(FechaActual & " " & HoraFin & ":" & MinFin, _
                                          "DD/MM/YYYY HH:MM:SS")
         Call pBuscar_CitasDia(FechaInicio, FechaFin, bytModoAsig, intNumCitasAdmi, _
                     intIntervCitas, rstResultado(11), rstResultado(10), rstResultado(0))
         rstResultado.MoveNext 'Siguiente franja
     Loop 'Not rstFranjasRec.EOF
End If
rstResultado.Close
qryResultado.Close

End Sub
'Busca las reservas para un recurso y un dia y las muestra
Private Sub pBuscar_ReservasDia()
Dim strSQL      As String
Dim qryRerser   As rdoQuery
Dim rstRerser   As rdoResultset
'Fecha de inicio y fin son el mismo dia pero jugamos con los minutos
Dim FechIni As String
Dim FechFin As String
'para sacar en pantalla la hora de la reserva
Dim strHora As String
'Para a�adir elementos a la lista
Dim lstItem2 As ListItem
Dim intI As Integer

FechIni = Format(FechaActual & " 00:00", "DD/MM/YYYY HH:MM:SS")
FechFin = Format(FechaActual & " 23:59", "DD/MM/YYYY HH:MM:SS")

strSQL = "SELECT CI01FECCONCERT,CI31NUMSOLICIT FROM CI0100 WHERE AG11CODRECURSO=?  "
strSQL = strSQL & "AND CI01SITCITA=5 AND TO_CHAR(CI01FECCONCERT, 'DD/MM/YYYY HH24:MI') >= ? "
strSQL = strSQL & "AND TO_CHAR(CI01FECCONCERT, 'DD/MM/YYYY HH24:MI') < ?"

FechIni = Format(FechaActual & " 00:00", "DD/MM/YYYY HH:MM:SS")
FechFin = Format(FechaActual & " 23:59", "DD/MM/YYYY HH:MM:SS")

Set qryRerser = Conexion.CreateQuery("Reservas", strSQL)
   qryRerser(0) = strRecurso
   qryRerser(1) = Format(FechIni, "DD/MM/YYYY HH:MM")
   qryRerser(2) = Format(FechFin, "DD/MM/YYYY HH:MM")
'EFS: Con esta consulta obtenemos la reservas realizadas en
'FechaActual para ese recurso
Set rstRerser = qryRerser.OpenResultset(rdOpenKeyset)
intI = 1
Do While Not rstRerser.EOF
    Set lstItem2 = lstReservas.ListItems.Add(intI, "R" & rstRerser(1), Format(rstRerser(0), "HH:MM"))
    intI = intI + 1
    rstRerser.MoveNext
Loop
rstRerser.Close
qryRerser.Close
End Sub
'Busca las citas para un recurso y un d�a
Private Sub pBuscar_CitasDia(Inicio As String, Fin As String, Asign As Byte, Citas As Integer, _
      Intervalo As Integer, AsignCitas As Byte, intPerfil As Integer, intFranja As Integer)

'''''''''''''''''''DECLARACI�N DE VARIABLES''''''''''''''''''''''''''''''''
Dim intCitas As Integer
Dim intCitasAct As Integer
Dim intHuecos As Integer
Dim intHuecosAct As Integer
Dim InicioCita As Date
Dim FinCita As Date
'Niveles de Restricciones
Dim strPadre As String
Dim strHijo As String
Dim strNieto As String
Dim strPadreAnt As String
Dim strHijoAnt As String
Dim strNietoAnt As String
Dim bytNivel As Byte
Dim strkey As String
Dim bytImagen As Byte
'Codigo de la actuacion
Dim strCodActuac As String
'Consultas SQL...
Dim sql As String 'Consulta SQL
Dim qryActCitadas As rdoQuery 'Objeto de la consulta
Dim rstActCitadas As rdoResultset 'Resultado de la consulta
Dim qryRestr As rdoQuery
Dim rstRestr As rdoResultset
'Contador de huecos cuando asignamos por cantidad
Dim intI As Integer
'Indice de las horas
Dim intHoras As Integer

intHoras = 1
'Se obtienen los datos de las citas para ese recurso y ese d�a
'SELECT
sql = "SELECT CI2700.CI31NUMSOLICIT, CI2700.CI01NUMCITA, CI2700.CI15NUMFASECITA, "
sql = sql & "TO_CHAR(CI2700.CI27FECOCUPREC,'DD/MM/YYYY HH24:MI:SS'), "
sql = sql & "TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YYYY HH24:MI:SS'), "
'Minutos de ocupaci�n del recurso
sql = sql & "1440*CI27NUMDIASREC+60*CI27NUMHORAREC+CI27NUMMINUREC, PR0300.PR01CODACTUACION "
'FROM
sql = sql & "FROM CI2700, CI0100, CI2200, PR0400, PR0300 " ', PR0100 "
'WHERE
sql = sql & "WHERE CI2700.AG11CODRECURSO = ? " 'qryActCitadas(0) = codRecurso
sql = sql & "AND CI0100.CI31NUMSOLICIT = CI2700.CI31NUMSOLICIT "
sql = sql & "AND CI0100.CI01NUMCITA = CI2700.CI01NUMCITA "
'SQL = SQL & "AND (TO_CHAR(CI2700.CI27FECOCUPREC, 'DD/MM/YYYY HH24:MI') BETWEEN ? AND ?) "
sql = sql & "AND TO_CHAR(CI2700.CI27FECOCUPREC, 'DD/MM/YYYY HH24:MI') >= ? "
sql = sql & "AND TO_CHAR(CI2700.CI27FECOCUPREC, 'DD/MM/YYYY HH24:MI') < ? "
sql = sql & "AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
sql = sql & "AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA "
sql = sql & "AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI "
''SQL = SQL & "AND PR0300.PR01CODACTUACION = PR0100.PR01CODACTUACION "
sql = sql & "AND (CI01SITCITA = 1 OR CI01SITCITA = 4 OR CI01SITCITA= 5)"
'ORDER BY
sql = sql & "ORDER BY "
sql = sql & "CI2700.CI27FECOCUPREC, CI2700.CI31NUMSOLICIT, CI2700.CI15NUMFASECITA"
    
Set qryActCitadas = Conexion.CreateQuery("ActCitadas", sql)
   qryActCitadas(0) = strRecurso
   qryActCitadas(1) = Format(Inicio, "DD/MM/YYYY HH:MM")
   qryActCitadas(2) = Format(Fin, "DD/MM/YYYY HH:MM")
Set rstActCitadas = qryActCitadas.OpenResultset(rdOpenKeyset)

Select Case Asign 'Modo de asignaci�n de la franja
   Case 1, 2 'Por cantidad, 'Secuencial
      Do While Not rstActCitadas.EOF 'Mientras haya citas...
         If rstActCitadas(5) > 0 Then 'Citas NO manuales
            intCitas = intCitas + 1
            If rstActCitadas(6) = SSDBCmbPrueba.Columns(0).Text Then _
                                                            intCitasAct = intCitasAct + 1
         End If 'rstActCitadas(5) > 0
         rstActCitadas.MoveNext 'Siguiente cita
      Loop
      'Primero se mira el n�mero total de consultas en esa franja...
      intHuecos = Citas - intCitas
      If intHuecos > 0 Then
         '...y luego el n�mero de consultas pertenecientes al grupo escogido
         intHuecos = AsignCitas - intCitasAct
         strkey = "R" & strRecurso & "D" & FechaActual & "H" & Inicio
         If intHuecos > 0 Then
'''            Set nodNodo = tvwCitLibres.Nodes.Add("R" & strRecurso & _
'''                              "D" & FechaActual, tvwNext, strKey, _
'''                              Format(Inicio, "HH:MM") & _
'''                              " (" & intHuecos & ")")
''            Set nodNodo = tvwCitLibres.Nodes.Add(, , "H" & Inicio, _
''            Format(Inicio, "HH:MM") & "  (" & intHuecos & ")")
             For intI = 1 To intHuecos
             strCodActuac = factFranj(intPerfil, intFranja)
                Set lstItem = lstCitasLibres.ListItems.Add(intHoras, "H" & Inicio & intI & "S" & strCodActuac, Format(Inicio, "HH:MM") & "  (" & intI & ")" & strActuaciones)
                intHoras = intHoras + 1
             Next
'            Call pCargar_Restricciones(intPerfil, intFranja, strKey)
         End If 'intHuecos > 0 (del grupo)
      End If 'intHuecos > 0 (totales de la franja)
   Case 3, 4 'Interv. Predet.
      If Not rstActCitadas.EOF Then rstActCitadas.MoveFirst
      Do While Not rstActCitadas.EOF 'Mientras haya citas...
         If rstActCitadas(5) > 0 Then 'Citas NO manuales
            intCitas = intCitas + 1
            intCitasAct = intCitasAct + 1
         End If 'rstActCitadas(5) > 0
         rstActCitadas.MoveNext 'Siguiente cita
      Loop
      If intCitas > 0 Then rstActCitadas.MoveFirst
      'Primero se mira el n�mero total de consultas en esa franja...
      intHuecos = Citas - intCitas
      If intHuecos > 0 Then
         '...y luego el n�mero de consultas pertenecientes al grupo escogido
         intHuecosAct = AsignCitas - intCitasAct
         If intHuecos < intHuecosAct Then intHuecosAct = intHuecos
         If intHuecosAct > 0 Then
            InicioCita = Inicio
            FinCita = DateAdd("N", Intervalo, InicioCita)
            If Not rstActCitadas.EOF Then
               Do While Not rstActCitadas.EOF 'Mientras haya citas...
                 If rstActCitadas(5) > 0 Then 'Citas NO manuales
                  If intHuecos = 0 Then Exit Do
                  If rstActCitadas(3) >= Format(InicioCita, _
                                             "DD/MM/YYYY HH:MM:SS") Then
                      If Not rstActCitadas(3) < Format(FinCita, _
                                             "DD/MM/YYYY HH:MM:SS") Then
                         strkey = "R" & strRecurso & "D" & FechaActual & "H" & InicioCita
                         'Se a�ade el hueco
''                        Set nodNodo = tvwCitLibres.Nodes.Add(, , "H" & InicioCita, _
''                                             Format(InicioCita, "HH:MM"), conImgHora)
                        strCodActuac = factFranj(intPerfil, intFranja)
                        Set lstItem = lstCitasLibres.ListItems.Add(intHoras, "H" & InicioCita & "S" & strCodActuac, Format(InicioCita, "HH:MM") & strActuaciones)
                         intHoras = intHoras + 1
                         intHuecos = intHuecos - 1
'                         Call pCargar_Restricciones(intPerfil, intFranja, strKey)
                      Else
                         'Siguiente cita
                         rstActCitadas.MoveNext
                         Citas = Citas - 1
                      End If
                      'Siguiente porci�n
                      InicioCita = FinCita
                      FinCita = DateAdd("N", Intervalo, InicioCita)
                   Else
                      'Cita que tambi�n pertenece a la porci�n anterior a la actual
                      rstActCitadas.MoveNext
                      Citas = Citas - 1
                   End If
                  Else
                     rstActCitadas.MoveNext
                  End If 'rstActCitadas(5) > 0
               Loop
               'Libre la �ltima porci�n
               Do While intHuecos > 0
                  strkey = "R" & strRecurso & "D" & FechaActual & "H" & InicioCita
                  'Se a�ade el hueco
''                  Set nodNodo = tvwCitLibres.Nodes.Add("R" & strRecurso & "D" & FechaActual, _
''                              tvwChild, strKey, objGen.ReplaceStr(Format(InicioCita, _
''                              "HH:MM"), ".", ":", 0), conImgHora)
''            Set nodNodo = tvwCitLibres.Nodes.Add(, , "H" & InicioCita, _
''                                             Format(InicioCita, "HH:MM"), conImgHora)
                  strCodActuac = factFranj(intPerfil, intFranja)
                  Set lstItem = lstCitasLibres.ListItems.Add(intHoras, "H" & InicioCita & "S" & strCodActuac, Format(InicioCita, "HH:MM") & strActuaciones)
                  intHoras = intHoras + 1
                  intHuecos = intHuecos - 1
'                  Call pCargar_Restricciones(intPerfil, intFranja, strKey)
                  'Siguiente porci�n
                  InicioCita = FinCita
                  FinCita = DateAdd("N", Intervalo, InicioCita)
                  If Format(FinCita, "DD/MM/YYYY hh:mm") > Format(Fin, "DD/MM/YYYY hh:mm") Then Exit Do
               Loop 'Citas > 0
            Else 'Si no hay citas, se a�aden tantos huecos como citas admitidas
               Do While intHuecos > 0
                  strkey = "R" & strRecurso & "D" & FechaActual & "H" & InicioCita
                  'Se a�ade el hueco
''                    Set nodNodo = tvwCitLibres.Nodes.Add(, , "H" & InicioCita, _
''                                             Format(InicioCita, "HH:MM"), conImgHora)
                    strCodActuac = factFranj(intPerfil, intFranja)
                  Set lstItem = lstCitasLibres.ListItems.Add(intHoras, "H" & InicioCita & "S" & strCodActuac, Format(InicioCita, "HH:MM") & strActuaciones)
                  intHoras = intHoras + 1
                  intHuecos = intHuecos - 1
'''                  Call pCargar_Restricciones(intPerfil, intFranja, strKey)
                  'Siguiente porci�n
                  InicioCita = FinCita
                  FinCita = DateAdd("N", Intervalo, InicioCita)
'                  If FinCita > Fin Then Exit Do
                   If DateDiff("s", FinCita, Fin) < 0 Then Exit Do
               Loop
            End If 'Not rstActCitadas.EOF
         End If 'intHuecos > 0 (prueba seleccionada)
      End If 'intHuecos > 0 (totales)
End Select 'Asign

rstActCitadas.Close
qryActCitadas.Close

End Sub
'Queremos anular una o varias reservas
Private Sub cmdAnular_Click()
Dim lstAnul As ListItem
Dim intInd As Integer
Dim intL1 As Integer
Dim intL2 As Integer
Dim strSol As String
Dim intIndice As Integer

cmdAnular.Enabled = False
Screen.MousePointer = vbHourglass
txtActual.Text = "Anulando.."
For intIndice = 1 To lstReservas.ListItems.Count
    If lstReservas.ListItems(intIndice).Selected = True Then
    intL1 = InStr(lstReservas.ListItems(intIndice).Key, "R")
    intL2 = Len(lstReservas.ListItems(intIndice).Key)
    intL2 = intL2 - intL1
    strSol = Right(lstReservas.ListItems(intIndice).Key, intL2)
    If blnAnular(strSol) Then
            txtActual = "Anulacion completada"
        Else
            MsgBox "Se ha producido un error al Anular. Por favor, int�ntelo de nuevo", _
            vbCritical, "Busque de nuevo los huecos"
        End If 'fblnAnular
    End If 'lstResrvas...
Next
lstCitasLibres.ListItems.Clear
Call pBuscar_HuecosDia
lstReservas.ListItems.Clear
Call pBuscar_ReservasDia
cmdAnular.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdReReserva_Click()
Dim intIndice As Integer 'EFS: Para recorrer las lista
Dim lstSelec As ListItem

cmdReReserva.Enabled = False
Screen.MousePointer = vbHourglass
txtActual.Text = "Comprobando..."
For intIndice = 1 To lstCitasLibres.ListItems.Count
    If lstCitasLibres.ListItems(intIndice).Selected = True Then
        strtexto = Left(lstCitasLibres.ListItems(intIndice).Text, 5)
        strkey = lstCitasLibres.ListItems(intIndice).Key
        If fblnComprobar_Hueco Then 'Comprobar que el hueco sigue libre
           txtActual.Text = "Reservando..."
           If fblnCitar Then 'Citar
              txtActual.Text = "Reserva Realizada"
           Else
              MsgBox "Se ha producido un error al Reservar. Por favor, int�ntelo de nuevo", _
                       vbCritical, "Busque de nuevo los huecos"
           End If 'fblnCitar
        Else
           MsgBox "Esta hora ha sido citada. Si quiere reservarla debe Anular la cita"
        End If 'fblnComprobar_Hueco
    End If
Next
lstCitasLibres.ListItems.Clear

Call pBuscar_HuecosDia
lstReservas.ListItems.Clear
Call pBuscar_ReservasDia
cmdReReserva.Enabled = True
Screen.MousePointer = vbDefault
End Sub
Private Function factFranj(intP As Integer, intF As Integer) As String
Dim strSQL As String
Dim qryAct As rdoQuery
Dim rstAct As rdoResultset
'En esta funcion sacamos el codigo de actuacion que podemos citar
    strSQL = "SELECT AG0100.PR01CODACTUACION,PR01DESCORTA FROM AG0100,PR0100 WHERE "
    strSQL = strSQL & "AG0100.PR01CODACTUACION = PR0100.PR01CODACTUACION AND "
    strSQL = strSQL & "AG11CODRECURSO = ? AND "
    strSQL = strSQL & "AG07CODPERFIL = ? AND "
    strSQL = strSQL & "AG04CODFRANJA = ?  "
    Set qryAct = Conexion.CreateQuery("Actfranj", strSQL)
    qryAct(0) = strRecurso
    qryAct(1) = intP
    qryAct(2) = intF
    Set rstAct = qryAct.OpenResultset(rdOpenKeyset)
    strActuaciones = " "
    If Not rstAct.EOF Then
        factFranj = rstAct(0).Value

        Do While Not rstAct.EOF
            strActuaciones = strActuaciones & " " & Left(rstAct(1), 1)
            rstAct.MoveNext
        Loop
        rstAct.Close
        qryAct.Close
    Else
        rstAct.Close
        qryAct.Close
        'Si puedo hacer todas las actuaciones en es franja cojo por defecto la primera
        'consulta definida
       strSQL = "SELECT PR0100.PR01CODACTUACION FROM PR0100,PR0200,AG1100 WHERE "
       strSQL = strSQL & "AG1100.AG11CODRECURSO = ? AND "
       strSQL = strSQL & "AG1100.AG02CODDPTO = PR0200.AG02CODDPTO AND "
       strSQL = strSQL & "PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION AND "
       strSQL = strSQL & "PR0100.PR12CODACTIVIDAD = '201'"
       Set qryAct = Conexion.CreateQuery("Actfranj", strSQL)
       qryAct(0) = strRecurso
       Set rstAct = qryAct.OpenResultset(rdOpenKeyset)
       If Not rstAct.EOF Then
        factFranj = rstAct(0).Value
       Else
        MsgBox "NO ENCUENTRO CONSULTA PARA CITAR LA RESERVA", vbCritical
        factFranj = "NOCONSULTA"
       End If
    End If
End Function
Private Function fgetDpto(strCodRe As String) As Long
Dim strSQL As String
Dim qryDto As rdoQuery
Dim rstDto As rdoResultset

strSQL = "SELECT AD02CODDPTO FROM AG1100 WHERE AG11CODRECURSO = ?"
Set qryDto = Conexion.CreateQuery("departamento", strSQL)
qryDto(0) = strCodRe
Set rstDto = qryDto.OpenResultset(rdOpenKeyset)
fgetDpto = rstDto(0).Value
End Function
Private Function fblnComprobar_Hueco() As Boolean
'Franjas
Dim FechaInicio As String
Dim HoraInicio As String
Dim MinInicio As String
Dim FechaFin As String
Dim HoraFin As String
Dim MinFin As String
'Cuando algunos campos son NULL...
Dim intNumCitasAdmi As Integer 'N�mero de citas admitidas
Dim bytModoAsig As Byte 'Modo de asignaci�n del recurso en esa franja
Dim intIntervCitas As Integer

'Consulta SQL
Dim sql As String
Dim qryResultado As rdoQuery
Dim rstResultado As rdoResultset
Dim qryActCitadas As rdoQuery
Dim rstActCitadas As rdoResultset



'Se obtienen los datos de las franjas para ese recurso y...
'SELECT
sql = "SELECT AG0400.AG04CODFRANJA, AG04HORINFRJHH, AG04HORINFRJMM, AG04HORFIFRJHH, "
sql = sql & "AG04HORFIFRJMM, PR1200.PR12DESACTIVIDAD, AG04MODASIGCITA, AG04INTERVCITA, "
sql = sql & "AG04NUMCITADMI, PR1200.PR12INDPLANIFIC, AG0400.AG07CODPERFIL, AG01NUMASIGADM "
'FROM
 sql = sql & "FROM AG0400, PR1200, AG0100 "
'If SSDBCmbTipEc.Text <> "" Then SQL = SQL & ", AG1200 "
'WHERE
sql = sql & "WHERE (AG04FECBAJA >= SYSDATE OR AG04FECBAJA IS NULL) AND " 'Franja activa
sql = sql & "(AG0400.PR12CODACTIVIDAD = ? OR AG0400.PR12CODACTIVIDAD IS NULL) AND " 'Consultas o lo que sea
sql = sql & "AG0400.PR12CODACTIVIDAD = PR1200.PR12CODACTIVIDAD(+) AND "
sql = sql & "AG0400.AG11CODRECURSO = ? AND AG0400.AG07CODPERFIL = " 'qryFranjasRec(0) = codRecurso
   '...para el perfil cuyo periodo de vigencia contenga a la fecha y...
   'SELECT
   sql = sql & "(SELECT AG0700.AG07CODPERFIL "
   'FROM
   sql = sql & "FROM AG0700, AG0900 "
   'WHERE
   sql = sql & "WHERE AG0900.AG11CODRECURSO = ? " 'qryFranjasRec(1) = codRecurso
   sql = sql & "AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO "
   sql = sql & "AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL "
   Select Case fstrBuscarDiaSemana(FechaActual)
       Case "Lunes"
           sql = sql & "AND AG04INDLUNFRJA = -1 "
       Case "Martes"
           sql = sql & "AND AG04INDMARFRJA = -1 "
       Case "Mi�rcoles"
           sql = sql & "AND AG04INDMIEFRJA = -1 "
       Case "Jueves"
           sql = sql & "AND AG04INDJUEFRJA = -1 "
       Case "Viernes"
           sql = sql & "AND AG04INDVIEFRJA = -1 "
       Case "S�bado"
           sql = sql & "AND AG04INDSABFRJA = -1 "
       Case "Domingo"
           sql = sql & "AND AG04INDDOMFRJA = -1 "
   End Select
   sql = sql & "AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG09FECINVIPER AND AG09FECFIVIPER "
   sql = sql & "AND AG07INDPERGENE ="
       '...si s�lo est� el gen�rico (-1) ser� ese, a no ser que haya uno m�s (0)...
       'SELECT
       sql = sql & "(SELECT MAX(AG07INDPERGENE) "
       'FROM
       sql = sql & "FROM AG0900, AG0700 " ', AG0400 "
       'WHERE
       sql = sql & "WHERE AG0900.AG11CODRECURSO = ? " 'qryFranjasRec(3) = codRecurso
       sql = sql & "AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO "
       sql = sql & "AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL "
'          SQL = SQL & "AND AG0400.AG11CODRECURSO = AG0700.AG11CODRECURSO "
'          SQL = SQL & "AND AG0400.AG07CODPERFIL = AG0700.AG07CODPERFIL "
       '...y cuya franja sea la del d�a seleccionado
       Select Case fstrBuscarDiaSemana(FechaActual)
           Case "Lunes"
              sql = sql & "AND AG04INDLUNFRJA = -1 "
           Case "Martes"
              sql = sql & "AND AG04INDMARFRJA = -1 "
           Case "Mi�rcoles"
              sql = sql & "AND AG04INDMIEFRJA = -1 "
           Case "Jueves"
              sql = sql & "AND AG04INDJUEFRJA = -1 "
           Case "Viernes"
              sql = sql & "AND AG04INDVIEFRJA = -1 "
           Case "S�bado"
              sql = sql & "AND AG04INDSABFRJA = -1 "
           Case "Domingo"
              sql = sql & "AND AG04INDDOMFRJA = -1 "
       End Select
       sql = sql & "AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG09FECINVIPER AND AG09FECFIVIPER "
       sql = sql & "AND (AG07FECBAJA >= TO_DATE(?, 'DD/MM/YYYY') OR AG07FECBAJA IS NULL)" 'qryFranjasRec(5) = SSMonth1.Date
       sql = sql & ")"
   sql = sql & ") "
'Filtro por actuaci�n
sql = sql & "AND AG0100.AG11CODRECURSO (+) = AG0400.AG11CODRECURSO AND "
sql = sql & "AG0100.AG07CODPERFIL (+) = AG0400.AG07CODPERFIL AND "
sql = sql & "AG0100.AG04CODFRANJA (+) = AG0400.AG04CODFRANJA AND "
sql = sql & "AG0100.PR01CODACTUACION = ? AND PR12INDPLANIFIC = -1 "
'Filtro de la hora de la cita
sql = sql & "AND (AG04HORINFRJHH < ? OR (AG04HORINFRJHH = ? AND AG04HORINFRJMM <= ?)) "
sql = sql & "AND (AG04HORFIFRJHH > ? OR (AG04HORFIFRJHH = ? AND AG04HORFIFRJMM > ?)) "

L1 = Len(strkey)
L2 = InStr(strkey, "S")
L3 = L1 - L2

sql = sql & "ORDER BY AG04HORINFRJHH, AG04HORINFRJMM" 'Se ordena cronol�gicamente
   
Set qryResultado = Conexion.CreateQuery("", sql)
   qryResultado(0) = SSDBCmbActividad.Columns(0).Value
   qryResultado(1) = strRecurso
   qryResultado(2) = qryResultado(1)
   qryResultado(3) = Format(FechaActual, "DD/MM/YYYY")
   qryResultado(4) = qryResultado(1)
   qryResultado(5) = qryResultado(3)
   qryResultado(6) = qryResultado(3)
   qryResultado(7) = Right(strkey, L3) 'Actuacion
   qryResultado(8) = Left(strtexto, 2) 'Hora
   qryResultado(9) = qryResultado(8)
   qryResultado(10) = Mid(strtexto, 4, 2) 'Minuto
   qryResultado(11) = Val(qryResultado(9))
   qryResultado(12) = Val(qryResultado(9))
   qryResultado(13) = qryResultado(10)
'   If SSDBCmbTipEc.Text <> "" Then qryResultado(13) = SSDBCmbTipEc.Columns(0).Text
    Set rstResultado = qryResultado.OpenResultset()
  
   If Not rstResultado.EOF Then 'Si hay franjas...
      If IsNull(rstResultado(8)) Then 'Si no hay citas admitidas...
          intNumCitasAdmi = 0 '...se establece como cero
      Else
          intNumCitasAdmi = rstResultado(8) 'AG04NUMCITADMI
      End If
      If IsNull(rstResultado(7)) Then
          intIntervCitas = 0
      Else
          intIntervCitas = rstResultado(7) 'AG04INTERVCITA
      End If
      If IsNull(rstResultado(6)) Then
          bytModoAsig = 0
      Else
          bytModoAsig = rstResultado(6) 'AG04MODASIGCITA
      End If
      'Para la franja, se mira las citas que tiene para obtener los huecos
      HoraInicio = rstResultado(1)
      If Len(HoraInicio) < 2 Then HoraInicio = "0" & HoraInicio
      MinInicio = rstResultado(2)
      If Len(MinInicio) < 2 Then MinInicio = "0" & MinInicio
      HoraFin = rstResultado(3)
      If Len(HoraFin) < 2 Then HoraFin = "0" & HoraFin
      MinFin = rstResultado(4)
      If Len(MinFin) < 2 Then MinFin = "0" & MinFin
      FechaInicio = Format(FechaActual & " " & HoraInicio & ":" & MinInicio, _
                                       "DD/MM/YYYY HH:MM:SS")
      FechaFin = Format(FechaActual & " " & HoraFin & ":" & MinFin, _
                                       "DD/MM/YYYY HH:MM:SS")
      If fblnComprobar_Cita(FechaInicio, FechaFin, bytModoAsig, intNumCitasAdmi, _
            intIntervCitas, rstResultado(11), rstResultado(10), rstResultado(0)) Then _
            fblnComprobar_Hueco = True
   End If 'rstResultado.EOF
rstResultado.Close
qryResultado.Close

End Function
Private Function fblnRecFaseAct(lngCodActuacion As Long, lngNumNecesid As Long, _
                                 lngTiemRec As Long, intIndPlan As Integer) As Boolean
Dim sql As String
Dim QyInsert As rdoQuery
Dim RsRespuesta As rdoResultset

sql = "SELECT PR13NUMNECESID, PR13NUMTIEMPREC, PR13INDPLANIF FROM PR1300 WHERE "
sql = sql & "PR01CODACTUACION = ? AND PR13INDPREFEREN = -1 AND PR13INDPLANIF = -1"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = lngCodActuacion
Set RsRespuesta = QyInsert.OpenResultset()
If RsRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   lngNumNecesid = RsRespuesta(0)
   lngTiemRec = RsRespuesta(1)
   intIndPlan = RsRespuesta(2)
   RsRespuesta.MoveNext
   If Not RsRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
RsRespuesta.Close
QyInsert.Close
Set RsRespuesta = Nothing
Set QyInsert = Nothing
fblnRecFaseAct = True

End Function
Private Function fblnComprobar_Cita(Inicio As String, Fin As String, Asign As Byte, _
         Citas As Integer, Intervalo As Integer, AsignCitas As Byte, intPerfil As Integer, _
         intFranja As Integer) As Boolean

Dim intCitas As Integer
Dim intCitasAct As Integer
Dim intHuecos As Integer
Dim intHuecosAct As Integer
Dim InicioCita As Date
Dim FinCita As Date
'Consultas SQL...
Dim sql As String 'Consulta SQL
Dim qryActCitadas As rdoQuery 'Objeto de la consulta
Dim rstActCitadas As rdoResultset 'Resultado de la consulta

'Se obtienen los datos de las citas para ese recurso y ese d�a
'SELECT
sql = "SELECT CI2700.CI31NUMSOLICIT, CI2700.CI01NUMCITA, CI2700.CI15NUMFASECITA, "
sql = sql & "TO_CHAR(CI2700.CI27FECOCUPREC,'DD/MM/YYYY HH24:MI:SS'), "
sql = sql & "TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YYYY HH24:MI:SS'), "
'Minutos de ocupaci�n del recurso
sql = sql & "1440*CI27NUMDIASREC+60*CI27NUMHORAREC+CI27NUMMINUREC, PR0300.PR01CODACTUACION "
'FROM
sql = sql & "FROM CI2700, CI0100, CI2200, PR0400, PR0300 " ', PR0100 "
'WHERE
sql = sql & "WHERE CI2700.AG11CODRECURSO = ? " 'qryActCitadas(0) = codRecurso
sql = sql & "AND CI0100.CI31NUMSOLICIT = CI2700.CI31NUMSOLICIT "
sql = sql & "AND CI0100.CI01NUMCITA = CI2700.CI01NUMCITA "
'SQL = SQL & "AND (TO_CHAR(CI2700.CI27FECOCUPREC, 'DD/MM/YYYY HH24:MI') BETWEEN ? AND ?) "
sql = sql & "AND TO_CHAR(CI2700.CI27FECOCUPREC, 'DD/MM/YYYY HH24:MI') >= ? "
sql = sql & "AND TO_CHAR(CI2700.CI27FECOCUPREC, 'DD/MM/YYYY HH24:MI') < ? "
sql = sql & "AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
sql = sql & "AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA "
sql = sql & "AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI "
''SQL = SQL & "AND PR0300.PR01CODACTUACION = PR0100.PR01CODACTUACION "
sql = sql & "AND (CI01SITCITA = 1 OR CI01SITCITA = 4 OR CI01SITCITA = 5)"
'ORDER BY
sql = sql & "ORDER BY "
sql = sql & "CI2700.CI27FECOCUPREC, CI2700.CI31NUMSOLICIT, CI2700.CI15NUMFASECITA"
    
Set qryActCitadas = Conexion.CreateQuery("ActCitadas", sql)
   qryActCitadas(0) = strRecurso
   qryActCitadas(1) = Format(Inicio, "DD/MM/YYYY HH:MM")
   qryActCitadas(2) = Format(Fin, "DD/MM/YYYY HH:MM")
Set rstActCitadas = qryActCitadas.OpenResultset(rdOpenKeyset)

Select Case Asign 'Modo de asignaci�n de la franja
   Case 1, 2 'Por cantidad, 'Secuencial
      Do While Not rstActCitadas.EOF 'Mientras haya citas...
         If rstActCitadas(5) > 0 Then 'Citas NO manuales
            intCitas = intCitas + 1
            If rstActCitadas(6) = Right(strkey, L3) Then _
                                                            intCitasAct = intCitasAct + 1
         End If 'rstActCitadas(5) > 0
         rstActCitadas.MoveNext 'Siguiente cita
      Loop
      'Primero se mira el n�mero total de consultas en esa franja...
      intHuecos = Citas - intCitas
      If intHuecos > 0 Then
         '...y luego el n�mero de consultas pertenecientes al grupo escogido
         intHuecos = AsignCitas - intCitasAct
         If intHuecos > 0 Then fblnComprobar_Cita = True
      End If 'intHuecos > 0 (totales de la franja)
   Case 3, 4 'Interv. Predet. y oscilantes
      If Not rstActCitadas.EOF Then rstActCitadas.MoveFirst
      Do While Not rstActCitadas.EOF 'Mientras haya citas...
         If rstActCitadas(5) > 0 Then 'Citas NO manuales
            intCitas = intCitas + 1
            If rstActCitadas(6) = Right(strkey, L3) Then _
                                                            intCitasAct = intCitasAct + 1
         End If 'rstActCitadas(5) > 0
         rstActCitadas.MoveNext 'Siguiente cita
      Loop
      If intCitas > 0 Then rstActCitadas.MoveFirst
      'Primero se mira el n�mero total de consultas en esa franja...
      intHuecos = Citas - intCitas
      If intHuecos > 0 Then
         '...y luego el n�mero de consultas pertenecientes al grupo escogido
         intHuecosAct = AsignCitas - intCitasAct
         If intHuecos < intHuecosAct Then intHuecosAct = intHuecos
         If intHuecosAct > 0 Then fblnComprobar_Cita = True
      End If 'intHuecos > 0 (totales)
End Select 'Asign

rstActCitadas.Close
qryActCitadas.Close

End Function
Private Function blnAnular(Solicit As String) As Boolean
Dim sql As String
Dim strActPlan As String
Dim QyAnular As rdoQuery
Dim RsAnular As rdoResultset
On Error Resume Next
Err = 0

sql = "SELECT PR04NUMACTPLAN FROM CI0100 WHERE CI31NUMSOLICIT = ?"
Set QyAnular = Conexion.CreateQuery("", sql)
   QyAnular(0) = Solicit
Set RsAnular = QyAnular.OpenResultset()
If Not RsAnular.EOF Then
    strActPlan = RsAnular(0).Value
End If
RsAnular.Close
QyAnular.Close

Conexion.BeginTrans
sql = "UPDATE PR0400 SET PR37CODESTADO = '6' WHERE PR04NUMACTPLAN = ? "
Set QyAnular = Conexion.CreateQuery("", sql)
    QyAnular(0) = strActPlan
QyAnular.Execute
QyAnular.Close

If Err > 0 Then
    Conexion.RollbackTrans
    blnAnular = False
    Exit Function
End If
sql = "UPDATE CI0100 SET CI01SITCITA= '2' WHERE "
sql = sql & "CI01SITCITA = 5 AND CI31NUMSOLICIT = ?"
Set QyAnular = Conexion.CreateQuery("", sql)
   QyAnular(0) = Solicit
QyAnular.Execute
QyAnular.Close

If Err > 0 Then
    Conexion.RollbackTrans
    blnAnular = False
    Exit Function
End If
blnAnular = True
Conexion.CommitTrans
End Function
Private Function fblnCitar() As Boolean

Dim fecCita As Date
'Conversi�n de tiempos
   Dim intDias As Integer
   Dim bytHoras As Byte
   Dim bytMinutos As Byte
'Campos Primery Key que se obtienen antes de insertar un registro
   Dim numPetic As Long
   Dim numActPedi As Long
   Dim numActPlan As Long
   Dim numSolicit As Long
'Datos necesarios que se obtienen con funciones
   Dim codDpto As Integer 'Departamento del usuario
   'Fase
   Dim strDesFase As String 'Descripci�n
   Dim lngMinutFase As Long 'Minutos de ocupaci�n del paciente
   'Tipo Recurso- Fase Actuaci�n
   Dim lngNumNecesid As Long
   Dim intIndPlan As Integer
   Dim lngTiemRec As Long
   'C�digo de recurso y Tipo de Recurso para codDoctor
   Dim intTipRec As Integer
   Dim lngCodRecurso As Long
'Consultas SQL
   Dim sql As String
   Dim QyInsert As rdoQuery
   Dim RsRespuesta As rdoResultset

'********************Inicializaci�n********************
On Error Resume Next
Err = 0
Conexion.BeginTrans

'COMIENZA EL BAILE**************************************************************************

'Datos previos
sql = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD = ? AND AD03FECINICIO <= SYSDATE AND "
sql = sql & "(AD03FECFIN >= SYSDATE OR AD03FECFIN IS NULL)"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = strCodUsuario
Set RsRespuesta = QyInsert.OpenResultset()
If Not RsRespuesta.EOF Then
    codDpto = RsRespuesta(0)
Else
    codDpto = fgetDpto(strRecurso)
End If
RsRespuesta.Close
QyInsert.Close

'PR0900 PETICI�N
sql = "INSERT INTO PR0900 (PR09NUMPETICION, CI21CODPERSONA, AD02CODDPTO, SG02COD, "
If txtText1.Text = "" Then
    sql = sql & "PR09FECPETICION, PR09NUMGRUPO) VALUES (?, ?, ?, ?, SYSDATE, ?)"
Else
    sql = sql & "PR09FECPETICION, PR09NUMGRUPO, PR09DESOBSERVAC) VALUES (?, ?, ?, ?, SYSDATE, ?, ?)"
End If

Set QyInsert = Conexion.CreateQuery("PETICION", sql)
   numPetic = flngNextClaveSeq("PR09NUMPETICION")
   QyInsert(0) = numPetic
   QyInsert(1) = lngCodPerson
   QyInsert(2) = codDpto
   QyInsert(3) = strCodUsuario
   QyInsert(4) = numPetic
   If txtText1.Text <> "" Then QyInsert(5) = txtText1.Text
QyInsert.Execute
QyInsert.Close

If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If

'PR0300 ACTUACI�N PEDIDA
sql = "INSERT INTO PR0300 (PR03NUMACTPEDI, AD02CODDPTO, CI21CODPERSONA, PR09NUMPETICION, "
sql = sql & "PR01CODACTUACION, PR03INDCONSFIRM, PR03INDLUNES, PR03INDMARTES, "
sql = sql & "PR03INDMIERCOLES, PR03INDJUEVES, PR03INDVIERNES, PR03INDSABADO, PR03INDDOMINGO,"
sql = sql & " PR03INDCITABLE) VALUES (?, ?, ?, ?, ?, 0, 0, 0, 0, 0, 0, 0, 0, -1)"
Set QyInsert = Conexion.CreateQuery("", sql)
   numActPedi = flngNextClaveSeq("PR03NUMACTPEDI") 'Se usar� m�s adelante
   QyInsert(0) = numActPedi
   QyInsert(1) = codDpto
   QyInsert(2) = lngCodPerson
   QyInsert(3) = numPetic
'   QyInsert(4) = Right(strkey, L3)
    QyInsert(4) = SSDBCmbPrueba.Columns(0).Value
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If

'PR0800 PETICI�N-ACTUACI�N PEDIDA
If txtText1.Text = "" Then
    sql = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA) "
    sql = sql & " VALUES (?, ?, 1)"
Else
    sql = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA, PR08DESOBSERV) "
    sql = sql & " VALUES (?, ?, 1, ?)"
End If

Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = numPetic
   QyInsert(1) = numActPedi
   If txtText1.Text <> "" Then QyInsert(2) = txtText1.Text
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If
'txtText1.Text = ""
'Datos previos
If Not fblnFase(SSDBCmbPrueba.Columns(0).Value, strDesFase, lngMinutFase) Then
   Conexion.RollbackTrans
   Exit Function
End If
'PR0600 FASE PEDIDA
sql = "INSERT INTO PR0600 (PR03NUMACTPEDI, PR06NUMFASE, PR06DESFASE, PR06NUMMINOCUPAC, "
sql = sql & "PR06NUMMINFPRE, PR06NUMMAXFPRE, PR06INDHABNATU, PR06INDINIFIN) "
sql = sql & "VALUES (?, 1, ?, ?, 0, 0, 0, 0)"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = numActPedi
   QyInsert(1) = strDesFase
   QyInsert(2) = lngMinutFase
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If

'Datos previos
If Not fblnRecFaseAct(SSDBCmbPrueba.Columns(0).Value, lngNumNecesid, lngTiemRec, _
                                                                        intIndPlan) Then
   Conexion.RollbackTrans
   Exit Function
End If
If Not fblnTipRec(intTipRec) Then
   Conexion.RollbackTrans
   Exit Function
End If
'PR1400 TIPO RECURSO PEDIDO
sql = "INSERT INTO PR1400 (PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID, AG14CODTIPRECU, "
sql = sql & "AD02CODDPTO, PR14NUMUNIREC, PR14NUMMINOCU, PR14NUMMINDESREC, PR14INDRECPREFE, "
sql = sql & "PR14INDPLANIF) VALUES (?, 1, ?, ?, ?, 1, ?, 0, -1, ?)"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = numActPedi 'PR03NUMACTPEDI
   QyInsert(1) = lngNumNecesid 'PR14NUMNECESID
   QyInsert(2) = intTipRec 'AG14CODTIPRECU
   QyInsert(3) = codDpto 'AD02CODDPTO
   QyInsert(4) = lngTiemRec 'PR14NUMMINOCU
   QyInsert(5) = intIndPlan 'PR14INDPLANIF
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If

'PR0400 ACTUACI�N PLANIFICADA
sql = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, "
sql = sql & "CI21CODPERSONA, PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC) "
sql = sql & "VALUES (?, ?, ?, ?, 2, ?, 0)"
Set QyInsert = Conexion.CreateQuery("", sql)
   numActPlan = flngNextClaveSeq("PR04NUMACTPLAN")
   QyInsert(0) = numActPlan
   QyInsert(1) = SSDBCmbPrueba.Columns(0).Value
   QyInsert(2) = codDpto 'AD02CODDPTO
   QyInsert(3) = lngCodPerson
   QyInsert(4) = numActPedi
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If

'CI3100 SOLICITUD
sql = "INSERT INTO CI3100 (CI31NUMSOLICIT, CI31FECPREFDES, CI31FECPREFHAS, CI31INDLUNPREF, "
sql = sql & "CI31INDMARPREF, CI31INDMIEPREF, CI31INDJUEPREF, CI31INDVIEPREF, CI31INDSABPREF,"
sql = sql & " CI31INDDOMPREF) VALUES (?, SYSDATE, SYSDATE + 15,  1, 1, 1, 1, 1, 1, 1)"
Set QyInsert = Conexion.CreateQuery("", sql)
   numSolicit = fNextClave("CI31NUMSOLICIT")
   QyInsert(0) = numSolicit
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
      Conexion.RollbackTrans
      Exit Function
End If

fecCita = FechaActual & " " & Left(strtexto, 5)
'CI0100 ACTUACI�N CITADA
sql = "INSERT INTO CI0100 (CI31NUMSOLICIT, CI01NUMCITA, AG11CODRECURSO, PR04NUMACTPLAN, "
sql = sql & "CI01FECCONCERT, CI01SITCITA, CI01INDRECORDA, CI01TIPRECORDA, CI01NUMENVRECO, "
sql = sql & "CI01INDLISESPE, CI01INDASIG) VALUES (?, 1, ?, ?, "
sql = sql & "TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), 5, -1, 1, 1, 0, 0)"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = numSolicit
   QyInsert(1) = strRecurso
   QyInsert(2) = numActPlan
   QyInsert(3) = Format(fecCita, "DD/MM/YYYY HH:MM:SS")
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If

'CI1500 FASE CITADA
Call pConvertMinutos(lngMinutFase, intDias, bytHoras, bytMinutos)
sql = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, CI15DESFASECITA, "
sql = sql & "CI15FECCONCPAC, CI15NUMDIASPAC, CI15NUMHORAPAC, CI15NUMMINUPAC) "
sql = sql & "VALUES (?, 1, 1, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?)"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = numSolicit
   QyInsert(1) = strDesFase
   QyInsert(2) = Format(fecCita, "DD/MM/YYYY HH:MM:SS")
   QyInsert(3) = intDias
   QyInsert(4) = bytHoras
   QyInsert(5) = bytMinutos
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If

'CI2700 RECURSO CITADO
Call pConvertMinutos(lngTiemRec, intDias, bytHoras, bytMinutos)
sql = "INSERT INTO CI2700 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, AG11CODRECURSO, "
sql = sql & "CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC) "
sql = sql & "VALUES (?, 1, 1, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?)"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = numSolicit
   QyInsert(1) = strRecurso
   QyInsert(2) = Format(fecCita, "DD/MM/YYYY HH:MM:SS")
   QyInsert(3) = intDias
   QyInsert(4) = bytHoras
   QyInsert(5) = bytMinutos
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   Conexion.RollbackTrans
   Exit Function
End If

Conexion.CommitTrans
fblnCitar = True

End Function
'HALLAR LA DESCRIPCI�N Y DURACI�N DE LA FASE QUE SE QUIERE CITAR
Private Function fblnFase(lngCodActuacion As Long, strDesFase As String, _
                           lngMinutFase As Long) As Boolean
Dim sql As String
Dim QyInsert As rdoQuery
Dim RsRespuesta As rdoResultset

sql = "SELECT PR05DESFASE, PR05NUMOCUPACI FROM PR0500 "
sql = sql & "WHERE PR01CODACTUACION = ? AND PR05NUMFASE = 1"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = lngCodActuacion
Set RsRespuesta = QyInsert.OpenResultset()
If RsRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   strDesFase = RsRespuesta(0)
   lngMinutFase = RsRespuesta(1)
   RsRespuesta.MoveNext
   If Not RsRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
RsRespuesta.Close
QyInsert.Close
Set RsRespuesta = Nothing
Set QyInsert = Nothing
fblnFase = True

End Function


'Devuelve el siguiente c�digo a utilizar en un registro nuevo utilizando los SEQUENCE
'Si se produce alg�n error, devuelve -1
Public Function flngNextClaveSeq(strCampo As String) As Long
    
Dim rstMaxClave As rdoResultset

On Error Resume Next
Err = 0
Set rstMaxClave = Conexion.OpenResultset("SELECT " & strCampo & "_SEQUENCE.NEXTVAL FROM DUAL")
flngNextClaveSeq = rstMaxClave(0)
rstMaxClave.Close
Set rstMaxClave = Nothing
If Err > 0 Then flngNextClaveSeq = -1
    
End Function

Private Sub Form_Load()
Dim sql As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset
'cargamos los tipo de actividad
sql = "SELECT PR12CODACTIVIDAD, PR12DESACTIVIDAD FROM PR1200"
sql = sql & " ORDER BY PR12DESACTIVIDAD"
Set rstConsulta = Conexion.OpenResultset(sql)
Do While Not rstConsulta.EOF
   SSDBCmbActividad.AddItem rstConsulta(0) & ";" & rstConsulta(1)
   rstConsulta.MoveNext
Loop
rstConsulta.Close
''''Cargamos Actuaciones
''''Actuaciones
'''sql = "SELECT PR0100.PR01CODACTUACION, PR01DESCORTA FROM PR0100, PR0200 "
'''sql = sql & "WHERE PR0200.AD02CODDPTO = ? AND "
'''sql = sql & "PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION AND PR12CODACTIVIDAD = 201 "
'''sql = sql & "AND PR01FECINICO <= SYSDATE AND "
'''sql = sql & "(PR01FECFIN >= SYSDATE OR PR01FECFIN IS NULL)"
'''Set qryConsulta = Conexion.CreateQuery("", sql)
'''   qryConsulta(0) = fgetDpto(strRecurso)
'''Set rstConsulta = qryConsulta.OpenResultset()
'''Do While Not rstConsulta.EOF
'''   SSDBCmbPrueba.AddItem rstConsulta(0) & ";" & rstConsulta(1)
'''   rstConsulta.MoveNext
'''Loop
'''rstConsulta.Close
'''qryConsulta.Close
lstCitasLibres.ListItems.Clear
lstCitasLibres.MultiSelect = True
Call pBuscar_HuecosDia
lstReservas.ListItems.Clear
lstReservas.MultiSelect = True
Call pBuscar_ReservasDia
End Sub


Private Sub lstCitasLibres_ItemClick(ByVal Item As ComctlLib.ListItem)
txtText1.Enabled = True
cmdAnular.Enabled = False
cmdReReserva.Enabled = True
End Sub

Private Sub lstReservas_ItemClick(ByVal Item As ComctlLib.ListItem)
Dim sql As String
Dim QyInsert As rdoQuery
Dim RsRespuesta As rdoResultset
Dim intL1 As Integer
Dim intL2 As Integer
Dim intL3 As Integer

cmdAnular.Enabled = True
cmdReReserva.Enabled = False
'Sacamos el numero de solicitud
    intL1 = InStr(Item.Key, "R")
    intL2 = Len(Item.Key)
    intL2 = intL2 - intL1
sql = "SELECT PR08DESOBSERV, PR0800.SG02COD_ADD,PR01DESCORTA FROM PR0800,CI0100,PR0400,PR0100 Where "
sql = sql & "CI0100.CI31NUMSOLICIT = ? AND "
sql = sql & "CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN AND "
sql = sql & "PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI AND "
sql = sql & "PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
Set QyInsert = Conexion.CreateQuery("", sql)
   QyInsert(0) = Right(Item.Key, intL2)
Set RsRespuesta = QyInsert.OpenResultset()
txtText1.Enabled = False
If Not RsRespuesta.EOF Then
    txtText1.Text = "Actuaci�n Reservada: " & RsRespuesta(2).Value
    If Not IsNull(RsRespuesta(0).Value) Then
        txtText1.Text = txtText1.Text & Chr$(13) & Chr$(10) & RsRespuesta(0).Value & Chr$(13) & Chr$(10) & _
        Chr$(13) & Chr$(10) & "Realizada por: " & RsRespuesta(1).Value
    Else
        txtText1.Text = txtText1.Text & Chr$(13) & Chr$(10) & "Realizada por: " & RsRespuesta(1).Value
    End If
    
Else
       txtText1.Text = ""
End If
End Sub

Private Sub lstReservas_LostFocus()
txtText1.Text = ""
End Sub



Private Sub SSDBCmbActividad_CloseUp()
Dim sql As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset

SSDBCmbPrueba.Text = ""
SSDBCmbPrueba.RemoveAll
'Cargamos Actuaciones
'Actuaciones
sql = "SELECT PR0100.PR01CODACTUACION, PR01DESCORTA FROM PR0100, PR0200 "
sql = sql & "WHERE PR0200.AD02CODDPTO = ? AND "
sql = sql & "PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION AND PR12CODACTIVIDAD = ? "
sql = sql & "AND PR01FECINICO <= SYSDATE AND "
sql = sql & "(PR01FECFIN <= SYSDATE OR PR01FECFIN IS NULL)"
sql = sql & " ORDER BY PR01DESCORTA"

Set qryConsulta = Conexion.CreateQuery("", sql)
   qryConsulta(0) = fgetDpto(strRecurso)
   qryConsulta(1) = SSDBCmbActividad.Columns(0).Value
Set rstConsulta = qryConsulta.OpenResultset()
Do While Not rstConsulta.EOF
   SSDBCmbPrueba.AddItem rstConsulta(0) & ";" & rstConsulta(1)
   rstConsulta.MoveNext
Loop
rstConsulta.Close
qryConsulta.Close
End Sub

Private Sub SSDBCmbActividad_KeyPress(KeyAscii As Integer)
 
Dim sql As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset
SSDBCmbPrueba.Text = ""
SSDBCmbPrueba.RemoveAll
'Cargamos Actuaciones
'Actuaciones
sql = "SELECT PR0100.PR01CODACTUACION, PR01DESCORTA FROM PR0100, PR0200 "
sql = sql & "WHERE PR0200.AD02CODDPTO = ? AND "
sql = sql & "PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION AND PR12CODACTIVIDAD = ? "
sql = sql & "AND PR01FECINICO <= SYSDATE AND "
sql = sql & "(PR01FECFIN >= SYSDATE OR PR01FECFIN IS NULL)"
Set qryConsulta = Conexion.CreateQuery("", sql)
   qryConsulta(0) = fgetDpto(strRecurso)
   qryConsulta(1) = SSDBCmbActividad.Columns(0).Value
Set rstConsulta = qryConsulta.OpenResultset()
Do While Not rstConsulta.EOF
   SSDBCmbPrueba.AddItem rstConsulta(0) & ";" & rstConsulta(1)
   rstConsulta.MoveNext
Loop
rstConsulta.Close
qryConsulta.Close

End Sub

Private Sub SSDBCmbPrueba_CloseUp()
lstCitasLibres.ListItems.Clear
lstCitasLibres.MultiSelect = True
Call pBuscar_HuecosDia
End Sub
Public Function fNextClave(strCampo$) As String
    Dim rs As rdoResultset, sql$
    sql = "SELECT " & strCampo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rs = Conexion.OpenResultset(sql)
    fNextClave = rs(0)
    rs.Close
End Function

Private Sub SSDBCmbPrueba_KeyPress(KeyAscii As Integer)
lstCitasLibres.ListItems.Clear
lstCitasLibres.MultiSelect = True
Call pBuscar_HuecosDia
End Sub
