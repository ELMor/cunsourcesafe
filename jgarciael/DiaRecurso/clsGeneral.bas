Attribute VB_Name = "mdlGeneral"
Option Explicit

Public Const conMeses As Byte = 2 'N�mero de meses que se revisan para ver huecos
Public Const conDias As Byte = 5
Public objCW As Object      ' referencia al objeto CodeWizard
Public objApp As clsCWApp
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object
 
Public objSecure As Object    'nuevo
Public objSecurity As Object  'nuevo
Public objmouse As Object     'nuevo

'Datos necesarios para buscar huecos
Public strRecurso As String  'Almacena el recurso para el que busca los huecos disponibles
Public FechaActual As Date  'Almacena la Fecha en la que est� buscando Huecos
'Objeto de la conexi�n RDO
Public Conexion As rdoConnection
'PERSONA RESERVA
Public Const lngCodPerson As Long = 1416157
'Codigo del usuario que esta en el sistema
Public strCodUsuario As String


Public Function fstrBuscarDiaSemana(datDia As Date) As String

Dim bytCodDiaSemana As Byte

bytCodDiaSemana = DatePart("w", datDia, vbMonday, vbFirstJan1)

Select Case bytCodDiaSemana
    Case 1: fstrBuscarDiaSemana = "Lunes"
    Case 2: fstrBuscarDiaSemana = "Martes"
    Case 3: fstrBuscarDiaSemana = "Mi�rcoles"
    Case 4: fstrBuscarDiaSemana = "Jueves"
    Case 5: fstrBuscarDiaSemana = "Viernes"
    Case 6: fstrBuscarDiaSemana = "S�bado"
    Case 7: fstrBuscarDiaSemana = "Domingo"
End Select
    
End Function

Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Set objmouse = objCW.objmouse
  Set objSecurity = objCW.objSecurity
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub
Public Sub ExitApp()
  Set objCW = Nothing
End Sub
