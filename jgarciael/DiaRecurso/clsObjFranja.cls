VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsObjFranja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Datos de la franja
Private intcodFranja As Integer, intHI As Integer, intMI As Integer
Private intHF As Integer, intMF As Integer, strDescrAct As String, intCitas As Integer
Private intModoAsig As Integer, intNumCit As Integer, intIntervCita As Integer
Private blnPlanificable As Boolean, strActuac As String

'Datos del rect�ngulo y bot�n
Private intLeft As Integer, lngTop As Long, lngHeight As Long
Private lngColor As Long, lngColorBoton As Long, blnBorde As Boolean

'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Franja!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Public Property Let codFranja(intcodFranjaNuevo As Integer)
   intcodFranja = intcodFranjaNuevo
End Property

Public Property Get codFranja() As Integer
   codFranja = intcodFranja
End Property

Public Property Let HI(intHINuevo As Integer)
   intHI = intHINuevo
End Property

Public Property Get HI() As Integer
   HI = intHI
End Property

Public Property Let MI(intMINuevo As Integer)
   intMI = intMINuevo
End Property

Public Property Get MI() As Integer
   MI = intMI
End Property

Public Property Let HF(intHFNuevo As Integer)
   intHF = intHFNuevo
End Property

Public Property Get HF() As Integer
   HF = intHF
End Property

Public Property Let MF(intMFNuevo As Integer)
   intMF = intMFNuevo
End Property

Public Property Get MF() As Integer
   MF = intMF
End Property

Public Property Let descrAct(strDescrActNuevo As String)
   strDescrAct = strDescrActNuevo
End Property

Public Property Get descrAct() As String
   descrAct = strDescrAct
End Property

Public Property Let citas(intCitasNuevo As Integer)
   intCitas = intCitasNuevo
End Property

Public Property Get citas() As Integer
   citas = intCitas
End Property

Public Property Let modoAsig(intModoAsigNuevo As Integer)
   intModoAsig = intModoAsigNuevo
End Property

Public Property Get modoAsig() As Integer
   modoAsig = intModoAsig
End Property

Public Property Let numCitas(intNumCitNuevo As Integer)
   intNumCit = intNumCitNuevo
End Property

Public Property Get numCitas() As Integer
   numCitas = intNumCit
End Property

Public Property Let intervCitas(intIntervCitaNuevo As Integer)
   intIntervCita = intIntervCitaNuevo
End Property

Public Property Get intervCitas() As Integer
   intervCitas = intIntervCita
End Property

Public Property Let Planificable(blnPlanificableNuevo As Integer)
   blnPlanificable = blnPlanificableNuevo
End Property

Public Property Get Planificable() As Integer
   Planificable = blnPlanificable
End Property

Public Property Let Actuaciones(strActuacNuevo As String)
   strActuac = strActuacNuevo
End Property

Public Property Get Actuaciones() As String
   Actuaciones = strActuac
End Property

'!!!!!!!!!!!!!!!!!!!!!!!!!!!!Rect�ngulo y bot�n!!!!!!!!!!!!!!!!!!!!!!!!!!!

Public Property Let Left(intLeftNuevo As Integer)
   intLeft = intLeftNuevo
End Property

Public Property Let Top(lngTopNuevo As Long)
   lngTop = lngTopNuevo
End Property

Public Property Let Height(lngHeightNuevo As Long)
   lngHeight = lngHeightNuevo
End Property

Public Property Let Color(lngColorNuevo As Long)
   lngColor = lngColorNuevo
End Property

Public Property Let ColorBoton(lngColorBotonNuevo As Long)
   lngColorBoton = lngColorBotonNuevo
End Property

Public Property Let Borde(blnBordeNuevo As Boolean)
   blnBorde = blnBordeNuevo
End Property

Public Property Get Left() As Integer
   Left = intLeft
End Property

Public Property Get Top() As Long
   Top = lngTop
End Property

Public Property Get Height() As Long
   Height = lngHeight
End Property

Public Property Get ColorBoton() As Long
   ColorBoton = lngColorBoton
End Property

Public Property Get Color() As Long
   Color = lngColor
End Property

Public Property Get Borde() As Boolean
   Borde = blnBorde
End Property

