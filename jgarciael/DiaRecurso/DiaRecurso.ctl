VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.UserControl DiaRecurso 
   ClientHeight    =   7125
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11580
   ScaleHeight     =   7125
   ScaleWidth      =   11580
   ToolboxBitmap   =   "DiaRecurso.ctx":0000
   Begin VB.CommandButton cmdReservar 
      Caption         =   "RESERVA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8640
      TabIndex        =   26
      Top             =   6710
      Width           =   2895
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2670
      Left            =   8670
      TabIndex        =   12
      Top             =   120
      Width           =   2865
      _Version        =   65537
      _ExtentX        =   5054
      _ExtentY        =   4710
      _StockProps     =   76
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1998/1/1"
      MaxDate         =   "2100/12/31"
      stylesets.count =   4
      stylesets(0).Name=   "NoTareas"
      stylesets(0).ForeColor=   65535
      stylesets(0).Picture=   "DiaRecurso.ctx":0312
      stylesets(1).Name=   "Libre"
      stylesets(1).ForeColor=   16711680
      stylesets(1).BackColor=   12632256
      stylesets(1).Picture=   "DiaRecurso.ctx":032E
      stylesets(2).Name=   "Normal"
      stylesets(2).ForeColor=   0
      stylesets(2).Picture=   "DiaRecurso.ctx":034A
      stylesets(3).Name=   "Ausente"
      stylesets(3).ForeColor=   255
      stylesets(3).Picture=   "DiaRecurso.ctx":0366
      StartofWeek     =   2
   End
   Begin VB.TextBox txtComentarios 
      BackColor       =   &H00C0C0C0&
      Enabled         =   0   'False
      Height          =   3195
      Left            =   8670
      MultiLine       =   -1  'True
      TabIndex        =   11
      Top             =   3420
      Width           =   2895
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   7110
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      Begin VB.PictureBox Picture2 
         Height          =   6780
         Left            =   30
         ScaleHeight     =   6720
         ScaleWidth      =   8520
         TabIndex        =   1
         Top             =   270
         Width           =   8580
         Begin VB.VScrollBar VScroll1 
            Height          =   6740
            LargeChange     =   5000
            Left            =   8280
            Max             =   0
            SmallChange     =   500
            TabIndex        =   10
            Top             =   0
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.PictureBox Picture1 
            Appearance      =   0  'Flat
            AutoRedraw      =   -1  'True
            BackColor       =   &H00C0C0C0&
            ForeColor       =   &H80000008&
            Height          =   45000
            Left            =   0
            ScaleHeight     =   44970
            ScaleWidth      =   8565
            TabIndex        =   2
            Top             =   0
            Width           =   8600
            Begin VB.CommandButton cmdFranja 
               BackColor       =   &H0080C0FF&
               Height          =   1935
               Index           =   0
               Left            =   800
               MousePointer    =   99  'Custom
               Style           =   1  'Graphical
               TabIndex        =   4
               Top             =   1320
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.CommandButton cmdCita 
               BackColor       =   &H0080FFFF&
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Index           =   0
               Left            =   1055
               MaskColor       =   &H00FFFFFF&
               MousePointer    =   99  'Custom
               Style           =   1  'Graphical
               TabIndex        =   3
               Top             =   1860
               Visible         =   0   'False
               Width           =   2520
            End
            Begin VB.Label lblValor 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Lucida Console"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   150
               Index           =   0
               Left            =   3660
               TabIndex        =   9
               Top             =   2040
               Visible         =   0   'False
               Width           =   615
            End
            Begin VB.Label lblCantidad 
               Alignment       =   2  'Center
               BackColor       =   &H00C0C0FF&
               BackStyle       =   0  'Transparent
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   165
               Index           =   0
               Left            =   1050
               TabIndex        =   8
               Top             =   2520
               Visible         =   0   'False
               Width           =   2520
            End
            Begin VB.Label lblMensaje 
               Alignment       =   2  'Center
               Caption         =   "El recurso no realiza tareas este d�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   24
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1215
               Left            =   1920
               TabIndex        =   7
               Top             =   4080
               Visible         =   0   'False
               Width           =   4455
            End
            Begin VB.Label lblFestivo 
               Alignment       =   2  'Center
               Caption         =   "D�a festivo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   24
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   2760
               TabIndex        =   6
               Top             =   1080
               Visible         =   0   'False
               Width           =   2475
            End
            Begin VB.Label lblDescripcion 
               Alignment       =   2  'Center
               BackColor       =   &H8000000A&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   24
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   975
               Left            =   1200
               TabIndex        =   5
               Top             =   3480
               Visible         =   0   'False
               Width           =   5535
            End
         End
      End
   End
   Begin VB.Frame Frame1 
      Height          =   675
      Left            =   8880
      TabIndex        =   13
      Top             =   2715
      Width           =   2490
      Begin VB.CommandButton cmdMes 
         Caption         =   "DI"
         Height          =   250
         Index           =   11
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   375
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "NO"
         Height          =   250
         Index           =   10
         Left            =   1650
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   375
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "OC"
         Height          =   250
         Index           =   9
         Left            =   1245
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   375
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "SP"
         Height          =   250
         Index           =   8
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   375
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "AG"
         Height          =   250
         Index           =   7
         Left            =   450
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   375
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "JL"
         Height          =   250
         Index           =   6
         Left            =   45
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   375
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "JN"
         Height          =   250
         Index           =   5
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   135
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         BackColor       =   &H8000000A&
         Caption         =   "MY"
         Height          =   250
         Index           =   4
         Left            =   1650
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   135
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "AB"
         Height          =   250
         Index           =   3
         Left            =   1245
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   135
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "MZ"
         Height          =   250
         Index           =   2
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   135
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "FE"
         Height          =   250
         Index           =   1
         Left            =   450
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   135
         Width           =   400
      End
      Begin VB.CommandButton cmdMes 
         Caption         =   "EN"
         Height          =   250
         Index           =   0
         Left            =   45
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   135
         Width           =   400
      End
   End
End
Attribute VB_Name = "DiaRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Default Property Values:
Const m_def_BackColor = 0
Const m_def_ForeColor = 0
Const m_def_Enabled = 0
Const m_def_Appearance = 0
Const m_def_BackStyle = 0
Const m_def_BorderStyle = 0
Const m_def_codRecurso = 0
Const m_def_Fecha = "0:00:00"
'Property Variables:
Dim m_BackColor As Long
Dim m_ForeColor As Long
Dim m_Enabled As Boolean
Dim m_Font As Font
Dim m_Appearance As Integer
Dim m_BackStyle As Integer
Dim m_BorderStyle As Integer
Dim m_codRecurso As Long
Dim m_Fecha As Date

'Event Declarations:
Event CloseUp() 'MappingInfo=SSDateCombo1(0),SSDateCombo1,0,CloseUp
Event Click()
Event DblClick()
Event KeyDown(KeyCode As Integer, Shift As Integer)
Event KeyPress(KeyAscii As Integer)
Event KeyUp(KeyCode As Integer, Shift As Integer)
Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
'Event CloseUp() 'MappingInfo=SSDateCombo1(1),SSDateCombo1,1,CloseUp

'///////////////////////////////////////////////////////////////////////////

Dim blnPrimera As Boolean 'Si es la primera vez que se entra = FALSE
Dim intAnterior As Integer 'Almacena el Top anterior al vscHorario_Change
Dim bytMesActual As Byte 'Almacena el n�mero del mes actual en el SSMonth1

'''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Dim Horario As New clsHorario
'''''''''''''''CLASE!!!!!!!!!!!!!!!!!!

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''PROPIEDADES DEL CONTROL''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Property Get BackColor() As Long
    BackColor = m_BackColor
End Property

Public Property Let BackColor(ByVal New_BackColor As Long)
   m_BackColor = New_BackColor
   PropertyChanged "BackColor"
End Property

Public Property Get ForeColor() As Long
   ForeColor = m_ForeColor
End Property

Public Property Let ForeColor(ByVal New_ForeColor As Long)
   m_ForeColor = New_ForeColor
   PropertyChanged "ForeColor"
End Property

Public Property Get Enabled() As Boolean
   Enabled = m_Enabled
End Property

Public Property Let Enabled(ByVal New_Enabled As Boolean)
   m_Enabled = New_Enabled
   PropertyChanged "Enabled"
End Property

Public Property Get Font() As Font
   Set Font = m_Font
End Property

Public Property Set Font(ByVal New_Font As Font)
   Set m_Font = New_Font
   PropertyChanged "Font"
End Property

Public Property Get Appearance() As Integer
   Appearance = m_Appearance
End Property

Public Property Let Appearance(ByVal New_Appearance As Integer)
   m_Appearance = New_Appearance
   PropertyChanged "Appearance"
End Property

Public Property Get BackStyle() As Integer
   BackStyle = m_BackStyle
End Property

Public Property Let BackStyle(ByVal New_BackStyle As Integer)
   m_BackStyle = New_BackStyle
   PropertyChanged "BackStyle"
End Property

Public Property Get BorderStyle() As Integer
   BorderStyle = m_BorderStyle
End Property

Public Property Let BorderStyle(ByVal New_BorderStyle As Integer)
   m_BorderStyle = New_BorderStyle
   PropertyChanged "BorderStyle"
End Property

Public Property Get codRecurso() As Long
7   codRecurso = m_codRecurso
End Property

Public Property Let codRecurso(ByVal New_codRecurso As Long)
   m_codRecurso = New_codRecurso
   PropertyChanged "codRecurso"
End Property

Public Property Get Fecha() As Date
   Fecha = m_Fecha
End Property

Public Property Let Fecha(ByVal New_Fecha As Date)
   m_Fecha = New_Fecha
   PropertyChanged "Fecha"
End Property


Private Sub cmdCita_Click(Index As Integer)
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   Call Horario.llenar_Comentarios(Index, False)
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
End Sub

Private Sub cmdCita_GotFocus(Index As Integer)
   cmdCita_Click Index
End Sub

Private Sub cmdFranja_Click(Index As Integer)
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   Call Horario.llenar_Comentarios(Index, True)
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
End Sub

Private Sub cmdFranja_GotFocus(Index As Integer)
   cmdFranja_Click Index
End Sub

Private Sub cmdMes_Click(Index As Integer)
cmdMes(Month(SSMonth1.Date) - 1).BackColor = &H8000000A  'Gris
SSMonth1.Date = Format("01/" & Index + 1 & "/" & Year(SSMonth1.Date), "DD/MM/YYYY")
End Sub

Private Sub cmdReserva_Click(Index As Integer)
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   Call Horario.llenar_Comentarios(Index, False)
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
End Sub

Private Sub cmdReservar_Click()
'Load frmReserva
'frmReserva.Visible = True
FechaActual = SSMonth1.Date
strRecurso = codRecurso
frmReserva.Show vbModal
actualizar_dibujo
End Sub

Private Sub SSMonth1_InitMonth(MonthNum As Integer, YearNum As Integer, RtnCancel As Integer)
If MonthNum <> bytMesActual Then _
                pRepresentar codRecurso, Conexion, "01/" & MonthNum & "/" & YearNum, strCodUsuario
                
                
End Sub

Private Sub SSMonth1_SelChange(SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
If OldSelDate <> "" Then
    cmdMes(Format(OldSelDate, "MM") - 1).BackColor = &H8000000A  'Gris
    cmdMes(Format(SelDate, "MM") - 1).BackColor = &HFFFFFF 'Color blanco
    actualizar_dibujo 'Se actualiza el dibujo a la nueva fecha
    If SSMonth1.Day(SelDate).StyleSet = "Ausente" Or SSMonth1.Day(SelDate).StyleSet = "" Then
      cmdReservar.Enabled = False
    Else
     cmdReservar.Enabled = True
    End If
End If
End Sub



Private Sub UserControl_InitProperties()

   m_BackColor = m_def_BackColor
   m_ForeColor = m_def_ForeColor
   m_Enabled = m_def_Enabled
   Set m_Font = Ambient.Font
   m_Appearance = m_def_Appearance
   m_BackStyle = m_def_BackStyle
   m_BorderStyle = m_def_BorderStyle
   m_codRecurso = m_def_codRecurso
   m_Fecha = m_def_Fecha
   
   Set Horario.BotonesFranja = cmdFranja
   Set Horario.LabelsHoras = lblValor
   Set Horario.PictureBox = Picture1
   Set Horario.VScroll = VScroll1
   Set Horario.Comentarios = txtComentarios
   
   intAnterior = 0
   
End Sub

' Cargar valores de propiedades desde el almacenamiento
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

   m_BackColor = PropBag.ReadProperty("BackColor", m_def_BackColor)
   m_ForeColor = PropBag.ReadProperty("ForeColor", m_def_ForeColor)
   m_Enabled = PropBag.ReadProperty("Enabled", m_def_Enabled)
   Set m_Font = PropBag.ReadProperty("Font", Ambient.Font)
   m_Appearance = PropBag.ReadProperty("Appearance", m_def_Appearance)
   m_BackStyle = PropBag.ReadProperty("BackStyle", m_def_BackStyle)
   m_BorderStyle = PropBag.ReadProperty("BorderStyle", m_def_BorderStyle)
   m_codRecurso = PropBag.ReadProperty("codRecurso", m_def_codRecurso)
   m_Fecha = PropBag.ReadProperty("Fecha", m_def_Fecha)

   Set Horario.BotonesFranja = cmdFranja
   Set Horario.BotonesCita = cmdCita
   Set Horario.LabelsHoras = lblValor
   Set Horario.LabelsCantidad = lblCantidad
   Set Horario.PictureBox = Picture1
   Set Horario.VScroll = VScroll1
   Set Horario.Comentarios = txtComentarios

End Sub

' Escribir valores de propiedades en el almacenamiento
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)

   Call PropBag.WriteProperty("BackColor", m_BackColor, m_def_BackColor)
   Call PropBag.WriteProperty("ForeColor", m_ForeColor, m_def_ForeColor)
   Call PropBag.WriteProperty("Enabled", m_Enabled, m_def_Enabled)
   Call PropBag.WriteProperty("Font", m_Font, Ambient.Font)
   Call PropBag.WriteProperty("Appearance", m_Appearance, m_def_Appearance)
   Call PropBag.WriteProperty("BackStyle", m_BackStyle, m_def_BackStyle)
   Call PropBag.WriteProperty("BorderStyle", m_BorderStyle, m_def_BorderStyle)
   Call PropBag.WriteProperty("codRecurso", m_codRecurso, m_def_codRecurso)
   Call PropBag.WriteProperty("Fecha", m_Fecha, m_def_Fecha)
   
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''COMIENZA EL C�DIGO'''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Sub pRepresentar(CodRecursoNuevo As Long, ConexionCW As rdoConnection, _
Optional datDate As Date, Optional strUser As String)

'''''''''''''''''''DECLARACI�N DE VARIABLES''''''''''''''''''''''''''''''''
Dim intDia As Integer 'No se deja Byte en SSMonth1.Month(intMes).Day(intDia)
Dim intMes As Integer 'No se deja Byte en SSMonth1.Month(intMes).Day(intDia)
'Consultas SQL
Dim SQL As String
Dim qryRecurso As rdoQuery
Dim rstRecurso As rdoResultset
Dim qryFestivos As rdoQuery
Dim rstFestivos As rdoResultset

On Error GoTo ErrorpRepresentar

Screen.MousePointer = vbHourglass

'Si se pasa un recurso y: si han cambiado el recurso y/o la fecha,
'se realiza todo el proceso
If CodRecursoNuevo <> 0 And (CodRecursoNuevo <> codRecurso Or datDate <> Fecha) Then
   'Vemos si pasamos el codigo de usuario del sistema
   If strUser <> "" Then
    strCodUsuario = strUser
   Else
    strCodUsuario = ""
   End If
   'Inicializaci�n de variables
   Frame2.Enabled = True 'Se activa el control
   codRecurso = CodRecursoNuevo 'Se establece el c�digo del recurso
   'Se establece el objeto de la conexi�n
   If Conexion Is Nothing Then Set Conexion = ConexionCW
   'Si no se le pasa la fecha, coge la actual
   If datDate = "0:00:00" Then datDate = SSMonth1.Date
'''    'S�lo se muestran 3 a�os: el anterior, el "actual" (strDate) y el siguiente
'''    SSMonth1.MaxDate = "31/12/" & (Year(datDate) + 1)
'''    SSMonth1.MinDate = "01/01/" & (Year(datDate) - 1)
   SSMonth1.DayofWeek(1).StyleSet = "Normal"
   SSMonth1.DayofWeek(2).StyleSet = "Normal"
   SSMonth1.DayofWeek(3).StyleSet = "Normal"
   SSMonth1.DayofWeek(4).StyleSet = "Normal"
   SSMonth1.DayofWeek(5).StyleSet = "Normal"
   SSMonth1.DayofWeek(6).StyleSet = "Normal"
   SSMonth1.DayofWeek(7).StyleSet = "Normal"
   Fecha = datDate
   SSMonth1.Date = Fecha 'Se pone la fecha en el control Mes
   bytMesActual = Month(datDate)
   cmdMes(bytMesActual - 1).BackColor = &HFFFFFF 'Color blanco
   txtComentarios.Text = ""

   If blnPrimera = False Then 'Primera vez que se ejecuta...
      blnPrimera = True 'La siguiente vez, ya no lo ser�
   Else 'Si no es la primera vez...
   ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      Call Horario.borrar_PictBox 'Se borra el Picture Box, antes de dibujar
   ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   End If
 
   'Se obtiene el nombre del recurso
   SQL = "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ?"
   Set qryRecurso = Conexion.CreateQuery("Recurso", SQL)
       qryRecurso(0) = codRecurso
   Set rstRecurso = qryRecurso.OpenResultset()
   If rstRecurso.EOF = False Then 'Si existe ese recurso...
      Frame2.Tag = rstRecurso(0) '...se guarda su descripci�n en el Tag
      Frame2.Caption = "Horario " & rstRecurso(0) '...se pone en el t�tulo del Frame
      txtComentarios.Enabled = True
      SSMonth1.Enabled = True

      'B�squeda de festivos semanales''''''''''''''''''''''''''''''''''''''''''''''''''''
      SQL = "SELECT AG08INDLUNFEST, AG08INDMARFEST, AG08INDMIEFEST, "
      SQL = SQL & "AG08INDJUEFEST, AG08INDVIEFEST, AG08INDSABFEST, AG08INDDOMFEST "
      SQL = SQL & "FROM AG0800, AG1100 "
      SQL = SQL & "WHERE AG1100.AG02CODCALENDA = AG0800.AG02CODCALENDA "
      SQL = SQL & "AND AG11CODRECURSO = ? "
      SQL = SQL & "AND TO_DATE(?, 'DD/MM/YYYY') > AG08FECINIPERI "
      SQL = SQL & "AND TO_DATE(?, 'DD/MM/YYYY') < AG08FECFINPERI"
      Set qryFestivos = Conexion.CreateQuery("", SQL)
        qryFestivos(0) = codRecurso
        qryFestivos(1) = Format(Fecha, "DD/MM/YYYY")
        qryFestivos(2) = qryFestivos(1)
      Set rstFestivos = qryFestivos.OpenResultset()
      Do While Not rstFestivos.EOF  'Si hay festivos semanales
        If rstFestivos(0) <> 0 Then SSMonth1.DayofWeek(1).StyleSet = "Ausente" 'Lunes
        If rstFestivos(1) <> 0 Then SSMonth1.DayofWeek(2).StyleSet = "Ausente" 'Martes
        If rstFestivos(2) <> 0 Then SSMonth1.DayofWeek(3).StyleSet = "Ausente" 'Mi�rcoles
        If rstFestivos(3) <> 0 Then SSMonth1.DayofWeek(4).StyleSet = "Ausente" 'Jueves
        If rstFestivos(4) <> 0 Then SSMonth1.DayofWeek(5).StyleSet = "Ausente" 'Viernes
        If rstFestivos(5) <> 0 Then SSMonth1.DayofWeek(6).StyleSet = "Ausente" 'S�bado
        If rstFestivos(6) <> 0 Then SSMonth1.DayofWeek(7).StyleSet = "Ausente" 'Domingo
        rstFestivos.MoveNext
      Loop
      rstFestivos.Close
      qryFestivos.Close
      
      'B�squeda de d�as especiales'''''''''''''''''''''''''''''''''''''''''''''''''''''''
      SQL = "SELECT AG03FECDIAESPE, AG03INDFESTIVO FROM AG0300, AG1100 "
      SQL = SQL & "WHERE AG1100.AG02CODCALENDA = AG0300.AG02CODCALENDA "
      SQL = SQL & "AND AG11CODRECURSO = ? "
      SQL = SQL & "AND TO_CHAR(AG03FECDIAESPE, 'YYYY') = ?"
          
      Set qryFestivos = Conexion.CreateQuery("", SQL)
          qryFestivos(0) = codRecurso
          qryFestivos(1) = Format(Fecha, "YYYY")
      Set rstFestivos = qryFestivos.OpenResultset()
           
      Do While Not rstFestivos.EOF  'Si hay d�as especiales
        intDia = Day(rstFestivos(0))
        intMes = Month(rstFestivos(0))
        
        If rstFestivos(1) = -1 Then
            SSMonth1.Month(intMes).Day(intDia).StyleSet = "Ausente"
        Else
            SSMonth1.Month(intMes).Day(intDia).StyleSet = "Normal"
        End If
        rstFestivos.MoveNext
      Loop
      'Se cierra la consulta
      rstFestivos.Close
      qryFestivos.Close
       
      'Se buscan las franjas activas para ese recurso y ese d�a as� como sus citas
      buscar_franjas
      buscar_citas

      '''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      Call Horario.dibujar_Horario
      ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      If datDate <> "0:00:00" Then pDiasLibres datDate, SSMonth1, codRecurso
   Else
      Frame2.Caption = ""
      lblMensaje.Visible = False
      lblFestivo.Visible = False
      lblDescripcion.Visible = False
      txtComentarios.Enabled = False
      SSMonth1.Enabled = False
   End If 'rstRecurso.EOF = False
   
   'Se cierra la consulta del nombre del recurso
   rstRecurso.Close
   qryRecurso.Close

End If 'CodRecursoNuevo <> 0 And (CodRecursoNuevo <> codRecurso Or strDate <> Fecha)

Screen.MousePointer = vbDefault


Exit Sub

ErrorpRepresentar:

    MsgBox "Error " & Err & " en la Representaci�n visual de la Agenda: " & Err.Description, _
            vbCritical, "Avisa al administrador del sistema"
    Screen.MousePointer = vbDefault

End Sub

'Busca las franjas para un recurso y un d�a
Private Sub buscar_franjas()

'''''''''''''''''''DECLARACI�N DE VARIABLES''''''''''''''''''''''''''''''''
Dim blnPrimeraFranja As Boolean 'Almacena si es la primera franja del recurso
Dim blnCitas As Boolean 'Almacena si hay citas en consulta o pruebas
Dim strActuaciones As String 'PR01DESCORTA para esa franja
'Consultas SQL...
Dim SQL As String 'Consulta SQL
Dim qryFranjasRec As rdoQuery 'Objeto de la consulta
Dim rstFranjasRec As rdoResultset 'Resultado de la consulta de franjas
Dim qryAct As rdoQuery
Dim rstAct As rdoResultset
'Cuando algunos campos son NULL...
Dim intIntervCitas As Integer 'Intervalo entre citas
Dim intNumCitasAdmi As Integer 'N�mero de citas admitidas
Dim intModoAsig As Integer 'Modo de asignaci�n del recurso en esa franja
Dim blnIndPlanif As Boolean 'Indicador de Planificable
Dim strDescrAct As String 'Descripci�n de la Actividad
'Para establecer intervalos sin actividad...
Dim horafinanterior As Integer 'Hora de fin de la franja anterior
Dim minfinanterior As Integer 'minuto de fin de la franja anterior

'Inicializaci�n de variables
blnPrimeraFranja = True 'Por defecto es la primera franja
blnCitas = False

'CONSULTA PARA CONOCER SI UN D�A-RECURSO ES FESTIVO
SQL = "SELECT 1 FROM DUAL WHERE TO_DATE(?, 'DD/MM/YYYY') IN(" 'qryFranjasRec(0) = SSMonth1.Date
   SQL = SQL & " SELECT AG03FECDIAESPE FROM AG0300, AG1100"
   SQL = SQL & " WHERE AG1100.AG02CODCALENDA = AG0300.AG02CODCALENDA"
   SQL = SQL & " AND AG11CODRECURSO = ?" 'qryFranjasRec(1) = codRecurso
   SQL = SQL & " AND AG03INDFESTIVO = -1)"
SQL = SQL & " OR 0 <>"
   SQL = SQL & " (SELECT COUNT(AG08CODPERVIGE) FROM AG0800, AG1100"
   SQL = SQL & " WHERE AG1100.AG02CODCALENDA = AG0800.AG02CODCALENDA"
   SQL = SQL & " AND AG11CODRECURSO = ?" 'qryFranjasRec(2) = codRecurso
   SQL = SQL & " AND (TO_DATE(?, 'DD/MM/YYYY') NOT IN(" 'qryFranjasRec(3) = SSMonth1.Date
      SQL = SQL & "  SELECT AG03FECDIAESPE FROM AG0300, AG1100"
      SQL = SQL & "  WHERE AG1100.AG02CODCALENDA = AG0300.AG02CODCALENDA"
      SQL = SQL & "  AND AG11CODRECURSO = ?" 'qryFranjasRec(4) = codRecurso
      SQL = SQL & "  AND AG03INDFESTIVO = 0))"
   SQL = SQL & " AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG08FECINIPERI AND AG08FECFINPERI "
   Select Case fstrBuscarDiaSemana(SSMonth1.Date)
      Case "Lunes"
         SQL = SQL & "AND AG08INDLUNFEST = -1 )"
      Case "Martes"
         SQL = SQL & "AND AG08INDMARFEST = -1 )"
      Case "Mi�rcoles"
7         SQL = SQL & "AND AG08INDMIEFEST = -1 )"
      Case "Jueves"
         SQL = SQL & "AND AG08INDJUEFEST = -1 )"
      Case "Viernes"
         SQL = SQL & "AND AG08INDVIEFEST = -1 )"
      Case "S�bado"
         SQL = SQL & "AND AG08INDSABFEST = -1 )"
      Case "Domingo"
         SQL = SQL & "AND AG08INDDOMFEST = -1 )"
   End Select

Set qryFranjasRec = Conexion.CreateQuery("", SQL)
    qryFranjasRec(0) = Format(SSMonth1.Date, "DD/MM/YYYY")
    qryFranjasRec(1) = codRecurso
    qryFranjasRec(2) = codRecurso
    qryFranjasRec(3) = qryFranjasRec(0)
    qryFranjasRec(4) = codRecurso
    qryFranjasRec(5) = qryFranjasRec(0)
Set rstFranjasRec = qryFranjasRec.OpenResultset()

If rstFranjasRec.EOF Then 'Si no es d�a festivo...
   rstFranjasRec.Close
   qryFranjasRec.Close
   'Se obtienen los datos de las franjas para ese recurso y...
   'SELECT
   SQL = "SELECT AG0400.AG04CODFRANJA, AG04HORINFRJHH, AG04HORINFRJMM, AG04HORFIFRJHH, "
   SQL = SQL & "AG04HORFIFRJMM, PR1200.PR12DESACTIVIDAD, AG04MODASIGCITA, AG04INTERVCITA, "
   SQL = SQL & "AG04NUMCITADMI, PR1200.PR12INDPLANIFIC, AG07CODPERFIL "
   'FROM
    SQL = SQL & "FROM AG0400, PR1200 "
   'WHERE
   SQL = SQL & "WHERE (AG04FECBAJA >= TO_DATE(?, 'DD/MM/YYYY') OR AG04FECBAJA IS NULL) AND " 'Franja activa
   SQL = SQL & "AG0400.PR12CODACTIVIDAD = PR1200.PR12CODACTIVIDAD(+) "
   SQL = SQL & "AND AG0400.AG11CODRECURSO = ? AND AG0400.AG07CODPERFIL = " 'qryFranjasRec(0) = codRecurso
      '...para el perfil cuyo periodo de vigencia contenga a la fecha y...
      'SELECT
      SQL = SQL & "(SELECT AG0700.AG07CODPERFIL "
      'FROM
      SQL = SQL & "FROM AG0700, AG0900 "
      'WHERE
      SQL = SQL & "WHERE AG0900.AG11CODRECURSO = ? " 'qryFranjasRec(1) = codRecurso
      SQL = SQL & "AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO "
      SQL = SQL & "AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL "
      Select Case fstrBuscarDiaSemana(SSMonth1.Date)
          Case "Lunes"
              SQL = SQL & "AND AG04INDLUNFRJA = -1 "
          Case "Martes"
              SQL = SQL & "AND AG04INDMARFRJA = -1 "
          Case "Mi�rcoles"
              SQL = SQL & "AND AG04INDMIEFRJA = -1 "
          Case "Jueves"
              SQL = SQL & "AND AG04INDJUEFRJA = -1 "
          Case "Viernes"
              SQL = SQL & "AND AG04INDVIEFRJA = -1 "
          Case "S�bado"
              SQL = SQL & "AND AG04INDSABFRJA = -1 "
          Case "Domingo"
              SQL = SQL & "AND AG04INDDOMFRJA = -1 "
      End Select
      SQL = SQL & "AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG09FECINVIPER AND AG09FECFIVIPER "
      SQL = SQL & "AND AG07INDPERGENE ="
          '...si s�lo est� el gen�rico (-1) ser� ese, a no ser que haya uno m�s (0)...
          'SELECT
          SQL = SQL & "(SELECT MAX(AG07INDPERGENE) "
          'FROM
          SQL = SQL & "FROM AG0900, AG0700 " ', AG0400 "
          'WHERE
          SQL = SQL & "WHERE AG0900.AG11CODRECURSO = ? " 'qryFranjasRec(3) = codRecurso
          SQL = SQL & "AND AG0700.AG11CODRECURSO = AG0900.AG11CODRECURSO "
          SQL = SQL & "AND AG0700.AG07CODPERFIL = AG0900.AG07CODPERFIL "
'          SQL = SQL & "AND AG0400.AG11CODRECURSO = AG0700.AG11CODRECURSO "
'          SQL = SQL & "AND AG0400.AG07CODPERFIL = AG0700.AG07CODPERFIL "
          '...y cuya franja sea la del d�a seleccionado
          Select Case fstrBuscarDiaSemana(SSMonth1.Date)
              Case "Lunes"
                 SQL = SQL & "AND AG04INDLUNFRJA = -1 "
              Case "Martes"
                 SQL = SQL & "AND AG04INDMARFRJA = -1 "
              Case "Mi�rcoles"
                 SQL = SQL & "AND AG04INDMIEFRJA = -1 "
              Case "Jueves"
                 SQL = SQL & "AND AG04INDJUEFRJA = -1 "
              Case "Viernes"
                 SQL = SQL & "AND AG04INDVIEFRJA = -1 "
              Case "S�bado"
                 SQL = SQL & "AND AG04INDSABFRJA = -1 "
              Case "Domingo"
                 SQL = SQL & "AND AG04INDDOMFRJA = -1 "
          End Select
          SQL = SQL & "AND TO_DATE(?, 'DD/MM/YYYY') BETWEEN AG09FECINVIPER AND AG09FECFIVIPER "
          SQL = SQL & "AND (AG07FECBAJA >= TO_DATE(?, 'DD/MM/YYYY') OR AG07FECBAJA IS NULL)" 'qryFranjasRec(5) = SSMonth1.Date
          SQL = SQL & ")"
      SQL = SQL & ") "
   'ORDER BY
   SQL = SQL & "ORDER BY AG04HORINFRJHH, AG04HORINFRJMM" 'Se ordena cronol�gicamente
      
    Set qryFranjasRec = Conexion.CreateQuery("", SQL)
        qryFranjasRec(0) = Format(SSMonth1.Date, "DD/MM/YYYY")
        qryFranjasRec(1) = codRecurso
        qryFranjasRec(2) = qryFranjasRec(1)
        qryFranjasRec(3) = Format(SSMonth1.Date, "DD/MM/YYYY")
        qryFranjasRec(4) = qryFranjasRec(1)
        qryFranjasRec(5) = qryFranjasRec(3)
        qryFranjasRec(6) = qryFranjasRec(3)
    Set rstFranjasRec = qryFranjasRec.OpenResultset()
    
    If rstFranjasRec.EOF = False Then 'Si hay franjas...
        
        txtComentarios.Text = ""
        lblMensaje.Visible = False 'Se hace invisible el mensaje de que no tiene franjas
        lblFestivo.Visible = False
        lblDescripcion.Visible = False
        Do While Not rstFranjasRec.EOF 'Mientras halla franjas...
            
            If IsNull(rstFranjasRec(8)) Then 'Si no hay citas admitidas...
                intNumCitasAdmi = 0 '...se establece como cero
            Else
                intNumCitasAdmi = rstFranjasRec(8) 'AG04NUMCITADMI
            End If
          
            If IsNull(rstFranjasRec(7)) Then
                intIntervCitas = 0
            Else
                intIntervCitas = rstFranjasRec(7) 'AG04INTERVCITA
            End If
          
            If IsNull(rstFranjasRec(6)) Then
                intModoAsig = 0
            Else
                intModoAsig = rstFranjasRec(6) 'AG04MODASIGCITA
            End If
            
            SQL = "SELECT PR01DESCORTA, AG01NUMASIGADM FROM PR0100, AG0100 WHERE "
            SQL = SQL & "AG11CODRECURSO = ? AND AG07CODPERFIL = ? AND AG04CODFRANJA = ? "
            SQL = SQL & "AND PR0100.PR01CODACTUACION = AG0100.PR01CODACTUACION"
            Set qryAct = Conexion.CreateQuery("", SQL)
               qryAct(0) = codRecurso
               qryAct(1) = rstFranjasRec(10)
               qryAct(2) = rstFranjasRec(0)
            Set rstAct = qryAct.OpenResultset()
            strActuaciones = ""
            If Not rstAct.EOF Then
               strActuaciones = rstAct(0) & " (" & rstAct(1) & ")"
               rstAct.MoveNext
               Do While Not rstAct.EOF
                  strActuaciones = strActuaciones & ", " & rstAct(0) & " (" & rstAct(1) & ")"
                  rstAct.MoveNext
               Loop 'Not rstAct.EOF
            End If 'Not rstAct.EOF
            rstAct.Close
            qryAct.Close
            If blnPrimeraFranja = True Then 'Si es la primera franja...
               ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               Call Horario.a�adir_ObjFranjas(0, rstFranjasRec(1) - 1, rstFranjasRec(2), _
                  rstFranjasRec(1), rstFranjasRec(2), "Fuera", 2, 0, 1, True, "")
               ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               blnPrimeraFranja = False '...la siguiente ya no lo ser�
            Else 'Si no es la primera franja...
         
               'Si la hora de inicio de la franja es <> de la hora de fin de la anterior...
               If (horafinanterior <> rstFranjasRec(1) Or _
                                                   minfinanterior <> rstFranjasRec(2)) Then
               ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  Call Horario.a�adir_ObjFranjas(0, horafinanterior, minfinanterior, _
               rstFranjasRec(1), rstFranjasRec(2), "Ausente", 0, 0, 0, False, "")
               ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               End If
            End If
            horafinanterior = rstFranjasRec(3) 'Se almacena la hora de fin de esta franja
            minfinanterior = rstFranjasRec(4) 'Se almacena el minuto de fin de esta franja
            
            If IsNull(rstFranjasRec(9)) Then
                blnIndPlanif = True
            Else
                blnIndPlanif = rstFranjasRec(9)
            End If
            If IsNull(rstFranjasRec(5)) Then
                strDescrAct = "Consulta/Pruebas"
            Else
                strDescrAct = rstFranjasRec(5)
            End If
            '''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            Call Horario.a�adir_ObjFranjas(rstFranjasRec(0), rstFranjasRec(1), _
                rstFranjasRec(2), rstFranjasRec(3), rstFranjasRec(4), strDescrAct, _
                intModoAsig, intIntervCitas, intNumCitasAdmi, blnIndPlanif, strActuaciones)
            '''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         
            'Si hay alguna franja con una actividad planificable, se buscar�n las citas
            If blnIndPlanif = True Then blnCitas = True
  
            rstFranjasRec.MoveNext 'Siguiente franja
        Loop
         ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         Call Horario.a�adir_ObjFranjas(0, horafinanterior, minfinanterior, _
                      horafinanterior + 1, minfinanterior, "Fuera", 2, 0, 1, True, "")
         ''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        'Si hay franjas citables, se buscan las citas para ese recurso y ese d�a
'        If blnCitas = True Then buscar_citas
    Else 'Si no hay franjas, se muestra un aviso en el Picture Box
        lblMensaje.Visible = True 'Se hace visible el label con el mensaje
        lblFestivo.Visible = False
        lblDescripcion.Visible = False
        txtComentarios.Text = "El recurso " & Mid(Frame2.Caption, 9) & _
                                                " no realiza tareas este d�a."
         '''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         Call Horario.a�adir_ObjFranjas(0, 0, 1, 13, 0, "Fuera", 1, 0, 1, True, "")
         Call Horario.a�adir_ObjFranjas(0, 13, 0, 23, 59, "Fuera", 1, 0, 1, True, "")
         '''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    End If
   rstFranjasRec.Close
   qryFranjasRec.Close
Else 'Si es un d�a festivo...
   rstFranjasRec.Close
   qryFranjasRec.Close
    'Se comprueba si es un d�a especial
    SQL = "SELECT AG03DESDIAESPE FROM AG0300, AG1100 "
    SQL = SQL & "WHERE TO_CHAR(AG03FECDIAESPE, 'DD/MM/YYYY') = ? "
    SQL = SQL & "AND AG1100.AG02CODCALENDA = AG0300.AG02CODCALENDA AND AG11CODRECURSO = ?"
    
    Set qryFranjasRec = Conexion.CreateQuery("", SQL)
     qryFranjasRec(0) = Format(SSMonth1.Date, "DD/MM/YYYY")
     qryFranjasRec(1) = codRecurso
    Set rstFranjasRec = qryFranjasRec.OpenResultset()
    
    If rstFranjasRec.EOF = False Then 'Si es un d�a especial...
       lblDescripcion.Caption = rstFranjasRec(0) '...pone la descripci�n
    Else
       lblDescripcion.Caption = ""
    End If
    lblFestivo.Visible = True 'Se muestra un mensaje
    lblDescripcion.Visible = True
    lblMensaje.Visible = False

End If
   
End Sub

'Busca las citas para un recurso y un d�a
Private Sub buscar_citas()

'''''''''''''''''''DECLARACI�N DE VARIABLES''''''''''''''''''''''''''''''''
'Consultas SQL...
Dim SQL As String 'Consulta SQL
Dim qryActCitadas As rdoQuery 'Objeto de la consulta
Dim rstActCitadas As rdoResultset 'Resultado de la consulta
Dim strPaciente As String 'Nombre completo del paciente
Dim strDesCorta As String 'Descripci�n corta de la actuaci�n

'Se obtienen los datos de las citas para ese recurso y ese d�a
'SELECT
SQL = "SELECT CI2700.CI31NUMSOLICIT, CI2700.CI01NUMCITA, CI2700.CI15NUMFASECITA, "
SQL = SQL & "TO_CHAR(CI2700.CI27FECOCUPREC,'DD/MM/YYYY HH24:MI:SS'), "
SQL = SQL & "TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YYYY HH24:MI:SS'), "
'Minutos de ocupaci�n del recurso
SQL = SQL & "1440*CI27NUMDIASREC+60*CI27NUMHORAREC+CI27NUMMINUREC, "
'Datos del paciente
SQL = SQL & "CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, "
SQL = SQL & "PR0100.PR01DESCORTA, CI01SITCITA " 'Datos de la actuaci�n
'FROM
SQL = SQL & "FROM CI2700, CI0100, CI2200, PR0400, PR0300, PR0100 "
'WHERE
SQL = SQL & "WHERE CI2700.AG11CODRECURSO = ? " 'qryActCitadas(0) = codRecurso
SQL = SQL & "AND CI0100.CI31NUMSOLICIT = CI2700.CI31NUMSOLICIT "
SQL = SQL & "AND CI0100.CI01NUMCITA = CI2700.CI01NUMCITA "
SQL = SQL & "AND TO_CHAR(CI2700.CI27FECOCUPREC, 'DD/MM/YYYY') = ? " 'qryActCitadas(1) = SSMonth1
SQL = SQL & "AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
SQL = SQL & "AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA "
SQL = SQL & "AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI "
SQL = SQL & "AND PR0300.PR01CODACTUACION = PR0100.PR01CODACTUACION "
SQL = SQL & "AND PR0100.PR12CODACTIVIDAD <> '209'" 'que no sea hospitalizacion
SQL = SQL & "AND CI01SITCITA NOT IN (2,3) " 'Anulada
'ORDER BY
SQL = SQL & "ORDER BY "
SQL = SQL & "CI2700.CI27FECOCUPREC, CI2700.CI31NUMSOLICIT, CI2700.CI15NUMFASECITA"
    
Set qryActCitadas = Conexion.CreateQuery("ActCitadas", SQL)
    qryActCitadas(0) = codRecurso
    qryActCitadas(1) = Format(SSMonth1.Date, "DD/MM/YYYY")
Set rstActCitadas = qryActCitadas.OpenResultset()

Do While Not rstActCitadas.EOF 'Mientras haya citas...
   strPaciente = StrConv(rstActCitadas(6) & " " & rstActCitadas(7) & " " & _
                     rstActCitadas(8), vbProperCase)
                     
   If IsNull(rstActCitadas(10)) Then strDesCorta = "" Else strDesCorta = rstActCitadas(10)
   
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   Call Horario.a�adir_ObjCitas(rstActCitadas(0), rstActCitadas(1), rstActCitadas(2), _
                              Right(rstActCitadas(3), 8), Right(rstActCitadas(4), 8), _
                              rstActCitadas(5), strPaciente, rstActCitadas(9), strDesCorta, Int(rstActCitadas(11).Value))
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    rstActCitadas.MoveNext 'Siguiente cita
    
Loop

rstActCitadas.Close
qryActCitadas.Close

End Sub

Private Sub actualizar_dibujo()

'Si el d�a anterior ten�a franjas, se borra el Picture Box, antes de dibujar
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
'If lblMensaje.Visible = False Then Call Horario.borrar_PictBox
Call Horario.borrar_PictBox
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

'Se buscan las franjas activas para ese recurso y ese d�a
buscar_franjas
buscar_citas

''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Call Horario.dibujar_Horario
''''''''''''''''CLASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

End Sub

Private Sub VScroll1_Change()
If VScroll1.Value = -32768 Then Exit Sub
Picture1.Top = Picture1.Top - (VScroll1.Value - intAnterior)
intAnterior = VScroll1.Value

End Sub
