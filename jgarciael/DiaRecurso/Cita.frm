VERSION 5.00
Begin VB.Form frmCita 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cita"
   ClientHeight    =   3135
   ClientLeft      =   2820
   ClientTop       =   2205
   ClientWidth     =   6330
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3135
   ScaleWidth      =   6330
   Begin VB.Frame fraSolicitante 
      Height          =   2235
      Left            =   120
      TabIndex        =   14
      Top             =   240
      Visible         =   0   'False
      Width           =   6075
      Begin VB.TextBox txtSolicitante 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   540
         Width           =   5775
      End
      Begin VB.TextBox txtSolicitante 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   1560
         Width           =   5775
      End
      Begin VB.Label lblSolicitante 
         Caption         =   "Dr. Solicitante"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label lblSolicitante 
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   1200
         Width           =   1815
      End
   End
   Begin VB.Frame fraPaciente 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2355
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   6075
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   480
         Width           =   5775
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   1200
         Width           =   5775
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   5
         Left            =   4980
         MaxLength       =   9
         TabIndex        =   5
         Top             =   1920
         Width           =   915
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   2
         Left            =   120
         MaxLength       =   9
         TabIndex        =   4
         Top             =   1920
         Width           =   915
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   3
         Left            =   1140
         MaxLength       =   9
         TabIndex        =   3
         Top             =   1920
         Width           =   1935
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   4
         Left            =   3180
         MaxLength       =   9
         TabIndex        =   2
         Top             =   1920
         Width           =   1695
      End
      Begin VB.Label lblPaciente 
         Caption         =   "Nombre completo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   13
         Top             =   180
         Width           =   1815
      End
      Begin VB.Label lblPaciente 
         Caption         =   "Direcci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Top             =   900
         Width           =   1395
      End
      Begin VB.Label lblPaciente 
         Caption         =   "Tel�fono"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   4980
         TabIndex        =   11
         Top             =   1620
         Width           =   915
      End
      Begin VB.Label lblPaciente 
         Caption         =   "C.P."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   10
         Top             =   1620
         Width           =   915
      End
      Begin VB.Label lblPaciente 
         Caption         =   "Localidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   1140
         TabIndex        =   9
         Top             =   1620
         Width           =   915
      End
      Begin VB.Label lblPaciente 
         Caption         =   "Provincia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   3180
         TabIndex        =   8
         Top             =   1620
         Width           =   915
      End
   End
   Begin VB.CommandButton cmdCita 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   0
      Top             =   2640
      Width           =   1095
   End
End
Attribute VB_Name = "frmCita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCita_Click()

   Unload Me

End Sub

Private Sub Form_Activate()

Dim SQL As String
Dim qryPaciente As rdoQuery
Dim rstPaciente As rdoResultset

'Direcci�n del paciente (si tiene)
SQL = "SELECT CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI07CODPOSTAL, CI10TELEFONO "
SQL = SQL & "FROM CI1000 "
SQL = SQL & "WHERE CI21CODPERSONA = ?" 'frmCita.Tag

Set qryPaciente = Conexion.CreateQuery("Direccion", SQL)
qryPaciente(0) = frmCita.Tag
Set rstPaciente = qryPaciente.OpenResultset()

If Not rstPaciente.EOF Then
   txtPaciente(1) = rstPaciente(0) & " n � " & rstPaciente(1)
   If Not IsNull(rstPaciente(2)) Then txtPaciente(1) = txtPaciente(1) & " - " & rstPaciente(2)
   If Not IsNull(rstPaciente(3)) Then txtPaciente(2) = rstPaciente(3)
   If Not IsNull(rstPaciente(4)) Then txtPaciente(5) = rstPaciente(4)
End If
   
If Not rstPaciente.ActiveConnection Is Nothing Then rstPaciente.Close
qryPaciente.Close

'Municipio del paciente (si hay)
SQL = "SELECT CI17DESMUNICIP "
SQL = SQL & "FROM CI1000, CI1700 "
SQL = SQL & "WHERE CI1700.CI17CODMUNICIP = CI1000.CI17CODMUNICIP "
SQL = SQL & "AND CI21CODPERSONA = ?" 'frmCita.Tag

Set qryPaciente = Conexion.CreateQuery("Municipios", SQL)
qryPaciente(0) = frmCita.Tag
Set rstPaciente = qryPaciente.OpenResultset()

If Not rstPaciente.EOF Then txtPaciente(3) = rstPaciente(0)

If Not rstPaciente.ActiveConnection Is Nothing Then rstPaciente.Close
qryPaciente.Close

'Provincia del paciente (si hay)
SQL = "SELECT CI26DESPROVI "
SQL = SQL & "FROM CI1000, CI2600 "
SQL = SQL & "WHERE CI2600.CI26CODPROVI = CI1000.CI26CODPROVI "
SQL = SQL & "AND CI21CODPERSONA = ?" 'frmCita.Tag

Set qryPaciente = Conexion.CreateQuery("Municipios", SQL)
qryPaciente(0) = frmCita.Tag
Set rstPaciente = qryPaciente.OpenResultset()

If Not rstPaciente.EOF Then txtPaciente(4) = rstPaciente(0)

If Not rstPaciente.ActiveConnection Is Nothing Then rstPaciente.Close
qryPaciente.Close

'Solicitante
SQL = "SELECT AD02DESDPTO, AG11DESRECURSO "
SQL = SQL & "FROM CI0100, PR0400, PR0300, PR0900, SG0200, AD0200, AG1100 "
SQL = SQL & "WHERE PR0900.SG02COD = SG0200.SG02COD "
SQL = SQL & "AND CI0100.CI31NUMSOLICIT = ? " 'qryPaciente(0) = txtSolicitante(0).Tag
SQL = SQL & "AND CI0100.CI01NUMCITA = ? " 'qryPaciente(1) = txtSolicitante(1).Tag
SQL = SQL & "AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
SQL = SQL & "AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI "
SQL = SQL & "AND PR0300.PR09NUMPETICION = PR0900.PR09NUMPETICION "
SQL = SQL & "AND PR0900.AD02CODDPTO = AD0200.AD02CODDPTO "
SQL = SQL & "AND AG1100.SG02COD = PR0900.SG02COD"

Set qryPaciente = Conexion.CreateQuery("Solicitante", SQL)
qryPaciente(0) = txtSolicitante(0).Tag 'Se pasa en el Tag el n� de solicitud
qryPaciente(1) = txtSolicitante(1).Tag 'Se pasa en el Tag el n� de cita
Set rstPaciente = qryPaciente.OpenResultset()

If Not rstPaciente.EOF Then
    txtSolicitante(0) = rstPaciente(1)
    txtSolicitante(1) = rstPaciente(0)
End If

If Not rstPaciente.ActiveConnection Is Nothing Then rstPaciente.Close
qryPaciente.Close

End Sub

''''Private Sub tabCita_Click()
''''
''''   If tabCita.SelectedItem.Index = 1 Then 'Paciente
''''      fraSolicitante.Visible = False
''''      fraPaciente.Visible = True
''''   Else 'Solicitante
''''      fraPaciente.Visible = False
''''      fraSolicitante.Visible = True
''''   End If
''''
''''End Sub

