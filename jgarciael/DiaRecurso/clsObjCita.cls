VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsObjCita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private lngNumSolicit As Long, lngNumCita As Long, intNumFaseCita As Integer
Private intHoraOcupRec As Integer, intMinOcupRec As Integer
Private intHoraConcert As Integer, intMinConcert As Integer
Private lngTiempoOcupRecMinutos As Long, strDescAct As String
Private strPaciente As String, lngCodPaciente As Long
Private intSitCita As Integer
Public Property Let SitCita(intSitCitaNuevo As Integer)
   intSitCita = intSitCitaNuevo
End Property
Public Property Get SitCita() As Integer
   SitCita = intSitCita
End Property
Public Property Let numSolicit(lngNumSolicitNuevo As Long)
   lngNumSolicit = lngNumSolicitNuevo
End Property

Public Property Get numSolicit() As Long
   numSolicit = lngNumSolicit
End Property

Public Property Let numCita(lngNumCitaNuevo As Long)
   lngNumCita = lngNumCitaNuevo
End Property

Public Property Get numCita() As Long
   numCita = lngNumCita
End Property

Public Property Let numFaseCita(intNumFaseCitaNuevo As Integer)
   intNumFaseCita = intNumFaseCitaNuevo
End Property

Public Property Get numFaseCita() As Integer
   numFaseCita = intNumFaseCita
End Property

Public Property Let horaOcupRec(intHoraOcupRecNuevo As Integer)
   intHoraOcupRec = intHoraOcupRecNuevo
End Property

Public Property Get horaOcupRec() As Integer
   horaOcupRec = intHoraOcupRec
End Property

Public Property Let minOcupRec(intMinOcupRecNuevo As Integer)
   intMinOcupRec = intMinOcupRecNuevo
End Property

Public Property Get minOcupRec() As Integer
   minOcupRec = intMinOcupRec
End Property

Public Property Let horaConcert(intHoraConcertNuevo As Integer)
   intHoraConcert = intHoraConcertNuevo
End Property

Public Property Get horaConcert() As Integer
   horaConcert = intHoraConcert
End Property

Public Property Let minConcert(intMinConcertNuevo As Integer)
   intMinConcert = intMinConcertNuevo
End Property

Public Property Get minConcert() As Integer
   minConcert = intMinConcert
End Property

Public Property Let tiempoOcupRecMinutos(lngTiempoOcupRecMinutosNuevo As Long)
   lngTiempoOcupRecMinutos = lngTiempoOcupRecMinutosNuevo
End Property

Public Property Get tiempoOcupRecMinutos() As Long
   tiempoOcupRecMinutos = lngTiempoOcupRecMinutos
End Property

Public Property Let Paciente(strPacienteNuevo As String)
   strPaciente = strPacienteNuevo
End Property

Public Property Get Paciente() As String
   Paciente = strPaciente
End Property

Public Property Let desActuacion(strDescActNuevo As String)
   strDescAct = strDescActNuevo
End Property

Public Property Get desActuacion() As String
   desActuacion = strDescAct
End Property

Public Property Let codPaciente(lngCodPacienteNuevo As Long)
   lngCodPaciente = lngCodPacienteNuevo
End Property

Public Property Get codPaciente() As Long
   codPaciente = lngCodPaciente
End Property
