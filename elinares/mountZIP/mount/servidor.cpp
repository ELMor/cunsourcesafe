
extern "C" { 
	#include "tcp4u.h" 
}

#define Mount		"mount -t vfat -o umask=000 /dev/hdc1 /mnt/zip"
#define Umount		"umount /mnt/zip"


#define SZBUFFER 10

int main(void){
	SOCKET LSock, CSock;
	unsigned short usPort=7101;
	int Rc;
	char *pszBuf=(char*)malloc(SZBUFFER);

	Tcp4uInit ();
	if((Rc=TcpGetListenSocket (&LSock, NULL, &usPort, 1))!=TCP4U_SUCCESS){
		return -1;
	}
	while(1){
		Rc = TcpAccept (&CSock, LSock, 0);
		Rc = TcpRecv (CSock, pszBuf, SZBUFFER, 60, HFILE_ERROR);
		switch(*pszBuf){
		case '0':
			system(Umount);
			break;
		case '1':
			system(Mount);
			break;
		}
		TcpClose (&CSock);
	} 
	Tcp4uCleanup ();
	return 0;
}
