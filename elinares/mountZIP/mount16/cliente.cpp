extern "C" {
	#include "tcp4u.h"
	#include <string.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <io.h>
}

#define PORT		501
int main(int argc, char *argv[]){
	SOCKET sock;
	unsigned short Puerto=PORT;
	int ret;
	char Buff[16];
	_wsetexit(_WINEXITNOPERSIST);
	if(argc!=2){
		puts("mount [-m] [-u] [-?]");
		return 0;
	}
	Tcp4uInit();							
	if(	(ret=TcpConnect(&sock,"159.237.128.211",NULL,&Puerto))!=TCP4U_SUCCESS){
		printf("Error en la conexion: %s\n",Tcp4uErrorString(ret));
		return -1;
	}
	switch( argv[1][1] ){
	case 'm':
		if(	(ret=TcpSend(sock,"1",2,0,HFILE_ERROR))!=TCP4U_SUCCESS){
			printf("Error en el comando: %s\n",Tcp4uErrorString(ret));
			return -1;
		}
		break;
	case 'u':
		if(	(ret=TcpSend(sock,"0",2,0,HFILE_ERROR))!=TCP4U_SUCCESS){
			printf("Error en el comando: %s\n",Tcp4uErrorString(ret));
			return -1;
		}
		break;
	case '?':
		if(	(ret=TcpSend(sock,"2",2,0,HFILE_ERROR))!=TCP4U_SUCCESS){
			printf("Error en el comando: %s\n",Tcp4uErrorString(ret));
			return -1;
		}
		TcpRecv(sock,Buff,15,0,HFILE_ERROR);
		puts(Buff);
		break;
	}
	TcpClose(&sock);
	Tcp4uCleanup();
	return 0;
}
