Attribute VB_Name = "StrBin"
Option Explicit
Private Cmd$(32)

Public Function Pack1(s1$) As String
    Cmd(1) = s1: Pack1 = Pack(Cmd, 1)
End Function

Public Function Pack2(s1$, s2$) As String
    Cmd(1) = s1: Cmd(2) = s2: Pack2 = Pack(Cmd, 2)
End Function

Public Function Pack3(s1$, s2$, s3$) As String
    Cmd(1) = s1: Cmd(2) = s2: Cmd(3) = s3: Pack3 = Pack(Cmd, 3)
End Function

Public Function Pack4(s1$, s2$, s3$, s4$) As String
    Cmd(1) = s1: Cmd(2) = s2: Cmd(3) = s3: Cmd(4) = s4: Pack4 = Pack(Cmd, 4)
End Function

Private Function Pack(Values$(), n&) As String
    Dim i&, sout$, head$
    For i = 1 To n
        If Len(Values(i)) > 7 Then ' Comprobar si ya tiene cabecera
            On Error Resume Next
            Err = 0
            head = CStr(CLng(Mid$(Values(i), 5, 4)))
            If Err = 0 Then
                head = ""
            Else
                head = "0001" & Format(Len(Values(i)), "0000")
            End If
        Else
            head = "0001" & Format(Len(Values(i)), "0000")
        End If
        
        sout = sout & head & Values(i)
    Next i
    Pack = Format(n, "0000") & Format(Len(sout), "0000") & sout
End Function

Public Sub UnPack(Values$(), n&, sin)
    Dim i&, pos&, lon&
    n = CLng(Left(sin, 4)): pos = 9
    For i = 1 To n
        lon = CLng(Mid$(sin, pos + 4, 4))
        Values(i) = Mid$(sin, pos, 8 + lon)
        pos = pos + 8 + lon
    Next i
End Sub

Public Function NumPackInPack(sin$) As Long
    NumPackInPack = CLng(Left(sin, 2))
End Function

Public Function GetBody(sin As String) As String
    GetBody = Right$(sin, CLng(Mid$(sin, 5, 4)))
End Function

Public Sub UnPackAndGetBody(Values$(), n&, sin$)
    Dim i&
    Call UnPack(Values, n, sin)
    For i = 1 To n
        Values(i) = GetBody(Values(i))
    Next i
End Sub
