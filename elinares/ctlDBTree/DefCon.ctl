VERSION 5.00
Begin VB.UserControl DefCon 
   ClientHeight    =   5505
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6855
   ScaleHeight     =   5505
   ScaleWidth      =   6855
   Begin VB.Frame f1 
      Height          =   2895
      Left            =   3240
      TabIndex        =   1
      Top             =   360
      Width           =   3015
      Begin VB.TextBox at 
         Height          =   285
         Index           =   0
         Left            =   1680
         TabIndex        =   11
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox at 
         Height          =   285
         Index           =   1
         Left            =   1680
         TabIndex        =   10
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox at 
         Height          =   285
         Index           =   2
         Left            =   1680
         TabIndex        =   9
         Top             =   960
         Width           =   1215
      End
      Begin VB.TextBox at 
         Height          =   285
         Index           =   3
         Left            =   1680
         TabIndex        =   8
         Top             =   1320
         Width           =   1215
      End
      Begin VB.CheckBox ac 
         Caption         =   "Necesario ?"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   1680
         Width           =   1335
      End
      Begin VB.CheckBox ac 
         Caption         =   "Suficiente ?"
         Height          =   255
         Index           =   1
         Left            =   1560
         TabIndex        =   6
         Top             =   1680
         Width           =   1335
      End
      Begin VB.CheckBox ac 
         Caption         =   "Excluido ?"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   5
         Top             =   1920
         Width           =   1455
      End
      Begin VB.CheckBox ac 
         Caption         =   "Descontable ?"
         Height          =   255
         Index           =   3
         Left            =   1560
         TabIndex        =   4
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CheckBox ac 
         Caption         =   "L�nea Obligatoria ?"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   3
         Top             =   2280
         Width           =   1695
      End
      Begin VB.CheckBox ac 
         Caption         =   "Facturaci�n Obligatoria ?"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   2
         Top             =   2520
         Width           =   2175
      End
      Begin VB.Label al 
         Caption         =   "Precio Fijo"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   975
      End
      Begin VB.Label al 
         Caption         =   "Franquicia de Suma"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   14
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label al 
         Caption         =   "Franquicia Unitaria"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label al 
         Caption         =   "Per�odo Garant�a"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   12
         Top             =   1320
         Width           =   1455
      End
   End
   Begin ctlDBTree.DBTree dbt 
      Height          =   4695
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   8281
   End
End
Attribute VB_Name = "DefCon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private rdoc As rdoConnection
Private rq(1 To 2) As New rdoQuery
Private chi$, cpa$

Private Sub ac_Click(Index As Integer)
    SaveData
End Sub

Private Sub at_Change(Index As Integer)
    SaveData
End Sub

Private Sub dbt_NodoSeleccionado(codigo As String, cpadre As String, nombre As String)
    Dim rs As rdoResultset
    f1.Caption = "Categor�a " & nombre
    rq(1).rdoParameters(0) = cpadre
    rq(1).rdoParameters(1) = codigo
    
    Set rs = rq(1).OpenResultset
    cpa = cpadre
    chi = codigo
    at(0).Text = IIf(IsNull(rs(0)), 0, rs(0))
    at(1).Text = IIf(IsNull(rs(1)), 0, rs(1))
    at(2).Text = IIf(IsNull(rs(2)), 0, rs(2))
    at(3).Text = IIf(IsNull(rs(3)), 0, rs(3))
    ac(0).Value = IIf(IsNull(rs(4)), 0, rs(4))
    ac(1).Value = IIf(IsNull(rs(5)), 0, rs(5))
    ac(2).Value = IIf(IsNull(rs(6)), 0, rs(6))
    ac(3).Value = IIf(IsNull(rs(7)), 0, rs(7))
    ac(4).Value = IIf(IsNull(rs(8)), 0, rs(8))
    ac(5).Value = IIf(IsNull(rs(9)), 0, rs(9))
        
    rs.Close
    Set rs = Nothing
End Sub


Private Sub SaveData()
    Dim i%
    If cpa = "" And chi = "" Then Exit Sub
    For i = 0 To 3
        rq(2).rdoParameters(i) = at(i).Text
    Next
    For i = 0 To 5
        rq(2).rdoParameters(i + 4) = ac(i).Value
    Next
    rq(2).rdoParameters(10) = cpa
    rq(2).rdoParameters(11) = chi
    rq(2).Execute
End Sub

Private Sub UserControl_Resize()
    If UserControl.Width < 3500 Then UserControl.Width = 3500
    If UserControl.Height < 2900 Then UserControl.Height = 2900
    
    dbt.Height = UserControl.Height
    dbt.Width = UserControl.Width - 3030
    dbt.Top = 0
    dbt.Left = 0
    
    f1.Height = UserControl.Height
    f1.Width = 3000
    f1.Top = 0
    f1.Left = dbt.Width + 50
End Sub

Public Sub Inicializa(rdo As rdoConnection, tn$, c$, d$, t$, tr$, p$, h$)
    Dim i%
    Set rdoc = rdo
    rq(1).SQL = "select pre,fsu,fun,pga,nec,suf,exc,dtb,lob,fob from xx02 " & _
        "where cpa=? and chi=?"
    rq(2).SQL = "update xx02 set pre=?, fsu=?, fun=?,pga=?,nec=?,suf=?,exc=?," & _
        "dtb=?,lob=?,fob=? where cpa=? and chi=?"
        
    For i = 1 To UBound(rq)
        Set rq(i).ActiveConnection = rdoc
        rq(i).Prepared = True
    Next
    Call dbt.Inicializa(rdoc, tn, c, d, t, tr, p, h)
End Sub
