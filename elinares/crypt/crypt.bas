Attribute VB_Name = "TEA"
Option Explicit
Private Const bits32 As Double = 4294967296#
Private Const bit16 As Double = 65536#
Private Const delta As Double = 2654435769#
Private Const uucode As String = "0123456789ABCDEFGHIJKLMNOPQRSTUV"

Private Function tb32(op1 As Double) As Double
    Dim op As Double
    op = op1
    While (op > bits32): op = op - bits32: Wend
    While (op < 0): op = op + bits32: Wend
    tb32 = Fix(op)
End Function

Private Function mult(op1 As Double, op2 As Double) As Double
    Dim n As Double
    n = op1 * op2
    n = tb32(n)
    mult = n
End Function

Private Function divi(op1 As Double, op2 As Double) As Double
    Dim n As Double
    n = op1 / op2
    n = tb32(n)
    divi = n
End Function

Public Function str2long(str$, pos%) As Double
    Dim ret#
    ret = ret + Asc(Mid$(str, pos + 0, 1))
    ret = ret * 256
    ret = ret + Asc(Mid$(str, pos + 1, 1))
    ret = ret * 256
    ret = ret + Asc(Mid$(str, pos + 2, 1))
    ret = ret * 256
    ret = ret + Asc(Mid$(str, pos + 3, 1))
    str2long = tb32(ret)
End Function

Public Function long2str(op#) As String
    Dim s$, op2#
    s = Space$(4)
    op2 = op
    Mid(s, 1, 1) = Chr(Fix(op2 / 16777216#))
    op2 = op2 - Asc(Mid(s, 1, 1)) * 16777216#
    Mid(s, 2, 1) = Chr(Fix(op2 / 65536#))
    op2 = op2 - Asc(Mid(s, 2, 1)) * 65536#
    Mid(s, 3, 1) = Chr(Fix(op2 / 256#))
    op2 = op2 - Asc(Mid(s, 3, 1)) * 256#
    Mid(s, 4, 1) = Chr(op2)
    long2str = s
End Function

Private Function UXor(op1#, op2#) As Double
    Dim l1&, l2&, l3&, l4&, ret#
    l1 = divi(tb32(op1), 65536#)
    l2 = op1 - l1 * 65536#
    l3 = divi(tb32(op2), 65536#)
    l4 = op2 - l3 * 65536#
    ret = (l1 Xor l3) * 65536# + (l2 Xor l4)
    UXor = ret
End Function

Private Function rare(op1#, op2#, op3#, op4#) As Double
    Dim au1#
    au1 = tb32(mult(op1, 16))
    au1 = tb32(au1 + UXor(op2, op1))
    au1 = tb32(au1 + UXor(op3, divi(op1, 32)))
    au1 = tb32(au1 + op4)
    rare = au1
End Function

Private Sub enc64(inp$, out$, key$)
    Dim y#, z#, sum#, a#, b#, c#, d#, n%
    a = str2long(key, 1)
    b = str2long(key, 5)
    c = str2long(key, 9)
    d = str2long(key, 13)
    y = str2long(inp, 1)
    z = str2long(inp, 5)
    For n = 1 To 32
        sum = tb32(sum + delta)
        y = tb32(y + rare(z, a, sum, b))
        z = tb32(z + rare(y, c, sum, d))
    Next
    out = long2str(y) & long2str(z)
End Sub

Private Sub dec64(inp$, out$, key$)
    Dim y#, z#, sum#, a#, b#, c#, d#, n%
    sum = tb32(3337565984#)
    a = str2long(key, 1)
    b = str2long(key, 5)
    c = str2long(key, 9)
    d = str2long(key, 13)
    y = str2long(inp, 1)
    z = str2long(inp, 5)
    For n = 1 To 32
        z = tb32(z - rare(y, c, sum, d))
        y = tb32(y - rare(z, a, sum, b))
        sum = tb32(sum - delta)
    Next
    out = long2str(y) & long2str(z)
End Sub

Public Sub encriptar(inp$, out$, key$)
    Dim s$, l%, aux$
    s = inp + Space(8 - Len(inp) Mod 8)
    For l = Len(inp) + 1 To Len(s)
        Mid(s, l, 1) = Chr(0)
    Next
    out = ""
    For l = Len(s) / 8 To 1 Step -1
        enc64 Mid(s, (l - 1) * 8 + 1, 8), aux, key
        out = out & aux
    Next
End Sub

Public Sub desencriptar(inp$, out$, key$)
    Dim l%, aux$
    out = ""
    While Asc(Left(inp, 1)) = 0: inp = Right(inp, Len(inp) - 1): Wend
    For l = Len(inp) / 8 To 1 Step -1
        dec64 Mid(inp, (l - 1) * 8 + 1, 8), aux, key
        out = out & aux
    Next
    While Asc(Right(out, 1)) = 0
        out = Left(out, Len(out) - 1)
    Wend
End Sub

' cv Supone que s$ es una tira de bits tomada de Byte en Byte,
' en los que s�lo son significativos los 'm%' primeros bits.
' De esta tira queremos coger el grupo n�mero 'x%' de un formateo
' de 'b%' bits por grupo
Private Function cv#(str$, lon%, efe%, bits%, xgrp%)
    Dim p%(0 To 7), i%, cpos%, ret#, char%
    p(0) = 1: For i = 1 To 7: p(i) = p(i - 1) * 2: Next i
    For i = xgrp * bits To (xgrp + 1) * bits - 1
        cpos = lon - Fix(i / efe)
        If cpos > 0 Then char = Asc(Mid(str, cpos, 1)) Else char = 0
        ret = ret + IIf((char And p(i Mod efe)) = 0, 0, 1) * p(i Mod bits)
    Next
    cv = ret
End Function

Public Function uuencode(inp$) As String
    Dim i%, out$, c%
    For i = 0 To Len(inp) * 8 / 5
        c = cv(inp, Len(inp), 8, 5, i)
        out = Mid(uucode, 1 + c, 1) & out
    Next
    uuencode = out
End Function

Public Function uudecode(inp$) As String
    Dim i%, aux$, out$, c%
    aux = inp
    For i = 1 To Len(aux)
        Mid(aux, i, 1) = Chr(InStr(uucode, Mid(aux, i, 1)) - 1)
    Next i
    out = ""
    For i = 0 To Len(aux) * 5 / 8
        c = cv(aux, Len(aux), 5, 8, i)
        out = Chr(c) & out
    Next
    uudecode = out
End Function
