VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4575
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3420
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4575
   ScaleWidth      =   3420
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   1
      Left            =   240
      TabIndex        =   7
      Text            =   "1"
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Desencriptar"
      Height          =   375
      Index           =   1
      Left            =   240
      TabIndex        =   6
      Top             =   4080
      Width           =   2895
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Encriptar"
      Height          =   375
      Index           =   0
      Left            =   240
      TabIndex        =   5
      Top             =   3480
      Width           =   2895
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   5
      Left            =   240
      TabIndex        =   4
      Top             =   2880
      Width           =   2895
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   4
      Left            =   1800
      TabIndex        =   3
      Text            =   "7"
      Top             =   2040
      Width           =   1335
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   3
      Left            =   240
      TabIndex        =   2
      Text            =   "5"
      Top             =   2040
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   2
      Left            =   1800
      TabIndex        =   1
      Text            =   "3"
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Text            =   "Eladio"
      Top             =   480
      Width           =   2895
   End
   Begin VB.Label Label1 
      Caption         =   "Texto Encriptado:"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   13
      Top             =   2640
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   "Clave 4"
      Height          =   255
      Index           =   3
      Left            =   1800
      TabIndex        =   12
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Clave 3"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   11
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Clave 2"
      Height          =   255
      Index           =   1
      Left            =   1800
      TabIndex        =   10
      Top             =   960
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Clave 1"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   9
      Top             =   960
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Texto a Encriptar:"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   8
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click(Index As Integer)
    Dim key$, inp$, out$
    key = long2str(Text1(4).Text) & long2str(Text1(3).Text) & _
          long2str(Text1(2).Text) & long2str(Text1(1).Text)
    Select Case Index
        Case 0
            encriptar Text1(0).Text, out, key
            Text1(5).Text = uuencode(out)
        Case 1
            desencriptar uudecode(Text1(5).Text), out, key
            Text1(0).Text = out
    End Select
End Sub
