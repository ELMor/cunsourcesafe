VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{06A3578E-B69D-11D2-8257-00C04F7E08E7}#56.0#0"; "logger.ocx"
Object = "{D2B988D5-F8A2-11D2-84E4-00C04F7DC189}#1.0#0"; "LVComputers.ocx"
Object = "{D2B98915-F8A2-11D2-84E4-00C04F7DC189}#1.0#0"; "TVSelection.ocx"
Begin VB.Form IFSAA01 
   Caption         =   "Interfaz del Servicio de Actualización de Software"
   ClientHeight    =   5925
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8655
   Icon            =   "IFSAA01.frx":0000
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   5925
   ScaleWidth      =   8655
   StartUpPosition =   3  'Windows Default
   Begin LVComp.LVComputers LVComputers1 
      Height          =   3615
      Left            =   480
      TabIndex        =   11
      Top             =   1320
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   6376
   End
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      TabIndex        =   10
      Top             =   5535
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   688
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            AutoSize        =   1
            Object.Width           =   14764
            TextSave        =   ""
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin TVSelection.TVSelect TVSelect1 
      Height          =   3735
      Left            =   600
      TabIndex        =   1
      Top             =   960
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   6588
   End
   Begin VB.Frame Frame1 
      Caption         =   "Inicio y Progreso de Trabajos"
      Height          =   4815
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   8415
      Begin VB.Frame Frame2 
         Caption         =   "Control"
         Height          =   4455
         Left            =   6840
         TabIndex        =   4
         Top             =   240
         Width           =   1455
         Begin VB.CheckBox Check2 
            Caption         =   "Detallado"
            Height          =   255
            Left            =   120
            TabIndex        =   16
            Top             =   2280
            Width           =   1215
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   120
            TabIndex        =   14
            Text            =   "10"
            Top             =   1920
            Width           =   495
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Refrescar"
            Height          =   375
            Left            =   120
            TabIndex        =   13
            Top             =   2640
            Width           =   1215
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Refresco Automatico"
            Height          =   375
            Left            =   120
            TabIndex        =   12
            Top             =   1440
            Width           =   1215
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   120
            TabIndex        =   8
            Top             =   600
            Width           =   1215
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Borrar Log"
            Height          =   375
            Index           =   2
            Left            =   120
            TabIndex        =   7
            Top             =   3480
            Width           =   1215
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Salir"
            Height          =   375
            Index           =   1
            Left            =   120
            TabIndex        =   6
            Top             =   3960
            Width           =   1215
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Iniciar"
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   5
            Top             =   960
            Width           =   1215
         End
         Begin VB.Label Label2 
            Caption         =   "seg."
            Height          =   255
            Left            =   840
            TabIndex        =   15
            Top             =   1920
            Width           =   375
         End
         Begin VB.Label Label1 
            Caption         =   "Trabajo:"
            Height          =   255
            Left            =   120
            TabIndex        =   9
            Top             =   360
            Width           =   735
         End
      End
      Begin pLogger.logger logger1 
         Height          =   4575
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   8070
      End
   End
   Begin ComctlLib.TabStrip ts1 
      Height          =   5535
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   9763
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   3
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "1º) Escoja Computadoras"
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "2º) Escoja Archivos de la misma"
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "3º) Iniciar proceso / Salir de la aplicacion"
            Key             =   ""
            Object.Tag             =   ""
            Object.ToolTipText     =   "Nombra el trabajo y lleva cuenta de las incidencias"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "IFSAA01"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private cFrame As Integer

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        If CInt("0" & Text2.Text) > 0 Then
            logger1.RefrAuto = Text2.Text
            Command2.Enabled = False
        Else
            MsgBox "Escriba un numero superior a 0 segundos"
            logger1.RefrAuto = 0
            Check1.Value = 0
            Text2.Text = 0
        End If
    Else
        logger1.RefrAuto = 0
        Command2.Enabled = True
    End If
End Sub

Private Sub Check2_Click()
    If Check2.Value = 1 Then
        logger1.NivDetalle = 0
    Else
        logger1.NivDetalle = 4
    End If
End Sub

Private Sub Command2_Click()
    logger1.Refrescar
End Sub

Private Sub Form_Load()
    Call TodoInvisible
    TVSelect1.Drive = "c"
    logger1.LogFile = "c:\temp\logSrvActAut.txt"
    logger1.RefrAuto = 0
    cFrame = 0
    Call ts1_Click
End Sub

Private Sub Command1_Click(Index As Integer)
    Dim s$, fname$, command$, dd&, nl$
    MousePointer = 11
    nl = Chr(13) & Chr(10)
    Select Case Index
        Case 1:  ' Finalizar
            End
        Case 0:  ' Enviar Trabajo
            If Text1.Text = "" Then
                MsgBox ("Introduzca un nombre para el trabajo")
            Else
                s$ = "Trabajo " & Text1.Text & nl
                s$ = s$ & "Notifica " & Environ("COMPUTERNAME") & nl
                s$ = s$ & "Grupos { " & LVComputers1.getCompTree & " } " & nl
                s$ = s$ & "Ficheros { " & TVSelect1.Ficheros & " } " & nl
                fname = Environ("TEMP") & "\" & Text1.Text & ".jobfile"
                Open fname For Output As #2
                Print #2, s
                Close #2
                Shell App.Path & "\srvactaut.exe -j " & fname
                MsgBox "Trabajo enviado"
                Kill fname
                Text1.Text = ""
            End If
        Case 2:  ' Borrar el Log
            Shell App.Path & "\srvactaut.exe -l", vbMinimizedNoFocus
            For dd = 1 To 10000: DoEvents: Next
            logger1.Reload
    End Select
    MousePointer = 0
End Sub

Private Sub TodoInvisible()
    TVSelect1.Visible = False
    LVComputers1.Visible = False
    Frame1.Visible = False
End Sub

Private Sub Form_Resize()
    Dim h&, w&
    If IFSAA01.Height < 5790 Then IFSAA01.Height = 5790
    If IFSAA01.Width < 6360 Then IFSAA01.Width = 6360
    
    h = IFSAA01.Height - StatusBar1.Height - 1000
    w = IFSAA01.Width - 1000
    
    ts1.Top = 0: ts1.Left = 0: ts1.Height = h + 600: ts1.Width = w + 850
    
    Frame1.Top = 400: Frame1.Left = 200
    Frame1.Height = h: Frame1.Width = w + 500
    logger1.Top = 200: logger1.Left = 200
    logger1.Height = h - 350
    logger1.Width = Frame1.Width - Frame2.Width - logger1.Left - 200
    Frame2.Top = logger1.Top
    Frame2.Left = logger1.Left + logger1.Width + 100
    Frame2.Height = logger1.Height
    
    Command1(1).Top = Frame2.Top + Frame2.Height - Command1(1).Height - 300
    Command1(2).Top = Command1(1).Top - Command1(2).Height - 40
    
    LVComputers1.Top = 400: LVComputers1.Left = 200
    LVComputers1.Height = h: LVComputers1.Width = w + 500
    
    TVSelect1.Top = 400: TVSelect1.Left = 200
    TVSelect1.Height = h: TVSelect1.Width = w + 500
    
End Sub

Private Sub LVComputers1_NewMaster(cname As String)
    TVSelect1.Computer = cname
    TVSelect1.Drive = "C"
End Sub

Private Sub Text3_Change()
    If CInt(Text3.Text) > 0 Then logger1.NivDetalle = CInt(Text3.Text)
End Sub

Private Sub ts1_Click()
    If ts1.SelectedItem.Index <> cFrame Then
        Call TodoInvisible
        Select Case ts1.SelectedItem.Index
            Case 1
                LVComputers1.Visible = True
                StatusBar1.Panels(1).Text = "Escoja la computadora que va a " & _
                    "actuar como servidora principal y sus hjas. Cada hija " & _
                    "puede ser a su vez Servidora de las nietas de la principal."
            Case 2
                TVSelect1.Visible = True
                StatusBar1.Panels(1).Text = "Estos son los archivos de " & _
                    "la computadora principal. La Computadora principal" & _
                    " debe tener compartido C$ para los administradores" & _
                    " del Dominio."
            Case 3
                Frame1.Visible = True
                StatusBar1.Panels(1).Text = "El LOG se refresca cada 10 seg."
        End Select
        cFrame = ts1.SelectedItem.Index
    End If
End Sub

