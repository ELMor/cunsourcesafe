#include "ctlserv.h"
#include "decompile.h"
#include "srvsocks.h"
#include "logging.h"

extern SERVICE_STATUS          ssStatus;       
extern SERVICE_STATUS_HANDLE   sshStatusHandle;
extern DWORD                   dwErr ;
extern BOOL                    bDebug ;
extern TCHAR                   szErr[256];



VOID CmdStartService(char *pszNC) {     
	SERVICE_STATUS ssStatus; 
    DWORD dwOldCheckPoint;      
    SC_HANDLE   schSCManager;
    SC_HANDLE   schService;

    schSCManager = OpenSCManager(
                        pszNC,                  // machine (NULL == local)
                        NULL,                   // database (NULL == default)
                        SC_MANAGER_ALL_ACCESS   // access required
                        );
    if ( schSCManager == NULL)         
        _tprintf(TEXT("Fallo OpenSCManager - %s\n"), GetLastErrorText(szErr,256));

    schService = OpenService(schSCManager, TEXT(SZSERVICENAME), SERVICE_ALL_ACCESS);

    if (!StartService( schService, 0, NULL) ) 
        _tprintf(TEXT("Fallo StartService - %s\n"), GetLastErrorText(szErr,256));
	else 
        printf("Servicio pendiente de arrancar\n");  

    // Check the status until the service is no longer start pending.  
    if (!QueryServiceStatus(schService, &ssStatus) )  
        _tprintf(TEXT("Fallo QueryServiceStatus - %s\n"), GetLastErrorText(szErr,256));
    while (ssStatus.dwCurrentState == SERVICE_START_PENDING){ 
        dwOldCheckPoint = ssStatus.dwCheckPoint;  
        Sleep(ssStatus.dwWaitHint);
        if (!QueryServiceStatus( schService,&ssStatus) ) 
            break;  
        if (dwOldCheckPoint >= ssStatus.dwCheckPoint)             
			break;     
	} 
    if (ssStatus.dwCurrentState == SERVICE_RUNNING) 
        printf("StartService O.K.\n");     
	else     { 
        printf("  Estado Actual: %d\n",ssStatus.dwCurrentState); 
        printf("  Codigo Salida: %d\n",ssStatus.dwWin32ExitCode); 
        printf("  '' Especifico: %d\n",ssStatus.dwServiceSpecificExitCode); 
        printf("  Check Point  : %d\n",ssStatus.dwCheckPoint); 
        printf("  Esperas      : %d\n",ssStatus.dwWaitHint);     
	}  
    CloseServiceHandle(schService); 
} 

