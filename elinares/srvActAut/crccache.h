

#ifndef __CRCCACHE_H__
#define __CRCCACHE_H__

typedef struct _crcc {
	char *fname;
	unsigned long crc32;
	long st_mtime;
} crc_cache;

#endif