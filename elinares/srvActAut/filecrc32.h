
#ifndef __FILECRC32_H__
#define __FILECRC32_H__

#ifdef __cplusplus
extern "C" {
#endif

int GetFileCRC32(char *fname, unsigned long* crc);

#ifdef __cplusplus
}
#endif

#endif