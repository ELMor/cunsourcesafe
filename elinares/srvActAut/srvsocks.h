/*
 * Fichero srvsock.h
 */

#ifndef __SRVSOCKS_H__
#define __SRVSOCKS_H__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <process.h>
#include <sys/stat.h>
#include "decompile.h"

#define SizeXMitBuf	(16*1024)
#define SizeCommandBuffer (256*1024)

#define DEFAULT_SERVICE_PORT 7101
#define DEFAULT_NOTIFY_PORT  7102

#define szQrGetFile		"QrGetFile"
#define szQrBatch		"QrBatch"
#define szRsBatch		"RsBatch"
#define szRsFileOK		"RsFileOK"
#define szFileCRC32		"RsFileCRC32"
#define szFileNotFound	"RsFileNotFound"
#define szSendFile		"QrSendFile"
#define szLogInfo		"Loginfo"
#define szQrPing		"QrPing"
#define szRsPing		"RsPing"

#define LogFileName "c:\\temp\\logsrvActAut.txt"
#define PURGE_LOG_FILE "rsPurgeLog"

#define SAME_REMOTE_VERSION	 1
#define OLDER_REMOTE_VERSION 2
#define NEWER_REMOTE_VERSION 3


//funciones Principales de Threads. MainSock a su vez genera m�s threads.
void __cdecl MainSock  ( void *p );
void         ThreadSock( void *lpv );

//envio de mensajes a Computadoras remotas.
int    SendCommandToCPUs(LisAct* la, CPU  *cpu, char *cmd);
SOCKET SendCommandToCPU (char *cna, char *comm, unsigned long lon, int Puerto=DEFAULT_SERVICE_PORT);
int	   SendCommandToCPU(LisAct* la, CPU *cpu, char *cmd);

//Actualizar ficheros desde computadora remota
void   GetRemoteFiles(LisAct *la);
int    BuildCommand(char* jn, char* ns, char* fs, CPUs* cpus, Fics *fics, char *buf);

//Exports de conexi�n para otras aplicaciones.
int  RsBatch(SOCKET CSock, char *cmd);
//Gestion de comunicaciones
int Ping(char *cname, char *version=NULL);
int RsPing(SOCKET CSock, char *cmd);
int QrBatch(SOCKET CSock, char *cmd);
int QrGetFile(SOCKET CSock, char *cmd);

//Inicio del servidor de recogida de mensajes
void InitMensajesSrvr(void);

typedef struct _DspSck {
	char *Command;
	int (*Handler)(SOCKET, char*);
} DispatchTable;

extern "C" {
	void copyDNS(char* dst,char* org);
	int  __declspec(dllexport) __stdcall BatchJob(char* command);
}

void CompruebaInstalaciones(LisAct* la, CPU* pc);


#endif