/*
 * Fichero logging.h
 */


#ifndef __LOGGING_H__
#define __LOGGING_H__

#include "Decompile.h"

void AddToMessageLog(char* lpszMsg);
void SendMessageTo(LisAct* la, char* pname, char* fname, char *msg);
static char Msg[1024];
void RecInitLogger( LisAct*, CPU* );
void InitLogger( LisAct* );

#endif