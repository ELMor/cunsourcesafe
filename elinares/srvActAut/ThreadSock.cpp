#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"

extern BOOL bDebug;

DispatchTable dsptab[]={
	{szQrGetFile,QrGetFile},
	{szQrBatch	,QrBatch  },
	{szRsBatch	,RsBatch  },
	{szRsPing	,RsPing   },
	{NULL       ,NULL     }
};

void ThreadSock( void *lpv ){
	unsigned long longitud,leidos;
	int i;
	SOCKET *pCSock=(SOCKET*)lpv;
	char *xBuf=(char*)malloc(SizeCommandBuffer);
	//Recibir el comando a ejecutar (Primero la longitud y luego el comando)
	recv(*pCSock,(char*)&longitud,sizeof(longitud),0);
	for(leidos=0;
		leidos<longitud;
		leidos+=recv(*pCSock,xBuf+leidos,SizeCommandBuffer-leidos,0)
		)
		;	//Cuerpo vac�o
	for(i=0;dsptab[i].Command;i++)
		if(!stricmp(xBuf,dsptab[i].Command)){
			(*dsptab[i].Handler)(*pCSock,xBuf);
			closesocket( *pCSock );
			free(lpv);
			break;
		}
	free(xBuf);
	_endthread();
}

