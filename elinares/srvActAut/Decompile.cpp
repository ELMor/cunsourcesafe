/*
 * Fichero Decompile.cpp
 */


#include "logging.h"
#include "decompile.h"
#include "CtlServ.h"

extern char		*actpar;

void setLis(LisAct *l, CPU *c, Fics *f, char* jname,char *nserver){
	l->cpu=c;
	l->fic=f;
	l->jobname=strdup(jname);
	l->notifysrv=strdup(nserver);
}

void setLis(LisAct *l,void* cp, void *f, void* j, void* n){
	setLis((LisAct*)l,(CPU*)cp,(Fics*)f,(char*)j,(char*)n);
}

CPU* newCPU(char *Nom, CPUs* cpus){
	CPU *c=(CPU*)malloc(sizeof(CPU));
	c->cname= (Nom ? strdup(Nom) : Nom );
	c->cpus=cpus;
	if(cpus)
		cpus->cpu_padre=c;
	return c;
}

CPU* newCPU (void *Nom,void* cpu){
	return newCPU((char*)Nom,(CPUs*) cpu);
}

CPUs* newCPUs(void){
	CPUs* c=(CPUs*)malloc(sizeof(CPUs));
	c->ncpu=0;
	c->cpu=NULL;
	c->cpu_padre=NULL;
	return c;
}

CPUs* addCPUtoCPUs(CPU *c, CPUs *sc){
	if(!sc) sc=newCPUs();
	sc->cpu=(CPU**)realloc(sc->cpu,(1+sc->ncpu)*sizeof(void*));
	sc->cpu[sc->ncpu]=c;
	sc->ncpu++;
	return sc;
}
CPUs* addCPUtoCPUs(void *c, void *sc){
	return addCPUtoCPUs((CPU*)c,(CPUs*)sc);
}

Fics* newFic(void){
	Fics* f=(Fics*)malloc(sizeof(Fics));
	f->carfic=0;
	f->fname=NULL;
	f->path=NULL;
	f->cmd=NULL;
	f->delay=NULL;
	f->downloaded=NULL;
	return f;
}

void addFictoFics(Fics *f, char* fn,char *pa, char *cm, int d){

	f->fname     =(char**)realloc(f->fname,     (1+f->carfic)*sizeof(char*));
	f->path      =(char**)realloc(f->path ,     (1+f->carfic)*sizeof(char*));
	f->cmd       =(char**)realloc(f->cmd  ,     (1+f->carfic)*sizeof(char*));
	f->delay     =(char* )realloc(f->delay,     (1+f->carfic)*sizeof(char ));
	f->downloaded=(char* )realloc(f->downloaded,(1+f->carfic)*sizeof(char ));

	f->fname[f->carfic]=strdup(fn);
	f->path [f->carfic]=strdup(pa);
	f->cmd  [f->carfic]= cm ? strdup(cm) : NULL ;
	f->delay[f->carfic]= d;
	f->carfic++;
}

void addFictoFics(void *f, void* fn, void *pa, void *cm){
	addFictoFics((Fics*)f,(char*)fn,(char*)pa,(char*)cm);
}

//Liberar la estructura CPU
void FreeCPU(CPU *cpu){
	free( cpu->cname );
	if(cpu->cpus){
		int i;
		for(i=0;i<cpu->cpus->ncpu;i++)
			FreeCPU( (CPU*)(cpu->cpus->cpu[i]) );
	}
}

//Liberar la estructura Fics
void FreeFIC(Fics *fic){
	int j;
	for(j=0;j<fic->carfic;j++){
		free(fic->fname[j]);
		free(fic->path [j]);
		free(fic->cmd  [j]);
	}
	free(fic->fname);
	free(fic->path);
	free(fic->cmd);
	free(fic->delay);
}

//Borrar todas las listas de Actualizacion (las 'np' listas)
void FreeAll(LisAct *p){
	int j;
	FreeFIC(p->fic);
	p->fic=NULL;
	for(j=0;p[0].cpu->cpus && j<p[0].cpu->cpus->ncpu;j++)
		FreeCPU( (CPU*)(p[0].cpu->cpus->cpu[j]) );
}

//Llamada al YaCC para realizar el parsing del JOB

int Decompile(char* in, LisAct* lis){
	actpar=in;
	if(	yyparse((void*)lis) )
		return 0;
	return 1;
}

