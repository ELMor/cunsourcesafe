/*
 * Fichero grammy.
 */

%{

//Includes Generales
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <winsock2.h>
#include "decompile.h"

//Tipo de los tokens
#define YYSTYPE void*
#define YYPARSE_PARAM lis

//Prototipos.
void yyerror(char*s);
int  yylex(void);

//Ofrecer mas informacion si hay error
#define YYERROR_VERBOSE

//Cadena para hacer el parsing.
char    *actpar;

%}

%token NOM
%token GRP	"Grupos"
%token FIL	"Ficheros"
%token NOT	"Notifica"
%token JNA	"Trabajo"
%token_table

%%
programs: program
		| programs program
;
program	: JNA NOM NOT NOM Grps Files	
										{ setLis((LisAct*)lis,$5,$6,$2,$4);
										}
;
Files	: FIL '{' fic '}'				{ $$=$3;							}
;
fic		:								{ $$=newFic();						}
		| fic NOM '{' NOM '}'			{ addFictoFics($1,$2,$4,NULL);$$=$1;}
		| fic NOM '{' NOM NOM '}'		{ addFictoFics($1,$2,$4,$5);$$=$1;	}
;
Grps	: GRP '{' cpu '}'				{ $$=$3;							}
;
cpus	: cpu							{ $$=addCPUtoCPUs($1,NULL);			}
		| cpus cpu						{ $$=addCPUtoCPUs($2,$1);			}
;
cpu		: NOM							{ $$=newCPU($1);					}
		| NOM '{' cpus '}'				{ $$=newCPU($1,$3);					}
;
%%


int yylex(){
	char *ret,c;

	c=*actpar;
	while( isspace(c) )
		c=*(++actpar);
	if(c==0 || c=='{' || c=='}' || c==':' ){
		++actpar;
		return c;
	}
	ret=actpar;
	if(c=='\"'){	//Entre comillas
		actpar++;
		ret=actpar;
		while( *actpar!='\"' && *actpar!=0 )
			actpar++;
		if(!*actpar){
			printf("Error: No hay fin de comillas");
			return 0;
		}else{
			*actpar++=0;
			yylval=ret;
			return NOM;
		}
	}
	while( !isspace(*actpar) && *actpar!=0)
		actpar++;
	if(!*actpar){
		return 0;
	}else{
		*actpar++=0;
		yylval=ret;
		if(! stricmp((char*)yylval,"Grupos") )
			return GRP;
		if(! stricmp((char*)yylval,"Ficheros") )
			return FIL;
		if(! stricmp((char*)yylval,"Trabajo") )
			return JNA;
		if(! stricmp((char*)yylval,"Notifica") )
			return NOT;
		return NOM;
	}
	return c;
}



void yyerror(char *s){
	printf("Error: %s\n",s);
}

