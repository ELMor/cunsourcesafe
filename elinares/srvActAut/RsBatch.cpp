
#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"

extern BOOL bDebug;


//RsBatch\0\Comandos\0
//RsBatch NO actualiza la m�quina local, simplemente emite los comandos de 
//Actualizaci�n a los jefe de grupo de las m�quinas remotas, si los hay.
int RsBatch(SOCKET sock, char *cmd){
	LisAct lis;
	int ErrorFound=0,j;

	Decompile(cmd+strlen(cmd)+1,&lis);

	char buf[512];
	int estan=1;
	//Comprobar que los archivos implicados existen.
	Fics* fic=lis.fic;
	for(j=0;j<fic->carfic;j++){
		struct stat st1;
		strcat(strcat(strcpy(buf,fic->path[j]),"\\"),fic->fname[j]);
		if(stat(buf,&st1)){
			sprintf(Msg,"srvActAut Error: No existe el archivo %s.",buf);
			SendMessageTo(&lis,fic->path[j],fic->fname[j],
				Msg);
			estan=0;
		}
	}
	//Enviarlos
	if(estan){
		SendMessageTo(&lis,"","","Todos los archivos existen en el servidor");
		SendCommandToCPUs(&lis,lis.cpu,szQrBatch);
	}else{
		SendMessageTo(&lis,"","","TRABAJO DETENIDO!!!");
	}
	FreeAll(&lis);
	if( sock!=NULL )
		closesocket(sock);
	return 0;
}
