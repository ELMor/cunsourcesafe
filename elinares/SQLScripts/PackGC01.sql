Drop Package Gc01;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
Create Package Gc01 As
   Procedure DupSys;
   Procedure PurgeLog(ProcNum number);
   Procedure XDelete(                 Tabla Varchar2, Cond Varchar2, log Number default 0);
   Procedure XInsert(Dblink Varchar2, Tabla Varchar2, Cond Varchar2, log Number default 0);
   Procedure XCopy  (Dblink Varchar2, Tabla Varchar2, Cond Varchar2, log Number default 0);
End Gc01;
/

Create Package Body Gc01 As
   /* Reglas:
         - Toda tabla tiene que tener Primary Key.
            (*) Si una tabla no tiene PK pero tampoco tiene tablas hijas, entonces se ignora en los XINSERT
            (**) Los XDelete contra los padres de estas tablas fallar�n
         - Todo campo que intervenga en un Primary Key debe ser number, varchar2, char o date.
         - Se han de construir unas tabla (gc0700,gc0800,gc0900,gc1000 y gc1100) inicialmente (execute gc01.dupsys;).
         - Si hay alg�n cambio/borrado/a�adido en la Integridad Referencial, hay que volver a ejecutar gc01.dupsys
   */         
   subtype matvar2 is dbms_sql.varchar2s;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*    Procedimientos y Funciones Privados                                                                                           */
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Function AliasFields(alias varchar2, fields varchar2) return varchar2 is
   Begin
      return alias||'.'||Replace(fields,',',','||alias||'.');
   End AliasFields;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Function GetLPKSize(t varchar2) return number is
      Sz Number;
   Begin
      select gc08Size into Sz from gc.gc0800 where gc08Tipo='P' and gc08thija=t;
      Return Sz;
   End GetLPKSize;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Function GetLPK(t varchar2, extendeduk number default 0) return Varchar2 is
      Cursor c1(tabla1 varchar2) is
         Select gc08lfkt from gc.gc0800 where gc08Tipo='U' and gc08THija=tabla1;
      tt varchar2(32000);
   Begin
      select gc08lfkt into tt from gc.gc0800 where gc08Tipo='P' and gc08thija=t;
      if extendeduk=1 then
         for r1 in c1(t) Loop
            tt:=tt||','||r1.gc08lfkt;
         End Loop;
      elsif extendeduk=2 then
         tt:='*';
         For r1 in c1(t) Loop
            if tt='*' then tt:=r1.gc08lfkt; else tt:=tt||','||r1.gc08lfkt; End If;
         End Loop;
      End if;
      return tt;
   end;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Function GetLPKEq(p number, t varchar2, a1 varchar2, a2 varchar2) return varchar2 is
      ret varchar2(32000);
   Begin
      ret:='(' || AliasFields(a1,GetLPK(t)) || ')=(' || AliasFields(a2,GetLPK(t)) || ')';
      Return ret;
   End GetLPKEq;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Function SaveCmd(pn number, nv Number, lic number, pk number, cm Number, tt varchar2, UpDown Number) return number is
      Linea number;
   Begin
      Select nvl(1+max(gc07linea),1) into Linea from gc.gc0700 where gc07codpro=pn;
      Insert Into Gc.Gc0700(Gc07Codpro,gc07Level,Gc07Linea,Gc07lic,gc07nrows,Gc07pk08,Gc07InsDel,Gc07Ttemp,Gc07UpDown)
         Values(pn,nv,Linea,lic,0,pk,cm,tt,UpDown);
      Commit;      
      return Linea;
   End SaveCmd;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Function ExeSQL(ProcNum Number, TexSQL matvar2) return number is
      NumFilas    number;
      dynSQL      number;
      i           number;
      a           Varchar2(256);
   Begin
   
      Select nvl(1+max(nvl(gc11Linea,0)),1) into NumFilas From gc.gc1100 Where gc11CodPro=ProcNum;
      for i in 1..TexSQL.count Loop
         a:=TexSQL(i);
         Insert into gc.gc1100(gc11CodPro,gc11Linea,gc11Texto) values (ProcNum,NumFilas+i-1,a);
         Commit;
      End Loop;
      
      dynSQL:=dbms_sql.Open_Cursor;
      dbms_sql.Parse(dynSQL,TexSQL,1,TexSQL.Count,false,dbms_sql.v7);
      NumFilas:=dbms_sql.Execute(dynSQL);
      dbms_sql.Close_Cursor(dynSQL);
      return NumFilas;
   Exception
      When others then
         a:=SQLERRM(SQLCODE);
         RollBack;
         Select nvl(1+max(nvl(gc11Linea,0)),1) into NumFilas From gc.gc1100 Where gc11CodPro=ProcNum;
         insert into gc.gc1100(gc11CodPro,gc11Linea,gc11Texto) 
            values(ProcNum,NumFilas,a);
         Commit;            
         if dbms_sql.is_open(dynSQL) then
            dbms_sql.close_cursor(dynSQL);
         End If;
         Return -9;      
   End ExeSQL;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Function GetTName(ProcNum Number, tabla Varchar2) return varchar2 is
   Begin
      return 'GC.TMP'||ProcNum||Replace(tabla,'.','_o_');
   End GetTName;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure ListValues(ProcNum Number,t varchar2, q in out matvar2) is
      C        integer;
      lpk      varchar2(2000);      
      lv       varchar2(256);
      Type     taon is table of Number          index by binary_integer;
      Type     taov is table of Varchar2(2000)  index by binary_integer;
      Type     taod is table of date            index by binary_integer;
      aon      taon;
      aov      taov;
      aod      taod;
      Sz       Number;
      Cursor   cType(tab varchar2) is Select gc10pos p,gc10tipo t from gc.gc1000 where gc10tab=tab order by gc10pos asc;
      Tipo_No_Soportado_En_PK Exception;
      Counter  Number;
      tsq      Varchar2(2000);
   Begin
      tsq:='Select count(*) A from '||GetTName(ProcNum,t);
      c:=dbms_sql.open_cursor;
      dbms_sql.parse(c,tsq,dbms_sql.v7);
      dbms_sql.define_column(c,1,Counter);
      Sz:=dbms_sql.execute(c);
      if dbms_sql.fetch_rows(c)>0 then
         dbms_sql.column_value(c,1,Counter);
      else
         Counter:=0;
      End if;
      dbms_sql.close_cursor(c);
      if Counter<254 then 
         lpk:=GetLPK(t);
         tsq:='Select '||lpk||' From '||GetTName(ProcNum,t);
         c:=dbms_sql.open_cursor;
         dbms_sql.parse(c,tsq,dbms_sql.v7);
         for r in cType(t) Loop
            if    r.t='N' then
               aon(r.p):=0;
               dbms_sql.define_column(c,r.p,aon(r.p));
            elsif r.t='V' or r.t='C' then
               aov(r.p):='';
               dbms_sql.define_column(c,r.p,aov(r.p),2000);
            elsif r.t='F' then
               aod(r.p):=NULL;
               dbms_sql.define_column(c,r.p,aod(r.p));
            else
               raise Tipo_No_Soportado_En_PK;
            End If;
         End Loop;
         Sz:=dbms_sql.execute(c);
         lv:='(('||lpk||') in ((';
         Counter:=0;
         Loop
            if dbms_sql.fetch_rows(c)>0 then
               for r in cType(t) Loop
                  if    r.t='N'            then aon(r.p):=0;    dbms_sql.column_value(c,r.p,aon(r.p));
                  elsif r.t='V' or r.t='C' then aov(r.p):='';   dbms_sql.column_value(c,r.p,aov(r.p));
                  elsif r.t='F'            then aod(r.p):=NULL; dbms_sql.column_value(c,r.p,aod(r.p));
                  End If;
               End Loop;
               if Counter>0 then lv:=lv||',('; End if;
               for r in cType(t) Loop
                  if r.p>1 then lv:=lv||','; end if;
                  if    r.t='N'            then lv:=lv||aon(r.p);
                  elsif r.t='V' or r.t='C' then lv:=lv||chr(39)||aov(r.p)||chr(39);
                  elsif r.t='F'            then lv:=lv||'to_date('||chr(39)||to_char(aod(r.p),'YYYYMMDDHH24MISS')||chr(39)||','||chr(39)||'YYYYMMDDHH24MISS'||chr(39)||')';
                  End If;
               End Loop;
               lv:=lv||')';
               if length(lv)>150 then
                  q( q.count+1 ):=lv;
                  lv:='';
               End If;
               if Counter>250 then
                  Counter:=-1;
                  q( q.count+1 ):=lv||')';
                  lv:=' or ('||lpk||') in ((';
               End If;
               Counter:=Counter+1;
            else
               Exit;
            End If;
         End Loop;
         dbms_sql.close_cursor(c);
         if Counter>0 then 
            q( q.count+1 ):=lv||')) ';      
         end if;
      Else
         q( q.count+1 ):='(('||GetLPK(t)||') in (Select '||GetLPK(t)||' From '||GetTName(ProcNum,t)||')) ';
      End If;         
   End ListValues;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure BuildTSQUp(p number, pad varchar2, hij varchar2, dbl varchar2, lfko varchar2, lfkt varchar2, tsq out matvar2) is
      tsq2  matvar2;
      tsq3  matvar2;
      ndx   Number;
   Begin
      ListValues(p,pad,tsq2);
      ListValues(p,hij,tsq3);
      if tsq3.count>0 and tsq2.count=0 then
         tsq(1):='Insert into '||GetTName(p,pad)||' Select '||GetLPK(pad,1)||' From '||pad||dbl;
         tsq(2):=' Where '||'('||lfko||') in (select distinct '||lfkt||' From '||hij||dbl||' Where ';
         For ndx in 1..tsq3.count Loop
            tsq(tsq.Count+1):=tsq3(ndx);
         End Loop;
         tsq(tsq.Count+1):=')';
      elsif tsq3.count>0 and tsq2.count>0 then
         tsq(1):='Insert into '||GetTName(p,pad)||' Select '||GetLPK(pad,1)||' From '||pad||dbl;
         tsq(2):=' Where ';
         tsq(3):='('||lfko||') in (select distinct '||lfkt||' From '||hij||dbl||' Where ';
         For ndx in 1..tsq3.Count Loop
            tsq( tsq.Count+1 ):=tsq3( ndx );
         End Loop;
         tsq( tsq.Count+1 ):=') And Not ';
         For ndx in 1..tsq2.Count Loop
            tsq( tsq.Count+1 ):=tsq2( ndx );
         End Loop;
      end if;
   End BuildTSQUp;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure BuildTSQDown(p number, pad varchar2, hij varchar2, dbl varchar2, lfko varchar2, lfkt varchar2, tsq out matvar2) is
      ndx   Number;
      tsq2  matvar2;
      tsq3  matvar2;
   Begin
      ListValues(p,hij,tsq2);
      ListValues(p,pad,tsq3);
      if tsq2.count=0 and tsq3.count>0 then
         tsq(1):='Insert into '||GetTName(p,hij)||' Select '||GetLPK(hij,1)||' From '||hij||dbl||' Where ';
         tsq(2):='('||lfkt||') in (select distinct '||lfko||' From '||pad||dbl||' Where ';
         For ndx in 1..tsq3.Count Loop
            tsq(tsq.Count+1):=tsq3(ndx);
         End Loop;
         tsq(tsq.Count+1):=')';
      elsif tsq2.count>0 and tsq3.count>0 then
         tsq(1):='Insert into '||GetTName(p,hij)||' Select '||GetLPK(hij,1)||' From '||hij||dbl||' Where ';
         tsq(2):='('||lfkt||') in (select distinct '||lfko||' From '||pad||dbl||' Where ';
         For ndx in 1..tsq3.Count Loop
            tsq(tsq.Count+1):=tsq3(ndx);
         End Loop;
         tsq(tsq.Count+1):=') And Not ';
         For ndx in 1..tsq2.Count Loop
            tsq(tsq.Count+1):=tsq2(ndx);
         End Loop;
      end if;
   End BuildTSQDown;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Function DetectCiclos(rid RowID) return number is
      Padre    Varchar2(32000);
      Hija     Varchar2(32000);
      Ciclo    Number;
      Cursor   cTree(Tab Varchar2) is
         Select Level,Rowid,gc08tpadre,gc08thija From gc.gc0800
            Start  with gc08TPadre=Tab and gc08Ciclo=0 and gc08Tipo='R'
            Connect By  Prior gc08THija=gc08TPadre and gc08Ciclo=0 and gc08Tipo='R';
      tree     cTree%RowType;
      Type     tRI is table of RowID Not Null index by binary_integer;
      path     tRI;
      i        Number;
      Ret      Number:=0;
   Begin
      Select gc08tpadre,gc08thija,gc08Ciclo into Padre,Hija,Ciclo from gc.gc0800 where rowid=rid;
      if Ciclo=0 then
         Select nvl(1+max(gc08ciclo),1) into ciclo from gc.gc0800;
         update gc.gc0800 set gc08ciclo=Ciclo where rowid=rid;
         if Padre=Hija then
            Ret:=1;
         else
            For tree in CTree( Hija ) Loop 
               path( tree.level ):=tree.RowID;
               If tree.gc08THija=Padre then
                  Ret:=Ret+1;
                  For i in 1..tree.level Loop
                     update gc.gc0800 set gc08Ciclo=Ciclo where rowid=path(i);
                  End Loop;
                  Commit;
               end if;
            End Loop;
         end if;
         if Ret>0 then
            Commit;
         else
            Rollback;
         End if;
      else
         Ret:=0;
      end if;
      Return Ret;
   Exception
      When Others Then
         RollBack;
         Return 0;      
   End DetectCiclos;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure ProcTreeXUp(ProcNum Number, startlevel number, pk Number, Cmd Number) is
      Cursor   ctnc(pk number)   is Select Level,gc08pk,gc08thija,gc08tpadre,gc08ciclo from gc.gc0800 
                                    start with gc08pk=pk
                                    connect by Prior gc08tpadre=gc08thija and gc08tipo='R' and gc08ciclo=0 and gc08cicpad=0;
      Cursor   ctc(c Number)        is Select * from gc.gc0800 where gc08ciclo=c and gc08tipo='R';
      Cursor   cpc(c Number)        is Select * from gc.gc0800 where gc08cicpad=c;
      Temp     Varchar2(32000);
      Ciclo    Number;
      Linea    Number;
      Dummy    Number;
   Begin
      For rtnc in ctnc(pk) Loop
         Temp:=GetTName(ProcNum,rtnc.gc08tpadre);
         Linea:=SaveCmd(ProcNum,startlevel+rtnc.level,NULL,rtnc.gc08pk,Cmd,Temp,1);
         Select gc08ciclo into ciclo from gc.gc0800 where gc08thija=rtnc.gc08tpadre and gc08tipo='P';
         If ciclo>0 then
            For rctc in ctc(Ciclo) Loop
               Temp:=GetTName(ProcNum,rctc.gc08tpadre);
               Dummy:=SaveCmd(ProcNum,startlevel+rtnc.level+1,Linea,rctc.gc08pk,Cmd,Temp,1);
            End Loop;
            For rcpc in cpc(Ciclo) Loop
               ProcTreeXUp(ProcNum,startlevel+rtnc.level+1,rcpc.gc08pk,Cmd);
            End Loop;
         End if;
      End Loop;
      commit;
   End ProcTreeXUp;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure ProcTreeXDown(ProcNum Number, startlevel number, pk Number, Cmd Number) is
      Cursor   ctnc(pk number)   is Select Level niv,gc08pk,gc08thija,gc08tpadre,gc08ciclo from gc.gc0800 
                                    start with gc08pk=pk
                                    connect by Prior gc08thija=gc08tpadre and gc08tipo='R' and gc08ciclo=0 and gc08cichij=0;
      Cursor   ctc(c Number)        is Select * from gc.gc0800 where gc08ciclo=c and gc08tipo='R';
      Cursor   cpc(c Number)        is Select * from gc.gc0800 where gc08cichij=c;
      Temp     Varchar2(32000);
      Ciclo    Number;
      Linea    Number;
      Dummy    Number;
   Begin
      For rtnc in ctnc(pk) Loop
         Temp:=GetTName(ProcNum,rtnc.gc08thija);
         Linea:=SaveCmd(ProcNum,startlevel+rtnc.niv,NULL,rtnc.gc08pk,Cmd,Temp,2);
         Select gc08ciclo into ciclo from gc.gc0800 where gc08tpadre=rtnc.gc08thija and gc08tipo='P';
         If ciclo>0 then
            For rctc in ctc(Ciclo) Loop
               Temp:=GetTName(ProcNum,rctc.gc08thija);
               Dummy:=SaveCmd(ProcNum,startlevel+rtnc.niv+1,Linea,rctc.gc08pk,Cmd,Temp,2);
            End Loop;
            For rcpc in cpc(Ciclo) Loop
               ProcTreeXDown(ProcNum,startlevel+rtnc.niv+1,rcpc.gc08pk,Cmd);
            End Loop;
         End if;
      End Loop;
      commit;
   End ProcTreeXDown;

/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure PurgeUKs(ProcNum Number, t varchar2) is
      Cursor   c5(t varchar2) is Select * from gc.gc0800 where gc08Tipo='U' and gc08thija=t;                                 
      sq       varchar2(32000);
      tsq      matvar2;
      Dummy    Number;
   Begin
      If GetLPK(t)<>GetLPK(t,1) then   /* Tiene UKs */
         /* En primer lugar, borrar los registros que estan entre los UK pero con otros PK */
         sq:='( 1=0 ';
         For r5 in c5(t) Loop
            sq:=sq||' or ('||r5.gc08lfkt||') in (Select '||r5.gc08lfkt||' From '||GetTName(ProcNum,t);
         End Loop;
         sq:=sq||') And ('||GetLPK(t,1)||') Not In (Select '||GetLPK(t,1)||' From '||GetTName(ProcNum,t)||')';
         gc01.XDelete(t,sq);
         /* A continuacion, hacer Update en los UK remotos no nulos para los PK locales con UK nulos */
         tsq.Delete;
         tsq(1):='Update '||t||' A Set ('||GetLPK(t,2)||') =  (Select '||GetLPK(t,2)||' From '||GetTName(ProcNum,t)||' B ';
         tsq(2):='  Where '||GetLPKEq(ProcNum,t,'A','B')||' )';
         tsq(3):='  Where ('||GetLPK(t)||') in (Select '||GetLPK(t)||' From '||GetTName(ProcNum,t)||')';
         Dummy:=ExeSQL(ProcNum,tsq);
      End If;
   End PurgeUKs;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure CreateTTemp(ProcNum number) is
      Cursor   c1(p number)   is Select distinct gc08thija  tabla from gc.gc0800,gc.gc0700 where gc08pk=gc07pk08 and gc07codpro=p 
                                 UNION
                                 Select distinct gc08tpadre tabla from gc.gc0800,gc.gc0700 where gc08pk=gc07pk08 and gc07codpro=p;
      Dummy    Number;          
      tsq      matvar2;                       
   Begin                                       
      /* Crear tablas temporales vacias */
      for r1 in c1(ProcNum) loop
         tsq.delete;
         tsq(1):='Create table '||GetTName(ProcNum,r1.Tabla)||' As select '||GetLPK(r1.Tabla,1)||' From '||r1.Tabla||' Where 1=0';
         Dummy:=ExeSQL(ProcNum,tsq);
         tsq.delete;
         tsq(1):='Alter table '||GetTName(ProcNum,r1.Tabla)||' add primary key ('||GetLPK(r1.Tabla)||')';
         Dummy:=ExeSQL(ProcNum,tsq);
      End Loop;
   End CreateTTemp;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure FreeTTemp(ProcNum Number) is
      Cursor   c1(p number)   is Select distinct gc08thija  tabla from gc.gc0800,gc.gc0700 where gc08pk=gc07pk08 and gc07codpro=p 
                                 UNION
                                 Select distinct gc08tpadre tabla from gc.gc0800,gc.gc0700 where gc08pk=gc07pk08 and gc07codpro=p;
      Dummy    Number;       
      tsq      matvar2;                         
   Begin                                 
      /* Borrar Tablas temporales */
      for r1 in c1(ProcNum) loop
         tsq.delete;
         tsq(1):='Drop table '||GetTName(ProcNum,r1.Tabla);
         Dummy:=ExeSQL(ProcNum,tsq);
      End Loop;
   End FreeTTemp;      
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure InitTTemp(ProcNum Number, dbl varchar2, Cond varchar2) is
      Cursor   c2(p number)   is Select * from gc.gc0700,gc.gc0800 where gc07pk08=gc08pk and gc07codpro=p order by gc07Linea asc;
      r2       c2%RowType;                              
      Dummy    Number;
      tsq      matvar2;
      LProg    Number;
      MaxLProg Number;
      LLevel    Number;
      type     tLevels  is table of Number index by binary_integer;
      Levels   tLevels;
      i        Number;
   Begin
      /* Insertar en las tablas temporales ordenadamente */
      Select * into r2 from gc.gc0700,gc.gc0800 where gc07pk08=gc08pk and gc07codpro=ProcNum and gc07linea=1;
      tsq(1):='Insert into '||GetTName(ProcNum,r2.gc08tpadre)||' Select '||GetLPK(r2.gc08tpadre,1)||' From '||r2.gc08tpadre;
      tsq(1):=tsq(1)||dbl||' where ';
      tsq(2):=cond;
      Dummy:=ExeSQL(ProcNum,tsq);
      LLevel:=1;
      Levels(LLevel):=Dummy;
      
      Update gc.gc0700 set gc07NRows=gc07NRows+Dummy where gc07codpro=ProcNum and gc07Linea=r2.gc07Linea;
      Commit;
      LProg:=2;
      Select max(gc07Linea) into MaxLProg from gc.gc0700 where gc07CodPro=ProcNum;
      Loop
         If LProg>MaxLProg then exit; end if;
         Select * into r2 from gc.gc0700,gc.gc0800 where gc07pk08=gc08pk and gc07codpro=ProcNum and gc07linea=LProg;
         Dummy:=0;
         if Levels(r2.gc07Level-1)>0 then /* Si la iteracion anterior no ha habido cambios, no procesar el �rbol */
            tsq.delete;
            If r2.gc07UpDown=1 then 
               BuildTSQUp  (ProcNum , r2.gc08tpadre, r2.gc08thija, dbl, r2.gc08lfko , r2.gc08lfkt, tsq );
            Elsif r2.gc07UpDown=2 then 
               BuildTSQDown(ProcNum , r2.gc08tpadre, r2.gc08thija, dbl, r2.gc08lfko , r2.gc08lfkt, tsq );
            End If;            
            Update gc.gc0700 set gc07Executed=nvl(gc07Executed,0)+1 where gc07codpro=ProcNum and gc07Linea=r2.gc07Linea;
            Commit;
            if tsq.count<>0 then 
               Dummy:=ExeSQL(ProcNum,tsq); 
            End if;            
         End If;            
         Update gc.gc0700 set gc07Executed=nvl(gc07Executed,0),gc07NRows=gc07NRows+Dummy 
            where gc07codpro=ProcNum and gc07Linea=r2.gc07Linea;
         Commit;
         If r2.gc07Level>LLevel then
            Levels(r2.gc07Level):=Dummy;
         elsif r2.gc07Level=LLevel then
            Levels(LLevel):=Levels(LLevel)+Dummy;
         else
            For i in r2.gc07Level+1..LLevel Loop
               Levels(i):=0;
            End Loop;
            Levels(r2.gc07Level):=Dummy;
         End if;
         LLevel:=r2.gc07Level;
         if r2.gc07lic is null then 
            LProg:=LProg+1;
         else
            if Dummy=0 then LProg:=LProg+1; else LProg:=r2.gc07lic+1; End if;
         end If;         
      End Loop;         
   End InitTTemp;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure Ejecuta(ProcNum Number, Cond Varchar2, dbl Varchar2 default NULL) is
      Cursor   c1(p number)   is Select distinct gc08thija  tabla from gc.gc0800,gc.gc0700 where gc08pk=gc07pk08 and gc07codpro=p 
                                 UNION
                                 Select distinct gc08tpadre tabla from gc.gc0800,gc.gc0700 where gc08pk=gc07pk08 and gc07codpro=p;
      Cursor   c3(p number)   is Select gc08tpadre,max(gc07level) Nivel from gc.gc0800,gc.gc0700 where gc07pk08=gc08pk and
                                 gc07codpro=p and gc07insdel=1 group by gc08tpadre order by 2 desc;
      Cursor   c4(p number)   is Select gc08thija,max(gc07level) Nivel from gc.gc0800,gc.gc0700 where gc07pk08=gc08pk and
                                 gc07codpro=p and gc07insdel=2 group by gc08thija order by 2 desc;
      lpk      Varchar2(1000);
      Dummy    Number;
      Nrows    Number;
      tsq      matvar2;
      tsq2     matvar2;
      LastLev  Number;
      InsRows  Number;
      ndx      Number;
   Begin
      /* Inicializar tablas temporales con los registros que se va a ver afectados */
      InitTTemp(ProcNum,dbl,Cond);
      /* Omitir/Insertar/Borrar Registros registros en las tablas reales */
      /* Hay que tener en cuenta los Unique y los registros que ya existen en las tablas destino */
      Select gc07InsDel into InsRows From gc.gc0700 where gc07CodPro=ProcNum and gc07Linea=1;
      LastLev:=0;
      tsq.Delete;
      tsq(1):='Begin ';
      if InsRows=1 then
         For r3 in c3(ProcNum) Loop
            tsq2.delete;
            ListValues(ProcNum,r3.gc08tpadre,tsq2);
            if tsq2.count>0 then
               /* Purgar Unique Keys Si estamos haciendo XCopy o XInsert */         
               PurgeUKs(ProcNum,r3.gc08tpadre);
               tsq(tsq.count+1):='Insert into '||r3.gc08tpadre||' Select * from '||r3.gc08tpadre||dbl||' Where ';
               For ndx in 1..tsq2.count Loop
                  tsq(tsq.count+1):=tsq2(ndx);
               End Loop;
               tsq(tsq.count+1):=' And ('||GetLPK(r3.gc08tpadre)||') Not in (Select '||GetLPK(r3.gc08tpadre)||' From '||r3.gc08tpadre||');';
            End if;               
            if LastLev>r3.Nivel and tsq.count>1 then
               tsq(tsq.count+1):=' Commit; End;';
               Dummy:=ExeSQL(ProcNum,tsq);
               Commit;
               tsq.delete;
               tsq(1):='Begin ';
            End If;
            LastLev:=r3.Nivel;
         End Loop;
      Else
         For r4 in c4(ProcNum) Loop
            tsq2.delete;
            ListValues(ProcNum,r4.gc08thija,tsq2);
            if tsq2.count>0 then
               tsq(tsq.count+1):='Delete from '||r4.gc08thija||' Where ';
               For ndx in 1..tsq2.count Loop
                  tsq(tsq.count+1):=tsq2(ndx);
               End Loop;
               tsq(tsq.count+1):=';';
            end if;               
            if LastLev>r4.Nivel and tsq.Count>1 then
               tsq(tsq.count+1):=' End;';
               Dummy:=ExeSQL(ProcNum,tsq);
               Commit;
               tsq.Delete;
               tsq(1):='Begin ';
            End If;
            LastLev:=r4.Nivel;
         End Loop;
      End If;
      if tsq.count>1 then 
         tsq(tsq.count+1):=' End;';
         Dummy:=ExeSQL(ProcNum,tsq);
         Commit;
      End If;         
   End Ejecuta;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
/*          INTERFAZ PUBLICA                                                                                                        */
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   /* Saca Copia De Las Tablas De Sistema Hay Que Llamarlo Si Se Producen Cambios En La Integridad Referencial*/
   /* Adem�s detecta ciclos en la integridad referencial */
   Procedure DupSys Is
      Cursor Ac Is
         Select * From Gc.Prcac Where (Constraint_Type) In ('P','U','R') And Status='ENABLED' And Owner<>'SYS' And Owner<>'SYSTEM';
      Cursor Acc1(Cn Varchar2) Is
         Select * From Gc.Prcacc Where Constraint_Name=Cn;
      Cursor Acc2(Cn Varchar2) Is
         Select * From Gc.Prcacc Where Constraint_Name=Cn Order By Position;
      Tname    Varchar2(32000);
      Lfkt     Varchar2(32000);
      Rtname   Varchar2(32000);
      Lfko     Varchar2(32000);
      Tipo     Varchar2(1);
      Existe   Number;
      pk       Number:=1;
      Cursor   c08            is    Select rowid from gc.gc0800 where gc08Tipo='R';
      r08      c08%rowtype;
      UpdCic   Number;
      Cursor   c5             is    Select gc08thija,count(gc08ciclo) from gc.gc0800 where gc08ciclo>0 group by gc08thija
                                       having count(gc08ciclo)>1;
      r5 c5%RowType;
      i        Number;
      YaEsta   Number;
      MaxCiclo Number;
      MaxPk    Number;
      Sz       Number;
      tsq      matvar2;
   Begin      
      Delete From Gc.Gc0700;
      Commit;
      Delete From Gc.Gc0800;
      Commit;
      tsq(1):='Drop table gc.prcac';
      i:=ExeSQL(0,tsq);
      tsq(1):='Drop table gc.prcacc';
      i:=ExeSQL(0,tsq);
      tsq(1):='Create Table Gc.Prcac As Select Owner,Constraint_Name,Constraint_Type,Table_Name,R_Owner,R_Constraint_Name,Status From All_Constraints';
      i:=ExeSQL(0,tsq);
      tsq(1):='Create Index Gc.Prcac01 On Gc.Prcac(Owner)';                
      i:=ExeSQL(0,tsq);
      tsq(1):='Create Index Gc.Prcac02 On Gc.Prcac(Constraint_Type,Owner)';
      i:=ExeSQL(0,tsq);
      tsq(1):='Create Table Gc.Prcacc As Select * From All_Cons_Columns';
      i:=ExeSQL(0,tsq);
      tsq(1):='Create Index Gc.Prcacc01 On Gc.Prcacc(Constraint_Name)';
      i:=ExeSQL(0,tsq);
      For Rac In Ac Loop
         Lfkt:='*';
         Lfko:='*';
         Tname:='';
         Rtname:='';
         Sz:=0;
         For Racc1 In Acc1(Rac.Constraint_Name) Loop
            Sz:=Sz+1;
            Tname:=Racc1.Owner||'.'||Racc1.Table_Name;
            If Lfkt='*' Then Lfkt:=Racc1.Column_Name; Else Lfkt:=Lfkt||','||Racc1.Column_Name; End If;
         End Loop;
         If Lfkt='*' Then Lfkt:=''; End If;
         For Racc2 In Acc2(Rac.R_Constraint_Name) Loop
            Rtname:=Racc2.Owner||'.'||Racc2.Table_Name;
            If Lfko='*' Then Lfko:=Racc2.Column_Name; Else Lfko:=Lfko||','||Racc2.Column_Name; End If;
         End Loop;
         If Lfko='*' Then Lfko:=''; End If;
         Tipo:=Rac.Constraint_Type;
         Select Count(*) Into Existe From Gc.Gc0800
            Where Gc08Tipo=Tipo And Gc08Thija=Tname And Gc08Lfkt=Lfkt And Gc08Tpadre=Rtname And Gc08Lfko=Lfko;
         If Existe=0 Then
            Insert Into Gc.Gc0800(Gc08pk,Gc08Tipo,Gc08Tpadre,Gc08Lfko,Gc08Thija,Gc08Lfkt,Gc08Ciclo,gc08cicpad,gc08cichij,gc08Size) 
               Values (pk,Tipo,Rtname,Lfko,Tname,Lfkt,0,0,0,Sz);
            pk:=pk+1;
            Commit;
         End If;
      End Loop;
      Update gc.gc0800 set gc08tpadre=gc08thija,gc08lfko=gc08lfkt where gc08Tipo='P';
      Commit;
      Loop
         UpdCic:=0;
         for r08 in c08 Loop
            UpdCic:=UpdCic+DetectCiclos(r08.rowid);
         End Loop;
         if UpdCic=0 then
            exit;
         End if;             
      End Loop;
      /* Unir ciclos (una tabla puede estar en mas de uno) */
      For r5 in c5 Loop
         Select max(gc08ciclo) into MaxCiclo from gc.gc0800 where gc08thija=r5.gc08thija;
         Update gc.gc0800 set gc08ciclo=MaxCiclo where gc08ciclo in 
            (select gc08ciclo from gc.gc0800 where gc08ciclo>0 and gc08thija=r5.gc08thija);
         Commit;            
      End Loop;
      Select max(gc08ciclo) Into MaxCiclo from gc.gc0800;
      /* Calcular Padres e Hijos del Ciclo como conjunto */
      For i in 1..MaxCiclo Loop
         /* Tablas hijas del ciclo */
         Update gc.gc0800 set gc08cicHij=i where 
            gc08ciclo=0 and
            gc08Tipo='R' and
            gc08tpadre in (select gc08thija from gc.gc0800 where gc08ciclo=i);
         /* Tablas Padres del ciclo */
         Update gc.gc0800 set gc08cicPad=i where 
            gc08ciclo=0 and
            gc08Tipo='R' and
            gc08thija in (select gc08thija from gc.gc0800 where gc08ciclo=i);
         Commit;
      End Loop;
      Update gc.gc0800 t1 set t1.gc08ciclo=(select distinct gc08ciclo from gc.gc0800 t2 
            where t2.gc08thija=t1.gc08thija and t2.gc08ciclo>0)   Where t1.gc08tipo='P';
      Update gc.gc0800 set gc08ciclo=0 where gc08ciclo is null;
      /* Borrar tablas que no tienen PK y no tienen tablas hijas */
      Delete From Gc.Gc0800 Where Gc08Tipo='R' And 
         Gc08Thija Not In (Select Distinct Gc08Thija From Gc.Gc0800 Where Gc08Tipo='P');
      /*Insertar en la gc0900 */
      delete from gc.gc0900;
      Commit;
      Insert into gc.gc0900(gc09tipohija,gc09hija,gc09tipopadre,gc09padre)
         Select distinct 1,gc08thija,1,gc08tpadre 
         from gc.gc0800
         where gc08tipo='R' and gc08ciclo=0 and gc08cicpad=0 and gc08cichij=0;
      Insert into gc.gc0900(gc09tipohija,gc09hija,gc09tipopadre,gc09padre)
         Select distinct 2,gc08cicpad,1,gc08tpadre 
         from gc.gc0800
         where gc08tipo='R' and gc08ciclo=0 and gc08cicpad>0;
      Insert into gc.gc0900(gc09tipohija,gc09hija,gc09tipopadre,gc09padre)
         Select distinct 1,gc08thija,2,gc08cichij
         from gc.gc0800
         where gc08tipo='R' and gc08ciclo=0 and gc08cichij>0;
      Commit;  
      Delete from gc.gc1000;
      Commit;
      Insert into gc.gc1000(gc10tab,gc10pos,gc10Tipo) 
         Select t1.owner||'.'||t1.table_name,t3.position,decode(t4.data_type,'NUMBER','N','DATE','F','VARCHAR2','V','CHAR','C','?')
            from all_catalog t1, all_constraints t2, all_cons_columns t3, all_tab_columns t4
            where
            t1.owner not in ('SYS','SYSTEM') and
            t1.table_type='TABLE' and
            t1.owner=t2.owner and t1.table_name=t2.table_name and
            t1.owner=t3.owner and t1.table_name=t3.table_name and
            t1.owner=t4.owner and t1.table_name=t4.table_name and
            t3.column_name=t4.column_name and
            t2.constraint_type='P' and
            t2.constraint_name=t3.constraint_name;
      Commit;       
   End DupSys;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure XInsert (Dblink Varchar2, Tabla Varchar2, Cond Varchar2, log Number default 0) Is
      ProcNum     Number;
      ProcNumRem  Number;
      temp        varchar2(32000);
      pk          Number;
   Begin
      /*Obtener el siguiente numero de proceso */
      Select sys.gc07CodPro_SEQ.nextval into ProcNum from dual;

      /*Registrar en la tabla LOG */
      Insert Into gc.gc1100(gc11CodPro,gc11Linea,gc11Texto)
         values(ProcNum,0,'XInsert('||chr(39)||dblink||chr(39)||','||chr(39)||Tabla||chr(39)||','||chr(39)||Cond||chr(39)||')');
      Commit;         
      
      Select gc08pk into pk from gc.gc0800 where gc08Tipo='P' and gc08thija=Tabla;
      ProcTreeXUp(ProcNum,0,pk,1);
      CreateTTemp(ProcNum);
      Ejecuta(ProcNum,cond,'@'||DBLink);
      if log=0 then 
         PurgeLog( ProcNum );
      End if;         
   End XInsert;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure XDelete (Tabla Varchar2, Cond Varchar2, log Number default 0) Is
      ProcNum     Number;
      temp        varchar2(32000);
      pk          Number;
   Begin
      /*Obtener el siguiente numero de proceso */
      Select sys.gc07CodPro_SEQ.nextval into ProcNum from dual;
      
      /*Registrar en la tabla LOG */
      Insert Into gc.gc1100(gc11CodPro,gc11Linea,gc11Texto)
         values(ProcNum,0,'XDelete('||chr(39)||Tabla||chr(39)||','||chr(39)||Cond||chr(39)||')');
      Commit;         
      
      Select gc08pk into pk from gc.gc0800 where gc08Tipo='P' and gc08thija=Tabla;
      ProcTreeXDown(ProcNum,0,pk,2);
      CreateTTemp(ProcNum);
      Ejecuta(ProcNum,cond,'');
      if log=0 then 
         PurgeLog( ProcNum );
      End if;         
      
   End XDelete;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure XCopy (Dblink Varchar2, Tabla Varchar2, Cond Varchar2, log Number default 0) Is
      ProcNum  Number;
      Cursor   cHojas(tabla varchar2) is 
         Select distinct gc08pk from gc.gc0900, gc.gc0800
            Where    
               (gc09hija) not in (select distinct gc09padre from gc.gc0900) and
               (gc09hija) in (Select gc09hija from gc.gc0900 start with gc09padre=tabla connect by prior gc09hija=gc09padre) and
               gc08tipo='P' and
               gc08thija=gc09hija;
      Ciclo    Number;                                                                                     
      aux      varchar2(50);
      pk       Number;
   Begin
      /*Obtener el siguiente numero de proceso */
      Select sys.gc07CodPro_SEQ.nextval into ProcNum from dual;

      /*Registrar en la tabla LOG */
      Insert Into gc.gc1100(gc11CodPro,gc11Linea,gc11Texto)
         values(ProcNum,0,'XCopy('||chr(39)||dblink||chr(39)||','||chr(39)||Tabla||chr(39)||','||chr(39)||Cond||chr(39)||')');
      Commit;         

      /* Propagar Hacia abajo */
      Select gc08pk into pk from gc.gc0800 where gc08Tipo='P' and gc08thija=Tabla;
      ProcTreeXDown(ProcNum,0,pk,0);
      Select gc08ciclo into Ciclo from gc.gc0800 where gc08Tipo='P' and gc08thija=Tabla;
      if ciclo>0 then aux:=ciclo; else aux:=tabla; end if;
      /* Realizar XInsert en cada una de las hojas */
      For hoja in cHojas(aux) Loop
         ProcTreeXUp(ProcNum,1000,hoja.gc08pk,1);
      End Loop;
      CreateTTemp(ProcNum);
      Ejecuta(ProcNum,cond,'@'||dblink);
      if log=0 then 
         PurgeLog( ProcNum );
      End if;         
   End XCopy;
/************************************************************************************************************************************/
/************************************************************************************************************************************/
   Procedure PurgeLog( ProcNum Number ) is
   Begin
      FreeTTemp(ProcNum);
      Delete from gc.gc1100 where gc11CodPro=ProcNum;
      Commit;
      Delete from gc.gc0700 where gc07codpro=ProcNum;
      Commit;
   End PurgeLog;
End Gc01;
/
