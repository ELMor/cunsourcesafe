
Drop Table Gc.Gc0700;
Create Table Gc.Gc0700(
   Gc07CodPro     Number(10),
   Gc07Linea      Number(10),
   Gc07Level      Number(10),
   gc07lic        Number(10),
   Gc07pk08       Number,
   Gc07InsDel     Number(1),
   Gc07TTemp      Varchar2(30),
   gc07NRows      Number(10),
   gc07Executed   Number(1),
   gc07UpDown     Number(1)
);
Alter Table gc.gc0700 add primary key(gc07codpro,gc07linea);
Create index gc.gc0701 on gc.gc0700(gc07pk08);

COMMENT ON TABLE  GC.GC0700              IS 'Procedimiento COPY,INSERT y DELETE|SQLs a lanzar en orden, para conseguir copy,insert o delete';
COMMENT ON COLUMN GC.GC0700.GC07CODPRO   IS 'Codigo del proceso XCOPY/XDEL/XINS';
COMMENT ON COLUMN GC.GC0700.GC07LINEA    IS 'Numero de linea dentro del proceso';
COMMENT ON COLUMN GC.GC0700.GC07LEVEL    IS 'Nivel del arbol de insercion/Borrado';
COMMENT ON COLUMN GC.GC0700.GC07LIC      IS 'Linea de Inicio de Ciclo de Integridad referencial';
COMMENT ON COLUMN GC.GC0700.GC07PK08     IS 'PK del registro de gc.gc0800';
COMMENT ON COLUMN GC.GC0700.GC07INSDEL   IS 'Sin accion=0, Insertar=1, Borrar=2';
COMMENT ON COLUMN GC.GC0700.GC07TTEMP    IS 'Tabla Temporal con los valores de PK que se han de Insertar en el padre/Borrar en la hija';
COMMENT ON COLUMN GC.GC0700.GC07NROWS    IS 'Numero de filas a insertar/Borrar';
COMMENT ON COLUMN GC.GC0700.GC07EXECUTED IS 'Ha sido o esta siendo ejecutada la creacion/insercion de la tabla';
COMMENT ON COLUMN GC.GC0700.GC07UPDOWN   IS 'Propagacion Arriba=1, Abajo=2 de los registros';

Drop Table Gc.Gc0800;
Create Table Gc.Gc0800(
   Gc08pk         Number,
   Gc08Tipo       Varchar2(1),
   Gc08Thija      Varchar2(50),
   Gc08Lfkt       Varchar2(250),
   Gc08Tpadre     Varchar2(50),
   Gc08Lfko       Varchar2(250),
   Gc08Size       Number(3),
   Gc08Ciclo      Number(10),
   Gc08CicHij     Number(10),
   Gc08CicPad     Number(10)
);
Alter table gc.gc0800 add primary key(gc08pk);
Create Index gc.gc0802 on gc.gc0800(gc08tpadre);
Create Index gc.gc0803 on gc.gc0800(gc08thija);

Comment On Table  Gc.Gc0800              Is 'Descripcion De Integridad Referencial';
Comment On Column Gc.Gc0800.Gc08Pk       Is 'Primary Key'; 
Comment On Column Gc.Gc0800.Gc08Tipo     Is 'Tipo De Restriccion: P,R,U'; 
Comment On Column Gc.Gc0800.Gc08Tpadre   Is 'Tabla Padre'; 
Comment On Column Gc.Gc0800.Gc08Lfkt     Is 'Lista De Campos Maestros'; 
Comment On Column Gc.Gc0800.Gc08Thija    Is 'Tabla Hija'; 
Comment On Column Gc.Gc0800.Gc08Lfko     Is 'Lista De Campos Hijos De Los Maestros'; 
Comment On Column Gc.Gc0800.Gc08Ciclo    Is 'Numero De Ciclo Al Que Pertenece'; 
Comment On Column Gc.Gc0800.Gc08Cichij   Is 'Gc08Thija De Este Registro Es Hija De Este Ciclo'; 
Comment On Column Gc.Gc0800.Gc08Cicpad   Is 'Gc08Tpadre De Este Registro Es Padre De Este Ciclo'; 

Create Index Gc.Gc0801 On Gc.Gc0800(Gc08Tipo,Gc08Thija,Gc08Tpadre);

Drop Table gc.gc0900;
Create Table gc.gc0900(
   Gc09tipohija   number(1),
   Gc09hija       varchar2(50),
   gc09tipopadre  number(1),
   gc09padre      varchar2(50)
);
Alter table gc.gc0900 add primary key(gc09hija,gc09padre);
Comment on table  gc.gc0900               is 'Arbol de dependencia entre tablas/ciclos';
Comment on column gc.gc0900.gc09tipohija  is 'Tipo de concepto de la tabla Hija: 1 Tabla, 2 Ciclo';
Comment on column gc.gc0900.gc09hija      is 'Nombre de la hija';
Comment on column gc.gc0900.gc09tipopadre is 'Tipo de concepto de la tabla Padre: 1 Tabla, 2 Ciclo';
Comment on column gc.gc0900.gc09padre     is 'Nombre del padre';

Drop Table gc.gc1000;
Create table gc.gc1000(
   gc10Tab        Varchar2(50),
   gc10pos        Number,
   gc10tipo       varchar2(1)
);
Alter Table gc.gc1000 add primary key(gc10Tab,gc10pos);
Comment on Table  gc.gc1000               is 'Tipos de datos de las Primary Keys';
Comment on column gc.gc1000.gc10Tab       is 'Tabla';
Comment on column gc.gc1000.gc10Pos       is 'Posicion dentro del Primary Key';
Comment on column gc.gc1000.gc10tipo      is 'Tipod de dato N:Numero, F: Fecha, V: varchar2, C: Char';

Drop Table gc.gc1100;
Create Table gc.gc1100(
   gc11CodPro     Number,
   gc11Linea      Number,
   gc11Texto      Varchar2(512)
);
Alter table gc.gc1100 add primary key (gc11CodPro,gc11Linea);   

Drop sequence sys.gc07codpro_seq;
Create Sequence sys.gc07CodPro_SEQ Start With 1 Increment by 1;

Create Table Gc.Prcac As Select Owner,Constraint_Name,Constraint_Type,Table_Name,R_Owner,R_Constraint_Name,Status From All_Constraints;
Create Index Gc.Prcac01 On Gc.Prcac(Owner);
Create Index Gc.Prcac02 On Gc.Prcac(Constraint_Type,Owner);
Create Table Gc.Prcacc As Select * From All_Cons_Columns;
Create Index Gc.Prcacc01 On Gc.Prcacc(Constraint_Name);

