#include "cie9.h"

#include "send.h"

#include "strbin.h"



#define  Global extern

#define  GlobalIni

#include "globals.h"



static char Buffer[4096];

static char *posCam[1024];



int cmpRootString(const void *p1, const void *p2){

	char *s1=(char*)p1,*s2=(char*)p2;

	long l,l1,l2;

	if(!s1 && !s2)

		return  0;

	if(!s1)

		return  1;

	if(!s2)

		return -1;

	l1=strlen(s1);

	l2=strlen(s2);

	l = (long)(((double)PCTLONGSTRING)*( l1>l2 ? l2 : l1 ));

	if( l<TAMROOTSTRING )

		return stricmp(s1,s2);

	else

		return strnicmp(s1,s2,l);

}



int cmppRef( const void *p1, const void *p2 ){

	long l1=*((long*)p1), 

		 l2=*((long*)p2);

	Ref  *r1=&Cie[l1]   , 

		 *r2=&Cie[l2];

	if(l1!=-1 && l2!=-1)

		return stricmp( r1->Referencia, 

		                r2->Referencia );

	else if(l2==-1)

		return -1;

	else if(l1==-1)

		return 1;

	else

		return 0;

}



int cmpMatch(const void *p1, const void *p2){

	Match *m1=(Match*)p1,*m2=(Match*)p2;

	return m2->Veces - m1->Veces;

}



int cmp2(const void *p1, const void *p2){

	if(!p1 && !p2)

		return 0;

	if(!p1)

		return 1;

	if(!p2)

		return -1;

	Pal *z1=*((Pal**)p1),*z2=*((Pal**)p2);

	return cmpRootString(z1->Palabra,z2->Palabra);

}



int cmp3(const void *p1, const void *p2){

	if(!p1 && !p2)

		return 0;

	if(!p1)

		return 1;

	if(!p2)

		return -1;

	Ref *z1=*((Ref**)p1),*z2=*((Ref**)p2);

	int l1=strlen(z1->Referencia);

	int l2=strlen(z2->Referencia);

	if(l2<l1) l1=l2;

	return strnicmp(z1->Referencia,z2->Referencia,l1);

}



long RefTree(char *ref, long *genea, long *tamgen){

	char buf[25],*aux;

	unsigned long t;

	

	if( NULL!=(aux=strchr(ref,'.')) )

		t=aux-ref;

	else

		t=strlen(ref);

	while(t<=strlen(ref)){

		strncpy(buf,ref,t);

		buf[t]=0;



		genea[(*tamgen)++]=BusRef(buf);

		t++;

		if(*(ref+t-1)=='.')

			t++;



	}

	return *tamgen;

}



int SearchRange(char *s, Pal ***p1, Pal ***p2 ){

	Pal **ini=(Pal**)NULL,**fin=(Pal**)NULL,dpal,*pdpal;

	pdpal=&dpal;

	dpal.Palabra=s;

	if( fin=(Pal**)bsearch(&pdpal,IndDic_Pal,TamDic,sizeof(Pal*),cmp2) ){

		for(ini=fin; 

			ini>&(IndDic_Pal[0]) && 

				!cmpRootString((*ini)->Palabra,(*fin)->Palabra); 

			ini-- )

			;

		for(;

			fin<=&(IndDic_Pal[TamDic-1]) && 

				!cmpRootString((*ini)->Palabra,(*fin)->Palabra); 

			fin++)

			;

	}

	*p1=ini+1;

	*p2=fin;

	return (int)fin;

}



int SearchSubClas(char *s, Ref ***p1, Ref ***p2){

	Ref **ini,**fin,dref,*pdref;

	pdref=&dref;

	dref.Referencia=s;

	ini=fin=(Ref**)NULL;

	if( fin=(Ref**)bsearch(&pdref,IndCie_Ref,TamCie,sizeof(Ref*),cmp3) ){

		for(ini=fin;

			ini>&(IndCie_Ref[0]) && 

				!strnicmp((*ini)->Referencia,(*fin)->Referencia,strlen(s));

			ini--)

			;

		for(;

			fin<=&(IndCie_Ref[TamCie-1]) && 

				!strnicmp((*ini)->Referencia,(*fin)->Referencia,strlen(s));

			fin++)

			;

	}

	*p1=ini+1;

	*p2=fin;

	return (int)fin;

}



inline void SumaMRef( Match *mr, long pospal, int suma){

	for(long i=0;i<Dic[pospal].NReferencias;i++)

		mr[Dic[pospal].Referencias[i]].Veces+=suma;

}



int Search( char *frase, Match *MRef){

	long pos,numpal,i,j;

	Pal **ppP1,**ppP2;

	strcpy(Buffer,frase); 

	//Buscar la primera palabra

	numpal=0;

	posCam[numpal]=Tokenize(Buffer,TokTodo);

	while( posCam[numpal] )

		posCam[++numpal]=Tokenize((char*)NULL,TokTodo);

	if( !numpal )								//No hay ninguna palabra en la frase

		return 0;

	if( posCam[numpal] )		

		numpal++;								//Para que haya 'numpal' palabras

	//Inicializacion

	memset(MRef,0,sizeof(Match)*TamCie);

	for(i=0;i<TamCie;i++) MRef[i].Valor=i;

	for(i=0;i<numpal;i++){

		if( BusPalCom(posCam[i] ) )

			continue;							// Desechamos palabras comunes.

		if( (pos=BusPal(posCam[i]))!= -1 ){		// Palabra correcta peso '10'

			SumaMRef(MRef,pos,10);

			for(j=0;j<Dic[pos].NPalabras;j++)

				SumaMRef(MRef,Dic[pos].Palabras[j],1);

		}

		if(SearchRange(posCam[i],&ppP1,&ppP2))	// Palabra 'derivada' pesa '1'

			while(ppP1<=ppP2){

				SumaMRef(MRef,*ppP1-&(Dic[0]),1);

				ppP1++;

			}

	}

	qsort(MRef,TamCie,sizeof(Match),cmpMatch);

	return i;

}



// Retorna 0:fin sesion, 1:dumpref(i), 2 dumppal(i), dumpall 3, 

// B�squeda normal 4, Busqueda Normal que no ha producido resultados 5

// Logout Server 6

long TextSearch(char *data, Match *MRef, char*Frases[128],char **sout){

	long ret,frases,k,NCmdBody,NCmd,flags; 

	char *Cmd[NMAXCOMMANDS],*CmdBody[NMAXCOMMANDS];



	//Inicializaci�n de la tabla de comandos

	memset(Cmd,0,sizeof(char*)*NMAXCOMMANDS);

	memset(CmdBody,0,sizeof(char*)*NMAXCOMMANDS);

	//Desenpaquetar

	NCmd=UnPack(data,Cmd);

	//Por ahora, ignorar Usuario y Password

	NCmdBody=UnPackAndGetBody(Cmd[2],CmdBody);

	if      ( !stricmp(CmdBody[0],"GetRefTxt") ){	//Asistente para codificar

		frases=Parrafos(Translate(CmdBody[1]),Frases);

		flags=atol(CmdBody[2]);

		for(k=0;k<frases&&Frases[k];k++)

			if( Search(Frases[k],MRef)>0 )

				SendFrase(sout,"Raiz",k+1,Frases[k],MRef,flags);

			else

				SendFrase(sout,"Raiz",0,"No se encontraron referencias",(Match*)NULL,0);

			memset(MRef,0,sizeof(Match)*TamCie);

			ret=1;

	}else if( !stricmp(CmdBody[0],"GetDiaFroCod")){	// Obtener texto diagn�stico

		flags = (atol(CmdBody[2])==1) ? 2 : 0;

		flags|= (atol(CmdBody[3])==1) ? 4 : 0;

		SendRef(sout,"Raiz",BusRef(Translate(CmdBody[1])),flags);

		ret=2;

	}else if( !stricmp(CmdBody[0],"GetPalPal"   ) ){

		flags=atol(CmdBody[2]);

		SendPal(sout,"Raiz",BusPal(Translate(CmdBody[1])),flags);

		SendRootPal(sout,"Raiz",CmdBody[1]);

		ret=3;

	}else if( !stricmp(CmdBody[0],"InsDiaFroRef") ){

	}else if( !stricmp(CmdBody[0],"InsSinForPal") ){

	}else{

	}

	while( NCmdBody-- )

		free(CmdBody[NCmdBody]);

	while( NCmd-- )

		free(Cmd[NCmd]);

	return ret;

}

