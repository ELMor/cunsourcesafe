



#include <stdio.h>

#include <stdarg.h>

#include <string.h>

#include <stdlib.h>

#include <ctype.h>



char *Pack( char **out, ... ){   

	va_list marker;

	char *s,buf[9];

	long i,n=0;



	buf[4]=0;

	*out=(char*)realloc(*out,64*1024);

	strcpy(*out,"00000000");

	va_start( marker, out );     

	do{ 

		if(NULL!=(s=va_arg(marker, char* ))){

			i=strlen(s);

			if( i>7 && strncpy(buf,s+4,4) && isdigit(*buf) && atol(buf)==i-8 )

				strcat(*out,s);

			else

				sprintf(*out+strlen(*out),"0001%04ld%s",i,s);

			n++;

		}

	}while( s );

	va_end( marker );

	sprintf(buf,"%04ld%04ld",n,strlen(*out)-8);

	strncpy(*out,buf,8);

	return *out;

}



long UnPack( char *sin, char **sout){

	long i,pos,lon,n;

	char Buf[5];

	

	strncpy(Buf,sin,4); Buf[4]=0; n=atol(Buf);

	pos=8;

	for(i=0;i<n;i++){

		strncpy(Buf,sin+pos+4,4); Buf[4]=0; lon=atol(Buf);

		sout[i]=(char*)realloc(sout[i],1+8+lon);

		strncpy(sout[i],sin+pos,lon+8); sout[i][lon+8]=0;

		pos+=8+lon;

	}

	return n;

}



char *GetBody(char *in, char **out){

	long n;

	char Buf[5];

	strncpy(Buf,in+4,4); Buf[4]=0; n=atol(Buf);

	*out=(char*)realloc(*out,n+1);

	strncpy(*out,in+8,n);

	(*out)[n]=0;

	return *out;

}



long UnPackAndGetBody(char *sin, char **sout){

	long numcom=UnPack(sin,sout),i;

	for(i=0;i<numcom;i++)

		GetBody(sout[i],&(sout[i]));

	return numcom;

}