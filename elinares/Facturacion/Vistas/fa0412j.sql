
CREATE OR REPLACE VIEW FA0412J ( FA48CODDISKACUNSA, 
FA3R1 ) AS SELECT  
FA4800.FA48CODDISKACUNSA,  
LPAD(SUBSTR(FA0400.FA04NUMFACT,4),7,'0')                    || -- Numero de Factura: Number(7)  
LPAD(FA16NUMLINEA,3,'0')                                    || -- Numero de Linea: Number(3)  
RPAD(FA16DESCRIP,50,' ')                                    || -- Texto de la linea: Varchar(150)  
LPAD(FA16IMPORTE,10,'0')   FA3R1                               -- Importe de la linea: Number(10)  
FROM  
FA0400,FA1600,FA4800,FA4900  
WHERE  
FA0400.FA04CODFACT=FA1600.FA04CODFACT  
AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA  
AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT  
AND FA04NUMFACREAL IS NULL
