/* USO:
   INCLUIR EN PRIMER LUGAR EN LA FA4800 EL NUMERO DEL DISKETTE
   INSERT INTO FA4800(FA48CODDISKACUNSA,FA48FECHA,FA48IMPTOTAL)
      VALUES(1,TO_DATE('31042000','DDMMYYYY'),0);
   INCLUIR EN LA FA4900 LAS FACTURAS QUE DEBEN APARECER EN EL DISKETTE
   INSERT INTO FA4900(FA48CODDISKACUNSA,FA04NUMFACT)
      SELECT DISTINCT 1,FA04NUMFACT
      FROM FA0400
      WHERE CI32CODTIPECON='J'
      AND FA04FECFACTURA BETWEEN TO_DATE('01012000','DDMMYYYY') AND TO_DATE('30042000','DDMMYYYY')+0.9999
      AND FA04NUMFACREAL IS NULL
      AND FA04CANTFACT>0;

   FICHERO 1:
      SELECT FA1R00  FROM FA0406J WHERE FA48CODDISKACUNSA=1
      UNION ALL
      SELECT FA1R10  FROM FA0407J WHERE FA48CODDISKACUNSA=1
      UNION ALL
      SELECT FA1R90  FROM FA0408J WHERE FA48CODDISKACUNSA=1
      UNION ALL
      SELECT FA1R99  FROM FA0409J WHERE FA48CODDISKACUNSA=1
      ORDER BY 1

   FICHERO 2:
      SELECT FA2RF   FROM FA0410J WHERE FA48CODDISKACUNSA=1
      UNION ALL
      SELECT MOVACUN FROM FA0411J WHERE FA48CODDISKACUNSA=1
      ORDER BY 1
      
   FICHERO 3:
      SELECT FA3R1   FROM FA0412J WHERE FA48CODDISKACUNSA=1
*/

-- FICHERO NUMERO 1 ACUNSA
--------------------------------------------------------------------------------
-- registros de tipo '00'
CREATE OR REPLACE VIEW FA0406J AS 
-- FICHERO ACUNSA NUMERO 1, REGISTROS DE TIPO '00'
SELECT DISTINCT 
      FA4800.FA48CODDISKACUNSA,
      LPAD(SUBSTR(FA0400.FA04NUMFACT,4),7,'0')                    || -- Numero de Factura: Number(7)
      '00'                                                        || -- Registros de tipo '00'
      to_char(FA04FECFACTURA,'YYYYMMDD')                          || -- Fecha de Factura  'YYYYMMDD'
      LPAD(NVL(CI03DNI,' '),12,' ')                               || -- DNI/NIF Varchar2(12)
      LPAD(NVL(TO_CHAR(CI03NUMPOLIZA),'          '),10,'0')       || -- Numero de poliza Number(10)
      TO_CHAR(AD08FECINICIO,'YYYYMMDD')                           || -- Fecha de Ingreso 
      RPAD(NVL(TO_CHAR(AD08FECFIN,'YYYYMMDD'),' '),8,' ')         || -- Fecha de Fin
      DECODE(AD12CODTIPOASIST,1,'H','A')                          || -- Tipo de Asistencia 'H' o 'A'
      SUBSTR(AD08NUMCASO,1,6)||LPAD(SUBSTR(AD08NUMCASO,7,4),5,'0')|| -- Historia, Caso : Number(6), Number(4)
	   RPAD(CI22PRIAPEL||','||CI22SEGAPEL||','||CI22NOMBRE,48,' ') || -- Apellido1 Apellido2, Nombre : Varchar(48)
	   RPAD(AD0500.AD02CODDPTO,3,'0')                              || -- Servicio Responsable:Number(3)
	   LPAD(NVL(TO_CHAR(CI03NUMASEGURA),' '),6,' ')                || -- Numero de As3egurado ACUNSA: Number(6)
	   LPAD(NVL(CI36CODTIPPOLI,' '),3,' ') FA1R00                     -- Tipo de poliza: Number(3)
FROM 
	   FA0400,CI0300,AD0800,AD0700,AD2500,AD0500,CI2200,
	   FA4800,FA4900
WHERE 
	    FA0400.AD07CODPROCESO=AD0800.AD07CODPROCESO 
   AND FA0400.AD01CODASISTENCI=AD0800.AD01CODASISTENCI 
	AND FA0400.AD07CODPROCESO=AD0500.AD07CODPROCESO 
   AND FA0400.AD01CODASISTENCI=AD0500.AD01CODASISTENCI 
   AND FA0400.AD01CODASISTENCI=AD2500.AD01CODASISTENCI 
   AND FA0400.AD07CODPROCESO=AD0700.AD07CODPROCESO 
   AND CI0300.CI22NUMHISTORIA(+)=AD0700.CI22NUMHISTORIA 
   AND FA0400.CI32CODTIPECON='J'
   AND CI2200.CI22NUMHISTORIA=AD0700.CI22NUMHISTORIA
   AND FA04FECFACTURA BETWEEN 
         NVL(CI0300.CI03FECINIPOLI,TO_DATE('01011999','DDMMYYYY')) 
     AND NVL(CI0300.CI03FECFINPOLI,SYSDATE)
   AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
   AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
   AND FA04NUMFACREAL IS NULL
--------------------------------------------------------------------------------
-- registros de tipo '10'
CREATE OR REPLACE VIEW FA0407J AS
-- FICHERO ACUNSA NUMERO 1, REGISTROS DE TIPO '10'
SELECT
      FA4800.FA48CODDISKACUNSA,      
      LPAD(SUBSTR(FA0400.FA04NUMFACT,4),7,'0')                    || -- Numero de Factura: Number(7)
      '10'                                                        || -- Registros de tipo '10'
	   RPAD(FA0300.AD02CODDPTO_R,3,'0')                            || -- Servicio Realizador:Number(3)
	   RPAD(GCFN05(AD02DESDPTO),12,' ')                            || -- Nombre del servicio anterior: Varchar
	   LPAD(SUM(FA03CANTFACT*FA03PRECIOFACT),10,'0') FA1R10           -- Reparto: Number(10)
FROM
      FA0400,FA0300,AD0200,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND AD0200.AD02CODDPTO=FA0300.AD02CODDPTO_R
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
GROUP BY 
      FA4800.FA48CODDISKACUNSA,FA0400.FA04NUMFACT,AD02CODDPTO_R,AD02DESDPTO
--------------------------------------------------------------------------------
-- registros de tipo '90'
CREATE OR REPLACE VIEW FA0408J AS
-- FICHERO ACUNSA NUMERO 1, REGISTROS DE TIPO '90'
SELECT DISTINCT
      FA4800.FA48CODDISKACUNSA,      
      LPAD(SUBSTR(FA0400.FA04NUMFACT,4),7,'0')                    || -- Numero de Factura: Number(7)
      '90'                                                        || -- Registros de Tipo '90'
      LPAD(FA04CANTFACT,10,'0')                                   || -- Cantidad Facturada (Total) Number(10)
      LPAD(SUM(FA03CANTFACT*10),6,0)                              || -- Numero de Estancias
      TO_CHAR(FA48FECHA,'YYYYMMDD')                               || -- Ultimo dia del mes de facturacion
      LPAD(NVL(AD0500.SG02COD,' '),5,' ')                         || -- Doctor Responsable: Varchar(5)
      LPAD(FNFA02(FA04OBSERV),8,'0')  FA1R90                         -- Numero de Volante
FROM 
      FA0400,FA0300,FA4800,FA4900,FA0500,AD0500
WHERE   
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA0400.FA04NUMFACT=FA4900.FA04NUMFACT
  AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG
  AND FA0500.FA08CODGRUPO=5
  AND FA0400.FA04CANTFACT<>0 -- SOLO LAS FACTURAS CON IMPORTE
  AND FA0400.AD07CODPROCESO=AD0500.AD07CODPROCESO
  AND FA0400.AD01CODASISTENCI=AD0500.AD01CODASISTENCI
  AND AD0500.AD05FECFINRESPON IS NULL
  AND FA04NUMFACREAL IS NULL
GROUP BY
      FA4800.FA48CODDISKACUNSA,FA0400.FA04NUMFACT,
      FA0400.FA04CANTFACT,FA48FECHA,AD0500.SG02COD,FA04OBSERV
--------------------------------------------------------------------------------
-- registros de tipo '99'
CREATE OR REPLACE VIEW FA0409J AS
-- FICHERO ACUNSA NUMERO 1, REGISTROS DE TIPO '99'
SELECT
      FA4800.FA48CODDISKACUNSA,
      '0000000'                                                   || -- Factura
      '99'                                                        || -- Registro tipo '99'
      LPAD(SUM(FA04CANTFACT),10,'0')       FA1R99                    -- Importe Total
FROM
      FA0400,FA4800,FA4900
WHERE
      FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
 AND  FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
GROUP BY
      FA4800.FA48CODDISKACUNSA
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Segundo Fichero
CREATE OR REPLACE VIEW FA0410J AS
-- SEGUNDO FICHERO DE ACUNSA, REGISTROS DE TIPO 'F'
SELECT 
      FA4800.FA48CODDISKACUNSA,
      'F'                                                         || -- 'F'
      SUBSTR(FA0400.FA04NUMFACT,4)                 FA2RF             -- Numero de Factura
FROM 
      FA0400,FA4800,FA4900
WHERE
      FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA0400.FA04NUMFACT=FA4900.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
--------------------------------------------------------------------------------
--MOVIMIENTOS
CREATE OR REPLACE VIEW FA0411J(FA48CODDISKACUNSA,MOVACUN) AS
-- SEGUNDO FICHERO DE ACUNSA, REGISTROS DE TIPO 'M', MOVIMIENTOS
SELECT 
      FA4800.FA48CODDISKACUNSA,
      'M'                                                         || -- 'M'
      SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura
      '8'                                                         || -- Tipo 8: Pruebas Menos las de Taller Ortopedico
      TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la prueba
      LPAD(FA0500.FA05CODCATEG,6,'0')                             || -- Codigo de la prueba : Number(6)
      LPAD(FA03CANTFACT,4,'0')                                    || -- Cantidad Facturada: Number(4)
      LPAD(FA03CANTFACT*FA03PRECIOFACT,10,'0')                    || -- Precio total Facturado: Number(10)
      LPAD(FA0300.AD02CODDPTO_R,3,'0')                            || -- Departamento que realiza: Number(3)
      LPAD(FA0300.SG02COD_R,4,' ')                                   -- Doctor que realiza
FROM 
      FA0400,FA0300,FA0500,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG
  AND FA0500.FA08CODGRUPO IN (7,9)
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA0300.AD02CODDPTO_R<>404 --TALLER ORTOPEDICO
  AND FA04NUMFACREAL IS NULL
UNION ALL
SELECT
      FA4800.FA48CODDISKACUNSA,
      'M'                                                         || -- 'M'
      SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de factura
      '1'                                                         || -- Tipo 1: Farmacia sin Protesis
      LPAD(SUM(FA03CANTFACT*FA03PRECIOFACT),10,'0')                  -- Suma de Farmacia sin Protesis
FROM 
   FA0400,FA0300,FA0500,FR7300,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG
  AND FA0500.FA08CODGRUPO=1
  AND FA0500.FA05CODORIGEN=FR7300.FR73CODINTFAR
  AND FR7300.FR00CODGRPTERAP NOT LIKE 'QI%'
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
GROUP BY
      FA4800.FA48CODDISKACUNSA,FA0400.FA04NUMFACT
UNION ALL
SELECT
      FA4800.FA48CODDISKACUNSA,
      'M'                                                         || -- 'M'
      SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de factura
      '9'                                                         || -- Tipo 9: Farmacia Prótesis + Taller Ortopédico
      LPAD(SUM(FA03CANTFACT*FA03PRECIOFACT),10,'0')                  -- Suma Prótesis + Taller Ortopédico
FROM 
   FA0400,FA0300,FA0500,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
  AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND ( (FA0300.AD02CODDPTO_R=404 
     AND FA0500.FA08CODGRUPO=8
        ) 
   OR 
        (FA0500.FA05CODORIGEN IN 
         (SELECT FR73CODINTFAR -- PROTESIS
          FROM FR7300 
          WHERE FR00CODGRPTERAP LIKE 'QI%') 
        )
     AND FA0500.FA08CODGRUPO=1
     )
GROUP BY
      FA4800.FA48CODDISKACUNSA,FA0400.FA04NUMFACT
UNION ALL
SELECT 
      FA4800.FA48CODDISKACUNSA,
      'M'                                                         || -- 'M'
      SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura
      '2'                                                         || -- Tipo 2: Intervenciones
      TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la Intervencion
      LPAD(FA0500.FA05CODCATEG,6,'0')                             || -- Codigo de la intervencion : Number(6)
      LPAD(FA03CANTFACT,4,'0')                                    || -- Cantidad Facturada: Number(4)
      LPAD(FA03CANTFACT*FA03PRECIOFACT,10,'0')                    || -- Precio total Facturado: Number(10)
      LPAD(AD02CODDPTO_R,3,'0')                                   || -- Departamento que realiza: Number(3)
      LPAD(SG02COD_R,4,' ')                                          -- Doctor que realiza
FROM 
      FA0400,FA0300,FA0500,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG
  AND FA0500.FA08CODGRUPO=6
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
UNION ALL
SELECT 
      FA4800.FA48CODDISKACUNSA,
      'M'                                                         || -- 'M'
      SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura
      '3'                                                         || -- Tipo 3: Derechos de quirofano
      TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la Intervencion
      LPAD(FA05CODCATEG,6,'0')                                    || -- Codigo de derechos de quirofano : Number(6)
      LPAD(FA03CANTFACT*15,4,'0')                                    -- Cantidad Facturada (Minutos): Number(4)
FROM 
      FA0400,FA0300,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND FA0300.FA05CODCATEG=35354
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
UNION ALL
SELECT 
      FA4800.FA48CODDISKACUNSA,
      'M'                                                         || -- 'M'
      SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura
      '4'                                                         || -- Tipo 4: Anestesista
      TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la Intervencion
      LPAD(FA05CODCATEG,6,'0')                                    || -- Codigo de honorarios anestesista : Number(6)
      LPAD(FA03CANTFACT,4,'0')                                    || -- Cantidad Facturada: Number(4)
      LPAD(FA03CANTFACT*FA03PRECIOFACT,10,'0')                       -- Precio total Facturado: Number(10)
FROM 
      FA0400,FA0300,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND FA0300.FA05CODCATEG=35353
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
UNION ALL
SELECT 
      FA4800.FA48CODDISKACUNSA,
      'M'                                                         || -- 'M'
      SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura
      '5'                                                         || -- Tipo 5: Estancias
      TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la estancia
      LPAD(FA0500.FA05CODCATEG,6,'0')                             || -- Codigo de la estancia : Number(6)
      LPAD(FA03CANTFACT,4,'0')                                    || -- Cantidad Facturada: Number(4)
      LPAD(FA03CANTFACT*FA03PRECIOFACT,10,'0')                    || -- Precio total Facturado: Number(10)
      LPAD(AD02CODDPTO_R,3,'0')                                   || -- Departamento que realiza: Number(3)
      LPAD(SG02COD_R,4,' ')                                          -- Doctor que realiza
FROM 
      FA0400,FA0300,FA0500,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA0300.FA04NUMFACT
  AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG
  AND FA0500.FA08CODGRUPO=5
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Tercer Fichero de Acunsa
CREATE OR REPLACE VIEW FA0412J AS
-- TERCER FICHERO DE ACUNSA, LINEAS DE FACTURA
SELECT
      FA4800.FA48CODDISKACUNSA,
      LPAD(SUBSTR(FA0400.FA04NUMFACT,4),7,'0')                    || -- Numero de Factura: Number(7)
      LPAD(FA16NUMLINEA,3,'0')                                    || -- Numero de Linea: Number(3)
      RPAD(FA16DESCRIP,50,' ')                                    || -- Texto de la linea: Varchar(150)
      LPAD(FA16IMPORTE,10,'0')   FA3R1                               -- Importe de la linea: Number(10)
FROM
      FA0400,FA1600,FA4800,FA4900
WHERE
      FA0400.FA04CODFACT=FA1600.FA04CODFACT
  AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA
  AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT
  AND FA04NUMFACREAL IS NULL
