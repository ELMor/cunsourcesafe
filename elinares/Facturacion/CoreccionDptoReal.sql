update fa0300
   set ad02coddpto_s=215
where
   fa05codcateg in (select fa05codcateg from fa0500 where fa08codgrupo=1)
   and (ad02coddpto_s is null or ad02coddpto_s=0)
   
update fa0300
   set ad02coddpto_s=(select ad02coddpto from ad1500 where ad15codcama=gcfn07(fa03observ))
where
   fa05codcateg in (select fa05codcateg from fa0500 where fa08codgrupo=5)
   and (ad02coddpto_s is null or ad02coddpto_s=0)
   
update fa0300
   set ad02coddpto_s=(select fa05codorigc1 from fa0500 where fa0500.fa05codcateg=fa0300.fa05codcateg)
where
   fa05codcateg in (select fa05codcateg from fa0500 where fa05codorigc1 is not null and fa08codgrupo in (6,7))

update fa0300
   set ad02coddpto_s=(select substr(fa05codorigen,1,3) from fa0500 where fa0500.fa05codcateg=fa0300.fa05codcateg)
where
   fa05codcateg in (select fa05codcateg from fa0500 where fa05codorigc1 is null and fa08codgrupo in (6,7) and length(fa05codorigen)=6)

update fa0300
   set ad02coddpto_s=()
where
   fa05codcateg in (select fa05codcateg from fa0500 where fa05codorigc1 is null and fa08codgrupo in (6,7) and length(fa05codorigen)<6)
