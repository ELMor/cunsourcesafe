VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{BE4F3AC8-AEC9-101A-947B-00DD010F7B46}#1.0#0"; "msoutl32.ocx"
Begin VB.Form Edit 
   Caption         =   "Editor de Campos"
   ClientHeight    =   5730
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7965
   LinkTopic       =   "Form1"
   ScaleHeight     =   5730
   ScaleWidth      =   7965
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Seleccione campo y opci�n. Cuando finalice, presione OK."
      Height          =   5655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7935
      Begin MSOutl.Outline ol 
         Height          =   5175
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   5535
         _Version        =   65536
         _ExtentX        =   9763
         _ExtentY        =   9128
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame2 
         Caption         =   "Propiedades "
         Height          =   4455
         Left            =   5760
         TabIndex        =   3
         Top             =   240
         Width           =   2055
         Begin VB.OptionButton Option1 
            Caption         =   "Ent Sin Signo/Bin"
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   21
            Top             =   2280
            Width           =   1815
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Binario"
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   20
            Top             =   1800
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Sin compresion"
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   15
            Top             =   3720
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Juego 32 carac."
            Height          =   195
            Index           =   7
            Left            =   120
            TabIndex        =   14
            Top             =   3960
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Juego 40 carac."
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   13
            Top             =   4200
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Tabla"
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   12
            Top             =   2880
            Width           =   1455
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   120
            TabIndex        =   11
            Text            =   "<Nombre de tabla>"
            Top             =   3120
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Entero sin signo"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   10
            Top             =   1320
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Entero con signo"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   9
            Top             =   1560
            Width           =   1575
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Decimal"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   8
            Top             =   2040
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Rel 1/1/1850"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   7
            Top             =   720
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "AAAAMMDD"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   6
            Top             =   480
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Texto:"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   19
            Top             =   3480
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Tabla:"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   18
            Top             =   2640
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "N�mero:"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   17
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha:"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   16
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Index           =   1
         Left            =   6840
         TabIndex        =   2
         Top             =   4800
         Width           =   975
      End
      Begin VB.CommandButton Command1 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         Height          =   375
         Index           =   0
         Left            =   5760
         TabIndex        =   1
         Top             =   4800
         Width           =   975
      End
      Begin VB.Label msg 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   5760
         TabIndex        =   5
         Top             =   5280
         Width           =   2055
      End
   End
   Begin MSWinsockLib.Winsock ctlsock 
      Left            =   5640
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327681
      Protocol        =   1
      RemoteHost      =   "127.0.0.1"
      RemotePort      =   7
   End
End
Attribute VB_Name = "Edit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim poslev(0 To 20) As Integer
Dim lev As Integer
Dim CurrIndex As Integer
Dim boo() As Integer
Dim nomtab() As String
Dim Tipos(0 To 10) As String

Private Sub udpSend(data$)
    ctlsock.SendData data
End Sub

Private Sub Command2_Click(Index As Integer)
    ol.ListIndex = ol.ListIndex + IIf(Index = 0, -1, 1)
End Sub

Private Sub Option1_Click(Index As Integer)
    Dim j%
    j = ol.ListIndex
    msg = "ListIndex " & j
    boo(j) = Index
    If Index = 5 Then
        Text1.Enabled = True: Text1.SetFocus
    Else
        udpSend CStr(j) & " " & Tipos(Index) & " null"
    End If
End Sub

Private Sub Command1_Click(Index As Integer)
    Dim j%
    Select Case Index
        Case 0: udpSend "CANCEL": DoEvents
        Case 1: udpSend "END": DoEvents
    End Select
    End
End Sub

Private Sub ctlsock_DataArrival(ByVal bytesTotal As Long)
    Dim i%, j%, str$, l&
    If bytesTotal = 202 Then
        ctlsock.GetData str, vbString: i = Left$(str, 2): str = Right(str, 200)
        For j = 1 To 200
            If Mid(str, j, 1) = "," Or Mid(str, j, 1) = ";" Then
                str = Left(str, j - 1): j = 200
            End If
        Next
        CurrIndex = CurrIndex + 1
        ReDim Preserve boo(CurrIndex)
        ReDim Preserve nomtab(CurrIndex)
        boo(CurrIndex) = -1
        For j = 0 To UBound(Tipos)
            l = InStr(1, str, Tipos(j))
            If l <> 0 Then
                boo(CurrIndex) = j
                If j = 5 Then
                    nomtab(CurrIndex) = Right(str, Len(str) - l - Len(Tipos(5)))
                End If
            End If
        Next
        If i <= lev Then
            ol.ListIndex = poslev(i - 1): ol.AddItem str
            lev = i: poslev(lev) = CurrIndex
        End If
        If i > lev Then
            ol.ListIndex = poslev(lev): ol.AddItem str
            lev = i: poslev(lev) = CurrIndex
        End If
        msg = "A�adiendo " & str & "..."
        udpSend "SIG"
    End If
    ctlsock.GetData str, vbString
    If str = "MAS" Then
        udpSend "SIG"
    End If
    If str = "EMPTY" Then
        msg = "Fin de lectura de campos."
    End If
End Sub

Private Sub ctlsock_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    MsgBox Description, vbCritical, "Error en la comunicaci�n"
    End
End Sub

Private Sub Form_Activate()
    lev = 0: poslev(0) = 0: ol.AddItem "Declares..."
    Tipos(0) = "FECABS": Tipos(1) = "FECREL": Tipos(2) = "ENTSINSIG"
    Tipos(3) = "ENTCONSIG": Tipos(4) = "DECIMAL": Tipos(5) = "TABLA"
    Tipos(6) = "TEXSINCOM": Tipos(7) = "TEXJUE32": Tipos(8) = "TEXJUE40"
    Tipos(9) = "BINARIO": Tipos(10) = "ENTSINSIGBIN"
    msg = "Conectando ...": ctlsock.Connect
    msg = "Conectado. Iniciando transferencia..."
    Call udpSend("INIT")
End Sub

Private Sub ol_Click()
    Dim i%
    msg = "ListIndex " & ol.ListIndex
    If (boo(ol.ListIndex) < 0) Then
        For i = 0 To UBound(Tipos)
            Option1(i).Value = False
        Next i
    Else
        Option1(boo(ol.ListIndex)).Value = True
    End If
    If boo(ol.ListIndex) = 5 Then
        Text1.Enabled = True
    Else
        Text1.Enabled = False
    End If
    Text1.Text = nomtab(ol.ListIndex)
End Sub

Private Sub Text1_Change()
    nomtab(ol.ListIndex) = Text1.Text
    If Text1.Text <> "" Then
        udpSend CStr(ol.ListIndex) & Tipos(5) & " " & Text1.Text
    End If
End Sub
