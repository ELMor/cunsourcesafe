
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "fichero.h"
#include "cursor.h"

CFic::CFic(char *n, CTReader* cb ){
	nom=strdup(n);
	ctf=new CTFile(n,cb);
	needsave=1;
	nbsp2=1+(ulong)(log(ctf->getnblk())/log(2));
	sp2=1<<nbsp2;
}

//tr==0 si es de tama�o variable, tr==-1 sise cree que hay un .info
CFic::CFic(char* n,long tr, long szcmsv){
	FILE *fp1;
	struct stat st1,st2;
	char tapen[1024],relpa[1024],*s;
	long fintape=0;

	ctf=NULL; //Inicializar CTapeFile
	nom=(char*)malloc( 256 );
	strcpy(nom,n);
	if( (s=strchr(nom,'#'))!=NULL ){
		*s=0;
		strcpy(tapen,nom);
		strcpy(relpa,s+1);
		*s='#';
		fintape=1;
	}
	strcat(nom,".info");
	if( !stat(nom,&st2) ){ //Existe .info; comprobar que este up-to-date
		if( stat( fintape ? tapen : n, &st1 ) ){
			perror(fintape? tapen : n);
			exit(-1);
		}
		if( st1.st_ctime <= st2.st_ctime ){
			fp1=fopen(nom,"rb");
			ctf=new CTFile( fp1 );
			fclose(fp1);
			needsave=0;
		}
	}
	if( !ctf ){	//No se pudo cargar del info
		if( strstr(n,".group.") ){ //Es un fichero de grupo
			needsave=0;
			FILE *fpaux=fopen(n,"rb");
			ctf= new CTFile( fpaux );
			fclose(fpaux);
		}else{
			needsave=1;
			if(fintape){
				CTape *tape=new CTape(tapen,szcmsv,NULL);
				ctf=tape->getFile( relpa );
			}else{
				CTReader *auxctr=new CTReader(n,0);
				CTReader *ccb   =new CTReader(auxctr,tr,4);
				if( tr==0 )
					ccb = new CTReader( ccb,0,4 );
				ctf=new CTFile(n, ccb);
			}
		}
	}
	nbsp2=1+(ulong)(log(ctf->getnblk())/log(2));
	sp2=1<<nbsp2;
}

CFic::~CFic(){
	if( needsave ){
		FILE *fp1=fopen(nom,"wb");
		ctf->Serialize(fp1);
		fclose(fp1);
	}
	//free(nom);
	delete ctf;
}
uchar* CFic::getStat(CCur* cur){
	memset(cur->ibuffer,0,32*1024);
	ctf->ReadBlock((char*)cur->ibuffer,cur->regact);
	return cur->ibuffer;
}

uchar* CFic::getReg(CCur *cur){
	if(cur->regact>=ctf->getnblk())
		return (uchar*)NULL;
	return getStat(cur);
}

bool CFic::busca(CCur* pCur,long PosReg, int numvals, va_list marker){
	puts("Busqueda en ficheros no indexados sin implementar.");
	exit(-1);
	return false;
}

