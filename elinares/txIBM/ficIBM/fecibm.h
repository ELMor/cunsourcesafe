
#ifndef __FECIBM_H__
#define __FECIBM_H__

typedef  long  ul;
typedef  long* ulp;

void fec2dias(ul a,ul m, ul d, ulp dias);
void dias2fec(ul dias, ulp a, ulp m, ulp d);
void fecmasdias(ul a,ul m, ul d, ul dias, ulp as, ulp ms, ulp ds);

#endif