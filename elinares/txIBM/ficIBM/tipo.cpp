
#include "tipo.h"

void CTip::dump(char *s, FILE *fp){
	char aux[200];
	switch(tipo){
		case NODEF:
		case TDCL:
			sprintf(aux," ");
			break;
		case TCHAr: 
			sprintf(aux," CHAR");       
			if(dim) 
				dim->dump(aux+strlen(aux)); 
			break;
		case TBIN:  
			sprintf(aux," BIN FIXED");  
			if(dim) 
				dim->dump(aux+strlen(aux)); 
			break;
		case TBIT:  
			sprintf(aux," BIT");        
			if(dim) 
				dim->dump(aux+strlen(aux)); 
			break;
		case TDEC:  
			sprintf(aux," DEC FIXED");  
			if(dim) 
				dim->dump(aux+strlen(aux)); 
			break;
		case TPIC:  
			sprintf(aux," PIC'%s'",pictur);						  
			break;
	}
	if(s)
		strcpy(s,aux);
	else
		fprintf(fp,"%s",aux);
}
