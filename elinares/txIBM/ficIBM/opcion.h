
#ifndef __OPCION_H__
#define __OPCION_H__

#include "objeto.h"

class COpc : public CObj {
	private:
		tipInt	tipo;
		char*	nomtab;
	public:
		COpc(){
			tipo=UnDef;
			nomtab=(char*)NULL;
		}
		COpc(tipInt t, char *s=(char*)NULL){
			tipo=t;
			nomtab=s;
		}
		~COpc(){
			if(nomtab) free(nomtab);
		}
		tipInt  getTip		(void)		{ return tipo;	}
		char*	getNTab		(void)		{ return nomtab;}
		void	setTip		(tipInt c,  
							char *s=(char*)NULL
							)			{ tipo=c;	nomtab=s;}
		void	dump(char *s,FILE *fp=(FILE*)NULL);

};

#endif
