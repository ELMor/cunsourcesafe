
#ifndef __FICHERO_H__
#define __FICHERO_H__

#include <stdio.h>
#include <stdarg.h>
#include "defgen.h"
#include "fmanip.h"
#include "tape.h"

class CCur;			//Referencias adelantada.

class CFic {
//-----------------------------------------------------------------------------
	protected:
		char		*nom;
		CTFile		*ctf;
		ulong		nbsp2,sp2,needsave;		//Siguiente potencia de 2 al tamreg
//-----------------------------------------------------------------------------
	public:
				CFic		(char* n, CTReader *cb );
				CFic		(char* n, long lr, long szcmsv);
				~CFic		();
		ulong	getNumReg	(void)			{ return ctf->getnblk(); }
		uchar*	getReg		(CCur* cur);
		CBlk*	getBlock	(long b)		{ return ctf->getBlock(b); }
		long	getTamReg	(long b)		{ return ctf->getBlockSize(b); }
		CTFile*	getCTFile	(void)			{ return ctf;}
virtual uchar*	getStat		(CCur* cur);	
virtual bool	esInd		(void)			{ return false; }
virtual long	numCamInd	(void)			{ return 0; } //En un fichero sin indexar
virtual char*	nomCamInd	(int i)			{ return (char*)NULL;}
virtual bool	busca(CCur* pCur,long PosReg, int numvals=0, va_list marker=NULL);
};

#endif
