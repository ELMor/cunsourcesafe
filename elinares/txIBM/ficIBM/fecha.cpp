
#include "fecha.h"

static int	AntesGregorianoDia    = 4;
static int	AntesGregorianoMes    = 10;
static int	AntesGregorianoAnio   = 1582;
static int	DespuesGregorianoDia  = 15;
static int	DespuesGregorianoMes  = 10;
static int	DespuesGregorianoAnio = 1582;
static int	StartHeisDia          = 1;
static int	StartHeisMes          = 1;
static int	StartHeisAnio         = 3200;

//A una fecha (Dia,Mes,Anio) le suma 'Dias' y obtiene
//*dDia,*dMes,*dAnio
void sumaDias(ushort Dia, ushort Mes, int Anio, int Dias, ushort *dDia, ushort *dMes, int *dAnio)
 {int dDias;

  *dDia = Dia;
  *dMes = Mes;
  *dAnio = Anio;
  if (Dias >= 0)
    {/* add */
     dDias = (int)restaFechas(*dDia,*dMes,*dAnio,1,1,(*dAnio)+1);
     while (Dias >= dDias)
       {/* Anios */
        *dDia = 1;
        *dMes = 1;
        (*dAnio)++;
        Dias -= dDias;
        dDias = (int)restaFechas(*dDia,*dMes,*dAnio,1,1,(*dAnio)+1);
       }
     dDias = (int)restaFechas(*dDia,*dMes,*dAnio,1,(*dMes)+1,*dAnio);
     while (Dias >= dDias)
       {/* Mess */
        *dDia = 1;
        (*dMes)++;
        Dias -= dDias;
        dDias = (int)restaFechas(*dDia,*dMes,*dAnio,1,(*dMes)+1,*dAnio);
       }
     if (Dias > 0)
       {/* Dias */
        *dDia += (ushort)Dias;
       }
    }
  else
    {/* sub */
     dDias = (int)restaFechas(*dDia,*dMes,*dAnio,31,12,(*dAnio)-1);
     while (Dias <= dDias)
       {/* Anios */
        *dDia = 31;
        *dMes = 12;
        (*dAnio)--;
        Dias -= dDias;
        dDias = (int)restaFechas(*dDia,*dMes,*dAnio,31,12,(*dAnio)-1);
       }
     dDias = (int)restaFechas(*dDia,*dMes,*dAnio,GregorianoMesDias((*dMes)-1,*dAnio),(*dMes-1),*dAnio);
     while (Dias <= dDias)
       {/* Mess */
        *dDia = GregorianoMesDias((*dMes-1),*dAnio);
        (*dMes)--;
        Dias -= dDias;
        dDias = (int)restaFechas(*dDia,*dMes,*dAnio,GregorianoMesDias((*dMes)-1,*dAnio),(*dMes)-1,*dAnio);
       }
     if (Dias < 0)
       *dDia -= (ushort)abs(Dias);
    }
 }

//Restar Fechas F2-F1
long restaFechas(unsigned short Dia1, unsigned short Mes1, int Anio1, unsigned short Dia2, unsigned short Mes2, int Anio2)
 {long t1,t2;

  t1 = Dia1; /* set Dias left in the actual Mes */
  t2 = Dia2;

  if ((Anio1 == 1582) && (Mes1 == 10))
   {if ((Dia1 < 5) && GregorianoDiamenor(Dia1,Mes1,Anio1,Dia2,Mes2,Anio2) && GregorianoDiamenor(Dia2,Mes2,Anio2,1,11,1582) && GregorianoDiaMayor(Dia2,Mes2,Anio2,14,10,1582))
      t2 -= 10;
    if (Dia1 > 14)
     {if (GregorianoDiamenor(Dia1,Mes1,Anio1,Dia2,Mes2,Anio2) && GregorianoDiaMayor(Dia2,Mes2,Anio2,31,10,1582))
        t2 += 10;
      if (GregorianoDiaMayor(Dia1,Mes1,Anio1,Dia2,Mes2,Anio2) && GregorianoDiamenor(Dia2,Mes2,Anio2,5,10,1582))
        t1 -= 10;
     }
   }
  if ((Anio2 == 1582) && (Mes2 == 10) && (Dia2 > 14))
   {if (GregorianoDiamenor(Dia2,Mes2,Anio2,Dia1,Mes1,Anio1) && GregorianoDiaMayor(Dia1,Mes1,Anio1,31,10,1582))
      t1 += 10;
    if (GregorianoDiaMayor(Dia2,Mes2,Anio2,Dia1,Mes1,Anio1) && GregorianoDiamenor(Dia1,Mes1,Anio1,1,10,1582))
      t2 -= 10;
   }

  while (Mes1 > 1)
    {/* calc Dias left by the gone Mes of the Anio1 */
     Mes1--;
     t1 += GregorianoMesDias(Mes1,Anio1);
    }

  while (Mes2 > 1)
    {/* calc Dias left by the gone Mes of the Anio2 */
     Mes2--;
     t2 += GregorianoMesDias(Mes2,Anio2);
    }

  while (Anio1 > Anio2)
    {/* calc Dias of Difer Anios */
     Anio1--;
     t1 += GregorianoAnioDias(Anio1);
    }

  while (Anio1 < Anio2)
    {/* calc Dias of Difer Anios */
     Anio2--;
     t2 += GregorianoAnioDias(Anio2);
    }

  return(t2-t1);
 }

unsigned short GregorianoMesDias(unsigned short Mes, int Anio){
       if ((Mes == 2) && (!GregorianoBisiestoAnio(Anio)))
         return(28);
       else
         {/* use Juliano function for other calcs. */
          return(JulianoMesDias(Mes,Anio));
         }
  }

unsigned int GregorianoAnioDias(int Anio){
  unsigned short Mes;
  unsigned int Dias;

  Dias = 0;
  for (Mes=1; Mes<=12; Mes++)
    {/* add the Dias of all 12 Mes */
     Dias += GregorianoMesDias(Mes,Anio);
    }
  return(Dias);
 }

unsigned short JulianoMesDias(unsigned short Mes, int Anio){
  switch (Mes)
     {case  1 :
      case  3 :
      case  5 :
      case  7 :
      case  8 :
      case 10 :
      case 12 : return(31);
      case  4 :
      case  6 :
      case  9 :
      case 11 : return(30);
      case  2 : if (JulianoBisiestoAnio(Anio))
                  return(29);
                else
                  if (!JulianoBisiestoAnio(Anio))
                    return(28);
      default : return(0);
     }
  }

bool GregorianoDiamenor(unsigned short Dia1, unsigned short Mes1, int Anio1, unsigned short Dia2, unsigned short Mes2, int Anio2){
	return(JulianoDiamenor(Dia1,Mes1,Anio1,Dia2,Mes2,Anio2));
 }

bool JulianoBisiestoAnio(int Anio){
  if (Anio <= 0)
     return((bool)(abs(Anio) % 4 == 1));
   else
     return((bool)(Anio % 4 == 0));
  }

bool GregorianoDiaMayor(unsigned short Dia1, unsigned short Mes1, int Anio1, unsigned short Dia2, unsigned short Mes2, int Anio2){
	return(JulianoDiaMayor(Dia1,Mes1,Anio1,Dia2,Mes2,Anio2));
}

bool GregorianoBisiestoAnio(int Anio){
  if (Anio < AntesGregorianoAnio)
     {/* Anio of the Gregoriano reform */
      return(JulianoBisiestoAnio(Anio));
     }
   else
     {/* DespuesGregorianoAnio reform */
      return((bool)((Anio % 4 == 0) && ((Anio % 100 > 0) || (Anio % 400 == 0))));
     }
  }

bool JulianoDiamenor(unsigned short Dia1, unsigned short Mes1, int Anio1, unsigned short Dia2, unsigned short Mes2, int Anio2){
  if (Anio1 == Anio2)
    {if (Mes1 == Mes2)
       return((bool)(Dia1 < Dia2));
     else
       return((bool)(Mes1 < Mes2));
    }
  else
    return((bool)(Anio1 < Anio2));
 }

bool JulianoDiaMayor(unsigned short Dia1, unsigned short Mes1, int Anio1, unsigned short Dia2, unsigned short Mes2, int Anio2){
  if (Anio1 == Anio2)
    {if (Mes1 == Mes2)
       return((bool)(Dia1 > Dia2));
     else
       return((bool)(Mes1 > Mes2));
    }
  else
    return((bool)(Anio1 > Anio2));
 }
