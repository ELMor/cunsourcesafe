
#ifndef __PARSE_H__
#define __PARSE_H__

#include "campo.h"

/**
 * Retorna el numero de DCL encontrados, cuya definiciones coloca en
 * base[0] ... base[nDCL-1]
 */
void Compila(char *s,int Modo=0);
void Compila(FILE *fp,int Modo=0);
int  yyparse(void);
void Init(char *def, char* tab);

#endif
