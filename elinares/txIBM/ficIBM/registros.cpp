
#include "fmanip.h"
#include "registros.h"

CRegs::CRegs(char* fn, char* frn, long treg, long readfull){
	r_fname=strdup(fn);			//Nombre del archivo base.
	fname=strdup(frn);			//Nombre del conjunto de registros.
	tamreg=treg;				//Tama�o del registro
	regs=NULL;					//inicializacion
	numreg=0;					//Numero de registros
	needsave=0;					//No necesita salvarse.
	//if(readfull)				//Es necesario construir el conjunto ahora con todos los reg
}

CRegs::CRegs(char* fn, char *frn){
	r_fname=strdup(fn);			
	fname=strdup(frn);
	carga();
	needsave=0;
}

CRegs::~CRegs(){
	if(needsave){
		graba();
		needsave=0;
	}
	free(fname);
	free(r_fname);
	if(numreg) free(regs);
}

void 
CRegs::carga(void){
	struct stat st1,st2;
	char fn[512],buf[512];
	int existg,existf;
	//Construir nombre del archivo (NombredeFichero.NombredeGrupo)
	strcat(strcat(strcpy(fn,r_fname),"."),fname);
	existg=!stat(fn,&st1);			//Existe el archivo de conjunto de registros?
	existf=!stat(r_fname,&st2);		//Existe el archivo maestro de ibm?
	if( !existf ){					//Error gordo, no se puede continuar
		perror(r_fname);
		exit(-1);
	}
	if( !existg ){					//Error Gordo.
		perror(fname);
		exit(-1);
	}
	if( st1.st_mtime<st2.st_mtime ){//Resulta que el archivo de conjunto es anterior al maestro
		printf("Error: %s es anterior a %s.",buf,r_fname);
		exit(-1);
	}
	//Comprobar marca de grupos:
	FILE *fp=fopen(fn,"rb");
	fread(buf,1,4,fp);
	if(strncmp(buf,"GRP",3)){
		printf("Error: %s no es un archivo de grupo.",fn);
		exit(-1);
	}
	LeeLong(fp,&tamreg);
	LeeLong(fp,&numreg);
	LeeLong(fp,&virt);
	if(!virt)
		LeeLoAr(fp,numreg,&regs);
	fclose(fp);
}

void
CRegs::graba(void){
	char fn[512];
	strcat(strcat(strcpy(fn,r_fname),"."),fname);
	FILE *fp=fopen(fn,"wb");
	if(!fp){
		perror(fn);
		exit(-1);
	}
	fprintf(fp,"GRP");
	EscLong(fp,tamreg);
	EscLong(fp,numreg);
	EscLong(fp,virt);
	if(!virt)
		EscLoAr(fp,numreg,regs);
	fclose(fp);
}

void	
CRegs::addReg	(long  reg){

}

long	
CRegs::getReg	(long  reg){

}
