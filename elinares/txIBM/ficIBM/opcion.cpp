
#include "opcion.h"

void COpc::dump(char *s, FILE *fp){
	char aux[200];
	*aux=0;
	switch (tipo){
		case UnDef:		sprintf(aux+strlen(aux)," ");			break;
		case FecAbs:	sprintf(aux+strlen(aux)," FECABS");		break;
		case FecRel:	sprintf(aux+strlen(aux)," FECREL");		break;
		case EntConSig: sprintf(aux+strlen(aux)," ENTCONSIG");	break;
		case EntSinSig: sprintf(aux+strlen(aux)," ENTSINSIG");	break;
		case EntSinSig_Binario: 
						sprintf(aux+strlen(aux)," ENTSINSIGBIN");break;
		case Binario:	sprintf(aux+strlen(aux)," BINARIO");	break;
		case Binarioss:	sprintf(aux+strlen(aux)," BINARIOSS");	break;
		case Decimal:	sprintf(aux+strlen(aux)," DECIMAL");	break;
		case TexSinCom: sprintf(aux+strlen(aux)," TEXSINCOM");	break;
		case TexJue32:	sprintf(aux+strlen(aux)," TEXJUE32");	break;
		case TexJue40:	sprintf(aux+strlen(aux)," TEXJUE40");	break;
		case Tabla:		sprintf(aux+strlen(aux)," TABLA %s",nomtab); break;
	}
	if(s)
		strcpy(s,aux);
	else
		fprintf(fp,"%s",aux);
}