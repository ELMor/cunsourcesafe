

#include <string.h>
#include "tablas.h"
#include "xlat.h"
#include "pack.h"

CLisTab lTab;

static int 
cmpKey(const void *p1, const void *p2){
	return memcmp(p1,p2,MAXTAMKEY);
}

uchar* 
CTab::getDes(uchar* key, int tdes){
	reg t,*pt;
	memcpy(t.key,key,MAXTAMKEY);
	pt=(reg*)bsearch(&t,regist,numreg,sizeof(reg),cmpKey);
	if(pt && tdes==2)
		return pt->keydec;
	if(pt && tdes==1)
		return pt->des;
	if(pt && tdes==0)
		return pt->cod;
	return (uchar*)" ";
}

CTab::CTab(uchar *nom, uchar *data){
	//Inicializacion miembros privados.
	nombre=nom;	numreg=0;	regist=(reg*)NULL;
	//Parsing de Raw data
	long tamsco,tamcod;
	long tamdat=*((long*)data);
	tamsco=upbincs(data+sizeof(long)+0,2);		//Tama�o del subcodigo
	tamcod=upbincs(data+sizeof(long)+2,2);		//del codigo
	tamkey=upbincs(data+sizeof(long)+4,2);		//y de la Clave
	if(tamkey>MAXTAMKEY)
		printf("Tabla con tamkey=%ld !!",tamkey);
	long tamrel,offnow=sizeof(long)+8;
	while(offnow<tamdat){
		uchar *p;
		regist=(reg*)realloc(regist,(1+numreg)*sizeof(reg));
		offnow+=tamsco;							//posicionar en 'Codigo'
		p=regist[numreg].cod=(uchar*)malloc(1+tamcod);
		memcpy(p,data+offnow,tamcod);
		p[tamcod]=0;							//Colocar un '0' al final.
		se2a(p,p,tamcod);						//EBCDIC -> ASCII de c�digo
		offnow+=tamcod;							//Ponerse en la Key
		p=regist[numreg].key;
		memset(p,0,MAXTAMKEY);					//inicializar la clave
		memcpy(p,data+offnow,tamkey);			//Copiarla
		offnow+=tamkey;							//Ponerse en Desc
		tamrel=upbinss(data+offnow,1);			//Calcular tama�o
		offnow+=1;
		p=regist[numreg].des=(uchar*)malloc(tamrel+1);
		memcpy(p,data+offnow,tamrel);			//Copiarla
		p[tamrel]=0;							//Cadena a 0
		se2a(p,data+offnow,tamrel);				//EBCDIC -> ASCII de Desc
		offnow+=tamrel;
		numreg++;
	}
	free(data);
	qsort(regist,numreg,sizeof(reg),cmpKey);//ordenar por key.
	//Suponemos que es un numero binario
	ulong idx,ne=0,n=0,c=0;
	for(idx=0;idx<numreg;idx++){
		unsigned char *s=&(regist[idx].key[0]);
		if( (s[tamkey-1]&0x0c)==0x0c||(s[tamkey-1]&0x0d)==0x0d ){
			ne++;
		}else if ((*s==0 || (tamkey==1 && *s<=numreg*2)||(tamkey==2&&*s<32)) ){
			n++;
		}else{
			c++;
		}
	}
	char bbuf[2*MAXTAMKEY];
	if(ne==numreg){
		tconv=1;
		for(idx=0;idx<numreg;idx++){
			sprintf(bbuf,"%d",updcbcs(regist[idx].key,2*tamkey));
			strcpy((char*)&(regist[idx].keydec[0]),bbuf);
		}
	}else if(n>ne && n>c){
		tconv=2;
		for(idx=0;idx<numreg;idx++){
			sprintf(bbuf,"%d",upbinss(regist[idx].key,tamkey));
			strcpy((char*)&(regist[idx].keydec[0]),bbuf);
		}
	}else{
		tconv=3;
		for(idx=0;idx<numreg;idx++){
			se2a(regist[idx].keydec,regist[idx].key,tamkey);
			regist[idx].keydec[tamkey]=0;
		}
	}
}

CTab::CTab( FILE *fp){
	ulong t;
	fread(&t,sizeof(ulong),1,fp);				//Leer nombre de la tabla
	nombre=(uchar*)malloc(1+t);
	fread(nombre,1,t,fp);
	nombre[t]=0;								//Finalizar con '0'
	fread(&numreg,sizeof(ulong),1,fp);			//Leer numero de registros
	fread(&tconv,sizeof(ulong),1,fp);			//Leer Tipo de Conversion
	fread(&tamkey,sizeof(ulong),1,fp);			//Leer Tipo de Conversion
	regist=(reg*)malloc(sizeof(reg)*numreg);
	ulong i;
	for(i=0;i<numreg;i++){
		reg *mreg=regist+i;
		fread(mreg->key,MAXTAMKEY,1,fp);		//Leer la clave
		fread(&t,sizeof(ulong),1,fp);			
		mreg->cod=(uchar*)malloc(1+t);
		fread(mreg->cod,t,1,fp);				//Leer el codigo
		mreg->cod[t]=0;							//Finalizar con '0'
		fread(&t,sizeof(ulong),1,fp);			
		mreg->des=(uchar*)malloc(1+t);
		fread(mreg->des,t,1,fp);				//Leer la descripcion
		mreg->des[t]=0;							//Finalizar con '0'.
		fread(mreg->keydec,MAXTAMKEY,1,fp);		//Leer la descripcion de la clave
	}
}

CTab::~CTab(){
	free(nombre);
	ulong i;
	for(i=0;i<numreg;i++){
		free( regist[i].cod );
		free( regist[i].des );
	}
	free( regist );
}

void 
CTab::grabar( FILE *fp){
	ulong t=strlen((ccp)(nombre));
	fwrite(&t,sizeof(ulong),1,fp);				//Grabar el nombre de la tabla
	fwrite(nombre,1,t,fp);
	fwrite(&numreg,sizeof(ulong),1,fp);			//Grabar el numero de registros
	fwrite(&tconv,sizeof(ulong),1,fp);			//Grabar el tipo de conversion de la clave
	fwrite(&tamkey,sizeof(ulong),1,fp);			//Grabar el tipo de conversion de la clave
	ulong i;
	for(i=0;i<numreg;i++){
		reg *mreg=regist+i;
		fwrite(mreg->key,MAXTAMKEY,1,fp);		//Grabar la clave
		t=strlen((ccp)(mreg->cod));
		fwrite(&t,sizeof(long),1,fp);			//Grabar el codigo
		fwrite(mreg->cod,t,1,fp);
		t=strlen((char*)(mreg->des));
		fwrite(&t,sizeof(long),1,fp);			//Grabar la descripcion
		fwrite(mreg->des,t,1,fp);
		fwrite(mreg->keydec,MAXTAMKEY,1,fp);	//Grabar la descipcion de la clave
	}
}

void 
CarTabl(char *fn){
	lTab.carga(fn);
}

int 
cmpNomTab(const void *p1, const void *p2){
	CTab *t1=*(CTab**)p1,*t2=*(CTab**)p2;
	return stricmp((ccp)t1->getNom(),(ccp)t2->getNom());
}

void 
CLisTab::carga(char *fn){
	FILE *fp=fopen(fn,"rb");
	if(!fp){
		perror(fn);
		exit(-1);
	}
	fread(&numtab,sizeof(ulong),1,fp);
	tablas=(CTab**)malloc(numtab*sizeof(CTab*));
	ulong i;
	for(i=0;i<numtab;i++)
		tablas[i]=new CTab(fp);
	fclose(fp);
	qsort(tablas,numtab,sizeof(CTab*),cmpNomTab);
}

void 
CLisTab::graba(char *fn){
	FILE *fp=fopen(fn,"wb");
	ulong i;
	fwrite(&numtab,sizeof(ulong),1,fp);
	for(i=0;i<numtab;i++)
		tablas[i]->grabar( fp );
}

void 
CLisTab::ins(CTab* c){
	numtab++;
	tablas=(CTab**)realloc(tablas,numtab*sizeof(CTab*));
	tablas[numtab-1]=c;
}

uchar* 
CLisTab::getDes(char *ntab,uchar* key,int tdes){
	char **p=&ntab;
	CTab **pt=(CTab**)bsearch(&p,tablas,numtab,sizeof(CTab*),cmpNomTab);
	if(pt)
		return (*pt)->getDes(key,tdes);
	else{
		printf("Tabla %s no encontrada.\n",ntab);
		exit(-1);
	}
	return (uchar*)NULL;
}

CTab*
CLisTab::getTab(char *ntab){
	char **p=&ntab;
	CTab **pt=(CTab**)bsearch(&p,tablas,numtab,sizeof(CTab*),cmpNomTab);
	if(pt)
		return *pt;
	else{
		printf("Tabla %s no encontrada.\n",ntab);
		exit(-1);
	}
	return (CTab*)NULL;
}

CLisTab::CLisTab(){
	numtab=0;
	tablas=(CTab**)NULL;
}

CLisTab::~CLisTab(){
	ulong i;
	for(i=0;i<numtab;i++)
		delete tablas[i];
	free(tablas);
}

void
CLisTab::Dump(FILE *fp){
	ulong i;
	fprintf(fp,"Total de Tablas: %ld\n",numtab);
	for(i=0;i<numtab;i++){
		fprintf(fp,"%03ld) ",i+1);
		tablas[i]->Dump(fp);
	}
}

void
CTab::Dump(FILE *fp){
	ulong i;
	fprintf(fp,"\t %s (%ld,%ld)\n\t\t",nombre,numreg,tamkey);
	for(i=0;i<numreg;i++){
		fprintf(fp,"'%s',",regist[i].keydec);
		if(!((i+1)%20))
			fprintf(fp,"\n\t\t");
	}
	fprintf(fp,"\n");
}
