#ifndef __FECHA_H__
#define __FECHA_H__

#include "defgen.h"
#include "stdlib.h"

/**
 * Suma 'Dias' dias a la fecha (Anio,Mes,Dia) y obtiene (dAnio,dMes,dDia)
 */
void	sumaDias(
	ushort Dia, ushort Mes, int Anio, int Dias,	//INPUT
	ushort *dDia, ushort *dMes, int *dAnio);	//OUTPUT

/**
 * Resta Fecha1-Fecha2 y devuelve el numero de dias.
 */
long	restaFechas(
	ushort Dia1, ushort Mes1, int Anio1, 
	ushort Dia2, ushort Mes2, int Anio2);

/**
 * Funciones auxiliares.
 */
ushort	GregorianoMesDias( 
	ushort Mes, int Anio);

uint	GregorianoAnioDias( 
	int Anio);

ushort	JulianoMesDias( 
	ushort Mes,  
	int Anio);

bool	GregorianoDiamenor( 
	ushort Dia1,  ushort Mes1,  int Anio1,  
	ushort Dia2,  ushort Mes2,  int Anio2);

bool	GregorianoDiaMayor( 
	ushort Dia1,  ushort Mes1,  int Anio1,  
	ushort Dia2,  ushort Mes2,  int Anio2);

bool	GregorianoBisiestoAnio( 
	int Anio);

bool	JulianoDiamenor( 
	ushort Dia1,  ushort Mes1,  int Anio1,  
	ushort Dia2,  ushort Mes2,  int Anio2);

bool	JulianoDiaMayor( 
	ushort Dia1,  ushort Mes1,  int Anio1,  
	ushort Dia2,  ushort Mes2,  int Anio2);

bool	JulianoBisiestoAnio( 
	int Anio);

#endif
