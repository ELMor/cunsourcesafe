
#ifndef __OBJETO_H__
#define __OBJETO_H__

#include "defgen.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

class CObj {							//Clase dummy para derivar todas
	private:
		char	*buffer;
	public:
		CObj()	{
			buffer=(char*)NULL;
		}
		CObj(char*s){
			buffer=strdup(s);
		}
		~CObj(){
			if(buffer) free(buffer);
		}
		char*	getBuf	(void){
			return buffer;
		}
		ulong	len(void){
			if(buffer)
				return strlen(buffer);
			else
				return 0;
		}
		void	ane(char *s){
			if(s){
				buffer=(char*)realloc(buffer,1+len()+strlen(s));
				strcat(buffer,s);
			}
		}
		friend bool operator==(const CObj&,const CObj&);
		friend bool operator!=(const CObj&,const CObj&);
		friend bool operator< (const CObj&,const CObj&);
		friend bool operator> (const CObj&,const CObj&);
};

inline bool operator==(const CObj& o1, const CObj& o2){
	if(o1.buffer && o2.buffer)
		return stricmp(o1.buffer,o2.buffer)==0;
	else
		return false;
}
inline bool operator!=(const CObj& o1, const CObj& o2){
	if(o1.buffer && o2.buffer)
		return stricmp(o1.buffer,o2.buffer)!=0;
	else
		return false;
}
inline bool operator<(const CObj& o1, const CObj& o2){
	if(o1.buffer && o2.buffer)
		return stricmp(o1.buffer,o2.buffer)<0;
	else
		return false;
}
inline bool operator>(const CObj& o1, const CObj& o2){
	if(o1.buffer && o2.buffer)
		return stricmp(o1.buffer,o2.buffer)<0;
	else
		return false;
}

#endif
