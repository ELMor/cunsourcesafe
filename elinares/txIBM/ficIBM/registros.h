
#ifndef __REGISTROS_H__
#define __REGISTROS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class CRegs {
	private:
void	carga	(void);
void	graba	(void);

	protected:
		char*	r_fname;
		long	tamreg;
		char*	fname;
		long	numreg;
		long*	regs;
		long	virt;		//Conjunto virtual (Archivo completo, tama�o de registro fijo).
		long	needsave;
	public:
void	addReg	(CRegs* regs, long  reg);
long	getReg	(long  reg);
		CRegs	(char* frn,char *fn);
		CRegs	(char* frn, char* fn, long tamreg, long readfull=0);
	   ~CRegs	();
};

#endif

