
#ifndef __TAPE_H_
#define __TAPE_H__

#include <stdio.h>
#include <stdarg.h>
#include "defgen.h"
#include "fmanip.h"
#include "xlat.h"

#define TAPE_VOL1	1
#define TAPE_TMA	2
#define TAPE_HDR1	3
#define TAPE_HDR2	4
#define TAPE_EOF1	5
#define TAPE_EOF2	6
#define TAPE_EOV1	7
#define TAPE_EOV2	8
#define TAPE_IVO1	9

#define fromFile	1
#define fromConBlk	2

class CBlk;

class CTReader {
private:
	int			tipo;
	char		*fname;
	FILE		*fp;
	CTReader	*inner;
	long		szCMSV;
	long		curpos;
	ulong		st_size;
	long		nblk;
	long		fixedtam;
	CBlk		*fixedblk;
	CBlk		**blk;
	long		OfSz2Blks(long off, long sz, long *cini, long *cfin);
public:
				CTReader(char *fname, long szcmsv);
				CTReader(CTReader *ctr, long TamReg, long sizeoftam);
				CTReader(CTReader *ctr);
				CTReader(FILE *fp2);
				CTReader();
				~CTReader();
	void		AddBlk( CBlk* blk );
	char*		getnom(void) { return fname; }
	long		Read(char* out, long offset, long size);
	long		ReadBlock( char *out, long numblk, long oinblk=0, long szinblk=-1 );
	long		readSize(void);	//Lee el tama�o de un bloque de tama�o variable
	void		Serialize( FILE *fp );
	long		tell(void)		{return curpos; };
	int			gettipo(void)	{return tipo; }
	CTReader	*getinner(void)	{return inner; }
	long		getTam(void);
	long		getnblk(void)	{return nblk; 	};
	long		tipoBlk (long nblk);
	void		fnameBlk(long nblk, char *fname );
	CBlk*		operator[](long i);
};

class CBlk {
private:
	long		size;
	long		offset;
public:
				CBlk(long os, long s);
	long		getsize(void)	         { return size; };
	long		getoffset(void)	         { return offset; };
	void		setparm(long os, long s) {size=s; offset=os;}
};

class CTFile {
private:
	char		*fname;
	CTReader	*tr;
	long		nhijos;
	CTFile		**hijos;
public:
				CTFile();
				CTFile(char *fname, CTReader *cblk);
				CTFile(FILE *fp2);
	CTFile		*AddHijo( CTFile *hijo );
	void		Parse(void);
	void		List(char *s,long level);
	void		Extract(void);
	void		Serialize( FILE *fp );
	long		Read(char* out, long offset, long size){
					return tr->Read(out,offset,size);
				}
	long		ReadBlock( char* out, long blknum ){
					return tr->ReadBlock(out,blknum);
				}
	long		getBlockSize(long blknum){
					return (*tr)[blknum]->getsize();
				}
	long		tell(void){
					return tr->tell();
				}
	CTFile*		operator[](long i){ 
					return i<nhijos ? hijos[i] : NULL ; 
				}	
	long		getTam(void){ 
					return tr->getTam(); 
				}
	long		getnblk(void){
					return tr->getnblk();
				}
	CBlk*		getBlock(long nblk){
					return  (*tr)[nblk];
				}
	CTReader	getReader(void){
					return tr;
				}
};
class CTape {
private:
	char		*fname;
	long		needsaveinfo;
	long		szCMSV;
	CTFile		*root;

	void		GenInfo(char *TapeDev);
	void		LoadInfo();
	void		SaveInfo();
	void		splitpath(char *f, ulong *pd, ulong *p);
	void		writeBlock(FILE *fp, short szb, char *buf);
	void		parseHDR2(char *buf, short &sz, long &nb);
	int			isEOV(char *buf);
public:
				CTape(char *fname,long szcmsv,char* TapeDev);
				CTape(FILE *fp2);
				~CTape();
	CTFile		*getFile(char *path);
	void		Serialize( FILE *fp );
	void		List(void);
	void		Extract(char* path)		{getFile(path)->Extract();}
	char		*getName(void)			{return fname;}
};

class CTapeDev {
private:
	int			filedescriptor;
	char		*iodevice;
	int			blocksize;
public:
				CTapeDev(char *device);
				~CTapeDev();
	int			setBlockSize(int tamano);
	int			getBlock(char *out);
	void		nextFile(void);
	void		rebobina(void);
};

#endif