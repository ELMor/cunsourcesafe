
#ifndef __TIPO_H__
#define __TIPO_H__

#include "dimens.h"

class CCur;

class CTip : public CObj {
	private:
		Tipos	tipo;
		char*	pictur;
		CDim*	dim;

	public:
		CTip()	{ 
			tipo=NODEF; 
		};
		CTip(Tipos c, CDim* d, char* pic=(char*)NULL){
			tipo=c;
			pictur=pic;
			dim=d;
		};
		void setDim( CObj* co ){
			dim=(CDim*)co;
		};
		Tipos	getTip	(void)	{ return tipo; }
		CDim*	getIns	(void)	{ return tipo!=NODEF ? dim : NULL; }
		/**
		 * atencion: devuelve el tama�o en bits
		 */
		long	getTam		(CCur* c=(CCur*)NULL)	{
			switch(tipo){
			case TCHAr: return (dim->getTam(c)*8);	break;
			case TBIN:  return (dim->getTam(c)+1);	break;
			case TDEC:	return (dim->getTam(c)+1)*4;break;
			case TBIT:	return (dim->getTam(c));	break;
			case TPIC:  return strlen(pictur)*8;break;
			}
			return 0;
		}
		void	dump	(char *s=(char*)NULL, FILE *fp=stdout);
};

#endif
