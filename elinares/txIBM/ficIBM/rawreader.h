
#ifndef __RAWREADER_H__
#define __RAWREADER_H__

#include "fichero.h"
#include "tape.h"

class RawReader {
private:
	char	*fname;
	CTape	*tape;
	FILE	*filep;
public:
		RawReader(char *f){
			fname=strdup(f);
			tape=NULL;
			if(!(filep=fopen(f,"rb"))==NULL){
				perror(fname);
				exit(-1);
			}
		};
		RawReader(CTape *t, char *f){
			fname=strdup(f);
			tape=t;
			filep=NULL;
		}
		~RawReader(){
			free(fname);
			if(filep) fclose(fp);
		}

}

#endif