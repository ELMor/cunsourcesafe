

#ifndef __TABLAS_H__
#define __TABLAS_H__

#include <stdlib.h>
#include <stdio.h>
#include "defgen.h"

//Tablas cargadas en memoria
class CTab	{
	private:
		//No se puede poner ningun campo antes que 'nombre', por el qsort
		uchar*	nombre;
		ulong	numreg;
		ulong	tamkey;
		reg*	regist;
	public:
		ulong	tconv; //Tipo de conversion que hay que realizar a la clave.
				CTab(uchar *nom, uchar *data);
				CTab( FILE *fp );
				~CTab();
		uchar*	getNom()		{ return nombre; }
		long	getNReg()		{ return numreg; }
		ulong	getTamKey()		{ return tamkey; }
		uchar*	getDes(uchar*key,int tdes=0);
		void	grabar(FILE *fp);
		void	Dump(FILE *fp);
};

//Lista de las tablas.
class CLisTab {
	private:
		ulong	numtab;
		CTab**	tablas;
	public:
		CLisTab();
		~CLisTab();
		void carga(char* fn);
		void graba(char *fn);
		void ins( CTab* c );
		void Dump( FILE *fp );
		CTab* getTab( long i )	{ return tablas[i]; }
		long getNTab(void)		{ return numtab;    }
		uchar*	getDes(char *ntab,uchar* key,int tdes=0);
		CTab*	getTab(char *ntab);
};

void CarTabl(char *fn);

#endif
