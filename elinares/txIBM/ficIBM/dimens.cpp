
#include "dimens.h"
#include "campo.h"
#include "valor.h"
#include "cursor.h"

extern CCam**				lisCam;
extern ulong				nlisCam;

void 
CDim::dump(char *s,FILE *fp){
	char aux[200];
	switch(tipdim){
	case 0: 
		sprintf(aux," "); break;
	case 1: 
		if(d2==0) 
			sprintf(aux,"(%d)",d1);
		else	  
			sprintf(aux,"(%d,%d)",d1,d2);
		break;
	case 2:
		sprintf(aux,"(dummy REFER (%s))",Refer->getVar()->getNom());
		break;
	case 3:
		sprintf(aux,"(%s NOT '%s' MAX %ld)",stopvaria,stopvalue,max);
		break;
	case 4:
		sprintf(aux,"(REST MAX %ld)",max);
		break;
	}
	if(s)
		strcpy(s,aux);
	else
		fprintf(fp,"%s",aux);
}

CDim::CDim( CCam *c, long m ){
	tipdim=2;
	Refer=c;
	cval=0;
	max=m;
	stopvalue=NULL;
}

CDim::CDim( char *c, char *stopval, long m){
	tipdim=3;
	Refer=NULL;
	cval=0;
	stopvaria=strdup(c);
	stopvalue=strdup(stopval);
	max=m;
}

CDim::CDim( char *rest, long m ){
	tipdim=4;
	Refer=NULL;
	cval=NULL;
	stopvaria=stopvalue=NULL;
	max=m;
}

long
CDim::getTam(CCur *pcur){
	char naux[512];
	long i,ret;
	switch( tipdim ){
	case 0: 
		return 0;  
		break;
	case 1: 
		return d1; 
		break;
	case 2:
		if( !cval ){
			Refer->getNomCom(naux,1);
			cval=new CVal(CVal::parse(pcur,naux));
		}
		if( !pcur )
			return 1;
		else{
			if( !cval->cursor ){
				cval->cursor=pcur;
				cval->proc();
			}
			ret=(long)(*cval);
			return ret ? ret : 1 ;
		}
		break;
	case 3:
		if( !cval ){
			Refer->getNomCom(naux,1);
			cval=new CVal(CVal::parse(pcur,naux));
		}
		if( !pcur )
			return 1;
		else{
			if( !cval->cursor ){
				cval->cursor=pcur;
				cval->proc();
			}
			for(i=0;i<1024;i++){
				if(!stricmp(stopvalue,(char*)( (*cval)[i][stopvaria] )))
					return i;
			}
			return -1;
		}
		break;
	case 4:
		if( pcur && pcur->getRegAct()>=0 && Refer )
			return pcur->tamReg()-Refer->getOff(pcur)/8;
		return max;
		break;
	}
	return -1;
}

CDim::~CDim(){
	if(cval) delete cval;
	if(stopvalue) free( stopvalue );
}

long
CDim::getD1(void){
	if( tipdim==2 || tipdim==3)
		return max;
	return d1;
}

