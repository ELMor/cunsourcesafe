
#include "fmanip.h"

static char Buffer[512];

void LeeLong(FILE* fp, long* lp){
	if(fread(lp,sizeof(long),1,fp)!=1){
		perror("LeeLong");
		exit(-1);
	}
}

void LeeLoAr(FILE* fp, long num, long* ar){
	if(fread(ar,sizeof(long),num,fp)!=(ulong)num){
		perror("LeeLoAr");
		exit(-1);
	}
}

void LeeStri(FILE* fp, char *str){
	long len;
	LeeLong(fp,&len);
	if(fread(str,1,len,fp)!=(ulong)len){
		perror("LeeStri");
		exit(-1);
	}
}

void EscLong(FILE* fp, long lp){
	if(fwrite(&lp,sizeof(long),1,fp)!=1){
		perror("EscLong");
		exit(-1);
	}
}

void EscLoAr(FILE* fp, long num, long* ar){
	if(fwrite(ar,sizeof(long),num,fp)!=(ulong)num){
		perror("EscLoAr");
		exit(-1);
	}
}

void EscStri(FILE* fp, char *str){
	long len=1+strlen(str);
	EscLong(fp,len);
	if(fwrite(str,1,len,fp)!=(ulong)len){
		perror("EscStri");
		exit(-1);
	}
}
