
#include "ficibm.h"

// Chequeo Antiguo
int main001(void){
	/**
	 * Carga de definiciones. Estas dos funcines siempre hay que llamarlas.
	 * en cualquier programa que utilice ficibm.lib
	 */
	Init("def0007.txt","tablas.bin");	//Archivo de definiciones y tablas.
	//---------------------------------------------------------------------
	//Crear un indice sobre FHISTOR. La llamada es:
	// NombreFichero,Tama�oRegistro,NombreIndice,DefinicionIndice
	//El ultimo argumento se puede omitir si el indice ya esta creado.
	//Archivos	: cuc01r (tama�o 451) Pacientes
	//			: cuc03e (tama�o 678) Casos
	//			: cuc04e (tama�o var) Movimientos
	CInd ind1(/*Nombre del archivo IBM*/	"cuc01r",							
			  /*Tama�o de registro*/		451,
			  /*Nombre del indice*/			"FHISTOR",
			  /*Campos del indice*/			"RMRC1.RM.FHISTOR"
			  );
	CInd ind2(/*Nombre del archivo IBM*/	"cuc03e",							
			  /*Tama�o de registro*/		678,
			  /*Nombre del indice*/			"CHISTOR-CNCASAC",			
			  /*Campos del indice (max 6)*/	"RMRC2.RC.CHISTOR,RMRC2.RC.CNCASAC"		
			  );
	//---------------------------------------------------------------------
	//Crear un Cursor sobre ese indice. Tambien se puede crear sobre un
	// CFic, pero el orden ser� el que se pas� de IBM.
	CCur cur1(&ind1);
	CCur cur2(&ind2);
	//---------------------------------------------------------------------
	//Definiciones de valores sobre ese cursor:
	CVal val1=CVal::parse(&cur1,"RMRC1.RM");
	CVal val2=CVal::parse(&cur2,"RMRC2.RC.CNCASAC");

	cur1.lee( 0 );
	do{
		printf("Hist(%06ld) : Nombre(%s)\n",
				(long) val1["FHISTOR"],
				(char*)val1["FNOMBRE"]);
		if( cur2.busca(RegPri,1, (long)val1["FHISTOR"]) ){
			printf("\tCasos:");
			do{
				printf("(%ld)",(long)val2);
			}while( cur2.busca(RegSig) );
			printf("\n");
		}
	}while( cur1.lee(RegSig) );

	return 0;
}