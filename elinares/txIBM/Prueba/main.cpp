//Este include es el unico necesario
#include "ficibm.h"
#define LON_COMMIT 100

void ImprimeTipo6(long& contador, CVal& val);
void ImprimeTipo7(long& contador, CVal& val);

int main(void){
	long contador=0;
	Init( getenv("DEF_PLI"),getenv("TAB_OL" ) );//Archivo de definiciones y tablas.
	CInd Indice("cuo24k.bin",0,"reg-clav-fvali","rprueb.fcclave.fcclav1.fctreg,"
		                                    "rprueb.fcclave.fcclav1.fpclav,"
									        "rprueb.fcfvali");
	CCur Cursor1(&Indice),Cursor2(&Indice);
	CVal c1=CVal::parse(&Cursor1,"RPrueb.FCClave.FCClav1.FPClav");
	CVal v1=CVal::parse(&Cursor2,"RInter"); //Tipo 6
	CVal v2=CVal::parse(&Cursor2,"RPrueb"); //Tipo 7
	//Registros de tipos 6
	Cursor1.busca(RegPri,1,"6");
	do{
		Cursor2.busca(RegUlt,2,"6",(char*)c1);
		ImprimeTipo6(contador,v1);
		Cursor1=Cursor2;
	}while( Cursor1.busca(RegSig,1) );
	//Idem tipo 7
	Cursor1.busca(RegPri,1,"7");
	do{
		Cursor2.busca(RegUlt,2,"7",(char*)c1);
		ImprimeTipo7(contador,v2);
		Cursor1=Cursor2;
	}while( Cursor1.busca(RegSig,1) );
	return 0;
}

//---------------FUNCIONES DE IMPRESION--------------------------------
void trunca(char *in,char *out){
	strcpy(out,in);
	char *aux=out+strlen(out)-1;
	while( *aux==' ')
		*(aux--)='\0';
}

void ImprimeTipo6(long& contador, CVal& val){
	char Buffer[1024];

	printf("INSERT INTO GC0100 (\n"
			"\tGC01FCTREG,GC01FPCLAV,GC01FCESREG,GC01FCFVALI,AD02CODDPTO,\n"
			"\tGC01FCDESCR,GC01FCDESCA,GC01FPGRUCO\n"
			"\t) VALUES (\n");
	printf("\t'%s',\n",(char*)val["FCCLAVE"]["FCCLAV1"]["FCTREG"]);
	printf("\t'%s',\n",(char*)val["FCCLAVE"]["FCCLAV1"]["FICLAV"]);
	printf("\t%ld ,\n",(long) val["FCESREG"]);
	printf("\tTO_DATE('%ld','YYYYMMDD') ,\n",(long) val["FCFVALI"]);
	printf("\t%s  ,\n",       val["FISREAL"].cod());
	trunca((char*)val["FCDESCR"],Buffer);
	printf("\t'%s',\n",Buffer);
	trunca((char*)val["FCDESCA"],Buffer);
	printf("\t'%s',\n",Buffer);
	printf("\t%ld  \n",(long) val["FIGRUCO"]);
	printf("\t);\n");
	if(++contador%LON_COMMIT==0)
		printf("COMMIT;\n");
}

void ImprimeTipo7(long& contador, CVal& val){
	char Buffer[1024];
	printf("INSERT INTO GC0100 (\n"
			"\tGC01FCTREG,GC01FPCLAV,GC01FCESREG,GC01FCFVALI,AD02CODDPTO,\n"
			"\tGC01FCDESCR,GC01FCDESCA,GC01FPGRUCO,GC01FPGRUFA,GC01FPDESCU\n"
			"\t) VALUES (\n");
	printf("\t'%s',\n",(char*)val["FCCLAVE"]["FCCLAV1"]["FCTREG"]);
	printf("\t'%s',\n",(char*)val["FCCLAVE"]["FCCLAV1"]["FPCLAV"]);
	printf("\t%ld ,\n",(long) val["FCESREG"]);
	printf("\tTO_DATE('%ld','YYYYMMDD') ,\n",(long) val["FCFVALI"]);
	printf("\t%s  ,\n",       val["FPSREAL"].cod());
	trunca((char*)val["FCDESCR"],Buffer);
	printf("\t'%s',\n",Buffer);
	trunca((char*)val["FCDESCA"],Buffer);
	printf("\t'%s',\n",Buffer);
	printf("\t%ld ,\n",(long) val["FPGRUCO"]);
	printf("\t%ld ,\n",(long) val["FPGRUFA"]);
	printf("\t'%s' \n",(char*)val["FPDESCU"]);
	printf("\t);\n");
	if(++contador%LON_COMMIT==0)
		printf("COMMIT;\n");
}
