
#include <sys/mtio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "defgen.h"
#include "ficibm.h"

int main(int argc, char *argv[]){
  unsigned long i,blksize;
  int fd;
  mtop mt_cmd;
  char *buf;
  long totread=0,totwrite=0,retfromwrite;

  if(argc!=3){
    puts("blk2tap tapefilename <cmsvSize+1 in KBytes>");
    return -1;
  }
  CTReader *cblk=new CTReader(argv[1],atol(argv[2]));

  fd=open("/dev/tape",O_WRONLY);
  printf("Open return %d\n",fd);
  if(fd==-1){
    perror("/dev/tape");
    return -1;
  }
  mt_cmd.mt_op=MTSETBLK;
  mt_cmd.mt_count=0;
  ioctl(fd,MTIOCTOP,&mt_cmd);
  buf=(char*)malloc(128*1024);

  for(i=0; i<cblk->getnblk(); i++){
    blksize=(*cblk)[i]->getsize();
    cblk->ReadBlock(buf,i);
    printf("%07ld %07ld %07ld ",i,blksize, retfromwrite=write(fd,buf,blksize));
    if(retfromwrite>=0){
      totread+=blksize;
      totwrite+=retfromwrite;
      printf("OK\n");
    }else{
      printf("Error %ld:%s\n",errno,strerror(errno));
    }
  }
  close(fd);
  free(buf);
  printf("TamRead=%07ld TamWrite=%07ld (Bytes)\n",totread,totwrite);
  return 0;
}

