
%{

//Includes Generales
#include <ctype.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "tablas.h"

//Fichero al que hay que hacer el parsing
extern FILE *fpExport;
extern CLisTab ltab;
//Tipo de los tokens
#define YYSTYPE char*

//Prototipos.
void yyerror(char*s);
int  yylex(void);

//Ofrecer mas informacion si hay error
#define YYERROR_VERBOSE

//Para depurar:
//#define duts(a) puts(a)
#define duts(a) 
//#define drintf(a) printf(a); printf(" ");
#define drintf(a)

%}



%token NUM 
%token NOM
%token DATA

%token LIST			"LIST"
%token MEMBER		"MEMBER"
%token SUBLIBRARY	"SUBLIBRARY"
%token DATE			"DATE"
%token TIME			"TIME"
%token RETURN		"RETURN"
%token CODE			"CODE"
%token OF			"OF"
%token IS			"IS"

%token_table

%%

program	: listtab
;

listtab : tabla								
		| listtab tabla
;
tabla	: cabecera DATA cola					{ ltab.ins( new CTab((uchar*)$1,(uchar*)$2) ); }
;
cabecera: LIST           NOM '.' NOM			
		  MEMBER '='     NOM '.' NOM
		  SUBLIBRARY '=' NOM '.' NOM
		  DATE ':'       NUM '-' NUM '-' NUM
		  TIME ':'       NUM ':' NUM			{ $$=$2; }
;

cola	: NOM RETURN CODE OF LIST IS NUM
;
%%

void skipblank(FILE *fp){
	char c=' ';
	while(c==' ' || c=='\t' || c=='\n')
		c=getc(fp);
	ungetc(c,fp);
}

int ctonib(char c){
	if(c>='A' && c<='F' )
		return c-'A'+10;
	if(c>='a' && c<='f' )
		return c-'a'+10;
	if(c>='0' && c<='9' )
		return c-'0';
	return -1;
}

int nibtoint(char c1, char c2){
	int ret1,ret2;
	ret1=ctonib(c1);
	ret2=ctonib(c2);
	ret1<<=4;
	return ret1+ret2;
}

static char Buffer[64*1024];

int yylex(){
	int c;
	while(1){
		skipblank(fpExport);
		c=getc(fpExport);
		if(c=='*'){
			while( c!='\n'){
				c=getc(fpExport);
				if(c==EOF)
					return 0;
			}
			continue;
		}
		break;
	}
	if(isdigit(c)){
		long valor;
		ungetc(c,fpExport);
		fscanf(fpExport,"%ld",&valor);
		sprintf(Buffer,"%ld",valor);
		yylval=strdup(Buffer);
		drintf(Buffer);
		return NUM;
	}
	if(isalpha(c)||c=='_'){
		int i=0;
		while( c!=EOF && (c=='_' || isalnum(c)) ){
			Buffer[i++]=c;
			c=getc(fpExport);
		}
		Buffer[i]=0;
		yylval=strdup(Buffer);
		ungetc(c,fpExport);
		for(i=0;yytname[i];i++)
			if(!strncmp(Buffer,yytname[i]+1,strlen(Buffer)) &&
				*(yytname[i]+1+strlen(Buffer))=='"' ){
				drintf(Buffer);
				return yytoknum[i];
			}
		drintf(Buffer);
		return NOM;
	}
	if( c=='-' ){
		c=getc(fpExport);
		if(c!='-'){
			ungetc(c,fpExport);
			return '-';
		}
		while( getc(fpExport)!='\n' )		//Hay un '------------'
			;
		int i=0,j=0,k=0;
		while( (c=getc(fpExport))!='L' ){
			if(c=='M'){						//Desechar 3 lineas
				char t[256];
				fgets(t,256,fpExport);
				fgets(t,256,fpExport);
				fgets(t,256,fpExport);
				c=getc(fpExport);
			}
			ungetc(c,fpExport);
			for(j=0;j<8;j++) getc(fpExport);	//Saltar los offsets
			while( (c=getc(fpExport))==' ' )
				;
			do{
				Buffer[i++]=nibtoint(c,getc(fpExport));
				while((c=getc(fpExport))==' ')
					;
			}while(c!='*');
			if(c=='*')
				while( getc(fpExport)!='\n')
					;
		}
		ungetc('L',fpExport);
		i-=2;
		yylval=(char*)malloc(sizeof(i)+i);
		memcpy(yylval,&i,sizeof(i));
		memcpy(yylval+sizeof(i),Buffer,i);
		return DATA;
	}
	if(c==EOF)
		return 0;
	return c;
}

void yyerror(char *s){
	printf("Error: %s\n",s);
}

