VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsProcess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public strProcess As String

Public cllCamposClaveEntrada As New Collection
Public cllFormatoClaveEntrada As New Collection
Public cllCamposClaveSalida As New Collection
Public cllFormatoClaveSalida As New Collection
Public intTimeOut As Integer

Public strInput As String
Public objInput As rdoResultset

Public strInput2 As String
Public objInput2 As rdoResultset
Public strInput3 As String
Public objInput3 As rdoResultset

Public strOutput As String
Public objOutput As rdoResultset

Public strOutput2 As String
Public objOutput2 As rdoResultset

Public strOutput3 As String
Public objOutput3 As rdoResultset

Public strObjetoError2 As String

Public strClaveEntrada As String
Public strClaveSalida As String
Public blnFinEntrada As Boolean
Public blnFinSalida As Boolean
Public blnHayError As Boolean

Public blnMustRead As Boolean
Public intCursorError As Integer
Dim strClaveMaxima As String
Dim objError As Object
Dim blnEntradaAbierta
Dim blnSalidaAbierta
Dim aniAnimate1 As New clsAnimate



Private Sub Class_Initialize()
  intTimeOut = 0
  blnMustRead = True
  intCursorError = 1

End Sub

'Llama a los metodos que crean el cursor de entrada
'y el/los cursores de salida. Despues (si debe), lee
'el primer registro de cada uno.

Public Sub emsInitProcess(ByVal objConnect As clsConnect)
  strClaveMaxima = "Z"
  strClaveEntrada = "0"
  strClaveSalida = "0"
  blnFinEntrada = False
  blnFinSalida = False
  blnHayError = False
  
  Set objError = objConnect.rdoError
  
  If Not blnHayError Then
     Call emsCreate(objConnect.rdoAppConnectEnt, True)
     Call emsCreate(objConnect.rdoAppConnectSal, False)
  End If
  
 'Para obtenr claves y situar cursores
  If Not blnHayError And blnMustRead Then
    Call emsReadInput(objInput, True)
'    Call emsRead(False, True)
  End If
End Sub

'M�todo que compara claves de entrada y salida llamando a
'eventUpdate con los parametros oportunos:
'true=A�adir
'false=Actualizar

Public Sub emsRunProcess(ByVal objConnect As clsConnect)
 ' On Error GoTo ErrorProceso
  Static Numregs As Integer
  If Not blnHayError Then
    frmCargaTextos.StatusBar2.Panels(1).Text = "Volcando datos del fichero de texto....."
    
    If stdBuscarFichero(App.Path, "FILECOPY.AVI") Then
      aniAnimate1.Create frmCargaTextos.hwnd, App.Path & "\FILECOPY.AVI", -5, 0, 500, 75
      aniAnimate1.AnimatePlay
    End If
    DoEvents
    
    Do While Not blnFinEntrada 'Or Not blnFinSalida
   '   If strClaveEntrada < strClaveSalida Then
        Call eventUpdate(Me, True)
        If Numregs > 500 Then
          objOutput.Close
          Call emsCreate(objConnect.rdoAppConnectSal, False)
          Numregs = 0
        End If
        Call emsReadInput(objInput, False)
        Numregs = Numregs + 1
   '   ElseIf strClaveEntrada = strClaveSalida Then
   '     Call eventUpdate(Me, False)
   '     Call emsRead(True, False)
   '     Call emsRead(False, False)
   '   Else
   '     Call emsRead(False, False)
   '   End If
        
    Loop
    aniAnimate1.AnimateStop
    aniAnimate1.Destroy

  End If
  Exit Sub
ErrorProceso: Select Case intCursorError
    Case 1
       With objOutput
       If .EditMode = rdEditInProgress Then
         .CancelUpdate
          Call emsProcesarError(gintEsErrorLeve, gstrEditar, gstrSalida)
       ElseIf .EditMode = rdEditAdd Then
         .CancelUpdate
          Call emsProcesarError(gintEsErrorLeve, gstrA�adir, gstrSalida)
       End If
       End With
    Case 2
       With objOutput2
       If .EditMode = rdEditInProgress Then
         .CancelUpdate
          Call emsProcesarError(gintEsErrorLeve, gstrEditar, gstrSalida)
       ElseIf .EditMode = rdEditAdd Then
         .CancelUpdate
          Call emsProcesarError(gintEsErrorLeve, gstrA�adir, gstrSalida)
       End If
       End With
    Case 3
       With objOutput3
       If .EditMode = rdEditInProgress Then
         .CancelUpdate
          Call emsProcesarError(gintEsErrorLeve, gstrEditar, gstrSalida)
       ElseIf .EditMode = rdEditAdd Then
         .CancelUpdate
          Call emsProcesarError(gintEsErrorLeve, gstrA�adir, gstrSalida)
       End If
       End With
  End Select
  Resume Next
End Sub

'M�todo que cierra los cursores y vacia las colecciones

Public Sub emsEndProcess()
   Call emsClose
   Call stdVaciarColeccion(cllCamposClaveEntrada)
   Call stdVaciarColeccion(cllCamposClaveSalida)
   Call stdVaciarColeccion(cllFormatoClaveEntrada)
   Call stdVaciarColeccion(cllFormatoClaveSalida)
End Sub

'M�todo que crea el cursor de entrada(blnEntrada=True)
'o el/los de salida(blnEntrada=False)

Public Sub emsCreate(ByVal rdoConnect As Object, _
                      ByVal blnEntrada As Boolean)
  Dim strFichero As String
 
  'On Error GoTo Errores
  If Not blnHayError Then
    If blnEntrada Then
      strFichero = gstrEntrada
      With rdoConnect
        .QueryTimeout = intTimeOut
        Set objInput = .OpenResultset(strInput, rdOpenKeyset, rdConcurReadOnly)
        
        '******** CAMBIAR *******
        'StatusBar2.Panels(2).Text = "Recuperando n� de registros a volcar... Espere un momento"
        'DoEvents

        'objInput.MoveLast
        'frmCargaTextos.prbprbBar1.Max = objInput.RowCount
        '***********************
      End With
    Else
      strFichero = gstrSalida
      With rdoConnect
        .QueryTimeout = intTimeOut
        Set objOutput = .OpenResultset(strOutput, rdOpenKeyset, rdConcurValues)
        If strInput2 <> "" Then
          Set objInput2 = .OpenResultset(strInput2, rdOpenStatic, rdConcurReadOnly)
        End If
        If strOutput2 <> "" Then
          Set objOutput2 = .OpenResultset(strOutput2, rdOpenKeyset, rdConcurValues)
        End If
        If strOutput3 <> "" Then
          Set objOutput3 = .OpenResultset(strOutput3, rdOpenKeyset, rdConcurValues)
        End If
      End With
      End If
    End If
    
  Exit Sub
Errores:
  blnHayError = True
  Call emsProcesarError(gintEsErrorGrave, gstrAbrir, strFichero)
End Sub

'M�todo para leer registros de los cursores de
'entrada o salida.Los parametros especifican qu�
'cursor se debe leer y si es o no el primer registro.

Public Sub emsRead(blnEntrada As Boolean, _
                   blnPrimero As Boolean)
  Dim strClave As String
  Dim objCursor As Object
  Dim strFichero As String
  Dim strClaveAnterior As String

  If blnEntrada Then
    strFichero = gstrEntrada
    Set objCursor = objInput
  Else
    strFichero = gstrSalida
    Set objCursor = objOutput
  End If
    
 ' On Error GoTo Errores
  With objCursor
    If blnEntrada Then
      If blnPrimero Then
        'If (.RowCount = -1) Or (.RowCount > 0) Then
          .MoveFirst
        'End If
      Else
        'If (.RowCount = -1) Or (.RowCount > 0) Then
          strClaveAnterior = strClaveEntrada
          .MoveNext
        'End If
      End If
      If .EOF Then
        blnFinEntrada = True
        strClaveEntrada = strClaveMaxima
      Else
        strClaveEntrada = emsCreateKey(True)  'M�todo que crea la clave
            While strClaveAnterior = strClaveEntrada
                .MoveNext
                strClaveEntrada = emsCreateKey(True)
            Wend
      End If
    Else
      If blnPrimero Then
        If (.RowCount = -1) Or (.RowCount > 0) Then
          .MoveFirst
        End If
      Else
        If (.RowCount = -1) Or (.RowCount > 0) Then
          .MoveNext
        End If
      End If
      If .EOF Then
        blnFinSalida = True
        strClaveSalida = strClaveMaxima
      Else
        strClaveSalida = emsCreateKey(False)  'M�todo que crea la clave
      End If
    End If
  End With
  Exit Sub
Errores:
  blnHayError = True
  Call emsProcesarError(gintEsErrorGrave, gstrLeer, strFichero)
End Sub

'M�todo que cierra los cursores

Private Sub emsClose()
  On Error Resume Next
  objInput.Close
  Set objInput = Nothing
  objInput2.Close
  Set objInput2 = Nothing
  objOutput.Close
  Set objOutput = Nothing
  objOutput2.Close
  Set objOutput2 = Nothing
  objOutput3.Close
  Set objOutput3 = Nothing
End Sub

'M�todo que lee s�lo la entrada y no crea claves

Public Sub emsReadInput(ByVal objCursor As rdoResultset, _
                        ByVal blnPrimero As Boolean)
  If Not blnHayError Then
    'On Error GoTo Errores
    With objCursor
      If blnPrimero Then
        'If (.RowCount = -1) Or (.RowCount > 0) Then
          .MoveFirst
        'End If
      Else
        'If (.RowCount = -1) Or (.RowCount > 0) Then
          .MoveNext
        'End If
      End If
      If .EOF Then
        blnFinEntrada = True
      End If
    End With
  End If
  Exit Sub
Errores:
  Call emsProcesarError(gintEsErrorGrave, gstrLeer, gstrEntrada)
End Sub

'M�todo que concatena los campos clave y crea
'un string strClave para la entrada y para la salida

Private Function emsCreateKey(blnEntrada As Boolean) As String
  Dim intIndex As Integer
  Dim strClave As String
  
  If blnEntrada Then
    For intIndex = 1 To cllCamposClaveEntrada.Count
      strClave = strClave & CStr(Format(objInput.rdoColumns(cllCamposClaveEntrada(intIndex)), cllFormatoClaveEntrada(intIndex)))
    Next
  Else
    For intIndex = 1 To cllCamposClaveSalida.Count
      strClave = strClave & CStr(Format(objOutput.rdoColumns(cllCamposClaveSalida(intIndex)), cllFormatoClaveSalida(intIndex)))
    Next
  End If
  emsCreateKey = strClave
End Function

'M�todo que inserta en la tabla de errores el error cometido

Public Function emsProcesarError(ByVal intTipo As Integer, _
                                 ByVal strMotivo As String, _
                                 ByVal strObjeto As String)
  Dim strRegistro As String
  Dim objColumn As rdoColumn
  Dim strODBCValue As String
  
  'On Error GoTo Errores
  With objError
    .AddNew
    
    .rdoColumns("fichero") = strProcess
    .rdoColumns("fecinser") = Date
    .rdoColumns("deserror") = strMotivo & " " & strObjeto
    .rdoColumns("nivel") = intTipo
    
    On Error Resume Next
    If intTipo = gintEsErrorLeve Then
      For Each objColumn In objInput.rdoColumns
        
        If IsNull(objColumn.Value) Then
            strODBCValue = String(objColumn.Size, " ")
        Else
            strODBCValue = objColumn.Value
        End If
        
        strRegistro = strRegistro & strODBCValue
      Next
    End If
    
   ' On Error GoTo Errores
    .rdoColumns("registro") = strRegistro
    
    .Update
  End With
  Exit Function
Errores:
  objError.CancelUpdate
End Function

Public Sub emsInitError(ByVal objConnect As clsConnect)

  Set objError = objConnect.rdoError

End Sub



Public Sub emsCreateSal(ByVal rdoConnect As Object, _
                      ByVal blnEntrada As Boolean)
  Dim strFichero As String
  Dim j As Long
 
  'On Error GoTo Errores
  If Not blnHayError Then
    If blnEntrada Then
      strFichero = gstrEntrada
      With rdoConnect
        '.QueryTimeout = intTimeOut
        Set objInput = .Query.OpenResultset(rdOpenStatic, rdConcurReadOnly)
      End With
    Else
      strFichero = gstrSalida
      With rdoConnect
       ' .QueryTimeout = intTimeOut
        Set objOutput = .Query.OpenResultset(rdOpenKeyset, rdConcurValues)
        If strInput2 <> "" Then
          Set objInput2 = .OpenResultset(strInput2, rdOpenStatic, rdConcurReadOnly)
        End If
        If strOutput2 <> "" Then
          Set objOutput2 = .OpenResultset(strOutput2, rdOpenKeyset, rdConcurValues)
        End If
        If strOutput3 <> "" Then
          Set objOutput3 = .OpenResultset(strOutput3, rdOpenKeyset, rdConcurValues)
        End If
      End With
      End If
    End If
    
  Exit Sub
Errores:
  blnHayError = True
  Call emsProcesarError(gintEsErrorGrave, gstrAbrir, strFichero)
End Sub

