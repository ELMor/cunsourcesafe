VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmVerSeleccion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Ver Selecci�n"
   ClientHeight    =   3735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8115
   ClipControls    =   0   'False
   Icon            =   "CI1032.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3735
   ScaleWidth      =   8115
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   2835
      Left            =   135
      TabIndex        =   2
      Top             =   150
      Width           =   7830
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   5
      AllowUpdate     =   0   'False
      SelectTypeCol   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   1588
      Columns(0).Caption=   "Solicitud"
      Columns(0).Name =   "Solicitud"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1508
      Columns(1).Caption=   "Petici�n"
      Columns(1).Name =   "Petici�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2275
      Columns(2).Caption=   "N�m.Act.Planif"
      Columns(2).Name =   "N�m.Act.Planif"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   7488
      Columns(3).Caption=   "Actuaci�n"
      Columns(3).Name =   "Actuaci�n"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   5583
      Columns(4).Caption=   "Departamento"
      Columns(4).Name =   "Departamento"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   13811
      _ExtentY        =   5001
      _StockProps     =   79
      Caption         =   "ACTUACIONES SELECCIONADAS"
      BackColor       =   -2147483637
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "&Borrar Selecci�n"
      Height          =   465
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   3150
      Width           =   2190
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "&Aceptar"
      Height          =   465
      Index           =   0
      Left            =   5775
      TabIndex        =   0
      Top             =   3150
      Width           =   2190
   End
End
Attribute VB_Name = "frmVerSeleccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim objPeticion As clsSelPeticion
  
  If intIndex = 0 Then
    Me.Hide
  End If
  If intIndex = 1 Then
    For Each objPeticion In objSolicitud.cllPeticiones
      objPeticion.RemoveActuaciones
    Next
    SSDBGrid1.RemoveAll
    blnBorrarSel = True
  End If
  Set objPeticion = Nothing
End Sub

Private Sub Form_Load()
  Dim objPeticion As New clsSelPeticion
  Dim objActuacion As New clsSelActuacion
  Dim strTexto As String
  
  SSDBGrid1.BackColorEven = objApp.objUserColor.lngReadOnly
  SSDBGrid1.BackColorOdd = objApp.objUserColor.lngReadOnly
  For Each objPeticion In objSolicitud.cllPeticiones
     For Each objActuacion In objPeticion.cllActuaciones
        strTexto = objPeticion.lngSolicitud & vbTab & objPeticion.lngPeticion & vbTab & objActuacion.lngNumActPlan & vbTab & objActuacion.lngCodigo & ". " & objActuacion.strDescripci�n & vbTab & GetTableColumn("SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & objActuacion.intDepartamento)("AD02DESDPTO")
        SSDBGrid1.AddItem strTexto
     Next
  Next
  Set objPeticion = Nothing
  Set objActuacion = Nothing

End Sub

