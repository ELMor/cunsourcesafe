Attribute VB_Name = "FunConver"
Option Explicit

    Public cllNombresProcesos As New Collection
    Public cllFicherosProcesos As New Collection
Public Function ConvertirSexo(strSexo As String) As Integer
  If UCase(strSexo) = "V" Then
    ConvertirSexo = 1
  End If
  If UCase(strSexo) = "H" Then
    ConvertirSexo = 2
  End If

End Function

 
Public Function stdBuscarError(ByVal rdoError1 As rdoResultset, _
                               ByVal strproc As String, _
                               ByVal objObjeto As clsProcess) As Boolean

'Dim strSQL As String
'Dim rdoError1 As rdoResultset

'strSQL = "SELECT FICHERO FROM EMS..ERRORES GROUP BY FICHERO"
'Set rdoError1 = objConnect.rdoAppConnectSal.OpenResultset(strSQL, rdOpenKeyset, rdConcurValues)

With objObjeto

    stdBuscarError = False
    .blnFinEntrada = False
   
   Call .emsReadInput(rdoError1, True)
   Do While (Not .blnFinEntrada) And (Not stdBuscarError)

        If Trim(rdoError1.rdoColumns("fichero")) = strproc Then
            stdBuscarError = True
        Else
            Call .emsReadInput(rdoError1, False)
        End If
  Loop
End With

End Function
Public Function stdBuscarProceso(objObjeto As clsProcess, strproc As String) As Boolean
With objObjeto
    stdBuscarProceso = False
    .blnFinEntrada = False
   Call .emsReadInput(.objOutput, True)
   Do While (Not .blnFinEntrada) And (Not stdBuscarProceso)

        If Trim(.objOutput.rdoColumns("proceso")) = strproc Then
            stdBuscarProceso = True
        Else
            Call .emsReadInput(.objOutput, False)
        End If
  Loop
End With
End Function
Public Function stdBuscarFichero(strPath As String, strFichero As String) As Boolean
    
    Dim strFile As String
    
    stdBuscarFichero = False
    strFile = ""
    
    strFile = Dir(strPath & "\" & strFichero)
    
    If UCase(strFile) = UCase(strFichero) Then
        stdBuscarFichero = True
    Else
        stdBuscarFichero = False
    End If
    
End Function


Public Sub stdVaciarColeccion(ByVal cllColeccionLlena As Collection)
  While cllColeccionLlena.Count > 0
    Call cllColeccionLlena.Remove(1)
  Wend
End Sub



Public Sub Reprocesando(strProceso As String, objConnect As clsConnect)

    
    Select Case strProceso
 
     Case "CargaPolizaAcunsa"
       Call CargaPolizasACUNSA(objConnect)
     Case "CargaCodigosPostales"
       Call CargaCodigosPostales(objConnect)

 
   End Select
End Sub




Public Function A�adirProceso(strName As String, strFile As String)

    Call cllNombresProcesos.Add(strName)
    Call cllFicherosProcesos.Add(strFile)
    
End Function
Public Function Apellidos(strNombreCompleto As String) As String
  Dim intPosicion As Integer
  
  intPosicion = InStr(strNombreCompleto, ",")
  If intPosicion > 0 Then
    Apellidos = Mid(strNombreCompleto, 1, intPosicion - 1)
  Else
    Apellidos = ""
  End If
End Function
Public Function Nombre(strNombreCompleto As String) As String
  Dim intPosicion As Integer
  
  intPosicion = InStr(strNombreCompleto, ",")
  Nombre = Mid(strNombreCompleto, intPosicion + 1)
End Function

Public Function NombreCiudad(strCiudad As String) As String
  Select Case strCiudad
    Case "ALAVA"
      NombreCiudad = "VITORIA"
    Case "VIZCAYA"
      NombreCiudad = "BILBAO"
    Case "GUIPUZCOA"
       NombreCiudad = "SAN SEBASTIAN"
    Case "NAVARRA"
       NombreCiudad = "PAMPLONA"
    Case Else
      NombreCiudad = strCiudad
  End Select
End Function
Public Function ConvertirFecha(strFecha As String) As Date
  Dim strA�o As String
  Dim strMes As String
  Dim strDia As String
  
  strA�o = Mid(strFecha, 1, 4)
  strMes = Mid(strFecha, 5, 2)
  strDia = Mid(strFecha, 7, 2)
  
  ConvertirFecha = CDate(strDia & "/" & strMes & "/" & strA�o)
End Function
Public Function ConvertirCorPago(strPago As String) As Integer
  If UCase(strPago) = "S" Then
    ConvertirCorPago = 1
  Else
    ConvertirCorPago = 2
  End If
End Function

