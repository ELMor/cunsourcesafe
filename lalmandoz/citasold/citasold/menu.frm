VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmCargaMunicipios 
   Caption         =   "Carga de Municipios y Localidades"
   ClientHeight    =   1560
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   7245
   Icon            =   "menu.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1560
   ScaleWidth      =   7245
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   1
      Top             =   1215
      Width           =   7245
      _ExtentX        =   12779
      _ExtentY        =   609
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Width           =   12779
            MinWidth        =   12779
            TextSave        =   ""
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ProgressBar prbPBar1 
      Height          =   465
      Left            =   480
      TabIndex        =   0
      Top             =   345
      Visible         =   0   'False
      Width           =   6210
      _ExtentX        =   10954
      _ExtentY        =   820
      _Version        =   327682
      Appearance      =   1
   End
   Begin MSComDlg.CommonDialog cdbCommonDialog1 
      Left            =   6840
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuImportar 
         Caption         =   "Importar Localidades"
      End
      Begin VB.Menu mnuLinea 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSalir 
         Caption         =   "Salir"
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuPath 
         Caption         =   "Establecer Path"
      End
   End
End
Attribute VB_Name = "frmCargaMunicipios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strDirectorio As String
Function ObtenerLocalidad(strFichero As String) As String
  Select Case UCase(strFichero)
    Case "CASTMAN"
      ObtenerComunidad = "CASTILLA LA MANCHA"
    Case "ANDALUCI"
      ObtenerComunidad = "ANDALUCIA"
    Case "MELILLA"
      ObtenerComunidad = "MELILLA"
    Case "CEUTA"
      ObtenerComunidad = "CEUTA"
    Case "CANARIAS"
      ObtenerComunidad = "CANARIAS"
    Case "GALICIA"
      ObtenerComunidad = "GALICIA"
    Case "CATALU�A"
      ObtenerComunidad = "CATALAU�A"
    Case "EXTREMA"
      ObtenerComunidad = "EXTREMADURA"
    Case "COVALEN"
      ObtenerComunidad = "VALENCIA"
    Case "CASTLEON"
      ObtenerComunidad = "CASTILLA LEON"
    Case "PAISVAS"
      ObtenerComunidad = "EUSKADI"
    Case "RIOJA"
      ObtenerComunidad = "RIOJA"
    Case "NAVARRA"
      ObtenerComunidad = "NAVARRA"
    Case "MURCIA"
      ObtenerComunidad = "MURCIA"
    Case "MADRID"
      ObtenerComunidad = "MADRID"
    Case "CANTABRI"
      ObtenerComunidad = "CANTABRIA"
    Case "BALEARES"
      ObtenerComunidad = "BALEARES"
    Case "ASTURIAS"
      ObtenerComunidad = "ASTURIAS"
    Case "ARAGON"
      ObtenerComunidad = "ARAGON"
    Case Else
      ObtenerComunidad = ""
  End Select
End Function
'Funci�n que devuelve el Path del fichero seleccionado mediante
'la common dialog box
'Se le pasa como par�metro de entrada el camino entero del fichero
'(la propiedad filename del common dialog box)
Function PathFichero(strFileName As String) As String
  On Error Resume Next
  Dim intContador As Integer

  For intContador = Len(strFileName) To 1 Step -1
    If Mid(strFileName, intContador, 1) = "\" Then
      Exit For
    End If
  Next

  PathFichero = Mid(strFileName, 1, intContador - 1)

End Function

'Funci�n que devuelve el Nombre del fichero seleccionado mediante
'la common dialog box
'Se le pasa como par�metro de entrada el camino entero del fichero
'(la propiedad filename del common dialog box)

Function NombreFichero(strFileName As String) As String
  On Error Resume Next
  Dim intContador As Integer

  For intContador = Len(strFileName) To 1 Step -1
    If Mid(strFileName, intContador, 1) = "\" Then
      Exit For
    End If
  Next

  NombreFichero = Mid(strFileName, intContador + 1, Len(strFileName) - intContador + 1)

End Function

Private Sub Form_Load()
'  Dim rc As Long, def As String * 20, cadena As String * 20
 ' cadena = String(255, " ")
  
 ' rc = GetPrivateProfileString("prueba.txt", "Format", def, cadena, Len(cadena) - 1, "c:\i�aki\schema.ini")
 ' cadena = Left(cadena, rc)
 ' cadena = "Delimited(;)"
 ' rc = WritePrivateProfileString("prueba.txt", "Format", cadena, "c:\i�aki\schema.ini")
End Sub

Private Sub mnuImportar_Click()
  Dim i As Integer, intNumFich As Integer, intRespuesta As Integer
  Dim Tabla As Recordset
  Dim strError As String
  Dim intErrors As Integer
  Dim bdPrueba As Database, bdOracle As rdoConnection
  Dim TablaOracle As rdoResultset
  Dim strConnect As String
  Dim strPath As String
  Dim strNombreTabla As String
  Dim intBDCont As Integer
  Dim rdoEnv As rdoEnvironment
  Dim vntProv As Variant
  Dim vntMunicipio As Variant
  Dim rdoMunicipio As rdoResultset
  Dim rdoLocalidad As rdoResultset
  Dim strDuplicado As String
  Dim Q As rdoQuery
  Dim strSql As String
  
  Static intCont As Integer
  On Error GoTo Error:
  cdbCommonDialog1.DialogTitle = "Seleccione el directorio donde se encuentran los ficheros"
  cdbCommonDialog1.ShowOpen
  strDirectorio = PathFichero(cdbCommonDialog1.filename)

  strConnect = "ODBC;DATABASE=Desarrollo_nt;UID=cundba;PWD=cundba;DSN=Oracle7 Tables"
  StatusBar1.Panels(1).Text = "Abriendo conexi�n con base de datos Oracle"
  DoEvents
  Set rdoEnv = rdoEnvironments(0)
  Set bdOracle = rdoEnv.OpenConnection("", _
    rdDriverNoPrompt, False, strConnect)
 ' Set Q = bdOracle.CreateQuery("", "select * from ci1700 WHERE CI17CODMUNICIP=0")
 ' Q.MaxRows = 1
  Set rdoMunicipio = bdOracle.OpenResultset("select * from ci1700 WHERE CI17CODMUNICIP=0", rdOpenKeyset, rdConcurRowVer)
 ' Set Q = bdOracle.CreateQuery("", "select * from ci1600 WHERE CI17CODMUNICIP=0")
 ' Q.MaxRows = 1
  Set rdoLocalidad = bdOracle.OpenResultset("select * from ci1600 WHERE CI17CODMUNICIP=0", rdOpenKeyset, rdConcurRowVer)


  
  StatusBar1.Panels(1).Text = "Abriendo conexi�n con base de datos dBase"
  strPath = strDirectorio ' "c:\projects\municip"
  strConnect = "dBASE IV"
  
  Set bdPrueba = OpenDatabase(strPath, False, False, strConnect)
  
  For intBDCont = 0 To bdPrueba.TableDefs.Count - 1
    strNombreTabla = bdPrueba.TableDefs(intBDCont).Name
   
   ' Ojo!!!!!
    strSql = "select Prov,Municipio,ES,Nombre from  " & strNombreTabla & " where nuc='00' AND EC='00'"
    Set Tabla = bdPrueba.OpenRecordset(strSql, dbOpenSnapshot)
    Tabla.MoveLast
    prbPBar1.Min = 0
    prbPBar1.Value = 0
    prbPBar1.Max = Tabla.RecordCount
    Label1(1) = Tabla.RecordCount
    Label1(0).Visible = True
    Label1(1).Visible = True
    Label1(2).Visible = True
    prbPBar1.Visible = True
    
    StatusBar1.Panels(1).Text = "Volcando Localidades de " & ObtenerLocalidad(strNombreTabla)
    Tabla.MoveFirst
   
 Do While Not Tabla.EOF
     vntProv = Tabla("Prov")
     While vntProv = Tabla("Prov")
       vntMunicipio = Tabla("Municipio")
       rdoMunicipio.AddNew
       rdoMunicipio(0) = vntProv
       rdoMunicipio(1) = vntMunicipio
       rdoMunicipio(2) = Tabla("Nombre")
       rdoMunicipio.Update
       While vntMunicipio = Tabla("Municipio")
         If strDuplicado <> Trim(Tabla("Nombre")) Then
           rdoLocalidad.AddNew
           rdoLocalidad(0) = vntProv
           rdoLocalidad(1) = vntMunicipio
           rdoLocalidad(2) = Tabla("ES")
           rdoLocalidad(3) = Tabla("Nombre")
           rdoLocalidad.Update
           strDuplicado = Tabla("Nombre")
         End If
         Tabla.MoveNext
         If Tabla.EOF Then
           Exit Do
         End If
         prbPBar1.Value = prbPBar1.Value + 1
         intCont = intCont + 1
         DoEvents
       
       Wend
      If intCont > 100 Then
        rdoMunicipio.Close
        rdoLocalidad.Close
        Set rdoMunicipio = bdOracle.OpenResultset("select * from ci1700 WHERE CI17CODMUNICIP=0", rdOpenKeyset, rdConcurRowVer)
        Set rdoLocalidad = bdOracle.OpenResultset("select * from ci1600 WHERE CI17CODMUNICIP=0", rdOpenKeyset, rdConcurRowVer)
        intCont = 0
      End If
     
     Wend
       
   Loop
   Label1(0).Visible = False
   Label1(1).Visible = False
   Label1(2).Visible = False
   prbPBar1.Visible = False
   StatusBar1.Panels(1).Text = ""
 Next intBDCont
  MsgBox "Se ha terminado el volcado de Municipios y Localidades" & vbCr & _
         "Se han producido " & intErrors & " error/es."
 
Exit Sub
Error:
  intErrors = intErrors + 1
  intNumFich = FreeFile
  Open PathFichero & Format(Now, "ddmmyy") & ".log" For Append As intNumFich
  strError = "Reg n�" & prbPBar1.Value + 1 & " " & Err.Number & " " & _
            Err.Description
  Print #intNumFich, strError
  Close #intNumFich
  intRespuesta = MsgBox("Se ha producido el siguiente error en el " & strError & Chr$(13) & "�Desea continuar con el volcado?", vbCritical + vbYesNo, "Citas. Carga de Personal")
  If intRespuesta = vbYes Then
    Resume Next
  Else
    Label1(0).Visible = False
    Label1(1).Visible = False
    prbPBar1.Visible = False
    StatusBar1.Panels(1).Text = ""
    Exit Sub
  End If
  StatusBar1.Panels(1).Text = ""
End Sub

Private Sub mnuPath_Click()
  cdbCommonDialog1.DialogTitle = "Seleccione el directorio donde se encuentran los ficheros"
  cdbCommonDialog1.ShowOpen
  strDirectorio = PathFichero(cdbCommonDialog1.filename)
End Sub

Private Sub mnuSalir_Click()
  Unload Me
End Sub
