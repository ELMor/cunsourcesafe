VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsSelPeticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public lngSolicitud     As Long
Public lngPeticion      As Long
Public lngPersona       As Long
'Public vntNombre        As Variant
Public cllActuaciones   As New Collection

Public Function RemoveActuaciones(Optional lngActuacion As Long = -1)
  Dim objActuacion        As New clsSelActuacion
  Dim blnBorrarTodas      As Boolean
  blnBorrarTodas = IIf(lngActuacion = -1, True, False)
  For Each objActuacion In Me.cllActuaciones
    If blnBorrarTodas Then
      lngActuacion = objActuacion.lngNumActPlan
    End If
    If objActuacion.lngNumActPlan = lngActuacion Or lngActuacion = -1 Then
      Call Me.cllActuaciones.Remove(Str(lngActuacion))
    End If
  Next
End Function

Public Function AddNewActuacion(Optional lngActuacion As Long = 0) As clsSelActuacion
  Dim objActuacion     As New clsSelActuacion
  
  objActuacion.lngNumActPlan = lngActuacion
  Call Me.cllActuaciones.Add(objActuacion, Str(lngActuacion))
  Set AddNewActuacion = Me.cllActuaciones(Str(lngActuacion))
  Set objActuacion = Nothing
End Function


Public Function GetActuacion(ByVal lngActuacion As Long) As clsSelActuacion

  Dim objActuacion        As New clsSelActuacion
  Dim blnExistActuacion   As Boolean

  blnExistActuacion = False
  For Each objActuacion In Me.cllActuaciones
    If objActuacion.lngNumActPlan = lngActuacion Then
      blnExistActuacion = True
      Exit For
    End If
  Next
  If blnExistActuacion Then
    Set GetActuacion = objActuacion
  Else
    Set GetActuacion = Nothing
  End If
  Set objActuacion = Nothing

End Function



