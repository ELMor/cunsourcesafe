VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmDelay 
   ClientHeight    =   1125
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   4530
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   1125
   ScaleWidth      =   4530
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Interval        =   50
      Left            =   4125
      Top             =   105
   End
   Begin ComctlLib.ProgressBar PBar1 
      Height          =   210
      Left            =   960
      TabIndex        =   1
      Top             =   780
      Width           =   2625
      _ExtentX        =   4630
      _ExtentY        =   370
      _Version        =   327682
      Appearance      =   1
      Max             =   28
   End
   Begin VB.Label Label1 
      Caption         =   "Procesando Datos, espere un momento, por favor ..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   345
      TabIndex        =   0
      Top             =   120
      Width           =   3915
   End
End
Attribute VB_Name = "frmDelay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intPos As Integer

Private Sub Timer1_Timer()
  PBar1.Value = PBar1.Value + 1
  DoEvents
  If PBar1.Value = 27 Then PBar1.Value = 0
End Sub
