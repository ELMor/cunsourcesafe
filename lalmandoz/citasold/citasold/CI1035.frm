VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form frmDocAutorizacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Mantenimiento de Documentos Autorizaci�n"
   ClientHeight    =   7560
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   10065
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "CI1035.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   10065
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   10065
      _ExtentX        =   17754
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Documentos Autorizaci�n"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6705
      Index           =   0
      Left            =   90
      TabIndex        =   6
      Top             =   480
      Width           =   9870
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   6
         Left            =   3930
         Locked          =   -1  'True
         TabIndex        =   18
         Tag             =   "Nombre|Nombre Entidad"
         Top             =   720
         Width           =   3615
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   40102
         Index           =   4
         Left            =   225
         Locked          =   -1  'True
         TabIndex        =   16
         Tag             =   "Descripci�n Tipo Econ�mico"
         Top             =   720
         Width           =   3435
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   5295
         Index           =   0
         Left            =   195
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   1305
         Width           =   9495
         _ExtentX        =   16748
         _ExtentY        =   9340
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1035.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraFrame1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraFrame1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "CommonDialog1"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1035.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin MSComDlg.CommonDialog CommonDialog1 
            Left            =   3660
            Top             =   3750
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   327681
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI32CODTIPECON"
            Height          =   330
            HelpContextID   =   40101
            Index           =   2
            Left            =   330
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "C�digo Tipo Econ�mico"
            Text            =   "C�d. TipE"
            Top             =   3180
            Visible         =   0   'False
            Width           =   810
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI13CODENTIDAD"
            Height          =   330
            HelpContextID   =   40101
            Index           =   3
            Left            =   1365
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "C�digo Entidad"
            Text            =   "C�d. Ent"
            Top             =   3180
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Imagen"
            Height          =   5100
            Index           =   2
            Left            =   4545
            TabIndex        =   12
            Top             =   60
            Width           =   4425
            Begin VB.CommandButton cmdCommand1 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   0
               Left            =   3930
               TabIndex        =   3
               Top             =   4635
               Width           =   345
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI11IMAGDOC"
               Height          =   330
               HelpContextID   =   30104
               Index           =   5
               Left            =   195
               TabIndex        =   2
               Tag             =   "Path Imagen"
               Top             =   4635
               Width           =   3650
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Path Imagen"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   195
               TabIndex        =   15
               Top             =   4380
               Width           =   1080
            End
            Begin VB.Image imgImage1 
               BorderStyle     =   1  'Fixed Single
               Height          =   3915
               Left            =   555
               Stretch         =   -1  'True
               Top             =   330
               Width           =   3450
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Documento"
            Height          =   2205
            Index           =   1
            Left            =   180
            TabIndex        =   9
            Top             =   75
            Width           =   4200
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "CI11CODDOCUMEN"
               Height          =   330
               HelpContextID   =   30101
               Index           =   0
               Left            =   270
               TabIndex        =   0
               TabStop         =   0   'False
               Tag             =   "C�digo|C�digo Documento"
               Top             =   555
               Width           =   1125
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFF00&
               DataField       =   "CI11DESDOCUMEN"
               Height          =   660
               HelpContextID   =   30104
               Index           =   1
               Left            =   285
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   1
               Tag             =   "Descripci�n|Descripci�n Documento"
               Top             =   1245
               Width           =   3650
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   270
               TabIndex        =   11
               Top             =   330
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   270
               TabIndex        =   10
               Top             =   1020
               Width           =   1020
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4890
            Index           =   0
            Left            =   -74835
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   210
            Width           =   8865
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15637
            _ExtentY        =   8625
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         DataField       =   "CI09FECENTVIGE"
         Height          =   330
         Index           =   0
         Left            =   7725
         TabIndex        =   20
         Tag             =   "Fecha Inicio Vigencia"
         Top             =   705
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BevelColorFace  =   12632256
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         AllowEdit       =   0   'False
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   7725
         TabIndex        =   21
         Top             =   495
         Width           =   1065
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Entidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   3900
         TabIndex        =   19
         Top             =   495
         Width           =   660
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Ec�nomico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   195
         TabIndex        =   17
         Top             =   495
         Width           =   1380
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   7275
      Width           =   10065
      _ExtentX        =   17754
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDocAutorizacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdCommand1_Click(Index As Integer)
  On Error Resume Next
  CommonDialog1.ShowOpen
  If CommonDialog1.filename <> "" Then
    txtText1(5).Text = CommonDialog1.filename
    imgImage1.Picture = LoadPicture(CommonDialog1.filename)
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  'Introducir la documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Mantenimiento de Documentos Autorizaci�n"
    .cwDAT = "4-12-97"
    .cwAUT = "I�aki Gabiola"
    .cwDES = "Esta ventana permite mantener los Documentos Autorizaci�n de la CUN"
    .cwUPD = "4-12-97 - I�aki Gabiola - Creaci�n del m�dulo"
    .cwEVT = ""
  End With
   ' Definici�n de la Tabla
  With objDetailInfo
    .strName = "Documentos"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("DATABASE")
    .strTable = "CI1100"
   
    .strWhere = "CI32CODTIPECON=" & objGen.ValueToSQL(frmTipoEcon.txtText1(1), cwString) _
              & " AND CI13CODENTIDAD=" & objGen.ValueToSQL(frmTipoEcon.txtText1(0), cwString) _
              & " AND CI09FECENTVIGE=" & objGen.ValueToSQL(frmTipoEcon.objWinInfo.objWinActiveForm.rdoCursor(2).Value, cwDate)
   
    .blnAskPrimary = False

    ' Definici�n del orden de clasificaci�n
    Call .FormAddOrderField("CI11DESDOCUMEN", cwAscending)
    
    ' Definici�n del impreso
    'Call .objPrinter.Add("CI0003", "Listado 1 de Conciertos Econ�micos")
       
    '.blnHasMaint = True
    
    ' Definici�n de los campos que se a�aden como filtros
    strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Concierto")
    'Call .FormAddFilterWhere(strKey, "CI43CODCONCECO", "C�digo", cwString)
    'Call .FormAddFilterWhere(strKey, "CI43DESCONCECO", "Descripci�n", cwString)
    
    ' Definici�n del orden de los campos filtro
    'Call .FormAddFilterOrder(strKey, "CI43CODCONCECO", "C�digo")
    'Call .FormAddFilterOrder(strKey, "CI43DESCONCECO", "Descripci�n")
    
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)

    ' Definici�n de los objetos del form en b�squedas
    .CtrlGetInfo(txtText1(1)).blnInFind = True
  
  ' Propiedades del campo clave calendario para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(0)).blnValidate = False
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    .CtrlGetInfo(txtText1(2)).vntDefaultValue = frmTipoEcon.txtText1(1)
    .CtrlGetInfo(txtText1(3)).vntDefaultValue = frmTipoEcon.txtText1(0)
    .CtrlGetInfo(dtcDateCombo1(0)).vntDefaultValue = frmTipoEcon.objWinInfo.objWinActiveForm.rdoCursor(2).Value
    .CtrlGetInfo(txtText1(4)).blnNegotiated = False
    .CtrlGetInfo(txtText1(6)).blnNegotiated = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
  txtText1(4) = frmTipoEcon.txtText1(2)
  txtText1(6) = frmTipoEcon.txtText1(3)
  txtText1(4).BackColor = objApp.objUserColor.lngReadOnly
  txtText1(6).BackColor = objApp.objUserColor.lngReadOnly
  dtcDateCombo1(0).Date = frmTipoEcon.objWinInfo.objWinActiveForm.rdoCursor(2).Value
  dtcDateCombo1(0).BackColor = objApp.objUserColor.lngReadOnly
  dtcDateCombo1(0).Enabled = False
  CommonDialog1.DialogTitle = "Seleccione la imagen deseada"
  CommonDialog1.Filter = "Imagenes (*.bmp;*.ico;*.wmf)|*.bmp;*.ico,*.wmf"
  objWinInfo.DataMoveFirst
  Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  imgImage1.Picture = LoadPicture("")
  dtcDateCombo1(0).Enabled = False
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  On Error GoTo Error
  If txtText1(5) <> "" Then
    If Dir(txtText1(5).Text) <> "" Then
      imgImage1.Picture = LoadPicture(txtText1(5).Text)
    Else
      imgImage1.Picture = LoadPicture("")
    End If
  Else
    imgImage1.Picture = LoadPicture("")
  End If
  dtcDateCombo1(0).Enabled = False
  Exit Sub
Error:
 imgImage1.Picture = LoadPicture("")
 Resume Next
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  If blnError = True Then
    Call objApp.rdoConnect.RollbackTrans
  Else
    Call objApp.rdoConnect.CommitTrans
  End If

End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim strSql As String
  
  If strFormName = "Documentos" Then
    
    ' Empieza transacci�n
    Call objApp.rdoConnect.BeginTrans
    With objWinInfo
      If .intWinStatus = cwModeSingleAddRest Then
      ' Abrimos cursor

        strSql = "select MAX(CI11CODDOCUMEN) from CI1100 WHERE " _
              & " CI32CODTIPECON=" & objGen.ValueToSQL(frmTipoEcon.txtText1(1), cwString) _
              & " AND CI13CODENTIDAD=" & objGen.ValueToSQL(frmTipoEcon.txtText1(0), cwString) _
              & " AND CI09FECENTVIGE=" & objGen.ValueToSQL(frmTipoEcon.objWinInfo.objWinActiveForm.rdoCursor(2).Value, cwDate)
        vntNuevoCod = GetNewCode(strSql)
        Call .CtrlStabilize(txtText1(0), vntNuevoCod)
      
      
      End If
    End With
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
' Definici�n del tratamiento del impreso

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "Concier" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
  If objWinInfo.intWinStatus <> cwModeSingleEmpty Then
    grdDBGrid1(0).Columns(3).width = 6000
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

End Sub


