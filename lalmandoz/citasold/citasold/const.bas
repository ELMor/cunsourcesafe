Attribute VB_Name = "Constantes"
Option Explicit

' constante para el path de datos txt



' constantes para tipos de errores
Public Const gintEsErrorGrave As Integer = 9
Public Const gintEsErrorLeve As Integer = 5

' constantes para motivos de error
Public Const gstrAbrir As String = "Abrir"
Public Const gstrLeer As String = "Leer"
Public Const gstrAņadir As String = "Insertar"
Public Const gstrEditar As String = "Actualizar"
Public Const gstrEntrada As String = "Fichero"
Public Const gstrSalida As String = "Tabla"
Public Const gstrStored As String = "Stored Procedure"

' constantes para describir las tablas que se cargan en cada proceso

Public Const ACUNSA As Integer = 56
