VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{4D2E11F7-A12E-11D1-9D0F-00C04FA619D6}#1.0#0"; "idperson.ocx"
Begin VB.Form frmPeticion3 
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CITAS. Peticiones de Citas"
   ClientHeight    =   8340
   ClientLeft      =   2550
   ClientTop       =   615
   ClientWidth     =   11910
   ControlBox      =   0   'False
   Icon            =   "CI1025.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Height          =   397
      Left            =   2310
      TabIndex        =   14
      Top             =   30
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   688
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
   End
   Begin ComctlLib.Toolbar tlbToolbarGrupo 
      Height          =   397
      Left            =   30
      TabIndex        =   13
      Top             =   30
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   688
      Appearance      =   1
      _Version        =   327682
   End
   Begin VB.CommandButton cmdDetalle 
      Caption         =   "De&talle"
      Height          =   375
      Index           =   0
      Left            =   10200
      TabIndex        =   20
      Top             =   6945
      Width           =   1575
   End
   Begin VB.CommandButton cmdFases 
      Caption         =   "Fa&ses/Recursos"
      Height          =   375
      Index           =   0
      Left            =   10200
      TabIndex        =   18
      Top             =   5445
      Width           =   1575
   End
   Begin VB.CommandButton cmdVerSolicitud 
      Caption         =   "Ver S&olicitud"
      Height          =   375
      Index           =   0
      Left            =   10200
      TabIndex        =   19
      Top             =   6465
      Width           =   1575
   End
   Begin VB.CommandButton cmdPruebas 
      Caption         =   "Pedir &Actuaciones"
      Height          =   375
      Left            =   10200
      TabIndex        =   16
      Top             =   4485
      Width           =   1575
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones Planificadas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3480
      Index           =   1
      Left            =   100
      TabIndex        =   29
      Top             =   4320
      WhatsThisHelpID =   10039
      Width           =   9870
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2910
         Index           =   1
         Left            =   180
         TabIndex        =   26
         Top             =   465
         Width           =   9615
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   16960
         _ExtentY        =   5133
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdVerSeleccion 
      Caption         =   "Ver Se&lecci�n"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   10200
      TabIndex        =   21
      Top             =   7425
      Width           =   1575
   End
   Begin VB.CommandButton cmdAgenda 
      Caption         =   "&Concertar Citas"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   10200
      TabIndex        =   17
      Top             =   4965
      Width           =   1575
   End
   Begin VB.TextBox txtPeticion 
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1785
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   615
      Visible         =   0   'False
      WhatsThisHelpID =   10041
      Width           =   375
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Peticiones"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3720
      Index           =   0
      Left            =   100
      TabIndex        =   24
      Top             =   555
      Width           =   11670
      Begin TabDlg.SSTab tabTab1 
         Height          =   3135
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   465
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   5530
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1025.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "tabTab1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtcDateCombo1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1025.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR09NUMPETICION"
            Height          =   330
            HelpContextID   =   40101
            Index           =   1
            Left            =   255
            Locked          =   -1  'True
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "Petici�n|N�mero de Petici�n"
            Top             =   360
            Width           =   1755
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR09NUMGRUPO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   2175
            Locked          =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Grupo|N�mero de Grupo"
            Top             =   360
            Width           =   1875
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2850
            Index           =   0
            Left            =   -74880
            TabIndex        =   27
            Top             =   120
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   5027
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "Pr09FecPeticion"
            Height          =   330
            Index           =   0
            Left            =   8970
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Fecha Petici�n"
            Top             =   240
            Visible         =   0   'False
            Width           =   1815
            _Version        =   65537
            _ExtentX        =   3201
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   2055
            HelpContextID   =   90001
            Index           =   1
            Left            =   255
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   840
            Width           =   10635
            _ExtentX        =   18759
            _ExtentY        =   3625
            _Version        =   327681
            Style           =   1
            Tabs            =   5
            TabsPerRow      =   5
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "&Persona/Paciente"
            TabPicture(0)   =   "CI1025.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "IdPersona1"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Solicita&nte"
            TabPicture(1)   =   "CI1025.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(10)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "txtText1(9)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "txtText1(5)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "txtText1(3)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "txtText1(2)"
            Tab(1).Control(5)=   "cboSSDBCombo1(0)"
            Tab(1).Control(6)=   "lblLabel1(2)"
            Tab(1).Control(7)=   "lblLabel1(1)"
            Tab(1).ControlCount=   8
            TabCaption(2)   =   "O&bservaciones"
            TabPicture(2)   =   "CI1025.frx":007C
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(6)"
            Tab(2).ControlCount=   1
            TabCaption(3)   =   "&Indicaciones"
            TabPicture(3)   =   "CI1025.frx":0098
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(7)"
            Tab(3).ControlCount=   1
            TabCaption(4)   =   "Persona &Contacto"
            TabPicture(4)   =   "CI1025.frx":00B4
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "txtText1(8)"
            Tab(4).Control(1)=   "txtText1(4)"
            Tab(4).Control(2)=   "lblLabel1(4)"
            Tab(4).Control(3)=   "lblLabel1(3)"
            Tab(4).ControlCount=   4
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   40101
               Index           =   10
               Left            =   -67395
               Locked          =   -1  'True
               TabIndex        =   12
               TabStop         =   0   'False
               Tag             =   "Usuario"
               Top             =   1515
               Width           =   2865
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   40101
               Index           =   9
               Left            =   -70365
               Locked          =   -1  'True
               TabIndex        =   11
               TabStop         =   0   'False
               Tag             =   "Usuario"
               Top             =   1515
               Width           =   2865
            End
            Begin IdPerson.IdPersona IdPersona1 
               Height          =   1335
               Left            =   240
               TabIndex        =   0
               Top             =   480
               Width           =   10095
               _ExtentX        =   17806
               _ExtentY        =   2355
               BackColor       =   12648384
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Datafield       =   "CI21CodPersona"
               MaxLength       =   7
               blnAvisos       =   0   'False
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   40101
               Index           =   5
               Left            =   -73335
               Locked          =   -1  'True
               TabIndex        =   10
               TabStop         =   0   'False
               Tag             =   "Usuario"
               Top             =   1515
               Width           =   2865
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   40101
               Index           =   3
               Left            =   -73320
               Locked          =   -1  'True
               TabIndex        =   3
               TabStop         =   0   'False
               Tag             =   "Departamento"
               Top             =   795
               Width           =   4935
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR09TFNOCONTAC"
               Height          =   330
               HelpContextID   =   40101
               Index           =   8
               Left            =   -74625
               Locked          =   -1  'True
               TabIndex        =   9
               Tag             =   "Tel�fono Contacto"
               Top             =   1425
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR09PERSCONTAC"
               Height          =   330
               HelpContextID   =   40101
               Index           =   4
               Left            =   -74625
               Locked          =   -1  'True
               TabIndex        =   8
               Tag             =   "Persona Contacto"
               Top             =   750
               Width           =   6060
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               HelpContextID   =   40101
               Index           =   2
               Left            =   -74745
               Locked          =   -1  'True
               TabIndex        =   5
               Tag             =   "C�digo Usuario"
               Top             =   1515
               Width           =   1215
            End
            Begin VB.TextBox txtText1 
               DataField       =   "PR09DESINDICACIO"
               Height          =   1335
               Index           =   7
               Left            =   -74805
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   15
               Tag             =   "Indicaciones"
               Text            =   "CI1025.frx":00D0
               Top             =   510
               Width           =   9975
            End
            Begin VB.TextBox txtText1 
               DataField       =   "PR09DESOBSERVAC"
               Height          =   1335
               Index           =   6
               Left            =   -74820
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   6
               Tag             =   "Observaciones"
               Text            =   "CI1025.frx":00D6
               Top             =   480
               Width           =   9975
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   2130
               Index           =   2
               Left            =   -74880
               TabIndex        =   32
               Top             =   120
               Width           =   10005
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowRowSizing  =   0   'False
               SelectTypeRow   =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17648
               _ExtentY        =   3757
               _StockProps     =   79
               ForeColor       =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   0
               Left            =   -74760
               TabIndex        =   4
               Tag             =   "C�digo Departamento"
               Top             =   795
               Width           =   1245
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1667
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(0).HasForeColor=   -1  'True
               Columns(0).HasBackColor=   -1  'True
               Columns(0).BackColor=   16777215
               Columns(1).Width=   5609
               Columns(1).Caption=   "Departamento"
               Columns(1).Name =   "Departamento"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2196
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 0"
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono Contacto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   -74625
               TabIndex        =   36
               Top             =   1185
               Width           =   1590
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Persona Contacto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   -74625
               TabIndex        =   35
               Top             =   495
               Width           =   1530
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Usuario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   -74730
               TabIndex        =   34
               Top             =   1245
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   -74730
               TabIndex        =   33
               Top             =   525
               Width           =   1200
            End
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   30
            Top             =   120
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N�mero Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   2160
            TabIndex        =   28
            Top             =   120
            Width           =   1230
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   23
      Top             =   7920
      WhatsThisHelpID =   10038
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPeticion3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 1

Public WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Dim btnx As Button
'Posici�n y tama�o de la toolbar de Lotes
Dim intToolbarLeft As Integer 'izquierda
Dim intToolbarWidth As Integer 'ancho
'Dim cllPeticiones As New Collection
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim lngGrupo As Long

Private Sub CallPruebasAdd()
   Dim rsa As rdoResultset
   Dim rsb As rdoResultset
   Dim rsc As rdoResultset
   Dim rsd As rdoResultset
   Dim rse As rdoResultset
   Dim rsf As rdoResultset
   Dim strSql2 As String
   Dim intNumActu As Integer
   Dim intNumPlan As Integer
   
   strSql2 = "select AD02CodDpto,PR01CodActuacion from PR0200 "
   Set rsb = objApp.rdoConnect.OpenResultset(strSql2, rdOpenKeyset, rdConcurValues)
   
   strSql2 = "select * from PR0300 ORDER BY PR03NumActPedi"
   Set rsa = objApp.rdoConnect.OpenResultset(strSql2, rdOpenKeyset, rdConcurValues)
   If rsa.RowCount = 0 Then
      intNumActu = 1
   Else
      rsa.MoveLast
      intNumActu = rsa.rdoColumns("PR03NumActPedi") + 1
   End If
   While Not rsb.EOF
      Randomize
      If Int((7 * Rnd) + 1) = 2 Then
         rsa.AddNew
         rsa.rdoColumns("PR03NumActPedi") = intNumActu
         rsa.rdoColumns("PR09NumPeticion") = txtText1(1).Text
         rsa.rdoColumns("PR01CodActuacion") = rsb.rdoColumns("pr01codactuacion")
         rsa.rdoColumns("AD02CodDpto") = rsb.rdoColumns("AD02CodDpto")
         rsa.rdoColumns("CI21CODPERSONA") = IdPersona1.Text
         rsa.Update
         
         strSql2 = "select * from PR0800 Where PR09NumPeticion = '" & txtText1(1) & "'"
         Set rsc = objApp.rdoConnect.OpenResultset(strSql2, rdOpenKeyset, rdConcurValues)
         rsc.AddNew
         rsc.rdoColumns("PR09NumPeticion") = Val(txtText1(1))
         rsc.rdoColumns("PR03NumActPedi") = intNumActu
         rsc.rdoColumns("PR08NumSecuencia") = 1
         rsc.Update
         rsc.Close
         
         intNumPlan = GetNewCode("select MAX(PR04NUMACTPLAN) from PR0400 ")
         strSql2 = "SELECT * FROM PR0400 WHERE PR04NUMACTPLAN = " & intNumPlan
         Set rsc = objApp.rdoConnect.OpenResultset(strSql2, rdOpenKeyset, rdConcurValues)
         If rsc.RowCount = 0 Then
            rsc.AddNew
         Else
            rsc.Edit
         End If
         rsc.rdoColumns("PR04NumActPlan") = intNumPlan
         rsc.rdoColumns("PR03NumActPedi") = intNumActu
         rsc.rdoColumns("AD02CODDPTO") = rsb.rdoColumns("AD02CodDpto")
         rsc.rdoColumns("PR01CODACTUACION") = rsb.rdoColumns("pr01codactuacion")
         rsc.rdoColumns("CI21CODPERSONA") = IdPersona1.Text
         rsc.Update
         rsc.Close
         
         strSql2 = "select * from PR0600 where PR03NumActPedi = " & intNumActu
         Set rsc = objApp.rdoConnect.OpenResultset(strSql2, rdOpenKeyset, rdConcurValues)
         If rsc.RowCount = 0 Then
            strSql2 = "select * from PR0500 where PR01CODACTUACION = " & rsb.rdoColumns("pr01codactuacion")
            Set rsd = objApp.rdoConnect.OpenResultset(strSql2, rdOpenKeyset, rdConcurValues)
            While Not rsd.EOF
               
               rsc.AddNew
               rsc.rdoColumns("PR03NumActPedi") = intNumActu
               rsc.rdoColumns("PR06NumFase") = rsd.rdoColumns("PR05NUMFASE").Value
               rsc.rdoColumns("PR06Desfase") = rsd.rdoColumns("PR05DESFASE").Value
               rsc.rdoColumns("PR06NumMinOcupac") = rsd.rdoColumns("PR05NumOcupaci").Value
               rsc.rdoColumns("PR06NumFase_Pre") = rsd.rdoColumns("PR05NumFase_Pre").Value
               rsc.rdoColumns("PR06NumMinFPre") = rsd.rdoColumns("PR05NumTMinFPre").Value
               rsc.rdoColumns("PR06NumMaxFPre") = rsd.rdoColumns("PR05NumTMaxFPre").Value
               rsc.rdoColumns("PR06IndHabNatu") = rsd.rdoColumns("PR05IndHabilNatu").Value
               rsc.rdoColumns("PR06IndIniFin") = rsd.rdoColumns("PR05IndInicioFin").Value
               rsc.Update
                              
               strSql2 = "select * from PR1400 where PR03NUMACTPEDI = " & intNumActu & " and PR06NUMFASE = " & rsd.rdoColumns("PR05NUMFASE").Value
               Set rse = objApp.rdoConnect.OpenResultset(strSql2, rdOpenKeyset, rdConcurValues)
               
               strSql2 = "select * from PR1300 where PR01CODACTUACION = " & rsb.rdoColumns("pr01codactuacion") & " and PR05NUMFASE = " & rsd.rdoColumns("PR05NUMFASE").Value
               Set rsf = objApp.rdoConnect.OpenResultset(strSql2, rdOpenKeyset, rdConcurValues)
               While Not rsf.EOF
                  rse.AddNew
                  rse.rdoColumns("PR03NUMACTPEDI") = intNumActu
                  rse.rdoColumns("PR06NUMFASE") = rsd.rdoColumns("PR05NUMFASE").Value
                  rse.rdoColumns("PR14NUMNECESID") = rsf.rdoColumns("PR13NUMNECESID").Value
                  rse.rdoColumns("AG14CODTIPRECU") = rsf.rdoColumns("AG14CODTIPRECU").Value
                  rse.rdoColumns("PR14NUMUNIREC") = rsf.rdoColumns("PR13NUMUNIREC").Value
                  rse.rdoColumns("PR14INDRECPREFE") = rsf.rdoColumns("PR13INDPREFEREN").Value
                  rse.rdoColumns("AD02CODDPTO") = rsf.rdoColumns("AD02CODDPTO").Value
                  rse.Update
                  rsf.MoveNext
               Wend
               rse.Close
               rsf.Close
               rsd.MoveNext
            Wend
            rsd.Close
         End If
         rsc.Close
         
         intNumActu = intNumActu + 1
      End If
      rsb.MoveNext
   Wend
      
   rsa.Close
   rsb.Close
   Set rsa = Nothing
   Set rsb = Nothing
   Set rsc = Nothing
   Set rsd = Nothing
   Set rse = Nothing
   Set rsf = Nothing
   
   'Call objWinInfo.DataRefresh
   'objWinInfo.DataMoveFirst

End Sub

Private Function SelRefreshGrid(lngPeticion As Long)
  Dim objPeticion               As New clsSelPeticion
  Dim objActuacion               As New clsSelActuacion
  Dim vntRowBookmark      As Variant
  
  Set objPeticion = objSolicitud.GetPeticion(lngPeticion)
  For Each objActuacion In objPeticion.cllActuaciones
    grdDBGrid1(1).SelBookmarks.Add (objActuacion.intRow)
  Next

  Set objPeticion = Nothing
  Set objActuacion = Nothing
End Function


Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 And KeyCode = 46 Then
    cboSSDBCombo1(intIndex).Text = ""
  End If

End Sub

Private Sub cmdDetalle_Click(intIndex As Integer)
  If intIndex = 0 And Val(grdDBGrid1(1).Columns("N�mero").Value) > 0 Then
      'Call ShowDetail(Val(grdDBGrid1(1).Columns("N�mero").Value), Val(grdDBGrid1(1).Columns("Solicitud").Value), Val(grdDBGrid1(1).Columns("N�mero Cita").Value))
     Call ShowDetail2(grdDBGrid1(1))
      Call IdPersona1.BeginControl(objApp, objGen)
  End If

End Sub

Private Sub cmdFases_Click(intIndex As Integer)
   vntNumActPedi = grdDBGrid1(1).Columns("Actuaci�n Pedida").Value
   vntNumActPlan = grdDBGrid1(1).Columns("N�mero").Value
   vntNumSolicit = grdDBGrid1(1).Columns("Solicitud").Value
   vntNumCita = grdDBGrid1(1).Columns("N�mero Cita").Value

   If Val(vntNumActPedi) > 0 Then
     Me.MousePointer = vbHourglass
     
     'Load frmFasesPCR
     
     'frmFasesPCR.Show vbModal
     'Unload frmFasesPCR
     'Set frmFasesPCR = Nothing
     
     Call objSecurity.LaunchProcess(ciFasesRecursos, True)
     
     Me.MousePointer = vbDefault
     
     vntNumActPedi = 0
   End If
End Sub

Private Sub cmdPruebas_Click()
  Dim lngCods(3) As Long
  Dim blnCancelTrans As Boolean
  
  If IdPersona1.Nombre = "" Then
    MsgBox "No hay ninguna Persona seleccionada.", vbInformation
    Exit Sub
  End If
  
    
  
  lngCods(1) = Val(txtText1(0).Text) 'Num. Grupo
  lngCods(2) = Val(txtText1(1).Text) 'Num. Petici�n
  lngCods(3) = Val(IdPersona1.Text)  'C�d. Persona
  On Error GoTo Canceltrans
  
'  Call CallPruebasAdd
   
  Call objSecurity.LaunchProcess("PR0152", lngCods)
  
  objWinInfo.objWinMainForm.blnChanged = False
  
  If blnCancelTrans Then
    'Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.SetError(cwCodeMsg, "Pedir Pruebas Cancelado")
    Call objError.Raise
  Else
    Me.MousePointer = vbHourglass
    IdPersona1.Locked = True
    IdPersona1.BackColor = objApp.objUserColor.lngReadOnly
    
    objWinInfo.objWinActiveForm.strWhere = "PR09NUMPETICION=" & lngCods(2)
    Call objWinInfo.DataRefresh
    objWinInfo.objWinActiveForm.strWhere = "Pr09NumGRUPO = " & lngCods(1)
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
      cmdAgenda(0).Enabled = True
      cmdVerSeleccion(0).Enabled = True
      cmdDetalle(0).Enabled = True
      cmdFases(0).Enabled = True
      cmdVerSolicitud(0).Enabled = True
    End If
    Me.MousePointer = vbDefault
  End If

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   MsgBox Err.Description
   Resume Next


End Sub

Private Sub cmdVerSeleccion_Click(Index As Integer)
  'Call objGen.RemoveCollection(objSolicitud.cllPeticiones)
  Call objSolicitud.RemovePeticiones(Val(txtText1(1).Text))
  Call objSelRefresh(grdDBGrid1(1), , , ciPdteConfirmar, False)
  Call objShowSelected
  If blnBorrarSel Then
    grdDBGrid1(1).SelBookmarks.RemoveAll
    blnBorrarSel = False
  End If

End Sub

Private Sub cmdAgenda_Click(intIndex As Integer)

  Dim blnCancelTrans As Boolean

  'objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
  If grdDBGrid1(1).SelBookmarks.Count > 0 Then
    Me.MousePointer = vbHourglass
    Call objSolicitud.RemovePeticiones(txtText1(1).Text)
    Call objSelRefresh(grdDBGrid1(1), 0, ciOpcAgenda, ciPdteConfirmar, False)

   'LLamar a la pantalla de Solicitud de citas
    'Load frmSolicitud
    'frmSolicitud.Show vbModal
    'Unload frmSolicitud
    'Set frmSolicitud = Nothing
  
    Call objSecurity.LaunchProcess(ciSoluciones)
  
    Me.MousePointer = vbDefault
    If blnCancelTrans Then
     ' objApp.rdoConnect.RollbackTrans
      'Call objError.SetError(cwCodeMsg, ciErrRollback)
      Call objError.SetError(cwCodeMsg, "Citar Actuaciones Cancelado")
      Call objError.Raise
    Else
      If blnCommit Then
        Me.MousePointer = vbHourglass
      '  objApp.rdoConnect.CommitTrans
        Call objWinInfo.DataRefresh
        Call Me.Refresh
        DoEvents
        Me.MousePointer = vbDefault
        blnCommit = False
      End If
    End If
  Else
    MsgBox "No hay ninguna Actuaci�n seleccionada", vbInformation
  End If
  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub


Private Sub cmdVerSolicitud_Click(Index As Integer)
  Dim blnCancelTrans      As Boolean
  Dim intGridIndex        As Integer
  Dim lngSolicitud        As Long

'  objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans

  Me.MousePointer = vbHourglass
  intGridIndex = 1
  lngSolicitud = grdDBGrid1(intGridIndex).Columns("grdDBGrid1(" & intGridIndex & ").Solicitud").Value
  
  objSolicitud.RemovePeticiones

  Call objLoadFromSolicit(lngSolicitud, ciOpcVerSolicitud)

  'Load frmSolicitud
  'frmPeticion3.MousePointer = vbDefault
  'frmSolicitud.Show vbModal
  'Unload frmSolicitud
  'Set frmSolicitud = Nothing

  Call objSecurity.LaunchProcess(ciSoluciones)
  Me.MousePointer = vbDefault
  
  If blnCancelTrans Then
 '   objApp.rdoConnect.RollbackTrans
    Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.Raise
  Else
  '  objApp.rdoConnect.CommitTrans
    If blnCommit Then
      Call objWinInfo.DataRefresh
      Call Me.Refresh
      DoEvents
    End If
  End If

  Call objGen.RemoveCollection(objSolicitud.cllPeticiones)

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub Form_Activate()
  If intToolbarLeft = 0 Then
    intToolbarLeft = tlbToolbar1.Left
    intToolbarWidth = tlbToolbar1.width
  End If
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, Shift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, Shift)

End Sub

Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim intPos As Integer
  
  Call objApp.SplashOn
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)

  Set objWinInfo = New clsCWWin
 
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  Call objSolicitud.RemovePeticiones
  
  With objMasterInfo
    .strName = "Petici�n"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0900"
    .intAllowance = cwAllowAdd
    .blnHasMaint = True
    
    strKey = .strDataBase & .strTable
    Call .FormAddOrderField("Pr09NumPeticion", cwAscending)
    
    'txtPeticion.Text = GetNewCode("select max(Pr09NUMGRUPO) from Pr0900") - 1
    
    '.strWhere = "Pr09NumGRUPO=" & Val(txtPeticion.Text) '& "'"
    .blnAskPrimary = False

  End With
  
  With objMultiInfo
    .strName = "Actuaciones"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .strWhere = "(CI01SITCITA = '" & ciPdteConfirmar & "' or  CI01SITCITA = '" & ciConfirmada & "' or CI01SITCITA = '" & ciPdteRecitar & "' or CI01SITCITA is null )"
    .intAllowance = cwAllowReadOnly
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0431J"
    
    strKey = .strDataBase & .strTable
    
    Call .FormAddOrderField("PR04NumActPlan", cwAscending)
    Call .FormAddRelation("PR09NumGRUPO", txtText1(0))
    Call .FormAddRelation("PR09NumPeticion", txtText1(1))
  End With
  
  With objWinInfo
  Call .FormAddInfo(objMasterInfo, cwFormDetail)
  Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
  Call .GridAddColumn(objMultiInfo, "Situaci�n", "CI01SITCITA")
  Call .GridAddColumn(objMultiInfo, "Citable", "PR03INDCITABLE", cwBoolean)
  Call .GridAddColumn(objMultiInfo, "Descripci�n", "")
  Call .GridAddColumn(objMultiInfo, "C�dDepartamento", "AD02CODDPTO")
  Call .GridAddColumn(objMultiInfo, "Departamento", "")
  Call .GridAddColumn(objMultiInfo, "Fecha Cita", "CI01FECCONCERT")
  Call .GridAddColumn(objMultiInfo, "CodRecAsi", "AG11CODRECURSO_ASI")
  Call .GridAddColumn(objMultiInfo, "Recurso Asignado", "")
  Call .GridAddColumn(objMultiInfo, "Fecha Preferencia", "PR03FECPREFEREN")
  Call .GridAddColumn(objMultiInfo, "CodPref", "AG11CODRECURSO_PRE")
  Call .GridAddColumn(objMultiInfo, "Recurso Preferente", "")
  Call .GridAddColumn(objMultiInfo, "Solicitud", "CI31NUMSOLICIT")
  Call .GridAddColumn(objMultiInfo, "N�mero Cita", "CI01NUMCITA")
  Call .GridAddColumn(objMultiInfo, "Persona", "CI21CODPERSONA")
  Call .GridAddColumn(objMultiInfo, "N�mero", "PR04NUMACTPLAN")
  Call .GridAddColumn(objMultiInfo, "C�digo", "PR01CODACTUACION")
  'Call .GridAddColumn(objMultiInfo, "Petici�n", "PR09NUMPETICION")
  Call .GridAddColumn(objMultiInfo, "Actuaci�n Pedida", "PR03NUMACTPEDI")
  Call .GridAddColumn(objMultiInfo, "Lista Espera", "CI01INDLISESPE", cwBoolean)
  Call .GridAddColumn(objMultiInfo, "Recordatorio", "CI01INDRECORDA", cwBoolean)
      
  
  Call .FormCreateInfo(objMasterInfo)
  grdDBGrid1(1).StyleSets.Add "Situaci�nCita"
  grdDBGrid1(1).StyleSets("Situaci�nCita").BackColor = &H808080
  grdDBGrid1(1).StyleSets("Situaci�nCita").ForeColor = &HFFFFFF
   
  grdDBGrid1(1).Columns("grddbgrid1(1).Situaci�n").Caption = "?"
  grdDBGrid1(1).Columns("grddbgrid1(1).Situaci�n").width = TextWidth("OOO")
  grdDBGrid1(1).Columns("grddbgrid1(1).Citable").width = 615
  grdDBGrid1(1).Columns("grddbgrid1(1).Situaci�n").Alignment = ssCaptionAlignmentCenter
  grdDBGrid1(1).Columns("grddbgrid1(1).Situaci�n").StyleSet = "Situaci�nCita"
  
  grdDBGrid1(1).Columns("grddbgrid1(1).Fecha Cita").width = TextWidth(String(14, "O"))
  grdDBGrid1(1).Columns("grddbgrid1(1).Fecha Cita").Alignment = ssCaptionAlignmentCenter
  
  grdDBGrid1(1).Columns("grddbgrid1(1).Descripci�n").width = TextWidth(String(33, "O"))
  grdDBGrid1(1).Columns("grddbgrid1(1).Departamento").width = TextWidth(String(22, "O"))
  grdDBGrid1(1).Columns("grdDBGrid1(1).Persona").Visible = False
  grdDBGrid1(1).Columns("grdDBGrid1(1).Actuaci�n Pedida").Visible = False
  grdDBGrid1(1).Columns("grdDBGrid1(1).C�dDepartamento").Visible = False
  grdDBGrid1(1).Columns("grdDBGrid1(1).CodRecAsi").Visible = False
  grdDBGrid1(1).Columns("grdDBGrid1(1).CodPref").Visible = False
  
  IdPersona1.ToolTipText = ""
  IdPersona1.MaxLength = 7
  
  'Crear la Toolbar de Lotes
  tlbToolbarGrupo.ImageList = objApp.imlImageList
    
  tlbToolbarGrupo.Buttons.Add , , , tbrSeparator
    
  Set btnx = tlbToolbarGrupo.Buttons.Add(, "NewGRUPO", , tbrDefault, "i1")
  btnx.ToolTipText = "A�adir un nuevo GRUPO"
  btnx.Description = btnx.ToolTipText
        
    tlbToolbarGrupo.Buttons.Add , , , tbrSeparator
    
    Set btnx = tlbToolbarGrupo.Buttons.Add(, "FirstGRUPO", , tbrDefault, "i15")
    btnx.ToolTipText = "Ir al primer GRUPO"
    btnx.Description = btnx.ToolTipText
    
    Set btnx = tlbToolbarGrupo.Buttons.Add(, "PrevGRUPO", , tbrDefault, "i16")
    btnx.ToolTipText = "Ir al GRUPO anterior"
    btnx.Description = btnx.ToolTipText
    
    Set btnx = tlbToolbarGrupo.Buttons.Add(, "NextGRUPO", , tbrDefault, "i17")
    btnx.ToolTipText = "Ir al GRUPO siguiente"
    btnx.Description = btnx.ToolTipText
    
    Set btnx = tlbToolbarGrupo.Buttons.Add(, "LastGRUPO", , tbrDefault, "i18")
    btnx.ToolTipText = "Ir al �ltimo GRUPO"
    btnx.Description = btnx.ToolTipText
     
     
    tlbToolbarGrupo.Buttons.Add , , , tbrSeparator
    
    .CtrlGetInfo(txtText1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(0)).vntDefaultValue = Format(objGen.GetDBDateTime, "DD/MM/YYYY")
    .CtrlGetInfo(dtcDateCombo1(0)).blnForeign = True
    .CtrlGetInfo(IdPersona1).blnForeign = True
    .CtrlGetInfo(IdPersona1).intType = cwNumeric
    .CtrlGetInfo(IdPersona1).strSmallDesc = "C�digo Persona"
    .CtrlGetInfo(IdPersona1).intMask = cwMaskInteger
    .CtrlGetInfo(IdPersona1).blnReadOnly = False
    .CtrlGetInfo(IdPersona1).objLinked.blnHasLinked = False
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    
    .CtrlGetInfo(txtText1(6)).blnInGrid = False
    .CtrlGetInfo(txtText1(7)).blnInGrid = False
    .CtrlGetInfo(txtText1(4)).blnInGrid = True
    .CtrlGetInfo(txtText1(8)).blnInGrid = True
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).N�mero")).intKeyNo = 1
     Call .FormChangeColor(objMultiInfo)
    .CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).N�mero")).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).C�digo")).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).Descripci�n")).blnInFind = True
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(IdPersona1).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    
    For intPos = 0 To 8
      .CtrlGetInfo(txtText1(intPos)).blnReadOnly = True
    Next intPos
    .CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).N�mero")).blnInGrid = False
    .CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).N�mero Cita")).blnInGrid = False
    .CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).Solicitud")).blnInGrid = False
    .CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).C�digo")).blnInGrid = False
    '.CtrlGetInfo(grdDBGrid1(1).Columns("grdDBGrid1(1).Petici�n")).blnInGrid = False
    
    .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02INDRESPONPROC=-1 ORDER BY AD02DESDPTO"

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "SG02COD", "SELECT SG02NOM, SG02APE1,SG02APE2 FROM " & objEnv.GetValue("Database") & "SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "SG02NOM")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(9), "SG02APE1")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(10), "SG02APE2")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "AD02CODDPTO", "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtText1(3), "AD02DESDPTO")

    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("grddbgrid1(1).C�digo")), "PR01CODACTUACION", "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("grddbgrid1(1).C�digo")), grdDBGrid1(1).Columns("grddbgrid1(1).Descripci�n"), "PR01DESCORTA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("grddbgrid1(1).C�dDepartamento")), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("grddbgrid1(1).C�dDepartamento")), grdDBGrid1(1).Columns("grddbgrid1(1).Departamento"), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("grddbgrid1(1).CodRecAsi")), "AG11CODRECURSO_ASI", "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("grddbgrid1(1).CodRecAsi")), grdDBGrid1(1).Columns("grddbgrid1(1).Recurso Asignado"), "AG11DESRECURSO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("grddbgrid1(1).CodPref")), "AG11CODRECURSO_PRE", "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("grddbgrid1(1).CodPref")), grdDBGrid1(1).Columns("grddbgrid1(1).Recurso Preferente"), "AG11DESRECURSO")
    
    Call .WinRegister
    Call objSolicitud.RemovePeticiones
    Call .WinStabilize
    
  End With
  Call objApp.SplashOff
   IdPersona1.BackColor = objApp.objUserColor.lngReadOnly
   txtText1(1).BackColor = objApp.objUserColor.lngReadOnly
   cmdAgenda(0).Enabled = False
   'cmdPruebas.Enabled = True
   cmdVerSeleccion(0).Enabled = False
   cmdDetalle(0).Enabled = False
   cmdFases(0).Enabled = False
   cmdVerSolicitud(0).Enabled = False

   grdDBGrid1(1).Columns("grdDBGrid1(1).N�mero").Visible = False
   grdDBGrid1(1).Columns("grdDBGrid1(1).N�mero Cita").Visible = False
   grdDBGrid1(1).Columns("grdDBGrid1(1).Solicitud").Visible = False
   grdDBGrid1(1).Columns("grdDBGrid1(1).C�digo").Visible = False
   'grdDBGrid1(1).Columns("grdDBGrid1(1).Petici�n").Visible = False

   'objWinInfo.objWinMainForm.blnEnabled = True
   'Call objWinInfo.WinPrepareScr
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  On Error Resume Next
  
  
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
  Set objSolicitud = Nothing
  Set btnx = Nothing

End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub grdDBGrid1_HeadClick(Index As Integer, ByVal ColIndex As Integer)
  Dim intRow As Integer
  grdDBGrid1(1).MoveFirst
  For intRow = 0 To grdDBGrid1(1).Rows - 1
    If grdDBGrid1(1).Columns("?").Value = "" And grdDBGrid1(1).Columns("Citable").Value = -1 Then
      grdDBGrid1(1).SelBookmarks.Add (intRow)
    End If
    grdDBGrid1(1).MoveNext
  Next
  grdDBGrid1(1).MoveFirst
  Call objSolicitud.RemovePeticiones(txtText1(1).Text)
  Call objSelRefresh(grdDBGrid1(1), , , ciPdteConfirmar, False)
End Sub

Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  If intIndex = 0 Then
    If Val(grdDBGrid1(0).Columns(3).Text) <> 0 Then
      grdDBGrid1(0).Columns(3).Text = GetPersonName(grdDBGrid1(0).Columns(3).Text)
    End If
  
  End If
  If intIndex = 1 Then
    If Val(grdDBGrid1(1).Columns("Solicitud").Text) = 0 And Val(grdDBGrid1(1).Columns("N�mero Cita").Text) = 0 Then
      grdDBGrid1(1).Columns("Recordatorio").Value = -1
    End If
    grdDBGrid1(1).Columns("Fecha Cita").Text = Format(grdDBGrid1(1).Columns("Fecha Cita").Text, "DD/MM/YYYY - HH:NN")
    grdDBGrid1(1).Columns("Fecha Preferencia").Text = Format(grdDBGrid1(1).Columns("Fecha Preferencia").Text, "DD/MM/YYYY - HH:NN")
    Select Case grdDBGrid1(1).Columns("?").Text
      Case "1"
        grdDBGrid1(1).Columns("?").Text = "C" 'Citado
      Case "2"
        grdDBGrid1(1).Columns("?").Text = "A" 'Anulada
      Case "3"
        grdDBGrid1(1).Columns("?").Text = "RE" 'Recitada
      Case "4"
        grdDBGrid1(1).Columns("?").Text = "PR" 'Pendiente Recitar
    End Select
  End If
End Sub

Private Sub grdDBGrid1_SelChange(intIndex As Integer, ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
   If intIndex = 1 Then
      If grdDBGrid1(intIndex).Columns("grdDBGrid1(1).Situaci�n").Value <> ciPdteConfirmar And grdDBGrid1(intIndex).Columns("grdDBGrid1(1).Situaci�n").Value <> "" Then
         
         If objWinInfo.objWinActiveForm.strName = "Petici�n" Then
            Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
         End If
         Cancel = True
      End If
      If grdDBGrid1(intIndex).Columns("grdDBGrid1(1).Citable").Value = 0 Then
       Cancel = True
      End If
   End If
End Sub

Private Sub IdPersona1_Change()
'  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
  'If IdPersona1.Apellido1 <> "" Then
  '  cmdPruebas.Enabled = True
  'Else
  '  cmdPruebas.Enabled = False
  'End If

End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Petici�n" Then
    If strCtrl = "IdPersona1" And txtText1(1) = "" Then
      IdPersona1.SearchPersona
      'If IdPersona1.Text <> "" Then
      '  cmdPruebas.Enabled = True
      'End If
      If IdPersona1.Apellido1 <> "" Then
        objWinInfo.objWinMainForm.blnChanged = False
      End If
    End If
    'If strCtrl = "txtText1(2)" Then
    '  Set objSearch = New clsCWSearch
    '  With objSearch
    '    .strTable = "AD0600"
    '    .strWhere = ""
    '    .strOrder = ""
    
    '    Set objField = .AddField("AD06CODPERSONA")
    '    objField.strSmallDesc = "C�digo Persona"
    '    Set objField = .AddField("AD06NOMBRE")
    '    objField.strSmallDesc = "Nombre"
    '    Set objField = .AddField("AD06PRIAPEL")
    '    objField.strSmallDesc = "Primer Apellido"
    '    Set objField = .AddField("AD06SEGAPEL")
    '    objField.strSmallDesc = "Segundo Apellido"
       
      
    '    If .Search Then
    '      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD06CODPERSONA"))
    '    End If
    '  End With
    ' End If

  End If
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  If Me.ActiveControl.DataField = "CI21CodPersona" And strFormName = "Petici�n" Then
    Me.MousePointer = vbHourglass
      
    If IdPersona1.Apellido1 <> "" Then
      blnAddMode = False
      lngUserCode = Val(IdPersona1.Text)
    Else
      blnAddMode = True
    
    End If
    
    'Versi�n EXE
    'Load frmPersF�sicas
    'Call frmPersF�sicas.Show(vbModal)
    
    'Unload frmPersF�sicas
    'Set frmPersF�sicas = Nothing
     
     'Versi�n DLL
    Call objSecurity.LaunchProcess(ciPersF�sicas)
    If lngUserCode > 0 Then
      IdPersona1.Text = lngUserCode
      lngUserCode = 0
    End If
    blnAddMode = False
    Me.MousePointer = vbDefault

  End If

End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  If strFormName = "Petici�n" Then
    tlbToolbar1.Left = intToolbarLeft
    tlbToolbar1.width = intToolbarWidth
    tlbToolbarGrupo.Visible = True
    cmdAgenda(0).Enabled = False
    'cmdPruebas.Enabled = False
    cmdVerSeleccion(0).Enabled = False
    cmdDetalle(0).Enabled = False
    cmdFases(0).Enabled = False
    cmdVerSolicitud(0).Enabled = False
    Call objSolicitud.RemovePeticiones(Val(txtText1(1).Text))
    Call objSelRefresh(grdDBGrid1(1), , , ciPdteConfirmar, False)
    IdPersona1.blnAvisos = True
  Else
    tlbToolbar1.Left = 0
    tlbToolbar1.width = Me.width - 100
    tlbToolbarGrupo.Visible = False
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
      cmdAgenda(0).Enabled = True
      cmdVerSeleccion(0).Enabled = True
      cmdDetalle(0).Enabled = True
      cmdFases(0).Enabled = True
      cmdVerSolicitud(0).Enabled = True
    End If
    'cmdPruebas.Enabled = True
    IdPersona1.blnAvisos = False
  
  End If
  If Val(txtText1(1).Text) > 0 Then
    Call SelRefreshGrid(txtText1(1).Text)
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If objWinInfo.intWinStatus <> cwModeSingleEmpty And txtText1(1).Text <> "" Then
    Call SelRefreshGrid(txtText1(1).Text)
  End If
'  If strFormName = "Petici�n" Then
'    Call IdPersona1.ReadPersona
'  End If
End Sub


Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
  If txtText1(0).Text <> "" Then
   'Call SelRefresh
  End If
End Sub


Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, ByRef blnError As Boolean)
 ' Dim strSql As String
 ' If strFormName = "Petici�n" Then
 '   If txtText1(0) = "" Then
 '     strSql = "select max(Pr09NumGRUPO) from Pr0900"
 '     Call objWinInfo.CtrlSet(txtText1(0), GetNewCode(strSql))
 '     txtPeticion.Text = txtText1(0).Text
 '     objWinInfo.objWinActiveForm.strWhere = "Pr09NumGRUPO = " & Val(txtPeticion) '& "'"
 '   End If
 '   If txtText1(1).Text = "" Then
 '     strSql = "select max(Pr09NumPeticion) from Pr0900"
 '     Call objWinInfo.CtrlSet(txtText1(1), GetNewCode(strSql))
 '   End If
 ' End If
End Sub
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim rsPeticion As rdoResultset
  Dim strSql As String
  Dim lngCodGrupo As Long
  
  If btnButton.Key = "b0" And objWinInfo.objWinActiveForm.strName = "Petici�n" Then
   If txtText1(0) <> "" Then
    lngCodGrupo = Val(txtText1(0))
    objWinInfo.objWinActiveForm.strWhere = "PR09NUMPETICION=0"
    objWinInfo.objWinActiveForm.blnChanged = False
    objWinInfo.DataRefresh
    objWinInfo.intWinStatus = cwModeSingleEdit
    txtText1(0) = lngCodGrupo
    'objWinInfo.CtrlGetInfo(txtText1(0)).blnBlocked = True
    IdPersona1.Apellido1 = ""
    IdPersona1.Apellido2 = ""
    IdPersona1.Dni = ""
    IdPersona1.Historia = ""
    IdPersona1.Nombre = ""
    IdPersona1.Text = ""
    IdPersona1.Locked = False
    IdPersona1.BackColor = objApp.objUserColor.lngNormal
    IdPersona1.SetFocus
   End If
  Else
    If btnButton.Key = "b9" And objWinInfo.objWinActiveForm.strName = "Petici�n" Then
      objWinInfo.objWinActiveForm.strWhere = ""
    End If
    If btnButton.Key <> "b1" Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      txtPeticion.Text = txtText1(0).Text
      If Val(txtPeticion) > 0 And btnButton.Key = "b9" Then
        objWinInfo.objWinMainForm.strWhere = "Pr09NumGRUPO = " & Val(txtPeticion) '& "'"
        grdDBGrid1(1).Refresh
      End If
      If btnButton.Key <> "b18" Then
        IdPersona1.Locked = True
        IdPersona1.BackColor = objApp.objUserColor.lngReadOnly
      End If
    End If
   
  End If
  'If btnButton.Key <> "b18" Then
  '  objWinInfo.CtrlGetInfo(txtText1(0)).blnBlocked = False
  'End If
End Sub
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  If intIndex = 1 And grdDBGrid1(1).Row <> 0 Then
        cmdVerSolicitud(0).Enabled = IIf(grdDBGrid1(intIndex).Columns("grdDBGrid1(" & intIndex & ").Situaci�n").Value = "", False, True)
  End If
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
     
    Call objWinInfo.FormChangeActive(tabTab1(0), False, True)
'  If tabtab1(intIndex).Tab = 0 Then
'     IdPersona1.Refresh
'     Me.Refresh
'  End If
'  DoEvents
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub tlbToolbarGRUPO_ButtonClick(ByVal btnButton As ComctlLib.Button)
  Dim strSql              As String
  Dim vntGrupo            As Variant
  
  Select Case btnButton.Key
   Case "NewGRUPO"
     While objSolicitud.cllPeticiones.Count > 0
        Call objSolicitud.cllPeticiones.Remove(1)
     Wend
     objWinInfo.objWinActiveForm.strWhere = "PR09NUMPETICION=0"
     objWinInfo.objWinActiveForm.blnChanged = False
     objWinInfo.DataRefresh
     objWinInfo.intWinStatus = cwModeSingleEdit
     'Call objWinInfo.DataNew
     IdPersona1.Apellido1 = ""
     IdPersona1.Apellido2 = ""
     IdPersona1.Dni = ""
     IdPersona1.Historia = ""
     IdPersona1.Nombre = ""
     IdPersona1.Text = ""
     IdPersona1.Locked = False
     IdPersona1.BackColor = objApp.objUserColor.lngNormal
     
     'strSql = "select max(Pr09NumGRUPO) from Pr0900"
     'Call objWinInfo.CtrlSet(txtText1(0), GetNewCode(strSql))
     Call objWinInfo.CtrlSet(txtText1(0), "")
     Call objWinInfo.CtrlSet(txtText1(1), "")
     txtPeticion.Text = txtText1(0).Text
     objWinInfo.objWinActiveForm.strWhere = "Pr09NumGRUPO = " & Val(txtPeticion) '& "'"

     objWinInfo.objWinActiveForm.blnChanged = False
     'IdPersona1.SetFocus

   Case "FirstGRUPO", "PrevGRUPO", "NextGRUPO", "LastGRUPO"
     Select Case btnButton.Key
       Case "FirstGRUPO"
         strSql = "select min(Pr09NumGRUPO) from Pr0900"
       Case "PrevGRUPO"
         strSql = "select max(Pr09NumGRUPO) from Pr0900 where pr09NumGRUPO < " & Val(txtPeticion) ' & "'"
       Case "NextGRUPO"
         strSql = "select min(Pr09NumGRUPO) from Pr0900 where pr09NumGRUPO > " & Val(txtPeticion) '& "'"
       Case "LastGRUPO"
         strSql = "select max(Pr09NumGRUPO) from Pr0900"
     End Select
     vntGrupo = GetTableColumn(strSql)(1)
     If vntGrupo <> "" Then
       txtPeticion.Text = vntGrupo
       objWinInfo.objWinActiveForm.strWhere = "Pr09NumGRUPO = " & Val(txtPeticion) '& "'"
       objWinInfo.objWinActiveForm.blnChanged = False
       Call objWinInfo.DataRefresh
       'objWinInfo.DataMoveFirst
     End If
  End Select
End Sub

Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  'If intIndex = 1 Then
  ' If IdPersona1.Text = "" Then
  '   'IdPersona1.SetFocus
 '  End If
 ' End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim objPeticion As clsSelPeticion
  
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 Then
    If lngGrupo <> Val(txtText1(0)) Then
     For Each objPeticion In objSolicitud.cllPeticiones
      objPeticion.RemoveActuaciones
     Next
   
     blnBorrarSel = True

    End If
  End If
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus

End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
   
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

End Sub
Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

End Sub


