VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmHora 
   Caption         =   "Introduzca una Hora"
   ClientHeight    =   1395
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   2430
   Icon            =   "CI1039.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1395
   ScaleWidth      =   2430
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "&Cancelar"
      Height          =   375
      Index           =   1
      Left            =   10
      TabIndex        =   1
      Top             =   960
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Aceptar"
      Height          =   375
      Index           =   0
      Left            =   1440
      TabIndex        =   2
      Top             =   960
      Width           =   975
   End
   Begin ComCtl2.UpDown UpDown3 
      Height          =   450
      Index           =   0
      Left            =   1560
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   240
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   794
      _Version        =   327681
      OrigLeft        =   720
      OrigTop         =   480
      OrigRight       =   960
      OrigBottom      =   810
      Increment       =   0
      Max             =   1440
      Enabled         =   -1  'True
   End
   Begin MSMask.MaskEdBox MaskEdBox1 
      Height          =   450
      Index           =   0
      Left            =   600
      TabIndex        =   0
      Top             =   240
      Width           =   915
      _ExtentX        =   1614
      _ExtentY        =   794
      _Version        =   327681
      MaxLength       =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   "hh:mm"
      Mask            =   "##:##"
      PromptChar      =   "_"
   End
End
Attribute VB_Name = "frmHora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public strHora As String

Private Sub Command1_Click(intIndex As Integer)
  If intIndex = 0 Then
   If Not IsDate(objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0)) Then
    Call objError.SetError(cwCodeMsg, "La hora introducida no es correcta")
    Call objError.Raise
    MaskEdBox1(intIndex).SetFocus
   Else
    strHora = Format(objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0), "hh:mm")
    
   End If
  End If
  If intIndex = 1 Then
    strHora = ""
  End If
  Me.Hide
End Sub

Private Sub UpDown3_Change(intIndex As Integer)
 ' Dim datFecha As Date
'incremento del spin para hora y minuto preferente
 ' UpDown3(intIndex).Increment = ciIntervaloCitaMM
  
 ' datFecha = CDate(Format("00:00", "HH:MM"))
 ' datFecha = DateAdd("n", Val(UpDown3(intIndex).Value), datFecha)
 ' MaskEdBox1(intIndex).Text = objGen.ReplaceStr(Format(datFecha, "HH:MM"), ".", ":", 0)

End Sub
Private Sub MaskEdBox1_Change(intIndex As Integer)
  'Dim datFechaDesde As Date
  'Dim datFechaHasta As Date
  'Dim strHora As String
  'Dim intUpdownValue As Integer
  
  'strHora = objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0)
  'If Mid(strHora, 1, 2) * 60 + Mid(strHora, 4, 2) <= UpDown3(intIndex).Max And IsDate(strHora) Then
  '  datFechaDesde = "00:00"
  '  datFechaHasta = CDate(Format(objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0), "hh:mm"))
  '  intUpdownValue = DateDiff("n", datFechaDesde, datFechaHasta) - (DateDiff("n", datFechaDesde, datFechaHasta) Mod ciIntervaloCitaMM)
    'If intUpdownValue >= UpDown3(intIndex).Min And intUpdownValue <= UpDown3(intIndex).Max Then
      'UpDown3(intIndex).Value = intUpdownValue
  '  End If
  'End If
  
  'If intIndex < 2 And blnSolutionAsked Then
  '  blnNewSolutions = True
  '  Call CambioSoluciones(False)
    
  'End If

End Sub

Private Sub MaskEdBox1_LostFocus(intIndex As Integer)
  If Not IsDate(objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0)) Then
    Call objError.SetError(cwCodeMsg, "La hora introducida no es correcta")
    Call objError.Raise
    MaskEdBox1(intIndex).SetFocus
  End If
End Sub

Private Sub UpDown3_DownClick(intIndex As Integer)
  Dim datFecha As Date
'incremento del spin para hora y minuto preferente
  UpDown3(intIndex).Increment = ciIntervaloCitaMM
  
  
  datFecha = CDate(Format(objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0), "hh:mm"))
  'datFecha = CDate(Format("00:00", "HH:MM"))
  datFecha = DateAdd("n", -Val(UpDown3(intIndex).Increment), datFecha)
  MaskEdBox1(intIndex).Text = objGen.ReplaceStr(Format(datFecha, "HH:MM"), ".", ":", 0)

End Sub

Private Sub UpDown3_UpClick(Index As Integer)
  Dim datFecha As Date
'incremento del spin para hora y minuto preferente
  UpDown3(Index).Increment = ciIntervaloCitaMM
  
  datFecha = CDate(Format(objGen.ReplaceStr(MaskEdBox1(Index).Text, "_", "0", 0), "hh:mm"))
  'datFecha = CDate(Format("00:00", "HH:MM"))
  datFecha = DateAdd("n", Val(UpDown3(Index).Increment), datFecha)
  MaskEdBox1(Index).Text = objGen.ReplaceStr(Format(datFecha, "HH:MM"), ".", ":", 0)

End Sub
