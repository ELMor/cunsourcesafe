VERSION 5.00
Object = "{BBF36B7A-55C7-11D1-A5F1-00C04FDAA4D2}#1.0#0"; "IDPERSON.OCX"
Begin VB.Form frmDetalleCita 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Detalle Cita"
   ClientHeight    =   7050
   ClientLeft      =   675
   ClientTop       =   1410
   ClientWidth     =   10515
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "DetCita.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7050
   ScaleWidth      =   10515
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Actuaci�n"
      Height          =   4110
      Index           =   0
      Left            =   60
      TabIndex        =   6
      Top             =   2790
      Width           =   10350
      Begin VB.CommandButton Command1 
         Caption         =   "&Volver"
         Height          =   450
         Left            =   8415
         TabIndex        =   41
         Top             =   3420
         Width           =   1800
      End
      Begin VB.Frame Frame2 
         Caption         =   "Incidencias"
         Height          =   900
         Left            =   255
         TabIndex        =   36
         Top             =   2970
         Width           =   6945
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   345
            HelpContextID   =   30104
            Index           =   16
            Left            =   1650
            TabIndex        =   39
            Tag             =   "Puesto"
            Text            =   "     "
            Top             =   435
            Width           =   5040
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   15
            Left            =   240
            TabIndex        =   37
            Tag             =   "N�mero Direcci�n"
            Top             =   435
            Width           =   1170
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   1695
            TabIndex        =   40
            Top             =   225
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N�mero"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   255
            TabIndex        =   38
            Top             =   225
            Width           =   660
         End
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   2
         Left            =   255
         TabIndex        =   26
         Tag             =   "N�mero Direcci�n"
         Top             =   525
         Width           =   1215
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   3
         Left            =   1725
         TabIndex        =   25
         Tag             =   "N�mero Direcci�n"
         Top             =   525
         Width           =   1230
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   345
         HelpContextID   =   30104
         Index           =   6
         Left            =   255
         TabIndex        =   24
         Tag             =   "Puesto"
         Text            =   "     "
         Top             =   1140
         Width           =   4545
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Fecha Preferente"
         Height          =   900
         Index           =   5
         Left            =   7815
         TabIndex        =   19
         Top             =   300
         Width           =   2385
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   11
            Left            =   210
            TabIndex        =   21
            Tag             =   "N�mero Direcci�n"
            Top             =   420
            Width           =   1245
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   12
            Left            =   1530
            TabIndex        =   20
            Tag             =   "N�mero Direcci�n"
            Top             =   420
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   195
            TabIndex        =   23
            Top             =   225
            Width           =   300
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   1530
            TabIndex        =   22
            Top             =   210
            Width           =   420
         End
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   7
         Left            =   4980
         TabIndex        =   18
         Tag             =   "N�mero Direcci�n"
         Top             =   1140
         Width           =   2220
      End
      Begin VB.CheckBox chkCheck1 
         Caption         =   "Lista de Espera"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   4995
         TabIndex        =   17
         Tag             =   "Persona Provisional"
         Top             =   1830
         Width           =   1665
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Fecha Concertada"
         Height          =   960
         Index           =   4
         Left            =   7830
         TabIndex        =   12
         Top             =   1395
         Width           =   2385
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   14
            Left            =   1545
            TabIndex        =   14
            Tag             =   "N�mero Direcci�n"
            Top             =   435
            Width           =   660
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   13
            Left            =   195
            TabIndex        =   13
            Tag             =   "N�mero Direcci�n"
            Top             =   435
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   1530
            TabIndex        =   16
            Top             =   225
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   195
            TabIndex        =   15
            Top             =   225
            Width           =   300
         End
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   5
         Left            =   4980
         TabIndex        =   11
         Tag             =   "N�mero Direcci�n"
         Top             =   525
         Width           =   1170
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   4
         Left            =   3315
         TabIndex        =   10
         Tag             =   "N�mero Direcci�n"
         Top             =   525
         Width           =   1290
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   255
         TabIndex        =   9
         Tag             =   "Puesto"
         Top             =   1830
         Width           =   3270
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   10
         Left            =   3840
         TabIndex        =   8
         Tag             =   "Puesto"
         Top             =   2505
         Width           =   3350
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   255
         TabIndex        =   7
         Tag             =   "Puesto"
         Top             =   2490
         Width           =   3350
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�m.Act.Ped."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   255
         TabIndex        =   35
         Top             =   300
         Width           =   1185
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�m.Act.Plan."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   1725
         TabIndex        =   34
         Top             =   300
         Width           =   1230
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   255
         TabIndex        =   33
         Top             =   915
         Width           =   1020
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n Cita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   4980
         TabIndex        =   32
         Top             =   915
         Width           =   1200
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�m. Cita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   4980
         TabIndex        =   31
         Top             =   300
         Width           =   840
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�m. Solicitud"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   3315
         TabIndex        =   30
         Top             =   300
         Width           =   1245
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   255
         TabIndex        =   29
         Top             =   1590
         Width           =   1200
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Recurso Asignado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   3840
         TabIndex        =   28
         Top             =   2265
         Width           =   1560
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Recurso Preferente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   255
         TabIndex        =   27
         Top             =   2250
         Width           =   1665
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Petici�n"
      Height          =   2610
      Left            =   60
      TabIndex        =   0
      Top             =   90
      Width           =   10350
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   1
         Left            =   1635
         TabIndex        =   3
         Tag             =   "N�mero Direcci�n"
         Top             =   510
         Width           =   1170
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   0
         Left            =   195
         TabIndex        =   2
         Tag             =   "N�mero Petici�n"
         Top             =   510
         Width           =   1155
      End
      Begin IdPerson.IdPersona IdPersona1 
         Height          =   1500
         Left            =   210
         TabIndex        =   1
         Top             =   1020
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   2646
         BackColor       =   12648384
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   1620
         TabIndex        =   5
         Top             =   270
         Width           =   525
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�mero"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   195
         TabIndex        =   4
         Top             =   270
         Width           =   660
      End
   End
End
Attribute VB_Name = "frmDetalleCita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Me.Hide
End Sub


Private Sub Form_Load()
  Call IdPersona1.BeginControl(objApp, objGen)
  
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call IdPersona1.EndControl
End Sub
