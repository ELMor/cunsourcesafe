VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public lngSolicitud     As Long
Public cllPeticiones    As New Collection
Public strOpcion        As String    'Acci�n a realizar con la solicitud
                                     'constantes ciOpcxxxx en GENERAL.BAS

Public Sub RemovePeticiones(Optional lngPeticion As Long = -1)

'Borra todas las actuaciones de la peticion = lngPeticion
'Si lngActuacion = -1 borra todas las peticiones

  Dim objPeticion         As New clsSelPeticion
'Dim blnIfFound          As Boolean

  If lngPeticion = -1 Then
     For Each objPeticion In Me.cllPeticiones
       If objPeticion.lngPeticion = lngPeticion Or lngPeticion = -1 Then
         'objPeticion.RemoveActuaciones
         Call objGen.RemoveCollection(objPeticion.cllActuaciones)
       End If
     Next
     Call objGen.RemoveCollection(Me.cllPeticiones)
  Else
   'blnIfFound = False
    For Each objPeticion In Me.cllPeticiones
      If objPeticion.lngPeticion = lngPeticion Then
        objPeticion.RemoveActuaciones
        Call Me.cllPeticiones.Remove(Str(lngPeticion))
        'blnIfFound = True
      End If
    Next
   'If blnIfFound Then
   '   Call Me.cllPeticiones.Remove(Str(lngPeticion))
   'End If
   
   'If Me.cllPeticiones(Str(lngPeticion)) <> Empty Then
   '   Set objPeticion = Me.cllPeticiones(Str(lngPeticion))
   '   objPeticion.RemoveActuaciones
   '   Call Me.cllPeticiones.Remove(Str(lngPeticion))
   'End If
End If

Set objPeticion = Nothing

End Sub

Public Function AddNewPeticion(Optional lngPeticion As Long = 0) As clsSelPeticion

  Dim objPeticion         As New clsSelPeticion

  objPeticion.lngPeticion = lngPeticion
  Call Me.cllPeticiones.Add(objPeticion, Str(lngPeticion))
  Set AddNewPeticion = Me.cllPeticiones(Str(lngPeticion))
  Set objPeticion = Nothing

End Function

Public Function GetPeticion(ByVal lngPeticion As Long) As clsSelPeticion

  Dim objPeticion         As New clsSelPeticion
  Dim blnExistPeticion    As Boolean

  blnExistPeticion = False
  For Each objPeticion In Me.cllPeticiones
    If objPeticion.lngPeticion = lngPeticion Then
      blnExistPeticion = True
    End If
  Next

  If blnExistPeticion Then
    Set GetPeticion = Me.cllPeticiones(Str(lngPeticion))
  Else
   Set GetPeticion = Me.AddNewPeticion(lngPeticion)
  End If
  Set objPeticion = Nothing

End Function

