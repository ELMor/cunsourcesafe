Attribute VB_Name = "General"
Option Explicit

Public objCW As clsCW       ' referencia al objeto CodeWizard
Public objApp As clsCWApp
Public objPipe As clsCWPipeLine
Public objGen As clsCWGen
Public objEnv As clsCWEnvironment
Public objError As clsCWError

Public blnRefrescar As Boolean
Public blaAceptar As Boolean
Public strWherePersDup As String
Public vntCodPersDup As Variant
Public Const ciRecordaMail = 1
Public Const ciRecordaTfno = 2

'constantes para status de las actuaciones
Public Const ciPdteConfirmar    As String = "0"   'sin citar o citada sin confirmar
Public Const ciConfirmada       As String = "1"   'citada y confirmada
Public Const ciAnulada          As String = "2"   'citada y anulada
Public Const ciRecitada         As String = "3"   'citada y anulada por una recitaci�n
Public Const ciPdteRecitar      As String = "4"   'Pendiente de recitar
Public Const ciAllCitas         As String = "9"   'Todas las situaciones


Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  End
End Sub

'******************************************************************
'Funci�n que devuelve el siguiente n� de c�digo al �ltimo
'c�digo asignado.
'Tiene como par�metro de entrada una sentencia Sql que servira
'para crear el cursor
'******************************************************************

Public Function GetNewCode(strSql As String) As Long
 'Creaci�n de variables recordset
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  
 'Apertura del cursor
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

 'Establezco el n� de registros a recuperar a 1
  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
 'Devuelvo el nuevo n� de c�digo
  If rdoCodigo.RowCount = 0 Then
    GetNewCode = 1
  Else
    If IsNull(rdoCodigo(0)) Then
      GetNewCode = 1
    Else
      GetNewCode = rdoCodigo(0) + 1
    End If
  End If
 
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

'Devuelve una colecci�n de valores con las columnas de una tabla
'para la primera fila que cumple la condicion de strSql
Public Function GetTableColumn(strSql As String) As Collection
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  Dim intCol As Integer
  Dim cllValues As New Collection
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
  If rdoCodigo.RowCount = 0 Then
    For intCol = 0 To rdoCodigo.rdoColumns.Count - 1
       Call cllValues.Add("", rdoCodigo.rdoColumns(intCol).Name)
    Next
    
    'Set GetTableColumn = Nothing
  Else
    rdoCodigo.MoveFirst
    For intCol = 0 To rdoCodigo.rdoColumns.Count - 1
       If Not IsNull(rdoCodigo.rdoColumns(intCol).Value) Then
          Call cllValues.Add(rdoCodigo.rdoColumns(intCol).Value, rdoCodigo.rdoColumns(intCol).Name)
       Else
          Call cllValues.Add("", rdoCodigo.rdoColumns(intCol).Name)
       End If
    Next
  End If
 Set GetTableColumn = cllValues
 Set cllValues = Nothing
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

Public Sub ShowDetail(lngNumActPlan As Long, Optional lngNumSolicit As Long = -1, Optional lngNumCita As Long = -1)
  Dim strSql As String
  Dim cllActuacion As New Collection
  
  If lngNumSolicit = -1 And lngNumCita = -1 Then
    strSql = "SELECT  CI21CODPERSONA, PR09NUMPETICION, " _
         & " PR09NUMGRUPO, PR03NUMACTPEDI, AD02CODDPTO, " _
         & " PR01CODACTUACION, PR03FECPREFEREN, PR03NUMACTPEDI_PRI, " _
         & " PR04NUMACTPLAN, PR37CODESTADO, CI31NUMSOLICIT, " _
         & " CI01NUMCITA, CI01SITCITA, AG05NUMINCIDEN, " _
         & " CI01FECCONCERT, CI01INDLISESPE, AG11CODRECURSO_PRE, " _
         & " AG11CODRECURSO_ASI FROM PR0401J WHERE " _
         & " PR04NUMACTPLAN=" & lngNumActPlan
  Else
      strSql = "SELECT  CI21CODPERSONA, PR09NUMPETICION, " _
         & " PR09NUMGRUPO, PR03NUMACTPEDI, AD02CODDPTO, " _
         & " PR01CODACTUACION, PR03FECPREFEREN, PR03NUMACTPEDI_PRI, " _
         & " PR04NUMACTPLAN, PR37CODESTADO, CI31NUMSOLICIT, " _
         & " CI01NUMCITA, CI01SITCITA, AG05NUMINCIDEN, " _
         & " CI01FECCONCERT, CI01INDLISESPE, AG11CODRECURSO_PRE, " _
         & " AG11CODRECURSO_ASI FROM PR0401J WHERE " _
         & " CI31NUMSOLICIT=" & lngNumSolicit _
         & " AND CI01NUMCITA=" & lngNumCita
  End If
  Set cllActuacion = GetTableColumn(strSql)
  Load frmDetalleCita
  With frmDetalleCita
    .txtText1(0) = cllActuacion("PR09NUMPETICION")
    .txtText1(1) = cllActuacion("PR09NUMGRUPO")
    .txtText1(2) = cllActuacion("PR03NUMACTPEDI")
    .txtText1(3) = cllActuacion("PR04NUMACTPLAN")
    .txtText1(4) = cllActuacion("CI31NUMSOLICIT")
    .txtText1(5) = cllActuacion("CI01NUMCITA")
    .txtText1(11) = Format(cllActuacion("PR03FECPREFEREN"), "DD/MM/YYYY")
    .txtText1(12) = Format(cllActuacion("PR03FECPREFEREN"), "HH:MM")
    .txtText1(13) = Format(cllActuacion("CI01FECCONCERT"), "DD/MM/YYYY")
    .txtText1(14) = Format(cllActuacion("CI01FECCONCERT"), "HH:MM")
    If cllActuacion("AD02CODDPTO") <> "" Then
      strSql = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & cllActuacion("AD02CODDPTO")
      .txtText1(8) = GetTableColumn(strSql)("AD02DESDPTO")
    End If
    If cllActuacion("PR01CODACTUACION") <> "" Then
      strSql = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & cllActuacion("PR01CODACTUACION")
      .txtText1(6) = GetTableColumn(strSql)("PR01DESCORTA")
    End If
    If cllActuacion("AG11CODRECURSO_PRE") <> "" Then
      strSql = "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" & cllActuacion("AG11CODRECURSO_PRE")
      .txtText1(9) = GetTableColumn(strSql)("AG11DESRECURSO")
    End If
    If cllActuacion("AG11CODRECURSO_ASI") <> "" Then
      strSql = "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" & cllActuacion("AG11CODRECURSO_ASI")
      .txtText1(10) = GetTableColumn(strSql)("AG11DESRECURSO")
    End If
    .txtText1(15) = cllActuacion("AG05NUMINCIDEN")
    If cllActuacion("AG05NUMINCIDEN") <> "" Then
      strSql = "SELECT AG05DESINCIDEN FROM AG0500 WHERE AG05NUMINCIDEN=" & cllActuacion("AG05NUMINCIDEN")
      .txtText1(16) = GetTableColumn(strSql)("AG05DESINCIDEN")
    End If
    
    Select Case cllActuacion("CI01SITCITA")
      Case ciPdteConfirmar
        .txtText1(7) = "PENDIENTE CONFIRMAR"
      Case ciConfirmada
        .txtText1(7) = "CONFIRMADA"
      Case ciAnulada
        .txtText1(7) = "ANULADA"
      Case ciRecitada
        .txtText1(7) = "RECITADA"
      Case ciPdteRecitar
        .txtText1(7) = "PENDIENTE RECITAR"
      Case Else
        .txtText1(7) = ""
    End Select
    
    .chkCheck1(0).Value = Val(cllActuacion("CI01INDLISESPE"))
    .IdPersona1.Text = cllActuacion("CI21CODPERSONA")
   ' .IdPersona1.SearchPersona
    .Show (vbModal)
  End With
  Unload frmDetalleCita
End Sub
