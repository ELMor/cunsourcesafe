VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmFasesSolucion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Fases de la Actuaci�n"
   ClientHeight    =   5025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10110
   Icon            =   "CI1036.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5025
   ScaleWidth      =   10110
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraFrame1 
      Caption         =   "Recursos Citados"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1995
      Index           =   3
      Left            =   165
      TabIndex        =   1
      Top             =   2250
      Width           =   9765
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1440
         Index           =   1
         Left            =   210
         TabIndex        =   2
         Top             =   375
         Width           =   9375
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         FieldSeparator  =   ","
         Col.Count       =   9
         MultiLine       =   0   'False
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   9
         Columns(0).Width=   1852
         Columns(0).Caption=   "CodRec"
         Columns(0).Name =   "CodRec"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).Style=   1
         Columns(1).Width=   5927
         Columns(1).Caption=   "Recurso"
         Columns(1).Name =   "Recurso"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3016
         Columns(2).Caption=   "Fecha"
         Columns(2).Name =   "Fecha"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   1
         Columns(3).Width=   1349
         Columns(3).Caption=   "Hora"
         Columns(3).Name =   "Hora"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   1
         Columns(4).Width=   1270
         Columns(4).Caption=   "D�as"
         Columns(4).Name =   "D�as"
         Columns(4).Alignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   1138
         Columns(5).Caption=   "Horas"
         Columns(5).Name =   "Horas"
         Columns(5).Alignment=   2
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   1296
         Columns(6).Caption=   "Minutos"
         Columns(6).Name =   "Minutos"
         Columns(6).Alignment=   2
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "TipRec"
         Columns(7).Name =   "TipRec"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "Planificable"
         Columns(8).Name =   "Planificable"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Style=   2
         _ExtentX        =   16536
         _ExtentY        =   2540
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Fases Citadas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1980
      Index           =   2
      Left            =   180
      TabIndex        =   0
      Top             =   165
      Width           =   9765
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1440
         Index           =   0
         Left            =   195
         TabIndex        =   3
         Top             =   360
         Width           =   9360
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ","
         Col.Count       =   7
         MultiLine       =   0   'False
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   7
         Columns(0).Width=   1482
         Columns(0).Caption=   "NumFase"
         Columns(0).Name =   "NumFase"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   6615
         Columns(1).Caption=   "Descripci�n Fase"
         Columns(1).Name =   "Descripci�n Fase"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   2461
         Columns(2).Caption=   "Fecha"
         Columns(2).Name =   "Fecha"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   1
         Columns(3).Width=   1323
         Columns(3).Caption=   "Hora"
         Columns(3).Name =   "Hora"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   1
         Columns(4).Width=   1270
         Columns(4).Caption=   "D�as"
         Columns(4).Name =   "D�as"
         Columns(4).Alignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   1164
         Columns(5).Caption=   "Horas"
         Columns(5).Name =   "Horas"
         Columns(5).Alignment=   2
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   1270
         Columns(6).Caption=   "Minutos"
         Columns(6).Name =   "Minutos"
         Columns(6).Alignment=   2
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         _ExtentX        =   16510
         _ExtentY        =   2540
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdCommand1 
      Height          =   465
      Index           =   5
      Left            =   2340
      Picture         =   "CI1036.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   4440
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.CommandButton cmdCommand1 
      Height          =   465
      Index           =   4
      Left            =   160
      Picture         =   "CI1036.frx":0316
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   4440
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "&Cancelar"
      Height          =   465
      Index           =   3
      Left            =   5280
      TabIndex        =   7
      Top             =   4425
      Width           =   2190
   End
   Begin VB.CommandButton cmdCommand1 
      Height          =   465
      Index           =   2
      Left            =   700
      Picture         =   "CI1036.frx":0620
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   4440
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton cmdCommand1 
      Height          =   465
      Index           =   1
      Left            =   1520
      Picture         =   "CI1036.frx":092A
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   4440
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "&Aceptar"
      Height          =   465
      Index           =   0
      Left            =   7750
      TabIndex        =   4
      Top             =   4425
      Width           =   2190
   End
End
Attribute VB_Name = "frmFasesSolucion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public objActElegida As clsSelActuacion
Public objSolucion As clsSolucion
Public intSolucion As Integer
Public intTotSol  As Integer
Dim lngPosRecPreferente As Long

Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
'Comprobaci�n de Campos de Fases y Recursos
Private Function CheckColumns(intNumSol As Integer) As Boolean
  Dim intPos As Integer
  Dim objFase As clsFasesActuacion
  Dim objRec As clsRecursoFase
  
  'Fases
  grdDBGrid1(0).MoveFirst
  For intPos = 0 To grdDBGrid1(0).Rows - 1
    If Trim(grdDBGrid1(0).Columns("Fecha").Value) = "" Then
      Call objError.SetError(cwCodeMsg, "El campo Fecha de la Fase: " & grdDBGrid1(0).Columns("Descripci�n Fase").Value & " est� vacio")
      Call objError.Raise
      CheckColumns = False
      Exit Function
    End If
    If Trim(grdDBGrid1(0).Columns("Hora").Value) = "" Then
      Call objError.SetError(cwCodeMsg, "El campo Hora de la Fase: " & grdDBGrid1(0).Columns("Descripci�n Fase").Value & " est� vacio")
      Call objError.Raise
      CheckColumns = False
      Exit Function
    End If
    grdDBGrid1(0).MoveNext
  Next
  
  'Recursos
  grdDBGrid1(1).MoveFirst
  For intPos = 0 To grdDBGrid1(1).Rows - 1
    If Trim(grdDBGrid1(1).Columns("CodRec").Value) = "" Then
      Call objError.SetError(cwCodeMsg, "El campo C�digo de Recurso est� vacio")
      Call objError.Raise
      CheckColumns = False
      Exit Function
    End If
    If Trim(grdDBGrid1(1).Columns("Fecha").Value) = "" Then
      Call objError.SetError(cwCodeMsg, "El campo Fecha del Recurso: " & grdDBGrid1(1).Columns("Recurso").Value & " est� vacio")
      Call objError.Raise
      CheckColumns = False
      Exit Function
    End If
    If Trim(grdDBGrid1(1).Columns("Hora").Value) = "" Then
      Call objError.SetError(cwCodeMsg, "El campo Hora del Recurso: " & grdDBGrid1(1).Columns("Recurso").Value & " est� vacio")
      Call objError.Raise
      CheckColumns = False
      Exit Function
    End If
    grdDBGrid1(1).MoveNext
  Next
  
  Set objSolucion = objActElegida.GetSolucion(intNumSol)
  For Each objFase In objSolucion.cllFasesSolucion
    If Val(objFase.datFechaFase) = 0 Or _
       Not IsDate(objFase.datFechaFase) Then
      Call objError.SetError(cwCodeMsg, "El campo Fecha de la fase: " & objFase.strDesFase & " no tiene un valor v�lido.")
      Call objError.Raise
      CheckColumns = False
      Exit Function
    End If
    For Each objRec In objFase.cllRecursosFase
      If Val(objRec.lngCodRecursoFase) = 0 Then
        Call objError.SetError(cwCodeMsg, "El campo C�digo de Recurso  la Fase: " & objFase.strDesFase & " est� vacio.")
        Call objError.Raise
        CheckColumns = False
        Exit Function
      End If
      If Val(objRec.datFechaRecurso) = 0 Or _
       Not IsDate(objRec.datFechaRecurso) Then
        Call objError.SetError(cwCodeMsg, "El campo Fecha del Recurso: " & objRec.lngCodRecursoFase & " de la Fase: " & objFase.strDesFase & " est� vacio.")
        Call objError.Raise
        CheckColumns = False
        Exit Function
      End If
    Next
  Next
  CheckColumns = True

End Function
Public Sub RefrescarGridSolucion(datNuevaFecha As Date)
   Dim vntBookmark As Variant
   Dim strSolucionActual As String
   
   'Tendre que averiguar cual es la solucion sobre la
   'cual tengo que modificar la fecha
   If frmSolicitud.blnNoSolutions Then
     strSolucionActual = "Asignado"
   Else
     strSolucionActual = "Soluci�n " & intSolucion
   End If
   With frmSolicitud.grdSSDBGrid1(0)
     vntBookmark = .Bookmark
     .Redraw = False
     Select Case .Columns("Cabecera").Value
       
       Case "Recurso"
          If objActElegida.lngNumActPlan = CLng(frmSolicitud.grdSSDBGrid1(0).Columns("Indice").Value) Then
             .MoveNext
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "DD/MM/YYYY")
             .MoveNext
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "HH:NN")
             .Bookmark = vntBookmark
          Else
             .MovePrevious
             .MovePrevious
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "HH:NN")
             .MovePrevious
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "DD/MM/YYYY")
             .Bookmark = vntBookmark
          End If
          
        Case "Fecha"
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "DD/MM/YYYY")
             .MoveNext
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "HH:NN")
             .Bookmark = vntBookmark
        Case "Hora"
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "HH:NN")
             .MovePrevious
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "DD/MM/YYYY")
             .Bookmark = vntBookmark
        Case Else
             .MovePrevious
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "HH:NN")
             .MovePrevious
             .Columns(strSolucionActual).Value = Format(datNuevaFecha, "DD/MM/YYYY")
             .Bookmark = vntBookmark
     
     End Select
     .Redraw = True
  End With
End Sub
Public Sub RefrescarRecPreferente(lngNuevoRec As Long)
   Dim vntBookmark As Variant
   Dim strSolucionActual As String
   
   'Tendre que averiguar cual es la solucion sobre la
   'cual tengo que modificar la fecha
   If frmSolicitud.blnNoSolutions Then
     strSolucionActual = "Asignado"
   Else
     strSolucionActual = "Soluci�n " & intSolucion
   End If
   With frmSolicitud.grdSSDBGrid1(0)
     vntBookmark = .Bookmark
     .Redraw = False
     Select Case .Columns("Cabecera").Value
       
       Case "Recurso"
          
          
        Case "Fecha"
             .MovePrevious
             
        Case "Hora"
             
             .MovePrevious
             .MovePrevious
        Case Else
             .MovePrevious
             .MovePrevious
             .MovePrevious
             
     
     End Select
     .Columns(strSolucionActual).Value = Me.grdDBGrid1(1).Columns("Recurso").Value & Space(100) & "/#" & lngNuevoRec & "#/"
     .Bookmark = vntBookmark
     objActElegida.lngRecursoPref = lngNuevoRec
     .Redraw = True
  End With
End Sub

Sub LoadRecs(intNumFase As Integer)
  Dim objFase As clsFasesActuacion
  Dim objRec As clsRecursoFase
  
  grdDBGrid1(1).RemoveAll
  Set objFase = objSolucion.GetFaseSol(grdDBGrid1(0).Row)
  If objFase.intNumFase = intNumFase Then
    For Each objRec In objFase.cllRecursosFase
      
          grdDBGrid1(1).AddItem IIf(objRec.lngCodRecursoFase = 0, "", objRec.lngCodRecursoFase) & "," _
                          & GetTableColumn("SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" & objRec.lngCodRecursoFase)("AG11DESRECURSO") & "," _
                          & IIf(objRec.datFechaRecurso = 1, " ", Format(objRec.datFechaRecurso, "DD/MM/YYYY")) & "," _
                          & IIf(Format(objRec.datFechaRecurso, "HH:MM") = "00.00", " ", Format(objRec.datFechaRecurso, "HH:MM")) & "," _
                          & objRec.intNumDiasRec & "," _
                          & objRec.intNumHoraRec & "," _
                          & objRec.intNumMinRec & "," & objRec.lngTipRec
      
    Next
  End If
End Sub

Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim objFase As clsFasesActuacion
  
  If intIndex = 0 Then
    If CheckColumns(intSolucion) Then
      frmSolicitud.cmdCitar(0).Enabled = True
      frmSolicitud.cmdConfirmarSolicitud(0).Enabled = True
      frmSolicitud.blnSecondTime = True
      Set objActElegida.cllFasesActuacion = objSolucion.cllFasesSolucion
      Me.Hide
      blnAceptar = True
    End If
  End If
  
  If intIndex = 1 Then
    If CheckColumns(intSolucion) Then
      intSolucion = intSolucion + 1
    
      Set objSolucion = objActElegida.GetSolucion(intSolucion)
      grdDBGrid1(0).RemoveAll
      grdDBGrid1(1).RemoveAll
    
      If objSolucion.cllFasesSolucion.Count > 0 Then
        For Each objFase In objSolucion.cllFasesSolucion
          Me.grdDBGrid1(0).AddItem objFase.intNumFase & "," _
                          & objFase.strDesFase & "," _
                          & Format(objFase.datFechaFase, "DD/MM/YYYY") & "," _
                          & Format(objFase.datFechaFase, "HH:MM") & "," _
                          & objFase.intNumDiasPac & "," _
                          & objFase.intNumHoraPac & "," _
                          & objFase.intNumMinPac
        Next
         
        Call grdDBGrid1_Click(0)
      End If
      If intSolucion = intTotSol Then
        cmdCommand1(1).Enabled = False
        cmdCommand1(5).Enabled = False
      End If
      cmdCommand1(2).Enabled = True
      cmdCommand1(4).Enabled = True
    End If
   
  End If
  
  If intIndex = 2 Then
    If CheckColumns(intSolucion) Then
      intSolucion = intSolucion - 1
      intSolucion = IIf(intSolucion < 1, 1, intSolucion)
      Set objSolucion = objActElegida.GetSolucion(intSolucion)
      grdDBGrid1(0).RemoveAll
      grdDBGrid1(1).RemoveAll
    
      If objSolucion.cllFasesSolucion.Count > 0 Then
        For Each objFase In objSolucion.cllFasesSolucion
          Me.grdDBGrid1(0).AddItem objFase.intNumFase & "," _
                          & objFase.strDesFase & "," _
                          & Format(objFase.datFechaFase, "DD/MM/YYYY") & "," _
                          & Format(objFase.datFechaFase, "HH:MM") & "," _
                          & objFase.intNumDiasPac & "," _
                          & objFase.intNumHoraPac & "," _
                          & objFase.intNumMinPac
        Next
         
        Call grdDBGrid1_Click(0)
      End If
      If intSolucion = 1 Then
        cmdCommand1(2).Enabled = False
        cmdCommand1(4).Enabled = False
      End If
      cmdCommand1(1).Enabled = True
      cmdCommand1(5).Enabled = True
    End If
  End If
  If intIndex = 3 Then
    frmSolicitud.blnSecondTime = True
    Me.Hide
    blnAceptar = False
  End If
    
  If intIndex = 4 Then
    If CheckColumns(intSolucion) Then
      intSolucion = 1
    
      Set objSolucion = objActElegida.GetSolucion(intSolucion)
      grdDBGrid1(0).RemoveAll
      grdDBGrid1(1).RemoveAll
    
      If objSolucion.cllFasesSolucion.Count > 0 Then
        For Each objFase In objSolucion.cllFasesSolucion
          Me.grdDBGrid1(0).AddItem objFase.intNumFase & "," _
                          & objFase.strDesFase & "," _
                          & Format(objFase.datFechaFase, "DD/MM/YYYY") & "," _
                          & Format(objFase.datFechaFase, "HH:MM") & "," _
                          & objFase.intNumDiasPac & "," _
                          & objFase.intNumHoraPac & "," _
                          & objFase.intNumMinPac
        Next
         
        Call grdDBGrid1_Click(0)
      End If
    
      cmdCommand1(2).Enabled = False
      cmdCommand1(4).Enabled = False
    
      cmdCommand1(1).Enabled = True
      cmdCommand1(5).Enabled = True
    End If
  End If
  If intIndex = 5 Then
    If CheckColumns(intSolucion) Then
      intSolucion = intTotSol
    
      Set objSolucion = objActElegida.GetSolucion(intSolucion)
      grdDBGrid1(0).RemoveAll
      grdDBGrid1(1).RemoveAll
    
      If objSolucion.cllFasesSolucion.Count > 0 Then
        For Each objFase In objSolucion.cllFasesSolucion
         Me.grdDBGrid1(0).AddItem objFase.intNumFase & "," _
                          & objFase.strDesFase & "," _
                          & Format(objFase.datFechaFase, "DD/MM/YYYY") & "," _
                          & Format(objFase.datFechaFase, "HH:MM") & "," _
                          & objFase.intNumDiasPac & "," _
                          & objFase.intNumHoraPac & "," _
                          & objFase.intNumMinPac
       Next
         
       Call grdDBGrid1_Click(0)
      End If
    
      cmdCommand1(1).Enabled = False
      cmdCommand1(5).Enabled = False
    
      cmdCommand1(2).Enabled = True
      cmdCommand1(4).Enabled = True
    End If
  End If
  
  
  
  
  
  Me.Caption = "CITAS. Fases de la Actuaci�n" & ": " & objActElegida.strDescripci�n & " (Soluci�n " & intSolucion & ")"
  Set objFase = Nothing
End Sub


Private Sub Form_Activate()
  Call grdDBGrid1(0).SetFocus
End Sub

Public Sub grdDBGrid1_BtnClick(intIndex As Integer)
  Dim objField As clsCWFieldSearch
  Dim strSql As String
  Dim rsRecurso As rdoResultset
  Dim objGetDate As New clsCWGetDate
  Dim intPos As Integer
  Dim vntBookmark As Variant
  Dim intRow As Integer
  Dim intCol As Integer
  'Si la columna activa es la de fecha muestro la ventana de
  'selecci�n de fecha
  If grdDBGrid1(intIndex).Col = 2 Then
    If objGetDate.GetDate(grdDBGrid1(intIndex).Columns("Fecha").Value) Then
      grdDBGrid1(intIndex).Columns("Fecha").Value = Format(objGetDate.strDate, "DD/MM/YYYY")
      Call grdDBGrid1_Change(intIndex)
    End If

    Exit Sub
  End If
  If grdDBGrid1(intIndex).Col = 3 Then
      Load frmHora
      
      If grdDBGrid1(intIndex).Columns("Hora").Value <> "" Then
        frmHora.strHora = grdDBGrid1(intIndex).Columns("Hora").Value
      End If
      frmHora.Show vbModal
      If frmHora.strHora <> "" Then
        grdDBGrid1(intIndex).Columns("Hora").Value = frmHora.strHora
      End If
      Unload frmHora
      Set frmHora = Nothing
      Call grdDBGrid1_Change(intIndex)
    

    Exit Sub
  End If
  
  'Si la columna activa es la del c�digo de recurso muestro la
  'lista de valores de recursos
  If intIndex = 1 Then
        
    
      Set objSearch = New clsCWSearch
      
      With objSearch
       .strTable = "AG1100"
       If frmSolicitud.blnNoSolutions Then
         .strWhere = "WHERE AG14CODTIPRECU=" & Val(grdDBGrid1(intIndex).Columns("TipRec").Value)
       Else
         strSql = "SELECT AG14CODTIPRECU FROM AG1100 WHERE AG11CODRECURSO=" & grdDBGrid1(intIndex).Columns("CodRec").Value
         Set rsRecurso = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
         If Not rsRecurso.EOF Then
           .strWhere = "WHERE AG14CODTIPRECU=" & rsRecurso(0)
           rsRecurso.Close
         End If
       End If
       .strOrder = ""
      
       Set objField = .AddField("AG11CODRECURSO")
       objField.strSmallDesc = "C�DIGO RECURSO"
       Set objField = .AddField("AG11DESRECURSO")
       objField.strSmallDesc = "DESCRIPCI�N               "
       Set objField = .AddField("AG11INDPLANIFI")
       objField.blnInGrid = False
       objField.blnInDialog = False
       If .Search Then
          intCol = grdDBGrid1(intIndex).Col
          vntBookmark = grdDBGrid1(intIndex).Bookmark
          intRow = grdDBGrid1(intIndex).Row
          grdDBGrid1(intIndex).MoveFirst
          For intPos = 0 To grdDBGrid1(intIndex).Rows - 1
            If intPos <> intRow Then
              If grdDBGrid1(intIndex).Columns("CodRec").Value = .cllValues("AG11CODRECURSO") Then
                Call objError.SetError(cwCodeMsg, "El Recurso elegido ya est� seleccionado")
                Call objError.Raise
                Exit Sub
              End If
            End If
            grdDBGrid1(intIndex).MoveNext
          Next intPos
          grdDBGrid1(intIndex).Bookmark = vntBookmark
          grdDBGrid1(intIndex).Columns("CodRec").Value = .cllValues("AG11CODRECURSO")
          grdDBGrid1(intIndex).Columns("Recurso").Value = .cllValues("AG11DESRECURSO")
          grdDBGrid1(intIndex).Columns("Planificable").Value = -.cllValues("AG11INDPLANIFI")
          grdDBGrid1(intIndex).Col = intCol
          Call grdDBGrid1_Change(intIndex)
       End If
      End With
      Set objSearch = Nothing
   
  End If
End Sub
Private Sub grdDBGrid1_Change(intIndex As Integer)
  Dim objFaseCambiada As clsFasesActuacion
  Dim objRecCambiado As clsRecursoFase
  Dim intPos As Integer
  
  'Si se cambia algo de la fase lo reflejo en la
  'estructura de objetos
  On Error Resume Next
  If intIndex = 0 Then
    If grdDBGrid1(0).Col = 2 Or grdDBGrid1(0).Col = 3 Then
      Set objFaseCambiada = objSolucion.GetFaseSol(grdDBGrid1(intIndex).Row)
      If objFaseCambiada Is Nothing Then
        'No se ha encontrado la fase
      Else
        If IsDate(grdDBGrid1(0).Columns("Fecha").Value) _
            Or IsDate(grdDBGrid1(0).Columns("Hora").Value) Then
          If IsDate(grdDBGrid1(0).Columns("Fecha").Value) And IsDate(grdDBGrid1(0).Columns("Hora").Value) Then
            objFaseCambiada.datFechaFase = CDate(grdDBGrid1(0).Columns("Fecha").Value & " " & grdDBGrid1(0).Columns("Hora").Value)
          Else
            If IsDate(grdDBGrid1(0).Columns("Hora").Value) Then
              objFaseCambiada.datFechaFase = CDate(grdDBGrid1(0).Columns("Hora").Value)
            Else
              objFaseCambiada.datFechaFase = CDate(grdDBGrid1(0).Columns("Fecha").Value)
            End If
          End If
          If grdDBGrid1(0).Row = 0 Then
            Call RefrescarGridSolucion(objFaseCambiada.datFechaFase)
          End If
        End If
        grdDBGrid1(1).MoveFirst
        For intPos = 0 To grdDBGrid1(1).Rows - 1
           grdDBGrid1(1).Columns("Fecha").Value = grdDBGrid1(0).Columns("Fecha").Value
           grdDBGrid1(1).Columns("Hora").Value = grdDBGrid1(0).Columns("Hora").Value
          
            Set objRecCambiado = objFaseCambiada.GetRecurso(grdDBGrid1(1).Row)
            If objRecCambiado Is Nothing Then
              'No se ha encontrado el recurso
            Else
              If IsDate(grdDBGrid1(1).Columns("Fecha").Value) _
                    Or IsDate(grdDBGrid1(1).Columns("Hora").Value) Then
               If IsDate(grdDBGrid1(1).Columns("Fecha").Value) And IsDate(grdDBGrid1(1).Columns("Hora").Value) Then
                 objRecCambiado.datFechaRecurso = CDate(grdDBGrid1(1).Columns("Fecha").Value & " " & grdDBGrid1(1).Columns("Hora").Value)
               Else
                 If IsDate(grdDBGrid1(1).Columns("Hora").Value) Then
                   objRecCambiado.datFechaRecurso = CDate(grdDBGrid1(1).Columns("Hora").Value)
                 Else
                   objRecCambiado.datFechaRecurso = CDate(grdDBGrid1(1).Columns("Fecha").Value)
                 End If
               End If
              End If
            End If
          
          grdDBGrid1(1).MoveNext
        Next intPos
      End If
    End If
  End If
  
  'Si se cambia alg�n recurso lo reflejo en la
  'estructura de objetos
  If intIndex = 1 Then
    If grdDBGrid1(1).Col = 2 Or grdDBGrid1(1).Col = 3 Then
      Set objFaseCambiada = objSolucion.GetFaseSol(grdDBGrid1(0).Row)
      If objFaseCambiada Is Nothing Then
        'No se ha encontrado la fase
      Else
        Set objRecCambiado = objFaseCambiada.GetRecurso(grdDBGrid1(1).Row)
        If objFaseCambiada Is Nothing Then
        'No se ha encontrado el recurso
        Else
          If IsDate(grdDBGrid1(1).Columns("Fecha").Value) _
                    Or IsDate(grdDBGrid1(1).Columns("Hora").Value) Then
               If IsDate(grdDBGrid1(1).Columns("Fecha").Value) And IsDate(grdDBGrid1(1).Columns("Hora").Value) Then
                 objRecCambiado.datFechaRecurso = CDate(grdDBGrid1(1).Columns("Fecha").Value & " " & grdDBGrid1(1).Columns("Hora").Value)
               Else
                 If IsDate(grdDBGrid1(1).Columns("Hora").Value) Then
                   objRecCambiado.datFechaRecurso = CDate(grdDBGrid1(1).Columns("Hora").Value)
                 Else
                   objRecCambiado.datFechaRecurso = CDate(grdDBGrid1(1).Columns("Fecha").Value)
                 End If
               End If
              End If

        End If
      End If
    End If
    If grdDBGrid1(1).Col = 0 Then
      Set objFaseCambiada = objSolucion.GetFaseSol(grdDBGrid1(0).Row)
      If objFaseCambiada Is Nothing Then
        'No se ha encontrado la fase
      Else
        Set objRecCambiado = objFaseCambiada.GetRecurso(grdDBGrid1(1).Row)
        If objRecCambiado Is Nothing Then
        'No se ha encontrado el recurso
        Else
          objRecCambiado.lngCodRecursoFase = grdDBGrid1(1).Columns("CodRec").Value
          If grdDBGrid1(0).Row = objActElegida.intNumFaseRecPref And grdDBGrid1(1).Row = lngPosRecPreferente Then
            Call RefrescarRecPreferente(objRecCambiado.lngCodRecursoFase)
            objSolucion.lngRecPrefSol = objRecCambiado.lngCodRecursoFase
            If frmSolicitud.blnNoSolutions Then
               objActElegida.lngRecurso = objSolucion.lngRecPrefSol
            End If
          End If
        End If
      End If
    End If
  
  End If
  Set objFaseCambiada = Nothing
  Set objRecCambiado = Nothing


End Sub

Public Sub grdDBGrid1_Click(intIndex As Integer)
  If intIndex = 0 And grdDBGrid1(0).Columns("NumFase").Value <> "" Then
    Call LoadRecs(grdDBGrid1(0).Columns("NumFase").Value)
    
  End If
End Sub

Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  If intIndex = 1 Then
       
    If frmSolicitud.blnNoSolutions Then
      If Val(grdDBGrid1(1).Columns("CodRec").Value) = 0 Then
        grdDBGrid1(1).Columns("CodRec").Value = ""
      End If
      
      If objSolucion.lngRecPrefSol = Val(grdDBGrid1(1).Columns("TipRec").Value) Then
        lngPosRecPreferente = grdDBGrid1(1).Row
      End If
    Else
      If objSolucion.lngRecPrefSol = Val(grdDBGrid1(1).Columns("CodRec").Value) Then
        lngPosRecPreferente = grdDBGrid1(1).Row
      End If
    End If
   ' If frmSolicitud.blnNoSolutions Then
   '   grdDBGrid1(1).Columns("TipRec").Value = grdDBGrid1(1).Columns("CodRec").Value
   '   grdDBGrid1(1).Columns("CodRec").Value = ""
   '   grdDBGrid1(1).Columns("Fecha").Value = ""
   '   grdDBGrid1(1).Columns("Hora").Value = ""
   '   grdDBGrid1(1).Columns("Recurso").Value = ""
      
   ' End If
  End If

  'If intIndex = 0 Then
  '  If frmSolicitud.blnNoSolutions Then
  '    grdDBGrid1(0).Columns("Fecha").Value = ""
  '    grdDBGrid1(0).Columns("Hora").Value = ""
  '  End If
    
  'End If

End Sub
