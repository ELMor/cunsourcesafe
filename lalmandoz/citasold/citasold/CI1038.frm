VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmVerSolicitud 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Citas Coordinadas"
   ClientHeight    =   3735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10095
   ClipControls    =   0   'False
   Icon            =   "CI1038.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3735
   ScaleWidth      =   10095
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Anular &Solicitud"
      Height          =   465
      Index           =   2
      Left            =   7800
      TabIndex        =   1
      Top             =   3150
      Width           =   2175
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   2355
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   9855
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      FieldSeparator  =   ","
      Col.Count       =   4
      AllowUpdate     =   0   'False
      SelectTypeCol   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   4577
      Columns(0).Caption=   "Paciente"
      Columns(0).Name =   "Paciente"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6033
      Columns(1).Caption=   "Actuaci�n"
      Columns(1).Name =   "Actuaci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   4048
      Columns(2).Caption=   "Departamento"
      Columns(2).Name =   "Departamento"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "Fecha"
      Columns(3).Name =   "Fecha"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   17383
      _ExtentY        =   4154
      _StockProps     =   79
      Caption         =   "ACTUACIONES COORDINADAS"
      BackColor       =   -2147483637
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "&Cancelar"
      Height          =   465
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   3150
      Width           =   2190
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "&Anular Actuaci�n"
      Height          =   465
      Index           =   0
      Left            =   5430
      TabIndex        =   0
      Top             =   3150
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "La Actuaci�n que va a borrar est� coordinada con las siguientes Actuaciones. Pulse  Aceptar para borrar la actuaci�n."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   9855
   End
End
Attribute VB_Name = "frmVerSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public blnAnularAct As Boolean
Public blnAnularSol As Boolean
Public blnCancelar As Boolean

Private Sub cmdCommand1_Click(intIndex As Integer)
  If intIndex = 0 Then
    blnAnularAct = True
  Else
    If intIndex = 2 Then
      blnAnularSol = True
    Else
      blnCancelar = True
    End If
  End If
  Me.Hide
End Sub

Private Sub Form_Load()
  Dim rdoCitas As rdoResultset
  Dim rdoActuacion As rdoResultset
  Dim strSql As String
  Dim cllPersona As New Collection
  Dim strPersona As String
  Dim strActuacion As String
  Dim strDepartamento As String
  
  SSDBGrid1.BackColorEven = objApp.objUserColor.lngReadOnly
  SSDBGrid1.BackColorOdd = objApp.objUserColor.lngReadOnly
  
  strSql = "SELECT PR04NUMACTPLAN,CI01FECCONCERT FROM CI0100 WHERE CI31NUMSOLICIT=" & lngNumSolicitud
  Set rdoCitas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
  While Not rdoCitas.EOF And rdoCitas.RowCount > 1
      strSql = "SELECT PR01CODACTUACION,AD02CODDPTO,CI21CODPERSONA FROM PR0400 WHERE PR04NUMACTPLAN=" & rdoCitas(0)
      Set rdoActuacion = objApp.rdoConnect.OpenResultset(strSql)
      If Not rdoActuacion.EOF Then
         strActuacion = GetTableColumn("SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & rdoActuacion(0).Value)("PR01DESCORTA")
         Set cllPersona = GetTableColumn("SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI21CODPERSONA=" & rdoActuacion(2).Value)
         strPersona = Trim(cllPersona("CI22NOMBRE") & " " & cllPersona("CI22PRIAPEL") & " " & cllPersona("CI22SEGAPEL"))
         strDepartamento = GetTableColumn("SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rdoActuacion(1).Value)("AD02DESDPTO")
         rdoActuacion.Close
         
      End If
      SSDBGrid1.AddItem strPersona & "," & strActuacion & "," & strDepartamento & "," & Format(rdoCitas(1), "DD/MM/YYYY HH:NN")
      rdoCitas.MoveNext
   Wend
  Set rdoCitas = Nothing
  Set rdoActuacion = Nothing
  If SSDBGrid1.Rows > 0 Then
    blnAnularAct = False
  Else
    blnAnularAct = True
  End If
End Sub

