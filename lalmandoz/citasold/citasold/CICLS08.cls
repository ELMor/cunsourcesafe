VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsRecursoFase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public intKeyRec As Integer
Public lngCodRecursoFase As Long
Public datFechaRecurso As Date
Public intNumDiasRec As Integer
Public intNumHoraRec As Integer
Public intNumMinRec As Integer
Public lngTipRec    As Long
