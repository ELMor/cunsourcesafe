VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmFasesPCR 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Fases Actuaci�n"
   ClientHeight    =   5775
   ClientLeft      =   975
   ClientTop       =   1695
   ClientWidth     =   9540
   ControlBox      =   0   'False
   Icon            =   "CI1027.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5775
   ScaleWidth      =   9540
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   9540
      _ExtentX        =   16828
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin TabDlg.SSTab tabSSTab1 
      Height          =   4950
      Index           =   0
      Left            =   75
      TabIndex        =   15
      Top             =   510
      Width           =   9405
      _ExtentX        =   16589
      _ExtentY        =   8731
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "&Pedidas"
      TabPicture(0)   =   "CI1027.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraFrame1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "C&itadas"
      TabPicture(1)   =   "CI1027.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(2)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "fraFrame1(3)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      Begin VB.Frame fraFrame1 
         Caption         =   "Recursos Citados"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1920
         Index           =   3
         Left            =   -74865
         TabIndex        =   45
         Top             =   2880
         Width           =   9180
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1440
            Index           =   3
            Left            =   210
            TabIndex        =   46
            Top             =   375
            Width           =   8775
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   15478
            _ExtentY        =   2540
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Fases Citadas"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2475
         Index           =   2
         Left            =   -74865
         TabIndex        =   26
         Top             =   405
         Width           =   9165
         Begin TabDlg.SSTab tabSSTab1 
            Height          =   1935
            Index           =   2
            Left            =   200
            TabIndex        =   27
            Top             =   435
            Width           =   8895
            _ExtentX        =   15690
            _ExtentY        =   3413
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            Tab             =   1
            TabHeight       =   520
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "CI1027.frx":0044
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "lblLabel1(6)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(10)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(11)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(12)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(39)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(9)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(8)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(7)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(10)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "Frame1"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(14)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).ControlCount=   11
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "CI1027.frx":0060
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "grdDBGrid1(2)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI15FECCONCPAC"
               Height          =   330
               HelpContextID   =   40101
               Index           =   14
               Left            =   -74760
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "Fecha Concertada"
               Top             =   1200
               Width           =   1860
            End
            Begin VB.Frame Frame1 
               Caption         =   "Tiempo Ocupaci�n"
               Height          =   780
               Left            =   -72255
               TabIndex        =   34
               Top             =   945
               Width           =   5730
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "CI15NUMDIASPAC"
                  Height          =   330
                  HelpContextID   =   40101
                  Index           =   11
                  Left            =   1995
                  TabIndex        =   10
                  TabStop         =   0   'False
                  Tag             =   "D�as Ocupaci�n"
                  Top             =   330
                  Width           =   645
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "CI15NUMMINUPAC"
                  Height          =   330
                  HelpContextID   =   40101
                  Index           =   13
                  Left            =   4050
                  TabIndex        =   12
                  TabStop         =   0   'False
                  Tag             =   "Minutos Ocupaci�n"
                  Top             =   330
                  Width           =   645
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "CI15NUMHORAPAC"
                  Height          =   330
                  HelpContextID   =   40101
                  Index           =   12
                  Left            =   3000
                  TabIndex        =   11
                  TabStop         =   0   'False
                  Tag             =   "Horas Ocupaci�n"
                  Top             =   330
                  Width           =   645
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dias"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   13
                  Left            =   1980
                  TabIndex        =   37
                  Top             =   135
                  Width           =   390
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Horas"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   8
                  Left            =   2985
                  TabIndex        =   36
                  Top             =   135
                  Width           =   510
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Minutos"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   7
                  Left            =   4020
                  TabIndex        =   35
                  Top             =   135
                  Width           =   675
               End
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI15DESFASECITA"
               Height          =   330
               HelpContextID   =   40102
               Index           =   10
               Left            =   -70965
               TabIndex        =   8
               Tag             =   "Descripci�n"
               Top             =   555
               Width           =   4455
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "CI31NUMSOLICIT"
               Height          =   330
               HelpContextID   =   40101
               Index           =   7
               Left            =   -74775
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "N�mero Solicitud"
               Top             =   555
               Width           =   915
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "CI01NUMCITA"
               Height          =   330
               HelpContextID   =   40101
               Index           =   8
               Left            =   -73605
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "N�mero Cita"
               Top             =   555
               Width           =   1020
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI15NUMFASECITA"
               Height          =   330
               HelpContextID   =   40101
               Index           =   9
               Left            =   -72270
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "N�mero Fase"
               Top             =   540
               Width           =   1020
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1440
               Index           =   2
               Left            =   90
               TabIndex        =   28
               Top             =   435
               Width           =   8685
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowRowSizing  =   0   'False
               SelectTypeRow   =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15319
               _ExtentY        =   2540
               _StockProps     =   79
               ForeColor       =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Concertada"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   39
               Left            =   -74775
               TabIndex        =   33
               Top             =   945
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   -70950
               TabIndex        =   32
               Top             =   315
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Solicitud"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   -74775
               TabIndex        =   31
               Top             =   315
               Width           =   750
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Cita"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   -73590
               TabIndex        =   30
               Top             =   315
               Width           =   345
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fase "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   -72285
               TabIndex        =   29
               Top             =   345
               Width           =   480
            End
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Recursos Pedidos"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1920
         Index           =   1
         Left            =   105
         TabIndex        =   17
         Top             =   2895
         Width           =   9180
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1440
            Index           =   1
            Left            =   200
            TabIndex        =   18
            Top             =   375
            Width           =   8775
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   15478
            _ExtentY        =   2540
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Fases Pedidas"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2475
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   405
         Width           =   9165
         Begin TabDlg.SSTab tabSSTab1 
            Height          =   1935
            Index           =   1
            Left            =   195
            TabIndex        =   19
            Top             =   420
            Width           =   8850
            _ExtentX        =   15610
            _ExtentY        =   3413
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            Tab             =   1
            TabHeight       =   520
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "CI1027.frx":007C
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "lblLabel1(9)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(1)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(0)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(2)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(5)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(2)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(0)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(1)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(3)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(6)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "Frame2"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).ControlCount=   11
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "CI1027.frx":0098
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            Begin VB.Frame Frame2 
               Caption         =   "Desfase"
               Height          =   885
               Left            =   -74835
               TabIndex        =   38
               Top             =   900
               Width           =   8490
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "D�as H�biles"
                  DataField       =   "PR06INDHABNATU"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   0
                  Left            =   4095
                  TabIndex        =   44
                  Tag             =   "Indicador D�as H�biles"
                  Top             =   150
                  Width           =   1455
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Desfase Respecto Inicio Fase Previa"
                  DataField       =   "PR06INDINIFIN"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   1
                  Left            =   4095
                  TabIndex        =   43
                  Tag             =   "Indicador Desfase"
                  Top             =   525
                  Width           =   3975
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR06NUMMAXFPRE"
                  Height          =   330
                  HelpContextID   =   40101
                  Index           =   5
                  Left            =   2115
                  TabIndex        =   40
                  TabStop         =   0   'False
                  Tag             =   "Desfase M�ximo"
                  Top             =   405
                  Width           =   660
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "PR06NUMMINFPRE"
                  Height          =   330
                  HelpContextID   =   40101
                  Index           =   4
                  Left            =   1110
                  TabIndex        =   39
                  TabStop         =   0   'False
                  Tag             =   "Desfase M�nimo"
                  Top             =   405
                  Width           =   690
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "M�ximo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   3
                  Left            =   2100
                  TabIndex        =   42
                  Top             =   180
                  Width           =   645
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "M�nimo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   4
                  Left            =   1125
                  TabIndex        =   41
                  Top             =   180
                  Width           =   630
               End
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR06NUMFASE_PRE"
               Height          =   330
               HelpContextID   =   40101
               Index           =   6
               Left            =   -67425
               TabIndex        =   4
               TabStop         =   0   'False
               Tag             =   "Fase Previa"
               Top             =   570
               Width           =   1020
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR06NUMMINOCUPAC"
               Height          =   330
               HelpContextID   =   40101
               Index           =   3
               Left            =   -68715
               TabIndex        =   3
               TabStop         =   0   'False
               Tag             =   "Tiempo Min. Ocupaci�n"
               Top             =   555
               Width           =   1020
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "PR06NUMFASE"
               Height          =   330
               HelpContextID   =   40101
               Index           =   1
               Left            =   -73755
               TabIndex        =   1
               TabStop         =   0   'False
               Tag             =   "N�mero Fase"
               Top             =   525
               Width           =   1020
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "PR03NUMACTPEDI"
               Height          =   330
               HelpContextID   =   40101
               Index           =   0
               Left            =   -74805
               TabIndex        =   0
               TabStop         =   0   'False
               Tag             =   "C�digo Actuaci�n"
               Top             =   525
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR06DESFASE"
               Height          =   330
               HelpContextID   =   40102
               Index           =   2
               Left            =   -72570
               TabIndex        =   2
               Tag             =   "Descripci�n"
               Top             =   540
               Width           =   3675
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1440
               Index           =   0
               Left            =   90
               TabIndex        =   20
               Top             =   420
               Width           =   8655
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               AllowUpdate     =   0   'False
               AllowRowSizing  =   0   'False
               SelectTypeRow   =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15266
               _ExtentY        =   2540
               _StockProps     =   79
               ForeColor       =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fase Previa"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   -67410
               TabIndex        =   25
               Top             =   375
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fase"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   -73725
               TabIndex        =   24
               Top             =   345
               Width           =   420
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Actuaci�n "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   -74805
               TabIndex        =   23
               Top             =   330
               Width           =   930
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   -72555
               TabIndex        =   22
               Top             =   345
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "T. M�n. Ocup"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   9
               Left            =   -68730
               TabIndex        =   21
               Top             =   360
               Width           =   1155
            End
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   13
      Top             =   5490
      Width           =   9540
      _ExtentX        =   16828
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmFasesPCR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public blnDataReadOnly As Boolean

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form multilinea
  Dim objDetailInfo1 As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim objDetailInfo2 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  
  Dim strSql As String
  Dim rdoActPlanif As rdoResultset
  Dim rdoActuacion As rdoResultset
  Dim rdoDepartamento As rdoResultset
' **********************************
' Fin declaraci�n de variables
' **********************************
  
  strSql = "SELECT  AD02CODDPTO, PR01CODACTUACION FROM PR0300 WHERE PR03NUMACTPEDI=" & vntNumActPedi
  Set rdoActPlanif = objApp.rdoConnect.OpenResultset(strSql) ', rdOpenKeyset, rdConcurRowVer)
  If rdoActPlanif.EOF Then
    MsgBox "N�mero de Actuaci�n Pedida incorrecto"
    'End
    Me.Hide
  Else
    strSql = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & rdoActPlanif("PR01CODACTUACION")
    Set rdoActuacion = objApp.rdoConnect.OpenResultset(strSql) ', rdOpenKeyset, rdConcurRowVer)
    strSql = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rdoActPlanif("AD02CODDPTO")
    Set rdoDepartamento = objApp.rdoConnect.OpenResultset(strSql) ', rdOpenKeyset, rdConcurRowVer)
    If Not rdoActuacion.EOF And Not rdoDepartamento.EOF Then
      Me.Caption = "CITAS. Fases de la Actuaci�n: " & rdoActuacion(0) & " -- Departamento: " & rdoDepartamento(0)
    End If
  End If

' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  


' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Consulta de Fases por actuaci�n planificada"
    .cwDAT = "14-10-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite consultar las fases pedidas,citadas o realizadas de una actuaci�n planificada"
    
    .cwUPD = "14-10-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
'******************************************************************
'******************************************************************
'**************   FASES PEDIDAS  **********************************
'******************************************************************
'******************************************************************

' Declaraci�n de las caracter�sticas del form multilinea
  With objDetailInfo1
  
' Asignaci�n del nombre del form
    .strName = "FasePedida"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabSSTab1(1)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
' Asignaci�n de la tabla asociada al form
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0600"
    .strWhere = "PR03NUMACTPEDI=" & vntNumActPedi
    .intAllowance = cwAllowReadOnly
' M�todo de ordenacion del form
    Call .FormAddOrderField("PR06NUMFASE", cwAscending)
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
' Creaci�n de los filtros de busqueda
'    Call .FormCreateFilterWhere(strKey, "Tabla de Clientes")
'    Call .FormAddFilterWhere(strKey, "ename", "Nombre", cwString)
'    Call .FormAddFilterWhere(strKey, "job", "Puesto de trabajo", cwString)
'    Call .FormAddFilterWhere(strKey, "sal", "Salario del cliente", cwNumeric)
    
'    Call .FormAddFilterOrder("ename", "Nombre")
'    Call .FormAddFilterOrder("job", "Puesto de trabajo")
  End With
  
  ' Declaraci�n de las caracter�sticas del form multilinea
  With objMultiInfo1
  
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(0)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
' Definici�n del tipo (modelo) de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR1400"
    .intAllowance = cwAllowReadOnly

' M�todo de ordenacion del form
 '   Call .FormAddOrderField("PR06NUMFASE", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("PR03NUMACTPEDI", txtText1(0))
    Call .FormAddRelation("PR06NUMFASE", txtText1(1))
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
' Creaci�n de los filtros de busqueda
'    Call .FormCreateFilterWhere(strKey, "Tabla de Clientes")
'    Call .FormAddFilterWhere(strKey, "ename", "Nombre", cwString)
'    Call .FormAddFilterWhere(strKey, "job", "Puesto de trabajo", cwString)
'    Call .FormAddFilterWhere(strKey, "sal", "Salario del cliente", cwNumeric)
    
'    Call .FormAddFilterOrder("ename", "Nombre")
'    Call .FormAddFilterOrder("job", "Puesto de trabajo")
  End With

  
'******************************************************************
'**************** FIN FASES PEDIDAS *******************************
'******************************************************************


'******************************************************************
'******************************************************************
'**************   FASES CITADAS  **********************************
'******************************************************************
'******************************************************************

  If Val(vntNumSolicit) <> 0 And Val(vntNumCita) <> 0 Then

' Declaraci�n de las caracter�sticas del form multilinea
  With objDetailInfo2
  
' Asignaci�n del nombre del form
    .strName = "FaseCitada"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(2)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabSSTab1(2)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(2)
' Asignaci�n de la tabla asociada al form
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI1500"
    .strWhere = "CI31NUMSOLICIT=" & vntNumSolicit & " AND CI01NUMCITA=" & vntNumCita
    If blnDataReadOnly Then
      .intAllowance = cwAllowReadOnly
    End If
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI15NUMFASECITA", cwAscending)
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
' Creaci�n de los filtros de busqueda
'    Call .FormCreateFilterWhere(strKey, "Tabla de Clientes")
'    Call .FormAddFilterWhere(strKey, "ename", "Nombre", cwString)
'    Call .FormAddFilterWhere(strKey, "job", "Puesto de trabajo", cwString)
'    Call .FormAddFilterWhere(strKey, "sal", "Salario del cliente", cwNumeric)
    
'    Call .FormAddFilterOrder("ename", "Nombre")
'    Call .FormAddFilterOrder("job", "Puesto de trabajo")
  End With
  
  ' Declaraci�n de las caracter�sticas del form multilinea
  With objMultiInfo2
  
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(3)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(2)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(3)
' Definici�n del tipo (modelo) de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2700"

' M�todo de ordenacion del form
 '   Call .FormAddOrderField("PR06NUMFASE", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI31NUMSOLICIT", txtText1(7))
    Call .FormAddRelation("CI01NUMCITA", txtText1(8))
    Call .FormAddRelation("CI15NUMFASECITA", txtText1(9))
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
' Creaci�n de los filtros de busqueda
'    Call .FormCreateFilterWhere(strKey, "Tabla de Clientes")
'    Call .FormAddFilterWhere(strKey, "ename", "Nombre", cwString)
'    Call .FormAddFilterWhere(strKey, "job", "Puesto de trabajo", cwString)
'    Call .FormAddFilterWhere(strKey, "sal", "Salario del cliente", cwNumeric)
    
'    Call .FormAddFilterOrder("ename", "Nombre")
'    Call .FormAddFilterOrder("job", "Puesto de trabajo")
  End With

  End If
'******************************************************************
'**************** FIN FASES CITADAS *******************************
'******************************************************************



' Declaraci�n de las caracter�sticas del objeto ventana

 
 With objWinInfo
    
' Se a�ade el formulario a la ventana
    Call .FormAddInfo(objDetailInfo1, cwFormDetail)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    
    If Val(vntNumSolicit) <> 0 And Val(vntNumCita) <> 0 Then
      Call .FormAddInfo(objDetailInfo2, cwFormDetail)
      Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    End If
' Campos que aparecen en el grid
   
   
    Call .GridAddColumn(objMultiInfo1, "NumActPedida", "PR03NUMACTPEDI") '3
    Call .GridAddColumn(objMultiInfo1, "NumFase", "PR06NUMFASE") '4
    Call .GridAddColumn(objMultiInfo1, "N� Necesidad", "PR14NUMNECESID") '5
    Call .GridAddColumn(objMultiInfo1, "Ind. Tipo Recurso", "AG14CODTIPRECU") '6
    Call .GridAddColumn(objMultiInfo1, "Tipo Recurso", "") '7
    Call .GridAddColumn(objMultiInfo1, "Cantidad", "PR14NUMUNIREC") '8
    Call .GridAddColumn(objMultiInfo1, "Tiempo.M�n.Ocupac", "PR14NUMMINOCU") '9
    Call .GridAddColumn(objMultiInfo1, "Cant. m�nima ", "PR14NUMMINDESREC") '10
    Call .GridAddColumn(objMultiInfo1, "Ind. Preferencia", "PR14INDRECPREFE", cwBoolean) '11
    
    
    If Val(vntNumSolicit) <> 0 And Val(vntNumCita) <> 0 Then
      Call .GridAddColumn(objMultiInfo2, "NumSolicitud", "CI31NUMSOLICIT") '3
      Call .GridAddColumn(objMultiInfo2, "NumCita", "CI01NUMCITA") '4
      Call .GridAddColumn(objMultiInfo2, "NumFaseCita", "CI15NUMFASECITA") '5
      Call .GridAddColumn(objMultiInfo2, "CodRecurso", "AG11CODRECURSO") '6
      Call .GridAddColumn(objMultiInfo2, "Recurso           ", "") '7
      Call .GridAddColumn(objMultiInfo2, "Departamento", "") '8'
   '   Call .GridAddColumn(objMultiInfo2, "Departamento", "") '9
      Call .GridAddColumn(objMultiInfo2, "Fecha Ocupaci�n", "CI27FECOCUPREC") '10
      Call .GridAddColumn(objMultiInfo2, "Dias Ocupaci�n", "CI27NUMDIASREC") '11
      Call .GridAddColumn(objMultiInfo2, "Horas Ocupaci�n", "CI27NUMHORAREC") '12
      Call .GridAddColumn(objMultiInfo2, "Minutos Ocupaci�n", "CI27NUMMINUREC") '13
      
      Call .FormCreateInfo(objDetailInfo2)
    
      .CtrlGetInfo(grdDBGrid1(3).Columns(3)).blnInGrid = False
      .CtrlGetInfo(grdDBGrid1(3).Columns(4)).blnInGrid = False
      .CtrlGetInfo(grdDBGrid1(3).Columns(5)).blnInGrid = False
      .CtrlGetInfo(grdDBGrid1(3).Columns(6)).blnInGrid = False
 
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(6)), "AG11CODRECURSO", "SELECT AG11CODRECURSO, AG11DESRECURSO,AD02CODDPTO FROM AG1100 WHERE AG11CODRECURSO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(6)), grdDBGrid1(3).Columns(7), "AG11DESRECURSO")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(6)), grdDBGrid1(3).Columns(8), "AD02CODDPTO")
    End If
    
    Call .FormCreateInfo(objDetailInfo1)
    
' Campo clave primaria y columnas obligatorias
' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
 '   .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
 '   .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnMandatory = True
 '   .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnMandatory = True
 '   .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnMandatory = True
' Se establecen los colores de los controles
  '  Call .FormChangeColor(objMultiInfo)
  
' Campos que intervienen en busquedas
  '  .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
  '  .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
  '  .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
  
' Valores con los que se cargara la DbCombo
  '  .CtrlGetInfo(grdDBGrid1(0).Columns(8)).strSql = "SELECT deptno, dname FROM " & objEnv.GetValue("Database") & "dept ORDER BY deptno"
  

' Definici�n de controles relacionados entre s�
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "AG14CODTIPRECU", "SELECT AG14CODTIPRECU, AG14DESTIPRECU FROM AG1400 WHERE AG14CODTIPRECU = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(7), "AG14DESTIPRECU")
    


   .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInGrid = False
   .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInGrid = False
   .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInGrid = False

   .CtrlGetInfo(txtText1(0)).blnInGrid = False
   .CtrlGetInfo(txtText1(1)).blnInGrid = False
'   .CtrlGetInfo(txtText1(7)).blnInGrid = False
'   .CtrlGetInfo(txtText1(8)).blnInGrid = False

' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
' Se estabiliza la ventana configurando las propiedades
' de los controles
    Call .WinStabilize

  End With

'+++++++ Hasta Mirar  en libreria +++++++
   grdDBGrid1(1).Columns(3).Visible = False
   grdDBGrid1(1).Columns(4).Visible = False
   grdDBGrid1(1).Columns(6).Visible = False
   If Val(vntNumSolicit) <> 0 And Val(vntNumCita) <> 0 Then
     grdDBGrid1(3).Columns(3).Visible = False
     grdDBGrid1(3).Columns(4).Visible = False
     grdDBGrid1(3).Columns(5).Visible = False
     grdDBGrid1(3).Columns(6).Visible = False
   Else
     fraFrame1(2).Enabled = False
     fraFrame1(3).Enabled = False
   End If
'+++++++++++++++++++++++++++++++++++++++++++

' Se oculta el formulario de splash
  Call objApp.SplashOff
    
  'Call tabSSTab1_Click(0, 0)
  Call objWinInfo.DataMoveFirst
  tabSSTab1(1).Tab = 1
  tabSSTab1(2).Tab = 1

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
  If Index = 3 Then
    grdDBGrid1(3).Columns("Departamento").Text = GetTableColumn("SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & Val(grdDBGrid1(3).Columns("Departamento").Text))("AD02DESDPTO")
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabSSTab1_Click(intIndex As Integer, PreviousTab As Integer)
  If intIndex = 0 Then
    With objWinInfo
     If tabSSTab1(0).Tab = 0 Then
       Call .FormChangeActive(fraFrame1(0), False, True)
       If objGen.GetRowCount(.objWinActiveForm.rdoCursor) = 0 Then
         .DataRefresh
       End If
     End If
     If tabSSTab1(0).Tab = 1 Then
       If Val(vntNumSolicit) <> 0 And Val(vntNumCita) <> 0 Then
         Call .FormChangeActive(fraFrame1(2), False, True)
         If objGen.GetRowCount(.objWinActiveForm.rdoCursor) = 0 Then
           .DataRefresh
         End If
       End If
      End If
    End With
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer _
                            )
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


