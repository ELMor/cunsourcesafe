VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsActuacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarintNumero As Long 'local copy
Private mvarintCodigo As Long 'local copy
Private mvarstrDesc As String 'local copy
Public Property Let strDesc(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strDesc = 5
    mvarstrDesc = vData
End Property


Public Property Get strDesc() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strDesc
    strDesc = mvarstrDesc
End Property



Public Property Let intCodigo(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.intCodigo = 5
    mvarintCodigo = vData
End Property


Public Property Get intCodigo() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.intCodigo
    intCodigo = mvarintCodigo
End Property



Public Property Let intNumero(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.intNumero = 5
    mvarintNumero = vData
End Property


Public Property Get intNumero() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.intNumero
    intNumero = mvarintNumero
End Property



