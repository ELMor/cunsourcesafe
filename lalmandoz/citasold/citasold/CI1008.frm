VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmTipoEcon 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Mantenimiento de Tipos Econ�micos"
   ClientHeight    =   7965
   ClientLeft      =   1050
   ClientTop       =   870
   ClientWidth     =   9780
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "CI1008.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7965
   ScaleWidth      =   9780
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9780
      _ExtentX        =   17251
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   25
      Top             =   7710
      Width           =   9780
      _ExtentX        =   17251
      _ExtentY        =   450
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraFrame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Entidades"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2310
      Index           =   1
      Left            =   120
      TabIndex        =   13
      Tag             =   "Mantenimiento de Entidades"
      Top             =   2640
      Width           =   9555
      Begin TabDlg.SSTab tabtab1 
         Height          =   1695
         HelpContextID   =   90001
         Index           =   1
         Left            =   165
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   450
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   2990
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1008.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(8)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(9)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(3)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1008.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "CI32CODTIPECON"
            Height          =   330
            Index           =   4
            Left            =   5880
            TabIndex        =   26
            Tag             =   "Nombre|Nombre Entidad"
            Top             =   1080
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "CI13CODENTIDAD"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   16
            Tag             =   "C�digo|C�digo Entidad"
            Top             =   360
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "CI13DESENTIDAD"
            Height          =   330
            Index           =   3
            Left            =   1200
            TabIndex        =   17
            Tag             =   "Nombre|Nombre Entidad"
            Top             =   360
            Width           =   4935
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI13FECINVGENT"
            Height          =   330
            Index           =   2
            Left            =   120
            TabIndex        =   18
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   1080
            Width           =   2595
            _Version        =   65537
            _ExtentX        =   4586
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2075/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI13FECFIVGENT"
            Height          =   330
            Index           =   3
            Left            =   2850
            TabIndex        =   20
            Tag             =   "Fecha Fin Vigencia"
            Top             =   1080
            Width           =   2595
            _Version        =   65537
            _ExtentX        =   4586
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2075/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1410
            Index           =   1
            Left            =   -74880
            TabIndex        =   24
            Top             =   120
            Width           =   8580
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15134
            _ExtentY        =   2487
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   23
            Top             =   120
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   1200
            TabIndex        =   22
            Top             =   120
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   2850
            TabIndex        =   21
            Top             =   840
            Width           =   1650
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Econ�mico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   5760
            TabIndex        =   19
            Top             =   840
            Visible         =   0   'False
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   15
            Top             =   840
            Width           =   1860
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipos Econ�micos"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2130
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Tag             =   "Mantenimiento Tipos Econ�micos"
      Top             =   480
      Width           =   9555
      Begin TabDlg.SSTab tabtab1 
         Height          =   1575
         HelpContextID   =   90001
         Index           =   0
         Left            =   180
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   390
         Width           =   9210
         _ExtentX        =   16245
         _ExtentY        =   2778
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1008.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtcDateCombo1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1008.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI32FECINVGTEC"
            Height          =   330
            Index           =   0
            Left            =   2040
            TabIndex        =   3
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   960
            Width           =   2415
            _Version        =   65537
            _ExtentX        =   4260
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2075/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.CheckBox chkCheck1 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Indicador Responsable"
            DataField       =   "CI32INDRESPECO"
            ForeColor       =   &H00000000&
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   2
            Tag             =   "Indicador Responsable"
            Top             =   960
            Width           =   1575
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI32CODTIPECON"
            Height          =   330
            HelpContextID   =   40101
            Index           =   1
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Tipo Econ�mico"
            Top             =   300
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "CI32DESTIPECON"
            Height          =   330
            HelpContextID   =   40102
            Index           =   2
            Left            =   2040
            TabIndex        =   1
            Tag             =   "Descripci�n Tipo Econ�mico"
            Top             =   300
            Width           =   5050
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1290
            Index           =   0
            Left            =   -74880
            TabIndex        =   7
            Top             =   120
            Width           =   8595
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15161
            _ExtentY        =   2275
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI32FECFIVGTEC"
            Height          =   330
            Index           =   1
            Left            =   4680
            TabIndex        =   4
            Tag             =   "Fecha Fin Vigencia"
            Top             =   960
            Width           =   2415
            _Version        =   65537
            _ExtentX        =   4260
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2075/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   4710
            TabIndex        =   12
            Top             =   720
            Width           =   1650
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   2055
            TabIndex        =   11
            Top             =   720
            Width           =   1860
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   9
            Top             =   75
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   2055
            TabIndex        =   8
            Top             =   75
            Width           =   1020
         End
      End
   End
   Begin TabDlg.SSTab tabtab2 
      Height          =   2595
      HelpContextID   =   90001
      Index           =   0
      Left            =   150
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   5010
      Width           =   9525
      _ExtentX        =   16801
      _ExtentY        =   4577
      _Version        =   327681
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   547
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Responsable Econ�mico"
      TabPicture(0)   =   "CI1008.frx":007C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame2(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Tipos P�liza"
      TabPicture(1)   =   "CI1008.frx":0098
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame2(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Conciertos"
      TabPicture(2)   =   "CI1008.frx":00B4
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraFrame2(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Colectivos"
      TabPicture(3)   =   "CI1008.frx":00D0
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraFrame2(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Entidad Colaboradora"
      TabPicture(4)   =   "CI1008.frx":00EC
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraFrame2(4)"
      Tab(4).ControlCount=   1
      Begin VB.Frame fraFrame2 
         BorderStyle     =   0  'None
         Height          =   2175
         Index           =   0
         Left            =   60
         TabIndex        =   36
         Top             =   360
         Width           =   9390
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1800
            Index           =   2
            Left            =   120
            TabIndex        =   37
            Top             =   240
            Width           =   9195
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   16219
            _ExtentY        =   3175
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame2 
         BorderStyle     =   0  'None
         Height          =   2175
         Index           =   1
         Left            =   -74940
         TabIndex        =   34
         Top             =   360
         Width           =   9390
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1800
            Index           =   3
            Left            =   120
            TabIndex        =   35
            Top             =   240
            Width           =   9255
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   16325
            _ExtentY        =   3175
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame2 
         BorderStyle     =   0  'None
         Height          =   2175
         Index           =   4
         Left            =   -74940
         TabIndex        =   32
         Top             =   360
         Width           =   9390
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1800
            Index           =   6
            Left            =   120
            TabIndex        =   33
            Top             =   240
            Width           =   9255
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   16325
            _ExtentY        =   3175
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame2 
         BorderStyle     =   0  'None
         Height          =   2175
         Index           =   2
         Left            =   -74925
         TabIndex        =   30
         Top             =   360
         Width           =   9390
         Begin VB.CommandButton cmdCommand1 
            Caption         =   "Do&cumentos Autorizaci�n"
            Enabled         =   0   'False
            Height          =   450
            Index           =   0
            Left            =   8010
            TabIndex        =   38
            Top             =   225
            Width           =   1245
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1800
            Index           =   4
            Left            =   120
            TabIndex        =   31
            Top             =   240
            Width           =   7740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   13652
            _ExtentY        =   3175
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame2 
         BorderStyle     =   0  'None
         Height          =   2175
         Index           =   3
         Left            =   -74940
         TabIndex        =   28
         Top             =   360
         Width           =   9390
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1800
            Index           =   5
            Left            =   120
            TabIndex        =   29
            Top             =   240
            Width           =   9255
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   16325
            _ExtentY        =   3175
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmTipoEcon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdCommand1_Click(intIndex As Integer)
  If objWinInfo.objWinActiveForm.blnChanged = True Then
    objWinInfo.DataSave
  End If
  
  'Versi�n DLL
  Call objSecurity.LaunchProcess(ciWinDocAutorizaci�n)
  
  'Versi�n EXE
  'Load frmDocAutorizacion
  'Call frmDocAutorizacion.Show(vbModal)
  'Unload frmDocAutorizacion
  'Set frmDocAutorizacion = Nothing
 
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form padre
  Dim objMasterInfo As New clsCWForm
' Form padre e hijo a la vez
  Dim objDetailInfo As New clsCWForm
' Form hijos
  Dim objMultiInfo0 As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
  Dim objMultiInfo3 As New clsCWForm
  Dim objMultiInfo4 As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  Dim colC1 As Column
' **********************************
' Fin declaraci�n de variables
' **********************************

' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Mantenimiento de Tipos Econ�micos"
    .cwDAT = "16-07-97"
    .cwAUT = "Jokin"
    
    .cwDES = "Esta ventana permite mantener los Tipos Econ�micos de la CUN"
    
    .cwUPD = "16-07-97 - Jokin - Creaci�n del m�dulo"
    
    .cwEVT = "En el evento de PreValidate se realizar�..."
  End With
  
' Declaraci�n de las caracter�sticas del form padre
  With objMasterInfo
' Asignaci�n del nombre del form
    .strName = "TipoEcon"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI3200"
' Reports generados por el form
    Call .objPrinter.Add("CI0015", "Listado 1 Relaci�n de Tipos Econ�micos")

' M�todo de ordenacion del form
    Call .FormAddOrderField("CI32CODTIPECON", cwAscending)
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
    ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Tipos Econ�micos")
    Call .FormAddFilterWhere(strKey, "CI32CODTIPECON", "C�digo", cwString)
    Call .FormAddFilterWhere(strKey, "CI32DESTIPECON", "Descripci�n", cwString)
    
    Call .FormAddFilterOrder(strKey, "CI32CODTIPECON", "C�digo")
    Call .FormAddFilterOrder(strKey, "CI32DESTIPECON", "Descripci�n")
  End With
  
' *******************************************************************
' Declaraci�n de las caracter�sticas del form padre e hijo a la vez
  With objDetailInfo
   .strName = "Entidades"
 ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
 ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI1300"
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI32CODTIPECON", cwAscending)
    Call .FormAddOrderField("CI13CODENTIDAD", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI32CODTIPECON", txtText1(1))
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Entidades")
    Call .FormAddFilterWhere(strKey, "CI13CODENTIDAD", "Entidad", cwString)
    Call .FormAddFilterWhere(strKey, "CI13DESENTIDAD", "Descripci�n", cwString)
           
    Call .FormAddFilterOrder(strKey, "CI13CODENTIDAD", "Entidad")
    Call .FormAddFilterOrder(strKey, "CI13DESENTIDAD", "Descripci�n")
  End With
'************************************************************************
' Declaraci�n de las caracter�sticas del form hijo 0 Responsable Econ�mico
  With objMultiInfo0
' Asignaci�n del nombre del form
    .strName = "Responsables"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame2(0)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(1)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(2)
' Definici�n del tipo de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2900"
    '.intAllowance = cwAllowAdd
    .intAllowance = cwAllowAdd + cwAllowDelete

' M�todo de ordenacion del form
    Call .FormAddOrderField("CI32CODTIPECON", cwAscending)

' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI32CODTIPECON", txtText1(4))
    Call .FormAddRelation("CI13CODENTIDAD", txtText1(0))
    
  End With

'************************************************************************
' Declaraci�n de las caracter�sticas del form hijo 1 Tipo P�liza
  With objMultiInfo1
    .strName = "TiposPoliza"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame2(1)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(1)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(3)
' Definici�n del tipo de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI3800"
    .intAllowance = cwAllowAdd + cwAllowDelete

' M�todo de ordenacion del form
    Call .FormAddOrderField("CI32CODTIPECON", cwAscending)

' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI32CODTIPECON", txtText1(4))
    Call .FormAddRelation("CI13CODENTIDAD", txtText1(0))
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de P�lizas Entidad")
    Call .FormAddFilterWhere(strKey, "CI36CODTIPPOLI", "Tipo P�liza", cwString)
    
    Call .FormAddFilterOrder(strKey, "CI36CODTIPPOLI", "Tipo P�liza")
    
  End With
' ***************************************************************************
' Declaraci�n de las caracter�sticas del form hijo 2 Concierto Entidad
  With objMultiInfo2
   .strName = "Conciertos"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame2(2)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(1)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(4)
' Definici�n del tipo de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI0900"
    .intAllowance = cwAllowAll

' M�todo de ordenacion del form
    Call .FormAddOrderField("CI32CODTIPECON", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI32CODTIPECON", txtText1(4))
    Call .FormAddRelation("CI13CODENTIDAD", txtText1(0))
    
  End With
' ***************************************************************************
' Declaraci�n de las caracter�sticas del form hijo 3 Colectivo Entidad
  With objMultiInfo3
    .strName = "Colectivos"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame2(3)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(1)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(5)
' Definici�n del tipo de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI3900"
    .intAllowance = cwAllowAdd + cwAllowDelete

' M�todo de ordenacion del form
    Call .FormAddOrderField("CI32CODTIPECON", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI32CODTIPECON", txtText1(4))
    Call .FormAddRelation("CI13CODENTIDAD", txtText1(0))
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
    
  End With
' ***************************************************************************
' Declaraci�n de las caracter�sticas del form hijo 4 Entidad Colaboradora
  With objMultiInfo4
    .strName = "EntidadCol"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame2(4)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(1)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(6)
' Definici�n del tipo de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI4000"
    .intAllowance = cwAllowAdd + cwAllowDelete

' M�todo de ordenacion del form
    Call .FormAddOrderField("CI32CODTIPECON", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI32CODTIPECON", txtText1(4))
    Call .FormAddRelation("CI13CODENTIDAD", txtText1(0))
    
  End With
' ***************************************************************************


' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)

' Se a�ade cada formulario y Campos que aparecen en el grid 0
    Call .FormAddInfo(objMultiInfo0, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo0, "Tipo Econ�mico", "CI32CODTIPECON")
    Call .GridAddColumn(objMultiInfo0, "Entidad", "CI13CODENTIDAD")
    Call .GridAddColumn(objMultiInfo0, "C�digo Persona", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo0, "Raz�n Social                   ", "")
    
' Se a�ade cada formulario y Campos que aparecen en el grid 1
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo1, "Tipo Econ�mico", "CI32CODTIPECON")
    Call .GridAddColumn(objMultiInfo1, "Entidad", "CI13CODENTIDAD")
    Call .GridAddColumn(objMultiInfo1, "Tipo P�liza       ", "CI36CODTIPPOLI")

' Se a�ade cada formulario y Campos que aparecen en el grid 2
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo2, "Tipo Econ�mico", "CI32CODTIPECON")
    Call .GridAddColumn(objMultiInfo2, "Entidad", "CI13CODENTIDAD")
    Call .GridAddColumn(objMultiInfo2, "Fecha Entrada Vigencia", "CI09FECENTVIGE")
    Call .GridAddColumn(objMultiInfo2, "Fecha Fin Vigencia", "CI09FECFINVIGE")
    Call .GridAddColumn(objMultiInfo2, "C�digo Concierto Econ�mico", "CI43CODCONCECO")

' Se a�ade cada formulario y Campos que aparecen en el grid 3
    Call .FormAddInfo(objMultiInfo3, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo3, "Tipo Econ�mico", "CI32CODTIPECON")
    Call .GridAddColumn(objMultiInfo3, "Entidad", "CI13CODENTIDAD")
    Call .GridAddColumn(objMultiInfo3, "Colectivo                 ", "CI08CODCOLECTI")
   
' Se a�ade cada formulario y Campos que aparecen en el grid 4
    Call .FormAddInfo(objMultiInfo4, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo4, "Tipo Econ�mico", "CI32CODTIPECON")
    Call .GridAddColumn(objMultiInfo4, "Entidad", "CI13CODENTIDAD")
    Call .GridAddColumn(objMultiInfo4, "Entidad Colaboradora        ", "CI37CODENTCOLA")
' Se obtiene informaci�n de las caracter�sticas de
' los controles del formulario
    Call .FormCreateInfo(objMasterInfo)
    
  
' Definici�n de controles relacionados entre s�
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), "CI21CODPERSONA", "SELECT CI21CODPERSONA, CI23RAZONSOCIAL FROM CI2300 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), grdDBGrid1(2).Columns(6), "CI23RAZONSOCIAL")

' Campos que intervienen en busquedas.
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    
' Sql de un campo del grid 1 del hijo
     .CtrlGetInfo(grdDBGrid1(3).Columns(5)).strSql = "SELECT CI36CODTIPPOLI, CI36DESTIPPOLI FROM " & objEnv.GetValue("DataBase") & "CI3600 ORDER BY CI36DESTIPPOLI"

' Sql de un campo del grid 2 del hijo
     .CtrlGetInfo(grdDBGrid1(4).Columns(7)).strSql = "SELECT CI43CODCONCECO, CI43DESCONCECO FROM " & objEnv.GetValue("DataBase") & "CI4300 ORDER BY CI43CODCONCECO"
  
' Sql de un campo del grid 3 del hijo
     .CtrlGetInfo(grdDBGrid1(5).Columns(5)).strSql = "SELECT CI08CODCOLECTI, CI08DESCOLECTI FROM " & objEnv.GetValue("DataBase") & "CI0800 ORDER BY CI08DESCOLECTI"
  
' Sql de un campo del grid 3 del hijo
     .CtrlGetInfo(grdDBGrid1(6).Columns(5)).strSql = "SELECT CI37CODENTCOLA, CI37DESENTCOLA FROM " & objEnv.GetValue("DataBase") & "CI3700 ORDER BY CI37DESENTCOLA"
  
' Eliminamos campo del grid
    '.CtrlGetInfo(txtText1(7)).blnInGrid = False
     .CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnForeign = True

    .CtrlGetInfo(dtcDateCombo1(0)).vntDefaultValue = Format(objGen.GetDBDateTime, "DD/MM/YYYY")
    .CtrlGetInfo(dtcDateCombo1(2)).vntDefaultValue = Format(objGen.GetDBDateTime, "DD/MM/YYYY")


' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
' Se estabiliza la ventana configurando las propiedades
' de los controles
    Call .WinStabilize
  
  End With
' la columna relacionada con la tabla padre no se visualiza
' Se controla su valor en los eventos del grid
  txtText1(4).Visible = False
  grdDBGrid1(2).Columns(3).Visible = False
  grdDBGrid1(2).Columns(4).Visible = False
  grdDBGrid1(3).Columns(3).Visible = False
  grdDBGrid1(3).Columns(4).Visible = False
  grdDBGrid1(4).Columns(3).Visible = False
  grdDBGrid1(4).Columns(4).Visible = False
  grdDBGrid1(5).Columns(3).Visible = False
  grdDBGrid1(5).Columns(4).Visible = False
  grdDBGrid1(6).Columns(3).Visible = False
  grdDBGrid1(6).Columns(4).Visible = False
' *******************************************************************
  If Not objWinInfo.cllWinForms("fraFrame2(2)").rdoCursor Is Nothing Then
   For Each colC1 In grdDBGrid1(4).Columns
    
      If colC1.DataField <> "" Then
        If objWinInfo.cllWinForms("fraFrame2(2)").rdoCursor(colC1.DataField).Type = rdTypeTIMESTAMP Then
          colC1.Style = 1
        End If
      End If
    Next
   End If
  
' Se oculta el formulario de splash
 Call objApp.SplashOff
End Sub

Private Sub grdDBGrid1_BtnClick(intIndex As Integer)
  Dim objGetDate As New clsCWGetDate
  If intIndex = 4 Then
    
    If Not objWinInfo.CtrlGetInfo(grdDBGrid1(4).Columns(grdDBGrid1(4).Col)).blnReadOnly Then
      If objGetDate.GetDate(objWinInfo.CtrlGet(grdDBGrid1(4).Columns(grdDBGrid1(4).Col))) Then
        Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(grdDBGrid1(4).Col), objGetDate.strDate)
      End If
    End If
  End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objSearch As New clsCWSearch
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Responsables" And strCtrl = "grdDBGrid1(2).C�digo Persona" Then
    With objSearch
      .strTable = "CI2300"
      .strWhere = ""
      .strOrder = ""
      
      Set objField = .AddField("CI21CODPERSONA")
      objField.strSmallDesc = "C�digo"
      Set objField = .AddField("CI23RAZONSOCIAL")
      objField.strSmallDesc = "Raz�n Social              "
      Set objField = .AddField("CI23DIREMAIL")
      objField.strSmallDesc = "Direcci�n E-mail"
      If .Search Then
        grdDBGrid1(2).Columns(5).Value = .cllValues("CI21CODPERSONA")
        grdDBGrid1(2).Columns(6).Value = .cllValues("CI23RAZONSOCIAL")
      
      End If
      
    End With
    Set objSearch = Nothing
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  If strFormName = "Conciertos" Then
    If objWinInfo.objWinActiveForm.rdoCursor.RowCount > 0 Then
      cmdCommand1(0).Enabled = True
    
      Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
    End If
  Else
    cmdCommand1(0).Enabled = False
  End If
    
  If strFormName = "Responsables" Then
    If chkCheck1(0) = 0 Then
     objWinInfo.cllWinForms("fraframe2(0)").intAllowance = cwAllowReadOnly
    Else
     objWinInfo.cllWinForms("fraframe2(0)").intAllowance = cwAllowAll
    End If
    objWinInfo.WinPrepareScr
  End If


End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  If strFormName = "Entidades" Then
    Call objWinInfo.CtrlSet(txtText1(4), objWinInfo.CtrlGet(txtText1(1)))
  End If
  If strFormName = "Responsables" Then
    Call objWinInfo.CtrlSet(grdDBGrid1(2).Columns(3), objWinInfo.CtrlGet(txtText1(1)))
    Call objWinInfo.CtrlSet(grdDBGrid1(2).Columns(4), objWinInfo.CtrlGet(txtText1(0)))
  End If
  If strFormName = "TiposPoliza" Then
    Call objWinInfo.CtrlSet(grdDBGrid1(3).Columns(3), objWinInfo.CtrlGet(txtText1(1)))
    Call objWinInfo.CtrlSet(grdDBGrid1(3).Columns(4), objWinInfo.CtrlGet(txtText1(0)))
  End If
  If strFormName = "Conciertos" Then
    Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(3), objWinInfo.CtrlGet(txtText1(1)))
    Call objWinInfo.CtrlSet(grdDBGrid1(4).Columns(4), objWinInfo.CtrlGet(txtText1(0)))
  End If
  If strFormName = "Colectivos" Then
    Call objWinInfo.CtrlSet(grdDBGrid1(5).Columns(3), objWinInfo.CtrlGet(txtText1(1)))
    Call objWinInfo.CtrlSet(grdDBGrid1(5).Columns(4), objWinInfo.CtrlGet(txtText1(0)))
  End If
  If strFormName = "EntidadCol" Then
    Call objWinInfo.CtrlSet(grdDBGrid1(6).Columns(3), objWinInfo.CtrlGet(txtText1(1)))
    Call objWinInfo.CtrlSet(grdDBGrid1(6).Columns(4), objWinInfo.CtrlGet(txtText1(0)))
  End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "TipoEcon" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
 
End Sub

Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
  If strFormName = "TipoEcon" Then
    If IsDate(dtcDateCombo1(0).Date) And IsDate(dtcDateCombo1(1).Date) Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
         Call objError.SetError(cwCodeMsg, "La Fecha Fin es menor que la Fecha Inicial")
         Call objError.Raise
         blnCancel = True
      End If
    End If
  End If
  
  If strFormName = "Entidades" Then
    If IsDate(dtcDateCombo1(3).Date) And IsDate(dtcDateCombo1(2).Date) Then
      If DateDiff("d", dtcDateCombo1(3).Date, dtcDateCombo1(2).Date) > 0 Then
        Call objError.SetError(cwCodeMsg, "La Fecha Fin es menor que la Fecha Inicial")
        Call objError.Raise
        blnCancel = True
      End If
    End If
  End If
  If strFormName = "Conciertos" Then
    If IsDate(grdDBGrid1(4).Columns(6).Text) Then
      If DateDiff("d", CDate(grdDBGrid1(4).Columns(6).Text), CDate(grdDBGrid1(4).Columns(5).Text)) > 0 Then
        Call objError.SetError(cwCodeMsg, "La Fecha Fin es menor que la Fecha Inicial")
        Call objError.Raise
        blnCancel = True
      End If
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabtab1_Click(Index As Integer, PreviousTab As Integer)
Call objWinInfo.FormChangeActive(fraFrame1(tabTab1(Index).Tab), True, True)

End Sub
Private Sub tabtab2_Click(intIndex As Integer, PreviousTab As Integer)
  If objWinInfo.objWinActiveForm.blnChanged = True Then
    objWinInfo.DataSave
  End If
  Call objWinInfo.FormChangeActive(fraFrame2(tabtab2(intIndex).Tab), True, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub
Private Sub tabTab2_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabtab2(intIndex), False, True)
    
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub fraFrame2_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame2(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


