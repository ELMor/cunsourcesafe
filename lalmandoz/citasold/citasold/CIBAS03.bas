Attribute VB_Name = "Estandar"
Option Explicit

Public Sub eventUpdate(ByVal objems As clsProcess, _
                       ByVal blnMode As Boolean)
  Select Case objems.strProcess
    Case "ACUNSA"
        Call UpdatePolizaACUNSA(objems, blnMode)
    Case "CódigosPostales"
        Call UpdateCodigosPostales(objems, blnMode)
  End Select
End Sub
Public Sub CargaPolizasACUNSA(ByVal objConnect As clsConnect)
  Dim objems As New clsProcess
  
  With objems
    .strProcess = "ACUNSA"
    .strInput = "SELECT " & _
                "NUMPOLIZA, NUMASEG, FECINIPOL, FECFINPOL, " & _
                "NOMBRETOM, NOMBREASEG, DNI, SEXO, FECNACIMIENTO, " & _
                "CORPAGO , EXCLUSIONES, TIPOPOLIZA " & _
                "From  ACUNSA.TXT Order By NUMPOLIZA,NUMASEG"
    .strOutput = "SELECT " & _
                 "CI03NUMPOLIZA, CI03NUMASEGURA, CI03FECINIPOLI, " & _
                 "CI03FECFINPOLI, CI03NOMBRTOMAD, CI03APELLITOMAD, " & _
                 "CI03NOMBRASEGU, CI03APELLIASEGU, CI22NUMHISTORIA, " & _
                 "CI03DNI, CI30CODSEXO, CI03FECNACIMIE, " & _
                 "CI03CORRIPAGO, CI36CODTIPPOLI, CI03EXCLUSION, CI03OBSERVACIO " & _
                 "From  CI0300 " & _
                 "ORDER BY CI03NUMPOLIZA, CI03NUMASEGURA"
   
   
    Call .cllCamposClaveEntrada.Add("NUMPOLIZA")
    Call .cllFormatoClaveEntrada.Add("000000")
    Call .cllCamposClaveEntrada.Add("NUMASEG")
    Call .cllFormatoClaveEntrada.Add("00000")
    
    Call .cllCamposClaveSalida.Add("CI03NUMPOLIZA")
    Call .cllFormatoClaveSalida.Add("0000000")
    Call .cllCamposClaveSalida.Add("CI03NUMASEGURA")
    Call .cllFormatoClaveSalida.Add("0000000")
   
    Call .emsInitProcess(objConnect)
    Call .emsRunProcess(objConnect)
    Call .emsEndProcess
  End With
  
  Set objems = Nothing
  
End Sub


Public Sub CargaCodigosPostales(ByVal objConnect As clsConnect)
  Dim objems As New clsProcess
  lngSecuencia = 0
  With objems
    .strProcess = "CódigosPostales"
    .strInput = "SELECT " & _
                "CODPROV,CODPOB, NOMCIUDAD, CALLE, NOMCALLE, " & _
                "COMIMPAR, FINIMPAR, COMPAR, FINPAR, CODPOSTAL " & _
                "From  CPOSTAL.TXT " & frmCargaTextos.strCodProv
    .strOutput = "SELECT  CI07SECUENCIA, " & _
                 "CI07CODPOSTAL, CI26CODPROVI, CI07TIPPOBLA,   " & _
                 "CI07DESPOBLA, CI07CALLE, CI07NUMIMPCOM, CI07NUMIMPFIN, " & _
                 "CI07NUMPARCOM, CI07NUMPARFIN " & _
                 "FROM  CI0700 WHERE CI07CODPOSTAL=0 "
   
   
   
    Call .emsInitProcess(objConnect)
    Call .emsRunProcess(objConnect)
    Call .emsEndProcess
  End With
  
  Set objems = Nothing
  
End Sub


