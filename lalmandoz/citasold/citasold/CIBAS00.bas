Attribute VB_Name = "General"
Option Explicit

Public objCW As clsCW       ' referencia al objeto CodeWizard
Public objApp As clsCWApp
Public objPipe As clsCWPipeLine
Public objGen As clsCWGen
Public objEnv As clsCWEnvironment
Public objError As clsCWError
Public objMouse As clsCWMouse
Public objSecurity As clsCWSecurity

Public blnRefrescar As Boolean
Public blnBorrarSel As Boolean
Public blnAceptar As Boolean
Public strWherePersDup As String
Public vntCodPersDup As Variant
Public blnCommit As Boolean


Public Const ciPersF�sicas           As String = "CI1017"
Public Const ciSoluciones            As String = "CI1026"
Public Const ciRecordatorio          As String = "CI1023"
Public Const ciFasesRecursos         As String = "CI1027"
Public Const ciRecitacion            As String = "CI1029"
Public Const ciCitarSinPlanificar    As String = "CI2001"
Public Const ciFasesSolucion         As String = "CI1036"

Public Const ciCitasConfirmadas      As String = "CI0019"
Public Const ciEstCitasConfirmadas   As String = "CI0020"

Public lngUserCode As Long
Public lngDeptCode As Long




Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Set objMouse = objCW.objMouse
  Set objSecurity = objCW.objSecurity
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  
  'End
End Sub

'******************************************************************
'Funci�n que devuelve el siguiente n� de c�digo al �ltimo
'c�digo asignado.
'Tiene como par�metro de entrada una sentencia Sql que servira
'para crear el cursor
'******************************************************************

Public Function GetNewCode(strSql As String) As Long
 'Creaci�n de variables recordset
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  
 'Apertura del cursor
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

 'Establezco el n� de registros a recuperar a 1
  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
 'Devuelvo el nuevo n� de c�digo
  If rdoCodigo.RowCount = 0 Then
    GetNewCode = 1
  Else
    If IsNull(rdoCodigo(0)) Then
      GetNewCode = 1
    Else
      GetNewCode = rdoCodigo(0) + 1
    End If
  End If
 
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

'Devuelve una colecci�n de valores con las columnas de una tabla
'para la primera fila que cumple la condicion de strSql
Public Function GetTableColumn(strSql As String) As Collection
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  Dim intCol As Integer
  Dim cllValues As New Collection
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
  If rdoCodigo.RowCount = 0 Then
    For intCol = 0 To rdoCodigo.rdoColumns.Count - 1
       Call cllValues.Add("", rdoCodigo.rdoColumns(intCol).Name)
    Next
    
    'Set GetTableColumn = Nothing
  Else
    rdoCodigo.MoveFirst
    For intCol = 0 To rdoCodigo.rdoColumns.Count - 1
       If Not IsNull(rdoCodigo.rdoColumns(intCol).Value) Then
          Call cllValues.Add(rdoCodigo.rdoColumns(intCol).Value, rdoCodigo.rdoColumns(intCol).Name)
       Else
          Call cllValues.Add("", rdoCodigo.rdoColumns(intCol).Name)
       End If
    Next
  End If
 Set GetTableColumn = cllValues
 Set cllValues = Nothing
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

Public Sub ShowDetail(lngNumActPlan As Long, Optional lngNumSolicit As Long = -1, Optional lngNumCita As Long = -1)
  Dim strSql            As String
  Dim cllActuacion      As New Collection
  
  If (lngNumSolicit = -1 And lngNumCita = -1) Or (lngNumSolicit = 0 And lngNumCita = 0) Then
    strSql = "SELECT  CI21CODPERSONA, PR09NUMPETICION, " _
         & " PR09NUMGRUPO, PR03NUMACTPEDI, AD02CODDPTO, " _
         & " PR01CODACTUACION, PR03FECPREFEREN, PR03NUMACTPEDI_PRI, " _
         & " PR04NUMACTPLAN, PR37CODESTADO, CI31NUMSOLICIT, " _
         & " CI01NUMCITA, CI01SITCITA, AG05NUMINCIDEN, " _
         & " CI01FECCONCERT, CI01INDLISESPE, AG11CODRECURSO_PRE, " _
         & " AG11CODRECURSO_ASI FROM PR0431J WHERE " _
         & " PR04NUMACTPLAN=" & lngNumActPlan
  Else
      strSql = "SELECT  CI21CODPERSONA, PR09NUMPETICION, " _
         & " PR09NUMGRUPO, PR03NUMACTPEDI, AD02CODDPTO, " _
         & " PR01CODACTUACION, PR03FECPREFEREN, PR03NUMACTPEDI_PRI, " _
         & " PR04NUMACTPLAN, PR37CODESTADO, CI31NUMSOLICIT, " _
         & " CI01NUMCITA, CI01SITCITA, AG05NUMINCIDEN, " _
         & " CI01FECCONCERT, CI01INDLISESPE, AG11CODRECURSO_PRE, " _
         & " AG11CODRECURSO_ASI FROM PR0431J WHERE " _
         & " CI31NUMSOLICIT=" & lngNumSolicit _
         & " AND CI01NUMCITA=" & lngNumCita
  End If
  Set cllActuacion = GetTableColumn(strSql)
  Load frmDetalleCita
  With frmDetalleCita
    .txtText1(0) = cllActuacion("PR09NUMPETICION")
    .txtText1(1) = cllActuacion("PR09NUMGRUPO")
    .txtText1(2) = cllActuacion("PR03NUMACTPEDI")
    .txtText1(3) = cllActuacion("PR04NUMACTPLAN")
    .txtText1(4) = cllActuacion("CI31NUMSOLICIT")
    .txtText1(5) = cllActuacion("CI01NUMCITA")
    .txtText1(11) = Format(cllActuacion("PR03FECPREFEREN"), "DD/MM/YYYY")
    .txtText1(12) = Format(cllActuacion("PR03FECPREFEREN"), "HH:NN")
    .txtText1(13) = Format(cllActuacion("CI01FECCONCERT"), "DD/MM/YYYY")
    .txtText1(14) = Format(cllActuacion("CI01FECCONCERT"), "HH:NN")
    If cllActuacion("AD02CODDPTO") <> "" Then
      strSql = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & cllActuacion("AD02CODDPTO")
      .txtText1(8) = GetTableColumn(strSql)("AD02DESDPTO")
    End If
    If cllActuacion("PR01CODACTUACION") <> "" Then
      strSql = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & cllActuacion("PR01CODACTUACION")
      .txtText1(6) = GetTableColumn(strSql)("PR01DESCORTA")
    End If
    If cllActuacion("AG11CODRECURSO_PRE") <> "" Then
      strSql = "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" & cllActuacion("AG11CODRECURSO_PRE")
      .txtText1(9) = GetTableColumn(strSql)("AG11DESRECURSO")
    End If
    If cllActuacion("AG11CODRECURSO_ASI") <> "" Then
      strSql = "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" & cllActuacion("AG11CODRECURSO_ASI")
      .txtText1(10) = GetTableColumn(strSql)("AG11DESRECURSO")
    End If
    .txtText1(15) = cllActuacion("AG05NUMINCIDEN")
    If cllActuacion("AG05NUMINCIDEN") <> "" Then
      strSql = "SELECT AG05DESINCIDEN FROM AG0500 WHERE AG05NUMINCIDEN=" & cllActuacion("AG05NUMINCIDEN")
      .txtText1(16) = GetTableColumn(strSql)("AG05DESINCIDEN")
    End If
    
    Select Case cllActuacion("CI01SITCITA")
      Case ciPdteConfirmar
        .txtText1(7) = "PENDIENTE CONFIRMAR"
      Case ciConfirmada
        .txtText1(7) = "CITADA"
      Case ciAnulada
        .txtText1(7) = "ANULADA"
      Case ciRecitada
        .txtText1(7) = "RECITADA"
      Case ciPdteRecitar
        .txtText1(7) = "PENDIENTE RECITAR"
      Case Else
        .txtText1(7) = ""
    End Select
    
    .chkCheck1(0).Value = -Val(cllActuacion("CI01INDLISESPE"))
    .chkCheck1(0).Enabled = False
    .IdPersona1.Text = cllActuacion("CI21CODPERSONA")
    .IdPersona1.Enabled = False
   ' .IdPersona1.SearchPersona
    .Show (vbModal)
  End With
  Unload frmDetalleCita
  Set frmDetalleCita = Nothing
End Sub
'Funci�n que genera un nuevo c�digo de incidencia.
'Tiene dos par�metros de entrada :
'  -intCodMotInci -> C�digo del Motivo de Incidencia
'  -blnHideFrmInciden -> Si es True no saca la ventana de incidencias
'La funci�n devuelve:
'   -El nuevo numero de incidencia ( si todo ha ido bien )
'   -Si el intCodMotInci pasado no existe devuelve -1
'   -Si se cancela la operaci�n o intCodMotInci no est� activo
'    devuelve 0
Public Function AddInciden(intCodInci As Integer, Optional blnHideFrmInciden As Boolean = False) As Variant
  ' Variable resultset y query para la descripci�n del motivo
  Dim rdoMotivoInci As rdoResultset
  Dim rdoQueryMotivo As rdoQuery
  Dim rdoTiposIncidencia As rdoResultset
  
' Variable para la sentencia Sql
  Dim strSql As String
  
  Load frmIncidencias
' Inicializamos blnRollback a false
  blnRollback = False
  blnActive = True
' Abrimos el cursor para obtener la descripci�n del motivo
  
  strSql = "select AG06DESMOTINCI,AG06INDINCDISM, AG06FECACTIVA from AG0600 " _
         & "where AG06CODMOTINCI = " & intCodInci
  
  Set rdoQueryMotivo = objApp.rdoConnect.CreateQuery("", strSql)
 'Establecemos el tama�o del cursor a 1
  rdoQueryMotivo.MaxRows = 1
  Set rdoMotivoInci = rdoQueryMotivo.OpenResultset(rdOpenKeyset, _
                                                rdConcurReadOnly)
  
' Miramos si existe el c�digo del motivo de incidencia
  If rdoMotivoInci.RowCount = 0 Then
    'No existe el codigo de motivo incidencia
     Call objError.SetError(cwCodeMsg, "No existe el c�digo de motivo incidencia N�" & intCodMotInci)
     Call objError.Raise
     
    ' MsgBox "No existe el c�digo de motivo incidencia N�" & intCodMotInci, vbExclamation, "AGENDA. Incidencias"
     blnRollback = True
     AddInciden = -1
     
  Else
    'Si la fecha activa del c�digo de incidencia es
    'frmincidenciasnor que la actual visualizamos el form
    If CDate(rdoMotivoInci("AG06FECACTIVA")) < CDate(Format(objGen.GetDBDateTime, "dd/mm/yyyy")) Then
      With frmIncidencias
      If blnHideFrmInciden Then
        .cboSSDBCombo1(0).Text = rdoMotivoInci("AG06DESMOTINCI")
        Call .A�adir_Incidencia(intCodInci)
        blnHideFrmInciden = False
      Else
        'Asignamos la descripci�n del motivo de incidencia
        .ConfigureWindow (Val(rdoMotivoInci("AG06INDINCDISM")))
        If Val(rdoMotivoInci("AG06INDINCDISM")) = 1 Then
           .cboSSDBCombo1(0).Text = rdoMotivoInci("AG06DESMOTINCI")
        Else
          strSql = "SELECT AG06CODMOTINCI, AG06DESMOTINCI,AG06INDINCDISM FROM AG0600 WHERE AG06INDINCDISM=" & Val(rdoMotivoInci("AG06INDINCDISM")) & " AND AG06FECACTIVA < " & objGen.ValueToSQL(objGen.GetDBDateTime, cwDate)
          Set rdoTiposIncidencia = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly, rdConcurReadOnly)
          Do While Not rdoTiposIncidencia.EOF
            Call .cboSSDBCombo1(0).AddItem(rdoTiposIncidencia(0) & "," & rdoTiposIncidencia(1))
            rdoTiposIncidencia.MoveNext
          Loop
          .cboSSDBCombo1(0).Value = intCodInci
          rdoTiposIncidencia.Close
        End If
        intCodMotInci = intCodInci
        .Show (vbModal)
      
        .MousePointer = vbDefault
      
      End If
      End With
      If blnRollback Then
        AddInciden = 0 ' Han pulsado cancelar
      Else
        AddInciden = vntNuevoCod
      End If
    Else
     'Si la fecha es mayor cancelamos la incidencia
     'pero no realizamos rollback
      AddInciden = 0
      blnActive = False
      
    End If
  End If
  Unload frmIncidencias
  Set frmIncidencias = Nothing
End Function

'Procedimiento que muestra los datos de un numero de incidencia

Public Sub ShowInciden(lngNumInciden As Long)
  Dim strSql As String
  Dim cllCodInciden As New Collection
  Dim cllIncidencia As New Collection
  
  strSql = "SELECT AG06CODMOTINCI,AG05DESALCANCE,AG05DESSOLUCIO FROM AG0500 WHERE AG05NUMINCIDEN=" & lngNumInciden
  Set cllIncidencia = GetTableColumn(strSql)
  If cllIncidencia(1) <> "" Then
    strSql = "SELECT AG06DESMOTINCI,AG06INDINCDISM FROM AG0600 WHERE AG06CODMOTINCI=" & Val(cllIncidencia(1))
    Set cllCodInciden = GetTableColumn(strSql)
    If cllCodInciden(1) = "" Then
      'No existe el codigo de motivo incidencia
      Call objError.SetError(cwCodeMsg, "No existe el c�digo de motivo incidencia N�" & cllCodInciden(0))
      Call objError.Raise
    Else
      Load frmIncidencias
      With frmIncidencias
        Call .ConfigureWindow(Val(cllCodInciden(2)))
        .cboSSDBCombo1(0).Text = cllCodInciden(1)
        .cboSSDBCombo1(0).Enabled = False
        .txtText1(1) = cllIncidencia(2)
        If Val(cllCodInciden(2)) = 1 Then
          .txtText1(2) = cllIncidencia(3)
        End If
        .txtText1(1).Locked = True
        .txtText1(2).Locked = True
        .cmdCommand1(0).Enabled = False
        .cmdCommand1(1).Enabled = False
        .Show (vbModal)
      End With
      Unload frmIncidencias
      Set frmIncidencias = Nothing
    End If
  Else
    Call objError.SetError(cwCodeMsg, "No existe el N�mero de Incidencia")
    Call objError.Raise
  
  End If
End Sub
Public Sub Main()
 'No hace nada
End Sub
Public Sub PrintReport(strHistoric As String)
  Dim objReport As New clsCWReport
  Dim strReportDesc As String
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
 
  Select Case strHistoric
    Case ciCitasConfirmadas
       strReportDesc = "Citas Conformadas"
    Case ciEstCitasConfirmadas
      strReportDesc = "Estad�stica de Citas Gestionadas"
  End Select
  If strReportDesc <> "" Then
    Call objReport.AddReport(strHistoric, strReportDesc)
    
    With objReport.PrinterDialog(False, strHistoric)
      intReport = .Selected
      If intReport > 0 Then
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
End Sub

