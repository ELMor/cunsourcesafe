VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWLauncher
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
Const CIRepTipoPoliza            As String = "CI0016"
Const CIRepColectivos            As String = "CI0002"
Const CIRepEntidadColab          As String = "CI0004"
Const CIRepCentroUniversidad     As String = "CI0001"
Const CIRepMotivoDescuento       As String = "CI0006"
Const CIRepRelTitular            As String = "CI0014"
Const CIRepConciertos            As String = "CI0003"
Const CIRepTipoEconomico         As String = "CI0015"
Const CIRepPersonalUni           As String = "CI0011"
'Const CIRepACUNSA                As String = "CI0010"
Const CIRepEstadoCivil           As String = "CI0005"
Const CIRepProfesion             As String = "CI0012"
Const CIRepTratamiento           As String = "CI0017"
Const CIRepVinculoFamiliar       As String = "CI0018"
Const CIRepPais                  As String = "CI0007"
Const CIRepProvincia             As String = "CI0013"
Const CIRepPersFisicas           As String = "CI0009"
Const CIRepPersJuridicas         As String = "CI0010"
'Const CIRepAvisos                As String = "CI0017"
Const CIRepPatologias            As String = "CI0008"
Const CIRepRecordatorio          As String = "CI0021"
Const CIRepRecordatorioMano      As String = "CI0022"
Const CIRepCitasConfirmadas      As String = "CI0019"
Const CIRepCitasGestionadas      As String = "CI0020"
Const CIRepRelacionesUDN         As String = "CI0023"

' Ahora las ventanas. Continuan la numeraci�n a partir del CI1000
Const CIWinTipoPoliza            As String = "CI1001"
Const CIWinColectivos            As String = "CI1002"
Const CIWinEntidadColab          As String = "CI1003"
Const CIWinCentroUniversidad     As String = "CI1004"
Const CIWinMotivoDescuento       As String = "CI1005"
Const CIWinRelTitular            As String = "CI1006"
Const CIWinConciertos            As String = "CI1007"
Const CIWinTipoEconomico         As String = "CI1008"
Const CIWinPersonalUni           As String = "CI1009"
Const CIWinACUNSA                As String = "CI1010"
Const CIWinEstadoCivil           As String = "CI1011"
Const CIWinProfesion             As String = "CI1012"
Const CIWinTratamiento           As String = "CI1013"
Const CIWinVinculoFamiliar       As String = "CI1014"
Const CIWinPais                  As String = "CI1015"
Const CIWinProvincia             As String = "CI1016"
Const CIWinPersFisicas           As String = "CI1017"
Const CIWinPersJuridicas         As String = "CI1018"
Const CIWinAvisos                As String = "CI1019"
Const CIWinPatologias            As String = "CI1020"
Const CIWinRecordatorio          As String = "CI1023"
Const CIWinCargaMunicip          As String = "CI1021"
Const CIWinCargaTextos           As String = "CI1022"
Const CIWinDetallePersona        As String = "CI1031"
Const CIWinDocAutorizacion       As String = "CI1035"
Const CIWinFasesSolucion         As String = "CI1036"
Const CIWinRelacionUDN           As String = "CI1037"

'*******
Const CIWinCitasPaciente         As String = "CI1024"
Const CIWinPeticiones            As String = "CI1025"
Const CIWinSoluciones            As String = "CI1026"
Const CIWinFasesRecursos         As String = "CI1027"
Const CIWinRecitacion            As String = "CI1029"
Const CIWinDetalleCita           As String = "CI1030"
Const CIWinCitasRecurso          As String = "CI1028"
Const CIWinListaEsperaPac        As String = "CI1033"
Const CIWinListaEsperaRec        As String = "CI1034"


' Ahora las rutinas. Continuan la numeraci�n a partir del CI2000
 Const CIRutCitarSinPlanificar   As String = "CI2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
    
    Case CIWinRelacionUDN
      'Call objSecurity.AddHelpContext(1)
      Call RelacionUDN
      Call objSecurity.RemoveHelpContext
    
    Case CIWinTratamiento
      Call objSecurity.AddHelpContext(1)
      Call Tratamientos
      Call objSecurity.RemoveHelpContext
    Case CIWinEstadoCivil
      Call objSecurity.AddHelpContext(2)
      Call Estados_Civiles
      Call objSecurity.RemoveHelpContext
    Case CIWinVinculoFamiliar
      Call objSecurity.AddHelpContext(3)
      Call V�nculos_Familiares
      Call objSecurity.RemoveHelpContext
    Case CIWinProfesion
      Call objSecurity.AddHelpContext(4)
      Call Profesiones
      Call objSecurity.RemoveHelpContext
    Case CIWinCentroUniversidad
      Call objSecurity.AddHelpContext(5)
      Call Centros_Cargo
      Call objSecurity.RemoveHelpContext
    Case CIWinColectivos
      Call objSecurity.AddHelpContext(6)
      Call Colectivos
      Call objSecurity.RemoveHelpContext
    Case CIWinACUNSA
      Call objSecurity.AddHelpContext(19)
      Load frmAsegurados
      Call frmAsegurados.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmAsegurados
      Set frmAsegurados = Nothing
    Case CIWinTipoEconomico
      Call objSecurity.AddHelpContext(152)
      Load frmTipoEcon
      Call frmTipoEcon.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmTipoEcon
      Set frmTipoEcon = Nothing
    Case CIWinPersFisicas
      Call objSecurity.AddHelpContext(143)
      Load frmPersF�sicas
      Call frmPersF�sicas.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmPersF�sicas
      Set frmPersF�sicas = Nothing
    Case CIWinTipoPoliza
      Call objSecurity.AddHelpContext(7)
      Call Tipo_Poliza
      Call objSecurity.RemoveHelpContext
    Case CIWinEntidadColab
      Call objSecurity.AddHelpContext(8)
      Call Entidad_Colaboradora
      Call objSecurity.RemoveHelpContext
    Case CIWinMotivoDescuento
      Call objSecurity.AddHelpContext(9)
      Call Motivo_Descuento
      Call objSecurity.RemoveHelpContext
    Case CIWinRelTitular
      Call objSecurity.AddHelpContext(12)
      Call Relaci�n_Titular
      Call objSecurity.RemoveHelpContext
    Case CIWinConciertos
      Call objSecurity.AddHelpContext(160)
      Load frmConcier
      Call frmConcier.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmConcier
      Set frmConcier = Nothing
    Case CIWinPersonalUni
      Call objSecurity.AddHelpContext(24)
      Load frmPersoUni
      Call frmPersoUni.Show(vbModal)
      'Unload frmPersoUni
      Set frmPersoUni = Nothing
      Call objSecurity.RemoveHelpContext
    Case CIWinPais
      Call objSecurity.AddHelpContext(14)
      Call Paises
      Call objSecurity.RemoveHelpContext
    Case CIWinProvincia
      Call objSecurity.AddHelpContext(68)
      Load frmProvMuniLoc
      Call frmProvMuniLoc.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmProvMuniLoc
      Set frmProvMuniLoc = Nothing
    Case CIWinPersJuridicas
      Call objSecurity.AddHelpContext(144)
      Load frmPersJur�dicas
      Call frmPersJur�dicas.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmPersJur�dicas
      Set frmPersJur�dicas = Nothing
    Case CIWinPatologias
      Call objSecurity.AddHelpContext(34)
      Load frmPatActu
      Call frmPatActu.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmPatActu
      Set frmPatActu = Nothing
    Case CIWinCargaMunicip
      Call objSecurity.AddHelpContext(47)
      Load frmCargaMunicipios
      Call frmCargaMunicipios.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmCargaMunicipios
      Set frmCargaMunicipios = Nothing
     Case CIWinCargaTextos
      Call objSecurity.AddHelpContext(47)
      Load frmCargaTextos
      Call frmCargaTextos.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmCargaTextos
      Set frmCargaTextos = Nothing
    Case CIWinRecordatorio
      'Call objSecurity.AddHelpContext(1)
      If vntData = Empty Then
        Load frmRecordatorio
        Call frmRecordatorio.Show(vbModal)
        'Unload frmRecordatorio
        Set frmRecordatorio = Nothing
      Else
        Load frmRecordatorio
        With frmRecordatorio
          .lngNumSolicit = Val(vntData)
          .dtcDateCombo1(0).Date = ""
          .dtcDateCombo1(1).Date = ""
          .dtcDateCombo1(0).BackColor = objApp.objColor.lngReadOnly
          .dtcDateCombo1(1).BackColor = objApp.objColor.lngReadOnly
          .dtcDateCombo1(0).Enabled = False
          .dtcDateCombo1(1).Enabled = False
          .txtText1(0).BackColor = objApp.objColor.lngReadOnly
          .txtText1(1).BackColor = objApp.objColor.lngReadOnly
          .txtText1(2).BackColor = objApp.objColor.lngReadOnly
          .txtText1(4).BackColor = objApp.objColor.lngReadOnly
          .chkCheck1(0).Enabled = False
          .chkCheck1(1).Enabled = False
          .cmdCommand1(0).Enabled = False
          .cmdCommand1(1).Enabled = False
          .cmdCommand1(2).Enabled = False
          .cboSSDBCombo1(0).BackColor = objApp.objColor.lngReadOnly
          .cboSSDBCombo1(0).Enabled = False
          .fraFrame1(0).Enabled = False
          .fraFrame1(2).Enabled = False
          .Show vbModal
        End With
        'Unload frmRecordatorio
        Set frmRecordatorio = Nothing
      End If
     'Call objSecurity.RemoveHelpContext
     
     Case CIWinCitasPaciente
      Call objSecurity.AddHelpContext(101)
      Load frmCitasPaciente
      Call frmCitasPaciente.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmCitasPaciente
      Set frmCitasPaciente = Nothing
     
     Case CIWinPeticiones
      Call objSecurity.AddHelpContext(134)
      Load frmPeticion3
      Call frmPeticion3.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmPeticion3
      Set frmPeticion3 = Nothing
     
     Case CIWinSoluciones
      Call objSecurity.AddHelpContext(145)
      Load frmSolicitud
      frmSolicitud.MousePointer = vbDefault
      frmSolicitud.Show vbModal
      Call objSecurity.RemoveHelpContext
      'Unload frmSolicitud
      Set frmSolicitud = Nothing
     
     Case CIWinFasesRecursos
      Call objSecurity.AddHelpContext(123)
      Load frmFasesPCR
      If vntData = True Then frmFasesPCR.blnDataReadOnly = True
      frmFasesPCR.MousePointer = vbDefault
      frmFasesPCR.Show vbModal
      Call objSecurity.RemoveHelpContext
      'Unload frmFasesPCR
      Set frmFasesPCR = Nothing
     
     Case CIWinRecitacion
      ' Call objSecurity.AddHelpContext(47)
       Load frmRecitaCion
       frmRecitaCion.Show vbModal
       'Unload frmRecitaCion
       Set frmRecitaCion = Nothing
     'Call objSecurity.RemoveHelpContext

    Case CIWinCitasRecurso
      Call objSecurity.AddHelpContext(112)
      Load frmCitasRecursos
      Call frmCitasRecursos.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmCitasRecursos
      Set frmCitasRecursos = Nothing
    Case CIWinListaEsperaPac
      Call objSecurity.AddHelpContext(79)
      If Val(vntData) > 0 Then
        lngUserCode = Val(vntData)
      End If
      Load frmListaEsperaPaciente
      Call frmListaEsperaPaciente.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmListaEsperaPaciente
      lngUserCode = 0
      Set frmListaEsperaPaciente = Nothing
    Case CIWinListaEsperaRec
      Call objSecurity.AddHelpContext(90)
      If Val(vntData(1)) > 0 Then
        lngUserCode = Val(vntData(1))
        lngDeptCode = Val(vntData(2))
      End If

      Load frmListaEsperaRecurso
      Call frmListaEsperaRecurso.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      'Unload frmListaEsperaRecurso
      Set frmListaEsperaRecurso = Nothing
      lngUserCode = 0
      lngDeptCode = 0
    Case CIRutCitarSinPlanificar
      Call frmSolicitud.CitarSinPlanificar
    
    Case CIWinFasesSolucion
      If CLng(vntData) > 0 Then
        Call frmSolicitud.ShowFasesPorObj(CLng(vntData))
      End If
    
     Case CIRepCitasConfirmadas
      Call PrintReport(CIRepCitasConfirmadas)

     Case CIRepCitasGestionadas
      Call PrintReport(CIRepCitasGestionadas)
    
    
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n�mero de procesos
  ReDim aProcess(1 To 58, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = CIWinTipoPoliza
  aProcess(1, 2) = "Mantenimiento de Tipos de Poliza"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = CIWinColectivos
  aProcess(2, 2) = "Mantenimiento de Colectivos"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = CIWinEntidadColab
  aProcess(3, 2) = "Mantenimiento de Entidades Colaboradoras"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = CIWinCentroUniversidad
  aProcess(4, 2) = "Mantenimiento de Centros Cargo"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = CIWinMotivoDescuento
  aProcess(5, 2) = "Mantenimiento de Motivos de Descuento"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = CIWinRelTitular
  aProcess(6, 2) = "Mantenimiento de Relaciones del Titular"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = CIWinConciertos
  aProcess(7, 2) = "Mantenimiento de Conciertos Econ�micos"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = CIWinTipoEconomico
  aProcess(8, 2) = "Mantenimiento de Tipos Econ�micos"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow
      
  aProcess(9, 1) = CIWinPersonalUni
  aProcess(9, 2) = "Mantenimiento de Personal Universitario"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = CIWinACUNSA
  aProcess(10, 2) = "Mantenimiento de Asegurados ACUNSA"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
     
  aProcess(11, 1) = CIWinEstadoCivil
  aProcess(11, 2) = "Mantenimiento de Estados Civiles"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow
      
  aProcess(12, 1) = CIWinProfesion
  aProcess(12, 2) = "Mantenimiento de Profesiones"
  aProcess(12, 3) = True
  aProcess(12, 4) = cwTypeWindow
      
  aProcess(13, 1) = CIWinTratamiento
  aProcess(13, 2) = "Mantenimiento de Tratamientos"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
      
  aProcess(14, 1) = CIWinVinculoFamiliar
  aProcess(14, 2) = "Mantenimiento de V�nculos Familiares"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
      
  aProcess(15, 1) = CIWinPais
  aProcess(15, 2) = "Mantenimiento de Pa�ses"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow
      
  aProcess(16, 1) = CIWinProvincia
  aProcess(16, 2) = "Mantenimiento de Provincias-Municipios"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
      
  aProcess(17, 1) = CIWinPersFisicas
  aProcess(17, 2) = "Mantenimiento de Personas F�sicas"
  aProcess(17, 3) = True
  aProcess(17, 4) = cwTypeWindow
      
  aProcess(18, 1) = CIWinPersJuridicas
  aProcess(18, 2) = "Mantenimiento de Personas Jur�dicas"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
      
  aProcess(19, 1) = CIWinPatologias
  aProcess(19, 2) = "Mantenimiento de Patolog�as"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
      
  aProcess(20, 1) = CIWinRecordatorio
  aProcess(20, 2) = "Emitir Recordatorio"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeWindow
      
  aProcess(21, 1) = CIWinCargaMunicip
  aProcess(21, 2) = "Carga de Municipios-Localidades"
  aProcess(21, 3) = True
  aProcess(21, 4) = cwTypeWindow
      
  aProcess(22, 1) = CIWinCargaTextos
  aProcess(22, 2) = "Carga de Ficheros de Texto"
  aProcess(22, 3) = True
  aProcess(22, 4) = cwTypeWindow
     
  ' LISTADOS
  aProcess(23, 1) = CIRepTipoPoliza
  aProcess(23, 2) = "Relaci�n de Tipos de Poliza"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeReport
      
  aProcess(24, 1) = CIRepColectivos
  aProcess(24, 2) = "Relaci�n de Colectivos"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeReport
  
  aProcess(25, 1) = CIRepEntidadColab
  aProcess(25, 2) = "Relaci�n de Entidades Colaboradoras"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeReport
  
  aProcess(26, 1) = CIRepCentroUniversidad
  aProcess(26, 2) = "Relaci�n de Centros Universidad"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeReport

  aProcess(27, 1) = CIRepMotivoDescuento
  aProcess(27, 2) = "Relaci�n de Motivos de Descuento"
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeReport
  
  aProcess(28, 1) = CIRepConciertos
  aProcess(28, 2) = "Relaci�n de Conciertos Econ�micos"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeReport

  aProcess(29, 1) = CIRepRelTitular
  aProcess(29, 2) = "Relaci�n de Relaciones del Titular"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeReport

  aProcess(30, 1) = CIRepTipoEconomico
  aProcess(30, 2) = "Relaci�n de Tipos Econ�micos"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeReport

  aProcess(31, 1) = CIRepPersonalUni
  aProcess(31, 2) = "Relaci�n de Personal de Universidad"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeReport

 ' aProcess(32, 1) = CIRepACUNSA
 ' aProcess(32, 2) = "Relaci�n de Asegurados ACUNSA"
 ' aProcess(32, 3) = False
 ' aProcess(32, 4) = cwTypeReport

  aProcess(33, 1) = CIRepEstadoCivil
  aProcess(33, 2) = "Relaci�n de Estados Civiles"
  aProcess(33, 3) = False
  aProcess(33, 4) = cwTypeReport

  aProcess(34, 1) = CIRepProfesion
  aProcess(34, 2) = "Relaci�n de Profesiones"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeReport

  aProcess(35, 1) = CIRepTratamiento
  aProcess(35, 2) = "Relaci�n de Tratamientos"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeReport

  aProcess(36, 1) = CIRepVinculoFamiliar
  aProcess(36, 2) = "Relaci�n de V�nculos Familiares"
  aProcess(36, 3) = False
  aProcess(36, 4) = cwTypeReport

  aProcess(37, 1) = CIRepPais
  aProcess(37, 2) = "Relaci�n de Pa�ses"
  aProcess(37, 3) = False
  aProcess(37, 4) = cwTypeReport

  aProcess(38, 1) = CIRepProvincia
  aProcess(38, 2) = "Relaci�n de Provincias-Municipios"
  aProcess(38, 3) = False
  aProcess(38, 4) = cwTypeReport

  aProcess(39, 1) = CIRepPersFisicas
  aProcess(39, 2) = "Relaci�n de Personas F�sicas"
  aProcess(39, 3) = False
  aProcess(39, 4) = cwTypeReport

  aProcess(40, 1) = CIRepPersJuridicas
  aProcess(40, 2) = "Relaci�n de Personas Jur�dicas"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeReport

  aProcess(41, 1) = CIRepPatologias
  aProcess(41, 2) = "Relaci�n de Patolog�as"
  aProcess(41, 3) = False
  aProcess(41, 4) = cwTypeReport

  aProcess(42, 1) = CIRepRecordatorio
  aProcess(42, 2) = "Recordatorio (Correo)"
  aProcess(42, 3) = False
  aProcess(42, 4) = cwTypeReport

'Proceso de Peticiones de Citas
  aProcess(43, 1) = CIWinCitasPaciente
  aProcess(43, 2) = "Mantenimiento de Citas (Pacientes)"
  aProcess(43, 3) = True
  aProcess(43, 4) = cwTypeWindow

  aProcess(44, 1) = CIWinPeticiones
  aProcess(44, 2) = "Peticiones de Citas"
  aProcess(44, 3) = True
  aProcess(44, 4) = cwTypeWindow

  aProcess(45, 1) = CIWinRecitacion
  aProcess(45, 2) = "Mantenimiento de Recitaciones"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeWindow

  aProcess(46, 1) = CIWinSoluciones
  aProcess(46, 2) = "Mantenimiento de Soluciones a Peticiones"
  aProcess(46, 3) = False
  aProcess(46, 4) = cwTypeWindow
  
  aProcess(47, 1) = CIWinFasesRecursos
  aProcess(47, 2) = "Fases/Recursos por Actuaci�n"
  aProcess(47, 3) = False
  aProcess(47, 4) = cwTypeWindow
  
  aProcess(48, 1) = CIWinCitasRecurso
  aProcess(48, 2) = "Mantenimiento de Citas (Recursos)"
  aProcess(48, 3) = True
  aProcess(48, 4) = cwTypeWindow
  
  aProcess(49, 1) = CIWinListaEsperaPac
  aProcess(49, 2) = "Mantenimiento Lista Espera (Pacientes)"
  aProcess(49, 3) = True
  aProcess(49, 4) = cwTypeWindow

  aProcess(50, 1) = CIWinListaEsperaRec
  aProcess(50, 2) = "Mantenimiento Lista Espera (Recursos)"
  aProcess(50, 3) = True
  aProcess(50, 4) = cwTypeWindow
 
  aProcess(51, 1) = CIWinDocAutorizacion
  aProcess(51, 2) = "Mantenimiento Documentos Autorizaci�n"
  aProcess(51, 3) = False
  aProcess(51, 4) = cwTypeWindow

  aProcess(52, 1) = CIRutCitarSinPlanificar
  aProcess(52, 2) = "Rutina que Cita sin tener en cuenta la Agenda"
  aProcess(52, 3) = False
  aProcess(52, 4) = cwTypeRoutine

  aProcess(53, 1) = CIWinFasesSolucion
  aProcess(53, 2) = "Fases de las Actuaciones devueltas como soluciones"
  aProcess(53, 3) = False
  aProcess(53, 4) = cwTypeWindow

  aProcess(54, 1) = CIRepRecordatorioMano
  aProcess(54, 2) = "Recordatorio (Mano)"
  aProcess(54, 3) = False
  aProcess(54, 4) = cwTypeReport

  aProcess(55, 1) = CIRepCitasConfirmadas
  aProcess(55, 2) = "Relaci�n de Citas Confirmadas"
  aProcess(55, 3) = True
  aProcess(55, 4) = cwTypeReport

  aProcess(56, 1) = CIRepCitasGestionadas
  aProcess(56, 2) = "Informe Estad�stico de Citas Gestionadas"
  aProcess(56, 3) = True
  aProcess(56, 4) = cwTypeReport

  aProcess(57, 1) = CIWinRelacionUDN
  aProcess(57, 2) = "Mantenimiento de Relaciones con la Universidad"
  aProcess(57, 3) = True
  aProcess(57, 4) = cwTypeWindow

  aProcess(58, 1) = CIRepRelacionesUDN
  aProcess(58, 2) = "Listado de Relaciones con la Universidad"
  aProcess(58, 3) = False
  aProcess(58, 4) = cwTypeReport

End Sub
