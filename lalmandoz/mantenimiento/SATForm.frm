VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form SATForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Servicios de Asistencia Tecnica"
   ClientHeight    =   3675
   ClientLeft      =   2865
   ClientTop       =   1545
   ClientWidth     =   6150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3675
   ScaleWidth      =   6150
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   8
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "Prim"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntrBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "Ult"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
      EndProperty
   End
   Begin VB.TextBox IndiceBox 
      Height          =   285
      Left            =   5160
      TabIndex        =   15
      Text            =   "0"
      Top             =   960
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Data SAT 
      Caption         =   "SAT"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      EOFAction       =   2  'Add New
      Exclusive       =   0   'False
      Height          =   420
      Left            =   4080
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SAT"
      Top             =   1800
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.TextBox Text7 
      DataField       =   "Fax"
      DataSource      =   "SAT"
      Height          =   285
      Left            =   2040
      TabIndex        =   13
      Text            =   "Text7"
      Top             =   3240
      Width           =   1575
   End
   Begin VB.TextBox Text6 
      DataField       =   "Telefono"
      DataSource      =   "SAT"
      Height          =   285
      Left            =   2040
      TabIndex        =   12
      Text            =   "Text6"
      Top             =   2880
      Width           =   1575
   End
   Begin VB.TextBox Text5 
      DataField       =   "CodigoPostal"
      DataSource      =   "SAT"
      Height          =   285
      Left            =   2040
      TabIndex        =   11
      Text            =   "Text5"
      Top             =   2520
      Width           =   1215
   End
   Begin VB.TextBox Text4 
      DataField       =   "Pais"
      DataSource      =   "SAT"
      Height          =   285
      Left            =   2040
      TabIndex        =   10
      Text            =   "Text4"
      Top             =   2160
      Width           =   1815
   End
   Begin VB.TextBox Text3 
      DataField       =   "Ciudad"
      DataSource      =   "SAT"
      Height          =   285
      Left            =   2040
      TabIndex        =   9
      Text            =   "Text3"
      Top             =   1800
      Width           =   1815
   End
   Begin VB.TextBox Text2 
      DataField       =   "Direccion"
      DataSource      =   "SAT"
      Height          =   285
      Left            =   2040
      TabIndex        =   8
      Text            =   "Text2"
      Top             =   1440
      Width           =   3975
   End
   Begin VB.TextBox CompBox 
      DataField       =   "Nombre"
      DataSource      =   "SAT"
      Height          =   285
      Left            =   2040
      TabIndex        =   7
      Text            =   "Text1"
      Top             =   1080
      Width           =   2055
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   5040
      Top             =   2760
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATForm.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATForm.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATForm.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATForm.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATForm.frx":0C68
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATForm.frx":0F82
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATForm.frx":129C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATForm.frx":15B6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label7 
      Caption         =   "Fax"
      Height          =   255
      Left            =   600
      TabIndex        =   6
      Top             =   3240
      Width           =   735
   End
   Begin VB.Label Label6 
      Caption         =   "Tel�fono"
      Height          =   255
      Left            =   600
      TabIndex        =   5
      Top             =   2880
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "C�digo Postal"
      Height          =   255
      Left            =   600
      TabIndex        =   4
      Top             =   2520
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Pa�s"
      Height          =   255
      Left            =   600
      TabIndex        =   3
      Top             =   2160
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Ciudad"
      Height          =   255
      Left            =   600
      TabIndex        =   2
      Top             =   1800
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Direcci�n"
      Height          =   255
      Left            =   600
      TabIndex        =   1
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Compa�ia"
      Height          =   255
      Left            =   600
      TabIndex        =   0
      Top             =   1080
      Width           =   855
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu first 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sig 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu lista 
      Caption         =   "&Listados"
      Begin VB.Menu totos 
         Caption         =   "Todos"
      End
   End
End
Attribute VB_Name = "SATForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function Anterior()
    SAT.Recordset.MovePrevious
 
    If SAT.Recordset.BOF = True Then
        SAT.Recordset.MoveNext
    End If
    
End Function

Private Function OtroSAT()
On Error GoTo msgError
    SAT.Recordset.MoveLast
    SAT.Recordset.AddNew
    SAT.Recordset.Fields("Nombre").Value = "(" & SAT.Recordset.RecordCount & ") Sin Introducir"
    SAT.Recordset.Update
    SAT.Recordset.MoveLast
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Siguiente()
    
    SAT.Recordset.MoveNext
    
    If SAT.Recordset.EOF = True Then
        SAT.Recordset.MovePrevious
    End If

End Function

Private Sub add_Click()
    Dim nada As Integer
    nada = OtroSAT()
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
End Sub

Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar el SAT?", vbYesNo)
    If nada = vbYes Then
        SAT.Recordset.Delete
        nada = Anterior()
    End If

End Sub

Private Sub exit_Click()

    SAT.UpdateRecord
    Select Case IndiceBox.Text
    Case "1"
        SATAlbForm.SATData.Refresh
    Case "2"
        PedSATForm.SATData.Refresh
    Case "3"
        SATFForm.SATData.Refresh
        
    End Select
    Unload Me
    
End Sub

Private Sub first_Click()
    SAT.Recordset.MoveFirst
End Sub

Private Sub last_Click()
    SAT.Recordset.MoveLast
End Sub

Private Sub sig_Click()
    Dim nada As Integer
    nada = Siguiente()
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        SAT.UpdateRecord
        Select Case IndiceBox.Text
        Case "1"
            SATAlbForm.SATData.Refresh
        Case "2"
            PedSATForm.SATData.Refresh
        Case "3"
            SATFForm.SATData.Refresh

        
        End Select
        Unload Me
    Case "Add"
        nada = OtroSAT()
    
    Case "Lista"
        nada = Listasat()

    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar el SAT?", vbYesNo)
        If nada = vbYes Then
            SAT.Recordset.Delete
            nada = Anterior()
        End If
    Case "Prim"
        SAT.Recordset.MoveFirst
    Case "Ult"
        SAT.Recordset.MoveLast

    End Select
    

End Sub

Private Function Listasat()
    Dim nada As Integer
    
    nada = NuevaBusq()
    SELE = "Nombre, Direccion, Ciudad"
    FRO = "SAT"
    WHER = ""
    ORD = "Nombre"
    
    Display(0) = "Nombre"
    Display(1) = "Direccion"
    Display(2) = "Ciudad"
    For nada = 0 To 2
        campos(nada) = Display(nada)
    Next
    
    Set Objeto = SATForm.SAT
    
    Selct2 = 2
    
    Selct = False
    
    MosBusForm.Show modal

End Function






Private Sub totos_Click()
    Dim nada As Integer
    nada = Listasat()
End Sub
