VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form BusqForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Par�metros de B�squeda"
   ClientHeight    =   7185
   ClientLeft      =   2640
   ClientTop       =   1215
   ClientWidth     =   7740
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   7740
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   "Frame2"
      Height          =   735
      Left            =   360
      TabIndex        =   3
      Top             =   1320
      Visible         =   0   'False
      Width           =   6975
      Begin VB.CheckBox Check2 
         Caption         =   "Check1"
         Height          =   255
         Left            =   3360
         TabIndex        =   47
         Top             =   360
         Width           =   255
      End
      Begin SSDataWidgets_B.SSDBCombo Frame2Combo2 
         Bindings        =   "BusqForm.frx":0000
         Height          =   375
         Left            =   4440
         TabIndex        =   18
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo Frame2Date2 
         Height          =   375
         Left            =   4440
         TabIndex        =   17
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Frame2Combo1 
         Bindings        =   "BusqForm.frx":0010
         Height          =   375
         Left            =   720
         TabIndex        =   16
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo Frame2Date1 
         Height          =   375
         Left            =   720
         TabIndex        =   15
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox Frame2Text2 
         Height          =   375
         Left            =   4440
         TabIndex        =   14
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Frame2Text1 
         Height          =   375
         Left            =   720
         TabIndex        =   12
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   13
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Frame5"
      Height          =   735
      Left            =   360
      TabIndex        =   6
      Top             =   3840
      Visible         =   0   'False
      Width           =   6975
      Begin SSDataWidgets_B.SSDBCombo Frame5Combo1 
         Bindings        =   "BusqForm.frx":0020
         Height          =   375
         Left            =   720
         TabIndex        =   44
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CheckBox Check5 
         Caption         =   "Check1"
         Height          =   255
         Left            =   3360
         TabIndex        =   50
         Top             =   360
         Width           =   255
      End
      Begin SSDataWidgets_B.SSDBCombo Frame5Combo2 
         Bindings        =   "BusqForm.frx":0030
         Height          =   375
         Left            =   4440
         TabIndex        =   45
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo Frame5Date2 
         Height          =   375
         Left            =   4440
         TabIndex        =   43
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo Frame5Date1 
         Height          =   375
         Left            =   720
         TabIndex        =   42
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox Frame5Text2 
         Height          =   375
         Left            =   4440
         TabIndex        =   33
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Frame5Text1 
         Height          =   375
         Left            =   720
         TabIndex        =   32
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label5 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   27
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Data Data4 
      Caption         =   "Data4"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6720
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data Data3 
      Caption         =   "Data3"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data Data2 
      Caption         =   "Data2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   5640
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6720
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame Frame7 
      Caption         =   "Frame7"
      Height          =   735
      Left            =   360
      TabIndex        =   8
      Top             =   5520
      Visible         =   0   'False
      Width           =   6975
      Begin VB.CheckBox Check7 
         Caption         =   "Check1"
         Height          =   255
         Left            =   3360
         TabIndex        =   52
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Frame7Text2 
         Height          =   375
         Left            =   4440
         TabIndex        =   37
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Frame7Text1 
         Height          =   375
         Left            =   720
         TabIndex        =   36
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label7 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   29
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Frame6"
      Height          =   735
      Left            =   360
      TabIndex        =   7
      Top             =   4680
      Visible         =   0   'False
      Width           =   6975
      Begin VB.CheckBox Check6 
         Caption         =   "Check1"
         Height          =   255
         Left            =   3360
         TabIndex        =   51
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Frame6Text2 
         Height          =   375
         Left            =   4440
         TabIndex        =   35
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Frame6Text1 
         Height          =   375
         Left            =   720
         TabIndex        =   34
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label6 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   28
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Frame4"
      Height          =   735
      Left            =   360
      TabIndex        =   5
      Top             =   3000
      Visible         =   0   'False
      Width           =   6975
      Begin VB.CheckBox Check4 
         Caption         =   "Check1"
         Height          =   255
         Left            =   3360
         TabIndex        =   49
         Top             =   360
         Width           =   255
      End
      Begin SSDataWidgets_B.SSDBCombo Frame4Combo2 
         Bindings        =   "BusqForm.frx":0040
         Height          =   375
         Left            =   4440
         TabIndex        =   39
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo Frame4Date2 
         Height          =   375
         Left            =   4440
         TabIndex        =   41
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo Frame4Combo1 
         Bindings        =   "BusqForm.frx":0050
         Height          =   375
         Left            =   720
         TabIndex        =   38
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo Frame4Date1 
         Height          =   375
         Left            =   720
         TabIndex        =   40
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox Frame4Text2 
         Height          =   375
         Left            =   4440
         TabIndex        =   31
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Frame4Text1 
         Height          =   375
         Left            =   720
         TabIndex        =   30
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label4 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   26
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Frame3"
      Height          =   735
      Left            =   360
      TabIndex        =   4
      Top             =   2160
      Visible         =   0   'False
      Width           =   6975
      Begin VB.CheckBox Check3 
         Caption         =   "Check1"
         Height          =   255
         Left            =   3360
         TabIndex        =   48
         Top             =   360
         Width           =   255
      End
      Begin SSDataWidgets_B.SSDBCombo Frame3Combo2 
         Bindings        =   "BusqForm.frx":0060
         Height          =   375
         Left            =   4440
         TabIndex        =   25
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo Frame3Date2 
         Height          =   375
         Left            =   4440
         TabIndex        =   24
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox Frame3Text2 
         Height          =   375
         Left            =   4440
         TabIndex        =   23
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin SSDataWidgets_B.SSDBCombo Frame3Combo1 
         Bindings        =   "BusqForm.frx":0070
         Height          =   375
         Left            =   720
         TabIndex        =   22
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefColWidth     =   7056
         Columns(0).Width=   7056
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo Frame3Date1 
         Height          =   375
         Left            =   720
         TabIndex        =   21
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
         _Version        =   65537
         _ExtentX        =   3413
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox Frame3Text1 
         Height          =   375
         Left            =   720
         TabIndex        =   19
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label3 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   20
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   735
      Left            =   360
      TabIndex        =   2
      Top             =   480
      Width           =   6975
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Left            =   3360
         TabIndex        =   46
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Fram1Text2 
         Height          =   375
         Left            =   4440
         TabIndex        =   11
         Top             =   240
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Frame1Text1 
         Height          =   375
         Left            =   720
         TabIndex        =   9
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Intervalo"
         Height          =   255
         Left            =   3120
         TabIndex        =   10
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   5640
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   6360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CancelarBoton 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   1920
      TabIndex        =   1
      Top             =   6600
      Width           =   1095
   End
   Begin VB.CommandButton ListadoBoton 
      Caption         =   "&Listado"
      Height          =   375
      Left            =   4200
      TabIndex        =   0
      Top             =   6600
      Width           =   1095
   End
End
Attribute VB_Name = "BusqForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CancelarBoton_Click()
    Unload Me
    
End Sub

Private Sub Check1_Click()

    If Check1.Value = 1 Then
        Fram1Text2.Visible = True
    Else
        Fram1Text2.Visible = False
    End If
    
End Sub

Private Sub Check2_Click()

    Dim opt As Boolean
    
    If Check2.Value = 1 Then
        opt = True
    Else
        opt = False
    End If
    
    Select Case TypeCampos(1)
     Case 1
        Frame2Text2.Visible = opt
     Case 2
        Frame2Date2.Visible = opt
     Case 3
        Frame2Combo2.Visible = opt
    End Select
    
End Sub

Private Sub Check3_Click()

    Dim opt As Boolean
    
    If Check3.Value = 1 Then
        opt = True
    Else
        opt = False
    End If
    
    Select Case TypeCampos(2)
     Case 1
        Frame3Text2.Visible = opt
     Case 2
        Frame3Date2.Visible = opt
     Case 3
        Frame3Combo2.Visible = opt
    End Select
    
End Sub

Private Sub Check4_Click()

    Dim opt As Boolean
    
    If Check4.Value = 1 Then
        opt = True
    Else
        opt = False
    End If
    
    Select Case TypeCampos(3)
     Case 1
        Frame4Text2.Visible = opt
     Case 2
        Frame4Date2.Visible = opt
     Case 3
        Frame4Combo2.Visible = opt
    End Select
    
End Sub

Private Sub Check5_Click()

    Dim opt As Boolean
    
    If Check5.Value = 1 Then
        opt = True
    Else
        opt = False
    End If
    
    Select Case TypeCampos(4)
     Case 1
        Frame5Text2.Visible = opt
     Case 2
        Frame5Date2.Visible = opt
     Case 3
        Frame5Combo2.Visible = opt
    End Select
    
End Sub


Private Sub Check6_Click()

    If Check6.Value = 1 Then
        Frame6Text2.Visible = True
    Else
        Frame6Text2.Visible = False
    End If
    
End Sub


Private Sub Check7_Click()

    If Check7.Value = 1 Then
        Frame7Text2.Visible = True
    Else
        Frame7Text2.Visible = False
    End If
    
End Sub




Private Sub ListadoBoton_Click()
    
    Dim pross As Integer
    pross = 0
    
    If WHER <> "" Then
        pross = pross + 1
    End If
    
    
    If Frame1Text1.Text <> "" Then
        If pross > 0 Then
                    WHER = WHER & " AND "
        End If
        If Check1.Value = 1 Then
            pross = pross + 1
            WHER = WHER & campos(0) & " BETWEEN " & Frame1Text1.Text & " AND " & Fram1Text2.Text
        Else
            pross = pross + 1
            WHER = campos(0) & "=" & Frame1Text1.Text
        End If
        
    End If
    
    Select Case TypeCampos(1)
        
        Case 1
            If Frame2Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check2.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(1) & " BETWEEN " & Frame2Text1.Text & " AND " & Fram2Text2.Text
                Else
                    pross = pross + 1
                    WHER = WHER & campos(1) & "=" & Frame2Text1.Text
                End If
            End If
        
        Case 2
            If Frame2Date1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check2.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(1) & " BETWEEN #" & Format(CDate(Frame2Date1.Text), "m/d/yyyy") & "# AND #" & Format(CDate(Frame2Date2.Text), "m/d/yyyy") & "#"
                Else
                    pross = pross + 1
                    WHER = WHER & campos(1) & "=#" & Format(CDate(Frame2Date1.Text), "m/d/yyyy") & "#"
                End If
            End If
        
        Case 3
            If Frame2Combo1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check2.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(1) & " BETWEEN " & Frame2Combo1.Text & " AND " & Fram2combo2.Text
                Else
                    pross = pross + 1
                    WHER = WHER & campos(1) & "=" & Frame2Combo1.Text
                End If
            End If
        Case 4
            If Frame2Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                pross = pross + 1
                WHER = WHER & campos(1) & " LIKE '" & Frame2Text1.Text & "*'"

            End If
    
    
    
    
    End Select
    
    Select Case TypeCampos(2)
            
        Case 1
            If Frame3Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check3.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(2) & " BETWEEN " & Frame3Text1.Text & " AND " & Fram3Text2.Text
                Else
                    pross = pross + 1
                    WHER = WHER & campos(2) & "=" & Frame3Text1.Text
                End If
            End If
        
        Case 2
            If Frame3Date1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check3.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(2) & " BETWEEN #" & Format(CDate(Frame3Date1.Text), "m/d/yyyy") & "# AND #" & Format(CDate(Frame3Date2.Text), "m/d/yyyy") & "#"
                Else
                    pross = pross + 1
                    WHER = WHER & campos(2) & "=#" & Format(CDate(Frame3Date1.Text), "m/d/yyyy") & "#"
                End If
            End If
        
        Case 3
            If Frame3Combo1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check3.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(2) & " BETWEEN " & Frame3Combo1.Text & " AND " & Frame3Combo2.Text
                Else
                    pross = pross + 1
                    WHER = WHER & campos(2) & "=" & Frame3Combo1.Text
                End If
            End If
        Case 4
            If Frame3Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                pross = pross + 1
                WHER = WHER & campos(2) & " LIKE '" & Frame3Text1.Text & "*'"
            End If
        

    End Select
    
    Select Case TypeCampos(3)
        
        Case 1
            If Frame4Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check4.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(3) & " BETWEEN " & Frame4Text1.Text & " AND " & Fram4Text2.Text
                Else
                    pross = pross + 1
                    WHER = WHER & campos(3) & "=" & Frame4Text1.Text
                End If
            End If
        
        Case 2
            If Frame4Date1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check4.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(3) & " BETWEEN #" & Format(CDate(Frame4Date1.Text), "m/d/yyyy") & "# AND #" & Format(CDate(Frame4Date2.Text), "m/d/yyyy") & "#"
                Else
                    pross = pross + 1
                    WHER = WHER & campos(3) & "=#" & Format(CDate(Frame4Date1.Text), "m/d/yyyy") & "#"
                End If
            End If
        
        Case 3
        
            If Display(3) <> "SAT" Then
                If Frame4Combo1.Text <> "" Then
                    If pross > 0 Then
                        WHER = WHER & " AND "
                    End If
                    If Check4.Value = 1 Then
                        pross = pross + 1
                        WHER = WHER & campos(3) & " BETWEEN " & Frame4Combo1.Text & " AND " & Frame4Combo2.Text
                    Else
                        pross = pross + 1
                        WHER = WHER & campos(3) & "=" & Frame4Combo1.Text
                    End If
                End If
            Else
                If Frame4Combo1.Text <> "" Then
                    If pross > 0 Then
                        WHER = WHER & " AND "
                    End If
                    pross = pross + 1
                    WHER = WHER & campos(3) & "='" & Frame4Combo1.Text & "'"
                End If
                
            End If
            
        Case 4
            If Frame4Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                pross = pross + 1
                WHER = WHER & campos(3) & " LIKE '" & Frame4Text1.Text & "*'"
            End If

    End Select
    
    Select Case TypeCampos(4)
        
        Case 1
            If Frame5Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check5.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(4) & " BETWEEN " & Frame5Text1.Text & " AND " & Fram5Text2.Text
                Else
                    pross = pross + 1
                    WHER = WHER & campos(4) & "=" & Frame5Text1.Text
                End If
            End If

        Case 2
            If Frame5Date1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check5.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(4) & " BETWEEN #" & Format(CDate(Frame5Date1.Text), "m/d/yyyy") & "# AND #" & Format(CDate(Frame5Date2.Text), "m/d/yyyy") & "#"
                Else
                    pross = pross + 1
                    WHER = WHER & campos(4) & "=#" & Format(CDate(Frame5Date1.Text), "m/d/yyyy") & "#"
                End If
            End If
        
        Case 3
        
            If Frame5Combo1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                
                'If Objeto = OrdenForm.OrdenData Then
                 '   pross = pross + 1
                 '   WHER = WHER & campos(4) & " LIKE '" & Frame5Combo1.Text & "*'"

                'Else
                
                    If Check5.Value = 1 Then
                        pross = pross + 1
                        WHER = WHER & campos(4) & " BETWEEN " & Frame5Combo1.Text & " AND " & Frame5Combo2.Text
                    Else
                        pross = pross + 1
                        WHER = WHER & campos(4) & "=" & Frame5Combo1.Text
                    End If
                'End If
                
            End If


        Case 4
            If Frame5Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                pross = pross + 1
                WHER = WHER & campos(4) & " LIKE '" & Frame5Text1.Text & "*'"
            End If

    End Select
    
    Select Case TypeCampos(5)
        
        Case 1
            If Frame6Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check6.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(5) & " BETWEEN " & Frame6Text1.Text & " AND " & Frame6Text2.Text
                Else
                    pross = pross + 1
                    WHER = WHER & campos(5) & "=" & Frame6Text1.Text
                End If
            End If
    End Select
    
    Select Case TypeCampos(6)
        
        Case 1
            If Frame7Text1.Text <> "" Then
                If pross > 0 Then
                    WHER = WHER & " AND "
                End If
                If Check7.Value = 1 Then
                    pross = pross + 1
                    WHER = WHER & campos(6) & " BETWEEN " & Frame7Text1.Text & " AND " & Frame7Text2.Text
                Else
                    pross = pross + 1
                    WHER = WHER & campos(6) & "=" & Frame7Text1.Text
                End If
            End If
    End Select
    
        
    MosBusForm.Show modal
        
    Unload Me
    
    
End Sub

