VERSION 5.00
Object = "{FAEEE763-117E-101B-8933-08002B2F4F5A}#1.1#0"; "DBLIST32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Begin VB.Form FacturaForm 
   Caption         =   "Facturas"
   ClientHeight    =   7950
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9405
   LinkTopic       =   "Form1"
   ScaleHeight     =   7950
   ScaleWidth      =   9405
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   9405
      _ExtentX        =   16589
      _ExtentY        =   1111
      ButtonWidth     =   1349
      ButtonHeight    =   953
      Appearance      =   1
      _Version        =   327680
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   8
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntrBoton"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Buscar"
            Key             =   "Buscar"
            Description     =   "BuscarBoton"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Articulos"
            Key             =   "AArticulo"
            Description     =   "AddArticulo"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
         EndProperty
      EndProperty
      MouseIcon       =   "Form1.frx":0000
   End
   Begin VB.Frame Frame2 
      Caption         =   "Art�culos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   240
      TabIndex        =   1
      Top             =   4080
      Width           =   8895
      Begin VB.CommandButton BorrarBoton 
         Caption         =   "&Borrar"
         Height          =   375
         Left            =   7800
         TabIndex        =   9
         Top             =   2880
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton ActualizarBoton 
         Caption         =   "&Actualizar"
         Height          =   375
         Left            =   7800
         TabIndex        =   8
         Top             =   3240
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox CABox 
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Top             =   3240
         Width           =   1095
      End
      Begin VB.TextBox CanBox 
         Height          =   285
         Left            =   5400
         TabIndex        =   6
         Top             =   3240
         Width           =   975
      End
      Begin VB.TextBox PrecBox 
         Height          =   285
         Left            =   6480
         TabIndex        =   5
         Top             =   3240
         Width           =   1095
      End
      Begin VB.CommandButton A�adirBoton 
         Caption         =   "A&�adir"
         Height          =   375
         Left            =   7800
         TabIndex        =   2
         Top             =   3240
         Width           =   975
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Bindings        =   "Form1.frx":001C
         Height          =   2055
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   8670
         ScrollBars      =   2
         _Version        =   131076
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         CellNavigation  =   1
         MaxSelectedRows =   1
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   2196
         Columns(0).Caption=   "C�digo art�culo"
         Columns(0).Name =   "CodigoArticulo"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "CodigoArticulo"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   6800
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripcion"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Descripcion"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1455
         Columns(2).Caption=   "Cantidad"
         Columns(2).Name =   "Cantidad"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Cantidad"
         Columns(2).DataType=   3
         Columns(2).FieldLen=   256
         Columns(3).Width=   1720
         Columns(3).Caption=   "PrecioUnit"
         Columns(3).Name =   "PrecioUnit"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "PrecioUnit"
         Columns(3).DataType=   6
         Columns(3).NumberFormat=   "CURRENCY"
         Columns(3).FieldLen=   256
         Columns(4).Width=   979
         Columns(4).Caption=   "Dto"
         Columns(4).Name =   "Descuento"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1323
         Columns(5).Caption=   "PParcial"
         Columns(5).Name =   "PParcial"
         Columns(5).CaptionAlignment=   0
         Columns(5).DataField=   "PParcial"
         Columns(5).DataType=   6
         Columns(5).NumberFormat=   "CURRENCY"
         Columns(5).FieldLen=   256
         _ExtentX        =   15293
         _ExtentY        =   3625
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDBCtls.DBCombo DescBox 
         Bindings        =   "Form1.frx":0033
         Height          =   315
         Left            =   1440
         TabIndex        =   4
         Top             =   3240
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   556
         _Version        =   327680
         ListField       =   "Descripcion"
         Text            =   ""
      End
      Begin VB.Label Label7 
         Caption         =   "C�digo Art�culo"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   3000
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "Descripci�n"
         Height          =   255
         Left            =   1440
         TabIndex        =   14
         Top             =   3000
         Width           =   1335
      End
      Begin VB.Label Label9 
         Caption         =   "Cantidad"
         Height          =   255
         Left            =   5400
         TabIndex        =   13
         Top             =   3000
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Precio Unitario"
         Height          =   255
         Left            =   6480
         TabIndex        =   12
         Top             =   3000
         Width           =   1095
      End
      Begin VB.Label TotalLabel 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label11"
         DataField       =   "Total"
         DataSource      =   "AlbaranData"
         Height          =   255
         Left            =   7920
         TabIndex        =   11
         Top             =   2400
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "TOTAL:"
         Height          =   255
         Left            =   7320
         TabIndex        =   10
         Top             =   2400
         Width           =   615
      End
   End
   Begin VB.Data AAlbaranData 
      Caption         =   "AAlbaran"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   4680
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "AAlbaran"
      Top             =   240
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data ProvData 
      Caption         =   "Proveedores"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   2280
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Proveedores"
      Top             =   240
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos Factura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   720
      TabIndex        =   18
      Top             =   1320
      Width           =   7815
      Begin VB.TextBox NFacturaBox 
         DataField       =   "NFactura"
         DataSource      =   "FacturaData"
         Height          =   285
         Left            =   1800
         TabIndex        =   23
         Top             =   480
         Width           =   975
      End
      Begin VB.TextBox CPBox 
         DataField       =   "CodigoProveedor"
         DataSource      =   "FacturaData"
         Height          =   285
         Left            =   1800
         TabIndex        =   21
         Top             =   1200
         Width           =   975
      End
      Begin VB.TextBox Text1 
         DataField       =   "Comentario"
         DataSource      =   "FacturaData"
         Height          =   735
         Left            =   1800
         TabIndex        =   20
         Top             =   1560
         Width           =   5415
      End
      Begin MSDBCtls.DBCombo ProvCombo 
         Bindings        =   "Form1.frx":0045
         Height          =   315
         Left            =   4680
         TabIndex        =   19
         Top             =   1200
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         _Version        =   327680
         ListField       =   "Nombre"
         BoundColumn     =   "Nombre"
         Text            =   ""
      End
      Begin SSCalendarWidgets_A.SSDateCombo FechaBox 
         DataField       =   "FechaFactura"
         DataSource      =   "FacturaData"
         Height          =   255
         Left            =   1800
         OleObjectBlob   =   "Form1.frx":0058
         TabIndex        =   22
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "N�Factura:"
         Height          =   255
         Left            =   360
         TabIndex        =   28
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha Factura"
         Height          =   255
         Left            =   360
         TabIndex        =   27
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo Proveedor:"
         Height          =   255
         Left            =   360
         TabIndex        =   26
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "Comentario:"
         Height          =   255
         Left            =   360
         TabIndex        =   25
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "Proveedor:"
         Height          =   255
         Left            =   3840
         TabIndex        =   24
         Top             =   1200
         Width           =   975
      End
   End
   Begin VB.TextBox NPABox 
      Height          =   285
      Left            =   4440
      TabIndex        =   17
      Top             =   840
      Width           =   975
   End
   Begin VB.CommandButton AsoBoton 
      Caption         =   "&Asociar"
      Height          =   375
      Left            =   5640
      TabIndex        =   16
      Top             =   840
      Width           =   975
   End
   Begin VB.TextBox IdBox 
      Height          =   285
      Left            =   720
      TabIndex        =   0
      Text            =   "Text2"
      Top             =   6720
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Data FacturaData 
      Caption         =   "Factura"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Factura"
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data FFacturaData 
      Caption         =   "FFactura"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   $"Form1.frx":0189
      Top             =   240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data FFactura2Data 
      Caption         =   "FFactura2"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   2280
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "FFactura"
      Top             =   0
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Data ArticulosData 
      Caption         =   "Articulos"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   4680
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Articulos"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label Label6 
      Caption         =   "N�mero de Albar�n Asociado:"
      Height          =   255
      Left            =   2160
      TabIndex        =   29
      Top             =   840
      Width           =   2175
   End
End
Attribute VB_Name = "FacturaForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
