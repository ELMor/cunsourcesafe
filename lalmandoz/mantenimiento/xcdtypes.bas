Attribute VB_Name = "XCDTYPES"
' Constants used by the Xceed Zip OCX

Public Const MethodStore = 0
Public Const MethodDeflate = 8
Public Const XcdFirstError = 100

' Error codes and Warning constants

Public Const XcdSuccess = 0

Public Const XcdWarningGeneral = 10
Public Const XcdWarningNoZipFile = 30
Public Const XcdWarningFilesSkipped = 40
Public Const XcdWarningEmptyZipFile = 50

Public Const XcdErrorNoZipFile = 100
Public Const XcdErrorZipStruct = 110
Public Const XcdErrorMemory = 120
Public Const XcdErrorBadCall = 130
Public Const XcdErrorNothingToDo = 140
Public Const XcdErrorDiskFull = 150
Public Const XcdErrorEOF = 160
Public Const XcdErrorLibInUse = 180
Public Const XcdErrorUserAbort = 190
Public Const XcdErrorTestFailed = 200
Public Const XcdErrorZeroTested = 210
Public Const XcdErrorDLLNotFound = 240
Public Const XcdErrorInternalLogic = 250
Public Const XcdErrorTempFile = 280
Public Const XcdErrorRead = 290
Public Const XcdErrorWrite = 300
Public Const XcdErrorCantCreateFile = 310
Public Const XcdErrorParentDir = 350
Public Const XcdErrorNameRepeat = 370
Public Const XcdErrorLatest = 380
Public Const XcdErrorDOSError = 400
Public Const XcdErrorMultidisk = 410
Public Const XcdErrorWrongDisk = 420
Public Const XcdErrorMultiDiskBadCall = 430
Public Const XcdErrorCantOpenBinary = 440
Public Const XcdErrorCantOpenSFXConfig = 450
Public Const XcdErrorInvalidEventParam = 460
Public Const XcdErrorCantWriteSfx = 470
Public Const XcdErrorTargetBufferTooSmall = 480
Public Const XcdErrorBinaryVersion = 490
Public Const XcdErrorNotLicensed = 500
Public Const XcdErrorCantCreateDir = 510

' Constants for the Add and Extract methods

Public Const xecAll = 0
Public Const xecFreshen = 1
Public Const xecUpdate = 2

' Constants for the SkippingFile event's 'reason' parameter

Public Const xsrFileNotFound = 0
Public Const xsrBadVersion = 1
Public Const xsrBadCRC = 2
Public Const xsrUpToDate = 3
Public Const xsrUnableToOpen = 4
Public Const xsrBadPassword = 5
Public Const xsrBadData = 6
Public Const xsrOverwrite = 7

' Constants for the Replace event's Cmd parameter

Public Const xrcSkip = 0
Public Const xrcReplace = 1
Public Const xrcRename = 2
Public Const xrcAlwaysSkip = 3
Public Const xrcAlwaysReplace = 4

' Constants for the PwdQuery event's Cmd parameter when Extracting or Testing

Public Const xpcSkip = 0
Public Const xpcAlwaysSkip = 1
Public Const xpcNew = 2

' Constants for the PwdQuery event's Cmd parameter when Adding or Updating

Public Const xpcAddingNone = 3
Public Const xpcAddingNew = 4
Public Const xpcAddingNewAlwaysUse = 5

' Constants for the OverWrite property

Public Const xowAsk = 0
Public Const xowNever = 1
Public Const xowAlways = 2

' Constants for determining the file attributes specified in the
' 'attr' parameter that is provided when the OnAdding, OnExtracting,
' OnListing and OnUpdating events occur.

Public Const xfaReadOnly = 1
Public Const xfaHidden = 2
Public Const xfaSystem = 4
Public Const xfaVolume = 8
Public Const xfaDirectory = 16
Public Const xfaArchiveBit = 32

' Constant for multidisk mode

Public Const LastDisk = 0

' Constants for Self-extractor properties

Public Const xsbOk = 0
Public Const xsbCancel = 1
Public Const xsbAbort = 2
Public Const xsbSkip = 3
Public Const xsbAlwaysSkip = 4
Public Const xsbYes = 5
Public Const xsbNo = 6
Public Const xsbOverwriteAll = 7
Public Const xsbOverwriteNone = 8
Public Const xsbContinue = 9
Public Const xsbExit = 10
Public Const xsbAgree = 11
Public Const xsbRefuse = 12

Public Const xspDirectory = 0
Public Const xspPassword = 1
Public Const xspInsertLastDisk = 2
Public Const xspInsertDisk = 3
Public Const xspAbortExtract = 4
Public Const xspCreateDirectory = 5
Public Const xspOverwrite = 6

Public Const xssExtractingFile = 0
Public Const xssProgress = 1
Public Const xssTitle = 2
Public Const xssCurrentExtractDir = 3
Public Const xssDriveShare = 4
Public Const xssEntireNetwork = 5

Public Const xsmSuccess = 0
Public Const xsmFail = 1
Public Const xsmErrorCreatingDir = 2
Public Const xsmIntro = 3
Public Const xsmLicense = 4

