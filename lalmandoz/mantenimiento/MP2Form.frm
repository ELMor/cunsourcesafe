VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form MP2Form 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento Preventivo"
   ClientHeight    =   6090
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   10935
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6090
   ScaleWidth      =   10935
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBDropDown SSDBDropDown3 
      Bindings        =   "MP2Form.frx":0000
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   5640
      Width           =   2055
      DataFieldList   =   "CodigoOperario"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1244
      Columns(0).Caption=   "C�d.Op."
      Columns(0).Name =   "CodigoOperario"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoOperario"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   4128
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Nombre"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2461
      Columns(2).Caption=   "C�d.Especialidad"
      Columns(2).Name =   "CodigoEspecialidad"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "CodigoEspecialidad"
      Columns(2).DataType=   3
      Columns(2).FieldLen=   256
      _ExtentX        =   3625
      _ExtentY        =   450
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Nombre"
   End
   Begin VB.Data OpData 
      Caption         =   "Operarios"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   8400
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT CodigoOperario,Nombre FROM Operarios WHERE Activo=True ORDER BY Nombre"
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin SSDataWidgets_B.SSDBDropDown SSDBDropDown1 
      Bindings        =   "MP2Form.frx":0011
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   5160
      Width           =   2055
      DataFieldList   =   "CodigoPeriodicidad"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1667
      Columns(0).Caption=   "C�d.Period"
      Columns(0).Name =   "CodigoPeriodicidad"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoPeriodicidad"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3704
      Columns(1).Caption=   "Periodicidad"
      Columns(1).Name =   "Periodicidad"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Periodicidad"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3625
      _ExtentY        =   450
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Periodicidad"
   End
   Begin VB.CommandButton SalirBoton 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   5160
      TabIndex        =   2
      Top             =   5520
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBDropDown SSDBDropDown2 
      Bindings        =   "MP2Form.frx":0025
      Height          =   255
      Left            =   480
      TabIndex        =   1
      Top             =   5400
      Width           =   2055
      DataFieldList   =   "CodigoMaquina"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   2355
      Columns(0).Caption=   "C�digo M�quina"
      Columns(0).Name =   "CodigoMaquina"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoMaquina"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3916
      Columns(1).Caption=   "Modelo"
      Columns(1).Name =   "Modelo"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Modelo"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   4207
      Columns(2).Caption=   "Descripci�n"
      Columns(2).Name =   "Descripcion"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Descripcion"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3836
      Columns(3).Caption=   "Instalaci�n"
      Columns(3).Name =   "Instalacion"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "Instalacion"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   3625
      _ExtentY        =   450
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "CodigoMaquina"
   End
   Begin VB.Data MaquinaData 
      Caption         =   "Maquinas"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   480
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "InventMaquinas"
      Top             =   120
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data PerioData 
      Caption         =   "Period"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   6000
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "Select CodigoPeriodicidad,Periodicidad FROM Periodicidad"
      Top             =   120
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Data MPData 
      Caption         =   "MP"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   3000
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM MantPreventivo ORDER BY CodigoMaquina"
      Top             =   120
      Visible         =   0   'False
      Width           =   1815
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "MP2Form.frx":003B
      Height          =   4695
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   10455
      ScrollBars      =   2
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      RowHeight       =   423
      ExtraHeight     =   2355
      Columns.Count   =   6
      Columns(0).Width=   1958
      Columns(0).Caption=   "C�d.M�quina"
      Columns(0).Name =   "CodigoMaquina"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoMaquina"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   2117
      Columns(1).Caption=   "C�d.Period."
      Columns(1).Name =   "CodigoPeriodicidad"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "CodigoPeriodicidad"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   6668
      Columns(2).Caption=   "Acciones"
      Columns(2).Name =   "Accion"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Accion"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).VertScrollBar=   -1  'True
      Columns(3).Width=   1455
      Columns(3).Caption=   "Dur.Estim."
      Columns(3).Name =   "DuracionEstimada"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "DuracionEstimada"
      Columns(3).DataType=   3
      Columns(3).FieldLen=   256
      Columns(4).Width=   1667
      Columns(4).Caption=   "Pr�x.Orden"
      Columns(4).Name =   "NextOrden"
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "NextOrden"
      Columns(4).DataType=   7
      Columns(4).FieldLen=   256
      Columns(5).Width=   3545
      Columns(5).Caption=   "Cod.Op."
      Columns(5).Name =   "CodigoOperario"
      Columns(5).CaptionAlignment=   0
      Columns(5).DataField=   "CodigoOperario"
      Columns(5).DataType=   3
      Columns(5).FieldLen=   256
      _ExtentX        =   18441
      _ExtentY        =   8281
      _StockProps     =   79
      Caption         =   "DESCRIPCI�N MANTENIMIENTO  PREVENTIVO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MP2Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub SalirBoton_Click()
    Unload Me
    
End Sub

Private Sub SSDBGrid1_InitColumnProps()
    With SSDBGrid1
        .Columns(1).DropDownHwnd = SSDBDropDown1.hWnd
        .Columns(0).DropDownHwnd = SSDBDropDown2.hWnd
        .Columns(5).DropDownHwnd = SSDBDropDown3.hWnd
        .Columns(2).FieldLen = 1000
        
    End With
    
End Sub
