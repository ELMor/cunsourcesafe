VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form MostraLForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Muestra"
   ClientHeight    =   7185
   ClientLeft      =   1140
   ClientTop       =   960
   ClientWidth     =   10125
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   10125
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtOpcion 
      Height          =   495
      Left            =   120
      TabIndex        =   6
      Text            =   "MP"
      Top             =   120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Data Data2 
      Caption         =   "Data2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   8160
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   8160
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data PerioData 
      Caption         =   "Periodicidad"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   600
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "Periodicidad"
      Top             =   240
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Data OrdenData 
      Caption         =   "Ordenes"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   360
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM Orden ORDER BY NOrden"
      Top             =   6600
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton GenerarBoton 
      Caption         =   "&Generar"
      Height          =   495
      Left            =   3720
      TabIndex        =   5
      Top             =   6360
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Cancelar"
      Height          =   495
      Left            =   7920
      TabIndex        =   1
      Top             =   6360
      Width           =   1455
   End
   Begin VB.Data MPData 
      Caption         =   "MantPrev"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   360
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   6240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "MostraLForm.frx":0000
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   1800
      Width           =   9855
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns(0).Width=   3200
      UseDefaults     =   0   'False
      _ExtentX        =   17383
      _ExtentY        =   7646
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin VB.Label Label2 
      Caption         =   "Fecha de Hoy:"
      Height          =   255
      Left            =   3960
      TabIndex        =   4
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Se van a generar Ordenes de trabajo pertenecientes a las siguientes tareas de Mantenimiento Preventivo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   1080
      Width           =   9855
   End
   Begin VB.Label DateLabel 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   5160
      TabIndex        =   2
      Top             =   240
      Width           =   1815
   End
End
Attribute VB_Name = "MostraLForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    Unload Me

End Sub

Private Sub Form_Load()

  Screen.MousePointer = vbNormal
    
End Sub

Private Sub GenerarBoton_Click()

    Dim i As Integer, j As Integer, no As Long
    Dim Fecha As Date, inter As String, num As Integer, Maq As Long, x As Integer
    Dim Wr As String, Cod As Boolean
On Error GoTo msgError
Select Case txtOpcion
  Case "MP"
  
    i = MPData.Recordset.RecordCount
    MPData.Recordset.MoveFirst
    OrdenData.Recordset.MoveLast
    no = OrdenData.Recordset.Fields("NOrden").Value
    
    For j = 1 To i
        
        Fecha = MPData.Recordset.Fields("NextOrden").Value
        inter = MPData.Recordset.Fields("CodigoPeriodicidad").Value
        PerioData.Recordset.FindFirst ("CodigoPeriodicidad=" & inter)
        inter = PerioData.Recordset.Fields("Unidad").Value
        num = PerioData.Recordset.Fields("Valor").Value
        Data1.RecordSource = "SELECT CodigoEspecialidad, Nombre FROM Operarios WHERE CodigoOperario=" & MPData.Recordset.Fields("CodigoOperario").Value
        Data1.Refresh
        
        OrdenData.Recordset.AddNew
        OrdenData.Recordset.Fields("NOrden").Value = no + j
        OrdenData.Recordset.Fields("FechaOrden").Value = Date
        OrdenData.Recordset.Fields("HoraOrden").Value = Time
        OrdenData.Recordset.Fields("TipoOrden").Value = "Mant.Preventivo"
        OrdenData.Recordset.Fields("CodigoMaquina").Value = MPData.Recordset.Fields("CodigoMaquina").Value
        OrdenData.Recordset.Fields("CodigoEspecialidad").Value = Data1.Recordset.Fields("CodigoEspecialidad").Value
        OrdenData.Recordset.Fields("CodigoOperario").Value = MPData.Recordset.Fields("CodigoOperario").Value
        If MPData.Recordset.Fields("CodigoPeriodicidad").Value <> 2 Then
            OrdenData.Recordset.Fields("Definicion").Value = PerioData.Recordset.Fields("Periodicidad").Value & " : " & MPData.Recordset.Fields("Accion").Value
        Else
            OrdenData.Recordset.Fields("Definicion").Value = "*" & PerioData.Recordset.Fields("Periodicidad").Value & " : " & MPData.Recordset.Fields("Accion").Value
        End If
        OrdenData.Recordset.Fields("TiempoEstimadoMP").Value = MPData.Recordset.Fields("DuracionEstimada").Value
        OrdenData.Recordset.Fields("CodigoPeriodicidad").Value = MPData.Recordset.Fields("CodigoPeriodicidad").Value
        OrdenData.Recordset.Update
        
        Maq = MPData.Recordset.Fields("CodigoMaquina").Value
        If MPData.Recordset.Fields("CodigoPeriodicidad").Value <> 2 Then
            With Data1
                .RecordSource = "SELECT CodigoMaquina FROM InventMaquinas WHERE CodigoMaquina LIKE '" & Maq & "*' AND CodigoMaquina<>" & Maq
                .Refresh
                If .Recordset.RecordCount <> 0 Then
                    .Recordset.MoveLast
                    .Recordset.MoveFirst
                    Data2.RecordSource = "SELECT * FROM ListaMP"
                    Data2.Refresh
                    For x = 1 To .Recordset.RecordCount
                        Data2.Recordset.AddNew
                        Data2.Recordset.Fields("NOrden").Value = no + j
                        Data2.Recordset.Fields("CodigoMaquina").Value = .Recordset.Fields("CodigoMaquina").Value
                        Data2.Recordset.Fields("Hecho").Value = True
                        Data2.Recordset.Update
                        .Recordset.MoveNext
                    Next x
                End If
            End With
        Else
            With Data1
                .RecordSource = "SELECT CodigoMaquina FROM InventMaquinas WHERE CodigoMaquina LIKE '" & Maq & "*' AND CodigoMaquina<>" & Maq
                .Refresh
                If .Recordset.RecordCount <> 0 Then
                    .Recordset.MoveLast
                    .Recordset.MoveFirst
                    Data2.RecordSource = "SELECT * FROM ListaMP2"
                    Data2.Refresh
                    For x = 1 To .Recordset.RecordCount
                        Data2.Recordset.AddNew
                        Data2.Recordset.Fields("NOrden").Value = no + j
                        Data2.Recordset.Fields("CodigoMaquina").Value = .Recordset.Fields("CodigoMaquina").Value
                        Data2.Recordset.Fields("HechoS1").Value = True
                        Data2.Recordset.Fields("HechoS2").Value = True
                        Data2.Recordset.Fields("HechoS3").Value = True
                        Data2.Recordset.Fields("HechoS4").Value = True
                        Data2.Recordset.Fields("HechoM").Value = True
                        Data2.Recordset.Update
                        .Recordset.MoveNext
                    Next x
                End If
            End With
            
        End If
        
        MPData.Recordset.Edit
        MPData.Recordset.Fields("NextOrden").Value = DateAdd(inter, num, Fecha)
        MPData.Recordset.Update
        
        MPData.Recordset.MoveNext
            
    Next
        
    OrdenForm.Show modal
    OrdenForm.OrdenData.RecordSource = "SELECT * FROM Orden WHERE NOrden > " & no
    OrdenForm.OrdenData.Refresh
    
    Unload MostraLForm
    
  Case "O"
  
    With MPData
        .Recordset.MoveFirst
        Do While .Recordset.EOF = False
          .Recordset.Edit
          .Recordset.Fields("Finalizada") = True
'          .Recordset.Fields("FechaFinalizacion") = Date
          .Recordset.Fields("CodigoEstado") = 999
          .Recordset.Update
          .Recordset.MoveNext
        Loop
    End With
    
    Unload Me
    
  End Select
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

