VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form PedidoAlForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Pedido Almac�n"
   ClientHeight    =   7920
   ClientLeft      =   45
   ClientTop       =   570
   ClientWidth     =   10050
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7920
   ScaleWidth      =   10050
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   32
      Top             =   0
      Width           =   10050
      _ExtentX        =   17727
      _ExtentY        =   1535
      ButtonWidth     =   1773
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   10
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "prim"
            Description     =   "PrimeBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntrBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Proveedores"
            Key             =   "Prov"
            Description     =   "ProvBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Articulos"
            Key             =   "AArticulo"
            Description     =   "AddArticulo"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "ulti"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin VB.Data PPedido2Data 
         Caption         =   "PPedido2"
         Connect         =   "Access"
         DatabaseName    =   "D:\mantcun.mdb"
         DefaultCursorType=   0  'DefaultCursor
         DefaultType     =   2  'UseODBC
         Exclusive       =   0   'False
         Height          =   375
         Left            =   2040
         Options         =   0
         ReadOnly        =   0   'False
         RecordsetType   =   1  'Dynaset
         RecordSource    =   "PPedidoAl"
         Top             =   840
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.Data ATData 
         Caption         =   "AreaT"
         Connect         =   "Access"
         DatabaseName    =   "D:\mantcun.mdb"
         DefaultCursorType=   0  'DefaultCursor
         DefaultType     =   2  'UseODBC
         Exclusive       =   0   'False
         Height          =   345
         Left            =   3600
         Options         =   0
         ReadOnly        =   0   'False
         RecordsetType   =   2  'Snapshot
         RecordSource    =   "SELECT CodigoMaquina, Descripcion FROM InventMaquinas"
         Top             =   840
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Data DptoData 
         Caption         =   "Departamento"
         Connect         =   "Access"
         DatabaseName    =   "D:\mantcun.mdb"
         DefaultCursorType=   0  'DefaultCursor
         DefaultType     =   2  'UseODBC
         Exclusive       =   0   'False
         Height          =   345
         Left            =   6000
         Options         =   0
         ReadOnly        =   0   'False
         RecordsetType   =   2  'Snapshot
         RecordSource    =   "Lugar"
         Top             =   840
         Visible         =   0   'False
         Width           =   1695
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Art�culos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   600
      TabIndex        =   18
      Top             =   3960
      Width           =   8775
      Begin VB.TextBox SuCodigoBox 
         Height          =   285
         Left            =   4800
         TabIndex        =   24
         Top             =   3240
         Width           =   1215
      End
      Begin VB.CommandButton A�aBoton 
         Caption         =   "&A�adir"
         Height          =   375
         Left            =   7560
         TabIndex        =   23
         Top             =   3240
         Width           =   975
      End
      Begin VB.TextBox IdBox 
         Height          =   285
         Left            =   480
         TabIndex        =   22
         Text            =   "Text1"
         Top             =   2640
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CommandButton ActualizarBoton 
         Caption         =   "&Actualizar"
         Height          =   375
         Left            =   7560
         TabIndex        =   21
         Top             =   3240
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton BorrarBoton 
         Caption         =   "&Borrar"
         Height          =   375
         Left            =   7560
         TabIndex        =   20
         Top             =   2880
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox CantBox 
         Height          =   285
         Left            =   6240
         TabIndex        =   19
         Top             =   3240
         Width           =   1095
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Bindings        =   "PedidoAlForm.frx":0000
         Height          =   2175
         Left            =   120
         TabIndex        =   25
         Top             =   360
         Width           =   8535
         ScrollBars      =   2
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2170
         Columns(0).Caption=   "C�digo Art�culo"
         Columns(0).Name =   "CodigoArticulo"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "CodigoArticulo"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   2011
         Columns(1).Caption=   "Ref.Proveedor"
         Columns(1).Name =   "SuCodigo"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "SuCodigo"
         Columns(1).DataType=   3
         Columns(1).FieldLen=   256
         Columns(2).Width=   6853
         Columns(2).Caption=   "Descripci�n"
         Columns(2).Name =   "Descripcion"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Descripcion"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1376
         Columns(3).Caption=   "Cantidad"
         Columns(3).Name =   "Cantidad"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "Cantidad"
         Columns(3).DataType=   3
         Columns(3).FieldLen=   256
         Columns(4).Width=   1588
         Columns(4).Caption=   "Pr.Unitario"
         Columns(4).Name =   "PrecioUnit"
         Columns(4).CaptionAlignment=   0
         Columns(4).DataField=   "PrecioUnit"
         Columns(4).DataType=   6
         Columns(4).NumberFormat=   "CURRENCY"
         Columns(4).FieldLen=   256
         UseDefaults     =   -1  'True
         _ExtentX        =   15055
         _ExtentY        =   3836
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo DescBox 
         Bindings        =   "PedidoAlForm.frx":0016
         Height          =   255
         Left            =   1560
         TabIndex        =   26
         Top             =   3240
         Width           =   3135
         DataFieldList   =   "Descripcion"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "CodigoArticulo"
         Columns(0).Name =   "CodigoArticulo"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   1
         Columns(0).DataField=   "CodigoArticulo"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   8387
         Columns(1).Caption=   "Descripcion"
         Columns(1).Name =   "Descripcion"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Descripcion"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5530
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Descripcion"
      End
      Begin VB.Label Label7 
         Caption         =   "Ref.Proveedor"
         Height          =   255
         Left            =   4800
         TabIndex        =   31
         Top             =   3000
         Width           =   1095
      End
      Begin VB.Label CALabel 
         Caption         =   "C�digo Art�culo"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   3000
         Width           =   1215
      End
      Begin VB.Label DesLabel 
         Caption         =   "Descripci�n"
         Height          =   255
         Left            =   1560
         TabIndex        =   29
         Top             =   3000
         Width           =   1095
      End
      Begin VB.Label CanLabel 
         Caption         =   "Cantidad"
         Height          =   255
         Left            =   6240
         TabIndex        =   28
         Top             =   3000
         Width           =   855
      End
      Begin VB.Label CABox 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   3240
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos Pedido Almac�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   1080
      TabIndex        =   0
      Top             =   960
      Width           =   7815
      Begin VB.TextBox NPedidoBox 
         BackColor       =   &H80000018&
         DataField       =   "NPedidoA"
         DataSource      =   "PedidoData"
         Enabled         =   0   'False
         Height          =   285
         Left            =   1920
         TabIndex        =   3
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox NOrdenBox 
         DataField       =   "NOrden"
         DataSource      =   "PedidoData"
         Height          =   285
         Left            =   1920
         TabIndex        =   2
         Top             =   720
         Width           =   1335
      End
      Begin VB.TextBox ComentBox 
         DataField       =   "Comentario"
         DataSource      =   "PedidoData"
         Height          =   495
         Left            =   1200
         TabIndex        =   1
         Top             =   2160
         Width           =   6135
      End
      Begin SSDataWidgets_B.SSDBCombo CDptoBox 
         Bindings        =   "PedidoAlForm.frx":0028
         DataField       =   "CodigoDepartamento"
         DataSource      =   "PedidoData"
         Height          =   255
         Left            =   1920
         TabIndex        =   4
         Top             =   1800
         Width           =   1335
         DataFieldList   =   "CodigoLugar"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Columns(0).Width=   3200
         _ExtentX        =   2355
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "CodigoLugar"
      End
      Begin SSDataWidgets_B.SSDBCombo CATBox 
         Bindings        =   "PedidoAlForm.frx":003B
         DataField       =   "CodigoMaquina"
         DataSource      =   "PedidoData"
         Height          =   255
         Left            =   1920
         TabIndex        =   5
         Top             =   1440
         Width           =   1335
         DataFieldList   =   "CodigoMaquina"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Columns(0).Width=   3200
         _ExtentX        =   2355
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "CodigoMaquina"
      End
      Begin SSDataWidgets_B.SSDBCombo CodEspBox 
         Bindings        =   "PedidoAlForm.frx":004C
         DataField       =   "CodigoEspecialidad"
         DataSource      =   "PedidoData"
         Height          =   255
         Left            =   1920
         TabIndex        =   6
         Top             =   1080
         Width           =   1335
         DataFieldList   =   "CodigoEspecialidad"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Columns(0).Width=   3200
         _ExtentX        =   2355
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "CodigoEspecialidad"
      End
      Begin SSCalendarWidgets_A.SSDateCombo FechaBox 
         DataField       =   "FechaPedido"
         DataSource      =   "PedidoData"
         Height          =   255
         Left            =   5520
         TabIndex        =   7
         Top             =   720
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line Line5 
         X1              =   3240
         X2              =   3600
         Y1              =   1920
         Y2              =   1920
      End
      Begin VB.Line Line4 
         X1              =   3240
         X2              =   3600
         Y1              =   1560
         Y2              =   1560
      End
      Begin VB.Line Line3 
         X1              =   3240
         X2              =   3600
         Y1              =   1200
         Y2              =   1200
      End
      Begin VB.Label DptoBox 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Localizacion"
         DataSource      =   "DptoData"
         Height          =   255
         Left            =   3600
         TabIndex        =   17
         Top             =   1800
         Width           =   3495
      End
      Begin VB.Label ATBox 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Descripcion"
         DataSource      =   "ATData"
         Height          =   255
         Left            =   3600
         TabIndex        =   16
         Top             =   1440
         Width           =   3495
      End
      Begin VB.Label EspeBox 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Especialidad"
         DataSource      =   "SeccionData"
         Height          =   255
         Left            =   3600
         TabIndex        =   15
         Top             =   1080
         Width           =   3495
      End
      Begin VB.Label Label9 
         Caption         =   "Departamento:"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label Label8 
         Caption         =   "�rea de Trabajo:"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "C�digo Especialidad:"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "N�Pedido:"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "N�Orden de Trabajo:"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label5 
         Caption         =   "Fecha del Pedido:"
         Height          =   255
         Left            =   4080
         TabIndex        =   9
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "Comentario:"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   2160
         Width           =   975
      End
   End
   Begin VB.Data PedidoData 
      Caption         =   "Pedido"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   1560
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "PedidoAl"
      Top             =   315
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Data PPedidoData 
      Caption         =   "PPedido"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2400
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   $"PedidoAlForm.frx":0062
      Top             =   0
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Data ArtData 
      Caption         =   "Articulo"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   6840
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM Articulos ORDER BY Descripcion"
      Top             =   0
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Data SeccionData 
      Caption         =   "Secciones"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   3840
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT CodigoEspecialidad, Especialidad FROM Tarifas"
      Top             =   0
      Visible         =   0   'False
      Width           =   1455
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   11
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":012E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":0448
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":0762
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":0A7C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":0D96
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":10B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":13CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":16E4
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":4D86
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":50A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedidoAlForm.frx":53BA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   9120
      Y1              =   600
      Y2              =   600
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu ira 
         Caption         =   "Ir a "
         Shortcut        =   {F1}
      End
      Begin VB.Menu first 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu confi 
      Caption         =   "&Configuraciones"
      Begin VB.Menu provee 
         Caption         =   "Proveedores"
      End
      Begin VB.Menu artia 
         Caption         =   "Art�culos"
      End
   End
   Begin VB.Menu list0 
      Caption         =   "&Listados"
      Begin VB.Menu list1 
         Caption         =   "Listado"
      End
   End
End
Attribute VB_Name = "PedidoAlForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function Anterior()
    
    Dim np As Integer
On Error GoTo msgError
    If PedidoData.Recordset.RecordCount = 0 Then
        Exit Function
    End If
    
    With PedidoData
        .Recordset.MovePrevious
        If .Recordset.BOF = True Then
            .Recordset.MoveNext
        End If
        np = .Recordset.Fields("NPedidoA").Value
    End With
           
   PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & np & " ORDER BY PP.Linea"
   PPedidoData.Refresh
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Sub add_Click()
    Dim nada As Integer
    nada = OtroPedidoA(Orden(0))

End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()

End Sub

Private Sub A�aBoton_Click()
   
  Dim id As Integer, np As Long
On Error GoTo msgError
  If CABox.Caption = "" Or CantBox.Text = "" Then
    
    MsgBox ("Necesita rellenar todos los campos")
  
  Else
     
    np = NPedidoBox.Text
    
    With PPedido2Data
        If PPedidoData.Recordset.EOF = False Then
            PPedidoData.Recordset.MoveLast
            id = PPedidoData.Recordset.Fields("Linea").Value
        Else
            id = 0
        End If
        
        id = id + 1
        
        .Recordset.AddNew
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NPedidoA").Value = NPedidoBox.Text
        .Recordset.Fields("CodigoArticulo").Value = CABox.Caption
        If SuCodigoBox.Text = "" Then
            .Recordset.Fields("SuCodigo").Value = 0
        Else
            .Recordset.Fields("SuCodigo").Value = SuCodigoBox.Text
        End If
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .UpdateRecord
        
    End With
    
    
    With ArtData
        .Recordset.FindFirst ("CodigoArticulo=" & CABox.Caption)
        .Recordset.Edit
        If SuCodigoBox.Text <> "" Then
            .Recordset.Fields("SuCodigo").Value = SuCodigoBox.Text
        End If
        .Recordset.Fields("Existencias").Value = (ArtData.Recordset.Fields("Existencias").Value) - CLng(CantBox.Text)
        .Recordset.Update
    End With
    
    PPedidoData.UpdateRecord
    PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & np & " ORDER BY PP.Linea"
    PPedidoData.Refresh
    
    PPedido2Data.Refresh

  End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub artia_Click()
    MantArt.Show modal
    
End Sub

Private Sub CATBox_Change()
On Error GoTo msgError
    If CATBox.Text <> "" Then
        ATData.Recordset.FindFirst ("CodigoMaquina=" & CATBox.Text)
        ATBox.Caption = ATData.Recordset.Fields("Descripcion").Value
    Else
        CATBox.Text = "0"
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CATBox_CloseUp()
On Error GoTo msgError
    ATData.Recordset.FindFirst ("CodigoMaquina=" & CATBox.Text)
    ATBox.Caption = ATData.Recordset.Fields("Descripcion").Value
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub CDptoBox_Change()
On Error GoTo msgError
    If CDptoBox.Text <> "" Then
        DptoData.Recordset.FindFirst ("CodigoLugar=" & CDptoBox.Text)
        DptoBox.Caption = DptoData.Recordset.Fields("Localizacion").Value
    Else
        CDptoBox.Text = "0"
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CDptoBox_CloseUp()
On Error GoTo msgError
    DptoData.Recordset.FindFirst ("CodigoLugar=" & CDptoBox.Text)
    DptoBox.Caption = DptoData.Recordset.Fields("Localizacion").Value
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub CodEspBox_Change()
On Error GoTo msgError
    If CodEspBox.Text <> "" Then
        SeccionData.Recordset.FindFirst ("CodigoEspecialidad=" & CodEspBox.Text)
        EspeBox.Caption = SeccionData.Recordset.Fields("Especialidad").Value
    Else
        CodEspBox.Text = "0"
    
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CodEspBox_CloseUp()
On Error GoTo msgError
    SeccionData.Recordset.FindFirst ("CodigoEspecialidad=" & CodEspBox.Text)
    EspeBox.Caption = SeccionData.Recordset.Fields("Especialidad").Value
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub CPBox_Change()
On Error GoTo msgError
    If CPBox.Text <> "" Then
        ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)
        If ProvData.Recordset.NoMatch = True Then
            ProvBox.Caption = "No Encontrado"
        End If
    
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Function Siguiente()
   
    Dim np As Integer
On Error GoTo msgError
    If PedidoData.Recordset.RecordCount = 0 Then
        Exit Function
    End If
    
    With PedidoData
        .Recordset.MoveNext
        If .Recordset.EOF = True Then
            .Recordset.MovePrevious
        End If
        np = .Recordset.Fields("NPedidoA").Value
    End With
           
   PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & np & " ORDER BY PP.Linea"
   PPedidoData.Refresh
    
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Sub CPBox_CloseUp()
On Error GoTo msgError
    ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)

    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub



Private Sub DescBox_Change()
On Error GoTo msgError
    If DescBox.Text <> "" Then
        ArtData.Recordset.FindFirst ("Descripcion='" & DescBox.Text & "'")
        CABox.Caption = ArtData.Recordset.Fields("CodigoArticulo").Value
    Else
        CABox.Caption = ""
    End If
    
    If ArtData.Recordset.NoMatch = True Then
        CABox.Caption = ""
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub DescBox_CloseUp()
On Error GoTo msgError
    If DescBox.Text <> "" Then
        ArtData.Recordset.FindFirst ("Descripcion='" & DescBox.Text & "'")
        CABox.Caption = ArtData.Recordset.Fields("CodigoArticulo").Value
    Else
        CABox.Caption = ""
    End If
    
    If ArtData.Recordset.NoMatch = True Then
        CABox.Caption = ""
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar la salida?", vbYesNo)
    If nada = vbYes Then
        PedidoData.Recordset.Delete
        nada = Siguiente()
    End If
    
End Sub

Private Sub exit_Click()
    Unload Me
    
End Sub

Private Sub first_Click()
    Dim nada As Integer
    nada = Primero()
    
End Sub

Private Sub ira_Click()
    Set Objeto = PedidoAlForm.PedidoData
    Set Objeto2 = PedidoForm.PPedidoData
    
    Selct = True
    
    Display(0) = "NPedidoA"
    DataCarac = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA="
    DataCarac2 = " ORDER BY PP.Linea"
    
    IraForm.Show modal
    
End Sub

Private Sub last_Click()
    Dim nada As Integer
    nada = Ultimo()

End Sub

Private Sub list1_Click()
    Dim nada As Integer
    nada = ListaPS()
End Sub

Private Sub provee_Click()
    MantProv.Show modal
    
End Sub

Private Sub sigui_Click()
    Dim nada As Integer
    nada = Siguiente()

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Orden(0) = 0
        Unload Me
    Case "Add"
        nada = OtroPedidoA(Orden(0))
    Case "AArticulo"
        MantArt.Show modal
        
    Case "Lista"
        nada = ListaPS()
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar la salida?", vbYesNo)
        If nada = vbYes Then
            PedidoData.Recordset.Delete
            nada = Siguiente()
        End If
    
    Case "Prov"
        MantProv.Show modal
    Case "prim"
        nada = Primero()
    Case "ulti"
        nada = Ultimo()
        
    
    End Select

End Sub

Public Function OtroPedidoA(Orden As Long) As Integer

    Dim num As Integer
On Error GoTo msgError
    num = UltimoPedidoA()
    
    With PedidoData
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveLast
        End If
        .Recordset.AddNew
        .Recordset.Fields("NPedidoA").Value = num
    End With

    NPedidoBox.Text = num
    NOrdenBox.Text = Orden
    PedidoData.Recordset.Fields("FechaPedido").Value = Date
    PedidoData.UpdateRecord
    PedidoData.Recordset.MoveLast
    
    PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & num & " ORDER BY PP.Linea"
    PPedidoData.Refresh

    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function


Private Sub ActualizarBoton_Click()
    
    Dim id As Integer, np As Integer
On Error GoTo msgError
    id = IdBox.Text
    np = NPedidoBox.Text
    
     With PPedido2Data
        .Recordset.FindFirst ("NPedidoA=" & np & " AND Linea=" & id)
        .Recordset.Edit
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NPedidoA").Value = NPedidoBox.Text
        .Recordset.Fields("CodigoArticulo").Value = CABox.Caption
        .Recordset.Fields("SuCodigo").Value = SuCodigoBox.Text
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .UpdateRecord
    
    End With

    With PPedidoData
        .RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & np & " ORDER BY PP.Linea"
        .Refresh
    End With
   
    ArtData.Recordset.FindFirst ("CodigoArticulo=" & CABox.Caption)
    ArtData.Recordset.Edit
    ArtData.Recordset.Fields("Existencias").Value = (ArtData.Recordset.Fields("Existencias").Value) - CLng(CantBox.Text)
    ArtData.Recordset.Update
    
    A�aBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True

    id = ClearFields()
    
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub BorrarBoton_Click()
    Dim id As Integer, np As Long
On Error GoTo msgError
    id = IdBox.Text
    np = NPedidoBox.Text
     
    With PPedido2Data
        .Recordset.FindFirst ("NPedidoA=" & np & " AND Linea=" & id)
        .Recordset.Delete
        .Refresh
    End With
    
    
    With PPedidoData
        .RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & np & " ORDER BY PP.Linea"
        .Refresh
    End With
    
        
    A�aBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub SSDBGrid1_DblClick()
    
    Dim id As Integer
On Error GoTo msgError
    With PPedidoData
        id = .Recordset.Fields("Linea").Value
        IdBox.Text = id
        CABox.Caption = .Recordset.Fields("CodigoArticulo").Value
        CantBox.Text = .Recordset.Fields("Cantidad").Value
        SuCodigoBox.Text = .Recordset.Fields("SuCodigo").Value
 
    End With
    
    With ArtData
        .Recordset.FindFirst ("CodigoArticulo=" & CABox.Caption)
        .Recordset.Edit
        .Recordset.Fields("Existencias").Value = (.Recordset.Fields("Existencias").Value) + CLng(CantBox.Text)
        .Recordset.Update
    End With

    
    A�aBoton.Visible = False
    BorrarBoton.Visible = True
    ActualizarBoton.Visible = True
    SSDBGrid1.Enabled = False
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Function ClearFields()

    CABox.Caption = ""
    CantBox.Text = ""
    SuCodigoBox.Text = ""
    
End Function


Private Function ListaPS()
    Dim nada As Integer
On Error GoTo msgError
    nada = NuevaBusq()
    Display(0) = "NPedidoA"
    Display(1) = "NOrden"
    Display(2) = "FechaPedido"
        
    TypeCampos(0) = 1
    TypeCampos(1) = 1
    TypeCampos(2) = 2
    TypeCampos(3) = 0
    TypeCampos(4) = 0
    TypeCampos(5) = 0
    TypeCampos(6) = 0
    TypeCampos(7) = 0
        
    For nada = 0 To 3
        campos(nada) = Display(nada)
    Next
        
    FRO = "PedidoAl"
    SELE = campos(0) & "," & campos(1) & "," & campos(2)
    WHER = ""
    ORD = ""
        
    Set Objeto = PedidoAlForm.PedidoData
    Set Objeto2 = PedidoAlForm.PPedidoData
    
    DataCarac = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA="
    DataCarac2 = " ORDER BY PP.Linea"
    
    Selct = True
    
    nada = Listado()
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Primero()
    
    Dim nada As Integer
On Error GoTo msgError
    PedidoData.Recordset.MoveFirst
    
    nada = PedidoData.Recordset.Fields("NPedidoA").Value
               
    PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & nada & " ORDER BY PP.Linea"
    PPedidoData.Refresh

    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Ultimo()
    
    Dim nada As Integer
On Error GoTo msgError
    PedidoData.Recordset.MoveLast
    
    nada = PedidoData.Recordset.Fields("NPedidoA").Value
               
    PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & nada & " ORDER BY PP.Linea"
    PPedidoData.Refresh

    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

