VERSION 5.00
Begin VB.Form MenuSATForm 
   Caption         =   "Men� Reparaci�n Exterior"
   ClientHeight    =   4275
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3885
   LinkTopic       =   "Form1"
   ScaleHeight     =   4275
   ScaleWidth      =   3885
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command5 
      Height          =   375
      Left            =   240
      TabIndex        =   7
      Top             =   2760
      Width           =   975
   End
   Begin VB.CommandButton Command4 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   1320
      TabIndex        =   3
      Top             =   3720
      Width           =   975
   End
   Begin VB.CommandButton Command3 
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   2040
      Width           =   975
   End
   Begin VB.CommandButton Command2 
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "SAT Pendientes de P.T."
      Height          =   255
      Left            =   1440
      TabIndex        =   8
      Top             =   2880
      Width           =   1935
   End
   Begin VB.Label Label3 
      Caption         =   "Factura S.A.T."
      Height          =   255
      Left            =   1440
      TabIndex        =   6
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Parte de Trabajo S.A.T."
      Height          =   255
      Left            =   1440
      TabIndex        =   5
      Top             =   1320
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Petici�n Reparaci�n Externa"
      Height          =   255
      Left            =   1440
      TabIndex        =   4
      Top             =   480
      Width           =   2055
   End
End
Attribute VB_Name = "MenuSATForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    PedSATForm.Show modal
    If Orden(0) <> 0 Then
        With PedSATForm
            .PedSATData.RecordSource = "SELECT * FROM PedidoSAT WHERE NOrden=" & Orden(0)
            .PedSATData.Refresh
        End With
    End If
    
    Unload MenuSATForm
    
End Sub

Private Sub Command2_Click()
    
    Dim res As Integer
    
    SATAlbForm.Show modal
    
    If Orden(0) <> 0 Then
        Load DBase
        With DBase
            .PedidoData.RecordSource = "SELECT NPetSAT FROM PedidoSAT WHERE NOrden=" & Orden(0)
            .PedidoData.Refresh
            If .PedidoData.Recordset.RecordCount <> 0 Then
                Orden(2) = .PedidoData.Recordset.Fields("NPetSAT").Value
            End If
        End With
        Unload DBase
                    
    End If
    
    If Orden(2) <> 0 Then
        With SATAlbForm
            .SATAlbData.RecordSource = "SELECT * FROM SATAlbaran WHERE NPetSAT=" & Orden(2)
            .SATAlbData.Refresh
            
            If .SATAlbData.Recordset.RecordCount <> 0 Then
                res = .SATAlbData.Recordset.Fields("NAlbaranSAT").Value
                .SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & res & " ORDER BY Linea"
                .SATAAlb2Data.Refresh
            End If
        End With
    End If
    
    Unload MenuSATForm
        
    
End Sub

Private Sub Command3_Click()
    SATFForm.Show modal
    Unload MenuSATForm
End Sub

Private Sub Command4_Click()
    Unload Me
End Sub

Private Sub Command5_Click()
    
    Dim nada
    
    nada = NuevaBusq()
    SELE = "NPetSAT, FechaEnvio, SAT, CodigoDpto"
    FRO = "PedidoSAT"
    WHER = "NPetSAT<>ALL (SELECT NPetSAT FROM SATAlbaran WHERE NPetSAT<>0) AND NPetSAT<>0"
    MosBusForm.Show modal
    MosBusForm.SSDBGrid1.Enabled = False
    Display(0) = "NPetSAT"
    Display(1) = "FechaEnvio"
    Display(2) = "SAT"
    Display(3) = "CodigoDpto"
    For nada = 0 To 3
        campos(nada) = Display(nada)
    Next
    
    
End Sub
