VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "crystl32.ocx"
Begin VB.Form ImpOpcionForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Impresi�n"
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6975
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   6975
   StartUpPosition =   3  'Windows Default
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   6120
      Top             =   2520
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin SSDataWidgets_B.SSDBCombo CodigoBox 
      Bindings        =   "ImpOpcionForm.frx":0000
      Height          =   255
      Left            =   3960
      TabIndex        =   15
      Top             =   480
      Width           =   2775
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Columns(0).Width=   3200
      _ExtentX        =   4895
      _ExtentY        =   450
      _StockProps     =   93
      Text            =   "0"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin VB.Frame Frame2 
      Caption         =   "Objetivo"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   600
      TabIndex        =   10
      Top             =   0
      Width           =   2415
      Begin VB.OptionButton Option5 
         Caption         =   "Secciones"
         Height          =   255
         Left            =   600
         TabIndex        =   13
         Top             =   960
         Width           =   1335
      End
      Begin VB.OptionButton Option4 
         Caption         =   "Areas de Trabajo"
         Height          =   255
         Left            =   600
         TabIndex        =   12
         Top             =   600
         Width           =   1695
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Departamentos"
         Height          =   255
         Left            =   600
         TabIndex        =   11
         Top             =   240
         Width           =   1575
      End
   End
   Begin SSCalendarWidgets_A.SSDateCombo FechInt2 
      Height          =   255
      Left            =   4680
      TabIndex        =   8
      Top             =   1560
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo FechInt1 
      Height          =   255
      Left            =   2280
      TabIndex        =   6
      Top             =   1560
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancelar"
      Height          =   495
      Left            =   2400
      TabIndex        =   4
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Imprimir"
      Height          =   495
      Left            =   4320
      TabIndex        =   3
      Top             =   3600
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Opci�n"
      Height          =   1335
      Left            =   2280
      TabIndex        =   0
      Top             =   2040
      Width           =   3495
      Begin VB.OptionButton Option2 
         Caption         =   "Impresi�n directa "
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   840
         Width           =   2895
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Imprimir en Documento WORD 97 "
         Height          =   375
         Left            =   360
         TabIndex        =   1
         Top             =   360
         Width           =   2775
      End
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   4920
      TabIndex        =   9
      Text            =   "Text1"
      Top             =   2280
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   4680
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   2760
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo:"
      Enabled         =   0   'False
      Height          =   255
      Left            =   3240
      TabIndex        =   14
      Top             =   480
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "hasta"
      Height          =   255
      Left            =   4080
      TabIndex        =   7
      Top             =   1560
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Desde"
      Height          =   255
      Left            =   1560
      TabIndex        =   5
      Top             =   1560
      Width           =   615
   End
End
Attribute VB_Name = "ImpOpcionForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
    
    Dim nada As Integer
    Dim FechaAr1 As String
    Dim FechaAr2 As String
    
    
    If Text1.Text = "1" Then
        FechaAr1 = SubSt(FechInt1.Text)
        FechaAr2 = SubSt(FechInt2.Text)
        With CrystalReport1
            .ReportFileName = "C:\archivos de programa\almacen\doc\facpend.rpt"
            .Formulas(0) = "Fecha1= Date(" & FechaAr1 & ")"
            .Formulas(1) = "Fecha2= Date(" & FechaAr2 & ")"
            .Action = 1
       End With
'        If Option1.Value = True Then
'            nada = FacPend(FechInt1.Text, FechInt2.Text)
'        Else
'            nada = FacPendD(FechInt1.Text, FechInt2.Text)
'        End If
'        Unload Me
    End If
    
    If Text1.Text = "2" Then
        FechaAr1 = SubSt(FechInt1.Text)
        FechaAr2 = SubSt(FechInt2.Text)
        With CrystalReport1
            .ReportFileName = "C:\archivos de programa\almacen\doc\satpend.rpt"
            .Formulas(0) = "Fecha1= Date(" & FechaAr1 & ")"
            .Formulas(1) = "Fecha2= Date(" & FechaAr2 & ")"
            .Action = 1
       End With
'        If Option1.Value = True Then
'            nada = SATPend(FechInt1.Text, FechInt2.Text)
'        Else
'            nada = SATPendD(FechInt1.Text, FechInt2.Text)
'        End If
'        Unload Me
    End If
    
    If Text1.Text = "3" Then
    If Option1.Value = True Then
        Select Case CodigoBox.DataFieldList
        Case "CodigoMaquina"
            nada = GastosInf("Area de Trabajo", CLng(CodigoBox.Text), FechInt1.Text, FechInt2.Text)
        Case "CodigoLugar"
            nada = GastosInf("Departamento", CLng(CodigoBox.Text), FechInt1.Text, FechInt2.Text)
        Case "CodigoEspecialidad"
            nada = GastosInf("Seccion", CLng(CodigoBox.Text), FechInt1.Text, FechInt2.Text)
        End Select
    Else
        Select Case CodigoBox.DataFieldList
        Case "CodigoMaquina"
            nada = GastosInfD("Area de Trabajo", CLng(CodigoBox.Text), FechInt1.Text, FechInt2.Text)
        Case "CodigoLugar"
            nada = GastosInfD("Departamento", CLng(CodigoBox.Text), FechInt1.Text, FechInt2.Text)
        Case "CodigoEspecialidad"
            nada = GastosInfD("Seccion", CLng(CodigoBox.Text), FechInt1.Text, FechInt2.Text)
        End Select
    End If
        Unload Me
    End If
                
    If Text1.Text = "4" Then
        Select Case CodigoBox.DataFieldList
        Case "CodigoEspecialidad"
            nada = CalidadEsp1(CLng(CodigoBox.Text), FechInt1.Text, FechInt2.Text)
        Case "Nombre"
            nada = CalidadSAT1(CodigoBox.Text, FechInt1.Text, FechInt2.Text)
        End Select
        Unload Me
    End If
       
    If Text1.Text = "ListAlbaranes" Then
        FechaAr1 = SubSt(FechInt1.Text)
        FechaAr2 = SubSt(FechInt2.Text)
        With CrystalReport1
            .ReportFileName = "C:\archivos de programa\almacen\doc\albaranes.rpt"
            .Formulas(0) = "Fecha1= Date(" & FechaAr1 & ")"
            .Formulas(1) = "Fecha2= Date(" & FechaAr2 & ")"
            .Formulas(2) = "Proveedor= " & CodigoBox.Columns(0).Text
            .Action = 1
       End With
    End If
 
    If Text1.Text = "ListFacturas" Then
        FechaAr1 = SubSt(FechInt1.Text)
        FechaAr2 = SubSt(FechInt2.Text)
        With CrystalReport1
            .ReportFileName = "C:\archivos de programa\almacen\doc\facturas.rpt"
            .Formulas(0) = "Fecha1= Date(" & FechaAr1 & ")"
            .Formulas(1) = "Fecha2= Date(" & FechaAr2 & ")"
            .Formulas(2) = "Proveedor= " & CodigoBox.Columns(0).Text
            .Action = 1
       End With
    End If
   
   
End Sub

Private Sub Command2_Click()
    Unload Me
    
End Sub


Private Sub Form_Load()
    Option3.Value = True
    Data1.RecordSource = "SELECT CodigoLugar, Localizacion FROM Lugar"
    Data1.Refresh
    CodigoBox.DataFieldList = "CodigoLugar"
    
End Sub

Private Sub Option3_Click()
    If Text1.Text <> "4" Then
        If Option3.Value = True Then
            Data1.RecordSource = "SELECT CodigoLugar, Localizacion FROM Lugar ORDER BY Localizacion"
            Data1.Refresh
            CodigoBox.DataFieldList = "CodigoLugar"
            CodigoBox.Text = "0"
        End If
    Else
        If Option3.Value = True Then
            Data1.RecordSource = "SELECT Nombre FROM SAT ORDER BY Nombre"
            Data1.Refresh
            CodigoBox.DataFieldList = "Nombre"
            CodigoBox.Text = "No Especificada"
        End If
    End If
    
End Sub

Private Sub Option4_Click()
    If Option4.Value = True Then
        Data1.RecordSource = "SELECT CodigoMaquina, Descripcion FROM InventMaquinas ORDER BY Descripcion"
        Data1.Refresh
        CodigoBox.DataFieldList = "CodigoMaquina"
        CodigoBox.Text = "0"
    End If
    
End Sub

Private Sub Option5_Click()
    
    If Option5.Value = True Then
        Data1.RecordSource = "SELECT CodigoEspecialidad, Especialidad FROM Tarifas ORDER BY Especialidad"
        Data1.Refresh
        CodigoBox.DataFieldList = "CodigoEspecialidad"
        CodigoBox.Text = "0"
    End If
    
End Sub



