VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form ListaMPForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lista"
   ClientHeight    =   7260
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5970
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   5970
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid2 
      Bindings        =   "ListaMPForm.frx":0000
      Height          =   6255
      Left            =   4680
      TabIndex        =   1
      Top             =   240
      Width           =   855
      ScrollBars      =   0
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      SelectTypeRow   =   1
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      UseDefaults     =   0   'False
      _ExtentX        =   1508
      _ExtentY        =   11033
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2400
      TabIndex        =   0
      Top             =   6720
      Width           =   1215
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "ListaMPForm.frx":0014
      Height          =   6255
      Left            =   360
      TabIndex        =   2
      Top             =   240
      Width           =   4215
      ScrollBars      =   0
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2275
      Columns(0).Caption=   "�rea de Trabajo"
      Columns(0).Name =   "CodigoMaquina"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoMaquina"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   5106
      Columns(1).Caption=   "Descripcion"
      Columns(1).Name =   "Descripcion"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Descripcion"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   7435
      _ExtentY        =   11033
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Data HechoData 
      Caption         =   "HechoMP"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   840
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT Hecho FROM ListaMP"
      Top             =   3840
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Data ListaData 
      Caption         =   "ListaMP"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   1200
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT M.CodigoMaquina, M.Descripcion FROM InventMaquinas M, ListaMP L WHERE L.CodigoMAquina=M.CodigoMaquina"
      Top             =   3120
      Visible         =   0   'False
      Width           =   2175
   End
End
Attribute VB_Name = "ListaMPForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    Dim i As Long
    Dim PerC2(4) As Double
    Dim PerC As Double
On Error GoTo msgError
    With HechoData.Recordset
    
    If ListaMPForm.Width = 6060 Then
        For i = 1 To .RecordCount
            If .Fields("Hecho").Value = True Then
                PerC = PerC + 1
            End If
        Next i
        OrdenForm.OrdenData.Recordset.Edit
        OrdenForm.OrdenData.Recordset.Fields("PCaje").Value = (PerC * 100) / .RecordCount
        OrdenForm.OrdenData.Recordset.Update
    
    Else
        For i = 1 To .RecordCount
            If .Fields("HechoS1").Value = True Then
                PerC = PerC + 1
            End If
            If .Fields("HechoS2").Value = True Then
                PerC = PerC + 1
            End If
            If .Fields("HechoS3").Value = True Then
                PerC = PerC + 1
            End If
            If .Fields("HechoS4").Value = True Then
                PerC = PerC + 1
            End If
            If .Fields("HechoM").Value = True Then
                PerC = PerC + 1
            End If
        Next i
        
        OrdenForm.OrdenData.Recordset.Edit
        OrdenForm.OrdenData.Recordset.Fields("PCaje").Value = (PerC * 100) / (.RecordCount * 5)
        OrdenForm.OrdenData.Recordset.Update
    End If
        
    End With
    
    Unload Me
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub
