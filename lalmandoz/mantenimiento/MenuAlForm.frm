VERSION 5.00
Begin VB.Form MenuAlForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Menu Almac�n"
   ClientHeight    =   7605
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   10620
   Icon            =   "MenuAlForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7605
   ScaleWidth      =   10620
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refrescar"
      Height          =   495
      Left            =   7800
      TabIndex        =   18
      Top             =   6960
      Width           =   1215
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   4440
      Top             =   5520
   End
   Begin VB.Data Data 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   1920
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   5520
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Frame Frame1 
      Height          =   3975
      Left            =   6000
      TabIndex        =   1
      Top             =   2880
      Width           =   4095
      Begin VB.CommandButton Command1 
         Caption         =   "Mantenimiento Prev. pendiente:"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1560
         Width           =   2415
      End
      Begin VB.CommandButton Command11 
         Caption         =   "Equipos enviados a reparar:"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   3360
         Width           =   2415
      End
      Begin VB.CommandButton Command9 
         Caption         =   "S.A.T. pendientes de P.T.:"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   2760
         Width           =   2415
      End
      Begin VB.CommandButton Command8 
         Caption         =   "Pedidos sin albar�n:"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   2160
         Width           =   2415
      End
      Begin VB.CommandButton Command7 
         Caption         =   "�rdenes pendientes finalizaci�n:"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   960
         Width           =   2415
      End
      Begin VB.CommandButton Command6 
         Caption         =   "�rdenes pendientes de Listar:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   2415
      End
      Begin VB.Label MPPFBox 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   2760
         TabIndex        =   15
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label FueraBox 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   2760
         TabIndex        =   13
         Top             =   3360
         Width           =   1095
      End
      Begin VB.Label SATBox 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   2760
         TabIndex        =   5
         Top             =   2760
         Width           =   1095
      End
      Begin VB.Label PSABox 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   2760
         TabIndex        =   4
         Top             =   2160
         Width           =   1095
      End
      Begin VB.Label OTPFBox 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   2760
         TabIndex        =   3
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label OTPABox 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   2760
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.CommandButton SalirBoton 
      Caption         =   "&Salir"
      Height          =   975
      Left            =   2160
      TabIndex        =   0
      Top             =   6000
      Width           =   1695
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      Height          =   5010
      Left            =   360
      Picture         =   "MenuAlForm.frx":1BEA
      ScaleHeight     =   4950
      ScaleWidth      =   5040
      TabIndex        =   16
      Top             =   120
      Width           =   5100
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "SERVICIO DE MANTENIMIENTO C.U.N."
      BeginProperty Font 
         Name            =   "Impact"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   5640
      TabIndex        =   17
      Top             =   720
      Width           =   4695
   End
   Begin VB.Label DateBox 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5640
      TabIndex        =   10
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label TimeBox 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label7"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   11
      Top             =   240
      Width           =   1815
   End
   Begin VB.Menu Pr 
      Caption         =   "&Programa"
      Begin VB.Menu ZIP 
         Caption         =   "Copia de Seguridad"
      End
      Begin VB.Menu acerca 
         Caption         =   "Acerca de"
      End
      Begin VB.Menu Exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu Ordenes 
      Caption         =   "&Ordenes"
      Index           =   1
      Begin VB.Menu OT 
         Caption         =   "�rdenes de Trabajo"
         Shortcut        =   ^O
      End
      Begin VB.Menu RecOT 
         Caption         =   "Recibir �rdenes"
      End
      Begin VB.Menu ImpOT 
         Caption         =   "Imprimir Nuevas Ordenes"
      End
      Begin VB.Menu Asig 
         Caption         =   "Asignar Nuevas Ordenes"
      End
      Begin VB.Menu stts 
         Caption         =   "Estad�sticas"
      End
   End
   Begin VB.Menu Almacen 
      Caption         =   "&Almac�n"
      Index           =   2
      Begin VB.Menu Sal 
         Caption         =   "Salidas Almac�n"
      End
   End
   Begin VB.Menu Configuracion 
      Caption         =   "&Configuraci�n"
      Index           =   3
      Begin VB.Menu sate 
         Caption         =   "S.A.T."
      End
      Begin VB.Menu Prov 
         Caption         =   "Proveedores"
      End
      Begin VB.Menu Art 
         Caption         =   "Art�culos"
      End
      Begin VB.Menu AT 
         Caption         =   "Areas de Trabajo"
      End
      Begin VB.Menu MantPrev 
         Caption         =   "Mantenimento Preventivo"
      End
      Begin VB.Menu Secc 
         Caption         =   "Secciones"
      End
      Begin VB.Menu Period 
         Caption         =   "Periodicidades"
      End
      Begin VB.Menu departo 
         Caption         =   "Departamentos"
      End
      Begin VB.Menu Op 
         Caption         =   "Operadores"
      End
      Begin VB.Menu arbol 
         Caption         =   "�rbol de AT"
      End
   End
   Begin VB.Menu Pedidos 
      Caption         =   "&Pedidos"
      Index           =   4
      Begin VB.Menu Pedi 
         Caption         =   "Pedidos"
      End
      Begin VB.Menu Alb 
         Caption         =   "Albaranes"
      End
      Begin VB.Menu fact 
         Caption         =   "Facturas"
      End
   End
   Begin VB.Menu sat 
      Caption         =   "&S.A.T."
      Index           =   5
      Begin VB.Menu PetRep 
         Caption         =   "Petici�n Reparaci�n"
      End
      Begin VB.Menu PT 
         Caption         =   "Partes de Trabajo"
      End
      Begin VB.Menu FactSAT 
         Caption         =   "Facturas S.A.T."
      End
      Begin VB.Menu PTPend 
         Caption         =   "Partes de Trabajo Pendientes"
      End
   End
   Begin VB.Menu Inform 
      Caption         =   "&Informes"
      Begin VB.Menu OrdInf 
         Caption         =   "Informes de �rdenes"
      End
      Begin VB.Menu factpe 
         Caption         =   "Facturas pendientes"
      End
      Begin VB.Menu facsatpe 
         Caption         =   "Facturas SAT pendientes"
      End
      Begin VB.Menu infogas 
         Caption         =   "Informes por Unidades"
      End
      Begin VB.Menu CalSecSAT 
         Caption         =   "Calidad Secci�n / SAT"
      End
      Begin VB.Menu ListAlbaranes 
         Caption         =   "Listado de Albaranes"
      End
      Begin VB.Menu ListFacturas 
         Caption         =   "Listado de Facturas"
      End
   End
End
Attribute VB_Name = "MenuAlForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub acerca_Click()
    AboutForm.Show modal
    
End Sub

Private Sub Alb_Click()
    
    AlbaranForm.Show modal
    
End Sub


Private Sub arbol_Click()

    frmArbol.Show vbModal

End Sub

Private Sub Art_Click()
    MantArt.Show modal
    
End Sub

Private Sub Asig_Click()
    With OrdenForm
        .Show modal
        .OrdenData.RecordSource = "SELECT * FROM Orden WHERE Listada=False AND NOrden<>0 ORDER BY NOrden"
        .OrdenData.Refresh
    End With
    
End Sub

Private Sub at_Click()
    MantInvForm.Show modal
End Sub


Private Sub CalSecSAT_Click()
    Load ImpOpcionForm
    With ImpOpcionForm
        .Text1.Text = "4"
        .Option3.Caption = "SAT"
        .Option4.Visible = False
        .Option5.Value = True
        .Frame2.Enabled = True
        .Label3.Enabled = True
        .CodigoBox.Enabled = True
        .Option1.Enabled = False
        .Show modal
        
    End With
    
End Sub

Private Sub cmdRefresh_Click()

    Call ActualizarB

End Sub

Private Sub Command1_Click()
    
    Dim nada As Integer
    
    nada = NuevaBusq()
    campos(0) = "NOrden"
    campos(1) = "Definicion"
    campos(2) = "FechaOrden"
    campos(3) = "CodigoEspecialidad"
    
    For nada = 0 To 3
        Display(nada) = campos(nada)
    Next

    SELE = "NOrden, Definicion, FechaOrden, CodigoEspecialidad"
    FRO = "Orden"
    WHER = "Finalizada=False AND TipoOrden='Mant.Preventivo'"
    ORD = "CodigoEspecialidad"
    
    MosBusForm.Show modal
    MosBusForm.OpcionBox.Text = "0"

End Sub




Private Sub Command11_Click()
    Dim nada As Integer
    
    nada = NuevaBusq()
    campos(0) = "NPetSAT"
    campos(1) = "FechaEnvio"
    campos(2) = "Descripcion"
    campos(3) = "SAT"
    
    For nada = 0 To 3
        Display(nada) = campos(nada)
    Next
    SELE = "NPetSAT, FechaEnvio, Descripcion, SAT"
    FRO = "PedidoSAT"
    WHER = "NPetSAT<>ALL(SELECT NPetSAT FROM SATAlbaran WHERE NPetSAT<>0) AND NPetSAT<>0 AND Salida=True"
    ORD = "NPetSAT"
    
    MosBusForm.Show modal
    MosBusForm.OpcionBox.Text = "0"

End Sub




Private Sub Command6_Click()
    
    Dim nada As Integer
    
    nada = NuevaBusq()
    campos(0) = "NOrden"
    campos(1) = "Definicion"
    campos(2) = "FechaOrden"
    campos(3) = "CodigoEspecialidad"
    campos(4) = "CodigoLugar"
    
    For nada = 0 To 4
        Display(nada) = campos(nada)
    Next
    
    SELE = "NOrden, Definicion, FechaOrden, CodigoEspecialidad, CodigoLugar"
    FRO = "Orden"
    WHER = "Listada=False"
    ORD = "FechaOrden"
    
    MosBusForm.Show modal
    MosBusForm.OpcionBox.Text = "0"
    
End Sub

Private Sub Command7_Click()
    
    Call NuevaBusq
    
    campos(0) = "NOrden"
    campos(1) = "Definicion"
    campos(2) = "FechaOrden"
    campos(3) = "CodigoEspecialidad"
    
    For nada = 0 To 3
        Display(nada) = campos(nada)
    Next
    
    SELE = "O.NOrden, O.Definicion, O.FechaOrden, O.CodigoEspecialidad, L.Localizacion"
    FRO = "Orden O, Lugar L"
    WHER = "Finalizada=False AND TipoOrden<>'Mant.Preventivo' AND O.CodigoLugar=L.CodigoLugar"
    ORD = "FechaOrden"
    
    MosBusForm.Show modal
    MosBusForm.OpcionBox.Text = "0"

End Sub

Private Sub Command8_Click()
    
    Dim nada As Integer
    
    nada = NuevaBusq()
    campos(0) = "NPedido"
    campos(1) = "FechaPedido"
    campos(2) = "Descripcion"
    campos(3) = "Nombre"
    
    For nada = 0 To 3
        Display(nada) = campos(nada)
    Next

    SELE = "PP.NPedido, P.FechaPedido, A.Descripcion, PR.Nombre"
    FRO = "PPedido PP, Pedido P, Articulos A, Proveedores PR"
    WHER = "PP.Cantidad>PP.Recibido AND PP.NPedido=P.NPedido AND PP.CodigoArticulo=A.CodigoArticulo AND P.CodigoProveedor=PR.CodigoProveedor AND PP.Linea=1"
    
    MosBusForm.Show modal
    MosBusForm.OpcionBox.Text = "0"
    
End Sub

Private Sub Command9_Click()

    Dim nada As Integer
    
    nada = NuevaBusq()
    campos(0) = "NPetSAT"
    campos(1) = "FechaEnvio"
    campos(2) = "Descripcion"
    campos(3) = "SAT"
    
    For nada = 0 To 3
        Display(nada) = campos(nada)
    Next

    SELE = "NPetSAT, FechaEnvio, Descripcion, SAT"
    FRO = "PedidoSAT"
    WHER = "NPetSAT<>ALL(SELECT NPetSAT FROM SATAlbaran WHERE NPetSAT<>0) AND NPetSAT<>0"
    ORD = "NPetSAT"
    
    MosBusForm.Show modal
    MosBusForm.OpcionBox.Text = "0"
    
End Sub

Private Sub departo_Click()
    MantDptoForm.Show modal
End Sub

Private Sub exit_Click()
    End
    
End Sub

Private Sub facsatpe_Click()
    ImpOpcionForm.Show modal
    ImpOpcionForm.Text1.Text = "2"

End Sub

Private Sub Fact_Click()
    FacturaForm.Show modal
    
End Sub

Private Sub factpe_Click()
    ImpOpcionForm.Show modal
    ImpOpcionForm.Text1.Text = "1"
    
End Sub

Private Sub FactSAT_Click()
    SATFForm.Show modal
    
End Sub

Private Sub Form_Activate()
'    Dim nada As Integer
'    nada = ActualizarB()

End Sub

Private Sub Form_Load()
       
  Call frmCWIntrod.Show
  
  Screen.MousePointer = vbHourglass
  DoEvents
  frmCWIntrod.ProgressBar1.Min = 0
  frmCWIntrod.ProgressBar1.Max = 10
  frmCWIntrod.ProgressBar1.Value = 1
  Call ManPrevOrdenes
  Screen.MousePointer = vbHourglass
  frmCWIntrod.ProgressBar1.Value = 2
  Call ActualizarB
  frmCWIntrod.ProgressBar1.Value = 9
  Call TipoImpresora
  frmCWIntrod.ProgressBar1.Value = 9.5
  Call OldOrdenes
  frmCWIntrod.ProgressBar1.Value = 10
  DateBox.Caption = CStr(Date)

  Screen.MousePointer = vbNormal
  
  Unload frmCWIntrod


End Sub

Private Sub ImpOT_Click()
    Dim nada As Integer
    
    Load OrdenForm
    With OrdenForm
        .OrdenData.RecordSource = "SELECT * FROM Orden WHERE Listada=False AND NOrden<>0 ORDER BY CodigoEspecialidad"
        .OrdenData.Refresh
        If .OrdenData.Recordset.RecordCount <> 0 Then
            .OrdenData.Recordset.MoveFirst
            While .OrdenData.Recordset.EOF = False
                nada = .ImpOrden()
                .OrdenData.Recordset.MoveNext
            Wend
        End If
    End With
    Unload OrdenForm
    
    nada = ActualizarB()
    
End Sub

Private Sub infogas_Click()
    ImpOpcionForm.Show modal
    ImpOpcionForm.Text1.Text = "3"
    ImpOpcionForm.Frame2.Enabled = True
    ImpOpcionForm.Label3.Enabled = True
    ImpOpcionForm.CodigoBox.Enabled = True
    
End Sub

Private Sub ListAlbaranes_Click()

    With ImpOpcionForm
        Load ImpOpcionForm
        .Frame1.Enabled = False
        .Frame2.Enabled = False
        .Data1.RecordSource = "SELECT CodigoProveedor,Nombre FROM Proveedores"
        .Data1.Refresh
        .CodigoBox.Enabled = True
        .Label3.Enabled = True
        .CodigoBox.DataFieldList = "CodigoProveedor"
        .CodigoBox.DataFieldToDisplay = "Nombre"
        .Text1.Text = "ListAlbaranes"
        .Show (vbModal)
    End With
    

End Sub

Private Sub ListFacturas_Click()
    With ImpOpcionForm
        Load ImpOpcionForm
        .Frame1.Enabled = False
        .Frame2.Enabled = False
        .Data1.RecordSource = "SELECT CodigoProveedor,Nombre FROM Proveedores"
        .Data1.Refresh
        .CodigoBox.Enabled = True
        .Label3.Enabled = True
        .CodigoBox.DataFieldList = "CodigoProveedor"
        .CodigoBox.DataFieldToDisplay = "Nombre"
        .Text1.Text = "ListFacturas"
        .Show (vbModal)
    End With

End Sub

Private Sub MantPrev_Click()
    MP2Form.Show modal
    
End Sub

Private Sub Op_Click()
    MantOperariosForm.Show modal
    
End Sub


Private Sub OrdInf_Click()

    InformeOrdenForm.Show vbModal

End Sub

Private Sub OT_Click()
    OrdenForm.Show modal

End Sub

Private Sub Pedi_Click()
    
    Dim result
    
    Orden(0) = 0
    
    PedidoForm.Show modal
    
End Sub

Private Sub Period_Click()
    MantPerio.Show modal
    
End Sub

Private Sub PetRep_Click()
    
    PedSATForm.Show modal

End Sub

Private Sub Prov_Click()
    MantProv.Show modal
End Sub

Private Sub PT_Click()
    
    SATAlbForm.Show modal

End Sub

Private Sub PTPend_Click()
    Dim nada
    
    nada = NuevaBusq()
    SELE = "NPetSAT, FechaEnvio, SAT, CodigoDpto"
    FRO = "PedidoSAT"
    WHER = "NPetSAT<>ALL (SELECT NPetSAT FROM SATAlbaran WHERE NPetSAT<>0) AND NPetSAT<>0"
    MosBusForm.Show modal
    Display(0) = "NPetSAT"
    Display(1) = "FechaEnvio"
    Display(2) = "SAT"
    Display(3) = "CodigoDpto"
    For nada = 0 To 3
        campos(nada) = Display(nada)
    Next
End Sub

Private Sub RecOT_Click()
    ReciOrdenForm.Show modal

End Sub

Private Sub Sal_Click()
    Orden(0) = 0
    PedidoAlForm.Show modal

End Sub

Private Sub SalirBoton_Click()
    End

End Sub

Private Sub sate_Click()
    SATForm.Show modal
End Sub

Private Sub Secc_Click()
    ManTarifasForm.Show modal
    
End Sub

Public Function ActualizarB()
Dim i As Integer
Dim j As Integer

Dim t As Integer
    Dim Fecha1 As Date
    
    Dim wrkODBC As Workspace
    Dim dbsNeptuno As Database
    Dim conEditores As Connection
    Dim rstTemp As Recordset
    Dim rstTemp2 As Recordset
    Dim strSql As String
On Error GoTo msgError
    ' Abre Microsoft Jet y el espacio de trabajo ODBCDirect, la base de datos
    ' Microsoft Jet y  la conexi�n ODBCDirect.
    Set wrkODBC = CreateWorkspace("", "admin", "", dbUseODBC)

    Set conEditores = wrkODBC.OpenConnection("", , , _
        "ODBC;DATABASE=;UID=;PWD=;DSN=mantenim")
        
        
    Fecha1 = DateAdd("ww", -2, Date)
    
    
    strSql = "SELECT COUNT(NOrden) FROM Orden WHERE Listada=False"
Set rstTemp = conEditores.OpenRecordset(strSql, dbOpenSnapshot, dbRunAsync)
    Do While rstTemp.StillExecuting
'        MsgBox "    [todav�a en ejecuci�n.1.]"
    Loop
'    dbsNeptuno.Close
'    conEditores.Close
'    wrkJet.Close
'    wrkODBC.Close

'    Data.RecordSource = "SELECT COUNT(NOrden) FROM Orden WHERE Listada=False"
'    Data.Refresh
    OTPABox.Caption = CStr(rstTemp.Fields(0).Value)
    rstTemp.Close
  frmCWIntrod.ProgressBar1.Value = 3
  
    strSql = "SELECT COUNT(NOrden) FROM Orden WHERE Finalizada=False AND TipoOrden<>'Mant.Preventivo'"
    Set rstTemp = conEditores.OpenRecordset(strSql, dbOpenSnapshot, dbRunAsync)
    Do While rstTemp.StillExecuting
'        MsgBox "    [todav�a en ejecuci�n.2.]"
    Loop
    OTPFBox.Caption = CStr(rstTemp.Fields(0).Value)
    rstTemp.Close
    
  frmCWIntrod.ProgressBar1.Value = 3.2
    strSql = "SELECT COUNT(NOrden) FROM Orden WHERE Finalizada=False AND TipoOrden<>'Mant.Preventivo' AND FechaOrden<=#" & Format(CDate(Fecha1), "m/d/yyyy") & "#"
    Set rstTemp = conEditores.OpenRecordset(strSql, dbOpenSnapshot, dbRunAsync)
    Do While rstTemp.StillExecuting
'        MsgBox "    [todav�a en ejecuci�n.3.]"
    Loop
    
    If rstTemp.Fields(0).Value <> 0 Then
        OTPFBox.BackColor = &H8080FF
    Else
        OTPFBox.BackColor = &H8000000F
    End If
    rstTemp.Close
    
  frmCWIntrod.ProgressBar1.Value = 3.4
    strSql = "SELECT COUNT(NOrden) FROM Orden WHERE Finalizada=False AND TipoOrden='Mant.Preventivo'"
    Set rstTemp = conEditores.OpenRecordset(strSql, dbOpenSnapshot, dbRunAsync)
    Do While rstTemp.StillExecuting
'        MsgBox "    [todav�a en ejecuci�n.4.]"
    Loop
    MPPFBox.Caption = CStr(rstTemp.Fields(0).Value)
    rstTemp.Close
    
  frmCWIntrod.ProgressBar1.Value = 3.6
    strSql = "SELECT COUNT(P.NPedido) FROM PPedido PP, Pedido P WHERE PP.Cantidad>PP.Recibido AND P.NPedido=PP.NPedido AND PP.Linea=1"
    Set rstTemp = conEditores.OpenRecordset(strSql, dbOpenSnapshot, dbRunAsync)
    Do While rstTemp.StillExecuting
'        MsgBox "    [todav�a en ejecuci�n.5.]"
    Loop
    PSABox.Caption = CStr(rstTemp.Fields(0).Value)
    rstTemp.Close
    
  frmCWIntrod.ProgressBar1.Value = 3.8
    strSql = "SELECT COUNT(P.NPedido) FROM PPedido PP, Pedido P WHERE PP.Cantidad>PP.Recibido AND P.NPedido=PP.NPedido AND PP.Linea=1 AND P.FechaPedido<=#" & Format(CDate(Fecha1), "m/d/yyyy") & "#"
    Set rstTemp = conEditores.OpenRecordset(strSql, dbOpenSnapshot, dbRunAsync)
    Do While rstTemp.StillExecuting
'        MsgBox "    [todav�a en ejecuci�n.6.]"
    Loop
    If rstTemp.Fields(0).Value <> 0 Then
        PSABox.BackColor = &H8080FF
    Else
        PSABox.BackColor = &H8000000F
    End If
    rstTemp.Close

  frmCWIntrod.ProgressBar1.Value = 4
    strSql = "SELECT COUNT(NPetSAT) FROM PedidoSAT WHERE NPetSAT<>ALL (SELECT NPetSAT FROM SATAlbaran WHERE NPetSAT<>0) AND NPetSAT<>0 "
    Set rstTemp = conEditores.OpenRecordset(strSql, dbOpenSnapshot, dbRunAsync)
    Do While rstTemp.StillExecuting
    frmCWIntrod.ProgressBar1.Value = frmCWIntrod.ProgressBar1.Value + 2
    Loop
    SATBox.Caption = CStr(rstTemp.Fields(0).Value)
    rstTemp.Close
    
  frmCWIntrod.ProgressBar1.Value = 5
    strSql = "SELECT COUNT(NPetSAT) FROM PedidoSAT WHERE NPetSAT<>ALL (SELECT NPetSAT FROM SATAlbaran WHERE NPetSAT<>0) AND NPetSAT<>0 AND Salida=True "
    Set rstTemp = conEditores.OpenRecordset(strSql, dbOpenSnapshot, dbRunAsync)
    Do While rstTemp.StillExecuting
    frmCWIntrod.ProgressBar1.Value = frmCWIntrod.ProgressBar1.Value + 1
    Loop
    FueraBox.Caption = CStr(rstTemp.Fields(0).Value)
    rstTemp.Close
    
'For i = 5 To 9
'  frmCWIntrod.ProgressBar1.Value = i
'    For j = 0 To 30
'    Next j
'Next i
    conEditores.Close
    wrkODBC.Close
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Sub stts_Click()
    EstadOrdForm.Show modal
    
End Sub

Private Sub Timer1_Timer()
    TimeBox.Caption = CStr(Time)

End Sub

Private Sub TipoImpresora()

    Dim Cac As String
    
    Open "C:\ARCHIVOS DE PROGRAMA\ALMACEN\Config.man" For Input As #1
    Cac = Input(LOF(1), #1)
    
    IMPRESORA = Mid$(Cac, InStr(1, Cac, "IMPRESORA=") + Len("IMPRESORA="), 1)
    MAXMES = Mid$(Cac, InStr(1, Cac, "MAXMES=") + Len("MAXMES="), 1)
    
    Close #1
    
End Sub

Private Sub OldOrdenes()

  Dim strSql As String
  Dim FechaFin As Date
On Error GoTo msgError
  FechaFin = DateAdd("m", -3, Date)
  
  strSql = "SELECT * FROM ORDEN WHERE Finalizada=False AND FechaOrden <= #" & Format(FechaFin, "m/d/yyyy") & "# ORDER BY FechaOrden"
  
  Load MostraLForm
  With MostraLForm
    
    .MPData.RecordSource = strSql
    .MPData.Refresh
    If .MPData.Recordset.RecordCount = 0 Then
      Unload MostraLForm
    Else
      .DateLabel.Caption = CStr(Date)
      .Label1.Caption = "Se va a proceder a finalizar las siguientes �rdenes caducadas..."
      .GenerarBoton.Caption = "Finalizar"
      .SSDBGrid1.Enabled = True
      .SSDBGrid1.AllowUpdate = False
      .txtOpcion = "O"
      MostraLForm.Show vbModal
    End If
  End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub ZIP_Click()

    'Copia de seguridad de la base de datos
    
    CopiaSegForm.Show vbModal
    

End Sub
