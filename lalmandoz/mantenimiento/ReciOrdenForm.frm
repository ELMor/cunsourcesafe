VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form ReciOrdenForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Nuevas O.T."
   ClientHeight    =   2850
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9210
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2850
   ScaleWidth      =   9210
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Data ContData 
      Caption         =   "Contador"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   360
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT Cte FROM Constantes WHERE IdConstante=1"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data ArribaData 
      Caption         =   "DBase Servidor"
      Connect         =   "dBASE IV;"
      DatabaseName    =   "C:\Proyecto\Vales"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   420
      Left            =   4800
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   0  'Table
      RecordSource    =   "Almtra01"
      Top             =   0
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   8160
      Top             =   2280
   End
   Begin ComCtl2.Animation Animation1 
      Height          =   1575
      Left            =   360
      TabIndex        =   4
      Top             =   240
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   2778
      _Version        =   327681
      AutoPlay        =   -1  'True
      Center          =   -1  'True
      FullWidth       =   265
      FullHeight      =   105
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3960
      TabIndex        =   2
      Top             =   2280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Data AbajoData 
      Caption         =   "DBase Local"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   4800
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Orden"
      Top             =   360
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label FinalBox 
      Caption         =   "Finalizado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7200
      TabIndex        =   3
      Top             =   1560
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label ContaBox 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   5880
      TabIndex        =   1
      Top             =   1560
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Recogiendo Nuevas Órdenes de Trabajo..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4680
      TabIndex        =   0
      Top             =   1080
      Width           =   4335
   End
End
Attribute VB_Name = "ReciOrdenForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    Animation1.Open "c:\archivos de programa\almacen\Filecopy.avi"
    
End Sub

Private Sub Timer1_Timer()

    Dim Nor As Long, Contador As Integer
    Dim Count As Long, prueb As Boolean
On Error GoTo msgError
    AbajoData.Recordset.MoveLast
    Nor = 1 + AbajoData.Recordset.Fields("NOrden").Value
'    Count = CLng(ContData.Recordset.Fields("Cte").Value) + 1
       
    While ArribaData.Recordset.EOF = False
        AbajoData.Recordset.AddNew
        AbajoData.Recordset.Fields("NOrden").Value = Nor
        AbajoData.Recordset.Fields("FechaOrden").Value = ArribaData.Recordset.Fields("PET_FECHA").Value
        'AbajoData.Recordset.Fields("HoraOrden").Value = ArribaData.Recordset.Fields("HoraOrden").Value
        AbajoData.Recordset.Fields("HoraOrden").Value = Time
        AbajoData.Recordset.Fields("CodigoLugar").Value = ArribaData.Recordset.Fields("PET_DEPART").Value
        AbajoData.Recordset.Fields("Lugar").Value = ArribaData.Recordset.Fields("PET_LUGARO").Value
        AbajoData.Recordset.Fields("Planta").Value = ArribaData.Recordset.Fields("PET_PLANTA").Value
        AbajoData.Recordset.Fields("Peticionario").Value = ArribaData.Recordset.Fields("PET_PETICI").Value
        'AbajoData.Recordset.Fields("ExTTelf").Value = ArribaData.Recordset.Fields("ExTTelf").Value
        AbajoData.Recordset.Fields("ExTTelf").Value = 0
        AbajoData.Recordset.Fields("Definicion").Value = "(" & ArribaData.Recordset.Fields("PET_NUMERO").Value & "): " & ArribaData.Recordset.Fields("PET_DESPE1").Value & " " & ArribaData.Recordset.Fields("PET_DESPE2").Value & " " & ArribaData.Recordset.Fields("PET_DESPE3").Value
        If ArribaData.Recordset.Fields("PET_TIPO").Value = "RE" Then
            AbajoData.Recordset.Fields("TipoOrden").Value = "Reparación"
        Else
            AbajoData.Recordset.Fields("TipoOrden").Value = "Instalación"
        End If
        If ArribaData.Recordset.Fields("PET_ESPECI").Value <> 99 Then
            AbajoData.Recordset.Fields("CodigoEspecialidad").Value = ArribaData.Recordset.Fields("PET_ESPECI").Value
        Else
            AbajoData.Recordset.Fields("CodigoEspecialidad").Value = 0
        End If
        
        'ArribaData.Recordset.Fields("NOrdenA").Value = Nor
        'ArribaData.Recordset.Fields("Estado").Value = 1
        'ArribaData.Recordset.Fields("Entregada").Value = True
        AbajoData.Recordset.Update
        Nor = Nor + 1
                
        Contador = Contador + 1
'        Count = Count + 1
        ContaBox.Caption = Contador
        ArribaData.Recordset.Delete
        ArribaData.Recordset.MoveNext
                
    Wend
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
    
    On Error GoTo AlmError

'    While OraData.Recordset.EOF = False
'        AbajoData.Recordset.AddNew
'        AbajoData.Recordset.Fields("NOrden").Value = Nor
'        AbajoData.Recordset.Fields("FechaOrden").Value = OraData.Recordset.Fields("FECHAORDEN").Value
'        AbajoData.Recordset.Fields("HoraOrden").Value = OraData.Recordset.Fields("HORAORDEN").Value
'        AbajoData.Recordset.Fields("CodigoLugar").Value = OraData.Recordset.Fields("CODIGODPTO").Value
'        AbajoData.Recordset.Fields("Lugar").Value = OraData.Recordset.Fields("LUGAR").Value
'        AbajoData.Recordset.Fields("Planta").Value = OraData.Recordset.Fields("PLANTA").Value
'        AbajoData.Recordset.Fields("Peticionario").Value = OraData.Recordset.Fields("PETICIONARIO").Value
'        AbajoData.Recordset.Fields("ExTTelf").Value = OraData.Recordset.Fields("EXTTELF").Value
'        AbajoData.Recordset.Fields("Definicion").Value = OraData.Recordset.Fields("DEFINICION").Value
'        AbajoData.Recordset.Fields("TipoOrden").Value = OraData.Recordset.Fields("TIPOORDEN").Value
'        AbajoData.Recordset.Fields("CodigoEspecialidad").Value = OraData.Recordset.Fields("CODIGOESPECIALIDAD").Value
'        AbajoData.Recordset.Update
'
'        OraData.Recordset.Edit
'        OraData.Recordset.Fields("NOrdenA").Value = Nor
'        OraData.Recordset.Fields("CodigoEstado").Value = 1
'        OraData.Recordset.Fields("Entregada").Value = 1
'        OraData.Recordset.Update
'        OraData.Recordset.MoveNext
'        Nor = Nor + 1
'
'        Contador = Contador + 1
'        ContaBox.Caption = Contador
'        ContaBox.Refresh
'
'    Wend
    
FinalSub:
    FinalBox.Visible = True
    Command1.Visible = True
    ContData.Recordset.Edit
    ContData.Recordset.Fields("Cte").Value = CStr(Count - 1)
    ContData.Recordset.Update
    
    Contador = MenuAlForm.ActualizarB()
    Timer1.Enabled = False
    
    Exit Sub
    
AlmError:
    MsgBox ("Se ha producido un error al conectarse a Desa.CUN")
    GoTo FinalSub
    
End Sub
