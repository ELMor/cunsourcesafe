Attribute VB_Name = "Module1"
Public IMPRESORA As Integer
Public MAXMES As Integer
Public slct As Long
Public Selct As Boolean
Public Selct2 As Integer
Public Flag As Boolean
Public Orden(3) As Long
Public campos(7) As String
Public Display(7) As String
Public TypeCampos(7) As Integer
Public Lists(4, 2) As String
Public SELE As String
Public FRO As String
Public WHER As String
Public ORD As String
Public Objeto As Data
Public Objeto2 As Variant
Public DataCarac As String
Public DataCarac2 As String





Public Function UltimoPedido() As Integer
    Dim num As Integer
        
    Load DBase
    With DBase
        .PedidoData.RecordSource = "SELECT NPedido FROM Pedido ORDER BY NPedido"
        .PedidoData.Refresh
        If .PedidoData.Recordset.EOF = False Then
            .PedidoData.Recordset.MoveLast
            num = .PedidoData.Recordset.Fields("NPedido").Value
        Else
            num = 0
        End If
        
    End With
    
    num = num + 1
    
    UltimoPedido = num
        
    Unload DBase
    
End Function


Public Function Listado() As Integer
    
    With BusqForm
        .Show modal
        .Frame1.Caption = Display(0)
        
        If TypeCampos(1) <> 0 Then
            
            .Frame2.Caption = Display(1)
            .Frame2.Visible = True
            
            Select Case TypeCampos(1)
            Case 1
                .Frame2Text1.Visible = True
            Case 2
                .Frame2Date1.Visible = True
            Case 3
                .Data1.RecordSource = "SELECT " & Lists(1, 0) & " FROM " & Lists(1, 2)
                .Data1.Refresh
                .Frame2Combo1.DataFieldList = Lists(1, 1)
                .Frame2Combo1.DataFieldToDisplay = Lists(1, 1)
                .Frame2Combo2.DataFieldList = Lists(1, 1)
                .Frame2Combo2.DataFieldToDisplay = Lists(1, 1)
                .Frame2Combo1.Visible = True
            Case 4
                .Frame2Text1.Visible = True
                
            End Select
        End If
        
        If TypeCampos(2) <> 0 Then
            
            .Frame3.Caption = Display(2)
            .Frame3.Visible = True
            
            Select Case TypeCampos(2)
            Case 1
                .Frame3Text1.Visible = True
            Case 2
                .Frame3Date1.Visible = True
            Case 3
                .Data2.RecordSource = "SELECT " & Lists(2, 0) & " FROM " & Lists(2, 2)
                .Data2.Refresh
                .Frame3Combo1.DataFieldList = Lists(2, 1)
                .Frame3Combo1.DataFieldToDisplay = Lists(2, 1)
                .Frame3Combo2.DataFieldList = Lists(2, 1)
                .Frame3Combo2.DataFieldToDisplay = Lists(2, 1)
                .Frame3Combo1.Visible = True
            Case 4
                .Frame3Text1.Visible = True

            End Select
        End If
        
        If TypeCampos(3) <> 0 Then
            
            .Frame4.Caption = Display(3)
            .Frame4.Visible = True
            
            Select Case TypeCampos(3)
            Case 1
                .Frame4Text1.Visible = True
            Case 2
                .Frame4Date1.Visible = True
            Case 3
                .Data3.RecordSource = "SELECT " & Lists(3, 0) & " FROM " & Lists(3, 2)
                .Data3.Refresh
                .Frame4Combo1.DataFieldList = Lists(3, 1)
                .Frame4Combo1.DataFieldToDisplay = Lists(3, 1)
                .Frame4Combo1.DataField = Lists(3, 1)
                .Frame4Combo2.DataFieldList = Lists(3, 1)
                .Frame4Combo2.DataFieldToDisplay = Lists(3, 1)
                .Frame4Combo1.Visible = True
            Case 4
                .Frame4Text1.Visible = True

            End Select
        End If
        
        If TypeCampos(4) <> 0 Then
            
            .Frame5.Caption = Display(4)
            .Frame5.Visible = True
            
            Select Case TypeCampos(4)
            Case 1
                .Frame5Text1.Visible = True
            Case 2
                .Frame5Date1.Visible = True
            Case 3
                .Data4.RecordSource = "SELECT " & Lists(4, 0) & " FROM " & Lists(4, 2)
                .Data4.Refresh
                .Frame5Combo1.DataFieldList = Lists(4, 1)
                .Frame5Combo1.DataFieldToDisplay = Lists(4, 1)
                .Frame5Combo2.DataFieldList = Lists(4, 1)
                .Frame5Combo2.DataFieldToDisplay = Lists(4, 1)
                .Frame5Combo1.Visible = True
            Case 4
                .Frame5Text1.Visible = True
                

            End Select

        End If
        
        If TypeCampos(5) <> 0 Then
            
            .Frame6.Caption = Display(5)
            .Frame6.Visible = True
            .Frame6Text1.Visible = True
        End If
        
        If TypeCampos(6) <> 0 Then
            
            .Frame7.Caption = Display(6)
            .Frame7.Visible = True
            .Frame2Text1.Visible = True
        End If
        
    
    
    End With
        
End Function


Public Function CaTotalAlb(na As Long) As Long

    Dim Total As Long
    
    Load DBase
    
    With DBase.PedidoData
        .RecordSource = "SELECT Cantidad, PrecioUnit, Descuento FROM AAlbaran WHERE NAlbaran=" & na
        .Refresh
        
        If .Recordset.RecordCount = 0 Then
        
            CaTotalAlb = 0
            Exit Function
        
        Else
            .Recordset.MoveFirst
        
            While .Recordset.EOF = False
                If (.Recordset.Fields("PrecioUnit").Value) <> 0 And (.Recordset.Fields("Cantidad").Value) <> 0 Then
                    Total = Total + ((.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value) * (1 - ((.Recordset.Fields("Descuento").Value) / 100)))
                End If
                .Recordset.MoveNext
            Wend
        
        End If
    
    End With
    
    CaTotalAlb = Total

    Unload DBase
    
End Function

Public Function CaTotalFac(nf As Long) As Long

    Dim Total As Long
    
    Load DBase
    
    With DBase.PedidoData
        .RecordSource = "SELECT Cantidad, PrecioUnit, Descuento FROM FFactura WHERE NFactura=" & nf
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveFirst
        
            While .Recordset.EOF = False
                If (.Recordset.Fields("PrecioUnit").Value) <> 0 And (.Recordset.Fields("Cantidad").Value) <> 0 Then
                    Total = Total + ((.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value) * (1 - (Val(.Recordset.Fields("Descuento").Value) / 100)))
                End If
                .Recordset.MoveNext
            Wend
        Else
            Total = 0
        End If
        
    End With
    
    CaTotalFac = Total

    Unload DBase
    
End Function

Public Function CaTotalSATFac(nf As Long) As Long

    Dim Total As Long
    
        Load DBase
        
        With DBase.PedidoData
            .RecordSource = "SELECT Cantidad, PrecioUnit, Descuento FROM SATFFactura WHERE NFacturaSAT=" & nf
            .Refresh
            If .Recordset.EOF = False Then
               
               .Recordset.MoveFirst
            
                While .Recordset.EOF = False
                    If (.Recordset.Fields("PrecioUnit").Value) <> 0 And (.Recordset.Fields("Cantidad").Value) <> 0 Then
                        Total = Total + ((.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value) * (1 - (Val(.Recordset.Fields("Descuento").Value) / 100)))
                    End If
                    .Recordset.MoveNext
                Wend
            End If
        End With
        
    CaTotalSATFac = Total
    
    Unload DBase
    
End Function

Public Function UltFactura() As Long

    Dim last As Long, SAT As Long, fa As Long
    
    Load DBase
    
    With DBase.PedidoData
        .RecordSource = "SELECT NFactura FROM Factura ORDER BY NFactura"
        .Refresh
        .Recordset.MoveLast
        fa = .Recordset.Fields("NFactura").Value
    End With
    
    With DBase.Data2
        .RecordSource = "SELECT NFacturaSAT FROM SATFact ORDER BY NFacturaSAT"
        .Refresh
        If .Recordset.EOF = True Then
            SAT = 0
        Else
            .Recordset.MoveLast
            SAT = .Recordset.Fields("NFacturaSAT").Value
        End If
        
    End With
    
    Select Case fa > SAT
        Case True
            UltFactura = fa
        Case False
            UltFactura = SAT
    End Select
    
End Function


Public Function CaTotalSATAlb(nf As Long) As Long

    Dim Total As Long
    
        Load DBase
        
        With DBase.PedidoData
            .RecordSource = "SELECT Cantidad, PrecioUnit, Descuento FROM SATAAlbaran WHERE NAlbaranSAT=" & nf
            .Refresh
            If .Recordset.EOF = False Then
               
               .Recordset.MoveFirst
            
                While .Recordset.EOF = False
                    If (.Recordset.Fields("PrecioUnit").Value) <> 0 And (.Recordset.Fields("Cantidad").Value) <> 0 Then
                        Total = Total + ((.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value) * (1 - (Val(.Recordset.Fields("Descuento").Value) / 100)))
                    End If
                    .Recordset.MoveNext
                Wend
            End If
        End With
        
    CaTotalSATAlb = Total
    
    Unload DBase
    
End Function

Public Function UltAlbaran() As Long

    Dim last As Long, SAT As Long, fa As Long
    
    Load DBase
    
    With DBase.PedidoData
        .RecordSource = "SELECT NAlbaran FROM Albaran ORDER BY NAlbaran"
        .Refresh
        .Recordset.MoveLast
        fa = .Recordset.Fields("NAlbaran").Value
    End With
    
    With DBase.Data2
        .RecordSource = "SELECT NAlbaranSAT FROM SATAlbaran ORDER BY NAlbaranSAT"
        .Refresh
        If .Recordset.EOF = True Then
            SAT = 0
        Else
            .Recordset.MoveLast
            SAT = .Recordset.Fields("NAlbaranSAT").Value
        End If
        
    End With
    
    Select Case fa > SAT
        Case True
            UltAlbaran = fa
        Case False
            UltAlbaran = SAT
    End Select
    
End Function

Public Function UltimoPedidoA() As Integer
    Dim num As Integer
        
    Load DBase
    With DBase
        .PedidoData.RecordSource = "SELECT NPedidoA FROM PedidoAl ORDER BY NPedidoA"
        .PedidoData.Refresh
        If .PedidoData.Recordset.EOF = False Then
            .PedidoData.Recordset.MoveLast
            num = .PedidoData.Recordset.Fields("NPedidoA").Value
        Else
            num = 0
        End If
        
    End With
    
    num = num + 1
    
    UltimoPedidoA = num
        
    Unload DBase
    
End Function

