VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form AlbaranForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Albar�n"
   ClientHeight    =   8310
   ClientLeft      =   2265
   ClientTop       =   1185
   ClientWidth     =   10350
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   10350
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   10350
      _ExtentX        =   18256
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      AllowCustomize  =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   9
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "first"
            Description     =   "primboton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntrBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Articulos"
            Key             =   "AArticulo"
            Description     =   "AddArticulo"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "last"
            Description     =   "UltimoBoton"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid2 
      Bindings        =   "AlbaranForm.frx":0000
      Height          =   1215
      Left            =   4680
      TabIndex        =   31
      Top             =   960
      Width           =   5415
      ScrollBars      =   2
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      SelectTypeRow   =   1
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1429
      Columns(0).Caption=   "NPedido"
      Columns(0).Name =   "NPedido"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "NPedido"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   1191
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "FechaPedido"
      Columns(1).Alignment=   1
      Columns(1).CaptionAlignment=   1
      Columns(1).DataField=   "FechaPedido"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   5927
      Columns(2).Caption=   "Nombre"
      Columns(2).Name =   "Nombre"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Nombre"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      UseDefaults     =   -1  'True
      _ExtentX        =   9551
      _ExtentY        =   2143
      _StockProps     =   79
      Caption         =   "Pedidos Pendientes de Albar�n"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Data PedidoData 
      Caption         =   "Pedidos"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   6840
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   $"AlbaranForm.frx":0015
      Top             =   360
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Frame Frame2 
      Caption         =   "Art�culos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   11
      Top             =   4440
      Width           =   10095
      Begin SSDataWidgets_B.SSDBCombo DescBox 
         Bindings        =   "AlbaranForm.frx":0142
         Height          =   255
         Left            =   1800
         TabIndex        =   34
         Top             =   2880
         Width           =   3615
         DataFieldList   =   "Descripcion"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2064
         Columns(0).Caption=   "CodigoArticulo"
         Columns(0).Name =   "CodigoArticulo"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   1
         Columns(0).DataField=   "CodigoArticulo"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   6826
         Columns(1).Caption=   "Descripcion"
         Columns(1).Name =   "Descripcion"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Descripcion"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "SuCodigo"
         Columns(2).Name =   "SuCodigo"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "SuCodigo"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6376
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Descripcion"
      End
      Begin VB.TextBox DescuentoBox 
         Height          =   285
         Left            =   7800
         TabIndex        =   25
         Text            =   "0"
         Top             =   2880
         Width           =   495
      End
      Begin VB.CommandButton A�adirBoton 
         Caption         =   "A&�adir"
         Height          =   375
         Left            =   8400
         TabIndex        =   16
         Top             =   2880
         Width           =   975
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Bindings        =   "AlbaranForm.frx":0154
         Height          =   1815
         Left            =   120
         TabIndex        =   19
         Top             =   360
         Width           =   9855
         ScrollBars      =   2
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnShrinking=   0   'False
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         CellNavigation  =   1
         MaxSelectedRows =   1
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   2196
         Columns(0).Caption=   "C�digo Art�culo"
         Columns(0).Name =   "CodigoArticulo"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "CodigoArticulo"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   7329
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripcion"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Descripcion"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1349
         Columns(2).Caption=   "Cantidad"
         Columns(2).Name =   "Cantidad"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Cantidad"
         Columns(2).DataType=   3
         Columns(2).FieldLen=   256
         Columns(3).Width=   1614
         Columns(3).Caption=   "Pr.Unitario"
         Columns(3).Name =   "PrecioUnit"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "PrecioUnit"
         Columns(3).DataType=   6
         Columns(3).FieldLen=   256
         Columns(4).Width=   1058
         Columns(4).Caption=   "Dto"
         Columns(4).Name =   "Descuento"
         Columns(4).CaptionAlignment=   0
         Columns(4).DataField=   "Descuento"
         Columns(4).DataType=   3
         Columns(4).FieldLen=   256
         Columns(5).Width=   1455
         Columns(5).Caption=   "Parcial"
         Columns(5).Name =   "PParcial"
         Columns(5).CaptionAlignment=   0
         Columns(5).DataField=   "PParcial"
         Columns(5).DataType=   6
         Columns(5).FieldLen=   256
         Columns(6).Width=   1296
         Columns(6).Caption=   "NPedido"
         Columns(6).Name =   "NPedido"
         Columns(6).Alignment=   1
         Columns(6).CaptionAlignment=   1
         Columns(6).DataField=   "NPedido"
         Columns(6).DataType=   3
         Columns(6).FieldLen=   256
         UseDefaults     =   0   'False
         _ExtentX        =   17383
         _ExtentY        =   3201
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox PrecBox 
         Height          =   285
         Left            =   6600
         TabIndex        =   17
         Text            =   "0"
         Top             =   2880
         Width           =   1095
      End
      Begin VB.TextBox CanBox 
         Height          =   285
         Left            =   5520
         TabIndex        =   14
         Text            =   "0"
         Top             =   2880
         Width           =   975
      End
      Begin VB.CommandButton ActualizarBoton 
         Caption         =   "&Actualizar"
         Height          =   375
         Left            =   8400
         TabIndex        =   23
         Top             =   3120
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton BorrarBoton 
         Caption         =   "&Borrar"
         Height          =   375
         Left            =   8400
         TabIndex        =   22
         Top             =   2640
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label CABox 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   480
         TabIndex        =   32
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label11 
         Caption         =   "Dto"
         Height          =   255
         Left            =   7800
         TabIndex        =   26
         Top             =   2640
         Width           =   495
      End
      Begin VB.Label Label12 
         Caption         =   "Total:"
         Height          =   255
         Left            =   7560
         TabIndex        =   21
         Top             =   2160
         Width           =   615
      End
      Begin VB.Label TotalLabel 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label11"
         DataField       =   "Total"
         DataSource      =   "AlbaranData"
         Height          =   255
         Left            =   8160
         TabIndex        =   20
         Top             =   2160
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "Precio Unitario"
         Height          =   255
         Left            =   6600
         TabIndex        =   18
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label Label9 
         Caption         =   "Cantidad"
         Height          =   255
         Left            =   5520
         TabIndex        =   15
         Top             =   2640
         Width           =   975
      End
      Begin VB.Label Label8 
         Caption         =   "Descripci�n"
         Height          =   255
         Left            =   1800
         TabIndex        =   13
         Top             =   2640
         Width           =   1335
      End
      Begin VB.Label Label7 
         Caption         =   "C�digo Art�culo"
         Height          =   255
         Left            =   480
         TabIndex        =   12
         Top             =   2640
         Width           =   1095
      End
   End
   Begin VB.Data PPedidoData 
      Caption         =   "PPedido"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "PPedido"
      Top             =   120
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Data AAlbaran 
      Caption         =   "AAlbar�n"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2160
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT AL.*,AR.Descripcion  FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=1"
      Top             =   360
      Visible         =   0   'False
      Width           =   1860
   End
   Begin VB.CommandButton AsoBoton 
      Caption         =   "&Asociar"
      Height          =   375
      Left            =   3480
      TabIndex        =   10
      Top             =   1200
      Width           =   975
   End
   Begin VB.TextBox NPABox 
      Height          =   285
      Left            =   2280
      TabIndex        =   9
      Top             =   1200
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos Albar�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   1200
      TabIndex        =   0
      Top             =   2160
      Width           =   7815
      Begin SSDataWidgets_B.SSDBCombo CPBox 
         Bindings        =   "AlbaranForm.frx":0167
         DataField       =   "CodigoProveedor"
         DataSource      =   "AlbaranData"
         Height          =   255
         Left            =   1800
         TabIndex        =   30
         Top             =   1080
         Width           =   1455
         DataFieldList   =   "CodigoProveedor"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2699
         Columns(0).Caption=   "C�digo Proveedor"
         Columns(0).Name =   "CodigoProveedor"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "CodigoProveedor"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   5318
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "Nombre"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Nombre"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2566
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "CodigoProveedor"
      End
      Begin VB.TextBox NAlbaranPBox 
         DataField       =   "NAlbaranP"
         DataSource      =   "AlbaranData"
         Height          =   285
         Left            =   5520
         TabIndex        =   28
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Text1 
         DataField       =   "Comentario"
         DataSource      =   "AlbaranData"
         Height          =   735
         Left            =   1800
         TabIndex        =   7
         Top             =   1440
         Width           =   5415
      End
      Begin SSCalendarWidgets_A.SSDateCombo FechaBox 
         DataField       =   "FechaAlbaran"
         DataSource      =   "AlbaranData"
         Height          =   255
         Left            =   1800
         TabIndex        =   4
         Top             =   720
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox NAlbaranBox 
         BackColor       =   &H80000018&
         DataField       =   "NAlbaran"
         DataSource      =   "AlbaranData"
         Enabled         =   0   'False
         Height          =   285
         Left            =   1800
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label ProvBox 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Nombre"
         DataSource      =   "ProvData"
         Height          =   255
         Left            =   3600
         TabIndex        =   29
         Top             =   1080
         Width           =   3615
      End
      Begin VB.Line Line1 
         X1              =   3240
         X2              =   3600
         Y1              =   1200
         Y2              =   1200
      End
      Begin VB.Label Label13 
         Caption         =   "N�Albar�n Proveedor:"
         Height          =   255
         Left            =   3840
         TabIndex        =   27
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "Comentario:"
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo Proveedor:"
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha Albar�n"
         Height          =   255
         Left            =   360
         TabIndex        =   3
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "N�Albar�n:"
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Data ArtData 
      Caption         =   "Art�culos"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   4680
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM Articulos ORDER BY Descripcion"
      Top             =   120
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data AAlbaran2 
      Caption         =   "AAlbaran2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   6840
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "AAlbaran"
      Top             =   120
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Data AlbaranData 
      Caption         =   "Albar�n"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2040
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM Albaran WHERE NAlbaran<>0 ORDER BY NAlbaran"
      Top             =   120
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Data ProvData 
      Caption         =   "Proveedores"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT * FROM Proveedores ORDER BY Nombre"
      Top             =   360
      Width           =   1935
   End
   Begin VB.TextBox IdBox 
      Height          =   285
      Left            =   600
      TabIndex        =   24
      Text            =   "Text2"
      Top             =   7080
      Visible         =   0   'False
      Width           =   255
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   9600
      Top             =   3120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":017A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":0494
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":07AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":0AC8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":0DE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":10FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":1416
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":1730
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AlbaranForm.frx":1A4A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label6 
      Caption         =   "N�mero de Pedido Asociado:"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   1200
      Width           =   2175
   End
   Begin VB.Menu Prg 
      Caption         =   "&Programa"
      Begin VB.Menu Sal 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu Reg 
      Caption         =   "&Registros"
      Begin VB.Menu ira 
         Caption         =   "Ir a"
         Shortcut        =   {F1}
      End
      Begin VB.Menu Prim 
         Caption         =   "Primero"
      End
      Begin VB.Menu Ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu Sig 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu Ulti 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu Add 
         Caption         =   "A�adir"
         Shortcut        =   ^I
      End
      Begin VB.Menu Eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu Listar 
      Caption         =   "&Listado"
      Begin VB.Menu Lista 
         Caption         =   "Listado"
      End
   End
   Begin VB.Menu Confi 
      Caption         =   "&Configuraci�n"
      Begin VB.Menu Arti 
         Caption         =   "Articulos"
      End
      Begin VB.Menu Prov 
         Caption         =   "Proveedores"
      End
   End
End
Attribute VB_Name = "AlbaranForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub ActualizarBoton_Click()
    
    Dim id As Integer, na As Integer, np As Integer, Ant As Double

On Error GoTo msgError
    
    id = IdBox.Text
    na = NAlbaranBox.Text
        
     With AAlbaran2
        .Recordset.FindFirst ("NAlbaran=" & na & " AND Linea=" & id)
        .Recordset.Edit
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NAlbaran").Value = NAlbaranBox.Text
        .Recordset.Fields("CodigoArticulo").Value = CABox.Caption
        Ant = .Recordset.Fields("Cantidad").Value
        .Recordset.Fields("Cantidad").Value = CanBox.Text
        .Recordset.Fields("PrecioUnit").Value = PrecBox.Text
        .Recordset.Fields("Descuento").Value = DescuentoBox.Text
        .Recordset.Fields("PParcial").Value = Val(PrecBox.Text) * Val(CanBox.Text) * (1 - (Val(DescuentoBox.Text) / 100))
        np = .Recordset.Fields("NPedido").Value
        .UpdateRecord
    
    End With
    
    With PPedidoData
        .Recordset.FindFirst ("NPedido=" & np & " AND CodigoArticulo=" & CABox.Caption)
        .Recordset.Edit
        
        If (.Recordset.Fields("Recibido")) <> 0 Then
            .Recordset.Fields("Recibido").Value = (.Recordset.Fields("Recibido").Value) - Ant
            .Recordset.Fields("Recibido") = (.Recordset.Fields("Recibido")) + CDbl(CanBox.Text)
        Else
            .Recordset.Fields("Recibido") = CanBox.Text
        End If
        
        .Recordset.Update
        
    End With
    
    With ArtData
        .Recordset.FindFirst ("CodigoArticulo=" & CABox.Caption)
        .Recordset.Edit
        .Recordset.Fields("PrecioUnit").Value = PrecBox.Text
        .Recordset.Fields("Existencias").Value = (.Recordset.Fields("Existencias").Value) + CLng(CanBox.Text)
        .Recordset.Update
    End With
    
    With AAlbaran
        .RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & NAlbaranBox.Text
        .Refresh
    End With
    
    AlbaranData.Recordset.Edit
    AlbaranData.Recordset.Fields("Total").Value = CaTotalAlb(Val(NAlbaranBox.Text))
    AlbaranData.Recordset.Update
    
    A�adirBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    
    id = ClearFields()
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Sub add_Click()
    Dim nada As Integer
    nada = OtroAlbaran()
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
    
End Sub

Private Sub A�adirBoton_Click()
   
    Dim id As Integer, Total As Integer, Cantidad As Integer, PrecU As Integer, np As Integer, Ant As Integer
On Error GoTo msgError
    With AAlbaran
    
        If .Recordset.BOF = False Then
            .Recordset.MoveLast
            id = .Recordset.Fields("Linea").Value
        Else
            id = 0
        End If
    
    End With
    
    
    With AAlbaran2
               
        id = id + 1
        
        .Recordset.AddNew
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NAlbaran").Value = NAlbaranBox.Text
        .Recordset.Fields("CodigoArticulo").Value = CABox.Caption
        .Recordset.Fields("Cantidad").Value = CanBox.Text
        .Recordset.Fields("PrecioUnit").Value = PrecBox.Text
        If DescuentoBox.Text = "" Then
            DescuentoBox.Text = "0"
        End If
        .Recordset.Fields("Descuento").Value = DescuentoBox.Text
        .Recordset.Fields("PParcial").Value = CDbl(PrecBox.Text) * CDbl(CanBox.Text) * (1 - (Val(DescuentoBox.Text) / 100))
        .Recordset.Fields("NPedido").Value = 0
        .UpdateRecord
        
    End With
       
    With AAlbaran
    
        .UpdateRecord
        .RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & NAlbaranBox.Text
        .Refresh
           
        .Recordset.MoveFirst
    End With
    
    With ArtData
        .Recordset.FindFirst ("CodigoArticulo=" & CABox.Caption)
        .Recordset.Edit
        .Recordset.Fields("PrecioUnit").Value = PrecBox.Text
        .Recordset.Fields("Existencias").Value = (.Recordset.Fields("Existencias").Value) + CLng(PrecBox.Text)
        .Recordset.Fields("UltAlbaran").Value = Date
        .Recordset.Update
    End With
    
    AlbaranData.Recordset.Edit
    AlbaranData.Recordset.Fields("Total").Value = CaTotalAlb(Val(NAlbaranBox.Text))
    AlbaranData.Recordset.Update
    
    AAlbaran2.Refresh
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Sub Arti_Click()
    MantArt.Show modal
    MantArt.OptionBox.Text = "2"
End Sub

Private Sub AsoBoton_Click()
    
    Dim id As Integer, ca As Long, Cant As Double
On Error GoTo msgError
    If NPABox.Text <> "" Then
        
        With AAlbaran
            If .Recordset.EOF = False Then
                .Recordset.MoveLast
                id = .Recordset.Fields("Linea").Value
            Else
                id = 0
            End If
        End With
        
        PPedidoData.Recordset.FindFirst ("NPedido=" & NPABox.Text)
        
        If PPedidoData.Recordset.NoMatch = False Then
            
            PedidoData.Recordset.FindFirst ("NPedido=" & NPABox.Text)
            
            If id <> 0 And CPBox.Text <> PedidoData.Recordset.Fields("CodigoProveedor").Value Then
                MsgBox ("Pedido no perteneciente al proveedor del albar�n")
            Else
                CPBox.Text = PedidoData.Recordset.Fields("CodigoProveedor").Value
                
                While PPedidoData.Recordset.NoMatch = False
               
                    If PPedidoData.Recordset.Fields("Recibido").Value < PPedidoData.Recordset.Fields("Cantidad").Value Then
                        With AAlbaran2
                                                
                            id = id + 1
                            
                            .Recordset.AddNew
                            .Recordset.Fields("Linea").Value = id
                            .Recordset.Fields("NAlbaran").Value = NAlbaranBox.Text
                            ca = PPedidoData.Recordset.Fields("CodigoArticulo").Value
                            ArtData.Recordset.FindFirst ("CodigoArticulo=" & ca)
                            .Recordset.Fields("PrecioUnit").Value = ArtData.Recordset.Fields("PrecioUnit").Value
                            .Recordset.Fields("CodigoArticulo").Value = ca
                            Cant = (PPedidoData.Recordset.Fields("Cantidad").Value) - (PPedidoData.Recordset.Fields("Recibido").Value)
                            .Recordset.Fields("Cantidad").Value = Cant
                            .Recordset.Fields("NPedido").Value = PPedidoData.Recordset.Fields("NPedido").Value
                            .Recordset.Fields("PParcial").Value = CDbl(Cant) * CDbl(ArtData.Recordset.Fields("PrecioUnit").Value)
                            .UpdateRecord
        
                        End With
    
                        With PPedidoData
                            .Recordset.Edit
                            .Recordset.Fields("Recibido").Value = (.Recordset.Fields("Cantidad").Value)
                            .Recordset.Update
                            .Recordset.FindNext ("NPedido=" & NPABox.Text)
                        End With
                        
                        With ArtData
                            .Recordset.FindFirst ("CodigoArticulo=" & ca)
                            .Recordset.Edit
                            .Recordset.Fields("Existencias").Value = (.Recordset.Fields("Existencias").Value) + Cant
                            .Recordset.Fields("UltAlbaran").Value = Date
                            .Recordset.Update
                        End With
                        
                    Else
                        PPedidoData.Recordset.FindNext ("NPedido=" & NPABox.Text)
                    End If
                
                Wend
                
                With AAlbaran
                    .RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & NAlbaranBox.Text
                    .Refresh
                End With

                With AlbaranData
                    .Recordset.Edit
                    .Recordset.Fields("Total").Value = CaTotalAlb(Val(NAlbaranBox.Text))
                    .Recordset.Update
                End With
            
            End If
        Else
        
            MsgBox ("Pedido no existente")
        
        End If
                       
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub BorrarBoton_Click()
    Dim id As Integer, na As Long, np As Long, Ant As Long, ca As Long
On Error GoTo msgError
    id = IdBox.Text
    na = NAlbaranBox.Text
     
    With AAlbaran2
        .Recordset.FindFirst ("NAlbaran=" & na & " AND Linea=" & id)
        np = .Recordset.Fields("NPedido").Value
        Ant = .Recordset.Fields("Cantidad").Value
        ca = .Recordset.Fields("CodigoArticulo").Value
        .Recordset.Delete
        .Refresh
    End With
    
    With PPedidoData
        .Recordset.FindFirst ("NPedido=" & np & " AND CodigoArticulo=" & ca)
        .Recordset.Edit
        .Recordset.Fields("Recibido").Value = .Recordset.Fields("Recibido").Value - Ant
        .Recordset.Update
    End With
    
    With AAlbaran
        .RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & NAlbaranBox.Text
        .Refresh
    End With
    
    AlbaranData.Recordset.Edit
    AlbaranData.Recordset.Fields("Total").Value = CaTotalAlb(Val(NAlbaranBox.Text))
    AlbaranData.Recordset.Update
    
    A�adirBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
    
End Sub

Private Sub CPBox_Change()
On Error GoTo msgError
    If CPBox.Text <> "" Then
        ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)
        If ProvData.Recordset.NoMatch = True Then
            ProvBox.Caption = "No Encontrado"
        End If
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Function Anterior()
    
    Dim np As Integer
On Error GoTo msgError
    With AlbaranData
        .Recordset.MovePrevious
        If .Recordset.BOF = True Then
            .Recordset.MoveNext
        End If
        np = .Recordset.Fields("NAlbaran").Value
    End With
           
    AAlbaran.RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & np
    AAlbaran.Refresh
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Siguiente()
   
    Dim np As Integer
On Error GoTo msgError
    With AlbaranData
        .Recordset.MoveNext
        If .Recordset.EOF = True Then
            .Recordset.MovePrevious
        End If
        np = .Recordset.Fields("NAlbaran").Value
    End With
    
    AAlbaran.RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & np
    AAlbaran.Refresh
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function


Private Sub CPBox_CloseUp()
On Error GoTo msgError
    ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)
        Exit Sub
msgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Sub DescBox_Change()
On Error GoTo msgError
    If DescBox.Text <> "" Then
        ArtData.Recordset.FindFirst ("Descripcion='" & DescBox.Text & "'")
        CABox.Caption = ArtData.Recordset.Fields("CodigoArticulo").Value
    Else
        CABox.Caption = ""
    End If
    
    If ArtData.Recordset.NoMatch = True Then
        CABox.Caption = ""
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Sub DescBox_CloseUp()
On Error GoTo msgError
    If DescBox.Text <> "" Then
        ArtData.Recordset.FindFirst ("Descripcion='" & DescBox.Text & "'")
        CABox.Caption = ArtData.Recordset.Fields("CodigoArticulo").Value
    Else
        CABox.Caption = ""
    End If
    
    If ArtData.Recordset.NoMatch = True Then
        CABox.Caption = ""
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub eraser_Click()
    Dim Resp
    Resp = MsgBox("�Est� usted seguro de querer borrar el albar�n?", vbYesNo)
    If Resp = vbYes Then
        AlbaranData.Recordset.Delete
        Resp = Anterior()
    End If
End Sub

Private Sub Form_Load()
  On Error GoTo cwint
  Exit Sub

cwint:

  MsgBox ("Pulsado")

End Sub

Private Sub ira_Click()
    Set Objeto = AlbaranForm.AlbaranData
    Set Objeto2 = AlbaranForm.AAlbaran
    
    Selct = True
    
    Display(0) = "NAlbaran"
    DataCarac = "SELECT AL.*,AR.Descripcion  FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran="
    DataCarac2 = " ORDER BY AL.Linea"
    
    IraForm.Show modal

End Sub

Private Sub lista_Click()
    Dim nada As Integer
    nada = ListAlb()
End Sub

Private Sub prim_Click()
    Dim nada As Integer
    nada = Primero()
    
End Sub

Private Sub Prov_Click()
    MantProv.Show modal
    ProvData.Refresh
End Sub

Private Sub Sal_Click()
    Unload Me
    
End Sub

Private Sub sig_Click()
    Dim nada As Integer
    nada = Siguiente()
    
End Sub

Private Sub SSDBGrid1_DblClick()
    Dim id As Integer
On Error GoTo msgError
    With AAlbaran
        id = .Recordset.Fields("Linea").Value
        IdBox.Text = id
        CABox.Caption = .Recordset.Fields("CodigoArticulo").Value
        DescBox.Text = .Recordset.Fields("Descripcion").Value
        CanBox.Text = .Recordset.Fields("Cantidad").Value
        DescuentoBox.Text = .Recordset.Fields("Descuento").Value
        
        If .Recordset.Fields("PrecioUnit").Value <> 0 Then
            PrecBox.Text = .Recordset.Fields("PrecioUnit").Value
        Else
            PrecBox.Text = "0"
        
        End If
     
    End With
    
    With ArtData
        .Recordset.FindFirst ("CodigoArticulo=" & CABox.Caption)
        .Recordset.Edit
        .Recordset.Fields("Existencias").Value = (.Recordset.Fields("Existencias").Value) - CLng(CanBox.Text)
        .Recordset.Update
    End With
    
    A�adirBoton.Visible = False
    BorrarBoton.Visible = True
    ActualizarBoton.Visible = True
    SSDBGrid1.Enabled = False
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub SSDBGrid2_DblClick()
    NPABox.Text = PedidoData.Recordset.Fields("NPedido").Value
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtroAlbaran()
    Case "AArticulo"
        MantArt.Show modal
        MantArt.OptionBox.Text = "2"
    Case "Lista"
        nada = ListAlb()
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de quere borrar el albar�n?", vbYesNo)
        If nada = vbYes Then
            AlbaranData.Recordset.Delete
            nada = Anterior()
        End If
    Case "first"
        nada = Primero()
    Case "last"
        nada = Ultimo()

    End Select
    

End Sub

Private Function OtroAlbaran() As Integer

    Dim num As Integer
On Error GoTo msgError
    AlbaranData.Recordset.MoveLast
        
    num = (AlbaranData.Recordset.Fields("NAlbaran").Value) + 1
    
    With AlbaranData
        .Recordset.AddNew
        .Recordset.Fields("NAlbaran").Value = num
        .Recordset.Fields("FechaAlbaran").Value = Date
        .Recordset.Fields("CodigoProveedor").Value = 0
        .Recordset.Update
        .Recordset.MoveLast
    End With

    AAlbaran.RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & num
    AAlbaran.Refresh
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function


Private Function ClearFields()

    CABox.Caption = ""
    CanBox.Text = ""
    PrecBox.Text = ""
    DescuentoBox.Text = ""
    
End Function

Private Sub ulti_Click()
    Dim nada As Integer
    nada = Ultimo()
End Sub

Private Function ListAlb()
        
    Dim nada As Integer
    nada = NuevaBusq()
    Display(0) = "NAlbaran"
    Display(1) = "CodigoProveedor"
    Display(2) = "FechaAlbaran"
    Display(3) = "NAlbaranP"
    Display(4) = ""
    Display(5) = ""
    Display(6) = ""
    Display(7) = ""
    
    campos(4) = ""
    campos(5) = ""
    campos(6) = ""
    campos(7) = ""
    
    TypeCampos(0) = 1
    TypeCampos(1) = 3
    TypeCampos(2) = 2
    TypeCampos(3) = 1
    TypeCampos(4) = 0
    TypeCampos(5) = 0
    TypeCampos(6) = 0
    TypeCampos(7) = 0
    
    For nada = 0 To 3
        campos(nada) = "AL." & Display(nada)
    Next
        
    Lists(1, 0) = "CodigoProveedor, Nombre"
    Lists(1, 1) = "CodigoProveedor"
    Lists(1, 2) = "Proveedores ORDER BY Nombre"
        
    FRO = "Albaran AL, Proveedores PR"
    SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3) & ",PR.Nombre"
    WHER = "AL.CodigoProveedor=PR.CodigoProveedor "
    ORD = "AL.NAlbaran"
        
    Set Objeto = AlbaranForm.AlbaranData
    Set Objeto2 = AlbaranForm.AAlbaran
    DataCarac = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran="
    DataCarac2 = ""
    
    Selct = True

    nada = Listado()

End Function

Private Function Primero()
On Error GoTo msgError
    AlbaranData.Recordset.MoveFirst
    AAlbaran.RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & NAlbaranBox.Text
    AAlbaran.Refresh
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Ultimo()
On Error GoTo msgError
    AlbaranData.Recordset.MoveLast
    AAlbaran.RecordSource = "SELECT AL.*,AR.Descripcion FROM AAlbaran AL, Articulos AR WHERE AL.CodigoArticulo=AR.CodigoArticulo AND AL.NAlbaran=" & NAlbaranBox.Text
    AAlbaran.Refresh
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function
