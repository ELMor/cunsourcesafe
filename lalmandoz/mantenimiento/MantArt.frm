VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form MantArt 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento Art�culos"
   ClientHeight    =   4860
   ClientLeft      =   2820
   ClientTop       =   3075
   ClientWidth     =   9615
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4860
   ScaleWidth      =   9615
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   1535
      ButtonWidth     =   1693
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   10
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "first"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Todos"
            Key             =   "Tutos"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Crear Grupo"
            Key             =   "Grp"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "last"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
      EndProperty
   End
   Begin VB.TextBox OptionBox 
      Height          =   375
      Left            =   240
      TabIndex        =   12
      Top             =   960
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "El Sue�o de Todo Buen Almacenero"
      Height          =   375
      Left            =   3120
      TabIndex        =   11
      Top             =   4320
      Width           =   3015
   End
   Begin SSDataWidgets_B.SSDBCombo GrArtCombo 
      Bindings        =   "MantArt.frx":0000
      Height          =   255
      Left            =   3600
      TabIndex        =   6
      Top             =   1080
      Width           =   1215
      DataFieldList   =   "CodigoGrArt"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2223
      Columns(0).Caption=   "C�digo Art�culo"
      Columns(0).Name =   "CodigoGrArt"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoGrArt"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   8784
      Columns(1).Caption=   "Grupo"
      Columns(1).Name =   "Grupo"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Grupo"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2143
      _ExtentY        =   450
      _StockProps     =   93
      Text            =   "100000"
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "CodigoGrArt"
   End
   Begin VB.Data GruposData 
      Caption         =   "Grupos"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   5280
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "GrArticulos"
      Top             =   240
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data ArtData 
      Caption         =   "Articulo"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   1920
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM Articulos WHERE GrupoArt=100000"
      Top             =   240
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.TextBox Text3 
      DataField       =   "SuCodigo"
      DataSource      =   "ArtData"
      Height          =   285
      Left            =   2520
      TabIndex        =   5
      Top             =   3240
      Width           =   2775
   End
   Begin VB.TextBox DescrBox 
      DataField       =   "Descripcion"
      DataSource      =   "ArtData"
      Height          =   285
      Left            =   2520
      TabIndex        =   3
      Top             =   2640
      Width           =   6375
   End
   Begin VB.TextBox CABox 
      DataField       =   "CodigoArticulo"
      DataSource      =   "ArtData"
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   1
      Top             =   2040
      Width           =   1215
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   10
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":0015
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":032F
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":0649
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":0963
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":0C7D
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":0F97
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":12B1
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":15CB
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":18E5
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantArt.frx":1BFF
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label ExBox 
      BorderStyle     =   1  'Fixed Single
      DataField       =   "Existencias"
      DataSource      =   "ArtData"
      Height          =   255
      Left            =   2520
      TabIndex        =   10
      Top             =   3840
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "Existencias:"
      Height          =   255
      Left            =   360
      TabIndex        =   9
      Top             =   3840
      Width           =   1935
   End
   Begin VB.Line Line2 
      X1              =   4800
      X2              =   5160
      Y1              =   1200
      Y2              =   1200
   End
   Begin VB.Label GrupoBox 
      BorderStyle     =   1  'Fixed Single
      DataField       =   "Grupo"
      DataSource      =   "GruposData"
      Height          =   255
      Left            =   5160
      TabIndex        =   8
      Top             =   1080
      Width           =   3015
   End
   Begin VB.Label Label4 
      Caption         =   "Grupo de Articulos:"
      Height          =   255
      Left            =   2040
      TabIndex        =   7
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   9120
      Y1              =   1680
      Y2              =   1680
   End
   Begin VB.Label Label3 
      Caption         =   "C�digo Art�culo Proveedor:"
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   3240
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Descripci�n:"
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   2640
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Art�culo:"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   2040
      Width           =   1335
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu Exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu first 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu Eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu Gr 
      Caption         =   "&Grupos"
      Begin VB.Menu MantGr 
         Caption         =   "Mantenimiento Grupos"
      End
   End
   Begin VB.Menu listas 
      Caption         =   "&Listados"
      Begin VB.Menu all 
         Caption         =   "Todos"
      End
      Begin VB.Menu listx 
         Caption         =   "Listado"
      End
   End
End
Attribute VB_Name = "MantArt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function Anterior()

    If ArtData.Recordset.RecordCount = 0 Then
        Exit Function
    End If
    
    ArtData.Recordset.MovePrevious
 
    If ArtData.Recordset.BOF = True Then
        ArtData.Recordset.MoveNext
    End If
    
End Function

Private Function OtroArt()

    Dim Con As Long
On Error GoTo msgError
    With ArtData
        If .Recordset.RecordCount = 0 Then
            Con = CLng(GrArtCombo.Text)
        Else
            .Recordset.MoveLast
            Con = .Recordset.Fields("CodigoArticulo").Value
        End If
        .Recordset.AddNew
        .Recordset.Fields("CodigoArticulo").Value = Con + 1
        .Recordset.Fields("GrupoArt").Value = CLng(GrArtCombo.Text)
        .Recordset.Update
        .Recordset.MoveLast
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
    End Function

Private Function Siguiente()
    
    If ArtData.Recordset.RecordCount = 0 Then
        Exit Function
    End If
    
    ArtData.Recordset.MoveNext
    
    If ArtData.Recordset.EOF = True Then
        ArtData.Recordset.MovePrevious
    End If

End Function


Private Sub add_Click()
    Dim nada As Integer
    nada = OtroArt()

End Sub

Private Sub all_Click()
    Dim nada As Integer
    nada = Tutos()
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
    
End Sub

Private Sub Command1_Click()
On Error GoTo msgError
    With PhillipsDream
        .Show modal
        .CABox.Caption = CABox.Text
        .DescBox.Caption = DescrBox.Text
        .DreamData.RecordSource = "SELECT F.FechaFactura, FF.PrecioUnit, FF.Descuento, P.Nombre FROM Proveedores P, Factura F, FFactura FF WHERE FF.CodigoArticulo=" & CABox.Text & " AND FF.NFactura=F.NFactura AND P.CodigoProveedor=F.CodigoProveedor ORDER BY F.FechaFactura"
        .DreamData.Refresh
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar el art�culo?", vbYesNo)
    If nada = vbYes Then
        ArtData.Recordset.Delete
        nada = Anterior()
    End If
    

End Sub

Private Sub exit_Click()
    Dim nada As Integer
    nada = Exitoz()
    
End Sub

Private Sub first_Click()
    ArtData.Recordset.MoveFirst
End Sub

Private Sub GrArtCombo_Change()
    
    Dim Cod As Long
On Error GoTo msgError
    GruposData.Recordset.FindFirst ("CodigoGrArt=" & GrArtCombo.Text)
    GrupoBox.Caption = GruposData.Recordset.Fields("Grupo").Value
    
    ArtData.RecordSource = "SELECT * FROM Articulos WHERE GrupoArt=" & GrArtCombo.Text & " ORDER BY CodigoArticulo"
    ArtData.Refresh
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub GrArtCombo_CloseUp()

    Dim Cod As Long
On Error GoTo msgError
    GruposData.Recordset.FindFirst ("CodigoGrArt=" & GrArtCombo.Text)
    GrupoBox.Caption = GruposData.Recordset.Fields("Grupo").Value
    
    ArtData.RecordSource = "SELECT * FROM Articulos WHERE GrupoArt=" & GrArtCombo.Text & " ORDER BY CodigoArticulo"
    ArtData.Refresh
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub last_Click()
    ArtData.Recordset.MoveLast
End Sub

Private Sub listx_Click()
    nada = ListaArt()
End Sub

Private Sub MantGr_Click()
    CreaGrpForm.Show modal
End Sub

Private Sub sigui_Click()
    Dim nada As Integer
    nada = Siguiente()
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        nada = Exitoz()
    Case "Add"
        nada = OtroArt()
    
    Case "Tutos"
        nada = Tutos()
        
    Case "Lista"
        nada = ListaArt()

    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar el art�culo?", vbYesNo)
        If nada = vbYes Then
            ArtData.Recordset.Delete
            nada = Anterior()
        End If

    Case "Grp"
        CreaGrpForm.Show modal
    
    Case "first"
        ArtData.Recordset.MoveFirst
    Case "last"
        ArtData.Recordset.MoveLast
    
    End Select
    

End Sub


Private Function Tutos()
    
    Dim nada As Integer
On Error GoTo msgError
    nada = NuevaBusq()
    campos(0) = "CodigoArticulo"
    campos(1) = "Descripcion"
    campos(2) = "SuCodigo"
    campos(3) = "Existencias"
    For nada = 0 To 3
        Display(nada) = campos(nada)
    Next
    
    SELE = "CodigoArticulo, Descripcion, SuCodigo, Existencias, GrupoArt"
    FRO = "Articulos"
    WHER = "GrupoArt=" & GrArtCombo.Text
    
    Set Objeto = MantArt.ArtData
    
    Selct = False
    
    Selct2 = 1
    
    MosBusForm.Show modal
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function ListaArt()
    Dim nada As Integer
On Error GoTo msgError
    nada = NuevaBusq()
    Display(0) = "CodigoArticulo"
    Display(1) = "Descripcion"
    Display(2) = "SuCodigo"
    Display(3) = "GrupoArt"
    
    TypeCampos(0) = 1
    TypeCampos(1) = 4
    TypeCampos(2) = 4
    TypeCampos(3) = 3
    TypeCampos(4) = 0
    TypeCampos(5) = 0
    TypeCampos(6) = 0
    TypeCampos(7) = 0
        
    For nada = 0 To 3
        campos(nada) = Display(nada)
    Next
        
    Lists(3, 0) = "CodigoGrArt, Grupo"
    Lists(3, 1) = "CodigoGrArt"
    Lists(3, 2) = "GrArticulos"
        
    FRO = "Articulos"
    SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3)
                
    Set Objeto = MantArt.ArtData
        
    Selct = False
    
    Selct2 = 1
        
    nada = Listado()
    
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Exitoz()
On Error GoTo msgError
     If OptionBox.Text <> "" Then
        Select Case CLng(OptionBox.Text)
        Case 1
            PedidoForm.ArtData.Refresh
        Case 3
            FacturaForm.ArticulosData.Refresh
        Case 2
            AlbaranForm.ArtData.Refresh
        End Select
    End If
            
    Unload Me
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function
