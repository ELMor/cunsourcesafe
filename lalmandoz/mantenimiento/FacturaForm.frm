VERSION 5.00
Object = "{FAEEE763-117E-101B-8933-08002B2F4F5A}#1.1#0"; "DBLIST32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form FacturaForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Facturas"
   ClientHeight    =   7650
   ClientLeft      =   3405
   ClientTop       =   -120
   ClientWidth     =   9510
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7650
   ScaleWidth      =   9510
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   40
      Top             =   0
      Width           =   9510
      _ExtentX        =   16775
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   9
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "prim"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntrBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Articulos"
            Key             =   "AArticulo"
            Description     =   "AddArticulo"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "ulti"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.Data AAlbaranData 
      Caption         =   "AAlbaran"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   4680
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "AAlbaran"
      Top             =   360
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data ProvData 
      Caption         =   "Proveedores"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2280
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT CodigoProveedor, Nombre FROM Proveedores ORDER BY Nombre"
      Top             =   360
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Data FacturaData 
      Caption         =   "Factura"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Factura"
      Top             =   120
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data FFacturaData 
      Caption         =   "FFactura"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT FF.*, A.Descripcion FROM FFactura FF, Articulos A WHERE FF.NFactura=3 AND FF.CodigoArticulo=A.CodigoArticulo"
      Top             =   360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data FFactura2Data 
      Caption         =   "FFactura2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2280
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "FFactura"
      Top             =   120
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Data ArticulosData 
      Caption         =   "Articulos"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   4680
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM Articulos ORDER BY Descripcion"
      Top             =   120
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data AlbaranData 
      Caption         =   "Albaranes"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   6960
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   $"FacturaForm.frx":0000
      Top             =   120
      Visible         =   0   'False
      Width           =   2055
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid2 
      Bindings        =   "FacturaForm.frx":014A
      Height          =   1335
      Left            =   5040
      TabIndex        =   38
      Top             =   960
      Width           =   4215
      ScrollBars      =   2
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowUpdate     =   0   'False
      SelectTypeRow   =   1
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1455
      Columns(0).Caption=   "NAlbaran"
      Columns(0).Name =   "NAlbaranP"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "NAlbaranP"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1508
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "FechaAlbaran"
      Columns(1).Alignment=   1
      Columns(1).CaptionAlignment=   1
      Columns(1).DataField=   "FechaAlbaran"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   3863
      Columns(2).Caption=   "Nombre"
      Columns(2).Name =   "Nombre"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Nombre"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      UseDefaults     =   -1  'True
      _ExtentX        =   7435
      _ExtentY        =   2355
      _StockProps     =   79
      Caption         =   "Albaranes Pendientes de Facturar"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame3 
      Caption         =   "TOTAL"
      Height          =   615
      Left            =   6240
      TabIndex        =   32
      Top             =   6960
      Width           =   2655
      Begin VB.Label TotalBox 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         DataField       =   "TTotal"
         DataSource      =   "FacturaData"
         Height          =   255
         Left            =   600
         TabIndex        =   33
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.TextBox DtoBox 
      DataField       =   "Dto"
      DataSource      =   "FacturaData"
      Height          =   285
      Left            =   4560
      TabIndex        =   31
      Top             =   6960
      Width           =   615
   End
   Begin VB.TextBox IVABox 
      DataField       =   "IVA"
      DataSource      =   "FacturaData"
      Height          =   285
      Left            =   2160
      TabIndex        =   29
      Top             =   6960
      Width           =   615
   End
   Begin VB.Frame Frame2 
      Caption         =   "Art�culos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Left            =   240
      TabIndex        =   1
      Top             =   3840
      Width           =   9015
      Begin VB.TextBox DescuentoBox 
         Height          =   285
         Left            =   6960
         TabIndex        =   27
         Text            =   "0"
         Top             =   2520
         Width           =   615
      End
      Begin VB.CommandButton BorrarBoton 
         Caption         =   "&Borrar"
         Height          =   375
         Left            =   7800
         TabIndex        =   8
         Top             =   2160
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton ActualizarBoton 
         Caption         =   "&Actualizar"
         Height          =   375
         Left            =   7800
         TabIndex        =   7
         Top             =   2520
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox CanBox 
         Height          =   285
         Left            =   4680
         TabIndex        =   6
         Text            =   "0"
         Top             =   2520
         Width           =   975
      End
      Begin VB.TextBox PrecBox 
         Height          =   285
         Left            =   5760
         TabIndex        =   5
         Text            =   "0"
         Top             =   2520
         Width           =   1095
      End
      Begin VB.CommandButton A�adirBoton 
         Caption         =   "A&�adir"
         Height          =   375
         Left            =   7800
         TabIndex        =   2
         Top             =   2520
         Width           =   975
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Bindings        =   "FacturaForm.frx":0160
         Height          =   1575
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   8670
         ScrollBars      =   2
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         CellNavigation  =   1
         MaxSelectedRows =   1
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   2196
         Columns(0).Caption=   "C�digo art�culo"
         Columns(0).Name =   "CodigoArticulo"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "CodigoArticulo"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   6509
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripcion"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Descripcion"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1455
         Columns(2).Caption=   "Cantidad"
         Columns(2).Name =   "Cantidad"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Cantidad"
         Columns(2).DataType=   3
         Columns(2).FieldLen=   256
         Columns(3).Width=   1720
         Columns(3).Caption=   "PrecioUnit"
         Columns(3).Name =   "PrecioUnit"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "PrecioUnit"
         Columns(3).DataType=   6
         Columns(3).FieldLen=   256
         Columns(4).Width=   1005
         Columns(4).Caption=   "Dto"
         Columns(4).Name =   "Descuento1"
         Columns(4).CaptionAlignment=   0
         Columns(4).DataField=   "Descuento"
         Columns(4).DataType=   3
         Columns(4).FieldLen=   256
         Columns(5).Width=   1402
         Columns(5).Caption=   "PParcial"
         Columns(5).Name =   "PParcial"
         Columns(5).CaptionAlignment=   0
         Columns(5).DataField=   "PParcial"
         Columns(5).DataType=   6
         Columns(5).FieldLen=   256
         UseDefaults     =   0   'False
         _ExtentX        =   15293
         _ExtentY        =   2778
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDBCtls.DBCombo DescBox 
         Bindings        =   "FacturaForm.frx":0177
         Height          =   315
         Left            =   1440
         TabIndex        =   4
         Top             =   2520
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   556
         _Version        =   327681
         ListField       =   "Descripcion"
         Text            =   ""
      End
      Begin VB.Label CABox 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   2520
         Width           =   1215
      End
      Begin VB.Label Label11 
         Caption         =   "Dto"
         Height          =   255
         Left            =   7080
         TabIndex        =   26
         Top             =   2280
         Width           =   375
      End
      Begin VB.Label Label7 
         Caption         =   "C�digo Art�culo"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "Descripci�n"
         Height          =   255
         Left            =   1440
         TabIndex        =   13
         Top             =   2280
         Width           =   1335
      End
      Begin VB.Label Label9 
         Caption         =   "Cantidad"
         Height          =   255
         Left            =   4680
         TabIndex        =   12
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Precio Unitario"
         Height          =   255
         Left            =   5760
         TabIndex        =   11
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label TotalLabel 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Total"
         DataSource      =   "FacturaData"
         Height          =   255
         Left            =   7680
         TabIndex        =   10
         Top             =   1920
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "Total:"
         Height          =   255
         Left            =   7080
         TabIndex        =   9
         Top             =   1920
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos Factura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   240
      TabIndex        =   17
      Top             =   2160
      Width           =   9015
      Begin SSDataWidgets_B.SSDBCombo CPBox 
         Bindings        =   "FacturaForm.frx":018F
         DataField       =   "CodigoProveedor"
         DataSource      =   "FacturaData"
         Height          =   255
         Left            =   1800
         TabIndex        =   36
         Top             =   720
         Width           =   1575
         DataFieldList   =   "CodigoProveedor"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2487
         Columns(0).Caption=   "C�digo Proveedor"
         Columns(0).Name =   "CodigoProveedor"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "CodigoProveedor"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   5689
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "Nombre"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Nombre"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2778
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "CodigoProveedor"
      End
      Begin VB.TextBox NFacturaPBox 
         DataField       =   "NFacturaP"
         DataSource      =   "FacturaData"
         Height          =   285
         Left            =   4320
         TabIndex        =   35
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox NFacturaBox 
         BackColor       =   &H80000018&
         DataField       =   "NFactura"
         DataSource      =   "FacturaData"
         Enabled         =   0   'False
         Height          =   285
         Left            =   1200
         TabIndex        =   20
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox Text1 
         DataField       =   "Comentario"
         DataSource      =   "FacturaData"
         Height          =   495
         Left            =   1800
         TabIndex        =   18
         Top             =   1080
         Width           =   6975
      End
      Begin SSCalendarWidgets_A.SSDateCombo FechaBox 
         DataField       =   "FechaFactura"
         DataSource      =   "FacturaData"
         Height          =   255
         Left            =   7200
         TabIndex        =   19
         Top             =   360
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label ProvBox 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Nombre"
         DataSource      =   "ProvData"
         Height          =   255
         Left            =   3600
         TabIndex        =   37
         Top             =   720
         Width           =   5175
      End
      Begin VB.Line Line1 
         X1              =   3360
         X2              =   3600
         Y1              =   840
         Y2              =   840
      End
      Begin VB.Label Label15 
         Caption         =   "N�Factura Proveedor:"
         Height          =   255
         Left            =   2760
         TabIndex        =   34
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "N�Factura:"
         Height          =   255
         Left            =   360
         TabIndex        =   24
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha Factura"
         Height          =   255
         Left            =   6000
         TabIndex        =   23
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "C�digo Proveedor:"
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "Comentario:"
         Height          =   255
         Left            =   360
         TabIndex        =   21
         Top             =   1080
         Width           =   975
      End
   End
   Begin VB.TextBox NPABox 
      Height          =   285
      Left            =   2520
      TabIndex        =   16
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton AsoBoton 
      Caption         =   "&Asociar"
      Height          =   375
      Left            =   3840
      TabIndex        =   15
      Top             =   1200
      Width           =   975
   End
   Begin VB.TextBox IdBox 
      Height          =   285
      Left            =   720
      TabIndex        =   0
      Text            =   "Text2"
      Top             =   6480
      Visible         =   0   'False
      Width           =   255
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":01A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":04BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":07D6
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":0AF0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":0E0A
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":1124
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":143E
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":1758
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FacturaForm.frx":1A72
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label14 
      Caption         =   "Descuento"
      Height          =   255
      Left            =   3600
      TabIndex        =   30
      Top             =   6960
      Width           =   855
   End
   Begin VB.Label Label13 
      Caption         =   "I.V.A. (%)"
      Height          =   255
      Left            =   1320
      TabIndex        =   28
      Top             =   6960
      Width           =   735
   End
   Begin VB.Label Label6 
      Caption         =   "N�mero de Albar�n Asociado:"
      Height          =   255
      Left            =   240
      TabIndex        =   25
      Top             =   1200
      Width           =   2175
   End
   Begin VB.Menu Prg 
      Caption         =   "&Programa"
      Begin VB.Menu Sal 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu Reg 
      Caption         =   "&Registros"
      Begin VB.Menu ira 
         Caption         =   "Ir a"
         Shortcut        =   {F1}
      End
      Begin VB.Menu Prim 
         Caption         =   "Primero"
      End
      Begin VB.Menu Ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu Sig 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu Ult 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu Add 
         Caption         =   "A�adir"
         Shortcut        =   ^I
      End
      Begin VB.Menu Eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu Confi 
      Caption         =   "&Configuraci�n"
      Begin VB.Menu Art 
         Caption         =   "Art�culos"
      End
      Begin VB.Menu Prov 
         Caption         =   "Proveedores"
      End
   End
   Begin VB.Menu Listillo 
      Caption         =   "&Listados"
      Begin VB.Menu Lista 
         Caption         =   "Listado"
      End
   End
End
Attribute VB_Name = "FacturaForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub ActualizarBoton_Click()
    
    Dim id As Integer, np As Integer, na As Integer
On Error GoTo msgError
    id = IdBox.Text
    nf = NFacturaBox.Text
        
     With FFactura2Data
        .Recordset.FindFirst ("NFactura=" & nf & " AND Linea=" & id)
        .Recordset.Edit
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("CodigoArticulo").Value = CABox.Caption
        .Recordset.Fields("Cantidad").Value = CanBox.Text
        .Recordset.Fields("PrecioUnit").Value = PrecBox.Text
        .Recordset.Fields("Descuento").Value = DescuentoBox.Text
        .Recordset.Fields("PParcial").Value = (Val(PrecBox.Text) * Val(CanBox.Text)) * (1 - (Val(DescuentoBox.Text) / 100))
        .UpdateRecord
    
    End With
    
    With ArticulosData
        .Recordset.FindFirst ("CodigoArticulo=" & CABox.Caption)
        .Recordset.Edit
        .Recordset.Fields("PrecioUnit").Value = PrecBox.Text
        .Recordset.Update
    End With
    
    With FFacturaData
        .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
        .Refresh
    End With
    
    With FacturaData
        .Recordset.Edit
        .Recordset.Fields("Total").Value = CaTotalFac(Val(NFacturaBox.Text))
        .Recordset.Fields("TTotal").Value = ((.Recordset.Fields("Total").Value) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100))) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
        .Recordset.Update
    End With
    
    A�adirBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    
    id = ClearFields()
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
    
End Sub

Private Sub add_Click()
    Dim nada As Integer
    nada = OtraFactura()
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
End Sub

Private Sub A�adirBoton_Click()
   
    Dim id As Integer, Total As Integer, Cantidad As Integer, PrecU As Integer, np As Integer, na As Integer, Ant As Integer
On Error GoTo msgError
    With FFacturaData
    
        If .Recordset.BOF = False Then
            .Recordset.MoveLast
            id = .Recordset.Fields("Linea").Value
        Else
            id = 0
        End If
    
    End With
    
    If DescuentoBox.Text = "" Then
        DescuentoBox.Text = "0"
    End If
        
    With FFactura2Data
               
        id = id + 1
        
        .Recordset.AddNew
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NFactura").Value = NFacturaBox.Text
        .Recordset.Fields("CodigoArticulo").Value = CABox.Caption
        .Recordset.Fields("Cantidad").Value = CanBox.Text
        .Recordset.Fields("PrecioUnit").Value = PrecBox.Text
        .Recordset.Fields("Descuento").Value = DescuentoBox.Text
        .Recordset.Fields("PParcial").Value = (Val(PrecBox.Text) * Val(CanBox.Text)) * (1 - (Val(DescuentoBox.Text) / 100))
        .Recordset.Fields("NPedido").Value = 1
        .Recordset.Fields("NAlbaran").Value = 1
        .UpdateRecord
        
    End With
       
    With FFacturaData
        .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
        .Refresh
    End With
    
    With ArticulosData
        .Recordset.FindFirst ("CodigoArticulo=" & CABox.Caption)
        .Recordset.Edit
        .Recordset.Fields("PrecioUnit").Value = PrecBox.Text
        .Recordset.Update
    End With
    
    With FacturaData
        .Recordset.Edit
        .Recordset.Fields("Total").Value = CaTotalFac(Val(NFacturaBox.Text))
        .Recordset.Fields("TTotal").Value = ((.Recordset.Fields("Total").Value) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100))) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
        .Recordset.Update
    End With
    
    FFactura2Data.Refresh
    
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
    End Sub

Private Sub Art_Click()
    MantArt.Show modal
    MantArt.OptionBox.Text = "3"
End Sub

Private Sub AsoBoton_Click()
    
    Dim id As Integer
On Error GoTo msgError
    If NPABox.Text <> "" Then
        
        With FFacturaData
            If .Recordset.EOF = False Then
                .Recordset.MoveLast
                id = .Recordset.Fields("Linea").Value
            Else
                id = 0
            End If
        End With
        
        AAlbaranData.Recordset.FindFirst ("NAlbaran=" & NPABox.Text)
        
        If AAlbaranData.Recordset.NoMatch = False Then
            
            AlbaranData.Recordset.FindFirst ("NAlbaran=" & NPABox.Text)
            
            If id <> 0 And CPBox.Text <> AlbaranData.Recordset.Fields("CodigoProveedor").Value Then
                MsgBox ("Albar�n no perteneciente al Proveedor de la factura")
            Else
                CPBox.Text = AlbaranData.Recordset.Fields("CodigoProveedor").Value
                
                While AAlbaranData.Recordset.NoMatch = False
               
                    With FFactura2Data
                                                
                        id = id + 1
                        
                        If AAlbaranData.Recordset.Fields("Facturado").Value = False Then
                            
                            .Recordset.AddNew
                            .Recordset.Fields("Linea").Value = id
                            .Recordset.Fields("NFactura").Value = NFacturaBox.Text
                            .Recordset.Fields("CodigoArticulo").Value = AAlbaranData.Recordset.Fields("CodigoArticulo").Value
                            .Recordset.Fields("Cantidad").Value = AAlbaranData.Recordset.Fields("Cantidad").Value
                            .Recordset.Fields("PrecioUnit").Value = AAlbaranData.Recordset.Fields("PrecioUnit").Value
                            .Recordset.Fields("Descuento").Value = AAlbaranData.Recordset.Fields("Descuento").Value
                            .Recordset.Fields("PParcial").Value = AAlbaranData.Recordset.Fields("PParcial").Value
                            .Recordset.Fields("NPedido").Value = AAlbaranData.Recordset.Fields("NPedido").Value
                            .Recordset.Fields("NAlbaran").Value = NPABox.Text
                            .UpdateRecord
                            
                            AAlbaranData.Recordset.Edit
                            AAlbaranData.Recordset.Fields("Facturado") = True
                            AAlbaranData.Recordset.Update
            
                            AAlbaranData.Recordset.FindNext ("NAlbaran=" & NPABox.Text)

                        End If
        
                    End With
    
               
                Wend
                
                With FFacturaData
                    .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
                    .Refresh
                End With
        
                FFactura2Data.Refresh

                With FacturaData
                    .Recordset.Edit
                    .Recordset.Fields("Total").Value = CaTotalFac(Val(NFacturaBox.Text))
                    .Recordset.Fields("TTotal").Value = ((.Recordset.Fields("Total").Value) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100))) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
                    .Recordset.Update
                End With
            
            End If
        
        End If
                       
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
    
    End Sub

Private Sub BorrarBoton_Click()
    Dim id As Integer, nf As Long, na As Long, np As Long, ca As Long
On Error GoTo msgError
    id = IdBox.Text
    nf = NFacturaBox.Text
     
    With FFactura2Data
        .Recordset.FindFirst ("NFactura=" & nf & " AND Linea=" & id)
        na = .Recordset.Fields("NAlbaran").Value
        ca = .Recordset.Fields("CodigoArticulo").Value
        .Recordset.Delete
        .Refresh
    End With
    
    With AAlbaranData
        .Recordset.FindFirst ("NAlbaran=" & na & " AND CodigoArticulo=" & ca)
        .Recordset.Edit
        .Recordset.Fields("Facturado").Value = False
        .Recordset.Update
    End With
    
    
    With FacturaData
        .Recordset.Edit
        .Recordset.Fields("Total").Value = CaTotalFac(Val(NFacturaBox.Text))
        .Recordset.Fields("TTotal").Value = ((.Recordset.Fields("Total").Value) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100))) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
        .Recordset.Update
    End With
    
    A�adirBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    
    With FFacturaData
        .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
        .Refresh
    End With
    
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
    
    End Sub

Private Sub CPBox_Change()
On Error GoTo msgError
If CPBox.Text <> "" Then
        ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)
        If ProvData.Recordset.NoMatch = True Then
            ProvBox.Caption = "No Encontrado"
        End If
End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
    
    End Sub

Private Sub CPBox_CloseUp()
On Error GoTo msgError
    ProvData.Recordset.FindFirst ("CodigoProveedor=" & CPBox.Text)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub



Private Sub DtoBox_LostFocus()
On Error GoTo msgError
    With FacturaData
        .Recordset.Edit
        .Recordset.Fields("TTotal").Value = ((.Recordset.Fields("Total").Value) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100))) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
        .Recordset.Update
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub eraser_Click()
    Dim Resp
    Resp = MsgBox("�Est� usted seguro de querer borrar la factura?", vbYesNo)
    If Resp = vbYes Then
        FacturaData.Recordset.Delete
        nada = Anterior()
    End If
End Sub

Private Sub ira_Click()
    Set Objeto = FacturaForm.FacturaData
    Set Objeto2 = FacturaForm.FFacturaData
    
    Selct = True
    
    Display(0) = "NFactura"
    DataCarac = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura="
    DataCarac2 = " ORDER BY FF.Linea"
    
    IraForm.Show modal

End Sub

Private Sub IVABox_LostFocus()
On Error GoTo msgError
    With FacturaData
        .Recordset.Edit
        .Recordset.Fields("TTotal").Value = ((.Recordset.Fields("Total").Value) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100))) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
        .Recordset.Update
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub ProvCombo_Click(Area As Integer)
On Error GoTo msgError
    If ProvCombo.Text <> "" Then
        ProvData.Recordset.FindFirst ("Nombre='" & ProvCombo.Text & "'")
        CPBox.Text = ProvData.Recordset.Fields("CodigoProveedor").Value
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Function Anterior()
    
    Dim nf As Integer
On Error GoTo msgError
    With FacturaData
        .Recordset.MovePrevious
        If .Recordset.BOF = True Then
            .Recordset.MoveNext
        End If
        nf = .Recordset.Fields("NFactura").Value
    End With
                
    With FFacturaData
        .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
        .Refresh
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Siguiente()
   
    Dim nf As Integer
On Error GoTo msgError
    With FacturaData
        .Recordset.MoveNext
        If .Recordset.EOF = True Then
            .Recordset.MovePrevious
        End If
        nf = .Recordset.Fields("NFactura").Value
    End With
    
    With FFacturaData
        .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
        .Refresh
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Sub lista_Click()
    Dim nada As Integer
    nada = LisFact()
End Sub

Private Sub prim_Click()
    Dim nada As Integer
    nada = Primero()
    
End Sub

Private Sub Prov_Click()
    MantProv.Show modal
    ProvData.Refresh
End Sub

Private Sub Sal_Click()
    Unload Me
    
End Sub

Private Sub sig_Click()
    Dim nada As Integer
    nada = Siguiente()
End Sub

Private Sub SSDBGrid1_DblClick()
    
    Dim id As Integer
    
    With FFacturaData
        id = .Recordset.Fields("Linea").Value
        IdBox.Text = id
        CABox.Caption = .Recordset.Fields("CodigoArticulo").Value
        CanBox.Text = .Recordset.Fields("Cantidad").Value
        If .Recordset.Fields("PrecioUnit").Value <> 0 Then
            PrecBox.Text = .Recordset.Fields("PrecioUnit").Value
        Else
            PrecBox.Text = "0"
        End If
        
        If .Recordset.Fields("Descuento").Value <> 0 Then
            DescuentoBox.Text = .Recordset.Fields("Descuento").Value
        Else
            DescuentoBox.Text = "0"
        End If
        
        DescBox.Text = .Recordset.Fields("Descripcion").Value
        
    End With
    
    A�adirBoton.Visible = False
    BorrarBoton.Visible = True
    ActualizarBoton.Visible = True
    SSDBGrid1.Enabled = False
        
End Sub

Private Sub SSDBGrid2_DblClick()
    NPABox.Text = AlbaranData.Recordset.Fields("NAlbaran").Value
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtraFactura()
    Case "AArticulo"
        MantArt.Show modal
        MantArt.OptionBox.Text = "3"
    Case "Lista"
        nada = LisFact()
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar la factura?", vbYesNo)
        If nada = vbYes Then
            FacturaData.Recordset.Delete
            nada = Anterior()
        End If
    Case "ulti"
        nada = Ultimo()
    Case "prim"
        nada = Primero()
        
    End Select
    

End Sub

Private Function OtraFactura() As Integer

    Dim num As Integer
On Error GoTo msgError
    num = UltFactura()
    
    With FacturaData
        .Recordset.AddNew
        .Recordset.Fields("NFactura").Value = num + 1
        .Recordset.Fields("FechaFactura").Value = Date
    End With

    NFacturaBox.Text = num + 1
    CPBox.Text = 123
    IVABox.Text = 16
    DtoBox.Text = 0
            
    FacturaData.UpdateRecord
    FacturaData.Recordset.MoveLast
    
    With FFacturaData
        .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
        .Refresh
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical

End Function



Private Sub DescBox_Click(Area As Integer)
On Error GoTo msgError
    If DescBox.Text <> "" Then
        ArticulosData.Recordset.FindFirst ("Descripcion='" & DescBox.Text & "'")
        CABox.Caption = ArticulosData.Recordset.Fields("CodigoArticulo").Value
    Else
        CABox.Caption = ""
    End If
    
    If ArticulosData.Recordset.NoMatch = True Then
        CABox.Caption = ""
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Function ClearFields()

    CABox.Caption = ""
    CanBox.Text = ""
    PrecBox.Text = ""
    DescuentoBox.Text = ""
    
End Function

Private Sub Ult_Click()
    Dim nada As Integer
    nada = Ultimo()
End Sub

Private Function LisFact()
    Dim nada As Long
    nada = NuevaBusq()
    Display(0) = "NFactura"
    Display(1) = "CodigoProveedor"
    Display(2) = "FechaFactura"
    Display(3) = "NFacturaP"
    Display(4) = ""
    Display(5) = ""
    Display(6) = ""
    Display(7) = ""
    
    campos(4) = ""
    campos(5) = ""
    campos(6) = ""
    campos(7) = ""
       
    TypeCampos(0) = 1
    TypeCampos(1) = 3
    TypeCampos(2) = 2
    TypeCampos(3) = 1
    TypeCampos(4) = 0
    TypeCampos(5) = 0
    TypeCampos(6) = 0
    TypeCampos(7) = 0
    
    For nada = 0 To 3
        campos(nada) = "F." & Display(nada)
    Next
        
    Lists(1, 0) = "CodigoProveedor, Nombre"
    Lists(1, 1) = "CodigoProveedor"
    Lists(1, 2) = "Proveedores ORDER BY Nombre"
        
    FRO = "Factura F, Proveedores PR"
    SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3) & ",PR.Nombre,F.TTotal"
    WHER = "F.CodigoProveedor=PR.CodigoProveedor "
    ORD = "F.NFactura"
    
    Set Objeto = FacturaForm.FacturaData
    Set Objeto2 = FacturaForm.FFacturaData
    DataCarac = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura="
    DataCarac2 = " ORDER BY FF.Linea"
    
    Selct = True
    
    nada = Listado()
                   

End Function


Private Function Primero()
    
    Dim nf As Integer
On Error GoTo msgError
    FacturaData.Recordset.MoveFirst
    nf = FacturaData.Recordset.Fields("NFactura").Value
                    
    With FFacturaData
        .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
        .Refresh
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Ultimo()
    
    Dim nf As Integer
On Error GoTo msgError
    FacturaData.Recordset.MoveLast
    nf = FacturaData.Recordset.Fields("NFactura").Value
                    
    With FFacturaData
        .RecordSource = "SELECT FF.*,AR.Descripcion FROM FFactura FF, Articulos AR WHERE FF.CodigoArticulo=AR.CodigoArticulo AND FF.NFactura=" & NFacturaBox.Text & " ORDER BY FF.Linea"
        .Refresh
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

