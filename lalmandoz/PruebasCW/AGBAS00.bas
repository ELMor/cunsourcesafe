Attribute VB_Name = "General"
Option Explicit
Option Base 1

Public objCW As clsCW       ' referencia al objeto CodeWizard
Public objApp As clsCWApp
Public objPipe As clsCWPipeLine
Public objGen As clsCWGen
Public objEnv As clsCWEnvironment
Public objError As clsCWError
Public objMouse As clsCWMouse
Public objSecurity As clsCWSecurity

'Variable que contiene el c�digo de motivo de incidencia
Public intCodMotInci As Integer
'Variable para saber si hay que hacer rollack
Public blnRollback As Boolean
'Variable que almacena el nuevo c�digo de incidencia
Public vntNuevoCod As Variant
' Variable que nos indicara si han cancelado la incidencia
Public blnActive As Boolean
' Variable que pasar� la where para obtener los perfiles de
' los recursos seleccionados
Public strWherePerfiles As String
Public strWherePerfInicial As String
'Variable que guardara el c�digo de recurso actual
Public lngCodRecurso As Long
'Variable que guardara el c�digo de perfil actual
Public lngCodPerfil As Long
'Variable que guardara el c�digo de franja actual
Public lngCodFranja As Long
'Variable que indica si se ha de refrescar el cursor
Public blnRefrescar As Boolean
'Indicador de tipo de restriccion por recurso oo franja
Public intTipoRest As Integer
Public lngCodTipRec As Long
Public blnHideFrmInciden As Boolean
Public blnAddMode As Boolean
Public vntCod As Variant

Public vntWhereRecursos(2) As Variant

Public Const agRestPorRecurso = 1
Public Const agRestPorFranja = 2
Public Const agRestPorFase = 3
Public Const agCitada = "1"
Public Const agPdteRecitar = "4"
Public Const agInclusion = 0
Public Const agExclusion = 1

Public Const ciIntervaloCitaMM  As Integer = 15

Public Const agHisRecursos          As String = "AG0013"
Public Const agHisPerVigCal         As String = "AG0011"
Public Const agHisDiaEsp            As String = "AG0012"
Public Const agHisPerVigFra         As String = "AG0015"
Public Const agHisPerfiles          As String = "AG0014"
Public Const agHisFranjas           As String = "AG0016"
Public Const agPerfiles             As String = "AG1006"
Public Const agFranjas              As String = "AG1007"
Public Const agRestricciones        As String = "AG1005"
Public Const agTipoRest             As String = "AG1001"
Public Const agTipoRecurso          As String = "AG1002"
Public Const agCalendarios          As String = "AG1003"






Public Function InitApp()
  InitApp = False
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Set objMouse = objCW.objMouse
  Set objSecurity = objCW.objSecurity
  InitApp = True
  Exit Function
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Function

Public Sub ExitApp()
  Set objCW = Nothing
  'End
End Sub

'******************************************************************
'Funci�n que devuelve el siguiente n� de c�digo al �ltimo
'c�digo asignado.
'Tiene como par�metro de entrada una sentencia Sql que servira
'para crear el cursor
'******************************************************************

Public Function GetNewCode(strSql As String) As Variant
 'Creaci�n de variables recordset
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  
 'Apertura del cursor
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

 'Establezco el n� de registros a recuperar a 1
  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
 'Devuelvo el nuevo n� de c�digo
  If rdoCodigo.RowCount = 0 Then
    GetNewCode = 1
  Else
    If IsNull(rdoCodigo(0)) Then
      GetNewCode = 1
    Else
      GetNewCode = rdoCodigo(0) + 1
    End If
  End If
 
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

Public Function treeGetLevel(ByVal nodNode As ComctlLib.Node) As Integer
  Dim intLevel As Integer
  Dim nodDummy As ComctlLib.Node
  
  On Error Resume Next
  Set nodDummy = nodNode
  Do While Err.Number = 0
    Set nodDummy = nodDummy.Parent
    intLevel = intLevel + 1
  Loop
  intLevel = intLevel - 1
  Set nodDummy = Nothing
  treeGetLevel = intLevel
End Function

'Procedimiento que carga el TreeView con los nodos del recurso
'o de la franja actual

Public Sub CargarTreeView(intTipoRestriccion As Integer, frmForm As Form, intIndex As Integer, lngRecurso As Long, Optional lngPerfil As Long, Optional lngFranja As Long)
  Dim rdoNodos As rdoResultset
  Dim rdoTipoRes As rdoResultset
  Dim rdoDescDesde As rdoResultset
  Dim nodNodo As Node
  Dim strSql As String
  Dim strKey As String
  Dim strNodeText As String
  Dim strCodCols As String
  Dim intPosicion As Integer
  Dim strPadre As String
  
  On Error GoTo Error
  With frmForm
  .TreeView1(intIndex).LabelEdit = tvwManual
  .TreeView1(intIndex).ImageList = objApp.imlImageList
  If intIndex = 0 Then
    Set nodNodo = .TreeView1(intIndex).Nodes.Add(, , "Root", "JERARQUIA DE RESTRICCIONES")
  Else
    Set nodNodo = .TreeView1(intIndex).Nodes.Add(, , "Root", "JERARQUIA DE RESTRICCIONES DEL RECURSO")
  End If
  nodNodo.Selected = True
  nodNodo.Sorted = False
  nodNodo.Image = "i20"
  
  If intTipoRestriccion = agRestPorRecurso Then
    strSql = "SELECT AG11CODRECURSO, AG13NUMRESTREC, AG13NIVELSUPER, AG13VALDESDERES, AG13VALHASTARES, AG16CODTIPREST, AG13INDINCEXCL FROM " _
         & objEnv.GetValue("Database") & "AG1300 WHERE AG11CODRECURSO=" & lngRecurso _
         & " ORDER BY AG13NIVELSUPER, AG16CODTIPREST,AG13NUMRESTREC"
  End If
  If intTipoRestriccion = agRestPorFranja Or intTipoRestriccion = agRestPorFranja Then
      strSql = "SELECT AG11CODRECURSO, AG12NUMRESFRJA, AG12NIVELSUPER, AG12VALDESDERES, " _
         & " AG12VALHASTARES, AG16CODTIPREST, AG12INDINCEXCL, AG07CODPERFIL, AG04CODFRANJA FROM " _
         & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=" & lngRecurso _
         & " AND AG07CODPERFIL=" & lngPerfil _
         & " AND AG04CODFRANJA=" & lngFranja _
         & " ORDER BY AG12NIVELSUPER, AG16CODTIPREST,AG12NUMRESFRJA"
    
  End If
  
  'Abro el cursor
  Set rdoNodos = objApp.rdoConnect.OpenResultset(strSql)
  'Recorro el cursor y voy creando el arbol mediante los campos
  'Nivel Superior (el padre) y el N�mero de secuencia (el Key)
  While Not rdoNodos.EOF
     strPadre = "N" & rdoNodos(2) '"NIVELSUPER"
     strKey = "N" & rdoNodos(1) '"NUM SEC"
     
        'Abro este cursor para obtener la descripci�n del tipo
     'de restricci�n
     strSql = "SELECT AG16DESTIPREST, AG16TABLAVALOR, AG16COLCODVALO, AG16COLDESVALO FROM " _
      & objEnv.GetValue("Database") & "AG1600 WHERE AG16CODTIPREST=" & rdoNodos("AG16CODTIPREST")
     Set rdoTipoRes = objApp.rdoConnect.OpenResultset(strSql)
     'Compruebo si el padre es el Nodo Raiz
     If strPadre = "N0" Then
       strPadre = "Root"
     End If
     If rdoTipoRes("AG16TABLAVALOR") <> "" Then
        strCodCols = rdoTipoRes("AG16COLCODVALO")
        strCodCols = objGen.ReplaceStr(strCodCols, ",", "||", 0)
        strSql = "SELECT " & strCodCols & ", " & rdoTipoRes("AG16COLDESVALO") & " FROM " & rdoTipoRes("AG16TABLAVALOR") & " WHERE " & rdoTipoRes("AG16COLDESVALO") & "='" & " '"
        Set rdoDescDesde = objApp.rdoConnect.OpenResultset(strSql)
        If rdoDescDesde(0).Type = rdTypeVARCHAR Or rdoDescDesde(0).Type = rdTypeCHAR Then
          strSql = "SELECT " & rdoTipoRes("AG16COLDESVALO") & " FROM " & rdoTipoRes("AG16TABLAVALOR") & " WHERE " & strCodCols & "='" & rdoNodos(3) & "'"
        Else
          strSql = "SELECT " & rdoTipoRes("AG16COLDESVALO") & " FROM " & rdoTipoRes("AG16TABLAVALOR") & " WHERE " & strCodCols & "=" & Val(rdoNodos(3))
        End If
        Set rdoDescDesde = objApp.rdoConnect.OpenResultset(strSql)
        If Not rdoDescDesde.EOF Then
          strNodeText = rdoTipoRes(0) & " ( " & rdoNodos(3) & " - " & Trim(rdoNodos(4) & rdoDescDesde(0) & "") & " )"
        Else
          strNodeText = rdoTipoRes(0) & " ( " & rdoNodos(3) & " - )"
        End If

     Else
       'Abro este cursor para obtener la descripci�n del campo
       '"Desde" de la tabla de valores por tipo de restricci�n
       strSql = "SELECT AG17DESVALTIRE FROM " _
          & objEnv.GetValue("Database") & "AG1700" _
          & " WHERE AG16CODTIPREST=" & rdoNodos("AG16CODTIPREST") _
          & " AND AG17VALDESDETRE='" & rdoNodos(3) & "'"
       Set rdoDescDesde = objApp.rdoConnect.OpenResultset(strSql)
       'Obtengo el texto que va a ir en el nodo
       If Not rdoDescDesde.EOF Then
         strNodeText = rdoTipoRes(0) & " ( " & rdoNodos(3) & " - " & Trim(rdoNodos(4) & rdoDescDesde(0) & "") & " )"
       Else
         strNodeText = rdoTipoRes(0) & " ( " & rdoNodos(3) & " - " & rdoNodos(4) & " )"
       End If
     
     End If
     'Creo el nodo
     Set nodNodo = .TreeView1(intIndex).Nodes.Add(strPadre, tvwChild, strKey, strNodeText)
     If Val(rdoNodos(6)) = 1 Then
        nodNodo.Image = "i28"
     Else
        nodNodo.Image = "i27"
     
     End If

Continuar:
     nodNodo.EnsureVisible
    ' rdoDescDesde.Close
     rdoNodos.MoveNext
  Wend
  End With
  Set rdoTipoRes = Nothing
  Set rdoDescDesde = Nothing
  rdoNodos.Close
  Set rdoNodos = Nothing
  Exit Sub
Error:
  If Err.Number = 40002 Then
     Call objError.SetError(cwCodeMsg, "Nombre de columna incorrecta en tabla asociada a la restricci�n " & rdoTipoRes(0))
     Call objError.Raise
    'MsgBox "Nombre de columna incorrecta en tabla asociada a la restricci�n " & rdoTipoRes(0), vbExclamation, "AGENDA. Restricciones"
    strNodeText = rdoTipoRes(0) & " ( " & rdoNodos(3) & " - " & rdoNodos(4) & " )"
    Set nodNodo = frmForm.TreeView1(intIndex).Nodes.Add(strPadre, tvwChild, strKey, strNodeText)
    GoTo Continuar
  Else
   Resume Next
  End If
End Sub

'Funci�n que genera un nuevo c�digo de incidencia.
'Tiene dos par�metros de entrada :
'  -intCodMotInci -> C�digo del Motivo de Incidencia
'  -blnHideFrmInciden -> Si es True no saca la ventana de incidencias
'La funci�n devuelve:
'   -El nuevo numero de incidencia ( si todo ha ido bien )
'   -Si el intCodMotInci pasado no existe devuelve -1
'   -Si se cancela la operaci�n o intCodMotInci no est� activo
'    devuelve 0
Public Function AddInciden(intCodInci As Integer, blnHideFrmInciden As Boolean) As Variant
  ' Variable resultset y query para la descripci�n del motivo
  Dim rdoMotivoInci As rdoResultset
  Dim rdoQueryMotivo As rdoQuery
  Dim rdoTiposIncidencia As rdoResultset
  
' Variable para la sentencia Sql
  Dim strSql As String
  
  Load frmIncidencias
' Inicializamos blnRollback a false
  blnRollback = False
  blnActive = True
' Abrimos el cursor para obtener la descripci�n del motivo
  
  strSql = "select AG06DESMOTINCI,AG06INDINCDISM, AG06FECACTIVA from AG0600 " _
         & "where AG06CODMOTINCI = " & intCodInci
  
  Set rdoQueryMotivo = objApp.rdoConnect.CreateQuery("", strSql)
 'Establecemos el tama�o del cursor a 1
  rdoQueryMotivo.MaxRows = 1
  Set rdoMotivoInci = rdoQueryMotivo.OpenResultset(rdOpenKeyset, _
                                                rdConcurReadOnly)
  
' Miramos si existe el c�digo del motivo de incidencia
  If rdoMotivoInci.RowCount = 0 Then
    'No existe el codigo de motivo incidencia
     Call objError.SetError(cwCodeMsg, "No existe el c�digo de motivo incidencia N�" & intCodMotInci)
     Call objError.Raise
     
    ' MsgBox "No existe el c�digo de motivo incidencia N�" & intCodMotInci, vbExclamation, "AGENDA. Incidencias"
     blnRollback = True
     AddInciden = -1
     
  Else
    'Si la fecha activa del c�digo de incidencia es
    'frmincidenciasnor que la actual visualizamos el form
    If CDate(rdoMotivoInci("AG06FECACTIVA")) < CDate(Format(objGen.GetDBDateTime, "dd/mm/yyyy")) Then
      With frmIncidencias
      If blnHideFrmInciden Then
        .cboSSDBCombo1(0).Text = rdoMotivoInci("AG06DESMOTINCI")
        Call .A�adir_Incidencia(intCodInci)
        blnHideFrmInciden = False
      Else
        'Asignamos la descripci�n del motivo de incidencia
        .ConfigureWindow (Val(rdoMotivoInci("AG06INDINCDISM")))
        If Val(rdoMotivoInci("AG06INDINCDISM")) = 1 Then
           .cboSSDBCombo1(0).Text = rdoMotivoInci("AG06DESMOTINCI")
        Else
          strSql = "SELECT AG06CODMOTINCI, AG06DESMOTINCI,AG06INDINCDISM FROM AG0600 WHERE AG06INDINCDISM=" & Val(rdoMotivoInci("AG06INDINCDISM")) & " AND AG06FECACTIVA < " & objGen.ValueToSQL(objGen.GetDBDateTime, cwDate)
          Set rdoTiposIncidencia = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly, rdConcurReadOnly)
          Do While Not rdoTiposIncidencia.EOF
            Call .cboSSDBCombo1(0).AddItem(rdoTiposIncidencia(0) & "," & rdoTiposIncidencia(1))
            rdoTiposIncidencia.MoveNext
          Loop
          .cboSSDBCombo1(0).Value = intCodInci
          rdoTiposIncidencia.Close
        End If
        intCodMotInci = intCodInci
        .Show (vbModal)
      
        .MousePointer = vbDefault
      
      End If
      End With
      If blnRollback Then
        AddInciden = 0 ' Han pulsado cancelar
      Else
        AddInciden = vntNuevoCod
      End If
    Else
     'Si la fecha es mayor cancelamos la incidencia
     'pero no realizamos rollback
      AddInciden = 0
      blnActive = False
      
    End If
  End If
  Unload frmIncidencias
  Set frmIncidencias = Nothing
End Function

'Procedimiento que muestra los datos de un numero de incidencia

Public Sub ShowInciden(lngNumInciden As Long)
  Dim strSql As String
  Dim cllCodInciden As New Collection
  Dim cllIncidencia As New Collection
  
  strSql = "SELECT AG06CODMOTINCI,AG05DESALCANCE,AG05DESSOLUCIO FROM AG0500 WHERE AG05NUMINCIDEN=" & lngNumInciden
  Set cllIncidencia = GetTableColumn(strSql)
  If cllIncidencia(1) <> "" Then
    strSql = "SELECT AG06DESMOTINCI,AG06INDINCDISM FROM AG0600 WHERE AG06CODMOTINCI=" & Val(cllIncidencia(1))
    Set cllCodInciden = GetTableColumn(strSql)
    If cllCodInciden(1) = "" Then
      'No existe el codigo de motivo incidencia
      Call objError.SetError(cwCodeMsg, "No existe el c�digo de motivo incidencia N�" & cllCodInciden(0))
      Call objError.Raise
    Else
      Load frmIncidencias
      With frmIncidencias
        Call .ConfigureWindow(Val(cllCodInciden(2)))
        .cboSSDBCombo1(0).Text = cllCodInciden(1)
        .cboSSDBCombo1(0).Enabled = False
        .txtText1(1) = cllIncidencia(2)
        If Val(cllCodInciden(2)) = 1 Then
          .txtText1(2) = cllIncidencia(3)
        End If
        .txtText1(1).Locked = True
        .txtText1(2).Locked = True
        .cmdCommand1(0).Enabled = False
        .cmdCommand1(1).Enabled = False
        .Show (vbModal)
      End With
      Unload frmIncidencias
      Set frmIncidencias = Nothing
    End If
  Else
    Call objError.SetError(cwCodeMsg, "No existe el N�mero de Incidencia")
    Call objError.Raise
  
  End If
End Sub
'Devuelve una colecci�n de valores con las columnas de una tabla
'para la primera fila que cumple la condicion de strSql
Public Function GetTableColumn(strSql As String) As Collection
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  Dim intCol As Integer
  Dim cllValues As New Collection
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSql)

  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
  If rdoCodigo.RowCount = 0 Then
    For intCol = 0 To rdoCodigo.rdoColumns.Count - 1
       Call cllValues.Add("", rdoCodigo.rdoColumns(intCol).Name)
    Next
    
    'Set GetTableColumn = Nothing
  Else
    rdoCodigo.MoveFirst
    For intCol = 0 To rdoCodigo.rdoColumns.Count - 1
       If Not IsNull(rdoCodigo.rdoColumns(intCol).Value) Then
          Call cllValues.Add(rdoCodigo.rdoColumns(intCol).Value, rdoCodigo.rdoColumns(intCol).Name)
       Else
          Call cllValues.Add("", rdoCodigo.rdoColumns(intCol).Name)
       End If
    Next
  End If
 Set GetTableColumn = cllValues
 Set cllValues = Nothing
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

Public Sub Main()
' no hace nada
End Sub
'Funci�n que descompone en campos individuales una cadena de campos
'separados por || o por comas y devuelve una colecci�n con los
'campos separados
Public Function SearchFields(strFields As String) As Collection
  Dim cllColumns As New Collection
  Dim intContador As Integer
  Dim strColField As String
  Dim intTama�o As Integer
  
  On Error Resume Next
  strFields = UCase(strFields)
  intTama�o = Len(strFields)
  Do While intTama�o > 0
    For intContador = 1 To Len(strFields)
      If Mid(strFields, intContador, 1) = "," Then
         strColField = Mid(strFields, 1, intContador - 1)
         cllColumns.Add strColField
         strFields = Mid(strFields, intContador + 1)
         intTama�o = intTama�o - 1
         Exit For
      ElseIf Mid(strFields, intContador, 1) = "|" Then
        strColField = Mid(strFields, 1, intContador - 1)
        cllColumns.Add strColField
        strFields = Mid(strFields, intContador + 2)
        intTama�o = intTama�o - 2
        Exit For
      End If
      intTama�o = intTama�o - 1
      
    Next

  Loop
  strColField = Mid(strFields, 1, intContador - 1)
  cllColumns.Add strColField
  Set SearchFields = cllColumns
End Function


Public Sub ReportHistoric(strHistoric As String)
  Dim objReport As New clsCWReport
  Dim strReportDesc As String
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
 
  Select Case strHistoric
    Case agHisRecursos
      strReportDesc = "Hist�rico de Recursos"
    Case agHisPerVigCal
      strReportDesc = "Hist�rico de Per�odos de Vigencia de los Calendarios"
    Case agHisDiaEsp
      strReportDesc = "Hist�rico de D�as Especiales"
    Case agHisPerVigFra
      strReportDesc = "Hist�rico de Per�odos de Vigencia de las Franjas"
    Case agHisPerfiles
      strReportDesc = "Hist�rico de Perfiles"
    Case agHisFranjas
      strReportDesc = "Hist�rico de Franjas"
  End Select
  If strReportDesc <> "" Then
    Call objReport.AddReport(strHistoric, strReportDesc)
    
    With objReport.PrinterDialog(False, strHistoric)
      intReport = .Selected
      If intReport > 0 Then
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
End Sub
'Funci�n que devuelve el d�a de la semana
'Tiene como par�metro de entrada un valor integer seg�n el d�a
'de la semana ( 0->Lunes .... 6->Domingo )
Public Function DiaSemana(intDia As Integer) As String
  Select Case intDia
    Case 0
      DiaSemana = "Lunes"
    Case 1
      DiaSemana = "Martes"
    Case 2
      DiaSemana = "Mi�rcoles"
    Case 3
      DiaSemana = "Jueves"
    Case 4
      DiaSemana = "Viernes"
    Case 5
      DiaSemana = "Sabado"
    Case 6
      DiaSemana = "Domingo"
    Case Else
      DiaSemana = " "
  End Select
End Function

