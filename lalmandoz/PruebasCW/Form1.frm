VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   5445
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7110
   LinkTopic       =   "Form1"
   ScaleHeight     =   5445
   ScaleWidth      =   7110
   StartUpPosition =   3  'Windows Default
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   960
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3000
      Top             =   2040
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   960
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const cwMsgMainExit  As String = "�Desea realmente salir del sistema?"


Dim mstrCaption   As String
Dim mblnMoving    As Boolean
Dim mobjSelected  As Object
Dim mblnTree      As Boolean
Dim blnShowed     As Boolean

Private Sub Form_Load()
  Dim strEntorno  As String
  Dim strUser     As String
  Dim strPassword As String
  Dim picImage    As IImage
  Dim i As Integer
 
  If InitApp Then
   ' Call FormResize
    With objApp
      .blnReg = False
      Call .Register(App, Screen)
      Set .frmFormMain = Me
      Set .imlImageList = ImageList1
'      Set .crCrystal = crCrystalReport
      Set .dlgCommon = CommonDialog1
    
      .blnUseRegistry = True
      .blnAskPassword = False
      .strUserName = objSecurity.GetDataBaseUser
      .strPassword = objSecurity.GetDataBasePassword
    End With

    strEntorno = "Entorno de Explotaci�n"
    Call objEnv.AddEnv(strEntorno)
    Call objEnv.AddValue(strEntorno, "DataBase", "")

'    For Each picImage In imlMainSmall.ListImages
'      Call imlMainBig.ListImages.Add(, picImage.Key, picImage.Picture)
'    Next
  
    mstrCaption = Me.Caption
  
    If Not objApp.CreateInfo Then
      Unload Me
    Else
'      If Not objSecurity.Identify Then
'        Unload Me
'      Else
'        blnShowed = True
'        Me.Caption = mstrCaption & ", " & objSecurity.strFullName
'   '    Call objSecurity.LoadInfo
''        Call objSecurity.CreateTreeView(tvwItems, lvwItems, imlMainSmall, imlMainBig)
        frmTipoRecu.Show (vbModal)
        

'      End If
    End If
  Else
    Unload Me
  End If
  objApp.blnReg = True
  
'  objCW.SetClockEnable (False)
  
End Sub


