VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmTipoRecu1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Mantenimiento Tipos de Recurso"
   ClientHeight    =   6705
   ClientLeft      =   1170
   ClientTop       =   1410
   ClientWidth     =   10215
   ControlBox      =   0   'False
   HelpContextID   =   1
   Icon            =   "Raulold.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   6705
   ScaleWidth      =   10215
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipo Restricci�n"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3225
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Tag             =   "Mantenimiento Tipos de Restricci�n"
      Top             =   3015
      Width           =   9975
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2700
         Index           =   1
         Left            =   135
         TabIndex        =   3
         Top             =   360
         Width           =   9690
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   17092
         _ExtentY        =   4762
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipo Recurso"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2490
      Index           =   0
      Left            =   90
      TabIndex        =   0
      Tag             =   "Mantenimiento Tipos de Recurso"
      Top             =   465
      Width           =   10005
      Begin VB.TextBox txtText1 
         Height          =   855
         Index           =   0
         Left            =   7440
         TabIndex        =   6
         Top             =   960
         Width           =   2055
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2055
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   9765
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   17224
         _ExtentY        =   3625
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   6420
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin Crystal.CrystalReport CRCrystalReport1 
      Left            =   2760
      Top             =   2880
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog DLGCommonDialog1 
      Left            =   1680
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin ComctlLib.ImageList IMLImageList1 
      Left            =   0
      Top             =   240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmTipoRecu1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim lngCodTipRest As Long

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim intMode As Integer
'  InitApp
'With objApp
'    Call .Register(App, Screen)
'    Set .frmFormMain = Me
'    Set .imlImageList = IMLImageList1
'    Set .crCrystal = CRCrystalReport1
'    Set .dlgCommon = DLGCommonDialog1
'    .objUserColor.lngReadOnly = &HC0C0C0
'    .objUserColor.lngMandatory = &HFFFF00
'  End With
'
'  With objApp
'    .blnUseRegistry = True
'    .strUserName = "cun"
'    .strPassword = "tuy"
'    .strDataSource = "oracle73"
'    .blnAskPassword = False
'  End With
'
'  If Not objApp.CreateInfo Then
'    MsgBox ("Int�ntalo de nuevo")
'  End If

  
'  Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
 
 
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
'Introducir la documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Tipos de Recursos"
    .cwDAT = "09-07-97"
    .cwAUT = "Jokin"
    
    .cwDES = "Esta ventana permite mantener los diferentes tipos de recursos de la CUN"
    .cwDES = "Para cada tipo de recurso se introducen sus Tipos de Restricci�n "
    .cwDES = "Se valida que si se introduce Fecha Fin Vigencia sea mayor que la Inicial"

    .cwUPD = "09-07-97 - Jokin - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
' Definici�n tabla padre
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    
    Set .grdGrid = grdDBGrid1(0)
  '  .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1400"
    .blnAskPrimary = False
'    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    Call .FormAddOrderField("AG14CODTIPRECU", cwAscending)

'Asignaci�n del nombre del form
    .strName = "Tipo Recurso"

' Definici�n del impreso
'    Call .objPrinter.Add("AG0005", "Listado 1 de Tipos de Recursos")

' Creaci�n de los filtros de busqueda del padre
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Tabla Tipo Recurso")
    Call .FormAddFilterWhere(strKey, "AG14DESTIPRECU", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AG14INDPLANIFI", "Indicador Planificable", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "AG14DESTIPRECU", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "AG14INDPLANIFI", "Planificable")
  End With

' Definici�n datos tabla hijo
  With objMultiInfo
    .strName = "TipoRestriccion"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .blnHasMaint = True
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
'     Call .FormAddRelation("AG14CODTIPRECU", grdDBGrid1(0).Columns(4))
    .strDataBase = objEnv.GetValue("DataBase")
    .strTable = "AG1500"
    .intAllowance = cwAllowAdd + cwAllowDelete

    Call .FormAddOrderField("AG14CODTIPRECU", cwAscending)

 ' Creaci�n de los filtros del hijo
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Tabla de Valores Restricci�n")
    Call .FormAddFilterWhere(strKey, "AG16CODTIPREST", "Tipo Restricci�n", cwNumeric)

    Call .FormAddFilterOrder(strKey, "AG16CODTIPREST", "Tipo Restricci�n")
  End With
'
' Columnas en el grid del hijo
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMasterInfo, "Tipo Recurso", "AG14CODTIPRECU")
    Call .GridAddColumn(objMasterInfo, "<Descripci�n Recurso       ", "AG14DESTIPRECU")
    Call .GridAddColumn(objMultiInfo, "Tipo Recurso", "AG14CODTIPRECU")
    Call .GridAddColumn(objMultiInfo, "Tipo Restricci�n                         ", "AG16CODTIPREST")
    Call .GridAddColumn(objMultiInfo, "C�d.Restricci�n", "")
'
    Call .FormCreateInfo(objMasterInfo)
  
' Campos que intervienen en busquedas del maestro y del detalle
    
    
'    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True

  ' Propiedades del campo clave  para asignaci�n autom�tica
   

  'Camposque no van en el grid
   
' Sql de un campo del grid del hijo
'    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).strSql = "SELECT AG16CODTIPREST, AG16DESTIPREST FROM " & objEnv.GetValue("DataBase") & "AG1600 ORDER BY AG16DESTIPREST"
'    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
'
'    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = False

     ' Definici�n de controles relacionados entre s�
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "AG16CODTIPREST", "SELECT AG16CODTIPREST, AG16DESTIPREST FROM " & objEnv.GetValue("DataBase") & "AG1600 WHERE AG16CODTIPREST = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(5), "AG16CODTIPREST")

    Call objMultiInfo.FormAddRelation("AG14CODTIPRECU", grdDBGrid1(0).Columns.Item(3))
     'Valor por defecto de la fecha inicio vigencia
    Call .WinRegister
    Call .WinStabilize
  End With

' la columna relacionada con la tabla padre no se visualiza
' Se controla su valor en los eventos del grid
'  grdDBGrid1(1).Columns(3).Visible = False
'  grdDBGrid1(1).Columns(5).Visible = False
  
'  Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    Me.MousePointer = vbHourglass
  
   'Versi�n EXE
  '  Load frmTipRest
  '  Call frmTipRest.Show(vbModal)
  '  Unload frmTipRest
  '  Set frmTipRest = Nothing
   
   'Versi�n DLL
     If grdDBGrid1(1).Columns(4).Value = "" Then
       blnAddMode = True
     Else
      If objWinInfo.intWinStatus = cwModeMultiLineEdit Then
        vntCod = GetTableColumn("SELECT AG16CODTIPREST FROM AG1600 WHERE AG16DESTIPREST=" & objGen.ValueToSQL(grdDBGrid1(1).Columns(4).Value, cwString))("AG16CODTIPREST")
      End If
     End If
     
     Call objSecurity.LaunchProcess(agTipoRest)
     blnAddMode = False
    Me.MousePointer = vbDefault
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  If strFormName = "TipoRestriccion" Then
    grdDBGrid1(1).Columns(3).Value = Val(txtText1(1))
  End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'******************************************************************
' Compruebosi ha habido error para
' validar los cambios o anularlos
'******************************************************************
  If blnError = True Then
    Call objApp.rdoConnect.RollbackTrans
  Else
    Call objApp.rdoConnect.CommitTrans
  End If

End Sub

Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
  Dim strSql As String
  Dim rdoQueryRecurso As rdoQuery
  Dim rdoRecurso As rdoResultset
 ' Compruebo que no hay ningun registro de este tipo
  If strFormName = "Tipo Recurso" Then
  
    strSql = "SELECT AG11CODRECURSO, AG11DESRECURSO FROM AG1100 WHERE AG14CODTIPRECU=" _
            & Val(txtText1(1))

    Set rdoQueryRecurso = objApp.rdoConnect.CreateQuery("", strSql)
    rdoQueryRecurso.MaxRows = 1
    Set rdoRecurso = rdoQueryRecurso.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
 
  ' Si no hay calendario principal saco un mensaje de error
    If objGen.GetRowCount(rdoRecurso) > 0 Then
      Call objError.SetError(cwCodeMsg, "Este Tipo de Recurso est� siendo utilizado por el Recurso: " & rdoRecurso(1))
      Call objError.Raise
      blnCancel = True
    End If
  End If
  
  If strFormName = "TipoRestriccion" Then
   'miro si este tipo de restricci�n esta siendo utilzado
    If objWinInfo.intWinStatus = cwModeMultiLineEdit Then
     'Busco en Restricciones por Recurso
      strSql = "SELECT DISTINCT AG14CODTIPRECU,AG11CODRECURSO FROM AG1100 " _
         & " WHERE AG11CODRECURSO IN " _
         & " ( SELECT AG11CODRECURSO FROM AG1300 " _
            & " WHERE AG16CODTIPREST=" & grdDBGrid1(1).Columns(5).Value & " )"
      Set rdoRecurso = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
      Do While Not rdoRecurso.EOF
        If rdoRecurso("AG14CODTIPRECU") = Val(txtText1(1)) Then
          Call objError.SetError(cwCodeMsg, "Este Tipo de Restricci�n est� siendo utilizado por el Recurso: " & GetTableColumn("SELECT  AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" _
              & rdoRecurso(1))("AG11DESRECURSO"))
          Call objError.Raise
          blnCancel = True
          rdoRecurso.Close
          Set rdoRecurso = Nothing
          Exit Sub
        End If
        rdoRecurso.MoveNext
      Loop
     'Busco en REstricciones por Franja
      strSql = "SELECT DISTINCT AG14CODTIPRECU FROM AG1100 " _
         & " WHERE AG11CODRECURSO IN " _
         & " ( SELECT AG11CODRECURSO FROM AG1200 " _
            & " WHERE AG16CODTIPREST=" & grdDBGrid1(1).Columns(5).Value & " )"
      Set rdoRecurso = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
      Do While Not rdoRecurso.EOF
        If rdoRecurso("AG14CODTIPRECU") = Val(txtText1(1)) Then
          Call objError.SetError(cwCodeMsg, "Este Tipo de Restricci�n est� siendo utilizado por el Recurso: " & GetTableColumn("SELECT  AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" _
              & Val(rdoRecurso(1)))("AG11DESRECURSO"))
          Call objError.Raise
          blnCancel = True
          rdoRecurso.Close
          Set rdoRecurso = Nothing
          Exit Sub
        End If
        rdoRecurso.MoveNext
      Loop

    End If
  End If
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim strSql As String
  If strFormName = "Tipo Recurso" Then
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      'Creo la Sql para obtener el nuevo c�digo
       strSql = "SELECT AG14CODTIPRECU FROM " & objEnv.GetValue("Database") & "AG1400 " _
             & "  ORDER BY AG14CODTIPRECU DESC"
       'Empieza transacci�n
       Call objApp.rdoConnect.BeginTrans
       vntNuevoCod = GetNewCode(strSql)
       Call objWinInfo.CtrlStabilize(txtText1(1), vntNuevoCod)
    End If
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
' Introducci�n de los impresos
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "Tipo Recurso" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
 
End Sub
' Validaciones del programa
Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
  If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
       Call objError.SetError(cwCodeMsg, "Fecha Fin es anterior que Fecha Inicial")
       Call objError.Raise
       blnCancel = True
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)

End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 1 Then
    If txtText1(1) = "" Then
      Command1.Enabled = False
    Else
      Command1.Enabled = True
    End If
  End If
End Sub


