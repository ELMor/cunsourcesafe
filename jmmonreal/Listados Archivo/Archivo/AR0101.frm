VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.ocx"
Begin VB.Form frmListArchivo 
   Caption         =   "Listado de Preadmisiones"
   ClientHeight    =   8325
   ClientLeft      =   2655
   ClientTop       =   1485
   ClientWidth     =   10200
   LinkTopic       =   "Form1"
   ScaleHeight     =   8325
   ScaleWidth      =   10200
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10200
      _ExtentX        =   17992
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   4455
      Left            =   600
      TabIndex        =   8
      Top             =   2160
      Width           =   8655
      Begin VB.OptionButton Option1 
         Caption         =   "Pacientes - Nuevos que han venido en una fecha"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   3
         Left            =   840
         TabIndex        =   23
         Top             =   1920
         Width           =   7335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Listado de pacientes fallecidos"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   7
         Left            =   840
         TabIndex        =   21
         Top             =   3840
         Width           =   6855
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pacientes hospitalizados dados de alta en una fecha"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   6
         Left            =   840
         TabIndex        =   19
         Top             =   3360
         Width           =   7455
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pacientes hospitalizados SNA dados de alta en una fecha"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   5
         Left            =   840
         TabIndex        =   17
         Top             =   2880
         Width           =   7215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pacientes Nuevos previstos para una fecha"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   4
         Left            =   840
         TabIndex        =   15
         Top             =   2400
         Width           =   6735
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pacientes - Exploraciones que han venido en una fecha"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   2
         Left            =   840
         TabIndex        =   11
         Top             =   1440
         Width           =   7215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Localizaci�n de Historias en Archivo (por Departamento)"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   1
         Left            =   840
         TabIndex        =   10
         Top             =   960
         Width           =   7095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Localizaci�n de Historias en Archivo (por Historia)"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   0
         Left            =   840
         TabIndex        =   9
         Top             =   480
         Width           =   6375
      End
      Begin VB.Label Label3 
         Caption         =   "4.-"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   7
         Left            =   480
         TabIndex        =   24
         Top             =   1920
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "8.-"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   4
         Left            =   480
         TabIndex        =   22
         Top             =   3840
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "7.-"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   3
         Left            =   480
         TabIndex        =   20
         Top             =   3360
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "6.-"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   18
         Top             =   2880
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "5.-"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   16
         Top             =   2400
         Width           =   255
      End
      Begin VB.Label Label3 
         Caption         =   "3.-"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   14
         Top             =   1440
         Width           =   255
      End
      Begin VB.Label Label2 
         Caption         =   "2.-"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   480
         TabIndex        =   13
         Top             =   960
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "1.-"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   480
         TabIndex        =   12
         Top             =   480
         Width           =   255
      End
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Imprimir Listado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3720
      MaskColor       =   &H00808080&
      TabIndex        =   1
      Top             =   6960
      Width           =   3015
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   5040
      Top             =   3840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   8040
      Width           =   10200
      _ExtentX        =   17992
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   405
      Index           =   0
      Left            =   2520
      TabIndex        =   3
      Tag             =   "Fecha Desde"
      ToolTipText     =   "Fecha Desde"
      Top             =   1320
      Width           =   2295
      _Version        =   65537
      _ExtentX        =   4048
      _ExtentY        =   714
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   405
      Index           =   1
      Left            =   5880
      TabIndex        =   4
      Tag             =   "Fecha Hasta"
      ToolTipText     =   "Fecha Hasta"
      Top             =   1320
      Width           =   2175
      _Version        =   65537
      _ExtentX        =   3836
      _ExtentY        =   714
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label Label4 
      Caption         =   "Listados de Archivo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   375
      Left            =   240
      TabIndex        =   7
      Top             =   480
      Width           =   3375
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   5880
      TabIndex        =   6
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   7
      Left            =   2520
      TabIndex        =   5
      Top             =   1080
      Width           =   1260
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmListArchivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm

''
''
''Private Sub Command5b_Click()
''
''    Dim strWhereTotal As String
''    Dim intTipoImpresora As Integer
''    Dim FechaInicio As String
''    Dim FechaFin As String
''
''If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
''        If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
''          Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
''          Call objError.Raise
''          Exit Sub
''        End If
''End If
''
'' 'Seleccionar la impresora
''If (InStr(Printer.DeviceName, "HP LaserJet 4Si") <> 0) Then
''    intTipoImpresora = 1
''ElseIf (InStr(Printer.DeviceName, "HP LaserJet 6P/6MP") <> 0) Then
''    intTipoImpresora = 2
''Else
''    intTipoImpresora = 2
''End If
''
''Me.MousePointer = vbHourglass
''    FechaInicio = "date(" & Right(dtcDateCombo1(0).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
''    FechaFin = "date(" & Right(dtcDateCombo1(1).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
''
''    If Option1(0) = True Then
''        strWhereTotal = "{AD0100.AD01FECINICIO}>=" & FechaInicio & _
''                    " AND {AD0100.AD01FECINICIO}<=" & FechaFin & _
''                    " AND {AD2500.AD12CODTIPOASIST}='2'"
''
''            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Arch1XXd.rpt"
''    End If
''
''    If Option1(1) = True Then
''        strWhereTotal = "{AD0100.AD01FECINICIO}>=" & FechaInicio & _
''                    " AND {AD0100.AD01FECINICIO}<=" & FechaFin & _
''                    " AND {AD2500.AD12CODTIPOASIST}='2'"
''
''            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Arch2XXd.rpt"
''    End If
''
''    If Option1(2) = True Then
''        strWhereTotal = "{AD0100.AD01FECINICIO}>=" & FechaInicio & _
''                    " AND {AD0100.AD01FECINICIO}<=" & FechaFin & _
''                    " AND {AD0800.AD10CODTIPPACIEN}='4'"
''
''            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "ExplXXd.rpt"
''    End If
''
''    If Option1(3) = True Then
''        strWhereTotal = "{AD0100.AD01FECINICIO}>=" & FechaInicio & _
''                    " AND {AD0100.AD01FECINICIO}<=" & FechaFin & _
''                    " AND {AD0800.AD10CODTIPPACIEN}='1'"
''
''            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "NuevXXd.rpt"
''    End If
''
''    If Option1(4) = True Then
''        strWhereTotal = "date({CI0100.CI01FECCONCERT})>=" & FechaInicio & _
''                    " date({CI0100.CI01FECCONCERT})<=" & FechaFin & _
''                    " and {CI0100.CI01SITCITA}='1'" & _
''                    " and {PR0400.PR37CODESTADO}=2" & _
''                    " and {AD2500.AD12CODTIPOASIST}='2'" & _
''                    " and {AD0800.AD10CODTIPPACIEN}='1'" & _
''                    " and (isnull({PR0400.AD01CODASISTENCI}) or ({PR0400.AD02CODDPTO}={AD0500.AD02CODDPTO} and isnull({AD0100.AD01FECFIN})))"
''
''            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "NuPreXXd.rpt"
''    End If
''
''    If Option1(5) = True Then
''        strWhereTotal = "{AD0100.AD01FECFIN}>=" & FechaInicio & _
''                    " AND {AD0100.AD01FECFIN}<=" & FechaFin & _
''                    " AND {AD2500.AD12CODTIPOASIST}='1'" & _
''                    " AND ({AD1100.CI32CODTIPECON}='S' AND {AD1100.CI13CODENTIDAD}='NA')"
''
''            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "AldsXXd.rpt"
''    End If
''
''    If Option1(6) = True Then
''        strWhereTotal = "{AD0100.AD01FECFIN}>=" & FechaInicio & _
''                    " AND {AD0100.AD01FECFIN}<=" & FechaFin & _
''                    " AND {AD2500.AD12CODTIPOASIST}='1'" & _
''                    " AND ({@RegElim}=0)"
''
''            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "AldiXXd.rpt"
''    End If
''
''    If Option1(7) = True Then
''        strWhereTotal = "{AD0100.AD01FECFIN}>=" & FechaInicio & _
''                    " AND {AD0100.AD01FECFIN}<=" & FechaFin & _
''                    " AND {AD0100.AD27CODALTAASIST}='6'"
''
''            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "FalleXXd.rpt"
''    End If
''
''            With crtCrystalReport1
''                .PrinterCopies = 1
''                .Destination = crptToPrinter
''                .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
''                .Destination = crptToPrinter
''            '        .Destination = crptToWindow
''                .Connect = objApp.rdoConnect.Connect
''                .DiscardSavedData = True
''                Me.MousePointer = vbHourglass
''                .Action = 1
''            '        .PrintReport
''
''            End With
''
''Me.MousePointer = vbDefault
''
''End Sub


Private Sub Command5_Click()


    Dim strWhereTotal As String
    Dim intTipoImpresora As Integer
    Dim FechaInicio As String
    Dim FechaFin As String
    Dim report As String
    Dim Caption As String
'    Dim Report As String
    Dim Impresora As Integer
    Dim sql As String
    
If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
        If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
          Call objError.Raise
          Exit Sub
        End If
End If

 'Seleccionar la impresora
If (InStr(Printer.DeviceName, "HP LaserJet 4Si") <> 0) Then
    intTipoImpresora = 1
ElseIf (InStr(Printer.DeviceName, "HP4P") <> 0) Then
    intTipoImpresora = 2
Else
    intTipoImpresora = 2
End If
'Call Imprimir_API(sql, Report, Formula, Caption, rdoCon, Impresora)
Me.MousePointer = vbHourglass
    
    FechaInicio = "TO_DATE('" & Right(dtcDateCombo1(0).Text, 4) & "-" & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "-" & Trim(Left(dtcDateCombo1(0).Text, 2)) & " 00:00:00','YYYY-MM-DD HH24:MI:SS')"
    FechaFin = "TO_DATE('" & Right(dtcDateCombo1(1).Text, 4) & "-" & Trim(Mid(dtcDateCombo1(1).Text, 4, 2)) & "-" & Trim(Left(dtcDateCombo1(1).Text, 2)) & " 23:59:59','YYYY-MM-DD HH24:MI:SS')"
    
    If Option1(0) = True Then
        sql = "PR0432J.CI01FECCONCERT >= " & FechaInicio & _
        " And PR0432J.CI01FECCONCERT <= " & FechaFin & _
        " AND (PR0432J.CI01SITCITA='1' or PR0432J.CI01SITCITA='5')" & _
        " AND PR0100.PR12CODACTIVIDAD <> 209"
        
        report = "Arci1XXd.rpt"
        
        If dtcDateCombo1(0).Text = dtcDateCombo1(1).Text Then
            Caption = "LOCALIZACION DE HISTORIAS EN ARCHIVO PARA EL DIA " & dtcDateCombo1(0).Text
        Else
            Caption = "LOCALIZACION DE HISTORIAS EN ARCHIVO PARA LOS DIAS " & dtcDateCombo1(0).Text & "A " & dtcDateCombo1(1).Text
        End If
    
        Impresora = True
        
        Call Imprimir_API(sql, report, , Caption, , Impresora)
        
        Me.MousePointer = Default
    End If

    If Option1(1) = True Then
        sql = "PR0432J.CI01FECCONCERT >= " & FechaInicio & _
        " And PR0432J.CI01FECCONCERT <= " & FechaFin & _
        " AND (PR0432J.CI01SITCITA='1' or PR0432J.CI01SITCITA='5')" & _
        " AND PR0100.PR12CODACTIVIDAD <> 209"

        
        report = "Arci2XXd.rpt"
        
        If dtcDateCombo1(0).Text = dtcDateCombo1(1).Text Then
            Caption = "LOCALIZACION DE HISTORIAS EN ARCHIVO PARA EL DIA " & dtcDateCombo1(0).Text
        Else
            Caption = "LOCALIZACION DE HISTORIAS EN ARCHIVO PARA LOS DIAS " & dtcDateCombo1(0).Text & "A " & dtcDateCombo1(1).Text
        End If
    
        Impresora = False
                
        Call Imprimir_API(sql, report, , Caption, , Impresora)
        
        Me.MousePointer = Default
    End If

    If Option1(2) = True Then
        sql = "AD0100.AD01FECINICIO >= " & FechaInicio & _
        " And AD0100.AD01FECINICIO <= " & FechaFin & _
        " AND (AD0800.AD10CODTIPPACIEN = '4') "
        
        report = "ExplXXd.rpt"
        
        If dtcDateCombo1(0).Text = dtcDateCombo1(1).Text Then
            Caption = "PACIENTES - EXPLORACIONES QUE HAN VENIDO A FECHA " & dtcDateCombo1(0).Text
        Else
            Caption = "PACIENTES - EXPLORACIONES QUE HAN VENIDO ENTRE " & dtcDateCombo1(0).Text & "Y " & dtcDateCombo1(1).Text
        End If
    
        Impresora = True
        
        Call Imprimir_API(sql, report, , Caption, , Impresora)
        
        Me.MousePointer = Default
    End If

    If Option1(3) = True Then
        sql = "AD0100.AD01FECINICIO >= " & FechaInicio & _
        " And AD0100.AD01FECINICIO <= " & FechaFin & _
        " AND (AD0800.AD10CODTIPPACIEN = '1') "
        
        report = "NuevXXd.rpt"
        
        If dtcDateCombo1(0).Text = dtcDateCombo1(1).Text Then
            Caption = "PACIENTES - NUEVOS QUE HAN VENIDO A FECHA " & dtcDateCombo1(0).Text
        Else
            Caption = "PACIENTES - NUEVOS QUE HAN VENIDO ENTRE " & dtcDateCombo1(0).Text & "Y " & dtcDateCombo1(1).Text
        End If
    
        Impresora = True
        
        Call Imprimir_API(sql, report, , Caption, , Impresora)
        
        Me.MousePointer = Default
    End If

    If Option1(4) = True Then
        sql = "CI0100.CI01FECCONCERT >= " & FechaInicio & _
        " And CI0100.CI01FECCONCERT <= " & FechaFin & _
        " and CI0100.CI01SITCITA='1'" & _
        " and PR0400.PR37CODESTADO=2" & _
        " and AD2500.AD12CODTIPOASIST='2'" & _
        " and AD0800.AD10CODTIPPACIEN='1'" & _
        " and (PR0400.AD01CODASISTENCI IS NULL OR" & _
        " (PR0400.AD02CODDPTO=AD0500.AD02CODDPTO and AD0100.AD01FECFIN IS NULL))"
        
        report = "NuPreXXd.rpt"
        
        If dtcDateCombo1(0).Text = dtcDateCombo1(1).Text Then
            Caption = "PACIENTES NUEVOS PREVISTOS PARA CONSULTA PARA EL DIA  " & dtcDateCombo1(0).Text
        Else
            Caption = "PACIENTES NUEVOS PREVISTOS PARA CONSULTA PARA LOS DIAS " & dtcDateCombo1(0).Text & "A " & dtcDateCombo1(1).Text
        End If
    
        Impresora = True
        
        Call Imprimir_API(sql, report, , Caption, , Impresora)
        
        Me.MousePointer = Default
    End If

    If Option1(5) = True Then
        sql = "PR0400.PR04FECFINACT >= " & FechaInicio & _
        " And PR0400.PR04FECFINACT <= " & FechaFin & _
        " AND (AD1100.CI32CODTIPECON='S') " & _
        " AND (AD1100.CI13CODENTIDAD='NA') " & _
        " AND (PR0100.PR12CODACTIVIDAD=209) " & _
        " AND (AD0500.AD05FECFINRESPON IS NULL) " & _
        " AND (AD1100.AD11FECFIN IS NULL)"
        
        report = "AldsXXDef.rpt"
        
        If dtcDateCombo1(0).Text = dtcDateCombo1(1).Text Then
            Caption = "PACIENTES HOSPITALIZADOS (SNA) DADOS DE ALTA A FECHA " & dtcDateCombo1(0).Text
        Else
            Caption = "PACIENTES HOSPITALIZADOS (SNA) DADOS DE ALTA ENTRE LOS DIAS " & dtcDateCombo1(0).Text & "A " & dtcDateCombo1(1).Text
        End If
    
        Impresora = True
        
        Call Imprimir_API(sql, report, , Caption, , Impresora)
        
        Me.MousePointer = Default
    End If

    If Option1(6) = True Then
        sql = "PR0400.PR04FECFINACT >= " & FechaInicio & _
        " And PR0400.PR04FECFINACT <= " & FechaFin & _
        " AND NOT (AD1100.CI32CODTIPECON = 'S' and AD1100.CI13CODENTIDAD = 'NA')" & _
        " AND (PR0100.PR12CODACTIVIDAD=209) " & _
        " AND (AD0500.AD05FECFINRESPON IS NULL) " & _
        " AND (AD1100.AD11FECFIN IS NULL)" & _
        " AND CI2200.CI22NUMHISTORIA<>11"
        
        report = "AldiXXDef.rpt"
        
        If dtcDateCombo1(0).Text = dtcDateCombo1(1).Text Then
            Caption = "PACIENTES HOSPITALIZADOS DADOS DE ALTA A FECHA " & dtcDateCombo1(0).Text
        Else
            Caption = "PACIENTES HOSPITALIZADOS DADOS DE ALTA A FECHA LOS DIAS " & dtcDateCombo1(0).Text & "A " & dtcDateCombo1(1).Text
        End If
    
        Impresora = True
        
        Call Imprimir_API(sql, report, , Caption, , Impresora)
        
        Me.MousePointer = Default
    End If

    If Option1(7) = True Then
        sql = "AD0100.AD01FECFIN >= " & FechaInicio & _
        " And AD0100.AD01FECFIN <= " & FechaFin & _
        " AND (AD0100.AD27CODALTAASIST = '6') "
        
        report = "FalleXXd.rpt"
        
        If dtcDateCombo1(0).Text = dtcDateCombo1(1).Text Then
            Caption = "PACIENTES FALLECIDOS ENTRE LOS DIAS EL DIA " & dtcDateCombo1(0).Text
        Else
            Caption = "PACIENTES FALLECIDOS ENTRE LOS DIAS " & dtcDateCombo1(0).Text & "A " & dtcDateCombo1(1).Text
        End If
    
        Impresora = True
        
        Call Imprimir_API(sql, report, , Caption, , Impresora)
        
        Me.MousePointer = Default
    End If


End Sub

Private Sub Form_Activate()
'  objWinInfo.DataRefresh
    
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
 tlbToolbar1.Buttons.Item(2).Enabled = False    'Nuevo
 tlbToolbar1.Buttons.Item(3).Enabled = False    'Abrir
 tlbToolbar1.Buttons.Item(4).Enabled = False    'Disquete
 tlbToolbar1.Buttons.Item(6).Enabled = False    'Imprimir
 
 tlbToolbar1.Buttons.Item(8).Enabled = False    'Eliminar
 
 tlbToolbar1.Buttons.Item(10).Enabled = False   'Cortar
 tlbToolbar1.Buttons.Item(11).Enabled = False   'Copiar
 tlbToolbar1.Buttons.Item(12).Enabled = False   'Pegar
 
 tlbToolbar1.Buttons.Item(14).Enabled = False   'Deshacer
 
 tlbToolbar1.Buttons.Item(16).Enabled = False   'Buscar
 
 tlbToolbar1.Buttons.Item(18).Enabled = False   'Activar filtro
 tlbToolbar1.Buttons.Item(19).Enabled = False   'Desactivar filtro
 
 tlbToolbar1.Buttons.Item(21).Enabled = False   'Primer registro
 tlbToolbar1.Buttons.Item(22).Enabled = False   'Anterior registro
 tlbToolbar1.Buttons.Item(23).Enabled = False   'Siguiente registro
 tlbToolbar1.Buttons.Item(24).Enabled = False   'Ultimo registro

 tlbToolbar1.Buttons.Item(26).Enabled = False   'Refrescar

 tlbToolbar1.Buttons.Item(28).Enabled = False   'Mantenimiento
 'tlbToolbar1.Buttons.Item(30).Enabled = False   'Salir

End Sub



Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
If Not gblnCancelar Then
'  intCancel = objWinInfo.WinExit
End If
End Sub

Private Sub Form_Unload(intCancel As Integer)
If Not gblnCancelar Then
'  Call objWinInfo.WinDeRegister
'  Call objWinInfo.WinRemoveInfo
End If
End Sub




Private Sub Frame2_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
If btnButton.Index = 30 Then
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Exit Sub
End If
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    'Activamos y desactivamos los botones
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

End Sub

Private Sub CtrBotonDeshacer()
End Sub
