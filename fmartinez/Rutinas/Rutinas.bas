Attribute VB_Name = "Rutinas"
Option Explicit
Sub R_CentrarForm(FormName As Form)
'******************************************************************
'* Objetivo: Centrar un Form en la pantalla fisica
'* Ejemplo : Call R_CentrarForm(Me)
'******************************************************************
    FormName.Move (Screen.Width - FormName.Width) / 2, _
                  (Screen.Height - FormName.Height) / 2
End Sub
Sub R_Error()
'******************************************************************
'* Objetivo: Mostrar el error producido y finalizar la ejecucion
'* Ejemplo : Call R_Error
'******************************************************************
MsgBox ("***** E R R O R *****" & Chr(13) & _
        "N�mero de error: " & Str(Err.Number) & Chr(13) & _
        "Descripci�n: " & Err.Description & Chr(13) & _
        "Fuente: " & Err.Source)
End ' Fin de la ejecucion
End Sub
Sub R_ErrorRdo()
'******************************************************************
'* Objetivo: Mostrar el error producido y finalizar la ejecucion
'* Ejemplo : Call R_ErrorRdo
'******************************************************************
MsgBox (R_ErrorRdoMsg)
End ' Fin de la ejecucion
End Sub
Function R_ErrorRdoMsg()
'******************************************************************
'* Objetivo: devuelve el mensaje del error producido
'******************************************************************
Dim er As rdoError
Dim strA As String


strA = "***** E R R O R *****" & Chr(13) & _
       "N�mero de error: " & Str(Err.Number) & Chr(13) & _
       "Descripci�n: " & Err.Description & Chr(13) & _
       "Fuente: " & Err.Source
For Each er In rdoErrors
  strA = strA & Chr(13) & "***** ERROR: R D O *****" & Chr(13) & _
          "N�mero de error: " & Str(er.Number) & Chr(13) & _
          "Descripci�n: " & er.Description & Chr(13) & _
         "Fuente: " & er.Source
Next er
R_ErrorRdoMsg = strA
End Function

Function R_ChrSwap(String1 As String, Source As String, Target As String) As String
'*************************************************************
'* Objetivo: Cambia todas las ocurrencias de Source por Target en String1.
'*           Source y Target pueden ser de distinto tama�o y de cualquier longitud
'* Ejemplo:
'*            R_ChrSwap("AAABBAAAAAAAAA","BB","CCC")
'*            devuelve  "AAACCCAAAAAAAAA"
'*************************************************************
Dim Ch As String
Dim I As Integer
Ch = String1
' cambiar Source por Chr(255)
' (puede ser que en SOURCE haya algunos caracteres de TARGET)
  Do While InStr(Ch, Source) > 0
     I = InStr(Ch, Source)
     Ch = Left$(Ch, I - 1) & Chr(255) & Right$(Ch, Len(Ch) - I - Len(Source) + 1)
  Loop
' cambiar Chr(255) por Target
  Do While InStr(Ch, Chr(255)) > 0
     I = InStr(Ch, Chr(255))
     Ch = Left$(Ch, I - 1) & Target & Right$(Ch, Len(Ch) - I)
  Loop
R_ChrSwap = Ch
End Function

Function R_Fecha(Fecha As String) As Variant
'*************************************************************
'* Objetivo: Devuelve una fecha incorporando el siglo
'* Ejemplo:
'*            R_Fecha("15/12/95")   --> 19951215
'*            R_Fecha("15/12/02")   --> 20051215
'*
'* Fecha llegar� en el formato "DD/MM/AA" y si es incorrecta se
'*       devuelve "Empty"
'*************************************************************
Dim Ch As String
Dim Ch1 As String
Dim I As Integer
Dim Fecha1 As Date
If Len(Fecha) = 8 Then
   I = Val(Right$(Fecha, 2)) ' a�o
   If I > 50 Then
      Ch = Left$(Fecha, 6) & "19" & Right$(Fecha, 2)
   Else
      Ch = Left$(Fecha, 6) & "20" & Right$(Fecha, 2)
   End If
   If IsDate(Ch) Then
      Fecha1 = CDate(Ch)
      ' comprobacion adicional para verificar que VB ha interpretado la
      ' fecha como se esperaba
      '            ? Print Format(CDate("18/02/1996"), "yyyymmdd")
      '              genera: 19960218
      '            ? Print Format(CDate("02/18/1996"), "yyyymmdd")
      '              genera: 19960218
        Ch1 = Right$(Ch, 4) & Mid$(Ch, 4, 2) & Left$(Ch, 2)
        If Format(Fecha1, "yyyymmdd") = Ch1 Then
           R_Fecha = CDate(Ch)
           End If
   End If
End If
End Function
Function R_Empty(String1 As String) As Boolean
'*************************************************************
'* Objetivo: Indica si la variable String1 est� vacia
'* Ejemplo:
'*            R_Empty("AA") -> False
'*            R_Empty(" ")  -> True
'*************************************************************
If Len(Trim(String1)) = 0 Then
   R_Empty = True
Else
   R_Empty = False
End If
End Function
Function R_StrLenPed(String1 As Variant, Longi As Long) As String
'*************************************************************
'* Objetivo: partiendo del string indicado devuelve un string
'*           de la longitud pedida
'* Ejemplo:
'*            R_StrLenPed("AA",5) -> "AA   "
'*************************************************************
Dim strA As String
If IsNull(String1) Then
   strA = ""
Else
   strA = String1
End If
R_StrLenPed = Mid(strA & Space(Longi), 1, Longi)
End Function

