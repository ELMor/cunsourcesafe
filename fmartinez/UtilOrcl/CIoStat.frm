VERSION 5.00
Begin VB.Form frmCIoStat 
   Caption         =   " "
   ClientHeight    =   3135
   ClientLeft      =   90
   ClientTop       =   375
   ClientWidth     =   4620
   LinkTopic       =   "Form1"
   ScaleHeight     =   3135
   ScaleWidth      =   4620
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtEntrada 
      Height          =   375
      Left            =   3960
      TabIndex        =   3
      Text            =   " "
      Top             =   840
      Width           =   7335
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "&Continuar"
      Default         =   -1  'True
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   495
      Left            =   10200
      TabIndex        =   0
      Top             =   8040
      Width           =   1575
   End
   Begin VB.Label lblEntrada 
      Caption         =   " "
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   3615
   End
End
Attribute VB_Name = "frmCIoStat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Actualizar la tabla de puestos GC0200
Option Explicit

Private Sub cmdContinuar_Click()
    Dim I As Long
    Dim J As Long
    Dim K As Long
    Dim L As Long
    Dim strA As String
    Dim strB As String
    Dim CodPuesto As String
    Dim DesPuesto As String
    Dim qd As rdoQuery
    Dim qd1 As rdoQuery
    Dim rs As rdoResultset
    
    
    On Error GoTo LocalErr
    Screen.MousePointer = vbHourglass

    Open txtEntrada.Text For Input As 1
    I = 0 ' registros leidos
    J = 0 ' registros a�adidos
    K = 0 ' registros actualizados
    Do While Not EOF(1)
       Line Input #1, strA ' leer una linea
       CodPuesto = Trim(Mid(strA, 1, 15))
       DesPuesto = Trim(Mid(strA, 16))
       If Not R_Empty(CodPuesto) Then
          I = I + 1 ' registros leidos
          If R_Empty(DesPuesto) Then
             DesPuesto = "NULL"
          Else
             DesPuesto = "'" & DesPuesto & "'"
          End If
          Set qd = conBaseDatos.CreateQuery("", " ")
          Set qd1 = conBaseDatos.CreateQuery("", " ")
          qd.SQL = "SELECT GC02CODPUESTO FROM GC.GC0200 " & _
                   "WHERE GC02CODPUESTO = '" & CodPuesto & "'"
          Set rs = qd.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          If rs.BOF Or rs.EOF Then
             qd1.SQL = "INSERT INTO GC.GC0200 (GC02CODPUESTO,GC02DESPUESTO) " & _
                       "VALUES ('" & CodPuesto & "'," & DesPuesto & ")"
             J = J + 1 ' registros a�adidos
          Else
             qd1.SQL = "UPDATE GC.GC0200 SET " & _
                       "GC02CODPUESTO = '" & CodPuesto & "', " & _
                       "GC02DESPUESTO = " & DesPuesto & _
                       " WHERE GC02CODPUESTO = '" & CodPuesto & "'"
             K = K + 1 ' registros actualizados
          End If
          qd1.Execute
          rs.Close
          qd.Close
          qd1.Close
       End If
    Loop
    Close #1
    
    strB = "Numero de Registros leidos: " & Str(I) & vbCrLf & _
           "Numero de Registros a�adidos: " + Str(J) & vbCrLf & _
           "Numero de Registros actualizados: " + Str(K)
    Screen.MousePointer = vbDefault
    MsgBox (strB)
    Me.Hide
    
    Exit Sub
LocalErr:
    Screen.MousePointer = vbDefault
    MsgBox (R_ErrorRdoMsg)
    Close
End Sub

Private Sub cmdSalir_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.Caption = "Actualizar la tabla de puestos GC0200"
    lblEntrada.Caption = "Fichero con la relaci�n de puestos:"
    txtEntrada.Text = "D:\FMRA\ORACLE\PUESTOS\PRUEBA1.TXT"
End Sub
