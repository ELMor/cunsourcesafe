VERSION 5.00
Begin VB.Form frmGrants 
   Caption         =   "Crear un script con triggers para mantener Usuario y fecha Add/Upd de las tablas de un Usuario"
   ClientHeight    =   3135
   ClientLeft      =   90
   ClientTop       =   375
   ClientWidth     =   4620
   LinkTopic       =   "Form1"
   ScaleHeight     =   3135
   ScaleWidth      =   4620
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtPrefTablas 
      Height          =   375
      Left            =   3960
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   1800
      Width           =   7335
   End
   Begin VB.TextBox txtTrigg 
      Height          =   375
      Left            =   3960
      TabIndex        =   4
      Text            =   "Text2"
      Top             =   3840
      Width           =   7335
   End
   Begin VB.TextBox txtCampos 
      Height          =   375
      Left            =   3960
      TabIndex        =   3
      Text            =   "Text2"
      Top             =   3000
      Width           =   7335
   End
   Begin VB.TextBox txtUsuario 
      Height          =   375
      Left            =   3960
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   840
      Width           =   7335
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "&Continuar"
      Default         =   -1  'True
      Height          =   495
      Left            =   0
      TabIndex        =   5
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   495
      Left            =   10200
      TabIndex        =   0
      Top             =   8040
      Width           =   1575
   End
   Begin VB.Label lblPrefTablas 
      Caption         =   "Prefijo de las tablas"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1800
      Width           =   3615
   End
   Begin VB.Label lblTrigg 
      Caption         =   "Script con triggers a a�adir"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   3960
      Width           =   3615
   End
   Begin VB.Label lblCampos 
      Caption         =   "Script con campos a a�adir"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   3120
      Width           =   3615
   End
   Begin VB.Label lblUsuario 
      Caption         =   "Usuario propietario de las tablas"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   3615
   End
End
Attribute VB_Name = "frmGrants"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Crear un script para crear campos y triggers para mantener el usuario
' y la fecha de creacion y ultima actualizacion de los registros de las
' tablas de un usuario
Option Explicit

Private Sub cmdContinuar_Click()
    Dim I As Long
    Dim J As Long
    Dim K As Long
    Dim L As Long
    Dim intNTablas As Integer
    Dim intNTrigg As Integer
    Dim strA As String
    Dim strB As String
    Dim qd As rdoQuery
    Dim rs As rdoResultset
    Dim qdTablas As rdoQuery
    Dim rsTablas As rdoResultset
    Dim Tabla As String
    Dim PrefijoCampos As String
    Dim NombreTrigger As String
    
    
    On Error GoTo LocalErr
    Screen.MousePointer = vbHourglass

    ' Fichero donde se va a generar el Script con los campos a a�adir a las
    ' tablas del usuario
    Open txtCampos.Text For Output As 1

    ' Fichero donde se va a generar el Script con los triggers a a�adir al
    ' usuario GC
    Open txtTrigg.Text For Output As 2
    
    Print #1, " "
    Print #1, "/* Script a ejecutar en el usuario: " & txtUsuario.Text & " */"
    
    Print #2, " "
    Print #2, "/* Script a ejecutar en el usuario: GC */"
    
    intNTablas = 0 ' numero de tablas a modificar
    intNTrigg = 0 ' numero de triggers a a�adir
    
    ' relacion de tablas del usuario
    strA = "SELECT TABLE_NAME FROM ALL_TABLES " & _
           "WHERE OWNER = '" & txtUsuario.Text & "'"
    Set qdTablas = conBaseDatos.CreateQuery("", " ")
    qdTablas.SQL = strA
    Set rsTablas = qdTablas.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    Do Until rsTablas.EOF
       Tabla = rsTablas("TABLE_NAME")
       ' prefijo de los campos
          If Len(Tabla) = 6 And _
             Mid(Tabla, 1, 2) = txtPrefTablas.Text And _
             Mid(Tabla, 5, 2) = "00" Then
             PrefijoCampos = Mid(Tabla, 1, 4)
          Else
             PrefijoCampos = ""
          End If
       ' verificar si estan a�adidos los campos a la tabla
          strA = "SELECT COLUMN_NAME FROM ALL_TAB_COLUMNS " & _
                 "WHERE OWNER = '" & txtUsuario.Text & "' AND " & _
                 "TABLE_NAME = '" & Tabla & "' AND " & _
                 "COLUMN_NAME = 'SG02COD_ADD'"
          Set qd = conBaseDatos.CreateQuery("", " ")
          qd.SQL = strA
          Set rs = qd.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          If rs.BOF Or rs.EOF Then
             ' script para a�adir los campos a la tabla
             intNTablas = intNTablas + 1 ' numero de tablas a modificar
             Print #1, " "
             Print #1, "ALTER TABLE " & Tabla & _
                       " ADD ( SG02COD_ADD VARCHAR2(6) NULL,"
             Print #1, Space(25) & PrefijoCampos & "FECADD DATE NULL,"
             Print #1, Space(25) & "SG02COD_UPD VARCHAR2(6) NULL,"
             Print #1, Space(25) & PrefijoCampos & "FECUPD DATE NULL);"
             Print #1, "COMMENT ON COLUMN " & Tabla & _
                       ".SG02COD_ADD IS 'Inserci�n Registro: Usuario de seguridad';"
             Print #1, "COMMENT ON COLUMN " & Tabla & "." & PrefijoCampos & _
                       "FECADD IS 'Inserci�n Registro: Fecha / Hora';"
             Print #1, "COMMENT ON COLUMN " & Tabla & _
                       ".SG02COD_UPD IS 'Ultima Act.Registro: Usuario de seguridad';"
             Print #1, "COMMENT ON COLUMN " & Tabla & "." & PrefijoCampos & _
                       "FECUPD IS 'Ultima Act.Registro: Fecha / Hora';"
             Print #1, "grant delete,insert,select,references,update on " & _
                       Tabla & " to GC with grant option;"
          End If
          rs.Close
          qd.Close
       ' nombre del trigger
          If Len(Tabla) = 6 And _
             Mid(Tabla, 1, 2) = txtPrefTablas.Text And _
             Mid(Tabla, 5, 2) = "00" Then
             NombreTrigger = Mid(Tabla, 1, 4) & "IUB50"
          Else
             NombreTrigger = Tabla & "_IUB50"
          End If
       ' verificar si esta creado el trigger
          strA = "SELECT TRIGGER_NAME FROM ALL_TRIGGERS " & _
                 "WHERE TRIGGER_NAME = '" & NombreTrigger & "'"
          Set qd = conBaseDatos.CreateQuery("", " ")
          qd.SQL = strA
          Set rs = qd.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          If rs.BOF Or rs.EOF Then
             ' script para a�adir los campos a la tabla
             intNTrigg = intNTrigg + 1 ' numero de triggers a a�adir
             Print #2, " "
             Print #2, "CREATE SYNONYM " & Tabla & _
                       " FOR " & txtUsuario.Text & "." & Tabla & ";"
             Print #2, "CREATE TRIGGER " & NombreTrigger
             Print #2, "   BEFORE"
             Print #2, "   INSERT OR UPDATE"
             Print #2, "   ON " & Tabla
             Print #2, "   FOR EACH ROW"
             Print #2, "BEGIN"
             Print #2, "   IF INSERTING THEN"
             Print #2, "      :NEW.SG02COD_ADD := GCFN01;  -- codigo del " & _
                       "usuario de seguridad"
             Print #2, "      :NEW." & PrefijoCampos & "FECADD  := SYSDATE;" & _
                       "-- fecha / hora"
             Print #2, "   END IF;"
             Print #2, "   IF UPDATING THEN"
             Print #2, "      :NEW.SG02COD_UPD := GCFN01;  -- codigo del " & _
                       "usuario de seguridad"
             Print #2, "      :NEW." & PrefijoCampos & "FECUPD  := SYSDATE;" & _
                       "-- fecha / hora"
             Print #2, "   END IF;"
             Print #2, "END;"
             Print #2, "/"
          End If
          rs.Close
          qd.Close
       rsTablas.MoveNext
    Loop
    rsTablas.Close
    qdTablas.Close
    
    
    Close
    
    strB = "Numero de Tablas a modificar: " & Str(intNTablas) & vbCrLf & _
           "Numero de Triggers a a�adir: " + Str(intNTrigg)
    Screen.MousePointer = vbDefault
    MsgBox (strB)
    Me.Hide
    
    
    Exit Sub
LocalErr:
    Screen.MousePointer = vbDefault
    MsgBox (R_ErrorRdoMsg)
    Close
End Sub

Private Sub cmdSalir_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    txtUsuario.Text = "DT"
    txtPrefTablas.Text = "DT"
    txtCampos.Text = "D:\CAMPOS.SQL"
    txtTrigg.Text = "D:\TRIGG.SQL"
End Sub
