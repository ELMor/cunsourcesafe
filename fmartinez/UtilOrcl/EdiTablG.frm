VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{00028C01-0000-0000-0000-000000000046}#1.0#0"; "DBGRID32.OCX"
Begin VB.Form frmEdiTablG 
   Caption         =   "DEPARTAMENTOS"
   ClientHeight    =   5580
   ClientLeft      =   45
   ClientTop       =   270
   ClientWidth     =   8640
   LinkTopic       =   "Form1"
   ScaleHeight     =   5580
   ScaleWidth      =   8640
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab SSTab1 
      Height          =   4932
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   8412
      _ExtentX        =   14843
      _ExtentY        =   8705
      _Version        =   327681
      TabOrientation  =   3
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   420
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Detalle"
      TabPicture(0)   =   "Promocio.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label5"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label2"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label6"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "txtProvin"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "txtPoblac"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "txtDirecc"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "txtNombre"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "txtCodigo"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtBookmark"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "chkVPO"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).ControlCount=   13
      TabCaption(1)   =   "Tabla"
      TabPicture(1)   =   "Promocio.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "DBGrid1"
      Tab(1).ControlCount=   1
      Begin VB.CheckBox chkVPO 
         Caption         =   "V.P.O."
         Height          =   372
         Left            =   5160
         TabIndex        =   15
         Top             =   480
         Width           =   1092
      End
      Begin VB.TextBox txtBookmark 
         Enabled         =   0   'False
         Height          =   372
         Left            =   1080
         TabIndex        =   13
         Text            =   " "
         Top             =   2880
         Width           =   3252
      End
      Begin MSDBGrid.DBGrid DBGrid1 
         Height          =   4572
         Left            =   -74880
         OleObjectBlob   =   "Promocio.frx":0038
         TabIndex        =   12
         Top             =   120
         Width           =   7932
      End
      Begin VB.TextBox txtCodigo 
         Height          =   288
         Left            =   1080
         TabIndex        =   1
         Text            =   "Text1"
         Top             =   240
         Width           =   972
      End
      Begin VB.TextBox txtNombre 
         Height          =   288
         Left            =   1080
         TabIndex        =   2
         Text            =   "Text2"
         Top             =   600
         Width           =   3852
      End
      Begin VB.TextBox txtDirecc 
         Height          =   288
         Left            =   1080
         TabIndex        =   3
         Text            =   "Text3"
         Top             =   960
         Width           =   3852
      End
      Begin VB.TextBox txtPoblac 
         Height          =   288
         Left            =   1080
         TabIndex        =   4
         Text            =   "Text4"
         Top             =   1320
         Width           =   3852
      End
      Begin VB.TextBox txtProvin 
         Height          =   288
         Left            =   1080
         TabIndex        =   5
         Text            =   "Text5"
         Top             =   1680
         Width           =   3852
      End
      Begin VB.Label Label6 
         Caption         =   "Bookmak"
         Height          =   252
         Left            =   120
         TabIndex        =   14
         Top             =   3000
         Width           =   852
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         Height          =   252
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   972
      End
      Begin VB.Label Label2 
         Caption         =   "Nombre"
         Height          =   252
         Left            =   120
         TabIndex        =   10
         Top             =   600
         Width           =   972
      End
      Begin VB.Label Label3 
         Caption         =   "Direcci�n"
         Height          =   252
         Left            =   120
         TabIndex        =   9
         Top             =   960
         Width           =   972
      End
      Begin VB.Label Label4 
         Caption         =   "Poblaci�n"
         Height          =   252
         Left            =   120
         TabIndex        =   8
         Top             =   1320
         Width           =   972
      End
      Begin VB.Label Label5 
         Caption         =   "Provincia"
         Height          =   252
         Left            =   120
         TabIndex        =   7
         Top             =   1680
         Width           =   972
      End
   End
   Begin VB.CommandButton cmdBtn 
      Caption         =   "Command1"
      Height          =   372
      Index           =   0
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   972
   End
End
Attribute VB_Name = "frmEdiTablG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
Dim objEdiTabla As Object

Private Sub cmdBtn_Click(Index As Integer)
    '
    ' handle all button selections
    '
    objEdiTabla.BBProcess Me, Index
    '
End Sub

Private Sub DBGrid1_UnboundGetRelativeBookmark(StartLocation As Variant, _
                                 ByVal OffSet As Long, _
                                 NewLocation As Variant, _
                                 ApproximatePosition As Long)

   objEdiTabla.UnboundGetRelativeBookmark StartLocation, _
                                   OffSet, _
                                   NewLocation, _
                                   ApproximatePosition

End Sub

Private Sub DBGrid1_UnboundReadData( _
   ByVal RowBuf As MSDBGrid.RowBuffer, _
   StartLocation As Variant, _
   ByVal ReadPriorRows As Boolean)
   
   objEdiTabla.UnboundReadData Me, RowBuf, StartLocation, _
                             ReadPriorRows

End Sub
Public Sub LeerFilaGrid(ByVal RowBuf As MSDBGrid.RowBuffer, _
                         Fila As Integer, _
                         rs As rdoResultset)
  ' leer un registro al grid
  RowBuf.Value(Fila, 0) = rs("BLQ_CODIGO")
  RowBuf.Value(Fila, 1) = rs("BLQ_NOMBRE")
  RowBuf.Value(Fila, 2) = rs("BLQ_DIRECC")
  RowBuf.Value(Fila, 3) = rs("BLQ_POBLAC")
  RowBuf.Value(Fila, 4) = rs("BLQ_PROVIN")
End Sub

Private Sub Form_Load()
    
   Dim intI As Integer

   ' a�adir las columnas al DBGrid
   ' Quita las columnas antiguas
     For intI = DBGrid1.Columns.Count - 1 To 0 Step -1
        DBGrid1.Columns.Remove intI
     Next intI
   ' Agrega nuevas columnas
     DBGrid1.Columns.Add 0
     DBGrid1.Columns(0).Caption = "Codigo"
     DBGrid1.Columns(0).Visible = True
     DBGrid1.Columns.Add 1
     DBGrid1.Columns(1).Caption = "Nombre"
     DBGrid1.Columns(1).Visible = True
     DBGrid1.Columns.Add 2
     DBGrid1.Columns(2).Caption = "Direcci�n"
     DBGrid1.Columns(2).Visible = True
     DBGrid1.Columns.Add 3
     DBGrid1.Columns(3).Caption = "Poblaci�n"
     DBGrid1.Columns(3).Visible = True
     DBGrid1.Columns.Add 4
     DBGrid1.Columns(4).Caption = "Provincia"
     DBGrid1.Columns(4).Visible = True
    
    ' tab
    SSTab1.TabStop = False
    
    Set objEdiTabla = New EdiTabla
    '
    objEdiTabla.SqlTop = "Select * from BLOQU000"
    objEdiTabla.SqlAdd = "Select * from BLOQU000 WHERE 1 = -1"

    objEdiTabla.Empezar Me, "Detalle", "Vacio"
'   objEdiTabla.Empezar Me, "Detalle", "Con Registros"
'   objEdiTabla.Empezar Me, "Tabla", "Vacio"
'   objEdiTabla.Empezar Me, "Tabla", "Con Registros"
    
End Sub
Private Sub Form_Resize()
    '
    objEdiTabla.BBInit Me
    '
End Sub
Private Sub Form_Unload(Cancel As Integer)
    '
    objEdiTabla.RSClose
    '
End Sub
Public Sub Validar_Campos()
' procedimiento para validar los campos
' en: objEdiTabla.Msg_Severos se indican los mensajes severos
'        este mensaje no deja salvar las modificaciones hasta
'        que no ha sido corregido
' en: objEdiTabla.Msg_aviso   se indican los mensajes de aviso
'        este mensaje es mostrado al operador y a continuaci�n
'        se pide confirmacion para actualizar el registro
    
    Dim ctlTemp As Control
    Dim strTag As String
    '
    For Each ctlTemp In Me.Controls
        strTag = UCase(Trim(ctlTemp.Tag))
        Select Case strTag
           Case "BLQ_NOMBRE"
              If R_Empty(ctlTemp.Text) Then
                 objEdiTabla.Msg_Severos = "Falta el Nombre"
                 ctlTemp.SetFocus
              End If
           Case "BLQ_DIRECC"
              If R_Empty(ctlTemp.Text) Then
                 objEdiTabla.Msg_Aviso = "Falta la Direcci�n"
                 ctlTemp.SetFocus
              End If
        End Select
    Next
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
      
   ' se activa este evento cuando se pasa de una ficha Tab
   ' a otra
   
   objEdiTabla.SelecFichaTab Me

End Sub

Public Function Buscar(rsBuscar As rdoResultset) As Boolean
    ' busqueda
    Dim strBusqueda As String
    Dim rs As rdoResultset
    Dim qd As rdoQuery
    
    Set qd = db.CreateQuery("", "")
    Set rsBuscar = Nothing
    
    strBusqueda = InputBox("C�digo de Promoci�n:", _
                           "B U S Q U E D A")
    strBusqueda = Trim(strBusqueda)
    If strBusqueda = "" Then
        Buscar = False
        Exit Function
    Else
        qd.SQL = "Select * from BLOQU000 WHERE BLQ_CODIGO >= " & _
                 strBusqueda
        Set rs = qd.OpenResultset(rdOpenKeyset, _
                                  rdConcurValues)
        Set rsBuscar = rs
        Buscar = True
    End If
    
End Function
Public Function PintarPantalla(rs As rdoResultset)
    '
    ' mover los campos del Resultset al formulario
    '
    txtCodigo.Text = rs("BLQ_CODIGO")
    txtNombre.Text = rs("BLQ_NOMBRE")
    txtDirecc.Text = rs("BLQ_DIRECC")
    txtPoblac.Text = rs("BLQ_POBLAC")
    txtProvin.Text = rs("BLQ_PROVIN")
    chkVPO.Value = IIf(rs("BLQ_VPO") = True, 1, 0)
    
    txtBookmark.Text = rs.Bookmark
End Function


Public Function GrabarRegistro(rs As rdoResultset)
    '
    ' mover los campos del formulario al Resultset
    '
    rs("BLQ_CODIGO") = txtCodigo.Text
    rs("BLQ_NOMBRE") = txtNombre.Text
    rs("BLQ_DIRECC") = txtDirecc.Text
    rs("BLQ_POBLAC") = txtPoblac.Text
    rs("BLQ_PROVIN") = txtProvin.Text
    rs("BLQ_VPO") = IIf(chkVPO.Value = 1, True, False)
End Function
Public Function CtlsInici(OptEditar As String)
    '
    ' Borra el contenido de todos los controles que
    ' muestran campos en pantalla
    '
    Dim rs As rdoResultset
    Dim strA As String
    
    txtCodigo.Text = ""
    txtNombre.Text = ""
    txtDirecc.Text = ""
    txtPoblac.Text = ""
    txtProvin.Text = ""
    chkVPO.Value = 0

    If OptEditar = "A" Then
       ' inicializar campos para crear un nuevo registro
       ' inicializar el codigo de la promocion
          strA = "SELECT BLQ_CODIGO FROM BLOQU000 " & _
                 "ORDER BY BLQ_CODIGO DESC"
          Set rs = db.OpenResultset(strA, rdOpenForwardOnly, _
                                rdConcurReadOnly)
          If rs.BOF Then
             txtCodigo.Text = 1
          Else
             txtCodigo.Text = rs("BLQ_CODIGO") + 1
          End If
          rs.Close
          Set rs = Nothing
       chkVPO.Value = 1
    End If

End Function
Public Function CtlsEnaDisa(Toggle As Boolean)
    '
    ' toggle the controls on/off
    '
    txtCodigo.Enabled = Toggle
    txtNombre.Enabled = Toggle
    txtDirecc.Enabled = Toggle
    txtPoblac.Enabled = Toggle
    txtProvin.Enabled = Toggle
    chkVPO.Enabled = Toggle
    
    ' indica el control que va a recibir el foco
    If Toggle = True Then
       txtNombre.SetFocus
    End If
End Function
