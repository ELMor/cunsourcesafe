VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmRelTbls 
   Caption         =   "Form1"
   ClientHeight    =   3405
   ClientLeft      =   45
   ClientTop       =   270
   ClientWidth     =   3750
   LinkTopic       =   "Form1"
   ScaleHeight     =   3405
   ScaleWidth      =   3750
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2280
      TabIndex        =   1
      Top             =   3000
      Width           =   1335
   End
   Begin ComctlLib.TreeView trvUsuario 
      Height          =   2775
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   4895
      _Version        =   327682
      LabelEdit       =   1
      Style           =   7
      Appearance      =   1
   End
End
Attribute VB_Name = "frmRelTbls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'*****************************************************************
'* Muestra un TreeView con todos los objetos de base de datos
'* accesibles por el usuario que ha hecho la conexi�n
'* permite ver la construccion de los objetos y el contenido de
'* los mismos.
'*****************************************************************
Option Explicit
' relaci�n de tablas accesibles por el usuario
   ' propietario de la tabla
   Private colRelTablasP As New Collection
   ' nombre de la tabla
   Private colRelTablasT As New Collection
' relaci�n de vistas accesibles por el usuario
   ' propietario de la vista
   Private colRelVistasP As New Collection
   ' nombre de la vista
   Private colRelVistasV As New Collection
' relaci�n de: procedures,functions,packages accesibles por el usuario
   ' propietario del procedure,function,package
   Private colRelProcedP As New Collection
   ' tipo: procedure,function,package
   Private colRelProcedT As New Collection
   ' nombre del procedure,function,package
   Private colRelProcedN As New Collection
' relaci�n de triggers accesibles por el usuario
   ' propietario del trigger
   Private colRelTriggerP As New Collection
   ' nombre del trigger
   Private colRelTriggerT As New Collection

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub Form_Load()
Dim nodX As Node
Dim nodY As Node

' identificaci�n de la base de datos conectada
Me.Caption = "Base de datos conectada: " & strSrvConnect

' primer nivel
Set nodX = trvUsuario.Nodes.Add(, , "C1", _
                             "Usuario conectado: " & strUidConnect)
' segundo nivel
Set nodY = trvUsuario.Nodes.Add("C1", tvwChild, _
                    "C2T", "Tablas")
Set nodY = trvUsuario.Nodes.Add("C1", tvwChild, _
                    "C2V", "Vistas")
Set nodY = trvUsuario.Nodes.Add("C1", tvwChild, _
                    "C2F", "Procedures, Functions, Packages")
Set nodY = trvUsuario.Nodes.Add("C1", tvwChild, _
                    "C2R", "Triggers")
Set nodY = trvUsuario.Nodes.Add("C1", tvwChild, _
                    "C2S", "Sin�nimos")
Set nodY = trvUsuario.Nodes.Add("C1", tvwChild, _
                    "C2Q", "Secuencias")
nodX.Expanded = True

End Sub


Private Sub Form_Resize()
    trvUsuario.Width = Me.ScaleWidth - (trvUsuario.Left * 2)
    trvUsuario.Height = Me.ScaleHeight - _
        (trvUsuario.Top * 3) - cmdCancelar.Height
    cmdCancelar.Top = trvUsuario.Height + _
        (trvUsuario.Top * 2)
End Sub

Private Sub trvUsuario_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then  ' enter
     trvUsuario_DblClick
  End If
End Sub

Private Sub trvUsuario_DblClick()
    Dim Node As Node
    Dim nodX As Node
    Dim strA As String
    Dim strPropiet As String
    Dim strTabla As String
    Dim strVista As String
    Dim strProced As String
    Dim strTipo As String
    Dim strTrigger As String
    Dim qdQuery As rdoQuery
    Dim rsResultset As rdoResultset
    
Set Node = trvUsuario.SelectedItem ' nodo seleccionado
Select Case Left(Node.Key, 3)
 Case "C2T" ' nivel 2 - tablas
  If Node.Children = 0 Then
   ' relaci�n de tablas accesibles por el usuario
   Screen.MousePointer = vbHourglass
   strA = "SELECT ALL_TABLES.TABLE_NAME TABLA, " & _
       "ALL_TABLES.OWNER PROPIET, " & _
       "SUBSTR(ALL_TAB_COMMENTS.COMMENTS,1,50) COMENTARIO " & _
       "From ALL_TABLES, ALL_TAB_COMMENTS " & _
       "WHERE ALL_TABLES.TABLE_NAME = ALL_TAB_COMMENTS.TABLE_NAME (+) " & _
       "AND ALL_TABLES.OWNER = ALL_TAB_COMMENTS.OWNER (+) " & _
       "ORDER BY  ALL_TABLES.OWNER,ALL_TABLES.TABLE_NAME"
   Set qdQuery = conBaseDatos.CreateQuery("", strA)
   ' qdQuery.MaxRows = 2
   Set rsResultset = qdQuery.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   Do Until rsResultset.EOF
    colRelTablasP.Add rsResultset.rdoColumns("PROPIET").Value
    colRelTablasT.Add rsResultset.rdoColumns("TABLA").Value
    Set nodX = trvUsuario.Nodes.Add("C2T", tvwChild, _
                    "C3T" & Format(colRelTablasT.Count, "00000"), _
                    rsResultset.rdoColumns("PROPIET") & "." & _
                    rsResultset.rdoColumns("TABLA") & "    " & _
                    rsResultset.rdoColumns("COMENTARIO"))
    rsResultset.MoveNext
   Loop
   rsResultset.Close
   qdQuery.Close
   Screen.MousePointer = vbDefault
  End If
 
 Case "C2V" ' nivel 2 - vistas
  If Node.Children = 0 Then
   ' relaci�n de vistas accesibles por el usuario
   Screen.MousePointer = vbHourglass
   strA = "SELECT ALL_VIEWS.VIEW_NAME VISTA, " & _
       "ALL_VIEWS.OWNER PROPIET, " & _
       "SUBSTR(ALL_TAB_COMMENTS.COMMENTS,1,50) COMENTARIO " & _
       "From ALL_VIEWS, ALL_TAB_COMMENTS " & _
       "WHERE ALL_VIEWS.VIEW_NAME = ALL_TAB_COMMENTS.TABLE_NAME (+) " & _
       "AND ALL_VIEWS.OWNER = ALL_TAB_COMMENTS.OWNER (+) " & _
       "ORDER BY  ALL_VIEWS.OWNER,ALL_VIEWS.VIEW_NAME"
   Set qdQuery = conBaseDatos.CreateQuery("", strA)
   ' qdQuery.MaxRows = 2
   Set rsResultset = qdQuery.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   Do Until rsResultset.EOF
    colRelVistasP.Add rsResultset.rdoColumns("PROPIET").Value
    colRelVistasV.Add rsResultset.rdoColumns("VISTA").Value
    Set nodX = trvUsuario.Nodes.Add("C2V", tvwChild, _
                    "C3V" & Format(colRelVistasV.Count, "00000"), _
                    rsResultset.rdoColumns("PROPIET") & "." & _
                    rsResultset.rdoColumns("VISTA") & "    " & _
                    rsResultset.rdoColumns("COMENTARIO"))
    rsResultset.MoveNext
   Loop
   rsResultset.Close
   qdQuery.Close
   Screen.MousePointer = vbDefault
  End If
 
 Case "C2F" ' nivel 2 - procedures, functions, packages
  If Node.Children = 0 Then
   ' relaci�n de: procedures, functions, packages accesibles por el usuario
   Screen.MousePointer = vbHourglass
   strA = "SELECT OWNER, " & _
     "OBJECT_NAME, " & _
     "OBJECT_TYPE " & _
     "From SYS.ALL_OBJECTS " & _
     "WHERE OBJECT_TYPE = 'PROCEDURE' OR " & _
     "OBJECT_TYPE = 'FUNCTION' OR " & _
     "OBJECT_TYPE = 'PACKAGE' " & _
     "ORDER BY OWNER, " & _
     "OBJECT_TYPE, " & _
     "OBJECT_NAME "
   Set qdQuery = conBaseDatos.CreateQuery("", strA)
   ' qdQuery.MaxRows = 2
   Set rsResultset = qdQuery.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   Do Until rsResultset.EOF
    colRelProcedP.Add rsResultset.rdoColumns("OWNER").Value
    colRelProcedT.Add rsResultset.rdoColumns("OBJECT_TYPE").Value
    colRelProcedN.Add rsResultset.rdoColumns("OBJECT_NAME").Value
    Set nodX = trvUsuario.Nodes.Add("C2F", tvwChild, _
                    "C3F" & Format(colRelProcedN.Count, "00000"), _
                    rsResultset.rdoColumns("OWNER") & "." & _
                    rsResultset.rdoColumns("OBJECT_NAME") & "    (" & _
                    rsResultset.rdoColumns("OBJECT_TYPE") & ")")
    rsResultset.MoveNext
   Loop
   rsResultset.Close
   qdQuery.Close
   Screen.MousePointer = vbDefault
  End If
 
 Case "C2R" ' nivel 2 - triggers
  If Node.Children = 0 Then
   ' relaci�n de triggers accesibles por el usuario
   Screen.MousePointer = vbHourglass
   strA = "SELECT OWNER, " & _
       "TRIGGER_NAME," & _
       "TABLE_OWNER, " & _
       "TABLE_NAME " & _
       "FROM ALL_TRIGGERS"
   Set qdQuery = conBaseDatos.CreateQuery("", strA)
   Set rsResultset = qdQuery.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   Do Until rsResultset.EOF
    colRelTriggerP.Add rsResultset.rdoColumns("OWNER").Value
    colRelTriggerT.Add rsResultset.rdoColumns("TRIGGER_NAME").Value
    Set nodX = trvUsuario.Nodes.Add("C2R", tvwChild, _
                    "C3R" & Format(colRelTriggerT.Count, "00000"), _
                    rsResultset.rdoColumns("OWNER") & "." & _
                    rsResultset.rdoColumns("TRIGGER_NAME") & _
                    "    (Tabla: " & _
                    rsResultset.rdoColumns("TABLE_OWNER") & "." & _
                    rsResultset.rdoColumns("TABLE_NAME") & ")")
    rsResultset.MoveNext
   Loop
   rsResultset.Close
   qdQuery.Close
   Screen.MousePointer = vbDefault
  End If
 
 Case "C2S" ' nivel 2 - sininimos
  MostrarSinonimos
 
 Case "C2Q" ' nivel 2 - secuencias
  MostrarSecuencias
 
 Case "C3T" ' nivel 3 - cada una de las tablas
  If Node.Children = 0 Then
   ' indicar que se puede hacer con cada una de las tablas
   strA = "C4" & Mid(Node.Key, 3, 6)
   Set nodX = trvUsuario.Nodes.Add(Node.Key, tvwChild, _
                  strA & "D", "Relaci�n de campos")
   Set nodX = trvUsuario.Nodes.Add(Node.Key, tvwChild, _
                  strA & "C", "Contenido de la tabla")
  End If
 
 Case "C3V" ' nivel 3 - cada una de las vistas
  If Node.Children = 0 Then
   ' indicar que se puede hacer con cada una de las vistas
   strA = "C4" & Mid(Node.Key, 3, 6)
   Set nodX = trvUsuario.Nodes.Add(Node.Key, tvwChild, _
                  strA & "F", "C�digo fuente de la vista")
   Set nodX = trvUsuario.Nodes.Add(Node.Key, tvwChild, _
                  strA & "D", "Relaci�n de Campos")
   Set nodX = trvUsuario.Nodes.Add(Node.Key, tvwChild, _
                  strA & "C", "Ejecuci�n de la vista")
  End If
 
 Case "C3F" ' nivel 3 - cada uno de los procedures,functions,packages
  If Node.Children = 0 Then
   ' indicar que se puede hacer con cada uno de los objetos
   strA = "C4" & Mid(Node.Key, 3, 6)
   Set nodX = trvUsuario.Nodes.Add(Node.Key, tvwChild, _
                  strA & "F", "C�digo fuente del objeto")
  End If
 
 Case "C3R" ' nivel 3 - cada uno de los triggers
  If Node.Children = 0 Then
   ' indicar que se puede hacer con cada uno de los tiggers
   strA = "C4" & Mid(Node.Key, 3, 6)
   Set nodX = trvUsuario.Nodes.Add(Node.Key, tvwChild, _
                  strA & "F", "C�digo fuente del trigger")
  End If
 
 Case "C4T" ' nivel 4 - mostrar informaci�n de una tabla concreta
  strPropiet = colRelTablasP.Item(Val(Mid(Node.Key, 4, 5)))
  strTabla = colRelTablasT.Item(Val(Mid(Node.Key, 4, 5)))
  Select Case Mid(Node.Key, 9, 1)
   Case "D" ' nivel 4 - mostrar la relacion de campos de una tabla
     MostrarCamposTabla strPropiet, strTabla
   Case "C" ' nivel 4 - mostrar el contenido de una tabla
     MostrarConteTabla strPropiet, strTabla
  End Select
 
 Case "C4V" ' nivel 4 - mostrar informaci�n de una vista concreta
  strPropiet = colRelVistasP.Item(Val(Mid(Node.Key, 4, 5)))
  strVista = colRelVistasV.Item(Val(Mid(Node.Key, 4, 5)))
  Select Case Mid(Node.Key, 9, 1)
   Case "F" ' nivel 4 - mostrar el codigo fuente de la vista
     MostrarFuenteVista strPropiet, strVista
   Case "D" ' nivel 4 - mostrar la relacion de campos de una vista
     MostrarCamposVista strPropiet, strVista
   Case "C" ' nivel 4 - ejecucion de la vista
     MostrarConteVista strPropiet, strVista
  End Select
 
 Case "C4F" ' nivel 4 - mostrar informaci�n de un procedure,
            '           function,package concreta
  strPropiet = colRelProcedP.Item(Val(Mid(Node.Key, 4, 5)))
  strProced = colRelProcedN.Item(Val(Mid(Node.Key, 4, 5)))
  strTipo = colRelProcedT.Item(Val(Mid(Node.Key, 4, 5)))
  Select Case Mid(Node.Key, 9, 1)
   Case "F" ' nivel 4 - mostrar el codigo fuente del objeto
     MostrarFuenteProced strPropiet, strTipo, strProced
  End Select
 
 Case "C4R" ' nivel 4 - mostrar informaci�n de un trigger
  strPropiet = colRelTriggerP.Item(Val(Mid(Node.Key, 4, 5)))
  strTrigger = colRelTriggerT.Item(Val(Mid(Node.Key, 4, 5)))
  Select Case Mid(Node.Key, 9, 1)
   Case "F" ' nivel 4 - mostrar el codigo fuente del trigger
     MostrarFuenteTrigger strPropiet, strTrigger
  End Select

End Select
    
    
End Sub
Private Sub MostrarCamposTabla(ByVal Propiet As String, _
                               ByVal Tabla As String)
  ' mostrar por pantalla la relacion de campos de la tabla indicada
   Dim frmM As New frmMostrar
   Dim intI As Integer
   Dim strA As String
   Dim strB As String
   Dim strC As String
   Dim strD As String
   Dim strE As String
   Dim strColumna As String
   Dim intColumna As Integer
   Dim qdQuery As rdoQuery
   Dim rsResultset As rdoResultset
   Dim qdQuery1 As rdoQuery
   Dim rsResultset1 As rdoResultset
   Dim qdIndices As rdoQuery
   Dim rsIndices As rdoResultset
   Dim qdIndColumnas As rdoQuery
   Dim rsIndColumnas As rdoResultset
  
'  On Error GoTo LocalErr
   
   Screen.MousePointer = vbHourglass
   
   frmM.Caption = "Relaci�n de campos de la tabla: " & Propiet & _
                  "." & Tabla
   
   ' mostrar el comentario de la tabla
   strA = "SELECT  TABLE_NAME, " & _
          "SUBSTR(ALL_TAB_COMMENTS.COMMENTS,1,50) COMENTARIO " & _
          "From ALL_TAB_COMMENTS " & _
          "WHERE TABLE_NAME = '" & Tabla & "' AND " & _
          "OWNER = '" & Propiet & "'"
   Set qdQuery = conBaseDatos.CreateQuery("", strA)
   Set rsResultset = qdQuery.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   strA = "Tabla: " & Propiet & "." & Tabla & "   " & _
          rsResultset("COMENTARIO")
   frmM.txtMostrar.Text = strA & vbCrLf
   rsResultset.Close
   qdQuery.Close
   
   
   ' crear un resultset con las columnas de la tabla
   strA = "SELECT ALL_TAB_COLUMNS.COLUMN_NAME COLUMNA, " & _
          "ALL_TAB_COLUMNS.DATA_TYPE DATA_TYPE, " & _
          "TO_CHAR(ALL_TAB_COLUMNS.DATA_LENGTH,'9999') LONGI, " & _
          "TO_CHAR(ALL_TAB_COLUMNS.DATA_PRECISION,'9999') || " & _
          "TO_CHAR(ALL_TAB_COLUMNS.DATA_SCALE,'9') PRECI, " & _
          "ALL_TAB_COLUMNS.NULLABLE NUL, " & _
          "SUBSTR(ALL_COL_COMMENTS.Comments,1,500) COMENTARIO " & _
          "From ALL_TAB_COLUMNS, ALL_COL_COMMENTS, ALL_OBJECTS " & _
          "WHERE ALL_TAB_COLUMNS.TABLE_NAME = '" & Tabla & "' AND " & _
          "ALL_TAB_COLUMNS.OWNER = '" & Propiet & "' AND " & _
          "ALL_TAB_COLUMNS.TABLE_NAME = ALL_OBJECTS.OBJECT_NAME (+) AND " & _
          "ALL_TAB_COLUMNS.OWNER = ALL_OBJECTS.OWNER (+) AND " & _
          "ALL_OBJECTS.OBJECT_TYPE = 'TABLE' AND " & _
          "ALL_TAB_COLUMNS.TABLE_NAME  = ALL_COL_COMMENTS.TABLE_NAME (+) AND " & _
          "ALL_TAB_COLUMNS.OWNER  = ALL_COL_COMMENTS.OWNER (+) AND " & _
          "ALL_TAB_COLUMNS.COLUMN_NAME = ALL_COL_COMMENTS.COLUMN_NAME (+) " & _
          "ORDER BY  ALL_TAB_COLUMNS.COLUMN_ID"
   Set qdQuery = conBaseDatos.CreateQuery("", strA)
   Set rsResultset = qdQuery.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   
   ' crear un resultset con los indices de la tabla
   strA = "SELECT INDEX_NAME, " & _
          "SUBSTR(UNIQUENESS,1,1) UNICO " & _
          "From ALL_INDEXES " & _
          "WHERE TABLE_NAME = '" & Tabla & "' AND " & _
          "OWNER = '" & Propiet & "'"
   Set qdIndices = conBaseDatos.CreateQuery("", strA)
   Set rsIndices = qdIndices.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   
   ' crear un resultset con la relaci�n de columnas que forman
   ' parte de los indices
   strA = "SELECT INDEX_NAME, " & _
          "COLUMN_NAME, " & _
          "COLUMN_POSITION " & _
          "From ALL_IND_COLUMNS " & _
          "WHERE TABLE_NAME = '" & Tabla & "' AND " & _
          "TABLE_OWNER = '" & Propiet & "' " & _
          "ORDER BY INDEX_NAME, " & _
          "COLUMN_POSITION  "
   Set qdIndColumnas = conBaseDatos.CreateQuery("", strA)
   Set rsIndColumnas = qdIndColumnas.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   
       
   ' crear un resulset con las constrains de las columnas
   strB = "SELECT ALL_CONSTRAINTS.CONSTRAINT_TYPE TIPO, " & _
          "ALL_CONS_COLUMNS.COLUMN_NAME COLUMNA " & _
          "FROM ALL_CONS_COLUMNS, " & _
          "ALL_CONSTRAINTS " & _
          "WHERE ALL_CONS_COLUMNS.TABLE_NAME = '" & Tabla & _
          "' AND " & _
          "ALL_CONS_COLUMNS.OWNER = '" & Propiet & "' AND " & _
          "ALL_CONS_COLUMNS.CONSTRAINT_NAME = " & _
          "ALL_CONSTRAINTS.CONSTRAINT_NAME"
   Set qdQuery1 = conBaseDatos.CreateQuery("", strB)
   Set rsResultset1 = qdQuery1.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   
   ' mostrar la relacion de campos con sus atributos
   strA = R_StrLenPed("COLUMNA", 25)
   strA = strA & " " & "TIPO    "
   strA = strA & " " & "LONGITUD"
   strA = strA & " " & "NULO"
   strA = strA & " " & "CONSTRAINTS, INDICES"
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
   strA = String(25, "-")
   strA = strA & " " & "--------"
   strA = strA & " " & "--------"
   strA = strA & " " & "----"
   strA = strA & " " & String(40, "-")
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
   rsResultset.MoveFirst
   Do Until rsResultset.EOF
    strColumna = rsResultset.rdoColumns("COLUMNA").Value
    strA = R_StrLenPed(strColumna, 25)
    strA = strA & " " & R_StrLenPed(rsResultset.rdoColumns("DATA_TYPE").Value, 8)
    strB = rsResultset.rdoColumns("DATA_TYPE").Value
    If strB = "NUMBER" Then
      strA = strA & " " & R_StrLenPed(rsResultset.rdoColumns("PRECI").Value, 8)
    Else
      strA = strA & " " & R_StrLenPed(rsResultset.rdoColumns("LONGI").Value, 8)
    End If
    strA = strA & " " & _
      IIf(rsResultset.rdoColumns("NUL").Value = "Y", " SI ", "    ")
       
       
    ' ver si la columna es Primary Key y/o Foreign Key
    strC = " " ' indica si es primary key
    strD = " " ' indica si es foreign key
    If Not rsResultset1.BOF Then
       rsResultset1.MoveFirst
       Do Until rsResultset1.EOF
        If rsResultset1.rdoColumns("COLUMNA").Value = strColumna Then
          If rsResultset1.rdoColumns("TIPO").Value = "P" Then
             strC = "P" ' forma parte de la primary key
          End If
          If rsResultset1.rdoColumns("TIPO").Value = "R" Then
             strD = "F" ' forma parte de una foreign key
          End If
        End If
        rsResultset1.MoveNext
       Loop
    End If
    strA = strA & " " & strC & " " & strD
   
    ' indicar los indices de los que forma parte la columna
    If Not rsIndices.BOF Then
      rsIndices.MoveFirst
      Do Until rsIndices.EOF
       strC = "  "
       rsIndColumnas.MoveFirst
       Do Until rsIndColumnas.EOF
          If rsIndColumnas.rdoColumns("COLUMN_NAME").Value = strColumna _
             And rsIndices.rdoColumns("INDEX_NAME").Value = _
                 rsIndColumnas.rdoColumns("INDEX_NAME").Value Then
             strC = rsIndices.rdoColumns("UNICO").Value & _
                  Format(rsIndColumnas.rdoColumns("COLUMN_POSITION").Value, "0")
           End If
          rsIndColumnas.MoveNext
       Loop
       strA = strA & " " & strC
       rsIndices.MoveNext
      Loop
    End If
    
   
   
    frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
    rsResultset.MoveNext
   Loop
   
   
   
   ' mostrar la relacion de campos con sus comentarios
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & vbCrLf
   strA = R_StrLenPed("COLUMNA", 25)
   strA = strA & " " & "COMENTARIO"
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
   strA = String(25, "-")
   strA = strA & " " & String(65, "-")
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
   rsResultset.MoveFirst
   Do Until rsResultset.EOF
    strA = R_StrLenPed(rsResultset.rdoColumns("COLUMNA").Value, 25)
    ' mostrar los comentarios
       strB = IIf(IsNull(rsResultset("COMENTARIO")), " ", _
                         rsResultset("COMENTARIO"))
       ' columna en la que se muestra el comentario
          intColumna = Len(strA)
       intI = 0
       ' si el comentario ocupa mas de 65 caracteres, mostrarlo en
       ' varias lineas
       Do While Len(strB) > 0
          intI = intI + 1
          If Len(strB) > 65 Then
            strC = Mid(strB, 1, 65)
            strB = Mid(strB, 66)
          Else
            strC = strB
            strB = ""
          End If
          If intI = 1 Then
             strA = strA & " " & strC
          Else
             strA = Space(intColumna) & " " & strC
          End If
          frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
       Loop
    rsResultset.MoveNext
   Loop
   
   ' mostrar la relaci�n de indices con sus campos
    If Not rsIndices.BOF Then
      rsIndices.MoveFirst
      Do Until rsIndices.EOF
       frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf
       strA = "Indice: " & rsIndices.rdoColumns("INDEX_NAME").Value
       strA = strA & " (" & IIf(rsIndices.rdoColumns("UNICO").Value = "U", _
                               "Unico", "No Unico") & ")"
       frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
       rsIndColumnas.MoveFirst
       Do Until rsIndColumnas.EOF
          If rsIndices.rdoColumns("INDEX_NAME").Value = _
             rsIndColumnas.rdoColumns("INDEX_NAME").Value Then
             strA = "      " & rsIndColumnas.rdoColumns("COLUMN_NAME").Value
             frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
           End If
          rsIndColumnas.MoveNext
       Loop
       rsIndices.MoveNext
      Loop
    End If
   
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf
   
   rsResultset.Close
   qdQuery.Close
   rsResultset1.Close
   qdQuery1.Close
   rsIndices.Close
   qdIndices.Close
   rsIndColumnas.Close
   qdIndColumnas.Close
   
   frmM.cmdContinuar.Visible = False
   Screen.MousePointer = vbDefault
   frmM.Show vbModal
   Unload frmM
   Set frmM = Nothing
   Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub

Private Sub MostrarConteTabla(ByVal Propiet As String, _
                               ByVal Tabla As String)
  ' mostrar por pantalla el contenido de una tabla
  ' pasos:  - preparar la SELECT a ejecutar
  '         - dar opci�n a modificar la SELECT
  '         - ejecutar la SELECT en un GRID
   Dim strA As String
   Dim qd As rdoQuery
   Dim rs As rdoResultset
  
'  On Error GoTo LocalErr
   
   Screen.MousePointer = vbHourglass
   
   ' crear un resultset con las columnas de la tabla
   strA = "SELECT ALL_TAB_COLUMNS.COLUMN_NAME COLUMNA, " & _
          "ALL_TAB_COLUMNS.DATA_TYPE DATA_TYPE, " & _
          "ALL_TAB_COLUMNS.DATA_LENGTH LONGI, " & _
          "ALL_TAB_COLUMNS.DATA_PRECISION PRECIS, " & _
          "ALL_TAB_COLUMNS.DATA_SCALE ESCALA " & _
          "From ALL_TAB_COLUMNS " & _
          "WHERE ALL_TAB_COLUMNS.TABLE_NAME = '" & Tabla & "' AND " & _
          "ALL_TAB_COLUMNS.OWNER = '" & Propiet & "' " & _
          "ORDER BY  ALL_TAB_COLUMNS.COLUMN_ID"
   Set qd = conBaseDatos.CreateQuery("", strA)
   Set rs = qd.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
       
   
   ' preparar la SELECT a ejecutar
   strA = "SELECT" & vbCrLf
   rs.MoveFirst
   Do Until rs.EOF
    strA = strA & "    " & rs.rdoColumns("COLUMNA").Value
    rs.MoveNext
    If Not rs.EOF Then
      strA = strA & ","
    End If
    strA = strA & vbCrLf
   Loop
   strA = strA & "FROM " & Propiet & "." & Tabla & vbCrLf
   rs.Close
   qd.Close
   
   Screen.MousePointer = vbDefault
   
   ' editar la select, ejecutarla y mostrar la salida por pantalla
   EditarEjecutarSelect strA, "Tabla: " & Propiet & "." & Tabla, "G"
  
  Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub

Private Sub MostrarFuenteVista(ByVal Propiet As String, _
                               ByVal Vista As String)
  ' mostrar por pantalla el codigo fuente de una vista
   Dim frmM As New frmMostrar  ' mostrar caja de texto
   Dim strA As String
   Dim qd As rdoQuery
   Dim rs As rdoResultset
  
'  On Error GoTo LocalErr
   
   Screen.MousePointer = vbHourglass
   frmM.Caption = "Vista: " & Propiet & "." & Vista
   
   strA = "SELECT OWNER, VIEW_NAME, TEXT_LENGTH, " & _
          "TEXT " & _
          "From ALL_VIEWS " & _
          "WHERE VIEW_NAME = '" & Vista & "' AND " & _
          "OWNER = '" & Propiet & "' "
   Set qd = conBaseDatos.CreateQuery("", strA)
   Set rs = qd.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
       
   strA = rs.rdoColumns("TEXT").Value
   strA = R_ChrSwap(strA, Chr(10), vbCrLf)
   frmM.txtMostrar.Text = strA
   
   frmM.cmdContinuar.Visible = False
   Screen.MousePointer = vbDefault
   frmM.Show vbModal
  
   rs.Close
   qd.Close
  
  Unload frmM
  Set frmM = Nothing
  
  Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub


Private Sub MostrarCamposVista(ByVal Propiet As String, _
                               ByVal Vista As String)
  ' mostrar por pantalla la relacion de campos de la tabla indicada
   Dim frmM As New frmMostrar
   Dim strA As String
   Dim strB As String
   Dim strC As String
   Dim strD As String
   Dim strE As String
   Dim intI As Integer
   Dim strColumna As String
   Dim intColumna As Integer
   Dim qdQuery As rdoQuery
   Dim rsResultset As rdoResultset
  
'  On Error GoTo LocalErr
   
   Screen.MousePointer = vbHourglass
   frmM.Caption = "Relaci�n de campos de la vista: " & Propiet & _
                  "." & Vista
   
   ' crear un resultset con las columnas de la vista
   strA = "SELECT ALL_TAB_COLUMNS.COLUMN_NAME COLUMNA, " & _
          "ALL_TAB_COLUMNS.DATA_TYPE DATA_TYPE, " & _
          "TO_CHAR(ALL_TAB_COLUMNS.DATA_LENGTH,'9999') LONGI, " & _
          "TO_CHAR(ALL_TAB_COLUMNS.DATA_PRECISION,'9999') || " & _
          "TO_CHAR(ALL_TAB_COLUMNS.DATA_SCALE,'9') PRECI, " & _
          "ALL_TAB_COLUMNS.NULLABLE NUL, " & _
          "SUBSTR(ALL_COL_COMMENTS.Comments,1,500) COMENTARIO " & _
          "From ALL_TAB_COLUMNS, ALL_COL_COMMENTS, ALL_OBJECTS " & _
          "WHERE ALL_TAB_COLUMNS.TABLE_NAME = '" & Vista & "' AND " & _
          "ALL_TAB_COLUMNS.OWNER = '" & Propiet & "' AND " & _
          "ALL_TAB_COLUMNS.TABLE_NAME = ALL_OBJECTS.OBJECT_NAME (+) AND " & _
          "ALL_OBJECTS.OBJECT_TYPE = 'VIEW' AND " & _
          "ALL_TAB_COLUMNS.TABLE_NAME  = ALL_COL_COMMENTS.TABLE_NAME (+) AND " & _
          "ALL_TAB_COLUMNS.COLUMN_NAME = ALL_COL_COMMENTS.COLUMN_NAME (+) " & _
          "ORDER BY  ALL_TAB_COLUMNS.COLUMN_ID"
   Set qdQuery = conBaseDatos.CreateQuery("", strA)
   Set rsResultset = qdQuery.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
   
   ' mostrar la relacion de campos con sus comentarios
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & vbCrLf
   strA = R_StrLenPed("COLUMNA", 25)
   strA = strA & " " & "TIPO    "
   strA = strA & " " & "LONGITUD"
   strA = strA & " " & "NULO"
   strA = strA & " " & R_StrLenPed("COMENTARIO", 40)
   frmM.txtMostrar.Text = strA
   strA = String(25, "-")
   strA = strA & " " & "--------"
   strA = strA & " " & "--------"
   strA = strA & " " & "----"
   strA = strA & " " & String(40, "-")
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
   Do Until rsResultset.EOF
    strA = R_StrLenPed(rsResultset.rdoColumns("COLUMNA").Value, 25)
    strA = strA & " " & R_StrLenPed(rsResultset.rdoColumns("DATA_TYPE").Value, 8)
    strB = rsResultset.rdoColumns("DATA_TYPE").Value
    If strB = "NUMBER" Then
      strA = strA & " " & R_StrLenPed(rsResultset.rdoColumns("PRECI").Value, 8)
    Else
      strA = strA & " " & R_StrLenPed(rsResultset.rdoColumns("LONGI").Value, 8)
    End If
    strA = strA & " " & _
      IIf(rsResultset.rdoColumns("NUL").Value = "Y", " SI ", "    ")
    
    ' mostrar los comentarios
       strB = IIf(IsNull(rsResultset("COMENTARIO")), " ", _
                         rsResultset("COMENTARIO"))
       ' columna en la que se muestra el comentario
          intColumna = Len(strA)
       intI = 0
       ' si el comentario ocupa mas de 40 caracteres, mostrarlo en
       ' varias lineas
       Do While Len(strB) > 0
          intI = intI + 1
          If Len(strB) > 40 Then
            strC = Mid(strB, 1, 40)
            strB = Mid(strB, 41)
          Else
            strC = strB
            strB = ""
          End If
          If intI = 1 Then
             strA = strA & " " & strC
          Else
             strA = Space(intColumna) & " " & strC
          End If
          frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
       Loop
    rsResultset.MoveNext
   Loop
   
   
   rsResultset.Close
   qdQuery.Close
   
   frmM.cmdContinuar.Visible = False
   Screen.MousePointer = vbDefault
   frmM.Show vbModal
   Unload frmM
   Set frmM = Nothing
   Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub



Private Sub MostrarConteVista(ByVal Propiet As String, _
                               ByVal Vista As String)
  ' mostrar por pantalla el contenido de una vista
  ' pasos:  - preparar la SELECT a ejecutar
  '         - dar opci�n a modificar la SELECT
  '         - ejecutar la SELECT en un GRID
   Dim strA As String
   Dim qd As rdoQuery
   Dim rs As rdoResultset
  
'  On Error GoTo LocalErr
   
   Screen.MousePointer = vbHourglass
   
   ' crear un resultset con las columnas de la tabla
   strA = "SELECT ALL_TAB_COLUMNS.COLUMN_NAME COLUMNA, " & _
          "ALL_TAB_COLUMNS.DATA_TYPE DATA_TYPE, " & _
          "ALL_TAB_COLUMNS.DATA_LENGTH LONGI, " & _
          "ALL_TAB_COLUMNS.DATA_PRECISION PRECIS, " & _
          "ALL_TAB_COLUMNS.DATA_SCALE ESCALA " & _
          "From ALL_TAB_COLUMNS " & _
          "WHERE ALL_TAB_COLUMNS.TABLE_NAME = '" & Vista & "' AND " & _
          "ALL_TAB_COLUMNS.OWNER = '" & Propiet & "' " & _
          "ORDER BY  ALL_TAB_COLUMNS.COLUMN_ID"
   Set qd = conBaseDatos.CreateQuery("", strA)
   Set rs = qd.OpenResultset(rdOpenKeyset, _
                           rdConcurReadOnly)
       
   ' preparar la SELECT a ejecutar
   strA = "SELECT" & vbCrLf
   rs.MoveFirst
   Do Until rs.EOF
    strA = strA & "    " & rs("COLUMNA")
    rs.MoveNext
    If Not rs.EOF Then
      strA = strA & ","
    End If
    strA = strA & vbCrLf
   Loop
   strA = strA & "FROM " & Propiet & "." & Vista & vbCrLf
   rs.Close
   qd.Close
   
   Screen.MousePointer = vbDefault
   
   ' editar la select, ejecutarla y mostrar la salida por pantalla
   EditarEjecutarSelect strA, "Vista: " & Propiet & "." & Vista, "G"
   
   
  
  Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub


Private Sub MostrarFuenteProced(ByVal Propiet As String, _
                               ByVal Tipo As String, _
                               ByVal Proced As String)
  ' mostrar por pantalla el codigo fuente de un procedure,
  ' function o package
   Dim frmM As New frmMostrar  ' mostrar caja de texto
   Dim strA As String
   Dim qd As rdoQuery
   Dim rs As rdoResultset
  
'  On Error GoTo LocalErr
   
   Screen.MousePointer = vbHourglass
   frmM.Caption = Tipo & ": " & Propiet & "." & Proced
   strA = "SELECT LINE, " & _
          "TEXT " & _
          "From ALL_SOURCE " & _
          "WHERE OWNER = '" & Propiet & "' AND " & _
          "NAME = '" & Proced & "' AND " & _
          "TYPE = '" & Tipo & "'"
   
   strA = "SELECT " & _
          "SUBSTR(TEXT,1,150) TEXT " & _
          "From ALL_SOURCE " & _
          "WHERE OWNER = '" & Propiet & "' AND " & _
          "NAME = '" & Proced & "' AND " & _
          "TYPE = '" & Tipo & "'"
   
   
   Set qd = conBaseDatos.CreateQuery("", strA)
   Set rs = qd.OpenResultset(rdOpenForwardOnly)
       
   frmM.txtMostrar.Text = " "
   Do Until rs.EOF
    strA = rs("TEXT")
    strA = R_ChrSwap(strA, Chr(10), "")
    frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & strA
    rs.MoveNext
   Loop
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf
       
   frmM.cmdContinuar.Visible = False
   Screen.MousePointer = vbDefault
   frmM.Show vbModal
  
   rs.Close
   qd.Close
  
  Unload frmM
  Set frmM = Nothing
  
  Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub


Private Sub MostrarFuenteTrigger(ByVal Propiet As String, _
                               ByVal Trigger As String)
  ' mostrar por pantalla el codigo fuente de un trigger
   Dim frmM As New frmMostrar  ' mostrar caja de texto
   Dim strA As String
   Dim qd As rdoQuery
   Dim rs As rdoResultset
  
'  On Error GoTo LocalErr
   
   Screen.MousePointer = vbHourglass
   frmM.Caption = "Trigger: " & Propiet & "." & Trigger
   
   strA = "SELECT DESCRIPTION, " & _
          "OWNER, " & _
          "TRIGGER_NAME, " & _
          "WHEN_CLAUSE, " & _
          "DESCRIPTION, " & _
          "TRIGGER_BODY " & _
          "From ALL_TRIGGERS " & _
          "WHERE OWNER = '" & Propiet & "' AND " & _
          "TRIGGER_NAME = '" & Trigger & "'"
   
   Set qd = conBaseDatos.CreateQuery("", strA)
'  Set rs = qd.OpenResultset(rdOpenForwardOnly)
   Set rs = qd.OpenResultset(rdOpenDynamic)
       
   strA = "DESCRIPTION:"
   frmM.txtMostrar.Text = strA
   strA = rs("DESCRIPTION")
   strA = R_ChrSwap(strA, Chr(10), vbCrLf)
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & vbCrLf & strA
   
   strA = "WHEN_CLAUSE:"
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & vbCrLf & strA
   strA = IIf(IsNull(rs("WHEN_CLAUSE")), "", rs("WHEN_CLAUSE"))
   strA = R_ChrSwap(strA, Chr(10), vbCrLf)
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & vbCrLf & strA
   
   strA = "TRIGGER_BODY:"
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & vbCrLf & strA
   strA = rs("TRIGGER_BODY")
   strA = R_ChrSwap(strA, Chr(10), vbCrLf)
   frmM.txtMostrar.Text = frmM.txtMostrar.Text & vbCrLf & vbCrLf & strA
       
   frmM.cmdContinuar.Visible = False
   Screen.MousePointer = vbDefault
   frmM.Show vbModal
  
   rs.Close
   qd.Close
  
  Unload frmM
  Set frmM = Nothing
  
  Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub

Private Sub MostrarSinonimos()
  ' mostrar la selaci�n de sinonimos accesibles por
  ' el usuario
   Dim strA As String
  
'  On Error GoTo LocalErr
  
   ' preparar la SELECT a ejecutar
   strA = "SELECT" & vbCrLf
   strA = strA & "    ALL_SYNONYMS.OWNER," & vbCrLf
   strA = strA & "    ALL_SYNONYMS.SYNONYM_NAME," & vbCrLf
   strA = strA & "    ALL_SYNONYMS.TABLE_OWNER OBJECT_OWNER," & vbCrLf
   strA = strA & "    ALL_SYNONYMS.TABLE_NAME  OBJECT_NAME," & vbCrLf
   strA = strA & "    ALL_OBJECTS.OBJECT_TYPE OBJECT_TYPE" & vbCrLf
   strA = strA & "FROM ALL_SYNONYMS," & vbCrLf
   strA = strA & "     ALL_OBJECTS" & vbCrLf
   strA = strA & "WHERE ALL_SYNONYMS.OWNER ='" & UCase(strUidConnect) & _
                        "' AND  /* <---------------------------- */" & vbCrLf
   strA = strA & "      ALL_SYNONYMS.TABLE_OWNER = ALL_OBJECTS.OWNER (+) AND" & vbCrLf
   strA = strA & "      ALL_SYNONYMS.TABLE_NAME  = ALL_OBJECTS.OBJECT_NAME (+) AND" & vbCrLf
   strA = strA & "      ALL_SYNONYMS.SYNONYM_NAME >= 'P'  /* <------------------------- */" & vbCrLf
   strA = strA & "ORDER BY ALL_SYNONYMS.OWNER," & vbCrLf
   strA = strA & "         ALL_SYNONYMS.SYNONYM_NAME" & vbCrLf
    
   
   ' editar la select, ejecutarla y mostrar la salida por pantalla
   EditarEjecutarSelect strA, "Sin�nimos accesibles por el usuario: " & _
      strUidConnect, "G"
  
  Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub


Private Sub MostrarSecuencias()
  ' mostrar la selaci�n de secuencias accesibles por
  ' el usuario
   Dim strA As String
  
'  On Error GoTo LocalErr
  
   ' preparar la SELECT a ejecutar
   strA = "SELECT" & vbCrLf
   strA = strA & "    SEQUENCE_OWNER," & vbCrLf
   strA = strA & "    SEQUENCE_NAME," & vbCrLf
   strA = strA & "    LAST_NUMBER," & vbCrLf
   strA = strA & "    INCREMENT_BY," & vbCrLf
   strA = strA & "    MIN_VALUE," & vbCrLf
   strA = strA & "    MAX_VALUE," & vbCrLf
   strA = strA & "    CYCLE_FLAG," & vbCrLf
   strA = strA & "    ORDER_FLAG," & vbCrLf
   strA = strA & "    CACHE_SIZE" & vbCrLf
   strA = strA & "FROM ALL_SEQUENCES" & vbCrLf
   strA = strA & "WHERE SEQUENCE_OWNER ='" & UCase(strUidConnect) & _
                        "'  /* <---------------------------- */" & vbCrLf
   strA = strA & "ORDER BY SEQUENCE_OWNER," & vbCrLf
   strA = strA & "         SEQUENCE_NAME" & vbCrLf
    
   
   ' editar la select, ejecutarla y mostrar la salida por pantalla
   EditarEjecutarSelect strA, "Secuencias accesibles por el usuario: " & _
      strUidConnect, "G"
  
  Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub


