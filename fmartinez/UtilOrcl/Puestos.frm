VERSION 5.00
Begin VB.Form frmPuestos 
   Caption         =   "Obtener un Script con la relaci�n de campos de un fichero trasmitido desde el IBM"
   ClientHeight    =   3135
   ClientLeft      =   90
   ClientTop       =   375
   ClientWidth     =   4620
   LinkTopic       =   "Form1"
   ScaleHeight     =   3135
   ScaleWidth      =   4620
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtSalida 
      Height          =   375
      Left            =   3960
      TabIndex        =   5
      Text            =   "Text2"
      Top             =   1800
      Width           =   7335
   End
   Begin VB.TextBox txtEntrada 
      Height          =   375
      Left            =   3960
      TabIndex        =   3
      Text            =   "Text1"
      Top             =   840
      Width           =   7335
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "&Continuar"
      Default         =   -1  'True
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   495
      Left            =   10200
      TabIndex        =   0
      Top             =   8040
      Width           =   1575
   End
   Begin VB.Label lblSalida 
      Caption         =   "Script a generar:"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1800
      Width           =   3615
   End
   Begin VB.Label lblEntrada 
      Caption         =   "Fichero de texto recibido desde el IBM:"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   3615
   End
End
Attribute VB_Name = "frmPuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' obtener la relacion de campos de un fichero recibido desde el IBM
Option Explicit

Private Sub cmdContinuar_Click()
    Dim I As Long
    Dim J As Long
    Dim K As Long
    Dim L As Long
    Dim strA As String
    Dim strB As String
    Dim colNomCampos As New Collection
    Dim colLenCampos As New Collection
    
    On Error GoTo LocalErr

    ' La primera linea del fichero de entrada contiene la relaci�n de campos.
    ' Entre un campo y el siguiente hay por lo menos un espacio en blanco.
    ' Todos los campos estan alineados a la izquierda.
    Open txtEntrada.Text For Input As 1
    Line Input #1, strA ' leer la primera linea del registro
    
    ' relacion de campos y tama�os de los mismos
    strA = strA & " "
    Do While Len(strA) > 0
       If Mid(strA, 1, 1) = " " Then
          MsgBox ("Primera posicion en blanco" & vbCrLf & strA)
          Exit Sub
       End If
       ' posicion del primer blanco
          For K = 1 To Len(strA)
             If Mid(strA, K, 1) = " " Then
                I = K
                Exit For
             End If
          Next K
       ' posicion del comienzo del siguiente campo
          J = Len(strA) + 1
          For K = 1 To Len(strA)
             If Mid(strA, K, 1) <> " " And K > I Then
                J = K
                Exit For
             End If
          Next K
       colNomCampos.Add Mid(strA, 1, I - 1)
       colLenCampos.Add (J - 1)
       strA = Mid(strA, J)
    Loop
    
    ' genear el script
    Open txtSalida.Text For Output As 2
    Print #2, "CREATE TABLE ?????? ("
    For K = 1 To colNomCampos.Count
       strB = R_StrLenPed(colNomCampos.Item(K), 20) & _
              " VARCHAR2(" & _
              Str(colLenCampos.Item(K)) & "),"
       Print #2, strB
    Next
    Print #2, " "
    Print #2, " "
    Print #2, "Load Data"
    Print #2, "INFILE 'G:\??????.TXT'"
    Print #2, "INTO TABLE ?????? ("
    L = 1
    For K = 1 To colNomCampos.Count
       strB = R_StrLenPed(colNomCampos.Item(K), 20) & _
              " POSITION(" & _
              Str(L) & ":"
       L = L + colLenCampos.Item(K)
       strB = strB & Str(L - 1) & ") CHAR,"
       Print #2, strB
    Next
    
    Close
    
    ' mostrar en pantalla la relaci�n de campos
    strB = ""
    For K = 1 To colNomCampos.Count
       strB = strB & colNomCampos.Item(K) & " " & _
              Str(colLenCampos.Item(K)) & " "
       L = K Mod 3
       If L = 0 Then
          strB = strB & vbCrLf
       End If
    Next
    MsgBox (strB)
    
    Exit Sub
LocalErr:
    MsgBox (R_ErrorRdoMsg)
    Close
End Sub

Private Sub cmdSalir_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    txtEntrada.Text = "D:\FMRA\ORACLE\LOADER\CUC01R.TXT"
    txtSalida.Text = "D:\FMRA\ORACLE\LOADER\CUC01R.TX1"
End Sub
